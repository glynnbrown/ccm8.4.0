﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
//V8-29010 D.Pleasance
//  Created
#endregion
#region Version History: (CCM 8.3.0)
//V8-31831 : A.Probyn
//  Added PlanogramAttribute
//  Added FetchByEntityIdName
//  Removed SeparatorType
// V8-32300 : A.Probyn
//  Centralised ValidatePlanogramNameUnique method.
#endregion
#endregion

using System;
using System.Reflection;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// PlanogramNameTemplate model object
    /// </summary>
    [DefaultNewMethod("NewPlanogramNameTemplate", "EntityId")]
    [Serializable]
    public sealed partial class PlanogramNameTemplate : ModelObject<PlanogramNameTemplate>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The row version timestamp
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region EntityId

        /// <summary>
        /// EntityId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);

        /// <summary>
        /// Gets/Sets the EntityId value
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
            set { SetProperty<Int32>(EntityIdProperty, value); }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name, Message.Generic_Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion
        
        #region BuilderFormula

        /// <summary>
        /// BuilderFormula property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> BuilderFormulaProperty =
            RegisterModelProperty<String>(c => c.BuilderFormula, Message.PlanogramNameTemplate_BuilderFormula);

        /// <summary>
        /// Gets/Sets the BuilderFormula  value
        /// </summary>
        public String BuilderFormula 
        {
            get { return GetProperty<String>(BuilderFormulaProperty); }
            set { SetProperty<String>(BuilderFormulaProperty, value); }
        }

        #endregion
        
        #region PlanogramAttribute

        /// <summary>
        /// PlanogramAttribute property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramNameTemplatePlanogramAttributeType> PlanogramAttributeProperty =
            RegisterModelProperty<PlanogramNameTemplatePlanogramAttributeType>(c => c.PlanogramAttribute);

        /// <summary>
        /// Gets/Sets the PlanogramAttribute value to update
        /// </summary>
        public PlanogramNameTemplatePlanogramAttributeType PlanogramAttribute
        {
            get { return GetProperty<PlanogramNameTemplatePlanogramAttributeType>(PlanogramAttributeProperty); }
            set { SetProperty<PlanogramNameTemplatePlanogramAttributeType>(PlanogramAttributeProperty, value); }
        }

        #endregion

        #region DateCreated

        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        /// Gets/Sets the DateCreated value
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
            set { SetProperty<DateTime>(DateCreatedProperty, value); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        /// Gets/Sets the DateLastModified value
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
            set { SetProperty<DateTime>(DateLastModifiedProperty, value); }
        }

        #endregion
        
        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new MinValue<Int32>(EntityIdProperty, 1));
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
            BusinessRules.AddRule(new Required(BuilderFormulaProperty));
        }

        #endregion

        #region Authorization Rules

        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PlanogramNameTemplate), new IsInRole(AuthorizationActions.GetObject, DomainPermission.PlanogramNameTemplateGet.ToString()));
            BusinessRules.AddRule(typeof(PlanogramNameTemplate), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.PlanogramNameTemplateCreate.ToString()));
            BusinessRules.AddRule(typeof(PlanogramNameTemplate), new IsInRole(AuthorizationActions.EditObject, DomainPermission.PlanogramNameTemplateEdit.ToString()));
            BusinessRules.AddRule(typeof(PlanogramNameTemplate), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.PlanogramNameTemplateDelete.ToString()));
        }

        #endregion

        #region Criteria

        #region FetchByIdCriteria

        // <summary>
        /// Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> IdProperty =
                RegisterProperty<Int32>(c => c.Id);
            /// <summary>
            /// Returns the id
            /// </summary>
            public Int32 Id
            {
                get { return this.ReadProperty<Int32>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(Int32 id)
            {
                this.LoadProperty<Int32>(IdProperty, id);
            }
            #endregion
        }

        #endregion

        #region FetchByEntityIdName Criteria

        [Serializable]
        public class FetchByEntityIdNameCriteria : CriteriaBase<FetchByEntityIdNameCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static readonly PropertyInfo<String> NameProperty =
                RegisterProperty<String>(c => c.Name);
            public String Name
            {
                get { return ReadProperty<String>(NameProperty); }
            }

            public FetchByEntityIdNameCriteria(Int32 entityId, String name)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<String>(NameProperty, name);
            }
        }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new MetricProfile
        /// </summary>
        public static PlanogramNameTemplate NewPlanogramNameTemplate(Int32 entityId)
        {
            PlanogramNameTemplate item = new PlanogramNameTemplate();
            item.Create(entityId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 entityId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.LoadProperty<PlanogramNameTemplatePlanogramAttributeType>(PlanogramAttributeProperty, PlanogramNameTemplatePlanogramAttributeType.PlanogramName);
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            base.Create();
        }

        #endregion

        #endregion

        #region Methods
        
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<PlanogramNameTemplate>(oldId, newId);
        }

        /// <summary>
        /// Ensures the planogram name is unique
        /// </summary>
        /// <param name="planogramNameTemplate">The planogram name</param>
        /// <param name="planogramNames">The list of current planogram names</param>
        /// <returns>The unique Planogram Name</returns>
        public static String ValidatePlanogramNameUnique(String newTemplateName, List<String> planogramNames)
        {
            if (planogramNames.Contains(newTemplateName))
            {
                Int32 uniqueKey = 1;
                String newPlanogramNameTemplate = String.Format("{0}{1}{2}", newTemplateName, "~", uniqueKey);

                while (planogramNames.Contains(newPlanogramNameTemplate)) // ensure the plan name is unique
                {
                    uniqueKey++;
                    newPlanogramNameTemplate = String.Format("{0}{1}{2}", newTemplateName, "~", uniqueKey);
                }
                newTemplateName = newPlanogramNameTemplate;
            }

            planogramNames.Add(newTemplateName);

            return newTemplateName;
        }

        #endregion

        #region Override
        public override string ToString()
        {
            return this.Name;
        }
        #endregion
    }
}