#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva 
//  Created.
// V8-26076 : A.Silva 
//  Removed Id property, use Path to identify.
// V8-24124 : A.Silva 
//  Modified Create to use PropertyInfo data.
// V8-26369 : A.Silva 
//  Added DataType Enum.
// V8-26408 : L.Ineson
//  Removed Mapping Id.
// V8-26472 : A.Probyn
//  Removed properties and added relevant code to allow lookup
// V8-27022 : A.Silva 
//  Added Byte to 'Integer' Type in Get DataType().
// V8-27197 : L.Ineson
//  Removed old parameters and layout type specific methods.
// Corrected implementation of CustomColumnTotalType & its helper.
// V8-27504 : A.Silva
//      Added HeaderGroupNumber property to keep track of the group order.
//      Amended factory methods.
#endregion
#region Version History: (CCM 8.2)
// V8-30870 : L.Ineson
//  Added Width and DisplayName
#endregion

#region Version History: (CCM 8.3)
// V8-31542 : L.Ineson
//  Added Id and updated implementation.
//V8-32122 : L.Ineson
//  Removed HeaderGroupNumber
#endregion

#endregion

using System;
using Galleria.Framework.Model;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class CustomColumn : ModelObject<CustomColumn>
    {
        #region Parent

        /// <summary>
        /// Gets the parent CustomColumnLayout
        /// </summary>
        public new CustomColumnLayout Parent
        {
            get 
            {
                CustomColumnList parentList = base.Parent as CustomColumnList;
                if (parentList != null)
                {
                    return parentList.Parent;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id

        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);

        /// <summary>
        /// Returns the item unqiue identifier.
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        #endregion

        #region Path

        /// <summary>
        /// Path property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> PathProperty = 
            RegisterModelProperty<String>(o => o.Path);

        /// <summary>
        ///  Gets or sets the binding path
        /// </summary>
        public String Path
        {
            get { return GetProperty(PathProperty); }
            set { SetProperty(PathProperty, value); }
        }

        #endregion

        #region Number

        /// <summary>
        /// Number property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> NumberProperty = 
            RegisterModelProperty<Int32>(o => o.Number);

        /// <summary>
        ///  Gets/Sets the column order number
        /// </summary>
        public Int32 Number
        {
            get { return GetProperty(NumberProperty); }
            set { SetProperty(NumberProperty, value); }
        }

        #endregion

        #region TotalType

        /// <summary>
        /// TotalType property definition
        /// </summary>
        private static readonly ModelPropertyInfo<CustomColumnTotalType> TotalTypeProperty =
            RegisterModelProperty<CustomColumnTotalType>(o => o.TotalType);

        /// <summary>
        /// Gets/Sets the column totals display type.
        /// </summary>
        public CustomColumnTotalType TotalType
        {
            get { return GetProperty(TotalTypeProperty); }
            set { SetProperty(TotalTypeProperty, value); }
        }

        #endregion

        #region Width

        /// <summary>
        /// Width property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> WidthProperty =
            RegisterModelProperty<Int32>(o => o.Width);

        /// <summary>
        /// Gets/Sets the width of the column in pixels.
        /// 0 = auto.
        /// </summary>
        public Int32 Width
        {
            get { return GetProperty(WidthProperty); }
            set { SetProperty(WidthProperty, value); }
        }

        #endregion

        #region DisplayName

        /// <summary>
        /// DisplayName property definition.
        /// </summary>
        private static readonly ModelPropertyInfo<String> DisplayNameProperty =
            RegisterModelProperty<String>(c => c.DisplayName);
        /// <summary>
        /// Gets/Sets the display name for the column
        /// if left blank the default field friendly name will be used.
        /// </summary>
        public String DisplayName
        {
            get { return GetProperty(DisplayNameProperty); }
            set { SetProperty(DisplayNameProperty, value); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Business Rules

        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Creates and initializes a new instance of the <see cref="CustomColumn"/> type.
        /// </summary>
        /// <param name="path">Property path for the custom column.</param>
        /// <param name="number">Order number within the group of attributes for this <see cref="CustomColumn"/> instance.</param>
        /// <returns>A new instance of the <see cref="CustomColumn"/> type.</returns>
        public static CustomColumn NewCustomColumn(String path, Int32 number)
        {
            var column = new CustomColumn();
            column.Create(path, number);
            return column;
        }

        #endregion

        #region Data Access

        /// <summary>
        ///     Initializes the properties of this instance and marks it as child.
        /// </summary>
        /// <param name="path">Property path for the custom column.</param>
        /// <param name="number">Order number within the group of attributes for this <see cref="CustomColumn"/> instance.</param>
        /// <param name="headerGroupNumber">Order number of the group of attributes for this <see cref="CustomColumn"/> instance.</param>
        private void Create(String path, Int32 number)
        {
            LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(PathProperty, path);
            this.LoadProperty<Int32>(NumberProperty, number);
            this.LoadProperty<CustomColumnTotalType>(TotalTypeProperty, CustomColumnTotalType.None);
            this.LoadProperty<Int32>(WidthProperty, 0);

            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<CustomColumn>(oldId, newId);
        }

        #endregion
    }
}