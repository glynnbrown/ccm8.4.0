﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class SyncTargetList
    {
        #region Constructor
        private SyncTargetList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a list of all sync targets
        /// </summary>
        /// <returns></returns>
        public static SyncTargetList GetAllSyncTargets(Boolean UISource)
        {
            return DataPortal.Fetch<SyncTargetList>(UISource);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when retrieving a list of sync targets
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(Boolean UISource)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory(Constants.SyncDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ISyncTargetDal dal = dalContext.GetDal<ISyncTargetDal>())
                {
                    IEnumerable<SyncTargetDto> dtoList = dal.FetchAll();
                    foreach (SyncTargetDto dto in dtoList)
                    {
                        SyncTarget syncTarget = SyncTarget.GetSyncTarget(dalContext, dto);
                        syncTarget.UISource = UISource;
                        this.Add(syncTarget);
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}