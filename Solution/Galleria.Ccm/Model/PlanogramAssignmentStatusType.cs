﻿using Galleria.Framework.Planograms.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Model
{
    public enum PlanogramAssignmentStatusType
    {
        Any = -1,
        None = PlanogramStatusType.None,
        InProgress = PlanogramStatusType.WorkInProgress,
        Approved = PlanogramStatusType.Approved,
        Published = PlanogramStatusType.Published,
        Archived = PlanogramStatusType.Archived
    }

    public static class PlanogramAssignmentStatusTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<PlanogramAssignmentStatusType, String> FriendlyNames =
            new Dictionary<PlanogramAssignmentStatusType, String>()
            {
                {PlanogramAssignmentStatusType.Any, "Any"},
                {PlanogramAssignmentStatusType.None, PlanogramStatusTypeHelper.FriendlyNames[PlanogramStatusType.None]},
                {PlanogramAssignmentStatusType.InProgress, PlanogramStatusTypeHelper.FriendlyNames[PlanogramStatusType.WorkInProgress]},
                {PlanogramAssignmentStatusType.Approved, PlanogramStatusTypeHelper.FriendlyNames[PlanogramStatusType.Approved]},
                {PlanogramAssignmentStatusType.Published, PlanogramStatusTypeHelper.FriendlyNames[PlanogramStatusType.Published]},
                {PlanogramAssignmentStatusType.Archived, PlanogramStatusTypeHelper.FriendlyNames[PlanogramStatusType.Archived]}
            };
    }
}