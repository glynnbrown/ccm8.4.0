﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class LocationSpaceInfoList
    {
        #region Constructors
        private LocationSpaceInfoList() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        public static LocationSpaceInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<LocationSpaceInfoList>(new FetchByEntityIdCriteria(entityId));
        }
        #endregion

        #region Data Access

        /// <summary>
        /// Called when retrieving a list of all entity infos
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationSpaceInfoDal dal = dalContext.GetDal<ILocationSpaceInfoDal>())
                {
                    IEnumerable<LocationSpaceInfoDto> dtoList = dal.FetchByEntityId(criteria.EntityId);
                    foreach (LocationSpaceInfoDto dto in dtoList)
                    {
                        this.Add(LocationSpaceInfo.GetLocationSpaceInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }
        #endregion

    }
}
