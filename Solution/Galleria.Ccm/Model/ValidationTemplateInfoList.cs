﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26401 : A.Silva ~ Created.

#endregion

#region Version History: (CCM v8.03)
// V8-29219 : L.Ineson
//  Removed FetchByEntityIdIncludingDeleted
#endregion

#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     A readonly list holding <see cref="ValidationTemplateInfo"/> instances.
    /// </summary>
    /// <remarks>This is a root object.</remarks>
    [Serializable]
    public sealed partial class ValidationTemplateInfoList : ModelReadOnlyList<ValidationTemplateInfoList, ValidationTemplateInfo>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ValidationTemplateInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(ValidationTemplateInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ValidationTemplateInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ValidationTemplateInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Criteria

        [Serializable]
        public class FetchByEntityIdCriteria : CriteriaBase<FetchByEntityIdCriteria>
        {
            #region EntityId

            /// <summary>
            ///		Metadata for the <see cref="EntityId"/> property.
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(o => o.EntityId);

            /// <summary>
            ///		Gets or sets the value for the <see cref="EntityId"/> property.
            /// </summary>
            public Int32 EntityId
            {
                get { return ReadProperty(EntityIdProperty); }
            }

            #endregion

            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty(EntityIdProperty,entityId);
            }
        }


        #endregion
    }
}
