﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Readonly list holding FixturePackageInfo objects.
    /// </summary>
    [Serializable]
    public sealed partial class FixturePackageInfoList : ModelReadOnlyList<FixturePackageInfoList, FixturePackageInfo>
    {
        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByIds
        /// </summary>
        [Serializable]
        public sealed class FetchByIdsCriteria : CriteriaBase<FetchByIdsCriteria>
        {
            #region Properties

            #region PackageType
            /// <summary>
            /// PackageType property definition
            /// </summary>
            public static readonly PropertyInfo<FixturePackageType> PackageTypeProperty =
                RegisterProperty<FixturePackageType>(c => c.PackageType);
            /// <summary>
            /// Returns the package type
            /// </summary>
            public FixturePackageType PackageType
            {
                get { return this.ReadProperty<FixturePackageType>(PackageTypeProperty); }
            }
            #endregion

            #region Ids
            public static PropertyInfo<List<Object>> IdsProperty 
                = RegisterProperty<List<Object>>(c => c.Ids);
            public List<Object> Ids
            {
                get { return ReadProperty<List<Object>>(IdsProperty); }
            }
            #endregion


            #endregion

            #region Constructor

            public FetchByIdsCriteria(FixturePackageType type, List<Object> idList)
            {
                LoadProperty<FixturePackageType>(PackageTypeProperty, type);
                LoadProperty<List<Object>>(IdsProperty, idList);
            }

            #endregion
        }


        #endregion
    }
}
