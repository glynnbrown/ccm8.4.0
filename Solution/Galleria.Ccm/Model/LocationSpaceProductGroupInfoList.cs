﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// V8-29026 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a list containing LocationSpaceProductGroupInfo objects
    /// </summary>
    [Serializable]
    public sealed partial class LocationSpaceProductGroupInfoList : ModelReadOnlyList<LocationSpaceProductGroupInfoList, LocationSpaceProductGroupInfo>
    {
        #region Authorisation Rules
        /// <summary>
        /// defines authorisation rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationSpaceProductGroupInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceProductGroupInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceProductGroupInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceProductGroupInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByProductGroupId
        /// </summary>
        [Serializable]
        public class FetchByProductGroupIdCriteria : Csla.CriteriaBase<FetchByProductGroupIdCriteria>
        {
            #region Properties

            public static readonly PropertyInfo<Int32> ProductGroupIdProperty =
            RegisterProperty<Int32>(c => c.ProductGroupId);
            public Int32 ProductGroupId
            {
                get { return ReadProperty<Int32>(ProductGroupIdProperty); }
            }

            #endregion

            public FetchByProductGroupIdCriteria(Int32 productGroupId)
            {
                LoadProperty<Int32>(ProductGroupIdProperty, productGroupId);
            }
        }

        #endregion
    }
}