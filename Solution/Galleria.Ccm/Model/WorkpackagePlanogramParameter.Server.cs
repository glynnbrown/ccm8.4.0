﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
// V8-25886 : L.Ineson
//  Removed value, added values
#endregion
#region Version History: CCM820
// V8-30596 : N.Foster
//  Resolved issue where duplicate tasks in same workflow resulted in exception
#endregion
#endregion

using System;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class WorkpackagePlanogramParameter
    {
        #region Constructors
        private WorkpackagePlanogramParameter() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a new instance from a dto
        /// </summary>
        internal static WorkpackagePlanogramParameter GetWorkpackagePlanogramParameter(IDalContext dalContext, WorkpackagePlanogramParameterDto dto, WorkflowTaskDto workflowTaskDto, WorkflowTaskParameterDto workflowTaskParameterDto)
        {
            return DataPortal.FetchChild<WorkpackagePlanogramParameter>(dalContext, dto, workflowTaskDto, workflowTaskParameterDto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, WorkpackagePlanogramParameterDto dto, WorkflowTaskDto workflowTaskDto, WorkflowTaskParameterDto workflowTaskParameterDto)
        {
            EngineTaskInfo taskInfo = EngineTaskInfo.GetEngineTaskInfo(workflowTaskDto.TaskType);
            EngineTaskParameterInfo parameterInfo = taskInfo.Parameters.FirstOrDefault(item => item.Id == workflowTaskParameterDto.ParameterId);

            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32>(WorkflowTaskIdProperty, workflowTaskDto.Id);
            this.LoadProperty<Int32>(WorkflowTaskParameterIdProperty, dto.WorkflowTaskParameterId);
            this.LoadProperty<EngineTaskInfo>(TaskDetailsProperty, taskInfo);
            this.LoadProperty<EngineTaskParameterInfo>(ParameterDetailsProperty, parameterInfo);
        }

        /// <summary>
        /// Gets a data transfer object from this instance
        /// </summary>
        private WorkpackagePlanogramParameterDto GetDataTransferObject(WorkpackagePlanogram dto)
        {
            return new WorkpackagePlanogramParameterDto()
            {
                Id = this.ReadProperty<Int32>(IdProperty),
                WorkflowTaskParameterId = this.ReadProperty<Int32>(WorkflowTaskParameterIdProperty),
                WorkpackagePlanogramId = Parent.Id
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when loading a child instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, WorkpackagePlanogramParameterDto dto, WorkflowTaskDto workflowTaskDto, WorkflowTaskParameterDto workflowTaskParameterDto)
        {
            this.LoadDataTransferObject(dalContext, dto, workflowTaskDto, workflowTaskParameterDto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting a child instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, WorkpackagePlanogram parent)
        {
            Int32 oldId = 0;
            batchContext.Insert<WorkpackagePlanogramParameterDto>(
            (dc) =>
            {
                WorkpackagePlanogramParameterDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                dc.RegisterId<WorkpackagePlanogramParameter>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating a child instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, WorkpackagePlanogram parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<WorkpackagePlanogramParameterDto>(this.GetDataTransferObject(parent));
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this child is being deleted
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, WorkpackagePlanogram parent)
        {
            batchContext.Delete<WorkpackagePlanogramParameterDto>(this.GetDataTransferObject(parent));
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #endregion
    }
}
