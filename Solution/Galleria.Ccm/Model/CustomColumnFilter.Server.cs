#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26076 : A.Silva ~ Removed Id property as identifier, user Path Property instead.

#endregion


#region Version History: (CCM 8.3)
// V8-31542 : L.Ineson
//  Added Id and updated implementation.
#endregion

#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public sealed partial class CustomColumnFilter
    {
        #region Constructor
        private CustomColumnFilter(){}//force us of factory methods.
        #endregion

        #region Factory Methods

        /// <summary>
        ///     Returns a existing <see cref="CustomColumnFilter"/>.
        /// </summary>
        /// <param name="dalContext">DAL context to access the data.</param>
        /// <param name="dto"></param>
        /// <returns>
        ///     Returns an existing <see cref="CustomColumnFilter" />.
        /// </returns>
        public static CustomColumnFilter FetchCustomColumnFilter(IDalContext dalContext, CustomColumnFilterDto dto)
        {
            return DataPortal.FetchChild<CustomColumnFilter>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Returns a new <see cref="CustomColumnFilterDto"/> with this instance's data.
        /// </summary>
        /// <returns>A new dto</returns>
        private CustomColumnFilterDto GetDataTransferObject()
        {
            return new CustomColumnFilterDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                Text = ReadProperty(TextProperty),
                Path = ReadProperty(PathProperty),
                Header = ReadProperty(HeaderProperty),
                CustomColumnLayoutId = Parent.Id
            };
        }

        /// <summary>
        ///     Loads the data from a <see cref="CustomColumnFilterDto"/> in this instance.
        /// </summary>
        /// <param name="dalContext">DAL context to access the data.</param>
        /// <param name="dto">The <see cref="CustomColumnFilterDto"/> to load data from.</param>
        private void LoadDataTransferObject(IDalContext dalContext, CustomColumnFilterDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty(TextProperty, dto.Text);
            LoadProperty(PathProperty, dto.Path);
            LoadProperty(HeaderProperty, dto.Header);
        }

        #endregion

        #region Fetch
        /// <summary>
        ///     Called via reflection when retrieving a <see cref="CustomColumnFilter" />.
        /// </summary>
        /// <param name="dalContext">DAL context to access the data.</param>
        /// <param name="dto"><see cref="CustomColumnFilterDto"/> containing the data to load into this instance.</param>
        private void Child_Fetch(IDalContext dalContext, CustomColumnFilterDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Called via reflection when inserting a <see cref="CustomColumnFilter" /> in a data store.
        /// </summary>
        /// <param name="dalContext">DAL context to access the data.</param>
        private void Child_Insert(IDalContext dalContext)
        {
            using (ICustomColumnFilterDal dal = dalContext.GetDal<ICustomColumnFilterDal>())
            {
                CustomColumnFilterDto dto = GetDataTransferObject();
                dal.Insert(dto);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Update
        /// <summary>
        ///     Called via reflection when updating a <see cref="CustomColumnFilter" /> in a data store.
        /// </summary>
        /// <param name="dalContext">DAL context to access the data.</param>
        private void Child_Update(IDalContext dalContext)
        {
            using (ICustomColumnFilterDal dal = dalContext.GetDal<ICustomColumnFilterDal>())
            {
                CustomColumnFilterDto dto = GetDataTransferObject();
                dal.Update(dto);

                LoadProperty<Int32>(IdProperty, dto.Id);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Delete
        /// <summary>
        ///     Called via reflection when deleting the <see cref="CustomColumnFilter" /> from a data store.
        /// </summary>
        /// <param name="dalContext">DAL context to access the data.</param>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (ICustomColumnFilterDal dal = dalContext.GetDal<ICustomColumnFilterDal>())
            {
                dal.DeleteById(this.Id);
            }
        }

        #endregion

        #endregion
    }
}