﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-25608 : N.Foster
//  Created
// V8-25787 : N.Foster
//  Added RowVersion
// CCM-25880 : N.Haywood
//  Added CategoryCode and CategoryName
// V8-25881 : A.Probyn
//  Added MetaData properties
// V8-26492 : L.Ineson
//  Added uom properties
// V8-26822 : L.Ineson
//  Added planogram group id and name
// V8-27058 : A.Probyn
//  Updated for up to date meta data properties
// V8-27154 : L.Luong
//  Added ProductPlacement X,Y,Z
// V8-27411 : M.Pettit
//  Added LockType, isLocked, LockUserId, LockUserName properties
//  Changed names of ProcessingStatus and Progress properties to be Automation-specific
//  AutomationProcessingStatusProperty was not calculated correctly
//  Added metadata and validation status properties
//  Changed dto's Status property to a byte as it is now an enum value
//  Added DateValidationDataCalculated Property
//  Added ValidationIsOutOfDate property
//  Added userName property

#endregion

#region Version History: CCM803

// V8-29590 : A.Probyn
//	Extended for new Added MetaNoOfErrors, MetaNoOfWarnings, 
//	MetaTotalErrorScore, MetaHighestErrorScore
// V8-28242 : M.Shelley
//  Added Status, DateWIP, DateApproved, DateArchived 

#endregion

#region Version History: CCM810

// V8-29860 : M.Shelley
//  Added the PlanogramType property

#endregion

#region Version History: CCM811

// V8-30164 : A.Silva
//  Amended MetaAverageFacings, MetaAverageUnits, MetaSpaceToUnitsIndex to be <Single?>.
// V8-30225 : M.Shelley
//  Added the Planogram UCR
// V8-30359 : A.Probyn
//  Added new MostRecentLinkedWorkpackageName

#endregion

#region Version History: CCM830
// CCM-32015 : N.Haywood
//  Added mission fields
// CCM-31831 : A.Probyn
//  Adding more missing fields.lo
// V8-32521 : J.Pickup
//	Added MetaHasComponentsOutsideOfFixtureArea
// V8-33003 : J.Pickup
//  PlanogramInfo_AssignedLocationCount Added.
#endregion

#region Version History: CCM 840
// V8-19069 : J.Mendes
//  Five new Note field attributes for the Custom Attribute Data
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramInfo
    {
        #region Constructor
        private PlanogramInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an instance from a dto
        /// </summary>
        internal static PlanogramInfo GetPlanogramInfo(IDalContext dalContext, PlanogramInfoDto dto)
        {
            return DataPortal.FetchChild<PlanogramInfo>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramInfoDto dto)
        {
            // Standard Properties
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Guid>(UniqueContentReferenceProperty, dto.UniqueContentReference);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<PlanogramStatusType>(StatusProperty, (PlanogramStatusType)dto.Status);
            this.LoadProperty<Single>(HeightProperty, dto.Height);
            this.LoadProperty<Single>(WidthProperty, dto.Width);
            this.LoadProperty<Single>(DepthProperty, dto.Depth);
            this.LoadProperty<String>(UserNameProperty, dto.UserName);
            
            this.LoadProperty<ProcessingStatus>(AutomationProcessingStatusProperty, (ProcessingStatus)dto.AutomationProcessingStatus);
            this.LoadProperty<String>(AutomationProcessingStatusDescriptionProperty, dto.AutomationProcessingStatusDescription);
            this.LoadProperty<Int32>(AutomationProgressMaxProperty, dto.AutomationProgressMax);
            this.LoadProperty<Int32>(AutomationProgressCurrentProperty, dto.AutomationProgressCurrent);
            this.LoadProperty<Single>(AutomationProgressProperty, dto.AutomationProgressMax == 0 ? 0 : (Single)dto.AutomationProgressCurrent/(Single)dto.AutomationProgressMax);
            this.LoadProperty<DateTime?>(AutomationDateStartedProperty, dto.AutomationDateStarted);
            this.LoadProperty<DateTime?>(AutomationDateLastUpdatedProperty, dto.AutomationDateLastUpdated);
            
            this.LoadProperty<ProcessingStatus>(MetaDataProcessingStatusProperty, (ProcessingStatus)dto.MetaDataProcessingStatus);
            this.LoadProperty<String>(MetaDataProcessingStatusDescriptionProperty, dto.MetaDataProcessingStatusDescription);
            this.LoadProperty<Int32>(MetaDataProgressMaxProperty, dto.MetaDataProgressMax);
            this.LoadProperty<Int32>(MetaDataProgressCurrentProperty, dto.MetaDataProgressCurrent);
            this.LoadProperty<Single>(MetaDataProgressProperty, dto.MetaDataProgressMax == 0 ? 0 : (Single)dto.MetaDataProgressCurrent / (Single)dto.MetaDataProgressMax);
            this.LoadProperty<DateTime?>(MetaDataDateStartedProperty, dto.MetaDataDateStarted);
            this.LoadProperty<DateTime?>(MetaDataDateLastUpdatedProperty, dto.MetaDataDateLastUpdated);

            this.LoadProperty<ProcessingStatus>(ValidationProcessingStatusProperty, (ProcessingStatus)dto.ValidationProcessingStatus);
            this.LoadProperty<String>(ValidationProcessingStatusDescriptionProperty, dto.ValidationProcessingStatusDescription);
            this.LoadProperty<Int32>(ValidationProgressMaxProperty, dto.ValidationProgressMax);
            this.LoadProperty<Int32>(ValidationProgressCurrentProperty, dto.ValidationProgressCurrent);
            this.LoadProperty<Single>(ValidationProgressProperty, dto.ValidationProgressMax == 0 ? 0 : (Single)dto.ValidationProgressCurrent / (Single)dto.ValidationProgressMax);
            this.LoadProperty<DateTime?>(ValidationDateStartedProperty, dto.ValidationDateStarted);
            this.LoadProperty<DateTime?>(ValidationDateLastUpdatedProperty, dto.ValidationDateLastUpdated);

            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);
            this.LoadProperty<String>(CategoryCodeProperty, dto.CategoryCode);
            this.LoadProperty<String>(CategoryNameProperty, dto.CategoryName);
            this.LoadProperty<PlanogramLengthUnitOfMeasureType>(LengthUnitsOfMeasureProperty, (PlanogramLengthUnitOfMeasureType)dto.LengthUnitsOfMeasure);
            this.LoadProperty<PlanogramAreaUnitOfMeasureType>(AreaUnitsOfMeasureProperty, (PlanogramAreaUnitOfMeasureType)dto.AreaUnitsOfMeasure);
            this.LoadProperty<PlanogramVolumeUnitOfMeasureType>(VolumeUnitsOfMeasureProperty, (PlanogramVolumeUnitOfMeasureType)dto.VolumeUnitsOfMeasure);
            this.LoadProperty<PlanogramWeightUnitOfMeasureType>(WeightUnitsOfMeasureProperty, (PlanogramWeightUnitOfMeasureType)dto.WeightUnitsOfMeasure);
            this.LoadProperty<PlanogramCurrencyUnitOfMeasureType>(CurrencyUnitsOfMeasureProperty, (PlanogramCurrencyUnitOfMeasureType)dto.CurrencyUnitsOfMeasure);
            this.LoadProperty<PlanogramProductPlacementXType>(ProductPlacementXProperty, (PlanogramProductPlacementXType)dto.ProductPlacementX);
            this.LoadProperty<PlanogramProductPlacementYType>(ProductPlacementYProperty, (PlanogramProductPlacementYType)dto.ProductPlacementY);
            this.LoadProperty<PlanogramProductPlacementZType>(ProductPlacementZProperty, (PlanogramProductPlacementZType)dto.ProductPlacementZ);
            this.LoadProperty<Int32?>(MetaBayCountProperty, dto.MetaBayCount);
            this.LoadProperty<Int32?>(MetaUniqueProductCountProperty, dto.MetaUniqueProductCount);
            this.LoadProperty<Int32?>(MetaComponentCountProperty, dto.MetaComponentCount);
            this.LoadProperty<Single?>(MetaTotalMerchandisableLinearSpaceProperty, dto.MetaTotalMerchandisableLinearSpace);
            this.LoadProperty<Single?>(MetaTotalMerchandisableAreaSpaceProperty, dto.MetaTotalMerchandisableAreaSpace);
            this.LoadProperty<Single?>(MetaTotalMerchandisableVolumetricSpaceProperty, dto.MetaTotalMerchandisableVolumetricSpace);
            this.LoadProperty<Single?>(MetaTotalLinearWhiteSpaceProperty, dto.MetaTotalLinearWhiteSpace);
            this.LoadProperty<Single?>(MetaTotalAreaWhiteSpaceProperty, dto.MetaTotalAreaWhiteSpace);
            this.LoadProperty<Single?>(MetaTotalVolumetricWhiteSpaceProperty, dto.MetaTotalVolumetricWhiteSpace);
            this.LoadProperty<Int32?>(MetaProductsPlacedProperty, dto.MetaProductsPlaced);
            this.LoadProperty<Int32?>(MetaProductsUnplacedProperty, dto.MetaProductsUnplaced);
            this.LoadProperty<Int32?>(MetaNewProductsProperty, dto.MetaNewProducts);
            this.LoadProperty<Int32?>(MetaChangesFromPreviousCountProperty, dto.MetaChangesFromPreviousCount);
            this.LoadProperty<Int32?>(MetaChangeFromPreviousStarRatingProperty, dto.MetaChangeFromPreviousStarRating);
            this.LoadProperty<Int32?>(MetaBlocksDroppedProperty, dto.MetaBlocksDropped);
            this.LoadProperty<Int32?>(MetaNotAchievedInventoryProperty, dto.MetaNotAchievedInventory);
            this.LoadProperty<Int32?>(MetaTotalFacingsProperty, dto.MetaTotalFacings);
            this.LoadProperty<Single?>(MetaAverageFacingsProperty, dto.MetaAverageFacings);
            this.LoadProperty<Int32?>(MetaTotalUnitsProperty, dto.MetaTotalUnits);
            this.LoadProperty<Single?>(MetaAverageUnitsProperty, dto.MetaAverageUnits);
            this.LoadProperty<Single?>(MetaMinDosProperty, dto.MetaMinDos);
            this.LoadProperty<Single?>(MetaMaxDosProperty, dto.MetaMaxDos);
            this.LoadProperty<Single?>(MetaAverageDosProperty, dto.MetaAverageDos);
            this.LoadProperty<Single?>(MetaMinCasesProperty, dto.MetaMinCases);
            this.LoadProperty<Single?>(MetaAverageCasesProperty, dto.MetaAverageCases);
            this.LoadProperty<Single?>(MetaMaxCasesProperty, dto.MetaMaxCases);
            this.LoadProperty<Int32?>(PlanogramGroupIdProperty, dto.PlanogramGroupId);
            this.LoadProperty<String>(PlanogramGroupNameProperty, dto.PlanogramGroupName);
            this.LoadProperty<Int32?>(MetaTotalComponentCollisionsProperty, dto.MetaTotalComponentCollisions);
            this.LoadProperty<Int32?>(MetaTotalComponentsOverMerchandisedDepthProperty, dto.MetaTotalComponentsOverMerchandisedDepth);
            this.LoadProperty<Int32?>(MetaTotalComponentsOverMerchandisedHeightProperty, dto.MetaTotalComponentsOverMerchandisedHeight);
            this.LoadProperty<Int32?>(MetaTotalComponentsOverMerchandisedWidthProperty, dto.MetaTotalComponentsOverMerchandisedWidth);
            this.LoadProperty<Int32?>(MetaTotalPositionCollisionsProperty, dto.MetaTotalPositionCollisions);
            this.LoadProperty<Int16?>(MetaTotalFrontFacingsProperty, dto.MetaTotalFrontFacings);
            this.LoadProperty<Single?>(MetaAverageFrontFacingsProperty, dto.MetaAverageFrontFacings);
            this.LoadProperty<Int32?>(MetaNoOfErrorsProperty, dto.MetaNoOfErrors);
            this.LoadProperty<Int32?>(MetaNoOfWarningsProperty, dto.MetaNoOfWarnings);
            this.LoadProperty<Int32?>(MetaNoOfInformationEventsProperty, dto.MetaNoOfInformationEvents);
            this.LoadProperty<Int32?>(MetaNoOfDebugEventsProperty, dto.MetaNoOfDebugEvents);
            this.LoadProperty<Int32?>(MetaTotalErrorScoreProperty, dto.MetaTotalErrorScore);
            this.LoadProperty<Byte?>(MetaHighestErrorScoreProperty, dto.MetaHighestErrorScore); 
            this.LoadProperty<DateTime?>(DateMetadataCalculatedProperty, dto.DateMetadataCalculated);
            this.LoadProperty<DateTime?>(DateValidationDataCalculatedProperty, dto.DateValidationDataCalculated);
            this.LoadProperty<Boolean>(IsLockedProperty, dto.IsLocked);
            this.LoadProperty<PackageLockType?>(LockTypeProperty, (PackageLockType?)dto.LockType);
            this.LoadProperty<Int32?>(LockUserIdProperty, dto.LockUserId);
            this.LoadProperty<String>(LockUserNameProperty, dto.LockUserName);
            this.LoadProperty<String>(LockUserDisplayNameProperty, dto.LockUserDisplayName);
            this.LoadProperty<DateTime?>(LockDateProperty, dto.LockDate);
            this.LoadProperty<Boolean>(ValidationIsOutOfDateProperty, dto.ValidationIsOutOfDate);

            this.LoadProperty<String>(ClusterSchemeNameProperty, dto.ClusterSchemeName);
            this.LoadProperty<String>(ClusterNameProperty, dto.ClusterName);
            this.LoadProperty<String>(LocationCodeProperty, dto.LocationCode);
            this.LoadProperty<String>(LocationNameProperty, dto.LocationName);

            this.LoadProperty<DateTime?>(DateWipProperty, dto.DateWip);
            this.LoadProperty<DateTime?>(DateApprovedProperty, dto.DateApproved);
            this.LoadProperty<DateTime?>(DateArchivedProperty, dto.DateArchived);
            this.LoadProperty<PlanogramType>(PlanogramTypeProperty, (PlanogramType)dto.PlanogramType);
            this.LoadProperty<String>(MostRecentLinkedWorkpackageNameProperty, dto.MostRecentLinkedWorkpackageName);

            this.LoadProperty<Byte>(AngleUnitsOfMeasureProperty, dto.AngleUnitsOfMeasure);
            this.LoadProperty<String>(HighlightSequenceStrategyProperty, dto.HighlightSequenceStrategy);
            this.LoadProperty<Int32>(AssignedLocationCountProperty, dto.AssignedLocationCount);

            this.LoadProperty<Int32?>(MetaCountOfPlacedProductsNotRecommendedInAssortmentProperty, dto.MetaCountOfPlacedProductsNotRecommendedInAssortment);
            this.LoadProperty<Int32?>(MetaCountOfPlacedProductsRecommendedInAssortmentProperty, dto.MetaCountOfPlacedProductsRecommendedInAssortment);
            this.LoadProperty<Int32?>(MetaCountOfPositionsOutsideOfBlockSpaceProperty, dto.MetaCountOfPositionsOutsideOfBlockSpace);
            this.LoadProperty<Int32?>(MetaCountOfProductNotAchievedCasesProperty, dto.MetaCountOfProductNotAchievedCases);
            this.LoadProperty<Int32?>(MetaCountOfProductNotAchievedDeliveriesProperty, dto.MetaCountOfProductNotAchievedDeliveries);
            this.LoadProperty<Int32?>(MetaCountOfProductNotAchievedDOSProperty, dto.MetaCountOfProductNotAchievedDOS);
            this.LoadProperty<Int32?>(MetaCountOfProductOverShelfLifePercentProperty, dto.MetaCountOfProductOverShelfLifePercent);
            this.LoadProperty<Int32?>(MetaCountOfRecommendedProductsInAssortmentProperty, dto.MetaCountOfRecommendedProductsInAssortment);
            this.LoadProperty<Int16?>(MetaNumberOfAssortmentRulesBrokenProperty, dto.MetaNumberOfAssortmentRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfCoreRulesBrokenProperty, dto.MetaNumberOfCoreRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfDelistProductRulesBrokenProperty, dto.MetaNumberOfDelistProductRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfDependencyFamilyRulesBrokenProperty, dto.MetaNumberOfDependencyFamilyRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfDelistFamilyRulesBrokenProperty, dto.MetaNumberOfDelistFamilyRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfDistributionRulesBrokenProperty, dto.MetaNumberOfDistributionRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfFamilyRulesBrokenProperty, dto.MetaNumberOfFamilyRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfForceProductRulesBrokenProperty, dto.MetaNumberOfForceProductRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfInheritanceRulesBrokenProperty, dto.MetaNumberOfInheritanceRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfLocalProductRulesBrokenProperty, dto.MetaNumberOfLocalProductRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfMaximumProductFamilyRulesBrokenProperty, dto.MetaNumberOfMaximumProductFamilyRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfMinimumHurdleProductRulesBrokenProperty, dto.MetaNumberOfMinimumHurdleProductRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfMinimumProductFamilyRulesBrokenProperty, dto.MetaNumberOfMinimumProductFamilyRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfPreserveProductRulesBrokenProperty, dto.MetaNumberOfPreserveProductRulesBroken);
            this.LoadProperty<Int16?>(MetaNumberOfProductRulesBrokenProperty, dto.MetaNumberOfProductRulesBroken);
            this.LoadProperty<Single?>(MetaPercentageOfAssortmentRulesBrokenProperty, dto.MetaPercentageOfAssortmentRulesBroken);
            this.LoadProperty<Single?>(MetaPercentageOfCoreRulesBrokenProperty, dto.MetaPercentageOfCoreRulesBroken);
            this.LoadProperty<Single?>(MetaPercentageOfDistributionRulesBrokenProperty, dto.MetaPercentageOfDistributionRulesBroken);
            this.LoadProperty<Single?>(MetaPercentageOfFamilyRulesBrokenProperty, dto.MetaPercentageOfFamilyRulesBroken);
            this.LoadProperty<Single?>(MetaPercentageOfInheritanceRulesBrokenProperty, dto.MetaPercentageOfInheritanceRulesBroken);
            this.LoadProperty<Single?>(MetaPercentageOfLocalProductRulesBrokenProperty, dto.MetaPercentageOfLocalProductRulesBroken);
            this.LoadProperty<Single?>(MetaPercentageOfProductRulesBrokenProperty, dto.MetaPercentageOfProductRulesBroken);
            this.LoadProperty<Single?>(MetaPercentOfPlacedProductsRecommendedInAssortmentProperty, dto.MetaPercentOfPlacedProductsRecommendedInAssortment);
            this.LoadProperty<Single?>(MetaPercentOfPositionsOutsideOfBlockSpaceProperty, dto.MetaPercentOfPositionsOutsideOfBlockSpace);
            this.LoadProperty<Single?>(MetaPercentOfProductNotAchievedCasesProperty, dto.MetaPercentOfProductNotAchievedCases);
            this.LoadProperty<Single?>(MetaPercentOfProductNotAchievedDeliveriesProperty, dto.MetaPercentOfProductNotAchievedDeliveries);
            this.LoadProperty<Single?>(MetaPercentOfProductNotAchievedDOSProperty, dto.MetaPercentOfProductNotAchievedDOS);
            this.LoadProperty<Single?>(MetaPercentOfProductOverShelfLifePercentProperty, dto.MetaPercentOfProductOverShelfLifePercent);
            this.LoadProperty<Boolean?>(MetaHasComponentsOutsideOfFixtureAreaProperty, dto.MetaHasComponentsOutsideOfFixtureArea);
            this.LoadProperty<Int16?>(MetaCountOfProductsBuddiedProperty, dto.MetaCountOfProductsBuddied);

            this.LoadProperty<String>(Text1Property, dto.Text1);
            this.LoadProperty<String>(Text2Property, dto.Text2);
            this.LoadProperty<String>(Text3Property, dto.Text3);
            this.LoadProperty<String>(Text4Property, dto.Text4);
            this.LoadProperty<String>(Text5Property, dto.Text5);
            this.LoadProperty<String>(Text6Property, dto.Text6);
            this.LoadProperty<String>(Text7Property, dto.Text7);
            this.LoadProperty<String>(Text8Property, dto.Text8);
            this.LoadProperty<String>(Text9Property, dto.Text9);
            this.LoadProperty<String>(Text10Property, dto.Text10);
            this.LoadProperty<String>(Text11Property, dto.Text11);
            this.LoadProperty<String>(Text12Property, dto.Text12);
            this.LoadProperty<String>(Text13Property, dto.Text13);
            this.LoadProperty<String>(Text14Property, dto.Text14);
            this.LoadProperty<String>(Text15Property, dto.Text15);
            this.LoadProperty<String>(Text16Property, dto.Text16);
            this.LoadProperty<String>(Text17Property, dto.Text17);
            this.LoadProperty<String>(Text18Property, dto.Text18);
            this.LoadProperty<String>(Text19Property, dto.Text19);
            this.LoadProperty<String>(Text20Property, dto.Text20);
            this.LoadProperty<String>(Text21Property, dto.Text21);
            this.LoadProperty<String>(Text22Property, dto.Text22);
            this.LoadProperty<String>(Text23Property, dto.Text23);
            this.LoadProperty<String>(Text24Property, dto.Text24);
            this.LoadProperty<String>(Text25Property, dto.Text25);
            this.LoadProperty<String>(Text26Property, dto.Text26);
            this.LoadProperty<String>(Text27Property, dto.Text27);
            this.LoadProperty<String>(Text28Property, dto.Text28);
            this.LoadProperty<String>(Text29Property, dto.Text29);
            this.LoadProperty<String>(Text30Property, dto.Text30);
            this.LoadProperty<String>(Text31Property, dto.Text31);
            this.LoadProperty<String>(Text32Property, dto.Text32);
            this.LoadProperty<String>(Text33Property, dto.Text33);
            this.LoadProperty<String>(Text34Property, dto.Text34);
            this.LoadProperty<String>(Text35Property, dto.Text35);
            this.LoadProperty<String>(Text36Property, dto.Text36);
            this.LoadProperty<String>(Text37Property, dto.Text37);
            this.LoadProperty<String>(Text38Property, dto.Text38);
            this.LoadProperty<String>(Text39Property, dto.Text39);
            this.LoadProperty<String>(Text40Property, dto.Text40);
            this.LoadProperty<String>(Text41Property, dto.Text41);
            this.LoadProperty<String>(Text42Property, dto.Text42);
            this.LoadProperty<String>(Text43Property, dto.Text43);
            this.LoadProperty<String>(Text44Property, dto.Text44);
            this.LoadProperty<String>(Text45Property, dto.Text45);
            this.LoadProperty<String>(Text46Property, dto.Text46);
            this.LoadProperty<String>(Text47Property, dto.Text47);
            this.LoadProperty<String>(Text48Property, dto.Text48);
            this.LoadProperty<String>(Text49Property, dto.Text49);
            this.LoadProperty<String>(Text50Property, dto.Text50);
            this.LoadProperty<Single?>(Value1Property, dto.Value1);
            this.LoadProperty<Single?>(Value2Property, dto.Value2);
            this.LoadProperty<Single?>(Value3Property, dto.Value3);
            this.LoadProperty<Single?>(Value4Property, dto.Value4);
            this.LoadProperty<Single?>(Value5Property, dto.Value5);
            this.LoadProperty<Single?>(Value6Property, dto.Value6);
            this.LoadProperty<Single?>(Value7Property, dto.Value7);
            this.LoadProperty<Single?>(Value8Property, dto.Value8);
            this.LoadProperty<Single?>(Value9Property, dto.Value9);
            this.LoadProperty<Single?>(Value10Property, dto.Value10);
            this.LoadProperty<Single?>(Value11Property, dto.Value11);
            this.LoadProperty<Single?>(Value12Property, dto.Value12);
            this.LoadProperty<Single?>(Value13Property, dto.Value13);
            this.LoadProperty<Single?>(Value14Property, dto.Value14);
            this.LoadProperty<Single?>(Value15Property, dto.Value15);
            this.LoadProperty<Single?>(Value16Property, dto.Value16);
            this.LoadProperty<Single?>(Value17Property, dto.Value17);
            this.LoadProperty<Single?>(Value18Property, dto.Value18);
            this.LoadProperty<Single?>(Value19Property, dto.Value19);
            this.LoadProperty<Single?>(Value20Property, dto.Value20);
            this.LoadProperty<Single?>(Value21Property, dto.Value21);
            this.LoadProperty<Single?>(Value22Property, dto.Value22);
            this.LoadProperty<Single?>(Value23Property, dto.Value23);
            this.LoadProperty<Single?>(Value24Property, dto.Value24);
            this.LoadProperty<Single?>(Value25Property, dto.Value25);
            this.LoadProperty<Single?>(Value26Property, dto.Value26);
            this.LoadProperty<Single?>(Value27Property, dto.Value27);
            this.LoadProperty<Single?>(Value28Property, dto.Value28);
            this.LoadProperty<Single?>(Value29Property, dto.Value29);
            this.LoadProperty<Single?>(Value30Property, dto.Value30);
            this.LoadProperty<Single?>(Value31Property, dto.Value31);
            this.LoadProperty<Single?>(Value32Property, dto.Value32);
            this.LoadProperty<Single?>(Value33Property, dto.Value33);
            this.LoadProperty<Single?>(Value34Property, dto.Value34);
            this.LoadProperty<Single?>(Value35Property, dto.Value35);
            this.LoadProperty<Single?>(Value36Property, dto.Value36);
            this.LoadProperty<Single?>(Value37Property, dto.Value37);
            this.LoadProperty<Single?>(Value38Property, dto.Value38);
            this.LoadProperty<Single?>(Value39Property, dto.Value39);
            this.LoadProperty<Single?>(Value40Property, dto.Value40);
            this.LoadProperty<Single?>(Value41Property, dto.Value41);
            this.LoadProperty<Single?>(Value42Property, dto.Value42);
            this.LoadProperty<Single?>(Value43Property, dto.Value43);
            this.LoadProperty<Single?>(Value44Property, dto.Value44);
            this.LoadProperty<Single?>(Value45Property, dto.Value45);
            this.LoadProperty<Single?>(Value46Property, dto.Value46);
            this.LoadProperty<Single?>(Value47Property, dto.Value47);
            this.LoadProperty<Single?>(Value48Property, dto.Value48);
            this.LoadProperty<Single?>(Value49Property, dto.Value49);
            this.LoadProperty<Single?>(Value50Property, dto.Value50);
            this.LoadProperty<Boolean?>(Flag1Property, dto.Flag1);
            this.LoadProperty<Boolean?>(Flag2Property, dto.Flag2);
            this.LoadProperty<Boolean?>(Flag3Property, dto.Flag3);
            this.LoadProperty<Boolean?>(Flag4Property, dto.Flag4);
            this.LoadProperty<Boolean?>(Flag5Property, dto.Flag5);
            this.LoadProperty<Boolean?>(Flag6Property, dto.Flag6);
            this.LoadProperty<Boolean?>(Flag7Property, dto.Flag7);
            this.LoadProperty<Boolean?>(Flag8Property, dto.Flag8);
            this.LoadProperty<Boolean?>(Flag9Property, dto.Flag9);
            this.LoadProperty<Boolean?>(Flag10Property, dto.Flag10);
            this.LoadProperty<DateTime?>(Date1Property, dto.Date1);
            this.LoadProperty<DateTime?>(Date2Property, dto.Date2);
            this.LoadProperty<DateTime?>(Date3Property, dto.Date3);
            this.LoadProperty<DateTime?>(Date4Property, dto.Date4);
            this.LoadProperty<DateTime?>(Date5Property, dto.Date5);
            this.LoadProperty<DateTime?>(Date6Property, dto.Date6);
            this.LoadProperty<DateTime?>(Date7Property, dto.Date7);
            this.LoadProperty<DateTime?>(Date8Property, dto.Date8);
            this.LoadProperty<DateTime?>(Date9Property, dto.Date9);
            this.LoadProperty<DateTime?>(Date10Property, dto.Date10);
            this.LoadProperty<String>(Note1Property, dto.Note1);
            this.LoadProperty<String>(Note2Property, dto.Note2);
            this.LoadProperty<String>(Note3Property, dto.Note3);
            this.LoadProperty<String>(Note4Property, dto.Note4);
            this.LoadProperty<String>(Note5Property, dto.Note5);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramInfoDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #endregion
    }
}