﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.1)
//V8-28622 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of PlanogramImportTemplatePerformanceMetric objects.
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramImportTemplatePerformanceMetricList : ModelList<PlanogramImportTemplatePerformanceMetricList, PlanogramImportTemplatePerformanceMetric>
    {
        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramImportTemplatePerformanceMetricList NewPlanogramImportTemplatePerformanceMetricList()
        {
            PlanogramImportTemplatePerformanceMetricList item = new PlanogramImportTemplatePerformanceMetricList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}