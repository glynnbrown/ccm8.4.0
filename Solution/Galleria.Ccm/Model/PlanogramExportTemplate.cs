﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
//  Refactored PlanogramFileType to Framework.Plangoram.Model and renamed PlanogramExportFileType
// V8-32253 : M.Pettit
//  Export to Apollo should have no default performance mappings applied.
// V8-32254 : M.Pettit
//  Export to Spaceman should have no default performance mappings applied.
// V8-32254 : J.Pickup
//  Changed commented performance values to correct fields. (Sales units - regular sales units).
// V8-32321 : M.Pettit
//  Apollo default mapping should use the Export mappings not the import mappings.
// V8-32725 : M.Pettit
//  Added SaveAs method
//  SaveAsFile method was not correctly clearing the DalFacotryname when saving a db item to a file
//  V8-32911 : J.Pickup
//   Enumerate Spaceman mappings now has the tax code removed - Spaceman stores tax values within the application.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Planograms.External;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class PlanogramExportTemplate : ModelObject<PlanogramExportTemplate>, IDisposable
    {
        #region Nested Classes
        /// <summary>
        /// Denotes the different dal factory types that may be used
        /// for this model.
        /// </summary>
        public enum PlanogramExportTemplateDalType
        {
            Unknown = 0,
            FileSystem = 1
        }
        #endregion

        #region Public Static

        /// <summary>
        /// Returns the file extension to use for this model.
        /// </summary>
        public static String FileExtension
        {
            get { return Constants.PlanogramExportTemplateFileExtension; }
        }

        #endregion

        #region Properties

        #region DalFactoryType

        /// <summary>
        /// DalFactoryType property definition
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramExportTemplateDalType> DalFactoryTypeProperty =
            RegisterModelProperty<PlanogramExportTemplateDalType>(c => c.DalFactoryType);

        /// <summary>
        /// Returns the dal factory type
        /// </summary>
        private PlanogramExportTemplateDalType DalFactoryType
        {
            get { return this.GetProperty<PlanogramExportTemplateDalType>(DalFactoryTypeProperty); }
        }

        #endregion

        #region DalFactoryName
        /// <summary>
        /// DalFactoryName property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> DalFactoryNameProperty =
            RegisterModelProperty<String>(c => c.DalFactoryName);
        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        protected override String DalFactoryName
        {
            get { return this.GetProperty<String>(DalFactoryNameProperty); }
        }
        #endregion

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> IdProperty =
            RegisterModelProperty<Object>(c => c.Id);
        /// <summary>
        /// Returns the unique identifier for this item.
        /// </summary>
        public Object Id
        {
            get { return this.GetProperty<Object>(IdProperty); }
        }
        #endregion

        #region RowVersion

        /// <summary>
        ///     Metadata for the <see cref="RowVersion" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);

        /// <summary>
        ///     Gets the row version timestamp.
        /// </summary>
        private RowVersion RowVersion
        {
            get { return GetProperty(RowVersionProperty); }
        }

        #endregion

        #region EntityId

        /// <summary>
        ///     Metadata for the <see cref="EntityId" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<int>(c => c.EntityId);

        /// <summary>
        ///     Gets or sets the unique Id value for the <see cref="Entity" /> model this instance belongs to.
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty(EntityIdProperty); }
            set { SetProperty(EntityIdProperty, value); }
        }

        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Gets or sets the workflow name
        /// </summary>
        public String Name
        {
            get { return this.GetProperty<String>(NameProperty); }
            set { this.SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region FileType
        /// <summary>
        /// FileType property definition.
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramExportFileType> FileTypeProperty =
            RegisterModelProperty<PlanogramExportFileType>(c => c.FileType);

        /// <summary>
        /// Gets/Sets the type of file this template is for.
        /// </summary>
        public PlanogramExportFileType FileType
        {
            get { return this.GetProperty<PlanogramExportFileType>(FileTypeProperty); }
            set
            {
                PlanogramExportFileType fileType = FileType;
                this.SetProperty<PlanogramExportFileType>(FileTypeProperty, value);
                if (!Equals(fileType, value))
                {
                    InitializeMappings(value);
                }
            }
        }
        #endregion

        #region FileVersion
        /// <summary>
        /// FileVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> FileVersionProperty =
            RegisterModelProperty<String>(c => c.FileVersion);

        /// <summary>
        /// Gets/Sets the file version that this template is for.
        /// </summary>
        public String FileVersion
        {
            get { return this.GetProperty<String>(FileVersionProperty); }
            set { SetProperty<String>(FileVersionProperty, value); }
        }
        #endregion

        #region Mappings
        /// <summary>
        /// Mappings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramExportTemplateMappingList> MappingsProperty =
            RegisterModelProperty<PlanogramExportTemplateMappingList>(c => c.Mappings);
        /// <summary>
        /// Returns the list of mappings for this template.
        /// </summary>
        public PlanogramExportTemplateMappingList Mappings
        {
            get { return this.GetProperty<PlanogramExportTemplateMappingList>(MappingsProperty); }
        }
        #endregion

        #region PerformanceMetrics
        /// <summary>
        /// PerformanceMetrics property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramExportTemplatePerformanceMetricList> PerformanceMetricsProperty =
            RegisterModelProperty<PlanogramExportTemplatePerformanceMetricList>(c => c.PerformanceMetrics);
        /// <summary>
        /// Returns the list of performance metrics for this template.
        /// </summary>
        public PlanogramExportTemplatePerformanceMetricList PerformanceMetrics
        {
            get { return this.GetProperty<PlanogramExportTemplatePerformanceMetricList>(PerformanceMetricsProperty); }
        }
        #endregion

        #region DateCreated

        /// <summary>
        ///     Metadata for the <see cref="DateCreated" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        ///     Gets or sets the date this instance was created originally.
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty(DateCreatedProperty); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        ///     Metadata for the <see cref="DateLastModified" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        ///     Gets or sets the last date this instance was modified (and persisted).
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty(DateLastModifiedProperty); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository


        /// <summary>
        /// Returns a dictionary of the current role permissions allocated to the user for this object type.
        /// </summary>
        /// <returns></returns>
        public static IDictionary<AuthorizationActions, Boolean> GetUserPermissions()
        {
            return new Dictionary<AuthorizationActions, Boolean>()
            {
                {AuthorizationActions.CreateObject, ApplicationContext.User.IsInRole(DomainPermission.PlanogramExportTemplateCreate.ToString())},
                {AuthorizationActions.GetObject, ApplicationContext.User.IsInRole(DomainPermission.PlanogramExportTemplateGet.ToString())},
                {AuthorizationActions.EditObject, ApplicationContext.User.IsInRole(DomainPermission.PlanogramExportTemplateEdit.ToString())},
                {AuthorizationActions.DeleteObject, ApplicationContext.User.IsInRole(DomainPermission.PlanogramExportTemplateDelete.ToString())},
            };
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
            BusinessRules.AddRule(new MaxLength(FileVersionProperty, 50));
        }
        #endregion

        #region Criteria

        #region FetchByIdCriteria

        /// <summary>
        /// Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region DalType
            /// <summary>
            /// DalType property definition
            /// </summary>
            public static readonly PropertyInfo<PlanogramExportTemplateDalType> DalTypeProperty =
                RegisterProperty<PlanogramExportTemplateDalType>(c => c.DalType);
            /// <summary>
            /// Returns the  dal type
            /// </summary>
            public PlanogramExportTemplateDalType DalType
            {
                get { return this.ReadProperty<PlanogramExportTemplateDalType>(DalTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #region AsReadOnly

            /// <summary>
            /// AsReadOnly property definition
            /// </summary>
            public static readonly PropertyInfo<Boolean> AsReadOnlyProperty =
                RegisterProperty<Boolean>(c => c.AsReadOnly);
            /// <summary>
            /// If true the model will be loaded as readonly.
            /// </summary>
            public Boolean AsReadOnly
            {
                get { return this.ReadProperty<Boolean>(AsReadOnlyProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(PlanogramExportTemplateDalType type, Object id)
            {
                this.LoadProperty<PlanogramExportTemplateDalType>(DalTypeProperty, type);
                this.LoadProperty<Object>(IdProperty, id);
                this.LoadProperty<Boolean>(AsReadOnlyProperty, false);
            }

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(PlanogramExportTemplateDalType type, Object id, Boolean asReadOnly)
            {
                this.LoadProperty<PlanogramExportTemplateDalType>(DalTypeProperty, type);
                this.LoadProperty<Object>(IdProperty, id);
                this.LoadProperty<Boolean>(AsReadOnlyProperty, asReadOnly);
            }
            #endregion
        }

        #endregion

        #region FetchByEntityIdNameCriteria

        [Serializable]
        public class FetchByEntityIdNameCriteria : CriteriaBase<FetchByEntityIdNameCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static readonly PropertyInfo<String> NameProperty =
                RegisterProperty<String>(c => c.Name);
            public String Name
            {
                get { return ReadProperty<String>(NameProperty); }
            }

            public FetchByEntityIdNameCriteria(Int32 entityId, String name)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<String>(NameProperty, name);
            }
        }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="fileType"></param>
        /// <param name="fileVersion"></param>
        public static PlanogramExportTemplate NewPlanogramExportTemplate(PlanogramExportFileType fileType, String fileVersion)
        {
            var item = new PlanogramExportTemplate();
            item.Create(fileType, fileVersion);
            item.InitializeMappings(fileType);

            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramExportTemplate NewPlanogramExportTemplate(Int32 entityId, PlanogramExportFileType fileType)
        {
            return NewPlanogramExportTemplate(entityId, fileType, PlanogramExportFileTypeHelper.MaxSupportedVersion[fileType]);
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramExportTemplate NewPlanogramExportTemplate(Int32 entityId, PlanogramExportFileType fileType, String fileVersion)
        {
            var item = new PlanogramExportTemplate();
            item.Create(entityId, fileType, fileVersion);
            item.InitializeMappings(fileType);

            return item;
        }

        /// <summary>
        /// Locks a model so that it cannot be edited or opened by another process
        /// </summary>
        internal static void LockPlanogramExportTemplateByFileName(String fileName)
        {
            DataPortal.Execute<LockPlanogramExportTemplateCommand>(new LockPlanogramExportTemplateCommand(PlanogramExportTemplateDalType.FileSystem, fileName));
        }

        /// <summary>
        /// Unlocks a model
        /// </summary>
        public static void UnlockPlanogramExportTemplateByFileName(String fileName)
        {
            DataPortal.Execute<UnlockPlanogramExportTemplateCommand>(new UnlockPlanogramExportTemplateCommand(PlanogramExportTemplateDalType.FileSystem, fileName));
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(PlanogramExportFileType fileType, String fileVersion)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<PlanogramExportFileType>(FileTypeProperty, fileType);
            this.LoadProperty<String>(FileVersionProperty, fileVersion);
            this.LoadProperty<PlanogramExportTemplateMappingList>(MappingsProperty, PlanogramExportTemplateMappingList.NewPlanogramExportTemplateMappingList());
            this.LoadProperty<PlanogramExportTemplatePerformanceMetricList>(PerformanceMetricsProperty, PlanogramExportTemplatePerformanceMetricList.NewPlanogramExportTemplatePerformanceMetricList());
            this.LoadProperty(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty(DateLastModifiedProperty, DateTime.UtcNow);
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(Int32 entityId, PlanogramExportFileType fileType, String fileVersion)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.LoadProperty<PlanogramExportFileType>(FileTypeProperty, fileType);
            this.LoadProperty<String>(FileVersionProperty, fileVersion);
            this.LoadProperty<PlanogramExportTemplateMappingList>(MappingsProperty, PlanogramExportTemplateMappingList.NewPlanogramExportTemplateMappingList());
            this.LoadProperty<PlanogramExportTemplatePerformanceMetricList>(PerformanceMetricsProperty, PlanogramExportTemplatePerformanceMetricList.NewPlanogramExportTemplatePerformanceMetricList());
            this.LoadProperty(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty(DateLastModifiedProperty, DateTime.UtcNow);
            base.Create();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<PlanogramExportTemplate>(oldId, newId);
        }

        /// <summary>
        /// ToString override.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Provide a SaveAs method to allow saving to the repository.
        /// This resets DalFactoryType and DalFactoryName to allow saving saving to the repository,
        /// otherwise a PlanogramExportTemplate instance loaded from a file tries to save back to a file.
        /// </summary>
        /// <returns></returns>
        public PlanogramExportTemplate SaveAs()
        {
            this.LoadProperty<PlanogramExportTemplateDalType>(DalFactoryTypeProperty, PlanogramExportTemplateDalType.Unknown);
            this.LoadProperty<String>(DalFactoryNameProperty, DalContainer.DalName);

            return this.Save();
        }

        /// <summary>
        /// Saves this to a new file.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public PlanogramExportTemplate SaveAsFile(String path)
        {
            PlanogramExportTemplate returnValue = null;

            String fileName = path;

            //make sure the file extension is correct.
            fileName = Path.ChangeExtension(path, FileExtension);

            //if this model is not new and was fecthed from another file
            // take a note of the old file name.
            String oldFileName = null;
            if ((!IsNew) && (this.Id as String != fileName))
            {
                if (DalFactoryType == PlanogramExportTemplateDalType.FileSystem)
                {
                    oldFileName = this.Id as String;
                }
                MarkGraphAsNew();
            }

            LoadProperty<PlanogramExportTemplateDalType>(DalFactoryTypeProperty, PlanogramExportTemplateDalType.FileSystem);
            LoadProperty<String>(DalFactoryNameProperty, ""); 
            LoadProperty<Object>(IdProperty, fileName);
            this.Name = Path.GetFileNameWithoutExtension(fileName);
            returnValue = Save();

            //unlock the old file.
            if (!String.IsNullOrEmpty(oldFileName))
            {
                UnlockPlanogramExportTemplateByFileName(oldFileName);
            }

            if (System.IO.File.Exists(fileName))
            {
                UnlockPlanogramExportTemplateByFileName(fileName);
            }

            ////V8-25873 : Make sure id is the file name for file system, dal is returning 1
            LoadProperty<Object>(IdProperty, fileName);
            return returnValue;
        }

        /// <summary>
        /// Updates this mapping list for the list of mappable ccm fields
        /// </summary>
        public void UpdateFromCcmFieldList()
        {
            var ccmFieldList = PlanogramExportTemplateFieldInfoList.NewPlanogramExportTemplateFieldInfoList(null, null);

            if (this.Mappings.Count == 0)
            {
                //just load in all fields
                foreach (PlanogramExportTemplateFieldInfo field in ccmFieldList)
                {
                    this.Mappings.Add(PlanogramExportTemplateMapping.NewPlanogramExportTemplateMapping(field.Field, field.FieldType));
                }
            }
            else
            {
                //need to update existing list
                List<String> ccmFields = ccmFieldList.Select(c => c.Field).ToList();

                //remove any mappings for fields which no longer exist
                this.Mappings.RemoveList(this.Mappings.Where(m => !ccmFields.Contains(m.Field)).ToList());

                //add in any new ones
                ccmFields = ccmFields.Except(this.Mappings.Select(m => m.Field)).ToList();

                foreach (PlanogramExportTemplateFieldInfo field in ccmFieldList)
               {
                    if (ccmFields.Contains(field.Field))
                    {
                        this.Mappings.Add(PlanogramExportTemplateMapping.NewPlanogramExportTemplateMapping(field.Field, field.FieldType));
                    }
                }
            }
        }

        /// <summary>
        /// Returns a list of planogram field mappings from this template.
        /// </summary>
        public List<PlanogramFieldMapping> GetPlanogramFieldMappings(Boolean isImport = false)
        {
            List<PlanogramFieldMapping> planMappings = new List<PlanogramFieldMapping>();

            foreach (PlanogramExportTemplateMapping mapping in this.Mappings)
            {
                if (!String.IsNullOrEmpty(mapping.ExternalField))
                {

                    if (isImport)
                    {
                        planMappings.Add(PlanogramFieldMapping.NewPlanogramFieldMapping(mapping.FieldType, mapping.Field, mapping.ExternalField));
                    }
                    else
                    {
                        //flip
                        planMappings.Add(PlanogramFieldMapping.NewPlanogramFieldMapping(mapping.FieldType, mapping.ExternalField, mapping.Field));
                    }
                }
            }

            return planMappings;
        }

        /// <summary>
        /// Returns a list of planogram metric mappings from this template.
        /// </summary>
        public List<PlanogramMetricMapping> GetPlanogramMetricMappings()
        {
            List<PlanogramMetricMapping> planMappings = new List<PlanogramMetricMapping>();

            foreach (PlanogramExportTemplatePerformanceMetric mapping in this.PerformanceMetrics)
            {
                if (!String.IsNullOrEmpty(mapping.ExternalField))
                {
                    planMappings.Add(PlanogramMetricMapping.NewPlanogramMetricMapping(mapping.Name, mapping.Description, mapping.Direction,
                                    mapping.SpecialType, mapping.MetricType, mapping.AggregationType, mapping.MetricId, mapping.ExternalField));
                }
            }

            return planMappings;
        }

        private void InitializeMappings(PlanogramExportFileType fileType)
        {
            if (Mappings.Count > 0) Mappings.Clear();
            if (PerformanceMetrics.Any()) PerformanceMetrics.Clear();
            UpdateFromCcmFieldList();

            switch (fileType)
            {
                case PlanogramExportFileType.Spaceman:
                    ApplySpacemanV9Mappings();
                    break;
                case PlanogramExportFileType.JDA:
                    ApplySpacePlanningMappings();
                    break;
                case PlanogramExportFileType.Apollo:
                    ApplyApolloMappings();
                    break;
                default:
                    Debug.Fail(String.Format("Unknown PlanogramExportFileType ({0}) when calling InitializeMappings", fileType));
                    break;
            }
        }

        #region Default Spaceman V9 mappings

        private void ApplySpacemanV9Mappings()
        {
            IEnumerable<PlanogramExportTemplateMapping> planogramMappings = Mappings.Where(m => m.FieldType == PlanogramFieldMappingType.Planogram).ToList();
            foreach (Tuple<String, String> fieldMapping in EnumerateSpacemanV9DefaultMappingsForPlanogram())
            {
                PlanogramExportTemplateMapping match = planogramMappings.FirstOrDefault(m => m.Field == fieldMapping.Item1);
                if (match != null) match.ExternalField = fieldMapping.Item2;
            }

            planogramMappings = Mappings.Where(m => m.FieldType == PlanogramFieldMappingType.Fixture).ToList();
            foreach (Tuple<String, String> fieldMapping in EnumerateSpacemanV9DefaultMappingsForFixture())
            {
                PlanogramExportTemplateMapping match = planogramMappings.FirstOrDefault(m => m.Field == fieldMapping.Item1);
                if (match != null) match.ExternalField = fieldMapping.Item2;
            }

            planogramMappings = Mappings.Where(m => m.FieldType == PlanogramFieldMappingType.Component).ToList();
            foreach (Tuple<String, String> fieldMapping in EnumerateSpacemanV9DefaultMappingsForComponent())
            {
                PlanogramExportTemplateMapping match = planogramMappings.FirstOrDefault(m => m.Field == fieldMapping.Item1);
                if (match != null) match.ExternalField = fieldMapping.Item2;
            }

            planogramMappings = Mappings.Where(m => m.FieldType == PlanogramFieldMappingType.Product).ToList();
            foreach (Tuple<String, String> fieldMapping in EnumerateSpacemanV9DefaultMappingsForProduct())
            {
                PlanogramExportTemplateMapping match = planogramMappings.FirstOrDefault(m => m.Field == fieldMapping.Item1);
                if (match != null) match.ExternalField = fieldMapping.Item2;
            }

            #region Performance Metrics
            //PerformanceMetrics.Add(PlanogramExportTemplatePerformanceMetric.NewPlanogramExportTemplatePerformanceMetric(Message.PlanogramPerformance_DefaultMetric1_SalesValue, Message.PlanogramPerformance_DefaultMetric1_RegularSalesValue, SpacemanFieldHelper.ProductSales, 1));
            //PerformanceMetrics[0].MetricType = PlanogramMetricType.Decimal;
            //PerformanceMetrics.Add(PlanogramExportTemplatePerformanceMetric.NewPlanogramExportTemplatePerformanceMetric(Message.PlanogramPerformance_DefaultMetric2_SalesVolume, Message.PlanogramPerformance_DefaultMetric2_RegularSalesVolume, SpacemanFieldHelper.ProductRegMovement, 2));
            //PerformanceMetrics[1].MetricType = PlanogramMetricType.Integer;
            //PerformanceMetrics[1].SpecialType = PlanogramMetricSpecialType.RegularSalesUnits;
            //PerformanceMetrics.Add(PlanogramExportTemplatePerformanceMetric.NewPlanogramExportTemplatePerformanceMetric(Message.PlanogramPerformance_DefaultMetric3_SalesMargin, Message.PlanogramPerformance_DefaultMetric3_RegularSalesMargin, SpacemanFieldHelper.ProductUnitProfit, 3));
            //PerformanceMetrics[2].MetricType = PlanogramMetricType.Decimal;
            #endregion
        }

        private static IEnumerable<Tuple<String, String>> EnumerateSpacemanV9DefaultMappingsForPlanogram()
        {
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text1Property.Name),
                                          SpacemanFieldHelper.SectionString1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text2Property.Name),
                                          SpacemanFieldHelper.SectionString2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text3Property.Name),
                                          SpacemanFieldHelper.SectionString3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text4Property.Name),
                                          SpacemanFieldHelper.SectionString4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text5Property.Name),
                                          SpacemanFieldHelper.SectionString5);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value1Property.Name),
                                          SpacemanFieldHelper.SectionDouble1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value2Property.Name),
                                          SpacemanFieldHelper.SectionDouble2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value3Property.Name),
                                          SpacemanFieldHelper.SectionDouble3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value4Property.Name),
                                          SpacemanFieldHelper.SectionDouble4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value5Property.Name),
                                          SpacemanFieldHelper.SectionDouble5);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value6Property.Name),
                                          SpacemanFieldHelper.SectionInteger1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value7Property.Name),
                                          SpacemanFieldHelper.SectionInteger2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value8Property.Name),
                                          SpacemanFieldHelper.SectionInteger3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value9Property.Name),
                                          SpacemanFieldHelper.SectionInteger4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value10Property.Name),
                                          SpacemanFieldHelper.SectionInteger5);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value11Property.Name),
                                          SpacemanFieldHelper.SectionLong1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value12Property.Name),
                                          SpacemanFieldHelper.SectionLong2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value13Property.Name),
                                          SpacemanFieldHelper.SectionLong3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value14Property.Name),
                                          SpacemanFieldHelper.SectionLong4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value15Property.Name),
                                          SpacemanFieldHelper.SectionLong5);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Date1Property.Name),
                                          SpacemanFieldHelper.SectionDate);
        }

        private static IEnumerable<Tuple<String, String>> EnumerateSpacemanV9DefaultMappingsForFixture()
        {
            yield return
                new Tuple<String, String>(PlanogramFixture.NameProperty.Name,
                                          SpacemanFieldHelper.FixelName);
        }

        private static IEnumerable<Tuple<String, String>> EnumerateSpacemanV9DefaultMappingsForComponent()
        {
            yield return
                new Tuple<String, String>(PlanogramComponent.NameProperty.Name,
                                          SpacemanFieldHelper.FixelName);
            yield return
                new Tuple<String, String>(PlanogramComponent.ManufacturerProperty.Name,
                                          SpacemanFieldHelper.FixelManufacturer);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text1Property.Name),
                                          SpacemanFieldHelper.FixelString1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text2Property.Name),
                                          SpacemanFieldHelper.FixelString2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text3Property.Name),
                                          SpacemanFieldHelper.FixelString3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text4Property.Name),
                                          SpacemanFieldHelper.FixelString4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text5Property.Name),
                                          SpacemanFieldHelper.FixelString5);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value1Property.Name),
                                          SpacemanFieldHelper.FixelDouble1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value2Property.Name),
                                          SpacemanFieldHelper.FixelDouble2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value3Property.Name),
                                          SpacemanFieldHelper.FixelDouble3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value4Property.Name),
                                          SpacemanFieldHelper.FixelDouble4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value5Property.Name),
                                          SpacemanFieldHelper.FixelDouble5);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value6Property.Name),
                                          SpacemanFieldHelper.FixelInteger1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value7Property.Name),
                                          SpacemanFieldHelper.FixelInteger2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value8Property.Name),
                                          SpacemanFieldHelper.FixelInteger3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value9Property.Name),
                                          SpacemanFieldHelper.FixelInteger4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value10Property.Name),
                                          SpacemanFieldHelper.FixelInteger5);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value11Property.Name),
                                          SpacemanFieldHelper.FixelLong1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value12Property.Name),
                                          SpacemanFieldHelper.FixelLong2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value13Property.Name),
                                          SpacemanFieldHelper.FixelLong3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value14Property.Name),
                                          SpacemanFieldHelper.FixelLong4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value15Property.Name),
                                          SpacemanFieldHelper.FixelLong5);
        }

        private static IEnumerable<Tuple<String, String>> EnumerateSpacemanV9DefaultMappingsForProduct()
        {
            yield return
                new Tuple<String, String>(PlanogramProduct.GtinProperty.Name,
                                          SpacemanFieldHelper.ProductGTIN);
            yield return
                new Tuple<String, String>(PlanogramProduct.NameProperty.Name,
                                          SpacemanFieldHelper.ProductName);
            yield return
                new Tuple<String, String>(PlanogramProduct.BrandProperty.Name,
                                          SpacemanFieldHelper.ProductBrand);
            yield return
                new Tuple<String, String>(PlanogramProduct.SubcategoryProperty.Name,
                                          SpacemanFieldHelper.ProductSubcategory);
            yield return
                new Tuple<String, String>(PlanogramProduct.ColourProperty.Name,
                                          SpacemanFieldHelper.ProductColour);
            yield return
                new Tuple<String, String>(PlanogramProduct.ManufacturerProperty.Name,
                                          SpacemanFieldHelper.ProductManufacturer);
            yield return
                new Tuple<String, String>(PlanogramProduct.SizeProperty.Name,
                                          SpacemanFieldHelper.ProductSize);
            yield return
                new Tuple<String, String>(PlanogramProduct.UnitOfMeasureProperty.Name,
                                          SpacemanFieldHelper.ProductUOM);
            yield return
                new Tuple<String, String>(PlanogramProduct.SellPriceProperty.Name,
                                          SpacemanFieldHelper.ProductPrice);
            yield return
                new Tuple<String, String>(PlanogramProduct.CostPriceProperty.Name,
                                          SpacemanFieldHelper.ProductCost);
            yield return
                new Tuple<String, String>(PlanogramProduct.CaseCostProperty.Name,
                                          SpacemanFieldHelper.ProductCaseCost);
            yield return
                new Tuple<String, String>(PlanogramProduct.CasePackUnitsProperty.Name,
                                          SpacemanFieldHelper.ProductUnitsCase);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text1Property.Name),
                                          SpacemanFieldHelper.ProductDescA);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text2Property.Name),
                                          SpacemanFieldHelper.ProductDescB);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text3Property.Name),
                                          SpacemanFieldHelper.ProductDescC);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text4Property.Name),
                                          SpacemanFieldHelper.ProductDescD);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text5Property.Name),
                                          SpacemanFieldHelper.ProductDescE);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text6Property.Name),
                                          SpacemanFieldHelper.ProductDescF);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text7Property.Name),
                                          SpacemanFieldHelper.ProductDescG);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text8Property.Name),
                                          SpacemanFieldHelper.ProductDescH);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text9Property.Name),
                                          SpacemanFieldHelper.ProductDescI);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text10Property.Name),
                                          SpacemanFieldHelper.ProductDescJ);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text11Property.Name),
                                          SpacemanFieldHelper.ProductString1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text12Property.Name),
                                          SpacemanFieldHelper.ProductString2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text13Property.Name),
                                          SpacemanFieldHelper.ProductString3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text14Property.Name),
                                          SpacemanFieldHelper.ProductString4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text15Property.Name),
                                          SpacemanFieldHelper.ProductString5);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value1Property.Name),
                                          SpacemanFieldHelper.ProductDouble1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value2Property.Name),
                                          SpacemanFieldHelper.ProductDouble2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value3Property.Name),
                                          SpacemanFieldHelper.ProductDouble3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value4Property.Name),
                                          SpacemanFieldHelper.ProductDouble4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value5Property.Name),
                                          SpacemanFieldHelper.ProductDouble5);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value6Property.Name),
                                          SpacemanFieldHelper.ProductInteger1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value7Property.Name),
                                          SpacemanFieldHelper.ProductInteger2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value8Property.Name),
                                          SpacemanFieldHelper.ProductInteger3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value9Property.Name),
                                          SpacemanFieldHelper.ProductInteger4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value10Property.Name),
                                          SpacemanFieldHelper.ProductInteger5);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value11Property.Name),
                                          SpacemanFieldHelper.ProductLong1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value12Property.Name),
                                          SpacemanFieldHelper.ProductLong2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value13Property.Name),
                                          SpacemanFieldHelper.ProductLong3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value14Property.Name),
                                          SpacemanFieldHelper.ProductLong4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value15Property.Name),
                                          SpacemanFieldHelper.ProductLong5);
        }

        #endregion

        private void ApplySpacePlanningMappings()
        {
            foreach (PlanogramFieldMappingType value in Enum.GetValues(typeof(PlanogramFieldMappingType)))
            {
                IEnumerable<PlanogramExportTemplateMapping> planogramMappings = Mappings.Where(m => m.FieldType == value).ToList();
                foreach (Tuple<String, String> fieldMapping in JDAExportHelper.EnumerateDefaultMappings(value))
                {
                    PlanogramExportTemplateMapping match = planogramMappings.FirstOrDefault(m => m.Field == fieldMapping.Item1);
                    if (match != null) match.ExternalField = fieldMapping.Item2;
                }
            }

            PerformanceMetrics.AddRange(
                JDAExportHelper.EnumerateDefaultMetrics().Select(AsPlanogramExportTemplatePerformanceMetric));
        }

        private void ApplyApolloMappings()
        {
            foreach (PlanogramFieldMappingType value in Enum.GetValues(typeof(PlanogramFieldMappingType)))
            {
                IEnumerable<PlanogramExportTemplateMapping> planogramMappings = Mappings.Where(m => m.FieldType == value).ToList();
                foreach (Tuple<String, String> fieldMapping in ApolloExportHelper.EnumerateDefaultMappings(value))
                {
                    PlanogramExportTemplateMapping match = planogramMappings.FirstOrDefault(m => m.Field == fieldMapping.Item1);
                    if (match != null) match.ExternalField = fieldMapping.Item2;
                }
            }

            #region Performance Metrics
            //PerformanceMetrics.Add(PlanogramExportTemplatePerformanceMetric.NewPlanogramExportTemplatePerformanceMetric(Message.PlanogramPerformance_DefaultMetric1_SalesValue, Message.PlanogramPerformance_DefaultMetric1_RegularSalesValue, ApolloImportHelper.PositionSales, 1));
            //PerformanceMetrics[0].MetricType = PlanogramMetricType.Decimal;
            //PerformanceMetrics.Add(PlanogramExportTemplatePerformanceMetric.NewPlanogramExportTemplatePerformanceMetric(Message.PlanogramPerformance_DefaultMetric2_SalesVolume, Message.PlanogramPerformance_DefaultMetric2_RegularSalesVolume, ApolloImportHelper.ProductRealMovement, 2));
            //PerformanceMetrics[1].MetricType = PlanogramMetricType.Integer;
            //PerformanceMetrics[1].SpecialType = PlanogramMetricSpecialType.RegularSalesUnits;
            //PerformanceMetrics.Add(PlanogramExportTemplatePerformanceMetric.NewPlanogramExportTemplatePerformanceMetric(Message.PlanogramPerformance_DefaultMetric3_SalesMargin, Message.PlanogramPerformance_DefaultMetric3_RegularSalesMargin, ApolloImportHelper.PositionPROFIT, 3));
            //PerformanceMetrics[2].MetricType = PlanogramMetricType.Decimal;
            #endregion
        }

        private static String FormatCustomAttributePropertyName(String customPropertyName)
        {
            return String.Format("{0}.{1}", Planogram.CustomAttributesProperty.Name, customPropertyName);
        }

        private static PlanogramExportTemplatePerformanceMetric AsPlanogramExportTemplatePerformanceMetric(SpacePlanningImportHelper.MetricData value)
        {
            PlanogramExportTemplatePerformanceMetric metric = PlanogramExportTemplatePerformanceMetric
                .NewPlanogramExportTemplatePerformanceMetric(value.Name,
                                                             value.Description,
                                                             value.ExternalField,
                                                             value.MetricId);
            metric.MetricType = value.MetricType;
            metric.SpecialType = value.SpecialType;
            return metric;
        }

        #endregion

        #region Commands

        #region LockPlanogramExportTemplateCommand
        /// <summary>
        /// Performs the locking of a PlanogramExportTemplate
        /// </summary>
        [Serializable]
        private partial class LockPlanogramExportTemplateCommand : CommandBase<LockPlanogramExportTemplateCommand>
        {
            #region Properties

            #region DalFactoryType
            /// <summary>
            /// DalFactoryType property definition
            /// </summary>
            public static readonly PropertyInfo<PlanogramExportTemplateDalType> DalFactoryTypeProperty =
                RegisterProperty<PlanogramExportTemplateDalType>(c => c.DalFactoryType);
            /// <summary>
            /// Returns the dal factory type
            /// </summary>
            public PlanogramExportTemplateDalType DalFactoryType
            {
                get { return this.ReadProperty<PlanogramExportTemplateDalType>(DalFactoryTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public LockPlanogramExportTemplateCommand(PlanogramExportTemplateDalType dalType, Object id)
            {
                this.LoadProperty<PlanogramExportTemplateDalType>(DalFactoryTypeProperty, dalType);
                this.LoadProperty<Object>(IdProperty, id);
            }
            #endregion
        }
        #endregion

        #region UnlockPlanogramExportTemplateCommand
        /// <summary>
        /// Performs the unlocking of an object
        /// </summary>
        [Serializable]
        private partial class UnlockPlanogramExportTemplateCommand : CommandBase<UnlockPlanogramExportTemplateCommand>
        {
            #region Properties

            #region DalFactoryType
            /// <summary>
            /// DalFactoryType property definition
            /// </summary>
            public static readonly PropertyInfo<PlanogramExportTemplateDalType> DalFactoryTypeProperty =
                RegisterProperty<PlanogramExportTemplateDalType>(c => c.DalFactoryType);
            /// <summary>
            /// Returns the dal factory type
            /// </summary>
            public PlanogramExportTemplateDalType DalFactoryType
            {
                get { return this.ReadProperty<PlanogramExportTemplateDalType>(DalFactoryTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public UnlockPlanogramExportTemplateCommand(PlanogramExportTemplateDalType sourceType, Object id)
            {
                this.LoadProperty<PlanogramExportTemplateDalType>(DalFactoryTypeProperty, sourceType);
                this.LoadProperty<Object>(IdProperty, id);
            }
            #endregion
        }
        #endregion

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    if (!IsNew)
                    {
                        //if (this.DalFactoryType == PlanogramExportTemplateDalType.FileSystem
                        //    && this.Id is String)
                        //{
                        //    UnlockPlanogramExportTemplateByFileName((String)Id);
                        //}
                    }
                }
                _isDisposed = true;
            }
        }

        #endregion
    }
}
