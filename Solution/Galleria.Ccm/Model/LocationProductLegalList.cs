﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a list containing LocationProductLegal objects
    /// </summary>
    [Serializable]
    public partial class LocationProductLegalList : ModelList<LocationProductLegalList, LocationProductLegal>
    {
        #region Authorisation Rules
        /// <summary>
        /// defines authorisation rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationProductLegalList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationProductLegalCreate.ToString()));
            BusinessRules.AddRule(typeof(LocationProductLegalList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.LocationProductLegalGet.ToString()));
            BusinessRules.AddRule(typeof(LocationProductLegalList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationProductLegalEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationProductLegalList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationProductLegalDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>a new list</returns>
        public static LocationProductLegalList NewList()
        {
            LocationProductLegalList list = new LocationProductLegalList();
            list.Create();
            return list;
        }

        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByEntityId
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : Csla.CriteriaBase<FetchByEntityIdCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
            RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
            }
        }

        #endregion
    }
}
