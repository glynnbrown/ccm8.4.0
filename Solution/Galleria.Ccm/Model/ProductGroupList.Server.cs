﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public sealed partial class ProductGroupList
    {
        #region Constructor
        private ProductGroupList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        /// <returns>A list of existing it</returns>
        internal static ProductGroupList FetchByParentProductGroupId(
            IDalContext dalContext, Int32 parentId, IEnumerable<ProductGroupDto> productGroupDtoList)
        {
            return DataPortal.FetchChild<ProductGroupList>(dalContext, parentId, productGroupDtoList);
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parentNodeId">The parent node id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 parentId, IEnumerable<ProductGroupDto> productGroupDtoList)
        {
            RaiseListChangedEvents = false;

            //get a list of child dtos and add them to this
            foreach (ProductGroupDto childDto in productGroupDtoList)
            {
                if (Object.Equals(childDto.ParentGroupId, parentId))
                {
                    this.Add(ProductGroup.Fetch(dalContext, childDto, productGroupDtoList));
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}