﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32524 : L.Ineson
//  Created
// V8-32636 : M.Brumby
//  Resolve Id Fix
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    public partial class FixtureAnnotation
    {
        #region Constructor
        private FixtureAnnotation() { } // Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static FixtureAnnotation Fetch(IDalContext dalContext, FixtureAnnotationDto dto)
        {
            return DataPortal.FetchChild<FixtureAnnotation>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this object from the given dto
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, FixtureAnnotationDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32?>(FixtureItemIdProperty, dto.FixtureItemId);
            this.LoadProperty<Int32?>(FixtureAssemblyItemIdProperty, dto.FixtureAssemblyItemId);
            this.LoadProperty<Int32?>(FixtureComponentItemIdProperty, dto.FixtureComponentItemId);
            this.LoadProperty<Int32?>(FixtureAssemblyComponentIdProperty, dto.FixtureAssemblyComponentId);
            this.LoadProperty<Int32?>(FixtureSubComponentIdProperty, dto.FixtureSubComponentId);
            this.LoadProperty<PlanogramAnnotationType>(AnnotationTypeProperty, (PlanogramAnnotationType)dto.AnnotationType);
            this.LoadProperty<String>(TextProperty, dto.Text);
            this.LoadProperty<Single?>(XProperty, dto.X);
            this.LoadProperty<Single?>(YProperty, dto.Y);
            this.LoadProperty<Single?>(ZProperty, dto.Z);
            this.LoadProperty<Single>(HeightProperty, dto.Height);
            this.LoadProperty<Single>(WidthProperty, dto.Width);
            this.LoadProperty<Single>(DepthProperty, dto.Depth);
            this.LoadProperty<Int32>(BorderColourProperty, dto.BorderColour);
            this.LoadProperty<Single>(BorderThicknessProperty, dto.BorderThickness);
            this.LoadProperty<Int32>(BackgroundColourProperty, dto.BackgroundColour);
            this.LoadProperty<Int32>(FontColourProperty, dto.FontColour);
            this.LoadProperty<Single>(FontSizeProperty, dto.FontSize);
            this.LoadProperty<String>(FontNameProperty, dto.FontName);
            this.LoadProperty<Boolean>(CanReduceFontToFitProperty, dto.CanReduceFontToFit);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private FixtureAnnotationDto GetDataTransferObject(FixturePackage parent)
        {
            return new FixtureAnnotationDto()
            {
                Id = this.ReadProperty<Int32>(IdProperty),
                FixturePackageId = parent.Id,
                FixtureItemId = this.ReadProperty<Int32?>(FixtureItemIdProperty),
                FixtureAssemblyItemId = this.ReadProperty<Int32?>(FixtureAssemblyItemIdProperty),
                FixtureComponentItemId = this.ReadProperty<Int32?>(FixtureComponentItemIdProperty),
                FixtureAssemblyComponentId = this.ReadProperty<Int32?>(FixtureAssemblyComponentIdProperty),
                FixtureSubComponentId = this.ReadProperty<Int32?>(FixtureSubComponentIdProperty),
                AnnotationType = (Byte)this.ReadProperty<PlanogramAnnotationType>(AnnotationTypeProperty),
                Text = this.ReadProperty<String>(TextProperty),
                X = this.ReadProperty<Single?>(XProperty),
                Y = this.ReadProperty<Single?>(YProperty),
                Z = this.ReadProperty<Single?>(ZProperty),
                Height = this.ReadProperty<Single>(HeightProperty),
                Width = this.ReadProperty<Single>(WidthProperty),
                Depth = this.ReadProperty<Single>(DepthProperty),
                BorderColour = this.ReadProperty<Int32>(BorderColourProperty),
                BorderThickness = this.ReadProperty<Single>(BorderThicknessProperty),
                BackgroundColour = this.ReadProperty<Int32>(BackgroundColourProperty),
                FontColour = this.ReadProperty<Int32>(FontColourProperty),
                FontSize = this.ReadProperty<Single>(FontSizeProperty),
                FontName = this.ReadProperty<String>(FontNameProperty),
                CanReduceFontToFit = this.ReadProperty<Boolean>(CanReduceFontToFitProperty),
            };
        }



        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, FixtureAnnotationDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(IDalContext dalContext, FixturePackage parent)
        {
            this.ResolveIds(dalContext);
            FixtureAnnotationDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IFixtureAnnotationDal dal = dalContext.GetDal<IFixtureAnnotationDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<FixtureAnnotation>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, FixturePackage parent)
        {
            if (this.IsSelfDirty)
            {
                using (IFixtureAnnotationDal dal = dalContext.GetDal<IFixtureAnnotationDal>())
                {
                    this.ResolveIds(dalContext);
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, FixturePackage parent)
        {
            using (IFixtureAnnotationDal dal = dalContext.GetDal<IFixtureAnnotationDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion
    }
}
