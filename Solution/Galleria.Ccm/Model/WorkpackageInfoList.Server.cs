﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
// V8-25757 : L.Ineson
//  Removed PlanogramGroup_Id link
//  Added Entity_Id link
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class WorkpackageInfoList
    {
        #region Constructors
        public WorkpackageInfoList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        #region FetchByEntityId
        /// <summary>
        /// Returns all workpackages for the specified product group
        /// </summary>
        public static WorkpackageInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<WorkpackageInfoList>(new FetchByEntityIdCriteria(entityId));
        }

        /// <summary>
        /// Returns all workpackages for the specified product group
        /// </summary>
        public static WorkpackageInfoList FetchByEntityId(Entity entity)
        {
            return DataPortal.Fetch<WorkpackageInfoList>(new FetchByEntityIdCriteria(entity.Id));
        }
        #endregion

        #endregion

        #region Data Access
        /// <summary>
        /// Called when returning all workflows for the specified entity
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IWorkpackageInfoDal dal = dalContext.GetDal<IWorkpackageInfoDal>())
                {
                    IEnumerable<WorkpackageInfoDto> dtoList = dal.FetchByEntityId(criteria.EntityId);
                    foreach (WorkpackageInfoDto dto in dtoList)
                    {
                        this.Add(WorkpackageInfo.GetWorkpackageInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }
        #endregion
    }
}
