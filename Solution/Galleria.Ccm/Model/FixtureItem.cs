﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
// V8-26182 : L.Ineson
//  Added NewPlanogramFixtureItem
// V8-27558 : L.Ineson
//  Removed IsBay
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// FixtureItem Model object
    /// </summary>
    [Serializable]
    public sealed partial class FixtureItem : ModelObject<FixtureItem>
    {
        #region Static Constructor
        static FixtureItem()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new FixturePackage Parent
        {
            get
            {
                FixtureItemList parentList = base.Parent as FixtureItemList;
                if (parentList != null)
                {
                    return parentList.Parent;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region FixtureId

        /// <summary>
        /// FixtureId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FixtureIdProperty =
            RegisterModelProperty<Int32>(c => c.FixtureId);

        /// <summary>
        /// Gets/Sets the FixtureId value
        /// </summary>
        public Int32 FixtureId
        {
            get { return GetProperty<Int32>(FixtureIdProperty); }
            set { SetProperty<Int32>(FixtureIdProperty, value); }
        }

        #endregion

        #region X

        /// <summary>
        /// X property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> XProperty =
            RegisterModelProperty<Single>(c => c.X);

        /// <summary>
        /// Gets/Sets the X value
        /// </summary>
        public Single X
        {
            get { return GetProperty<Single>(XProperty); }
            set { SetProperty<Single>(XProperty, value); }
        }

        #endregion

        #region Y

        /// <summary>
        /// Y property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> YProperty =
            RegisterModelProperty<Single>(c => c.Y);

        /// <summary>
        /// Gets/Sets the Y value
        /// </summary>
        public Single Y
        {
            get { return GetProperty<Single>(YProperty); }
            set { SetProperty<Single>(YProperty, value); }
        }

        #endregion

        #region Z

        /// <summary>
        /// Z property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ZProperty =
            RegisterModelProperty<Single>(c => c.Z);

        /// <summary>
        /// Gets/Sets the Z value
        /// </summary>
        public Single Z
        {
            get { return GetProperty<Single>(ZProperty); }
            set { SetProperty<Single>(ZProperty, value); }
        }

        #endregion

        #region Slope

        /// <summary>
        /// Slope property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SlopeProperty =
            RegisterModelProperty<Single>(c => c.Slope);

        /// <summary>
        /// Gets/Sets the Slope value
        /// </summary>
        public Single Slope
        {
            get { return GetProperty<Single>(SlopeProperty); }
            set { SetProperty<Single>(SlopeProperty, value); }
        }

        #endregion

        #region Angle

        /// <summary>
        /// Angle property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AngleProperty =
            RegisterModelProperty<Single>(c => c.Angle);

        /// <summary>
        /// Gets/Sets the Angle value
        /// </summary>
        public Single Angle
        {
            get { return GetProperty<Single>(AngleProperty); }
            set { SetProperty<Single>(AngleProperty, value); }
        }

        #endregion

        #region Roll

        /// <summary>
        /// Roll property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> RollProperty =
            RegisterModelProperty<Single>(c => c.Roll);

        /// <summary>
        /// Gets/Sets the Roll value
        /// </summary>
        public Single Roll
        {
            get { return GetProperty<Single>(RollProperty); }
            set { SetProperty<Single>(RollProperty, value); }
        }

        #endregion

        #region IsBay

        /// <summary>
        /// IsBay property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsBayProperty =
            RegisterModelProperty<Boolean>(c => c.IsBay);

        /// <summary>
        /// Gets/Sets the IsBay value
        /// </summary>
        public Boolean IsBay
        {
            get { return GetProperty<Boolean>(IsBayProperty); }
            set { SetProperty<Boolean>(IsBayProperty, value); }
        }

        #endregion


        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();


        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static FixtureItem NewFixtureItem()
        {
            FixtureItem item = new FixtureItem();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        internal static FixtureItem NewFixtureItem(PlanogramFixtureItem planFixtureItem)
        {
            FixtureItem item = new FixtureItem();
            item.Create(planFixtureItem);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Boolean>(IsBayProperty, true);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(PlanogramFixtureItem planFixtureItem)
        {
            this.LoadProperty<Int32>(IdProperty, (Int32)planFixtureItem.Id);
            this.LoadProperty<Int32>(FixtureIdProperty, (Int32)planFixtureItem.PlanogramFixtureId);
            this.LoadProperty<Single>(XProperty, planFixtureItem.X);
            this.LoadProperty<Single>(YProperty, planFixtureItem.Y);
            this.LoadProperty<Single>(ZProperty, planFixtureItem.Z);
            this.LoadProperty<Single>(SlopeProperty, planFixtureItem.Slope);
            this.LoadProperty<Single>(AngleProperty, planFixtureItem.Angle);
            this.LoadProperty<Single>(RollProperty, planFixtureItem.Roll);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<FixtureItem>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes on this instance
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }

        /// <summary>
        /// Called when resolving ids on this instance
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // FixtureId
            Object fixtureId = context.ResolveId<Fixture>(this.ReadProperty<Int32>(FixtureIdProperty));
            if (fixtureId != null) this.LoadProperty<Int32>(FixtureIdProperty, (Int32)fixtureId);
        }


        /// <summary>
        /// Updates this fixture item on the plan.
        /// </summary>
        /// <param name="plan"></param>
        /// <returns></returns>
        public PlanogramFixtureItem AddOrUpdatePlanogramFixtureItem(Planogram plan)
        {
            //update the fixture first
            Fixture fixure = GetFixture();
            if (fixure != null) fixure.AddOrUpdatePlanogramFixture(plan);

            PlanogramFixtureItem item = plan.FixtureItems.FindById(this.Id);
            if (item == null)
            {
                 item = PlanogramFixtureItem.NewPlanogramFixtureItem();
                 item.Id = this.Id;
                 item.PlanogramFixtureId = this.FixtureId;
                 plan.FixtureItems.Add(item);
            }

            item.X = this.X;
            item.Y = this.Y;
            item.Z = this.Z;
            item.Slope = this.Slope;
            item.Angle = this.Angle;
            item.Roll = this.Roll;
            item.PlanogramFixtureId = this.FixtureId;


            return item;
        }

        public Fixture GetFixture()
        {
            if (this.FixtureId == null) return null;
            if (this.Parent == null) return null;
            return this.Parent.Fixtures.FindById(this.FixtureId);
        }


        #endregion
    }
}