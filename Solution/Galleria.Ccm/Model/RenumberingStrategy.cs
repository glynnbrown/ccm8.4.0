﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva
//      Created.
// V8-27562 : A.Silva
//      Added FetchByEntityIdNameCriteria.
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.

#endregion

#region Version History: CCM 801

// V8-28923 : A.Silva
//      Amended DateDeleted Property to be a Nullable DateTime.
// V8-28937 : A.Silva
//      Added friendly names for the Strategy Priorities and Strategy Types.
// V8-28878 : A.Silva
//      Amended name of RestartComponentRenumberingPerBayProperty to make it consistent with the database field name.

#endregion
#region Version History: CCM810
// V8-29818 : L.Ineson
//  Amended defaults.
#endregion

#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Represents the settings for a particular planogram renumbering strategy.
    /// </summary>
    [Serializable]
    public sealed partial class RenumberingStrategy : ModelObject<RenumberingStrategy>, IPlanogramRenumberingStrategy
    {
        #region Business Rules

        /// <summary>
        ///     Adds business rules to this instance.
        /// </summary>
        protected override void AddBusinessRules()
        {
            this.BusinessRules.AddRule(new Required(NameProperty));
            this.BusinessRules.AddRule(new MaxLength(NameProperty, 100));
            this.BusinessRules.AddRule(new MinValue<Byte>(BayComponentXRenumberStrategyPriorityProperty, 1));
            this.BusinessRules.AddRule(new MaxValue<Byte>(BayComponentXRenumberStrategyPriorityProperty, 3));
            this.BusinessRules.AddRule(new MinValue<Byte>(BayComponentYRenumberStrategyPriorityProperty, 1));
            this.BusinessRules.AddRule(new MaxValue<Byte>(BayComponentYRenumberStrategyPriorityProperty, 3));
            this.BusinessRules.AddRule(new MinValue<Byte>(BayComponentZRenumberStrategyPriorityProperty, 1));
            this.BusinessRules.AddRule(new MaxValue<Byte>(BayComponentZRenumberStrategyPriorityProperty, 3));
            this.BusinessRules.AddRule(new MinValue<Byte>(PositionXRenumberStrategyPriorityProperty, 1));
            this.BusinessRules.AddRule(new MaxValue<Byte>(PositionXRenumberStrategyPriorityProperty, 3));
            this.BusinessRules.AddRule(new MinValue<Byte>(PositionYRenumberStrategyPriorityProperty, 1));
            this.BusinessRules.AddRule(new MaxValue<Byte>(PositionYRenumberStrategyPriorityProperty, 3));
            this.BusinessRules.AddRule(new MinValue<Byte>(PositionZRenumberStrategyPriorityProperty, 1));
            this.BusinessRules.AddRule(new MaxValue<Byte>(PositionZRenumberStrategyPriorityProperty, 3));

            base.AddBusinessRules();
        }

        #endregion

        #region Authorization Rules

        /// <summary>
        ///     Defines the authorization rules for this type.
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof (RenumberingStrategy),
                new IsInRole(AuthorizationActions.GetObject, DomainPermission.RenumberingStrategyGet.ToString()));
            BusinessRules.AddRule(typeof (RenumberingStrategy),
                new IsInRole(AuthorizationActions.CreateObject, DomainPermission.RenumberingStrategyCreate.ToString()));
            BusinessRules.AddRule(typeof (RenumberingStrategy),
                new IsInRole(AuthorizationActions.EditObject, DomainPermission.RenumberingStrategyEdit.ToString()));
            BusinessRules.AddRule(typeof (RenumberingStrategy),
                new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.RenumberingStrategyDelete.ToString()));
        }

        #endregion

        #region Properties

        #region Id

        /// <summary>
        ///     Metadata for the <see cref="Id" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(o => o.Id);

        /// <summary>
        ///     Gets or sets the value for the <see cref="Id" /> property.
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty(IdProperty); }
        }

        #endregion

        #region RowVersion

        /// <summary>
        ///     Metadata for the <see cref="RowVersion" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(o => o.RowVersion);

        /// <summary>
        ///     Gets or sets the value for the <see cref="RowVersion" /> property.
        /// </summary>
        private RowVersion RowVersion
        {
            get { return this.GetProperty(RowVersionProperty); }
        }

        #endregion

        #region EntityId

        /// <summary>
        ///     Metadata for the <see cref="EntityId" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(o => o.EntityId);

        /// <summary>
        ///     Gets or sets the value for the <see cref="EntityId" /> property.
        /// </summary>
        public Int32 EntityId
        {
            get { return this.GetProperty(EntityIdProperty); }
        }

        #endregion

        #region Name

        /// <summary>
        ///     Metadata for the <see cref="Name" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(o => o.Name, "Name");

        /// <summary>
        ///     Gets or sets the value for the <see cref="Name" /> property.
        /// </summary>
        public String Name
        {
            get { return this.GetProperty(NameProperty); }
            set { this.SetProperty(NameProperty, value); }
        }

        #endregion

        #region BayComponentXRenumberStrategyPriority

        /// <summary>
        ///     Metadata for the <see cref="BayComponentXRenumberStrategyPriority" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Byte> BayComponentXRenumberStrategyPriorityProperty =
            RegisterModelProperty<Byte>(o => o.BayComponentXRenumberStrategyPriority, Message.RenumberingStrategy_BayComponentXRenumberStrategyPriority);

        /// <summary>
        ///     Gets or sets the value for the <see cref="BayComponentXRenumberStrategyPriority" /> property.
        /// </summary>
        public Byte BayComponentXRenumberStrategyPriority
        {
            get { return this.GetProperty(BayComponentXRenumberStrategyPriorityProperty); }
            set { this.SetProperty(BayComponentXRenumberStrategyPriorityProperty, value); }
        }

        #endregion

        #region BayComponentYRenumberStrategyPriority

        /// <summary>
        ///     Metadata for the <see cref="BayComponentYRenumberStrategyPriority" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Byte> BayComponentYRenumberStrategyPriorityProperty =
            RegisterModelProperty<Byte>(o => o.BayComponentYRenumberStrategyPriority, Message.RenumberingStrategy_BayComponentYRenumberStrategyPriority);

        /// <summary>
        ///     Gets or sets the value for the <see cref="BayComponentYRenumberStrategyPriority" /> property.
        /// </summary>
        public Byte BayComponentYRenumberStrategyPriority
        {
            get { return this.GetProperty(BayComponentYRenumberStrategyPriorityProperty); }
            set { this.SetProperty(BayComponentYRenumberStrategyPriorityProperty, value); }
        }

        #endregion

        #region BayComponentZRenumberStrategyPriority

        /// <summary>
        ///     Metadata for the <see cref="BayComponentZRenumberStrategyPriority" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Byte> BayComponentZRenumberStrategyPriorityProperty =
            RegisterModelProperty<Byte>(o => o.BayComponentZRenumberStrategyPriority, Message.RenumberingStrategy_BayComponentZRenumberStrategyPriority);

        /// <summary>
        ///     Gets or sets the value for the <see cref="BayComponentZRenumberStrategyPriority" /> property.
        /// </summary>
        public Byte BayComponentZRenumberStrategyPriority
        {
            get { return this.GetProperty(BayComponentZRenumberStrategyPriorityProperty); }
            set { this.SetProperty(BayComponentZRenumberStrategyPriorityProperty, value); }
        }

        #endregion

        #region PositionXRenumberStrategyPriority

        /// <summary>
        ///     Metadata for the <see cref="PositionXRenumberStrategyPriority" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Byte> PositionXRenumberStrategyPriorityProperty =
            RegisterModelProperty<Byte>(o => o.PositionXRenumberStrategyPriority, Message.RenumberingStrategy_PositionXRenumberStrategyPriority);

        /// <summary>
        ///     Gets or sets the value for the <see cref="PositionXRenumberStrategyPriority" /> property.
        /// </summary>
        public Byte PositionXRenumberStrategyPriority
        {
            get { return this.GetProperty(PositionXRenumberStrategyPriorityProperty); }
            set { this.SetProperty(PositionXRenumberStrategyPriorityProperty, value); }
        }

        #endregion

        #region PositionYRenumberStrategyPriority

        /// <summary>
        ///     Metadata for the <see cref="PositionYRenumberStrategyPriority" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Byte> PositionYRenumberStrategyPriorityProperty =
            RegisterModelProperty<Byte>(o => o.PositionYRenumberStrategyPriority, Message.RenumberingStrategy_PositionYRenumberStrategyPriority);

        /// <summary>
        ///     Gets or sets the value for the <see cref="PositionYRenumberStrategyPriority" /> property.
        /// </summary>
        public Byte PositionYRenumberStrategyPriority
        {
            get { return this.GetProperty(PositionYRenumberStrategyPriorityProperty); }
            set { this.SetProperty(PositionYRenumberStrategyPriorityProperty, value); }
        }

        #endregion

        #region PositionZRenumberStrategyPriority

        /// <summary>
        ///     Metadata for the <see cref="PositionZRenumberStrategyPriority" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Byte> PositionZRenumberStrategyPriorityProperty =
            RegisterModelProperty<Byte>(o => o.PositionZRenumberStrategyPriority, Message.RenumberingStrategy_PositionZRenumberStrategyPriority);

        /// <summary>
        ///     Gets or sets the value for the <see cref="PositionZRenumberStrategyPriority" /> property.
        /// </summary>
        public Byte PositionZRenumberStrategyPriority
        {
            get { return this.GetProperty(PositionZRenumberStrategyPriorityProperty); }
            set { this.SetProperty(PositionZRenumberStrategyPriorityProperty, value); }
        }

        #endregion

        #region BayComponentXRenumberStrategy

        /// <summary>
        ///     Metadata for the <see cref="BayComponentXRenumberStrategy" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramRenumberingStrategyXRenumberType>
            BayComponentXRenumberStrategyProperty =
                RegisterModelProperty<PlanogramRenumberingStrategyXRenumberType>(o => o.BayComponentXRenumberStrategy, Message.RenumberingStrategy_BayComponentXRenumberStrategy);

        /// <summary>
        ///     Gets or sets the type of X (horizontal) renumbering strategy to apply to bays and components.
        /// </summary>
        public PlanogramRenumberingStrategyXRenumberType BayComponentXRenumberStrategy
        {
            get { return this.GetProperty(BayComponentXRenumberStrategyProperty); }
            set { this.SetProperty(BayComponentXRenumberStrategyProperty, value); }
        }

        #endregion

        #region BayComponentYRenumberStrategy

        /// <summary>
        ///     Metadata for the <see cref="BayComponentYRenumberStrategy" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramRenumberingStrategyYRenumberType>
            BayComponentYRenumberStrategyProperty =
                RegisterModelProperty<PlanogramRenumberingStrategyYRenumberType>(o => o.BayComponentYRenumberStrategy, Message.RenumberingStrategy_BayComponentYRenumberStrategy);

        /// <summary>
        ///     Gets or sets the type of Y (vertical) renumbering strategy to apply to bays and components.
        /// </summary>
        public PlanogramRenumberingStrategyYRenumberType BayComponentYRenumberStrategy
        {
            get { return this.GetProperty(BayComponentYRenumberStrategyProperty); }
            set { this.SetProperty(BayComponentYRenumberStrategyProperty, value); }
        }

        #endregion

        #region BayComponentZRenumberStrategy

        /// <summary>
        ///     Metadata for the <see cref="BayComponentZRenumberStrategy" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramRenumberingStrategyZRenumberType>
            BayComponentZRenumberStrategyProperty =
                RegisterModelProperty<PlanogramRenumberingStrategyZRenumberType>(o => o.BayComponentZRenumberStrategy, Message.RenumberingStrategy_BayComponentZRenumberStrategy);

        /// <summary>
        ///     Gets or sets the type of Z (depth) renumbering strategy to apply to bays and components.
        /// </summary>
        public PlanogramRenumberingStrategyZRenumberType BayComponentZRenumberStrategy
        {
            get { return this.GetProperty(BayComponentZRenumberStrategyProperty); }
            set { this.SetProperty(BayComponentZRenumberStrategyProperty, value); }
        }

        #endregion

        #region PositionXRenumberStrategy

        /// <summary>
        ///     Metadata for the <see cref="PositionXRenumberStrategy" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramRenumberingStrategyXRenumberType>
            PositionXRenumberStrategyProperty =
                RegisterModelProperty<PlanogramRenumberingStrategyXRenumberType>(o => o.PositionXRenumberStrategy, Message.RenumberingStrategy_PositionXRenumberStrategy);

        /// <summary>
        ///     Gets or sets the type of X (horizontal) renumbering strategy to apply to positions.
        /// </summary>
        public PlanogramRenumberingStrategyXRenumberType PositionXRenumberStrategy
        {
            get { return this.GetProperty(PositionXRenumberStrategyProperty); }
            set { this.SetProperty(PositionXRenumberStrategyProperty, value); }
        }

        #endregion

        #region PositionYRenumberStrategy

        /// <summary>
        ///     Metadata for the <see cref="PositionYRenumberStrategy" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramRenumberingStrategyYRenumberType>
            PositionYRenumberStrategyProperty =
                RegisterModelProperty<PlanogramRenumberingStrategyYRenumberType>(o => o.PositionYRenumberStrategy, Message.RenumberingStrategy_PositionYRenumberStrategy);

        /// <summary>
        ///     Gets or sets the type of Y (vertical) renumbering strategy to apply to positions.
        /// </summary>
        public PlanogramRenumberingStrategyYRenumberType PositionYRenumberStrategy
        {
            get { return this.GetProperty(PositionYRenumberStrategyProperty); }
            set { this.SetProperty(PositionYRenumberStrategyProperty, value); }
        }

        #endregion

        #region PositionZRenumberStrategy

        /// <summary>
        ///     Metadata for the <see cref="PositionZRenumberStrategy" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramRenumberingStrategyZRenumberType>
            PositionZRenumberStrategyProperty =
                RegisterModelProperty<PlanogramRenumberingStrategyZRenumberType>(o => o.PositionZRenumberStrategy, Message.RenumberingStrategy_PositionZRenumberStrategy);

        /// <summary>
        ///     Gets or sets the type of Z (depth) renumbering strategy to apply to positions.
        /// </summary>
        public PlanogramRenumberingStrategyZRenumberType PositionZRenumberStrategy
        {
            get { return this.GetProperty(PositionZRenumberStrategyProperty); }
            set { this.SetProperty(PositionZRenumberStrategyProperty, value); }
        }

        #endregion

        #region RestartComponentRenumberingPerBay

        /// <summary>
        ///     Metadata for the <see cref="RestartComponentRenumberingPerBay" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean>
            RestartComponentRenumberingPerBayProperty =
                RegisterModelProperty<Boolean>(o => o.RestartComponentRenumberingPerBay);

        /// <summary>
        ///     Gets or sets whether component sequence renumbering restarts from #1 for each bay.
        /// </summary>
        public Boolean RestartComponentRenumberingPerBay
        {
            get { return this.GetProperty(RestartComponentRenumberingPerBayProperty); }
            set { this.SetProperty(RestartComponentRenumberingPerBayProperty, value); }
        }

        #endregion

        #region RestartComponentRenumberingPerComponentType

        /// <summary>
        ///     Metadata for the <see cref="RestartComponentRenumberingPerComponentType" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean>
            RestartComponentRenumberingPerComponentTypeProperty =
                RegisterModelProperty<Boolean>(o => o.RestartComponentRenumberingPerComponentType);

        /// <summary>
        ///     Gets or sets whether component sequence renumbering restarts from #1 for each component type.
        /// </summary>
        public Boolean RestartComponentRenumberingPerComponentType
        {
            get { return this.GetProperty(RestartComponentRenumberingPerComponentTypeProperty); }
            set { this.SetProperty(RestartComponentRenumberingPerComponentTypeProperty, value); }
        }

        #endregion

        #region IgnoreNonMerchandisingComponents

        /// <summary>
        ///     Metadata for the <see cref="IgnoreNonMerchandisingComponents" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> IgnoreNonMerchandisingComponentsProperty =
            RegisterModelProperty<Boolean>(o => o.IgnoreNonMerchandisingComponents);

        /// <summary>
        ///     Gets or sets whether component sequence renumbering ignores non merchandising ones.
        /// </summary>
        public Boolean IgnoreNonMerchandisingComponents
        {
            get { return this.GetProperty(IgnoreNonMerchandisingComponentsProperty); }
            set { this.SetProperty(IgnoreNonMerchandisingComponentsProperty, value); }
        }

        #endregion

        #region RestartPositionRenumberingPerComponent

        /// <summary>
        ///     Metadata for the <see cref="RestartPositionRenumberingPerComponent" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> RestartPositionRenumberingPerComponentProperty =
            RegisterModelProperty<Boolean>(o => o.RestartPositionRenumberingPerComponent);

        /// <summary>
        ///     Gets or sets whether position sequence renumbering restarts from #1 for each component.
        /// </summary>
        public Boolean RestartPositionRenumberingPerComponent
        {
            get { return this.GetProperty(RestartPositionRenumberingPerComponentProperty); }
            set { this.SetProperty(RestartPositionRenumberingPerComponentProperty, value); }
        }

        #endregion

        #region RestartPositionRenumberingPerBay

        /// <summary>
        ///     Metadata for the <see cref="RestartPositionRenumberingPerBay" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> RestartPositionRenumberingPerBayProperty =
            RegisterModelProperty<Boolean>(o => o.RestartPositionRenumberingPerBay);

        /// <summary>
        ///     Gets or sets whether position sequence renumbering restarts from #1 for each Bay.
        /// </summary>
        public Boolean RestartPositionRenumberingPerBay
        {
            get { return this.GetProperty(RestartPositionRenumberingPerBayProperty); }
            set { this.SetProperty(RestartPositionRenumberingPerBayProperty, value); }
        }

        #endregion

        #region UniqueNumberMultiPositionProductsPerComponent

        /// <summary>
        ///     Metadata for the <see cref="UniqueNumberMultiPositionProductsPerComponent" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> UniqueNumberMultiPositionProductsPerComponentProperty =
            RegisterModelProperty<Boolean>(o => o.UniqueNumberMultiPositionProductsPerComponent);

        /// <summary>
        ///     Gets or sets whether different positions for the same product get unique sequence numbers.
        /// </summary>
        public Boolean UniqueNumberMultiPositionProductsPerComponent
        {
            get { return this.GetProperty(UniqueNumberMultiPositionProductsPerComponentProperty); }
            set { this.SetProperty(UniqueNumberMultiPositionProductsPerComponentProperty, value); }
        }

        #endregion

        #region ExceptAdjacentPositions

        /// <summary>
        ///     Metadata for the <see cref="ExceptAdjacentPositions" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ExceptAdjacentPositionsProperty =
            RegisterModelProperty<Boolean>(o => o.ExceptAdjacentPositions);

        /// <summary>
        ///     Gets or sets whether different positions for the same product, but adjacent,
        ///     get the same sequence number when unique sequence numbers per position is in place.
        /// </summary>
        public Boolean ExceptAdjacentPositions
        {
            get { return this.GetProperty(ExceptAdjacentPositionsProperty); }
            set { this.SetProperty(ExceptAdjacentPositionsProperty, value); }
        }

        #endregion

        #region DateCreated

        /// <summary>
        ///     Metadata for the <see cref="DateCreated" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(o => o.DateCreated);

        /// <summary>
        ///     Gets or sets the value for the <see cref="DateCreated" /> property.
        /// </summary>
        private DateTime DateCreated
        {
            get { return this.GetProperty(DateCreatedProperty); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        ///     Metadata for the <see cref="DateLastModified" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(o => o.DateLastModified);

        /// <summary>
        ///     Gets or sets the value for the <see cref="DateLastModified" /> property.
        /// </summary>
        private DateTime DateLastModified
        {
            get { return this.GetProperty(DateLastModifiedProperty); }
        }

        #endregion

        #region DateDeleted

        /// <summary>
        ///     Metadata for the <see cref="DateDeleted" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(o => o.DateDeleted);

        /// <summary>
        ///     Gets or sets the value for the <see cref="DateDeleted" /> property.
        /// </summary>
        private DateTime? DateDeleted
        {
            get { return this.GetProperty(DateDeletedProperty); }
        }

        #endregion

        #endregion

        #region Criteria

        #region FetchById

        public sealed class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Constructor

            /// <summary>
            ///     Creates a new instance of <see cref="FetchByIdCriteria" />.
            /// </summary>
            /// <param name="id">Unique id value identifying the instance to be fetched.</param>
            public FetchByIdCriteria(Int32 id)
            {
                this.LoadProperty(IdProperty, id);
            }

            #endregion

            #region Properties

            #region Id

            /// <summary>
            ///     Metadata for the <see cref="Id" /> property.
            /// </summary>
            private static readonly PropertyInfo<Int32> IdProperty =
                RegisterProperty<Int32>(o => o.Id);

            /// <summary>
            ///     Gets or sets the value for the <see cref="Id" /> property.
            /// </summary>
            public Int32 Id
            {
                get { return this.ReadProperty(IdProperty); }
            }

            #endregion

            #endregion
        }

        #endregion

        #region FetchById

        public sealed class FetchByEntityIdNameCriteria : CriteriaBase<FetchByEntityIdNameCriteria>
        {
            #region Constructor

            /// <summary>
            ///     Creates a new instance of <see cref="FetchByIdCriteria" />.
            /// </summary>
            /// <param name="entityId">Unique id value identifying the entity of the instance to be fetched.</param>
            /// <param name="name">Name identifying the instance to be fetched.</param>
            public FetchByEntityIdNameCriteria(Int32 entityId, String name)
            {
                this.LoadProperty(EntityIdProperty, entityId);
                this.LoadProperty(NameProperty, name);
            }

            #endregion

            #region Properties

            #region EnitityId

            /// <summary>
            ///     Metadata for the <see cref="EnitityId" /> property.
            /// </summary>
            private static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(o => o.EnitityId);

            /// <summary>
            ///     Gets or sets the value for the <see cref="EnitityId" /> property.
            /// </summary>
            public Int32 EnitityId
            {
                get { return this.ReadProperty(EntityIdProperty); }
            }

            #endregion

            #region Name

            /// <summary>
            ///     Metadata for the <see cref="Name" /> property.
            /// </summary>
            private static readonly PropertyInfo<String> NameProperty =
                RegisterProperty<String>(o => o.Name);

            /// <summary>
            ///     Gets or sets the value for the <see cref="Name" /> property.
            /// </summary>
            public String Name
            {
                get { return this.ReadProperty(NameProperty); }
            }

            #endregion

            #endregion
        }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Creates a new instance of <see cref="RenumberingStrategy" />.
        /// </summary>
        /// <returns>A new instance of <see cref="RenumberingStrategy" /> with default values.</returns>
        public static RenumberingStrategy NewRenumberingStrategy()
        {
            RenumberingStrategy newInstance = new RenumberingStrategy();
            newInstance.Create();
            return newInstance;
        }

        /// <summary>
        ///     Creates a new instance of <see cref="RenumberingStrategy" />.
        /// </summary>
        /// <param name="entityId">The unique id for the entity the new instance will belong to.</param>
        /// <returns>A new instance of <see cref="RenumberingStrategy" /> with default values.</returns>
        public static RenumberingStrategy NewRenumberingStrategy(Int32 entityId)
        {
            RenumberingStrategy newInstance = new RenumberingStrategy();
            newInstance.Create(entityId);
            return newInstance;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type.
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Byte>(BayComponentXRenumberStrategyPriorityProperty, 1);
            this.LoadProperty<Byte>(BayComponentYRenumberStrategyPriorityProperty, 2);
            this.LoadProperty<Byte>(BayComponentZRenumberStrategyPriorityProperty, 3);
            this.LoadProperty<Byte>(PositionXRenumberStrategyPriorityProperty, 1);
            this.LoadProperty<Byte>(PositionYRenumberStrategyPriorityProperty, 2);
            this.LoadProperty<Byte>(PositionZRenumberStrategyPriorityProperty, 3);
            this.LoadProperty(BayComponentXRenumberStrategyProperty, PlanogramRenumberingStrategyXRenumberType.LeftToRight);
            this.LoadProperty(BayComponentYRenumberStrategyProperty, PlanogramRenumberingStrategyYRenumberType.BottomToTop);
            this.LoadProperty(BayComponentZRenumberStrategyProperty, PlanogramRenumberingStrategyZRenumberType.FrontToBack);
            this.LoadProperty(PositionXRenumberStrategyProperty, PlanogramRenumberingStrategyXRenumberType.LeftToRight);
            this.LoadProperty(PositionYRenumberStrategyProperty, PlanogramRenumberingStrategyYRenumberType.BottomToTop);
            this.LoadProperty(PositionZRenumberStrategyProperty, PlanogramRenumberingStrategyZRenumberType.FrontToBack);
            this.LoadProperty(RestartComponentRenumberingPerBayProperty, true);
            this.LoadProperty(IgnoreNonMerchandisingComponentsProperty, true);
            this.LoadProperty(RestartPositionRenumberingPerComponentProperty, true);
            this.LoadProperty(UniqueNumberMultiPositionProductsPerComponentProperty, false);
            this.LoadProperty(ExceptAdjacentPositionsProperty, false);
            this.LoadProperty(RestartPositionRenumberingPerBayProperty, true);
            this.LoadProperty(RestartComponentRenumberingPerComponentTypeProperty, false);
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type.
        /// </summary>
        /// <param name="entityId">The unique id for the entity the new instance will belong to.</param>
        private void Create(Int32 entityId)
        {
            this.LoadProperty(EntityIdProperty, entityId);
            this.Create();
        }

        #endregion

        #endregion

        #region Overrides

        /// <summary>
        ///     Returns a <see cref="T:System.String" /> that represents the current <see cref="T:System.RenumberingStrategy" />.
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.String" /> that represents the current <see cref="T:System.RenumberingStrategy" />.
        /// </returns>
        public override string ToString()
        {
            return this.Name;
        }

        /// <summary>
        ///     Invoked when this instance is being copied.
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty(IdProperty, newId);
            context.RegisterId<RenumberingStrategy>(oldId, newId);
        }

        #endregion
    }
}