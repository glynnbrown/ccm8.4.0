﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-26222 : L.Luong
//		Created (Auto-generated)

#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class UserInfo
    {
        #region Constructors
        private UserInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static UserInfo FetchUserInfo(IDalContext dalContext, UserInfoDto dto)
        {
            return DataPortal.FetchChild<UserInfo>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, UserInfoDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<String>(UserNameProperty, dto.UserName);
            LoadProperty<String>(FirstNameProperty, dto.FirstName);
            LoadProperty<String>(LastNameProperty, dto.LastName);
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Called when loading this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        private void Child_Fetch(IDalContext dalContext, UserInfoDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #endregion
    }
}
