﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-V8-24261 : L.Hodson
//	Created (Auto-generated)
// V8-27957 : N.Foster
//  Implement CCM security
#endregion

#region Version History : CCM 820
// V8-31140 : L.Ineson
//  Added helper methods
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of RecentPlanogram objects.
    /// </summary>
    [Serializable]
    public sealed partial class RecentPlanogramList : ModelList<RecentPlanogramList, RecentPlanogram>
    {
        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        internal static RecentPlanogramList NewRecentPlanogramList()
        {
            RecentPlanogramList item = new RecentPlanogramList();
            item.Create();
            return item;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Adds or updates the entry for the given file.
        /// </summary>
        public void AddOrUpdateFileItem(String planName, String fileName)
        {
            RecentPlanogramType packageType = RecentPlanogramType.Pog;

            //check for an existing entry
            RecentPlanogram entry =
                this.FirstOrDefault(r => 
                    r.Type == packageType
                    && String.Compare(r.Location, fileName, true) == 0);

            if (entry == null)
            {
                //add a new entry
                entry = RecentPlanogram.NewRecentPlanogram(planName, fileName, fileName, packageType);
                Add(entry);
            }
            else
            {
                //update the existing one.
                entry.Name = planName;
                entry.DateLastAccessed = DateTime.UtcNow;
            }
        }

        /// <summary>
        /// Adds or updates the entry for the given repository item.
        /// </summary>
        /// <param name="serverName">the name of the repository server</param>
        /// <param name="databaseName">the name of the repository database</param>
        /// <param name="entityId">the id of the entity</param>
        /// <param name="planId">the id of the plan</param>
        /// <param name="planName">the name of the plan</param>
        /// <param name="repositoryFolderPath">the repository folder path to the plan.</param>
        public void AddOrUpdateRepositoryItem(
            String serverName, String databaseName, Int32 entityId,
            Object planId, String planName, String repositoryFolderPath)
        {
            RecentPlanogramType packageType = RecentPlanogramType.Repository;

            // Try to get a recent plan item for this plan.
            String location = String.Join(RecentPlanogram.LocationSeparator.ToString(),
                new String[]{serverName, databaseName, Convert.ToString(entityId), Convert.ToString(planId)});

            RecentPlanogram entry = this.FirstOrDefault(r => 
                r.Type == packageType
                && (r.Location == location || Object.Equals(r.PlanogramId, planId)));

            if (entry == null)
            {
                entry = RecentPlanogram.NewRecentPlanogram(planName, location, repositoryFolderPath, packageType);
                Add(entry);
            }
            else
            {
                //update the entry
                entry.Name = planName;
                entry.DateLastAccessed = DateTime.UtcNow;
                entry.Location = location;
                entry.Path = repositoryFolderPath;
            }
        }

        /// <summary>
        /// Removes non-pinned items beyond the max count from this list.
        /// </summary>
        public void EnforceMaxItemCount(Int32 maxItemCount)
        {
            if (maxItemCount <= 0) return;

            //cycle through removing non pinned items based on last accessed date.
            while (this.Count(m => !m.IsPinned) > maxItemCount)
            {
                Remove(this.Where(m => !m.IsPinned).OrderBy(l => l.DateLastAccessed).First());
            }
        }

        

        #endregion
    }
}