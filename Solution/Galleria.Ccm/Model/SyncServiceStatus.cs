﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// An enum that defines the
    /// available sync service states
    /// </summary>
    [Serializable]
    public enum SyncServiceStatus
    {
        Error, // an error occurred when attempting a sync
        Idle, // the service is running but idle
        Paused, // the service is paused
        Stopped, // the service is stopped
        Syncing, // the service is running and performing a sync
        Unknown // the service status cannot be determined
    }

    public static class SyncServiceStatusHelper
    {
        public static readonly Dictionary<SyncServiceStatus, string> FriendlyNames =
            new Dictionary<SyncServiceStatus, string>()
            {
                {SyncServiceStatus.Error, Message.Enum_SyncServiceStatus_Error},
                {SyncServiceStatus.Idle, Message.Enum_SyncServiceStatus_Idle},
                {SyncServiceStatus.Paused, Message.Enum_SyncServiceStatus_Paused},
                {SyncServiceStatus.Stopped, Message.Enum_SyncServiceStatus_Stopped},
                {SyncServiceStatus.Syncing, Message.Enum_SyncServiceStatus_Syncing},
                {SyncServiceStatus.Unknown, Message.Enum_SyncServiceStatus_Unknown}
            };

        public static readonly Dictionary<SyncServiceStatus, string> FriendlyDescriptions =
            new Dictionary<SyncServiceStatus, string>()
            {
                {SyncServiceStatus.Error, Message.Enum_SyncServiceStatus_Error_Description},
                {SyncServiceStatus.Idle, Message.Enum_SyncServiceStatus_Idle_Description},
                {SyncServiceStatus.Paused, Message.Enum_SyncServiceStatus_Paused_Description},
                {SyncServiceStatus.Stopped, Message.Enum_SyncServiceStatus_Stopped_Description},
                {SyncServiceStatus.Syncing, Message.Enum_SyncServiceStatus_Syncing_Description},
                {SyncServiceStatus.Unknown, Message.Enum_SyncServiceStatus_Unknown_Description}
            };

        /// <summary>
        /// Returns the matching enum value from the specifed description
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static SyncServiceStatus? SyncServiceStatusGetEnum(string description)
        {
            foreach (KeyValuePair<SyncServiceStatus, string> keyPair in SyncServiceStatusHelper.FriendlyNames)
            {
                if (keyPair.Value.Equals(description))
                {
                    return keyPair.Key;
                }
            }
            return null;
        }
    }
}