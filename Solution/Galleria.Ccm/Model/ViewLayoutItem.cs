﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{ 
    /// <summary>
    /// A layout item child of viewlayout
    /// </summary>
    [Serializable]
    public sealed partial class ViewLayoutItem : ModelObject<ViewLayoutItem>
    {
        #region Parent

        /// <summary>
        /// Returns the parent ViewLayoutItem
        /// </summary>
        public ViewLayoutItem ParentViewLayoutItem
        {
            get
            {
                ViewLayoutItemList parentList = base.Parent as ViewLayoutItemList;

                if (parentList != null)
                {
                    return parentList.Parent as ViewLayoutItem;
                }

                return null;
            }
        }

        /// <summary>
        /// Returns the parent ViewLayout
        /// </summary>
        public ViewLayout ParentViewLayout
        {
            get
            {
                ViewLayoutItem lastParent = this;
                while (lastParent != null)
                {
                    ViewLayoutItem parentItem = lastParent.ParentViewLayoutItem;
                    if(parentItem == null)
                    {
                        ViewLayoutItemList parentList = lastParent.Parent as ViewLayoutItemList;
                        if (parentList != null)
                        {
                            return parentList.Parent as ViewLayout;
                        }
                    }
                    lastParent = parentItem;
                }

                return null;
            }

        }

        #endregion

        #region Properties

        private static readonly ModelPropertyInfo<Int32> IdProperty =
           RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The unique object id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }


        public static readonly ModelPropertyInfo<Byte> OrderProperty =
           RegisterModelProperty<Byte>(c => c.Order);
        /// <summary>
        /// The order number of this item
        /// </summary>
        public Byte Order
        {
            get { return GetProperty<Byte>(OrderProperty); }
        }

        public static readonly ModelPropertyInfo<Boolean> IsNewPaneProperty =
            RegisterModelProperty<Boolean>(c => c.IsNewPane);
        /// <summary>
        /// If true, this item starts a new pane
        /// </summary>
        public Boolean IsNewPane
        {
            get { return GetProperty<Boolean>(IsNewPaneProperty); }
            set { SetProperty<Boolean>(IsNewPaneProperty, value); }
        }

        public static readonly ModelPropertyInfo<DocumentType> DocumentTypeProperty =
            RegisterModelProperty<DocumentType>(c => c.DocumentType);
        /// <summary>
        /// The type of document this represents
        /// </summary>
        public DocumentType DocumentType
        {
            get { return GetProperty<DocumentType>(DocumentTypeProperty); }
            set { SetProperty<DocumentType>(DocumentTypeProperty, value); }
        }


        public static readonly ModelPropertyInfo<Boolean> IsVerticalProperty =
            RegisterModelProperty<Boolean>(c => c.IsVertical);
        /// <summary>
        /// If true, the items orientation is vertical.
        /// </summary>
        public Boolean IsVertical
        {
            get { return GetProperty<Boolean>(IsVerticalProperty); }
            set { SetProperty<Boolean>(IsVerticalProperty, value); }
        }


        public static readonly ModelPropertyInfo<ViewLayoutItemList> ItemsProperty =
           RegisterModelProperty<ViewLayoutItemList>(c => c.Items);
        /// <summary>
        /// The list of child items.
        /// </summary>
        public ViewLayoutItemList Items
        {
            get { return GetProperty<ViewLayoutItemList>(ItemsProperty); }
        }


        #region Helper Properties

        public Boolean IsDocument
        {
            get { return this.DocumentType != Model.DocumentType.None; }
        }

        #endregion

        #endregion

        #region Business Rules

        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ViewLayoutItem), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(ViewLayoutItem), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(ViewLayoutItem), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(ViewLayoutItem), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Temporary.ToString()));
        }
        #endregion

        #region Factory Methods

        public static ViewLayoutItem NewViewLayoutItem()
        {
            ViewLayoutItem item = new ViewLayoutItem();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        protected override void Create()
        {
            LoadProperty<ViewLayoutItemList>(ItemsProperty, ViewLayoutItemList.NewViewLayoutItemList());

            MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Helper Methods

        public List<ViewLayoutItem> GetAllChildItems()
        {
            List<ViewLayoutItem> returnList = new List<ViewLayoutItem>();
            returnList.Add(this);

            foreach (ViewLayoutItem childItem in this.Items)
            {
                returnList.AddRange(childItem.GetAllChildItems());
            }

            return returnList;
        }

        #endregion

    }

}
