﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Model
{
    public enum HighlightMethodType
    {
        Group,
        Quadrant,
        ColourScale,
        Characteristic
    }

    public static class HighlightMethodTypeHelper
    {
        public static readonly Dictionary<HighlightMethodType, String> FriendlyNames =
            new Dictionary<HighlightMethodType, String>()
            {
                {HighlightMethodType.Group, Message.HighlightMethodType_Group },
                {HighlightMethodType.Quadrant, Message.HighlightMethodType_Quadrant },
                {HighlightMethodType.ColourScale, Message.HighlightMethodType_ColorScale},
                {HighlightMethodType.Characteristic, Message.HighlightMethodType_Characteristic },
            };

        public static readonly Dictionary<HighlightMethodType, String> FriendlyDescriptions =
            new Dictionary<HighlightMethodType, String>()
            {
                {HighlightMethodType.Group, Message.HighlightMethodType_Group_Desc },
                {HighlightMethodType.Quadrant, Message.HighlightMethodType_Quadrant_Desc },
                {HighlightMethodType.ColourScale, Message.HighlightMethodType_ColorScale_Desc},
                {HighlightMethodType.Characteristic, Message.HighlightMethodType_Characteristic_Desc},
            };

        public static PlanogramHighlightMethodType ToPlanogramHighlightMethodType(this HighlightMethodType value)
        {
            switch (value)
            {
                default:
                case HighlightMethodType.Group: return PlanogramHighlightMethodType.Group;
                case HighlightMethodType.Characteristic: return PlanogramHighlightMethodType.Characteristic;
                case HighlightMethodType.ColourScale: return PlanogramHighlightMethodType.ColourScale;
                case HighlightMethodType.Quadrant: return PlanogramHighlightMethodType.Quadrant;
            }
        }
    }
}
