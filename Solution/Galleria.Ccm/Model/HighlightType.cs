﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24801 L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Highlight Type Enum
    /// </summary>
    public enum HighlightType
    {
        Position = 0,
        Fixture = 1
    }

    /// <summary>
    /// HighlightFillPatternType Helper Class
    /// </summary>
    public static class HighlightTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<HighlightType, String> FriendlyNames =
            new Dictionary<HighlightType, String>()
            {
                {HighlightType.Position, Message.Enum_HighlightType_Position},
                {HighlightType.Fixture, Message.Enum_HighlightType_Fixture},
            };

    }
}
