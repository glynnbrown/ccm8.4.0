﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GFS 1.0)
// CCM-25445 : L.Ineson
//		Copied from GFS 
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a list containing LocationGroupLocation objects
    /// (child of LocationGroup)
    /// </summary>
    [Serializable]
    public partial class LocationGroupLocationList : ModelList<LocationGroupLocationList, LocationGroupLocation>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationGroupLocationList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationGroupLocationList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(LocationGroupLocationList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationGroupLocationList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationHierarchyEdit.ToString()));
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns the parent product group
        /// </summary>
        public LocationGroup ParentLocationGroup
        {
            get { return base.Parent as LocationGroup; }
        }

        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        internal static LocationGroupLocationList NewLocationGroupLocationList()
        {
            LocationGroupLocationList item = new LocationGroupLocationList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}
