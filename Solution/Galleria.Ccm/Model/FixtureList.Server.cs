﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public sealed partial class FixtureList
    {
        #region Constructor
        private FixtureList() { } // force use of factory methods
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returing all items for a given parent id
        /// </summary>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = this.GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IFixtureDal dal = dalContext.GetDal<IFixtureDal>())
                {
                    IEnumerable<FixtureDto> dtoList = dal.FetchByFixturePackageId(criteria.ParentId);
                    foreach (FixtureDto dto in dtoList)
                    {
                        this.Add(Fixture.Fetch(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
            this.MarkAsChild();
        }
        #endregion

        #endregion
    }
}