﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25395 : A.Probyn
//		Created 
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Readonly list holding ProductLibraryInfo objects.
    /// </summary>
    [Serializable]
    public sealed partial class ProductLibraryInfoList : ModelReadOnlyList<ProductLibraryInfoList, ProductLibraryInfo>
    {
        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByIds
        /// </summary>
        [Serializable]
        public sealed class FetchByIdsCriteria : CriteriaBase<FetchByIdsCriteria>
        {
            #region Properties

            #region DalType
            /// <summary>
            /// DalType property definition
            /// </summary>
            public static readonly PropertyInfo<ProductLibraryDalFactoryType> DalTypeProperty =
                RegisterProperty<ProductLibraryDalFactoryType>(c => c.DalType);
            /// <summary>
            /// Returns the package type
            /// </summary>
            public ProductLibraryDalFactoryType DalType
            {
                get { return this.ReadProperty<ProductLibraryDalFactoryType>(DalTypeProperty); }
            }
            #endregion

            #region Ids
            public static PropertyInfo<List<Object>> IdsProperty
                = RegisterProperty<List<Object>>(c => c.Ids);
            public List<Object> Ids
            {
                get { return ReadProperty<List<Object>>(IdsProperty); }
            }
            #endregion

            #endregion

            #region Constructor

            public FetchByIdsCriteria(ProductLibraryDalFactoryType dalType, List<Object> idList)
            {
                LoadProperty<ProductLibraryDalFactoryType>(DalTypeProperty, dalType);
                LoadProperty<List<Object>>(IdsProperty, idList);
            }

            #endregion
        }

        #region FetchByEntityIdCriteria

        /// <summary>
        /// Criteria for the Fetch factory method
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : CriteriaBase<FetchByEntityIdCriteria>
        {
            #region Properties

            #region DalType
            /// <summary>
            /// DalType property definition
            /// </summary>
            public static readonly PropertyInfo<ProductLibraryDalFactoryType> DalTypeProperty =
                RegisterProperty<ProductLibraryDalFactoryType>(c => c.DalType);
            /// <summary>
            /// Returns the dal type
            /// </summary>
            public ProductLibraryDalFactoryType DalType
            {
                get { return this.ReadProperty<ProductLibraryDalFactoryType>(DalTypeProperty); }
            }
            #endregion

            #region EntityId
            /// <summary>
            /// EntityId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            /// <summary>
            /// Returns the Entity Id
            /// </summary>
            public Int32 EntityId
            {
                get { return this.ReadProperty<Int32>(EntityIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByEntityIdCriteria(Int32 entityId)
            {
                this.LoadProperty<ProductLibraryDalFactoryType>(DalTypeProperty, ProductLibraryDalFactoryType.Unknown);
                this.LoadProperty<Int32>(EntityIdProperty, entityId);
            }
            #endregion
        }

        #endregion


        #endregion


    }
}
