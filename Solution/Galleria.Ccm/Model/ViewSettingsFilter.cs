﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva
//  Created.

#endregion

#endregion

using System;
using Csla;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Model implementation of the Grid View Settings Filter.
    /// </summary>
    [Serializable]
    public sealed partial class ViewSettingsFilter : ModelObject<ViewSettingsFilter>
    {
        private const string ColumnFriendlyName = "Column";

        #region Properties

        #region Path property

        public readonly PropertyInfo<String> PathProperty = RegisterModelProperty<String>(o => o.Path,
            ColumnFriendlyName);

        public String Path
        {
            get { return GetProperty(PathProperty); }
        }

        #endregion

        #endregion
    }
}
