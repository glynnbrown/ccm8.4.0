﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-V8-24801 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class HighlightGroup
    {
        #region Constructor
        private HighlightGroup() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static HighlightGroup Fetch(IDalContext dalContext, HighlightGroupDto dto)
        {
            return DataPortal.FetchChild<HighlightGroup>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, HighlightGroupDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32>(OrderProperty, dto.Order);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(DisplayNameProperty, dto.DisplayName);
            this.LoadProperty<Int32>(FillColourProperty, dto.FillColour);
            this.LoadProperty<HighlightFillPatternType>(FillPatternTypeProperty, (HighlightFillPatternType)dto.FillPatternType);

        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private HighlightGroupDto GetDataTransferObject(Highlight parent)
        {
            return new HighlightGroupDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                HighlightId = parent.Id,
                Order = ReadProperty<Int32>(OrderProperty),
                Name = ReadProperty<String>(NameProperty),
                DisplayName = ReadProperty<String>(DisplayNameProperty),
                FillColour = ReadProperty<Int32>(FillColourProperty),
                FillPatternType = (Byte)ReadProperty<HighlightFillPatternType>(FillPatternTypeProperty),

            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, HighlightGroupDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, Highlight parent)
        {
            HighlightGroupDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IHighlightGroupDal dal = dalContext.GetDal<IHighlightGroupDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<HighlightGroup>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, Highlight parent)
        {
            if (this.IsSelfDirty)
            {
                using (IHighlightGroupDal dal = dalContext.GetDal<IHighlightGroupDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, Highlight parent)
        {
            using (IHighlightGroupDal dal = dalContext.GetDal<IHighlightGroupDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion

    }
}