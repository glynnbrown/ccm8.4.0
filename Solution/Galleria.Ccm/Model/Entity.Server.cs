﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25559 : L.Hodson
//	Created (Auto-generated)
//CCM-26179: I.George
// added more fields
// CCM-26306 : L.Ineson
//  Added System Settings & moved uom values there
// V8-27964 : A.Silva
//  Added ProductLevelId and DateLastMerchSynced to the DataTransferObject methods.
// V8-28420 : D.Pleasance
//  Added default PlanogramImportTemplate
#endregion

#region Version History: CCM801
// V8-28622 : D.Pleasance
//  Amended so that ApplySpacemanV9Mappings is called on the NewPlanogramImportTemplate method.
#endregion

#region Version History: CCM803
// V8-29680 : N.Foster
//  Added default workflows
// V8-29733 : N.Foster
//  Added default inventory profile
#endregion

#region Version History: CCM810
// V8-29730 : L.Ineson
//  Added default validation template.
// V8-29818 : L.Ineson
//  Added default renumbering strategy
// V8-29946 : N.Haywood
//  Added comment about changing defaults warning to add to sync
// V8-29971 : M.Brumby
//  Updated workflow task orders to be inline with the version 5.6 workflow doc
// V8-30000 : L.Ineson
//  Added default name templates.
// V8-30068 : M.Brumby
//  Default task lists are now all in one location
// V8-30194 : M.Shelley
//  Add default highlights and labels when a new entity is created.
#endregion

#region Version History: CCM811
// V8-30261 : M.Pettit
//  Renamed PlanogramNameTemplateSeparatorTypeHelper Underline to Underscore
// V8-30446 : M.Shelley
//  Added new defualt product label "06 Blocking Sequence Number"
// V8-30490 : M.Shelley
//  Updated the definition of the default highlight "02 COS vs DOS"
// V8-30501 : I.George
//  changed name from Fails on DOS but achieves on COS and Fails inventory on DOS and COS 
//      to Achieves on COS but fails on DOS and Fails inventory on COS and DOS respectively
// V8-30803/30804/30805 : A.Kuszyk
//  Added additional default workflows.
#endregion

#region Version History: CCM820
// V8-30897/30898/30899/30900/30901/30902 : L.Luong
//  Changed default values for workflows
// V8-30972 : D.Pleasance
//  Amended CreateDefaultHighlights so that highlight groups are no longer created. These are generated when the highlight is applied.
// V8-30989 : A.Kuszyk
//  Added additional tasks to Merchandise workflows.
// V8-31057 - M.Brumby
//  PCR01417 Default Apollo Tempate
// V8-31076 : A.Probyn
//  Removed Merchandised Planogram from the default workflow tasks.
// V8-31073 : A.Silva
//  Minor change to retrieve the default versions for each import file type.
// V8-31115 : A.Probyn
//  Updated certain work flow task parameters in DataPortal_Insert.
// V8-31200 : A.Probyn
//  Updated DataPortal_Insert to create default planogram print template.
// V8-31246 : A.Kuszyk
//  Added Update Product Attributes task to Create Sequence Template workflow.
// V8-31283 : J.Pickup
//  Blocking Sequnce Number label changed to OptimizationSequenceNumber (.SequnceNumber property on position rather than product).
// V8-31565 : D.Pleasance
//  Added Merchandise planogram placement type parameter for SSASSI workflow
#endregion

#region Version History: CCM830
// V8-31831 : A.Probyn
//  Removed SeparatorType from PlanogramNameTemplate defaults, and added PlanogramAttribute
// V8-31808 : D.Pleasance
//  Amended Merchandise CLASSI and Location Specific workflows to include new merchandise inventory tasks.
// V8-3261 : A.Silva
//  Added Default Planogram Comparison Template for new entities.
// V8-32654 : D.Pleasance
//  Updated default workflows, Update product performance now defaulted to 100% location for all work flows, 
//  updated classi and location specific planogram workflows to use new space constraint within presentation.
// V8-32597 : A.Probyn
//  Corrected settings of default workflow parameters to reference parameters by Id of parameter (a specified byte value)
//  rather than using the index. New parameters preceeding old ones had knocked them out of line.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using System.Linq;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    public partial class Entity
    {
        #region Constructors
        private Entity() { } //force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing Entity with the given id
        /// </summary>
        public static Entity FetchById(Int32 id)
        {
            return DataPortal.Fetch<Entity>(new FetchByIdCriteria(id));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, EntityDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32?>(GFSIdProperty, dto.GFSId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(DescriptionProperty, dto.Description);

            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);

            this.LoadProperty<SystemSettings>(SystemSettingsProperty, SystemSettings.FetchSystemSettings(dalContext, dto.Id));

            this.LoadProperty<Int32?>(ProductLevelIdProperty, dto.ProductLevelId);
            this.LoadProperty<DateTime?>(DateLastMerchSyncedProperty, dto.DateLastMerchSynced);
            this.LoadProperty<EntityComparisonAttributeList>(ComparisonAttributesProperty, EntityComparisonAttributeList.FetchByParentId(dalContext, dto.Id));
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private EntityDto GetDataTransferObject()
        {
            return new EntityDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                GFSId = ReadProperty<Int32?>(GFSIdProperty),
                Name = ReadProperty<String>(NameProperty),
                Description = ReadProperty<String>(DescriptionProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty),
                ProductLevelId = ReadProperty<Int32?>(ProductLevelIdProperty),
                DateLastMerchSynced = ReadProperty<DateTime?>(DateLastMerchSyncedProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning FetchById
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.Id));
                }
            }
        }

        #endregion

        #region Insert

        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            EntityDto dto = GetDataTransferObject();
            EntityDto deletedEntityDto = null;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                Int32 oldId = dto.Id;
                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    //first check if the entity already exists
                    try
                    {
                        deletedEntityDto = dal.FetchDeletedByName(dto.Name);
                    }
                    catch (DtoDoesNotExistException) { }

                    //insert the new one - this will update if one already exists.
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<Entity>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }

            //Insert any required defaults
            if (deletedEntityDto == null)
            {
                #region IF YOU CHANGE THESE DEFAULTS PLEASE CHANGE THEM IN THE SYNC TOO (SynchronizeEntities method)
                #region Product Hierarchy

                //ProductHierarcy
                ProductHierarchy defaultHierarchy = ProductHierarchy.NewProductHierarchy(dto.Id);
                defaultHierarchy.Name = String.Format(Message.ProductHierarchy_DefaultName, dto.Name);
                defaultHierarchy.ConstructDefaultHierarchy();
                defaultHierarchy = defaultHierarchy.Save();

                #endregion

                #region Planogram Hierarchy

                //PlanogramHierarchy
                PlanogramHierarchy planHierarchy = PlanogramHierarchy.NewPlanogramHierarchy(dto.Id);
                planHierarchy.Name = Message.Entity_PlanogramHierarchyDefaultName;
                planHierarchy.RootGroup.Name = Message.Entity_PlanogramHierarchyDefaultRootName;
                planHierarchy = planHierarchy.Save();

                #endregion

                #region Location Hierarchy

                //LocationHierarchy
                LocationHierarchy defaultLocHierarchy = LocationHierarchy.NewLocationHierarchy(dto.Id);
                defaultLocHierarchy.Name = Message.Entity_LocationHierarchyDefaultName;
                defaultLocHierarchy.ConstructDefaultHierarchy();
                defaultLocHierarchy = defaultLocHierarchy.Save();

                #endregion

                #region Planogram Import Template

                //PlanogramImportTemplate Spaceman
                PlanogramImportTemplate planogramImportTemplateSpaceman = PlanogramImportTemplate.NewPlanogramImportTemplate(dto.Id, PlanogramImportFileType.SpacemanV9);
                planogramImportTemplateSpaceman.Name = Message.PlanogramImportTemplate_SpacemanDefaultName;
                planogramImportTemplateSpaceman = planogramImportTemplateSpaceman.Save();

                //PlanogramImportTemplate Apollo
                PlanogramImportTemplate planogramImportTemplateApollo = PlanogramImportTemplate.NewPlanogramImportTemplate(dto.Id, PlanogramImportFileType.Apollo);
                planogramImportTemplateApollo.Name = Message.PlanogramImportTemplate_ApolloDefaultName;
                planogramImportTemplateApollo = planogramImportTemplateApollo.Save();

                //PlanogramImportTemplate ProSpace
                PlanogramImportTemplate planogramImportTemplateProSpace = PlanogramImportTemplate.NewPlanogramImportTemplate(dto.Id, PlanogramImportFileType.ProSpace);
                planogramImportTemplateProSpace.Name = Message.PlanogramImportTemplate_ProSpaceDefaultName;
                planogramImportTemplateProSpace = planogramImportTemplateProSpace.Save();

                #endregion

                #region Validation Template

                ValidationTemplate validationTemplate = ValidationTemplate.CreateDefaultValidationTemplate(dto.Id);
                validationTemplate = validationTemplate.Save();

                #endregion

                #region Renumbering Strategy

                RenumberingStrategy renumberingStrategy = RenumberingStrategy.NewRenumberingStrategy(dto.Id);
                renumberingStrategy.Name = Message.RenumberingStrategy_EntityDefaultStrategyName;

                renumberingStrategy = renumberingStrategy.Save();

                #endregion

                #region Workflows

                foreach (Workflow workflow in CreateDefaultWorkflows(dto.Id))
                {
                    workflow.Save();
                }

                #endregion

                #region Inventory Profiles

                // default inventory profile
                InventoryProfile defaultInventoryProfile = InventoryProfile.NewInventoryProfile(dto.Id);
                defaultInventoryProfile.Name = Message.InventoryProfile_DefaultName;
                defaultInventoryProfile.CasePack = 1.25F;
                defaultInventoryProfile.DaysOfSupply = 3;
                defaultInventoryProfile.Save();

                #endregion

                #region Planogram Name Template

                PlanogramNameTemplate clusterNameTemplate = PlanogramNameTemplate.NewPlanogramNameTemplate(dto.Id);
                clusterNameTemplate.Name = Message.PlanogramNameTemplate_ClusterDefaultName;
                clusterNameTemplate.PlanogramAttribute = PlanogramNameTemplatePlanogramAttributeType.PlanogramName;
                clusterNameTemplate.BuilderFormula = "[Planogram.CategoryCode]_[Planogram.CategoryName]_[Planogram.ClusterName]_[LocationSpaceProductGroup.BayCount]_[Planogram.Width]_[Planogram.Height]";
                clusterNameTemplate = clusterNameTemplate.Save();

                PlanogramNameTemplate storeSpecificNameTemplate = PlanogramNameTemplate.NewPlanogramNameTemplate(dto.Id);
                storeSpecificNameTemplate.Name = Message.PlanogramNameTemplate_StoreSpecificDefaultName;
                storeSpecificNameTemplate.PlanogramAttribute = PlanogramNameTemplatePlanogramAttributeType.PlanogramName;
                storeSpecificNameTemplate.BuilderFormula = "[Planogram.CategoryCode]_[Planogram.CategoryName]_[Location.Code]_[LocationSpaceProductGroup.BayCount]_[Planogram.Width]";
                storeSpecificNameTemplate = storeSpecificNameTemplate.Save();

                #endregion

                #region Highlights

                // Create a list of default Highlights
                var highList = CreateDefaultHighlights(dto.Id);

                // And save them given the current context
                foreach (Highlight highItem in highList)
                {
                    highItem.Save();
                }

                #endregion

                // ToDo: Default entity labels
                #region Labels

                // Create a list of default Labels
                var labelList = CreateDefaultLabels(dto.Id);

                // And save them given the current context
                foreach (Label labelItem in labelList)
                {
                    labelItem.Save();
                }

                #endregion

                #region Print Templates

                //Default Print Template - Planogram
                PrintTemplate defaultPrintTemplate = PrintTemplate.CreateDefaultPlanogramPrintTemplate(dto.Id, false);
                defaultPrintTemplate.Save();

                #endregion

                #region Default Planogram Comparison Template

                PlanogramComparisonTemplate.CreateDefaultPlanogramComparisonTemplate(dto.Id).Save();

                #endregion

                #endregion
            }
        }

        /// <summary>
        /// Sets the parameter value for the remove product illegal task in the given workflow, if it exists.
        /// </summary>
        /// <param name="workflow"></param>
        /// <param name="removeAction"></param>
        private static void SetRemoveIllegalProductsParameters(Workflow workflow, Int32 removeAction)
        {
            WorkflowTask removeIllegalProducts = workflow.Tasks
                .FirstOrDefault(t => t.Details.TaskType.Equals("Galleria.Ccm.Engine.Tasks.RemoveIllegalProductsLocationSpecific.v1.Task"));
            if (removeIllegalProducts == null) return;
            removeIllegalProducts.Parameters.FirstOrDefault(p => p.ParameterDetails.Id.Equals(0)).Value = removeAction;
        }

        /// <summary>
        /// Sets the parameter values for the Update Product Performance task if it exists in the given workflow.
        /// </summary>
        /// <param name="workflow"></param>
        /// <param name="enterprise"></param>
        /// <param name="cluster"></param>
        /// <param name="location"></param>
        /// <param name="planAssignment"></param>
        private static void SetUpdateProductPerformanceParameters(
            Workflow workflow, Single enterprise, Single cluster, Single location, Single planAssignment)
        {
            WorkflowTask updateProductPerformance = workflow.Tasks
                .FirstOrDefault(t => t.Details.TaskType.Equals("Galleria.Ccm.Engine.Tasks.UpdateProductPerformance.v1.Task"));
            if (updateProductPerformance == null) return;
            updateProductPerformance.Parameters.FirstOrDefault(p => p.ParameterDetails.Id.Equals(1)).Value = enterprise;
            updateProductPerformance.Parameters.FirstOrDefault(p => p.ParameterDetails.Id.Equals(2)).Value = cluster;
            updateProductPerformance.Parameters.FirstOrDefault(p => p.ParameterDetails.Id.Equals(3)).Value = location;
            updateProductPerformance.Parameters.FirstOrDefault(p => p.ParameterDetails.Id.Equals(4)).Value = planAssignment;
        }
        private static void SetMerchandisePlanogramUsingAssortmentParameters(
            Workflow workflow, Boolean ignoreMerchandisingDimensions, Int32 placementUnits = -1, Int32 placementOrder = -1)
        { 
            WorkflowTask updateProductPerformance = workflow.Tasks
                .FirstOrDefault(t => t.Details.TaskType.Equals("Galleria.Ccm.Engine.Tasks.MerchandisePlanogram.v1.Task"));
            if (updateProductPerformance == null) return;
            updateProductPerformance.Parameters[1].Value = ignoreMerchandisingDimensions;
            if (placementUnits > 0)
            {
                updateProductPerformance.Parameters.FirstOrDefault(p => p.ParameterDetails.Id.Equals(0)).Value = placementUnits;
            }
            if (placementOrder > 0)
            {
                updateProductPerformance.Parameters.FirstOrDefault(p => p.ParameterDetails.Id.Equals(3)).Value = placementOrder;
            }
        }

        private static void SetMerchandiseProductsInSequenceUsingAssortmentParameters(
            Workflow workflow, Int32 placementOrder, Int32 spaceConstraint = 0)
        {
            WorkflowTask merchandiseProductsUsingAssortment = workflow.Tasks
                .FirstOrDefault(t => t.Details.TaskType.Equals("Galleria.Ccm.Engine.Tasks.MerchandiseProductsUsingAssortment.v1.Task"));

            if (merchandiseProductsUsingAssortment == null) return;
            merchandiseProductsUsingAssortment.Parameters.FirstOrDefault(p => p.ParameterDetails.Id.Equals(3)).Value = placementOrder;
            merchandiseProductsUsingAssortment.Parameters.FirstOrDefault(p => p.ParameterDetails.Id.Equals(4)).Value = spaceConstraint;
        }

        private static void SetIncreaseAssortmentInventoryUsingAssortment(
            Workflow workflow, Single topPercentageOfProducts, Int32 processingMethod1, Int32 processingMethod2)
        {
            WorkflowTask updateProductPerformance = workflow.Tasks
                .FirstOrDefault(t => t.Details.TaskType.Equals("Galleria.Ccm.Engine.Tasks.IncreaseAssortmentInventory.v1.Task"));
            if (updateProductPerformance == null) return;
            updateProductPerformance.Parameters.FirstOrDefault(p => p.ParameterDetails.Id.Equals(0)).Value = topPercentageOfProducts;
            updateProductPerformance.Parameters.FirstOrDefault(p => p.ParameterDetails.Id.Equals(1)).Value = processingMethod1;
            updateProductPerformance.Parameters.FirstOrDefault(p => p.ParameterDetails.Id.Equals(2)).Value = processingMethod2;
        }

        #endregion

        #region Update
        /// <summary>
        /// Called when updating this instance
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                EntityDto dto = GetDataTransferObject();
                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when this instance is being deleted
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #endregion

        #region Methods

        internal static void InsertDefaultWorkflows(IDalContext dalContext, Int32 entityId)
        {
            foreach (Workflow workflow in CreateDefaultWorkflows(entityId))
            {
                workflow.InsertUsingExistingContext(dalContext);
            }
        }

        /// <summary>
        /// Inserts the default Highlights using the existing dal context.
        /// </summary>
        internal static void InsertDefaultHighlights(IDalContext dalContext, Int32 entityId)
        {
            var highList = CreateDefaultHighlights(entityId);

            foreach (Highlight highItem in highList)
            {
                highItem.InsertUsingExistingContext(dalContext);
            }
        }

        /// <summary>
        /// Inserts the default Labels using the existing dal context.
        /// </summary>
        internal static void InsertDefaultLabels(IDalContext dalContext, Int32 entityId)
        {
            var labelList = CreateDefaultLabels(entityId);

            foreach (Label labelItem in labelList)
            {
                labelItem.InsertUsingExistingContext(dalContext);
            }
        }

        /// <summary>
        /// Inserts the default print templates using the existing dal context.
        /// </summary>
        internal static void InsertDefaultPrintTemplates(IDalContext dalContext, Int32 entityId)
        {
            PrintTemplate defaultPrintTemplate = PrintTemplate.CreateDefaultPlanogramPrintTemplate(entityId, true);

            defaultPrintTemplate.InsertUsingExistingContext(dalContext);
        }
        
        /// <summary>
        ///     Insert the default <see cref="PlanogramComparisonTemplate"/> using the given <paramref name="dalContext"/> and for the given <paramref name="entityId"/>.
        /// </summary>
        /// <param name="dalContext">The current DAL context to use for the Insert.</param>
        /// <param name="entityId">The ID of the <see cref="Entity"/> that needs a default template inserted.</param>
        public static void InsertDefaultComparisonTemplate(IDalContext dalContext, Int32 entityId)
        {
            PlanogramComparisonTemplate.CreateDefaultPlanogramComparisonTemplate(entityId).InsertUsingExistingContext(dalContext);
        }
        
        private static IEnumerable<Highlight> CreateDefaultHighlights(Int32 entityId)
        {
            List<Highlight> highlightsList = new List<Highlight>();

            #region "linearSpacetoSales" Highlight definition

            Highlight linearSpacetoSales = Highlight.NewHighlight(entityId);

            linearSpacetoSales.Name = "01 Linear Space to Sales";
            linearSpacetoSales.Type = HighlightType.Position;
            linearSpacetoSales.MethodType = HighlightMethodType.Quadrant;
            linearSpacetoSales.IsFilterEnabled = false;
            linearSpacetoSales.IsPreFilter = true;
            linearSpacetoSales.IsAndFilter = true;
            linearSpacetoSales.Field1 = "[PlanogramProduct.MetaPlanogramLinearSpacePercentage]";
            linearSpacetoSales.Field2 = "[PlanogramProduct.PerformanceData.MetaP2Percentage]";
            linearSpacetoSales.QuadrantXType = HighlightQuadrantType.Mean;
            linearSpacetoSales.QuadrantXConstant = 0F;
            linearSpacetoSales.QuadrantYType = HighlightQuadrantType.Mean;
            linearSpacetoSales.QuadrantYConstant = 0F;

            highlightsList.Add(linearSpacetoSales);

            #endregion "linearSpacetoSales" Highlight definition

            #region "cosVsDos" Highlight definition

            Highlight cosVsDos = Highlight.NewHighlight(entityId);

            cosVsDos.Name = "02 COS vs DOS";
            cosVsDos.Type = HighlightType.Position;
            cosVsDos.MethodType = HighlightMethodType.Quadrant;
            cosVsDos.IsFilterEnabled = false;
            cosVsDos.IsPreFilter = true;
            cosVsDos.IsAndFilter = true;
            cosVsDos.Field1 = "[PlanogramProduct.PerformanceData.AchievedCasePacks]";
            cosVsDos.Field2 = "[PlanogramProduct.PerformanceData.AchievedDos]";
            cosVsDos.QuadrantXType = HighlightQuadrantType.Constant;
            cosVsDos.QuadrantXConstant = 1.25F;
            cosVsDos.QuadrantYType = HighlightQuadrantType.Constant;
            cosVsDos.QuadrantYConstant = 3F;

            #region Highlight Characteristics

            var cosVsDosChar01 = HighlightCharacteristic.NewHighlightCharacteristic();
            cosVsDosChar01.Name = "Group 1";
            cosVsDosChar01.IsAndFilter = true;

            cosVsDos.Characteristics.Add(cosVsDosChar01);

            #endregion

            highlightsList.Add(cosVsDos);

            #endregion "cosVsDos" Highlight definition

            #region "failsInventoryRule" Highlight definition

            Highlight failsInventoryRule = Highlight.NewHighlight(entityId);

            failsInventoryRule.Name = "03 Fails inventory rule";
            failsInventoryRule.Type = HighlightType.Position;
            failsInventoryRule.MethodType = HighlightMethodType.Characteristic;
            failsInventoryRule.IsFilterEnabled = false;
            failsInventoryRule.IsPreFilter = true;
            failsInventoryRule.IsAndFilter = true;
            failsInventoryRule.Field1 = "";
            failsInventoryRule.Field2 = "";
            failsInventoryRule.QuadrantXType = HighlightQuadrantType.Mean;
            failsInventoryRule.QuadrantXConstant = 0F;
            failsInventoryRule.QuadrantYType = HighlightQuadrantType.Mean;
            failsInventoryRule.QuadrantYConstant = 0F;
            
            #region Highlight Characteristics

            var failsInventoryRuleChar01 = HighlightCharacteristic.NewHighlightCharacteristic();
            failsInventoryRuleChar01.Name = "Fails on COS but achieves DOS";
            failsInventoryRuleChar01.IsAndFilter = true;

            var failsInventoryRuleChar01Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            failsInventoryRuleChar01Rule01.Field = "[PlanogramProduct.MetaNotAchievedCases]";
            failsInventoryRuleChar01Rule01.Type = HighlightCharacteristicRuleType.Equals;
            failsInventoryRuleChar01Rule01.Value = "1";
            failsInventoryRuleChar01.Rules.Add(failsInventoryRuleChar01Rule01);

            var failsInventoryRuleChar01Rule02 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            failsInventoryRuleChar01Rule02.Field = "[PlanogramProduct.MetaNotAchievedDOS]";
            failsInventoryRuleChar01Rule02.Type = HighlightCharacteristicRuleType.NotEquals;
            failsInventoryRuleChar01Rule02.Value = "1";
            failsInventoryRuleChar01.Rules.Add(failsInventoryRuleChar01Rule02);

            failsInventoryRule.Characteristics.Add(failsInventoryRuleChar01);

            var failsInventoryRuleChar02 = HighlightCharacteristic.NewHighlightCharacteristic();
            failsInventoryRuleChar02.Name = "Achieves on COS but fails on DOS";
            failsInventoryRuleChar02.IsAndFilter = true;

            var failsInventoryRuleChar02Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            failsInventoryRuleChar02Rule01.Field = "[PlanogramProduct.MetaNotAchievedCases]";
            failsInventoryRuleChar02Rule01.Type = HighlightCharacteristicRuleType.NotEquals;
            failsInventoryRuleChar02Rule01.Value = "1";
            failsInventoryRuleChar02.Rules.Add(failsInventoryRuleChar02Rule01);

            var failsInventoryRuleChar02Rule02 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            failsInventoryRuleChar02Rule02.Field = "[PlanogramProduct.MetaNotAchievedDOS]";
            failsInventoryRuleChar02Rule02.Type = HighlightCharacteristicRuleType.Equals;
            failsInventoryRuleChar02Rule02.Value = "1";
            failsInventoryRuleChar02.Rules.Add(failsInventoryRuleChar02Rule02);

            failsInventoryRule.Characteristics.Add(failsInventoryRuleChar02);

            var failsInventoryRuleChar03 = HighlightCharacteristic.NewHighlightCharacteristic();
            failsInventoryRuleChar03.Name = "Fails inventory on COS and DOS";
            failsInventoryRuleChar03.IsAndFilter = true;

            var failsInventoryRuleChar03Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            failsInventoryRuleChar03Rule01.Field = "[PlanogramProduct.MetaNotAchievedCases]";
            failsInventoryRuleChar03Rule01.Type = HighlightCharacteristicRuleType.Equals;
            failsInventoryRuleChar03Rule01.Value = "1";
            failsInventoryRuleChar03.Rules.Add(failsInventoryRuleChar03Rule01);

            var failsInventoryRuleChar03Rule02 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            failsInventoryRuleChar03Rule02.Field = "[PlanogramProduct.MetaNotAchievedDOS]";
            failsInventoryRuleChar03Rule02.Type = HighlightCharacteristicRuleType.Equals;
            failsInventoryRuleChar03Rule02.Value = "1";
            failsInventoryRuleChar03.Rules.Add(failsInventoryRuleChar03Rule02);

            failsInventoryRule.Characteristics.Add(failsInventoryRuleChar03);

            var failsInventoryRuleChar04 = HighlightCharacteristic.NewHighlightCharacteristic();
            failsInventoryRuleChar04.Name = "Achieves Both COS and DOS";
            failsInventoryRuleChar04.IsAndFilter = true;

            var failsInventoryRuleChar04Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            failsInventoryRuleChar04Rule01.Field = "[PlanogramProduct.MetaNotAchievedCases]";
            failsInventoryRuleChar04Rule01.Type = HighlightCharacteristicRuleType.NotEquals;
            failsInventoryRuleChar04Rule01.Value = "1";
            failsInventoryRuleChar04.Rules.Add(failsInventoryRuleChar04Rule01);

            var failsInventoryRuleChar04Rule02 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            failsInventoryRuleChar04Rule02.Field = "[PlanogramProduct.MetaNotAchievedDOS]";
            failsInventoryRuleChar04Rule02.Type = HighlightCharacteristicRuleType.NotEquals;
            failsInventoryRuleChar04Rule02.Value = "1";
            failsInventoryRuleChar04.Rules.Add(failsInventoryRuleChar04Rule02);

            failsInventoryRule.Characteristics.Add(failsInventoryRuleChar04);

            #endregion


            highlightsList.Add(failsInventoryRule);

            #endregion "failsInventoryRule" Highlight definition

            #region "brand" Highlight definition

            Highlight brand = Highlight.NewHighlight(entityId);

            brand.Name = "04 Brand";
            brand.Type = HighlightType.Position;
            brand.MethodType = HighlightMethodType.Group;
            brand.IsFilterEnabled = false;
            brand.IsPreFilter = true;
            brand.IsAndFilter = true;
            brand.Field1 = "[PlanogramProduct.Brand]";
            brand.Field2 = "";
            brand.QuadrantXType = HighlightQuadrantType.Mean;
            brand.QuadrantXConstant = 0F;
            brand.QuadrantYType = HighlightQuadrantType.Mean;
            brand.QuadrantYConstant = 0F;

            highlightsList.Add(brand);

            #endregion "brand" Highlight definition

            #region "subCategory" Highlight definition

            Highlight subCategory = Highlight.NewHighlight(entityId);

            subCategory.Name = "05 Sub-Category";
            subCategory.Type = HighlightType.Position;
            subCategory.MethodType = HighlightMethodType.Group;
            subCategory.IsFilterEnabled = false;
            subCategory.IsPreFilter = true;
            subCategory.IsAndFilter = true;
            subCategory.Field1 = "[PlanogramProduct.Subcategory]";
            subCategory.Field2 = "";
            subCategory.QuadrantXType = HighlightQuadrantType.Mean;
            subCategory.QuadrantXConstant = 0F;
            subCategory.QuadrantYType = HighlightQuadrantType.Mean;
            subCategory.QuadrantYConstant = 0F;
            
            highlightsList.Add(subCategory);

            #endregion "subCategory" Highlight definition

            #region "productAchievedCasepackMinimums" Highlight definition

            Highlight productAchievedCasepackMinimums = Highlight.NewHighlight(entityId);

            productAchievedCasepackMinimums.Name = "06 Product Achieved Casepack Minimums";
            productAchievedCasepackMinimums.Type = HighlightType.Position;
            productAchievedCasepackMinimums.MethodType = HighlightMethodType.Group;
            productAchievedCasepackMinimums.IsFilterEnabled = false;
            productAchievedCasepackMinimums.IsPreFilter = true;
            productAchievedCasepackMinimums.IsAndFilter = true;
            productAchievedCasepackMinimums.Field1 = "{ [PlanogramProduct.PerformanceData.AchievedCasePacks] >= [Planogram.Inventory.MinCasePacks] }";
            productAchievedCasepackMinimums.Field2 = "";
            productAchievedCasepackMinimums.QuadrantXType = HighlightQuadrantType.Mean;
            productAchievedCasepackMinimums.QuadrantXConstant = 0F;
            productAchievedCasepackMinimums.QuadrantYType = HighlightQuadrantType.Mean;
            productAchievedCasepackMinimums.QuadrantYConstant = 0F;
            
            highlightsList.Add(productAchievedCasepackMinimums);

            #endregion "productAchievedCasepackMinimums" Highlight definition

            #region "productAchievedDosMinimums" Highlight definition

            Highlight productAchievedDosMinimums = Highlight.NewHighlight(entityId);

            productAchievedDosMinimums.Name = "07 Product Achieved DOS Minimums";
            productAchievedDosMinimums.Type = HighlightType.Position;
            productAchievedDosMinimums.MethodType = HighlightMethodType.Group;
            productAchievedDosMinimums.IsFilterEnabled = false;
            productAchievedDosMinimums.IsPreFilter = true;
            productAchievedDosMinimums.IsAndFilter = true;
            productAchievedDosMinimums.Field1 = "{ [PlanogramProduct.PerformanceData.AchievedDos]  >= [Planogram.Inventory.MinDos] }";
            productAchievedDosMinimums.Field2 = "";
            productAchievedDosMinimums.QuadrantXType = HighlightQuadrantType.Mean;
            productAchievedDosMinimums.QuadrantXConstant = 0F;
            productAchievedDosMinimums.QuadrantYType = HighlightQuadrantType.Mean;
            productAchievedDosMinimums.QuadrantYConstant = 0F;
            
            highlightsList.Add(productAchievedDosMinimums);

            #endregion "productAchievedDosMinimums" Highlight definition

            #region "outofStockRisk" Highlight definition

            Highlight outofStockRisk = Highlight.NewHighlight(entityId);

            outofStockRisk.Name = "08 Out of Stock Risk";
            outofStockRisk.Type = HighlightType.Position;
            outofStockRisk.MethodType = HighlightMethodType.Group;
            outofStockRisk.IsFilterEnabled = false;
            outofStockRisk.IsPreFilter = true;
            outofStockRisk.IsAndFilter = true;
            outofStockRisk.Field1 = "{ [PlanogramProduct.DeliveryFrequencyDays]  <= [PlanogramProduct.PerformanceData.AchievedDos] }";
            outofStockRisk.Field2 = "";
            outofStockRisk.QuadrantXType = HighlightQuadrantType.Mean;
            outofStockRisk.QuadrantXConstant = 0F;
            outofStockRisk.QuadrantYType = HighlightQuadrantType.Mean;
            outofStockRisk.QuadrantYConstant = 0F;
            
            highlightsList.Add(outofStockRisk);

            #endregion "outofStockRisk" Highlight definition

            #region "salesValue" Highlight definition

            Highlight salesValue = Highlight.NewHighlight(entityId);

            salesValue.Name = "09 Sales Value";
            salesValue.Type = HighlightType.Position;
            salesValue.MethodType = HighlightMethodType.ColourScale;
            salesValue.IsFilterEnabled = false;
            salesValue.IsPreFilter = true;
            salesValue.IsAndFilter = true;
            salesValue.Field1 = "[PlanogramProduct.PerformanceData.P1]";
            salesValue.Field2 = "";
            salesValue.QuadrantXType = HighlightQuadrantType.Mean;
            salesValue.QuadrantXConstant = 0F;
            salesValue.QuadrantYType = HighlightQuadrantType.Mean;
            salesValue.QuadrantYConstant = 0F;
            
            highlightsList.Add(salesValue);

            #endregion "salesValue" Highlight definition

            #region "salesVolume" Highlight definition

            Highlight salesVolume = Highlight.NewHighlight(entityId);

            salesVolume.Name = "10 Sales Volume";
            salesVolume.Type = HighlightType.Position;
            salesVolume.MethodType = HighlightMethodType.ColourScale;
            salesVolume.IsFilterEnabled = false;
            salesVolume.IsPreFilter = true;
            salesVolume.IsAndFilter = true;
            salesVolume.Field1 = "[PlanogramProduct.PerformanceData.P2]";
            salesVolume.Field2 = "";
            salesVolume.QuadrantXType = HighlightQuadrantType.Mean;
            salesVolume.QuadrantXConstant = 0F;
            salesVolume.QuadrantYType = HighlightQuadrantType.Mean;
            salesVolume.QuadrantYConstant = 0F;
            
            highlightsList.Add(salesVolume);

            #endregion "salesVolume" Highlight definition

            #region "salesMargin" Highlight definition

            Highlight salesMargin = Highlight.NewHighlight(entityId);

            salesMargin.Name = "11 Sales Margin";
            salesMargin.Type = HighlightType.Position;
            salesMargin.MethodType = HighlightMethodType.ColourScale;
            salesMargin.IsFilterEnabled = false;
            salesMargin.IsPreFilter = true;
            salesMargin.IsAndFilter = true;
            salesMargin.Field1 = "[PlanogramProduct.PerformanceData.P3]";
            salesMargin.Field2 = "";
            salesMargin.QuadrantXType = HighlightQuadrantType.Mean;
            salesMargin.QuadrantXConstant = 0F;
            salesMargin.QuadrantYType = HighlightQuadrantType.Mean;
            salesMargin.QuadrantYConstant = 0F;
            
            highlightsList.Add(salesMargin);

            #endregion "salesMargin" Highlight definition

            #region "shelfLifeRisk" Highlight definition

            Highlight shelfLifeRisk = Highlight.NewHighlight(entityId);

            shelfLifeRisk.Name = "12 Shelf Life Risk";
            shelfLifeRisk.Type = HighlightType.Position;
            shelfLifeRisk.MethodType = HighlightMethodType.Group;
            shelfLifeRisk.IsFilterEnabled = false;
            shelfLifeRisk.IsPreFilter = true;
            shelfLifeRisk.IsAndFilter = true;
            shelfLifeRisk.Field1 = "{ [PlanogramProduct.PerformanceData.AchievedShelfLife] <= [Planogram.Inventory.MinShelfLife] }";
            shelfLifeRisk.Field2 = "";
            shelfLifeRisk.QuadrantXType = HighlightQuadrantType.Mean;
            shelfLifeRisk.QuadrantXConstant = 0F;
            shelfLifeRisk.QuadrantYType = HighlightQuadrantType.Mean;
            shelfLifeRisk.QuadrantYConstant = 0F;
            
            highlightsList.Add(shelfLifeRisk);

            #endregion "shelfLifeRisk" Highlight definition

            #region "dosFastMovingGoods" Highlight definition

            Highlight dosFastMovingGoods = Highlight.NewHighlight(entityId);

            dosFastMovingGoods.Name = "13 DOS - Fast moving goods";
            dosFastMovingGoods.Type = HighlightType.Position;
            dosFastMovingGoods.MethodType = HighlightMethodType.Characteristic;
            dosFastMovingGoods.IsFilterEnabled = false;
            dosFastMovingGoods.IsPreFilter = true;
            dosFastMovingGoods.IsAndFilter = true;
            dosFastMovingGoods.Field1 = "";
            dosFastMovingGoods.Field2 = "";
            dosFastMovingGoods.QuadrantXType = HighlightQuadrantType.Mean;
            dosFastMovingGoods.QuadrantXConstant = 0F;
            dosFastMovingGoods.QuadrantYType = HighlightQuadrantType.Mean;
            dosFastMovingGoods.QuadrantYConstant = 0F;
            
            #region Highlight Characteristics

            var dosFastMovingGoodsChar01 = HighlightCharacteristic.NewHighlightCharacteristic();
            dosFastMovingGoodsChar01.Name = "Less than 1 DOS";
            dosFastMovingGoodsChar01.IsAndFilter = true;

            var dosFastMovingGoodsChar01Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            dosFastMovingGoodsChar01Rule01.Field = "[PlanogramProduct.PerformanceData.AchievedDos]";
            dosFastMovingGoodsChar01Rule01.Type = HighlightCharacteristicRuleType.LessThan;
            dosFastMovingGoodsChar01Rule01.Value = "1";
            dosFastMovingGoodsChar01.Rules.Add(dosFastMovingGoodsChar01Rule01);

            dosFastMovingGoods.Characteristics.Add(dosFastMovingGoodsChar01);

            var dosFastMovingGoodsChar02 = HighlightCharacteristic.NewHighlightCharacteristic();
            dosFastMovingGoodsChar02.Name = "1 to 3 DOS";
            dosFastMovingGoodsChar02.IsAndFilter = true;

            var dosFastMovingGoodsChar02Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            dosFastMovingGoodsChar02Rule01.Field = "[PlanogramProduct.PerformanceData.AchievedDos]";
            dosFastMovingGoodsChar02Rule01.Type = HighlightCharacteristicRuleType.MoreThanOrEqualTo;
            dosFastMovingGoodsChar02Rule01.Value = "1";
            dosFastMovingGoodsChar02.Rules.Add(dosFastMovingGoodsChar02Rule01);

            var dosFastMovingGoodsChar02Rule02 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            dosFastMovingGoodsChar02Rule02.Field = "[PlanogramProduct.PerformanceData.AchievedDos]";
            dosFastMovingGoodsChar02Rule02.Type = HighlightCharacteristicRuleType.LessThan;
            dosFastMovingGoodsChar02Rule02.Value = "3";
            dosFastMovingGoodsChar02.Rules.Add(dosFastMovingGoodsChar02Rule02);

            dosFastMovingGoods.Characteristics.Add(dosFastMovingGoodsChar02);

            var dosFastMovingGoodsChar03 = HighlightCharacteristic.NewHighlightCharacteristic();
            dosFastMovingGoodsChar03.Name = "3 to 7 DOS";
            dosFastMovingGoodsChar03.IsAndFilter = true;

            var dosFastMovingGoodsChar03Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            dosFastMovingGoodsChar03Rule01.Field = "[PlanogramProduct.PerformanceData.AchievedDos]";
            dosFastMovingGoodsChar03Rule01.Type = HighlightCharacteristicRuleType.MoreThanOrEqualTo;
            dosFastMovingGoodsChar03Rule01.Value = "3";
            dosFastMovingGoodsChar03.Rules.Add(dosFastMovingGoodsChar03Rule01);

            var dosFastMovingGoodsChar03Rule02 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            dosFastMovingGoodsChar03Rule02.Field = "[PlanogramProduct.PerformanceData.AchievedDos]";
            dosFastMovingGoodsChar03Rule02.Type = HighlightCharacteristicRuleType.LessThan;
            dosFastMovingGoodsChar03Rule02.Value = "7";
            dosFastMovingGoodsChar03.Rules.Add(dosFastMovingGoodsChar03Rule02);

            dosFastMovingGoods.Characteristics.Add(dosFastMovingGoodsChar03);

            var dosFastMovingGoodsChar04 = HighlightCharacteristic.NewHighlightCharacteristic();
            dosFastMovingGoodsChar04.Name = "Over 7 DOS";
            dosFastMovingGoodsChar04.IsAndFilter = true;

            var dosFastMovingGoodsChar04Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            dosFastMovingGoodsChar04Rule01.Field = "[PlanogramProduct.PerformanceData.AchievedDos]";
            dosFastMovingGoodsChar04Rule01.Type = HighlightCharacteristicRuleType.MoreThanOrEqualTo;
            dosFastMovingGoodsChar04Rule01.Value = "7";
            dosFastMovingGoodsChar04.Rules.Add(dosFastMovingGoodsChar04Rule01);

            dosFastMovingGoods.Characteristics.Add(dosFastMovingGoodsChar04);

            #endregion


            highlightsList.Add(dosFastMovingGoods);

            #endregion "dosFastMovingGoods" Highlight definition

            #region "dosSlowMovingGoods" Highlight definition

            Highlight dosSlowMovingGoods = Highlight.NewHighlight(entityId);

            dosSlowMovingGoods.Name = "14 DOS - Slow moving goods";
            dosSlowMovingGoods.Type = HighlightType.Position;
            dosSlowMovingGoods.MethodType = HighlightMethodType.Characteristic;
            dosSlowMovingGoods.IsFilterEnabled = false;
            dosSlowMovingGoods.IsPreFilter = true;
            dosSlowMovingGoods.IsAndFilter = true;
            dosSlowMovingGoods.Field1 = "";
            dosSlowMovingGoods.Field2 = "";
            dosSlowMovingGoods.QuadrantXType = HighlightQuadrantType.Mean;
            dosSlowMovingGoods.QuadrantXConstant = 0F;
            dosSlowMovingGoods.QuadrantYType = HighlightQuadrantType.Mean;
            dosSlowMovingGoods.QuadrantYConstant = 0F;
            
            #region Highlight Characteristics

            var dosSlowMovingGoodsChar01 = HighlightCharacteristic.NewHighlightCharacteristic();
            dosSlowMovingGoodsChar01.Name = "Less than 7 DOS";
            dosSlowMovingGoodsChar01.IsAndFilter = true;

            var dosSlowMovingGoodsChar01Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            dosSlowMovingGoodsChar01Rule01.Field = "[PlanogramProduct.PerformanceData.AchievedDos]";
            dosSlowMovingGoodsChar01Rule01.Type = HighlightCharacteristicRuleType.LessThan;
            dosSlowMovingGoodsChar01Rule01.Value = "7";
            dosSlowMovingGoodsChar01.Rules.Add(dosSlowMovingGoodsChar01Rule01);

            dosSlowMovingGoods.Characteristics.Add(dosSlowMovingGoodsChar01);

            var dosSlowMovingGoodsChar02 = HighlightCharacteristic.NewHighlightCharacteristic();
            dosSlowMovingGoodsChar02.Name = "7 to 14 DOS";
            dosSlowMovingGoodsChar02.IsAndFilter = true;

            var dosSlowMovingGoodsChar02Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            dosSlowMovingGoodsChar02Rule01.Field = "[PlanogramProduct.PerformanceData.AchievedDos]";
            dosSlowMovingGoodsChar02Rule01.Type = HighlightCharacteristicRuleType.MoreThanOrEqualTo;
            dosSlowMovingGoodsChar02Rule01.Value = "7";
            dosSlowMovingGoodsChar02.Rules.Add(dosSlowMovingGoodsChar02Rule01);

            var dosSlowMovingGoodsChar02Rule02 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            dosSlowMovingGoodsChar02Rule02.Field = "[PlanogramProduct.PerformanceData.AchievedDos]";
            dosSlowMovingGoodsChar02Rule02.Type = HighlightCharacteristicRuleType.LessThan;
            dosSlowMovingGoodsChar02Rule02.Value = "14";
            dosSlowMovingGoodsChar02.Rules.Add(dosSlowMovingGoodsChar02Rule02);

            dosSlowMovingGoods.Characteristics.Add(dosSlowMovingGoodsChar02);

            var dosSlowMovingGoodsChar03 = HighlightCharacteristic.NewHighlightCharacteristic();
            dosSlowMovingGoodsChar03.Name = "14 to 30 DOS";
            dosSlowMovingGoodsChar03.IsAndFilter = true;

            var dosSlowMovingGoodsChar03Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            dosSlowMovingGoodsChar03Rule01.Field = "[PlanogramProduct.PerformanceData.AchievedDos]";
            dosSlowMovingGoodsChar03Rule01.Type = HighlightCharacteristicRuleType.MoreThanOrEqualTo;
            dosSlowMovingGoodsChar03Rule01.Value = "14";
            dosSlowMovingGoodsChar03.Rules.Add(dosSlowMovingGoodsChar03Rule01);

            var dosSlowMovingGoodsChar03Rule02 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            dosSlowMovingGoodsChar03Rule02.Field = "[PlanogramProduct.PerformanceData.AchievedDos]";
            dosSlowMovingGoodsChar03Rule02.Type = HighlightCharacteristicRuleType.LessThan;
            dosSlowMovingGoodsChar03Rule02.Value = "30";
            dosSlowMovingGoodsChar03.Rules.Add(dosSlowMovingGoodsChar03Rule02);

            dosSlowMovingGoods.Characteristics.Add(dosSlowMovingGoodsChar03);

            var dosSlowMovingGoodsChar04 = HighlightCharacteristic.NewHighlightCharacteristic();
            dosSlowMovingGoodsChar04.Name = "Over 30 DOS";
            dosSlowMovingGoodsChar04.IsAndFilter = true;

            var dosSlowMovingGoodsChar04Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            dosSlowMovingGoodsChar04Rule01.Field = "[PlanogramProduct.PerformanceData.AchievedDos]";
            dosSlowMovingGoodsChar04Rule01.Type = HighlightCharacteristicRuleType.MoreThanOrEqualTo;
            dosSlowMovingGoodsChar04Rule01.Value = "30";
            dosSlowMovingGoodsChar04.Rules.Add(dosSlowMovingGoodsChar04Rule01);

            dosSlowMovingGoods.Characteristics.Add(dosSlowMovingGoodsChar04);

            #endregion


            highlightsList.Add(dosSlowMovingGoods);

            #endregion "dosSlowMovingGoods" Highlight definition

            #region "dosShortShelfLifeGoods" Highlight definition

            Highlight dosShortShelfLifeGoods = Highlight.NewHighlight(entityId);

            dosShortShelfLifeGoods.Name = "15 DOS - Short shelf life goods";
            dosShortShelfLifeGoods.Type = HighlightType.Position;
            dosShortShelfLifeGoods.MethodType = HighlightMethodType.Characteristic;
            dosShortShelfLifeGoods.IsFilterEnabled = false;
            dosShortShelfLifeGoods.IsPreFilter = true;
            dosShortShelfLifeGoods.IsAndFilter = true;
            dosShortShelfLifeGoods.Field1 = "";
            dosShortShelfLifeGoods.Field2 = "";
            dosShortShelfLifeGoods.QuadrantXType = HighlightQuadrantType.Mean;
            dosShortShelfLifeGoods.QuadrantXConstant = 0F;
            dosShortShelfLifeGoods.QuadrantYType = HighlightQuadrantType.Mean;
            dosShortShelfLifeGoods.QuadrantYConstant = 0F;
            
            #region Highlight Characteristics

            var dosShortShelfLifeGoodsChar01 = HighlightCharacteristic.NewHighlightCharacteristic();
            dosShortShelfLifeGoodsChar01.Name = "Less than 0.5 DOS";
            dosShortShelfLifeGoodsChar01.IsAndFilter = true;

            var dosShortShelfLifeGoodsChar01Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            dosShortShelfLifeGoodsChar01Rule01.Field = "[PlanogramProduct.PerformanceData.AchievedDos]";
            dosShortShelfLifeGoodsChar01Rule01.Type = HighlightCharacteristicRuleType.LessThan;
            dosShortShelfLifeGoodsChar01Rule01.Value = "0.5";
            dosShortShelfLifeGoodsChar01.Rules.Add(dosShortShelfLifeGoodsChar01Rule01);

            dosShortShelfLifeGoods.Characteristics.Add(dosShortShelfLifeGoodsChar01);

            var dosShortShelfLifeGoodsChar02 = HighlightCharacteristic.NewHighlightCharacteristic();
            dosShortShelfLifeGoodsChar02.Name = "0.5 to 1 DOS";
            dosShortShelfLifeGoodsChar02.IsAndFilter = true;

            var dosShortShelfLifeGoodsChar02Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            dosShortShelfLifeGoodsChar02Rule01.Field = "[PlanogramProduct.PerformanceData.AchievedDos]";
            dosShortShelfLifeGoodsChar02Rule01.Type = HighlightCharacteristicRuleType.MoreThanOrEqualTo;
            dosShortShelfLifeGoodsChar02Rule01.Value = "0.5";
            dosShortShelfLifeGoodsChar02.Rules.Add(dosShortShelfLifeGoodsChar02Rule01);

            var dosShortShelfLifeGoodsChar02Rule02 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            dosShortShelfLifeGoodsChar02Rule02.Field = "[PlanogramProduct.PerformanceData.AchievedDos]";
            dosShortShelfLifeGoodsChar02Rule02.Type = HighlightCharacteristicRuleType.LessThan;
            dosShortShelfLifeGoodsChar02Rule02.Value = "1";
            dosShortShelfLifeGoodsChar02.Rules.Add(dosShortShelfLifeGoodsChar02Rule02);

            dosShortShelfLifeGoods.Characteristics.Add(dosShortShelfLifeGoodsChar02);

            var dosShortShelfLifeGoodsChar03 = HighlightCharacteristic.NewHighlightCharacteristic();
            dosShortShelfLifeGoodsChar03.Name = "1 to 3 DOS";
            dosShortShelfLifeGoodsChar03.IsAndFilter = true;

            var dosShortShelfLifeGoodsChar03Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            dosShortShelfLifeGoodsChar03Rule01.Field = "[PlanogramProduct.PerformanceData.AchievedDos]";
            dosShortShelfLifeGoodsChar03Rule01.Type = HighlightCharacteristicRuleType.MoreThanOrEqualTo;
            dosShortShelfLifeGoodsChar03Rule01.Value = "1";
            dosShortShelfLifeGoodsChar03.Rules.Add(dosShortShelfLifeGoodsChar03Rule01);

            var dosShortShelfLifeGoodsChar03Rule02 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            dosShortShelfLifeGoodsChar03Rule02.Field = "[PlanogramProduct.PerformanceData.AchievedDos]";
            dosShortShelfLifeGoodsChar03Rule02.Type = HighlightCharacteristicRuleType.LessThan;
            dosShortShelfLifeGoodsChar03Rule02.Value = "3";
            dosShortShelfLifeGoodsChar03.Rules.Add(dosShortShelfLifeGoodsChar03Rule02);

            dosShortShelfLifeGoods.Characteristics.Add(dosShortShelfLifeGoodsChar03);

            var dosShortShelfLifeGoodsChar04 = HighlightCharacteristic.NewHighlightCharacteristic();
            dosShortShelfLifeGoodsChar04.Name = "Over 3 DOS";
            dosShortShelfLifeGoodsChar04.IsAndFilter = true;

            var dosShortShelfLifeGoodsChar04Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            dosShortShelfLifeGoodsChar04Rule01.Field = "[PlanogramProduct.PerformanceData.AchievedDos]";
            dosShortShelfLifeGoodsChar04Rule01.Type = HighlightCharacteristicRuleType.MoreThanOrEqualTo;
            dosShortShelfLifeGoodsChar04Rule01.Value = "3";
            dosShortShelfLifeGoodsChar04.Rules.Add(dosShortShelfLifeGoodsChar04Rule01);

            dosShortShelfLifeGoods.Characteristics.Add(dosShortShelfLifeGoodsChar04);

            #endregion


            highlightsList.Add(dosShortShelfLifeGoods);

            #endregion "dosShortShelfLifeGoods" Highlight definition

            #region "weeklyTurns" Highlight definition

            Highlight weeklyTurns = Highlight.NewHighlight(entityId);

            weeklyTurns.Name = "16 Weekly Turns";
            weeklyTurns.Type = HighlightType.Position;
            weeklyTurns.MethodType = HighlightMethodType.Characteristic;
            weeklyTurns.IsFilterEnabled = false;
            weeklyTurns.IsPreFilter = true;
            weeklyTurns.IsAndFilter = true;
            weeklyTurns.Field1 = "";
            weeklyTurns.Field2 = "";
            weeklyTurns.QuadrantXType = HighlightQuadrantType.Mean;
            weeklyTurns.QuadrantXConstant = 0F;
            weeklyTurns.QuadrantYType = HighlightQuadrantType.Mean;
            weeklyTurns.QuadrantYConstant = 0F;
            
            #region Highlight Characteristics

            var weeklyTurnsChar01 = HighlightCharacteristic.NewHighlightCharacteristic();
            weeklyTurnsChar01.Name = "Less than 1 Turn";
            weeklyTurnsChar01.IsAndFilter = true;

            var weeklyTurnsChar01Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            weeklyTurnsChar01Rule01.Field = "{([PlanogramProduct.PerformanceData.UnitsSoldPerDay]*7)/ [PlanogramPosition.TotalUnits]}";
            weeklyTurnsChar01Rule01.Type = HighlightCharacteristicRuleType.LessThanOrEqualTo;
            weeklyTurnsChar01Rule01.Value = "1";
            weeklyTurnsChar01.Rules.Add(weeklyTurnsChar01Rule01);

            weeklyTurns.Characteristics.Add(weeklyTurnsChar01);

            var weeklyTurnsChar02 = HighlightCharacteristic.NewHighlightCharacteristic();
            weeklyTurnsChar02.Name = "Over 1 Turn";
            weeklyTurnsChar02.IsAndFilter = true;

            var weeklyTurnsChar02Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            weeklyTurnsChar02Rule01.Field = "{([PlanogramProduct.PerformanceData.UnitsSoldPerDay]*7)/ [PlanogramPosition.TotalUnits]}";
            weeklyTurnsChar02Rule01.Type = HighlightCharacteristicRuleType.MoreThan;
            weeklyTurnsChar02Rule01.Value = "1";
            weeklyTurnsChar02.Rules.Add(weeklyTurnsChar02Rule01);

            weeklyTurns.Characteristics.Add(weeklyTurnsChar02);

            #endregion


            highlightsList.Add(weeklyTurns);

            #endregion "weeklyTurns" Highlight definition

            #region "monthlyTurns" Highlight definition

            Highlight monthlyTurns = Highlight.NewHighlight(entityId);

            monthlyTurns.Name = "17 Monthly Turns";
            monthlyTurns.Type = HighlightType.Position;
            monthlyTurns.MethodType = HighlightMethodType.Characteristic;
            monthlyTurns.IsFilterEnabled = false;
            monthlyTurns.IsPreFilter = true;
            monthlyTurns.IsAndFilter = true;
            monthlyTurns.Field1 = "";
            monthlyTurns.Field2 = "";
            monthlyTurns.QuadrantXType = HighlightQuadrantType.Mean;
            monthlyTurns.QuadrantXConstant = 0F;
            monthlyTurns.QuadrantYType = HighlightQuadrantType.Mean;
            monthlyTurns.QuadrantYConstant = 0F;
            
            #region Highlight Characteristics

            var monthlyTurnsChar01 = HighlightCharacteristic.NewHighlightCharacteristic();
            monthlyTurnsChar01.Name = "Less than 1 Turn per Month";
            monthlyTurnsChar01.IsAndFilter = true;

            var monthlyTurnsChar01Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            monthlyTurnsChar01Rule01.Field = "{([PlanogramProduct.PerformanceData.UnitsSoldPerDay]*7)/ [PlanogramPosition.TotalUnits]}";
            monthlyTurnsChar01Rule01.Type = HighlightCharacteristicRuleType.LessThanOrEqualTo;
            monthlyTurnsChar01Rule01.Value = "0.25";
            monthlyTurnsChar01.Rules.Add(monthlyTurnsChar01Rule01);

            monthlyTurns.Characteristics.Add(monthlyTurnsChar01);

            var monthlyTurnsChar02 = HighlightCharacteristic.NewHighlightCharacteristic();
            monthlyTurnsChar02.Name = "Over 1 Turn per Month";
            monthlyTurnsChar02.IsAndFilter = true;

            var monthlyTurnsChar02Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            monthlyTurnsChar02Rule01.Field = "{([PlanogramProduct.PerformanceData.UnitsSoldPerDay]*7)/ [PlanogramPosition.TotalUnits]}";
            monthlyTurnsChar02Rule01.Type = HighlightCharacteristicRuleType.MoreThan;
            monthlyTurnsChar02Rule01.Value = "0.25";
            monthlyTurnsChar02.Rules.Add(monthlyTurnsChar02Rule01);

            var monthlyTurnsChar02Rule02 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            monthlyTurnsChar02Rule02.Field = "{([PlanogramProduct.PerformanceData.UnitsSoldPerDay]*7)/ [PlanogramPosition.TotalUnits]}";
            monthlyTurnsChar02Rule02.Type = HighlightCharacteristicRuleType.LessThan;
            monthlyTurnsChar02Rule02.Value = "1";
            monthlyTurnsChar02.Rules.Add(monthlyTurnsChar02Rule02);

            monthlyTurns.Characteristics.Add(monthlyTurnsChar02);

            var monthlyTurnsChar03 = HighlightCharacteristic.NewHighlightCharacteristic();
            monthlyTurnsChar03.Name = "Weekly Turns";
            monthlyTurnsChar03.IsAndFilter = true;

            var monthlyTurnsChar03Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            monthlyTurnsChar03Rule01.Field = "{([PlanogramProduct.PerformanceData.UnitsSoldPerDay]*7)/ [PlanogramPosition.TotalUnits]}";
            monthlyTurnsChar03Rule01.Type = HighlightCharacteristicRuleType.MoreThanOrEqualTo;
            monthlyTurnsChar03Rule01.Value = "1";
            monthlyTurnsChar03.Rules.Add(monthlyTurnsChar03Rule01);

            monthlyTurns.Characteristics.Add(monthlyTurnsChar03);

            #endregion


            highlightsList.Add(monthlyTurns);

            #endregion "monthlyTurns" Highlight definition

            #region "merchandisingStyle" Highlight definition

            Highlight merchandisingStyle = Highlight.NewHighlight(entityId);

            merchandisingStyle.Name = "18 Merchandising Style";
            merchandisingStyle.Type = HighlightType.Position;
            merchandisingStyle.MethodType = HighlightMethodType.Group;
            merchandisingStyle.IsFilterEnabled = false;
            merchandisingStyle.IsPreFilter = true;
            merchandisingStyle.IsAndFilter = true;
            merchandisingStyle.Field1 = "[PlanogramPosition.MerchandisingStyle]";
            merchandisingStyle.Field2 = "";
            merchandisingStyle.QuadrantXType = HighlightQuadrantType.Mean;
            merchandisingStyle.QuadrantXConstant = 0F;
            merchandisingStyle.QuadrantYType = HighlightQuadrantType.Mean;
            merchandisingStyle.QuadrantYConstant = 0F;
            
            highlightsList.Add(merchandisingStyle);

            #endregion "merchandisingStyle" Highlight definition

            #region "productHasMoreThan1Position" Highlight definition

            Highlight productHasMoreThan1Position = Highlight.NewHighlight(entityId);

            productHasMoreThan1Position.Name = "19 Product has more than 1 position";
            productHasMoreThan1Position.Type = HighlightType.Position;
            productHasMoreThan1Position.MethodType = HighlightMethodType.Group;
            productHasMoreThan1Position.IsFilterEnabled = false;
            productHasMoreThan1Position.IsPreFilter = true;
            productHasMoreThan1Position.IsAndFilter = true;
            productHasMoreThan1Position.Field1 = "{ [PlanogramProduct.MetaPositionCount] <= 1}";
            productHasMoreThan1Position.Field2 = "";
            productHasMoreThan1Position.QuadrantXType = HighlightQuadrantType.Mean;
            productHasMoreThan1Position.QuadrantXConstant = 0F;
            productHasMoreThan1Position.QuadrantYType = HighlightQuadrantType.Mean;
            productHasMoreThan1Position.QuadrantYConstant = 0F;
            
            highlightsList.Add(productHasMoreThan1Position);

            #endregion "productHasMoreThan1Position" Highlight definition

            #region "productsWithGreaterThanOneFacing" Highlight definition

            Highlight productsWithGreaterThanOneFacing = Highlight.NewHighlight(entityId);

            productsWithGreaterThanOneFacing.Name = "20 Products with greater than one facing";
            productsWithGreaterThanOneFacing.Type = HighlightType.Position;
            productsWithGreaterThanOneFacing.MethodType = HighlightMethodType.Group;
            productsWithGreaterThanOneFacing.IsFilterEnabled = false;
            productsWithGreaterThanOneFacing.IsPreFilter = true;
            productsWithGreaterThanOneFacing.IsAndFilter = true;
            productsWithGreaterThanOneFacing.Field1 = "{ [PlanogramPosition.FacingsWide]>1}";
            productsWithGreaterThanOneFacing.Field2 = "";
            productsWithGreaterThanOneFacing.QuadrantXType = HighlightQuadrantType.Mean;
            productsWithGreaterThanOneFacing.QuadrantXConstant = 0F;
            productsWithGreaterThanOneFacing.QuadrantYType = HighlightQuadrantType.Mean;
            productsWithGreaterThanOneFacing.QuadrantYConstant = 0F;
            
            highlightsList.Add(productsWithGreaterThanOneFacing);

            #endregion "productsWithGreaterThanOneFacing" Highlight definition

            #region "productManufacturer" Highlight definition

            Highlight productManufacturer = Highlight.NewHighlight(entityId);

            productManufacturer.Name = "21 Product Manufacturer";
            productManufacturer.Type = HighlightType.Position;
            productManufacturer.MethodType = HighlightMethodType.Group;
            productManufacturer.IsFilterEnabled = false;
            productManufacturer.IsPreFilter = true;
            productManufacturer.IsAndFilter = true;
            productManufacturer.Field1 = "[PlanogramProduct.Manufacturer]";
            productManufacturer.Field2 = "";
            productManufacturer.QuadrantXType = HighlightQuadrantType.Mean;
            productManufacturer.QuadrantXConstant = 0F;
            productManufacturer.QuadrantYType = HighlightQuadrantType.Mean;
            productManufacturer.QuadrantYConstant = 0F;
            
            highlightsList.Add(productManufacturer);

            #endregion "productManufacturer" Highlight definition

            #region "productVendor" Highlight definition

            Highlight productVendor = Highlight.NewHighlight(entityId);

            productVendor.Name = "22 Product Vendor";
            productVendor.Type = HighlightType.Position;
            productVendor.MethodType = HighlightMethodType.Group;
            productVendor.IsFilterEnabled = false;
            productVendor.IsPreFilter = true;
            productVendor.IsAndFilter = true;
            productVendor.Field1 = "[PlanogramProduct.Vendor]";
            productVendor.Field2 = "";
            productVendor.QuadrantXType = HighlightQuadrantType.Mean;
            productVendor.QuadrantXConstant = 0F;
            productVendor.QuadrantYType = HighlightQuadrantType.Mean;
            productVendor.QuadrantYConstant = 0F;
            
            highlightsList.Add(productVendor);

            #endregion "productVendor" Highlight definition

            #region "privateLabel" Highlight definition

            Highlight privateLabel = Highlight.NewHighlight(entityId);

            privateLabel.Name = "23 Private Label";
            privateLabel.Type = HighlightType.Position;
            privateLabel.MethodType = HighlightMethodType.Group;
            privateLabel.IsFilterEnabled = false;
            privateLabel.IsPreFilter = true;
            privateLabel.IsAndFilter = true;
            privateLabel.Field1 = "[PlanogramProduct.IsPrivateLabel]";
            privateLabel.Field2 = "";
            privateLabel.QuadrantXType = HighlightQuadrantType.Mean;
            privateLabel.QuadrantXConstant = 0F;
            privateLabel.QuadrantYType = HighlightQuadrantType.Mean;
            privateLabel.QuadrantYConstant = 0F;

            highlightsList.Add(privateLabel);

            #endregion "privateLabel" Highlight definition

            #region "productStatus" Highlight definition

            Highlight productStatus = Highlight.NewHighlight(entityId);

            productStatus.Name = "24 Product Status";
            productStatus.Type = HighlightType.Position;
            productStatus.MethodType = HighlightMethodType.Group;
            productStatus.IsFilterEnabled = false;
            productStatus.IsPreFilter = true;
            productStatus.IsAndFilter = true;
            productStatus.Field1 = "[PlanogramProduct.StatusType]";
            productStatus.Field2 = "";
            productStatus.QuadrantXType = HighlightQuadrantType.Mean;
            productStatus.QuadrantXConstant = 0F;
            productStatus.QuadrantYType = HighlightQuadrantType.Mean;
            productStatus.QuadrantYConstant = 0F;

            highlightsList.Add(productStatus);

            #endregion "productStatus" Highlight definition

            #region "positionOrientation" Highlight definition

            Highlight positionOrientation = Highlight.NewHighlight(entityId);

            positionOrientation.Name = "25 Position Orientation";
            positionOrientation.Type = HighlightType.Position;
            positionOrientation.MethodType = HighlightMethodType.Group;
            positionOrientation.IsFilterEnabled = false;
            positionOrientation.IsPreFilter = true;
            positionOrientation.IsAndFilter = true;
            positionOrientation.Field1 = "[PlanogramPosition.OrientationType]";
            positionOrientation.Field2 = "";
            positionOrientation.QuadrantXType = HighlightQuadrantType.Mean;
            positionOrientation.QuadrantXConstant = 0F;
            positionOrientation.QuadrantYType = HighlightQuadrantType.Mean;
            positionOrientation.QuadrantYConstant = 0F;
            
            highlightsList.Add(positionOrientation);

            #endregion "positionOrientation" Highlight definition

            #region "productCountryofOrigin" Highlight definition

            Highlight productCountryofOrigin = Highlight.NewHighlight(entityId);

            productCountryofOrigin.Name = "26 Product Country of Origin";
            productCountryofOrigin.Type = HighlightType.Position;
            productCountryofOrigin.MethodType = HighlightMethodType.Group;
            productCountryofOrigin.IsFilterEnabled = false;
            productCountryofOrigin.IsPreFilter = true;
            productCountryofOrigin.IsAndFilter = true;
            productCountryofOrigin.Field1 = "[PlanogramProduct.CountryOfOrigin]";
            productCountryofOrigin.Field2 = "[PlanogramProduct.AchievedDos]";
            productCountryofOrigin.QuadrantXType = HighlightQuadrantType.Mean;
            productCountryofOrigin.QuadrantXConstant = 0F;
            productCountryofOrigin.QuadrantYType = HighlightQuadrantType.Mean;
            productCountryofOrigin.QuadrantYConstant = 0F;
            
            #region Highlight Characteristics

            var productCountryofOriginChar01 = HighlightCharacteristic.NewHighlightCharacteristic();
            productCountryofOriginChar01.Name = " < 1.5 Cases & 3 Days";
            productCountryofOriginChar01.IsAndFilter = true;

            var productCountryofOriginChar01Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            productCountryofOriginChar01Rule01.Field = "[PlanogramProduct.AchievedCasePacks]";
            productCountryofOriginChar01Rule01.Type = HighlightCharacteristicRuleType.LessThan;
            productCountryofOriginChar01Rule01.Value = "1.5";
            productCountryofOriginChar01.Rules.Add(productCountryofOriginChar01Rule01);

            var productCountryofOriginChar01Rule02 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            productCountryofOriginChar01Rule02.Field = "[PlanogramProduct.AchievedDos]";
            productCountryofOriginChar01Rule02.Type = HighlightCharacteristicRuleType.LessThan;
            productCountryofOriginChar01Rule02.Value = "3";
            productCountryofOriginChar01.Rules.Add(productCountryofOriginChar01Rule02);

            productCountryofOrigin.Characteristics.Add(productCountryofOriginChar01);

            var productCountryofOriginChar02 = HighlightCharacteristic.NewHighlightCharacteristic();
            productCountryofOriginChar02.Name = ">= 1.5 Cases < 3 Days";
            productCountryofOriginChar02.IsAndFilter = true;

            var productCountryofOriginChar02Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            productCountryofOriginChar02Rule01.Field = "[PlanogramProduct.AchievedCasePacks]";
            productCountryofOriginChar02Rule01.Type = HighlightCharacteristicRuleType.MoreThanOrEqualTo;
            productCountryofOriginChar02Rule01.Value = "1.5";
            productCountryofOriginChar02.Rules.Add(productCountryofOriginChar02Rule01);

            var productCountryofOriginChar02Rule02 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            productCountryofOriginChar02Rule02.Field = "[PlanogramProduct.AchievedDos]";
            productCountryofOriginChar02Rule02.Type = HighlightCharacteristicRuleType.MoreThanOrEqualTo;
            productCountryofOriginChar02Rule02.Value = "3";
            productCountryofOriginChar02.Rules.Add(productCountryofOriginChar02Rule02);

            productCountryofOrigin.Characteristics.Add(productCountryofOriginChar02);

            var productCountryofOriginChar03 = HighlightCharacteristic.NewHighlightCharacteristic();
            productCountryofOriginChar03.Name = "< 1.5 Cases >= 3 Days";
            productCountryofOriginChar03.IsAndFilter = true;

            var productCountryofOriginChar03Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            productCountryofOriginChar03Rule01.Field = "[PlanogramProduct.AchievedCasePacks]";
            productCountryofOriginChar03Rule01.Type = HighlightCharacteristicRuleType.LessThan;
            productCountryofOriginChar03Rule01.Value = "1.5";
            productCountryofOriginChar03.Rules.Add(productCountryofOriginChar03Rule01);

            var productCountryofOriginChar03Rule02 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            productCountryofOriginChar03Rule02.Field = "[PlanogramProduct.AchievedDos]";
            productCountryofOriginChar03Rule02.Type = HighlightCharacteristicRuleType.MoreThanOrEqualTo;
            productCountryofOriginChar03Rule02.Value = "3";
            productCountryofOriginChar03.Rules.Add(productCountryofOriginChar03Rule02);

            productCountryofOrigin.Characteristics.Add(productCountryofOriginChar03);

            #endregion


            highlightsList.Add(productCountryofOrigin);

            #endregion "productCountryofOrigin" Highlight definition

            #region "productFlavour" Highlight definition

            Highlight productFlavour = Highlight.NewHighlight(entityId);

            productFlavour.Name = "27 Product Flavour";
            productFlavour.Type = HighlightType.Position;
            productFlavour.MethodType = HighlightMethodType.Group;
            productFlavour.IsFilterEnabled = false;
            productFlavour.IsPreFilter = true;
            productFlavour.IsAndFilter = true;
            productFlavour.Field1 = "[PlanogramProduct.Flavour]";
            productFlavour.Field2 = "[PlanogramProduct.AchievedDos]";
            productFlavour.QuadrantXType = HighlightQuadrantType.Mean;
            productFlavour.QuadrantXConstant = 0F;
            productFlavour.QuadrantYType = HighlightQuadrantType.Mean;
            productFlavour.QuadrantYConstant = 0F;
            
            #region Highlight Characteristics

            var productFlavourChar01 = HighlightCharacteristic.NewHighlightCharacteristic();
            productFlavourChar01.Name = " < 1.5 Cases & 3 Days";
            productFlavourChar01.IsAndFilter = true;

            var productFlavourChar01Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            productFlavourChar01Rule01.Field = "[PlanogramProduct.AchievedCasePacks]";
            productFlavourChar01Rule01.Type = HighlightCharacteristicRuleType.LessThan;
            productFlavourChar01Rule01.Value = "1.5";
            productFlavourChar01.Rules.Add(productFlavourChar01Rule01);

            var productFlavourChar01Rule02 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            productFlavourChar01Rule02.Field = "[PlanogramProduct.AchievedDos]";
            productFlavourChar01Rule02.Type = HighlightCharacteristicRuleType.LessThan;
            productFlavourChar01Rule02.Value = "3";
            productFlavourChar01.Rules.Add(productFlavourChar01Rule02);

            productFlavour.Characteristics.Add(productFlavourChar01);

            var productFlavourChar02 = HighlightCharacteristic.NewHighlightCharacteristic();
            productFlavourChar02.Name = ">= 1.5 Cases < 3 Days";
            productFlavourChar02.IsAndFilter = true;

            var productFlavourChar02Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            productFlavourChar02Rule01.Field = "[PlanogramProduct.AchievedCasePacks]";
            productFlavourChar02Rule01.Type = HighlightCharacteristicRuleType.MoreThanOrEqualTo;
            productFlavourChar02Rule01.Value = "1.5";
            productFlavourChar02.Rules.Add(productFlavourChar02Rule01);

            var productFlavourChar02Rule02 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            productFlavourChar02Rule02.Field = "[PlanogramProduct.AchievedDos]";
            productFlavourChar02Rule02.Type = HighlightCharacteristicRuleType.MoreThanOrEqualTo;
            productFlavourChar02Rule02.Value = "3";
            productFlavourChar02.Rules.Add(productFlavourChar02Rule02);

            productFlavour.Characteristics.Add(productFlavourChar02);

            var productFlavourChar03 = HighlightCharacteristic.NewHighlightCharacteristic();
            productFlavourChar03.Name = "< 1.5 Cases >= 3 Days";
            productFlavourChar03.IsAndFilter = true;

            var productFlavourChar03Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            productFlavourChar03Rule01.Field = "[PlanogramProduct.AchievedCasePacks]";
            productFlavourChar03Rule01.Type = HighlightCharacteristicRuleType.LessThan;
            productFlavourChar03Rule01.Value = "1.5";
            productFlavourChar03.Rules.Add(productFlavourChar03Rule01);

            var productFlavourChar03Rule02 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            productFlavourChar03Rule02.Field = "[PlanogramProduct.AchievedDos]";
            productFlavourChar03Rule02.Type = HighlightCharacteristicRuleType.MoreThanOrEqualTo;
            productFlavourChar03Rule02.Value = "3";
            productFlavourChar03.Rules.Add(productFlavourChar03Rule02);

            productFlavour.Characteristics.Add(productFlavourChar03);

            var productFlavourChar04 = HighlightCharacteristic.NewHighlightCharacteristic();
            productFlavourChar04.Name = ">= 1.5 Cases < 3 Days";
            productFlavourChar04.IsAndFilter = true;

            var productFlavourChar04Rule01 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            productFlavourChar04Rule01.Field = "[PlanogramProduct.AchievedCasePacks]";
            productFlavourChar04Rule01.Type = HighlightCharacteristicRuleType.MoreThanOrEqualTo;
            productFlavourChar04Rule01.Value = "1.5";
            productFlavourChar04.Rules.Add(productFlavourChar04Rule01);

            var productFlavourChar04Rule02 = HighlightCharacteristicRule.NewHighlightCharacteristicRule();
            productFlavourChar04Rule02.Field = "[PlanogramProduct.AchievedDos]";
            productFlavourChar04Rule02.Type = HighlightCharacteristicRuleType.LessThan;
            productFlavourChar04Rule02.Value = "3";
            productFlavourChar04.Rules.Add(productFlavourChar04Rule02);

            productFlavour.Characteristics.Add(productFlavourChar04);

            #endregion


            highlightsList.Add(productFlavour);

            #endregion "productFlavour" Highlight definition

            #region "productPackagingType" Highlight definition

            Highlight productPackagingType = Highlight.NewHighlight(entityId);

            productPackagingType.Name = "28 Product Packaging Type";
            productPackagingType.Type = HighlightType.Position;
            productPackagingType.MethodType = HighlightMethodType.Group;
            productPackagingType.IsFilterEnabled = false;
            productPackagingType.IsPreFilter = true;
            productPackagingType.IsAndFilter = true;
            productPackagingType.Field1 = "[PlanogramProduct.PackagingType]";
            productPackagingType.Field2 = "";
            productPackagingType.QuadrantXType = HighlightQuadrantType.Mean;
            productPackagingType.QuadrantXConstant = 0F;
            productPackagingType.QuadrantYType = HighlightQuadrantType.Mean;
            productPackagingType.QuadrantYConstant = 0F;
            
            highlightsList.Add(productPackagingType);

            #endregion "productPackagingType" Highlight definition

            #region "productPegDepth" Highlight definition

            Highlight productPegDepth = Highlight.NewHighlight(entityId);

            productPegDepth.Name = "29 Product Peg Depth";
            productPegDepth.Type = HighlightType.Position;
            productPegDepth.MethodType = HighlightMethodType.Group;
            productPegDepth.IsFilterEnabled = false;
            productPegDepth.IsPreFilter = true;
            productPegDepth.IsAndFilter = true;
            productPegDepth.Field1 = "[PlanogramProduct.PegDepth]";
            productPegDepth.Field2 = "";
            productPegDepth.QuadrantXType = HighlightQuadrantType.Mean;
            productPegDepth.QuadrantXConstant = 0F;
            productPegDepth.QuadrantYType = HighlightQuadrantType.Mean;
            productPegDepth.QuadrantYConstant = 0F;
            
            highlightsList.Add(productPegDepth);

            #endregion "productPegDepth" Highlight definition

            #region "productCanBreakTrayBack" Highlight definition

            Highlight productCanBreakTrayBack = Highlight.NewHighlight(entityId);

            productCanBreakTrayBack.Name = "31 Product can Break Tray Back";
            productCanBreakTrayBack.Type = HighlightType.Position;
            productCanBreakTrayBack.MethodType = HighlightMethodType.Group;
            productCanBreakTrayBack.IsFilterEnabled = false;
            productCanBreakTrayBack.IsPreFilter = true;
            productCanBreakTrayBack.IsAndFilter = true;
            productCanBreakTrayBack.Field1 = "[PlanogramProduct.CanBreakTrayBack]";
            productCanBreakTrayBack.Field2 = "";
            productCanBreakTrayBack.QuadrantXType = HighlightQuadrantType.Mean;
            productCanBreakTrayBack.QuadrantXConstant = 0F;
            productCanBreakTrayBack.QuadrantYType = HighlightQuadrantType.Mean;
            productCanBreakTrayBack.QuadrantYConstant = 0F;
            
            highlightsList.Add(productCanBreakTrayBack);

            #endregion "productCanBreakTrayBack" Highlight definition

            #region "productCanBreakTrayDown" Highlight definition

            Highlight productCanBreakTrayDown = Highlight.NewHighlight(entityId);

            productCanBreakTrayDown.Name = "32 Product can Break Tray Down";
            productCanBreakTrayDown.Type = HighlightType.Position;
            productCanBreakTrayDown.MethodType = HighlightMethodType.Group;
            productCanBreakTrayDown.IsFilterEnabled = false;
            productCanBreakTrayDown.IsPreFilter = true;
            productCanBreakTrayDown.IsAndFilter = true;
            productCanBreakTrayDown.Field1 = "[PlanogramProduct.CanBreakTrayDown]";
            productCanBreakTrayDown.Field2 = "";
            productCanBreakTrayDown.QuadrantXType = HighlightQuadrantType.Mean;
            productCanBreakTrayDown.QuadrantXConstant = 0F;
            productCanBreakTrayDown.QuadrantYType = HighlightQuadrantType.Mean;
            productCanBreakTrayDown.QuadrantYConstant = 0F;
            
            highlightsList.Add(productCanBreakTrayDown);

            #endregion "productCanBreakTrayDown" Highlight definition

            #region "productCanBreakTrayTop" Highlight definition

            Highlight productCanBreakTrayTop = Highlight.NewHighlight(entityId);

            productCanBreakTrayTop.Name = "33 Product can Break Tray Top";
            productCanBreakTrayTop.Type = HighlightType.Position;
            productCanBreakTrayTop.MethodType = HighlightMethodType.Group;
            productCanBreakTrayTop.IsFilterEnabled = false;
            productCanBreakTrayTop.IsPreFilter = true;
            productCanBreakTrayTop.IsAndFilter = true;
            productCanBreakTrayTop.Field1 = "[PlanogramProduct.CanBreakTrayTop]";
            productCanBreakTrayTop.Field2 = "";
            productCanBreakTrayTop.QuadrantXType = HighlightQuadrantType.Mean;
            productCanBreakTrayTop.QuadrantXConstant = 0F;
            productCanBreakTrayTop.QuadrantYType = HighlightQuadrantType.Mean;
            productCanBreakTrayTop.QuadrantYConstant = 0F;
            
            highlightsList.Add(productCanBreakTrayTop);

            #endregion "productCanBreakTrayTop" Highlight definition

            #region "productCanBreakTrayUp" Highlight definition

            Highlight productCanBreakTrayUp = Highlight.NewHighlight(entityId);

            productCanBreakTrayUp.Name = "34 Product can Break Tray Up";
            productCanBreakTrayUp.Type = HighlightType.Position;
            productCanBreakTrayUp.MethodType = HighlightMethodType.Group;
            productCanBreakTrayUp.IsFilterEnabled = false;
            productCanBreakTrayUp.IsPreFilter = true;
            productCanBreakTrayUp.IsAndFilter = true;
            productCanBreakTrayUp.Field1 = "[PlanogramProduct.CanBreakTrayUp]";
            productCanBreakTrayUp.Field2 = "";
            productCanBreakTrayUp.QuadrantXType = HighlightQuadrantType.Mean;
            productCanBreakTrayUp.QuadrantXConstant = 0F;
            productCanBreakTrayUp.QuadrantYType = HighlightQuadrantType.Mean;
            productCanBreakTrayUp.QuadrantYConstant = 0F;
            
            highlightsList.Add(productCanBreakTrayUp);

            #endregion "productCanBreakTrayUp" Highlight definition

            #region "componentNotchNumber" Highlight definition

            Highlight componentNotchNumber = Highlight.NewHighlight(entityId);

            componentNotchNumber.Name = "50 Component Notch Number";
            componentNotchNumber.Type = HighlightType.Position;
            componentNotchNumber.MethodType = HighlightMethodType.Group;
            componentNotchNumber.IsFilterEnabled = false;
            componentNotchNumber.IsPreFilter = true;
            componentNotchNumber.IsAndFilter = true;
            componentNotchNumber.Field1 = "[PlanogramComponent.NotchNumber]";
            componentNotchNumber.Field2 = "";
            componentNotchNumber.QuadrantXType = HighlightQuadrantType.Mean;
            componentNotchNumber.QuadrantXConstant = 0F;
            componentNotchNumber.QuadrantYType = HighlightQuadrantType.Mean;
            componentNotchNumber.QuadrantYConstant = 0F;
            
            highlightsList.Add(componentNotchNumber);

            #endregion "componentNotchNumber" Highlight definition

            #region "componentCombine" Highlight definition

            Highlight componentCombine = Highlight.NewHighlight(entityId);

            componentCombine.Name = "51 Component Combine";
            componentCombine.Type = HighlightType.Position;
            componentCombine.MethodType = HighlightMethodType.Group;
            componentCombine.IsFilterEnabled = false;
            componentCombine.IsPreFilter = true;
            componentCombine.IsAndFilter = true;
            componentCombine.Field1 = "[PlanogramSubComponent.CombineType]";
            componentCombine.Field2 = "";
            componentCombine.QuadrantXType = HighlightQuadrantType.Mean;
            componentCombine.QuadrantXConstant = 0F;
            componentCombine.QuadrantYType = HighlightQuadrantType.Mean;
            componentCombine.QuadrantYConstant = 0F;
            
            highlightsList.Add(componentCombine);

            #endregion "componentCombine" Highlight definition

            #region "componentXlocation" Highlight definition

            Highlight componentXlocation = Highlight.NewHighlight(entityId);

            componentXlocation.Name = "52 Component X location";
            componentXlocation.Type = HighlightType.Position;
            componentXlocation.MethodType = HighlightMethodType.Group;
            componentXlocation.IsFilterEnabled = false;
            componentXlocation.IsPreFilter = true;
            componentXlocation.IsAndFilter = true;
            componentXlocation.Field1 = "[PlanogramComponent.MetaWorldX]";
            componentXlocation.Field2 = "";
            componentXlocation.QuadrantXType = HighlightQuadrantType.Mean;
            componentXlocation.QuadrantXConstant = 0F;
            componentXlocation.QuadrantYType = HighlightQuadrantType.Mean;
            componentXlocation.QuadrantYConstant = 0F;
            
            highlightsList.Add(componentXlocation);

            #endregion "componentXlocation" Highlight definition

            #region "componentYlocation" Highlight definition

            Highlight componentYlocation = Highlight.NewHighlight(entityId);

            componentYlocation.Name = "53 Component Y location";
            componentYlocation.Type = HighlightType.Position;
            componentYlocation.MethodType = HighlightMethodType.Group;
            componentYlocation.IsFilterEnabled = false;
            componentYlocation.IsPreFilter = true;
            componentYlocation.IsAndFilter = true;
            componentYlocation.Field1 = "[PlanogramComponent.MetaWorldY]";
            componentYlocation.Field2 = "";
            componentYlocation.QuadrantXType = HighlightQuadrantType.Mean;
            componentYlocation.QuadrantXConstant = 0F;
            componentYlocation.QuadrantYType = HighlightQuadrantType.Mean;
            componentYlocation.QuadrantYConstant = 0F;
            
            #region Highlight Characteristics

            var componentYlocationChar01 = HighlightCharacteristic.NewHighlightCharacteristic();
            componentYlocationChar01.Name = "Group 1";
            componentYlocationChar01.IsAndFilter = true;

            componentYlocation.Characteristics.Add(componentYlocationChar01);

            #endregion


            highlightsList.Add(componentYlocation);

            #endregion "componentYlocation" Highlight definition

            #region "componentZlocation" Highlight definition

            Highlight componentZlocation = Highlight.NewHighlight(entityId);

            componentZlocation.Name = "54 Component Z location";
            componentZlocation.Type = HighlightType.Position;
            componentZlocation.MethodType = HighlightMethodType.Group;
            componentZlocation.IsFilterEnabled = false;
            componentZlocation.IsPreFilter = true;
            componentZlocation.IsAndFilter = true;
            componentZlocation.Field1 = "[PlanogramComponent.MetaWorldZ]";
            componentZlocation.Field2 = "";
            componentZlocation.QuadrantXType = HighlightQuadrantType.Mean;
            componentZlocation.QuadrantXConstant = 0F;
            componentZlocation.QuadrantYType = HighlightQuadrantType.Mean;
            componentZlocation.QuadrantYConstant = 0F;
            
            #region Highlight Characteristics

            var componentZlocationChar01 = HighlightCharacteristic.NewHighlightCharacteristic();
            componentZlocationChar01.Name = "Group 1";
            componentZlocationChar01.IsAndFilter = true;

            componentZlocation.Characteristics.Add(componentZlocationChar01);

            var componentZlocationChar02 = HighlightCharacteristic.NewHighlightCharacteristic();
            componentZlocationChar02.Name = "Group 1";
            componentZlocationChar02.IsAndFilter = false;

            componentZlocation.Characteristics.Add(componentZlocationChar02);

            #endregion


            highlightsList.Add(componentZlocation);

            #endregion "componentZlocation" Highlight definition

            #region "componentDepth" Highlight definition

            Highlight componentDepth = Highlight.NewHighlight(entityId);

            componentDepth.Name = "55 Component Depth";
            componentDepth.Type = HighlightType.Position;
            componentDepth.MethodType = HighlightMethodType.Group;
            componentDepth.IsFilterEnabled = false;
            componentDepth.IsPreFilter = true;
            componentDepth.IsAndFilter = true;
            componentDepth.Field1 = "[PlanogramComponent.Depth]";
            componentDepth.Field2 = "";
            componentDepth.QuadrantXType = HighlightQuadrantType.Mean;
            componentDepth.QuadrantXConstant = 0F;
            componentDepth.QuadrantYType = HighlightQuadrantType.Mean;
            componentDepth.QuadrantYConstant = 0F;
            
            highlightsList.Add(componentDepth);

            #endregion "componentDepth" Highlight definition

            return highlightsList;
        }

        private static IEnumerable<Label> CreateDefaultLabels(Int32 entityId)
        {
            List<Label> labelList = new List<Label>();

            #region "componentDetails" Label definition

            Label componentDetails = Label.NewLabel(entityId, LabelType.Fixture);

            componentDetails.Name = "01 Component Details";
            componentDetails.BackgroundColour = -1;
            componentDetails.BorderColour = -16777216;
            componentDetails.BorderThickness = 1;
            componentDetails.Text = "[PlanogramFixture.BaySequenceNumber]:[PlanogramComponent.ComponentSequenceNumber]    Notch: [PlanogramComponent.NotchNumber]";
            componentDetails.TextColour = -16777216;
            componentDetails.Font = "Arial";
            componentDetails.FontSize = 20;
            componentDetails.IsRotateToFitOn = true;
            componentDetails.IsShrinkToFitOn = true;
            componentDetails.TextBoxFontBold = false;
            componentDetails.TextBoxFontItalic = false;
            componentDetails.TextBoxFontUnderlined = false;
            componentDetails.LabelHorizontalPlacement = LabelHorizontalPlacementType.Center;
            componentDetails.LabelVerticalPlacement = LabelVerticalPlacementType.Center;
            componentDetails.TextBoxTextAlignment = TextAlignment.Center;
            componentDetails.TextDirection = LabelTextDirectionType.Horizontal;
            componentDetails.TextWrapping = LabelTextWrapping.Word;
            componentDetails.BackgroundTransparency = 1;
            componentDetails.ShowOverImages = true;
            componentDetails.ShowLabelPerFacing = false;

            labelList.Add(componentDetails);

            #endregion "componentDetails" Label definition

            #region "productDetails" Label definition

            Label productDetails = Label.NewLabel(entityId, LabelType.Product);

            productDetails.Name = "01 Product Details";
            productDetails.BackgroundColour = -1;
            productDetails.BorderColour = -16777216;
            productDetails.BorderThickness = 1;
            productDetails.Text = "GTIN: [PlanogramProduct.Gtin] " + System.Environment.NewLine + "Name: [PlanogramProduct.Name]";
            productDetails.TextColour = -16777216;
            productDetails.Font = "Arial";
            productDetails.FontSize = 8;
            productDetails.IsRotateToFitOn = true;
            productDetails.IsShrinkToFitOn = true;
            productDetails.TextBoxFontBold = false;
            productDetails.TextBoxFontItalic = false;
            productDetails.TextBoxFontUnderlined = false;
            productDetails.LabelHorizontalPlacement = LabelHorizontalPlacementType.Center;
            productDetails.LabelVerticalPlacement = LabelVerticalPlacementType.Center;
            productDetails.TextBoxTextAlignment = TextAlignment.Center;
            productDetails.TextDirection = LabelTextDirectionType.Horizontal;
            productDetails.TextWrapping = LabelTextWrapping.Word;
            productDetails.BackgroundTransparency = 1;
            productDetails.ShowOverImages = false;
            productDetails.ShowLabelPerFacing = false;

            labelList.Add(productDetails);

            #endregion "productDetails" Label definition

            #region "cosAndDOS" Label definition

            Label cosAndDos = Label.NewLabel(entityId, LabelType.Product);

            cosAndDos.Name = "02 COS & DOS";
            cosAndDos.BackgroundColour = -1;
            cosAndDos.BorderColour = -16777216;
            cosAndDos.BorderThickness = 1;
            cosAndDos.Text = "GTIN:[PlanogramProduct.Gtin] " + System.Environment.NewLine + "COS:[PlanogramProduct.PerformanceData.AchievedCasePacks] " + System.Environment.NewLine + "DOS:[PlanogramProduct.PerformanceData.AchievedDos]";
            cosAndDos.TextColour = -16777216;
            cosAndDos.Font = "Arial";
            cosAndDos.FontSize = 8;
            cosAndDos.IsRotateToFitOn = true;
            cosAndDos.IsShrinkToFitOn = true;
            cosAndDos.TextBoxFontBold = false;
            cosAndDos.TextBoxFontItalic = false;
            cosAndDos.TextBoxFontUnderlined = false;
            cosAndDos.LabelHorizontalPlacement = LabelHorizontalPlacementType.Center;
            cosAndDos.LabelVerticalPlacement = LabelVerticalPlacementType.Center;
            cosAndDos.TextBoxTextAlignment = TextAlignment.Center;
            cosAndDos.TextDirection = LabelTextDirectionType.Horizontal;
            cosAndDos.TextWrapping = LabelTextWrapping.Word;
            cosAndDos.BackgroundTransparency = 1;
            cosAndDos.ShowOverImages = false;
            cosAndDos.ShowLabelPerFacing = false;

            labelList.Add(cosAndDos);

            #endregion "cosAndDOS" Label definition

            #region "totalUnits" Label definition

            Label totalUnits = Label.NewLabel(entityId, LabelType.Product);

            totalUnits.Name = "03 Total Units";
            totalUnits.BackgroundColour = -1;
            totalUnits.BorderColour = -16777216;
            totalUnits.BorderThickness = 1;
            totalUnits.Text = "GTIN:[PlanogramProduct.Gtin]" + System.Environment.NewLine + "Units:[PlanogramPosition.TotalUnits]";
            totalUnits.TextColour = -16777216;
            totalUnits.Font = "Arial";
            totalUnits.FontSize = 8;
            totalUnits.IsRotateToFitOn = true;
            totalUnits.IsShrinkToFitOn = true;
            totalUnits.TextBoxFontBold = false;
            totalUnits.TextBoxFontItalic = false;
            totalUnits.TextBoxFontUnderlined = false;
            totalUnits.LabelHorizontalPlacement = LabelHorizontalPlacementType.Center;
            totalUnits.LabelVerticalPlacement = LabelVerticalPlacementType.Center;
            totalUnits.TextBoxTextAlignment = TextAlignment.Center;
            totalUnits.TextDirection = LabelTextDirectionType.Horizontal;
            totalUnits.TextWrapping = LabelTextWrapping.Word;
            totalUnits.BackgroundTransparency = 1;
            totalUnits.ShowOverImages = false;
            totalUnits.ShowLabelPerFacing = false;

            labelList.Add(totalUnits);

            #endregion "totalUnits" Label definition

            #region "sequenceNumber" Label definition

            Label sequenceNumber = Label.NewLabel(entityId, LabelType.Product);

            sequenceNumber.Name = "04 Sequence Number";
            sequenceNumber.BackgroundColour = -1;
            sequenceNumber.BorderColour = -16777216;
            sequenceNumber.BorderThickness = 1;
            sequenceNumber.Text = "[PlanogramPosition.PositionSequenceNumber]";
            sequenceNumber.TextColour = -16777216;
            sequenceNumber.Font = "Arial";
            sequenceNumber.FontSize = 8;
            sequenceNumber.IsRotateToFitOn = true;
            sequenceNumber.IsShrinkToFitOn = true;
            sequenceNumber.TextBoxFontBold = false;
            sequenceNumber.TextBoxFontItalic = false;
            sequenceNumber.TextBoxFontUnderlined = false;
            sequenceNumber.LabelHorizontalPlacement = LabelHorizontalPlacementType.Center;
            sequenceNumber.LabelVerticalPlacement = LabelVerticalPlacementType.Center;
            sequenceNumber.TextBoxTextAlignment = TextAlignment.Center;
            sequenceNumber.TextDirection = LabelTextDirectionType.Horizontal;
            sequenceNumber.TextWrapping = LabelTextWrapping.Word;
            sequenceNumber.BackgroundTransparency = 1;
            sequenceNumber.ShowOverImages = false;
            sequenceNumber.ShowLabelPerFacing = false;

            labelList.Add(sequenceNumber);

            #endregion "sequenceNumber" Label definition

            #region "productCasePackUnits" Label definition

            Label productCasePackUnits = Label.NewLabel(entityId, LabelType.Product);

            productCasePackUnits.Name = "05 Case Pack Units";
            productCasePackUnits.BackgroundColour = -1;
            productCasePackUnits.BorderColour = -16777216;
            productCasePackUnits.BorderThickness = 1;
            productCasePackUnits.Text = "GTIN: [PlanogramProduct.Gtin] " + System.Environment.NewLine + "CP Units:[PlanogramProduct.CasePackUnits]";
            productCasePackUnits.TextColour = -16777216;
            productCasePackUnits.Font = "Arial";
            productCasePackUnits.FontSize = 8;
            productCasePackUnits.IsRotateToFitOn = true;
            productCasePackUnits.IsShrinkToFitOn = true;
            productCasePackUnits.TextBoxFontBold = false;
            productCasePackUnits.TextBoxFontItalic = false;
            productCasePackUnits.TextBoxFontUnderlined = false;
            productCasePackUnits.LabelHorizontalPlacement = LabelHorizontalPlacementType.Center;
            productCasePackUnits.LabelVerticalPlacement = LabelVerticalPlacementType.Center;
            productCasePackUnits.TextBoxTextAlignment = TextAlignment.Center;
            productCasePackUnits.TextDirection = LabelTextDirectionType.Horizontal;
            productCasePackUnits.TextWrapping = LabelTextWrapping.Word;
            productCasePackUnits.BackgroundTransparency = 1;
            productCasePackUnits.ShowOverImages = false;
            productCasePackUnits.ShowLabelPerFacing = false;

            labelList.Add(productCasePackUnits);

            #endregion "productCasePackUnits" Label definition

            #region "optimizationSequenceOrder" Label definition

            Label optimizationSequenceNumber = Label.NewLabel(entityId, LabelType.Product);

            optimizationSequenceNumber.Name = "06 Optimization Sequence Order";
            optimizationSequenceNumber.BackgroundColour = -1;
            optimizationSequenceNumber.BorderColour = -16777216;
            optimizationSequenceNumber.BorderThickness = 1;
            optimizationSequenceNumber.Text = "[PlanogramPosition.SequenceNumber]";
            optimizationSequenceNumber.TextColour = -16777216;
            optimizationSequenceNumber.Font = "Arial";
            optimizationSequenceNumber.FontSize = 8;
            optimizationSequenceNumber.IsRotateToFitOn = true;
            optimizationSequenceNumber.IsShrinkToFitOn = true;
            optimizationSequenceNumber.TextBoxFontBold = false;
            optimizationSequenceNumber.TextBoxFontItalic = false;
            optimizationSequenceNumber.TextBoxFontUnderlined = false;
            optimizationSequenceNumber.LabelHorizontalPlacement = LabelHorizontalPlacementType.Center;
            optimizationSequenceNumber.LabelVerticalPlacement = LabelVerticalPlacementType.Center;
            optimizationSequenceNumber.TextBoxTextAlignment = TextAlignment.Center;
            optimizationSequenceNumber.TextDirection = LabelTextDirectionType.Horizontal;
            optimizationSequenceNumber.TextWrapping = LabelTextWrapping.Word;
            optimizationSequenceNumber.BackgroundTransparency = 1;
            optimizationSequenceNumber.ShowOverImages = false;
            optimizationSequenceNumber.ShowLabelPerFacing = false;

            labelList.Add(optimizationSequenceNumber);

            #endregion "optimizationSequenceOrder" Label definition

            return labelList;
        }

        private static IEnumerable<Workflow> CreateDefaultWorkflows(Int32 entityId)
        {
            // Range Refresh
            Workflow rangeRefresh = Workflow.NewWorkflow(entityId,
                Message.Workflow_RangeRefresh_Name,
                Message.Workflow_RangeRefresh_Description,
                Workflow.RangeRefreshWorkFlowTasks);
            SetUpdateProductPerformanceParameters(
                rangeRefresh, enterprise: 0f, cluster: 0f, location: 1f, planAssignment: 0f);
            yield return rangeRefresh;

            // Inventory Refresh
            Workflow inventoryRefresh = Workflow.NewWorkflow(entityId,
                Message.Workflow_InventoryRefresh_Name,
                Message.Workflow_InventoryRefresh_Description,
                Workflow.InventoryRefreshWorkFlowTasks);
            SetUpdateProductPerformanceParameters(
                inventoryRefresh, enterprise: 0f, cluster: 0f, location: 1f, planAssignment: 0f);
            SetIncreaseAssortmentInventoryUsingAssortment(inventoryRefresh, 1, 0, 0);
            yield return inventoryRefresh;

            // Range & Inventory Refresh
            Workflow rangeAndInventoryRefresh = Workflow.NewWorkflow(entityId,
                Message.Workflow_RangeAndInventoryRefresh_Name,
                Message.Workflow_RangeAndInventoryRefresh_Description,
                Workflow.RangeAndInventoryRefreshWorkFlowTasks);
            SetUpdateProductPerformanceParameters(
                rangeAndInventoryRefresh, enterprise: 0f, cluster: 0f, location: 1f, planAssignment: 0f);
            SetIncreaseAssortmentInventoryUsingAssortment(rangeAndInventoryRefresh, 1, 0, 0);
            yield return rangeAndInventoryRefresh;

            // Basic Change
            Workflow basicChange = Workflow.NewWorkflow(entityId,
                Message.Workflow_BasicChange_Name,
                Message.Workflow_BasicChange_Description,
                Workflow.BasicChangeWorkFlowTasks);
            SetUpdateProductPerformanceParameters(
                basicChange, enterprise: 0f, cluster: 0f, location: 1f, planAssignment: 0f);
            yield return basicChange;

            // Validation
            Workflow validation = Workflow.NewWorkflow(entityId,
                Message.Workflow_Validation_Name,
                Message.Workflow_Validation_Description,
                Workflow.ValidationWorkFlowTasks);
            yield return validation;

            // Publish to GFS
            Workflow publishToGfs = Workflow.NewWorkflow(entityId,
                Message.Workflow_PublishToGfs_Name,
                Message.Workflow_PublishToGfs_Description,
                Workflow.PublishPlanogramWorkFlowTasks);
            yield return publishToGfs;

            // Go Location Specific
            Workflow goLocationSpecfic = Workflow.NewWorkflow(entityId,
                Message.Workflow_GoLocationSpecific_Name,
                Message.Workflow_GoLocationSpecific_Description,
                Workflow.GoLocationSpecificWorkFlowTasks);
            SetUpdateProductPerformanceParameters(
                goLocationSpecfic, enterprise: 0f, cluster: 0f, location: 1f, planAssignment: 0f);
            yield return goLocationSpecfic;

            // Merchandise CLASSI Planogram
            Workflow merchandiseClassiPlanogram = Workflow.NewWorkflow(entityId,
                Message.Workflow_MerchandiseClassiPlanogram_Name,
                Message.Workflow_MerchandiseClassiPlanogram_Description,
                Workflow.MerchandiseClassiPlanogramWorkflowTasks);
            SetUpdateProductPerformanceParameters(
                merchandiseClassiPlanogram, enterprise: 0f, cluster: 0f, location: 1f, planAssignment: 0f);
            SetRemoveIllegalProductsParameters(merchandiseClassiPlanogram, removeAction: /* Remove positions and products */1);

            SetMerchandisePlanogramUsingAssortmentParameters(merchandiseClassiPlanogram, false, 1, 0);
            SetMerchandiseProductsInSequenceUsingAssortmentParameters(merchandiseClassiPlanogram, 0, 2);            
            WorkflowTask merchandiseClassiProductInventoryUsingAssortmentTask = merchandiseClassiPlanogram.Tasks.FirstOrDefault(t => t.Details.TaskType.Equals("Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.Task"));
            SetMerchandiseProductInventoryUsingAssortmentParameters(merchandiseClassiProductInventoryUsingAssortmentTask, 2);
            WorkflowTask firstClassiMerchandiseIncreaseProductInventoryTask = merchandiseClassiPlanogram.Tasks.FirstOrDefault(t => t.Details.TaskType.Equals("Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1.Task"));
            SetMerchandiseProductInventoryParameters(firstClassiMerchandiseIncreaseProductInventoryTask, 2);
            WorkflowTask lastClassiMerchandiseIncreaseProductInventoryTask = merchandiseClassiPlanogram.Tasks.LastOrDefault(t => t.Details.TaskType.Equals("Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1.Task"));
            SetMerchandiseProductInventoryParameters(lastClassiMerchandiseIncreaseProductInventoryTask, 1);
            yield return merchandiseClassiPlanogram;

            // Merchandise Location Specific Planogram
            Workflow merchandiseLocationSpecificPlanogram = Workflow.NewWorkflow(entityId,
                Message.Workflow_MerchandiseLocationSpecificPlanogram_Name,
                Message.Workflow_MerchandiseLocationSpecificPlanogram_Description,
                Workflow.MerchandiseLocationSpecificPlanogramWorkflowTasks);
            SetUpdateProductPerformanceParameters(
                merchandiseLocationSpecificPlanogram, enterprise: 0f, cluster: 0f, location: 1f, planAssignment: 0f);
            SetRemoveIllegalProductsParameters(merchandiseLocationSpecificPlanogram, removeAction: /* Remove positions and products */1);
            
            SetMerchandisePlanogramUsingAssortmentParameters(merchandiseLocationSpecificPlanogram, false, 1, 2);
            SetMerchandiseProductsInSequenceUsingAssortmentParameters(merchandiseLocationSpecificPlanogram, 2, 2);
            WorkflowTask merchandiseLocationProductInventoryUsingAssortmentTask = merchandiseLocationSpecificPlanogram.Tasks.FirstOrDefault(t => t.Details.TaskType.Equals("Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.Task"));
            SetMerchandiseProductInventoryUsingAssortmentParameters(merchandiseLocationProductInventoryUsingAssortmentTask, 2);
            WorkflowTask firstLocationMerchandiseIncreaseProductInventoryTask = merchandiseLocationSpecificPlanogram.Tasks.FirstOrDefault(t => t.Details.TaskType.Equals("Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1.Task"));
            SetMerchandiseProductInventoryParameters(firstLocationMerchandiseIncreaseProductInventoryTask, 2);
            WorkflowTask lastLocationMerchandiseIncreaseProductInventoryTask = merchandiseLocationSpecificPlanogram.Tasks.LastOrDefault(t => t.Details.TaskType.Equals("Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1.Task"));
            SetMerchandiseProductInventoryParameters(lastLocationMerchandiseIncreaseProductInventoryTask, 1);
            yield return merchandiseLocationSpecificPlanogram;

            // Create Sequence Template
            Workflow createSequenceTemplate = Workflow.NewWorkflow(entityId,
                Message.Workflow_CreateSequenceTemplate_Name,
                Message.Workflow_CreateSequenceTemplate_Description,
                Workflow.CreateSequenceTemplateWorkflowTasks);
            // Create Sequence Template parameters
            createSequenceTemplate.Tasks[0].Parameters[1].Values.AddRange(PlanogramProduct
                .EnumerateDisplayableFieldInfos(includeMetadata: false, includeCustom: true, includePerformanceData: true)
                .Where(f => !f.PropertyName.Equals(PlanogramProduct.FillColourProperty.Name))
                .Select(f =>
                {
                    var parameterValue = WorkflowTaskParameterValue.NewWorkflowTaskParameterValue();
                    parameterValue.Value1 = ProductAttributeSourceTypeHelper.GetSourceType(f);
                    parameterValue.Value2 = f.PropertyName;
                    return parameterValue;
                })); // Update Product Attributes (all attributes but FillColour).
            createSequenceTemplate.Tasks[3].Parameters[4].Value = 0; // Add to the car park component.
            yield return createSequenceTemplate;
        }

        private static void SetMerchandiseProductInventoryUsingAssortmentParameters(WorkflowTask merchandiseProductInventoryUsingAssortmentTask, Int32 spaceConstraint)
        {
            if (merchandiseProductInventoryUsingAssortmentTask == null) return;
            merchandiseProductInventoryUsingAssortmentTask.Parameters[3].Value = spaceConstraint;
        }

        private static void SetMerchandiseProductInventoryParameters(Workflow workflow, Int32 spaceConstraint)
        {
            WorkflowTask merchandiseProductInventoryTask = workflow.Tasks
                .FirstOrDefault(t => t.Details.TaskType.Equals("Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1.Task"));
            if (merchandiseProductInventoryTask == null) return;
            merchandiseProductInventoryTask.Parameters[0].Value = spaceConstraint;
        }

        private static void SetMerchandiseProductInventoryParameters(WorkflowTask merchandiseIncreaseProductInventoryTask, Int32 spaceConstraint)
        {
            if (merchandiseIncreaseProductInventoryTask == null) return;
            merchandiseIncreaseProductInventoryTask.Parameters[0].Value = spaceConstraint;
        }
                
        #endregion
    }
}