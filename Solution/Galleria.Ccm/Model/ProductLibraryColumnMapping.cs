﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25395 : A.Probyn
//	Created (Auto-generated)
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// ProductLibraryColumnMapping Model object
    /// </summary>
    [Serializable]
    public sealed partial class ProductLibraryColumnMapping : ModelObject<ProductLibraryColumnMapping>
    {
        #region Static Constructor
        static ProductLibraryColumnMapping()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new ProductLibrary Parent
        {
            get
            {
                ProductLibraryColumnMappingList parentList = base.Parent as ProductLibraryColumnMappingList;
                if (parentList != null)
                {
                    return parentList.Parent;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            private set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region Source

        /// <summary>
        /// Source property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> SourceProperty =
            RegisterModelProperty<String>(c => c.Source);

        /// <summary>
        /// Gets/Sets the Source value
        /// </summary>
        public String Source
        {
            get { return GetProperty<String>(SourceProperty); }
            set { SetProperty<String>(SourceProperty, value); }
        }

        #endregion

        #region Destination

        /// <summary>
        /// Destination property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DestinationProperty =
            RegisterModelProperty<String>(c => c.Destination);

        /// <summary>
        /// Gets/Sets the Destination value
        /// </summary>
        public String Destination
        {
            get { return GetProperty<String>(DestinationProperty); }
            set { SetProperty<String>(DestinationProperty, value); }
        }

        #endregion

        #region IsMandatory

        /// <summary>
        /// IsMandatory property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsMandatoryProperty =
            RegisterModelProperty<Boolean>(c => c.IsMandatory);

        /// <summary>
        /// Gets/Sets the IsMandatory value
        /// </summary>
        public Boolean IsMandatory
        {
            get { return GetProperty<Boolean>(IsMandatoryProperty); }
            set { SetProperty<Boolean>(IsMandatoryProperty, value); }
        }

        #endregion

        #region Type

        /// <summary>
        /// Type property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> TypeProperty =
            RegisterModelProperty<String>(c => c.Type);

        /// <summary>
        /// Gets/Sets the Type value
        /// </summary>
        public String Type
        {
            get { return GetProperty<String>(TypeProperty); }
            set { SetProperty<String>(TypeProperty, value); }
        }

        #endregion

        #region Min

        /// <summary>
        /// Min property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MinProperty =
            RegisterModelProperty<Single>(c => c.Min);

        /// <summary>
        /// Gets/Sets the Min value
        /// </summary>
        public Single Min
        {
            get { return GetProperty<Single>(MinProperty); }
            set { SetProperty<Single>(MinProperty, value); }
        }

        #endregion

        #region Max

        /// <summary>
        /// Max property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MaxProperty =
            RegisterModelProperty<Single>(c => c.Max);

        /// <summary>
        /// Gets/Sets the Max value
        /// </summary>
        public Single Max
        {
            get { return GetProperty<Single>(MaxProperty); }
            set { SetProperty<Single>(MaxProperty, value); }
        }

        #endregion

        #region CanDefault

        /// <summary>
        /// CanDefault property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> CanDefaultProperty =
            RegisterModelProperty<Boolean>(c => c.CanDefault);

        /// <summary>
        /// Gets/Sets the CanDefault value
        /// </summary>
        public Boolean CanDefault
        {
            get { return GetProperty<Boolean>(CanDefaultProperty); }
            set { SetProperty<Boolean>(CanDefaultProperty, value); }
        }

        #endregion

        #region Default

        /// <summary>
        /// Default property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DefaultProperty =
            RegisterModelProperty<String>(c => c.Default);

        /// <summary>
        /// Gets/Sets the Default value
        /// </summary>
        public String Default
        {
            get { return GetProperty<String>(DefaultProperty); }
            set { SetProperty<String>(DefaultProperty, value); }
        }

        #endregion

        #region TextFixedWidthColumnStart

        /// <summary>
        /// TextFixedWidthColumnStart property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> TextFixedWidthColumnStartProperty =
            RegisterModelProperty<Int32>(c => c.TextFixedWidthColumnStart);

        /// <summary>
        /// Gets/Sets the TextFixedWidthColumnStart value
        /// </summary>
        public Int32 TextFixedWidthColumnStart
        {
            get { return GetProperty<Int32>(TextFixedWidthColumnStartProperty); }
            set { SetProperty<Int32>(TextFixedWidthColumnStartProperty, value); }
        }

        #endregion

        #region TextFixedWidthColumnEnd

        /// <summary>
        /// TextFixedWidthColumnEnd property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> TextFixedWidthColumnEndProperty =
            RegisterModelProperty<Int32>(c => c.TextFixedWidthColumnEnd);

        /// <summary>
        /// Gets/Sets the TextFixedWidthColumnEnd value
        /// </summary>
        public Int32 TextFixedWidthColumnEnd
        {
            get { return GetProperty<Int32>(TextFixedWidthColumnEndProperty); }
            set { SetProperty<Int32>(TextFixedWidthColumnEndProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new MaxLength(SourceProperty, 150));
            BusinessRules.AddRule(new Required(DestinationProperty));
            BusinessRules.AddRule(new MaxLength(DestinationProperty, 150));
            BusinessRules.AddRule(new Required(TypeProperty));
            BusinessRules.AddRule(new MaxLength(TypeProperty, 50));
            //BusinessRules.AddRule(new Required(DefaultProperty));
            //BusinessRules.AddRule(new MaxLength(DefaultProperty, 50));

        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static ProductLibraryColumnMapping NewProductLibraryColumnMapping()
        {
            ProductLibraryColumnMapping item = new ProductLibraryColumnMapping();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<ProductLibraryColumnMapping>(oldId, newId);
        }
        #endregion

        #region Helper Properties

        /// <summary>
        /// Has the column been mapped
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsMappedProperty =
            RegisterModelProperty<Boolean>(c => c.IsMapped);

        /// <summary>
        /// Gets whether the column is mapped
        /// </summary>
        public Boolean IsMapped
        {
            get { return !String.IsNullOrEmpty(Source); }
        }

        #endregion

    }
}