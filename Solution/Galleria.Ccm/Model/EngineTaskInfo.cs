﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
// V8-25839: L.Ineson
//  Added Category
// V8-28060 : A.Kuszyk
//  Added GetNameAndVersion method.
#endregion

#region Version History: CCM8.1.1
// V8-28224 : M.Shelley
//  Added Description property
#endregion

#endregion

using System;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class EngineTaskInfo : ModelReadOnlyObject<EngineTaskInfo>
    {
        #region Fields

        private String _nameAndVersion;

        #endregion

        #region Properties

        #region TaskType
        /// <summary>
        /// TaskType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> TaskTypeProperty =
            RegisterModelProperty<String>(c => c.TaskType);
        /// <summary>
        /// Returns the task type
        /// </summary>
        public String TaskType
        {
            get { return this.ReadProperty<String>(TaskTypeProperty); }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Returns the task name
        /// </summary>
        public String Name
        {
            get { return this.ReadProperty<String>(NameProperty); }
        }
        #endregion

        #region Description property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);
        /// <summary>
        /// 
        /// </summary>
        public String Description
        {
            get { return this.ReadProperty<String>(DescriptionProperty); }
        }

        #endregion

        #region Category
        /// <summary>
        /// Category property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CategoryProperty =
            RegisterModelProperty<String>(c => c.Category);
        /// <summary>
        /// Returns the task name
        /// </summary>
        public String Category
        {
            get { return this.ReadProperty<String>(CategoryProperty); }
        }
        #endregion

        #region MajorVersion
        /// <summary>
        /// MajorVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> MajorVersionProperty =
            RegisterModelProperty<Byte>(c => c.MajorVersion);
        /// <summary>
        /// Returns the tasks major version number
        /// </summary>
        public Byte MajorVersion
        {
            get { return this.ReadProperty<Byte>(MajorVersionProperty); }
        }
        #endregion

        #region MinorVersion
        /// <summary>
        /// MinorVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> MinorVersionProperty =
            RegisterModelProperty<Byte>(c => c.MinorVersion);
        /// <summary>
        /// Returns the tasks minor version number
        /// </summary>
        public Byte MinorVersion
        {
            get { return this.ReadProperty<Byte>(MinorVersionProperty); }
        }
        #endregion

        #region HasPreStep
        /// <summary>
        /// HasPreStep property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasPreStepProperty =
            RegisterModelProperty<Boolean>(c => c.HasPreStep);
        /// <summary>
        /// Indicates if this task has a pre step
        /// </summary>
        public Boolean HasPreStep
        {
            get { return this.ReadProperty<Boolean>(HasPreStepProperty); }
        }
        #endregion

        #region HasPostStep
        /// <summary>
        /// HasPostStep property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasPostStepProperty =
            RegisterModelProperty<Boolean>(c => c.HasPostStep);
        /// <summary>
        /// Indicates if this task has a post step
        /// </summary>
        public Boolean HasPostStep
        {
            get { return this.ReadProperty<Boolean>(HasPostStepProperty); }
        }
        #endregion

        #region Parameters
        /// <summary>
        /// Parameters property definition
        /// </summary>
        public static readonly ModelPropertyInfo<EngineTaskParameterInfoList> ParametersProperty =
            RegisterModelProperty<EngineTaskParameterInfoList>(c => c.Parameters);
        /// <summary>
        /// Returns all the parameters that are required by the task
        /// </summary>
        public EngineTaskParameterInfoList Parameters
        {
            get { return this.ReadProperty<EngineTaskParameterInfoList>(ParametersProperty); }
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(EngineTaskInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(EngineTaskInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(EngineTaskInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(EngineTaskInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Methods
        /// <summary>
        /// Creates an instance of this task
        /// </summary>
        public TaskBase CreateTask()
        {
            return TaskContainer.CreateTask(this.TaskType);
        }

        /// <summary>
        /// Returns the name and version number formatted in a string. E.g. TaskName v1.0
        /// </summary>
        public String GetNameAndVersion()
        {
            if (_nameAndVersion == null)
            {
                // Format name and version and ensure that the returned string is not more than 100 characters long.
                _nameAndVersion = String.Format("{0} v{1}.{2}", Name, MajorVersion, MinorVersion);
                if (_nameAndVersion.Length > 100) _nameAndVersion = _nameAndVersion.Substring(0, 100);
            }
            return _nameAndVersion;
        }

        #endregion
    }
}
