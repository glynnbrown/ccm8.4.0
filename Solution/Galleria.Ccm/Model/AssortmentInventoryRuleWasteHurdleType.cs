﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Used by the Assortment InventoryRules and InventoryObjectives screens
    /// </summary>
    public enum AssortmentInventoryRuleWasteHurdleType
    {
        Units,
        CasePacks
    }
    public static class AssortmentInventoryRuleWasteHurdleTypeHelper
    {
        public static readonly Dictionary<AssortmentInventoryRuleWasteHurdleType, String> FriendlyNames =
            new Dictionary<AssortmentInventoryRuleWasteHurdleType, String>()
            {
                {AssortmentInventoryRuleWasteHurdleType.Units, Message.Enum_AssortmentInventoryRuleWasteHurdleType_Units},
                {AssortmentInventoryRuleWasteHurdleType.CasePacks, Message.Enum_AssortmentInventoryRuleWasteHurdleType_CasePacks},
            };
    }
}
