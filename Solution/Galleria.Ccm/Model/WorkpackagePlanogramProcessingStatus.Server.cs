﻿using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public partial class WorkpackagePlanogramProcessingStatus
    {
        #region Constructors
        private WorkpackagePlanogramProcessingStatus() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the specified workpackage processing status
        /// </summary>
        public static WorkpackagePlanogramProcessingStatus FetchByPlanogramIdWorkpackageId(Int32 planogramId, Int32 workpackageId)
        {
            return DataPortal.Fetch<WorkpackagePlanogramProcessingStatus>(new FetchByPlanogramIdWorkpackageIdCriteria(planogramId, workpackageId));
        }

        /// <summary>
        /// Returns the specified workpackage processing status
        /// </summary>
        internal static WorkpackagePlanogramProcessingStatus FetchChild(IDalContext dalContext, WorkpackagePlanogramProcessingStatusDto dto)
        {
            return DataPortal.FetchChild<WorkpackagePlanogramProcessingStatus>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, WorkpackagePlanogramProcessingStatusDto dto)
        {
            this.LoadProperty<Int32>(PlanogramIdProperty, dto.PlanogramId);
            this.LoadProperty<Int32>(WorkpackageIdProperty, dto.WorkpackageId);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<ProcessingStatus>(AutomationStatusProperty, (ProcessingStatus)dto.AutomationStatus);
            this.LoadProperty<String>(AutomationStatusDescriptionProperty, dto.AutomationStatusDescription);
            this.LoadProperty<Int32>(AutomationProgressMaxProperty, dto.AutomationProgressMax);
            this.LoadProperty<Int32>(AutomationProgressCurrentProperty, dto.AutomationProgressCurrent);
            this.LoadProperty<DateTime?>(AutomationDateStartedProperty, dto.AutomationDateStarted);
            this.LoadProperty<DateTime?>(AutomationDateLastUpdatedProperty, dto.AutomationDateLastUpdated);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private WorkpackagePlanogramProcessingStatusDto GetDataTransferObject()
        {
            return new WorkpackagePlanogramProcessingStatusDto()
            {
                PlanogramId = this.ReadProperty<Int32>(PlanogramIdProperty),
                WorkpackageId = this.ReadProperty<Int32>(WorkpackageIdProperty),
                RowVersion = this.ReadProperty<RowVersion>(RowVersionProperty),
                AutomationStatus = (Byte)this.ReadProperty<ProcessingStatus>(AutomationStatusProperty),
                AutomationStatusDescription = this.ReadProperty<String>(AutomationStatusDescriptionProperty),
                AutomationProgressMax = this.ReadProperty<Int32>(AutomationProgressMaxProperty),
                AutomationProgressCurrent = this.ReadProperty<Int32>(AutomationProgressCurrentProperty),
                AutomationDateStarted = this.ReadProperty<DateTime?>(AutomationDateStartedProperty),
                AutomationDateLastUpdated = this.ReadProperty<DateTime?>(AutomationDateLastUpdatedProperty),
            };
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching a single instance by its id
        /// </summary>
        private void DataPortal_Fetch(FetchByPlanogramIdWorkpackageIdCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IWorkpackagePlanogramProcessingStatusDal dal = dalContext.GetDal<IWorkpackagePlanogramProcessingStatusDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchByPlanogramIdWorkpackageId(criteria.PlanogramId, criteria.WorkpackageId));
                }
            }
        }

        /// <summary>
        /// Called when this instance is being loaded
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, WorkpackagePlanogramProcessingStatusDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Update
        /// <summary>
        /// Called when updating this instance into the data store
        /// </summary>
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                WorkpackagePlanogramProcessingStatusDto dto = this.GetDataTransferObject();
                using (IWorkpackagePlanogramProcessingStatusDal dal = dalContext.GetDal<IWorkpackagePlanogramProcessingStatusDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #endregion

        #region Commands

        #region Increments

        /// <summary>
        /// Increments the automation progress of a planogram
        /// </summary>
        private partial class AutomationIncrementCommand
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this command on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IWorkpackagePlanogramProcessingStatusDal dal = dalContext.GetDal<IWorkpackagePlanogramProcessingStatusDal>())
                    {
                        dal.AutomationStatusIncrement(this.PlanogramId, this.WorkpackageId, this.StatusDescription, this.DateLastUpdated);
                    }
                }
            }
            #endregion

            #endregion
        }


        #endregion

        #endregion
    }
}
