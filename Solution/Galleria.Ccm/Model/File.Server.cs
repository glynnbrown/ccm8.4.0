﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion

#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model server implementation of the File
    /// </summary>
    public partial class File
    {
        #region Constructors
        //Force use of factory methods
        private File() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the specified file
        /// </summary>
        /// <param name="id">The id of the file</param>
        /// <returns></returns>
        public static File GetById(Int32 id)
        {
            return DataPortal.Fetch<File>(new SingleCriteria<Int32>(id));
        }

        /// <summary>
        /// Returns an existing File
        /// </summary>
        /// <returns>Existing File</returns>
        internal static File GetFile(IDalContext dalContext, FileDto dto)
        {
            return DataPortal.FetchChild<File>(dalContext, dto);
        }

        /// <summary>
        /// Returns an existing File
        /// </summary>
        /// <returns>Existing File with specified id</returns>
        internal static File GetFile(Int32 fileId)
        {
            return DataPortal.FetchChild<File>(new SingleCriteria<File, Int32>(fileId));
        }

        /// <summary>
        /// Returns an existing File
        /// </summary>
        /// <returns>Existing File with specified id</returns>
        internal static File GetFile(IDalContext dalContext, Int32 fileId)
        {
            return DataPortal.FetchChild<File>(dalContext, new SingleCriteria<File, Int32>(fileId));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Returns a new dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private FileDto GetDataTransferObject()
        {
            return new FileDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                Name = ReadProperty<String>(NameProperty),
                SourceFilePath = ReadProperty<String>(SourceFilePathProperty),
                SizeInBytes = ReadProperty<Int64>(SizeInBytesProperty),
                BlobId = ReadProperty<Int32>(BlobIdProperty),
                UserId = ReadProperty<Int32?>(UserIdProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty)
            };
        }
        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dalContext">The dalcontext to use</param>
        /// <param name="dto">The Dto to Load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, FileDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(SourceFilePathProperty, dto.SourceFilePath);
            this.LoadProperty<Int64>(SizeInBytesProperty, dto.SizeInBytes);
            this.LoadProperty<Int32>(BlobIdProperty, dto.BlobId);
            this.LoadProperty<Int32?>(UserIdProperty, dto.UserId);
            LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when return FetchById
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IFileDal dal = dalContext.GetDal<IFileDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchById(criteria.Value));
                }
            }
        }

        /// <summary>
        /// Called when returning an existing Object
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FileDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        /// <summary>
        /// Called when returning an existing object
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(SingleCriteria<File, Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IFileDal dal = dalContext.GetDal<IFileDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchById(criteria.Value));
                }
            }
        }
        /// <summary>
        /// Called when returning an existing object
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, SingleCriteria<File, Int32> criteria)
        {
            using (IFileDal dal = dalContext.GetDal<IFileDal>())
            {
                LoadDataTransferObject(dalContext, dal.FetchById(criteria.Value));
            }
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this instance into the database
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                FieldManager.UpdateChildren(dalContext, this);
                this.LoadProperty<Int32>(BlobIdProperty, FileBlob.Id);
                FileDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (IFileDal dal = dalContext.GetDal<IFileDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<File>(oldId, dto.Id);
                dalContext.Commit();
            }
        }

        /// <summary>
        /// Called when inserting this object into a data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext)
        {
            FieldManager.UpdateChildren(dalContext, this);
            this.LoadProperty<Int32>(BlobIdProperty, FileBlob.Id);
            FileDto dto = GetDataTransferObject();
            Int32 oldId = dto.Id;
            using (IFileDal dal = dalContext.GetDal<IFileDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            dalContext.RegisterId<File>(oldId, dto.Id);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this object in the data store as a root
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                FileDto dto = GetDataTransferObject();
                using (IFileDal dal = dalContext.GetDal<IFileDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }

        /// <summary>
        /// caled when updating this object in the data store
        /// </summary>
        /// <param name="dalContext"></param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            FileDto dto = GetDataTransferObject();
            using (IFileDal dal = dalContext.GetDal<IFileDal>())
            {
                dal.Update(dto);
            }
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when object deletes itself as a root
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IFileDal dal = dalContext.GetDal<IFileDal>())
                {
                    //delete this
                    dal.DeleteById(this.Id);
                }
                using (IBlobDal dal = dalContext.GetDal<IBlobDal>())
                {
                    dal.DeleteById(this.BlobID);
                }
                dalContext.Commit();
            }
        }
        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        /// <param name="dalContext"></param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IFileDal dal = dalContext.GetDal<IFileDal>())
            {
                dal.DeleteById(this.Id);
            }
            using (IBlobDal dal = dalContext.GetDal<IBlobDal>())
            {
                dal.DeleteById(this.BlobID);
            }
        }

        #endregion

        #endregion
    }
}
