﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25452 : N.Haywood
//	Added from SA
// V8-25453 : A.Kuszyk
//  Added FetchByEntityIdProductIds
#endregion
#region Version History: CCM820
// V8-31456 : N.Foster
//  Added batch fetch of custom attribute data
#endregion
#region Version History: (CCM 8.3.0)
// V8-32356 : N.Haywood
//  Added FetchByEntityIdMultipleSearchText
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class ProductList
    {
        #region Constructor
        private ProductList() { }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a list of product objects for the given entity
        /// List is a root object
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static ProductList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<ProductList>(new FetchByEntityIdCriteria(entityId, true));
        }

        /// <summary>
        /// Returns a list of product objects for the given entity id, product ids
        /// </summary>
        public static ProductList FetchByProductIds(IEnumerable<Int32> productIds)
        {
            return DataPortal.Fetch<ProductList>(new FetchByProductIdsCriteria(productIds));
        }

        ///<summary>
        ///Returns a list of product objects for the given product universe
        ///List is a root object
        ///</summary>
        public static ProductList FetchByProductUniverseId(Int32 productUniverseId)
        {
            return DataPortal.Fetch<ProductList>(new FetchByProductUniverseIdCriteria(productUniverseId));
        }

        /// <summary>
        /// Returns all specified products by their ids
        /// </summary>
        public static ProductList FetchByEntityIdProductGtins(Int32 entityId, IEnumerable<String> productGtins)
        {
            return DataPortal.Fetch<ProductList>(new FetchByEntityIdProductGtinsCriteria(entityId, productGtins));
        }

        ///<summary>
        ///Returns a list of product objects for the given merchandising group id.
        ///Note that as a category can have multiple product universes this will return distinct products
        ///for all universes associated with the category.
        ///</summary>
        public static ProductList FetchByMerchandisingGroupId(Int32 categoryId)
        {
            return DataPortal.Fetch<ProductList>(new FetchByMerchandisingGroupIdCriteria(categoryId));
        }

        /// <summary>
        /// Returns a list of product objects for the given entity and
        /// search text. 
        /// </summary>
        public static ProductList FetchByEntityIdSearchText(Int32 entityId, String searchText)
        {
            return DataPortal.Fetch<ProductList>(new FetchByEntityIdSearchTextCriteria(entityId, searchText));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when lazy loading this list from an existing group
        /// note the addition of the MarkAsChild method to the end
        /// of the method
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (BatchFetchContext batchContext = new BatchFetchContext(dalContext))
                {
                    // register batch handlers in order they will be fetched
                    batchContext.Register<ProductDto, IProductDal>((context, dal) => dal.FetchByEntityId(criteria.EntityId));
                    batchContext.Register<CustomAttributeDataDto, ICustomAttributeDataDal>((context, dal) => dal.FetchByParentTypeParentIds((Byte)CustomAttributeDataParentType.Product, context.Dtos<ProductDto>().Select(dto => (Object)dto.Id)));

                    // perform the batch fetch
                    batchContext.Fetch();

                    // build a lookup of custom attribute data indexed by parent Id
                    Dictionary<Object, CustomAttributeDataDto> customAttributeData = new Dictionary<Object, CustomAttributeDataDto>();
                    foreach (CustomAttributeDataDto customAttributeDataDto in batchContext.Dtos<CustomAttributeDataDto>())
                    {
                        customAttributeData.Add(customAttributeDataDto.ParentId, customAttributeDataDto);
                    }

                    // now add the products
                    foreach (ProductDto productDto in batchContext.Dtos<ProductDto>())
                    {
                        CustomAttributeDataDto customAttributeDataDto = null;
                        customAttributeData.TryGetValue(productDto.Id, out customAttributeDataDto);
                        this.Add(Product.GetProduct(dalContext, productDto, customAttributeDataDto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when lazy loading this list from an existing group
        /// note the addition of the MarkAsChild method to the end
        /// of the method 
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        private void DataPortal_Fetch(FetchByProductGroupIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (BatchFetchContext batchContext = new BatchFetchContext(dalContext))
                {
                    // register batch handlers in order they will be fetched
                    batchContext.Register<ProductDto, IProductDal>((context, dal) => dal.FetchByProductGroupId(criteria.ProductGroupId));
                    batchContext.Register<CustomAttributeDataDto, ICustomAttributeDataDal>((context, dal) => dal.FetchByParentTypeParentIds((Byte)CustomAttributeDataParentType.Product, context.Dtos<ProductDto>().Select(dto => (Object)dto.Id)));

                    // perform the batch fetch
                    batchContext.Fetch();

                    // build a lookup of custom attribute data indexed by parent Id
                    Dictionary<Object, CustomAttributeDataDto> customAttributeData = new Dictionary<Object, CustomAttributeDataDto>();
                    foreach (CustomAttributeDataDto customAttributeDataDto in batchContext.Dtos<CustomAttributeDataDto>())
                    {
                        customAttributeData.Add(customAttributeDataDto.ParentId, customAttributeDataDto);
                    }

                    // now add the products
                    foreach (ProductDto productDto in batchContext.Dtos<ProductDto>())
                    {
                        CustomAttributeDataDto customAttributeDataDto = null;
                        customAttributeData.TryGetValue(productDto.Id, out customAttributeDataDto);
                        this.Add(Product.GetProduct(dalContext, productDto, customAttributeDataDto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when lazy loading this list from an existing universe
        /// note the addition of the MarkAsChild method to the end
        /// of the method
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        private void DataPortal_Fetch(FetchByProductUniverseIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (BatchFetchContext batchContext = new BatchFetchContext(dalContext))
                {
                    // register batch handlers in order they will be fetched
                    batchContext.Register<ProductDto, IProductDal>((context, dal) => dal.FetchByProductUniverseId(criteria.ProductUniverseId));
                    batchContext.Register<CustomAttributeDataDto, ICustomAttributeDataDal>((context, dal) => dal.FetchByParentTypeParentIds((Byte)CustomAttributeDataParentType.Product, context.Dtos<ProductDto>().Select(dto => (Object)dto.Id)));

                    // perform the batch fetch
                    batchContext.Fetch();

                    // build a lookup of custom attribute data indexed by parent Id
                    Dictionary<Object, CustomAttributeDataDto> customAttributeData = new Dictionary<Object, CustomAttributeDataDto>();
                    foreach (CustomAttributeDataDto customAttributeDataDto in batchContext.Dtos<CustomAttributeDataDto>())
                    {
                        customAttributeData.Add(customAttributeDataDto.ParentId, customAttributeDataDto);
                    }

                    // now add the products
                    foreach (ProductDto productDto in batchContext.Dtos<ProductDto>())
                    {
                        CustomAttributeDataDto customAttributeDataDto = null;
                        customAttributeData.TryGetValue(productDto.Id, out customAttributeDataDto);
                        this.Add(Product.GetProduct(dalContext, productDto, customAttributeDataDto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when lazy loading this list from an existing group
        /// note the addition of the MarkAsChild method to the end
        /// of the method
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        private void DataPortal_Fetch(FetchByProductIdsCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (BatchFetchContext batchContext = new BatchFetchContext(dalContext))
                {
                    // register batch handlers in order they will be fetched
                    batchContext.Register<ProductDto, IProductDal>((context, dal) => dal.FetchByProductIds(criteria.ProductIds));
                    batchContext.Register<CustomAttributeDataDto, ICustomAttributeDataDal>((context, dal) => dal.FetchByParentTypeParentIds((Byte)CustomAttributeDataParentType.Product, context.Dtos<ProductDto>().Select(dto => (Object)dto.Id)));

                    // perform the batch fetch
                    batchContext.Fetch();

                    // build a lookup of custom attribute data indexed by parent Id
                    Dictionary<Object, CustomAttributeDataDto> customAttributeData = new Dictionary<Object, CustomAttributeDataDto>();
                    foreach (CustomAttributeDataDto customAttributeDataDto in batchContext.Dtos<CustomAttributeDataDto>())
                    {
                        customAttributeData.Add(customAttributeDataDto.ParentId, customAttributeDataDto);
                    }

                    // now add the products
                    foreach (ProductDto productDto in batchContext.Dtos<ProductDto>())
                    {
                        CustomAttributeDataDto customAttributeDataDto = null;
                        customAttributeData.TryGetValue(productDto.Id, out customAttributeDataDto);
                        this.Add(Product.GetProduct(dalContext, productDto, customAttributeDataDto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when fetching products by a list of product ids
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdProductGtinsCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (BatchFetchContext batchContext = new BatchFetchContext(dalContext))
                {
                    // register batch handlers in order they will be fetched
                    batchContext.Register<ProductDto, IProductDal>((context, dal) => dal.FetchByEntityIdProductGtins(criteria.EntityId, criteria.ProductGtins));
                    batchContext.Register<CustomAttributeDataDto, ICustomAttributeDataDal>((context, dal) => dal.FetchByParentTypeParentIds((Byte)CustomAttributeDataParentType.Product, context.Dtos<ProductDto>().Select(dto => (Object)dto.Id)));

                    // perform the batch fetch
                    batchContext.Fetch();

                    // build a lookup of custom attribute data indexed by parent Id
                    Dictionary<Object, CustomAttributeDataDto> customAttributeData = new Dictionary<Object, CustomAttributeDataDto>();
                    foreach (CustomAttributeDataDto customAttributeDataDto in batchContext.Dtos<CustomAttributeDataDto>())
                    {
                        customAttributeData.Add(customAttributeDataDto.ParentId, customAttributeDataDto);
                    }

                    // now add the products
                    foreach (ProductDto productDto in batchContext.Dtos<ProductDto>())
                    {
                        CustomAttributeDataDto customAttributeDataDto = null;
                        customAttributeData.TryGetValue(productDto.Id, out customAttributeDataDto);
                        this.Add(Product.GetProduct(dalContext, productDto, customAttributeDataDto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when lazy loading this list from an existing group
        /// note the addition of the MarkAsChild method to the end
        /// of the method
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        private void DataPortal_Fetch(FetchByMerchandisingGroupIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (BatchFetchContext batchContext = new BatchFetchContext(dalContext))
                {
                    // register batch handlers in order they will be fetched
                    batchContext.Register<ProductDto, IProductDal>((context, dal) => dal.FetchByMerchandisingGroupId(criteria.CategoryId));
                    batchContext.Register<CustomAttributeDataDto, ICustomAttributeDataDal>((context, dal) => dal.FetchByParentTypeParentIds((Byte)CustomAttributeDataParentType.Product, context.Dtos<ProductDto>().Select(dto => (Object)dto.Id)));

                    // perform the batch fetch
                    batchContext.Fetch();

                    // build a lookup of custom attribute data indexed by parent Id
                    Dictionary<Object, CustomAttributeDataDto> customAttributeData = new Dictionary<Object, CustomAttributeDataDto>();
                    foreach (CustomAttributeDataDto customAttributeDataDto in batchContext.Dtos<CustomAttributeDataDto>())
                    {
                        customAttributeData.Add(customAttributeDataDto.ParentId, customAttributeDataDto);
                    }

                    // now add the products
                    foreach (ProductDto productDto in batchContext.Dtos<ProductDto>())
                    {
                        CustomAttributeDataDto customAttributeDataDto = null;
                        customAttributeData.TryGetValue(productDto.Id, out customAttributeDataDto);
                        this.Add(Product.GetProduct(dalContext, productDto, customAttributeDataDto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when lazy loading this list from an existing group
        /// note the addition of the MarkAsChild method to the end
        /// of the method
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        private void DataPortal_Fetch(FetchByEntityIdSearchTextCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (BatchFetchContext batchContext = new BatchFetchContext(dalContext))
                {
                    // register batch handlers in order they will be fetched
                    batchContext.Register<ProductDto, IProductDal>((context, dal) => dal.FetchByEntityIdSearchText(criteria.EntityId, criteria.SearchText));
                    batchContext.Register<CustomAttributeDataDto, ICustomAttributeDataDal>((context, dal) => dal.FetchByParentTypeParentIds((Byte)CustomAttributeDataParentType.Product, context.Dtos<ProductDto>().Select(dto => (Object)dto.Id)));

                    // perform the batch fetch
                    batchContext.Fetch();

                    // build a lookup of custom attribute data indexed by parent Id
                    Dictionary<Object, CustomAttributeDataDto> customAttributeData = new Dictionary<Object, CustomAttributeDataDto>();
                    foreach (CustomAttributeDataDto customAttributeDataDto in batchContext.Dtos<CustomAttributeDataDto>())
                    {
                        customAttributeData.Add(customAttributeDataDto.ParentId, customAttributeDataDto);
                    }

                    // now add the products
                    foreach (ProductDto productDto in batchContext.Dtos<ProductDto>())
                    {
                        CustomAttributeDataDto customAttributeDataDto = null;
                        customAttributeData.TryGetValue(productDto.Id, out customAttributeDataDto);
                        this.Add(Product.GetProduct(dalContext, productDto, customAttributeDataDto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when lazy loading this list from an existing group
        /// note the addition of the MarkAsChild method to the end
        /// of the method
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        private void DataPortal_Fetch(FetchByEntityIdMultipleSearchTextCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (BatchFetchContext batchContext = new BatchFetchContext(dalContext))
                {
                    // register batch handlers in order they will be fetched
                    batchContext.Register<ProductDto, IProductDal>((context, dal) => dal.FetchByEntityIdMultipleSearchText(criteria.EntityId, criteria.MultipleSearchText));
                    batchContext.Register<CustomAttributeDataDto, ICustomAttributeDataDal>((context, dal) => dal.FetchByParentTypeParentIds((Byte)CustomAttributeDataParentType.Product, context.Dtos<ProductDto>().Select(dto => (Object)dto.Id)));

                    // perform the batch fetch
                    batchContext.Fetch();

                    // build a lookup of custom attribute data indexed by parent Id
                    Dictionary<Object, CustomAttributeDataDto> customAttributeData = new Dictionary<Object, CustomAttributeDataDto>();
                    foreach (CustomAttributeDataDto customAttributeDataDto in batchContext.Dtos<CustomAttributeDataDto>())
                    {
                        customAttributeData[customAttributeDataDto.ParentId] = customAttributeDataDto;
                    }

                    // now add the products
                    foreach (ProductDto productDto in batchContext.Dtos<ProductDto>())
                    {
                        CustomAttributeDataDto customAttributeDataDto = null;
                        customAttributeData.TryGetValue(productDto.Id, out customAttributeDataDto);
                        this.Add(Product.GetProduct(dalContext, productDto, customAttributeDataDto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #region Update
        /// <summary>
        /// Called when this list is being updated
        /// (Only when it is a root object)
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                Child_Update(dalContext);
                dalContext.Commit();
            }
        }
        #endregion

        #endregion
    }
}
