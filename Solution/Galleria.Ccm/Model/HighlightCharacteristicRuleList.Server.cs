﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24801 L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public sealed partial class HighlightCharacteristicRuleList
    {
        #region Constructor
        private HighlightCharacteristicRuleList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        internal static HighlightCharacteristicRuleList FetchByParentId(IDalContext dalContext, Int32 parentId)
        {
            return DataPortal.FetchChild<HighlightCharacteristicRuleList>(dalContext, new FetchByParentIdCriteria(null, parentId));
        }

        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FetchByParentIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            using (IHighlightCharacteristicRuleDal dal = dalContext.GetDal<IHighlightCharacteristicRuleDal>())
            {
                IEnumerable<HighlightCharacteristicRuleDto> dtoList = dal.FetchByHighlightCharacteristicId((Int32)criteria.ParentId);
                foreach (HighlightCharacteristicRuleDto dto in dtoList)
                {
                    this.Add(HighlightCharacteristicRule.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
