﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25919 : L.Ineson
//  Created
#endregion
#region Version History: CCM830
// V8-32357 : M.Brumby
//  [PCR01561] added DisplayName and DisplayDescription
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class WorkflowTaskInfo
    {
        #region Constructor
        private WorkflowTaskInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an instance from a dto
        /// </summary>
        internal static WorkflowTaskInfo GetWorkflowTaskInfo(IDalContext dalContext, WorkflowTaskInfoDto dto)
        {
            return DataPortal.FetchChild<WorkflowTaskInfo>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, WorkflowTaskInfoDto dto)
        {
            EngineTaskInfo task = EngineTaskInfo.GetEngineTaskInfo(dto.TaskType);
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Byte>(SequenceIdProperty, dto.SequenceId);
            this.LoadProperty<EngineTaskInfo>(DetailsProperty, task);
            this.LoadProperty<String>(DisplayNameProperty, dto.DisplayName);
            this.LoadProperty<String>(DisplayDescriptionProperty, dto.DisplayDescription);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, WorkflowTaskInfoDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #endregion
    }
}
