﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25559 : L.Hodson
//	Copied from SA
#endregion
#endregion

using System;
using System.Linq;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using System.Collections.Generic;


namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model server implementation of the MetricList
    /// </summary>
    public partial class ConnectionList
    {
        #region Constructors
        //Force use of factory methods
        private ConnectionList() { }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns all connections for the user
        /// </summary>
        /// <returns>A list of Connections</returns>
        internal static ConnectionList Fetch(IDalContext dalContext)
        {
            return DataPortal.FetchChild<ConnectionList>(dalContext);
        }

        /// <summary>
        /// Return the current connection
        /// </summary>
        /// <returns></returns>
        public Connection GetCurrentConnection()
        {
            return this.Where(p => p.IsCurrentConnection == true).FirstOrDefault();
        }
        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Call when returning an existing list
        /// </summary>
        /// <param name="entityId">The current entity id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext)
        {
            RaiseListChangedEvents = false;
            using (IConnectionDal dal = dalContext.GetDal<IConnectionDal>())
            {
                IEnumerable<ConnectionDto> dtoList = dal.FetchAll();
                foreach (ConnectionDto dto in dtoList)
                {
                    this.Add(Connection.GetConnection(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #region Update

        /// <summary>
        /// Called when updating the root list
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            using (dalContext)
            {
                dalContext.Begin();
                Child_Update(dalContext);
                dalContext.Commit();
            }
        }

        #endregion

        #endregion
    }
}
