﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (V8 1.0)
// V8-25395 : A.Probyn
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Enum of values available for ImportDefinition FileType property
    /// Represents a type of the import source file 
    /// </summary>
    public enum ImportDefinitionFileType
    {
        None = 0,
        Excel = 1,
        Text = 2
    }

    public static class ImportDefinitionFileTypeHelper
    {
        public static readonly Dictionary<ImportDefinitionFileType, String> FriendlyNames =
            new Dictionary<ImportDefinitionFileType, String>()
            {
                {ImportDefinitionFileType.None, Message.Enum_ImportDefinitionFileType_None},
                {ImportDefinitionFileType.Excel, Message.Enum_ImportDefinitionFileType_Excel},
                {ImportDefinitionFileType.Text, Message.Enum_ImportDefinitionFileType_Text}
            };

        public static ImportDefinitionFileType? ImportDefinitionFileTypeGetEnum(String description)
        {
            foreach (KeyValuePair<ImportDefinitionFileType, String> keyPair in ImportDefinitionFileTypeHelper.FriendlyNames)
            {
                if (keyPair.Value.Equals(description))
                {
                    return keyPair.Key;
                }
            }
            return null;
        }
    }
}
