﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26159 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public sealed partial class PerformanceSelectionTimelineGroupList
    {
        #region Constructor
        private PerformanceSelectionTimelineGroupList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a list of existing items based on their parent location space
        /// </summary>
        /// <returns>A list of existing it</returns>
        internal static PerformanceSelectionTimelineGroupList FetchByPerformanceSelectionId(IDalContext dalContext, Int32 parentId)
        {
            return DataPortal.FetchChild<PerformanceSelectionTimelineGroupList>(dalContext, parentId);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="productUniverseId">The parent location space id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 parentId)
        {
            RaiseListChangedEvents = false;
            using (IPerformanceSelectionTimelineGroupDal dal = dalContext.GetDal<IPerformanceSelectionTimelineGroupDal>())
            {
                IEnumerable<PerformanceSelectionTimelineGroupDto> dtoList = dal.FetchByPerformanceSelectionId(parentId);
                foreach (PerformanceSelectionTimelineGroupDto dto in dtoList)
                {
                    this.Add(PerformanceSelectionTimelineGroup.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}