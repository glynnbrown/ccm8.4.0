﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A list of products contained within an assortment
    /// </summary>
    [Serializable]
    public partial class AssortmentLocationList : ModelList<AssortmentLocationList, AssortmentLocation>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentLocationList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.AssortmentCreate.ToString()));
            BusinessRules.AddRule(typeof(AssortmentLocationList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.AssortmentGet.ToString()));
            BusinessRules.AddRule(typeof(AssortmentLocationList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.AssortmentEdit.ToString()));
            BusinessRules.AddRule(typeof(AssortmentLocationList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.AssortmentDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A new list</returns>
        public static AssortmentLocationList NewAssortmentLocationList()
        {
            AssortmentLocationList item = new AssortmentLocationList();
            item.Create();
            return item;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds a product to this assortment
        /// </summary>
        /// <param name="product">The product to add</param>
        public void Add(Location location)
        {
            this.Add(AssortmentLocation.NewAssortmentLocation(location));
        }

        /// <summary>
        /// Adds a location to this assortment
        /// </summary>
        /// <param name="locationInfo">The location to add</param>
        public AssortmentLocation Add(LocationInfo locationInfo)
        {
            AssortmentLocation addedItem = AssortmentLocation.NewAssortmentLocation(locationInfo);
            this.Add(addedItem);
            return addedItem;
        }

        /// <summary>
        /// Adds a list of LocationInfos to this assortment
        /// </summary>
        /// <param name="range"></param>
        public void AddList(IEnumerable<LocationInfo> range)
        {
            this.RaiseListChangedEvents = false;

            //cycle through adding all the items
            List<AssortmentLocation> addedItems = new List<AssortmentLocation>();
            foreach (LocationInfo element in range)
            {
                addedItems.Add(this.Add(element));
            }

            this.RaiseListChangedEvents = true;

            //raise out a bulk change notification
            NotifyBulkChange(new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, addedItems));
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}