﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26234 : L.Ineson
//	Copied from GFS
#endregion
#endregion


using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Read only list of Role Info objects
    /// </summary>
    public partial class RoleInfoList
    {
        #region Constructor
        private RoleInfoList() { } //Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all roles in GFS
        /// </summary>
        /// <returns>All roles in GFS</returns>
        public static RoleInfoList FetchAll()
        {
            return DataPortal.Fetch<RoleInfoList>(new FetchAllCriteria());
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning all roles from GFS
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchAllCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IRoleInfoDal dal = dalContext.GetDal<IRoleInfoDal>())
                {
                    foreach (RoleInfoDto dto in dal.FetchAll())
                    {
                        this.Add(RoleInfo.GetRoleInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
