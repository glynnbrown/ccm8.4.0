﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A list of view layout items
    /// </summary>
    [Serializable]
    public sealed partial class ViewLayoutItemList : ModelList<ViewLayoutItemList, ViewLayoutItem> 
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ViewLayoutItemList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(ViewLayoutItemList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(ViewLayoutItemList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(ViewLayoutItemList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Temporary.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        internal static ViewLayoutItemList NewViewLayoutItemList()
        {
            ViewLayoutItemList item = new ViewLayoutItemList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}
