﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Location Space Bay Type Enum
    /// </summary>
    public enum LocationSpaceBayType
    {
        HighGondola = 0,
        LowGondala = 1,
        EndCap = 2,
        HighBackWall = 3,
        LowBackWall = 4,
        UprightFreezerDoors = 5,
        None = 6
    }

    /// <summary>
    /// Location Space Bay Type Helper
    /// </summary>
    public static class LocationSpaceBayTypeHelper
    {
        /// <summary>
        /// Dictionary with Friendly Name as key and BayType as value
        /// </summary>
        public static readonly Dictionary<String, LocationSpaceBayType> EnumFromFriendlyName =
            new Dictionary<String, LocationSpaceBayType>()
        {
            {Message.Enum_LocationSpaceBayType_HighGondola.ToLowerInvariant(), LocationSpaceBayType.HighGondola},
            {Message.Enum_LocationSpaceBayType_LowGondola.ToLowerInvariant(), LocationSpaceBayType.LowGondala},
            {Message.Enum_LocationSpaceBayType_EndCap.ToLowerInvariant(), LocationSpaceBayType.EndCap},
            {Message.Enum_LocationSpaceBayType_HighBackWall.ToLowerInvariant(), LocationSpaceBayType.HighBackWall},
            {Message.Enum_LocationSpaceBayType_LowBackWall.ToLowerInvariant(), LocationSpaceBayType.LowBackWall},
            {Message.Enum_LocationSpaceBayType_UprightFreezerDoors.ToLowerInvariant(), LocationSpaceBayType.UprightFreezerDoors},
            {Message.Enum_LocationSpaceBayType_None.ToLowerInvariant(), LocationSpaceBayType.None}
        };

        public static readonly Dictionary<LocationSpaceBayType, string> FriendlyNames =
            new Dictionary<LocationSpaceBayType, string>()
            {
                {LocationSpaceBayType.HighGondola, Message.Enum_LocationSpaceBayType_HighGondola},
                {LocationSpaceBayType.LowGondala, Message.Enum_LocationSpaceBayType_LowGondola},
                {LocationSpaceBayType.EndCap, Message.Enum_LocationSpaceBayType_EndCap},
                {LocationSpaceBayType.HighBackWall, Message.Enum_LocationSpaceBayType_HighBackWall},
                {LocationSpaceBayType.LowBackWall, Message.Enum_LocationSpaceBayType_LowBackWall},
                {LocationSpaceBayType.UprightFreezerDoors, Message.Enum_LocationSpaceBayType_UprightFreezerDoors},
                {LocationSpaceBayType.None, Message.Enum_LocationSpaceBayType_None},
            };
    }
}
