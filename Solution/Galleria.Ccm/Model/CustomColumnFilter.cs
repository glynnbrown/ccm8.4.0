#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26076 : A.Silva ~ Removed Id property as identifier, user Path Property instead.

#endregion


#region Version History: (CCM 8.3)
// V8-31542 : L.Ineson
//  Added Id and updated implementation.
#endregion

#endregion

using System;
using Galleria.Framework.Model;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class CustomColumnFilter : ModelObject<CustomColumnFilter>
    {
        #region Parent
        /// <summary>
        /// Returns the parent CustomColumnLayout
        /// </summary>
        public new CustomColumnLayout Parent
        {
            get 
            { 
                CustomColumnFilterList parentList = base.Parent as CustomColumnFilterList;
                if(parentList == null) return null;
                else return parentList.Parent;
            }
        }
        #endregion

        #region Properties

        #region Id

        /// <summary>
        ///  Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
           RegisterModelProperty<Int32>(c => c.Id);

        /// <summary>
        /// The item unqiue identifier
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        #endregion

        #region Text Property

        /// <summary>
        ///  Text property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> TextProperty = 
            RegisterModelProperty<String>(c => c.Text, Message.CustomColumnFilter_Text);

        /// <summary>
        ///  Gets/Sets the filter text
        /// </summary>
        public String Text
        {
            get { return GetProperty(TextProperty); }
            set { SetProperty(TextProperty, value); }
        }

        #endregion

        #region Path Property

        /// <summary>
        ///  Path property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> PathProperty = RegisterModelProperty<String>(
            c => c.Path,Message.CustomColumnFilter_Path);

        /// <summary>
        ///     The binding path to the column being filtered.
        /// </summary>
        public String Path
        {
            get { return GetProperty(PathProperty); }
            set { SetProperty(PathProperty, value); }
        }

        #endregion

        #region Header Property

        /// <summary>
        ///  Header property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> HeaderProperty = 
            RegisterModelProperty<String>(c => c.Header,Message.CustomColumnFilter_Header);

        /// <summary>
        ///     The binding header of the column being filtered.
        /// </summary>
        public String Header
        {
            get { return GetProperty(HeaderProperty); }
            set { SetProperty(HeaderProperty, value); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Business Rules

        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="path"></param>
        /// <param name="header"></param>
        /// <returns></returns>
        public static CustomColumnFilter NewCustomColumnFilter(String text, String path, String header)
        {
            CustomColumnFilter item = new CustomColumnFilter();
            item.Create(text, path, header);
            return item;
        }

        #endregion

        #region Data Access

        /// <summary>
        ///     Called when a new instance of <see cref="CustomColumnFilter"/> is being created.
        /// </summary>
        private void Create(String text, String path, String header)
        {
            LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty<String>(TextProperty, text);
            LoadProperty<String>(PathProperty, path);
            LoadProperty<String>(HeaderProperty, header);
            
            MarkAsChild();
            Create();
        }

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<CustomColumnFilter>(oldId, newId);
        }

        #endregion
    }
}