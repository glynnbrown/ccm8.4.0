﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24801 L.Hodson
//  Created
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
#endregion
#region Version History: (CCM 8.10)
// V8-28661 : L.Ineson
//  Implemented IPlanogramHighlight interface
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a HighlightGroup object.
    /// </summary>
    [Serializable]
    public sealed partial class HighlightGroup : ModelObject<HighlightGroup>, IPlanogramHighlightGroup
    {
        #region Static Constructor
        static HighlightGroup()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Highlight Parent
        {
            get
            {
                HighlightGroupList parentList = base.Parent as HighlightGroupList;
                if (parentList != null)
                {
                    return parentList.Parent;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            private set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region Order

        /// <summary>
        /// Order property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> OrderProperty =
            RegisterModelProperty<Int32>(c => c.Order);
        
        /// <summary>
        /// Gets/Sets the order number of this group.
        /// </summary>
        public Int32 Order
        {
            get { return GetProperty<Int32>(OrderProperty); }
            set { SetProperty<Int32>(OrderProperty, value); }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        
        /// <summary>
        /// Gets/Sets the group name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region DisplayName

        /// <summary>
        /// DisplayName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DisplayNameProperty =
            RegisterModelProperty<String>(c => c.DisplayName);
        
        /// <summary>
        /// Gets/Sets the display name for this group
        /// If none is provided then the actual name will be used.
        /// </summary>
        public String DisplayName
        {
            get { return GetProperty<String>(DisplayNameProperty); }
            set { SetProperty<String>(DisplayNameProperty, value); }
        }

        #endregion

        #region FillColour

        /// <summary>
        /// FillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FillColourProperty =
            RegisterModelProperty<Int32>(c => c.FillColour);
        
        /// <summary>
        /// Gets/Sets the fill colour to be used for this group.
        /// </summary>
        public Int32 FillColour
        {
            get { return GetProperty<Int32>(FillColourProperty); }
            set { SetProperty<Int32>(FillColourProperty, value); }
        }

        #endregion

        #region FillPatternType

        /// <summary>
        /// FillPatternType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<HighlightFillPatternType> FillPatternTypeProperty =
            RegisterModelProperty<HighlightFillPatternType>(c => c.FillPatternType);

        /// <summary>
        /// Gets/Sets the fill pattern to be used for this group.
        /// </summary>
        public HighlightFillPatternType FillPatternType
        {
            get { return GetProperty<HighlightFillPatternType>(FillPatternTypeProperty); }
            set { SetProperty<HighlightFillPatternType>(FillPatternTypeProperty, value); }
        }

        PlanogramHighlightFillPatternType IPlanogramHighlightGroup.FillPatternType
        {
            get { return FillPatternType.ToPlanogramHighlightFillPatternType(); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new MinValue<Int32>(OrderProperty, 1));

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <returns></returns>
        public static HighlightGroup NewHighlightGroup()
        {
            HighlightGroup item = new HighlightGroup();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<HighlightFillPatternType>(FillPatternTypeProperty, HighlightFillPatternType.Solid);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<HighlightGroup>(oldId, newId);
        }

        /// <summary>
        /// ToString override.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }

        #endregion

    }
}
