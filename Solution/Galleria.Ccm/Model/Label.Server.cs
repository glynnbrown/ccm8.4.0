﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// v8-24265 : J.Pickup
//		Created
// CCM-24265 : N.Haywood
//      Added more properties
// V8-27940 : L.Luong
//      Added RowVersion, EntityId, DateCreated, DateModified  and DateDeleted 
// V8-28362 : L.Ineson
//  Added the ability to open a file as readonly.
#endregion

#region Version History: (CCM 8.1.0)
// V8-30194 : M.Shelley
//  Added InsertUsingExistingContext method to allow inserting of Labels using the the DAL Context.
#endregion

#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using System.Windows;
using System.IO;
using Galleria.Framework.Dal.Configuration;

namespace Galleria.Ccm.Model
{
    public partial class Label
    {
        #region Constructor
        private Label() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing Label with the given string
        /// </summary>
        public static Label FetchByFilename(String fileName)
        {
            return FetchByFilename(fileName, /*asReadOnly*/false);
        }

        /// <summary>
        /// Returns an existing Label with the given string
        /// </summary>
        public static Label FetchByFilename(String fileName, Boolean asReadOnly)
        {
            return DataPortal.Fetch<Label>(new FetchByIdCriteria(LabelDalFactoryType.FileSystem, fileName, asReadOnly));
        }

        /// <summary>
        /// Returns an existing Label with the given id from the repository
        /// </summary>
        public static Label FetchById(Object id)
        {
            return DataPortal.Fetch<Label>(new FetchByIdCriteria(LabelDalFactoryType.Unknown, id));
        }

        /// <summary>
        /// Returns an existing Label with the given id from the repository
        /// </summary>
        public static Label FetchById(Object id, Boolean asReadOnly)
        {
            return DataPortal.Fetch<Label>(new FetchByIdCriteria(LabelDalFactoryType.Unknown, id, asReadOnly));
        }


        /// <summary>
        /// Returns an existing PlanogramImportTemplate with the given entity id and name.
        /// </summary>
        public static Label FetchByEntityIdName(Int32 entityId, String name)
        {
            return DataPortal.Fetch<Label>(new FetchByEntityIdNameCriteria(entityId, name));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(LabelDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<LabelType>(TypeProperty, (LabelType)dto.Type);
            this.LoadProperty<Int32>(BackgroundColourProperty, dto.BackgroundColour);
            this.LoadProperty<Int32>(BorderColourProperty, dto.BorderColour);
            this.LoadProperty<Int32>(BorderThicknessProperty, dto.BorderThickness);
            this.LoadProperty<String>(TextProperty, dto.Text);
            this.LoadProperty<Int32>(TextColourProperty, dto.TextColour);
            this.LoadProperty<String>(FontProperty, dto.Font);
            this.LoadProperty<Int32>(FontSizeProperty, dto.FontSize);
            this.LoadProperty<Boolean>(IsRotateToFitOnProperty, dto.IsRotateToFitOn);
            this.LoadProperty<Boolean>(IsShrinkToFitOnProperty, dto.IsShrinkToFitOn);
            this.LoadProperty<Boolean>(TextBoxFontBoldProperty, dto.TextBoxFontBold);
            this.LoadProperty<Boolean>(TextBoxFontItalicProperty, dto.TextBoxFontItalic);
            this.LoadProperty<Boolean>(TextBoxFontUnderlinedProperty, dto.TextBoxFontUnderlined);
            this.LoadProperty<LabelHorizontalPlacementType>(LabelHorizontalPlacementProperty, (LabelHorizontalPlacementType)dto.LabelHorizontalPlacement);
            this.LoadProperty<LabelVerticalPlacementType>(LabelVerticalPlacementProperty, (LabelVerticalPlacementType)dto.LabelVerticalPlacement);
            this.LoadProperty<TextAlignment>(TextBoxTextAlignmentProperty, (TextAlignment)dto.TextBoxTextAlignment);
            this.LoadProperty<LabelTextDirectionType>(TextDirectionProperty, (LabelTextDirectionType)dto.TextDirection);
            this.LoadProperty<LabelTextWrapping>(TextWrappingProperty, (LabelTextWrapping)dto.TextWrapping);
            this.LoadProperty<Single>(BackgroundTransparencyProperty, dto.BackgroundTransparency);
            this.LoadProperty<Boolean>(ShowOverImagesProperty, dto.ShowOverImages);
            this.LoadProperty<Boolean>(ShowLabelPerFacingProperty, dto.ShowLabelPerFacing);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private LabelDto GetDataTransferObject()
        {
            return new LabelDto
            {
                Id = ReadProperty<Object>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                Name = ReadProperty<String>(NameProperty),
                Type = (Byte)ReadProperty<LabelType>(TypeProperty),
                BackgroundColour = ReadProperty<Int32>(BackgroundColourProperty),
                BorderColour = ReadProperty<Int32>(BorderColourProperty),
                BorderThickness = ReadProperty<Int32>(BorderThicknessProperty),
                Text = ReadProperty<String>(TextProperty),
                TextColour = ReadProperty<Int32>(TextColourProperty),
                Font = ReadProperty<String>(FontProperty),
                FontSize = ReadProperty<Int32>(FontSizeProperty),
                IsRotateToFitOn = ReadProperty<Boolean>(IsRotateToFitOnProperty),
                IsShrinkToFitOn = ReadProperty<Boolean>(IsShrinkToFitOnProperty),
                TextBoxFontBold = ReadProperty<Boolean>(TextBoxFontBoldProperty),
                TextBoxFontItalic = ReadProperty<Boolean>(TextBoxFontItalicProperty),
                TextBoxFontUnderlined = ReadProperty<Boolean>(TextBoxFontUnderlinedProperty),
                LabelHorizontalPlacement = (Byte)ReadProperty<LabelHorizontalPlacementType>(LabelHorizontalPlacementProperty),
                LabelVerticalPlacement = (Byte)ReadProperty<LabelVerticalPlacementType>(LabelVerticalPlacementProperty),
                TextBoxTextAlignment = (Byte)ReadProperty<TextAlignment>(TextBoxTextAlignmentProperty),
                TextDirection = (Byte)ReadProperty<LabelTextDirectionType>(TextDirectionProperty),
                TextWrapping = (Byte)ReadProperty<LabelTextWrapping>(TextWrappingProperty),
                BackgroundTransparency = ReadProperty<Single>(BackgroundTransparencyProperty),
                ShowOverImages = ReadProperty<Boolean>(ShowOverImagesProperty),
                ShowLabelPerFacing = ReadProperty<Boolean>(ShowLabelPerFacingProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty),
            };
        }


        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching a package by its Id
        /// </summary>
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            if (criteria.LabelDalType == LabelDalFactoryType.FileSystem
                && !criteria.AsReadOnly)
            {
                LockLabelByFileName((String)criteria.Id);
            }
            IDalFactory dalFactory = this.GetDalFactory(criteria.LabelDalType);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILabelDal dal = dalContext.GetDal<ILabelDal>())
                {
                    this.LoadDataTransferObject(dal.FetchById(criteria.Id));

                    //set the readonly flag.
                    this.LoadProperty<Boolean>(IsReadOnlyProperty, criteria.AsReadOnly);
                }
            }
        }

        /// <summary>
        /// Called when fetching a package by entityId and Name
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdNameCriteria criteria)
        {
            IDalFactory dalFactory = GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILabelDal dal = dalContext.GetDal<ILabelDal>())
                {
                    LoadDataTransferObject(dal.FetchByEntityIdName(criteria.EntityId, criteria.Name));
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                LabelDto dto = this.GetDataTransferObject();
                Object oldId = dto.Id;
                using (ILabelDal dal = dalContext.GetDal<ILabelDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Object>(IdProperty, dto.Id);
                this.LoadProperty(RowVersionProperty, dto.RowVersion);
                this.LoadProperty(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<Label>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                LabelDto dto = GetDataTransferObject();
                using (ILabelDal dal = dalContext.GetDal<ILabelDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty(RowVersionProperty, dto.RowVersion);
                this.LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ILabelDal dal = dalContext.GetDal<ILabelDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }
        #endregion

        #endregion

        #region Commands

        #region LockLabelCommand
        /// <summary>
        /// Performs the locking of a label
        /// </summary>
        private partial class LockLabelCommand : CommandBase<LockLabelCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.LabelDalFactoryType, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (ILabelDal dal = dalContext.GetDal<ILabelDal>())
                    {
                        dal.LockById(this.Id);
                    }
                }
            }
            #endregion

            #endregion
        }
        #endregion

        #region UnlockLabelCommand
        /// <summary>
        /// Performs the unlocking of a label
        /// </summary>
        private partial class UnlockLabelCommand : CommandBase<UnlockLabelCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.LabelDalFactoryType, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (ILabelDal dal = dalContext.GetDal<ILabelDal>())
                    {
                        dal.UnlockById(this.Id);
                    }
                }
            }
            #endregion

            #endregion
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Returns the correct dal factory based on the
        /// package type and id
        /// </summary>
        private IDalFactory GetDalFactory(LabelDalFactoryType dalType)
        {
            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(dalType, out dalFactoryName);
            this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            return dalFactory;
        }

        /// <summary>
        /// Returns the correct dal factory for this instance
        /// </summary>
        protected override IDalFactory GetDalFactory()
        {
            if (String.IsNullOrEmpty(this.DalFactoryName))
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.DalFactoryType, out dalFactoryName);
                this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            }

            return DalContainer.GetDalFactory(this.DalFactoryName);
        }


        /// <summary>
        /// Returns the correct dal factory based on the given type and args.
        /// </summary>
        private static IDalFactory GetDalFactory(LabelDalFactoryType dalType, out String dalFactoryName)
        {
            switch (dalType)
            {
                case LabelDalFactoryType.FileSystem:
                    dalFactoryName = Constants.UserDal;
                    return DalContainer.GetDalFactory(dalFactoryName);

                default:
                    dalFactoryName = DalContainer.DalName;
                    return DalContainer.GetDalFactory();
            }

        }

        /// <summary>
        /// Called when inserting this object and its children using an existing dal context.
        /// </summary>
        ///<remarks>used to insert default by sync process.</remarks>
        internal void InsertUsingExistingContext(IDalContext dalContext)
        {
            LabelDto dto = GetDataTransferObject();
            Object oldId = dto.Id;

            using (ILabelDal dal = dalContext.GetDal<ILabelDal>())
            {
                dal.Insert(dto);
            }

            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty(RowVersionProperty, dto.RowVersion);
            this.LoadProperty(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
            dalContext.RegisterId<Label>(oldId, dto.Id);

            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion
    }
}