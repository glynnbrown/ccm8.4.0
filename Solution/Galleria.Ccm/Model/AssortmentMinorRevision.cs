﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
// V8-27720 : I.George
// Made ProductGroupId Not nullable and added business rules
#endregion
#region Version History: (CCM 8.0.3)
// V8-29214 : D.Pleasance
//  Removed DateDeleted as items are now deleted outright
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a minor revision in GFS
    /// 
    /// This is a root MasterData object and so it has the following properties/actions:
    /// - This object has a DateDeleted property and associated field in its supporting database table
    /// - If this object is deleted, it is marked as deleted (this allows for 'undeletes') and any child objects are left unchanged
    /// </summary>
    [Serializable]
    [DefaultNewMethod("NewAssortmentMinorRevision", "EntityId")]
    public partial class AssortmentMinorRevision : ModelObject<AssortmentMinorRevision>
    {
        #region Static Constructor
        static AssortmentMinorRevision()
        {
            FriendlyName = Message.AssortmentMinorRevision;

            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
           RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The Consumer Decision Tree Id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32?> ConsumerDecisionTreeIdProperty =
            RegisterModelProperty<Int32?>(c => c.ConsumerDecisionTreeId);
        /// <summary>
        /// The Consumer Decision Tree Id
        /// </summary>
        public Int32? ConsumerDecisionTreeId
        {
            get { return GetProperty<Int32?>(ConsumerDecisionTreeIdProperty); }
            set { SetProperty<Int32?>(ConsumerDecisionTreeIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
           RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The Consumer Decision Tree Id
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }

        public static readonly ModelPropertyInfo<Guid> UniqueContentReferenceProperty =
           RegisterModelProperty<Guid>(c => c.UniqueContentReference);
        /// <summary>
        /// The Consumer Decision Tree Id
        /// </summary>
        public Guid UniqueContentReference
        {
            get { return GetProperty<Guid>(UniqueContentReferenceProperty); }
            set { SetProperty<Guid>(UniqueContentReferenceProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The Consumer Decision Tree Id
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        /// <summary>
        /// Entity id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        /// <summary>
        /// The entity id
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
        }

        /// <summary>
        /// Product group id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProductGroupIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductGroupId);
        /// <summary>
        /// The id of the product group this item is assigned to
        /// </summary>
        public Int32 ProductGroupId
        {
            get { return GetProperty<Int32>(ProductGroupIdProperty); }
            set { SetProperty<Int32>(ProductGroupIdProperty, value); }
        }

        /// <summary>
        /// Date Created property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);
        /// <summary>
        /// Returns the date this item was created
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
        }


        /// <summary>
        /// Date last modified proeprty definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        /// <summary>
        /// Returns the date this item was last modified
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
        }
        
        /// <summary>
        /// The Assortment minor revision list actions list
        /// </summary>
        public static readonly ModelPropertyInfo<AssortmentMinorRevisionListActionList> ListActionsProperty =
            RegisterModelProperty<AssortmentMinorRevisionListActionList>(c => c.ListActions);
        public AssortmentMinorRevisionListActionList ListActions
        {
            get { return GetProperty<AssortmentMinorRevisionListActionList>(ListActionsProperty); }
        }

        /// <summary>
        /// The minor revision delist actions list
        /// </summary>
        public static readonly ModelPropertyInfo<AssortmentMinorRevisionDeListActionList> DeListActionsProperty =
            RegisterModelProperty<AssortmentMinorRevisionDeListActionList>(c => c.DeListActions);
        public AssortmentMinorRevisionDeListActionList DeListActions
        {
            get { return GetProperty<AssortmentMinorRevisionDeListActionList>(DeListActionsProperty); }
        }

        /// <summary>
        /// The Assortment minor revision amend distribution actions list
        /// </summary>
        public static readonly ModelPropertyInfo<AssortmentMinorRevisionAmendDistributionActionList> AmendDistributionActionsProperty =
            RegisterModelProperty<AssortmentMinorRevisionAmendDistributionActionList>(c => c.AmendDistributionActions);
        public AssortmentMinorRevisionAmendDistributionActionList AmendDistributionActions
        {
            get { return GetProperty<AssortmentMinorRevisionAmendDistributionActionList>(AmendDistributionActionsProperty); }
        }

        /// <summary>
        /// The Assortment minor revision replace actions list
        /// </summary>
        public static readonly ModelPropertyInfo<AssortmentMinorRevisionReplaceActionList> ReplaceActionsProperty =
            RegisterModelProperty<AssortmentMinorRevisionReplaceActionList>(c => c.ReplaceActions);
        public AssortmentMinorRevisionReplaceActionList ReplaceActions
        {
            get { return GetProperty<AssortmentMinorRevisionReplaceActionList>(ReplaceActionsProperty); }
        }

        /// <summary>
        /// Parent unique content reference property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Guid?> ParentUniqueContentReferenceProperty =
            RegisterModelProperty<Guid?>(c => c.ParentUniqueContentReference);
        /// <summary>
        /// Parent unique content reference
        /// </summary>
        public Guid? ParentUniqueContentReference
        {
            get { return GetProperty<Guid?>(ParentUniqueContentReferenceProperty); }
        }

        #endregion

        #region HelperProperties

        /// <summary>
        /// returns a count of all actions in the current scenario
        /// </summary>
        public Int32 TotalActionsCount
        {
            get
            {

                if (this.ListActions != null
                    && this.DeListActions != null
                    && this.AmendDistributionActions != null)
                {
                    return this.ListActions.Count + this.DeListActions.Count + this.AmendDistributionActions.Count;
                }

                return 0;
            }
        }

        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
            BusinessRules.AddRule(new MinValue<Int32>(ProductGroupIdProperty, 1));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentMinorRevision), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.AssortmentMinorRevisionCreate.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevision), new IsInRole(AuthorizationActions.GetObject, DomainPermission.AssortmentMinorRevisionGet.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevision), new IsInRole(AuthorizationActions.EditObject, DomainPermission.AssortmentMinorRevisionEdit.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevision), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.AssortmentMinorRevisionDelete.ToString()));
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByUniqueContentReference
        /// </summary>
        [Serializable]
        public class FetchByUniqueContentReferenceCriteria : CriteriaBase<FetchByUniqueContentReferenceCriteria>
        {
            public static readonly PropertyInfo<Guid> UniqueContentReferenceProperty =
                RegisterProperty<Guid>(c => c.UniqueContentReference);
            public Guid UniqueContentReference
            {
                get { return ReadProperty<Guid>(UniqueContentReferenceProperty); }
            }

            public FetchByUniqueContentReferenceCriteria(Guid ucr)
            {
                LoadProperty<Guid>(UniqueContentReferenceProperty, ucr);
            }
        }

        /// <summary>
        /// Criteria for FetchByLatestVersionByName
        /// </summary>
        [Serializable]
        public class FetchByEntityIdNameCriteria : CriteriaBase<FetchByEntityIdNameCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static readonly PropertyInfo<String> NameProperty =
                RegisterProperty<String>(c => c.Name);
            public String Name
            {
                get { return ReadProperty<String>(NameProperty); }
            }

            public FetchByEntityIdNameCriteria(Int32 entityId, String name)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<String>(NameProperty, name);
            }
        }


        /// <summary>
        /// Criteria for DeleteById
        /// </summary>
        [Serializable]
        public class DeleteByAssortmentMinorRevisonIdCriteria : Csla.CriteriaBase<DeleteByAssortmentMinorRevisonIdCriteria>
        {
            public static readonly PropertyInfo<Int32> AssortmentIdProperty =
            RegisterProperty<Int32>(c => c.AssortmentId);
            public Int32 AssortmentId
            {
                get { return ReadProperty<Int32>(AssortmentIdProperty); }
            }

            public DeleteByAssortmentMinorRevisonIdCriteria(Int32 assortmentId)
            {
                LoadProperty<Int32>(AssortmentIdProperty, assortmentId);
            }
        }


        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="entityId">The id of the entity to create for</param>
        /// <returns>The new assortment</returns>
        public static AssortmentMinorRevision NewAssortmentMinorRevision(Int32 entityId)
        {
            AssortmentMinorRevision item = new AssortmentMinorRevision();
            item.Create(entityId);
            return item;
        }

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns></returns>
        public static AssortmentMinorRevision NewAssortmentMinorRevision(Int32 entityId, Guid UniqueContentReference)
        {
            AssortmentMinorRevision item = new AssortmentMinorRevision();
            item.Create(entityId, UniqueContentReference);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when this instance is being created
        /// </summary>
        private void Create(Int32 entityId)
        {
            LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty<Int32>(EntityIdProperty, entityId);
            LoadProperty<Guid>(UniqueContentReferenceProperty, Guid.NewGuid());
            LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            LoadProperty<AssortmentMinorRevisionAmendDistributionActionList>(AmendDistributionActionsProperty, AssortmentMinorRevisionAmendDistributionActionList.NewAssortmentMinorRevisionAmendDistributionActionList());
            LoadProperty<AssortmentMinorRevisionDeListActionList>(DeListActionsProperty, AssortmentMinorRevisionDeListActionList.NewAssortmentMinorRevisionDeListActionList());
            LoadProperty<AssortmentMinorRevisionListActionList>(ListActionsProperty, AssortmentMinorRevisionListActionList.NewAssortmentMinorRevisionListActionList());
            LoadProperty<AssortmentMinorRevisionReplaceActionList>(ReplaceActionsProperty, AssortmentMinorRevisionReplaceActionList.NewAssortmentMinorRevisionReplaceActionList());
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="uniqueContentReference"></param>
        private void Create(Int32 entityId, Guid uniqueContentReference)
        {
            LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty<Int32>(EntityIdProperty, entityId);
            LoadProperty<Guid>(UniqueContentReferenceProperty, uniqueContentReference);
            LoadProperty<AssortmentMinorRevisionAmendDistributionActionList>(AmendDistributionActionsProperty, AssortmentMinorRevisionAmendDistributionActionList.NewAssortmentMinorRevisionAmendDistributionActionList());
            LoadProperty<AssortmentMinorRevisionDeListActionList>(DeListActionsProperty, AssortmentMinorRevisionDeListActionList.NewAssortmentMinorRevisionDeListActionList());
            LoadProperty<AssortmentMinorRevisionListActionList>(ListActionsProperty, AssortmentMinorRevisionListActionList.NewAssortmentMinorRevisionListActionList());
            LoadProperty<AssortmentMinorRevisionReplaceActionList>(ReplaceActionsProperty, AssortmentMinorRevisionReplaceActionList.NewAssortmentMinorRevisionReplaceActionList());
            base.Create();
        }

        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Method to handle the removal of a minor revision action
        /// </summary>
        /// <param name="action"></param>
        public void RemoveAction(IAssortmentMinorRevisionAction action)
        {
            //Remove from specific action list
            switch (action.Type)
            {
                case (AssortmentMinorRevisionActionType.List):
                    AssortmentMinorRevisionListAction listAction = action as AssortmentMinorRevisionListAction;
                    if (listAction != null)
                    {
                        //Remove action
                        this.ListActions.Remove(listAction);
                    }
                    break;

                case (AssortmentMinorRevisionActionType.DeList):
                    AssortmentMinorRevisionDeListAction delistAction = action as AssortmentMinorRevisionDeListAction;
                    if (delistAction != null)
                    {
                        //Remove action
                        this.DeListActions.Remove(delistAction);
                    }
                    break;

                case (AssortmentMinorRevisionActionType.AmendDistribution):
                    AssortmentMinorRevisionAmendDistributionAction distAction = action as AssortmentMinorRevisionAmendDistributionAction;
                    if (distAction != null)
                    {
                        //Remove action
                        this.AmendDistributionActions.Remove(distAction);
                    }
                    break;
                case (AssortmentMinorRevisionActionType.Replace):
                    AssortmentMinorRevisionReplaceAction replaceAction = action as AssortmentMinorRevisionReplaceAction;
                    if (replaceAction != null)
                    {
                        //Remove action
                        this.ReplaceActions.Remove(replaceAction);
                    }
                    break;
            }
        }

        /// <summary>
        /// Gets a list of all product ids referenced by the minor revision
        /// </summary>
        /// <param name="minorRevision"></param>
        /// <returns></returns>
        public static List<Int32> GetReferencedProductIds(Model.AssortmentMinorRevision minorRevision)
        {
            List<Int32> productIds = new List<Int32>();

            //list actions
            foreach (var action in minorRevision.ListActions)
            {
                if (action.ProductId.HasValue)
                {
                    if (!productIds.Contains(action.ProductId.Value))
                    {
                        productIds.Add(action.ProductId.Value);
                    }
                }
            }

            //delist actions
            foreach (var action in minorRevision.DeListActions)
            {
                if (action.ProductId.HasValue)
                {
                    if (!productIds.Contains(action.ProductId.Value))
                    {
                        productIds.Add(action.ProductId.Value);
                    }
                }
            }

            //amend dist actions
            foreach (var action in minorRevision.AmendDistributionActions)
            {
                if (action.ProductId.HasValue)
                {
                    if (!productIds.Contains(action.ProductId.Value))
                    {
                        productIds.Add(action.ProductId.Value);
                    }
                }
            }

            //replace actions
            foreach (var action in minorRevision.ReplaceActions)
            {
                if (action.ProductId.HasValue)
                {
                    if (!productIds.Contains(action.ProductId.Value))
                    {
                        productIds.Add(action.ProductId.Value);
                    }
                }

                if (action.ReplacementProductId.HasValue)
                {
                    if (!productIds.Contains(action.ReplacementProductId.Value))
                    {
                        productIds.Add(action.ReplacementProductId.Value);
                    }
                }
            }

            return productIds;
        }

        #endregion

        #region Override

        public override string ToString()
        {
            return this.Name;
        }

        #endregion
    }
}
