﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630: A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A class representing a product universe within GFS
    /// </summary>
    [Serializable]
    public sealed partial class ProductUniverseProduct : ModelObject<ProductUniverseProduct>
    {
        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The product universe product id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> ProductIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductId);
        /// <summary>
        /// The product id
        /// </summary>
        public Int32 ProductId
        {
            get { return GetProperty<Int32>(ProductIdProperty); }
        }

        public static readonly ModelPropertyInfo<String> GtinProperty =
            RegisterModelProperty<String>(c => c.Gtin);
        /// <summary>
        /// The unique product code
        /// </summary>
        public String Gtin
        {
            get { return GetProperty<String>(GtinProperty); }
        }


        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The product universe product name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ProductUniverseProduct), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ProductUniverseCreate.ToString()));
            BusinessRules.AddRule(typeof(ProductUniverseProduct), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ProductUniverseGet.ToString()));
            BusinessRules.AddRule(typeof(ProductUniverseProduct), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ProductUniverseEdit.ToString()));
            BusinessRules.AddRule(typeof(ProductUniverseProduct), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ProductUniverseDelete.ToString()));
        }
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new MinValue<Int32>(ProductIdProperty, 1));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static ProductUniverseProduct NewProductUniverseProduct(Product product)
        {
            ProductUniverseProduct item = new ProductUniverseProduct();
            item.Create(product);
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static ProductUniverseProduct NewProductUniverseProduct(ProductInfo productInfo)
        {
            ProductUniverseProduct item = new ProductUniverseProduct();
            item.Create(productInfo);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create(Product product)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(GtinProperty, product.Gtin);
            this.LoadProperty<String>(NameProperty, product.Name);
            this.LoadProperty<Int32>(ProductIdProperty, product.Id);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="productInfo"></param>
        private void Create(ProductInfo productInfo)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(GtinProperty, productInfo.Gtin);
            this.LoadProperty<String>(NameProperty, productInfo.Name);
            this.LoadProperty<Int32>(ProductIdProperty, productInfo.Id);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.Gtin + ' ' + this.Name;
        }

        #endregion
    }
}
