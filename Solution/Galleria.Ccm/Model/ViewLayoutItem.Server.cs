﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using Csla;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class ViewLayoutItem
    {
        #region Constructor
        private ViewLayoutItem() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static ViewLayoutItem GetViewLayoutItem(IDalContext dalContext, ViewLayoutItemDto dto, IEnumerable<ViewLayoutItemDto> layoutItemDtos)
        {
            return DataPortal.FetchChild<ViewLayoutItem>(dalContext, dto, layoutItemDtos);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private ViewLayoutItemDto GetDataTransferObject()
        {
            ViewLayoutItem parentItem = this.ParentViewLayoutItem;
            ViewLayout parentViewLayout = this.ParentViewLayout;

            return new ViewLayoutItemDto()
            {
               Id = ReadProperty<Int32>(IdProperty),
               Order = ReadProperty<Byte>(OrderProperty),
               IsNewPane = ReadProperty<Boolean>(IsNewPaneProperty),
               IsVertical = ReadProperty<Boolean>(IsVerticalProperty),
               DocumentType = (Byte)ReadProperty<DocumentType>(DocumentTypeProperty),
               ParentViewLayoutItemId = (parentItem != null)? (Int32?)parentItem.Id : null,
               ViewLayoutId = parentViewLayout.Id
            };
        }

        /// <summary>
        /// Loads this object from the given dto
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, ViewLayoutItemDto dto,
            IEnumerable<ViewLayoutItemDto> layoutItemDtos)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Byte>(OrderProperty, dto.Order);
            LoadProperty<Boolean>(IsNewPaneProperty, dto.IsNewPane);
            LoadProperty<Boolean>(IsVerticalProperty, dto.IsVertical);
            LoadProperty<DocumentType>(DocumentTypeProperty, (DocumentType)dto.DocumentType);

            LoadProperty<ViewLayoutItemList>(ItemsProperty, ViewLayoutItemList.GetByViewLayoutItemId(dalContext, dto.Id, layoutItemDtos));
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when this instance is being loaded
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ViewLayoutItemDto dto, IEnumerable<ViewLayoutItemDto> layoutItemDtos)
        {
            LoadDataTransferObject(dalContext, dto, layoutItemDtos);
        }

        #endregion

        #region Insert

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext)
        {
            //TODO:
            ViewLayoutItemDto dto = GetDataTransferObject();
            MockDataHelper.InsertViewLayoutItem(dto);
            LoadProperty<Int32>(IdProperty, dto.Id);
            
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Update

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            //TODO:
            MockDataHelper.UpdateViewLayoutItem(GetDataTransferObject());


            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Delete

        /// <summary>
        /// Called when this instance is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            //TODO:
            MockDataHelper.DeleteViewLayoutItem(this.Id);
        }
        #endregion

        #endregion
    }
}
