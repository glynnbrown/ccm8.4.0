﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-31560 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Denotes the possible values for the 
    /// decrease type
    /// </summary>
    public enum ProductDecreaseType
    {
        ReduceUnitsTo = 0,
        ReduceFacingsBy = 1,
        ReduceFacingsTo = 2,
        ReduceToCasePackMultiplier = 3
    }

    /// <summary>
    /// Contains helpers for the ProductDecreaseType enum.
    /// </summary>
    public static class ProductDecreaseTypeHelper
    {
        public static readonly Dictionary<ProductDecreaseType, String> FriendlyNames =
            new Dictionary<ProductDecreaseType, String>()
            {
                {ProductDecreaseType.ReduceUnitsTo, Message.Enum_ProductDecreaseType_ReduceUnitsTo },
                {ProductDecreaseType.ReduceFacingsBy, Message.Enum_ProductDecreaseType_ReduceFacingsBy }, 
                {ProductDecreaseType.ReduceFacingsTo, Message.Enum_ProductDecreaseType_ReduceFacingsTo },
                {ProductDecreaseType.ReduceToCasePackMultiplier, Message.Enum_ProductDecreaseType_ReduceToCasePackMultiplier }, 
            };
    }
}