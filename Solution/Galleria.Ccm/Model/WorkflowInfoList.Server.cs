﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class WorkflowInfoList
    {
        #region Constructors
        public WorkflowInfoList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        #region FetchByEntityId
        /// <summary>
        /// Returns all workflows for the specified entity
        /// </summary>
        public static WorkflowInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<WorkflowInfoList>(new FetchByEntityIdCriteria(entityId));
        }

        /// <summary>
        /// Returns all workflows for the specified entity
        /// </summary>
        public static WorkflowInfoList FetchByEntityId(Entity entity)
        {
            return DataPortal.Fetch<WorkflowInfoList>(new FetchByEntityIdCriteria(entity.Id));
        }

        /// <summary>
        /// Returns all workflows for the specified entity
        /// </summary>
        public static WorkflowInfoList FetchByEntityId(EntityInfo entity)
        {
            return DataPortal.Fetch<WorkflowInfoList>(new FetchByEntityIdCriteria(entity.Id));
        }
        #endregion

        #region FetchByEntityIdIncludingDeleted
        /// <summary>
        /// Returns all workflows for the specified entity
        /// </summary>
        public static WorkflowInfoList FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            return DataPortal.Fetch<WorkflowInfoList>(new FetchByEntityIdIncludingDeletedCriteria(entityId));
        }

        /// <summary>
        /// Returns all workflows for the specified entity
        /// </summary>
        public static WorkflowInfoList FetchByEntityIdIncludingDeleted(Entity entity)
        {
            return DataPortal.Fetch<WorkflowInfoList>(new FetchByEntityIdIncludingDeletedCriteria(entity.Id));
        }

        /// <summary>
        /// Returns all workflows for the specified entity
        /// </summary>
        public static WorkflowInfoList FetchByEntityIdIncludingDeleted(EntityInfo entity)
        {
            return DataPortal.Fetch<WorkflowInfoList>(new FetchByEntityIdIncludingDeletedCriteria(entity.Id));
        }
        #endregion

        #endregion

        #region Data Access
        /// <summary>
        /// Called when returning all workflows for the specified entity
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IWorkflowInfoDal dal = dalContext.GetDal<IWorkflowInfoDal>())
                {
                    IEnumerable<WorkflowInfoDto> dtoList = dal.FetchByEntityId(criteria.EntityId);
                    foreach (WorkflowInfoDto dto in dtoList)
                    {
                        this.Add(WorkflowInfo.GetWorkflowInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returning all workflows for the specified entity
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdIncludingDeletedCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IWorkflowInfoDal dal = dalContext.GetDal<IWorkflowInfoDal>())
                {
                    IEnumerable<WorkflowInfoDto> dtoList = dal.FetchByEntityIdIncludingDeleted(criteria.EntityId);
                    foreach (WorkflowInfoDto dto in dtoList)
                    {
                        this.Add(WorkflowInfo.GetWorkflowInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }
        #endregion
    }
}
