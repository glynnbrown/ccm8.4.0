﻿using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class WorkpackagePlanogramProcessingStatus : ModelObject<WorkpackagePlanogramProcessingStatus>
    {
        #region Properties

        #region PlanogramId
        /// <summary>
        /// PlanogramId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanogramIdProperty =
            RegisterModelProperty<Int32>(c => c.PlanogramId);
        /// <summary>
        /// Returns the  planogram id
        /// </summary>
        public Int32 PlanogramId
        {
            get { return this.GetProperty<Int32>(PlanogramIdProperty); }
        }
        #endregion

        #region WorkpackageId
        /// <summary>
        /// WorkpackageId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> WorkpackageIdProperty =
            RegisterModelProperty<Int32>(c => c.WorkpackageId);
        /// <summary>
        /// Returns the work package id
        /// </summary>
        public Int32 WorkpackageId
        {
            get { return this.GetProperty<Int32>(WorkpackageIdProperty); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// Returns the row version
        /// </summary>
        public RowVersion RowVersion
        {
            get { return this.GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region AutomationStatus
        /// <summary>
        /// AutomationStatus property definition
        /// </summary>
        public static readonly ModelPropertyInfo<ProcessingStatus> AutomationStatusProperty =
            RegisterModelProperty<ProcessingStatus>(c => c.AutomationStatus);
        /// <summary>
        /// Gets or sets the processing status of the workpackage
        /// </summary>
        public ProcessingStatus AutomationStatus
        {
            get { return this.GetProperty<ProcessingStatus>(AutomationStatusProperty); }
            set { this.SetProperty<ProcessingStatus>(AutomationStatusProperty, value); }
        }
        #endregion

        #region AutomationStatusDescription
        /// <summary>
        /// Automation Status Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> AutomationStatusDescriptionProperty =
            RegisterModelProperty<String>(c => c.AutomationStatusDescription);
        /// <summary>
        /// Gets or sets the processing status description of the planogram
        /// </summary>
        public String AutomationStatusDescription
        {
            get { return this.GetProperty<String>(AutomationStatusDescriptionProperty); }
            set { this.SetProperty<String>(AutomationStatusDescriptionProperty, value); }
        }
        #endregion

        #region AutomationProgressMax
        /// <summary>
        /// AutomationProgressMax property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> AutomationProgressMaxProperty =
            RegisterModelProperty<Int32>(c => c.AutomationProgressMax);
        /// <summary>
        /// Gets or sets the Automation's max progress value
        /// </summary>
        public Int32 AutomationProgressMax
        {
            get { return this.ReadProperty<Int32>(AutomationProgressMaxProperty); }
            set { this.SetProperty<Int32>(AutomationProgressMaxProperty, value); }
        }
        #endregion

        #region AutomationProgressCurrent
        /// <summary>
        /// ProgressCurrent property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> AutomationProgressCurrentProperty =
            RegisterModelProperty<Int32>(c => c.AutomationProgressCurrent);
        /// <summary>
        /// Gets or sets the current Automation progress
        /// </summary>
        public Int32 AutomationProgressCurrent
        {
            get { return this.ReadProperty<Int32>(AutomationProgressCurrentProperty); }
            set { this.SetProperty<Int32>(AutomationProgressCurrentProperty, value); }
        }
        #endregion

        #region AutomationDateStarted
        /// <summary>
        /// StartDate property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> AutomationDateStartedProperty =
            RegisterModelProperty<DateTime?>(c => c.AutomationDateStarted);
        /// <summary>
        /// Gets or sets the date the current Automation process was started
        /// </summary>
        public DateTime? AutomationDateStarted
        {
            get { return this.ReadProperty<DateTime?>(AutomationDateStartedProperty); }
            set { this.SetProperty<DateTime?>(AutomationDateStartedProperty, value); }
        }
        #endregion

        #region AutomationDateLastUpdated
        /// <summary>
        /// Date Last Updated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> AutomationDateLastUpdatedProperty =
            RegisterModelProperty<DateTime?>(c => c.AutomationDateLastUpdated);
        /// <summary>
        /// Gets or sets the date the current Automation process was last updated
        /// </summary>
        public DateTime? AutomationDateLastUpdated
        {
            get { return this.ReadProperty<DateTime?>(AutomationDateLastUpdatedProperty); }
            set { this.SetProperty<DateTime?>(AutomationDateLastUpdatedProperty, value); }
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(WorkpackagePlanogramProcessingStatus), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramProcessingStatus), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramProcessingStatus), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramProcessingStatus), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Authenticated.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Increments the Automation progress for the specified planogram
        /// </summary>
        public static void IncrementAutomationProgress(Int32 planogramId, Int32 workpackageId, String statusDescription, DateTime dateLastUpdated)
        {
            DataPortal.Execute<AutomationIncrementCommand>(new AutomationIncrementCommand(planogramId, workpackageId, statusDescription, dateLastUpdated));
        }

        #endregion

        #region Commands

        #region Automation Increment Command
        /// <summary>
        /// Increments the automation progess for a planogram
        /// </summary>
        [Serializable]
        private partial class AutomationIncrementCommand : CommandBase<AutomationIncrementCommand>
        {
            #region Authorization Rules
            /// <summary>
            /// Defines the authorization rules for this type
            /// </summary>
            private static void AddObjectAuthorizationRules()
            {
                BusinessRules.AddRule(typeof(AutomationIncrementCommand), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(AutomationIncrementCommand), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(AutomationIncrementCommand), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
                BusinessRules.AddRule(typeof(AutomationIncrementCommand), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
            }
            #endregion

            #region Properties

            #region PlanogramId
            /// <summary>
            /// PlanogramId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> PlanogramIdProperty =
                RegisterProperty<Int32>(c => c.PlanogramId);
            /// <summary>
            /// Returns the planogram id
            /// </summary>
            public Int32 PlanogramId
            {
                get { return this.ReadProperty<Int32>(PlanogramIdProperty); }
            }
            #endregion

            #region WorkpackageId
            /// <summary>
            /// WorkpackageId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> WorkpackageIdProperty =
                RegisterProperty<Int32>(c => c.WorkpackageId);
            /// <summary>
            /// Returns the WorkpackageId 
            /// </summary>
            public Int32 WorkpackageId
            {
                get { return this.ReadProperty<Int32>(WorkpackageIdProperty); }
            }
            #endregion

            #region Status Description
            /// <summary>
            /// Status Description property definition
            /// </summary>
            public static readonly PropertyInfo<String> StatusDescriptionProperty =
                RegisterProperty<String>(c => c.StatusDescription);
            /// <summary>
            /// Returns the Status Description
            /// </summary>
            public String StatusDescription
            {
                get { return this.ReadProperty<String>(StatusDescriptionProperty); }
            }
            #endregion

            #region Date Last Updated
            /// <summary>
            /// Date Last Updated property definition
            /// </summary>
            public static readonly PropertyInfo<DateTime> DateLastUpdatedProperty =
                RegisterProperty<DateTime>(c => c.DateLastUpdated);
            /// <summary>
            /// Returns the Date Last Updated
            /// </summary>
            public DateTime DateLastUpdated
            {
                get { return this.ReadProperty<DateTime>(DateLastUpdatedProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public AutomationIncrementCommand(Int32 planogramId, Int32 workpackageId, String statusDescription, DateTime dateLastUpdated)
            {
                this.LoadProperty<Int32>(PlanogramIdProperty, planogramId);
                this.LoadProperty<Int32>(WorkpackageIdProperty, workpackageId);
                this.LoadProperty<String>(StatusDescriptionProperty, statusDescription);
                this.LoadProperty<DateTime>(DateLastUpdatedProperty, dateLastUpdated);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Criteria
        /// <summary>
        /// Criteria for the FetchByPlanogramIdWorkpackageId
        /// </summary>
        [Serializable]
        public class FetchByPlanogramIdWorkpackageIdCriteria : CriteriaBase<FetchByPlanogramIdWorkpackageIdCriteria>
        {
            #region Properties

            public static PropertyInfo<Int32> PlanogramIdProperty =
                RegisterProperty<Int32>(c => c.PlanogramId);
            public Int32 PlanogramId
            {
                get { return ReadProperty<Int32>(PlanogramIdProperty); }
            }

            public static PropertyInfo<Int32> WorkpackageIdProperty =
               RegisterProperty<Int32>(c => c.WorkpackageId);
            public Int32 WorkpackageId
            {
                get { return ReadProperty<Int32>(WorkpackageIdProperty); }
            }

            #endregion

            public FetchByPlanogramIdWorkpackageIdCriteria(Int32 planogramId, Int32 workpackageId)
            {
                LoadProperty<Int32>(PlanogramIdProperty, planogramId);
                LoadProperty<Int32>(WorkpackageIdProperty, workpackageId);
            }
        }

        #endregion
    }
}
