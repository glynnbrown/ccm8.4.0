﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 800)
// CCM-26520 :J.Pickup
//	Created
#endregion

#region Version History: (CCM 811)
// V8-30155 : L.Ineson
//  Added FetchByPlanogramIdsCriteria
#endregion

#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Csla;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class ContentLookupList : ModelList<ContentLookupList, ContentLookup>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ContentLookupList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(ContentLookupList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(ContentLookupList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(ContentLookupList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Authenticated.ToString()));
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for ContentLookupList.FetchByPlanogramIds
        /// </summary>
        [Serializable]
        public class FetchByPlanogramIdsCriteria : CriteriaBase<FetchByPlanogramIdsCriteria>
        {
            #region Properties

            /// <summary>
            /// PlanogramIds property definition
            /// </summary>
            public static PropertyInfo<IEnumerable<Int32>> PlanogramIdsProperty =
                RegisterProperty<IEnumerable<Int32>>(c => c.PlanogramIds);
            /// <summary>
            /// Gets the list of planogram ids to fetch for.
            /// </summary>
            public IEnumerable<Int32> PlanogramIds
            {
                get { return ReadProperty<IEnumerable<Int32>>(PlanogramIdsProperty); }
            }

            #endregion

            #region Constructor

            /// <summary>
            /// Creates a new instance of this type.
            /// </summary>
            /// <param name="planogramIds">the planogram ids to fetch for.</param>
            public FetchByPlanogramIdsCriteria(IEnumerable<Int32> planogramIds)
            {
                LoadProperty<IEnumerable<Int32>>(PlanogramIdsProperty, planogramIds);
            }

            #endregion
        }

        #endregion
    }
}
