﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a synchronisation source for
    /// the sychronisation service
    /// </summary>
    public partial class SyncSource
    {
        #region Constructors
        private SyncSource() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the specified sync source
        /// </summary>
        /// <param name="id">The sync source id</param>
        public static SyncSource GetSyncSourceById(Int32 id)
        {
            return DataPortal.Fetch<SyncSource>(new SingleCriteria<SyncSource, Int32>(id));
        }

        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        /// <returns>An existing sync target object</returns>
        internal static SyncSource GetSyncSource(IDalContext dalContext, SyncSourceDto dto)
        {
            return DataPortal.FetchChild<SyncSource>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Returns a dto for this object
        /// </summary>
        /// <returns>A dto created from this instance</returns>
        private SyncSourceDto GetDataTransferObject()
        {
            return new SyncSourceDto()
            {
                Id = this.ReadProperty<Int32>(IdProperty),
                RowVersion = this.ReadProperty<RowVersion>(RowVersionProperty),
                ServerName = this.ReadProperty<String>(ServerNameProperty),
                SiteName = this.ReadProperty<String>(SiteNameProperty),
                PortNumber = this.ReadProperty<Int32>(PortNumberProperty)
            };
        }

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, SyncSourceDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<String>(ServerNameProperty, dto.ServerName);
            this.LoadProperty<String>(SiteNameProperty, dto.SiteName);
            this.LoadProperty<Int32>(PortNumberProperty, dto.PortNumber);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when loading this instance
        /// from a data reader as part of a
        /// review list
        /// </summary>
        /// <param name="criteria">an object used to identify the desired review</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(SingleCriteria<SyncSource, Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory(Constants.SyncDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ISyncSourceDal dal = dalContext.GetDal<ISyncSourceDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchById(criteria.Value));
                }
            }
        }
        /// <summary>
        /// Called when loading this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, SyncSourceDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this object into the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory(Constants.SyncDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                SyncSourceDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (ISyncSourceDal dal = dalContext.GetDal<ISyncSourceDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                dalContext.RegisterId<SyncSource>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this object in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory(Constants.SyncDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                SyncSourceDto dto = GetDataTransferObject();
                using (ISyncSourceDal dal = dalContext.GetDal<ISyncSourceDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory(Constants.SyncDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ISyncSourceDal dal = dalContext.GetDal<ISyncSourceDal>())
                {
                    dalContext.Begin();
                    dal.DeleteById(ReadProperty<int>(IdProperty));
                    dalContext.Commit();
                }
            }
        }
        #endregion

        #endregion
    }
}