﻿#region Header Information 
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31699 : A.Heathcote
//  Created.

#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Data;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{   
    public partial class UserEditorSettingsRecentLabelList
    {
        #region Constructor
        private UserEditorSettingsRecentLabelList() { }  //Forces use of Factory methods
        #endregion

        #region Factory Methods

        internal static UserEditorSettingsRecentLabelList GetUserEditorSettingsRecentLabel(IDalContext dalContext)
        {
            return DataPortal.FetchChild<UserEditorSettingsRecentLabelList>(dalContext);
        }
        #endregion

        #region Data Access 

        #region Fetch
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dalContext">This is the current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext)
        {
            RaiseListChangedEvents = false;
            using (IUserEditorSettingsRecentLabelDal dal = dalContext.GetDal<IUserEditorSettingsRecentLabelDal>())
            {
                foreach (UserEditorSettingsRecentLabelDto dto in dal.FetchAll())
                {
                    this.Add(UserEditorSettingsRecentLabel.GetUserEditorSettingsRecentLabel(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
