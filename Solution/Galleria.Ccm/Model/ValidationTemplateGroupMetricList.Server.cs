﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created (Auto-generated)

#endregion

#region Version History: (CCM v8.20)
// V8-31533 : A.Probyn
//  Updated lazy loaded properties to not be lazy loaded as they were causing serialization exceptions
//  when used in the workflow client.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Csla;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public sealed partial class ValidationTemplateGroupMetricList
    {
        #region Constructor

        private ValidationTemplateGroupMetricList()
        {
        } // force use of factory methods

        #endregion

        #region Factory Methods
        
        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        internal static ValidationTemplateGroupMetricList FetchByParentId(IDalContext dalContext, Object parentId)
        {
            return DataPortal.FetchChild<ValidationTemplateGroupMetricList>(dalContext, new FetchByParentIdCriteria(null, parentId));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FetchByParentIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            using (IValidationTemplateGroupMetricDal dal = dalContext.GetDal<IValidationTemplateGroupMetricDal>())
            {
                IEnumerable<ValidationTemplateGroupMetricDto> dtoList = dal.FetchByValidationTemplateGroupId((Int32)criteria.ParentId);
                foreach (ValidationTemplateGroupMetricDto dto in dtoList)
                {
                    this.Add(ValidationTemplateGroupMetric.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}