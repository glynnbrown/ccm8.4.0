﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25436 L.Luong
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Readonly list holding HighlightInfo objects.
    /// </summary>
    [Serializable]
    public sealed partial class HighlightInfoList : ModelReadOnlyList<HighlightInfoList, HighlightInfo>
    {
        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Criteria

        #region FetchByPlanogramIdsCriteria

        /// <summary>
        /// Criteria for the Fetch factory method
        /// </summary>
        [Serializable]
        public class FetchByIdsCriteria : CriteriaBase<FetchByIdsCriteria>
        {
            #region Properties

            #region DalType
            /// <summary>
            /// DalType property definition
            /// </summary>
            public static readonly PropertyInfo<HighlightDalFactoryType> DalTypeProperty =
                RegisterProperty<HighlightDalFactoryType>(c => c.DalType);
            /// <summary>
            /// Returns the dal type
            /// </summary>
            public HighlightDalFactoryType DalType
            {
                get { return this.ReadProperty<HighlightDalFactoryType>(DalTypeProperty); }
            }
            #endregion

            #region Ids property

            /// <summary>
            /// Ids property definition
            /// </summary>
            public static readonly PropertyInfo<List<Object>> IdsProperty =
                RegisterProperty<List<Object>>(c => c.Ids);

            public List<Object> Ids
            {
                get { return this.ReadProperty<List<Object>>(IdsProperty); }
            }

            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdsCriteria(HighlightDalFactoryType sourceType, List<Object> ids)
            {
                this.LoadProperty<HighlightDalFactoryType>(DalTypeProperty, sourceType);
                this.LoadProperty<List<Object>>(IdsProperty, ids);
            }
            #endregion
        }

        #endregion

        #region FetchByEntityIdCriteria

        /// <summary>
        /// Criteria for the Fetch factory method
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : CriteriaBase<FetchByEntityIdCriteria>
        {
            #region Properties

            #region DalType
            /// <summary>
            /// DalType property definition
            /// </summary>
            public static readonly PropertyInfo<HighlightDalFactoryType> DalTypeProperty =
                RegisterProperty<HighlightDalFactoryType>(c => c.DalType);
            /// <summary>
            /// Returns the dal type
            /// </summary>
            public HighlightDalFactoryType DalType
            {
                get { return this.ReadProperty<HighlightDalFactoryType>(DalTypeProperty); }
            }
            #endregion

            #region EntityId
            /// <summary>
            /// EntityId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            /// <summary>
            /// Returns the Entity Id
            /// </summary>
            public Int32 EntityId
            {
                get { return this.ReadProperty<Int32>(EntityIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByEntityIdCriteria(Int32 entityId)
            {
                this.LoadProperty<HighlightDalFactoryType>(DalTypeProperty, HighlightDalFactoryType.Unknown);
                this.LoadProperty<Int32>(EntityIdProperty, entityId);
            }
            #endregion
        }

        #endregion

        #region FetchByEntityIdIncludingDeletedCriteria

        /// <summary>
        /// Criteria for the Fetch factory method
        /// </summary>
        [Serializable]
        public class FetchByEntityIdIncludingDeletedCriteria : CriteriaBase<FetchByEntityIdIncludingDeletedCriteria>
        {
            #region Properties

            #region DalType
            /// <summary>
            /// DalType property definition
            /// </summary>
            public static readonly PropertyInfo<HighlightDalFactoryType> DalTypeProperty =
                RegisterProperty<HighlightDalFactoryType>(c => c.DalType);
            /// <summary>
            /// Returns the dal type
            /// </summary>
            public HighlightDalFactoryType DalType
            {
                get { return this.ReadProperty<HighlightDalFactoryType>(DalTypeProperty); }
            }
            #endregion

            #region EntityId
            /// <summary>
            /// EntityId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            /// <summary>
            /// Returns the Entity Id
            /// </summary>
            public Int32 EntityId
            {
                get { return this.ReadProperty<Int32>(EntityIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByEntityIdIncludingDeletedCriteria(HighlightDalFactoryType sourceType, Int32 entityId)
            {
                this.LoadProperty<HighlightDalFactoryType>(DalTypeProperty, sourceType);
                this.LoadProperty<Int32>(EntityIdProperty, entityId);
            }
            #endregion
        }

        #endregion

        #endregion
    }
}
