﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26944 : A.Silva ~ Created.
// V8-27004 : A.Silva ~ Properly implemented.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Server-side implementation for <see cref="PlanogramValidationInfo" />.
    /// </summary>
    public partial class PlanogramValidationInfo
    {
        #region Constructor

        /// <summary>
        ///     Private constructor. Use factory methods to retrieve new instances.
        /// </summary>
        private PlanogramValidationInfo() { }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Gets a <see cref="PlanogramValidationInfo"/> from a given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dalContext">Not used.</param>
        /// <param name="dto">Source <see cref="PlanogramValidationInfoDto"/> to load in the new instance.</param>
        /// <param name="validationGroupInfoDtos">Collection of <see cref="PlanogramValidationGroupInfoDto"/> instances to get this instance's groups from.</param>
        /// <param name="validationMetricInfoDtos">Collection of <see cref="PlanogramValidationMetricInfoDto"/> instances to get this instance's group's metrics from.</param>
        /// <returns>A new instance of <see cref="PlanogramValidationInfo"/> loaded with the data in the given <paramref name="dto"/>.</returns>
        /// <remarks>CSLA will invoke <see cref="Child_Fetch(IDalContext,PlanogramValidationInfoDto,IEnumerable{PlanogramValidationGroupInfoDto},IEnumerable{PlanogramValidationMetricInfoDto})"/> on the server via reflection.</remarks>
        public static PlanogramValidationInfo GetPlanogramValidationInfo(IDalContext dalContext,
            PlanogramValidationInfoDto dto,
            IEnumerable<PlanogramValidationGroupInfoDto> validationGroupInfoDtos,
            IEnumerable<PlanogramValidationMetricInfoDto> validationMetricInfoDtos)
        {
            return DataPortal.FetchChild<PlanogramValidationInfo>(dalContext, dto, validationGroupInfoDtos,
                validationMetricInfoDtos);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Loads data from the given <paramref name="dto"/> into this instance.
        /// </summary>
        /// <param name="dalContext">Not used.</param>
        /// <param name="dto">The source <see cref="PlanogramValidationInfoDto"/>.</param>
        /// <param name="validationGroupInfoDtos">The collection of <see cref="PlanogramValidationGroupInfoDto"/> containing the groups for this instance.</param>
        /// <param name="validationMetricInfoDtos">The collection of <see cref="PlanogramValidationMetricInfoDto"/> containing the metrics for this instance's groups.</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramValidationInfoDto dto, IEnumerable<PlanogramValidationGroupInfoDto> validationGroupInfoDtos, IEnumerable<PlanogramValidationMetricInfoDto> validationMetricInfoDtos)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32>(PlanogramIdProperty, dto.PlanogramId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            var dtoGroupDtos = validationGroupInfoDtos.Where(o => Convert.ToInt32(o.PlanogramValidationId) == dto.Id);
            this.LoadProperty<PlanogramValidationGroupInfoList>(GroupsProperty, PlanogramValidationGroupInfoList.Fetch(dalContext, dtoGroupDtos, validationMetricInfoDtos));
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Invoked by CSLA via reflection when calling <see cref="GetPlanogramValidationInfo" />.
        /// </summary>
        /// <param name="dalContext">Not used.</param>
        /// <param name="dto">
        ///     Instance of <see cref="PlanogramValidationInfoDto" /> containing the data to initialize the new
        ///     instance.
        /// </param>
        /// <param name="validationGroupInfoDtos">The collection of <see cref="PlanogramValidationGroupInfoDto"/> containing the groups for this instance.</param>
        /// <param name="validationMetricInfoDtos">The collection of <see cref="PlanogramValidationMetricInfoDto"/> containing the metrics for this instance's groups.</param>
        private void Child_Fetch(IDalContext dalContext,
            PlanogramValidationInfoDto dto,
            IEnumerable<PlanogramValidationGroupInfoDto> validationGroupInfoDtos,
            IEnumerable<PlanogramValidationMetricInfoDto> validationMetricInfoDtos)
        {
            this.LoadDataTransferObject(dalContext, dto, validationGroupInfoDtos, validationMetricInfoDtos);
        }

        #endregion

        #endregion
    }
}