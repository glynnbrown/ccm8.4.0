﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// V8-29026 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.CodeAnalysis;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Csla;

namespace Galleria.Ccm.Model
{
    public partial class LocationSpaceProductGroupInfoList
    {
        #region Constructor
        private LocationSpaceProductGroupInfoList() { }//Force use of factory methods
        #endregion

        #region Factory Methods

        public static LocationSpaceProductGroupInfoList FetchByProductGroupId(Int32 productGroupId)
        {
            return DataPortal.Fetch<LocationSpaceProductGroupInfoList>(new FetchByProductGroupIdCriteria(productGroupId));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when fetching all objects with specific product group id
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByProductGroupIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationSpaceProductGroupInfoDal dal = dalContext.GetDal<ILocationSpaceProductGroupInfoDal>())
                {
                    IEnumerable<LocationSpaceProductGroupInfoDto> dtoList = dal.FetchByProductGroupId(criteria.ProductGroupId);
                    foreach (LocationSpaceProductGroupInfoDto dto in dtoList)
                    {
                        this.Add(LocationSpaceProductGroupInfo.GetLocationSpaceProductGroupInfo(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion
        #endregion
    }
}