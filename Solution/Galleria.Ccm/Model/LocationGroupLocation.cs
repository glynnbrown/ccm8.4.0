﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GFS 1.0)
// CCM-25445 : L.Ineson
//		Copied from GFS 
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// LocationGroupLocation
    /// (Child of LocationGroupLocationList)
    /// </summary>
    /// <remarks>
    /// This is essentially an editable info object so that locations can be 
    /// moved between locationgroups.
    /// No create method is provided as this should always be fetched.
    /// </remarks>
    [Serializable]
    public partial class LocationGroupLocation : ModelObject<LocationGroupLocation>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationGroupLocation), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationGroupLocation), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(LocationGroupLocation), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationGroupLocation), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationHierarchyEdit.ToString()));
        }
        #endregion

        #region Parent

        public LocationGroupLocationList ParentLocationGroupLocationList
        {
            get { return base.Parent as LocationGroupLocationList; }
        }

        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int16> LocationIdProperty =
            RegisterModelProperty<Int16>(c => c.LocationId);
        /// <summary>
        /// The id of the location this pertains to.
        /// </summary>
        public Int16 LocationId
        {
            get { return GetProperty<Int16>(LocationIdProperty); }
        }



        public static readonly ModelPropertyInfo<String> CodeProperty =
        RegisterModelProperty<String>(c => c.Code);
        /// <summary>
        /// The location code
        /// </summary>
        public String Code
        {
            get { return GetProperty<String>(CodeProperty); }
        }


        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The location name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        #endregion

        #region Overrides

        /// <summary>
        /// To String override
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return String.Format("{0} {1}", this.Code, this.Name);
        }

        #endregion
    }
}
