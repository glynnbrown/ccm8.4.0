﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26520 :J.Pickup
//	Created
#endregion

#region Version History: (CCM 811)
// V8-30155 : L.Ineson
//  Added FetchByPlanogramIds
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class ContentLookupList : ModelList<ContentLookupList, ContentLookup>
    {
        #region Constructor

        private ContentLookupList() { }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of items for the given entity id
        /// </summary>
        public static ContentLookupList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<ContentLookupList>(new SingleCriteria<Int32>(entityId));
        }

        /// <summary>
        /// Returns a list of items for the given planogram ids.
        /// </summary>
        public static ContentLookupList FetchByPlanogramIds(IEnumerable<Int32> planogramIds)
        {
            return DataPortal.Fetch<ContentLookupList>(new FetchByPlanogramIdsCriteria(planogramIds.ToList()));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when fetching by entity id
        /// </summary>
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IContentLookupDal dal = dalContext.GetDal<IContentLookupDal>())
                {
                    IEnumerable<ContentLookupDto> dtoList = dal.FetchByEntityId(criteria.Value);
                    foreach (ContentLookupDto dto in dtoList)
                    {
                        this.Add(ContentLookup.GetContentLookup(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when fetching by planogram ids.
        /// </summary>
        private void DataPortal_Fetch(FetchByPlanogramIdsCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IContentLookupDal dal = dalContext.GetDal<IContentLookupDal>())
                {
                    IEnumerable<ContentLookupDto> dtoList = dal.FetchByPlanogramIds(criteria.PlanogramIds);
                    foreach (ContentLookupDto dto in dtoList)
                    {
                        this.Add(ContentLookup.GetContentLookup(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #region  Update

        private void DataPortal_Update()
        {
            this.Child_Update();
        }

        #endregion

        #endregion
    }
}
