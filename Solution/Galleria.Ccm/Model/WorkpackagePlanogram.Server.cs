﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  Added UserName property
// V8-28232 : N.Foster
//  Added support for batch operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class WorkpackagePlanogram
    {
        #region Constructors
        private WorkpackagePlanogram() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a new instance from a dto
        /// </summary>
        internal static WorkpackagePlanogram GetWorkpackagePlanogram(IDalContext dalContext, WorkpackagePlanogramDto dto, Int32 workflowId)
        {
            return DataPortal.FetchChild<WorkpackagePlanogram>(dalContext, dto, workflowId);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, WorkpackagePlanogramDto dto, Int32 workflowId)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32>(WorkflowIdProperty, workflowId);
            this.LoadProperty<Int32?>(SourcePlanogramIdProperty, dto.SourcePlanogramId);
            this.LoadProperty<Int32>(DestinationPlanogramIdProperty, dto.DestinationPlanogramId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(UserNameProperty, dto.UserName);
            this.LoadProperty<ProcessingStatus>(ProcessingStatusProperty, (ProcessingStatus)dto.ProcessingStatus);
        }

        /// <summary>
        /// Gets a data transfer object from this instance
        /// </summary>
        private WorkpackagePlanogramDto GetDataTransferObject(Workpackage parent)
        {
            return new WorkpackagePlanogramDto()
            {
                Id = this.ReadProperty<Int32>(IdProperty),
                WorkpackageId = parent.Id,
                SourcePlanogramId = this.ReadProperty<Int32?>(SourcePlanogramIdProperty),
                DestinationPlanogramId = this.ReadProperty<Int32>(DestinationPlanogramIdProperty),
                ProcessingStatus = (Byte)this.ReadProperty<ProcessingStatus>(ProcessingStatusProperty),
                Name = this.ReadProperty<String>(NameProperty),
                UserName = this.ReadProperty<String>(UserNameProperty)
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when loading this instance from a dto
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, WorkpackagePlanogramDto dto, Int32 workflowId)
        {
            this.LoadDataTransferObject(dalContext, dto, workflowId);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, Workpackage parent)
        {
            Int32 oldId = 0;
            batchContext.Insert<WorkpackagePlanogramDto>(
            (dc) =>
            {
                WorkpackagePlanogramDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                dc.RegisterId<WorkpackagePlanogram>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, Workpackage parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<WorkpackagePlanogramDto>(this.GetDataTransferObject(parent));
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this child is being deleted
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, Workpackage parent)
        {
            batchContext.Delete<WorkpackagePlanogramDto>(this.GetDataTransferObject(parent));
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #endregion
    }
}
