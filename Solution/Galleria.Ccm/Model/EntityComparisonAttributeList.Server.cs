using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{

    public sealed partial class EntityComparisonAttributeList
    {
        #region Constructor

        /// <summary>
        ///     Private constructor to enforce use of factory methods.
        /// </summary>
        private EntityComparisonAttributeList() { }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        internal static EntityComparisonAttributeList FetchByParentId(IDalContext dalContext, Object parentId)
        {
            var items =  DataPortal.FetchChild<EntityComparisonAttributeList>(dalContext, new FetchByParentIdCriteria(null, parentId));
            return items;
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        ///     Called by reflection when fetching an instance of <see cref="EntityComparisonAttributeList" />.
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, FetchByParentIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            using (var dal = dalContext.GetDal<IEntityComparisonAttributeDal>())
            {
                IEnumerable<EntityComparisonAttributeDto> dtoList = dal.FetchByEntityId(criteria.ParentId);
                foreach (EntityComparisonAttributeDto dto in dtoList)
                {
                    Add(EntityComparisonAttribute.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
            MarkAsChild();
        }

        #endregion

        #endregion
    }
}