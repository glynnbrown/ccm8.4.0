﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Model
{
    public enum HighlightQuadrantType
    {
        Mean,
        Median,
        MidRange,
        Constant
    }

    public static class HighlightQuadrantTypeHelper
    {
        public static readonly Dictionary<HighlightQuadrantType, String> FriendlyNames =
            new Dictionary<HighlightQuadrantType, String>()
            {
                {HighlightQuadrantType.Mean, Message.HighlightQuadrantType_Mean },
                {HighlightQuadrantType.Median, Message.HighlightQuadrantType_Median },
                {HighlightQuadrantType.MidRange, Message.HighlightQuadrantType_MidRange },
                {HighlightQuadrantType.Constant, Message.HighlightQuadrantType_Constant },
            };

        public static PlanogramHighlightQuadrantType ToPlanogramHighlightQuadrantType(this HighlightQuadrantType value)
        {
            switch (value)
            {
                default:
                case HighlightQuadrantType.Mean: return PlanogramHighlightQuadrantType.Mean;
                case HighlightQuadrantType.Constant: return PlanogramHighlightQuadrantType.Constant;
                case HighlightQuadrantType.Median: return PlanogramHighlightQuadrantType.Median;
                case HighlightQuadrantType.MidRange: return PlanogramHighlightQuadrantType.MidRange;
            }
        }
    }
}
