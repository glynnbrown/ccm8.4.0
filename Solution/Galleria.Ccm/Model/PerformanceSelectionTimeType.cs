﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26159 : L.Ineson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    public enum PerformanceSelectionTimeType
    {
        Day = 0,
        Week = 1,
        Month = 2,
        Quarter = 3,
        Year = 4
    }

    public static class PerformanceSelectionTimeTypeHelper
    {
        public static readonly Dictionary<PerformanceSelectionTimeType, String> FriendlyNames =
            new Dictionary<PerformanceSelectionTimeType, String>()
            {
                {PerformanceSelectionTimeType.Day, Message.Enum_PerformanceSelectionTimeType_Day},
                {PerformanceSelectionTimeType.Week, Message.Enum_PerformanceSelectionTimeType_Week},
                {PerformanceSelectionTimeType.Month, Message.Enum_PerformanceSelectionTimeType_Month},
                {PerformanceSelectionTimeType.Quarter, Message.Enum_PerformanceSelectionTimeType_Quarter},
                {PerformanceSelectionTimeType.Year, Message.Enum_PerformanceSelectionTimeType_Year}
            };

        public static readonly Dictionary<PerformanceSelectionTimeType, String> FriendlyPlurals =
            new Dictionary<PerformanceSelectionTimeType, String>()
            {
                {PerformanceSelectionTimeType.Day, Message.Enum_PerformanceSelectionTimeType_Days},
                {PerformanceSelectionTimeType.Week, Message.Enum_PerformanceSelectionTimeType_Weeks},
                {PerformanceSelectionTimeType.Month, Message.Enum_PerformanceSelectionTimeType_Months},
                {PerformanceSelectionTimeType.Quarter, Message.Enum_PerformanceSelectionTimeType_Quarters},
                {PerformanceSelectionTimeType.Year, Message.Enum_PerformanceSelectionTimeType_Years}
            };
    }
}
