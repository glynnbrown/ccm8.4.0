﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27155 : L.Luong
//  Created
#endregion
#region Version History: CCM802
// V8-28766 : J.Pickup
//  Switched single to 'Manual' for user clarity.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Denotes the available product placement Z types
    /// </summary>
    public enum ProductPlacementZType
    {
        Manual = 0,
        FillDeep = 1
    }

    public static class ProductPlacementZTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<ProductPlacementZType, String> FriendlyNames =
            new Dictionary<ProductPlacementZType, String>()
            {
                {ProductPlacementZType.Manual, Message.Enum_ProductPlacementZType_Manual},
                {ProductPlacementZType.FillDeep, Message.Enum_ProductPlacementZType_FillDeep},
            };
    }
}
