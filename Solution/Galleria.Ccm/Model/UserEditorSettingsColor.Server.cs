﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30791 : D.Pleasance
//	Created
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A user color
    /// </summary>
    public partial class UserEditorSettingsColor
    {
        #region Constructor
        private UserEditorSettingsColor() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a user color
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        /// <returns>The user color</returns>
        internal static UserEditorSettingsColor GetUserEditorSettingsColor(IDalContext dalContext, UserEditorSettingColorDto dto)
        {
            return DataPortal.FetchChild<UserEditorSettingsColor>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private UserEditorSettingColorDto GetDataTransferObject()
        {
            return new UserEditorSettingColorDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                Color = ReadProperty<Int32>(ColorProperty)
            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, UserEditorSettingColorDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Int32>(ColorProperty, dto.Color);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when return FetchById
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, UserEditorSettingColorDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext)
        {
            UserEditorSettingColorDto dto = GetDataTransferObject();
            Int32 oldId = dto.Id;
            using (IUserEditorSettingColorDal dal = dalContext.GetDal<IUserEditorSettingColorDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<UserEditorSettingsColor>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this object in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            if (this.IsSelfDirty)
            {
                using (IUserEditorSettingColorDal dal = dalContext.GetDal<IUserEditorSettingColorDal>())
                {
                    dal.Update(this.GetDataTransferObject());
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IUserEditorSettingColorDal dal = dalContext.GetDal<IUserEditorSettingColorDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion
    }
}
