﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion


using System;
using System.Linq;
using System.Collections.Generic;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// PrintTemplateSectionGroup Model object
    /// </summary>
    [Serializable]
    public sealed partial class PrintTemplateSectionGroup : ModelObject<PrintTemplateSectionGroup>
    {
        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PrintTemplate Parent
        {
            get
            {
                PrintTemplateSectionGroupList parentList = base.Parent as PrintTemplateSectionGroupList;
                if (parentList != null)
                {
                    return parentList.Parent as PrintTemplate;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region Number

        /// <summary>
        /// Number property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> NumberProperty =
            RegisterModelProperty<Byte>(c => c.Number);

        /// <summary>
        /// Gets/Sets the Number value
        /// </summary>
        public Byte Number
        {
            get { return GetProperty<Byte>(NumberProperty); }
            set { SetProperty<Byte>(NumberProperty, value); }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region BaysPerPage

        /// <summary>
        /// BaysPerPage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> BaysPerPageProperty =
            RegisterModelProperty<Byte>(c => c.BaysPerPage);

        /// <summary>
        /// Gets/Sets the BaysPerPage value
        /// </summary>
        public Byte BaysPerPage
        {
            get { return GetProperty<Byte>(BaysPerPageProperty); }
            set { SetProperty<Byte>(BaysPerPageProperty, value); }
        }

        #endregion

        #region Sections
        /// <summary>
        /// Sections property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PrintTemplateSectionList> SectionsProperty =
           RegisterModelProperty<PrintTemplateSectionList>(c => c.Sections);
        /// <summary>
        /// The sections of the print option section group
        /// </summary>
        public PrintTemplateSectionList Sections
        {
            get { return GetProperty<PrintTemplateSectionList>(SectionsProperty); }
        }
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods

        /// <summary>
        /// TEMP
        /// </summary>
        /// <returns></returns>
        public static PrintTemplateSectionGroup NewEmpty()
        {
            PrintTemplateSectionGroup item = new PrintTemplateSectionGroup();
            item.Create(1);
            return item;
        }

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static PrintTemplateSectionGroup NewPrintTemplateSectionGroup(Byte number)
        {
            PrintTemplateSectionGroup item = new PrintTemplateSectionGroup();
            item.Create(number);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(Byte number)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());

            this.LoadProperty<PrintTemplateSectionList>(SectionsProperty,
                PrintTemplateSectionList.NewPrintTemplateSectionList());

            this.LoadProperty<Byte>(NumberProperty, number);
            this.LoadProperty<Byte>(BaysPerPageProperty, 0);

            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<PrintTemplateSectionGroup>(oldId, newId);
        }

        /// <summary>
        /// Moves section from this section group to another
        /// </summary>
        public void MoveSectionToNewGroup(PrintTemplateSectionGroup newGroup, PrintTemplateSection selectionToMove, Byte targetNumber)
        {
            //loop through all the sections in this group and decrease he section number by 1 if more than moved section
            foreach (PrintTemplateSection section in this.Sections)
            {
                // if section group number is equal to or more than the removed section then decrease
                if (section.Number > selectionToMove.Number)
                {
                    section.Number--;
                }
            }

            //loop through all the sections within the new group
            foreach (PrintTemplateSection section in newGroup.Sections)
            {
                //if the section is equal to or greater than the target then increase it by 1
                if (section.Number >= targetNumber)
                {
                    section.Number = (Byte)(section.Number + 1);
                }
            }

            //finally set the section number of the moved section
            selectionToMove.Number = targetNumber;

            PrintTemplateSection.MovePrintTemplateSection(selectionToMove, newGroup);
        }

        /// <summary>
        /// Moves all sections from this section group to another
        /// </summary>
        public void MoveAllSectionsToNewGroup(PrintTemplateSectionGroup newGroup)
        {
            List<PrintTemplateSection> movedSections = new List<PrintTemplateSection>(this.Sections.OrderBy(p => p.Number));

            foreach (PrintTemplateSection section in movedSections)
            {
                PrintTemplateSection.MovePrintTemplateSection(section, newGroup);
            }
        }

        /// <summary>
        /// Moves a section within it's section group
        /// </summary>
        public void MoveSectionWithinGroup(PrintTemplateSection movedSection, Byte targetSectionNumber)
        {
            //loop through all the sections within the group
            foreach (PrintTemplateSection section in this.Sections)
            {
                //check to see if the section being moved is going up or down the list
                if (movedSection.Number > targetSectionNumber)
                {
                    //if the section number is less than the one being moved and greater than or equal to the target then increase the sections number
                    if (section.Number < movedSection.Number && section.Number >= targetSectionNumber)
                    {
                        section.Number = (Byte)(section.Number + 1);
                    }
                }
                else
                {
                    //if the section number is greater than the one being moved and less than or equal to the target then decrease the sections number
                    if (section.Number > movedSection.Number && section.Number <= targetSectionNumber)
                    {
                        section.Number = (Byte)(section.Number - 1);
                    }
                }
            }

            //finally set the section number of the moved section
            movedSection.Number = targetSectionNumber;

            PrintTemplateSection.MovePrintTemplateSection(movedSection, this);
        }

        #endregion
    }
}