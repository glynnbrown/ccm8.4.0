﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class SyncTargetGfsPerformanceList
    {
        #region Constructor
        private SyncTargetGfsPerformanceList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a list of performance sources for the sync target
        /// </summary>
        /// <returns>A list of performance source metrics</returns>
        public static SyncTargetGfsPerformanceList FetchBySyncTargetIdAsChild(IDalContext dalContext, int performanceSourceId)
        {
            return DataPortal.FetchChild<SyncTargetGfsPerformanceList>(dalContext, performanceSourceId);
        }

        /// <summary>
        /// Returns a list of performance sources for the sync target
        /// </summary>
        /// <returns></returns>
        public static SyncTargetGfsPerformanceList FetchBySyncTargetId(Int32 performanceSourceId)
        {
            return DataPortal.Fetch<SyncTargetGfsPerformanceList>(new SingleCriteria<SyncTargetGfsPerformanceList, Int32>(performanceSourceId));
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when retrieving a list of sync target performance sources
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(SingleCriteria<SyncTargetGfsPerformanceList, Int32> criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory(Constants.SyncDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ISyncTargetGfsPerformanceDal dal = dalContext.GetDal<ISyncTargetGfsPerformanceDal>())
                {
                    IEnumerable<SyncTargetGfsPerformanceDto> dtoList = dal.FetchBySyncTargetId(criteria.Value);
                    foreach (SyncTargetGfsPerformanceDto dto in dtoList)
                    {
                        this.Add(SyncTargetGfsPerformance.FetchSyncTargetGfsPerformanceAsChild(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parentNodeId">The parent sync target ID</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, int syncTargetId)
        {
            RaiseListChangedEvents = false;
            using (ISyncTargetGfsPerformanceDal dal = dalContext.GetDal<ISyncTargetGfsPerformanceDal>())
            {
                IEnumerable<SyncTargetGfsPerformanceDto> dtoList = dal.FetchBySyncTargetId(syncTargetId);
                foreach (SyncTargetGfsPerformanceDto dto in dtoList)
                {
                    this.Add(SyncTargetGfsPerformance.FetchSyncTargetGfsPerformanceAsChild(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }
        #endregion

        #endregion

    }
}
