﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
// GFS-25455 : J.Pickup
//  Added fetch by entityIdAssortmentIds
// CCM-26140 :I.George
//  Added FetchByEntityIdIncludingDeleted
// CCM-26520 :J.Pickup
//  Added FetchByProductGroupId
#endregion
#region Version History: (CCM 8.0.3)
// GFS-29134 : D.Pleasance
//  removed FetchByEntityIdIncludingDeletedCriteria as not required since we now delete outright
#endregion

#region Version History: (CCM 8.2.0)
// V8-30461 : M.Shelley
//  Added Entity_Id to FetchByProductGroupId
#endregion

#endregion


using System;
using System.Collections.Generic;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Linq.Expressions;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// AssortmentInfoList
    /// </summary>
    public partial class AssortmentInfoList
    {
        #region Constructors
        private AssortmentInfoList() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns all items for the given entity id
        /// </summary>
        /// <returns>A list of entity infos</returns>
        public static AssortmentInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<AssortmentInfoList>(new FetchByEntityIdCriteria(entityId));
        }
        
        /// <summary>
        /// Returns all items for the given entity id and change date
        /// </summary>
        /// <returns>A list of entity infos</returns>
        public static AssortmentInfoList FetchByEntityIdChangeDate(Int32 entityId, DateTime changeDate)
        {
            return DataPortal.Fetch<AssortmentInfoList>(new FetchByEntityIdChangeDateCriteria(entityId, changeDate));
        }

        /// <summary>
        /// Returns all items for the given entity id and optional date deleted
        /// </summary>
        /// <returns>A list of entity infos</returns>
        public static AssortmentInfoList FetchDeletedByEntityId(Int32 entityId, DateTime? deletedDate)
        {
            return DataPortal.Fetch<AssortmentInfoList>(new FetchDeletedByEntityIdCriteria(entityId, deletedDate));
        }

        /// <summary>
        /// Returns all items for the given entity id and search criteria
        /// </summary>
        /// <returns>A list of entity infos</returns>
        public static AssortmentInfoList FetchByEntityIdAssortmentSearchCriteria(Int32 entityId, String assortmentSearchCriteria)
        {
            return DataPortal.Fetch<AssortmentInfoList>(new FetchAssortmentByEntityIdAssortmentSearchCriteria(entityId, assortmentSearchCriteria));
        }

        /// <summary>
        /// Returns all specified assortments by their ids (entity specific)
        /// </summary>
        public static AssortmentInfoList FetchByEntityIdAssortmentIds(Int32 entityId, IEnumerable<Int32> assortmentIds)
        {
            return DataPortal.Fetch<AssortmentInfoList>(new FetchByEntityIdAssortmentIdsCriteria(entityId, assortmentIds));
        }


        /// <summary>
        /// Returns all items for the given product group id
        /// </summary>
        /// <returns>A list of product group infos</returns>
        public static AssortmentInfoList FetchByProductGroupId(Int32 entityId, Int32 productGroupId)
        {
            return DataPortal.Fetch<AssortmentInfoList>(new FetchByProductGroupIdCriteria(entityId, productGroupId));
        }
        #endregion

        #region Data Access

        private void Fetch(Expression<Func<IAssortmentInfoDal, IEnumerable<AssortmentInfoDto>>> expression)
        {
            RaiseListChangedEvents = false;
            IsReadOnly = false;
            try
            {
                using (IDalContext dalContext = DalContainer.GetDalFactory().CreateContext())
                using (var dal = dalContext.GetDal<IAssortmentInfoDal>())
                {
                    IEnumerable<AssortmentInfoDto> dtos = expression.Compile()(dal);
                    foreach (AssortmentInfoDto dto in dtos)
                    {
                        Add(AssortmentInfo.GetAssortmentInfo(dalContext, dto));
                    }
                }
            }
            catch
            {
                // There is no connection or it was not set up.
            }
            IsReadOnly = true;
            RaiseListChangedEvents = true;
        }
        /// <summary>
        /// Called when retrieving a list of all entity infos
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            Fetch(dal => dal.FetchByEntityId(criteria.EntityId));
        }


        /// <summary>
        /// Called when retrieving a list of all infos by product group
        /// </summary>
        private void DataPortal_Fetch(FetchByProductGroupIdCriteria criteria)
        {
            Fetch(dal => dal.FetchByProductGroupId(criteria.EntityId, criteria.ProductGroupId));
        }

        /// <summary>
        /// Called when retrieving a list of assortments wit search critiera
        /// </summary>
        private void DataPortal_Fetch(FetchAssortmentByEntityIdAssortmentSearchCriteria criteria)
        {
            Fetch(dal => dal.FetchByEntityIdAssortmentSearchCriteria(criteria.EntityId, criteria.AssortmentSearchCriteria));
        }

        /// <summary>
        /// Called when fetching products by a list of product ids
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdAssortmentIdsCriteria criteria)
        {
            Fetch(dal => dal.FetchByEntityIdAssortmentIds(criteria.EntityId, criteria.AssortmentIds));
        }        
        #endregion
    }
}