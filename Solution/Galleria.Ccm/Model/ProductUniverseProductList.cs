﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630: A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A list of products contained within a product universe
    /// </summary>
    [Serializable]
    public sealed partial class ProductUniverseProductList : ModelList<ProductUniverseProductList,ProductUniverseProduct>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ProductUniverseProductList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ProductUniverseCreate.ToString()));
            BusinessRules.AddRule(typeof(ProductUniverseProductList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ProductUniverseGet.ToString()));
            BusinessRules.AddRule(typeof(ProductUniverseProductList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ProductUniverseEdit.ToString()));
            BusinessRules.AddRule(typeof(ProductUniverseProductList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ProductUniverseDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        internal static ProductUniverseProductList NewProductUniverseProductList()
        {
            ProductUniverseProductList item = new ProductUniverseProductList();
            item.Create();
            return item;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds a product to this product universe
        /// </summary>
        /// <param name="product">The product to add</param>
        public void Add(Product product)
        {
            this.Add(ProductUniverseProduct.NewProductUniverseProduct(product));
        }

        /// <summary>
        /// Adds a product to this product universe
        /// </summary>
        /// <param name="productInfo">The product to add</param>
        public ProductUniverseProduct Add(ProductInfo productInfo)
        {
            ProductUniverseProduct addedItem = ProductUniverseProduct.NewProductUniverseProduct(productInfo);
            this.Add(addedItem);
            return addedItem;
        }

        /// <summary>
        /// Adds a list of ProductInfos to this product universe
        /// </summary>
        /// <param name="range"></param>
        public void AddList(IEnumerable<ProductInfo> range)
        {
            this.RaiseListChangedEvents = false;

            //cycle through adding all the items
            List<ProductUniverseProduct> addedItems = new List<ProductUniverseProduct>();
            foreach (ProductInfo element in range)
            {
                addedItems.Add(this.Add(element));
            }

            this.RaiseListChangedEvents = true;

            //raise out a bulk change notification
            NotifyBulkChange(new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, addedItems));
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private new void Create()
        {
            MarkAsChild();
        }
        #endregion

        #endregion
    }
}
