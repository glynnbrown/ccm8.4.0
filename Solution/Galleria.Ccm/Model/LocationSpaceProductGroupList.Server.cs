﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Csla;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class LocationSpaceProductGroupList
    {
        #region Constructor
        private LocationSpaceProductGroupList() { } // Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a list of existing items based on their parent location space
        /// </summary>
        /// <returns>A list of existing it</returns>
        internal static LocationSpaceProductGroupList FetchByLocationSpaceId(IDalContext dalContext, Int32 locationSpaceId)
        {
            return DataPortal.FetchChild<LocationSpaceProductGroupList>(dalContext, locationSpaceId);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="productUniverseId">The parent location space id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 locationSpaceId)
        {
            RaiseListChangedEvents = false;
            using (ILocationSpaceProductGroupDal dal = dalContext.GetDal<ILocationSpaceProductGroupDal>())
            {
                IEnumerable<LocationSpaceProductGroupDto> dtoList = dal.FetchByLocationSpaceId(locationSpaceId);
                foreach (LocationSpaceProductGroupDto dto in dtoList)
                {
                    this.Add(LocationSpaceProductGroup.GetLocationSpaceProductGroup(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
