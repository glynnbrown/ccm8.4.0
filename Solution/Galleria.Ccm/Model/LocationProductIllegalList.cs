﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25447 : N.Haywood
//  Copied over from GFS
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a list containing LocationProductIllegal objects
    /// </summary>
    [Serializable]
    public partial class LocationProductIllegalList : ModelList<LocationProductIllegalList, LocationProductIllegal>
    {
        #region Authorisation Rules
        /// <summary>
        /// defines authorisation rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationProductIllegalList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationProductIllegalCreate.ToString()));
            BusinessRules.AddRule(typeof(LocationProductIllegalList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.LocationProductIllegalGet.ToString()));
            BusinessRules.AddRule(typeof(LocationProductIllegalList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationProductIllegalEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationProductIllegalList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationProductIllegalDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>a new list</returns>
        public static LocationProductIllegalList NewList()
        {
            LocationProductIllegalList list = new LocationProductIllegalList();
            list.Create();
            return list;
        }

        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByEntityId
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : Csla.CriteriaBase<FetchByEntityIdCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
            RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
            }
        }

        #endregion
    }
}
