﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629 : A.Kuszyk
//		Created (copied from SA).
// V8-27927 : I.George
//  Added OnCopy method and the Id to the Create method
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class ClusterLocation : ModelObject<ClusterLocation>
    {
        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public Cluster ParentCluster
        {
            get
            {
                ClusterLocationList parentList = base.Parent as ClusterLocationList;
                if (parentList != null)
                {
                    return parentList.Parent as Cluster;
                }
                return null;
            }
        }

        #endregion

        #region Authorisation Rules
        /// <summary>
        /// Defines the authorisation rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ClusterLocation), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ClusterSchemeCreate.ToString()));
            BusinessRules.AddRule(typeof(ClusterLocation), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ClusterSchemeGet.ToString()));
            BusinessRules.AddRule(typeof(ClusterLocation), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ClusterSchemeEdit.ToString()));
            BusinessRules.AddRule(typeof(ClusterLocation), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ClusterSchemeDelete.ToString()));
        }

        #endregion

        #region Properties

        /// <summary>
        /// The unqiue cluster location id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// The location id
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> LocationIdProperty =
            RegisterModelProperty<Int16>(c => c.LocationId);
        public Int16 LocationId
        {
            get { return GetProperty<Int16>(LocationIdProperty); }
        }

        /// <summary>
        /// The location address 1
        /// </summary>
        public static readonly ModelPropertyInfo<String> Address1Property =
            RegisterModelProperty<String>(c => c.Address1);
        public String Address1
        {
            get { return GetProperty<String>(Address1Property); }
        }

        /// <summary>
        /// The location address 2
        /// </summary>
        public static readonly ModelPropertyInfo<String> Address2Property =
            RegisterModelProperty<String>(c => c.Address2);
        public String Address2
        {
            get { return GetProperty<String>(Address2Property); }
        }

        /// <summary>
        /// Returns the city
        /// </summary>
        public static readonly ModelPropertyInfo<String> CityProperty =
            RegisterModelProperty<String>(c => c.City);
        public String City
        {
            get { return GetProperty<String>(CityProperty); }
        }

        /// <summary>
        /// The location code
        /// </summary>
        public static readonly ModelPropertyInfo<String> CodeProperty =
            RegisterModelProperty<String>(c => c.Code);
        public String Code
        {
            get { return GetProperty<String>(CodeProperty); }
        }

        /// <summary>
        /// Returns the Country
        /// </summary>
        public static readonly ModelPropertyInfo<String> CountryProperty =
            RegisterModelProperty<String>(c => c.Country);
        public String Country
        {
            get { return GetProperty<String>(CountryProperty); }
        }

        /// <summary>
        /// The location county
        /// </summary>
        public static readonly ModelPropertyInfo<String> CountyProperty =
            RegisterModelProperty<String>(c => c.County);
        public String County
        {
            get { return GetProperty<String>(CountyProperty); }
        }

        /// <summary>
        /// The location name
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        /// <summary>
        /// The location postal code
        /// </summary>
        public static readonly ModelPropertyInfo<String> PostalCodeProperty =
            RegisterModelProperty<String>(c => c.PostalCode);
        public String PostalCode
        {
            get { return GetProperty<String>(PostalCodeProperty); }
        }

        /// <summary>
        /// The location region
        /// </summary>
        public static readonly ModelPropertyInfo<String> RegionProperty =
            RegisterModelProperty<String>(c => c.Region);
        public String Region
        {
            get { return GetProperty<String>(RegionProperty); }
        }


        /// <summary>
        /// The store tv region
        /// </summary>
        public static readonly ModelPropertyInfo<String> TVRegionProperty =
            RegisterModelProperty<String>(c => c.TVRegion);
        public String TVRegion
        {
            get { return GetProperty<String>(TVRegionProperty); }
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new MinValue<Int16>(LocationIdProperty, 1));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a new peer group store
        /// </summary>
        /// <returns>A new peer group store</returns>
        public static ClusterLocation NewClusterLocation(LocationInfo location)
        {
            ClusterLocation item = new ClusterLocation();
            item.Create(location);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(LocationInfo location)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty<Int16>(LocationIdProperty, location.Id);
            LoadProperty<String>(NameProperty, location.Name);
            LoadProperty<String>(CodeProperty, location.Code);
            LoadProperty<String>(Address1Property, location.Address1);
            LoadProperty<String>(Address2Property, location.Address2);
            LoadProperty<String>(CityProperty, location.City);
            LoadProperty<String>(CountyProperty, location.County);
            LoadProperty<String>(CountryProperty, location.Country);
            LoadProperty<String>(PostalCodeProperty, location.PostalCode);
            LoadProperty<String>(TVRegionProperty, location.TVRegion);
            LoadProperty<String>(RegionProperty, location.Region);

            MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<ClusterLocation>(oldId, newId);
        }

        #endregion
    }
}
