﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25664: A.Kuszyk
//		Created (adapted from SA).
// V8-26222 : L.Luong
//      Added date deleted
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;
using System.Security.Principal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a user within the solution
    /// (root object)
    /// </summary>
    public partial class User
    {
        #region Constructors
        private User() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the current user details
        /// </summary>
        public static User GetCurrentUser()
        {
            return DataPortal.Fetch<User>();
        }

        /// <summary>
        /// Returns the user with the given id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static User FetchById(Int32 userId)
        {
            return DataPortal.Fetch<User>(new SingleCriteria<Int32>(userId));
        }

        /// <summary>
        /// Gets a user for the given user name.
        /// </summary>
        /// <param name="userName">The user name of the user to fetch.</param>
        /// <returns></returns>
        public static User FetchByUserName(String userName)
        {
            return DataPortal.Fetch<User>(new SingleCriteria<String>(userName));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns></returns>
        private UserDto GetDataTransferObject()
        {
            return new UserDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                UserName = ReadProperty<String>(UserNameProperty),
                FirstName = ReadProperty<String>(FirstNameProperty),
                LastName = ReadProperty<String>(LastNameProperty),
                Description = ReadProperty<String>(DescriptionProperty),
                Office = ReadProperty<String>(OfficeProperty),
                TelephoneNumber = ReadProperty<String>(TelephoneNumberProperty),
                EMailAddress = ReadProperty<String>(EMailAddressProperty),
                Title = ReadProperty<String>(TitleProperty),
                Department = ReadProperty<String>(DepartmentProperty),
                Company = ReadProperty<String>(CompanyProperty),
                Manager = ReadProperty<String>(ManagerProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty)
            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, UserDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            LoadProperty<String>(UserNameProperty, dto.UserName);
            LoadProperty<String>(FirstNameProperty, dto.FirstName);
            LoadProperty<String>(LastNameProperty, dto.LastName);
            LoadProperty<String>(DescriptionProperty, dto.Description);
            LoadProperty<String>(OfficeProperty, dto.Office);
            LoadProperty<String>(TelephoneNumberProperty, dto.TelephoneNumber);
            LoadProperty<String>(EMailAddressProperty, dto.EMailAddress);
            LoadProperty<String>(TitleProperty, dto.Title);
            LoadProperty<String>(DepartmentProperty, dto.Department);
            LoadProperty<String>(CompanyProperty, dto.Company);
            LoadProperty<String>(ManagerProperty, dto.Manager);
            LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);

            LoadProperty<RoleMemberList>(RoleMembersProperty, RoleMemberList.FetchByUserId(dalContext, dto.Id));
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when creating this object for the current user
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IUserDal dal = dalContext.GetDal<IUserDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchByUserName(WindowsIdentity.GetCurrent().Name));
                }
            }
        }

        /// <summary>
        /// Fetches a user for the given criteria, containing a user name.
        /// </summary>
        /// <param name="criteria">The user name criteria.</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(SingleCriteria<String> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IUserDal dal = dalContext.GetDal<IUserDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchByUserName(criteria.Value));
                }
            }
        }

        /// <summary>
        /// Called when creating this object for the current user
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IUserDal dal = dalContext.GetDal<IUserDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchById(criteria.Value));
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this object into the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                UserDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (IUserDal dal = dalContext.GetDal<IUserDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                dalContext.RegisterId<User>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this object in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                UserDto dto = GetDataTransferObject();
                using (IUserDal dal = dalContext.GetDal<IUserDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                using (IUserDal dal = dalContext.GetDal<IUserDal>())
                {
                    dal.DeleteById(ReadProperty<Int32>(IdProperty));
                }

                dalContext.Commit();
            }
        }

        #endregion

        #endregion
    }
}
