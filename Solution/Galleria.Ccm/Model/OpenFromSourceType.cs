﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-32790 : M.Pettit 
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Describes the source options for an Open method
    /// </summary>
    public enum OpenFromSourceType
    {
        OpenFromRepository = 0,
        OpenFromFile = 1
    }
}
