﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25886 : L.Ineson
//  Created
// V8-27426 : L.Ineson
//  Added 2 more values
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class WorkpackagePlanogramParameterValue
    {
        #region Constructors
        private WorkpackagePlanogramParameterValue() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a new instance from a dto
        /// </summary>
        internal static WorkpackagePlanogramParameterValue GetWorkpackagePlanogramParameterValue(IDalContext dalContext, WorkpackagePlanogramParameterValueDto dto)
        {
            return DataPortal.FetchChild<WorkpackagePlanogramParameterValue>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, WorkpackagePlanogramParameterValueDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Object>(Value1Property, dto.Value1);
            this.LoadProperty<Object>(Value2Property, dto.Value2);
            this.LoadProperty<Object>(Value3Property, dto.Value3);
        }

        /// <summary>
        /// Gets a data transfer object from this instance
        /// </summary>
        private WorkpackagePlanogramParameterValueDto GetDataTransferObject(WorkpackagePlanogramParameter parent)
        {
            return new WorkpackagePlanogramParameterValueDto()
            {
                Id = this.ReadProperty<Int32>(IdProperty),
                Value1 = this.ReadProperty<Object>(Value1Property),
                Value2 = this.ReadProperty<Object>(Value2Property),
                Value3 = this.ReadProperty<Object>(Value3Property),
                WorkpackagePlanogramParameterId = parent.Id
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when loading a child instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, WorkpackagePlanogramParameterValueDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting a child instance of this type
        /// </summary>
        private void Child_Insert(BatchSaveContext batchContext, WorkpackagePlanogramParameter parent)
        {
            Int32 oldId = 0;
            batchContext.Insert<WorkpackagePlanogramParameterValueDto>(
            (dc) =>
            {
                WorkpackagePlanogramParameterValueDto dto = this.GetDataTransferObject(parent);
                oldId = dto.Id;
                return dto;
            },
            (dc, dto) =>
            {
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                dc.RegisterId<WorkpackagePlanogramParameterValue>(oldId, dto.Id);
            });
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating a child instance of this type
        /// </summary>
        private void Child_Update(BatchSaveContext batchContext, WorkpackagePlanogramParameter parent)
        {
            if (this.IsSelfDirty)
            {
                batchContext.Update<WorkpackagePlanogramParameterValueDto>(this.GetDataTransferObject(parent));
            }
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this child is being deleted
        /// </summary>
        private void Child_DeleteSelf(BatchSaveContext batchContext, WorkpackagePlanogramParameter parent)
        {
            batchContext.Delete<WorkpackagePlanogramParameterValueDto>(this.GetDataTransferObject(parent));
            FieldManager.UpdateChildren(batchContext, this);
        }
        #endregion

        #endregion
    }
}
