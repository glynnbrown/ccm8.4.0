﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Ineson
//	Created (Auto-generated)
// V8-27468 : L.Ineson
//  Added IsMerchandisedTopDown.
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class FixtureComponent
    {
        #region Constructor
        private FixtureComponent() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static FixtureComponent Fetch(IDalContext dalContext, FixtureComponentDto dto)
        {
            return DataPortal.FetchChild<FixtureComponent>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, FixtureComponentDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32?>(Mesh3DIdProperty, dto.Mesh3DId);
            this.LoadProperty<Int32?>(ImageIdFrontProperty, dto.ImageIdFront);
            this.LoadProperty<Int32?>(ImageIdBackProperty, dto.ImageIdBack);
            this.LoadProperty<Int32?>(ImageIdTopProperty, dto.ImageIdTop);
            this.LoadProperty<Int32?>(ImageIdBottomProperty, dto.ImageIdBottom);
            this.LoadProperty<Int32?>(ImageIdLeftProperty, dto.ImageIdLeft);
            this.LoadProperty<Int32?>(ImageIdRightProperty, dto.ImageIdRight);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Single>(HeightProperty, dto.Height);
            this.LoadProperty<Single>(WidthProperty, dto.Width);
            this.LoadProperty<Single>(DepthProperty, dto.Depth);
            this.LoadProperty<Int16>(NumberOfSubComponentsProperty, dto.NumberOfSubComponents);
            this.LoadProperty<Int16>(NumberOfMerchandisedSubComponentsProperty, dto.NumberOfMerchandisedSubComponents);
            this.LoadProperty<Int16>(NumberOfShelfEdgeLabelsProperty, dto.NumberOfShelfEdgeLabels);
            this.LoadProperty<Boolean>(IsMoveableProperty, dto.IsMoveable);
            this.LoadProperty<Boolean>(IsDisplayOnlyProperty, dto.IsDisplayOnly);
            this.LoadProperty<Boolean>(CanAttachShelfEdgeLabelProperty, dto.CanAttachShelfEdgeLabel);
            this.LoadProperty<String>(RetailerReferenceCodeProperty, dto.RetailerReferenceCode);
            this.LoadProperty<String>(BarCodeProperty, dto.BarCode);
            this.LoadProperty<String>(ManufacturerProperty, dto.Manufacturer);
            this.LoadProperty<String>(ManufacturerPartNameProperty, dto.ManufacturerPartName);
            this.LoadProperty<String>(ManufacturerPartNumberProperty, dto.ManufacturerPartNumber);
            this.LoadProperty<String>(SupplierNameProperty, dto.SupplierName);
            this.LoadProperty<String>(SupplierPartNumberProperty, dto.SupplierPartNumber);
            this.LoadProperty<Single?>(SupplierCostPriceProperty, dto.SupplierCostPrice);
            this.LoadProperty<Single?>(SupplierDiscountProperty, dto.SupplierDiscount);
            this.LoadProperty<Single?>(SupplierLeadTimeProperty, dto.SupplierLeadTime);
            this.LoadProperty<Int32?>(MinPurchaseQtyProperty, dto.MinPurchaseQty);
            this.LoadProperty<Single?>(WeightLimitProperty, dto.WeightLimit);
            this.LoadProperty<Single?>(WeightProperty, dto.Weight);
            this.LoadProperty<Single?>(VolumeProperty, dto.Volume);
            this.LoadProperty<Single?>(DiameterProperty, dto.Diameter);
            this.LoadProperty<Int16?>(CapacityProperty, dto.Capacity);
            this.LoadProperty<FixtureComponentType>(ComponentTypeProperty, (FixtureComponentType)dto.ComponentType);
            this.LoadProperty<Boolean>(IsMerchandisedTopDownProperty, dto.IsMerchandisedTopDown);

        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private FixtureComponentDto GetDataTransferObject(FixturePackage parent)
        {
            return new FixtureComponentDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                FixturePackageId = parent.Id,
                Mesh3DId = ReadProperty<Int32?>(Mesh3DIdProperty),
                ImageIdFront = ReadProperty<Int32?>(ImageIdFrontProperty),
                ImageIdBack = ReadProperty<Int32?>(ImageIdBackProperty),
                ImageIdTop = ReadProperty<Int32?>(ImageIdTopProperty),
                ImageIdBottom = ReadProperty<Int32?>(ImageIdBottomProperty),
                ImageIdLeft = ReadProperty<Int32?>(ImageIdLeftProperty),
                ImageIdRight = ReadProperty<Int32?>(ImageIdRightProperty),
                Name = ReadProperty<String>(NameProperty),
                Height = ReadProperty<Single>(HeightProperty),
                Width = ReadProperty<Single>(WidthProperty),
                Depth = ReadProperty<Single>(DepthProperty),
                NumberOfSubComponents = ReadProperty<Int16>(NumberOfSubComponentsProperty),
                NumberOfMerchandisedSubComponents = ReadProperty<Int16>(NumberOfMerchandisedSubComponentsProperty),
                NumberOfShelfEdgeLabels = ReadProperty<Int16>(NumberOfShelfEdgeLabelsProperty),
                IsMoveable = ReadProperty<Boolean>(IsMoveableProperty),
                IsDisplayOnly = ReadProperty<Boolean>(IsDisplayOnlyProperty),
                CanAttachShelfEdgeLabel = ReadProperty<Boolean>(CanAttachShelfEdgeLabelProperty),
                RetailerReferenceCode = ReadProperty<String>(RetailerReferenceCodeProperty),
                BarCode = ReadProperty<String>(BarCodeProperty),
                Manufacturer = ReadProperty<String>(ManufacturerProperty),
                ManufacturerPartName = ReadProperty<String>(ManufacturerPartNameProperty),
                ManufacturerPartNumber = ReadProperty<String>(ManufacturerPartNumberProperty),
                SupplierName = ReadProperty<String>(SupplierNameProperty),
                SupplierPartNumber = ReadProperty<String>(SupplierPartNumberProperty),
                SupplierCostPrice = ReadProperty<Single?>(SupplierCostPriceProperty),
                SupplierDiscount = ReadProperty<Single?>(SupplierDiscountProperty),
                SupplierLeadTime = ReadProperty<Single?>(SupplierLeadTimeProperty),
                MinPurchaseQty = ReadProperty<Int32?>(MinPurchaseQtyProperty),
                WeightLimit = ReadProperty<Single?>(WeightLimitProperty),
                Weight = ReadProperty<Single?>(WeightProperty),
                Volume = ReadProperty<Single?>(VolumeProperty),
                Diameter = ReadProperty<Single?>(DiameterProperty),
                Capacity = ReadProperty<Int16?>(CapacityProperty),
                ComponentType = (Byte)ReadProperty<FixtureComponentType>(ComponentTypeProperty),
                IsMerchandisedTopDown = ReadProperty<Boolean>(IsMerchandisedTopDownProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FixtureComponentDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, FixturePackage parent)
        {
            this.ResolveIds(dalContext);
            FixtureComponentDto dto = this.GetDataTransferObject(parent);
            Object oldId = dto.Id;
            using (IFixtureComponentDal dal = dalContext.GetDal<IFixtureComponentDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<FixtureComponent>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, FixturePackage parent)
        {
            this.ResolveIds(dalContext);
            if (this.IsSelfDirty)
            {
                using (IFixtureComponentDal dal = dalContext.GetDal<IFixtureComponentDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, FixturePackage parent)
        {
            using (IFixtureComponentDal dal = dalContext.GetDal<IFixtureComponentDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion

    }
}