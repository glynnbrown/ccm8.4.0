﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26317 : N.Foster
//  Created
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class WorkpackagePlanogramDebugList : ModelList<WorkpackagePlanogramDebugList, WorkpackagePlanogramDebug>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(WorkpackagePlanogramDebugList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramDebugList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.WorkpackageCreate.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramDebugList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.WorkpackageEdit.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramDebugList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.WorkpackageDelete.ToString()));
        }
        #endregion

        #region Criteria

        #region FetchBySourcePlanogramIdCriteria
        /// <summary>
        /// Criteria to fetch details by workpackage planogram id
        /// </summary>
        public class FetchBySourcePlanogramIdCriteria : CriteriaBase<FetchBySourcePlanogramIdCriteria>
        {
            #region Properties

            #region SourcePlanogramId
            /// <summary>
            /// WorkpackagePlanogramId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> SourcePlanogramIdProperty =
                RegisterProperty<Int32>(c => c.SourcePlanogramId);
            /// <summary>
            /// Returns the specified workpackage planogram id
            /// </summary>
            public Int32 SourcePlanogramId
            {
                get { return this.ReadProperty<Int32>(SourcePlanogramIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchBySourcePlanogramIdCriteria(Int32 sourcePlanogramId)
            {
                this.LoadProperty<Int32>(SourcePlanogramIdProperty, sourcePlanogramId);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Adds a new item to this collection
        /// </summary>
        public void Add(Planogram sourcePlanogram, WorkflowTask workflowTask, Planogram debugPlanogram)
        {
            this.Add(WorkpackagePlanogramDebug.NewWorkpackagePlanogramDebug(
                (Int32)sourcePlanogram.Id,
                workflowTask.Id,
                (Int32)debugPlanogram.Id));
        }

        /// <summary>
        /// Adds a new item to this collection
        /// </summary>
        public void Add(Package sourcePackage, WorkflowTask workflowTask, Package debugPackage)
        {
            this.Add(WorkpackagePlanogramDebug.NewWorkpackagePlanogramDebug(
                (Int32)sourcePackage.Planograms[0].Id,
                workflowTask.Id,
                (Int32)debugPackage.Planograms[0].Id));
        }

        /// <summary>
        /// Resolves a debug planogram id change
        /// </summary>
        public void ResolveId(Int32 oldId, Int32 newId)
        {
            if (oldId != newId)
            {
                foreach (var planogram in this)
                {
                    if (planogram.SourcePlanogramId == oldId) planogram.SourcePlanogramId = newId;
                    if (planogram.DebugPlanogramId == oldId) planogram.DebugPlanogramId = newId;
                }
            }
        }
        #endregion
    }
}
