﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24801 L.Hodson
//  Created
// V8-25526 : K.Pickup
//  Use RelationshipTypes.LazyLoad when registering lazy-loaded model properties.
// V8-25436 : L.Luong
//  Changed to fit new design
// V8-27266 : L.Ineson
//  Added SaveAsFile 
// V8-27941 J.Pickup
//  Introduced new properties for respository. (rowversion etc).
// V8-28362 : L.Ineson
//  Added the ability to open a file as readonly.
#endregion
#region Version History: (CCM 8.03)
// V8-28661 : L.Ineson
//  Added FetchByEntityIdNameCriteria
#endregion
#region Version History: (CCM 8.10)
// V8-28661 : L.Ineson
//  Implemented IPlanogramHighlight interface
// V8-30202 : M.Shelley
//  Added SaveAs method to explicitly save to the repository
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a Highlight object.
    /// </summary>
    [DefaultNewMethod("NewHighlight", "EntityId")]
    [Serializable]
    public sealed partial class Highlight : ModelObject<Highlight>, IDisposable, IPlanogramHighlight
    {
        #region Public Static

        /// <summary>
        /// Returns the file extension to use for highlights.
        /// </summary>
        public static String FileExtension
        {
            get { return Constants.HighlightFileExtension; }
        }

        #endregion

        #region Static Constructor
        static Highlight()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Properties

        #region DalFactoryType

        /// <summary>
        /// DalFactoryType property definition
        /// </summary>
        private static readonly ModelPropertyInfo<HighlightDalFactoryType> DalFactoryTypeProperty =
            RegisterModelProperty<HighlightDalFactoryType>(c => c.DalFactoryType);

        /// <summary>
        /// Returns the dal factory type
        /// </summary>
        private HighlightDalFactoryType DalFactoryType
        {
            get { return this.GetProperty<HighlightDalFactoryType>(DalFactoryTypeProperty); }
        }

        #endregion

        #region DalFactoryName
        /// <summary>
        /// DalFactoryName property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> DalFactoryNameProperty =
            RegisterModelProperty<String>(c => c.DalFactoryName);
        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        protected override String DalFactoryName
        {
            get { return this.GetProperty<String>(DalFactoryNameProperty); }
        }
        #endregion

        #region IsReadOnly

        private static readonly ModelPropertyInfo<Boolean> IsReadOnlyProperty =
             RegisterModelProperty<Boolean>(c => c.IsReadOnly);

        /// <summary>
        /// Returns true if this model has been loaded as readonly.
        /// </summary>
        public Boolean IsReadOnly
        {
            get { return this.GetProperty<Boolean>(IsReadOnlyProperty); }
        }

        #endregion

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> IdProperty =
            RegisterModelProperty<Object>(c => c.Id);
        /// <summary>
        /// Returns the unique highlight id
        /// For a filestystem this is the filepath.
        /// </summary>
        public Object Id
        {
            get { return this.GetProperty<Object>(IdProperty); }
        }
        #endregion

        #region RowVersion

        /// <summary>
        ///     Metadata for the <see cref="RowVersion" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);

        /// <summary>
        ///     Gets the row version timestamp.
        /// </summary>
        private RowVersion RowVersion
        {
            get { return GetProperty(RowVersionProperty); }
        }

        #endregion

        #region EntityId

        /// <summary>
        ///     Metadata for the <see cref="EntityId" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<int>(c => c.EntityId);

        /// <summary>
        ///     Gets or sets the unique Id value for the <see cref="Entity" /> model this instance belongs to.
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty(EntityIdProperty); }
            set { SetProperty(EntityIdProperty, value); }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name, Message.Generic_Name);

        /// <summary>
        /// Gets or sets the highlight name
        /// </summary>
        public String Name
        {
            get { return this.GetProperty<String>(NameProperty); }
            set { this.SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region Type

        /// <summary>
        /// Type property definition
        /// </summary>
        public static readonly ModelPropertyInfo<HighlightType> TypeProperty =
            RegisterModelProperty<HighlightType>(c => c.Type);

        /// <summary>
        /// Gets or sets the type of highlight this is.
        /// </summary>
        public HighlightType Type
        {
            get { return this.GetProperty<HighlightType>(TypeProperty); }
            set { this.SetProperty<HighlightType>(TypeProperty, value); }
        }
        
        #endregion

        #region MethodType

        /// <summary>
        /// MethodType property definition 
        /// </summary>
        public static readonly ModelPropertyInfo<HighlightMethodType> MethodTypeProperty =
            RegisterModelProperty<HighlightMethodType>(c => c.MethodType, Message.Highlight_MethodType);
        
        /// <summary>
        /// Gets/Sets the method type
        /// </summary>
        public HighlightMethodType MethodType
        {
            get { return GetProperty<HighlightMethodType>(MethodTypeProperty); }
            set 
            { 
                SetProperty<HighlightMethodType>(MethodTypeProperty, value);
                OnPropertyChanged("MethodTypeFriendlyName");
            }
        }

        PlanogramHighlightMethodType IPlanogramHighlight.MethodType
        {
            get { return MethodType.ToPlanogramHighlightMethodType(); }
        }

        #endregion

        #region IsFilterEnabled

        /// <summary>
        /// IsFilterEnabled property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsFilterEnabledProperty =
            RegisterModelProperty<Boolean>(c => c.IsFilterEnabled);
        
        /// <summary>
        /// Gets/Sets whether filters should be used.
        /// </summary>
        public Boolean IsFilterEnabled
        {
            get { return GetProperty<Boolean>(IsFilterEnabledProperty); }
            set { SetProperty<Boolean>(IsFilterEnabledProperty, value); }
        }

        #endregion

        #region IsPreFilter

        /// <summary>
        /// IsPreFilter property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsPreFilterProperty =
            RegisterModelProperty<Boolean>(c => c.IsPreFilter);
        
        /// <summary>
        /// Gets/Sets is the filter should be applied before the method.
        /// If false it will be applied post.
        /// </summary>
        public Boolean IsPreFilter
        {
            get { return GetProperty<Boolean>(IsPreFilterProperty); }
            set { SetProperty<Boolean>(IsPreFilterProperty, value); }
        }

        #endregion

        #region IsAndFilter

        /// <summary>
        /// IsAndFilter property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsAndFilterProperty =
           RegisterModelProperty<Boolean>(c => c.IsAndFilter);

        /// <summary>
        /// Gets/Sets if filters should be processed using an and condition.
        /// If false they will be proccessed as or.
        /// </summary>
        public Boolean IsAndFilter
        {
            get { return GetProperty<Boolean>(IsAndFilterProperty); }
            set { SetProperty<Boolean>(IsAndFilterProperty, value); }
        }


        #endregion

        #region Field1

        /// <summary>
        /// Field1 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Field1Property =
           RegisterModelProperty<String>(c => c.Field1);
        
        /// <summary>
        /// The field to use for group or colourscale analysis.
        /// The x axis field for quadrant analysis.
        /// </summary>
        public String Field1
        {
            get { return GetProperty<String>(Field1Property); }
            set { SetProperty<String>(Field1Property, value); }
        }

        #endregion

        #region Field2

        /// <summary>
        /// Field2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Field2Property =
           RegisterModelProperty<String>(c => c.Field2);

        /// <summary>
        /// The y axis field for quadrant analysis.
        /// </summary>
        public String Field2
        {
            get { return GetProperty<String>(Field2Property); }
            set { SetProperty<String>(Field2Property, value); }
        }

        #endregion

        #region QuadrantXType

        public static readonly ModelPropertyInfo<HighlightQuadrantType> QuadrantXTypeProperty =
           RegisterModelProperty<HighlightQuadrantType>(c => c.QuadrantXType);
        /// <summary>
        /// The type used to process the horizontal axis
        /// </summary>
        public HighlightQuadrantType QuadrantXType
        {
            get { return GetProperty<HighlightQuadrantType>(QuadrantXTypeProperty); }
            set { SetProperty<HighlightQuadrantType>(QuadrantXTypeProperty, value); }
        }

        PlanogramHighlightQuadrantType IPlanogramHighlight.QuadrantXType
        {
            get { return QuadrantXType.ToPlanogramHighlightQuadrantType(); }
        }

        #endregion

        #region QuadrantXConstant

        /// <summary>
        /// QuadrantXConstant property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> QuadrantXConstantProperty =
           RegisterModelProperty<Single>(c => c.QuadrantXConstant);

        /// <summary>
        /// The value used for constant type analysis on the x quadrant.
        /// </summary>
        public Single QuadrantXConstant
        {
            get { return GetProperty<Single>(QuadrantXConstantProperty); }
            set { SetProperty<Single>(QuadrantXConstantProperty, value); }
        }

        #endregion

        #region QuadrantYType

        public static readonly ModelPropertyInfo<HighlightQuadrantType> QuadrantYTypeProperty =
           RegisterModelProperty<HighlightQuadrantType>(c => c.QuadrantYType);
        /// <summary>
        /// The type used to process the horizontal axis
        /// </summary>
        public HighlightQuadrantType QuadrantYType
        {
            get { return GetProperty<HighlightQuadrantType>(QuadrantYTypeProperty); }
            set { SetProperty<HighlightQuadrantType>(QuadrantYTypeProperty, value); }
        }

        PlanogramHighlightQuadrantType IPlanogramHighlight.QuadrantYType
        {
            get { return QuadrantYType.ToPlanogramHighlightQuadrantType(); }
        }

        #endregion

        #region QuadrantYConstant

        /// <summary>
        /// QuadrantYConstant property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> QuadrantYConstantProperty =
           RegisterModelProperty<Single>(c => c.QuadrantYConstant);

        /// <summary>
        /// The value used for constant type analysis on the x quadrant.
        /// </summary>
        public Single QuadrantYConstant
        {
            get { return GetProperty<Single>(QuadrantYConstantProperty); }
            set { SetProperty<Single>(QuadrantYConstantProperty, value); }
        }

        #endregion

        #region DateCreated

        /// <summary>
        ///     Metadata for the <see cref="DateCreated" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        ///     Gets or sets the date this instance was created originally.
        /// </summary>
        private DateTime DateCreated
        {
            get { return GetProperty(DateCreatedProperty); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        ///     Metadata for the <see cref="DateLastModified" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        ///     Gets or sets the last date this instance was modified (and persisted).
        /// </summary>
        private DateTime DateLastModified
        {
            get { return GetProperty(DateLastModifiedProperty); }
        }

        #endregion

        #region DateDeleted

        /// <summary>
        ///     Metadata for the <see cref="DateDeleted" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);

        /// <summary>
        ///     Gets or sets the date this instance was marked for deletion.
        /// </summary>
        private DateTime? DateDeleted
        {
            get { return GetProperty(DateDeletedProperty); }
        }

        #endregion

        #region Filters
        /// <summary>
        /// Filters property definition
        /// </summary>
        public static readonly ModelPropertyInfo<HighlightFilterList> FiltersProperty =
            RegisterModelProperty<HighlightFilterList>(c => c.Filters, RelationshipTypes.LazyLoad);
        
        /// <summary>
        /// List of filters that limit what should be highlighted
        /// </summary>
        public HighlightFilterList Filters
        {
            get { return this.GetProperty<HighlightFilterList>(FiltersProperty); }
        }

        IEnumerable<IPlanogramHighlightFilter> IPlanogramHighlight.Filters
        {
            get { return this.GetProperty<HighlightFilterList>(FiltersProperty); }
        }
        #endregion

        #region Groups
        /// <summary>
        /// Groups property definition
        /// </summary>
        public static readonly ModelPropertyInfo<HighlightGroupList> GroupsProperty =
            RegisterModelProperty<HighlightGroupList>(c => c.Groups, RelationshipTypes.LazyLoad);
        
        /// <summary>
        /// List of groups processed for this highlight
        /// </summary>
        public HighlightGroupList Groups
        {
            get { return this.GetProperty<HighlightGroupList>(GroupsProperty);}
        }

        IEnumerable<IPlanogramHighlightGroup> IPlanogramHighlight.Groups
        {
            get { return this.GetProperty<HighlightGroupList>(GroupsProperty); }
        }

        #endregion

        #region Characteristics
        /// <summary>
        /// Characteristics property definition
        /// </summary>
        public static readonly ModelPropertyInfo<HighlightCharacteristicList> CharacteristicsProperty =
             RegisterModelProperty<HighlightCharacteristicList>(c => c.Characteristics, RelationshipTypes.LazyLoad);

        /// <summary>
        /// List of characteristic groupings used by characteristic analysis.
        /// </summary>
        public HighlightCharacteristicList Characteristics
        {
            get {return this.GetProperty<HighlightCharacteristicList>(CharacteristicsProperty);}
        }

        IEnumerable<IPlanogramHighlightCharacteristic> IPlanogramHighlight.Characteristics
        {
            get { return this.GetProperty<HighlightCharacteristicList>(CharacteristicsProperty); }
        }

        #endregion

        #endregion

        #region Helper Properties

        /// <summary>
        /// Returns the localised friendly name for the method type.
        /// </summary>
        public String MethodTypeFriendlyName
        {
            get { return HighlightMethodTypeHelper.FriendlyNames[this.MethodType]; }
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));
        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository

        /// <summary>
        /// Returns a dictionary of the current role permissions allocated to the user for this object type.
        /// </summary>
        /// <returns></returns>
        public static IDictionary<AuthorizationActions, Boolean> GetUserPermissions()
        {
            return new Dictionary<AuthorizationActions, Boolean>()
            {
                {AuthorizationActions.CreateObject, ApplicationContext.User.IsInRole(DomainPermission.HighlightCreate.ToString())},
                {AuthorizationActions.GetObject, ApplicationContext.User.IsInRole(DomainPermission.HighlightGet.ToString())},
                {AuthorizationActions.EditObject, ApplicationContext.User.IsInRole(DomainPermission.HighlightEdit.ToString())},
                {AuthorizationActions.DeleteObject, ApplicationContext.User.IsInRole(DomainPermission.HighlightDelete.ToString())},
            };
        }

        #endregion

        #region Criteria

		#region FetchByIdCriteria

		/// <summary>
        /// Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region PackageType
            /// <summary>
            /// HighlightDalType property definition
            /// </summary>
            public static readonly PropertyInfo<HighlightDalFactoryType> HighlightDalTypeProperty =
                RegisterProperty<HighlightDalFactoryType>(c => c.HighlightDalType);
            /// <summary>
            /// Returns the highlight dal type
            /// </summary>
            public HighlightDalFactoryType HighlightDalType
            {
                get { return this.ReadProperty<HighlightDalFactoryType>(HighlightDalTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #region AsReadOnly

            /// <summary>
            /// AsReadOnly property definition
            /// </summary>
            public static readonly PropertyInfo<Boolean> AsReadOnlyProperty =
                RegisterProperty<Boolean>(c => c.AsReadOnly);
            /// <summary>
            /// If true the model will be loaded as readonly.
            /// </summary>
            public Boolean AsReadOnly
            {
                get { return this.ReadProperty<Boolean>(AsReadOnlyProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(HighlightDalFactoryType type, Object id)
            {
                this.LoadProperty<HighlightDalFactoryType>(HighlightDalTypeProperty, type);
                this.LoadProperty<Object>(IdProperty, id);
                this.LoadProperty<Boolean>(AsReadOnlyProperty, false);
            }

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(HighlightDalFactoryType type, Object id, Boolean asReadOnly)
            {
                this.LoadProperty<HighlightDalFactoryType>(HighlightDalTypeProperty, type);
                this.LoadProperty<Object>(IdProperty, id);
                this.LoadProperty<Boolean>(AsReadOnlyProperty, asReadOnly);
            }
            #endregion
        }

		#endregion

        #region FetchByEntityIdName Criteria

        [Serializable]
        public class FetchByEntityIdNameCriteria : CriteriaBase<FetchByEntityIdNameCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static readonly PropertyInfo<String> NameProperty =
                RegisterProperty<String>(c => c.Name);
            public String Name
            {
                get { return ReadProperty<String>(NameProperty); }
            }

            public FetchByEntityIdNameCriteria(Int32 entityId, String name)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<String>(NameProperty, name);
            }
        }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        /// Locks a Highlight so that it cannot be
        /// edited or opened by another process
        /// </summary>
        internal static void LockHighlightByFileName(String fileName)
        {
            DataPortal.Execute<LockHighlightCommand>(new LockHighlightCommand(HighlightDalFactoryType.FileSystem, fileName));
        }

        /// <summary>
        /// Unlocks a highlight
        /// </summary>
        public static void UnlockHighlightByFileName(String fileName)
        {
            DataPortal.Execute<UnlockHighlightCommand>(new UnlockHighlightCommand(HighlightDalFactoryType.FileSystem, fileName));
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <returns></returns>
        public static Highlight NewHighlight()
        {
            Highlight item = new Highlight();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <returns></returns>
        public static Highlight NewHighlight(Int32 entityId)
        {
            Highlight item = new Highlight();
            item.Create(entityId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 entityId)
        {
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<HighlightDalFactoryType>(DalFactoryTypeProperty, HighlightDalFactoryType.Unknown);
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<HighlightGroupList>(GroupsProperty, HighlightGroupList.NewHighlightGroupList());
            this.LoadProperty<HighlightFilterList>(FiltersProperty, HighlightFilterList.NewHighlightFilterList());
            this.LoadProperty<HighlightCharacteristicList>(CharacteristicsProperty, HighlightCharacteristicList.NewHighlightCharacteristicList());
            this.LoadProperty<HighlightType>(TypeProperty, HighlightType.Position);
            this.LoadProperty<HighlightMethodType>(MethodTypeProperty, HighlightMethodType.Group);
            this.LoadProperty<Boolean>(IsFilterEnabledProperty, false);
            this.LoadProperty<Boolean>(IsPreFilterProperty, true);
            this.LoadProperty<Boolean>(IsAndFilterProperty, true);
            this.LoadProperty<HighlightQuadrantType>(QuadrantXTypeProperty, HighlightQuadrantType.Mean);
            this.LoadProperty<HighlightQuadrantType>(QuadrantYTypeProperty, HighlightQuadrantType.Mean);
            
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();

            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<Highlight>(oldId, newId);
        }

        /// <summary>
        /// ToString override.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Provide a SaveAs method to allow saving to the repository.
        /// This resets DalFactoryType and DalFactoryName to allow saving saving to the repository,
        /// otherwise a Highlight instance loaded from a file tries to save back to a file.
        /// </summary>
        /// <returns></returns>
        public Highlight SaveAs()
        {
            this.LoadProperty<HighlightDalFactoryType>(DalFactoryTypeProperty, HighlightDalFactoryType.Unknown);
            this.LoadProperty<String>(DalFactoryNameProperty, DalContainer.DalName);

            return this.Save();
        }

        /// <summary>
        /// Saves this to a new file.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public Highlight SaveAsFile(String path)
        {
            Highlight returnValue = null;
            String fileName = path;

            //make sure the file extension is correct.
            fileName = Path.ChangeExtension(path, FileExtension);

            //if this model is not new and was fetched from another file
            // take a note of the old file name.
            String oldFileName = null;
            if ((!IsNew) && (this.Id as String != fileName))
            {
                if (DalFactoryType == HighlightDalFactoryType.FileSystem)
                {
                    oldFileName = this.Id as String;
                }
                MarkGraphAsNew();
            }

            LoadProperty<HighlightDalFactoryType>(DalFactoryTypeProperty, HighlightDalFactoryType.FileSystem);
            LoadProperty<String>(DalFactoryNameProperty, "");
            LoadProperty<Object>(IdProperty, fileName);
            this.Name = Path.GetFileNameWithoutExtension(fileName);
            returnValue = Save();

            //unlock the old file.
            if (!String.IsNullOrEmpty(oldFileName))
            {
                UnlockHighlightByFileName(oldFileName);
            }

            //V8-25873 : Make sure id is the file name for file system, dal is returning 1
            LoadProperty<Object>(IdProperty, fileName);
            return returnValue;
        }

        void IPlanogramHighlight.ClearGroups()
        {
            this.Groups.Clear();
        }

        #endregion

        #region Commands

        #region LockHighlightCommand
        /// <summary>
        /// Performs the locking of a Highlight
        /// </summary>
        [Serializable]
        private partial class LockHighlightCommand : CommandBase<LockHighlightCommand>
        {
            #region Properties

            #region HighlightDalFactoryType
            /// <summary>
            /// HighlightDalFactoryType property definition
            /// </summary>
            public static readonly PropertyInfo<HighlightDalFactoryType> HighlightDalFactoryTypeProperty =
                RegisterProperty<HighlightDalFactoryType>(c => c.HighlightDalFactoryType);
            /// <summary>
            /// Returns the highlight dal factory type
            /// </summary>
            public HighlightDalFactoryType HighlightDalFactoryType
            {
                get { return this.ReadProperty<HighlightDalFactoryType>(HighlightDalFactoryTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public LockHighlightCommand(HighlightDalFactoryType dalType, Object id)
            {
                this.LoadProperty<HighlightDalFactoryType>(HighlightDalFactoryTypeProperty, dalType);
                this.LoadProperty<Object>(IdProperty, id);
            }
            #endregion
        }
        #endregion

        #region UnlockHighlightCommand
        /// <summary>
        /// Performs the unlocking of a highlight
        /// </summary>
        [Serializable]
        private partial class UnlockHighlightCommand : CommandBase<UnlockHighlightCommand>
        {
            #region Properties

            #region HighlightDalFactoryType
            /// <summary>
            /// SourceType property definition
            /// </summary>
            public static readonly PropertyInfo<HighlightDalFactoryType> HighlightDalFactoryTypeProperty =
                RegisterProperty<HighlightDalFactoryType>(c => c.HighlightDalFactoryType);
            /// <summary>
            /// Returns the Highlight source type
            /// </summary>
            public HighlightDalFactoryType HighlightDalFactoryType
            {
                get { return this.ReadProperty<HighlightDalFactoryType>(HighlightDalFactoryTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public UnlockHighlightCommand(HighlightDalFactoryType sourceType, Object id)
            {
                this.LoadProperty<HighlightDalFactoryType>(HighlightDalFactoryTypeProperty, sourceType);
                this.LoadProperty<Object>(IdProperty, id);
            }
            #endregion
        }
        #endregion

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    if (!IsNew)
                    {
                        if (this.DalFactoryType == HighlightDalFactoryType.FileSystem
                            && this.Id is String)
                        {
                            UnlockHighlightByFileName((String)Id);
                        }
                    }
                }
                _isDisposed = true;
            }
        }

        #endregion

    }
}
