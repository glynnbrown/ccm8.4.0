﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.

#endregion

#endregion

using System;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     This class holds a collection of <see cref="ColumnLayoutSetting"/> objects. Child of <see cref="UserSystemSetting"/>.
    /// </summary>
    [Serializable]
    public sealed partial class ColumnLayoutSettingList : ModelList<ColumnLayoutSettingList, ColumnLayoutSetting>
    {
        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods
        /// <summary>
        ///     Initializes a new instance of the <see cref="ColumnLayoutSettingList"/> class.
        /// </summary>
        /// <returns>A new instance of <see cref="ColumnLayoutSettingList"/>.</returns>
        public static ColumnLayoutSettingList NewColumnLayoutSettingList()
        {
            var newInstance = new ColumnLayoutSettingList();
            newInstance.Create();
            return newInstance;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        ///     Creates a new instance. Called from the Factory Methods.
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            AllowNew = true;
        }

        #endregion

        #endregion
    }
}
