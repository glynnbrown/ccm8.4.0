﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
#endregion
#endregion

using System;
using Csla.Rules.CommonRules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Model;
using Galleria.Ccm.Resources.Language;
using Csla.Rules;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a synchronisation source for
    /// the sychronisation service
    /// </summary>
    [Serializable]
    public sealed partial class SyncSource : ModelObject<SyncSource>
    {
        #region Authorization Rules
        //this object is accessed without authentication and so cannot have rules
        #endregion

        #region Properties

        /// <summary>
        /// The id property
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// The row version property
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
            set { SetProperty<RowVersion>(RowVersionProperty, value); }
        }

        /// <summary>
        /// The web service server name property
        /// </summary>
        public static readonly ModelPropertyInfo<String> ServerNameProperty =
            RegisterModelProperty<String>(c => c.ServerName, Message.SyncSource_ServerName);

        public String ServerName
        {
            get { return GetProperty<String>(ServerNameProperty); }
            set { SetProperty<String>(ServerNameProperty, value); }
        }

        /// <summary>
        /// The web service site name property
        /// </summary>
        public static readonly ModelPropertyInfo<String> SiteNameProperty =
            RegisterModelProperty<String>(c => c.SiteName, Message.SyncSource_SiteName);
        public String SiteName
        {
            get { return GetProperty<String>(SiteNameProperty); }
            set { SetProperty<String>(SiteNameProperty, value); }
        }

        /// <summary>
        /// The web service port number property
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PortNumberProperty =
            RegisterModelProperty<Int32>(c => c.PortNumber, Message.SyncSource_PortNumber);
        public Int32 PortNumber
        {
            get { return GetProperty<Int32>(PortNumberProperty); }
            set { SetProperty<Int32>(PortNumberProperty, value); }
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(ServerNameProperty));
            BusinessRules.AddRule(new MaxLength(ServerNameProperty, 255));
            BusinessRules.AddRule(new MaxLength(SiteNameProperty, 255));
            BusinessRules.AddRule(new Required(PortNumberProperty));
            BusinessRules.AddRule(new MinValue<Int32>(PortNumberProperty, 1));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new synchornisation source
        /// </summary>
        /// <returns>A new synchornisation source</returns>
        public static SyncSource NewSyncSource()
        {
            SyncSource item = new SyncSource();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            base.Create();
        }
        #endregion

    }
}