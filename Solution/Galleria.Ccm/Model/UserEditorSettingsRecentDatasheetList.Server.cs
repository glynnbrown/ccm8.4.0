﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31699 : A.Heathcote
//  Created.

#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Data;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Diagnostics.CodeAnalysis;
 
namespace Galleria.Ccm.Model
{
    public partial class UserEditorSettingsRecentDatasheetList
    {
        #region Constructor
        private UserEditorSettingsRecentDatasheetList() { }  //Forces use of Factory methods
        #endregion

        #region Factory Methods

        internal static UserEditorSettingsRecentDatasheetList GetUserEditorSettingsRecentDatasheet(IDalContext dalContext)
        {
            return DataPortal.FetchChild<UserEditorSettingsRecentDatasheetList>(dalContext);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dalContext">This is the current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext)
        {
            RaiseListChangedEvents = false;
            using (IUserEditorSettingsRecentDatasheetDal dal = dalContext.GetDal<IUserEditorSettingsRecentDatasheetDal>())
            {
                foreach (UserEditorSettingsRecentDatasheetDto dto in dal.FetchAll())
                {
                    this.Add(UserEditorSettingsRecentDatasheet.GetUserEditorSettingsRecentDatasheet(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}