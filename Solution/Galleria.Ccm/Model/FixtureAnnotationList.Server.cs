﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// CCM-32524 : L.Ineson
//  Created
#endregion
#endregion

using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class FixtureAnnotationList
    {
        #region Constructor
        private FixtureAnnotationList() { } // Force use of factory methods
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning all annotations for a planogram
        /// </summary>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = this.GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IFixtureAnnotationDal dal = dalContext.GetDal<IFixtureAnnotationDal>())
                {
                    IEnumerable<FixtureAnnotationDto> dtoList = dal.FetchByFixturePackageId(criteria.ParentId);
                    foreach (FixtureAnnotationDto dto in dtoList)
                    {
                        this.Add(FixtureAnnotation.Fetch(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
            this.MarkAsChild();
        }
        #endregion

        #endregion
    }
}
