﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
// V8-25526 : K.Pickup
//  Use RelationshipTypes.LazyLoad when registering lazy-loaded model properties.
// V8-26495 : L.Ineson
//  Added display types.
// V8-27058 : A.Probyn
//  Removed references to MetaNumberOfShelfEdgeLabels
// V8-26182 : L.Ineson
//  Added NewPlanogramFixtureComponent
#endregion
#region Version History: (CCM CCM830)
// V8-32524 : L.Ineson
//  Added FixtureAnnotations and simplified updated back to plan
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// FixtureComponent Model object
    /// </summary>
    [Serializable]
    public sealed partial class FixtureComponent : ModelObject<FixtureComponent>
    {
        #region Static Constructor
        static FixtureComponent()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new FixturePackage Parent
        {
            get
            {
                FixtureComponentList parentList = base.Parent as FixtureComponentList;
                if (parentList != null)
                {
                    return parentList.Parent;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region Mesh3DId

        /// <summary>
        /// Mesh3DId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Mesh3DIdProperty =
            RegisterModelProperty<Int32?>(c => c.Mesh3DId);

        /// <summary>
        /// Gets/Sets the Mesh3DId value
        /// </summary>
        public Int32? Mesh3DId
        {
            get { return GetProperty<Int32?>(Mesh3DIdProperty); }
            set { SetProperty<Int32?>(Mesh3DIdProperty, value); }
        }

        #endregion

        #region ImageIdFront

        /// <summary>
        /// ImageIdFront property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> ImageIdFrontProperty =
            RegisterModelProperty<Int32?>(c => c.ImageIdFront);

        /// <summary>
        /// Gets/Sets the ImageIdFront value
        /// </summary>
        public Int32? ImageIdFront
        {
            get { return GetProperty<Int32?>(ImageIdFrontProperty); }
            set { SetProperty<Int32?>(ImageIdFrontProperty, value); }
        }

        #endregion

        #region ImageIdBack

        /// <summary>
        /// ImageIdBack property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> ImageIdBackProperty =
            RegisterModelProperty<Int32?>(c => c.ImageIdBack);

        /// <summary>
        /// Gets/Sets the ImageIdBack value
        /// </summary>
        public Int32? ImageIdBack
        {
            get { return GetProperty<Int32?>(ImageIdBackProperty); }
            set { SetProperty<Int32?>(ImageIdBackProperty, value); }
        }

        #endregion

        #region ImageIdTop

        /// <summary>
        /// ImageIdTop property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> ImageIdTopProperty =
            RegisterModelProperty<Int32?>(c => c.ImageIdTop);

        /// <summary>
        /// Gets/Sets the ImageIdTop value
        /// </summary>
        public Int32? ImageIdTop
        {
            get { return GetProperty<Int32?>(ImageIdTopProperty); }
            set { SetProperty<Int32?>(ImageIdTopProperty, value); }
        }

        #endregion

        #region ImageIdBottom

        /// <summary>
        /// ImageIdBottom property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> ImageIdBottomProperty =
            RegisterModelProperty<Int32?>(c => c.ImageIdBottom);

        /// <summary>
        /// Gets/Sets the ImageIdBottom value
        /// </summary>
        public Int32? ImageIdBottom
        {
            get { return GetProperty<Int32?>(ImageIdBottomProperty); }
            set { SetProperty<Int32?>(ImageIdBottomProperty, value); }
        }

        #endregion

        #region ImageIdLeft

        /// <summary>
        /// ImageIdLeft property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> ImageIdLeftProperty =
            RegisterModelProperty<Int32?>(c => c.ImageIdLeft);

        /// <summary>
        /// Gets/Sets the ImageIdLeft value
        /// </summary>
        public Int32? ImageIdLeft
        {
            get { return GetProperty<Int32?>(ImageIdLeftProperty); }
            set { SetProperty<Int32?>(ImageIdLeftProperty, value); }
        }

        #endregion

        #region ImageIdRight

        /// <summary>
        /// ImageIdRight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> ImageIdRightProperty =
            RegisterModelProperty<Int32?>(c => c.ImageIdRight);

        /// <summary>
        /// Gets/Sets the ImageIdRight value
        /// </summary>
        public Int32? ImageIdRight
        {
            get { return GetProperty<Int32?>(ImageIdRightProperty); }
            set { SetProperty<Int32?>(ImageIdRightProperty, value); }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name, Message.Generic_Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region Height

        /// <summary>
        /// Height property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HeightProperty =
            RegisterModelProperty<Single>(c => c.Height, Message.FixtureComponent_Height, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the Height value
        /// </summary>
        public Single Height
        {
            get { return GetProperty<Single>(HeightProperty); }
            set { SetProperty<Single>(HeightProperty, value); }
        }

        #endregion

        #region Width

        /// <summary>
        /// Width property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> WidthProperty =
            RegisterModelProperty<Single>(c => c.Width, Message.FixtureComponent_Width, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the Width value
        /// </summary>
        public Single Width
        {
            get { return GetProperty<Single>(WidthProperty); }
            set { SetProperty<Single>(WidthProperty, value); }
        }

        #endregion

        #region Depth

        /// <summary>
        /// Depth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DepthProperty =
            RegisterModelProperty<Single>(c => c.Depth, Message.FixtureComponent_Depth, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the Depth value
        /// </summary>
        public Single Depth
        {
            get { return GetProperty<Single>(DepthProperty); }
            set { SetProperty<Single>(DepthProperty, value); }
        }

        #endregion

        #region NumberOfSubComponents

        /// <summary>
        /// NumberOfSubComponents property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> NumberOfSubComponentsProperty =
            RegisterModelProperty<Int16>(c => c.NumberOfSubComponents, Message.FixtureComponent_NumberOfSubComponents);

        /// <summary>
        /// Gets/Sets the NumberOfSubComponents value
        /// </summary>
        public Int16 NumberOfSubComponents
        {
            get { return GetProperty<Int16>(NumberOfSubComponentsProperty); }
            set { SetProperty<Int16>(NumberOfSubComponentsProperty, value); }
        }

        #endregion

        #region NumberOfMerchandisedSubComponents

        /// <summary>
        /// NumberOfMerchandisedSubComponents property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> NumberOfMerchandisedSubComponentsProperty =
            RegisterModelProperty<Int16>(c => c.NumberOfMerchandisedSubComponents, Message.FixtureComponent_NumberOfMerchandisedSubComponents);

        /// <summary>
        /// Gets/Sets the NumberOfMerchandisedSubComponents value
        /// </summary>
        public Int16 NumberOfMerchandisedSubComponents
        {
            get { return GetProperty<Int16>(NumberOfMerchandisedSubComponentsProperty); }
            set { SetProperty<Int16>(NumberOfMerchandisedSubComponentsProperty, value); }
        }

        #endregion

        #region NumberOfShelfEdgeLabels

        /// <summary>
        /// NumberOfShelfEdgeLabels property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> NumberOfShelfEdgeLabelsProperty =
            RegisterModelProperty<Int16>(c => c.NumberOfShelfEdgeLabels, Message.FixtureComponent_NumberOfShelfEdgeLabels);

        /// <summary>
        /// Gets/Sets the NumberOfShelfEdgeLabels value
        /// </summary>
        public Int16 NumberOfShelfEdgeLabels
        {
            get { return GetProperty<Int16>(NumberOfShelfEdgeLabelsProperty); }
            set { SetProperty<Int16>(NumberOfShelfEdgeLabelsProperty, value); }
        }

        #endregion

        #region IsMoveable

        /// <summary>
        /// IsMoveable property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsMoveableProperty =
            RegisterModelProperty<Boolean>(c => c.IsMoveable, Message.FixtureComponent_IsMoveable);

        /// <summary>
        /// Gets/Sets the IsMoveable value
        /// </summary>
        public Boolean IsMoveable
        {
            get { return GetProperty<Boolean>(IsMoveableProperty); }
            set { SetProperty<Boolean>(IsMoveableProperty, value); }
        }

        #endregion

        #region IsDisplayOnly

        /// <summary>
        /// IsDisplayOnly property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsDisplayOnlyProperty =
            RegisterModelProperty<Boolean>(c => c.IsDisplayOnly, Message.FixtureComponent_IsDisplayOnly);

        /// <summary>
        /// Gets/Sets the IsDisplayOnly value
        /// </summary>
        public Boolean IsDisplayOnly
        {
            get { return GetProperty<Boolean>(IsDisplayOnlyProperty); }
            set { SetProperty<Boolean>(IsDisplayOnlyProperty, value); }
        }

        #endregion

        #region CanAttachShelfEdgeLabel

        /// <summary>
        /// CanAttachShelfEdgeLabel property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> CanAttachShelfEdgeLabelProperty =
            RegisterModelProperty<Boolean>(c => c.CanAttachShelfEdgeLabel, Message.FixtureComponent_CanAttachShelfEdgeLabel);

        /// <summary>
        /// Gets/Sets the CanAttachShelfEdgeLabel value
        /// </summary>
        public Boolean CanAttachShelfEdgeLabel
        {
            get { return GetProperty<Boolean>(CanAttachShelfEdgeLabelProperty); }
            set { SetProperty<Boolean>(CanAttachShelfEdgeLabelProperty, value); }
        }

        #endregion

        #region RetailerReferenceCode

        /// <summary>
        /// RetailerReferenceCode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> RetailerReferenceCodeProperty =
            RegisterModelProperty<String>(c => c.RetailerReferenceCode, Message.FixtureComponent_RetailerReferenceCode);

        /// <summary>
        /// Gets/Sets the RetailerReferenceCode value
        /// </summary>
        public String RetailerReferenceCode
        {
            get { return GetProperty<String>(RetailerReferenceCodeProperty); }
            set { SetProperty<String>(RetailerReferenceCodeProperty, value); }
        }

        #endregion

        #region BarCode

        /// <summary>
        /// BarCode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> BarCodeProperty =
            RegisterModelProperty<String>(c => c.BarCode, Message.FixtureComponent_Barcode);

        /// <summary>
        /// Gets/Sets the BarCode value
        /// </summary>
        public String BarCode
        {
            get { return GetProperty<String>(BarCodeProperty); }
            set { SetProperty<String>(BarCodeProperty, value); }
        }

        #endregion

        #region Manufacturer

        /// <summary>
        /// Manufacturer property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ManufacturerProperty =
            RegisterModelProperty<String>(c => c.Manufacturer, Message.FixtureComponent_Manufacturer);

        /// <summary>
        /// Gets/Sets the Manufacturer value
        /// </summary>
        public String Manufacturer
        {
            get { return GetProperty<String>(ManufacturerProperty); }
            set { SetProperty<String>(ManufacturerProperty, value); }
        }

        #endregion

        #region ManufacturerPartName

        /// <summary>
        /// ManufacturerPartName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ManufacturerPartNameProperty =
            RegisterModelProperty<String>(c => c.ManufacturerPartName, Message.FixtureComponent_ManufacturerPartName);

        /// <summary>
        /// Gets/Sets the ManufacturerPartName value
        /// </summary>
        public String ManufacturerPartName
        {
            get { return GetProperty<String>(ManufacturerPartNameProperty); }
            set { SetProperty<String>(ManufacturerPartNameProperty, value); }
        }

        #endregion

        #region ManufacturerPartNumber

        /// <summary>
        /// ManufacturerPartNumber property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ManufacturerPartNumberProperty =
            RegisterModelProperty<String>(c => c.ManufacturerPartNumber, Message.FixtureComponent_ManufacturerPartNumber);

        /// <summary>
        /// Gets/Sets the ManufacturerPartNumber value
        /// </summary>
        public String ManufacturerPartNumber
        {
            get { return GetProperty<String>(ManufacturerPartNumberProperty); }
            set { SetProperty<String>(ManufacturerPartNumberProperty, value); }
        }

        #endregion

        #region SupplierName

        /// <summary>
        /// SupplierName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> SupplierNameProperty =
            RegisterModelProperty<String>(c => c.SupplierName, Message.FixtureComponent_SupplierName);

        /// <summary>
        /// Gets/Sets the SupplierName value
        /// </summary>
        public String SupplierName
        {
            get { return GetProperty<String>(SupplierNameProperty); }
            set { SetProperty<String>(SupplierNameProperty, value); }
        }

        #endregion

        #region SupplierPartNumber

        /// <summary>
        /// SupplierPartNumber property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> SupplierPartNumberProperty =
            RegisterModelProperty<String>(c => c.SupplierPartNumber, Message.FixtureComponent_SupplierPartNumber);

        /// <summary>
        /// Gets/Sets the SupplierPartNumber value
        /// </summary>
        public String SupplierPartNumber
        {
            get { return GetProperty<String>(SupplierPartNumberProperty); }
            set { SetProperty<String>(SupplierPartNumberProperty, value); }
        }

        #endregion

        #region SupplierCostPrice

        /// <summary>
        /// SupplierCostPrice property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> SupplierCostPriceProperty =
            RegisterModelProperty<Single?>(c => c.SupplierCostPrice, Message.FixtureComponent_SupplierCostPrice, ModelPropertyDisplayType.Currency);

        /// <summary>
        /// Gets/Sets the SupplierCostPrice value
        /// </summary>
        public Single? SupplierCostPrice
        {
            get { return GetProperty<Single?>(SupplierCostPriceProperty); }
            set { SetProperty<Single?>(SupplierCostPriceProperty, value); }
        }

        #endregion

        #region SupplierDiscount

        /// <summary>
        /// SupplierDiscount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> SupplierDiscountProperty =
            RegisterModelProperty<Single?>(c => c.SupplierDiscount, Message.FixtureComponent_SupplierDiscount, ModelPropertyDisplayType.Currency);

        /// <summary>
        /// Gets/Sets the SupplierDiscount value
        /// </summary>
        public Single? SupplierDiscount
        {
            get { return GetProperty<Single?>(SupplierDiscountProperty); }
            set { SetProperty<Single?>(SupplierDiscountProperty, value); }
        }

        #endregion

        #region SupplierLeadTime

        /// <summary>
        /// SupplierLeadTime property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> SupplierLeadTimeProperty =
            RegisterModelProperty<Single?>(c => c.SupplierLeadTime, Message.FixtureComponent_SupplierLeadTime);

        /// <summary>
        /// Gets/Sets the SupplierLeadTime value
        /// </summary>
        public Single? SupplierLeadTime
        {
            get { return GetProperty<Single?>(SupplierLeadTimeProperty); }
            set { SetProperty<Single?>(SupplierLeadTimeProperty, value); }
        }

        #endregion

        #region MinPurchaseQty

        /// <summary>
        /// MinPurchaseQty property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MinPurchaseQtyProperty =
            RegisterModelProperty<Int32?>(c => c.MinPurchaseQty, Message.FixtureComponent_MinPurchaseQty);

        /// <summary>
        /// Gets/Sets the MinPurchaseQty value
        /// </summary>
        public Int32? MinPurchaseQty
        {
            get { return GetProperty<Int32?>(MinPurchaseQtyProperty); }
            set { SetProperty<Int32?>(MinPurchaseQtyProperty, value); }
        }

        #endregion

        #region WeightLimit

        /// <summary>
        /// WeightLimit property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> WeightLimitProperty =
            RegisterModelProperty<Single?>(c => c.WeightLimit, Message.FixtureComponent_WeightLimit);

        /// <summary>
        /// Gets/Sets the WeightLimit value
        /// </summary>
        public Single? WeightLimit
        {
            get { return GetProperty<Single?>(WeightLimitProperty); }
            set { SetProperty<Single?>(WeightLimitProperty, value); }
        }

        #endregion

        #region Weight

        /// <summary>
        /// Weight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> WeightProperty =
            RegisterModelProperty<Single?>(c => c.Weight, Message.FixtureComponent_Weight);

        /// <summary>
        /// Gets/Sets the Weight value
        /// </summary>
        public Single? Weight
        {
            get { return GetProperty<Single?>(WeightProperty); }
            set { SetProperty<Single?>(WeightProperty, value); }
        }

        #endregion

        #region Volume

        /// <summary>
        /// Volume property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> VolumeProperty =
            RegisterModelProperty<Single?>(c => c.Volume, Message.FixtureComponent_Volume);

        /// <summary>
        /// Gets/Sets the Volume value
        /// </summary>
        public Single? Volume
        {
            get { return GetProperty<Single?>(VolumeProperty); }
            set { SetProperty<Single?>(VolumeProperty, value); }
        }

        #endregion

        #region Diameter

        /// <summary>
        /// Diameter property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> DiameterProperty =
            RegisterModelProperty<Single?>(c => c.Diameter, Message.FixtureComponent_Diameter);

        /// <summary>
        /// Gets/Sets the Diameter value
        /// </summary>
        public Single? Diameter
        {
            get { return GetProperty<Single?>(DiameterProperty); }
            set { SetProperty<Single?>(DiameterProperty, value); }
        }

        #endregion

        #region Capacity

        /// <summary>
        /// Capacity property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> CapacityProperty =
            RegisterModelProperty<Int16?>(c => c.Capacity, Message.FixtureComponent_Capacity);

        /// <summary>
        /// Gets/Sets the Capacity value
        /// </summary>
        public Int16? Capacity
        {
            get { return GetProperty<Int16?>(CapacityProperty); }
            set { SetProperty<Int16?>(CapacityProperty, value); }
        }

        #endregion

        #region ComponentType

        /// <summary>
        /// ComponentType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureComponentType> ComponentTypeProperty =
            RegisterModelProperty<FixtureComponentType>(c => c.ComponentType, Message.FixtureComponent_ComponentType);

        /// <summary>
        /// Gets/Sets the ComponentType value
        /// </summary>
        public FixtureComponentType ComponentType
        {
            get { return GetProperty<FixtureComponentType>(ComponentTypeProperty); }
            set { SetProperty<FixtureComponentType>(ComponentTypeProperty, value); }
        }

        #endregion

        #region IsMerchandisedTopDown

        /// <summary>
        /// Is Merchandised Top Down
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsMerchandisedTopDownProperty =
            RegisterModelProperty<Boolean>(c => c.IsMerchandisedTopDown, Message.FixtureComponent_IsMerchandisedTopDown);

        /// <summary>
        /// Gets/Sets whether this component is merchandised 
        /// looking at it from the top.
        /// </summary>
        public Boolean IsMerchandisedTopDown
        {
            get { return GetProperty<Boolean>(IsMerchandisedTopDownProperty); }
            set { SetProperty<Boolean>(IsMerchandisedTopDownProperty, value); }
        }

        #endregion

        #region SubComponents
        /// <summary>
        /// SubComponents property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureSubComponentList> SubComponentsProperty =
            RegisterModelProperty<FixtureSubComponentList>(c => c.SubComponents, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Returns the SubComponents within this package
        /// </summary>
        public FixtureSubComponentList SubComponents
        {
            get
            {
                return this.GetPropertyLazy<FixtureSubComponentList>(
                    SubComponentsProperty,
                    new FixtureSubComponentList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// Asynchronously returns the SubComponents within this package
        /// </summary>
        public FixtureSubComponentList SubComponentsAsync
        {
            get
            {
                return this.GetPropertyLazy<FixtureSubComponentList>(
                    SubComponentsProperty,
                    new FixtureSubComponentList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));
            BusinessRules.AddRule(new MaxLength(RetailerReferenceCodeProperty, 50));
            BusinessRules.AddRule(new MaxLength(BarCodeProperty, 13));
            BusinessRules.AddRule(new MaxLength(ManufacturerProperty, 50));
            BusinessRules.AddRule(new MaxLength(ManufacturerPartNameProperty, 50));
            BusinessRules.AddRule(new MaxLength(ManufacturerPartNumberProperty, 50));
            BusinessRules.AddRule(new MaxLength(SupplierNameProperty, 50));
            BusinessRules.AddRule(new MaxLength(SupplierPartNumberProperty, 50));

        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static FixtureComponent NewFixtureComponent()
        {
            FixtureComponent item = new FixtureComponent();
            item.Create();
            return item;
        }
        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static FixtureComponent NewFixtureComponent(PlanogramComponent planComponent, FixturePackage parentPackage)
        {
            FixtureComponent item = new FixtureComponent();
            item.Create(planComponent, parentPackage);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<FixtureSubComponentList>(SubComponentsProperty, FixtureSubComponentList.NewFixtureSubComponentList());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create(PlanogramComponent planComponent, FixturePackage parentPackage)
        {
            LoadProperty<Int32>(IdProperty, (Int32)planComponent.Id);
            LoadProperty<String>(NameProperty, planComponent.Name);
            LoadProperty<Single>(HeightProperty, planComponent.Height);
            LoadProperty<Single>(WidthProperty, planComponent.Width);
            LoadProperty<Single>(DepthProperty, planComponent.Depth);

            if (planComponent.MetaNumberOfSubComponents.HasValue)
                LoadProperty<Int16>(NumberOfSubComponentsProperty, planComponent.MetaNumberOfSubComponents.Value);

            if (planComponent.MetaNumberOfMerchandisedSubComponents.HasValue)
                LoadProperty<Int16>(NumberOfMerchandisedSubComponentsProperty, planComponent.MetaNumberOfMerchandisedSubComponents.Value);

            LoadProperty<Boolean>(IsMoveableProperty, planComponent.IsMoveable);
            LoadProperty<Boolean>(IsDisplayOnlyProperty, planComponent.IsDisplayOnly);
            LoadProperty<Boolean>(CanAttachShelfEdgeLabelProperty, planComponent.CanAttachShelfEdgeLabel);
            LoadProperty<String>(RetailerReferenceCodeProperty, planComponent.RetailerReferenceCode);
            LoadProperty<String>(BarCodeProperty, planComponent.BarCode);
            LoadProperty<String>(ManufacturerProperty, planComponent.Manufacturer);
            LoadProperty<String>(ManufacturerPartNameProperty, planComponent.ManufacturerPartName);
            LoadProperty<String>(ManufacturerPartNumberProperty, planComponent.ManufacturerPartNumber);
            LoadProperty<String>(SupplierNameProperty, planComponent.SupplierName);
            LoadProperty<String>(SupplierPartNumberProperty, planComponent.SupplierPartNumber);
            LoadProperty<Single?>(SupplierCostPriceProperty, planComponent.SupplierCostPrice);
            LoadProperty<Single?>(SupplierDiscountProperty, planComponent.SupplierDiscount);
            LoadProperty<Single?>(SupplierLeadTimeProperty, planComponent.SupplierLeadTime);
            LoadProperty<Int32?>(MinPurchaseQtyProperty, planComponent.MinPurchaseQty);
            LoadProperty<Single?>(WeightLimitProperty, planComponent.WeightLimit);
            LoadProperty<Single?>(WeightProperty, planComponent.Weight);
            LoadProperty<Single?>(VolumeProperty, planComponent.Volume);
            LoadProperty<Single?>(DiameterProperty, planComponent.Diameter);
            LoadProperty<Int16?>(CapacityProperty, planComponent.Capacity);
            LoadProperty<FixtureComponentType>(ComponentTypeProperty, FixtureComponentTypeHelper.Parse(planComponent.ComponentType.ToString()));

           

            //copy any images required by the component.
            if (parentPackage != null)
            {
                parentPackage.AddImages(
                    new Int32?[]
                    {
                        (Int32?)planComponent.ImageIdFront, 
                        (Int32?)planComponent.ImageIdBack,
                        (Int32?)planComponent.ImageIdTop, 
                        (Int32?)planComponent.ImageIdBottom, 
                        (Int32?)planComponent.ImageIdLeft,
                        (Int32?) planComponent.ImageIdRight

                    }, planComponent.Parent);
            }
            LoadProperty<Int32?>(ImageIdFrontProperty, planComponent.ImageIdFront as Int32?);
            LoadProperty<Int32?>(ImageIdBackProperty, planComponent.ImageIdBack as Int32?);
            LoadProperty<Int32?>(ImageIdTopProperty, planComponent.ImageIdTop as Int32?);
            LoadProperty<Int32?>(ImageIdBottomProperty, planComponent.ImageIdBottom as Int32?);
            LoadProperty<Int32?>(ImageIdLeftProperty, planComponent.ImageIdLeft as Int32?);
            LoadProperty<Int32?>(ImageIdRightProperty, planComponent.ImageIdRight as Int32?);


            //subcomponents:
            this.LoadProperty<FixtureSubComponentList>(SubComponentsProperty,
                FixtureSubComponentList.NewFixtureSubComponentList(planComponent.SubComponents, parentPackage));

            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<FixtureComponent>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes on this instance
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }

        /// <summary>
        /// Called when resolving ids on this instance
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // ImageIdFront
            Object imageIdFront = context.ResolveId<FixtureImage>(this.ReadProperty<Int32?>(ImageIdFrontProperty));
            if (imageIdFront != null) this.LoadProperty<Int32?>(ImageIdFrontProperty, (Int32?)imageIdFront);

            // ImageIdBack
            Object imageIdBack = context.ResolveId<FixtureImage>(this.ReadProperty<Int32?>(ImageIdBackProperty));
            if (imageIdBack != null) this.LoadProperty<Int32?>(ImageIdBackProperty, (Int32?)imageIdBack);

            // ImageIdTop
            Object imageIdTop = context.ResolveId<FixtureImage>(this.ReadProperty<Int32?>(ImageIdTopProperty));
            if (imageIdTop != null) this.LoadProperty<Int32?>(ImageIdTopProperty, (Int32?)imageIdTop);

            // ImageIdBottom
            Object imageIdBottom = context.ResolveId<FixtureImage>(this.ReadProperty<Int32?>(ImageIdBottomProperty));
            if (imageIdBottom != null) this.LoadProperty<Int32?>(ImageIdBottomProperty, (Int32?)imageIdBottom);

            // ImageIdLeft
            Object imageIdLeft = context.ResolveId<FixtureImage>(this.ReadProperty<Int32?>(ImageIdLeftProperty));
            if (imageIdLeft != null) this.LoadProperty<Int32?>(ImageIdLeftProperty, (Int32?)imageIdLeft);

            // ImageIdRight
            Object imageIdRight = context.ResolveId<FixtureImage>(this.ReadProperty<Int32?>(ImageIdRightProperty));
            if (imageIdRight != null) this.LoadProperty<Int32?>(ImageIdRightProperty, (Int32?)imageIdRight);
        }

        /// <summary>
        /// Adds a new component to the plan or updates the existing one.
        /// </summary>
        /// <param name="plan"></param>
        /// <returns></returns>
        public PlanogramComponent AddOrUpdatePlanogramComponent(Planogram plan)
        {
            PlanogramComponent planComponent = plan.Components.FindById(this.Id);
            if (planComponent == null)
            {
                planComponent = PlanogramComponent.NewPlanogramComponent();
                planComponent.Id = this.Id;
                plan.Components.Add(planComponent);
            }

            //update values
            planComponent.Name = this.Name;
            planComponent.Height = this.Height;
            planComponent.Width = this.Width;
            planComponent.Depth = this.Depth;
            planComponent.IsMoveable = this.IsMoveable;
            planComponent.IsDisplayOnly = this.IsDisplayOnly;
            planComponent.CanAttachShelfEdgeLabel = this.CanAttachShelfEdgeLabel;
            planComponent.RetailerReferenceCode = this.RetailerReferenceCode;
            planComponent.BarCode = this.BarCode;
            planComponent.Manufacturer = this.Manufacturer;
            planComponent.ManufacturerPartName = this.ManufacturerPartName;
            planComponent.ManufacturerPartNumber = this.ManufacturerPartNumber;
            planComponent.SupplierName = this.SupplierName;
            planComponent.SupplierPartNumber = this.SupplierPartNumber;
            planComponent.SupplierCostPrice = this.SupplierCostPrice;
            planComponent.SupplierDiscount = this.SupplierDiscount;
            planComponent.SupplierLeadTime = this.SupplierLeadTime;
            planComponent.MinPurchaseQty = this.MinPurchaseQty;
            planComponent.WeightLimit = this.WeightLimit;
            planComponent.Weight = this.Weight;
            planComponent.Volume = this.Volume;
            planComponent.Diameter = this.Diameter;
            planComponent.Capacity = this.Capacity;
            planComponent.ComponentType = (PlanogramComponentType)this.ComponentType;
           

            //images
            planComponent.ImageIdFront = this.ImageIdFront;
            planComponent.ImageIdBack = this.ImageIdBack;
            planComponent.ImageIdTop = this.ImageIdTop;
            planComponent.ImageIdBottom = this.ImageIdBottom;
            planComponent.ImageIdLeft = this.ImageIdLeft;
            planComponent.ImageIdRight = this.ImageIdRight;
            foreach(Object imgId in EnumerateImageIds())
            {
                FixtureImage img = this.Parent.Images.FindById(imgId);
                if (img == null) continue;
                img.AddOrUpdatePlanogramImage(plan);
            }

            //subcomponents
            List<PlanogramSubComponent> updatedSubs = new List<PlanogramSubComponent>(this.SubComponents.Count);
            foreach (FixtureSubComponent fixtureSubComponent in this.SubComponents)
            {
                updatedSubs.Add(fixtureSubComponent.AddOrUpdatePlanogramSubComponent(planComponent));
            }
            planComponent.SubComponents.RemoveList(planComponent.SubComponents.Except(updatedSubs).ToArray());

            return planComponent;
        }

        /// <summary>
        /// Enumerates through all associated image ids.
        /// </summary>
        public IEnumerable<Object> EnumerateImageIds()
        {
            if (ImageIdFront != null) yield return ImageIdFront;
            if (ImageIdLeft != null) yield return ImageIdLeft;
            if (ImageIdTop != null) yield return ImageIdTop;
            if (ImageIdBack != null) yield return ImageIdBack;
            if (ImageIdRight != null) yield return ImageIdRight;
            if (ImageIdBottom != null) yield return ImageIdBottom;
        }

        #endregion
    }
}