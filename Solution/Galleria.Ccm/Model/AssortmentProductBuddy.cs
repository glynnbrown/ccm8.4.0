﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
// V8-32396 : A.Probyn
//  Removed obsolete properties
#endregion

#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using AutoMapper;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Rules;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A class representing an Assortment Product Buddy within CCM
    /// (Child of Assortment)
    /// This is a child Content object and so it has the following properties/actions:
    /// - This object does NOT have a DateDeleted property or DateDeleted field in its supporting database table
    /// - if its parent is deleted, it is not removed or marked as deleted (this allows for parent 'undeletes')
    /// - if it is deleted directly, it's associated record is removed from the database. 
    /// </summary>
    [Serializable]
    [DefaultNewMethod("NewAssortmentProductBuddy")]
    public partial class AssortmentProductBuddy : ModelObject<AssortmentProductBuddy>, IPlanogramAssortmentProductBuddy
    {
        #region Static Constructor
        static AssortmentProductBuddy()
        {
        }
        #endregion

        #region Properties

        #region Parent

        /// <summary>
        /// Returns the parent assortment
        /// </summary>
        public Assortment Parent
        {
            get
            {
                AssortmentProductBuddyList listParent = base.Parent as AssortmentProductBuddyList;
                if (listParent != null)
                {
                    return listParent.Parent;
                }
                return null;
            }
        }

        #endregion

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The AssortmentRegion id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> ProductIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductId);
        /// <summary>
        /// The Product Id
        /// </summary>
        public Int32 ProductId
        {
            get { return GetProperty<Int32>(ProductIdProperty); }
            set { SetProperty<Int32>(ProductIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> ProductGtinProperty =
           RegisterModelProperty<String>(c => c.ProductGtin);
        /// <summary>
        /// The product Gtin
        /// </summary>
        public String ProductGtin
        {
            get { return GetProperty<String>(ProductGtinProperty); }
            set { SetProperty<String>(ProductGtinProperty, value); }
        }

        public static ModelPropertyInfo<PlanogramAssortmentProductBuddySourceType> SourceTypeProperty =
        RegisterModelProperty<PlanogramAssortmentProductBuddySourceType>(c => c.SourceType);
        public PlanogramAssortmentProductBuddySourceType SourceType
        {
            get { return GetProperty<PlanogramAssortmentProductBuddySourceType>(SourceTypeProperty); }
            set { SetProperty<PlanogramAssortmentProductBuddySourceType>(SourceTypeProperty, value); }
        }

        public static ModelPropertyInfo<PlanogramAssortmentProductBuddyTreatmentType> TreatmentTypeProperty =
        RegisterModelProperty<PlanogramAssortmentProductBuddyTreatmentType>(c => c.TreatmentType);
        public PlanogramAssortmentProductBuddyTreatmentType TreatmentType
        {
            get { return GetProperty<PlanogramAssortmentProductBuddyTreatmentType>(TreatmentTypeProperty); }
            set { SetProperty<PlanogramAssortmentProductBuddyTreatmentType>(TreatmentTypeProperty, value); }
        }

        public static ModelPropertyInfo<Single> TreatmentTypePercentageProperty =
        RegisterModelProperty<Single>(c => c.TreatmentTypePercentage);
        public Single TreatmentTypePercentage
        {
            get { return GetProperty<Single>(TreatmentTypePercentageProperty); }
            set { SetProperty<Single>(TreatmentTypePercentageProperty, value); }
        }

        public static ModelPropertyInfo<PlanogramAssortmentProductBuddyProductAttributeType> ProductAttributeTypeProperty =
        RegisterModelProperty<PlanogramAssortmentProductBuddyProductAttributeType>(c => c.ProductAttributeType);
        public PlanogramAssortmentProductBuddyProductAttributeType ProductAttributeType
        {
            get { return GetProperty<PlanogramAssortmentProductBuddyProductAttributeType>(ProductAttributeTypeProperty); }
            set { SetProperty<PlanogramAssortmentProductBuddyProductAttributeType>(ProductAttributeTypeProperty, value); }
        }

        #region S1

        public static readonly ModelPropertyInfo<Int32?> S1ProductIdProperty =
            RegisterModelProperty<Int32?>(c => c.S1ProductId);
        /// <summary>
        /// The ProductId of the source Product 1
        /// </summary>
        public Int32? S1ProductId
        {
            get { return GetProperty<Int32?>(S1ProductIdProperty); }
            set { SetProperty<Int32?>(S1ProductIdProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> S1ProductGtinProperty =
           RegisterModelProperty<String>(c => c.S1ProductGtin);
        /// <summary>
        /// The source 1 product Gtin
        /// </summary>
        public String S1ProductGtin
        {
            get { return GetProperty<String>(S1ProductGtinProperty); }
            set { SetProperty<String>(S1ProductGtinProperty, value); }
        }

        public static readonly ModelPropertyInfo<Single?> S1PercentageProperty =
        RegisterModelProperty<Single?>(c => c.S1Percentage);
        public Single? S1Percentage
        {
            get { return GetProperty<Single?>(S1PercentageProperty); }
            set { SetProperty<Single?>(S1PercentageProperty, value); }
        }

        #endregion

        #region S2

        public static readonly ModelPropertyInfo<Int32?> S2ProductIdProperty =
            RegisterModelProperty<Int32?>(c => c.S2ProductId);
        /// <summary>
        /// The ProductId of the source Product 2
        /// </summary>
        public Int32? S2ProductId
        {
            get { return GetProperty<Int32?>(S2ProductIdProperty); }
            set { SetProperty<Int32?>(S2ProductIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> S2ProductGtinProperty =
           RegisterModelProperty<String>(c => c.S2ProductGtin);
        /// <summary>
        /// The source 2 product Gtin
        /// </summary>
        public String S2ProductGtin
        {
            get { return GetProperty<String>(S2ProductGtinProperty); }
            set { SetProperty<String>(S2ProductGtinProperty, value); }
        }

        public static readonly ModelPropertyInfo<Single?> S2PercentageProperty =
        RegisterModelProperty<Single?>(c => c.S2Percentage);
        public Single? S2Percentage
        {
            get { return GetProperty<Single?>(S2PercentageProperty); }
            set { SetProperty<Single?>(S2PercentageProperty, value); }
        }

        #endregion

        #region S3

        public static readonly ModelPropertyInfo<Int32?> S3ProductIdProperty =
            RegisterModelProperty<Int32?>(c => c.S3ProductId);
        /// <summary>
        /// The ProductId of the source Product 3
        /// </summary>
        public Int32? S3ProductId
        {
            get { return GetProperty<Int32?>(S3ProductIdProperty); }
            set { SetProperty<Int32?>(S3ProductIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> S3ProductGtinProperty =
           RegisterModelProperty<String>(c => c.S3ProductGtin);
        /// <summary>
        /// The source 3 product Gtin
        /// </summary>
        public String S3ProductGtin
        {
            get { return GetProperty<String>(S3ProductGtinProperty); }
            set { SetProperty<String>(S3ProductGtinProperty, value); }
        }

        public static readonly ModelPropertyInfo<Single?> S3PercentageProperty =
        RegisterModelProperty<Single?>(c => c.S3Percentage);
        public Single? S3Percentage
        {
            get { return GetProperty<Single?>(S3PercentageProperty); }
            set { SetProperty<Single?>(S3PercentageProperty, value); }
        }

        #endregion

        #region S4

        public static readonly ModelPropertyInfo<Int32?> S4ProductIdProperty =
            RegisterModelProperty<Int32?>(c => c.S4ProductId);
        /// <summary>
        /// The ProductId of the source Product 4
        /// </summary>
        public Int32? S4ProductId
        {
            get { return GetProperty<Int32?>(S4ProductIdProperty); }
            set { SetProperty<Int32?>(S4ProductIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> S4ProductGtinProperty =
           RegisterModelProperty<String>(c => c.S4ProductGtin);
        /// <summary>
        /// The source 4 product Gtin
        /// </summary>
        public String S4ProductGtin
        {
            get { return GetProperty<String>(S4ProductGtinProperty); }
            set { SetProperty<String>(S4ProductGtinProperty, value); }
        }

        public static readonly ModelPropertyInfo<Single?> S4PercentageProperty =
        RegisterModelProperty<Single?>(c => c.S4Percentage);
        public Single? S4Percentage
        {
            get { return GetProperty<Single?>(S4PercentageProperty); }
            set { SetProperty<Single?>(S4PercentageProperty, value); }
        }

        #endregion

        #region S5

        public static readonly ModelPropertyInfo<Int32?> S5ProductIdProperty =
            RegisterModelProperty<Int32?>(c => c.S5ProductId);
        /// <summary>
        /// The ProductId of the source Product 5
        /// </summary>
        public Int32? S5ProductId
        {
            get { return GetProperty<Int32?>(S5ProductIdProperty); }
            set { SetProperty<Int32?>(S5ProductIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> S5ProductGtinProperty =
           RegisterModelProperty<String>(c => c.S5ProductGtin);
        /// <summary>
        /// The source 5 product Gtin
        /// </summary>
        public String S5ProductGtin
        {
            get { return GetProperty<String>(S5ProductGtinProperty); }
            set { SetProperty<String>(S5ProductGtinProperty, value); }
        }

        public static readonly ModelPropertyInfo<Single?> S5PercentageProperty =
        RegisterModelProperty<Single?>(c => c.S5Percentage);
        public Single? S5Percentage
        {
            get { return GetProperty<Single?>(S5PercentageProperty); }
            set { SetProperty<Single?>(S5PercentageProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {

            base.AddBusinessRules();

            BusinessRules.AddRule(new NullableMaxValue<Single>(S1PercentageProperty, 10));
            BusinessRules.AddRule(new NullableMaxValue<Single>(S2PercentageProperty, 10));
            BusinessRules.AddRule(new NullableMaxValue<Single>(S3PercentageProperty, 10));
            BusinessRules.AddRule(new NullableMaxValue<Single>(S4PercentageProperty, 10));
            BusinessRules.AddRule(new NullableMaxValue<Single>(S5PercentageProperty, 10));
            BusinessRules.AddRule(new NullableMinValue<Single>(S1PercentageProperty, 0.01f));
            BusinessRules.AddRule(new NullableMinValue<Single>(S2PercentageProperty, 0.01f));
            BusinessRules.AddRule(new NullableMinValue<Single>(S3PercentageProperty, 0.01f));
            BusinessRules.AddRule(new NullableMinValue<Single>(S4PercentageProperty, 0.01f));
            BusinessRules.AddRule(new NullableMinValue<Single>(S5PercentageProperty, 0.01f));
            BusinessRules.AddRule(new MaxLength(S1PercentageProperty, 4));
            BusinessRules.AddRule(new MaxLength(S2PercentageProperty, 4));
            BusinessRules.AddRule(new MaxLength(S3PercentageProperty, 4));
            BusinessRules.AddRule(new MaxLength(S4PercentageProperty, 4));
            BusinessRules.AddRule(new MaxLength(S5PercentageProperty, 4));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentProductBuddy), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.AssortmentCreate.ToString()));
            BusinessRules.AddRule(typeof(AssortmentProductBuddy), new IsInRole(AuthorizationActions.GetObject, DomainPermission.AssortmentGet.ToString()));
            BusinessRules.AddRule(typeof(AssortmentProductBuddy), new IsInRole(AuthorizationActions.EditObject, DomainPermission.AssortmentEdit.ToString()));
            BusinessRules.AddRule(typeof(AssortmentProductBuddy), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.AssortmentDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentProductBuddy NewAssortmentProductBuddy()
        {
            AssortmentProductBuddy item = new AssortmentProductBuddy();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentProductBuddy NewAssortmentProductBuddy(AssortmentProduct product)
        {
            AssortmentProductBuddy item = new AssortmentProductBuddy();
            item.Create(product);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create(AssortmentProduct product)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(ProductIdProperty, product.ProductId);
            this.LoadProperty<String>(ProductGtinProperty, product.Gtin);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion
    }
}
