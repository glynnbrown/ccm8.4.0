﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-V8-24261 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public sealed partial class RecentPlanogramList
    {
        #region Constructor
        private RecentPlanogramList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of all items
        /// </summary>
        /// <param name="filenames"></param>
        /// <returns></returns>
        public static RecentPlanogramList FetchAll()
        {
            return DataPortal.Fetch<RecentPlanogramList>(new FetchAllCriteria());
        }

        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returing all items for a given parent id
        /// </summary>
        private void DataPortal_Fetch(FetchAllCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = this.GetDalFactory(Constants.UserDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IRecentPlanogramDal dal = dalContext.GetDal<IRecentPlanogramDal>())
                {
                    IEnumerable<RecentPlanogramDto> dtoList = dal.FetchAll();
                    foreach (RecentPlanogramDto dto in dtoList)
                    {
                        this.Add(RecentPlanogram.Fetch(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #region Update
     
        /// <summary>
        /// Called when this list is being updated
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory(Constants.UserDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                Child_Update(dalContext);
                dalContext.Commit();
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Updates the IsForCurrentRepository status flag for all items in this list.
        /// </summary>
        /// <param name="server">the name of the current server</param>
        /// <param name="dbName">the name of the current db</param>
        /// <param name="entityId">the name of the current entity id</param>
        public void UpdateIsCurrentRepositoryStatus(String server, String dbName, Int32 entityId)
        {
            foreach (RecentPlanogram r in this)
            {
                r.UpdateIsForCurrentRepository(server, dbName, entityId);
            }
        }

        #endregion
    }
}