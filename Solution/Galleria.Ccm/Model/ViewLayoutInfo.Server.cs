﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public partial class ViewLayoutInfo
    {
        #region Constructors
        private ViewLayoutInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static ViewLayoutInfo GetViewLayoutInfo(IDalContext dalContext, ViewLayoutInfoDto dto)
        {
            return DataPortal.FetchChild<ViewLayoutInfo>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, ViewLayoutInfoDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<String>(NameProperty, dto.Name);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when loading this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ViewLayoutInfoDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #endregion
    }
}
