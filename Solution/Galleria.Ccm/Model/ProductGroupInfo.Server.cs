﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using System.Collections.Generic;
using System.Linq;

namespace Galleria.Ccm.Model
{
    public partial class ProductGroupInfo
    {
        #region Constructors
        private ProductGroupInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static ProductGroupInfo Fetch(IDalContext dalContext, ProductGroupInfoDto dto)
        {
            return DataPortal.FetchChild<ProductGroupInfo>(dalContext, dto);
        }

        

        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static ProductGroupInfo FetchProductGroupInfo(IDalContext dalContext, ProductGroupInfoDto dto)
        {
            return DataPortal.FetchChild<ProductGroupInfo>(dalContext, dto);
        }

        #endregion

        #region Data Access

        

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, ProductGroupInfoDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(CodeProperty, dto.Code);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ProductGroupInfoDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #endregion

        #region Helpers

        /// <summary>
        /// Returns a single productgroup info by id.
        /// Uses the ProductGroupInfoList FetchByProductGroupIds.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ProductGroupInfo FetchById(Int32 id)
        {
            return ProductGroupInfoList.FetchByProductGroupIds(new List<Int32> { id }).FirstOrDefault();
        }

        #endregion
    }
}
