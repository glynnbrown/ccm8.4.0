﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM810)
// V8-29861 : A.Probyn
//	Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramEventLogInfoList
    {
        #region Constructors
        private PlanogramEventLogInfoList() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a list of all planogram event logs by workpackage id
        /// </summary>
        /// <returns></returns>
        public static PlanogramEventLogInfoList FetchByWorkpackageId(Int32 workpackageId)
        {
            return DataPortal.Fetch<PlanogramEventLogInfoList>(new FetchByWorkpackageIdCriteria(workpackageId));
        }

        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when retrieving a list of cluster scheme infos
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByWorkpackageIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramEventLogInfoDal dal = dalContext.GetDal<IPlanogramEventLogInfoDal>())
                {
                    IEnumerable<PlanogramEventLogInfoDto> dtoList = dal.FetchByWorkpackageId(criteria.WorkpackageId);
                    foreach (PlanogramEventLogInfoDto dto in dtoList)
                    {
                        this.Add(PlanogramEventLogInfo.GetPlanogramEventLogInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}
