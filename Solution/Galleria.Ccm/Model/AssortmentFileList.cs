﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentFileList : ModelList<AssortmentFileList, AssortmentFile>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentFileList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.AssortmentCreate.ToString()));
            BusinessRules.AddRule(typeof(AssortmentFileList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.AssortmentGet.ToString()));
            BusinessRules.AddRule(typeof(AssortmentFileList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.AssortmentEdit.ToString()));
            BusinessRules.AddRule(typeof(AssortmentFileList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.AssortmentDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A new list</returns>
        public static AssortmentFileList NewAssortmentFileList()
        {
            AssortmentFileList item = new AssortmentFileList();
            item.Create();
            return item;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds a file to this assortment
        /// </summary>
        /// <param name="file">The product to add</param>
        public void Add(Galleria.Ccm.Model.File file)
        {
            this.Add(AssortmentFile.NewAssortmentFile(file));
        }

        /// <summary>
        /// Adds a file to this assortment
        /// </summary>
        /// <param name="fileInfo">The file to add</param>
        public AssortmentFile Add(Galleria.Ccm.Model.FileInfo fileInfo)
        {
            AssortmentFile addedItem = AssortmentFile.NewAssortmentFile(fileInfo);
            this.Add(addedItem);
            return addedItem;
        }

        /// <summary>
        /// Adds a list of FileInfos to this assortment
        /// </summary>
        /// <param name="range"></param>
        public void AddList(IEnumerable<FileInfo> range)
        {
            this.RaiseListChangedEvents = false;

            //cycle through adding all the items
            List<AssortmentFile> addedItems = new List<AssortmentFile>();
            foreach (FileInfo element in range)
            {
                addedItems.Add(this.Add(element));
            }

            this.RaiseListChangedEvents = true;

            //raise out a bulk change notification
            NotifyBulkChange(new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, addedItems));
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}
