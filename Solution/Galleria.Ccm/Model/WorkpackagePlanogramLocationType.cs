﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.1.0)
// CCM-29811 : A.Probyn
//  Created 
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Workpackage Planogram Location Type Enum
    /// </summary>
    public enum WorkpackagePlanogramLocationType
    {
        InNewSubFolder = 0,
        InSourceFolder = 1
    }

    /// <summary>
    /// Helper class to provide internationaalized strings for each WorkpackagePlanogramLocationType
    /// </summary>
    public static class WorkpackagePlanogramLocationTypeHelper
    {
        public static readonly Dictionary<WorkpackagePlanogramLocationType, String> FriendlyNames =
            new Dictionary<WorkpackagePlanogramLocationType, String>()
            {
                {WorkpackagePlanogramLocationType.InNewSubFolder, Message.Enum_WorkpackagePlanogramLocationType_InNewSubFolder},
                {WorkpackagePlanogramLocationType.InSourceFolder, Message.Enum_WorkpackagePlanogramLocationType_InSourceFolder}
            };
    }
}

