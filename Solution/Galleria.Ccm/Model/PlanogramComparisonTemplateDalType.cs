#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Defines the allowed values to indicate the source of a <see cref="PlanogramComparisonTemplate" />.
    /// </summary>
    public enum PlanogramComparisonTemplateDalType
    {
        /// <summary>
        ///     The source for the <see cref="PlanogramComparisonTemplate" /> instance has not been set and is unknown.
        /// </summary>
        Unknown = 0,

        /// <summary>
        ///     The source for the <see cref="PlanogramComparisonTemplate" /> is the file system.
        /// </summary>
        FileSystem = 1
    }
}