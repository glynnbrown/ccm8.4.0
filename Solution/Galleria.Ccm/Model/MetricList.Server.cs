﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History:(CCM800)
//CCM-26158 : I.George
// Initial Version
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public sealed partial class MetricList
    {
        #region Constructor
        private MetricList() { } //force use of factory methods
        #endregion

        #region Factory Mehtods
        /// <summary>
        /// Returns a list of objects for the given entity
        /// List is a root object
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static MetricList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<MetricList>(new FetchByEntityIdCriteria(entityId));
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when loading this list from entity
        /// </summary>
        /// <param name="criteria">the current entity id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                //fetch all metric for the entity
                //IEnumerable<MetricDto> MetricDtoList;
                using (IMetricDal dal = dalContext.GetDal<IMetricDal>())
                {
                    IEnumerable<MetricDto> dtoList = dal.FetchByEntityId(criteria.EntityId);
                    //load the Metric
                    foreach (MetricDto dto in dtoList)
                    {
                        Add(Metric.FetchMetric(dalContext, dto));
                    }

                }
            }
            RaiseListChangedEvents = true;
        }
        #endregion

        #region Update
        /// <summary>
        /// called when this list is being updated
        /// (only when it is a root object)
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                Child_Update(dalContext);
                dalContext.Commit();
            }
        }
        #endregion
        #endregion
    }
}
