﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25879 : N.Haywood
//	Created (Auto-generated)
#endregion

#region Version History: (CCM 8.0.3)
// V8-28242 : M.Shelley
//  Added DateCommunicated and DateLive fields
#endregion

#region Version History: (CCM 8.1.1)
// V8-30422 : L.Ineson
//  Set modeldisplay types to datetime for date properties.
// V8-30463 : L.Ineson
//  Amended user properties to be ids for performance.
#endregion

#region Version History: (CCM 8.2.0)
// V8-30732 : M.Shelley
//  Added 2 new fields : OutputDestination and PlanPublishType
// V8-30508 : M.Shelley
//  Added a new property : PublishStatus
// V8-31408 : M.Shelley
//  Make the various date properties read-only and add accessor methods
#endregion

#endregion

using System;
using System.Collections.Generic;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// LocationPlanAssignment Model object
    /// </summary>
    [Serializable]
    public sealed partial class LocationPlanAssignment : ModelObject<LocationPlanAssignment>
    {
        #region Static Constructor
        static LocationPlanAssignment()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region PlanogramId

        /// <summary>
        /// PlanId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanogramIdProperty =
            RegisterModelProperty<Int32>(c => c.PlanogramId);

        /// <summary>
        /// Gets/Sets the PlanId value
        /// </summary>
        public Int32 PlanogramId
        {
            get { return GetProperty<Int32>(PlanogramIdProperty); }
            set { SetProperty<Int32>(PlanogramIdProperty, value); }
        }

        #endregion

        #region ProductGroupId

        /// <summary>
        /// ProductGroupId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProductGroupIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductGroupId);

        /// <summary>
        /// Gets/Sets the CategoryCode value
        /// </summary>
        public Int32 ProductGroupId
        {
            get { return GetProperty<Int32>(ProductGroupIdProperty); }
            set { SetProperty<Int32>(ProductGroupIdProperty, value); }
        }

        #endregion

        #region Location

        /// <summary>
        /// LocationId property defintion
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> LocationIdProperty =
            RegisterModelProperty<Int16>(c => c.LocationId);
        /// <summary>
        /// Gets/Sets the id of the location to which this assignment relates
        /// </summary>
        public Int16 LocationId
        {
            get { return this.GetProperty<Int16>(LocationIdProperty); }
            set { this.SetProperty<Int16>(LocationIdProperty, value); }
        }

        #endregion

        #region AssignedByUserId

        /// <summary>
        /// AssignedByUserId property defintion
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> AssignedByUserIdProperty =
            RegisterModelProperty<Int32?>(c => c.AssignedByUserId, Message.LocationPlanAssignment_AssignedByUser);
        /// <summary>
        /// Gets/Sets the id of the user who created this assignment
        /// </summary>
        public Int32? AssignedByUserId
        {
            get { return this.GetProperty<Int32?>(AssignedByUserIdProperty); }
            set { this.SetProperty<Int32?>(AssignedByUserIdProperty, value); }
        }

        #endregion

        #region PublishedByUserId

        /// <summary>
        /// PublishedByUserId property defintion
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> PublishedByUserIdProperty =
            RegisterModelProperty<Int32?>(c => c.PublishedByUserId, Message.LocationPlanAssignment_PublishedByUser);
        /// <summary>
        /// Gets/Sets the id of user who published this assignment
        /// </summary>
        public Int32? PublishedByUserId
        {
            get { return this.GetProperty<Int32?>(PublishedByUserIdProperty); }
            set { this.SetProperty<Int32?>(PublishedByUserIdProperty, value); }
        }

        #endregion

        #region DateAssigned

        /// <summary>
        /// DateAssigned property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateAssignedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateAssigned, Message.LocationPlanAssignment_DateAssigned,
            ModelPropertyDisplayType.Date);
        /// <summary>
        /// Gets/Sets the datetime on which the plan was assigned
        /// </summary>
        public DateTime? DateAssigned
        {
            get { return this.GetProperty<DateTime?>(DateAssignedProperty); }
            //set { this.SetProperty<DateTime?>(DateAssignedProperty, value); }
        }

        #endregion

        #region DatePublished

        /// <summary>
        /// DatePublished property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DatePublishedProperty =
            RegisterModelProperty<DateTime?>(c => c.DatePublished, Message.LocationPlanAssignment_DatePublished,
            ModelPropertyDisplayType.Date);
        /// <summary>
        ///  Gets/Sets the datetime on which the plan was published
        /// </summary>
        public DateTime? DatePublished
        {
            get { return this.GetProperty<DateTime?>(DatePublishedProperty); }
            //set { this.SetProperty<DateTime?>(DatePublishedProperty, value); }
        }

        #endregion

		#region DateCommunicated
		
		/// <summary>
        /// DateCommunicated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateCommunicatedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateCommunicated, Message.LocationPlanAssignment_DateCommunicated,
            ModelPropertyDisplayType.Date);
        /// <summary>
        /// Gets/Sets the datetime on which the plan was communicated
        /// </summary>
        public DateTime? DateCommunicated
        {
            get { return this.GetProperty<DateTime?>(DateCommunicatedProperty); }
            //set { this.SetProperty<DateTime?>(DateCommunicatedProperty, value); }
        }
				
        #endregion

        #region DateLive

        /// <summary>
        /// DateLive property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateLiveProperty =
            RegisterModelProperty<DateTime?>(c => c.DateLive, Message.LocationPlanAssignment_DateLive,
            ModelPropertyDisplayType.Date);
        /// <summary>
        /// Gets/Sets the datetime on which the plan made live
        /// </summary>
        public DateTime? DateLive
        {
            get { return this.GetProperty<DateTime?>(DateLiveProperty); }
            //set { this.SetProperty<DateTime?>(DateLiveProperty, value); }
        }

        #endregion

        #region OutputDestination property

        public static readonly ModelPropertyInfo<String> OutputDestinationProperty =
            RegisterModelProperty<String>(c => c.OutputDestination, Message.LocationPlanAssignment_OutputDestination);

        /// <summary>
        /// The output destination when a plan is published e.g. the GFS project name, the PDF output folder etc.
        /// </summary>
        public String OutputDestination
        {
            get { return this.GetProperty<String>(OutputDestinationProperty); }
            set { this.SetProperty<String>(OutputDestinationProperty, value); }
        }

        #endregion

        #region PublishType property

        public static readonly ModelPropertyInfo<PlanPublishType?> PublishTypeProperty =
            RegisterModelProperty<PlanPublishType?>(c => c.PublishType, Message.LocationPlanAssignment_PublishType);

        /// <summary>
        /// The plan publish type e.g. Publish to GFS, Exported to PDF etc.
        /// </summary>
        public PlanPublishType? PublishType
        {
            get { return this.GetProperty<PlanPublishType?>(PublishTypeProperty); }
            set { this.SetProperty<PlanPublishType?>(PublishTypeProperty, value); }
        }

        #endregion

        #region PublishStatus property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<PlanAssignmentPublishStatusType?> PublishStatusProperty =
            RegisterModelProperty<PlanAssignmentPublishStatusType?>(c => c.PublishStatus);
        /// <summary>
        /// 
        /// </summary>
        public PlanAssignmentPublishStatusType? PublishStatus
        {
            get { return this.GetProperty<PlanAssignmentPublishStatusType?>(PublishStatusProperty); }
            set { this.SetProperty<PlanAssignmentPublishStatusType?>(PublishStatusProperty, value); }
        }

        #endregion
				
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationPlanAssignment), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(LocationPlanAssignment), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationPlanAssignmentCreate.ToString()));
            BusinessRules.AddRule(typeof(LocationPlanAssignment), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationPlanAssignmentEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationPlanAssignment), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationPlanAssignmentDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
		/// Create a new LocationPlanAssignment given the product group id, planogram id and location id
        /// </summary>
		/// <param name="productGroupId">The product group id</param>
		/// <param name="planogramId">The planogram id</param>
		/// <param name="locationId">The location id</param>
		/// <returns>A new LocationPlanAssignment object</returns>
		public static LocationPlanAssignment NewLocationPlanAssignment(
			Int32 productGroupId, Int32 planogramId, Int16 locationId)
        {
            LocationPlanAssignment item = new LocationPlanAssignment();
			item.Create(productGroupId, planogramId, locationId);
			return item;
		}

		public static LocationPlanAssignment NewLocationPlanAssignment(
			Int32 productGroupId, Int32 planogramId, Int16 locationId, User assignUser, DateTime? currentDate)
		{
			LocationPlanAssignment item = new LocationPlanAssignment();
			item.Create(productGroupId, planogramId, locationId, assignUser, currentDate);
            return item;
        }

        public static LocationPlanAssignment NewLocationPlanAssignment(
            Int32 productGroupId, Int32 planogramId, Int16 locationId, User assignUser, DateTime? dateCommunicated, DateTime? dateLive)
        {
            LocationPlanAssignment item = new LocationPlanAssignment();
            item.Create(productGroupId, planogramId, locationId, assignUser, dateCommunicated, dateLive);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
		private void Create(Int32 productGroupId, Int32 planogramId, Int16 locationId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(ProductGroupIdProperty, productGroupId);
			this.LoadProperty<Int32>(PlanogramIdProperty, planogramId);
			this.LoadProperty<Int16>(LocationIdProperty, locationId);

            this.MarkAsChild();
			base.Create();
		}

		/// <summary>
		/// Called when creating a new instance of this type
		/// </summary>
		private void Create(Int32 productGroupId, Int32 planogramId, Int16 locationId, User assignUser, DateTime? currentDate)
		{
			this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
			this.LoadProperty<Int32>(ProductGroupIdProperty, productGroupId);
			this.LoadProperty<Int32>(PlanogramIdProperty, planogramId);
			this.LoadProperty<Int16>(LocationIdProperty, locationId);

            this.LoadProperty<Int32?>(AssignedByUserIdProperty, (assignUser != null)? (Int32?)assignUser.Id : null);

            if (currentDate != null)
            {
                // Convert the local date to UTC
                var utcDate = ((DateTime)currentDate).ToUniversalTime();
                this.LoadProperty<DateTime?>(DateAssignedProperty, utcDate);
            }
            else
            {
                this.LoadProperty<DateTime?>(DateAssignedProperty, null);
            }

            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(Int32 productGroupId, Int32 planogramId, Int16 locationId, User assignUser, 
                            DateTime? newDateCommunicated, DateTime? newDateLive)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(ProductGroupIdProperty, productGroupId);
            this.LoadProperty<Int32>(PlanogramIdProperty, planogramId);
            this.LoadProperty<Int16>(LocationIdProperty, locationId);
            this.LoadProperty<Int32?>(AssignedByUserIdProperty, (assignUser != null) ? (Int32?)assignUser.Id : null);
            this.LoadProperty<DateTime?>(DateCommunicatedProperty, newDateCommunicated);
            this.LoadProperty<DateTime?>(DateLiveProperty, newDateLive);

            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<LocationPlanAssignment>(oldId, newId);
        }

        /// <summary>
        /// Enumerates through infos for all properties on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos()
        {
            Type type = typeof(LocationPlanAssignment);
            String typeFriendly = LocationPlanAssignment.FriendlyName;

            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationPlanAssignment.AssignedByUserIdProperty );
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationPlanAssignment.DateAssignedProperty);
            //yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationPlanAssignment.PublishedByUserIdProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationPlanAssignment.DatePublishedProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationPlanAssignment.DateCommunicatedProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationPlanAssignment.DateLiveProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationPlanAssignment.PublishTypeProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationPlanAssignment.OutputDestinationProperty);
        }

        public void SetAssignedDate(DateTime? newDate)
        {
            this.LoadProperty<DateTime?>(DateAssignedProperty, newDate);
        }

        public void SetPublishedDate(DateTime? newDate)
        {
            this.LoadProperty<DateTime?>(DatePublishedProperty, newDate);
        }

        public void SetCommunicatedDate(DateTime? newDate)
        {
            this.LoadProperty<DateTime?>(DateCommunicatedProperty, newDate);
        }

        public void SetLiveDate (DateTime? newDate)
        {
            this.LoadProperty<DateTime?>(DateLiveProperty, newDate);
        }

        #endregion
    }
}
