﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a list containing AssortmentMinorRevisionListActionLocation objects
    /// </summary>
    [Serializable]
    public sealed partial class AssortmentMinorRevisionListActionLocationList : ModelList<AssortmentMinorRevisionListActionLocationList, AssortmentMinorRevisionListActionLocation>, ICollection<IAssortmentMinorRevisionActionLocation>
    {
        #region Parent

        /// <summary>
        /// Returns the parent Assortment Minor Revision Action
        /// </summary>
        public AssortmentMinorRevisionListAction Parent
        {
            get { return base.Parent as AssortmentMinorRevisionListAction; }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionListActionLocationList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.AssortmentMinorRevisionCreate.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionListActionLocationList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.AssortmentMinorRevisionGet.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionListActionLocationList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.AssortmentMinorRevisionEdit.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionListActionLocationList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.AssortmentMinorRevisionDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        internal static AssortmentMinorRevisionListActionLocationList NewAssortmentMinorRevisionListActionLocationList()
        {
            AssortmentMinorRevisionListActionLocationList item = new AssortmentMinorRevisionListActionLocationList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private new void Create()
        {
            MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region ICollection Implementation

        void ICollection<IAssortmentMinorRevisionActionLocation>.Add(IAssortmentMinorRevisionActionLocation item)
        {
            AssortmentMinorRevisionListActionLocation action = item as AssortmentMinorRevisionListActionLocation;
            if (action != null)
            {
                this.Add(action);
            }
        }

        void ICollection<IAssortmentMinorRevisionActionLocation>.Clear()
        {
            this.Clear();
        }

        bool ICollection<IAssortmentMinorRevisionActionLocation>.Contains(IAssortmentMinorRevisionActionLocation item)
        {
            AssortmentMinorRevisionListActionLocation action = item as AssortmentMinorRevisionListActionLocation;
            if (action != null)
            {
                return this.Contains(action);
            }
            return false;
        }

        void ICollection<IAssortmentMinorRevisionActionLocation>.CopyTo(IAssortmentMinorRevisionActionLocation[] array, int arrayIndex)
        {
            List<AssortmentMinorRevisionListActionLocation> castItems = new List<AssortmentMinorRevisionListActionLocation>();

            foreach (IAssortmentMinorRevisionActionLocation item in array)
            {
                AssortmentMinorRevisionListActionLocation castItem = item as AssortmentMinorRevisionListActionLocation;
                if (castItem != null)
                {
                    castItems.Add(castItem);
                }
            }

            this.CopyTo(castItems.ToArray(), arrayIndex);
        }

        int ICollection<IAssortmentMinorRevisionActionLocation>.Count
        {
            get { return this.Count; }
        }

        bool ICollection<IAssortmentMinorRevisionActionLocation>.IsReadOnly
        {
            get { return false; }
        }

        bool ICollection<IAssortmentMinorRevisionActionLocation>.Remove(IAssortmentMinorRevisionActionLocation item)
        {
            AssortmentMinorRevisionListActionLocation action = item as AssortmentMinorRevisionListActionLocation;
            if (action != null)
            {
                return this.Remove(action);
            }
            return false;
        }

        IEnumerator<IAssortmentMinorRevisionActionLocation> IEnumerable<IAssortmentMinorRevisionActionLocation>.GetEnumerator()
        {
            //Cast and add to collection
            List<IAssortmentMinorRevisionActionLocation> castItems = new List<IAssortmentMinorRevisionActionLocation>();
            foreach (AssortmentMinorRevisionListActionLocation item in this)
            {
                IAssortmentMinorRevisionActionLocation castItem = item as IAssortmentMinorRevisionActionLocation;
                if (castItem != null)
                {
                    castItems.Add(castItem);
                }
            }

            //return enumerator
            return castItems.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion
    }
}