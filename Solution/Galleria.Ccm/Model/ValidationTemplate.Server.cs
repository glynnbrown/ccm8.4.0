﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.00)

// V8-26474 : A.Silva ~ Created (Auto-generated)
// V8-26815 : A.Silva ~ Added LockValidationTemplateCommand and UnlockValidationTemplateCommand.
// V8-27269 : A.Kuszyk
//  Added FetchByEntityIdName.
// V8-27366 : L.Ineson
//  Corrected lock and unlock commands to no longer dispose of the dalfactory.
#endregion

#region Version History: (CCM v8.03)
// V8-29219 : L.Ineson
//  Removed date deleted
#endregion

#region Version History: (CCM v8.20)
// V8-31533 : A.Probyn
//  Updated lazy loaded properties to not be lazy loaded as they were causing serialization exceptions
//  when used in the workflow client.
#endregion

#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class ValidationTemplate
    {
        #region Constants

        private const String DalAssemblyName = "Galleria.Ccm.Dal.{0}.dll";

        #endregion

        #region Constructors

        /// <summary>
        ///     Private constructor. Use public factory methods to initialize new instances.
        /// </summary>
        private ValidationTemplate() { }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Returns an existing ValidationTemplate with the given id
        /// </summary>
        public static ValidationTemplate FetchById(Object id)
        {
            var fetchByIdCriteria = id is String
                ? new FetchByIdCriteria(ValidationTemplateType.FileSystem, id)
                : new FetchByIdCriteria(ValidationTemplateType.Unknown, id);
            return DataPortal.Fetch<ValidationTemplate>(fetchByIdCriteria);
        }

        /// <summary>
        /// Returns an existing validation template from the given
        /// file path.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static ValidationTemplate FetchByFilename(String fileName)
        {
            return DataPortal.Fetch<ValidationTemplate>(new FetchByIdCriteria(ValidationTemplateType.FileSystem, fileName));
        }

        /// <summary>
        /// Returns an existing ValidationTemplate with the given entity id and name.
        /// </summary>
        public static ValidationTemplate FetchByEntityIdName(Int32 entityId, String name)
        {
            return DataPortal.Fetch<ValidationTemplate>(new FetchByEntityIdNameCriteria(entityId, name));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Creates a data transfer object from this instance
        /// </summary>
        private ValidationTemplateDto GetDataTransferObject()
        {
            return new ValidationTemplateDto
            {
                Id = ReadProperty(IdProperty),
                RowVersion = ReadProperty(RowVersionProperty),
                EntityId = ReadProperty(EntityIdProperty),
                Name = ReadProperty(NameProperty),
                DateCreated = ReadProperty(DateCreatedProperty),
                DateLastModified = ReadProperty(DateLastModifiedProperty)
            };
        }

        /// <summary>
        ///     Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, ValidationTemplateDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(RowVersionProperty, dto.RowVersion);
            LoadProperty(EntityIdProperty, dto.EntityId);
            LoadProperty(NameProperty, dto.Name);
            LoadProperty(DateCreatedProperty, dto.DateCreated);
            LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
            LoadProperty<ValidationTemplateGroupList>(GroupListProperty, ValidationTemplateGroupList.FetchByParentId(dalContext, dto.Id));
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Called when returning FetchById
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            var dalFactory = GetDalFactory(criteria.DalFactoryType);
            using (var dalContext = dalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IValidationTemplateDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchById(criteria.Id));
                }
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdNameCriteria criteria)
        {
            var dalFactory = GetDalFactory();
            using (var dalContext = dalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IValidationTemplateDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchByEntityIdName(criteria.EntityId, criteria.Name));
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        ///     Called when inserting an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            var dalFactory = GetDalFactory();
            using (var dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                ValidationTemplateDto dto = GetDataTransferObject();
                Object oldId = dto.Id;
                using (IValidationTemplateDal dal = dalContext.GetDal<IValidationTemplateDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty(IdProperty, dto.Id);
                this.LoadProperty(RowVersionProperty, dto.RowVersion);
                this.LoadProperty(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<ValidationTemplate>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }

        /// <summary>
        /// Called when inserting this object and its children using an existing dal context.
        /// </summary>
        ///<remarks>Used to insert default by sync process.</remarks>
        private void InsertUsingExistingContext(IDalContext dalContext)
        {
            ValidationTemplateDto dto = GetDataTransferObject();
            Object oldId = dto.Id;
            using (IValidationTemplateDal dal = dalContext.GetDal<IValidationTemplateDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty(IdProperty, dto.Id);
            this.LoadProperty(RowVersionProperty, dto.RowVersion);
            this.LoadProperty(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
            dalContext.RegisterId<ValidationTemplate>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Update
        /// <summary>
        ///     Called when updating this instance
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            var dalFactory = GetDalFactory();
            using (var dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                ValidationTemplateDto dto = GetDataTransferObject();
                using (IValidationTemplateDal dal = dalContext.GetDal<IValidationTemplateDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty(RowVersionProperty, dto.RowVersion);
                this.LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        ///     Called when this instance is being deleted
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            var dalFactory = GetDalFactory();
            using (var dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (var dal = dalContext.GetDal<IValidationTemplateDal>())
                {
                    dal.DeleteById(Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #endregion

        #region Commands

        #region LockValidationTemplateCommand

        private partial class LockValidationTemplateCommand
        {
            #region Data Access

            #region Execute

            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.Type, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IValidationTemplateDal dal = dalContext.GetDal<IValidationTemplateDal>())
                    {
                        dal.LockById(this.Id);
                    }
                }
            }

            #endregion

            #endregion
        }
        #endregion

        #region UnlockValidationTemplateCommand

        private partial class UnlockValidationTemplateCommand
        {
            /// <summary>
            ///     Code invoked from the server side when executing this command.
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.DalFactoryType, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IValidationTemplateDal dal = dalContext.GetDal<IValidationTemplateDal>())
                    {
                        dal.UnlockById(this.Id);
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the correct dal factory
        /// </summary>
        protected override IDalFactory GetDalFactory()
        {
            if (String.IsNullOrEmpty(this.DalFactoryName))
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.DalFactoryType, out dalFactoryName);
                this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            }

            return DalContainer.GetDalFactory(this.DalFactoryName);
        }

        private IDalFactory GetDalFactory(ValidationTemplateType dalType)
        {
            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(dalType, out dalFactoryName);
            this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            this.LoadProperty<ValidationTemplateType>(DalFactoryTypeProperty, dalType);
            return dalFactory;
        }

        /// <summary>
        ///     Returns the correct <see cref="IDalFactory"/> instance based on the provided <paramref name="type"/> of <see cref="ValidationTemplate"/> and <paramref name="dalId"/>.
        /// </summary>
        /// <param name="type">The type of persistance used by this instance of <see cref="ValidationTemplate"/>.</param>
        /// <param name="dalFactoryName">The name of the actual DAL Factory used.</param>
        /// <returns>The corresponding instance stored in the DalContainer.</returns>
        private static IDalFactory GetDalFactory(ValidationTemplateType type, out String dalFactoryName)
        {
            switch (type)
            {
                case ValidationTemplateType.FileSystem:
                    dalFactoryName = Constants.UserDal;
                    return DalContainer.GetDalFactory(dalFactoryName);
                default:
                    dalFactoryName = DalContainer.DalName;
                    return DalContainer.GetDalFactory();
            }
        }

        /// <summary>
        /// Inserts the default validation template using the existing dal context.
        /// </summary>
        internal static void InsertDefaultValidationTemplate(IDalContext dalContext, Int32 entityId)
        {
            ValidationTemplate defaultTemplate = ValidationTemplate.CreateDefaultValidationTemplate(entityId);
            defaultTemplate.InsertUsingExistingContext(dalContext);
        }

        #endregion
    }
}