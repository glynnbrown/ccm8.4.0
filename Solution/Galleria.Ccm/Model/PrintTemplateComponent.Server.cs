﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class PrintTemplateComponent
    {
        #region Constructor
        private PrintTemplateComponent() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static PrintTemplateComponent Fetch(IDalContext dalContext, PrintTemplateComponentDto dto)
        {
            return DataPortal.FetchChild<PrintTemplateComponent>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PrintTemplateComponentDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<PrintTemplateComponentType>(TypeProperty, (PrintTemplateComponentType)dto.Type);
            this.LoadProperty<Single>(XProperty, dto.X);
            this.LoadProperty<Single>(YProperty, dto.Y);
            this.LoadProperty<Single>(HeightProperty, dto.Height);
            this.LoadProperty<Single>(WidthProperty, dto.Width);

            this.LoadProperty<PrintTemplateComponentDetail>(ComponentDetailsProperty,
                PrintTemplateComponentDetail.FetchByParentId(dalContext, dto.Id, (PrintTemplateComponentType)dto.Type));
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private PrintTemplateComponentDto GetDataTransferObject(PrintTemplateSection parent)
        {
            return new PrintTemplateComponentDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                PrintTemplateSectionId = parent.Id,
                Type = (Byte)ReadProperty<PrintTemplateComponentType>(TypeProperty),
                X = ReadProperty<Single>(XProperty),
                Y = ReadProperty<Single>(YProperty),
                Height = ReadProperty<Single>(HeightProperty),
                Width = ReadProperty<Single>(WidthProperty),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, PrintTemplateComponentDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, PrintTemplateSection parent)
        {
            PrintTemplateComponentDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IPrintTemplateComponentDal dal = dalContext.GetDal<IPrintTemplateComponentDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<PrintTemplateComponent>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, PrintTemplateSection parent)
        {
            if (this.IsSelfDirty)
            {
                using (IPrintTemplateComponentDal dal = dalContext.GetDal<IPrintTemplateComponentDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IPrintTemplateComponentDal dal = dalContext.GetDal<IPrintTemplateComponentDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion

    }
}