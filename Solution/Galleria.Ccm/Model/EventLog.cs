﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : L.Luong
//   Created (Copied From GFS)
// V8-27404 : L.Luong
//   Changed LevelId to EntryType
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
#endregion
#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//	Extended for new WorkpackageName, WorkpackageId property
//  Corrected EventLogNameName to be EventLogName
#endregion
#region Version History: (CCM CCM811)
// V8-29717 : D.Pleasance
//	Added friendly names for event log export
#endregion 
#endregion

using System;
using System.Diagnostics;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing an Entity
    /// (Root object)
    /// </summary>
    [Serializable]
    public partial class EventLog : ModelObject<EventLog>
    {
        #region Properties

        /// <summary>
        /// The unique object id
        /// </summary>
        /// 
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// The Entity Id where the event log record came from.
        /// </summary>
        /// 
        public static readonly ModelPropertyInfo<Int32?> EntityIdProperty =
            RegisterModelProperty<Int32?>(c => c.EntityId, Message.EventLog_EntityId);
        public Int32? EntityId
        {
            get { return GetProperty<Int32?>(EntityIdProperty); }
            set { SetProperty<Int32?>(EntityIdProperty, value); }
        }

        /// <summary>
        /// The Event Log Name Id where the event log record came from. 
        /// </summary>
        /// 
        public static readonly ModelPropertyInfo<String> EntityNameProperty =
            RegisterModelProperty<String>(c => c.EntityName, Message.EventLog_EntityName);
        public String EntityName
        {
            get { return GetProperty<String>(EntityNameProperty); }
            set { SetProperty<String>(EntityNameProperty, value); }
        }

        /// <summary>
        /// The Source application where the event log record came from.
        /// </summary>
        /// 
        public static readonly ModelPropertyInfo<String> SourceProperty =
            RegisterModelProperty<String>(c => c.Source, Message.EventLog_Source);
        public String Source
        {
            get { return GetProperty<String>(SourceProperty); }
            set { SetProperty<String>(SourceProperty, value); }
        }

        /// <summary>
        /// The Event Log Name Id where the event log record came from. 
        /// </summary>
        /// 
        public static readonly ModelPropertyInfo<Int32> EventLogNameIdProperty =
            RegisterModelProperty<Int32>(c => c.EventLogNameId, Message.EventLog_EventLogNameId);
        public Int32 EventLogNameId
        {
            get { return GetProperty<Int32>(EventLogNameIdProperty); }
            set { SetProperty<Int32>(EventLogNameIdProperty, value); }
        }

        /// <summary>
        /// The Event Log Name Id where the event log record came from. 
        /// </summary>
        /// 
        public static readonly ModelPropertyInfo<String> EventLogNameProperty =
            RegisterModelProperty<String>(c => c.EventLogName, Message.EventLog_EventLogName);
        public String EventLogName
        {
            get { return GetProperty<String>(EventLogNameProperty); }
            set { SetProperty<String>(EventLogNameProperty, value); }
        }

        /// <summary>
        /// The Event Id where the event log record came from.
        /// </summary>
        ///
        public static readonly ModelPropertyInfo<Int32> EventIdProperty =
            RegisterModelProperty<Int32>(c => c.EventId, Message.EventLog_EventId);
        public Int32 EventId
        {
            get { return GetProperty<Int32>(EventIdProperty); }
            set { SetProperty<Int32>(EventIdProperty, value); }
        }

        /// <summary>
        /// The warning/error Entry Type where the event log record came from.
        /// </summary>
        /// 
        public static readonly ModelPropertyInfo<EventLogEntryType> EntryTypeProperty =
            RegisterModelProperty<EventLogEntryType>(c => c.EntryType, Message.EventLog_EntryType);
        public EventLogEntryType EntryType
        {
            get { return GetProperty<EventLogEntryType>(EntryTypeProperty); }
            set { SetProperty<EventLogEntryType>(EntryTypeProperty, value); }
        }

        /// <summary>
        /// The User Id where the event log record came from.
        /// </summary>
        /// 
        public static readonly ModelPropertyInfo<Int32?> UserIdProperty =
            RegisterModelProperty<Int32?>(c => c.UserId, Message.EventLog_UserId);
        public Int32? UserId
        {
            get { return GetProperty<Int32?>(UserIdProperty); }
            set { SetProperty<Int32?>(UserIdProperty, value); }
        }

        /// <summary>
        /// The username of the user that logged the event.
        /// </summary>
        public static readonly ModelPropertyInfo<String> UserNameProperty =
            RegisterModelProperty<String>(c => c.UserName, Message.EventLog_UserName);
        public String UserName
        {
            get { return GetProperty<String>(UserNameProperty); }
            set { SetProperty<String>(UserNameProperty, value); }
        }

        /// <summary>
        /// The User Id where the event log record came from.
        /// </summary>
        /// 
        public static readonly ModelPropertyInfo<DateTime> DateTimeProperty =
            RegisterModelProperty<DateTime>(c => c.DateTime, Message.EventLog_DateTime);
        public DateTime DateTime
        {
            get { return GetProperty<DateTime>(DateTimeProperty); }
            set { SetProperty<DateTime>(DateTimeProperty, value); }
        }

        /// <summary>
        /// The Process where the event log record came from.
        /// </summary>
        /// 
        public static readonly ModelPropertyInfo<String> ProcessProperty =
            RegisterModelProperty<String>(c => c.Process, Message.EventLog_Process);
        public String Process
        {
            get { return GetProperty<String>(ProcessProperty); }
            set { SetProperty<String>(ProcessProperty, value); }
        }

        /// <summary>
        /// The Computer Name where the event log record came from.
        /// </summary>
        /// 
        public static readonly ModelPropertyInfo<String> ComputerNameProperty =
            RegisterModelProperty<String>(c => c.ComputerName, Message.EventLog_ComputerName);
        public String ComputerName
        {
            get { return GetProperty<String>(ComputerNameProperty); }
            set { SetProperty<String>(ComputerNameProperty, value); }
        }

        /// <summary>
        /// The any URL Helper Link where the event log record came from.
        /// </summary>
        /// 
        public static readonly ModelPropertyInfo<String> UrlHelperLinkProperty =
            RegisterModelProperty<String>(c => c.UrlHelperLink, Message.EventLog_UrlHelperLink);
        public String UrlHelperLink
        {
            get { return GetProperty<String>(UrlHelperLinkProperty); }
            set { SetProperty<String>(UrlHelperLinkProperty, value); }
        }

        /// <summary>
        /// The Description where the event log record came from.
        /// </summary>
        /// 
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description, Message.EventLog_Description);
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
            set { SetProperty<String>(DescriptionProperty, value); }
        }

        /// <summary>
        /// The Content where the event log record came from.
        /// </summary>
        /// 
        public static readonly ModelPropertyInfo<String> ContentProperty =
            RegisterModelProperty<String>(c => c.Content, Message.EventLog_Content);
        public String Content
        {
            get { return GetProperty<String>(ContentProperty); }
            set { SetProperty<String>(ContentProperty, value); }
        }

        /// <summary>
        /// The ID of the current Gibaraltar session.
        /// </summary>
        public static readonly ModelPropertyInfo<String> GibraltarSessionIdProperty =
            RegisterModelProperty<String>(c => c.GibraltarSessionId, Message.EventLog_GibraltarSessionId);
        public String GibraltarSessionId
        {
            get { return GetProperty<String>(GibraltarSessionIdProperty); }
            set { SetProperty<String>(GibraltarSessionIdProperty, value); }
        }

        /// <summary>
        /// The name of the workpackage the event log represents.
        /// </summary>
        public static readonly ModelPropertyInfo<String> WorkpackageNameProperty =
            RegisterModelProperty<String>(c => c.WorkpackageName, Message.EventLog_WorkpackageName);
        public String WorkpackageName
        {
            get { return GetProperty<String>(WorkpackageNameProperty); }
            set { SetProperty<String>(WorkpackageNameProperty, value); }
        }

        /// <summary>
        /// The Id of the workpackage the event log represents.
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> WorkpackageIdProperty =
            RegisterModelProperty<Int32?>(c => c.WorkpackageId, Message.EventLog_WorkpackageId);
        public Int32? WorkpackageId
        {
            get { return GetProperty<Int32?>(WorkpackageIdProperty); }
            set { SetProperty<Int32?>(WorkpackageIdProperty, value); }
        }

        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new MinValue<Int32>(EntityIdProperty, 1));
            BusinessRules.AddRule(new MaxLength(EntityNameProperty, 50));
            BusinessRules.AddRule(new MaxLength(SourceProperty, 50));
            BusinessRules.AddRule(new MaxLength(EventLogNameProperty, 50));
            BusinessRules.AddRule(new MinValue<Int32>(EventIdProperty, 1));
            BusinessRules.AddRule(new MinValue<Int32>(UserIdProperty, 1));
            BusinessRules.AddRule(new MaxLength(ProcessProperty, 50));
            BusinessRules.AddRule(new MaxLength(ComputerNameProperty, 50));
            BusinessRules.AddRule(new MaxLength(UrlHelperLinkProperty, 255));
            BusinessRules.AddRule(new MaxLength(DescriptionProperty, 1000));
            BusinessRules.AddRule(new MaxLength(ContentProperty, 536854528));
            BusinessRules.AddRule(new MaxLength(GibraltarSessionIdProperty, 36));
            BusinessRules.AddRule(new MaxLength(WorkpackageNameProperty, 100));
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(EventLog), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(EventLog), new IsInRole(AuthorizationActions.GetObject, DomainPermission.EventLogGet.ToString()));
            BusinessRules.AddRule(typeof(EventLog), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(EventLog), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static EventLog NewEventLog()
        {
            EventLog item = new EventLog();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(DescriptionProperty, Message.EventLog_DefaultName);
            base.Create();
        }
        #endregion

        #endregion

        #region Overrides
        /// <summary>
        /// Returns a string representation of this object
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Source;
        }
        #endregion
    }
}
