﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
//CCM-25460 : L.Ineson
//  Added FetchBySearchCriteria
// CCM-25460 : L.Ineson
//  AddedFetchByIds
// CCM-26520 :J.Pickup
//	Added FetchByCategoryCode Criteria & FetchByNullCategoryCode
// CCM-27599 : J.Pickup
//  Added Entity to FetchBySearchCriteria
#endregion
#region Version History: (CCM 801)
// V8-28507 : D.Pleasance
//  Added FetchByWorkpackageIdPagingCriteria \ FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria
#endregion
#region Version History: CCM810
// V8-29986 : A.Probyn
//  Added FetchNonDebugByCategoryCode
// V8-30213 : L.Luong
//  Added FetchNonDebugByLocationCode
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class PlanogramInfoList : ModelReadOnlyList<PlanogramInfoList, PlanogramInfo>
    {
        #region Criteria

        #region FetchByProductGroupIdCriteria
        [Serializable]
        public class FetchByPlanogramGroupIdCriteria : CriteriaBase<FetchByPlanogramGroupIdCriteria>
        {
            #region Properties

            #region EntityId
            /// <summary>
            /// EntityId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> PlanogramGroupIdProperty =
                RegisterProperty<Int32>(c => c.PlanogramGroupId);
            /// <summary>
            /// Returns the entity id
            /// </summary>
            public Int32 PlanogramGroupId
            {
                get { return this.ReadProperty<Int32>(PlanogramGroupIdProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByPlanogramGroupIdCriteria(Int32 planogramGroupId)
            {
                this.LoadProperty<Int32>(PlanogramGroupIdProperty, planogramGroupId);
            }
            #endregion
        }
        #endregion

        #region FetchByWorkpackageIdCriteria
        [Serializable]
        public class FetchByWorkpackageIdCriteria : CriteriaBase<FetchByWorkpackageIdCriteria>
        {
            #region Properties

            #region WorkpackageId
            /// <summary>
            /// EntityId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> WorkpackageIdProperty =
                RegisterProperty<Int32>(c => c.WorkpackageId);
            /// <summary>
            /// Returns the entity id
            /// </summary>
            public Int32 WorkpackageId
            {
                get { return this.ReadProperty<Int32>(WorkpackageIdProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByWorkpackageIdCriteria(Int32 workackageId)
            {
                this.LoadProperty<Int32>(WorkpackageIdProperty, workackageId);
            }
            #endregion
        }
        #endregion

        #region FetchByWorkpackageIdPagingCriteria
        [Serializable]
        public class FetchByWorkpackageIdPagingCriteria : CriteriaBase<FetchByWorkpackageIdPagingCriteria>
        {
            #region Properties

            #region WorkpackageId
            /// <summary>
            /// WorkpackageId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> WorkpackageIdProperty =
                RegisterProperty<Int32>(c => c.WorkpackageId);
            /// <summary>
            /// Returns the workpackage id
            /// </summary>
            public Int32 WorkpackageId
            {
                get { return this.ReadProperty<Int32>(WorkpackageIdProperty); }
            }
            #endregion

            #region PageNumber
            /// <summary>
            /// PageNumber property definition
            /// </summary>
            public static readonly PropertyInfo<Byte> PageNumberProperty =
                RegisterProperty<Byte>(c => c.PageNumber);
            /// <summary>
            /// Returns the Page Number
            /// </summary>
            public Byte PageNumber
            {
                get { return this.ReadProperty<Byte>(PageNumberProperty); }
            }
            #endregion

            #region PageSize
            /// <summary>
            /// PageSize property definition
            /// </summary>
            public static readonly PropertyInfo<Int16> PageSizeProperty =
                RegisterProperty<Int16>(c => c.PageSize);
            /// <summary>
            /// Returns the Page Size
            /// </summary>
            public Int16 PageSize
            {
                get { return this.ReadProperty<Int16>(PageSizeProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByWorkpackageIdPagingCriteria(Int32 workackageId, Byte pageNumber, Int16 pageSize)
            {
                this.LoadProperty<Int32>(WorkpackageIdProperty, workackageId);
                this.LoadProperty<Byte>(PageNumberProperty, pageNumber);
                this.LoadProperty<Int16>(PageSizeProperty, pageSize);
            }
            #endregion
        }
        #endregion

        #region FetchByWorkpackageIdAutomationProcessingStatusCriteria
        [Serializable]
        public class FetchByWorkpackageIdAutomationProcessingStatusCriteria : CriteriaBase<FetchByWorkpackageIdAutomationProcessingStatusCriteria>
        {
            #region Properties

            #region WorkpackageId
            /// <summary>
            /// EntityId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> WorkpackageIdProperty =
                RegisterProperty<Int32>(c => c.WorkpackageId);
            /// <summary>
            /// Returns the entity id
            /// </summary>
            public Int32 WorkpackageId
            {
                get { return this.ReadProperty<Int32>(WorkpackageIdProperty); }
            }
            #endregion

            #region AutomationStatus

            public static readonly PropertyInfo<ProcessingStatus> AutomationStatusProperty =
                RegisterProperty<ProcessingStatus>(c => c.AutomationStatus);
            /// <summary>
            /// Returns the automation status
            /// </summary>
            public ProcessingStatus AutomationStatus
            {
                get { return this.ReadProperty<ProcessingStatus>(AutomationStatusProperty); }
            }

            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByWorkpackageIdAutomationProcessingStatusCriteria(Int32 workackageId, ProcessingStatus automationStatus)
            {
                this.LoadProperty<Int32>(WorkpackageIdProperty, workackageId);
                this.LoadProperty<ProcessingStatus>(AutomationStatusProperty, automationStatus);
            }
            #endregion
        }
        #endregion

        #region FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria
        [Serializable]
        public class FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria : CriteriaBase<FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria>
        {
            #region Properties

            #region WorkpackageId
            /// <summary>
            /// EntityId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> WorkpackageIdProperty =
                RegisterProperty<Int32>(c => c.WorkpackageId);
            /// <summary>
            /// Returns the entity id
            /// </summary>
            public Int32 WorkpackageId
            {
                get { return this.ReadProperty<Int32>(WorkpackageIdProperty); }
            }
            #endregion

            #region AutomationStatusList

            public static readonly PropertyInfo<List<ProcessingStatus>> AutomationStatusListProperty =
                RegisterProperty<List<ProcessingStatus>>(c => c.AutomationStatusList);
            /// <summary>
            /// Returns the automation status
            /// </summary>
            public List<ProcessingStatus> AutomationStatusList
            {
                get { return this.ReadProperty<List<ProcessingStatus>>(AutomationStatusListProperty); }
            }

            #endregion

            #region PageNumber
            /// <summary>
            /// PageNumber property definition
            /// </summary>
            public static readonly PropertyInfo<Byte> PageNumberProperty =
                RegisterProperty<Byte>(c => c.PageNumber);
            /// <summary>
            /// Returns the Page Number
            /// </summary>
            public Byte PageNumber
            {
                get { return this.ReadProperty<Byte>(PageNumberProperty); }
            }
            #endregion

            #region PageSize
            /// <summary>
            /// PageSize property definition
            /// </summary>
            public static readonly PropertyInfo<Int16> PageSizeProperty =
                RegisterProperty<Int16>(c => c.PageSize);
            /// <summary>
            /// Returns the Page Size
            /// </summary>
            public Int16 PageSize
            {
                get { return this.ReadProperty<Int16>(PageSizeProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria(Int32 workackageId, List<ProcessingStatus> automationStatusList, Byte pageNumber, Int16 pageSize)
            {
                this.LoadProperty<Int32>(WorkpackageIdProperty, workackageId);
                this.LoadProperty<List<ProcessingStatus>>(AutomationStatusListProperty, automationStatusList);
                this.LoadProperty<Byte>(PageNumberProperty, pageNumber);
                this.LoadProperty<Int16>(PageSizeProperty, pageSize);
            }
            #endregion
        }
        #endregion

        #region FetchByEntityIdSearchCriteria
        [Serializable]
        public class FetchBySearchCriteriaCriteria : CriteriaBase<FetchBySearchCriteriaCriteria>
        {
            #region Properties

            #region Name
            /// <summary>
            /// Name property definition
            /// </summary>
            public static readonly PropertyInfo<String> NameProperty =
                RegisterProperty<String>(c => c.Name);
            /// <summary>
            /// Returns the name to filter by
            /// </summary>
            public String Name
            {
                get { return this.ReadProperty<String>(NameProperty); }
            }
            #endregion

            #region PlanogramGroupId
            /// <summary>
            /// PlanogramGroupId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32?> PlanogramGroupIdProperty =
                RegisterProperty<Int32?>(c => c.PlanogramGroupId);
            /// <summary>
            /// Returns the planogram group id to filter by
            /// </summary>
            public Int32? PlanogramGroupId
            {
                get { return this.ReadProperty<Int32?>(PlanogramGroupIdProperty); }
            }
            #endregion

            #region EntityId
            /// <summary>
            /// EntityId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            /// <summary>
            /// Returns the entity id to filter by
            /// </summary>
            public Int32 EntityId
            {
                get { return this.ReadProperty<Int32>(EntityIdProperty); }
            }
            #endregion


            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchBySearchCriteriaCriteria(String name, Int32? planogramGroupId, Int32 entityId)
            {
                this.LoadProperty<String>(NameProperty, name);
                this.LoadProperty<Int32?>(PlanogramGroupIdProperty, planogramGroupId);
                this.LoadProperty<Int32>(EntityIdProperty, entityId);
            }
            #endregion
        }
        #endregion

        #region FetchByPlanogramIdsCriteria
        [Serializable]
        public class FetchByIdsCriteria : CriteriaBase<FetchByIdsCriteria>
        {
            #region Properties

            #region PlanogramIds
            /// <summary>
            /// PlanogramIds property definition
            /// </summary>
            public static readonly PropertyInfo<List<Int32>> IdListProperty =
                RegisterProperty<List<Int32>>(c => c.IdList);
            /// <summary>
            /// Returns the list of ids to fetch for
            /// </summary>
            public List<Int32> IdList
            {
                get { return this.ReadProperty<List<Int32>>(IdListProperty); }
            }
            #endregion


            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdsCriteria(IEnumerable<Int32> idList)
            {
                this.LoadProperty<List<Int32>>(IdListProperty, idList.ToList());
            }
            #endregion
        }
        #endregion

        #region FetchByCategoryCodeCriteria

        [Serializable]
        public class FetchByCategoryCodeCriteria : CriteriaBase<FetchByCategoryCodeCriteria>
        {
            #region Properties

            #region CategoryCode

            /// <summary>
            /// CategoryCodeId property definition
            /// </summary>
            public static readonly PropertyInfo<String> CategoryCodeProperty =
                RegisterProperty<String>(c => c.CatgeoryCode);
            /// <summary>
            /// Returns the entity id
            /// </summary>
            public String CatgeoryCode
            {
                get { return this.ReadProperty<String>(CategoryCodeProperty); }
            }

            #endregion

            #endregion

            #region Constructors

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByCategoryCodeCriteria(String categoryCode)
            {
                this.LoadProperty<String>(CategoryCodeProperty, categoryCode);
            }

            #endregion
        }

        #endregion

        #region FetchNonDebugByCategoryCodeCriteria

        [Serializable]
        public class FetchNonDebugByCategoryCodeCriteria : CriteriaBase<FetchNonDebugByCategoryCodeCriteria>
        {
            #region Properties

            #region CategoryCode

            /// <summary>
            /// CategoryCodeId property definition
            /// </summary>
            public static readonly PropertyInfo<String> CategoryCodeProperty =
                RegisterProperty<String>(c => c.CatgeoryCode);
            /// <summary>
            /// Returns the entity id
            /// </summary>
            public String CatgeoryCode
            {
                get { return this.ReadProperty<String>(CategoryCodeProperty); }
            }

            #endregion

            #endregion

            #region Constructors

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchNonDebugByCategoryCodeCriteria(String categoryCode)
            {
                this.LoadProperty<String>(CategoryCodeProperty, categoryCode);
            }

            #endregion
        }

        #endregion

        #region FetchNonDebugByLocationCodeCriteria

        [Serializable]
        public class FetchNonDebugByLocationCodeCriteria : CriteriaBase<FetchNonDebugByLocationCodeCriteria>
        {
            #region Properties

            #region LocationCode

            /// <summary>
            /// LocationCodeId property definition
            /// </summary>
            public static readonly PropertyInfo<String> LocationCodeProperty =
                RegisterProperty<String>(c => c.LocationCode);
            /// <summary>
            /// Returns the entity id
            /// </summary>
            public String LocationCode
            {
                get { return this.ReadProperty<String>(LocationCodeProperty); }
            }

            #endregion

            #endregion

            #region Constructors

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchNonDebugByLocationCodeCriteria(String locationCode)
            {
                this.LoadProperty<String>(LocationCodeProperty, locationCode);
            }

            #endregion
        }

        #endregion

        #region FetchByNullCategoryCodeCriteria

        [Serializable]
        public class FetchByNullCategoryCodeCriteria : CriteriaBase<FetchByNullCategoryCodeCriteria>
        {
            #region Constructors

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByNullCategoryCodeCriteria()
            {
                        
            }

            #endregion
        }

        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PlanogramInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanogramInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(PlanogramInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanogramInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
