﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630: A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;

using Csla;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    public partial class ProductUniverseProduct
    {
        #region Constructor
        private ProductUniverseProduct() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating an instance from a dto
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new instance of this type</returns>
        internal static ProductUniverseProduct FetchProductUniverseProduct(IDalContext dalContext, ProductUniverseProductDto dto)
        {
            return DataPortal.FetchChild<ProductUniverseProduct>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(ProductUniverseProductDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<String>(GtinProperty, dto.Gtin);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<Int32>(ProductIdProperty, dto.ProductId);
        }

        /// <summary>
        /// Returns a dto created from this instance
        /// </summary>
        /// <returns>A data transfer object</returns>
        private ProductUniverseProductDto GetDataTransferObject(ProductUniverse parent)
        {
            return new ProductUniverseProductDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                Gtin = ReadProperty<String>(GtinProperty),
                Name = ReadProperty<String>(NameProperty),
                ProductId = ReadProperty<Int32>(ProductIdProperty),
                ProductUniverseId = parent.Id
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when this instance is being loaded
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ProductUniverseProductDto dto)
        {
            this.LoadDataTransferObject(dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when this instance is being inserted into the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent product universe</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, ProductUniverse parent)
        {
            ProductUniverseProductDto dto = GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IProductUniverseProductDal dal = dalContext.GetDal<IProductUniverseProductDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<ProductUniverseProduct>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when this instance is being updated in the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent product universe</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, ProductUniverse parent)
        {
            if (this.IsSelfDirty)
            {
                using (IProductUniverseProductDal dal = dalContext.GetDal<IProductUniverseProductDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this instance is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        private void Child_DeleteSelf(IDalContext dalContext, ProductUniverse parent)
        {
            using (IProductUniverseProductDal dal = dalContext.GetDal<IProductUniverseProductDal>())
            {
                dal.DeleteById(this.Id);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #endregion
    }
}
