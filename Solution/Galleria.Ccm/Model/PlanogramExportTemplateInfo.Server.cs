﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramExportTemplateInfo
    {        
        #region Constructors

        private PlanogramExportTemplateInfo() { }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Request a <see cref="ValidationTemplateInfo"/> instance from its <see cref="dto"/>.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">The dto to load from.</param>
        /// <returns>A new instance of <see cref="ValidationTemplateInfo"/>.</returns>
        internal static PlanogramExportTemplateInfo FetchPlanogramExportTemplateInfo(IDalContext dalContext,
            PlanogramExportTemplateInfoDto dto)
        {
            return DataPortal.FetchChild<PlanogramExportTemplateInfo>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object

        private void LoadDataTransferObject(IDalContext dalContext, PlanogramExportTemplateInfoDto dto)
        {
            LoadProperty<Object>(IdProperty, dto.Id);
            LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            LoadProperty<String>(NameProperty, dto.Name);
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     The data portal invokes this method via reflection when loading this object from a dto.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">The dto to load from.</param>
        private void Child_Fetch(IDalContext dalContext, PlanogramExportTemplateInfoDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #endregion
    }
}
