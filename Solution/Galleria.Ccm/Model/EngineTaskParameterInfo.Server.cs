﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion

#region Version History : CCM811
// V8-30429  : J.Pickup
//  Introduced ParentId
#endregion

#endregion

using System;
using Csla;
using Galleria.Ccm.Engine;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    public partial class EngineTaskParameterInfo
    {
        #region Constructors
        private EngineTaskParameterInfo() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a task parameter info object
        /// </summary>
        internal static EngineTaskParameterInfo GetEngineTaskParameterInfo(TaskParameter parameter)
        {
            return DataPortal.FetchChild<EngineTaskParameterInfo>(parameter);
        }
        #endregion

        #region Data Access
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(TaskParameter parameter)
        {
            this.LoadProperty<Int32>(IdProperty, parameter.Id);
            this.LoadProperty<String>(NameProperty, parameter.Name);
            this.LoadProperty<String>(DescriptionProperty, parameter.Description);
            this.LoadProperty<String>(CategoryProperty, parameter.Category);
            this.LoadProperty<TaskParameterType>(ParameterTypeProperty, parameter.ParameterType);
            this.LoadProperty<Object>(DefaultValueProperty, parameter.DefaultValue);
            this.LoadProperty<Boolean>(IsHiddenByDefaultProperty, parameter.IsHiddenByDefault);
            this.LoadProperty<Boolean>(IsReadOnlyByDefaultProperty, parameter.IsReadOnlyByDefault);
            this.LoadProperty<Int32?>(ParentIdProperty, parameter.ParentId);

            if (parameter.EnumValues != null)
            {
                this.LoadProperty<EngineTaskParameterEnumValueInfoList>(EnumValuesProperty,
                    EngineTaskParameterEnumValueInfoList.GetEngineTaskParameterEnumValueInfoList(parameter.EnumValues));
            }
        }
        #endregion
    }

}
