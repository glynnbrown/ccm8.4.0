﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24801 : L.Hodson
//  Created
#endregion
#endregion

using System;
using Csla.Rules;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a model object within this assembly
    /// </summary>
    internal interface IModelObject
    {
        #region Properties
        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        String DalFactoryName { get; }
        #endregion

        #region Methods

        /// <summary>
        /// Returns all broken rules for this object and all
        /// child objects.
        /// </summary>
        /// <returns></returns>
        BrokenRulesCollection GetAllBrokenRules();

        #endregion
    }
}
