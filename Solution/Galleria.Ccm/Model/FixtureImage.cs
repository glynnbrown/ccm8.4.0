﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
// V8-26182 : L.Ineson
//  Added NewPlanogramImage
#endregion
#region Version History: (CCM CCM830)
// V8-32524 : L.Ineson
//  Added FixtureAnnotations and simplified updated back to plan
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// FixtureImage Model object
    /// </summary>
    [Serializable]
    public sealed partial class FixtureImage : ModelObject<FixtureImage>
    {
        #region Static Constructor
        static FixtureImage()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new FixturePackage Parent
        {
            get
            {
                FixtureImageList parentList = base.Parent as FixtureImageList;
                if (parentList != null)
                {
                    return parentList.Parent;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region FileName

        /// <summary>
        /// FileName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> FileNameProperty =
            RegisterModelProperty<String>(c => c.FileName);

        /// <summary>
        /// Gets/Sets the FileName value
        /// </summary>
        public String FileName
        {
            get { return GetProperty<String>(FileNameProperty); }
            set { SetProperty<String>(FileNameProperty, value); }
        }

        #endregion

        #region Description

        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);

        /// <summary>
        /// Gets/Sets the Description value
        /// </summary>
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
            set { SetProperty<String>(DescriptionProperty, value); }
        }

        #endregion

        #region ImageData

        /// <summary>
        /// ImageData property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte[]> ImageDataProperty =
            RegisterModelProperty<Byte[]>(c => c.ImageData);

        /// <summary>
        /// Gets/Sets the ImageData value
        /// </summary>
        public Byte[] ImageData
        {
            get { return GetProperty<Byte[]>(ImageDataProperty); }
            set { SetProperty<Byte[]>(ImageDataProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(FileNameProperty));
            BusinessRules.AddRule(new Required(DescriptionProperty));
            BusinessRules.AddRule(new MaxLength(DescriptionProperty, 255));

        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static FixtureImage NewFixtureImage()
        {
            FixtureImage item = new FixtureImage();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        internal static FixtureImage NewFixtureImage(PlanogramImage planImage)
        {
            FixtureImage item = new FixtureImage();
            item.Create(planImage);
            return item;
        }


        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

         /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(PlanogramImage planImage)
        {
            this.LoadProperty<Int32>(IdProperty, (Int32)planImage.Id);
            this.LoadProperty<String>(FileNameProperty, planImage.FileName);
            this.LoadProperty<String>(DescriptionProperty, planImage.Description);
            this.LoadProperty<Byte[]>(ImageDataProperty, planImage.ImageData);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<FixtureItem>(oldId, newId);
        }

        protected override object GetIdValue()
        {
            return this.Id;
        }

        /// <summary>
        /// Updates the values of this image back to the plan
        /// </summary>
        public PlanogramImage AddOrUpdatePlanogramImage(Planogram plan)
        {
            PlanogramImage img = plan.Images.FindById(this.Id);
            if (img == null)
            {
                img = PlanogramImage.NewPlanogramImage();
                img.Id = this.Id;
                plan.Images.Add(img);
            }

            img.FileName = this.FileName;
            img.Description = this.Description;
            img.ImageData = this.ImageData;

            return img;
        }

        #endregion
    }
}