﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#region Version History: (CCM 8.0.3)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#region Version History: (CCM 8.1.0)
// V8-29936 : N.Haywood
//  Update will set a new ucr
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    public partial class LocationSpace
    {
        #region Constructor
        private LocationSpace() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Fetchs a location space by id
        /// </summary>
        public static LocationSpace GetLocationSpaceById(Int32 id)
        {
            return DataPortal.Fetch<LocationSpace>(new SingleCriteria<LocationSpace, Int32>(id));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a new dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private LocationSpaceDto GetDataTransferObject()
        {
            return new LocationSpaceDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                UniqueContentReference = ReadProperty<Guid>(UniqueContentReferenceProperty),
                LocationId = ReadProperty<Int16>(LocationIdProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                ParentUniqueContentReference = ReadProperty<Guid?>(ParentUniqueContentReferenceProperty)
            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, LocationSpaceDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            LoadProperty<Guid>(UniqueContentReferenceProperty, dto.UniqueContentReference);
            LoadProperty<Int16>(LocationIdProperty, dto.LocationId);
            LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            LoadProperty<LocationSpaceProductGroupList>(LocationSpaceProductGroupsProperty, LocationSpaceProductGroupList.FetchByLocationSpaceId(dalContext, dto.Id));
            LoadProperty<Guid?>(ParentUniqueContentReferenceProperty, dto.ParentUniqueContentReference);
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching a location space by id
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationSpaceDal dal = dalContext.GetDal<ILocationSpaceDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchById(criteria.Value));
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an object
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                LocationSpaceDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (ILocationSpaceDal dal = dalContext.GetDal<ILocationSpaceDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                dalContext.RegisterId<LocationSpace>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an existing object
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                LocationSpaceDto dto = GetDataTransferObject();
                dto.UniqueContentReference = Guid.NewGuid();
                using (ILocationSpaceDal dal = dalContext.GetDal<ILocationSpaceDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this instance is deleting itself
        /// </summary>
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ILocationSpaceDal dal = dalContext.GetDal<ILocationSpaceDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }
        #endregion

        #endregion

    }
}
