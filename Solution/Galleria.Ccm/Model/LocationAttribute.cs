﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class LocationAttribute : ModelObject<LocationAttribute>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationAttribute), new IsInRole(AuthorizationActions.GetObject, DomainPermission.LocationGet.ToString()));
            BusinessRules.AddRule(typeof(LocationAttribute), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationCreate.ToString()));
            BusinessRules.AddRule(typeof(LocationAttribute), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationAttribute), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationDelete.ToString()));
        }
        #endregion

        #region Properties

        /// <summary>
        /// Returns true if the Locations available have region data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasRegionProperty =
            RegisterModelProperty<Boolean>(c => c.HasRegion);
        public Boolean HasRegion
        {
            get { return GetProperty<Boolean>(HasRegionProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have county data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasCountyProperty =
            RegisterModelProperty<Boolean>(c => c.HasCounty);
        public Boolean HasCounty
        {
            get { return GetProperty<Boolean>(HasCountyProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have tv region data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasTVRegionProperty =
            RegisterModelProperty<Boolean>(c => c.HasTVRegion);
        public Boolean HasTVRegion
        {
            get { return GetProperty<Boolean>(HasTVRegionProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have address 1 data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasAddress1Property =
            RegisterModelProperty<Boolean>(c => c.HasAddress1);
        public Boolean HasAddress1
        {
            get { return GetProperty<Boolean>(HasAddress1Property); }
        }

        /// <summary>
        /// Returns true if the Locations available have address 2 data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasAddress2Property =
            RegisterModelProperty<Boolean>(c => c.HasAddress2);
        public Boolean HasAddress2
        {
            get { return GetProperty<Boolean>(HasAddress2Property); }
        }

        /// <summary>
        /// Returns true if the Locations available have open 24 hours data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> Has24HoursProperty =
            RegisterModelProperty<Boolean>(c => c.Has24Hours);
        public Boolean Has24Hours
        {
            get { return GetProperty<Boolean>(Has24HoursProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have petrol forecourt data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasPetrolForecourtProperty =
            RegisterModelProperty<Boolean>(c => c.HasPetrolForecourt);
        public Boolean HasPetrolForecourt
        {
            get { return GetProperty<Boolean>(HasPetrolForecourtProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have petrol forecourt type data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasPetrolForecourtTypeProperty =
            RegisterModelProperty<Boolean>(c => c.HasPetrolForecourtType);
        public Boolean HasPetrolForecourtType
        {
            get { return GetProperty<Boolean>(HasPetrolForecourtTypeProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have restaurant details data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasRestaurantProperty =
            RegisterModelProperty<Boolean>(c => c.HasRestaurant);
        public Boolean HasRestaurant
        {
            get { return GetProperty<Boolean>(HasRestaurantProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have a mezz floor data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasMezzFittedProperty =
            RegisterModelProperty<Boolean>(c => c.HasMezzFitted);
        public Boolean HasMezzFitted
        {
            get { return GetProperty<Boolean>(HasMezzFittedProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have SizeGrossArea data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasSizeGrossAreaProperty =
            RegisterModelProperty<Boolean>(c => c.HasSizeGrossArea);
        public Boolean HasSizeGrossArea
        {
            get { return GetProperty<Boolean>(HasSizeGrossAreaProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have SizeNetSalesArea data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasSizeNetSalesAreaProperty =
            RegisterModelProperty<Boolean>(c => c.HasSizeNetSalesArea);
        public Boolean HasSizeNetSalesArea
        {
            get { return GetProperty<Boolean>(HasSizeNetSalesAreaProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have SizeMezzSalesArea data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasSizeMezzSalesAreaProperty =
            RegisterModelProperty<Boolean>(c => c.HasSizeMezzSalesArea);
        public Boolean HasSizeMezzSalesArea
        {
            get { return GetProperty<Boolean>(HasSizeMezzSalesAreaProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have city data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasCityProperty =
            RegisterModelProperty<Boolean>(c => c.HasCity);
        public Boolean HasCity
        {
            get { return GetProperty<Boolean>(HasCityProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have Country data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasCountryProperty =
            RegisterModelProperty<Boolean>(c => c.HasCountry);
        public Boolean HasCountry
        {
            get { return GetProperty<Boolean>(HasCountryProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have ManagerName data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasManagerNameProperty =
            RegisterModelProperty<Boolean>(c => c.HasManagerName);
        public Boolean HasManagerName
        {
            get { return GetProperty<Boolean>(HasManagerNameProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have RegionalManagerName data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasRegionalManagerNameProperty =
            RegisterModelProperty<Boolean>(c => c.HasRegionalManagerName);
        public Boolean HasRegionalManagerName
        {
            get { return GetProperty<Boolean>(HasRegionalManagerNameProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have DivisionalManagerName data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasDivisionalManagerNameProperty =
            RegisterModelProperty<Boolean>(c => c.HasDivisionalManagerName);
        public Boolean HasDivisionalManagerName
        {
            get { return GetProperty<Boolean>(HasDivisionalManagerNameProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have AdvertisingZone data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasAdvertisingZoneProperty =
            RegisterModelProperty<Boolean>(c => c.HasAdvertisingZone);
        public Boolean HasAdvertisingZone
        {
            get { return GetProperty<Boolean>(HasAdvertisingZoneProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have DistributionCentre data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasDistributionCentreProperty =
            RegisterModelProperty<Boolean>(c => c.HasDistributionCentre);
        public Boolean HasDistributionCentre
        {
            get { return GetProperty<Boolean>(HasDistributionCentreProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasNewsCube data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasNewsCubeProperty =
            RegisterModelProperty<Boolean>(c => c.HasNewsCube);
        public Boolean HasNewsCube
        {
            get { return GetProperty<Boolean>(HasNewsCubeProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasAtmMachines data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasAtmMachinesProperty =
            RegisterModelProperty<Boolean>(c => c.HasAtmMachines);
        public Boolean HasAtmMachines
        {
            get { return GetProperty<Boolean>(HasAtmMachinesProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have NoOfCheckouts data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasNoOfCheckoutsProperty =
            RegisterModelProperty<Boolean>(c => c.HasNoOfCheckouts);
        public Boolean HasNoOfCheckouts
        {
            get { return GetProperty<Boolean>(HasNoOfCheckoutsProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasCustomerWC data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasCustomerWCProperty =
            RegisterModelProperty<Boolean>(c => c.HasCustomerWC);
        public Boolean HasCustomerWC
        {
            get { return GetProperty<Boolean>(HasCustomerWCProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasBabyChanging data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasBabyChangingProperty =
            RegisterModelProperty<Boolean>(c => c.HasBabyChanging);
        public Boolean HasBabyChanging
        {
            get { return GetProperty<Boolean>(HasBabyChangingProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasInLocationBakery data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasInLocationBakeryProperty =
            RegisterModelProperty<Boolean>(c => c.HasInLocationBakery);
        public Boolean HasInLocationBakery
        {
            get { return GetProperty<Boolean>(HasInLocationBakeryProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasHotFoodToGo data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasHotFoodToGoProperty =
            RegisterModelProperty<Boolean>(c => c.HasHotFoodToGo);
        public Boolean HasHotFoodToGo
        {
            get { return GetProperty<Boolean>(HasHotFoodToGoProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasRotisserie data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasRotisserieProperty =
            RegisterModelProperty<Boolean>(c => c.HasRotisserie);
        public Boolean HasRotisserie
        {
            get { return GetProperty<Boolean>(HasRotisserieProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasFishMonger data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasFishmongerProperty =
            RegisterModelProperty<Boolean>(c => c.HasFishmonger);
        public Boolean HasFishmonger
        {
            get { return GetProperty<Boolean>(HasFishmongerProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasButcher data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasButcherProperty =
            RegisterModelProperty<Boolean>(c => c.HasButcher);
        public Boolean HasButcher
        {
            get { return GetProperty<Boolean>(HasButcherProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasPizza data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasPizzaProperty =
            RegisterModelProperty<Boolean>(c => c.HasPizza);
        public Boolean HasPizza
        {
            get { return GetProperty<Boolean>(HasPizzaProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasPizza data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasDeliProperty =
            RegisterModelProperty<Boolean>(c => c.HasDeli);
        public Boolean HasDeli
        {
            get { return GetProperty<Boolean>(HasDeliProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasSaladBar data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasSaladBarProperty =
            RegisterModelProperty<Boolean>(c => c.HasSaladBar);
        public Boolean HasSaladBar
        {
            get { return GetProperty<Boolean>(HasSaladBarProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasOrganic data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasOrganicProperty =
            RegisterModelProperty<Boolean>(c => c.HasOrganic);
        public Boolean HasOrganic
        {
            get { return GetProperty<Boolean>(HasOrganicProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasGrocery data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasGroceryProperty =
            RegisterModelProperty<Boolean>(c => c.HasGrocery);
        public Boolean HasGrocery
        {
            get { return GetProperty<Boolean>(HasGroceryProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasMobilePhones data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasMobilePhonesProperty =
            RegisterModelProperty<Boolean>(c => c.HasMobilePhones);
        public Boolean HasMobilePhones
        {
            get { return GetProperty<Boolean>(HasMobilePhonesProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasDryCleaning data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasDryCleaningProperty =
            RegisterModelProperty<Boolean>(c => c.HasDryCleaning);
        public Boolean HasDryCleaning
        {
            get { return GetProperty<Boolean>(HasDryCleaningProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasHomeShoppingAvailable data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasHomeShoppingAvailableProperty =
            RegisterModelProperty<Boolean>(c => c.HasHomeShoppingAvailable);
        public Boolean HasHomeShoppingAvailable
        {
            get { return GetProperty<Boolean>(HasHomeShoppingAvailableProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasOptician data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasOpticianProperty =
            RegisterModelProperty<Boolean>(c => c.HasOptician);
        public Boolean HasOptician
        {
            get { return GetProperty<Boolean>(HasOpticianProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasPharmacy data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasPharmacyProperty =
            RegisterModelProperty<Boolean>(c => c.HasPharmacy);
        public Boolean HasPharmacy
        {
            get { return GetProperty<Boolean>(HasPharmacyProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasTravel data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasTravelProperty =
            RegisterModelProperty<Boolean>(c => c.HasTravel);
        public Boolean HasTravel
        {
            get { return GetProperty<Boolean>(HasTravelProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasPhotoDepartment data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasPhotoDepartmentProperty =
            RegisterModelProperty<Boolean>(c => c.HasPhotoDepartment);
        public Boolean HasPhotoDepartment
        {
            get { return GetProperty<Boolean>(HasPhotoDepartmentProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasCarServiceArea data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasCarServiceAreaProperty =
            RegisterModelProperty<Boolean>(c => c.HasCarServiceArea);
        public Boolean HasCarServiceArea
        {
            get { return GetProperty<Boolean>(HasCarServiceAreaProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasGardenCenter data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasGardenCentreProperty =
            RegisterModelProperty<Boolean>(c => c.HasGardenCentre);
        public Boolean HasGardenCentre
        {
            get { return GetProperty<Boolean>(HasGardenCentreProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasClinic data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasClinicProperty =
            RegisterModelProperty<Boolean>(c => c.HasClinic);
        public Boolean HasClinic
        {
            get { return GetProperty<Boolean>(HasClinicProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasAlcohol data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasAlcoholProperty =
            RegisterModelProperty<Boolean>(c => c.HasAlcohol);
        public Boolean HasAlcohol
        {
            get { return GetProperty<Boolean>(HasAlcoholProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasFashion data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasFashionProperty =
            RegisterModelProperty<Boolean>(c => c.HasFashion);
        public Boolean HasFashion
        {
            get { return GetProperty<Boolean>(HasFashionProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasCafe data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasCafeProperty =
            RegisterModelProperty<Boolean>(c => c.HasCafe);
        public Boolean HasCafe
        {
            get { return GetProperty<Boolean>(HasCafeProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasRecycling data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasRecyclingProperty =
            RegisterModelProperty<Boolean>(c => c.HasRecycling);
        public Boolean HasRecycling
        {
            get { return GetProperty<Boolean>(HasRecyclingProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasPhotocopier data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasPhotocopierProperty =
            RegisterModelProperty<Boolean>(c => c.HasPhotocopier);
        public Boolean HasPhotocopier
        {
            get { return GetProperty<Boolean>(HasPhotocopierProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasLottery data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasLotteryProperty =
            RegisterModelProperty<Boolean>(c => c.HasLottery);
        public Boolean HasLottery
        {
            get { return GetProperty<Boolean>(HasLotteryProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasPostOffice data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasPostOfficeProperty =
            RegisterModelProperty<Boolean>(c => c.HasPostOffice);
        public Boolean HasPostOffice
        {
            get { return GetProperty<Boolean>(HasPostOfficeProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have HasMovieRental data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasMovieRentalProperty =
            RegisterModelProperty<Boolean>(c => c.HasMovieRental);
        public Boolean HasMovieRental
        {
            get { return GetProperty<Boolean>(HasMovieRentalProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have is open on a monday data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasOpenMondayProperty =
            RegisterModelProperty<Boolean>(c => c.HasOpenMonday);
        public Boolean HasOpenMonday
        {
            get { return GetProperty<Boolean>(HasOpenMondayProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have is open on a Tuesday data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasOpenTuesdayProperty =
            RegisterModelProperty<Boolean>(c => c.HasOpenTuesday);
        public Boolean HasOpenTuesday
        {
            get { return GetProperty<Boolean>(HasOpenTuesdayProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have is open on a Wednesday data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasOpenWednesdayProperty =
            RegisterModelProperty<Boolean>(c => c.HasOpenWednesday);
        public Boolean HasOpenWednesday
        {
            get { return GetProperty<Boolean>(HasOpenWednesdayProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have is open on a Thursday data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasOpenThursdayProperty =
            RegisterModelProperty<Boolean>(c => c.HasOpenThursday);
        public Boolean HasOpenThursday
        {
            get { return GetProperty<Boolean>(HasOpenThursdayProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have is open on a Friday data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasOpenFridayProperty =
            RegisterModelProperty<Boolean>(c => c.HasOpenFriday);
        public Boolean HasOpenFriday
        {
            get { return GetProperty<Boolean>(HasOpenFridayProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have is open on a Saturday data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasOpenSaturdayProperty =
            RegisterModelProperty<Boolean>(c => c.HasOpenSaturday);
        public Boolean HasOpenSaturday
        {
            get { return GetProperty<Boolean>(HasOpenSaturdayProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have is open on a sunday data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasOpenSundayProperty =
            RegisterModelProperty<Boolean>(c => c.HasOpenSunday);
        public Boolean HasOpenSunday
        {
            get { return GetProperty<Boolean>(HasOpenSundayProperty); }
        }

        /// <summary>
        /// Returns true if the Locations available have a jewellery section data
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasJewelleryProperty =
            RegisterModelProperty<Boolean>(c => c.HasJewellery);
        public Boolean HasJewellery
        {
            get { return GetProperty<Boolean>(HasJewelleryProperty); }
        }

        #endregion
    }
}
