﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class SyncSourceList
    {
        #region Constructor
        private SyncSourceList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a list of all sync sources
        /// </summary>
        /// <returns></returns>
        public static SyncSourceList GetAllSyncSources()
        {
            return DataPortal.Fetch<SyncSourceList>();
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when retrieving a list of sync sources
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch()
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory(Constants.SyncDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ISyncSourceDal dal = dalContext.GetDal<ISyncSourceDal>())
                {
                    IEnumerable<SyncSourceDto> dtoList = dal.FetchAll();
                    foreach (SyncSourceDto dto in dtoList)
                    {
                        this.Add(SyncSource.GetSyncSource(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}