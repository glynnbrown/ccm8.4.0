﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25886 : L.Ineson
//  Created
// V8-27426 : L.Ineson
//  Added 2 more values
#endregion
#region Version History: CCM801
// V8-28853 : D.Pleasance
//  Added OnCopy implementation
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class WorkflowTaskParameterValue : ModelObject<WorkflowTaskParameterValue>, ITaskParameterValue
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent planogram
        /// </summary>
        public new WorkflowTaskParameter Parent
        {
            get
            {
                WorkflowTaskParameterValueList parentList =
                    base.Parent as WorkflowTaskParameterValueList;
                if (parentList != null)
                {
                    return parentList.Parent as WorkflowTaskParameter;
                }
                return null;
            }
        }
        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns unique parameter id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region Value1
        /// <summary>
        /// Value1 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> Value1Property =
            RegisterModelProperty<Object>(c => c.Value1);
        /// <summary>
        /// Gets or sets the parameter value
        /// </summary>
        public Object Value1
        {
            get { return this.GetProperty<Object>(Value1Property); }
            set { this.SetProperty<Object>(Value1Property, value); }
        }
        #endregion

        #region Value2
        /// <summary>
        /// Value2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> Value2Property =
            RegisterModelProperty<Object>(c => c.Value2);
        /// <summary>
        /// Gets or sets the parameter value
        /// </summary>
        public Object Value2
        {
            get { return this.GetProperty<Object>(Value2Property); }
            set { this.SetProperty<Object>(Value2Property, value); }
        }
        #endregion

        #region Value3
        /// <summary>
        /// Value3 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> Value3Property =
            RegisterModelProperty<Object>(c => c.Value3);
        /// <summary>
        /// Gets or sets the parameter value
        /// </summary>
        public Object Value3
        {
            get { return this.GetProperty<Object>(Value3Property); }
            set { this.SetProperty<Object>(Value3Property, value); }
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(WorkflowTaskParameterValue), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskParameterValue), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.WorkflowCreate.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskParameterValue), new IsInRole(AuthorizationActions.EditObject, DomainPermission.WorkflowEdit.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskParameterValue), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.WorkflowDelete.ToString()));
        }
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new workpackage planogram parameter value
        /// </summary>
        public static WorkflowTaskParameterValue NewWorkflowTaskParameterValue()
        {
            WorkflowTaskParameterValue item = new WorkflowTaskParameterValue();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new workpackage planogram parameter value
        /// </summary>
        public static WorkflowTaskParameterValue NewWorkflowTaskParameterValue(ITaskParameterValue source)
        {
            WorkflowTaskParameterValue item = new WorkflowTaskParameterValue();
            item.Create(source);
            return item;
        }
        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected void Create(ITaskParameterValue source)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());   
            this.LoadProperty<Object>(Value1Property, source.Value1);
            this.LoadProperty<Object>(Value2Property, source.Value2);
            this.LoadProperty<Object>(Value3Property, source.Value3);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Overrides

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<WorkpackagePlanogramParameterValue>(oldId, newId);
        }

        #endregion
    }
}
