﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A list of location space product group contained within a location space
    /// </summary>
    [Serializable]
    public sealed partial class LocationSpaceProductGroupList : ModelList<LocationSpaceProductGroupList, LocationSpaceProductGroup>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new LocationSpace Parent
        {
            get { return (LocationSpace)base.Parent; }
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationSpaceProductGroupList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationSpaceCreate.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceProductGroupList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.LocationSpaceGet.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceProductGroupList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationSpaceEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceProductGroupList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationSpaceDelete.ToString()));
        }

        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        public static LocationSpaceProductGroupList NewLocationSpaceProductGroupList()
        {
            LocationSpaceProductGroupList item = new LocationSpaceProductGroupList();
            item.Create();
            return item;
        }
        #endregion

        #region Methods

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create()
        {
            MarkAsChild();
        }
        #endregion

        #endregion
    }
}
