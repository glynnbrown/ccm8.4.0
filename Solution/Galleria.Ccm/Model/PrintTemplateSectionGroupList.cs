﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of PrintTemplateSectionGroup objects.
    /// </summary>
    [Serializable]
    public sealed partial class PrintTemplateSectionGroupList : ModelList<PrintTemplateSectionGroupList, PrintTemplateSectionGroup>
    {
        #region Parent

        /// <summary>
        /// Returns the parent PrintTemplate.
        /// </summary>
        public new PrintTemplate Parent
        {
            get { return base.Parent as PrintTemplate; }
        }

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PrintTemplateSectionGroupList NewPrintTemplateSectionGroupList()
        {
            PrintTemplateSectionGroupList item = new PrintTemplateSectionGroupList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Adds a new section group to the end of this list.
        /// </summary>
        public PrintTemplateSectionGroup AddNewSectionGroup()
        {
            PrintTemplate parentPrintTemplate = this.Parent;

            Byte newGroupNumber = (parentPrintTemplate != null)? 
                (Byte)(parentPrintTemplate.SectionGroups.Count +1) : (Byte)1;


            //Add it
            PrintTemplateSectionGroup item = PrintTemplateSectionGroup.NewPrintTemplateSectionGroup(newGroupNumber);
            this.Add(item);
            return item;
        }

        /// <summary>
        /// Adds a new section group to this list at the given group number.
        /// </summary>
        /// <returns></returns>
        public PrintTemplateSectionGroup AddNewSectionGroup(Byte groupNumber)
        {
            PrintTemplate parentPrintTemplate = this.Parent;

            if (parentPrintTemplate != null)
            {
                // loop through all the other section groups and increment the section group number
                foreach (PrintTemplateSectionGroup sectionGroup in parentPrintTemplate.SectionGroups)
                {
                    // if section group number is equal to or more than the new section group then increment
                    if (sectionGroup.Number >= groupNumber)
                    {
                        sectionGroup.Number++;
                    }
                }
            }


            //Add it
            PrintTemplateSectionGroup item = PrintTemplateSectionGroup.NewPrintTemplateSectionGroup(groupNumber);
            this.Add(item);
            return item;
        }

        #endregion
    }

}