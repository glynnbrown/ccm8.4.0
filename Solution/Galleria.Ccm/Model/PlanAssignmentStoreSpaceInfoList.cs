#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25879 : Martin Shelley
//	Created (Auto-generated)
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of PlanAssignmentStoreSpaceInfo objects.
    /// </summary>
    [Serializable]
    public partial class PlanAssignmentStoreSpaceInfoList : ModelReadOnlyList<PlanAssignmentStoreSpaceInfoList, PlanAssignmentStoreSpaceInfo>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PlanAssignmentStoreSpaceInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(PlanAssignmentStoreSpaceInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanAssignmentStoreSpaceInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanAssignmentStoreSpaceInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Data Access

        #region Criteria

        #region FetchByEntityIdProductIdCriteria

        /// <summary>
        /// Criteria for FetchByEntityIdProductGroupId
        /// </summary>
        [Serializable]
        public class FetchByEntityIdProductIdCriteria : Csla.CriteriaBase<FetchByEntityIdProductIdCriteria>
        {
            #region Properties

            #region EntityId property

            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            #endregion

            #region ProductGroupId

            public static readonly PropertyInfo<Int32> ProductGroupIdProperty =
                RegisterProperty<Int32>(c => c.ProductGroupId);
            public Int32 ProductGroupId
            {
                get { return ReadProperty<Int32>(ProductGroupIdProperty); }
            }

            #endregion

            #region ClusterSchemeId property

            /// <summary>
            /// 
            /// </summary>
            public static readonly PropertyInfo<Int32?> ClusterSchemeIdProperty =
                RegisterProperty<Int32?>(c => c.ClusterSchemeId);
            /// <summary>
            /// 
            /// </summary>
            public Int32? ClusterSchemeId
            {
                get { return ReadProperty<Int32?>(ClusterSchemeIdProperty); }
            }

            #endregion

            #endregion

            #region Contructors

            public FetchByEntityIdProductIdCriteria(Int32 entityId, Int32 productGroupId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<Int32>(ProductGroupIdProperty, productGroupId);
                LoadProperty<Int32?>(ClusterSchemeIdProperty, null);
            }

            public FetchByEntityIdProductIdCriteria(Int32 entityId, Int32 productGroupId, Int32? clusterSchemeId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<Int32>(ProductGroupIdProperty, productGroupId);
                LoadProperty<Int32?>(ClusterSchemeIdProperty, clusterSchemeId);
            }

            #endregion
        }

        #endregion

        #region FetchByEntityIdLocationIdCriteria

        /// <summary>
        /// Criteria for FetchByEntityIdProductGroupId
        /// </summary>
        [Serializable]
        public class FetchByEntityIdLocationIdCriteria : Csla.CriteriaBase<FetchByEntityIdLocationIdCriteria>
        {
            #region Properties

            #region EntityId property

            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            #endregion

            #region LocationId

            public static readonly PropertyInfo<Int16> LocationIdProperty =
                RegisterProperty<Int16>(c => c.LocationId);
            public Int16 LocationId
            {
                get { return ReadProperty<Int16>(LocationIdProperty); }
            }

            #endregion

            #region ClusterSchemeId

            public static readonly PropertyInfo<Int32?> ClusterSchemeIdProperty =
                RegisterProperty<Int32?>(c => c.ClusterSchemeId);
            public Int32? ClusterSchemeId
            {
                get { return ReadProperty<Int32?>(ClusterSchemeIdProperty); }
            }

            #endregion

            #endregion

            #region Contructors

            public FetchByEntityIdLocationIdCriteria(Int32 entityId, Int16 LocationId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<Int16>(LocationIdProperty, LocationId);
                LoadProperty<Int32?>(ClusterSchemeIdProperty, null);
            }

            public FetchByEntityIdLocationIdCriteria(Int32 entityId, Int16 LocationId, Int32? clusterSchemeId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<Int16>(LocationIdProperty, LocationId);
                LoadProperty<Int32?>(ClusterSchemeIdProperty, clusterSchemeId);
            }

            #endregion
        }

        #endregion

        #endregion

        #endregion
    }
}