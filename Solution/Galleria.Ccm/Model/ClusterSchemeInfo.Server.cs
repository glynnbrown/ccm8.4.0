﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629 : A.Kuszyk
//		Created (copied from SA).
#endregion
#region Version History: (CCM 8.0.3)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#region Version History: (CCM 8.1.0)
// V8-29598 : L.Luong
//  Added ProductGroupId And DateLastModified
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Csla;
using Csla.Data;
using Csla.Security;
using Csla.Serialization;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    public partial class ClusterSchemeInfo
    {
        #region Constructors
        private ClusterSchemeInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static ClusterSchemeInfo FetchClusterSchemeInfo(IDalContext dalContext, ClusterSchemeInfoDto dto)
        {
            return DataPortal.FetchChild<ClusterSchemeInfo>(dalContext, dto);
        }


        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Returns a new dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private ClusterSchemeInfoDto GetDataTransferObject()
        {
            return new ClusterSchemeInfoDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                Name = ReadProperty<String>(NameProperty),
                UniqueContentReference = ReadProperty<Guid>(UniqueContentReferenceProperty),
                IsDefault = ReadProperty<Boolean>(IsDefaultProperty),
                ClusterCount = ReadProperty<Int32>(ClusterCountProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                ProductGroupId = ReadProperty<Int32?>(ProductGroupIdProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                ParentUniqueContentReference = ReadProperty<Guid?>(ParentUniqueContentReferenceProperty)
            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, ClusterSchemeInfoDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Guid>(UniqueContentReferenceProperty, dto.UniqueContentReference);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<Boolean>(IsDefaultProperty, dto.IsDefault);
            LoadProperty<Int32>(ClusterCountProperty, dto.ClusterCount);
            LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            LoadProperty<Int32?>(ProductGroupIdProperty, dto.ProductGroupId);
            LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            LoadProperty<Guid?>(ParentUniqueContentReferenceProperty, dto.ParentUniqueContentReference);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when loading this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ClusterSchemeInfoDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #endregion
    }
}