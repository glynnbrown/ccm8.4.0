﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Readonly list holding <see cref="PlanogramComparisonTemplateInfo"/> items.
    /// </summary>
    [Serializable]
    public partial class PlanogramComparisonTemplateInfoList : ModelReadOnlyList<PlanogramComparisonTemplateInfoList, PlanogramComparisonTemplateInfo>
    {
        #region Authorization Rules

        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository

        #endregion

        #region Criteria

        #region FetchByIdsCriteria

        /// <summary>
        /// Criteria for the Fetch factory method
        /// </summary>
        [Serializable]
        public class FetchByIdsCriteria : CriteriaBase<FetchByIdsCriteria>
        {
            #region Properties

            #region DalType
            /// <summary>
            /// DalType property definition
            /// </summary>
            public static readonly PropertyInfo<PlanogramComparisonTemplateDalType> DalTypeProperty =
                RegisterProperty<PlanogramComparisonTemplateDalType>(c => c.DalType);
            /// <summary>
            /// Returns the dal type
            /// </summary>
            public PlanogramComparisonTemplateDalType DalType
            {
                get { return ReadProperty(DalTypeProperty); }
            }
            #endregion

            #region Ids property

            /// <summary>
            /// Ids property definition
            /// </summary>
            public static readonly PropertyInfo<List<Object>> IdsProperty =
                RegisterProperty<List<Object>>(c => c.Ids);

            public List<Object> Ids
            {
                get { return ReadProperty(IdsProperty); }
            }

            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdsCriteria(PlanogramComparisonTemplateDalType sourceType, List<Object> ids)
            {
                LoadProperty(DalTypeProperty, sourceType);
                LoadProperty(IdsProperty, ids);
            }
            #endregion
        }

        #endregion

        #region FetchByEntityIdCriteria

        /// <summary>
        /// Criteria for the Fetch factory method
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : CriteriaBase<FetchByEntityIdCriteria>
        {
            #region Properties

            #region DalType
            /// <summary>
            /// DalType property definition
            /// </summary>
            public static readonly PropertyInfo<PlanogramComparisonTemplateDalType> DalTypeProperty =
                RegisterProperty<PlanogramComparisonTemplateDalType>(c => c.DalType);
            /// <summary>
            /// Returns the dal type
            /// </summary>
            public PlanogramComparisonTemplateDalType DalType
            {
                get { return ReadProperty(DalTypeProperty); }
            }
            #endregion

            #region EntityId
            /// <summary>
            /// EntityId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            /// <summary>
            /// Returns the Entity Id
            /// </summary>
            public Int32 EntityId
            {
                get { return ReadProperty(EntityIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty(DalTypeProperty, PlanogramComparisonTemplateDalType.Unknown);
                LoadProperty(EntityIdProperty, entityId);
            }
            #endregion
        }

        #endregion

        #region FetchByEntityIdIncludingDeletedCriteria

        /// <summary>
        /// Criteria for the Fetch factory method
        /// </summary>
        [Serializable]
        public class FetchByEntityIdIncludingDeletedCriteria : CriteriaBase<FetchByEntityIdIncludingDeletedCriteria>
        {
            #region Properties

            #region DalType
            /// <summary>
            ///     DalType property definition.
            /// </summary>
            public static readonly PropertyInfo<PlanogramComparisonTemplateDalType> DalTypeProperty =
                RegisterProperty<PlanogramComparisonTemplateDalType>(c => c.DalType);
            /// <summary>
            ///     Get the dal type.
            /// </summary>
            public PlanogramComparisonTemplateDalType DalType
            {
                get { return ReadProperty(DalTypeProperty); }
            }
            #endregion

            #region EntityId
            /// <summary>
            ///     EntityId property definition.
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);

            /// <summary>
            ///     Get the Entity Id.
            /// </summary>
            public Int32 EntityId { get { return ReadProperty(EntityIdProperty); } }

            #endregion

            #endregion

            #region Constructor
            /// <summary>
            ///     Creates a new instance of <see cref="FetchByEntityIdIncludingDeletedCriteria"/>.
            /// </summary>
            public FetchByEntityIdIncludingDeletedCriteria(PlanogramComparisonTemplateDalType sourceType, Int32 entityId)
            {
                LoadProperty(DalTypeProperty, sourceType);
                LoadProperty(EntityIdProperty, entityId);
            }
            #endregion
        }

        #endregion

        #endregion
    }
}