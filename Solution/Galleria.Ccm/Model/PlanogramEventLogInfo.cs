﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM810)
// V8-29861 : A.Probyn
//	Created
#endregion

#endregion

using System;
using System.Collections.Generic;

using Csla;
using Csla.Data;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Csla.Security;
using Csla.Serialization;

using Galleria.Ccm.Resources;
using Galleria.Framework.Model;
using Galleria.Ccm.Security;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class PlanogramEventLogInfo : ModelReadOnlyObject<PlanogramEventLogInfo>
    {
        #region Properties

        #region Id
        /// <summary>
        /// The unique event log id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region PlanogramId

        /// <summary>
        /// The planogram id that the event log represents
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanogramIdProperty =
            RegisterModelProperty<Int32>(c => c.PlanogramId);
        public Int32 PlanogramId
        {
            get { return GetProperty<Int32>(PlanogramIdProperty); }
        }

        #endregion

        #region PlanogramName

        /// <summary>
        /// The planogram name that the event log represents
        /// </summary>
        public static readonly ModelPropertyInfo<String> PlanogramNameProperty =
            RegisterModelProperty<String>(c => c.PlanogramName);
        public String PlanogramName
        {
            get { return GetProperty<String>(PlanogramNameProperty); }
        }

        #endregion

        #region ExecutionOrder

        /// <summary>
        /// The event log execution order per planogram
        /// </summary>
        public static readonly ModelPropertyInfo<Int64> ExecutionOrderProperty =
            RegisterModelProperty<Int64>(c => c.ExecutionOrder);
        public Int64 ExecutionOrder
        {
            get { return GetProperty<Int64>(ExecutionOrderProperty); }
        }

        #endregion

        #region EventType
        /// <summary>
        /// EventType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramEventLogEventType> EventTypeProperty =
            RegisterModelProperty<PlanogramEventLogEventType>(c => c.EventType);
        /// <summary>
        /// Returns the Event type
        /// </summary>
        public PlanogramEventLogEventType EventType
        {
            get { return this.GetProperty<PlanogramEventLogEventType>(EventTypeProperty); }
        }
        #endregion

        #region EntryType
        /// <summary>
        /// EntryType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramEventLogEntryType> EntryTypeProperty =
            RegisterModelProperty<PlanogramEventLogEntryType>(c => c.EntryType);
        /// <summary>
        /// Returns the Entry type
        /// </summary>
        public PlanogramEventLogEntryType EntryType
        {
            get { return this.GetProperty<PlanogramEventLogEntryType>(EntryTypeProperty); }
        }
        #endregion

        #region AffectedType
        /// <summary>
        /// AffectedType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramEventLogAffectedType?> AffectedTypeProperty =
            RegisterModelProperty<PlanogramEventLogAffectedType?>(c => c.AffectedType);
        /// <summary>
        /// Returns the Affected type
        /// </summary>
        public PlanogramEventLogAffectedType? AffectedType
        {
            get { return this.GetProperty<PlanogramEventLogAffectedType?>(AffectedTypeProperty); }
        }
        #endregion

        #region AffectedId
        /// <summary>
        /// AffectedId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> AffectedIdProperty =
            RegisterModelProperty<Object>(c => c.AffectedId);
        /// <summary>
        /// Returns the Affected Id
        /// </summary>
        public Object AffectedId
        {
            get { return this.GetProperty<Object>(AffectedIdProperty); }
        }
        #endregion

        #region WorkpackageSource
        /// <summary>
        /// WorkpackageSource property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> WorkpackageSourceProperty =
            RegisterModelProperty<String>(c => c.WorkpackageSource);
        /// <summary>
        /// Returns the name of the Workpackage that generated this Event Log entry.
        /// </summary>
        public String WorkpackageSource
        {
            get { return this.GetProperty<String>(WorkpackageSourceProperty); }
        }
        #endregion

        #region TaskSource
        /// <summary>
        /// TaskSource property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> TaskSourceProperty =
            RegisterModelProperty<String>(c => c.TaskSource);
        /// <summary>
        /// Returns the name of the Engine task that generated this Event Log entry.
        /// </summary>
        public String TaskSource
        {
            get { return this.GetProperty<String>(TaskSourceProperty); }
        }
        #endregion

        #region EventId
        /// <summary>
        /// EventId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EventIdProperty =
            RegisterModelProperty<Int32>(c => c.EventId);
        /// <summary>
        /// Returns the Id of the event that this event log entry relates to. 
        /// </summary>
        /// <remarks>
        /// Event Ids can be found in the Galleria.Ccm.Logging.EventLogEvent enum.
        /// </remarks>
        public Int32 EventId
        {
            get { return this.GetProperty<Int32>(EventIdProperty); }
        }
        #endregion

        #region DateTime
        /// <summary>
        /// DateTime property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateTimeProperty =
            RegisterModelProperty<DateTime>(c => c.DateTime);
        /// <summary>
        /// Returns the DateTime
        /// </summary>
        public DateTime DateTime
        {
            get { return this.GetProperty<DateTime>(DateTimeProperty); }
        }
        #endregion

        #region Description
        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);
        /// <summary>
        /// Returns the EventLog Description
        /// </summary>
        public String Description
        {
            get { return this.GetProperty<String>(DescriptionProperty); }
        }
        #endregion

        #region Content
        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ContentProperty =
            RegisterModelProperty<String>(c => c.Content);
        /// <summary>
        /// Returns the EventLog Description
        /// </summary>
        public String Content
        {
            get { return this.GetProperty<String>(ContentProperty); }
        }
        #endregion

        #region Score
        /// <summary>
        /// Score property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> ScoreProperty =
            RegisterModelProperty<Byte>(c => c.Score);
        /// <summary>
        /// Returns the EventLog Score
        /// </summary>
        public Byte Score
        {
            get { return this.GetProperty<Byte>(ScoreProperty); }
        }
        #endregion


        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PlanogramEventLogInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanogramEventLogInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(PlanogramEventLogInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanogramEventLogInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

    }
}
