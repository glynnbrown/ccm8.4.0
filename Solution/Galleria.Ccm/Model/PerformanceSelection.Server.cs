﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26159 : L.Ineson
//	Created (Auto-generated)
// V8-27763 : N.Foster
//  Added FetchByName
// V8-26953 : I.George
//  Added PerformanceSelectionMetricList
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    public partial class PerformanceSelection
    {
        #region Constructors
        private PerformanceSelection() { } //force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing PerformanceSelection with the given id
        /// </summary>
        public static PerformanceSelection FetchById(Int32 id)
        {
            return DataPortal.Fetch<PerformanceSelection>(new FetchByIdCriteria(id));
        }

        /// <summary>
        /// Returns an existing performance selection with the given name
        /// </summary>
        public static PerformanceSelection FetchByEntityIdName(Int32 entityId, String name)
        {
            return DataPortal.Fetch<PerformanceSelection>(new FetchByEntityIdNameCriteria(entityId, name));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PerformanceSelectionDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Int32>(GFSPerformanceSourceIdProperty, dto.GFSPerformanceSourceId);
            this.LoadProperty<PerformanceSelectionTimeType>(TimeTypeProperty, (PerformanceSelectionTimeType)dto.TimeType);
            this.LoadProperty<PerformanceSelectionType>(SelectionTypeProperty, (PerformanceSelectionType)dto.SelectionType);
            this.LoadProperty<Int32>(DynamicValueProperty, dto.DynamicValue);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);

            this.LoadProperty<PerformanceSelectionTimelineGroupList>(TimelineGroupsProperty,
               PerformanceSelectionTimelineGroupList.FetchByPerformanceSelectionId(dalContext, dto.Id));

            this.LoadProperty<PerformanceSelectionMetricList>(MetricsProperty,
                PerformanceSelectionMetricList.FetchByPerformanceSelectionId(dalContext, dto.Id));
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private PerformanceSelectionDto GetDataTransferObject()
        {
            return new PerformanceSelectionDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                Name = ReadProperty<String>(NameProperty),
                GFSPerformanceSourceId = ReadProperty<Int32>(GFSPerformanceSourceIdProperty),
                TimeType = (Byte)ReadProperty<PerformanceSelectionTimeType>(TimeTypeProperty),
                SelectionType = (Byte)ReadProperty<PerformanceSelectionType>(SelectionTypeProperty),
                DynamicValue = ReadProperty<Int32>(DynamicValueProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty),

            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning FetchById
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPerformanceSelectionDal dal = dalContext.GetDal<IPerformanceSelectionDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.Id));
                }
            }
        }

        /// <summary>
        /// Called when returning FetchByName
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdNameCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPerformanceSelectionDal dal = dalContext.GetDal<IPerformanceSelectionDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchByEntityIdName(criteria.EntityId, criteria.Name));
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                PerformanceSelectionDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (IPerformanceSelectionDal dal = dalContext.GetDal<IPerformanceSelectionDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<PerformanceSelection>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this instance
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                PerformanceSelectionDto dto = GetDataTransferObject();
                using (IPerformanceSelectionDal dal = dalContext.GetDal<IPerformanceSelectionDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when this instance is being deleted
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IPerformanceSelectionDal dal = dalContext.GetDal<IPerformanceSelectionDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #endregion

    }
}