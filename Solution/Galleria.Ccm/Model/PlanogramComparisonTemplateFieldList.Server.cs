﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramComparisonTemplateFieldList
    {
        #region Constructor

        /// <summary>
        ///     Private constructor to enforce use of factory methods.
        /// </summary>
        private PlanogramComparisonTemplateFieldList() { }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        internal static PlanogramComparisonTemplateFieldList FetchByParentId(IDalContext dalContext, Object parentId)
        {
            return DataPortal.FetchChild<PlanogramComparisonTemplateFieldList>(dalContext, new FetchByParentIdCriteria(null, parentId));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        ///     Called by reflection when fetching an instance of <see cref="PlanogramComparisonTemplateFieldList" />.
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, FetchByParentIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateFieldDal>())
            {
                IEnumerable<PlanogramComparisonTemplateFieldDto> dtoList = dal.FetchByPlanogramComparisonTemplateId(criteria.ParentId);
                foreach (PlanogramComparisonTemplateFieldDto dto in dtoList)
                {
                    Add(PlanogramComparisonTemplateField.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
            MarkAsChild();
        }

        #endregion

        #endregion
    }
}
