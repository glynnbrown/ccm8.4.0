#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26076 : A.Silva ~ Removed Id property as identifier, user Path Property instead.

#endregion

#region Version History: (CCM 8.3)
// V8-31541 : L.Ineson
//  Added Id and updated implementation.
#endregion

#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.ComponentModel;

namespace Galleria.Ccm.Model
{
    public sealed partial class CustomColumnSort
    {
        #region Constructor
        private CustomColumnSort(){} //force use of factory methods.
        #endregion

        #region Factory Methods

        /// <summary>
        ///     Returns a existing <see cref="CustomColumnSort" />.
        /// </summary>
        /// <param name="dalContext">DAL context to access the data.</param>
        /// <param name="dto"></param>
        /// <returns>
        ///     Returns an existing <see cref="CustomColumnSort" />.
        /// </returns>
        internal static CustomColumnSort FetchCustomColumnSort(IDalContext dalContext, CustomColumnSortDto dto)
        {
            return DataPortal.FetchChild<CustomColumnSort>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Returns a new <see cref="CustomColumnSortDto" /> with this instance's data.
        /// </summary>
        /// <returns>A new dto</returns>
        private CustomColumnSortDto GetDataTransferObject()
        {
            return new CustomColumnSortDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                Path = ReadProperty<String>(PathProperty),
                Direction = (Byte)ReadProperty<ListSortDirection>(DirectionProperty),
                CustomColumnLayoutId = this.Parent.Id
            };
        }

        /// <summary>
        ///     Loads the data from a <see cref="CustomColumnSortDto" /> in this instance.
        /// </summary>
        /// <param name="dalContext">DAL context to access the data.</param>
        /// <param name="dto">The <see cref="CustomColumnSortDto" /> to load data from.</param>
        private void LoadDataTransferObject(IDalContext dalContext, CustomColumnSortDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<String>(PathProperty, dto.Path);
            LoadProperty<ListSortDirection>(DirectionProperty, (ListSortDirection)dto.Direction);
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Called via reflection when retrieving a <see cref="CustomColumnSort" />.
        /// </summary>
        /// <param name="dalContext">DAL context to access the data.</param>
        /// <param name="dto"><see cref="CustomColumnSortDto" /> containing the data to load into this instance.</param>
        private void Child_Fetch(IDalContext dalContext, CustomColumnSortDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Called via reflection when inserting a <see cref="CustomColumnSort" /> in a data store.
        /// </summary>
        /// <param name="dalContext">DAL context to access the data.</param>
        private void Child_Insert(IDalContext dalContext)
        {
            using (ICustomColumnSortDal dal = dalContext.GetDal<ICustomColumnSortDal>())
            {
                CustomColumnSortDto dto = GetDataTransferObject();
                dal.Insert(dto);

                LoadProperty<Int32>(IdProperty, dto.Id);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Update

        /// <summary>
        ///     Called via reflection when updating a <see cref="CustomColumnSort" /> in a data store.
        /// </summary>
        /// <param name="dalContext">DAL context to access the data.</param>
        private void Child_Update(IDalContext dalContext)
        {
            using (ICustomColumnSortDal dal = dalContext.GetDal<ICustomColumnSortDal>())
            {
                CustomColumnSortDto dto = GetDataTransferObject();
                dal.Update(dto);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Called via reflection when deleting the <see cref="CustomColumnSort" /> from a data store.
        /// </summary>
        /// <param name="dalContext">DAL context to access the data.</param>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (ICustomColumnSortDal dal = dalContext.GetDal<ICustomColumnSortDal>())
            {
                dal.DeleteById(this.Id);
            }
        }

        #endregion

        #endregion
    }
}