﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public sealed partial class ProductLevelList
    {
        #region Constructor
        private ProductLevelList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        /// <returns>A list of existing it</returns>
        internal static ProductLevelList FetchByParentProductLevelId(
            IDalContext dalContext, Int32? parentId, IEnumerable<ProductLevelDto> productLevelDtoList)
        {
            return DataPortal.FetchChild<ProductLevelList>(dalContext, parentId, productLevelDtoList);
        }

        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returing all items for a given parent id
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32? parentId, IEnumerable<ProductLevelDto> productLevelDtoList)
        {
            RaiseListChangedEvents = false;

            //get a list of child dtos and add them to this
            foreach (ProductLevelDto childDto in productLevelDtoList)
            {
                if (childDto.ParentLevelId == parentId)
                {
                    this.Add(ProductLevel.Fetch(dalContext, childDto, productLevelDtoList));
                }
            }

            RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}