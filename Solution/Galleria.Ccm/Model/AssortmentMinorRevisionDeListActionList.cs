﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a list containing AssortmentMinorRevisionDeListAction objects
    /// </summary>
    [Serializable]
    public sealed partial class AssortmentMinorRevisionDeListActionList : ModelList<AssortmentMinorRevisionDeListActionList, AssortmentMinorRevisionDeListAction>
    {
        #region Parent

        /// <summary>
        /// Returns the parent Assortment
        /// </summary>
        public AssortmentMinorRevision Parent
        {
            get { return base.Parent as AssortmentMinorRevision; }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionDeListActionList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.AssortmentMinorRevisionCreate.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionDeListActionList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.AssortmentMinorRevisionGet.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionDeListActionList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.AssortmentMinorRevisionEdit.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionDeListActionList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.AssortmentMinorRevisionDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        internal static AssortmentMinorRevisionDeListActionList NewAssortmentMinorRevisionDeListActionList()
        {
            AssortmentMinorRevisionDeListActionList item = new AssortmentMinorRevisionDeListActionList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private new void Create()
        {
            MarkAsChild();
            base.Create();
        }
        #endregion

        public new void Remove()
        {

        }

        #endregion
    }
}

