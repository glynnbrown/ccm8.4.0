﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of PrintTemplateSection objects.
    /// </summary>
    [Serializable]
    public sealed partial class PrintTemplateSectionList : ModelList<PrintTemplateSectionList, PrintTemplateSection>
    {
        #region Parent

        public new PrintTemplateSectionGroup Parent
        {
            get { return base.Parent as PrintTemplateSectionGroup; }
        }

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PrintTemplateSectionList NewPrintTemplateSectionList()
        {
            PrintTemplateSectionList item = new PrintTemplateSectionList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Adds a new section to the end of the list.
        /// </summary>
        public PrintTemplateSection AddNewSection()
        {
            PrintTemplateSectionGroup parentGroup = this.Parent;
            PrintTemplate parentTemplate = (parentGroup != null) ? parentGroup.Parent : null;

            //get the paper size
            PrintTemplatePaperSizeType paperSize = (parentTemplate != null)? 
                parentTemplate.PaperSize                
                : PrintTemplatePaperSizeType.A4;

            //get the new section number
            Int32 sectionNumber = (parentGroup != null)?
                parentGroup.Sections.Count +1 
                : 1;
            
            //Add it
            PrintTemplateSection item = PrintTemplateSection.NewPrintTemplateSection(paperSize, (Byte)sectionNumber);
            this.Add(item);
            return item;
        }

        /// <summary>
        /// Adds a new section as the given section number
        /// </summary>
        public PrintTemplateSection AddNewSection(Byte newSectionNumber)
        {
            PrintTemplateSectionGroup parentGroup = this.Parent;
            PrintTemplate parentTemplate = (parentGroup != null) ? parentGroup.Parent : null;

            //get the paper size
            PrintTemplatePaperSizeType paperSize = (parentTemplate != null) ?
                parentTemplate.PaperSize
                : PrintTemplatePaperSizeType.A4;


            // loop through all the other sections and increment the section number
            foreach (PrintTemplateSection section in this)
            {
                // if section number is equal to or more than the new section then increment
                if (section.Number >= newSectionNumber)
                {
                    section.Number++;
                }
            }

            //Add it
            PrintTemplateSection item = PrintTemplateSection.NewPrintTemplateSection(paperSize, newSectionNumber);
            this.Add(item);
            return item;
        }

        #endregion
    }

}