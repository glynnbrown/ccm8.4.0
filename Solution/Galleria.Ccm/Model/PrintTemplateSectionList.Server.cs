﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public sealed partial class PrintTemplateSectionList
    {
        #region Constructor
        private PrintTemplateSectionList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Fetches the list by its parent id
        /// </summary>
        internal static PrintTemplateSectionList FetchByParentId(IDalContext dalContext, Int32 parentId)
        {
            return DataPortal.FetchChild<PrintTemplateSectionList>(dalContext, parentId);
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returing all items for a given parent id
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 parentId)
        {
            RaiseListChangedEvents = false;
            using (IPrintTemplateSectionDal dal = dalContext.GetDal<IPrintTemplateSectionDal>())
            {
                IEnumerable<PrintTemplateSectionDto> dtoList = dal.FetchByPrintTemplateSectionGroupId(parentId);
                foreach (PrintTemplateSectionDto dto in dtoList)
                {
                    this.Add(PrintTemplateSection.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}