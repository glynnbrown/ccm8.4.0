﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25395 : A.Probyn
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class ProductLibraryColumnMapping
    {
        #region Constructor
        private ProductLibraryColumnMapping() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static ProductLibraryColumnMapping Fetch(IDalContext dalContext, ProductLibraryColumnMappingDto dto)
        {
            return DataPortal.FetchChild<ProductLibraryColumnMapping>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, ProductLibraryColumnMappingDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<String>(SourceProperty, dto.Source);
            this.LoadProperty<String>(DestinationProperty, dto.Destination);
            this.LoadProperty<Boolean>(IsMandatoryProperty, dto.IsMandatory);
            this.LoadProperty<String>(TypeProperty, dto.Type);
            this.LoadProperty<Single>(MinProperty, dto.Min);
            this.LoadProperty<Single>(MaxProperty, dto.Max);
            this.LoadProperty<Boolean>(CanDefaultProperty, dto.CanDefault);
            this.LoadProperty<String>(DefaultProperty, dto.Default);
            this.LoadProperty<Int32>(TextFixedWidthColumnStartProperty, dto.TextFixedWidthColumnStart);
            this.LoadProperty<Int32>(TextFixedWidthColumnEndProperty, dto.TextFixedWidthColumnEnd);

        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private ProductLibraryColumnMappingDto GetDataTransferObject(ProductLibrary parent)
        {
            return new ProductLibraryColumnMappingDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                ProductLibraryId = parent.Id,
                Source = ReadProperty<String>(SourceProperty),
                Destination = ReadProperty<String>(DestinationProperty),
                IsMandatory = ReadProperty<Boolean>(IsMandatoryProperty),
                Type = ReadProperty<String>(TypeProperty),
                Min = ReadProperty<Single>(MinProperty),
                Max = ReadProperty<Single>(MaxProperty),
                CanDefault = ReadProperty<Boolean>(CanDefaultProperty),
                Default = ReadProperty<String>(DefaultProperty),
                TextFixedWidthColumnStart = ReadProperty<Int32>(TextFixedWidthColumnStartProperty),
                TextFixedWidthColumnEnd = ReadProperty<Int32>(TextFixedWidthColumnEndProperty),

            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ProductLibraryColumnMappingDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, ProductLibrary parent)
        {
            ProductLibraryColumnMappingDto dto = this.GetDataTransferObject(parent);
            Object oldId = dto.Id;
            using (IProductLibraryColumnMappingDal dal = dalContext.GetDal<IProductLibraryColumnMappingDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<ProductLibraryColumnMapping>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, ProductLibrary parent)
        {
            if (this.IsSelfDirty)
            {
                using (IProductLibraryColumnMappingDal dal = dalContext.GetDal<IProductLibraryColumnMappingDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, ProductLibrary parent)
        {
            using (IProductLibraryColumnMappingDal dal = dalContext.GetDal<IProductLibraryColumnMappingDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion
    }
}