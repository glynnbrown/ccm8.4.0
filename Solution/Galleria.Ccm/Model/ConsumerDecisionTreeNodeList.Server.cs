﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8025632 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class ConsumerDecisionTreeNodeList
    {
        #region Constructor
        private ConsumerDecisionTreeNodeList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        /// <returns>A list of existing it</returns>
        internal static ConsumerDecisionTreeNodeList FetchListByParentNodeId(
            IDalContext dalContext, Int32 parentId, IEnumerable<ConsumerDecisionTreeNodeDto> masterNodeDtoList)
        {
            return DataPortal.FetchChild<ConsumerDecisionTreeNodeList>(dalContext, parentId, masterNodeDtoList);
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parentNodeId">The parent node id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 parentId, IEnumerable<ConsumerDecisionTreeNodeDto> masterNodeDtoList)
        {
            RaiseListChangedEvents = false;

            //get a list of child dtos and add them to this
            IEnumerable<ConsumerDecisionTreeNodeDto> childList = masterNodeDtoList.Where(p => p.ParentNodeId == parentId);
            foreach (ConsumerDecisionTreeNodeDto childDto in childList)
            {
                this.Add(ConsumerDecisionTreeNode.FetchConsumerDecisionTreeNode(dalContext, childDto, masterNodeDtoList));
            }

            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}
