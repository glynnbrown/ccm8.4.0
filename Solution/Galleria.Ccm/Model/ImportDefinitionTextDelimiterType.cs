﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (V8 1.0)
// V8-25395 : A.Probyn
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Enum of values available for ImportDefinition DataFormat property
    /// Represents a type of the data format for txt files
    /// </summary>
    public enum ImportDefinitionTextDelimiterType
    {
        Tab = 1,
        Semicolon = 2,
        Comma = 3,
        Pipe = 5
    }

    public static class ImportDefinitionTextDelimiterTypeHelper
    {
        public static readonly Dictionary<ImportDefinitionTextDelimiterType, String> FriendlyNames =
            new Dictionary<ImportDefinitionTextDelimiterType, String>()
            {
                {ImportDefinitionTextDelimiterType.Comma, Message.Enum_ImportDefinitionTextDelimiterType_Comma},
                {ImportDefinitionTextDelimiterType.Pipe, Message.Enum_ImportDefinitionTextDelimiterType_Pipe},
                {ImportDefinitionTextDelimiterType.Semicolon, Message.Enum_ImportDefinitionTextDelimiterType_Semicolon},
                {ImportDefinitionTextDelimiterType.Tab, Message.Enum_ImportDefinitionTextDelimiterType_Tab},

            };

        public static ImportDefinitionTextDelimiterType? ImportDefinitionTextDelimiterTypeGetEnum(String description)
        {
            foreach (KeyValuePair<ImportDefinitionTextDelimiterType, String> keyPair in ImportDefinitionTextDelimiterTypeHelper.FriendlyNames)
            {
                if (keyPair.Value.Equals(description))
                {
                    return keyPair.Key;
                }
            }
            return null;
        }
    }
}
