﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class LocationSpaceBayList : ModelList<LocationSpaceBayList, LocationSpaceBay>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new LocationSpaceProductGroup Parent
        {
            get { return (LocationSpaceProductGroup)base.Parent; }
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationSpaceBayList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationSpaceCreate.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceBayList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.LocationSpaceGet.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceBayList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationSpaceEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceBayList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationSpaceDelete.ToString()));
        }

        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new list
        /// </summary>
        public static LocationSpaceBayList NewLocationSpaceBayList(Boolean createDefaultBay = true)
        {
            LocationSpaceBayList list = new LocationSpaceBayList();
            list.Create(createDefaultBay);
            return list;
        }
        #endregion

        #region Data Access
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        #region Create
        private void Create(Boolean createDefaultBay)
        {
            MarkAsChild();
            //Create default bay
            if (createDefaultBay)
            {
                AddNew();
            }
        }
        #endregion
        #endregion

        #region Criteria

        [Serializable]
        public class FetchByLocationSpaceProductGroupIdCriteria : CriteriaBase<FetchByLocationSpaceProductGroupIdCriteria>
        {
            #region Properties

            public static PropertyInfo<Int32> LocationSpaceProductGroupIdProperty =
                RegisterProperty<Int32>(c => c.LocationSpaceProductGroupId);
            public Int32 LocationSpaceProductGroupId
            {
                get { return ReadProperty<Int32>(LocationSpaceProductGroupIdProperty); }
            }

            #endregion

            public FetchByLocationSpaceProductGroupIdCriteria(Int32 locationSpaceProductGroupId)
            {
                LoadProperty<Int32>(LocationSpaceProductGroupIdProperty, locationSpaceProductGroupId);
            }
        }

        #endregion
    }
}
