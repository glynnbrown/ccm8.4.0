﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24265 : N.Haywood
//  Created
// V8-27940 : L.Luong
//  Added EntityId and DateDeleted
#endregion

#region Version History: (CCM 8.03)
// V8-29313 : L.Ineson
//  Added toString override.
#endregion

#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Model;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class LabelInfo : ModelReadOnlyObject<LabelInfo>
    {
        #region Properties

        #region Id

        public static readonly ModelPropertyInfo<Object> IdProperty =
         RegisterModelProperty<Object>(c => c.Id);
        /// <summary>
        /// The unique object id
        /// </summary>
        public Object Id
        {
            get { return GetProperty<Object>(IdProperty); }
        }

        #endregion

        #region FileName

        /// <summary>
        /// FileName property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> FileNameProperty =
            RegisterModelProperty<String>(c => c.FileName);
        /// <summary>
        /// Returns the name of the file this info represents
        /// if of a file system type.
        /// </summary>
        public String FileName
        {
            get { return this.GetProperty<String>(FileNameProperty); }
        }

        #endregion

        #region EntityId

        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(o => o.EntityId);
        /// <summary>
        ///		Gets or sets the value for the <see cref="EntityId"/> property.
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty(EntityIdProperty); }
        }

        #endregion

        #region Name

        public static readonly ModelPropertyInfo<String> NameProperty =
           RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The product name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        #endregion

        #region Type

        public static readonly ModelPropertyInfo<LabelType> TypeProperty =
            RegisterModelProperty<LabelType>(c => c.Type);
        /// <summary>
        /// The product name
        /// </summary>
        public LabelType Type
        {
            get { return GetProperty<LabelType>(TypeProperty); }
        }

        #endregion

        #region DateDeleted

        /// <summary>
        /// The date when the item record was deleted 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);

        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        #endregion
    }
}
