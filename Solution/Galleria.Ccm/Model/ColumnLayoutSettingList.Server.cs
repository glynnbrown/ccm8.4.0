﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.

#endregion

#endregion

using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Server side implementation of the <see cref="ColumnLayoutSettingList"/> class.
    /// </summary>
    public partial class ColumnLayoutSettingList
    {
        #region Constructor
        private ColumnLayoutSettingList() { }
        #endregion

        #region Factory Methods

        /// <summary>
        ///     Fetches a list with all the <see cref="ColumnLayoutSetting"/> instances in the DAL.
        /// </summary>
        /// <returns>A new instance of <see cref="ColumnLayoutSettingList"/> with the data.</returns>
        internal static ColumnLayoutSettingList Fetch(IDalContext dalContext)
        {
            return DataPortal.FetchChild<ColumnLayoutSettingList>(dalContext);
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        ///     Called by reflection by CSLA when fetching this item as a child.
        /// </summary>
        private void Child_Fetch(IDalContext dalContext)
        {
            RaiseListChangedEvents = false;

            using (var dal = dalContext.GetDal<IColumnLayoutSettingDal>())
            {
               dal.FetchAll().ToList().ForEach(dto => Add(ColumnLayoutSetting.GetColumnLayoutSetting(dalContext,dto)));
            }

            RaiseListChangedEvents = true;
        }

        #endregion

        #region Update

        /// <summary>
        ///     Called by reflection by CSLA when updating the item as a child.
        /// </summary>
        private void Child_Update(IDalContext dalContext)
        {
            using (dalContext)
            {
                dalContext.Begin();
                Child_Update(dalContext);
                dalContext.Commit();
            }
        }

        #endregion

        #endregion
    }
}
