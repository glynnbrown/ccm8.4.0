﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Read only list containing <see cref="RenumberingStrategyInfo" /> instances.
    /// </summary>
    /// <remarks>This is a root object.</remarks>
    [Serializable]
    public sealed partial class RenumberingStrategyInfoList :
        ModelReadOnlyList<RenumberingStrategyInfoList, RenumberingStrategyInfo>
    {
        #region Authorization Rules
        /// <summary>
        ///     Defines the authorization rules for <see cref="RenumberingStrategyInfoList" />.
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(RenumberingStrategyInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(RenumberingStrategyInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(RenumberingStrategyInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(RenumberingStrategyInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Criteria

        #region FetchByEntityIdCriteria

        /// <summary>
        ///     Criteria type used to retrieve <see cref="RenumberingStrategyInfo" /> instances according to their Entity Id.
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : CriteriaBase<FetchByEntityIdCriteria>
        {
            #region EntityId

            /// <summary>
            ///     Metadata for the <see cref="EntityId" /> property.
            /// </summary>
            private static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(o => o.EntityId);

            /// <summary>
            ///     Gets or sets the value for the <see cref="EntityId" /> property.
            /// </summary>
            public Int32 EntityId
            {
                get { return ReadProperty(EntityIdProperty); }
            }

            #endregion

            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty(EntityIdProperty, entityId);
            }
        }

        #endregion

        #region FetchByEntityIdIncludingDeletedCriteria

        /// <summary>
        ///     Criteria type used to retrieve <see cref="RenumberingStrategyInfo" /> instances according to their Entity Id,
        ///     including deleted records.
        /// </summary>
        [Serializable]
        public class FetchByEntityIdIncludingDeletedCriteria : CriteriaBase<FetchByEntityIdIncludingDeletedCriteria>
        {
            #region EntityId

            /// <summary>
            ///     Metadata for the <see cref="EntityId" /> property.
            /// </summary>
            private static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(o => o.EntityId);

            /// <summary>
            ///     Gets or sets the value for the <see cref="EntityId" /> property.
            /// </summary>
            public Int32 EntityId
            {
                get { return ReadProperty(EntityIdProperty); }
            }

            #endregion

            public FetchByEntityIdIncludingDeletedCriteria(Int32 entityId)
            {
                LoadProperty(EntityIdProperty, entityId);
            }
        }

        #endregion

        #endregion
    }
}