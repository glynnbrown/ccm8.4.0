﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using System.Text;

namespace Galleria.Ccm.Model
{
    public partial class CompressionList
    {
        #region Constructor
        private CompressionList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        public static CompressionList FetchAll()
        {
            return DataPortal.Fetch<CompressionList>();
        }

        public static CompressionList FetchAllEnabled()
        {
            return DataPortal.Fetch<CompressionList>(new SingleCriteria<Boolean>(true));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returning all library types
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch()
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ICompressionDal dal = dalContext.GetDal<ICompressionDal>())
                {
                    IEnumerable<CompressionDto> dtoList = dal.FetchAll();
                    foreach (CompressionDto dto in dtoList)
                    {
                        this.Add(Compression.GetCompression(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returning all library types that are enabled
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(SingleCriteria<Boolean> criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ICompressionDal dal = dalContext.GetDal<ICompressionDal>())
                {
                    IEnumerable<CompressionDto> dtoList = dal.FetchAllEnabled();
                    foreach (CompressionDto dto in dtoList)
                    {
                        this.Add(Compression.GetCompression(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #region Update
        /// <summary>
        /// Called when this list is being updated
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                Child_Update(dalContext);
                dalContext.Commit();
            }
        }
        #endregion
        #endregion
    }
}
