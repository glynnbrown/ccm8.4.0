﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.DataStructures;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class ProductImage
    {
        #region Constructor
        private ProductImage() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the product image for the given criteria
        /// </summary>
        /// <param name="productGtin">the product code</param>
        /// <param name="imageType"></param>
        /// <param name="facingType"></param>
        /// <param name="compressionId"></param>
        /// <returns></returns>
        public static ProductImage FetchByProductIdImageTypeFacingTypeCompressionId
            (Int32 productId, ProductImageType imageType, ProductImageFacing facingType, Int32 compressionId)
        {
            return DataPortal.Fetch<ProductImage>(new FetchByProductIdImageTypeFacingTypeCompressionIdCriteria(
                productId, imageType, facingType, compressionId));
        }


        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <returns>An existing unit</returns>
        internal static ProductImage GetProductImage(IDalContext dalContext, ProductImageDto dto)
        {
            return DataPortal.FetchChild<ProductImage>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns></returns>
        private ProductImageDto GetDataTransferObject()
        {
            return this.GetDataTransferObject(null);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private ProductImageDto GetDataTransferObject(Product parent)
        {
            return new ProductImageDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                ProductId = parent == null ? ReadProperty<Int32>(ProductIdProperty) : parent.Id,
                ImageId = ReadProperty<Int32>(ImageIdProperty),
                ImageType = (Byte)ReadProperty<ProductImageType>(ImageTypeProperty),
                FacingType = (Byte)ReadProperty<ProductImageFacing>(FacingTypeProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty)
            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, ProductImageDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            LoadProperty<Int32>(ProductIdProperty, dto.ProductId);
            LoadProperty<Int32>(ImageIdProperty, dto.ImageId);
            LoadProperty<ProductImageType>(ImageTypeProperty, (ProductImageType)(Byte)dto.ImageType);
            LoadProperty<ProductImageFacing>(FacingTypeProperty, (ProductImageFacing)(Byte)dto.FacingType);
            LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching by ProductId ImageType FacingType CompressionId
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByProductIdImageTypeFacingTypeCompressionIdCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductImageDal dal = dalContext.GetDal<IProductImageDal>())
                {
                    var dto = dal.FetchByProductIdImageTypeFacingTypeCompressionId(
                        criteria.ProductId,
                        (Byte)criteria.ImageType,
                        (Byte)criteria.FacingType,
                        criteria.CompressionId);
                    LoadDataTransferObject(dalContext, dto);
                }
            }
        }

        /// <summary>
        /// Called when returning an existing item
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ProductImageDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a new object into the solution
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent model</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, Product parent)
        {
            //update children first as we need image ids
            FieldManager.UpdateChildren(dalContext, this);

            //If productimagedata is null then the imageId should already be
            //populated. Lazy loading the ProductImageData at this point
            //would cause vistaDb to throw an error. If ProductImageData is not
            //null then we need to update the imageId to ensure that it is
            //correct.
            if (GetProperty<Image>(ProductImageDataProperty) != null)
            {
                LoadProperty<Int32>(ImageIdProperty, ProductImageData.Id);
            }
            // update the data source
            using (IProductImageDal dal = dalContext.GetDal<IProductImageDal>())
            {
                // get the dto
                ProductImageDto dto = GetDataTransferObject(parent);
                dal.Insert(dto);

                //update this.id from the dto
                LoadProperty<Int32>(IdProperty, dto.Id);
                LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            }
        }

        private void Child_Insert(IDalContext dalContext)
        {
            //update children first as we need image ids
            FieldManager.UpdateChildren(dalContext, this);

            //If productimagedata is null then the imageId should already be
            //populated. Lazy loading the ProductImageData at this point
            //would cause vistaDb to throw an error. If ProductImageData is not
            //null then we need to update the imageId to ensure that it is
            //correct.
            if (GetProperty<Image>(ProductImageDataProperty) != null)
            {
                LoadProperty<Int32>(ImageIdProperty, ProductImageData.Id);
            }
            // update the data source
            using (IProductImageDal dal = dalContext.GetDal<IProductImageDal>())
            {
                // get the dto
                ProductImageDto dto = GetDataTransferObject();
                dal.Insert(dto);

                //update this.id from the dto
                LoadProperty<Int32>(IdProperty, dto.Id);
                LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IProductImageDal dal = dalContext.GetDal<IProductImageDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion        
    }
}
