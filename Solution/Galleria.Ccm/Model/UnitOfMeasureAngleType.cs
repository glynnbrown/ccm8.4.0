﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26179  : I.George
//  Created
// V8-28149 : M.Pettit
//  Ensured all types have friendly names for reporting
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;


namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Denotes the available angle unit of measure types
    /// </summary>
    public enum UnitOfMeasureAngleType
    {
        Unknown = 0,
        Radians = 1,
        Degrees = 2
    }
    /// <summary>
    /// UnitOfMeasureAngleTypeHelper Class
    /// </summary>
    public static class UnitOfMeasureAngleTypeHelper
    {
        public static readonly Dictionary<UnitOfMeasureAngleType, String> FriendlyNames =
            new Dictionary<UnitOfMeasureAngleType, String>()
            {
                {UnitOfMeasureAngleType.Unknown, String.Empty},
                {UnitOfMeasureAngleType.Radians, Message.Enum_UnitOfMeasureAngleType_Radians},
                {UnitOfMeasureAngleType.Degrees, Message.Enum_UnitOfMeasureAngleType_Degrees},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<UnitOfMeasureAngleType, String> Abbreviations =
            new Dictionary<UnitOfMeasureAngleType, String>()
            {
                {UnitOfMeasureAngleType.Unknown, String.Empty},
                {UnitOfMeasureAngleType.Radians, Message.Enum_UnitOfMeasureAngleType_Radians_Abbrev},
                {UnitOfMeasureAngleType.Degrees, Message.Enum_UnitOfMeasureAngleType_Degrees_Abbrev},
            };
    }
}
