﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// AssortmentMinorRevisionReplaceActionLocation Model object
    /// (Child of AssortmentMinorRevisionReplaceAction)
    /// This is a child Content object and so it has the following properties/actions:
    /// - This object does NOT have a DateDeleted property or DateDeleted field in its supporting database table
    /// - if its parent is deleted, it is not removed or marked as deleted (this allows for parent 'undeletes')
    /// - if it is deleted directly, it's associated record is removed from the database. 
    [Serializable]
    [DefaultNewMethod("NewAssortmentMinorRevisionReplaceActionLocation")]
    public sealed partial class AssortmentMinorRevisionReplaceActionLocation : ModelObject<AssortmentMinorRevisionReplaceActionLocation>, IAssortmentMinorRevisionActionLocation
    {
        #region Static Constructor
        static AssortmentMinorRevisionReplaceActionLocation()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns the parent Assortment minor revision action
        /// </summary>
        public AssortmentMinorRevisionReplaceAction ParentAssortmentMinorRevisionAction
        {
            get { return base.Parent as AssortmentMinorRevisionReplaceAction; }
        }

        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The item unique id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<String> LocationCodeProperty =
            RegisterModelProperty<String>(c => c.LocationCode);
        public String LocationCode
        {
            get { return GetProperty<String>(LocationCodeProperty); }
            set { SetProperty<String>(LocationCodeProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> LocationNameProperty =
            RegisterModelProperty<String>(c => c.LocationName);
        public String LocationName
        {
            get { return GetProperty<String>(LocationNameProperty); }
            set { SetProperty<String>(LocationNameProperty, value); }
        }


        public static readonly ModelPropertyInfo<Int16?> LocationIdProperty =
       RegisterModelProperty<Int16?>(c => c.LocationId);
        public Int16? LocationId
        {
            get { return GetProperty<Int16?>(LocationIdProperty); }
            set { SetProperty<Int16?>(LocationIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32> UnitsProperty =
            RegisterModelProperty<Int32>(c => c.Units);
        public Int32 Units
        {
            get { return GetProperty<Int32>(UnitsProperty); }
            set { SetProperty<Int32>(UnitsProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32> FacingsProperty =
            RegisterModelProperty<Int32>(c => c.Facings);
        public Int32 Facings
        {
            get { return GetProperty<Int32>(FacingsProperty); }
            set { SetProperty<Int32>(FacingsProperty, value); }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionReplaceActionLocation), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.AssortmentMinorRevisionCreate.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionReplaceActionLocation), new IsInRole(AuthorizationActions.GetObject, DomainPermission.AssortmentMinorRevisionGet.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionReplaceActionLocation), new IsInRole(AuthorizationActions.EditObject, DomainPermission.AssortmentMinorRevisionEdit.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionReplaceActionLocation), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.AssortmentMinorRevisionDelete.ToString()));
        }
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(LocationCodeProperty));
            BusinessRules.AddRule(new MaxLength(LocationCodeProperty, 50));
            BusinessRules.AddRule(new Required(LocationNameProperty));
            BusinessRules.AddRule(new MaxLength(LocationNameProperty, 50));

        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        internal static AssortmentMinorRevisionReplaceActionLocation NewAssortmentMinorRevisionReplaceActionLocation()
        {
            AssortmentMinorRevisionReplaceActionLocation item = new AssortmentMinorRevisionReplaceActionLocation();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentMinorRevisionReplaceActionLocation NewAssortmentMinorRevisionReplaceActionLocation(Int16 locationId, String code, String name, Int32 units, Int32 facings)
        {
            AssortmentMinorRevisionReplaceActionLocation item = new AssortmentMinorRevisionReplaceActionLocation();
            item.Create(locationId, code, name, units, facings);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create(Int16 locationId, String code, String name, Int32 units, Int32 facings)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int16?>(LocationIdProperty, locationId);
            this.LoadProperty<String>(LocationCodeProperty, code);
            this.LoadProperty<String>(LocationNameProperty, name);
            this.LoadProperty<Int32>(UnitsProperty, units);
            this.LoadProperty<Int32>(FacingsProperty, facings);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

    }
}
