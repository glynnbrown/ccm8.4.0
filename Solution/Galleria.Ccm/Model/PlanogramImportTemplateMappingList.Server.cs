﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25587 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public sealed partial class PlanogramImportTemplateMappingList
    {
        #region Constructor
        private PlanogramImportTemplateMappingList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        internal static PlanogramImportTemplateMappingList FetchByParentId(IDalContext dalContext, Object parentId)
        {
            return DataPortal.FetchChild<PlanogramImportTemplateMappingList>(dalContext, new FetchByParentIdCriteria(null, parentId));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FetchByParentIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            using (IPlanogramImportTemplateMappingDal dal = dalContext.GetDal<IPlanogramImportTemplateMappingDal>())
            {
                IEnumerable<PlanogramImportTemplateMappingDto> dtoList = dal.FetchByPlanogramImportTemplateId(criteria.ParentId);
                foreach (PlanogramImportTemplateMappingDto dto in dtoList)
                {
                    this.Add(PlanogramImportTemplateMapping.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}