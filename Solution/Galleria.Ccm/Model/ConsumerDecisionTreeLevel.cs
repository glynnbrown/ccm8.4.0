﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8025632 : A.Kuszyk
//		Created (copied from SA).
// V8-27132 : A.Kuszyk
//  Implemented common interface.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// ConsumerDecisionTreeLevel Model object
    /// </summary>
    [Serializable]
    public sealed partial class ConsumerDecisionTreeLevel : ModelObject<ConsumerDecisionTreeLevel>, IPlanogramConsumerDecisionTreeLevel
    {
        #region Parent

        /// <summary>
        /// Returns the parent level or null if this is the root
        /// </summary>
        public ConsumerDecisionTreeLevel ParentLevel
        {
            get
            {
                ConsumerDecisionTreeLevelList listParent = base.Parent as ConsumerDecisionTreeLevelList;
                if (listParent != null)
                {
                    return listParent.ParentLevel;
                }
                return null;
                //return base.Parent as ConsumerDecisionTreeLevel; 
            }
        }

        /// <summary>
        /// returns the parent consumer decision tree
        /// </summary>
        public ConsumerDecisionTree ParentConsumerDecisionTree
        {
            get
            {
                ConsumerDecisionTree parentCDT = base.Parent as ConsumerDecisionTree;
                if (parentCDT == null)
                {
                    ConsumerDecisionTreeLevel currentParent = this.ParentLevel;
                    while (currentParent != null)
                    {
                        if (currentParent.ParentLevel != null)
                        {
                            currentParent = currentParent.ParentLevel;
                        }
                        else
                        {
                            return currentParent.Parent as ConsumerDecisionTree;
                        }
                    }
                }
                else
                {
                    return parentCDT;
                }

                return null;
                //if (this.ParentLevel != null)
                //{
                //    return this.ParentLevel.ParentConsumerDecisionTree;
                //}
                //else
                //{
                //    return base.Parent as ConsumerDecisionTree;
                //}
            }
        }

        #endregion

        #region Constants
        private const Int16 _maxNamePropertyLenght = 100;
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
           RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The unique object id
        /// </summary>
        /// <remarks>Do not expose</remarks>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #region ChildLevel

        private static readonly ModelPropertyInfo<ConsumerDecisionTreeLevelList> ChildLevelListProperty =
            RegisterModelProperty<ConsumerDecisionTreeLevelList>(c => c.ChildLevelList);
        /// <summary>
        /// The Child level list: see GFS-13403
        /// </summary>
        private ConsumerDecisionTreeLevelList ChildLevelList
        {
            get
            {
                ConsumerDecisionTreeLevelList returnList = GetProperty<ConsumerDecisionTreeLevelList>(ChildLevelListProperty);
                if (returnList == null)
                {
                    returnList = ConsumerDecisionTreeLevelList.NewConsumerDecisionTreeLevelList();
                    LoadProperty<ConsumerDecisionTreeLevelList>(ChildLevelListProperty, returnList);
                }
                return returnList;
            }
        }
        public const String ChildLevelProperty = "ChildLevel";
        /// <summary>
        /// The child level of this, may be null
        /// </summary>
        public ConsumerDecisionTreeLevel ChildLevel
        {
            get { return this.ChildLevelList.FirstOrDefault(); }
            set
            {
                //remove the existing level
                ConsumerDecisionTreeLevel existingLevel = this.ChildLevelList.FirstOrDefault();
                if (existingLevel != null)
                {
                    this.ChildLevelList.Remove(existingLevel);
                }

                if (value != null)
                {
                    //add the new level
                    this.ChildLevelList.Add(value);
                }

                //fire the on property changed
                OnPropertyChanged(ChildLevelProperty);
            }
        }


        #endregion

        /// <summary>
        /// Returns true if this is the root level
        /// </summary>
        public Boolean IsRoot
        {
            get { return this.ParentLevel == null; }
        }

        /// <summary>
        /// Returns the number of this level, assuming that levels are numbered from top (root) to bottom.
        /// </summary>
        public Int32 LevelNumber
        {
            get
            {
                if (IsRoot)
                {
                    return 1;
                }
                return 1 + ParentLevel.LevelNumber;
            }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeLevel), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ConsumerDecisionTreeCreate.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeLevel), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ConsumerDecisionTreeGet.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeLevel), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ConsumerDecisionTreeEdit.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeLevel), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ConsumerDecisionTreeDelete.ToString()));
        }
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, _maxNamePropertyLenght));

        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static ConsumerDecisionTreeLevel NewConsumerDecisionTreeLevel()
        {
            ConsumerDecisionTreeLevel item = new ConsumerDecisionTreeLevel();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static ConsumerDecisionTreeLevel NewConsumerDecisionTreeLevel(String name)
        {
            ConsumerDecisionTreeLevel item = new ConsumerDecisionTreeLevel();
            item.Create(name);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(String name)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, name);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<ConsumerDecisionTreeLevel>(oldId, newId);
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Creates a dictionary of level id links, will create levels if required
        /// </summary>
        internal Dictionary<Int32, Int32> MergeFrom(ConsumerDecisionTreeLevel sourceLevel)
        {
            Dictionary<Int32, Int32> matchedLevelIds = new Dictionary<Int32, Int32>();

            //Add the level ids to the dictionary
            matchedLevelIds.Add(sourceLevel.Id, this.Id);
            String testChildLevelName = String.Empty;

            if (sourceLevel.ChildLevel != null)
            {
                ConsumerDecisionTreeLevel sourceChildLevel = sourceLevel.ChildLevel;
                if (this.ChildLevel == null)
                {
                    this.ChildLevel = ConsumerDecisionTreeLevel.NewConsumerDecisionTreeLevel();
                    testChildLevelName = String.Format("{0} - {1}", sourceLevel.ParentConsumerDecisionTree.Name, sourceChildLevel.Name);
                }
                else
                {
                    //if exists, and name already been changed to mixed level, use the name
                    if (this.ChildLevel.Name == Message.ConsumerDecisionTree_ChildNameTooLong_ReplacementName)
                    {
                        testChildLevelName = this.ChildLevel.Name;
                    }
                    else
                    {
                        testChildLevelName = String.Format("{0}\n{1} - {2}", this.ChildLevel.Name, sourceLevel.ParentConsumerDecisionTree.Name, sourceChildLevel.Name);
                    }
                }

                //validate child level name lenght
                if (testChildLevelName.Length > _maxNamePropertyLenght)
                {
                    testChildLevelName = Message.ConsumerDecisionTree_ChildNameTooLong_ReplacementName;
                }
                this.ChildLevel.Name = testChildLevelName;
                matchedLevelIds = matchedLevelIds.Union(this.ChildLevel.MergeFrom(sourceChildLevel)).ToDictionary(p => p.Key, p => p.Value);
            }
            return matchedLevelIds;
        }

        public void RemoveLevel(ConsumerDecisionTreeLevel level)
        {
            ChildLevelList.Remove(level);
        }

        #endregion

        #region IPlanogramConsumerDecisionTreeLevel members
        IPlanogramConsumerDecisionTreeLevel IPlanogramConsumerDecisionTreeLevel.ChildLevel
        {
            get { return ChildLevel; }
        }
        #endregion
    }
}