﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
#endregion
#endregion

using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using System;
using Csla;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// List containing ViewLayoutInfo
    /// </summary>
    [Serializable]
    public sealed partial class ViewLayoutInfoList : ModelReadOnlyList<ViewLayoutInfoList, ViewLayoutInfo>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ViewLayoutInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(ViewLayoutInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(ViewLayoutInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(ViewLayoutInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Temporary.ToString()));
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchAll
        /// </summary>
        [Serializable]
        public sealed class FetchAllCriteria : CriteriaBase<FetchAllCriteria>
        {
            public FetchAllCriteria()
            {
            }
        }


        #endregion

    }
}
