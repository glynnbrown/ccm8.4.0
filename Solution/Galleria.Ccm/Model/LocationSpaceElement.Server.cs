﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class LocationSpaceElement
    {
        #region Constructor
        private LocationSpaceElement() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new location space Element obect from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new location space Element object</returns>
        internal static LocationSpaceElement GetLocationSpaceElement(IDalContext dalContext, LocationSpaceElementDto dto)
        {
            return DataPortal.FetchChild<LocationSpaceElement>(dalContext, dto);
        }

        #endregion

        #region Data Access
        #region Data Transfer Object

        /// <summary>
        /// Creates a new dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private LocationSpaceElementDto GetDataTransferObject(LocationSpaceBay parent)
        {
            return new LocationSpaceElementDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                Order = ReadProperty<Byte>(OrderProperty),
                X = ReadProperty<Single>(XProperty),
                Y = ReadProperty<Single>(YProperty),
                Z = ReadProperty<Single>(ZProperty),
                MerchandisableHeight = ReadProperty<Single>(MerchandisableHeightProperty),
                Height = ReadProperty<Single>(HeightProperty),
                Width = ReadProperty<Single>(WidthProperty),
                Depth = ReadProperty<Single>(DepthProperty),
                FaceThickness = ReadProperty<Single>(FaceThicknessProperty),
                NotchNumber = ReadProperty<Nullable<Single>>(NotchNumberProperty),
                LocationSpaceBayId = parent.Id
            };

        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, LocationSpaceElementDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Byte>(OrderProperty, dto.Order);
            LoadProperty<Single>(XProperty, dto.X);
            LoadProperty<Single>(YProperty, dto.Y);
            LoadProperty<Single>(ZProperty, dto.Z);
            LoadProperty<Single>(MerchandisableHeightProperty, dto.MerchandisableHeight);
            LoadProperty<Single>(HeightProperty, dto.Height);
            LoadProperty<Single>(WidthProperty, dto.Width);
            LoadProperty<Single>(DepthProperty, dto.Depth);
            LoadProperty<Single>(FaceThicknessProperty, dto.FaceThickness);
            LoadProperty<Nullable<Single>>(NotchNumberProperty, dto.NotchNumber);
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Called when returning an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, LocationSpaceElementDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting a child
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, LocationSpaceBay parent)
        {
            LocationSpaceElementDto dto = GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (ILocationSpaceElementDal dal = dalContext.GetDal<ILocationSpaceElementDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<LocationSpaceElement>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an existing child
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, LocationSpaceBay parent)
        {
            if (this.IsSelfDirty)
            {
                using (ILocationSpaceElementDal dal = dalContext.GetDal<ILocationSpaceElementDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this instance is deleting itself
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (ILocationSpaceElementDal dal = dalContext.GetDal<ILocationSpaceElementDal>())
            {
                dal.DeleteById(this.Id);
            }
        }

        #endregion

        #endregion
    }
}
