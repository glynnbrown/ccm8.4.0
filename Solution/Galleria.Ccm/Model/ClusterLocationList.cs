﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Model;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class ClusterLocationList : ModelList<ClusterLocationList, ClusterLocation>
    {
        #region Authorisation Rules
        /// <summary>
        /// Defines the authorisation rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ClusterLocationList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ClusterSchemeCreate.ToString()));
            BusinessRules.AddRule(typeof(ClusterLocationList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ClusterSchemeGet.ToString()));
            BusinessRules.AddRule(typeof(ClusterLocationList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ClusterSchemeEdit.ToString()));
            BusinessRules.AddRule(typeof(ClusterLocationList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ClusterSchemeDelete.ToString()));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds the given location as a new cluster location
        /// </summary>
        /// <param name="addStore"></param>
        /// <returns></returns>
        public ClusterLocation Add(LocationInfo addLocation)
        {
            ClusterLocation newClusterLocation = ClusterLocation.NewClusterLocation(addLocation);
            this.Add(newClusterLocation);
            return newClusterLocation;
        }

        /// <summary>
        /// Removes the cluster location relative to the given location
        /// </summary>
        /// <param name="removeStore"></param>
        public void Remove(Location removeLocation)
        {
            Int32 locationId = removeLocation.Id;

            ClusterLocation removeClusterLocation =
                this.FirstOrDefault(p => p.LocationId == locationId);

            if (removeClusterLocation != null)
            {
                this.Remove(removeClusterLocation);
            }
        }

        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new cluster location list
        /// </summary>
        /// <returns>A new cluster location list</returns>
        internal static ClusterLocationList NewClusterLocationList()
        {
            ClusterLocationList item = new ClusterLocationList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private new void Create()
        {
            AllowNew = true;
            MarkAsChild();
        }
        #endregion

        #endregion

    }
}
