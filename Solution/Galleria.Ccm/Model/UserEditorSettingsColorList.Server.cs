﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30791 : D.Pleasance
//	Created
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;
using Csla.Data;

using Galleria.Framework.Dal;

using Galleria.Ccm.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// List of user colors
    /// </summary>
    public partial class UserEditorSettingsColorList
    {
        #region Constructor
        private UserEditorSettingsColorList() { } //Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all user colors
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <returns>A list of user colors</returns>
        internal static UserEditorSettingsColorList GetUserEditorSettingsColors(IDalContext dalContext)
        {
            return DataPortal.FetchChild<UserEditorSettingsColorList>(dalContext);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext)
        {
            RaiseListChangedEvents = false;
            using (IUserEditorSettingColorDal dal = dalContext.GetDal<IUserEditorSettingColorDal>())
            {
                foreach (UserEditorSettingColorDto dto in dal.FetchAll())
                {
                    this.Add(UserEditorSettingsColor.GetUserEditorSettingsColor(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}