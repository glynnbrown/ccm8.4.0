﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8025632 : A.Kuszyk
//		Created (copied from SA).
#endregion
#region Version History: (CCM 8.01)
// V8-28866 : A.Probyn
//  Updated Remove to use base. not this.items.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a list containing ConsumerDecisionTreeNodeProduct objects
    /// (Child of ConsumerDecisionTreeNode)
    /// </summary>
    [Serializable]
    public sealed partial class ConsumerDecisionTreeNodeProductList : ModelList<ConsumerDecisionTreeNodeProductList, ConsumerDecisionTreeNodeProduct>
    {
        #region Parent

        public ConsumerDecisionTreeNode ParentConsumerDecisionTreeNode
        {
            get { return base.Parent as ConsumerDecisionTreeNode; }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeNodeProductList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ConsumerDecisionTreeCreate.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeNodeProductList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ConsumerDecisionTreeGet.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeNodeProductList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ConsumerDecisionTreeEdit.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeNodeProductList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ConsumerDecisionTreeDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        public static ConsumerDecisionTreeNodeProductList NewConsumerDecisionTreeNodeProductList()
        {
            ConsumerDecisionTreeNodeProductList item = new ConsumerDecisionTreeNodeProductList();
            item.Create();
            return item;
        }

        /// <summary>
        /// Called when creating a new list from a list of product Ids
        /// </summary>
        /// <returns>A newlist</returns>
        public static ConsumerDecisionTreeNodeProductList NewConsumerDecisionTreeNodeProductList(IEnumerable<Int32> productIdList)
        {
            ConsumerDecisionTreeNodeProductList item = new ConsumerDecisionTreeNodeProductList();
            item.Create(productIdList);
            return item;
        }

        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        public static ConsumerDecisionTreeNodeProductList NewConsumerDecisionTreeNodeProductList(
            ConsumerDecisionTreeNodeProductList products, ConsumerDecisionTreeNode rootNode)
        {
            ConsumerDecisionTreeNodeProductList item = new ConsumerDecisionTreeNodeProductList();
            item.Create(products, rootNode);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create()
        {
            MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(IEnumerable<Int32> productIdList)
        {
            foreach (Int32 productId in productIdList)
            {
                this.Add(ConsumerDecisionTreeNodeProduct.NewConsumerDecisionTreeNodeProduct(productId));
            }

            MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(ConsumerDecisionTreeNodeProductList products, ConsumerDecisionTreeNode rootNode)
        {
            //get the products assigned to the root node as this is where the child products were moved
            IEnumerable<ConsumerDecisionTreeNodeProduct> rootProducts = rootNode.Products.ToList();

            foreach (ConsumerDecisionTreeNodeProduct product in products)
            {
                ConsumerDecisionTreeNodeProduct rootProductFound = rootProducts.FirstOrDefault(p => p.ProductId == product.ProductId);

                if (rootProductFound != null)
                {
                    this.Add(rootProductFound);
                    rootNode.Products.Remove(rootProductFound);
                }
                else
                {
                    this.Add(ConsumerDecisionTreeNodeProduct.NewConsumerDecisionTreeNodeProduct(product.ProductId));
                }
            }
            MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Overrides

        protected override void OnBulkCollectionChanged(BulkCollectionChangedEventArgs e)
        {
            base.OnBulkCollectionChanged(e);

            if (this.ParentConsumerDecisionTreeNode != null)
            {
                this.ParentConsumerDecisionTreeNode.RaiseTotalProductCountChanged();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds a new ConsumerDecisionTreeNodeProduct to this collection for the given productId
        /// </summary>
        /// <param name="productId">The Product Id for the new item</param>
        public ConsumerDecisionTreeNodeProduct Add(Int32 productId)
        {
            ConsumerDecisionTreeNodeProduct addedItem = ConsumerDecisionTreeNodeProduct.NewConsumerDecisionTreeNodeProduct(productId);
            this.Add(addedItem);
            return addedItem;
        }

        /// <summary>
        /// Adds a collection of products to this collection
        /// </summary>
        /// <param name="productIds">The Product Ids for the new items</param>
        public void AddRange(IEnumerable<Int32> productIds)
        {
            this.RaiseListChangedEvents = false;

            //cycle through adding all the items
            List<ConsumerDecisionTreeNodeProduct> addedItems = new List<ConsumerDecisionTreeNodeProduct>();
            foreach (Int32 productId in productIds)
            {
                addedItems.Add(this.Add(productId));
            }

            this.RaiseListChangedEvents = true;

            //raise out a bulk change notification
            NotifyBulkChange(new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, addedItems));
        }

        /// <summary>
        /// Removes the node product linked to the given product
        /// </summary>
        /// <param name="scenLocation"></param>
        public void Remove(Product product)
        {
            ConsumerDecisionTreeNodeProduct foundProd =
                this.Items.FirstOrDefault(l => product.Id.Equals(l.ProductId));

            if (foundProd != null)
            {
                base.Remove(foundProd);
            }
        }

        #endregion
    }
}
