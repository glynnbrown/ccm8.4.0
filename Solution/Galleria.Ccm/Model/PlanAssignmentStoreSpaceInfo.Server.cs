#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25879 : Martin Shelley
//		Created (Auto-generated)
#endregion

#region Version History: (CCM 8.1.0)
// V8-29598 : L.Luong
//	Added Height
#endregion

#region Version History: CCM840

// CCM-13408 : J.Mendes
//  Added LocationSpaceProductGroup fields to the Planogram Assignment

#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class PlanAssignmentStoreSpaceInfo
    {
        #region Constructors
        private PlanAssignmentStoreSpaceInfo() { } //force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing PlanAssignmentStoreSpaceInfo with the given id
        /// </summary>
        public static PlanAssignmentStoreSpaceInfo GetPlanAssignmentStoreSpaceInfo(IDalContext dalContext, PlanAssignmentStoreSpaceInfoDto dto)
        {
            return DataPortal.FetchChild<PlanAssignmentStoreSpaceInfo>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanAssignmentStoreSpaceInfoDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int16>(LocationIdProperty, dto.LocationId);
            this.LoadProperty<String>(LocationCodeProperty, dto.LocationCode);
            this.LoadProperty<String>(LocationNameProperty, dto.LocationName);
            this.LoadProperty<String>(LocationAddressProperty, dto.LocationAddress);
            this.LoadProperty<Int32>(ProductGroupIdProperty, dto.ProductGroupId);
            this.LoadProperty<String>(ProductGroupCodeProperty, dto.ProductGroupCode);
            this.LoadProperty<String>(ProductGroupNameProperty, dto.ProductGroupName);
            this.LoadProperty<String>(BayDimensionsProperty, dto.BayDimensions);
            this.LoadProperty<Double>(BayTotalWidthProperty, dto.BayTotalWidth);
            this.LoadProperty<Double>(BayHeightProperty, dto.BayHeight);
            this.LoadProperty<Int32>(BayCountProperty, dto.BayCount);
            this.LoadProperty<String>(ClusterSchemeNameProperty, dto.ClusterSchemeName);
            this.LoadProperty<String>(ClusterNameProperty, dto.ClusterName);
            this.LoadProperty<Int32>(ProductCountProperty, dto.ProductCount);
            this.LoadProperty<Double>(AverageBayWidthProperty, dto.AverageBayWidth);
            this.LoadProperty<String>(AisleNameProperty, dto.AisleName);
            this.LoadProperty<String>(ValleyNameProperty, dto.ValleyName);
            this.LoadProperty<String>(ZoneNameProperty, dto.ZoneName);
            this.LoadProperty<String>(CustomAttribute01Property, dto.CustomAttribute01);
            this.LoadProperty<String>(CustomAttribute02Property, dto.CustomAttribute02);
            this.LoadProperty<String>(CustomAttribute03Property, dto.CustomAttribute03);
            this.LoadProperty<String>(CustomAttribute04Property, dto.CustomAttribute04);
            this.LoadProperty<String>(CustomAttribute05Property, dto.CustomAttribute05);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning FetchById
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, PlanAssignmentStoreSpaceInfoDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #endregion
    }
}