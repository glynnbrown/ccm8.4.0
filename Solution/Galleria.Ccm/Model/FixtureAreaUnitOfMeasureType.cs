﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Denotes the available area unit of measure types
    /// </summary>
    public enum FixtureAreaUnitOfMeasureType
    {
        Unknown = 0,
        SquareCentimeters = 1,
        SquareInches = 2
    }

    /// <summary>
    /// FixtureAreaUnitOfMeasureType Helper Class
    /// </summary>
    public static class FixtureAreaUnitOfMeasureTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<FixtureAreaUnitOfMeasureType, String> FriendlyNames =
            new Dictionary<FixtureAreaUnitOfMeasureType, String>()
            {
                {FixtureAreaUnitOfMeasureType.Unknown, String.Empty},
                {FixtureAreaUnitOfMeasureType.SquareCentimeters, Message.Enum_FixtureAreaUnitOfMeasureType_SquareCentimeters},
                {FixtureAreaUnitOfMeasureType.SquareInches, Message.Enum_FixtureAreaUnitOfMeasureType_SquareInches},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<FixtureAreaUnitOfMeasureType, String> Abbreviations =
            new Dictionary<FixtureAreaUnitOfMeasureType, String>()
            {
                {FixtureAreaUnitOfMeasureType.Unknown, String.Empty},
                {FixtureAreaUnitOfMeasureType.SquareCentimeters, Message.Enum_FixtureAreaUnitOfMeasureType_SquareCentimeters_Abbrev},
                {FixtureAreaUnitOfMeasureType.SquareInches, Message.Enum_FixtureAreaUnitOfMeasureType_SquareInches_Abbrev},
            };

        public static FixtureAreaUnitOfMeasureType Parse(String value)
        {
            return EnumHelper.Parse<FixtureAreaUnitOfMeasureType>(value, FixtureAreaUnitOfMeasureType.Unknown);
        }
    }
}
