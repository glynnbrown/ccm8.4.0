﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// V8-24265 : J.Pickup
//  Created
#endregion
#region Version History: (CCM 8.03)
// V8-28662 : L.Ineson
//  Added ToPlanogramLabelVerticalAlignment
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Model
{
    public enum LabelVerticalPlacementType
    {
        Top = 0,
        Center = 1,
        Bottom = 2,
    }

    public static class LabelVerticalPlacementTypeHelper
    {
        public static readonly Dictionary<LabelVerticalPlacementType, String> FriendlyNames =
            new Dictionary<LabelVerticalPlacementType, String>()
            {
                {LabelVerticalPlacementType.Top, Message.Enum_LabelVerticalPlacementType_Top}, 
                {LabelVerticalPlacementType.Center, Message.Enum_LabelVerticalPlacementType_Center},
                {LabelVerticalPlacementType.Bottom, Message.Enum_LabelVerticalPlacementType_Bottom},
            };

        public static PlanogramLabelVerticalAlignment ToPlanogramLabelVerticalAlignment(this LabelVerticalPlacementType value)
        {
            switch (value)
            {
                default:
                case LabelVerticalPlacementType.Center: return PlanogramLabelVerticalAlignment.Center;
                case LabelVerticalPlacementType.Bottom: return PlanogramLabelVerticalAlignment.Bottom;
                case LabelVerticalPlacementType.Top: return PlanogramLabelVerticalAlignment.Top;
            }
        }
    }
}
