﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Engine;

namespace Galleria.Ccm.Model
{
    public partial class EngineTaskInfoList
    {
        #region Constructors
        private EngineTaskInfoList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all registers engine tasks
        /// </summary>
        public static EngineTaskInfoList FetchAll()
        {
            return DataPortal.Fetch<EngineTaskInfoList>();
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning all registered engine tasks
        /// </summary>
        private void DataPortal_Fetch()
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            foreach (String taskType in TaskContainer.RegisteredTaskTypes)
            {
                this.Add(EngineTaskInfo.GetEngineTaskInfo(taskType));
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
