﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
// V8-24779 : D.Pleasance
//  Added implementation to create from plan file
#endregion
#region Version History: CCM802
// V8-29026 : D.Pleasance
//  Added new Add implementation to include planogramName
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class WorkpackagePlanogramList : ModelList<WorkpackagePlanogramList, WorkpackagePlanogram>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent workpackage
        /// </summary>
        public new Workpackage Parent
        {
            get { return (Workpackage)base.Parent; }
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(WorkpackagePlanogramList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.WorkpackageCreate.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.WorkpackageEdit.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.WorkpackageDelete.ToString()));
        }
        #endregion

        #region Criteria

        #region FetchByWorkpackgeIdCriteria
        /// <summary>
        /// Criteria for FetchByUniqueContentReference
        /// </summary>
        [Serializable]
        public class FetchByWorkpackgeIdCriteria : CriteriaBase<FetchByWorkpackgeIdCriteria>
        {
            #region Properties

            #region WorkpackageId
            /// <summary>
            /// WorkpackageId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> WorkpackageIdProperty =
                RegisterProperty<Int32>(c => c.WorkpackageId);
            /// <summary>
            /// Returns the workpackage id
            /// </summary>
            public Int32 WorkpackageId
            {
                get { return this.ReadProperty<Int32>(WorkpackageIdProperty); }
            }
            #endregion

            #region WorkflowId
            /// <summary>
            /// Workflow id property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> WorkflowIdProperty =
                RegisterProperty<Int32>(c => c.WorkflowId);
            /// <summary>
            /// Returns the workflow id
            /// </summary>
            public Int32 WorkflowId
            {
                get { return this.ReadProperty<Int32>(WorkflowIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByWorkpackgeIdCriteria(Int32 workpackageId, Int32 workflowId)
            {
                this.LoadProperty<Int32>(WorkpackageIdProperty, workpackageId);
                this.LoadProperty<Int32>(WorkflowIdProperty, workflowId);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        internal static WorkpackagePlanogramList NewWorkpackagePlanogramList()
        {
            WorkpackagePlanogramList item = new WorkpackagePlanogramList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Creates a new workpackage planogram
        /// based on the specified planogram
        /// </summary>
        public WorkpackagePlanogram Add(PlanogramInfo planogram)
        {
            WorkpackagePlanogram item = WorkpackagePlanogram.NewWorkpackagePlanogram(planogram);
            this.Add(item);
            return item;
        }

        /// <summary>
        /// Creates a new workpackage planogram
        /// based on the specified planogram
        /// </summary>
        public WorkpackagePlanogram Add(PlanogramInfo planogram, String planogramName)
        {
            WorkpackagePlanogram item = WorkpackagePlanogram.NewWorkpackagePlanogram(planogram, planogramName);
            this.Add(item);
            return item;
        }

        /// <summary>
        /// Creates a new workpackage planogram
        /// based on the specified plan file
        /// </summary>
        public WorkpackagePlanogram Add(String planFile)
        {
            WorkpackagePlanogram item = WorkpackagePlanogram.NewWorkpackagePlanogram(planFile);
            this.Add(item);
            return item;
        }

        /// <summary>
        /// Adds a set of planograms to this list
        /// </summary>
        public void AddList(IEnumerable<PlanogramInfo> planograms)
        {
            List<WorkpackagePlanogram> items = new List<WorkpackagePlanogram>();
            foreach (PlanogramInfo planogram in planograms)
            {
                items.Add(WorkpackagePlanogram.NewWorkpackagePlanogram(planogram));
            }
            this.AddList(items);
        }

        /// <summary>
        /// Resolves a planogram id change
        /// </summary>
        public void ResolveId(Int32 oldId, Int32 newId)
        {
            if (oldId != newId)
            {
                foreach (var planogram in this)
                {
                    if (planogram.DestinationPlanogramId == oldId) planogram.DestinationPlanogramId = newId;
                }
            }
        }
        #endregion
    }
}
