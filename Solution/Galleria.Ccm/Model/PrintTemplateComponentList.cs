﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of PrintTemplateComponent objects.
    /// </summary>
    [Serializable]
    public sealed partial class PrintTemplateComponentList : ModelList<PrintTemplateComponentList, PrintTemplateComponent>
    {
        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PrintTemplateComponentList NewPrintTemplateComponentList()
        {
            PrintTemplateComponentList item = new PrintTemplateComponentList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Adds a new component to this section.
        /// </summary>
        public PrintTemplateComponent AddNewComponent(PrintTemplateComponentType componentType)
        {
            PrintTemplateComponent item = PrintTemplateComponent.NewPrintTemplateComponent(componentType);
            this.Add(item);
            return item;
        }

        #endregion
    }

}