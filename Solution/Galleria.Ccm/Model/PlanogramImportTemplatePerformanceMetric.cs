﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.1)
//V8-28622 : D.Pleasance
//  Created
#endregion
#region Version History: (CCM 8.0.3)
// V8-29682 : A.Probyn
//  Added AggregationType
#endregion
#region Version History: (CCM 8.0.3)
// V8-30215 : M.Brumby
//  Changed AggregationType to use PlanogramPerformanceMetricAggregationType for consitancey
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a PlanogramImportTemplatePerformanceMetric object.
    /// </summary>
    [Serializable]
    [DefaultNewMethod("NewPlanogramImportTemplatePerformanceMetric")]
    public sealed partial class PlanogramImportTemplatePerformanceMetric : ModelObject<PlanogramImportTemplatePerformanceMetric>
    {
        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramImportTemplate Parent
        {
            get
            {
                PlanogramImportTemplateMappingList parentList = base.Parent as PlanogramImportTemplateMappingList;
                if (parentList != null)
                {
                    return parentList.Parent as PlanogramImportTemplate;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique identifier for this item.
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name, Message.PlanogramImportTemplatePerformanceMetric_Name);
        /// <summary>
        /// Name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region Description
        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description, Message.PlanogramImportTemplatePerformanceMetric_Description);
        /// <summary>
        /// Description
        /// </summary>
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
            set { SetProperty<String>(DescriptionProperty, value); }
        }
        #endregion

        #region Direction
        /// <summary>
        /// Direction property definition
        /// </summary>
        public static readonly ModelPropertyInfo<MetricDirectionType> DirectionProperty =
            RegisterModelProperty<MetricDirectionType>(c => c.Direction, Message.PlanogramImportTemplatePerformanceMetric_Direction);
        /// <summary>
        /// Direction
        /// </summary>
        public MetricDirectionType Direction
        {
            get { return GetProperty(DirectionProperty); }
            set { SetProperty(DirectionProperty, (MetricDirectionType)value); }
        }
        #endregion

        #region SpecialType
        /// <summary>
        /// SpecialType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<MetricSpecialType> SpecialTypeProperty =
            RegisterModelProperty<MetricSpecialType>(c => c.SpecialType, Message.PlanogramImportTemplatePerformanceMetric_SpecialType);
        /// <summary>
        /// SpecialType
        /// </summary>
        public MetricSpecialType SpecialType
        {
            get { return GetProperty(SpecialTypeProperty); }
            set
            {
                MetricSpecialType currentValue = this.ReadProperty<MetricSpecialType>(SpecialTypeProperty);
                if (value != currentValue)
                {
                    SetProperty(SpecialTypeProperty, (MetricSpecialType)value);
                    NotifyObjectGraph(SpecialTypeProperty);
                }
            }
        }
        #endregion

        #region MetricType
        /// <summary>
        /// MetricType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<MetricType> MetricTypeProperty =
            RegisterModelProperty<MetricType>(c => c.MetricType, Message.PlanogramImportTemplatePerformanceMetric_MetricType);
        /// <summary>
        /// MetricType
        /// </summary>
        public MetricType MetricType
        {
            get { return GetProperty(MetricTypeProperty); }
            set { SetProperty(MetricTypeProperty, (MetricType)value); }
        }
        #endregion

        #region MetricId
        /// <summary>
        /// MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> MetricIdProperty =
            RegisterModelProperty<Byte>(c => c.MetricId, Message.PlanogramImportTemplatePerformanceMetric_MetricId);
        /// <summary>
        /// MetricId
        /// </summary>
        public Byte MetricId
        {
            get { return GetProperty<Byte>(MetricIdProperty); }
            set { SetProperty<Byte>(MetricIdProperty, value); }
        }
        #endregion

        #region ExternalField
        /// <summary>
        /// External Field property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ExternalFieldProperty =
            RegisterModelProperty<String>(c => c.ExternalField, Message.PlanogramImportTemplatePerformanceMetric_ExternalField);
        /// <summary>
        /// Gets/Sets the external file field
        /// </summary>
        public String ExternalField
        {
            get { return this.GetProperty<String>(ExternalFieldProperty); }
            set { this.SetProperty<String>(ExternalFieldProperty, value); }
        }
        #endregion

        #region AggregationType

        /// <summary>
        /// External Field property definition
        /// </summary>
        public static readonly ModelPropertyInfo<AggregationType> AggregationTypeProperty =
            RegisterModelProperty<AggregationType>(c => c.AggregationType, Message.PlanogramImportTemplatePerformanceMetric_AggregationType);
        /// <summary>
        /// Gets/Sets the Aggregation Type
        /// </summary>
        public AggregationType AggregationType
        {
            get { return this.GetProperty<AggregationType>(AggregationTypeProperty); }
            set { this.SetProperty<AggregationType>(AggregationTypeProperty, value); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new Required(MetricIdProperty));
            BusinessRules.AddRule(new Required(ExternalFieldProperty));
            BusinessRules.AddRule(new MinValue<Byte>(MetricIdProperty, 1));
            BusinessRules.AddRule(new MaxValue<Byte>(MetricIdProperty, Framework.Planograms.Constants.MaximumStandardGlobalMetricsPerPerformanceSource));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));
            BusinessRules.AddRule(new MaxLength(DescriptionProperty, 255));
            BusinessRules.AddRule(new MaxLength(ExternalFieldProperty, 1000));
            base.AddBusinessRules();
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramImportTemplatePerformanceMetric NewPlanogramImportTemplatePerformanceMetric()
        {
            var item = new PlanogramImportTemplatePerformanceMetric();
            item.Create(0);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramImportTemplatePerformanceMetric NewPlanogramImportTemplatePerformanceMetric(Byte metricId)
        {
            var item = new PlanogramImportTemplatePerformanceMetric();
            item.Create(metricId);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramImportTemplatePerformanceMetric NewPlanogramImportTemplatePerformanceMetric(String name, String description, String externalField, Byte metricId)
        {
            var item = new PlanogramImportTemplatePerformanceMetric();
            item.Create(name, description, externalField, metricId);
            return item;
        }
        #endregion

        #region Data Access

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Byte metricId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Byte>(MetricIdProperty, metricId);
            this.LoadProperty<AggregationType>(AggregationTypeProperty, AggregationType.Sum);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(String name, String description, String externalField, Byte metricId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, name);
            this.LoadProperty<String>(DescriptionProperty, description);
            this.LoadProperty<String>(ExternalFieldProperty, externalField);
            this.LoadProperty<Byte>(MetricIdProperty, metricId);
            this.LoadProperty<AggregationType>(AggregationTypeProperty, AggregationType.Sum);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<PlanogramImportTemplatePerformanceMetric>(oldId, newId);
        }

        /// <summary>
        /// Copies the values from the given metric
        /// </summary>
        public void CopyValues(PlanogramImportTemplatePerformanceMetric metric)
        {
            this.Name = metric.Name;
            this.Description = metric.Description;
            this.Direction = metric.Direction;
            this.SpecialType = metric.SpecialType;
            this.MetricType = metric.MetricType;
            this.MetricId = metric.MetricId;
            this.AggregationType = metric.AggregationType;
        }

        #endregion
    }

    
}
