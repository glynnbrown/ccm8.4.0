﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    public sealed partial class EntityComparisonAttribute 
    {
        #region Constructor

        /// <summary>
        ///     Private constructor to enforce use of factory methods.
        /// </summary>
        public EntityComparisonAttribute()
        {
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Called by reflection when fetching an item from a <paramref name="dto" />.
        /// </summary>
        internal static EntityComparisonAttribute Fetch(IDalContext dalContext, EntityComparisonAttributeDto dto)
        {
            return DataPortal.FetchChild<EntityComparisonAttribute>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Load the data from the given <paramref name="dto" /> into this instance.
        /// </summary>
        /// <param name="dalContext">[<c>not used</c>]</param>
        /// <param name="dto">The <see cref="EntityComparisonAttributeDto" /> containing the values to load into this instance.</param>
        private void LoadDataTransferObject(IDalContext dalContext, EntityComparisonAttributeDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(PropertyNameProperty, dto.PropertyName);
            LoadProperty(PropertyDisplayNameProperty, dto.PropertydisplayName);
            LoadProperty(ItemTypeProperty, dto.ItemType);

        }

        /// <summary>
        ///     Create a data transfer object from the data in this instance.
        /// </summary>
        /// <param name="parent">Planogram Comparison that contains this instance.</param>
        /// <returns>A new instance of <see cref="EntityComparisonAttributeDto" /> with the data in this instance.</returns>
        private EntityComparisonAttributeDto GetDataTransferObject(Entity parent)
        {
            return new EntityComparisonAttributeDto
            {
                Id = ReadProperty(IdProperty),
                EntityId = parent.Id,
                ItemType = (Byte)ReadProperty(ItemTypeProperty),
                PropertyName = ReadProperty(PropertyNameProperty),
                PropertydisplayName = ReadProperty(PropertyDisplayNameProperty),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Called by reflection when fetching an instance of <see cref="EntityComparisonAttribute" />.
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, EntityComparisonAttributeDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Called by reflection when inserting an instance of <see cref="EntityComparisonAttribute" />.
        /// </summary>
        private void Child_Insert(IDalContext dalContext, Entity parent)
        {
            EntityComparisonAttributeDto dto = GetDataTransferObject(parent);
            var oldId = (Int32)dto.Id;
            using (var dal = dalContext.GetDal<IEntityComparisonAttributeDal>())
            {
                dal.Insert(dto);
            }
            LoadProperty(IdProperty, (Int32)dto.Id);
            dalContext.RegisterId<EntityComparisonAttribute>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Update

        /// <summary>
        ///     Called by reflection when updating an instance of <see cref="PlanogramComparisonTemplateField" />.
        /// </summary>
        private void Child_Update(IDalContext dalContext, Entity parent)
        {
            if (IsSelfDirty)
            {
                using (var dal = dalContext.GetDal<IEntityComparisonAttributeDal>())
                {
                    dal.Update(GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Called by reflection when deleting an instance of <see cref="PlanogramComparisonTemplate" />.
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, Entity parent)
        {
            using (var dal = dalContext.GetDal<IEntityComparisonAttributeDal>())
            {
                dal.DeleteById(Id);
            }
        }

        

        #endregion

        #endregion
    }
}

