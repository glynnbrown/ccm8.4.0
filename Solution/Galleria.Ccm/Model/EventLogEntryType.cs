﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : Luong
//   Created (Copied From GFS)
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Event Log Entry Type Helper Class
    /// </summary>
    public static class EventLogEntryTypeHelper
    {
        public static readonly Dictionary<EventLogEntryType, String> FriendlyNames =
            new Dictionary<EventLogEntryType, String>()
            {
                {EventLogEntryType.Error, Message.Enum_EventLogEntryType_Error},
                {EventLogEntryType.Information, Message.Enum_EventLogEntryType_Information},
                {EventLogEntryType.Warning, Message.Enum_EventLogEntryType_Warning},
                {EventLogEntryType.SuccessAudit, Message.Enum_EventLogEntryType_SuccessAudit},
                {EventLogEntryType.FailureAudit, Message.Enum_EventLogEntryType_FailureAudit},
            };

        //public static readonly Dictionary<EventLogEntryType, String> FriendlyDescriptions =
        //    new Dictionary<EventLogEntryType, String>()
        //    {
        //        {EventLogEntryType.Error, "TODO"/*Message.Enum_EventLogLevel_Error_Description*/},
        //        {EventLogEntryType.Information, "TODO"/*Message.Enum_EventLogLevel_Information_Description*/},
        //        {EventLogEntryType.Warning, "TODO"/*Message.Enum_EventLogLevel_Warning_Description*/},
        //        {EventLogEntryType.SuccessAudit, "TODO"/*Message.Enum_EventLogLevel_AuditSuccess_Description*/},
        //        {EventLogEntryType.FailureAudit, "TODO"/*Message.Enum_EventLogLevel_Failure_Description*/},
        //    };

        public static EventLogEntryType? EventLogLevelGetEnum(String description)
        {
            foreach (KeyValuePair<EventLogEntryType, String> keyPair in EventLogEntryTypeHelper.FriendlyNames)
            {
                if (keyPair.Value.Equals(description))
                {
                    return keyPair.Key;
                }
            }
            return null;
        }

        public static String EventLogLevelGetDescription(EventLogEntryType EventLogEventEnum)
        {
            foreach (KeyValuePair<EventLogEntryType, String> keyPair in EventLogEntryTypeHelper.FriendlyNames)
            {
                if (keyPair.Key.Equals(EventLogEventEnum))
                {
                    return keyPair.Value;
                }
            }
            return null;
        }
    }
}
