﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-V8-24261 : L.Hodson
//	Created (Auto-generated)
// V8-27957 : N.Foster
//  Implement CCM security
#endregion

#region Version History: CCM 802
// V8-27433 : A.Kuszyk
//  Added Path property.
#endregion

#region Version History : CCM 811
// V8-30590 : A.Kuszyk
//  Added FriendlyPath property.
#endregion

#region Version History : CCM 820
// V8-31140 : L.Ineson
//  Added more detail to repository path.
#endregion

#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using System.Diagnostics;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// RecentPlanogram Model object
    /// </summary>
    [Serializable]
    public sealed partial class RecentPlanogram : ModelObject<RecentPlanogram>
    {
        #region Constants
        public static readonly Char LocationSeparator = '|';
        #endregion

        #region Static Constructor
        static RecentPlanogram()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Object> IdProperty =
            RegisterModelProperty<Object>(c => c.Id);
        /// <summary>
        /// Returns the unique highlight id
        /// For a filestystem this is the filepath.
        /// </summary>
        public Object Id
        {
            get { return this.GetProperty<Object>(IdProperty); }
            set { SetProperty<Object>(IdProperty, value); }
        }
        #endregion

        #region Path
        /// <summary>
        /// Path property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> PathProperty =
            RegisterModelProperty<String>(c => c.Path);
        /// <summary>
        /// Represents the location path (file path or plan hierarchy path) of a planogram.
        /// </summary>
        public String Path
        {
            get { return this.GetProperty<String>(PathProperty); }
            set { SetProperty<String>(PathProperty, value); }
        }
        #endregion

        #region FriendlyPath
        /// <summary>
        /// Gets the parent path of the Path property for use in the UI.
        /// </summary>
        public String FriendlyPath
        {
            get
            {
                switch(Type)
                {
                    case RecentPlanogramType.Pog:
                        return System.IO.Path.GetDirectoryName(Path);
                    case RecentPlanogramType.Repository:
                        return Path;
                    default:
                        return Path;
                }
            }
        }
        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region Type

        /// <summary>
        /// Type property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RecentPlanogramType> TypeProperty =
            RegisterModelProperty<RecentPlanogramType>(c => c.Type);

        /// <summary>
        /// Gets/Sets the Type value
        /// </summary>
        public RecentPlanogramType Type
        {
            get { return GetProperty<RecentPlanogramType>(TypeProperty); }
            set { SetProperty<RecentPlanogramType>(TypeProperty, value); }
        }

        #endregion

        #region DateLastAccessed

        /// <summary>
        /// DateLastAccessed property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastAccessedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastAccessed);

        /// <summary>
        /// Gets/Sets the DateLastAccessed value
        /// </summary>
        public DateTime DateLastAccessed
        {
            get { return GetProperty<DateTime>(DateLastAccessedProperty); }
            set { SetProperty<DateTime>(DateLastAccessedProperty, value); }
        }

        #endregion

        #region IsPinned

        /// <summary>
        /// IsPinned property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsPinnedProperty =
            RegisterModelProperty<Boolean>(c => c.IsPinned);

        /// <summary>
        /// Gets/Sets whether this document is pinned.
        /// </summary>
        public Boolean IsPinned
        {
            get { return GetProperty<Boolean>(IsPinnedProperty); }
            set { SetProperty<Boolean>(IsPinnedProperty, value); }
        }

        #endregion

        #region Location

        /// <summary>
        /// Location property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> LocationProperty =
            RegisterModelProperty<String>(c => c.Location);

        /// <summary>
        /// Gets/Sets the Location value
        /// </summary>
        internal String Location
        {
            get { return GetProperty<String>(LocationProperty); }
            set { SetProperty<String>(LocationProperty, value); }
        }

        #endregion

        #region Helper Properties

        #region IsForCurrentRepository

        /// <summary>
        /// IsForCurrentRepository property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsForCurrentRepositoryProperty =
            RegisterModelProperty<Boolean>(c => c.IsForCurrentRepository);

        /// <summary>
        /// Gets/Sets a helper property to indicate if this entry is for the currently connected db.
        /// </summary>
        public Boolean IsForCurrentRepository
        {
            get { return GetProperty<Boolean>(IsForCurrentRepositoryProperty); }
            private set {SetProperty<Boolean>(IsForCurrentRepositoryProperty, value);}
        }

        #endregion

        /// <summary>
        /// Returns the id to use when fetching the current planogram
        /// </summary>
        public Object PlanogramId
        {
            get
            {
                switch (this.Type)
                {
                    case RecentPlanogramType.Pog:
                        return this.Location;
                        

                    case RecentPlanogramType.Repository:
                        {
                            String[] locationPaths = this.Location.Split(LocationSeparator);

                            String locationPlanId =
                                (locationPaths.Length == 4)?
                                locationPaths[3]
                                : this.Location;
                            

                            Int32 planId = 0;
                            Int32.TryParse(locationPlanId, out planId);

                            return planId;
                        }
                        

                    default:
                        Debug.Fail("Plan type not recognised");
                        return this.Location;
                        
                }
            }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));

        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static RecentPlanogram NewRecentPlanogram()
        {
            RecentPlanogram item = new RecentPlanogram();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static RecentPlanogram NewRecentPlanogram(
            String planName, String location, String displayPath, RecentPlanogramType planogramType)
        {
            RecentPlanogram item = new RecentPlanogram();
            item.Create(planName, location, displayPath, planogramType);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(String planName, String location, String displayPath, RecentPlanogramType planogramType)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, planName);
            this.LoadProperty<String>(PathProperty, displayPath);
            this.LoadProperty<String>(LocationProperty, location);
            this.LoadProperty<RecentPlanogramType>(TypeProperty, planogramType);
            this.LoadProperty<DateTime>(DateLastAccessedProperty, DateTime.UtcNow);
            this.LoadProperty<Boolean>(IsForCurrentRepositoryProperty, (planogramType == RecentPlanogramType.Repository));
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<RecentPlanogram>(oldId, newId);
        }

        /// <summary>
        /// Updates the IsForCurrentRepositoryFlag.
        /// </summary>
        internal void UpdateIsForCurrentRepository(String server, String dbName, Int32 entityId)
        {
            if(this.Type != RecentPlanogramType.Repository)
            {
                this.IsForCurrentRepository = false;
                return;
            }

            String[] locationPaths = this.Location.Split(LocationSeparator);
            
            //if we dont have server details then just mark this as ok.
            if (locationPaths.Length != 4)
            {
                this.IsForCurrentRepository = true;
                return;
            }

            //compare the parts of the location path.
            if (String.Compare(server, locationPaths[0], true) == 0
                && String.Compare(dbName, locationPaths[1], true) == 0
                && String.Compare(entityId.ToString(), locationPaths[2], true) == 0)
            {
                this.IsForCurrentRepository = true;
            }
            else
            {
                this.IsForCurrentRepository = false;
            }
        }

        #endregion
    }
}