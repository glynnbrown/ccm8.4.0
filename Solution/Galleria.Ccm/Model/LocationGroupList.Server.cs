﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public sealed partial class LocationGroupList
    {
        #region Constructor
        private LocationGroupList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        /// <returns>A list of existing it</returns>
        internal static LocationGroupList FetchByParentLocationGroupId(
            IDalContext dalContext, Int32 parentId, IEnumerable<LocationGroupDto> groupDtoList)
        {
            return DataPortal.FetchChild<LocationGroupList>(dalContext, parentId, groupDtoList);
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parentNodeId">The parent node id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 parentId, IEnumerable<LocationGroupDto> groupDtoList)
        {
            RaiseListChangedEvents = false;

            //get a list of child dtos and add them to this
            foreach (LocationGroupDto childDto in groupDtoList)
            {
                if (Object.Equals(childDto.ParentGroupId, parentId))
                {
                    this.Add(LocationGroup.Fetch(dalContext, childDto, groupDtoList));
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}