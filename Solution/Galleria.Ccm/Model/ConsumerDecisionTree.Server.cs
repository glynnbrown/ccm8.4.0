﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8025632 : A.Kuszyk
//		Created (copied from SA).
// V8-25455 : J.Pickup
//		Added FetchById
// V8-27132 : A.Kuszyk
//      Added FetchByEntityIdName.
#endregion
#region Version History: (CCM 8.0.3)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#region Version History: (CCM 8.1.0)
// V8-29936 : N.Haywood
//  Update will set a new ucr
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    public partial class ConsumerDecisionTree
    {
        #region Constructor
        private ConsumerDecisionTree() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the consumer decision tree with the given id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ConsumerDecisionTree FetchById(Int32 id)
        {
            return DataPortal.Fetch<ConsumerDecisionTree>(new FetchByIdCriteria(id));
        }

        public static ConsumerDecisionTree FetchByEntityIdName(Int32 entityId, String name)
        {
            return DataPortal.Fetch<ConsumerDecisionTree>(new FetchByEntityIdNameCriteria(entityId, name));
        }

        /// <summary>
        /// Returns an consumer decision tree from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        internal static ConsumerDecisionTree FetchConsumerDecisionTree(IDalContext dalContext, ConsumerDecisionTreeDto dto)
        {
            return DataPortal.FetchChild<ConsumerDecisionTree>(dalContext, dto);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Ensures that all level ids are correctly synched with product groups
        /// </summary>
        private void UpdateChildGroupToLevelIds()
        {
            //only proceed if levels exist
            List<ConsumerDecisionTreeLevel> childLevelList = this.FetchAllLevels().ToList();
            if (childLevelList.Count > 0)
            {
                //get the resolved id dict
                //Dictionary<Guid, Int32> resolvedIdDict =
                  //  ConsumerDecisionTreeLevel.ConstructResolvedIdDictionary(childLevelList);

                //get the list of groups to be ammended
                IEnumerable<ConsumerDecisionTreeNode> nodeList = this.FetchAllNodes();

                //resolve
                //ConsumerDecisionTreeNode.ResolveConsumerDecisionTreeLevelReferences(nodeList, resolvedIdDict);
            }

        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private ConsumerDecisionTreeDto GetDataTransferObject()
        {
            return new ConsumerDecisionTreeDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                UniqueContentReference = ReadProperty<Guid>(UniqueContentReferenceProperty),
                Name = ReadProperty<String>(NameProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                ProductGroupId = ReadProperty<Int32?>(ProductGroupIdProperty),
                Type = (Byte)ReadProperty<ConsumerDecisionTreeType>(TypeProperty),
                ParentUniqueContentReference = ReadProperty<Guid?>(ParentUniqueContentReferenceProperty)
            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        /// <param name="childNodeList">list of child nodes for this hierarchy</param>
        private void LoadDataTransferObject(
            IDalContext dalContext,
            ConsumerDecisionTreeDto dto,
            IEnumerable<ConsumerDecisionTreeLevelDto> childConsumerDecisionTreeLevelList,
            IEnumerable<ConsumerDecisionTreeNodeDto> childNodeList)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            LoadProperty<Guid>(UniqueContentReferenceProperty, dto.UniqueContentReference);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            LoadProperty<Int32?>(ProductGroupIdProperty, dto.ProductGroupId);
            LoadProperty<ConsumerDecisionTreeType>(TypeProperty, (ConsumerDecisionTreeType)dto.Type);
            LoadProperty<Guid?>(ParentUniqueContentReferenceProperty, dto.ParentUniqueContentReference);

            //get the root level dto and load it
            ConsumerDecisionTreeLevelDto rootLevelDto = childConsumerDecisionTreeLevelList.FirstOrDefault(l => l.ParentLevelId.Equals(null));
            if (rootLevelDto != null)
            {
                LoadProperty<ConsumerDecisionTreeLevel>(RootLevelProperty,
                    ConsumerDecisionTreeLevel.FetchConsumerDecisionTreeLevel(dalContext, rootLevelDto, childConsumerDecisionTreeLevelList));
            }

            //get the root group dto and load
            ConsumerDecisionTreeNodeDto rootNodeDto = childNodeList.FirstOrDefault(l => l.ParentNodeId == null);
            if (rootNodeDto != null)
            {
                LoadProperty<ConsumerDecisionTreeNode>(RootNodeProperty,
                    ConsumerDecisionTreeNode.FetchConsumerDecisionTreeNode(dalContext, rootNodeDto, childNodeList));
            }
        }

        #endregion

        #region Fetch

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdNameCriteria criteria)
        {
            var dalFactory = DalContainer.GetDalFactory();
            ConsumerDecisionTreeDto dto;
            using(var dalContext = dalFactory.CreateContext())
            using(var dal = dalContext.GetDal<IConsumerDecisionTreeDal>())
            {
                dto = dal.FetchByEntityIdName(criteria.EntityId, criteria.Name);
                LoadHierarchy(dto.Id, dalContext, dto);
            }
        }

        /// <summary>
        /// Called when return FetchById
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(ConsumerDecisionTree.FetchByIdCriteria criteria)
        {
            Int32 treeId = criteria.Id;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                // fetch the structure dto
                ConsumerDecisionTreeDto dto;
                using (IConsumerDecisionTreeDal dal = dalContext.GetDal<IConsumerDecisionTreeDal>())
                {
                    dto = dal.FetchById(treeId);
                }

                LoadHierarchy(treeId, dalContext, dto);
            }
        }

        private void LoadHierarchy(Int32 treeId, IDalContext dalContext, ConsumerDecisionTreeDto dto)
        {
            //now use the hierarchy id to get all the levels in one go
            IEnumerable<ConsumerDecisionTreeLevelDto> levelDtoList;
            using (IConsumerDecisionTreeLevelDal dal = dalContext.GetDal<IConsumerDecisionTreeLevelDal>())
            {
                levelDtoList = dal.FetchByConsumerDecisionTreeId(treeId);
            }


            //and get all the nodes in one go
            IEnumerable<ConsumerDecisionTreeNodeDto> nodeDtoList;
            using (IConsumerDecisionTreeNodeDal dal = dalContext.GetDal<IConsumerDecisionTreeNodeDal>())
            {
                nodeDtoList = dal.FetchByConsumerDecisionTreeId(treeId);
            }

            //load the hierarchy
            LoadDataTransferObject(dalContext, dto, levelDtoList, nodeDtoList);
        }

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ConsumerDecisionTreeDto dto)
        {
            //now use the hierarchy id to get all the levels in one go
            IEnumerable<ConsumerDecisionTreeLevelDto> levelDtoList;
            using (IConsumerDecisionTreeLevelDal dal = dalContext.GetDal<IConsumerDecisionTreeLevelDal>())
            {
                levelDtoList = dal.FetchByConsumerDecisionTreeId(dto.Id);
            }

            //and get all the nodes in one go
            IEnumerable<ConsumerDecisionTreeNodeDto> nodeDtoList;
            using (IConsumerDecisionTreeNodeDal dal = dalContext.GetDal<IConsumerDecisionTreeNodeDal>())
            {
                nodeDtoList = dal.FetchByConsumerDecisionTreeId(dto.Id);
            }

            this.LoadDataTransferObject(dalContext, dto, levelDtoList, nodeDtoList);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                ConsumerDecisionTreeDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (IConsumerDecisionTreeDal dal = dalContext.GetDal<IConsumerDecisionTreeDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                dalContext.RegisterId<ConsumerDecisionTree>(oldId, dto.Id);
                DataPortal.UpdateChild(this.RootLevel, dalContext, this);
                FieldManager.UpdateChildren(dalContext, this);
                DataPortal.UpdateChild(this.RootNode, dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this object in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                ConsumerDecisionTreeDto dto = GetDataTransferObject();
                dto.UniqueContentReference = Guid.NewGuid();
                using (IConsumerDecisionTreeDal dal = dalContext.GetDal<IConsumerDecisionTreeDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                DataPortal.UpdateChild(this.RootLevel, dalContext, this);
                FieldManager.UpdateChildren(dalContext, this);
                DataPortal.UpdateChild(this.RootNode, dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IConsumerDecisionTreeDal dal = dalContext.GetDal<IConsumerDecisionTreeDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #endregion
    }
}
