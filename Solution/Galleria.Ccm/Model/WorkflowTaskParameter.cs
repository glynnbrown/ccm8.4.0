﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
// V8-25886 : L.Ineson
//  Removed value, added values
//  Added IsHidden and IsReadOnly
#endregion
#region Version History: CCM820
// V8-30596 : N.Foster
//  Resolved issue where duplicate tasks in same workflow resulted in exception
// V8-30771 : N.Foster
//  Add support for additional task parameter properties
#endregion
#region Version History: CCM830
// V8-32133 : A.Probyn
//   Updated AddValues helper method
// V8-32639 : A.Silva
//  Pulled in ListRelatedParameters method from PerformanceSelectionNameTaskParameterHelper.

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class WorkflowTaskParameter : ModelObject<WorkflowTaskParameter>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent workflow
        /// </summary>
        public new WorkflowTask Parent
        {
            get { return ((WorkflowTaskParameterList)base.Parent).Parent; }
        }
        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id Property
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the parameter id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region TaskDetails
        /// <summary>
        /// TaskDetails property definition
        /// </summary>
        public static readonly ModelPropertyInfo<EngineTaskInfo> TaskDetailsProperty =
            RegisterModelProperty<EngineTaskInfo>(c => c.TaskDetails);
        /// <summary>
        /// Returns the workflow task details
        /// </summary>
        public EngineTaskInfo TaskDetails
        {
            get { return this.GetProperty<EngineTaskInfo>(TaskDetailsProperty); }
        }
        #endregion

        #region ParameterDetails
        /// <summary>
        /// ParameterDetails property definition
        /// </summary>
        public static readonly ModelPropertyInfo<EngineTaskParameterInfo> ParameterDetailsProperty =
            RegisterModelProperty<EngineTaskParameterInfo>(c => c.ParameterDetails);
        /// <summary>
        /// Returns the workflow task parameter details
        /// </summary>
        public EngineTaskParameterInfo ParameterDetails
        {
            get { return this.GetProperty<EngineTaskParameterInfo>(ParameterDetailsProperty); }
        }
        #endregion

        #region IsHidden
        /// <summary>
        /// IsHidden Property
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsHiddenProperty =
            RegisterModelProperty<Boolean>(c => c.IsHidden);
        /// <summary>
        /// Gets/Sets whether this parameter should be hidden on the bag grid.
        /// </summary>
        public Boolean IsHidden
        {
            get { return this.GetProperty<Boolean>(IsHiddenProperty); }
            set { this.SetProperty<Boolean>(IsHiddenProperty, value); }
        }
        #endregion

        #region IsReadOnly
        /// <summary>
        /// IsReadOnly Property
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsReadOnlyProperty =
            RegisterModelProperty<Boolean>(c => c.IsReadOnly);
        /// <summary>
        /// Gets/Sets whether this parameter should be hidden on the bag grid.
        /// </summary>
        public Boolean IsReadOnly
        {
            get { return this.GetProperty<Boolean>(IsReadOnlyProperty); }
            set { this.SetProperty<Boolean>(IsReadOnlyProperty, value); }
        }
        #endregion

        #region Values
        /// <summary>
        /// Values property definition
        /// </summary>
        public static readonly ModelPropertyInfo<WorkflowTaskParameterValueList> ValuesProperty =
            RegisterModelProperty<WorkflowTaskParameterValueList>(c => c.Values);
        /// <summary>
        /// Returns all parameter values
        /// </summary>
        public WorkflowTaskParameterValueList Values
        {
            get { return this.GetProperty<WorkflowTaskParameterValueList>(ValuesProperty); }
        }
        #endregion

        #region Helper Properties

        /// <summary>
        /// Helper property to Get/Set the value
        /// where this is a single value parameter type.
        /// </summary>
        public Object Value
        {
            get
            {
                if (this.Values.Count > 0)
                {
                    return this.Values.First().Value1;
                }
                return null;
            }

            set
            {
                this.Values.Clear();

                if (value != null)
                {
                    var paramValue = WorkflowTaskParameterValue.NewWorkflowTaskParameterValue();
                    paramValue.Value1 = value;

                    this.Values.Add(paramValue);
                }
                OnPropertyChanged("Value");
            }
        }

        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(WorkflowTaskParameter), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskParameter), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.WorkflowCreate.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskParameter), new IsInRole(AuthorizationActions.EditObject, DomainPermission.WorkflowEdit.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskParameter), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.WorkflowDelete.ToString()));
        }
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new Entity
        /// </summary>
        internal static WorkflowTaskParameter NewWorkflowTaskParameter(EngineTaskInfo task, EngineTaskParameterInfo parameter)
        {
            WorkflowTaskParameter item = new WorkflowTaskParameter();
            item.Create(task, parameter);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected void Create(EngineTaskInfo task, EngineTaskParameterInfo parameter)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<WorkflowTaskParameterValueList>(ValuesProperty, WorkflowTaskParameterValueList.NewWorkflowTaskParameterValueList(parameter.DefaultValue));
            this.LoadProperty<EngineTaskInfo>(TaskDetailsProperty, task.Copy());
            this.LoadProperty<EngineTaskParameterInfo>(ParameterDetailsProperty, parameter.Copy());
            this.LoadProperty<Boolean>(IsHiddenProperty, parameter.IsHiddenByDefault);
            this.LoadProperty<Boolean>(IsReadOnlyProperty, parameter.IsReadOnlyByDefault);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// ToString override
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.ParameterDetails.Name;
        }

        /// <summary>
        /// Returns the related parent parameter
        /// </summary>
        public WorkflowTaskParameter GetParentParameter()
        {
            return this.Parent.Parameters.FirstOrDefault(p => p.ParameterDetails.Id == this.ParameterDetails.ParentId);
        }

        /// <summary>
        /// Returns the related child parameters
        /// </summary>
        public IEnumerable<WorkflowTaskParameter> GetChildParameters()
        {
            return this.Parent.Parameters.Where(p => p.ParameterDetails.ParentId == this.ParameterDetails.Id);
        }

        /// <summary>
        /// Indicates if a value is required for this parameter. Returns null
        /// if the parameter is required, else returns a description of why
        /// the parameter is not required to be provided a value
        /// </summary>
        public String IsRequired()
        {
            using (TaskBase task = this.TaskDetails.CreateTask())
            {
                task.InitializeParameters(this.Parent);
                TaskParameter taskParameter = task.Parameters.FirstOrDefault(p => p.Id == this.ParameterDetails.Id);
                if (taskParameter != null)
                {
                    return taskParameter.IsRequired;
                }
            }
            return null;
        }

        /// <summary>
        /// Indicates if a null value is allowed for this parameter
        /// </summary>
        public Boolean IsNullAllowed()
        {
            using (TaskBase task = this.TaskDetails.CreateTask())
            {
                task.InitializeParameters(this.Parent);
                TaskParameter taskParameter = task.Parameters.FirstOrDefault(p => p.Id == this.ParameterDetails.Id);
                if (taskParameter != null)
                {
                    return taskParameter.IsNullAllowed;
                }
            }
            return false;
        }

        /// <summary>
        /// Indicates if the parameter is value is valid or not. If value
        /// then null is returned, else a desciption why the parameter is not
        /// valid is returned instead
        /// </summary>
        public String IsValid()
        {
            using (TaskBase task = this.TaskDetails.CreateTask())
            {
                task.InitializeParameters(this.Parent);
                TaskParameter taskParameter = task.Parameters.FirstOrDefault(p => p.Id == this.ParameterDetails.Id);
                if (taskParameter != null)
                {
                    return taskParameter.IsValid;
                }
            }
            return null;
        }

        /// <summary>
        /// Helper methods to add a series of new planogram parameter values, and triggered the property changed event
        /// on the value property.
        /// </summary>
        /// <param name="values"></param>
        /// <param name="triggerValueChanged">True if the OnValueChanged is required to be triggered as
        /// a result of this update as the 'Value' property changed is triggered. False if only the 'Values'
        /// property changed changed is required.</param>
        public void AddValues(IEnumerable<WorkflowTaskParameterValue> values, Boolean triggerValueChanged)
        {
            this.Values.AddRange(values);

            if (triggerValueChanged)
            {
                OnPropertyChanged("Value");
            }
            else
            {
                //Trigger values changed so any 'Values' related properties can be updated too. This
                //stops all the other updates as part of the 'Value' property changed.
                OnPropertyChanged("Values");
            }
        }

        /// <summary>
        ///     Enumerate the <see cref="WorkflowTaskParameter"/> instances in the parent <see cref="WorkflowTask"/> that are related to this workflowTaskParameter.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<WorkflowTaskParameter> EnumerateRelatedParameters()
        {
            if (Parent == null) return new List<WorkflowTaskParameter>();

            Int32 myId = Parent.Parameters.IndexOf(this);
            return Parent.Parameters.Where(p => p.ParameterDetails.ParentId == myId);
        }

        #endregion
    }
}