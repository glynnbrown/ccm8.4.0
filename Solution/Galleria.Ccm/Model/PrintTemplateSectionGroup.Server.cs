﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class PrintTemplateSectionGroup
    {
        #region Constructor
        private PrintTemplateSectionGroup() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static PrintTemplateSectionGroup Fetch(IDalContext dalContext, PrintTemplateSectionGroupDto dto)
        {
            return DataPortal.FetchChild<PrintTemplateSectionGroup>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PrintTemplateSectionGroupDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Byte>(NumberProperty, dto.Number);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Byte>(BaysPerPageProperty, dto.BaysPerPage);

            this.LoadProperty<PrintTemplateSectionList>(SectionsProperty, 
                PrintTemplateSectionList.FetchByParentId(dalContext, dto.Id));
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private PrintTemplateSectionGroupDto GetDataTransferObject(PrintTemplate parent)
        {
            return new PrintTemplateSectionGroupDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                PrintTemplateId = parent.Id,
                Number = ReadProperty<Byte>(NumberProperty),
                Name = ReadProperty<String>(NameProperty),
                BaysPerPage = ReadProperty<Byte>(BaysPerPageProperty),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, PrintTemplateSectionGroupDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, PrintTemplate parent)
        {
            PrintTemplateSectionGroupDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IPrintTemplateSectionGroupDal dal = dalContext.GetDal<IPrintTemplateSectionGroupDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<PrintTemplateSectionGroup>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, PrintTemplate parent)
        {
            if (this.IsSelfDirty)
            {
                using (IPrintTemplateSectionGroupDal dal = dalContext.GetDal<IPrintTemplateSectionGroupDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IPrintTemplateSectionGroupDal dal = dalContext.GetDal<IPrintTemplateSectionGroupDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion

    }
}