﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Denotes the available area unit of measure types
    /// </summary>
    public enum FixtureAngleUnitOfMeasureType
    {
        Unknown = 0,
        Radians = 1,
        Degrees = 2
    }

    /// <summary>
    /// FixtureAngleUnitOfMeasureType Helper Class
    /// </summary>
    public static class FixtureAngleUnitOfMeasureTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<FixtureAngleUnitOfMeasureType, String> FriendlyNames =
            new Dictionary<FixtureAngleUnitOfMeasureType, String>()
            {
                {FixtureAngleUnitOfMeasureType.Unknown, String.Empty},
                {FixtureAngleUnitOfMeasureType.Radians, Message.Enum_FixtureAngleUnitOfMeasureType_Radians},
                {FixtureAngleUnitOfMeasureType.Degrees, Message.Enum_FixtureAngleUnitOfMeasureType_Degrees},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<FixtureAngleUnitOfMeasureType, String> Abbreviations =
            new Dictionary<FixtureAngleUnitOfMeasureType, String>()
            {
                {FixtureAngleUnitOfMeasureType.Unknown, String.Empty},
                {FixtureAngleUnitOfMeasureType.Radians, Message.Enum_FixtureAngleUnitOfMeasureType_Radians_Abbrev},
                {FixtureAngleUnitOfMeasureType.Degrees, Message.Enum_FixtureAngleUnitOfMeasureType_Degrees_Abbrev},
            };

    }
}
