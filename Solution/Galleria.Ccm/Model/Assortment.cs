﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
// CCM-26322 : L.Ineson
//  Made ProductGroupId not null to match table script.
// V8-26704 : A.Kuszyk
//  Implemented IPlanogramAssortment.
// V8-27241 : A.Kuszyk
//  Added FetchByEntityIdName.
#endregion
#region Version History: (CCM CCM803)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#region Version History: (CCM CCM830)
// V8-31550 : A.Probyn
//  Added ProductBuddies & LocationBuddies
// V8-31551 : A.Probyn
//  Added InventoryRules
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a Assortment object
    /// (Root Object)
    /// 
    /// A class representing an assortment within CCM. Assortments contain a list of products 
    /// along with criteria by which the products will be ranged, and a list of locations that dictates 
    /// in which stores the asssortment is valid.
    /// 
    /// This is a root object and so it has the following properties/actions:
    /// - This object has a DateDeleted property and associated field in its supporting database table
    /// - If this object is deleted, it is marked as deleted (this allows for 'undeletes') and any child objects are left unchanged
    /// </summary>
    [Serializable]
    [DefaultNewMethod("NewAssortment", "EntityId")]
    public sealed partial class Assortment : ModelObject<Assortment>, IPlanogramAssortment
    {
        #region Static Constructor
        static Assortment()
        {
            FriendlyName = Message.Assortment;
        }
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The Assortment Id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }


        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
           RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The row version timestamp
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }


        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        /// <summary>
        /// The entity id
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
        }


        /// <summary>
        /// Product group id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProductGroupIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductGroupId);
        /// <summary>
        /// The id of the product group this item is assigned to
        /// </summary>
        public Int32 ProductGroupId
        {
            get { return GetProperty<Int32>(ProductGroupIdProperty); }
            set { SetProperty<Int32>(ProductGroupIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<Guid> UniqueContentReferenceProperty =
            RegisterModelProperty<Guid>(c => c.UniqueContentReference);
        /// <summary>
        /// Unique content reference
        /// </summary>
        public Guid UniqueContentReference
        {
            get { return GetProperty<Guid>(UniqueContentReferenceProperty); }
            set { SetProperty<Guid>(UniqueContentReferenceProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Assortments Name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }


        public static readonly ModelPropertyInfo<Int32?> ConsumerDecisionTreeIdProperty =
            RegisterModelProperty<Int32?>(c => c.ConsumerDecisionTreeId);
        /// <summary>
        /// The Consumer Decision Tree Id
        /// </summary>
        public Int32? ConsumerDecisionTreeId
        {
            get { return GetProperty<Int32?>(ConsumerDecisionTreeIdProperty); }
            set { SetProperty<Int32?>(ConsumerDecisionTreeIdProperty, value); }
        }


        public static readonly ModelPropertyInfo<AssortmentProductList> ProductsProperty =
            RegisterModelProperty<AssortmentProductList>(c => c.Products);
        /// <summary>
        /// The Assortment Products. The list of products in the assortment, including
        /// whether they are ranged, any product rules that apply etc
        /// </summary>
        public AssortmentProductList Products
        {
            get { return GetProperty<AssortmentProductList>(ProductsProperty); }
        }


        public static readonly ModelPropertyInfo<AssortmentLocationList> LocationsProperty =
            RegisterModelProperty<AssortmentLocationList>(c => c.Locations);
        /// <summary>
        /// The Assortment Locations. This is a list of stores where the assortment is used
        /// </summary>
        public AssortmentLocationList Locations
        {
            get { return GetProperty<AssortmentLocationList>(LocationsProperty); }
        }


        public static readonly ModelPropertyInfo<AssortmentLocalProductList> LocalProductsProperty =
            RegisterModelProperty<AssortmentLocalProductList>(c => c.LocalProducts);
        /// <summary>
        /// The Assortment Local Product List. This is a list of products and the specific 
        /// locations where they MUST be placed.
        /// </summary>
        public AssortmentLocalProductList LocalProducts
        {
            get { return GetProperty<AssortmentLocalProductList>(LocalProductsProperty); }
        }


        public static readonly ModelPropertyInfo<AssortmentRegionList> RegionsProperty =
            RegisterModelProperty<AssortmentRegionList>(c => c.Regions);
        /// <summary>
        /// The Assortment Regions. These are used for marking where regional product
        /// swaps will occur.
        /// </summary>
        public AssortmentRegionList Regions
        {
            get { return GetProperty<AssortmentRegionList>(RegionsProperty); }
        }


        public static readonly ModelPropertyInfo<AssortmentFileList> FilesProperty =
            RegisterModelProperty<AssortmentFileList>(c => c.Files);
        /// <summary>
        /// The Assortment Files.
        /// </summary>
        public AssortmentFileList Files
        {
            get { return GetProperty<AssortmentFileList>(FilesProperty); }
        }

        public static readonly ModelPropertyInfo<AssortmentInventoryRuleList> InventoryRulesProperty =
            RegisterModelProperty<AssortmentInventoryRuleList>(c => c.InventoryRules);
        /// <summary>
        /// The Assortment Inventory Rules. These are used for marking which products have inventory rules.
        /// </summary>
        public AssortmentInventoryRuleList InventoryRules
        {
            get { return GetProperty<AssortmentInventoryRuleList>(InventoryRulesProperty); }
        }

        public static readonly ModelPropertyInfo<AssortmentLocationBuddyList> LocationBuddiesProperty =
            RegisterModelProperty<AssortmentLocationBuddyList>(c => c.LocationBuddies);
        /// <summary>
        /// The Assortment Location Buddys. These are used for marking which products have location buddys.
        /// </summary>
        public AssortmentLocationBuddyList LocationBuddies
        {
            get { return GetProperty<AssortmentLocationBuddyList>(LocationBuddiesProperty); }
        }

        public static readonly ModelPropertyInfo<AssortmentProductBuddyList> ProductBuddiesProperty =
            RegisterModelProperty<AssortmentProductBuddyList>(c => c.ProductBuddies);
        /// <summary>
        /// The Assortment Product Buddys. These are used for marking which products have Product buddys.
        /// </summary>
        public AssortmentProductBuddyList ProductBuddies
        {
            get { return GetProperty<AssortmentProductBuddyList>(ProductBuddiesProperty); }
        }

        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);
        /// <summary>
        /// The Assortments created date.
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
        }


        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        /// <summary>
        /// The Assortments date last modified
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
        }

        /// <summary>
        /// Parent unique content reference property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Guid?> ParentUniqueContentReferenceProperty =
            RegisterModelProperty<Guid?>(c => c.ParentUniqueContentReference);
        /// <summary>
        /// Parent unique content reference
        /// </summary>
        public Guid? ParentUniqueContentReference
        {
            get { return GetProperty<Guid?>(ParentUniqueContentReferenceProperty); }
        }
        
        #region IPlanogramAssortment members

        /// <summary>
        /// IPlanogramAssortment Products member.
        /// </summary>
        IEnumerable<IPlanogramAssortmentProduct> IPlanogramAssortment.Products
        {
            get { return this.Products.Select(i => (IPlanogramAssortmentProduct)i); }
        }

        /// <summary>
        /// IPlanogramAssortment Local Products member.
        /// </summary>
        IEnumerable<IPlanogramAssortmentLocalProduct> IPlanogramAssortment.LocalProducts
        {
            get { return LocalProducts.Select(o => (IPlanogramAssortmentLocalProduct)o); }
        }

        /// <summary>
        /// IPlanogramAssortment Regions member.
        /// </summary>
        IEnumerable<IPlanogramAssortmentRegion> IPlanogramAssortment.Regions
        {
            get { return Regions.Select(o => (IPlanogramAssortmentRegion)o); }
        }

        /// <summary>
        /// IPlanogramAssortment InventoryRules member.
        /// </summary>
        IEnumerable<IPlanogramAssortmentInventoryRule> IPlanogramAssortment.InventoryRules
        {
            get { return InventoryRules.Select(o => (IPlanogramAssortmentInventoryRule)o); }
        }

        /// <summary>
        /// IPlanogramAssortment ProductBuddies member.
        /// </summary>
        IEnumerable<IPlanogramAssortmentProductBuddy> IPlanogramAssortment.ProductBuddies
        {
            get { return ProductBuddies.Select(o => (IPlanogramAssortmentProductBuddy)o); }
        }

        /// <summary>
        /// IPlanogramAssortment LocationBuddies member.
        /// </summary>
        IEnumerable<IPlanogramAssortmentLocationBuddy> IPlanogramAssortment.LocationBuddies
        {
            get { return LocationBuddies.Select(o => (IPlanogramAssortmentLocationBuddy)o); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(Assortment), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.AssortmentCreate.ToString()));
            BusinessRules.AddRule(typeof(Assortment), new IsInRole(AuthorizationActions.GetObject, DomainPermission.AssortmentGet.ToString()));
            BusinessRules.AddRule(typeof(Assortment), new IsInRole(AuthorizationActions.EditObject, DomainPermission.AssortmentEdit.ToString()));
            BusinessRules.AddRule(typeof(Assortment), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.AssortmentDelete.ToString()));
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByUniqueContentReference
        /// </summary>
        [Serializable]
        public class FetchByUniqueContentReferenceCriteria : CriteriaBase<FetchByUniqueContentReferenceCriteria>
        {
            public static readonly PropertyInfo<Guid> UniqueContentReferenceProperty =
                RegisterProperty<Guid>(c => c.UniqueContentReference);
            public Guid UniqueContentReference
            {
                get { return ReadProperty<Guid>(UniqueContentReferenceProperty); }
            }

            public FetchByUniqueContentReferenceCriteria(Guid ucr)
            {
                LoadProperty<Guid>(UniqueContentReferenceProperty, ucr);
            }
        }

        [Serializable]
        public class FetchByEntityIdNameCriteria : CriteriaBase<FetchByEntityIdNameCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static readonly PropertyInfo<String> NameProperty =
                RegisterProperty<String>(c => c.Name);
            public String Name
            {
                get { return ReadProperty<String>(NameProperty); }
            }

            public FetchByEntityIdNameCriteria(Int32 entityId, String name)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<String>(NameProperty, name);
            }
        }

        /// <summary>
        /// Criteria for DeleteByParentContentId
        /// </summary>
        [Serializable]
        public class DeleteByAssortmentIdCriteria : Csla.CriteriaBase<DeleteByAssortmentIdCriteria>
        {
            public static readonly PropertyInfo<Int32> AssortmentIdProperty =
            RegisterProperty<Int32>(c => c.AssortmentId);
            public Int32 AssortmentId
            {
                get { return ReadProperty<Int32>(AssortmentIdProperty); }
            }

            public DeleteByAssortmentIdCriteria(Int32 assortmentId)
            {
                LoadProperty<Int32>(AssortmentIdProperty, assortmentId);
            }
        }

        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="entityId">The id of the entity to create for</param>
        /// <returns>The new assortment</returns>
        public static Assortment NewAssortment(Int32 entityId)
        {
            Assortment item = new Assortment();
            item.Create(entityId);
            return item;
        }

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns></returns>
        public static Assortment NewAssortment(Int32 entityId, Guid UniqueContentReference)
        {
            Assortment item = new Assortment();
            item.Create(entityId, UniqueContentReference);
            return item;
        }


        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when this instance is being created
        /// </summary>
        private void Create(Int32 entityId)
        {
            LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty<Int32>(EntityIdProperty, entityId);
            LoadProperty<Guid>(UniqueContentReferenceProperty, Guid.NewGuid());
            LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            LoadProperty<AssortmentProductList>(ProductsProperty, AssortmentProductList.NewAssortmentProductList());
            LoadProperty<AssortmentLocationList>(LocationsProperty, AssortmentLocationList.NewAssortmentLocationList());
            LoadProperty<AssortmentLocalProductList>(LocalProductsProperty, AssortmentLocalProductList.NewAssortmentLocalProductList());
            LoadProperty<AssortmentRegionList>(RegionsProperty, AssortmentRegionList.NewAssortmentRegionList());
            LoadProperty<AssortmentFileList>(FilesProperty, AssortmentFileList.NewAssortmentFileList());
            LoadProperty<AssortmentInventoryRuleList>(InventoryRulesProperty, AssortmentInventoryRuleList.NewAssortmentInventoryRuleList());
            LoadProperty<AssortmentLocationBuddyList>(LocationBuddiesProperty, AssortmentLocationBuddyList.NewAssortmentLocationBuddyList());
            LoadProperty<AssortmentProductBuddyList>(ProductBuddiesProperty, AssortmentProductBuddyList.NewAssortmentProductBuddyList());
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="uniqueContentReference"></param>
        private void Create(Int32 entityId, Guid uniqueContentReference)
        {
            LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty<AssortmentProductList>(ProductsProperty, AssortmentProductList.NewAssortmentProductList());
            LoadProperty<AssortmentLocationList>(LocationsProperty, AssortmentLocationList.NewAssortmentLocationList());
            LoadProperty<AssortmentLocalProductList>(LocalProductsProperty, AssortmentLocalProductList.NewAssortmentLocalProductList());
            LoadProperty<AssortmentRegionList>(RegionsProperty, AssortmentRegionList.NewAssortmentRegionList());
            LoadProperty<AssortmentFileList>(FilesProperty, AssortmentFileList.NewAssortmentFileList());
            LoadProperty<AssortmentInventoryRuleList>(InventoryRulesProperty, AssortmentInventoryRuleList.NewAssortmentInventoryRuleList());
            LoadProperty<AssortmentLocationBuddyList>(LocationBuddiesProperty, AssortmentLocationBuddyList.NewAssortmentLocationBuddyList());
            LoadProperty<AssortmentProductBuddyList>(ProductBuddiesProperty, AssortmentProductBuddyList.NewAssortmentProductBuddyList());
            base.Create();
        }

        #endregion

        #endregion

        #region Override

        public override string ToString()
        {
            return this.Name;
        }

        #endregion

    }
}