﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM810)
// V8-29861 : A.Probyn
//	Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Csla.Rules;
using Galleria.Ccm.Security;
using Csla.Rules.CommonRules;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class PlanogramEventLogInfoList : ReadOnlyListBase<PlanogramEventLogInfoList, PlanogramEventLogInfo>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PlanogramEventLogInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanogramEventLogInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(PlanogramEventLogInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanogramEventLogInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Criteria
        
        #region FetchByWorkpackageIdCriteria
        [Serializable]
        public class FetchByWorkpackageIdCriteria : CriteriaBase<FetchByWorkpackageIdCriteria>
        {
            #region Properties

            #region WorkpackageId
            /// <summary>
            /// EntityId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> WorkpackageIdProperty =
                RegisterProperty<Int32>(c => c.WorkpackageId);
            /// <summary>
            /// Returns the entity id
            /// </summary>
            public Int32 WorkpackageId
            {
                get { return this.ReadProperty<Int32>(WorkpackageIdProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByWorkpackageIdCriteria(Int32 workackageId)
            {
                this.LoadProperty<Int32>(WorkpackageIdProperty, workackageId);
            }
            #endregion
        }
        #endregion

        #endregion
    }
}
