﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26944 : A.Silva ~ Created.
// V8-27004 : A.Silva ~ Properly implemented.

#endregion

#endregion

using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Server-side implementation of <see cref="PlanogramValidationMetricInfoList"/>.
    /// </summary>
    public partial class PlanogramValidationMetricInfoList
    {
        #region Constructor

        /// <summary>
        ///     Private constructor. Use factory methods to retrieve new instances.
        /// </summary>
        private PlanogramValidationMetricInfoList() {}

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Fetches the <see cref="PlanogramValidationMetricInfoList"/> resulting from the collection of given <paramref name="dtos"/>.
        /// </summary>
        /// <param name="dalContext">Not used.</param>
        /// <param name="dtos">Collection of <see cref="PlanogramValidationMetricInfoDto"/> to use when creating the new instance.</param>
        /// <returns>A new instance of <see cref="PlanogramValidationMetricInfoList"/> containing items created from the given <paramref name="dtos"/>.</returns>
        /// <remarks>CSLA will invoke <see cref="Child_Fetch(IDalContext, IEnumerable{PlanogramValidationMetricInfoDto})"/> on the server via reflection.</remarks>
        internal static PlanogramValidationMetricInfoList Fetch(IDalContext dalContext,
            IEnumerable<PlanogramValidationMetricInfoDto> dtos)
        {
            return DataPortal.FetchChild<PlanogramValidationMetricInfoList>(dalContext, dtos);
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        ///     Invoked by CSLA via reflection when calling <see cref="Fetch(IDalContext, IEnumerable{PlanogramValidationMetricInfoDto})" />.
        /// </summary>
        /// <param name="dalContext">Not used.</param>
        /// <param name="dtos">
        ///     Collection of <see cref="PlanogramValidationMetricInfoDto" /> containing the data to initialize the new
        ///     instance.
        /// </param>
        private void Child_Fetch(IDalContext dalContext, IEnumerable<PlanogramValidationMetricInfoDto> dtos)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            foreach (var dto in dtos)
            {
                this.Add(PlanogramValidationMetricInfo.GetPlanogramValidationMetricInfo(dalContext, dto));
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
