﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// CCM-29010 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramNameTemplateInfo
    {
        #region Constructors
        private PlanogramNameTemplateInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static PlanogramNameTemplateInfo FetchPlanogramNameTemplateInfo(IDalContext dalContext, PlanogramNameTemplateInfoDto dto)
        {
            return DataPortal.FetchChild<PlanogramNameTemplateInfo>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramNameTemplateInfoDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<String>(NameProperty, dto.Name);

        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when loading this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        private void Child_Fetch(IDalContext dalContext, PlanogramNameTemplateInfoDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #endregion
    }
}