﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Denotes the available length unit of measure types
    /// </summary>
    public enum FixtureVolumeUnitOfMeasureType
    {
        Unknown = 0,
        Litres = 1,
        CubicFeet = 2
    }

    /// <summary>
    /// FixtureVolumnUnitOfMeasureType Helper Class
    /// </summary>
    public static class FixtureVolumeUnitOfMeasureTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<FixtureVolumeUnitOfMeasureType, String> FriendlyNames =
            new Dictionary<FixtureVolumeUnitOfMeasureType, String>()
            {
                {FixtureVolumeUnitOfMeasureType.Unknown, String.Empty},
                {FixtureVolumeUnitOfMeasureType.Litres, Message.Enum_FixtureVolumnUnitOfMeasureType_Liters},
                {FixtureVolumeUnitOfMeasureType.CubicFeet, Message.Enum_FixtureVolumnUnitOfMeasureType_CubicFeet},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<FixtureVolumeUnitOfMeasureType, String> Abbreviations =
            new Dictionary<FixtureVolumeUnitOfMeasureType, String>()
            {
                {FixtureVolumeUnitOfMeasureType.Unknown, String.Empty},
                {FixtureVolumeUnitOfMeasureType.Litres, Message.Enum_FixtureVolumnUnitOfMeasureType_Liters_Abbrev},
                {FixtureVolumeUnitOfMeasureType.CubicFeet, Message.Enum_FixtureVolumnUnitOfMeasureType_CubicFeet_Abrev},
            };

        public static FixtureVolumeUnitOfMeasureType Parse(String value)
        {
            return EnumHelper.Parse<FixtureVolumeUnitOfMeasureType>(value, FixtureVolumeUnitOfMeasureType.Unknown);
        }
    }
}
