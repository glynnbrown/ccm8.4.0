﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25919 : L.Ineson
//  Created
#endregion
#region Version History: CCM830
// V8-32357 : M.Brumby
//  [PCR01561] added DisplayName and DisplayDescription
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class WorkflowTaskInfo : ModelReadOnlyObject<WorkflowTaskInfo>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the workflow task id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region SequenceId
        /// <summary>
        /// SequenceId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> SequenceIdProperty =
            RegisterModelProperty<Byte>(c => c.SequenceId);
        /// <summary>
        /// Returns the position in the workflow for the task
        /// </summary>
        public Byte SequenceId
        {
            get { return this.GetProperty<Byte>(SequenceIdProperty); }
        }
        #endregion

        #region Details
        /// <summary>
        /// Details property definition
        /// </summary>
        public static readonly ModelPropertyInfo<EngineTaskInfo> DetailsProperty =
            RegisterModelProperty<EngineTaskInfo>(c => c.Details);
        /// <summary>
        /// Returns the workflow task type details
        /// </summary>
        public EngineTaskInfo Details
        {
            get { return this.GetProperty<EngineTaskInfo>(DetailsProperty); }
        }
        #endregion

        #region User Display Fields

        /// <summary>
        /// DisplayName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DisplayNameProperty =
            RegisterModelProperty<String>(c => c.DisplayName);
        /// <summary>
        /// Returns the DisplayName for the workflow for the task
        /// </summary>
        public String DisplayName
        {
            get { return this.GetProperty<String>(DisplayNameProperty); }
        }

        /// <summary>
        /// Display Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DisplayDescriptionProperty =
            RegisterModelProperty<String>(c => c.DisplayDescription);
        /// <summary>
        /// Returns the Display Description for the workflow for the task
        /// </summary>
        public String DisplayDescription
        {
            get { return this.GetProperty<String>(DisplayDescriptionProperty); }
        }

        #endregion

        #region Helper Properties

        public String VisibleName
        {
            get
            {
                return String.IsNullOrWhiteSpace(DisplayName) ?
                            Details.Name : DisplayName;
            }
        }

        public String VisibleDescription
        {
            get
            {
                return String.IsNullOrWhiteSpace(DisplayDescription) ?
                            Details.Description : DisplayDescription;
            }
        }

        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(WorkflowTaskInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

    }
}
