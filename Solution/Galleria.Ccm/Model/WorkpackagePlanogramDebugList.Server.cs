﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26317 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class WorkpackagePlanogramDebugList
    {
        #region Constructor
        private WorkpackagePlanogramDebugList() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all debug information for the
        /// specified planogram
        /// </summary>
        public static WorkpackagePlanogramDebugList FetchBySourcePlanogramId(Int32 sourcePlanogramId)
        {
            return DataPortal.Fetch<WorkpackagePlanogramDebugList>(new FetchBySourcePlanogramIdCriteria(sourcePlanogramId));
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when fetching debug planograms by the planogram id
        /// </summary>
        protected void DataPortal_Fetch(FetchBySourcePlanogramIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IWorkpackagePlanogramDebugDal dal = dalContext.GetDal<IWorkpackagePlanogramDebugDal>())
                {
                    IEnumerable<WorkpackagePlanogramDebugDto> dtoList = dal.FetchBySourcePlanogramId(criteria.SourcePlanogramId);
                    foreach (WorkpackagePlanogramDebugDto dto in dtoList)
                    {
                        this.Add(WorkpackagePlanogramDebug.GetWorkpackagePlanogramDebug(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when this list is being updated
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                Child_Update(dalContext);
                dalContext.Commit();
            }
        }
        #endregion

        #endregion
    }
}
