﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
// CCM-24979 : L.Hodson
//      Added FixtureLibraryLocation property
//  V8-25395 : A.Probyn
//      Extended for new ProductLibraryLocation & LastUsedProductLibraryLocation
// V8-27154 : L.Luong
//      Added ProductPlacement X,Y,Z
// V8-27239 : I.George
//  Added CanSelectComponents and CanSelectPosition Properties
// V8-27266 : L.Ineson
//  Added folder locations for labels and highlights.
//  Renamed slotlines to divider lines
//  Renamed show chests top down to rotate top down components.
// V8-27694 : L.Ineson
//  Uom properties are now enums.
// V8-27625 : A.Silva
//      Added IsRenumberOnSaveEnabled property, false by default.
// V8-27804 : L.Ineson
//  Added PlanogramImportTemplateLocationProperty
// V8-27938 : N.Haywood
//  Added Data Sheet location and favourites
// V8-28676 : A.Silva
//      Added product image settings.

#endregion

#region Version History: CCM802

// V8-27828 : J.Pickup
//  Introduced IsShowSelectionAsBlinkingEnabled
// V8-29054 : M.Pettit
//  Added Validation warning visibility settings
// V8-28766 : J.Pickup
//  ProductPlacementType refactored from single to Manual. 
// V8-27775 : L.Ineson
//  Added new setting IsDynamicTextScalingEnabled
#endregion
#region Version History: CCM803
// V8-29751 : L.Ineson
//  Added highlight setting.
#endregion
#region Version History: CCM820
// V8-30791 : D.Pleasance
//  Added User Colors
// V8-30738 : L.Ineson
//  Added DefaultPrintTemplateId setting.
// V8-30705 : A.Probyn
//  Added new properties to support showing assortment warnings.
// V8-30936 : M.Brumby
//  Slotwall defaults
// V8-29439 : J.Pickup
//  lots of instant warnings introduced.
#endregion
#region Version History: CCM820
// V8-30791 : D.Pleasance
//  Added User Colors
//V8-30738 : L.Ineson
//  Added DefaultPrintTemplateId setting.
// V8-30932 : L.Ineson
//  Removed CanUserSelectPositions and CanUserSelectComponents.
// V8-30705 : A.Probyn
//  Added new properties to support showing assortment warnings.
// V8-30936 : M.Brumby
//  Slotwall defaults
// V8-29439 : J.Pickup
//  lots of instant warnings introduced.
//V8-30193 : L.Ineson
//  Removed IsRenumberOnSaveEnabled.
// V8-31078 : L.Ineson
//  Added PrintTemplateLocation
#endregion
#region Version History: CCM830
// V8-31546 : M.Pettit
//  Added DefaultExportApolloFileTemplate,DefaultExportJDAFileTemplate,DefaultExportSpacemanFileTemplate
//  Added PlanogramExportFileTemplateLocation
// V8-31699 : A.Heathcote
//  Added "LoadProperty<UserEditorSettingsRecentLabelList>...", "LoadProperty<UserEditorSettingsRecentHighlightList>..." and "LoadProperty<UserEditorSettingsRecentDatasheetList>..." to LoadDataTransferObjects
// V8-31835 : N.Haywood
//  Added Illegal and Legal MetaData and Validation
// V8-31945 : A.Silva
//  Added PlanogramComparisonTemplateLocation
// V8-32085 : N.Haywood
//  Added StatusBarTextFontSize
// V8-32361 : L.Ineson
//  Added default product universe template id.
// V8-32511 : M.Pettit
//  For templates file settings default values are applied. Do not overwrite this if the file value is empty
// V8-32733 : A.Silva
//  Added Default Planogram Comparison Template Id.
// V8-32626 : A.Probyn
//  Added Annotations
// V8-32591 : L.Ineson
//  Added textbox defaults.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Ccm.Model
{
    public partial class UserEditorSettings
    {
        #region Constants

        private const String _displayLanguageCode = "DisplayLanguageCode";
        private const String _isShowSelectionAsBlinkingEnabled = "IsShowSelectionAsBlinkingEnabled";
        private const String _blinkSpeed = "BlinkSpeed";
        private const String _isAutosaveEnabled = "IsAutosaveEnabled";
        private const String _autosaveInterval = "AutosaveInterval";
        private const String _autosaveLocation = "AutosaveLocation";
        private const String _planogramLocation = "PlanogramLocation";
        private const String _fixtureLibraryLocation = "FixtureLibraryLocation";
        private const String _labelLocation = "LabelLocation";
        private const String _highlightLocation = "HighlightLocation";
        private const String _columnLayoutLocation = "ColumnLayoutLocation";
        private const String _printTemplateLocation = "PrintTemplateLocation";
        private const String _planogramComparisonTemplateLocation = "PlanogramComparisonTemplateLocation";
        private const String _dataSheetLayoutLocation = "DataSheetLayoutLocation";
        private const String _dataSheetFavourite1 = "DataSheetFavourite1";
        private const String _dataSheetFavourite2 = "DataSheetFavourite2";
        private const String _dataSheetFavourite3 = "DataSheetFavourite3";
        private const String _dataSheetFavourite4 = "DataSheetFavourite4";
        private const String _dataSheetFavourite5 = "DataSheetFavourite5";
        private const String _validationTemplateLocation = "ValidationTemplateLocation";
        private const String _planogramfileTemplateLocation = "PlanogramFileTemplateLocation";
        private const String _defaultSpacemanFileTemplate = "DefaultSpacemanFileTemplate";
        private const String _defaultApolloFileTemplate = "DefaultApolloFileTemplate";
        private const String _defaultProspaceFileTemplate = "DefaultProspaceFileTemplate";
        private const String _planogramExportFileTemplateLocation = "PlanogramExportFileTemplateLocation";
        private const String _defaultExportSpacemanFileTemplate = "DefaultExportSpacemanFileTemplate";
        private const String _defaultExportApolloFileTemplate = "DefaultExportApolloFileTemplate";
        private const String _defaultExportJDAFileTemplate = "DefaultExportJDAFileTemplate";
        private const String _defaultProductLibrarySource = "DefaultProductLibrarySource";
        private const String _defaultProductLibrarySourceType = "DefaultProductLibrarySourceType";
        private const String _lastUsedProductLibrarySource = "LastUsedProductLibrarySource";
        private const String _imageLocation = "ImageLocation";
        private const String _newPlanViewLayout = "NewPlanViewLayout";
        private const String _openPlanViewLayout = "OpenPlanViewLayout";
        private const String _componentHoverStatusBarText = "ComponentHoverStatusBarText";
        private const String _positionHoverStatusBarText = "PositionHoverStatusBarText";
        private const String _statusBarTextFontSize = "StatusBarTextFontSize";
        private const String _fixtureLabel = "FixtureLabel";
        private const String _productLabel = "ProductLabel";
        private const String _highlight = "Highlight";
        private const String _positionUnits = "PositionUnits";
        private const String _productImages = "ProductImages";
        private const String _productShapes = "ProductShapes";
        private const String _productFillPatterns = "ProductFillPatterns";
        private const String _fixtureImages = "FixtureImages";
        private const String _fixtureFillPatterns = "FixtureFillPatterns";
        private const String _chestWalls = "ChestWalls";
        private const String _rotateTopDownComponents = "RotateTopDownComponents";
        private const String _shelfRisers = "ShelfRisers";
        private const String _notches = "Notches";
        private const String _pegHoles = "PegHoles";
        private const String _pegs = "Pegs";
        private const String _dividerLines = "DividerLines";
        private const String _dividers = "Dividers";
        private const String _annotations = "Annotations";
        private const String _lengthUnitOfMeasure = "LengthUnitOfMeasure";
        private const String _currencyUnitOfMeasure = "CurrencyUnitOfMeasure";
        private const String _volumeUnitOfMeasure = "VolumeUnitOfMeasure";
        private const String _weightUnitOfMeasure = "WeightUnitOfMeasure";
        private const String _productPlacementX = "ProductPlacementX";
        private const String _productPlacementY = "ProductPlacementY";
        private const String _productPlacementZ = "ProductPlacementZ";
        private const String _productHeight = "ProductHeight";
        private const String _productWidth = "ProductWidth";
        private const String _productDepth = "ProductDepth";
        private const String _fixtureHeight = "FixtureHeight";
        private const String _fixtureWidth = "FixtureWidth";
        private const String _fixtureDepth = "FixtureDepth";
        private const String _fixtureHasBackboard = "FixtureHasBackboard";
        private const String _backboardDepth = "BackboardDepth";
        private const String _fixtureHasBase = "FixtureHasBase";
        private const String _baseHeight = "BaseHeight";
        private const String _shelfHeight = "ShelfHeight";
        private const String _shelfWidth = "ShelfWidth";
        private const String _shelfDepth = "ShelfDepth";
        private const String _shelfFillColour = "ShelfFillColour";
        private const String _pegboardHeight = "PegboardHeight";
        private const String _pegboardWidth = "PegboardWidth";
        private const String _pegboardDepth = "PegboardDepth";
        private const String _pegboardFillColour = "PegboardFillColour";
        private const String _chestHeight = "ChestHeight";
        private const String _chestWidth = "ChestWidth";
        private const String _chestDepth = "ChestDepth";
        private const String _chestFillColour = "ChestFillColour";
        private const String _barHeight = "BarHeight";
        private const String _barWidth = "BarWidth";
        private const String _barDepth = "BarDepth";
        private const String _barFillColour = "BarFillColour";
        private const String _rodHeight = "RodHeight";
        private const String _rodWidth = "RodWidth";
        private const String _rodDepth = "RodDepth";
        private const String _rodFillColour = "RodFillColour";
        private const String _clipStripHeight = "ClipStripHeight";
        private const String _clipStripWidth = "ClipstripWidth";
        private const String _clipStripDepth = "ClipStripDepth";
        private const String _clipStripFillColour = "ClipStripFillColour";
        private const String _palletHeight = "PalletHeight";
        private const String _palletWidth = "PalletWidth";
        private const String _palletDepth = "PalletDepth";
        private const String _palletFillColour = "PalletFillColour";
        private const String _slotwallHeight = "SlotwallHeight";
        private const String _slotwallWidth = "SlotwallWidth";
        private const String _slotwallDepth = "SlotwallDepth";
        private const String _slotwallFillColour = "SlotwallFillColour";
        private const String _isLibraryPanelVisible = "IsLibraryPanelVisible";
        private const String _statusBarHeight = "StatusBarHeight";
        private const String _positionProperties = "PositionProperties";
        private const String _componentProperties = "ComponentProperties";
        private const String _productImageSource = "ProductImageSource";
        private const String _productImageLocation = "ProductImageLocation";
        private const String _productImageAttribute = "ProductImageAttribute";
        private const String _productImageIgnoreLeadingZeros = "ProductImageIgnoreLeadingZeros";
        private const String _productImageMinFieldLength = "ProductImageMinFieldLength";
        private const String _productImageFacingPostfixFront = "ProductImageFacingPostfixFront";
        private const String _productImageFacingPostfixLeft = "ProductImageFacingPostfixLeft";
        private const String _productImageFacingPostfixTop = "ProductImageFacingPostfixTop";
        private const String _productImageFacingPostfixBack = "ProductImageFacingPostfixBack";
        private const String _productImageFacingPostfixRight = "ProductImageFacingPostfixRight";
        private const String _productImageFacingPostfixBottom = "ProductImageFacingPostfixBottom";
        private const String _productImageCompression = "ProductImageCompression";
        private const String _productImageColourDepth = "ProductImageColourDepth";
        private const String _productImageTransparency = "ProductImageTransparency";
        private const String _showComponentHasCollisionsWarnings = "ShowComponentHasCollisionsWarnings";
        private const String _showComponentIsOverfilledWarnings = "ShowComponentIsOverfilledWarnings";
        private const String _showComponentIsOutsideOfFixtureAreaWarnings = "ShowComponentIsOutsideOfFixtureAreaWarnings";
        private const String _showComponentSlopeWithNoRiserWarnings = "ShowComponentSlopeWithNoRiserWarnings";
        private const String _showPegIsOverfilledWarnings = "ShowPegIsOverfilledWarnings";
        private const String _showPositionCannotBreakTrayBackWarnings = "ShowPositionCannotBreakTrayBackWarnings";
        private const String _showPositionCannotBreakTrayDownWarnings = "ShowPositionCannotBreakTrayDownWarnings";
        private const String _showPositionCannotBreakTrayTopWarnings = "ShowPositionCannotBreakTrayTopWarnings";
        private const String _showPositionCannotBreakTrayUpWarnings = "ShowPositionCannotBreakTrayUpWarnings";
        private const String _showPositionDoesNotAchieveMinDeepWarnings = "ShowPositionDoesNotAchieveMinDeepWarnings";
        private const String _showPositionExceedsMaxDeepWarnings = "ShowPositionExceedsMaxDeepWarnings";
        private const String _showPositionExceedsMaxRightCapWarnings = "ShowPositionExceedsMaxRightCapWarnings";
        private const String _showPositionExceedsMaxStackWarnings = "ShowPositionExceedsMaxStackWarnings";
        private const String _showPositionExceedsMaxTopCapWarnings = "ShowPositionExceedsMaxTopCapWarnings";
        private const String _showPositionHasCollisionsWarnings = "ShowPositionHasCollisionsWarnings";
        private const String _showPositionHasInvalidMerchStyleSettingsWarnings = "ShowPositionHasInvalidMerchStyleSettingsWarnings";
        private const String _showPositionIsHangingTrayWarnings = "ShowPositionIsHangingTrayWarnings";
        private const String _showPositionIsHangingWithoutPegWarnings = "ShowPositionIsHangingWithoutPegWarnings";
        private const String _showPositionIsOutsideMerchandisingSpaceWarnings = "ShowPositionIsOutsideMerchandisingSpaceWarnings";
        private const String _showProductDoesNotAchieveMinCasesWarnings = "ShowProductDoesNotAchieveMinCasesWarnings";
        private const String _showProductDoesNotAchieveMinDeliveriesWarnings = "ShowProductDoesNotAchieveMinDeliveriesWarnings";
        private const String _showProductDoesNotAchieveMinDosWarnings = "ShowProductDoesNotAchieveMinDosWarnings";
        private const String _showProductDoesNotAchieveMinShelfLifeWarnings = "ShowProductDoesNotAchieveMinShelfLifeWarnings";
        private const String _showProductHasDuplicateGtinWarnings = "ShowProductHasDuplicateGtinWarnings";
        private const String _isDynamicTextScalingEnabled = "IsDynamicTextScalingEnabled";
        private const String _defaultPrintTemplateId = "DefaultPrintTemplateId";
        private const String _defaultProductUniverseTemplateId = "DefaultProductUniverseTemplateId";
        private const String _defaultPlanogramComparisonTemplateId = "DefaultPlanogramComparisonTemplateId";
        private const String _showAssortmentLocalLineRulesWarnings = "ShowAssortmentLocalLineRulesWarnings";
        private const String _showAssortmentDistributionRulesWarnings = "ShowAssortmentDistributionRulesWarnings";
        private const String _showAssortmentProductRulesWarnings = "ShowAssortmentProductRulesWarnings";
        private const String _showAssortmentFamilyRulesWarnings = "ShowAssortmentFamilyRulesWarnings";
        private const String _showAssortmentInheritanceRulesWarnings = "ShowAssortmentInheritanceRulesWarnings";
        private const String _showAssortmentCoreRulesWarnings = "ShowAssortmentCoreRulesWarnings";
        private const String _showLocationProductIllegalWarnings = "ShowLocationProductIllegalWarnings";
        private const String _showLocationProductLegalWarnings = "ShowLocationProductLWarnings";
        private const String _showInstantComponentHasCollisionsWarnings = "ShowInstantComponentHasCollisionsWarnings";
        private const String _showInstantComponentIsOverfilledWarnings = "ShowInstantComponentIsOverfilledWarnings";
        private const String _showInstantComponentSlopeWithNoRiserWarnings = "ShowInstantComponentSlopeWithNoRiserWarnings";
        private const String _showInstantComponentIsOutsideOfFixtureAreaWarnings = "ShowInstantComponentIsOutsideOfFixtureAreaWarnings";
        private const String _showInstantPegIsOverfilledWarnings = "ShowInstantPegIsOverfilledWarnings";
        private const String _showInstantPositionCannotBreakTrayBackWarnings = "ShowInstantPositionCannotBreakTrayBackWarnings";
        private const String _showInstantPositionCannotBreakTrayDownWarnings = "ShowInstantPositionCannotBreakTrayDownWarnings";
        private const String _showInstantPositionCannotBreakTrayTopWarnings = "ShowInstantPositionCannotBreakTrayTopWarnings";
        private const String _showInstantPositionCannotBreakTrayUpWarnings = "ShowInstantPositionCannotBreakTrayUpWarnings";
        private const String _showInstantPositionDoesNotAchieveMinDeepWarnings = "ShowInstantPositionDoesNotAchieveMinDeepWarnings";
        private const String _showInstantPositionExceedsMaxDeepWarnings = "ShowInstantPositionExceedsMaxDeepWarnings";
        private const String _showInstantPositionExceedsMaxRightCapWarnings = "ShowInstantPositionExceedsMaxRightCapWarnings";
        private const String _showInstantPositionExceedsMaxStackWarnings = "ShowInstantPositionExceedsMaxStackWarnings";
        private const String _showInstantPositionExceedsMaxTopCapWarnings = "ShowInstantPositionExceedsMaxTopCapWarnings";
        private const String _showInstantPositionHasCollisionsWarnings = "ShowInstantPositionHasCollisionsWarnings";
        private const String _showInstantPositionHasInvalidMerchStyleSettingsWarnings = "ShowInstantPositionHasInvalidMerchStyleSettingsWarnings";
        private const String _showInstantPositionIsHangingTrayWarnings = "ShowInstantPositionIsHangingTrayWarnings";
        private const String _showInstantPositionIsHangingWithoutPegWarnings = "ShowInstantPositionIsHangingWithoutPegWarnings";
        private const String _showInstantPositionIsOutsideMerchandisingSpaceWarnings = "ShowInstantPositionIsOutsideMerchandisingSpaceWarnings";
        private const String _showInstantProductDoesNotAchieveMinCasesWarnings = "ShowInstantProductDoesNotAchieveMinCasesWarnings";
        private const String _showInstantProductDoesNotAchieveMinDeliveriesWarnings = "ShowInstantProductDoesNotAchieveMinDeliveriesWarnings";
        private const String _showInstantProductDoesNotAchieveMinDosWarnings = "ShowInstantProductDoesNotAchieveMinDosWarnings";
        private const String _showInstantProductDoesNotAchieveMinShelfLifeWarnings = "ShowInstantProductDoesNotAchieveMinShelfLifeWarnings";
        private const String _showInstantProductHasDuplicateGtinWarnings = "ShowInstantProductHasDuplicateGtinWarnings";
        private const String _showInstantAssortmentLocalLineRulesWarnings = "ShowInstantAssortmentLocalLineRulesWarnings";
        private const String _showInstantAssortmentDistributionRulesWarnings = "ShowInstantAssortmentDistributionRulesWarnings";
        private const String _showInstantAssortmentProductRulesWarnings = "ShowInstantAssortmentProductRulesWarnings";
        private const String _showInstantAssortmentFamilyRulesWarnings = "ShowInstantAssortmentFamilyRulesWarnings";
        private const String _showInstantAssortmentInheritanceRulesWarnings = "ShowInstantAssortmentInheritanceRulesWarnings";
        private const String _showInstantAssortmentCoreRulesWarnings = "ShowInstantAssortmentCoreRulesWarnings";
        private const String _showInstantLocationProductIllegalWarnings = "ShowInstantLocationProductIllegalWarnings";
        private const String _showInstantLocationProductLegalWarnings = "ShowInstantLocationProductLWarnings";
        private const String _textBoxFont = "TextBoxFont";
        private const String _textBoxFontSize = "TextBoxFontSize";
        private const String _textBoxCanReduceToFit = "TextBoxCanReduceToFit";
        private const String _textBoxFontColour = "TextBoxFontColour";
        private const String _textBoxBorderColour = "TextBoxBorderColour";
        private const String _textBoxBackgroundColour = "TextBoxBackgroundColour";
        private const String _textBoxBorderThickness = "TextBoxBorderThickness";
        private const String _showPositionPegHolesAreInvalidWarnings = "ShowPositionPegHolesAreInvalidWarnings";
        private const String _showInstantPositionPegHolesAreInvalidWarnings = "ShowInstantPositionPegHolesAreInvalidWarnings";

        #endregion

        #region Constructors
        private UserEditorSettings() { } //force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the UserEditorSettings items for the current user
        /// </summary>
        public static UserEditorSettings FetchUserEditorSettings()
        {
            return DataPortal.Fetch<UserEditorSettings>(new FetchCriteria(Constants.UserDal));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the dal factory with the given name,
        /// </summary>
        protected override IDalFactory GetDalFactory(String dalName)
        {
            LoadProperty<String>(DalFactoryNameProperty, dalName);
            return base.GetDalFactory(dalName);
        }

        /// <summary>
        /// Returns the dal factory
        /// </summary>
        protected override IDalFactory GetDalFactory()
        {
            return base.GetDalFactory(this.DalFactoryName);
        }

        /// <summary>
        /// Method to update or insert config values for the current entity id
        /// as required. This is used by both the child_insert & child_update
        /// </summary>
        private void UpdateConfig(IUserEditorSettingDal dal)
        {
            IEnumerable<UserEditorSettingDto> oldDtoList = dal.FetchAll();

            UpdateConfigItem(dal, oldDtoList, _displayLanguageCode, ReadProperty<String>(DisplayLanguageCodeProperty));
            UpdateConfigItem(dal, oldDtoList, _isShowSelectionAsBlinkingEnabled, ReadProperty<Boolean>(IsShowSelectionAsBlinkingEnabledProperty));
            UpdateConfigItem(dal, oldDtoList, _blinkSpeed, ReadProperty<Int32>(BlinkSpeedProperty));
            UpdateConfigItem(dal, oldDtoList, _isAutosaveEnabled, ReadProperty<Boolean>(IsAutosaveEnabledProperty));
            UpdateConfigItem(dal, oldDtoList, _autosaveInterval, ReadProperty<Int32>(AutosaveIntervalProperty));
            UpdateConfigItem(dal, oldDtoList, _autosaveLocation, ReadProperty<String>(AutosaveLocationProperty));
            UpdateConfigItem(dal, oldDtoList, _planogramLocation, ReadProperty<String>(PlanogramLocationProperty));
            UpdateConfigItem(dal, oldDtoList, _fixtureLibraryLocation, ReadProperty<String>(FixtureLibraryLocationProperty));
            UpdateConfigItem(dal, oldDtoList, _labelLocation, ReadProperty<String>(LabelLocationProperty));
            UpdateConfigItem(dal, oldDtoList, _highlightLocation, ReadProperty<String>(HighlightLocationProperty));
            UpdateConfigItem(dal, oldDtoList, _validationTemplateLocation, ReadProperty<String>(ValidationTemplateLocationProperty));
            UpdateConfigItem(dal, oldDtoList, _planogramfileTemplateLocation, ReadProperty<String>(PlanogramFileTemplateLocationProperty));
            UpdateConfigItem(dal, oldDtoList, _defaultSpacemanFileTemplate, ReadProperty<String>(DefaultSpacemanFileTemplateProperty));
            UpdateConfigItem(dal, oldDtoList, _defaultApolloFileTemplate, ReadProperty<String>(DefaultApolloFileTemplateProperty));
            UpdateConfigItem(dal, oldDtoList, _defaultProspaceFileTemplate, ReadProperty<String>(DefaultProspaceFileTemplateProperty));
            UpdateConfigItem(dal, oldDtoList, _planogramExportFileTemplateLocation, ReadProperty<String>(PlanogramExportFileTemplateLocationProperty));
            UpdateConfigItem(dal, oldDtoList, _defaultExportSpacemanFileTemplate, ReadProperty<String>(DefaultExportSpacemanFileTemplateProperty));
            UpdateConfigItem(dal, oldDtoList, _defaultExportApolloFileTemplate, ReadProperty<String>(DefaultExportApolloFileTemplateProperty));
            UpdateConfigItem(dal, oldDtoList, _defaultExportJDAFileTemplate, ReadProperty<String>(DefaultExportJDAFileTemplateProperty));
            UpdateConfigItem(dal, oldDtoList, _columnLayoutLocation, ReadProperty<String>(ColumnLayoutLocationProperty));
            UpdateConfigItem(dal, oldDtoList, _printTemplateLocation, ReadProperty<String>(PrintTemplateLocationProperty));
            UpdateConfigItem(dal, oldDtoList, _planogramComparisonTemplateLocation, ReadProperty<String>(PlanogramComparisonTemplateLocationProperty));
            UpdateConfigItem(dal, oldDtoList, _dataSheetLayoutLocation, ReadProperty<String>(DataSheetLayoutLocationProperty));
            UpdateConfigItem(dal, oldDtoList, _dataSheetFavourite1, ReadProperty<String>(DataSheetFavourite1Property));
            UpdateConfigItem(dal, oldDtoList, _dataSheetFavourite2, ReadProperty<String>(DataSheetFavourite2Property));
            UpdateConfigItem(dal, oldDtoList, _dataSheetFavourite3, ReadProperty<String>(DataSheetFavourite3Property));
            UpdateConfigItem(dal, oldDtoList, _dataSheetFavourite4, ReadProperty<String>(DataSheetFavourite4Property));
            UpdateConfigItem(dal, oldDtoList, _dataSheetFavourite5, ReadProperty<String>(DataSheetFavourite5Property));
            UpdateConfigItem(dal, oldDtoList, _defaultProductLibrarySource, ReadProperty<String>(DefaultProductLibrarySourceProperty));
            UpdateConfigItem(dal, oldDtoList, _defaultProductLibrarySourceType, ReadProperty<DefaultProductLibrarySourceType>(DefaultProductLibrarySourceTypeProperty));
            UpdateConfigItem(dal, oldDtoList, _defaultProductUniverseTemplateId, ReadProperty<String>(DefaultProductUniverseTemplateIdProperty));
            UpdateConfigItem(dal, oldDtoList, _defaultPlanogramComparisonTemplateId, ReadProperty<String>(DefaultPlanogramComparisonTemplateIdProperty));
            UpdateConfigItem(dal, oldDtoList, _imageLocation, ReadProperty<String>(ImageLocationProperty));
            UpdateConfigItem(dal, oldDtoList, _newPlanViewLayout, ReadProperty<String>(NewPlanViewLayoutProperty));
            UpdateConfigItem(dal, oldDtoList, _componentHoverStatusBarText, ReadProperty<String>(ComponentHoverStatusBarTextProperty));
            UpdateConfigItem(dal, oldDtoList, _positionHoverStatusBarText, ReadProperty<String>(PositionHoverStatusBarTextProperty));
            UpdateConfigItem(dal, oldDtoList, _statusBarTextFontSize, ReadProperty<Byte>(StatusBarTextFontSizeProperty));
            UpdateConfigItem(dal, oldDtoList, _fixtureLabel, ReadProperty<String>(FixtureLabelProperty));
            UpdateConfigItem(dal, oldDtoList, _productLabel, ReadProperty<String>(ProductLabelProperty));
            UpdateConfigItem(dal, oldDtoList, _highlight, ReadProperty<String>(HighlightProperty));
            UpdateConfigItem(dal, oldDtoList, _positionUnits, ReadProperty<Boolean>(PositionUnitsProperty));
            UpdateConfigItem(dal, oldDtoList, _productImages, ReadProperty<Boolean>(ProductImagesProperty));
            UpdateConfigItem(dal, oldDtoList, _productShapes, ReadProperty<Boolean>(ProductShapesProperty));
            UpdateConfigItem(dal, oldDtoList, _productFillPatterns, ReadProperty<Boolean>(ProductFillPatternsProperty));
            UpdateConfigItem(dal, oldDtoList, _fixtureImages, ReadProperty<Boolean>(FixtureImagesProperty));
            UpdateConfigItem(dal, oldDtoList, _fixtureFillPatterns, ReadProperty<Boolean>(FixtureFillPatternsProperty));
            UpdateConfigItem(dal, oldDtoList, _chestWalls, ReadProperty<Boolean>(ChestWallsProperty));
            UpdateConfigItem(dal, oldDtoList, _rotateTopDownComponents, ReadProperty<Boolean>(RotateTopDownComponentsProperty));
            UpdateConfigItem(dal, oldDtoList, _shelfRisers, ReadProperty<Boolean>(ShelfRisersProperty));
            UpdateConfigItem(dal, oldDtoList, _notches, ReadProperty<Boolean>(NotchesProperty));
            UpdateConfigItem(dal, oldDtoList, _pegHoles, ReadProperty<Boolean>(PegHolesProperty));
            UpdateConfigItem(dal, oldDtoList, _pegs, ReadProperty<Boolean>(PegsProperty));
            UpdateConfigItem(dal, oldDtoList, _dividerLines, ReadProperty<Boolean>(DividerLinesProperty));
            UpdateConfigItem(dal, oldDtoList, _dividers, ReadProperty<Boolean>(DividersProperty));
            UpdateConfigItem(dal, oldDtoList, _annotations, ReadProperty<Boolean>(AnnotationsProperty));
            UpdateConfigItem(dal, oldDtoList, _lengthUnitOfMeasure, ReadProperty<PlanogramLengthUnitOfMeasureType>(LengthUnitOfMeasureProperty));
            UpdateConfigItem(dal, oldDtoList, _currencyUnitOfMeasure, ReadProperty<PlanogramCurrencyUnitOfMeasureType>(CurrencyUnitOfMeasureProperty));
            UpdateConfigItem(dal, oldDtoList, _volumeUnitOfMeasure, ReadProperty<PlanogramVolumeUnitOfMeasureType>(VolumeUnitOfMeasureProperty));
            UpdateConfigItem(dal, oldDtoList, _weightUnitOfMeasure, ReadProperty<PlanogramWeightUnitOfMeasureType>(WeightUnitOfMeasureProperty));
            UpdateConfigItem(dal, oldDtoList, _productPlacementX, ReadProperty<PlanogramProductPlacementXType>(ProductPlacementXProperty));
            UpdateConfigItem(dal, oldDtoList, _productPlacementY, ReadProperty<PlanogramProductPlacementYType>(ProductPlacementYProperty));
            UpdateConfigItem(dal, oldDtoList, _productPlacementZ, ReadProperty<PlanogramProductPlacementZType>(ProductPlacementZProperty));
            UpdateConfigItem(dal, oldDtoList, _productHeight, ReadProperty<Single>(ProductHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _productWidth, ReadProperty<Single>(ProductWidthProperty));
            UpdateConfigItem(dal, oldDtoList, _productDepth, ReadProperty<Single>(ProductDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _fixtureHeight, ReadProperty<Single>(FixtureHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _fixtureWidth, ReadProperty<Single>(FixtureWidthProperty));
            UpdateConfigItem(dal, oldDtoList, _fixtureDepth, ReadProperty<Single>(FixtureDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _fixtureHasBackboard, ReadProperty<Boolean>(FixtureHasBackboardProperty));
            UpdateConfigItem(dal, oldDtoList, _backboardDepth, ReadProperty<Single>(BackboardDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _fixtureHasBase, ReadProperty<Boolean>(FixtureHasBaseProperty));
            UpdateConfigItem(dal, oldDtoList, _baseHeight, ReadProperty<Single>(BaseHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _shelfHeight, ReadProperty<Single>(ShelfHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _shelfWidth, ReadProperty<Single>(ShelfWidthProperty));
            UpdateConfigItem(dal, oldDtoList, _shelfDepth, ReadProperty<Single>(ShelfDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _shelfFillColour, ReadProperty<Int32>(ShelfFillColourProperty));
            UpdateConfigItem(dal, oldDtoList, _pegboardHeight, ReadProperty<Single>(PegboardHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _pegboardWidth, ReadProperty<Single>(PegboardWidthProperty));
            UpdateConfigItem(dal, oldDtoList, _pegboardDepth, ReadProperty<Single>(PegboardDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _pegboardFillColour, ReadProperty<Int32>(PegboardFillColourProperty));
            UpdateConfigItem(dal, oldDtoList, _chestHeight, ReadProperty<Single>(ChestHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _chestWidth, ReadProperty<Single>(ChestWidthProperty));
            UpdateConfigItem(dal, oldDtoList, _chestDepth, ReadProperty<Single>(ChestDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _chestFillColour, ReadProperty<Int32>(ChestFillColourProperty));
            UpdateConfigItem(dal, oldDtoList, _barHeight, ReadProperty<Single>(BarHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _barWidth, ReadProperty<Single>(BarWidthProperty));
            UpdateConfigItem(dal, oldDtoList, _barDepth, ReadProperty<Single>(BarDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _barFillColour, ReadProperty<Int32>(BarFillColourProperty));
            UpdateConfigItem(dal, oldDtoList, _rodHeight, ReadProperty<Single>(RodHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _rodWidth, ReadProperty<Single>(RodWidthProperty));
            UpdateConfigItem(dal, oldDtoList, _rodDepth, ReadProperty<Single>(RodDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _rodFillColour, ReadProperty<Int32>(RodFillColourProperty));
            UpdateConfigItem(dal, oldDtoList, _clipStripHeight, ReadProperty<Single>(ClipStripHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _clipStripWidth, ReadProperty<Single>(ClipStripWidthProperty));
            UpdateConfigItem(dal, oldDtoList, _clipStripDepth, ReadProperty<Single>(ClipStripDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _clipStripFillColour, ReadProperty<Int32>(ClipStripFillColourProperty));
            UpdateConfigItem(dal, oldDtoList, _palletHeight, ReadProperty<Single>(PalletHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _palletWidth, ReadProperty<Single>(PalletWidthProperty));
            UpdateConfigItem(dal, oldDtoList, _palletDepth, ReadProperty<Single>(PalletDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _palletFillColour, ReadProperty<Int32>(PalletFillColourProperty));
            UpdateConfigItem(dal, oldDtoList, _slotwallHeight, ReadProperty<Single>(SlotwallHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _slotwallWidth, ReadProperty<Single>(SlotwallWidthProperty));
            UpdateConfigItem(dal, oldDtoList, _slotwallDepth, ReadProperty<Single>(SlotwallDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _slotwallFillColour, ReadProperty<Int32>(SlotwallFillColourProperty));
            UpdateConfigItem(dal, oldDtoList, _isLibraryPanelVisible, ReadProperty<Boolean>(IsLibraryPanelVisibleProperty));
            UpdateConfigItem(dal, oldDtoList, _statusBarHeight, ReadProperty<Int32>(StatusBarHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _positionProperties, ReadProperty<String>(PositionPropertiesProperty));
            UpdateConfigItem(dal, oldDtoList, _componentProperties, ReadProperty<String>(ComponentPropertiesProperty));
            UpdateConfigItem(dal, oldDtoList, _isDynamicTextScalingEnabled, ReadProperty<Boolean>(IsDynamicTextScalingEnabledProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageSource, ReadProperty<RealImageProviderType>(ProductImageSourceProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageLocation, ReadProperty<String>(ProductImageLocationProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageAttribute, ReadProperty<String>(ProductImageAttributeProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageIgnoreLeadingZeros, ReadProperty<Boolean>(ProductImageIgnoreLeadingZerosProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageMinFieldLength, ReadProperty<Byte>(ProductImageMinFieldLengthProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageFacingPostfixFront, ReadProperty<String>(ProductImageFacingPostfixFrontProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageFacingPostfixLeft, ReadProperty<String>(ProductImageFacingPostfixLeftProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageFacingPostfixTop, ReadProperty<String>(ProductImageFacingPostfixTopProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageFacingPostfixBack, ReadProperty<String>(ProductImageFacingPostfixBackProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageFacingPostfixRight, ReadProperty<String>(ProductImageFacingPostfixRightProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageFacingPostfixBottom, ReadProperty<String>(ProductImageFacingPostfixBottomProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageCompression, ReadProperty<RealImageCompressionType>(ProductImageCompressionProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageColourDepth, ReadProperty<RealImageColourDepthType>(ProductImageColourDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageTransparency, ReadProperty<Boolean>(ProductImageTransparencyProperty));
            UpdateConfigItem(dal, oldDtoList, _showComponentHasCollisionsWarnings, ReadProperty<Boolean>(ShowComponentHasCollisionsWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showComponentIsOverfilledWarnings, ReadProperty<Boolean>(ShowComponentIsOverfilledWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showComponentSlopeWithNoRiserWarnings, ReadProperty<Boolean>(ShowComponentSlopeWithNoRiserWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showComponentIsOutsideOfFixtureAreaWarnings, ReadProperty<Boolean>(ShowComponentIsOutsideOfFixtureAreaWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showPegIsOverfilledWarnings, ReadProperty<Boolean>(ShowPegIsOverfilledWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showPositionCannotBreakTrayBackWarnings, ReadProperty<Boolean>(ShowPositionCannotBreakTrayBackWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showPositionCannotBreakTrayDownWarnings, ReadProperty<Boolean>(ShowPositionCannotBreakTrayDownWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showPositionCannotBreakTrayTopWarnings, ReadProperty<Boolean>(ShowPositionCannotBreakTrayTopWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showPositionCannotBreakTrayUpWarnings, ReadProperty<Boolean>(ShowPositionCannotBreakTrayUpWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showPositionDoesNotAchieveMinDeepWarnings, ReadProperty<Boolean>(ShowPositionDoesNotAchieveMinDeepWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showPositionExceedsMaxDeepWarnings, ReadProperty<Boolean>(ShowPositionExceedsMaxDeepWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showPositionExceedsMaxRightCapWarnings, ReadProperty<Boolean>(ShowPositionExceedsMaxRightCapWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showPositionExceedsMaxStackWarnings, ReadProperty<Boolean>(ShowPositionExceedsMaxStackWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showPositionExceedsMaxTopCapWarnings, ReadProperty<Boolean>(ShowPositionExceedsMaxTopCapWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showPositionHasCollisionsWarnings, ReadProperty<Boolean>(ShowPositionHasCollisionsWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showPositionHasInvalidMerchStyleSettingsWarnings, ReadProperty<Boolean>(ShowPositionHasInvalidMerchStyleSettingsWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showPositionIsHangingTrayWarnings, ReadProperty<Boolean>(ShowPositionIsHangingTrayWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showPositionIsHangingWithoutPegWarnings, ReadProperty<Boolean>(ShowPositionIsHangingWithoutPegWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showPositionIsOutsideMerchandisingSpaceWarnings, ReadProperty<Boolean>(ShowPositionIsOutsideMerchandisingSpaceWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showProductDoesNotAchieveMinCasesWarnings, ReadProperty<Boolean>(ShowProductDoesNotAchieveMinCasesWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showProductDoesNotAchieveMinDeliveriesWarnings, ReadProperty<Boolean>(ShowProductDoesNotAchieveMinDeliveriesWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showProductDoesNotAchieveMinDosWarnings, ReadProperty<Boolean>(ShowProductDoesNotAchieveMinDosWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showProductDoesNotAchieveMinShelfLifeWarnings, ReadProperty<Boolean>(ShowProductDoesNotAchieveMinShelfLifeWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showProductHasDuplicateGtinWarnings, ReadProperty<Boolean>(ShowProductHasDuplicateGtinWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _defaultPrintTemplateId, ReadProperty<String>(DefaultPrintTemplateIdProperty));
            UpdateConfigItem(dal, oldDtoList, _showAssortmentLocalLineRulesWarnings, ReadProperty<Boolean>(ShowAssortmentLocalLineRulesWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showAssortmentDistributionRulesWarnings, ReadProperty<Boolean>(ShowAssortmentDistributionRulesWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showAssortmentProductRulesWarnings, ReadProperty<Boolean>(ShowAssortmentProductRulesWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showAssortmentFamilyRulesWarnings, ReadProperty<Boolean>(ShowAssortmentFamilyRulesWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showAssortmentInheritanceRulesWarnings, ReadProperty<Boolean>(ShowAssortmentInheritanceRulesWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showAssortmentCoreRulesWarnings, ReadProperty<Boolean>(ShowAssortmentCoreRulesWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showLocationProductIllegalWarnings, ReadProperty<Boolean>(ShowLocationProductIllegalWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showLocationProductLegalWarnings, ReadProperty<Boolean>(ShowLocationProductLegalWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showPositionPegHolesAreInvalidWarnings, ReadProperty<Boolean>(ShowPositionPegHolesAreInvalidWarningsProperty));

            UpdateConfigItem(dal, oldDtoList, _showInstantComponentHasCollisionsWarnings, ReadProperty<Boolean>(ShowInstantComponentHasCollisionsWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantComponentIsOverfilledWarnings, ReadProperty<Boolean>(ShowInstantComponentIsOverfilledWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantComponentSlopeWithNoRiserWarnings, ReadProperty<Boolean>(ShowInstantComponentSlopeWithNoRiserWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantComponentIsOutsideOfFixtureAreaWarnings, ReadProperty<Boolean>(ShowInstantComponentIsOutsideOfFixtureAreaWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantPegIsOverfilledWarnings, ReadProperty<Boolean>(ShowInstantPegIsOverfilledWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantPositionCannotBreakTrayBackWarnings, ReadProperty<Boolean>(ShowInstantPositionCannotBreakTrayBackWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantPositionCannotBreakTrayDownWarnings, ReadProperty<Boolean>(ShowInstantPositionCannotBreakTrayDownWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantPositionCannotBreakTrayTopWarnings, ReadProperty<Boolean>(ShowInstantPositionCannotBreakTrayTopWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantPositionCannotBreakTrayUpWarnings, ReadProperty<Boolean>(ShowInstantPositionCannotBreakTrayUpWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantPositionDoesNotAchieveMinDeepWarnings, ReadProperty<Boolean>(ShowInstantPositionDoesNotAchieveMinDeepWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantPositionExceedsMaxDeepWarnings, ReadProperty<Boolean>(ShowInstantPositionExceedsMaxDeepWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantPositionExceedsMaxRightCapWarnings, ReadProperty<Boolean>(ShowInstantPositionExceedsMaxRightCapWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantPositionExceedsMaxStackWarnings, ReadProperty<Boolean>(ShowInstantPositionExceedsMaxStackWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantPositionExceedsMaxTopCapWarnings, ReadProperty<Boolean>(ShowInstantPositionExceedsMaxTopCapWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantPositionHasCollisionsWarnings, ReadProperty<Boolean>(ShowInstantPositionHasCollisionsWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantPositionHasInvalidMerchStyleSettingsWarnings, ReadProperty<Boolean>(ShowInstantPositionHasInvalidMerchStyleSettingsWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantPositionIsHangingTrayWarnings, ReadProperty<Boolean>(ShowInstantPositionIsHangingTrayWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantPositionIsHangingWithoutPegWarnings, ReadProperty<Boolean>(ShowInstantPositionIsHangingWithoutPegWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantPositionIsOutsideMerchandisingSpaceWarnings, ReadProperty<Boolean>(ShowInstantPositionIsOutsideMerchandisingSpaceWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantProductDoesNotAchieveMinCasesWarnings, ReadProperty<Boolean>(ShowInstantProductDoesNotAchieveMinCasesWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantProductDoesNotAchieveMinDeliveriesWarnings, ReadProperty<Boolean>(ShowInstantProductDoesNotAchieveMinDeliveriesWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantProductDoesNotAchieveMinDosWarnings, ReadProperty<Boolean>(ShowInstantProductDoesNotAchieveMinDosWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantProductDoesNotAchieveMinShelfLifeWarnings, ReadProperty<Boolean>(ShowInstantProductDoesNotAchieveMinShelfLifeWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantProductHasDuplicateGtinWarnings, ReadProperty<Boolean>(ShowInstantProductHasDuplicateGtinWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantAssortmentLocalLineRulesWarnings, ReadProperty<Boolean>(ShowInstantAssortmentLocalLineRulesWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantAssortmentDistributionRulesWarnings, ReadProperty<Boolean>(ShowInstantAssortmentDistributionRulesWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantAssortmentProductRulesWarnings, ReadProperty<Boolean>(ShowInstantAssortmentProductRulesWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantAssortmentFamilyRulesWarnings, ReadProperty<Boolean>(ShowInstantAssortmentFamilyRulesWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantAssortmentInheritanceRulesWarnings, ReadProperty<Boolean>(ShowInstantAssortmentInheritanceRulesWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantAssortmentCoreRulesWarnings, ReadProperty<Boolean>(ShowInstantAssortmentCoreRulesWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantLocationProductIllegalWarnings, ReadProperty<Boolean>(ShowInstantLocationProductIllegalWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantLocationProductLegalWarnings, ReadProperty<Boolean>(ShowInstantLocationProductLegalWarningsProperty));
            UpdateConfigItem(dal, oldDtoList, _textBoxFont, ReadProperty<String>(TextBoxFontProperty));
            UpdateConfigItem(dal, oldDtoList, _textBoxFontSize, ReadProperty<Single>(TextBoxFontSizeProperty));
            UpdateConfigItem(dal, oldDtoList, _textBoxFontColour, ReadProperty<Int32>(TextBoxFontColourProperty));
            UpdateConfigItem(dal, oldDtoList, _textBoxCanReduceToFit, ReadProperty<Boolean>(TextBoxCanReduceToFitProperty));
            UpdateConfigItem(dal, oldDtoList, _textBoxBorderColour, ReadProperty<Int32>(TextBoxBorderColourProperty));
            UpdateConfigItem(dal, oldDtoList, _textBoxBackgroundColour, ReadProperty<Int32>(TextBoxBackgroundColourProperty));
            UpdateConfigItem(dal, oldDtoList, _textBoxBorderThickness, ReadProperty<Single>(TextBoxBorderThicknessProperty));
            UpdateConfigItem(dal, oldDtoList, _showInstantPositionPegHolesAreInvalidWarnings, ReadProperty<Boolean>(ShowInstantPositionPegHolesAreInvalidWarningsProperty));
            
        }

        /// <summary>
        /// Updates or inserts based on the given key.
        /// </summary>
        private static void UpdateConfigItem(IUserEditorSettingDal dal, IEnumerable<UserEditorSettingDto> oldDtoList, String key, Object value)
        {
            UserEditorSettingDto newDto = GetDataTransferObject(key, value);
            UserEditorSettingDto oldDto = oldDtoList.FirstOrDefault(c => c.Key == key);

            if (oldDto == null)
            {
                dal.Insert(newDto);
            }
            else
            {
                oldDto.Value = newDto.Value;
                dal.Update(oldDto);
            }
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto with the provided details
        /// </summary>
        /// <param name="name">The config value name</param>
        /// <param name="value">The config value</param>
        /// <returns>A new dto</returns>
        private static UserEditorSettingDto GetDataTransferObject(String key, Object value)
        {
            return new UserEditorSettingDto()
            {
                Key = key,
                Value = value.ToString(),
            };
        }

        /// <summary>
        /// Returns a list of dtos for this item.
        /// </summary>
        /// <returns></returns>
        private List<UserEditorSettingDto> GetDataTransferObjects()
        {
            List<UserEditorSettingDto> dtoList = new List<UserEditorSettingDto>();

            dtoList.Add(GetDataTransferObject(_displayLanguageCode, ReadProperty<String>(DisplayLanguageCodeProperty)));
            dtoList.Add(GetDataTransferObject(_isShowSelectionAsBlinkingEnabled, ReadProperty<Boolean>(IsShowSelectionAsBlinkingEnabledProperty)));
            dtoList.Add(GetDataTransferObject(_blinkSpeed, ReadProperty<Int32>(BlinkSpeedProperty)));
            dtoList.Add(GetDataTransferObject(_isAutosaveEnabled, ReadProperty<Boolean>(IsAutosaveEnabledProperty)));
            dtoList.Add(GetDataTransferObject(_autosaveInterval, ReadProperty<Int32>(AutosaveIntervalProperty)));
            dtoList.Add(GetDataTransferObject(_autosaveLocation, ReadProperty<String>(AutosaveLocationProperty)));
            dtoList.Add(GetDataTransferObject(_planogramLocation, ReadProperty<String>(PlanogramLocationProperty)));
            dtoList.Add(GetDataTransferObject(_fixtureLibraryLocation, ReadProperty<String>(FixtureLibraryLocationProperty)));
            dtoList.Add(GetDataTransferObject(_labelLocation, ReadProperty<String>(LabelLocationProperty)));
            dtoList.Add(GetDataTransferObject(_highlightLocation, ReadProperty<String>(HighlightLocationProperty)));
            dtoList.Add(GetDataTransferObject(_columnLayoutLocation, ReadProperty<String>(ColumnLayoutLocationProperty)));
            dtoList.Add(GetDataTransferObject(_printTemplateLocation, ReadProperty<String>(PrintTemplateLocationProperty)));
            dtoList.Add(GetDataTransferObject(_planogramComparisonTemplateLocation, ReadProperty<String>(PlanogramComparisonTemplateLocationProperty)));
            dtoList.Add(GetDataTransferObject(_dataSheetLayoutLocation, ReadProperty<String>(DataSheetLayoutLocationProperty)));
            dtoList.Add(GetDataTransferObject(_dataSheetFavourite1, ReadProperty<String>(DataSheetFavourite1Property)));
            dtoList.Add(GetDataTransferObject(_dataSheetFavourite2, ReadProperty<String>(DataSheetFavourite2Property)));
            dtoList.Add(GetDataTransferObject(_dataSheetFavourite3, ReadProperty<String>(DataSheetFavourite3Property)));
            dtoList.Add(GetDataTransferObject(_dataSheetFavourite4, ReadProperty<String>(DataSheetFavourite4Property)));
            dtoList.Add(GetDataTransferObject(_dataSheetFavourite5, ReadProperty<String>(DataSheetFavourite5Property)));
            dtoList.Add(GetDataTransferObject(_validationTemplateLocation, ReadProperty<String>(ValidationTemplateLocationProperty)));
            dtoList.Add(GetDataTransferObject(_planogramfileTemplateLocation, ReadProperty<String>(PlanogramFileTemplateLocationProperty)));
            dtoList.Add(GetDataTransferObject(_defaultSpacemanFileTemplate, ReadProperty<String>(DefaultSpacemanFileTemplateProperty)));
            dtoList.Add(GetDataTransferObject(_defaultApolloFileTemplate, ReadProperty<String>(DefaultApolloFileTemplateProperty)));
            dtoList.Add(GetDataTransferObject(_defaultProspaceFileTemplate, ReadProperty<String>(DefaultProspaceFileTemplateProperty)));
            dtoList.Add(GetDataTransferObject(_planogramExportFileTemplateLocation, ReadProperty<String>(PlanogramExportFileTemplateLocationProperty)));
            dtoList.Add(GetDataTransferObject(_defaultExportSpacemanFileTemplate, ReadProperty<String>(DefaultExportSpacemanFileTemplateProperty)));
            dtoList.Add(GetDataTransferObject(_defaultExportApolloFileTemplate, ReadProperty<String>(DefaultExportApolloFileTemplateProperty)));
            dtoList.Add(GetDataTransferObject(_defaultExportJDAFileTemplate, ReadProperty<String>(DefaultExportJDAFileTemplateProperty)));

            dtoList.Add(GetDataTransferObject(_defaultProductLibrarySource, ReadProperty<String>(DefaultProductLibrarySourceProperty)));
            dtoList.Add(GetDataTransferObject(_defaultProductLibrarySourceType, ReadProperty<DefaultProductLibrarySourceType>(DefaultProductLibrarySourceTypeProperty)));
            dtoList.Add(GetDataTransferObject(_defaultProductUniverseTemplateId, ReadProperty<String>(DefaultProductUniverseTemplateIdProperty)));
            dtoList.Add(GetDataTransferObject(_defaultPlanogramComparisonTemplateId, ReadProperty<String>(DefaultProductUniverseTemplateIdProperty)));
            
            dtoList.Add(GetDataTransferObject(_imageLocation, ReadProperty<String>(ImageLocationProperty)));
            dtoList.Add(GetDataTransferObject(_newPlanViewLayout, ReadProperty<String>(NewPlanViewLayoutProperty)));
            dtoList.Add(GetDataTransferObject(_componentHoverStatusBarText, ReadProperty<String>(ComponentHoverStatusBarTextProperty)));
            dtoList.Add(GetDataTransferObject(_positionHoverStatusBarText, ReadProperty<String>(PositionHoverStatusBarTextProperty)));
            dtoList.Add(GetDataTransferObject(_statusBarTextFontSize, ReadProperty<Byte>(StatusBarTextFontSizeProperty)));
            dtoList.Add(GetDataTransferObject(_fixtureLabel, ReadProperty<String>(FixtureLabelProperty)));
            dtoList.Add(GetDataTransferObject(_productLabel, ReadProperty<String>(ProductLabelProperty)));
            dtoList.Add(GetDataTransferObject(_highlight, ReadProperty<String>(HighlightProperty)));
            dtoList.Add(GetDataTransferObject(_positionUnits, ReadProperty<Boolean>(PositionUnitsProperty)));
            dtoList.Add(GetDataTransferObject(_productImages, ReadProperty<Boolean>(ProductImagesProperty)));
            dtoList.Add(GetDataTransferObject(_productShapes, ReadProperty<Boolean>(ProductShapesProperty)));
            dtoList.Add(GetDataTransferObject(_productFillPatterns, ReadProperty<Boolean>(ProductFillPatternsProperty)));
            dtoList.Add(GetDataTransferObject(_fixtureImages, ReadProperty<Boolean>(FixtureImagesProperty)));
            dtoList.Add(GetDataTransferObject(_fixtureFillPatterns, ReadProperty<Boolean>(FixtureFillPatternsProperty)));
            dtoList.Add(GetDataTransferObject(_chestWalls, ReadProperty<Boolean>(ChestWallsProperty)));
            dtoList.Add(GetDataTransferObject(_rotateTopDownComponents, ReadProperty<Boolean>(RotateTopDownComponentsProperty)));
            dtoList.Add(GetDataTransferObject(_shelfRisers, ReadProperty<Boolean>(ShelfRisersProperty)));
            dtoList.Add(GetDataTransferObject(_notches, ReadProperty<Boolean>(NotchesProperty)));
            dtoList.Add(GetDataTransferObject(_pegHoles, ReadProperty<Boolean>(PegHolesProperty)));
            dtoList.Add(GetDataTransferObject(_pegs, ReadProperty<Boolean>(PegsProperty)));
            dtoList.Add(GetDataTransferObject(_dividerLines, ReadProperty<Boolean>(DividerLinesProperty)));
            dtoList.Add(GetDataTransferObject(_dividers, ReadProperty<Boolean>(DividersProperty)));
            dtoList.Add(GetDataTransferObject(_annotations, ReadProperty<Boolean>(AnnotationsProperty)));
            dtoList.Add(GetDataTransferObject(_lengthUnitOfMeasure, ReadProperty<PlanogramLengthUnitOfMeasureType>(LengthUnitOfMeasureProperty)));
            dtoList.Add(GetDataTransferObject(_currencyUnitOfMeasure, ReadProperty<PlanogramCurrencyUnitOfMeasureType>(CurrencyUnitOfMeasureProperty)));
            dtoList.Add(GetDataTransferObject(_volumeUnitOfMeasure, ReadProperty<PlanogramVolumeUnitOfMeasureType>(VolumeUnitOfMeasureProperty)));
            dtoList.Add(GetDataTransferObject(_weightUnitOfMeasure, ReadProperty<PlanogramWeightUnitOfMeasureType>(WeightUnitOfMeasureProperty)));
            dtoList.Add(GetDataTransferObject(_productPlacementX, ReadProperty<PlanogramProductPlacementXType>(ProductPlacementXProperty)));
            dtoList.Add(GetDataTransferObject(_productPlacementY, ReadProperty<PlanogramProductPlacementYType>(ProductPlacementYProperty)));
            dtoList.Add(GetDataTransferObject(_productPlacementZ, ReadProperty<PlanogramProductPlacementZType>(ProductPlacementZProperty)));
            dtoList.Add(GetDataTransferObject(_productHeight, ReadProperty<Single>(ProductHeightProperty)));
            dtoList.Add(GetDataTransferObject(_productWidth, ReadProperty<Single>(ProductWidthProperty)));
            dtoList.Add(GetDataTransferObject(_productDepth, ReadProperty<Single>(ProductDepthProperty)));
            dtoList.Add(GetDataTransferObject(_fixtureHeight, ReadProperty<Single>(FixtureHeightProperty)));
            dtoList.Add(GetDataTransferObject(_fixtureWidth, ReadProperty<Single>(FixtureWidthProperty)));
            dtoList.Add(GetDataTransferObject(_fixtureDepth, ReadProperty<Single>(FixtureDepthProperty)));
            dtoList.Add(GetDataTransferObject(_fixtureHasBackboard, ReadProperty<Boolean>(FixtureHasBackboardProperty)));
            dtoList.Add(GetDataTransferObject(_backboardDepth, ReadProperty<Single>(BackboardDepthProperty)));
            dtoList.Add(GetDataTransferObject(_fixtureHasBase, ReadProperty<Boolean>(FixtureHasBaseProperty)));
            dtoList.Add(GetDataTransferObject(_baseHeight, ReadProperty<Single>(BaseHeightProperty)));
            dtoList.Add(GetDataTransferObject(_shelfHeight, ReadProperty<Single>(ShelfHeightProperty)));
            dtoList.Add(GetDataTransferObject(_shelfWidth, ReadProperty<Single>(ShelfWidthProperty)));
            dtoList.Add(GetDataTransferObject(_shelfDepth, ReadProperty<Single>(ShelfDepthProperty)));
            dtoList.Add(GetDataTransferObject(_shelfFillColour, ReadProperty<Int32>(ShelfFillColourProperty)));
            dtoList.Add(GetDataTransferObject(_pegboardHeight, ReadProperty<Single>(PegboardHeightProperty)));
            dtoList.Add(GetDataTransferObject(_pegboardWidth, ReadProperty<Single>(PegboardWidthProperty)));
            dtoList.Add(GetDataTransferObject(_pegboardDepth, ReadProperty<Single>(PegboardDepthProperty)));
            dtoList.Add(GetDataTransferObject(_pegboardFillColour, ReadProperty<Int32>(PegboardFillColourProperty)));
            dtoList.Add(GetDataTransferObject(_chestHeight, ReadProperty<Single>(ChestHeightProperty)));
            dtoList.Add(GetDataTransferObject(_chestWidth, ReadProperty<Single>(ChestWidthProperty)));
            dtoList.Add(GetDataTransferObject(_chestDepth, ReadProperty<Single>(ChestDepthProperty)));
            dtoList.Add(GetDataTransferObject(_chestFillColour, ReadProperty<Int32>(ChestFillColourProperty)));
            dtoList.Add(GetDataTransferObject(_barHeight, ReadProperty<Single>(BarHeightProperty)));
            dtoList.Add(GetDataTransferObject(_barWidth, ReadProperty<Single>(BarWidthProperty)));
            dtoList.Add(GetDataTransferObject(_barDepth, ReadProperty<Single>(BarDepthProperty)));
            dtoList.Add(GetDataTransferObject(_barFillColour, ReadProperty<Int32>(BarFillColourProperty)));
            dtoList.Add(GetDataTransferObject(_rodHeight, ReadProperty<Single>(RodHeightProperty)));
            dtoList.Add(GetDataTransferObject(_rodWidth, ReadProperty<Single>(RodWidthProperty)));
            dtoList.Add(GetDataTransferObject(_rodDepth, ReadProperty<Single>(RodDepthProperty)));
            dtoList.Add(GetDataTransferObject(_rodFillColour, ReadProperty<Int32>(RodFillColourProperty)));
            dtoList.Add(GetDataTransferObject(_clipStripHeight, ReadProperty<Single>(ClipStripHeightProperty)));
            dtoList.Add(GetDataTransferObject(_clipStripWidth, ReadProperty<Single>(ClipStripWidthProperty)));
            dtoList.Add(GetDataTransferObject(_clipStripDepth, ReadProperty<Single>(ClipStripDepthProperty)));
            dtoList.Add(GetDataTransferObject(_clipStripFillColour, ReadProperty<Int32>(ClipStripFillColourProperty)));
            dtoList.Add(GetDataTransferObject(_palletHeight, ReadProperty<Single>(PalletHeightProperty)));
            dtoList.Add(GetDataTransferObject(_palletWidth, ReadProperty<Single>(PalletWidthProperty)));
            dtoList.Add(GetDataTransferObject(_palletDepth, ReadProperty<Single>(PalletDepthProperty)));
            dtoList.Add(GetDataTransferObject(_palletFillColour, ReadProperty<Int32>(PalletFillColourProperty)));
            dtoList.Add(GetDataTransferObject(_slotwallHeight, ReadProperty<Single>(SlotwallHeightProperty)));
            dtoList.Add(GetDataTransferObject(_slotwallWidth, ReadProperty<Single>(SlotwallWidthProperty)));
            dtoList.Add(GetDataTransferObject(_slotwallDepth, ReadProperty<Single>(SlotwallDepthProperty)));
            dtoList.Add(GetDataTransferObject(_slotwallFillColour, ReadProperty<Int32>(SlotwallFillColourProperty)));
            dtoList.Add(GetDataTransferObject(_isLibraryPanelVisible, ReadProperty<Boolean>(IsLibraryPanelVisibleProperty)));
            dtoList.Add(GetDataTransferObject(_statusBarHeight, ReadProperty<Int32>(StatusBarHeightProperty)));
            dtoList.Add(GetDataTransferObject(_positionProperties, ReadProperty<String>(PositionPropertiesProperty)));
            dtoList.Add(GetDataTransferObject(_componentProperties, ReadProperty<String>(ComponentPropertiesProperty)));
            dtoList.Add(GetDataTransferObject(_productImageSource, ReadProperty<RealImageProviderType>(ProductImageSourceProperty)));
            dtoList.Add(GetDataTransferObject(_productImageLocation, ReadProperty<String>(ProductImageLocationProperty)));
            dtoList.Add(GetDataTransferObject(_productImageAttribute, ReadProperty<String>(ProductImageAttributeProperty)));
            dtoList.Add(GetDataTransferObject(_productImageIgnoreLeadingZeros, ReadProperty<Boolean>(ProductImageIgnoreLeadingZerosProperty)));
            dtoList.Add(GetDataTransferObject(_productImageMinFieldLength, ReadProperty<Byte>(ProductImageMinFieldLengthProperty)));
            dtoList.Add(GetDataTransferObject(_productImageFacingPostfixFront, ReadProperty<String>(ProductImageFacingPostfixFrontProperty)));
            dtoList.Add(GetDataTransferObject(_productImageFacingPostfixLeft, ReadProperty<String>(ProductImageFacingPostfixLeftProperty)));
            dtoList.Add(GetDataTransferObject(_productImageFacingPostfixTop, ReadProperty<String>(ProductImageFacingPostfixTopProperty)));
            dtoList.Add(GetDataTransferObject(_productImageFacingPostfixBack, ReadProperty<String>(ProductImageFacingPostfixBackProperty)));
            dtoList.Add(GetDataTransferObject(_productImageFacingPostfixRight, ReadProperty<String>(ProductImageFacingPostfixRightProperty)));
            dtoList.Add(GetDataTransferObject(_productImageFacingPostfixBottom, ReadProperty<String>(ProductImageFacingPostfixBottomProperty)));
            dtoList.Add(GetDataTransferObject(_productImageCompression, ReadProperty<RealImageCompressionType>(ProductImageCompressionProperty)));
            dtoList.Add(GetDataTransferObject(_productImageColourDepth, ReadProperty<RealImageColourDepthType>(ProductImageColourDepthProperty)));
            dtoList.Add(GetDataTransferObject(_productImageTransparency, ReadProperty<Boolean>(ProductImageTransparencyProperty)));
            dtoList.Add(GetDataTransferObject(_showComponentHasCollisionsWarnings, ReadProperty<Boolean>(ShowComponentHasCollisionsWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showComponentIsOverfilledWarnings, ReadProperty<Boolean>(ShowComponentIsOverfilledWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showComponentSlopeWithNoRiserWarnings, ReadProperty<Boolean>(ShowComponentSlopeWithNoRiserWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showComponentIsOutsideOfFixtureAreaWarnings, ReadProperty<Boolean>(ShowComponentIsOutsideOfFixtureAreaWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showPegIsOverfilledWarnings, ReadProperty<Boolean>(ShowPegIsOverfilledWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showPositionCannotBreakTrayBackWarnings, ReadProperty<Boolean>(ShowPositionCannotBreakTrayBackWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showPositionCannotBreakTrayDownWarnings, ReadProperty<Boolean>(ShowPositionCannotBreakTrayDownWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showPositionCannotBreakTrayTopWarnings, ReadProperty<Boolean>(ShowPositionCannotBreakTrayTopWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showPositionCannotBreakTrayUpWarnings, ReadProperty<Boolean>(ShowPositionCannotBreakTrayUpWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showPositionDoesNotAchieveMinDeepWarnings, ReadProperty<Boolean>(ShowPositionDoesNotAchieveMinDeepWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showPositionExceedsMaxDeepWarnings, ReadProperty<Boolean>(ShowPositionExceedsMaxDeepWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showPositionExceedsMaxRightCapWarnings, ReadProperty<Boolean>(ShowPositionExceedsMaxRightCapWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showPositionExceedsMaxStackWarnings, ReadProperty<Boolean>(ShowPositionExceedsMaxStackWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showPositionExceedsMaxTopCapWarnings, ReadProperty<Boolean>(ShowPositionExceedsMaxTopCapWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showPositionHasCollisionsWarnings, ReadProperty<Boolean>(ShowPositionHasCollisionsWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showPositionHasInvalidMerchStyleSettingsWarnings, ReadProperty<Boolean>(ShowPositionHasInvalidMerchStyleSettingsWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showPositionIsHangingTrayWarnings, ReadProperty<Boolean>(ShowPositionIsHangingTrayWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showPositionIsHangingWithoutPegWarnings, ReadProperty<Boolean>(ShowPositionIsHangingWithoutPegWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showPositionIsOutsideMerchandisingSpaceWarnings, ReadProperty<Boolean>(ShowPositionIsOutsideMerchandisingSpaceWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showProductDoesNotAchieveMinCasesWarnings, ReadProperty<Boolean>(ShowProductDoesNotAchieveMinCasesWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showProductDoesNotAchieveMinDeliveriesWarnings, ReadProperty<Boolean>(ShowProductDoesNotAchieveMinDeliveriesWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showProductDoesNotAchieveMinDosWarnings, ReadProperty<Boolean>(ShowProductDoesNotAchieveMinDosWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showProductDoesNotAchieveMinShelfLifeWarnings, ReadProperty<Boolean>(ShowProductDoesNotAchieveMinShelfLifeWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showProductHasDuplicateGtinWarnings, ReadProperty<Boolean>(ShowProductHasDuplicateGtinWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_isDynamicTextScalingEnabled, ReadProperty<Boolean>(IsDynamicTextScalingEnabledProperty)));
            dtoList.Add(GetDataTransferObject(_defaultPrintTemplateId, ReadProperty<String>(DefaultPrintTemplateIdProperty)));
            dtoList.Add(GetDataTransferObject(_showAssortmentLocalLineRulesWarnings, ReadProperty<Boolean>(ShowAssortmentLocalLineRulesWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showAssortmentDistributionRulesWarnings, ReadProperty<Boolean>(ShowAssortmentDistributionRulesWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showAssortmentProductRulesWarnings, ReadProperty<Boolean>(ShowAssortmentProductRulesWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showAssortmentFamilyRulesWarnings, ReadProperty<Boolean>(ShowAssortmentFamilyRulesWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showAssortmentInheritanceRulesWarnings, ReadProperty<Boolean>(ShowAssortmentInheritanceRulesWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showAssortmentCoreRulesWarnings, ReadProperty<Boolean>(ShowAssortmentCoreRulesWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showLocationProductIllegalWarnings, ReadProperty<Boolean>(ShowLocationProductIllegalWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showLocationProductLegalWarnings, ReadProperty<Boolean>(ShowLocationProductLegalWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showPositionPegHolesAreInvalidWarnings, ReadProperty<Boolean>(ShowPositionPegHolesAreInvalidWarningsProperty)));

            dtoList.Add(GetDataTransferObject(_showInstantComponentHasCollisionsWarnings, ReadProperty<Boolean>(ShowInstantComponentHasCollisionsWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantComponentIsOverfilledWarnings, ReadProperty<Boolean>(ShowInstantComponentIsOverfilledWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantComponentSlopeWithNoRiserWarnings, ReadProperty<Boolean>(ShowInstantComponentSlopeWithNoRiserWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantComponentIsOutsideOfFixtureAreaWarnings, ReadProperty<Boolean>(ShowInstantComponentIsOutsideOfFixtureAreaWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantPegIsOverfilledWarnings, ReadProperty<Boolean>(ShowInstantPegIsOverfilledWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantPositionCannotBreakTrayBackWarnings, ReadProperty<Boolean>(ShowInstantPositionCannotBreakTrayBackWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantPositionCannotBreakTrayDownWarnings, ReadProperty<Boolean>(ShowInstantPositionCannotBreakTrayDownWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantPositionCannotBreakTrayTopWarnings, ReadProperty<Boolean>(ShowInstantPositionCannotBreakTrayTopWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantPositionCannotBreakTrayUpWarnings, ReadProperty<Boolean>(ShowInstantPositionCannotBreakTrayUpWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantPositionDoesNotAchieveMinDeepWarnings, ReadProperty<Boolean>(ShowInstantPositionDoesNotAchieveMinDeepWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantPositionExceedsMaxDeepWarnings, ReadProperty<Boolean>(ShowInstantPositionExceedsMaxDeepWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantPositionExceedsMaxRightCapWarnings, ReadProperty<Boolean>(ShowInstantPositionExceedsMaxRightCapWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantPositionExceedsMaxStackWarnings, ReadProperty<Boolean>(ShowInstantPositionExceedsMaxStackWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantPositionExceedsMaxTopCapWarnings, ReadProperty<Boolean>(ShowInstantPositionExceedsMaxTopCapWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantPositionHasCollisionsWarnings, ReadProperty<Boolean>(ShowInstantPositionHasCollisionsWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantPositionHasInvalidMerchStyleSettingsWarnings, ReadProperty<Boolean>(ShowInstantPositionHasInvalidMerchStyleSettingsWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantPositionIsHangingTrayWarnings, ReadProperty<Boolean>(ShowInstantPositionIsHangingTrayWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantPositionIsHangingWithoutPegWarnings, ReadProperty<Boolean>(ShowInstantPositionIsHangingWithoutPegWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantPositionIsOutsideMerchandisingSpaceWarnings, ReadProperty<Boolean>(ShowInstantPositionIsOutsideMerchandisingSpaceWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantProductDoesNotAchieveMinCasesWarnings, ReadProperty<Boolean>(ShowInstantProductDoesNotAchieveMinCasesWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantProductDoesNotAchieveMinDeliveriesWarnings, ReadProperty<Boolean>(ShowInstantProductDoesNotAchieveMinDeliveriesWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantProductDoesNotAchieveMinDosWarnings, ReadProperty<Boolean>(ShowInstantProductDoesNotAchieveMinDosWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantProductDoesNotAchieveMinShelfLifeWarnings, ReadProperty<Boolean>(ShowInstantProductDoesNotAchieveMinShelfLifeWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantProductHasDuplicateGtinWarnings, ReadProperty<Boolean>(ShowInstantProductHasDuplicateGtinWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantAssortmentLocalLineRulesWarnings, ReadProperty<Boolean>(ShowInstantAssortmentLocalLineRulesWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantAssortmentDistributionRulesWarnings, ReadProperty<Boolean>(ShowInstantAssortmentDistributionRulesWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantAssortmentProductRulesWarnings, ReadProperty<Boolean>(ShowInstantAssortmentProductRulesWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantAssortmentFamilyRulesWarnings, ReadProperty<Boolean>(ShowInstantAssortmentFamilyRulesWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantAssortmentInheritanceRulesWarnings, ReadProperty<Boolean>(ShowInstantAssortmentInheritanceRulesWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantAssortmentCoreRulesWarnings, ReadProperty<Boolean>(ShowInstantAssortmentCoreRulesWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantLocationProductIllegalWarnings, ReadProperty<Boolean>(ShowInstantLocationProductIllegalWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantLocationProductLegalWarnings, ReadProperty<Boolean>(ShowInstantLocationProductLegalWarningsProperty)));
            dtoList.Add(GetDataTransferObject(_textBoxFont, ReadProperty<String>(TextBoxFontProperty)));
            dtoList.Add(GetDataTransferObject(_textBoxFontSize, ReadProperty<Single>(TextBoxFontSizeProperty)));
            dtoList.Add(GetDataTransferObject(_textBoxFontColour, ReadProperty<Int32>(TextBoxFontColourProperty)));
            dtoList.Add(GetDataTransferObject(_textBoxCanReduceToFit, ReadProperty<Boolean>(TextBoxCanReduceToFitProperty)));
            dtoList.Add(GetDataTransferObject(_textBoxBorderColour, ReadProperty<Int32>(TextBoxBorderColourProperty)));
            dtoList.Add(GetDataTransferObject(_textBoxBackgroundColour, ReadProperty<Int32>(TextBoxBackgroundColourProperty)));
            dtoList.Add(GetDataTransferObject(_textBoxBorderThickness, ReadProperty<Single>(TextBoxBorderThicknessProperty)));
            dtoList.Add(GetDataTransferObject(_showInstantPositionPegHolesAreInvalidWarnings, ReadProperty<Boolean>(ShowInstantPositionPegHolesAreInvalidWarningsProperty)));

            return dtoList;
        }

        /// <summary>
        /// Loads a list of dtos into this object
        /// </summary>
        /// <param name="dtoList">The dto list to load</param>
        private void LoadDataTransferObjects(IDalContext dalContext, IEnumerable<UserEditorSettingDto> dtoList)
        {
            foreach (UserEditorSettingDto dto in dtoList)
            {
                try
                {
                    switch (dto.Key)
                    {
                        case _displayLanguageCode: LoadProperty<String>(DisplayLanguageCodeProperty, dto.Value); break;
                        case _isShowSelectionAsBlinkingEnabled: LoadProperty<Boolean>(IsShowSelectionAsBlinkingEnabledProperty, Boolean.Parse(dto.Value)); break;
                        case _blinkSpeed: LoadProperty<Int32>(BlinkSpeedProperty, Convert.ToInt32(dto.Value)); break;
                        case _isAutosaveEnabled: LoadProperty<Boolean>(IsAutosaveEnabledProperty, Boolean.Parse(dto.Value)); break;
                        case _autosaveInterval: LoadProperty<Int32>(AutosaveIntervalProperty, Int32.Parse(dto.Value)); break;
                        case _autosaveLocation: LoadProperty<String>(AutosaveLocationProperty, dto.Value); break;
                        case _planogramLocation: LoadProperty<String>(PlanogramLocationProperty, dto.Value); break;
                        case _fixtureLibraryLocation: LoadProperty<String>(FixtureLibraryLocationProperty, dto.Value); break;
                        case _labelLocation: LoadProperty<String>(LabelLocationProperty, dto.Value); break;
                        case _highlightLocation: LoadProperty<String>(HighlightLocationProperty, dto.Value); break;
                        case _columnLayoutLocation: LoadProperty<String>(ColumnLayoutLocationProperty, dto.Value); break;
                        case _printTemplateLocation: LoadProperty<String>(PrintTemplateLocationProperty, dto.Value); break;
                        case _planogramComparisonTemplateLocation: LoadProperty<String>(PlanogramComparisonTemplateLocationProperty, dto.Value); break;
                        case _dataSheetLayoutLocation: LoadProperty<String>(DataSheetLayoutLocationProperty, dto.Value); break;
                        case _dataSheetFavourite1: LoadProperty<String>(DataSheetFavourite1Property, dto.Value); break;
                        case _dataSheetFavourite2: LoadProperty<String>(DataSheetFavourite2Property, dto.Value); break;
                        case _dataSheetFavourite3: LoadProperty<String>(DataSheetFavourite3Property, dto.Value); break;
                        case _dataSheetFavourite4: LoadProperty<String>(DataSheetFavourite4Property, dto.Value); break;
                        case _dataSheetFavourite5: LoadProperty<String>(DataSheetFavourite5Property, dto.Value); break;
                        case _validationTemplateLocation: LoadProperty<String>(ValidationTemplateLocationProperty, dto.Value); break;
                        

                        //V8-32511 : For templates file settings default values are applied. Do not overwrite this if the file value is empty
                        case _planogramfileTemplateLocation:
                            //Apply the default if value not set
                            if (!String.IsNullOrEmpty(dto.Value))
                            {
                                LoadProperty<String>(PlanogramFileTemplateLocationProperty, dto.Value);
                            }
                            //LoadProperty<String>(PlanogramFileTemplateLocationProperty, dto.Value);
                            break;

                        case _planogramExportFileTemplateLocation:
                            //Apply the default if value not set
                            if (!String.IsNullOrEmpty(dto.Value))
                            {
                                LoadProperty<String>(PlanogramExportFileTemplateLocationProperty, dto.Value);
                            }
                            //LoadProperty<String>(PlanogramExportFileTemplateLocationProperty, dto.Value);
                            break;

                        case _defaultSpacemanFileTemplate:
                            //Apply the default if value not set
                            if (!String.IsNullOrEmpty(dto.Value))
                            {
                                LoadProperty<String>(DefaultSpacemanFileTemplateProperty, dto.Value);
                            }
                            //LoadProperty<String>(DefaultSpacemanFileTemplateProperty, dto.Value);
                            break;
                        
                        case _defaultApolloFileTemplate:
                            //Apply the default if value not set
                            if (!String.IsNullOrEmpty(dto.Value))
                            {
                                LoadProperty<String>(DefaultApolloFileTemplateProperty, dto.Value); 
                            }
                            //LoadProperty<String>(DefaultApolloFileTemplateProperty, dto.Value); 
                            break;
                            
                        case _defaultProspaceFileTemplate:
                            //Apply the default if value not set
                            if (!String.IsNullOrEmpty(dto.Value))
                            {
                                LoadProperty<String>(DefaultProspaceFileTemplateProperty, dto.Value);
                            }
                            //LoadProperty<String>(DefaultProspaceFileTemplateProperty, dto.Value);
                            break;

                        case _defaultExportSpacemanFileTemplate:
                            //Apply the default if value not set
                            if (!String.IsNullOrEmpty(dto.Value))
                            {
                                LoadProperty<String>(DefaultExportSpacemanFileTemplateProperty, dto.Value);
                            }
                            //LoadProperty<String>(DefaultExportSpacemanFileTemplateProperty, dto.Value);
                            break;

                        case _defaultExportApolloFileTemplate:
                            //Apply the default if value not set
                            if (!String.IsNullOrEmpty(dto.Value))
                            {
                                LoadProperty<String>(DefaultExportApolloFileTemplateProperty, dto.Value);
                            }
                            //LoadProperty<String>(DefaultExportApolloFileTemplateProperty, dto.Value);
                            break;

                        case _defaultExportJDAFileTemplate:
                            //Apply the default if value not set
                            if (!String.IsNullOrEmpty(dto.Value))
                            {
                                LoadProperty<String>(DefaultExportJDAFileTemplateProperty, dto.Value);
                            }
                            //LoadProperty<String>(DefaultExportJDAFileTemplateProperty, dto.Value);
                            break;

                        case _defaultProductLibrarySource: LoadProperty<String>(DefaultProductLibrarySourceProperty, dto.Value); break;
                        case _defaultProductLibrarySourceType: LoadProperty<DefaultProductLibrarySourceType>(DefaultProductLibrarySourceTypeProperty, EnumHelper.Parse(dto.Value, DefaultProductLibrarySourceType.ProductLibrary)); break;
                        case _defaultProductUniverseTemplateId: LoadProperty<String>(DefaultProductUniverseTemplateIdProperty, dto.Value); break;
                        case _defaultPlanogramComparisonTemplateId: LoadProperty<String>(DefaultPlanogramComparisonTemplateIdProperty, dto.Value); break;
                        case _imageLocation: LoadProperty<String>(ImageLocationProperty, dto.Value); break;
                        case _newPlanViewLayout: LoadProperty<String>(NewPlanViewLayoutProperty, dto.Value); break;
                        case _componentHoverStatusBarText: LoadProperty<String>(ComponentHoverStatusBarTextProperty, dto.Value); break;
                        case _positionHoverStatusBarText: LoadProperty<String>(PositionHoverStatusBarTextProperty, dto.Value); break;
                        case _statusBarTextFontSize: LoadProperty<Byte>(StatusBarTextFontSizeProperty, Convert.ToByte(dto.Value)); break;
                        case _fixtureLabel: LoadProperty<String>(FixtureLabelProperty, dto.Value); break;
                        case _productLabel: LoadProperty<String>(ProductLabelProperty, dto.Value); break;
                        case _highlight: LoadProperty<String>(HighlightProperty, dto.Value); break;
                        case _positionUnits: LoadProperty<Boolean>(PositionUnitsProperty, Boolean.Parse(dto.Value)); break;
                        case _productImages: LoadProperty<Boolean>(ProductImagesProperty, Boolean.Parse(dto.Value)); break;
                        case _productShapes: LoadProperty<Boolean>(ProductShapesProperty, Boolean.Parse(dto.Value)); break;
                        case _productFillPatterns: LoadProperty<Boolean>(ProductFillPatternsProperty, Boolean.Parse(dto.Value)); break;
                        case _fixtureImages: LoadProperty<Boolean>(FixtureImagesProperty, Boolean.Parse(dto.Value)); break;
                        case _fixtureFillPatterns: LoadProperty<Boolean>(FixtureFillPatternsProperty, Boolean.Parse(dto.Value)); break;
                        case _chestWalls: LoadProperty<Boolean>(ChestWallsProperty, Boolean.Parse(dto.Value)); break;
                        case _rotateTopDownComponents: LoadProperty<Boolean>(RotateTopDownComponentsProperty, Boolean.Parse(dto.Value)); break;
                        case _shelfRisers: LoadProperty<Boolean>(ShelfRisersProperty, Boolean.Parse(dto.Value)); break;
                        case _notches: LoadProperty<Boolean>(NotchesProperty, Boolean.Parse(dto.Value)); break;
                        case _pegHoles: LoadProperty<Boolean>(PegHolesProperty, Boolean.Parse(dto.Value)); break;
                        case _pegs: LoadProperty<Boolean>(PegsProperty, Boolean.Parse(dto.Value)); break;
                        case _dividerLines: LoadProperty<Boolean>(DividerLinesProperty, Boolean.Parse(dto.Value)); break;
                        case _dividers: LoadProperty<Boolean>(DividersProperty, Boolean.Parse(dto.Value)); break;
                        case _annotations: LoadProperty<Boolean>(AnnotationsProperty, Boolean.Parse(dto.Value)); break;
                        case _lengthUnitOfMeasure: LoadProperty<PlanogramLengthUnitOfMeasureType>(LengthUnitOfMeasureProperty, EnumHelper.Parse(dto.Value, PlanogramLengthUnitOfMeasureType.Centimeters)); break;
                        case _currencyUnitOfMeasure: LoadProperty<PlanogramCurrencyUnitOfMeasureType>(CurrencyUnitOfMeasureProperty,EnumHelper.Parse(dto.Value,PlanogramCurrencyUnitOfMeasureType.GBP)); break;
                        case _volumeUnitOfMeasure: LoadProperty<PlanogramVolumeUnitOfMeasureType>(VolumeUnitOfMeasureProperty, EnumHelper.Parse(dto.Value,PlanogramVolumeUnitOfMeasureType.Litres)); break;
                        case _weightUnitOfMeasure: LoadProperty<PlanogramWeightUnitOfMeasureType>(WeightUnitOfMeasureProperty, EnumHelper.Parse(dto.Value,PlanogramWeightUnitOfMeasureType.Kilograms)); break;
                        case _productPlacementX: LoadProperty<PlanogramProductPlacementXType>(ProductPlacementXProperty, EnumHelper.Parse(dto.Value, PlanogramProductPlacementXType.Manual)); break;
                        case _productPlacementY: LoadProperty<PlanogramProductPlacementYType>(ProductPlacementYProperty, EnumHelper.Parse(dto.Value, PlanogramProductPlacementYType.Manual)); break;
                        case _productPlacementZ: LoadProperty<PlanogramProductPlacementZType>(ProductPlacementZProperty, EnumHelper.Parse(dto.Value, PlanogramProductPlacementZType.Manual)); break;
                        case _productHeight: LoadProperty<Single>(ProductHeightProperty, Single.Parse(dto.Value)); break;
                        case _productWidth: LoadProperty<Single>(ProductWidthProperty, Single.Parse(dto.Value)); break;
                        case _productDepth: LoadProperty<Single>(ProductDepthProperty, Single.Parse(dto.Value)); break;
                        case _fixtureHeight: LoadProperty<Single>(FixtureHeightProperty, Single.Parse(dto.Value)); break;
                        case _fixtureWidth: LoadProperty<Single>(FixtureWidthProperty, Single.Parse(dto.Value)); break;
                        case _fixtureDepth: LoadProperty<Single>(FixtureDepthProperty, Single.Parse(dto.Value)); break;
                        case _fixtureHasBackboard: LoadProperty<Boolean>(FixtureHasBackboardProperty, Boolean.Parse(dto.Value)); break;
                        case _backboardDepth: LoadProperty<Single>(BackboardDepthProperty, Single.Parse(dto.Value)); break;
                        case _fixtureHasBase: LoadProperty<Boolean>(FixtureHasBaseProperty, Boolean.Parse(dto.Value)); break;
                        case _baseHeight: LoadProperty<Single>(BaseHeightProperty, Single.Parse(dto.Value)); break;
                        case _shelfHeight: LoadProperty<Single>(ShelfHeightProperty, Single.Parse(dto.Value)); break;
                        case _shelfWidth: LoadProperty<Single>(ShelfWidthProperty, Single.Parse(dto.Value)); break;
                        case _shelfDepth: LoadProperty<Single>(ShelfDepthProperty, Single.Parse(dto.Value)); break;
                        case _shelfFillColour: LoadProperty<Int32>(ShelfFillColourProperty, Int32.Parse(dto.Value)); break;
                        case _pegboardHeight: LoadProperty<Single>(PegboardHeightProperty, Single.Parse(dto.Value)); break;
                        case _pegboardWidth: LoadProperty<Single>(PegboardWidthProperty, Single.Parse(dto.Value)); break;
                        case _pegboardDepth: LoadProperty<Single>(PegboardDepthProperty, Single.Parse(dto.Value)); break;
                        case _pegboardFillColour: LoadProperty<Int32>(PegboardFillColourProperty, Int32.Parse(dto.Value)); break;
                        case _chestHeight: LoadProperty<Single>(ChestHeightProperty, Single.Parse(dto.Value)); break;
                        case _chestWidth: LoadProperty<Single>(ChestWidthProperty, Single.Parse(dto.Value)); break;
                        case _chestDepth: LoadProperty<Single>(ChestDepthProperty, Single.Parse(dto.Value)); break;
                        case _chestFillColour: LoadProperty<Int32>(ChestFillColourProperty, Int32.Parse(dto.Value)); break;
                        case _barHeight: LoadProperty<Single>(BarHeightProperty, Single.Parse(dto.Value)); break;
                        case _barWidth: LoadProperty<Single>(BarWidthProperty, Single.Parse(dto.Value)); break;
                        case _barDepth: LoadProperty<Single>(BarDepthProperty, Single.Parse(dto.Value)); break;
                        case _barFillColour: LoadProperty<Int32>(BarFillColourProperty, Int32.Parse(dto.Value)); break;
                        case _rodHeight: LoadProperty<Single>(RodHeightProperty, Single.Parse(dto.Value)); break;
                        case _rodWidth: LoadProperty<Single>(RodWidthProperty, Single.Parse(dto.Value)); break;
                        case _rodDepth: LoadProperty<Single>(RodDepthProperty, Single.Parse(dto.Value)); break;
                        case _rodFillColour: LoadProperty<Int32>(RodFillColourProperty, Int32.Parse(dto.Value)); break;
                        case _clipStripHeight: LoadProperty<Single>(ClipStripHeightProperty, Single.Parse(dto.Value)); break;
                        case _clipStripWidth: LoadProperty<Single>(ClipStripWidthProperty, Single.Parse(dto.Value)); break;
                        case _clipStripDepth: LoadProperty<Single>(ClipStripDepthProperty, Single.Parse(dto.Value)); break;
                        case _clipStripFillColour: LoadProperty<Int32>(ClipStripFillColourProperty, Int32.Parse(dto.Value)); break;
                        case _palletHeight: LoadProperty<Single>(PalletHeightProperty, Single.Parse(dto.Value)); break;
                        case _palletWidth: LoadProperty<Single>(PalletWidthProperty, Single.Parse(dto.Value)); break;
                        case _palletDepth: LoadProperty<Single>(PalletDepthProperty, Single.Parse(dto.Value)); break;
                        case _palletFillColour: LoadProperty<Int32>(PalletFillColourProperty, Int32.Parse(dto.Value)); break;
                        case _slotwallHeight: LoadProperty<Single>(SlotwallHeightProperty, Single.Parse(dto.Value)); break;
                        case _slotwallWidth: LoadProperty<Single>(SlotwallWidthProperty, Single.Parse(dto.Value)); break;
                        case _slotwallDepth: LoadProperty<Single>(SlotwallDepthProperty, Single.Parse(dto.Value)); break;
                        case _slotwallFillColour: LoadProperty<Int32>(SlotwallFillColourProperty, Int32.Parse(dto.Value)); break;                            
                        case _isLibraryPanelVisible: LoadProperty<Boolean>(IsLibraryPanelVisibleProperty, Boolean.Parse(dto.Value)); break;
                        case _statusBarHeight: LoadProperty<Int32>(StatusBarHeightProperty, Int32.Parse(dto.Value)); break;
                        case _positionProperties: LoadProperty<String>(PositionPropertiesProperty, dto.Value); break;
                        case _componentProperties: LoadProperty<String>(ComponentPropertiesProperty, dto.Value); break;
                        case _productImageSource: LoadProperty<RealImageProviderType>(ProductImageSourceProperty, EnumHelper.Parse(dto.Value, RealImageProviderType.Repository)); break;
                        case _productImageLocation: LoadProperty<String>(ProductImageLocationProperty, dto.Value); break;
                        case _productImageAttribute: LoadProperty<String>(ProductImageAttributeProperty, dto.Value); break;
                        case _productImageIgnoreLeadingZeros: LoadProperty<Boolean>(ProductImageIgnoreLeadingZerosProperty, Boolean.Parse(dto.Value)); break;
                        case _productImageMinFieldLength: LoadProperty<Byte>(ProductImageMinFieldLengthProperty, Byte.Parse(dto.Value)); break;
                        case _productImageFacingPostfixFront: LoadProperty<String>(ProductImageFacingPostfixFrontProperty, dto.Value); break;
                        case _productImageFacingPostfixLeft: LoadProperty<String>(ProductImageFacingPostfixLeftProperty, dto.Value); break;
                        case _productImageFacingPostfixTop: LoadProperty<String>(ProductImageFacingPostfixTopProperty, dto.Value); break;
                        case _productImageFacingPostfixBack: LoadProperty<String>(ProductImageFacingPostfixBackProperty, dto.Value); break;
                        case _productImageFacingPostfixRight: LoadProperty<String>(ProductImageFacingPostfixRightProperty, dto.Value); break;
                        case _productImageFacingPostfixBottom: LoadProperty<String>(ProductImageFacingPostfixBottomProperty, dto.Value); break;
                        case _productImageCompression: LoadProperty<RealImageCompressionType>(ProductImageCompressionProperty, EnumHelper.Parse(dto.Value, RealImageCompressionType.None)); break;
                        case _productImageColourDepth: LoadProperty<RealImageColourDepthType>(ProductImageColourDepthProperty, EnumHelper.Parse(dto.Value, RealImageColourDepthType.Bit8)); break;
                        case _productImageTransparency: LoadProperty<Boolean>(ProductImageTransparencyProperty, Boolean.Parse(dto.Value)); break;
                        case _showComponentHasCollisionsWarnings: this.LoadProperty<Boolean>(ShowComponentHasCollisionsWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showComponentIsOverfilledWarnings: LoadProperty<Boolean>(ShowComponentIsOverfilledWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showComponentSlopeWithNoRiserWarnings: LoadProperty<Boolean>(ShowComponentSlopeWithNoRiserWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showComponentIsOutsideOfFixtureAreaWarnings: LoadProperty<Boolean>(ShowComponentIsOutsideOfFixtureAreaWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showPegIsOverfilledWarnings: LoadProperty<Boolean>(ShowPegIsOverfilledWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showPositionCannotBreakTrayBackWarnings: LoadProperty<Boolean>(ShowPositionCannotBreakTrayBackWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showPositionCannotBreakTrayDownWarnings: LoadProperty<Boolean>(ShowPositionCannotBreakTrayDownWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showPositionCannotBreakTrayTopWarnings: LoadProperty<Boolean>(ShowPositionCannotBreakTrayTopWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showPositionCannotBreakTrayUpWarnings: LoadProperty<Boolean>(ShowPositionCannotBreakTrayUpWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showPositionDoesNotAchieveMinDeepWarnings: LoadProperty<Boolean>(ShowPositionDoesNotAchieveMinDeepWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showPositionExceedsMaxDeepWarnings: LoadProperty<Boolean>(ShowPositionExceedsMaxDeepWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showPositionExceedsMaxRightCapWarnings: LoadProperty<Boolean>(ShowPositionExceedsMaxRightCapWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showPositionExceedsMaxStackWarnings: LoadProperty<Boolean>(ShowPositionExceedsMaxStackWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showPositionExceedsMaxTopCapWarnings: LoadProperty<Boolean>(ShowPositionExceedsMaxTopCapWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showPositionHasCollisionsWarnings: LoadProperty<Boolean>(ShowPositionHasCollisionsWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showPositionHasInvalidMerchStyleSettingsWarnings: LoadProperty<Boolean>(ShowPositionHasInvalidMerchStyleSettingsWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showPositionIsHangingTrayWarnings: LoadProperty<Boolean>(ShowPositionIsHangingTrayWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showPositionIsHangingWithoutPegWarnings: LoadProperty<Boolean>(ShowPositionIsHangingWithoutPegWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showPositionIsOutsideMerchandisingSpaceWarnings: LoadProperty<Boolean>(ShowPositionIsOutsideMerchandisingSpaceWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showProductDoesNotAchieveMinCasesWarnings: LoadProperty<Boolean>(ShowProductDoesNotAchieveMinCasesWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showProductDoesNotAchieveMinDeliveriesWarnings: LoadProperty<Boolean>(ShowProductDoesNotAchieveMinDeliveriesWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showProductDoesNotAchieveMinDosWarnings: LoadProperty<Boolean>(ShowProductDoesNotAchieveMinDosWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showProductDoesNotAchieveMinShelfLifeWarnings: LoadProperty<Boolean>(ShowProductDoesNotAchieveMinShelfLifeWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showProductHasDuplicateGtinWarnings: LoadProperty<Boolean>(ShowProductHasDuplicateGtinWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _isDynamicTextScalingEnabled: LoadProperty<Boolean>(IsDynamicTextScalingEnabledProperty, Boolean.Parse(dto.Value)); break;
                        case _defaultPrintTemplateId: LoadProperty<String>(DefaultPrintTemplateIdProperty, dto.Value); break;
                        case _showAssortmentLocalLineRulesWarnings: LoadProperty<Boolean>(ShowAssortmentLocalLineRulesWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showAssortmentDistributionRulesWarnings: LoadProperty<Boolean>(ShowAssortmentDistributionRulesWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showAssortmentProductRulesWarnings: LoadProperty<Boolean>(ShowAssortmentProductRulesWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showAssortmentFamilyRulesWarnings: LoadProperty<Boolean>(ShowAssortmentFamilyRulesWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showAssortmentInheritanceRulesWarnings: LoadProperty<Boolean>(ShowAssortmentInheritanceRulesWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showAssortmentCoreRulesWarnings: LoadProperty<Boolean>(ShowAssortmentCoreRulesWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showLocationProductIllegalWarnings: LoadProperty<Boolean>(ShowLocationProductIllegalWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showLocationProductLegalWarnings: LoadProperty<Boolean>(ShowLocationProductLegalWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showPositionPegHolesAreInvalidWarnings: LoadProperty<Boolean>(ShowPositionPegHolesAreInvalidWarningsProperty, Boolean.Parse(dto.Value)); break;

                        case _showInstantComponentHasCollisionsWarnings: this.LoadProperty<Boolean>(ShowInstantComponentHasCollisionsWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantComponentIsOverfilledWarnings: LoadProperty<Boolean>(ShowInstantComponentIsOverfilledWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantComponentSlopeWithNoRiserWarnings: LoadProperty<Boolean>(ShowInstantComponentSlopeWithNoRiserWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantComponentIsOutsideOfFixtureAreaWarnings: LoadProperty<Boolean>(ShowInstantComponentIsOutsideOfFixtureAreaWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantPegIsOverfilledWarnings: LoadProperty<Boolean>(ShowInstantPegIsOverfilledWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantPositionCannotBreakTrayBackWarnings: LoadProperty<Boolean>(ShowInstantPositionCannotBreakTrayBackWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantPositionCannotBreakTrayDownWarnings: LoadProperty<Boolean>(ShowInstantPositionCannotBreakTrayDownWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantPositionCannotBreakTrayTopWarnings: LoadProperty<Boolean>(ShowInstantPositionCannotBreakTrayTopWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantPositionCannotBreakTrayUpWarnings: LoadProperty<Boolean>(ShowInstantPositionCannotBreakTrayUpWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantPositionDoesNotAchieveMinDeepWarnings: LoadProperty<Boolean>(ShowInstantPositionDoesNotAchieveMinDeepWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantPositionExceedsMaxDeepWarnings: LoadProperty<Boolean>(ShowInstantPositionExceedsMaxDeepWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantPositionExceedsMaxRightCapWarnings: LoadProperty<Boolean>(ShowInstantPositionExceedsMaxRightCapWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantPositionExceedsMaxStackWarnings: LoadProperty<Boolean>(ShowInstantPositionExceedsMaxStackWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantPositionExceedsMaxTopCapWarnings: LoadProperty<Boolean>(ShowInstantPositionExceedsMaxTopCapWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantPositionHasCollisionsWarnings: LoadProperty<Boolean>(ShowInstantPositionHasCollisionsWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantPositionHasInvalidMerchStyleSettingsWarnings: LoadProperty<Boolean>(ShowInstantPositionHasInvalidMerchStyleSettingsWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantPositionIsHangingTrayWarnings: LoadProperty<Boolean>(ShowInstantPositionIsHangingTrayWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantPositionIsHangingWithoutPegWarnings: LoadProperty<Boolean>(ShowInstantPositionIsHangingWithoutPegWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantPositionIsOutsideMerchandisingSpaceWarnings: LoadProperty<Boolean>(ShowInstantPositionIsOutsideMerchandisingSpaceWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantProductDoesNotAchieveMinCasesWarnings: LoadProperty<Boolean>(ShowInstantProductDoesNotAchieveMinCasesWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantProductDoesNotAchieveMinDeliveriesWarnings: LoadProperty<Boolean>(ShowInstantProductDoesNotAchieveMinDeliveriesWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantProductDoesNotAchieveMinDosWarnings: LoadProperty<Boolean>(ShowInstantProductDoesNotAchieveMinDosWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantProductDoesNotAchieveMinShelfLifeWarnings: LoadProperty<Boolean>(ShowInstantProductDoesNotAchieveMinShelfLifeWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantProductHasDuplicateGtinWarnings: LoadProperty<Boolean>(ShowInstantProductHasDuplicateGtinWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantAssortmentLocalLineRulesWarnings: LoadProperty<Boolean>(ShowInstantAssortmentLocalLineRulesWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantAssortmentDistributionRulesWarnings: LoadProperty<Boolean>(ShowInstantAssortmentDistributionRulesWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantAssortmentProductRulesWarnings: LoadProperty<Boolean>(ShowInstantAssortmentProductRulesWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantAssortmentFamilyRulesWarnings: LoadProperty<Boolean>(ShowInstantAssortmentFamilyRulesWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantAssortmentInheritanceRulesWarnings: LoadProperty<Boolean>(ShowInstantAssortmentInheritanceRulesWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantAssortmentCoreRulesWarnings: LoadProperty<Boolean>(ShowInstantAssortmentCoreRulesWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantLocationProductIllegalWarnings: LoadProperty<Boolean>(ShowInstantLocationProductIllegalWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _showInstantLocationProductLegalWarnings: LoadProperty<Boolean>(ShowInstantLocationProductLegalWarningsProperty, Boolean.Parse(dto.Value)); break;
                        case _textBoxFont: LoadProperty<String>(TextBoxFontProperty, dto.Value); break;
                        case _textBoxFontSize: LoadProperty<Single>(TextBoxFontSizeProperty, Single.Parse(dto.Value)); break;
                        case _textBoxFontColour: LoadProperty<Int32>(TextBoxFontColourProperty, Int32.Parse(dto.Value)); break;
                        case _textBoxCanReduceToFit: LoadProperty<Boolean>(TextBoxCanReduceToFitProperty, Boolean.Parse(dto.Value)); break;
                        case _textBoxBorderColour: LoadProperty<Int32>(TextBoxBorderColourProperty, Int32.Parse(dto.Value)); break;
                        case _textBoxBackgroundColour: LoadProperty<Int32>(TextBoxBackgroundColourProperty, Int32.Parse(dto.Value)); break;
                        case _textBoxBorderThickness: LoadProperty<Single>(TextBoxBorderThicknessProperty, Single.Parse(dto.Value)); break;
                        case _showInstantPositionPegHolesAreInvalidWarnings: LoadProperty<Boolean>(ShowInstantPositionPegHolesAreInvalidWarningsProperty, Boolean.Parse(dto.Value)); break;

                    }
                }
                catch (Exception) { }
            }
            LoadProperty<UserEditorSettingsColorList>(ColorsProperty, UserEditorSettingsColorList.GetUserEditorSettingsColors(dalContext));
            LoadProperty<UserEditorSettingsRecentLabelList>(RecentLabelProperty, UserEditorSettingsRecentLabelList.GetUserEditorSettingsRecentLabel(dalContext));
            LoadProperty<UserEditorSettingsRecentHighlightList>(RecentHighlightProperty, UserEditorSettingsRecentHighlightList.GetUserEditorSettingsRecentHighlight(dalContext));
            LoadProperty<UserEditorSettingsRecentDatasheetList>(RecentDatasheetProperty, UserEditorSettingsRecentDatasheetList.GetUserEditorSettingsRecentDatasheet(dalContext));
            LoadProperty<UserEditorSettingsSelectedColumnList>(SelectedColumnsProperty, UserEditorSettingsSelectedColumnList.GetUserEditorSettingsSelectedColumn(dalContext));
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning FetchById
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            // set default values incase we are missing information
            // within the solution database
            LoadDefaults();

            //Load from the dal.
            IDalFactory dalFactory = this.GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IUserEditorSettingDal dal = dalContext.GetDal<IUserEditorSettingDal>())
                {
                    LoadDataTransferObjects(dalContext, dal.FetchAll());
                }
            }
        }

        #endregion

        #region Insert

        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                using (IUserEditorSettingDal dal = dalContext.GetDal<IUserEditorSettingDal>())
                {
                    UpdateConfig(dal);
                }

                FieldManager.UpdateChildren(dalContext, this);

                dalContext.Commit();
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Called when updating this object in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                using (IUserEditorSettingDal dal = dalContext.GetDal<IUserEditorSettingDal>())
                {
                    UpdateConfig(dal);
                }

                FieldManager.UpdateChildren(dalContext, this);

                dalContext.Commit();
            }
        }

        #endregion

        #endregion
    }
}
