﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// V8-24265 : J.Pickup
//  Created
#endregion
#region Version History: (CCM 8.03)
// V8-28662 : L.Ineson
//  Added ToPlanogramLabelTextDirection
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Model
{
    public enum LabelTextDirectionType
    {
        Vertical = 0,
        Horizontal = 1,
    }

    public static class LabelTextDirectionTypeHelper
    {
        public static readonly Dictionary<LabelTextDirectionType, String> FriendlyNames =
            new Dictionary<LabelTextDirectionType, String>()
            {
                {LabelTextDirectionType.Vertical, Message.Enum_TextDirectionType_Vertical},
                {LabelTextDirectionType.Horizontal, Message.Enum_TextDirectionType_Horizontal},
            };

        public static PlanogramLabelTextDirection ToPlanogramLabelTextDirection(this LabelTextDirectionType value)
        {
            switch (value)
            {
                default:
                case LabelTextDirectionType.Horizontal: return PlanogramLabelTextDirection.Horizontal;
                case LabelTextDirectionType.Vertical: return PlanogramLabelTextDirection.Vertical;
            }
        }
    }
}
