﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using AutoMapper;
using Galleria.Framework2.Repository.Services.DataContracts;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model object representing info about a repository
    /// </summary>
    public partial class RepositoryInfo : ModelReadOnlyObject<RepositoryInfo>
    {
        #region Static Constructor
        static RepositoryInfo()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Guid> UniqueIdProperty =
            RegisterModelProperty<Guid>(c => c.UniqueId);
        /// <summary>
        /// Returns the repository id
        /// </summary>
        public Guid UniqueId
        {
            get { return this.GetProperty<Guid>(UniqueIdProperty); }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Returns the repository name
        /// </summary>
        public String Name
        {
            get { return this.GetProperty<String>(NameProperty); }
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(RepositoryInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(RepositoryInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(RepositoryInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(RepositoryInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Mappings
        /// <summary>
        /// Create all mappings for this type
        /// </summary>
        internal static void CreateMappings()
        {
            // model to data contract
            Mapper.CreateMap<RepositoryInfo, RepositoryInfoDc>();

            // data contract to model
            Mapper.CreateMap<RepositoryInfoDc, RepositoryInfo>();
        }
        #endregion
    }
}
