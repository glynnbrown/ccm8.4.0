﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25453 : A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class ConsumerDecisionTreeList 
    {
        #region Constructor
        private ConsumerDecisionTreeList() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of consumer decision tree objects for the given entity id, consumer decision tree ids
        /// </summary>
        public static ConsumerDecisionTreeList FetchByEntityIdConsumerDecisionTreeIds(Int32 entityId, IEnumerable<Int32> consumerDecisionTreeIds)
        {
            return DataPortal.Fetch<ConsumerDecisionTreeList>(new FetchByEntityIdConsumerDecisionTreeIdsCriteria(entityId, consumerDecisionTreeIds));
        }

        #endregion

        #region Data Access

        #region Fetch

        private void DataPortal_Fetch(FetchByEntityIdConsumerDecisionTreeIdsCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                //fetch all products for the productGroup
                using (IConsumerDecisionTreeDal dal = dalContext.GetDal<IConsumerDecisionTreeDal>())
                {
                    IEnumerable<ConsumerDecisionTreeDto> dtoList = dal.FetchByEntityIdConsumerDecisionTreeIds(criteria.EntityId, criteria.ConsumerDecisionTreeIds);
                    foreach (ConsumerDecisionTreeDto dto in dtoList)
                    {
                        this.Add(ConsumerDecisionTree.FetchConsumerDecisionTree(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #region Update
        /// <summary>
        /// Called when this list is being updated
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                Child_Update(dalContext);
                dalContext.Commit();
            }
        }
        #endregion

        #endregion
    }
}
