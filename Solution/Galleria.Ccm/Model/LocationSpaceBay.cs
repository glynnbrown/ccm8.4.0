﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
// V8-24264 : A.Probyn
//      Added missing ModelPropertyType
// V8-28482 : A.Kuszyk
//  Added OnCopy override.
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Rules;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a description of a type of location space Bay
    /// (child of location space Bay list)
    /// </summary>
    [Serializable]
    public sealed partial class LocationSpaceBay : ModelObject<LocationSpaceBay>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new LocationSpaceProductGroup Parent
        {
            get { return ((LocationSpaceBayList)base.Parent).Parent; }
        }
        #endregion

        #region Properties
        /// <summary>
        /// The Location Space Bay Id.
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }

        /// <summary>
        /// The Location Space Order.
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> OrderProperty =
            RegisterModelProperty<Byte>(c => c.Order, Message.LocationSpaceBay_Number);
        public Byte Order
        {
            get { return GetProperty<Byte>(OrderProperty); }
            set { SetProperty<Byte>(OrderProperty, value); }
        }

        /// <summary>
        /// The Location Space Height.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HeightProperty =
            RegisterModelProperty<Single>(c => c.Height, Message.LocationSpaceBay_Height, ModelPropertyDisplayType.Length);
        public Single Height
        {
            get { return GetProperty<Single>(HeightProperty); }
            set { SetProperty<Single>(HeightProperty, value); }
        }

        /// <summary>
        /// The Location Space Width.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> WidthProperty =
            RegisterModelProperty<Single>(c => c.Width, Message.LocationSpaceBay_Width, ModelPropertyDisplayType.Length);
        public Single Width
        {
            get { return GetProperty<Single>(WidthProperty); }
            set { SetProperty<Single>(WidthProperty, value); }
        }

        /// <summary>
        /// The Location Space Depth.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DepthProperty =
            RegisterModelProperty<Single>(c => c.Depth, Message.LocationSpaceBay_Depth, ModelPropertyDisplayType.Length);
        public Single Depth
        {
            get { return GetProperty<Single>(DepthProperty); }
            set { SetProperty<Single>(DepthProperty, value); }
        }

        /// <summary>
        /// The Location Space Base Height.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BaseHeightProperty =
            RegisterModelProperty<Single>(c => c.BaseHeight, Message.LocationSpaceBay_BaseHeight, ModelPropertyDisplayType.Length);
        public Single BaseHeight
        {
            get { return GetProperty<Single>(BaseHeightProperty); }
            set { SetProperty<Single>(BaseHeightProperty, value); }
        }

        /// <summary>
        /// The Location Space Base Width.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BaseWidthProperty =
            RegisterModelProperty<Single>(c => c.BaseWidth, Message.LocationSpaceBay_BaseWidth, ModelPropertyDisplayType.Length);
        public Single BaseWidth
        {
            get { return GetProperty<Single>(BaseWidthProperty); }
            set { SetProperty<Single>(BaseWidthProperty, value); }
        }

        /// <summary>
        /// The Location Space Base Depth.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BaseDepthProperty =
            RegisterModelProperty<Single>(c => c.BaseDepth, Message.LocationSpaceBay_BaseDepth, ModelPropertyDisplayType.Length);
        public Single BaseDepth
        {
            get { return GetProperty<Single>(BaseDepthProperty); }
            set { SetProperty<Single>(BaseDepthProperty, value); }
        }

        /// <summary>
        /// The Location Space Element
        /// </summary>
        public static readonly ModelPropertyInfo<LocationSpaceElementList> ElementsProperty =
            RegisterModelProperty<LocationSpaceElementList>(c => c.Elements);

        public LocationSpaceElementList Elements
        {
            get { return GetProperty<LocationSpaceElementList>(ElementsProperty); }
        }

        /// <summary>
        /// Is Aisle Start.
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsAisleStartProperty =
            RegisterModelProperty<Boolean>(c => c.IsAisleStart, Message.LocationSpaceBay_IsAisleStart);
        public Boolean IsAisleStart
        {
            get { return GetProperty<Boolean>(IsAisleStartProperty); }
            set { SetProperty<Boolean>(IsAisleStartProperty, value); }
        }

        /// <summary>
        /// Is Aisle End.
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsAisleEndProperty =
            RegisterModelProperty<Boolean>(c => c.IsAisleEnd, Message.LocationSpaceBay_IsAisleEnd);
        public Boolean IsAisleEnd
        {
            get { return GetProperty<Boolean>(IsAisleEndProperty); }
            set { SetProperty<Boolean>(IsAisleEndProperty, value); }
        }

        /// <summary>
        /// Is Aisle Right.
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsAisleRightProperty =
            RegisterModelProperty<Boolean>(c => c.IsAisleRight, Message.LocationSpaceBay_IsAisleRight);
        public Boolean IsAisleRight
        {
            get { return GetProperty<Boolean>(IsAisleRightProperty); }
            set { SetProperty<Boolean>(IsAisleRightProperty, value); }
        }

        /// <summary>
        /// Is Aisle Left.
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsAisleLeftProperty =
            RegisterModelProperty<Boolean>(c => c.IsAisleLeft, Message.LocationSpaceBay_IsIsleLeft);
        public Boolean IsAisleLeft
        {
            get { return GetProperty<Boolean>(IsAisleLeftProperty); }
            set { SetProperty<Boolean>(IsAisleLeftProperty, value); }
        }

        /// <summary>
        /// BayLocationRef
        /// </summary>
        public static readonly ModelPropertyInfo<String> BayLocationRefProperty =
            RegisterModelProperty<String>(c => c.BayLocationRef, Message.LocationSpaceBay_BayLocationRef);
        public String BayLocationRef
        {
            get { return GetProperty<String>(BayLocationRefProperty); }
            set { SetProperty<String>(BayLocationRefProperty, value); }
        }

        /// <summary>
        /// BayFloorDrawingNo
        /// </summary>
        public static readonly ModelPropertyInfo<String> BayFloorDrawingNoProperty =
            RegisterModelProperty<String>(c => c.BayFloorDrawingNo, Message.LocationSpaceBay_BayFloorDrawingNo);
        public String BayFloorDrawingNo
        {
            get { return GetProperty<String>(BayFloorDrawingNoProperty); }
            set { SetProperty<String>(BayFloorDrawingNoProperty, value); }
        }

        /// <summary>
        /// Fixture Name
        /// </summary>
        public static readonly ModelPropertyInfo<String> FixtureNameProperty =
            RegisterModelProperty<String>(c => c.FixtureName, Message.LocationSpaceBay_FixtureName);
        public String FixtureName
        {
            get { return GetProperty<String>(FixtureNameProperty); }
            set { SetProperty<String>(FixtureNameProperty, value); }
        }

        /// <summary>
        /// Barcode
        /// </summary>
        public static readonly ModelPropertyInfo<String> BarcodeProperty =
            RegisterModelProperty<String>(c => c.Barcode, Message.LocationSpaceBay_Barcode);
        public String Barcode
        {
            get { return GetProperty<String>(BarcodeProperty); }
            set { SetProperty<String>(BarcodeProperty, value); }
        }

        /// <summary>
        /// Manufacturer
        /// </summary>
        public static readonly ModelPropertyInfo<String> ManufacturerProperty =
            RegisterModelProperty<String>(c => c.Manufacturer, Message.LocationSpaceBay_Manufacturer);
        public String Manufacturer
        {
            get { return GetProperty<String>(ManufacturerProperty); }
            set { SetProperty<String>(ManufacturerProperty, value); }
        }

        /// <summary>
        /// Colour
        /// </summary>
        public static readonly ModelPropertyInfo<String> ColourProperty =
            RegisterModelProperty<String>(c => c.Colour, Message.LocationSpaceBay_Color);
        public String Colour
        {
            get { return GetProperty<String>(ColourProperty); }
            set { SetProperty<String>(ColourProperty, value); }
        }

        /// <summary>
        /// Bay Type
        /// </summary>
        public static readonly ModelPropertyInfo<LocationSpaceBayType> BayTypeProperty =
            RegisterModelProperty<LocationSpaceBayType>(c => c.BayType, Message.LocationSpaceBay_BayType,
            Message.LocationSpaceBay_BayType, LocationSpaceBayType.None);
        public LocationSpaceBayType BayType
        {
            get { return GetProperty<LocationSpaceBayType>(BayTypeProperty); }
            set { SetProperty<LocationSpaceBayType>(BayTypeProperty, value); }
        }        /// <summary>

        /// Asset
        /// </summary>
        public static readonly ModelPropertyInfo<String> AssetProperty =
            RegisterModelProperty<String>(c => c.Asset, Message.LocationSpaceBay_Asset);
        public String Asset
        {
            get { return GetProperty<String>(AssetProperty); }
            set { SetProperty<String>(AssetProperty, value); }
        }

        /// Fixture Type
        /// </summary>
        public static readonly ModelPropertyInfo<LocationSpaceFixtureType> FixtureTypeProperty =
            RegisterModelProperty<LocationSpaceFixtureType>(c => c.FixtureType, Message.LocationSpaceBay_Fixture,
            null, LocationSpaceFixtureType.Standard);
        public LocationSpaceFixtureType FixtureType
        {
            get { return GetProperty<LocationSpaceFixtureType>(FixtureTypeProperty); }
            set { SetProperty<LocationSpaceFixtureType>(FixtureTypeProperty, value); }
        }

        /// Temperature
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> TemperatureProperty =
            RegisterModelProperty<Single?>(c => c.Temperature, Message.LocationSpaceBay_Temperature);
        public Single? Temperature
        {
            get { return GetProperty<Single?>(TemperatureProperty); }
            set { SetProperty<Single?>(TemperatureProperty, value); }
        }

        /// Fixture Shape Type
        /// </summary>
        public static readonly ModelPropertyInfo<LocationSpaceFixtureShapeType> FixtureShapeTypeProperty =
            RegisterModelProperty<LocationSpaceFixtureShapeType>(c => c.FixtureShapeType, Message.LocationSpaceBay_FixtureShapeType,
            null, LocationSpaceFixtureShapeType.Rectangle);
        public LocationSpaceFixtureShapeType FixtureShapeType
        {
            get { return GetProperty<LocationSpaceFixtureShapeType>(FixtureShapeTypeProperty); }
            set { SetProperty<LocationSpaceFixtureShapeType>(FixtureShapeTypeProperty, value); }
        }


        /// Promotional
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsPromotionalProperty =
            RegisterModelProperty<Boolean>(c => c.IsPromotional, Message.LocationSpaceBay_Promotional);
        public Boolean IsPromotional
        {
            get { return GetProperty<Boolean>(IsPromotionalProperty); }
            set { SetProperty<Boolean>(IsPromotionalProperty, value); }
        }



        /// Bay Type Post Fix
        /// </summary>
        public static readonly ModelPropertyInfo<String> BayTypePostFixProperty =
            RegisterModelProperty<String>(c => c.BayTypePostFix, Message.LocationSpaceBay_BayTypePostFix);
        public String BayTypePostFix
        {
            get { return GetProperty<String>(BayTypePostFixProperty); }
            set { SetProperty<String>(BayTypePostFixProperty, value); }
        }

        /// HasPower
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasPowerProperty =
            RegisterModelProperty<Boolean>(c => c.HasPower, Message.LocationSpaceBay_HasPower);
        public Boolean HasPower
        {
            get { return GetProperty<Boolean>(HasPowerProperty); }
            set { SetProperty<Boolean>(HasPowerProperty, value); }
        }

        /// Bay Type Calculation
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> BayTypeCalculationProperty =
            RegisterModelProperty<Single?>(c => c.BayTypeCalculation, Message.LocationSpaceBay_BayTypeCalculation, ModelPropertyDisplayType.Length);
        public Single? BayTypeCalculation
        {
            get { return GetProperty<Single?>(BayTypeCalculationProperty); }
            set { SetProperty<Single?>(BayTypeCalculationProperty, value); }
        }


        /// Notch Pitch
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> NotchPitchProperty =
            RegisterModelProperty<Single?>(c => c.NotchPitch, Message.LocationSpaceBay_NotchPitch, ModelPropertyDisplayType.Length);
        public Single? NotchPitch
        {
            get { return GetProperty<Single?>(NotchPitchProperty); }
            set { SetProperty<Single?>(NotchPitchProperty, value); }
        }

        /// Notch Start Y
        /// </summary>
        public static readonly ModelPropertyInfo<Single> NotchStartYProperty =
            RegisterModelProperty<Single>(c => c.NotchStartY, Message.LocationSpaceBay_NotchStartY, ModelPropertyDisplayType.Length);
        public Single NotchStartY
        {
            get { return GetProperty<Single>(NotchStartYProperty); }
            set { SetProperty<Single>(NotchStartYProperty, value); }
        }

        /// FixtureShape
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FixtureShapeProperty =
            RegisterModelProperty<Single>(c => c.FixtureShape, Message.LocationSpaceBay_FixtureShapeType);
        public Single FixtureShape
        {
            get { return GetProperty<Single>(FixtureShapeProperty); }
            set { SetProperty<Single>(FixtureShapeProperty, value); }
        }



        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationSpaceBay), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationSpaceCreate.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceBay), new IsInRole(AuthorizationActions.GetObject, DomainPermission.LocationSpaceGet.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceBay), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationSpaceEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceBay), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationSpaceDelete.ToString()));
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(OrderProperty));
            BusinessRules.AddRule(new MinValue<Byte>(OrderProperty, 1));

            BusinessRules.AddRule(new Required(HeightProperty));
            BusinessRules.AddRule(new MinValue<Single>(HeightProperty, 0));
            BusinessRules.AddRule(new MaxValue<Single>(HeightProperty, 999.99f));
            BusinessRules.AddRule(new Required(WidthProperty));
            BusinessRules.AddRule(new MinValue<Single>(WidthProperty, 0));
            BusinessRules.AddRule(new MaxValue<Single>(WidthProperty, 999.99f));
            BusinessRules.AddRule(new Required(DepthProperty));
            BusinessRules.AddRule(new MinValue<Single>(DepthProperty, 0));
            BusinessRules.AddRule(new MaxValue<Single>(DepthProperty, 999.99f));

            BusinessRules.AddRule(new NullableMinValue<Single>(BaseHeightProperty, 0));
            BusinessRules.AddRule(new NullableMaxValue<Single>(BaseHeightProperty, 999.99f));
            BusinessRules.AddRule(new NullableMinValue<Single>(BaseWidthProperty, 0));
            BusinessRules.AddRule(new NullableMaxValue<Single>(BaseWidthProperty, 999.99f));
            BusinessRules.AddRule(new NullableMinValue<Single>(BaseDepthProperty, 0));
            BusinessRules.AddRule(new NullableMaxValue<Single>(BaseDepthProperty, 999.99f));

            BusinessRules.AddRule(new NullableMinValue<Single>(NotchPitchProperty, 0));
            BusinessRules.AddRule(new NullableMaxValue<Single>(NotchPitchProperty, 99.99f));

            BusinessRules.AddRule(new MaxLength(BayTypeCalculationProperty, 50));
            BusinessRules.AddRule(new NullableMaxValue<Single>(BayTypeCalculationProperty, 999.99f));

            //added as per current import min/max values for those properties            
            BusinessRules.AddRule(new MinValue<Single>(NotchStartYProperty, -250f));
            BusinessRules.AddRule(new MaxValue<Single>(NotchStartYProperty, 250f));

        }

        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a new LocationSpaceBay object
        /// </summary>
        /// <returns></returns>
        public static LocationSpaceBay NewLocationSpaceBay(Boolean createDefaultElement = true)
        {
            LocationSpaceBay item = new LocationSpaceBay();
            item.Create(createDefaultElement);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Boolean createDefaultElement)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Byte>(OrderProperty, 1);
            this.LoadProperty<LocationSpaceElementList>(ElementsProperty, LocationSpaceElementList.NewLocationSpaceElementList(createDefaultElement));
            this.LoadProperty<Single?>(NotchPitchProperty, 0);
            this.LoadProperty<Single>(NotchStartYProperty, 0);
            this.LoadProperty<LocationSpaceBayType>(BayTypeProperty, LocationSpaceBayType.None);
            this.MarkAsChild();
            this.MarkGraphAsInitialized();
        }

        #endregion

        #endregion

        #region Override

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            SetProperty<Int32>(IdProperty, newId);
            context.RegisterId<LocationSpaceBay>(oldId, newId);
        }

        public override string ToString()
        {
            return this.Id.ToString();
        }
        #endregion
    }
}
