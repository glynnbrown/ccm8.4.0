﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson
//  Copied/Amended from GFS 212
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    public enum ProductMerchandisingStyle
    {
        Unit,
        Tray,
        Case,
        Alternate,
        Display,
        PointOfPurchase
    }

    public static class ProductMerchandisingStyleHelper
    {
        public static readonly Dictionary<ProductMerchandisingStyle, String> FriendlyNames =
            new Dictionary<ProductMerchandisingStyle, String>()
            {
                {ProductMerchandisingStyle.Unit, Message.Enum_ProductMerchandisingStyle_Unit},
                {ProductMerchandisingStyle.Tray, Message.Enum_ProductMerchandisingStyle_Tray},
                {ProductMerchandisingStyle.Case, Message.Enum_ProductMerchandisingStyle_Case},
                {ProductMerchandisingStyle.Alternate, Message.Enum_ProductMerchandisingStyle_Alternate},
                {ProductMerchandisingStyle.Display, Message.Enum_ProductMerchandisingStyle_Display},
                {ProductMerchandisingStyle.PointOfPurchase, Message.Enum_ProductMerchandisingStyle_PointOfPurchase}
            };

        /// <summary>
        /// Dictionary with Friendly Name as key and ProductStatusType as value
        /// </summary>
        /// <remarks>[GFS-16246] Added to reduce the time the import takes to retrieve a merchandising style from 
        /// the imported friendly name.
        /// [GFS-14364] Set friendly names to lower invariant to allow import of all case variants</remarks>
        public static readonly Dictionary<String, ProductMerchandisingStyle> EnumFromFriendlyName =
            new Dictionary<String, ProductMerchandisingStyle>()
            {
                {Message.Enum_ProductMerchandisingStyle_Unit.ToLowerInvariant(), ProductMerchandisingStyle.Unit},
                {Message.Enum_ProductMerchandisingStyle_Tray.ToLowerInvariant(), ProductMerchandisingStyle.Tray},
                {Message.Enum_ProductMerchandisingStyle_Case.ToLowerInvariant(), ProductMerchandisingStyle.Case},
                {Message.Enum_ProductMerchandisingStyle_Alternate.ToLowerInvariant(), ProductMerchandisingStyle.Alternate},
                {Message.Enum_ProductMerchandisingStyle_Display.ToLowerInvariant(), ProductMerchandisingStyle.Display},
                {Message.Enum_ProductMerchandisingStyle_PointOfPurchase.ToLowerInvariant(), ProductMerchandisingStyle.PointOfPurchase}
            };

        public static ProductMerchandisingStyle? ProductMerchandisingStyleGetEnum(String description)
        {
            foreach (KeyValuePair<ProductMerchandisingStyle, String> keyPair in ProductMerchandisingStyleHelper.FriendlyNames)
            {
                if (keyPair.Value.Equals(description))
                {
                    return keyPair.Key;
                }
            }
            return null;
        }
    }
}
