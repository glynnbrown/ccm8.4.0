﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Enums;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramExportTemplatePerformanceMetric
    {
        
        #region Constructor
        private PlanogramExportTemplatePerformanceMetric() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static PlanogramExportTemplatePerformanceMetric Fetch(IDalContext dalContext, PlanogramExportTemplatePerformanceMetricDto dto)
        {
            return DataPortal.FetchChild<PlanogramExportTemplatePerformanceMetric>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramExportTemplatePerformanceMetricDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(DescriptionProperty, dto.Description);
            this.LoadProperty<MetricDirectionType>(DirectionProperty, (MetricDirectionType)dto.Direction);
            this.LoadProperty<MetricSpecialType>(SpecialTypeProperty, (MetricSpecialType)dto.SpecialType);
            this.LoadProperty<MetricType>(MetricTypeProperty, (MetricType)dto.MetricType);
            this.LoadProperty<Byte>(MetricIdProperty, dto.MetricId);
            this.LoadProperty<String>(ExternalFieldProperty, dto.ExternalField);
            this.LoadProperty<AggregationType>(AggregationTypeProperty, (AggregationType)dto.AggregationType);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private PlanogramExportTemplatePerformanceMetricDto GetDataTransferObject(PlanogramExportTemplate parent)
        {
            return new PlanogramExportTemplatePerformanceMetricDto()
            {
                Id = ReadProperty(IdProperty),
                PlanogramExportTemplateId = parent.Id,
                Name = this.ReadProperty<String>(NameProperty),
                Description = this.ReadProperty<String>(DescriptionProperty),
                Direction = (Byte)this.ReadProperty<MetricDirectionType>(DirectionProperty),
                SpecialType = (Byte)this.ReadProperty<MetricSpecialType>(SpecialTypeProperty),
                MetricType = (Byte)this.ReadProperty<MetricType>(MetricTypeProperty),
                MetricId = this.ReadProperty<Byte>(MetricIdProperty),
                ExternalField = ReadProperty(ExternalFieldProperty),
                AggregationType = (Byte)this.ReadProperty<AggregationType>(AggregationTypeProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, PlanogramExportTemplatePerformanceMetricDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, PlanogramExportTemplate parent)
        {
            PlanogramExportTemplatePerformanceMetricDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IPlanogramExportTemplatePerformanceMetricDal dal = dalContext.GetDal<IPlanogramExportTemplatePerformanceMetricDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<PlanogramExportTemplatePerformanceMetric>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, PlanogramExportTemplate parent)
        {
            if (this.IsSelfDirty)
            {
                using (IPlanogramExportTemplatePerformanceMetricDal dal = dalContext.GetDal<IPlanogramExportTemplatePerformanceMetricDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, PlanogramExportTemplate parent)
        {
            using (IPlanogramExportTemplatePerformanceMetricDal dal = dalContext.GetDal<IPlanogramExportTemplatePerformanceMetricDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion
    }
}
