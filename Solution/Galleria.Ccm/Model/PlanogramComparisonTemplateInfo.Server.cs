﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramComparisonTemplateInfo
    {
        #region Constructors
        private PlanogramComparisonTemplateInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static PlanogramComparisonTemplateInfo GetPlanogramComparisonTemplateInfo(IDalContext dalContext, PlanogramComparisonTemplateInfoDto dto)
        {
            return DataPortal.FetchChild<PlanogramComparisonTemplateInfo>(dalContext, dto);
        }

        /// <summary>
        /// Returns an info of an existing object
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        internal static PlanogramComparisonTemplateInfo GetPlanogramComparisonTemplateInfo(IDalContext dalContext, PlanogramComparisonTemplateInfoDto dto, String fileName)
        {
            return DataPortal.FetchChild<PlanogramComparisonTemplateInfo>(dalContext, dto, fileName);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramComparisonTemplateInfoDto dto)
        {
            LoadProperty<Object>(IdProperty, dto.Id);
            LoadProperty<String>(NameProperty, dto.Name);
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">The dto to load</param>
        /// <param name="fileName"></param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramComparisonTemplateInfoDto dto, String fileName)
        {
            LoadProperty<Object>(IdProperty, dto.Id);
            LoadProperty<String>(FileNameProperty, fileName);
            LoadProperty<String>(NameProperty, dto.Name);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when loading this object from a dto
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">The dto to load from</param>
        private void Child_Fetch(IDalContext dalContext, PlanogramComparisonTemplateInfoDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        /// <summary>
        /// Called when loading this object from a dto
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">The dto to load from</param>
        /// <param name="fileName"></param>
        private void Child_Fetch(IDalContext dalContext, PlanogramComparisonTemplateInfoDto dto, String fileName)
        {
            LoadDataTransferObject(dalContext, dto, fileName);
        }

        #endregion

        #endregion
    }
}