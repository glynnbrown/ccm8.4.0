﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  ProcessingStatusProperty was not calculated correctly
// V8-27411 : M.Pettit
//  Added ProcessingStatusDescription, ProgressDateStarted, ProgressDateLastUpdated properties
//  Added DateCompleted and UserId, UserFullName properties
#endregion
#region Version History: CCM810
// V8-29811 : A.Probyn
// Added PlanogramLocationType
// V8-29842 : A.Probyn
//  Extended to include WorkpackageType
// V8-29590 : A.Probyn
//      Extended for new PlanogramWarningCount, PlanogramTotalErrorScore, PlanogramHighestErrorScore
// V8-30079 : L.Ineson
//  Ammended status property enum type.
#endregion
#region Version History: CCM820
// V8-30887 : L.Ineson
//  Added workflow name
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    public partial class WorkpackageInfo
    {
        #region Constructor
        private WorkpackageInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an instance from a dto
        /// </summary>
        internal static WorkpackageInfo GetWorkpackageInfo(IDalContext dalContext, WorkpackageInfoDto dto)
        {
            return DataPortal.FetchChild<WorkpackageInfo>(dalContext, dto);
        }

        /// <summary>
        /// Returns the specified workpackage info
        /// </summary>
        public static WorkpackageInfo FetchById(Int32 id)
        {
            return DataPortal.Fetch<WorkpackageInfo>(new SingleCriteria<Int32>(id));
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, WorkpackageInfoDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Int32>(UserIdProperty, dto.UserId);
            this.LoadProperty<String>(UserFullNameProperty, dto.UserFullName);
            this.LoadProperty<WorkpackagePlanogramLocationType>(PlanogramLocationTypeProperty, (WorkpackagePlanogramLocationType)dto.PlanogramLocationType);
            this.LoadProperty<WorkpackageType>(WorkpackageTypeProperty, (WorkpackageType)dto.WorkpackageType);
            this.LoadProperty<Int32>(PlanogramCountProperty, dto.PlanogramCount);
            this.LoadProperty<Int32>(PlanogramPendingCountProperty, dto.PlanogramPendingCount);
            this.LoadProperty<Int32>(PlanogramQueuedCountProperty, dto.PlanogramQueuedCount);
            this.LoadProperty<Int32>(PlanogramProcessingCountProperty, dto.PlanogramProcessingCount);
            this.LoadProperty<Int32>(PlanogramCompleteCountProperty, dto.PlanogramCompleteCount);
            this.LoadProperty<Int32>(PlanogramFailedCountProperty, dto.PlanogramFailedCount);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<DateTime?>(DateCompletedProperty, dto.DateCompleted);
            this.LoadProperty<WorkpackageProcessingStatusType>(ProcessingStatusProperty, (WorkpackageProcessingStatusType)dto.ProcessingStatus);
            this.LoadProperty<String>(ProcessingStatusDescriptionProperty, dto.ProcessingStatusDescription);
            this.LoadProperty<Int32>(ProgressMaxProperty, dto.ProgressMax);
            this.LoadProperty<Int32>(ProgressCurrentProperty, dto.ProgressCurrent);
            this.LoadProperty<Single>(ProgressProperty, dto.ProgressMax == 0 ? 0 : (Single)dto.ProgressCurrent/(Single)dto.ProgressMax);
            this.LoadProperty<DateTime?>(ProgressDateStartedProperty, (dto.ProgressDateStarted.HasValue)? dto.ProgressDateStarted : dto.DateCreated);
            this.LoadProperty<DateTime?>(ProgressDateLastUpdatedProperty, dto.ProgressDateLastUpdated);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<Int32>(PlanogramWarningCountProperty, dto.PlanogramWarningCount);
            this.LoadProperty<Int32>(PlanogramTotalErrorScoreProperty, dto.PlanogramTotalErrorScore);
            this.LoadProperty<Int32>(PlanogramHighestErrorScoreProperty, dto.PlanogramHighestErrorScore);
            this.LoadProperty<String>(WorkflowNameProperty, dto.WorkflowName);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, WorkpackageInfoDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IWorkpackageInfoDal dal = dalContext.GetDal<IWorkpackageInfoDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.Value));
                }
            }
        }
        #endregion

        #endregion
    }
}
