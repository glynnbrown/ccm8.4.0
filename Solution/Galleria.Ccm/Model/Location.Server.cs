﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-25628 : A.Kuszyk
//		Created (Auto-generated)
// V8-25748 : K.Pickup
//      Changed data type of LocationId to Int16.
// CCM-25793 : N.Haywood
//      Added child update method
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class Location
    {
        #region Constructors
        private Location() { } //force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing Location with the given id
        /// </summary>
        public static Location FetchById(Int16 id)
        {
            return DataPortal.Fetch<Location>(new FetchByIdCriteria(id));
        }

        /// <summary>
        /// Returns an existing child location from the given dto.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        internal static Location FetchLocation(IDalContext dalContext, LocationDto dto)
        {
            return DataPortal.FetchChild<Location>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, LocationDto dto)
        {
            this.LoadProperty<Int16>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<Int32>(LocationGroupIdProperty, dto.LocationGroupId);
            this.LoadProperty<String>(CodeProperty, dto.Code);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(RegionProperty, dto.Region);
            this.LoadProperty<String>(TVRegionProperty, dto.TVRegion);
            this.LoadProperty<String>(LocationAttributeProperty, dto.Location);
            this.LoadProperty<String>(DefaultClusterAttributeProperty, dto.DefaultClusterAttribute);
            this.LoadProperty<String>(Address1Property, dto.Address1);
            this.LoadProperty<String>(Address2Property, dto.Address2);
            this.LoadProperty<String>(CityProperty, dto.City);
            this.LoadProperty<String>(CountyProperty, dto.County);
            this.LoadProperty<String>(StateProperty, dto.State);
            this.LoadProperty<String>(PostalCodeProperty, dto.PostalCode);
            this.LoadProperty<String>(CountryProperty, dto.Country);
            this.LoadProperty<Single?>(LongitudeProperty, dto.Longitude);
            this.LoadProperty<Single?>(LatitudeProperty, dto.Latitude);
            this.LoadProperty<DateTime?>(DateOpenProperty, dto.DateOpen);
            this.LoadProperty<DateTime?>(DateLastRefittedProperty, dto.DateLastRefitted);
            this.LoadProperty<DateTime?>(DateClosedProperty, dto.DateClosed);
            this.LoadProperty<Int16?>(CarParkSpacesProperty, dto.CarParkSpaces);
            this.LoadProperty<String>(CarParkManagementProperty, dto.CarParkManagement);
            this.LoadProperty<String>(PetrolForecourtTypeProperty, dto.PetrolForecourtType);
            this.LoadProperty<String>(RestaurantProperty, dto.Restaurant);
            this.LoadProperty<Int32?>(SizeGrossFloorAreaProperty, dto.SizeGrossFloorArea);
            this.LoadProperty<Int32?>(SizeNetSalesAreaProperty, dto.SizeNetSalesArea);
            this.LoadProperty<Int32?>(SizeMezzSalesAreaProperty, dto.SizeMezzSalesArea);
            this.LoadProperty<String>(TelephoneCountryCodeProperty, dto.TelephoneCountryCode);
            this.LoadProperty<String>(TelephoneAreaCodeProperty, dto.TelephoneAreaCode);
            this.LoadProperty<String>(TelephoneNumberProperty, dto.TelephoneNumber);
            this.LoadProperty<String>(FaxCountryCodeProperty, dto.FaxCountryCode);
            this.LoadProperty<String>(FaxAreaCodeProperty, dto.FaxAreaCode);
            this.LoadProperty<String>(FaxNumberProperty, dto.FaxNumber);
            this.LoadProperty<String>(OpeningHoursProperty, dto.OpeningHours);
            this.LoadProperty<Single?>(AverageOpeningHoursProperty, dto.AverageOpeningHours);
            this.LoadProperty<String>(ManagerNameProperty, dto.ManagerName);
            this.LoadProperty<String>(ManagerEmailProperty, dto.ManagerEmail);
            this.LoadProperty<String>(RegionalManagerNameProperty, dto.RegionalManagerName);
            this.LoadProperty<String>(RegionalManagerEmailProperty, dto.RegionalManagerEmail);
            this.LoadProperty<String>(DivisionalManagerNameProperty, dto.DivisionalManagerName);
            this.LoadProperty<String>(DivisionalManagerEmailProperty, dto.DivisionalManagerEmail);
            this.LoadProperty<String>(AdvertisingZoneProperty, dto.AdvertisingZone);
            this.LoadProperty<String>(DistributionCentreProperty, dto.DistributionCentre);
            this.LoadProperty<Byte?>(NoOfCheckoutsProperty, dto.NoOfCheckouts);
            this.LoadProperty<Boolean?>(IsMezzFittedProperty, dto.IsMezzFitted);
            this.LoadProperty<Boolean?>(IsFreeholdProperty, dto.IsFreehold);
            this.LoadProperty<Boolean?>(Is24HoursProperty, dto.Is24Hours);
            this.LoadProperty<Boolean?>(IsOpenMondayProperty, dto.IsOpenMonday);
            this.LoadProperty<Boolean?>(IsOpenTuesdayProperty, dto.IsOpenTuesday);
            this.LoadProperty<Boolean?>(IsOpenWednesdayProperty, dto.IsOpenWednesday);
            this.LoadProperty<Boolean?>(IsOpenThursdayProperty, dto.IsOpenThursday);
            this.LoadProperty<Boolean?>(IsOpenFridayProperty, dto.IsOpenFriday);
            this.LoadProperty<Boolean?>(IsOpenSaturdayProperty, dto.IsOpenSaturday);
            this.LoadProperty<Boolean?>(IsOpenSundayProperty, dto.IsOpenSunday);
            this.LoadProperty<Boolean?>(HasPetrolForecourtProperty, dto.HasPetrolForecourt);
            this.LoadProperty<Boolean?>(HasNewsCubeProperty, dto.HasNewsCube);
            this.LoadProperty<Boolean?>(HasAtmMachinesProperty, dto.HasAtmMachines);
            this.LoadProperty<Boolean?>(HasCustomerWCProperty, dto.HasCustomerWC);
            this.LoadProperty<Boolean?>(HasBabyChangingProperty, dto.HasBabyChanging);
            this.LoadProperty<Boolean?>(HasInStoreBakeryProperty, dto.HasInStoreBakery);
            this.LoadProperty<Boolean?>(HasHotFoodToGoProperty, dto.HasHotFoodToGo);
            this.LoadProperty<Boolean?>(HasRotisserieProperty, dto.HasRotisserie);
            this.LoadProperty<Boolean?>(HasFishmongerProperty, dto.HasFishmonger);
            this.LoadProperty<Boolean?>(HasButcherProperty, dto.HasButcher);
            this.LoadProperty<Boolean?>(HasPizzaProperty, dto.HasPizza);
            this.LoadProperty<Boolean?>(HasDeliProperty, dto.HasDeli);
            this.LoadProperty<Boolean?>(HasSaladBarProperty, dto.HasSaladBar);
            this.LoadProperty<Boolean?>(HasOrganicProperty, dto.HasOrganic);
            this.LoadProperty<Boolean?>(HasGroceryProperty, dto.HasGrocery);
            this.LoadProperty<Boolean?>(HasMobilePhonesProperty, dto.HasMobilePhones);
            this.LoadProperty<Boolean?>(HasDryCleaningProperty, dto.HasDryCleaning);
            this.LoadProperty<Boolean?>(HasHomeShoppingAvailableProperty, dto.HasHomeShoppingAvailable);
            this.LoadProperty<Boolean?>(HasOpticianProperty, dto.HasOptician);
            this.LoadProperty<Boolean?>(HasPharmacyProperty, dto.HasPharmacy);
            this.LoadProperty<Boolean?>(HasTravelProperty, dto.HasTravel);
            this.LoadProperty<Boolean?>(HasPhotoDepartmentProperty, dto.HasPhotoDepartment);
            this.LoadProperty<Boolean?>(HasCarServiceAreaProperty, dto.HasCarServiceArea);
            this.LoadProperty<Boolean?>(HasGardenCentreProperty, dto.HasGardenCentre);
            this.LoadProperty<Boolean?>(HasClinicProperty, dto.HasClinic);
            this.LoadProperty<Boolean?>(HasAlcoholProperty, dto.HasAlcohol);
            this.LoadProperty<Boolean?>(HasFashionProperty, dto.HasFashion);
            this.LoadProperty<Boolean?>(HasCafeProperty, dto.HasCafe);
            this.LoadProperty<Boolean?>(HasRecyclingProperty, dto.HasRecycling);
            this.LoadProperty<Boolean?>(HasPhotocopierProperty, dto.HasPhotocopier);
            this.LoadProperty<Boolean?>(HasLotteryProperty, dto.HasLottery);
            this.LoadProperty<Boolean?>(HasPostOfficeProperty, dto.HasPostOffice);
            this.LoadProperty<Boolean?>(HasMovieRentalProperty, dto.HasMovieRental);
            this.LoadProperty<Boolean?>(HasJewelleryProperty, dto.HasJewellery);
            this.LoadProperty<String>(LocationTypeProperty, dto.LocationType);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);

        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private LocationDto GetDataTransferObject()
        {
            return new LocationDto
            {
                Id = ReadProperty<Int16>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                LocationGroupId = ReadProperty<Int32>(LocationGroupIdProperty),
                Code = ReadProperty<String>(CodeProperty),
                Name = ReadProperty<String>(NameProperty),
                Region = ReadProperty<String>(RegionProperty),
                TVRegion = ReadProperty<String>(TVRegionProperty),
                Location = ReadProperty<String>(LocationAttributeProperty),
                DefaultClusterAttribute = ReadProperty<String>(DefaultClusterAttributeProperty),
                Address1 = ReadProperty<String>(Address1Property),
                Address2 = ReadProperty<String>(Address2Property),
                City = ReadProperty<String>(CityProperty),
                County = ReadProperty<String>(CountyProperty),
                State = ReadProperty<String>(StateProperty),
                PostalCode = ReadProperty<String>(PostalCodeProperty),
                Country = ReadProperty<String>(CountryProperty),
                Longitude = ReadProperty<Single?>(LongitudeProperty),
                Latitude = ReadProperty<Single?>(LatitudeProperty),
                DateOpen = ReadProperty<DateTime?>(DateOpenProperty),
                DateLastRefitted = ReadProperty<DateTime?>(DateLastRefittedProperty),
                DateClosed = ReadProperty<DateTime?>(DateClosedProperty),
                CarParkSpaces = ReadProperty<Int16?>(CarParkSpacesProperty),
                CarParkManagement = ReadProperty<String>(CarParkManagementProperty),
                PetrolForecourtType = ReadProperty<String>(PetrolForecourtTypeProperty),
                Restaurant = ReadProperty<String>(RestaurantProperty),
                SizeGrossFloorArea = ReadProperty<Int32?>(SizeGrossFloorAreaProperty),
                SizeNetSalesArea = ReadProperty<Int32?>(SizeNetSalesAreaProperty),
                SizeMezzSalesArea = ReadProperty<Int32?>(SizeMezzSalesAreaProperty),
                TelephoneCountryCode = ReadProperty<String>(TelephoneCountryCodeProperty),
                TelephoneAreaCode = ReadProperty<String>(TelephoneAreaCodeProperty),
                TelephoneNumber = ReadProperty<String>(TelephoneNumberProperty),
                FaxCountryCode = ReadProperty<String>(FaxCountryCodeProperty),
                FaxAreaCode = ReadProperty<String>(FaxAreaCodeProperty),
                FaxNumber = ReadProperty<String>(FaxNumberProperty),
                OpeningHours = ReadProperty<String>(OpeningHoursProperty),
                AverageOpeningHours = ReadProperty<Single?>(AverageOpeningHoursProperty),
                ManagerName = ReadProperty<String>(ManagerNameProperty),
                ManagerEmail = ReadProperty<String>(ManagerEmailProperty),
                RegionalManagerName = ReadProperty<String>(RegionalManagerNameProperty),
                RegionalManagerEmail = ReadProperty<String>(RegionalManagerEmailProperty),
                DivisionalManagerName = ReadProperty<String>(DivisionalManagerNameProperty),
                DivisionalManagerEmail = ReadProperty<String>(DivisionalManagerEmailProperty),
                AdvertisingZone = ReadProperty<String>(AdvertisingZoneProperty),
                DistributionCentre = ReadProperty<String>(DistributionCentreProperty),
                NoOfCheckouts = ReadProperty<Byte?>(NoOfCheckoutsProperty),
                IsMezzFitted = ReadProperty<Boolean?>(IsMezzFittedProperty),
                IsFreehold = ReadProperty<Boolean?>(IsFreeholdProperty),
                Is24Hours = ReadProperty<Boolean?>(Is24HoursProperty),
                IsOpenMonday = ReadProperty<Boolean?>(IsOpenMondayProperty),
                IsOpenTuesday = ReadProperty<Boolean?>(IsOpenTuesdayProperty),
                IsOpenWednesday = ReadProperty<Boolean?>(IsOpenWednesdayProperty),
                IsOpenThursday = ReadProperty<Boolean?>(IsOpenThursdayProperty),
                IsOpenFriday = ReadProperty<Boolean?>(IsOpenFridayProperty),
                IsOpenSaturday = ReadProperty<Boolean?>(IsOpenSaturdayProperty),
                IsOpenSunday = ReadProperty<Boolean?>(IsOpenSundayProperty),
                HasPetrolForecourt = ReadProperty<Boolean?>(HasPetrolForecourtProperty),
                HasNewsCube = ReadProperty<Boolean?>(HasNewsCubeProperty),
                HasAtmMachines = ReadProperty<Boolean?>(HasAtmMachinesProperty),
                HasCustomerWC = ReadProperty<Boolean?>(HasCustomerWCProperty),
                HasBabyChanging = ReadProperty<Boolean?>(HasBabyChangingProperty),
                HasInStoreBakery = ReadProperty<Boolean?>(HasInStoreBakeryProperty),
                HasHotFoodToGo = ReadProperty<Boolean?>(HasHotFoodToGoProperty),
                HasRotisserie = ReadProperty<Boolean?>(HasRotisserieProperty),
                HasFishmonger = ReadProperty<Boolean?>(HasFishmongerProperty),
                HasButcher = ReadProperty<Boolean?>(HasButcherProperty),
                HasPizza = ReadProperty<Boolean?>(HasPizzaProperty),
                HasDeli = ReadProperty<Boolean?>(HasDeliProperty),
                HasSaladBar = ReadProperty<Boolean?>(HasSaladBarProperty),
                HasOrganic = ReadProperty<Boolean?>(HasOrganicProperty),
                HasGrocery = ReadProperty<Boolean?>(HasGroceryProperty),
                HasMobilePhones = ReadProperty<Boolean?>(HasMobilePhonesProperty),
                HasDryCleaning = ReadProperty<Boolean?>(HasDryCleaningProperty),
                HasHomeShoppingAvailable = ReadProperty<Boolean?>(HasHomeShoppingAvailableProperty),
                HasOptician = ReadProperty<Boolean?>(HasOpticianProperty),
                HasPharmacy = ReadProperty<Boolean?>(HasPharmacyProperty),
                HasTravel = ReadProperty<Boolean?>(HasTravelProperty),
                HasPhotoDepartment = ReadProperty<Boolean?>(HasPhotoDepartmentProperty),
                HasCarServiceArea = ReadProperty<Boolean?>(HasCarServiceAreaProperty),
                HasGardenCentre = ReadProperty<Boolean?>(HasGardenCentreProperty),
                HasClinic = ReadProperty<Boolean?>(HasClinicProperty),
                HasAlcohol = ReadProperty<Boolean?>(HasAlcoholProperty),
                HasFashion = ReadProperty<Boolean?>(HasFashionProperty),
                HasCafe = ReadProperty<Boolean?>(HasCafeProperty),
                HasRecycling = ReadProperty<Boolean?>(HasRecyclingProperty),
                HasPhotocopier = ReadProperty<Boolean?>(HasPhotocopierProperty),
                HasLottery = ReadProperty<Boolean?>(HasLotteryProperty),
                HasPostOffice = ReadProperty<Boolean?>(HasPostOfficeProperty),
                HasMovieRental = ReadProperty<Boolean?>(HasMovieRentalProperty),
                HasJewellery = ReadProperty<Boolean?>(HasJewelleryProperty),
                LocationType = ReadProperty<String>(LocationTypeProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty),

            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning FetchById
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.Id));
                }
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, LocationDto dto)
        {
            LoadDataTransferObject(dalContext,dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                LocationDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int16>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<Location>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this instance
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                LocationDto dto = GetDataTransferObject();
                using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }

        /// <summary>
        /// Called when updating an object of this type
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent model</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            LocationDto dto = GetDataTransferObject();
            using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
            {
                dal.Update(dto);
            }
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when this instance is being deleted
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #endregion

    }
}