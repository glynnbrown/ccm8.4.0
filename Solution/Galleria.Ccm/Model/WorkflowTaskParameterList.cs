﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion
#region Version History: CCM820
// V8-30596 : N.Foster
//  Resolved issue where duplicate tasks in same workflow resulted in exception
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class WorkflowTaskParameterList : ModelList<WorkflowTaskParameterList, WorkflowTaskParameter>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent workflow task
        /// </summary>
        public new WorkflowTask Parent
        {
            get { return (WorkflowTask)base.Parent; }
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(WorkflowTaskParameterList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskParameterList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.WorkflowCreate.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskParameterList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.WorkflowEdit.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskParameterList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.WorkflowDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        internal static WorkflowTaskParameterList NewWorkflowTaskParameterList(EngineTaskInfo task)
        {
            WorkflowTaskParameterList item = new WorkflowTaskParameterList();
            item.Create(task);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected void Create(EngineTaskInfo task)
        {
            this.RaiseListChangedEvents = false;
            foreach (EngineTaskParameterInfo parameter in task.Parameters)
            {
                this.Add(WorkflowTaskParameter.NewWorkflowTaskParameter(task, parameter));
            }
            this.RaiseListChangedEvents = true;
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Creates a new task based on the specified engine task type
        /// </summary>
        public WorkflowTaskParameter Add(EngineTaskInfo task, EngineTaskParameterInfo parameter)
        {
            WorkflowTaskParameter item = WorkflowTaskParameter.NewWorkflowTaskParameter(task, parameter);
            this.Add(item);
            return item;
        }
        #endregion
    }
}
