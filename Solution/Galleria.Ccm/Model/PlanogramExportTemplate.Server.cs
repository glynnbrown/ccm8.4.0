﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
//  Refactored PlanogramFileType to Framework.Plangoram.Model and renamed PlanogramExportFileType
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramExportTemplate
    {
        #region Constructor
        private PlanogramExportTemplate() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing PlanogramExportTemplate with the given string
        /// </summary>
        public static PlanogramExportTemplate FetchByFilename(String fileName)
        {
            return FetchByFilename(fileName, /*asReadOnly*/false);
        }

        /// <summary>
        /// Returns an existing PlanogramExportTemplate with the given string
        /// </summary>
        public static PlanogramExportTemplate FetchByFilename(String fileName, Boolean asReadOnly)
        {
            return DataPortal.Fetch<PlanogramExportTemplate>(new FetchByIdCriteria(PlanogramExportTemplateDalType.FileSystem, fileName, asReadOnly));
        }

        /// <summary>
        /// Returns an existing PlanogramExportTemplate with the given entity id and name.
        /// </summary>
        public static PlanogramExportTemplate FetchByEntityIdName(Int32 entityId, String name)
        {
            return DataPortal.Fetch<PlanogramExportTemplate>(new FetchByEntityIdNameCriteria(entityId, name));
        }

        /// <summary>
        /// Returns an existing PlanogramExportTemplate with the given id
        /// For unit testing purposes.
        /// </summary>
        public static PlanogramExportTemplate FetchById(Object id)
        {
            return DataPortal.Fetch<PlanogramExportTemplate>(new FetchByIdCriteria(PlanogramExportTemplateDalType.Unknown, id));
        }
        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramExportTemplateDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<PlanogramExportFileType>(FileTypeProperty, (PlanogramExportFileType)dto.FileType);
            this.LoadProperty<String>(FileVersionProperty, dto.FileVersion);
            this.LoadProperty<PlanogramExportTemplateMappingList>(MappingsProperty, PlanogramExportTemplateMappingList.FetchByParentId(dalContext, dto.Id));
            this.LoadProperty<PlanogramExportTemplatePerformanceMetricList>(PerformanceMetricsProperty, PlanogramExportTemplatePerformanceMetricList.FetchByParentId(dalContext, dto.Id));
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private PlanogramExportTemplateDto GetDataTransferObject()
        {
            return new PlanogramExportTemplateDto()
            {
                Id = ReadProperty<Object>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                Name = ReadProperty<String>(NameProperty),
                FileType = (Byte)ReadProperty<PlanogramExportFileType>(FileTypeProperty),
                FileVersion = (String)ReadProperty<String>(FileVersionProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching a package by its Id
        /// </summary>
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            //lock the file first
            if (criteria.DalType == PlanogramExportTemplateDalType.FileSystem
                && !criteria.AsReadOnly)
            {
                LockPlanogramExportTemplateByFileName((String)criteria.Id);
            }

            IDalFactory dalFactory = this.GetDalFactory(criteria.DalType);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.Id));
                }
            }
        }

        /// <summary>
        /// Called when fetching a package by entityId and Name
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdNameCriteria criteria)
        {
            IDalFactory dalFactory = GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchByEntityIdName(criteria.EntityId, criteria.Name));
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                PlanogramExportTemplateDto dto = this.GetDataTransferObject();
                Object oldId = dto.Id;
                using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Object>(IdProperty, dto.Id);
                this.LoadProperty(RowVersionProperty, dto.RowVersion);
                this.LoadProperty(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<PlanogramExportTemplate>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                PlanogramExportTemplateDto dto = this.GetDataTransferObject();
                if (this.IsSelfDirty)
                {
                    using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                    {
                        dal.Update(dto);
                    }
                }
                this.LoadProperty(RowVersionProperty, dto.RowVersion);
                this.LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #endregion

        #region Commands

        #region LockPlanogramExportTemplateCommand
        /// <summary>
        /// Performs the locking of an export tempalte
        /// </summary>
        private partial class LockPlanogramExportTemplateCommand : CommandBase<LockPlanogramExportTemplateCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.DalFactoryType, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                    {
                        dal.LockById(this.Id);
                    }
                }
            }
            #endregion

            #endregion
        }
        #endregion

        #region UnlockPlanogramExportTemplateCommand
        /// <summary>
        /// Performs the unlocking of a PlanogramExportTemplate
        /// </summary>
        private partial class UnlockPlanogramExportTemplateCommand : CommandBase<UnlockPlanogramExportTemplateCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.DalFactoryType, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IPlanogramExportTemplateDal dal = dalContext.GetDal<IPlanogramExportTemplateDal>())
                    {
                        dal.UnlockById(this.Id);
                    }
                }
            }
            #endregion

            #endregion
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the correct dal factory based on the daltype and args.
        /// </summary>
        /// <param name="dalType"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private IDalFactory GetDalFactory(PlanogramExportTemplateDalType dalType)
        {
            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(dalType, out dalFactoryName);
            this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            this.LoadProperty<PlanogramExportTemplateDalType>(DalFactoryTypeProperty, dalType);

            return dalFactory;
        }

        /// <summary>
        /// Returns the correct dal factory for this instance
        /// </summary>
        protected override IDalFactory GetDalFactory()
        {
            if (String.IsNullOrEmpty(this.DalFactoryName))
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.DalFactoryType, out dalFactoryName);
                this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            }

            return DalContainer.GetDalFactory(this.DalFactoryName);
        }

        /// <summary>
        /// Returns the correct dal factory based on the given type and args.
        /// </summary>
        private static IDalFactory GetDalFactory(PlanogramExportTemplateDalType dalType, out String dalFactoryName)
        {
            switch (dalType)
            {
                case PlanogramExportTemplateDalType.FileSystem:
                    dalFactoryName = Constants.UserDal;
                    return DalContainer.GetDalFactory(dalFactoryName);

                default:
                    dalFactoryName = DalContainer.DalName;
                    return DalContainer.GetDalFactory();
            }

        }
        #endregion
    }
}
