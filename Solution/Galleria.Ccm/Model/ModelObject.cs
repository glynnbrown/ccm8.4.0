﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-24801 : L.Hodson
//  Created
#endregion
#region Version History: CCM830
// CCM-32115 : L.Ineson
//  Added child changing event handler.
#endregion
#endregion

using System;
using System.Collections;
using Csla.Core;
using Csla.Rules;
using Galleria.Framework.Dal;
using System.ComponentModel;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a base model class from
    /// which the other model objects in
    /// this assembly will inherit
    /// </summary>
    [Serializable]
    public abstract partial class ModelObject<T> : Galleria.Framework.Model.ModelObject<T>, IModelObject, INotifyChildChanging
        where T : ModelObject<T>
    {
        #region Properties

        #region DalFactoryName
        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        String IModelObject.DalFactoryName
        {
            get { return this.DalFactoryName; }
        }

        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        protected virtual String DalFactoryName
        {
            get
            {
                // attempt to get the name from a parent model object
                IModelObject modelObject = this.Parent as IModelObject;
                if (modelObject != null) return modelObject.DalFactoryName;

                // attempt to get the name from a parent model list
                IModelList modelList = this.Parent as IModelList;
                if (modelList != null) return modelList.DalFactoryName;

                return null;
            }
        }
        #endregion

        //#region Id
        ///// <summary>
        ///// Id property definition
        ///// </summary>
        //public static readonly ModelPropertyInfo<Object> IdProperty =
        //    RegisterModelProperty<Object>(c => c.Id);
        ///// <summary>
        ///// Returns the unique package id
        ///// </summary>
        //public Object Id
        //{
        //    get { return this.GetProperty<Object>(IdProperty); }
        //    protected set { SetProperty<Object>(IdProperty, value); }
        //}
        //#endregion

        #endregion

        #region Methods
        /// <summary>
        /// Returns the correct dal factory
        /// </summary>
        protected virtual IDalFactory GetDalFactory()
        {
            return DalContainer.GetDalFactory(this.DalFactoryName);
        }

        /// <summary>
        /// Returns the specified dal factory
        /// </summary>
        protected virtual IDalFactory GetDalFactory(String dalName)
        {
            return DalContainer.GetDalFactory(dalName);
        }

        /// <summary>
        /// Helper method to return all broken rules in the 
        /// object graph.
        /// </summary>
        /// <returns></returns>
        public BrokenRulesCollection GetAllBrokenRules()
        {
            BrokenRulesCollection list = new BrokenRulesCollection();

            if (FieldManager.HasFields)
            {
                // add rules for this.
                list.AddRange(this.BrokenRulesCollection);

                //check child objects
                foreach (object child in this.FieldManager.GetChildren())
                {
                    if ((child is IModelObject) && (child as IEditableBusinessObject != null) && (((IEditableBusinessObject)child).IsValid == false))
                    {
                        //add child object rules and recurse
                        list.AddRange(((IModelObject)child).GetAllBrokenRules());
                    }
                    else
                    {
                        //cycle through child list objects
                        if (child is IEditableCollection)
                        {
                            foreach (Object childObject in (IEnumerable)child)
                            {
                                if (childObject is IModelObject)
                                {
                                    list.AddRange((childObject as IModelObject).GetAllBrokenRules());
                                }
                            }
                        }
                    }
                }

            }

            return list;
        }

        /// <summary>
        /// Hook into child object events.
        /// </summary>
        protected override void OnAddEventHooks(IBusinessObject child)
        {
            base.OnAddEventHooks(child);


            INotifyPropertyChanging pc = child as INotifyPropertyChanging;
            if (pc != null)
            {
                pc.PropertyChanging += Child_PropertyChanging;
            }

            INotifyChildChanging cc = child as INotifyChildChanging;
            if (cc != null)
            {
                cc.ChildChanging += Child_Changing;
            }
        }


        /// <summary>
        /// Unhook child object events.
        /// </summary>
        protected override void OnRemoveEventHooks(IBusinessObject child)
        {
            base.OnRemoveEventHooks(child);

            INotifyPropertyChanging pc = child as INotifyPropertyChanging;
            if (pc != null)
            {
                pc.PropertyChanging -= Child_PropertyChanging;
            }

            INotifyChildChanging cc = child as INotifyChildChanging;
            if (cc != null)
            {
                cc.ChildChanging -= Child_Changing;
            }
        }

        /// <summary>
        /// Override of the get id value method
        /// </summary>
        /// <returns></returns>
        protected override object GetIdValue()
        {
            try
            {
                var pInfo = this.GetType().GetProperty("Id");
                if (pInfo != null)
                {
                    return pInfo.GetValue(this, null);
                }
            }
            catch (Exception)
            { }

            return base.GetIdValue();
        }

        #endregion

        #region Child Changing Notification

        //TODO: This needs moving into the framework model object.

        [NonSerialized]
        [Csla.NotUndoable]
        private EventHandler<ChildChangingEventArgs> _childChangingHandlers;

        /// <summary>
        /// Event raised when a child object is about to change.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:ValidateArgumentsOfPublicMethods")]
        public event EventHandler<ChildChangingEventArgs> ChildChanging
        {
            add
            {
                _childChangingHandlers = (EventHandler<ChildChangingEventArgs>)
                  System.Delegate.Combine(_childChangingHandlers, value);
            }
            remove
            {
                _childChangingHandlers = (EventHandler<ChildChangingEventArgs>)
                  System.Delegate.Remove(_childChangingHandlers, value);
            }
        }

        /// <summary>
        /// Raises the child changing event to indicate that a child object is about to change.
        /// </summary>
        /// <param name="e">ChildChangingEventArgs object</param>
        protected virtual void OnChildChanging(ChildChangingEventArgs e)
        {
            if (_childChangingHandlers != null)
                _childChangingHandlers.Invoke(this, e);
        }

        /// <summary>
        /// Creates a ChildChangingEventArgs and raises the event. 
        /// </summary>
        private void RaiseChildChanging(object childObject, PropertyChangingEventArgs propertyArgs)
        {
            ChildChangingEventArgs args = new ChildChangingEventArgs(childObject, propertyArgs);
            OnChildChanging(args);
        }

        /// <summary>
        /// Raises the event
        /// </summary>
        private void RaiseChildChanging(ChildChangingEventArgs e)
        {
            OnChildChanging(e);
        }

        /// <summary>
        /// Handles any PropertyChanging event from a child object and echoes it up as a
        /// ChildChanging event.
        /// </summary>
        private void Child_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            RaiseChildChanging(sender, e);
        }

        /// <summary>
        /// Handles any ChildChanging event from a child object and echoes it up as a
        /// ChildChanging event.
        /// </summary>
        private void Child_Changing(object sender, ChildChangingEventArgs e)
        {
            RaiseChildChanging(e);
        }

        #endregion
    }


}
