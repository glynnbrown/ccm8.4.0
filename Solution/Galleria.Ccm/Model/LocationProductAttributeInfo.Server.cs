﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
#endregion
#region Version History: (CCM 8.03)
//V8-29267 : L.Ineson
//  Removed date deleted.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Csla;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public partial class LocationProductAttributeInfo
    {
        #region Constructor
        private LocationProductAttributeInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates the item from the given dto
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal static LocationProductAttributeInfo GetLocationProductAttributeInfo(IDalContext dalContext, LocationProductAttributeInfoDto dto)
        {
            return DataPortal.FetchChild<LocationProductAttributeInfo>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private LocationProductAttributeInfoDto GetDataTransferObject()
        {
            return new LocationProductAttributeInfoDto
            {
                ProductId = ReadProperty<Int32>(ProductIdProperty),
                LocationId = ReadProperty<Int16>(LocationIdProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                GTIN = ReadProperty<String>(GTINProperty),
                LocationName = ReadProperty<String>(LocationNameProperty),
                ProductName = ReadProperty<String>(ProductNameProperty),
                CasePackUnits = ReadProperty<Int16?>(CasePackUnitsProperty),
                DaysOfSupply = ReadProperty<Byte?>(DaysOfSupplyProperty),
                DeliveryFrequencyDays = ReadProperty<Single?>(DeliveryFrequencyDaysProperty),
                ShelfLife = ReadProperty<Int16?>(ShelfLifeProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
            };
        }


        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, LocationProductAttributeInfoDto dto)
        {
            LoadProperty<Int32>(ProductIdProperty, dto.ProductId);
            LoadProperty<Int16>(LocationIdProperty, dto.LocationId);
            LoadProperty<String>(LocationNameProperty, dto.LocationName);
            LoadProperty<String>(ProductNameProperty, dto.ProductName);
            LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            LoadProperty<String>(GTINProperty, dto.GTIN);
            LoadProperty<Int16?>(CasePackUnitsProperty, dto.CasePackUnits);
            LoadProperty<Byte?>(DaysOfSupplyProperty, dto.DaysOfSupply);
            LoadProperty<Single?>(DeliveryFrequencyDaysProperty, dto.DeliveryFrequencyDays);
            LoadProperty<Int16?>(ShelfLifeProperty, dto.ShelfLife);
            LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning an existing object
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, LocationProductAttributeInfoDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #endregion
    }
}
