﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion

#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a File Info within the solution
    /// </summary>
    public partial class FileInfo
    {
        #region Constructor
        private FileInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a File Info from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        /// <returns>An File Info object</returns>
        public static FileInfo GetFileInfo(Int32 id)
        {
            return DataPortal.Fetch<FileInfo>(new SingleCriteria<Int32>(id));
        }
        #endregion

        #region Data Access

        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, FileInfoDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(SourceFilePathProperty, dto.SourceFilePath);
            this.LoadProperty<Int64>(SizeInBytesProperty, dto.SizeInBytes);
            this.LoadProperty<DateTime>(DateModifiedProperty, dto.DateLastModified);
            this.LoadProperty<Int32?>(UserIdProperty, dto.UserId);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IFileInfoDal dal = dalContext.GetDal<IFileInfoDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.Value));
                }
            }
        }
        #endregion

        #endregion
    }
}