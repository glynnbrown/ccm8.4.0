﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
#endregion
#endregion


using System;
using System.Collections.Generic;
using Galleria.Ccm.Helpers;
using Galleria.Framework.Model;
using Csla.Rules;
using Galleria.Ccm.Security;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// AssortmentMinorRevisionAmendDistributionAction Model object
    /// (Child of AssortmentMinorRevision)
    /// This is a child Content object and so it has the following properties/actions:
    /// - This object does NOT have a DateDeleted property or DateDeleted field in its supporting database table
    /// - if its parent is deleted, it is not removed or marked as deleted (this allows for parent 'undeletes')
    /// - if it is deleted directly, it's associated record is removed from the database. 
    [Serializable]
    [DefaultNewMethod("NewAssortmentMinorRevisionAmendDistributionAction")]
    public sealed partial class AssortmentMinorRevisionAmendDistributionAction : ModelObject<AssortmentMinorRevisionAmendDistributionAction>, IAssortmentMinorRevisionAction
    {
        #region Static Constructor
        static AssortmentMinorRevisionAmendDistributionAction()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The item unique id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<AssortmentMinorRevisionActionType> TypeProperty =
            RegisterModelProperty<AssortmentMinorRevisionActionType>(c => c.Type);
        public AssortmentMinorRevisionActionType Type
        {
            get { return AssortmentMinorRevisionActionType.AmendDistribution; }
        }

        public static readonly ModelPropertyInfo<String> ProductGtinProperty =
       RegisterModelProperty<String>(c => c.ProductGtin);
        public String ProductGtin
        {
            get { return GetProperty<String>(ProductGtinProperty); }
            set { SetProperty<String>(ProductGtinProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> ProductNameProperty =
       RegisterModelProperty<String>(c => c.ProductName);
        public String ProductName
        {
            get { return GetProperty<String>(ProductNameProperty); }
            set { SetProperty<String>(ProductNameProperty, value); }
        }


        public static readonly ModelPropertyInfo<Int32?> ProductIdProperty =
       RegisterModelProperty<Int32?>(c => c.ProductId);
        public Int32? ProductId
        {
            get { return GetProperty<Int32?>(ProductIdProperty); }
            set { SetProperty<Int32?>(ProductIdProperty, value); }
        }


        public static readonly ModelPropertyInfo<Int32> PriorityProperty =
       RegisterModelProperty<Int32>(c => c.Priority);
        public Int32 Priority
        {
            get { return GetProperty<Int32>(PriorityProperty); }
            set { SetProperty<Int32>(PriorityProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> CommentsProperty =
       RegisterModelProperty<String>(c => c.Comments);
        public String Comments
        {
            get { return GetProperty<String>(CommentsProperty); }
            set { SetProperty<String>(CommentsProperty, value); }
        }

        public static readonly ModelPropertyInfo<AssortmentMinorRevisionAmendDistributionActionLocationList> LocationsProperty =
            RegisterModelProperty<AssortmentMinorRevisionAmendDistributionActionLocationList>(c => c.Locations);
        /// <summary>
        /// The action locations
        /// </summary>
        public AssortmentMinorRevisionAmendDistributionActionLocationList Locations
        {
            get { return GetProperty<AssortmentMinorRevisionAmendDistributionActionLocationList>(LocationsProperty); }
        }

        
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionAmendDistributionAction), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.AssortmentMinorRevisionCreate.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionAmendDistributionAction), new IsInRole(AuthorizationActions.GetObject, DomainPermission.AssortmentMinorRevisionGet.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionAmendDistributionAction), new IsInRole(AuthorizationActions.EditObject, DomainPermission.AssortmentMinorRevisionEdit.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionAmendDistributionAction), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.AssortmentMinorRevisionDelete.ToString()));
        }
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(ProductGtinProperty));
            BusinessRules.AddRule(new MaxLength(ProductGtinProperty, 14));
            BusinessRules.AddRule(new Required(ProductNameProperty));
            BusinessRules.AddRule(new MaxLength(ProductNameProperty, 100));
            BusinessRules.AddRule(new MinValue<Int32>(PriorityProperty, 1));
            BusinessRules.AddRule(new MaxLength(CommentsProperty, 1000));

        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentMinorRevisionAmendDistributionAction NewAssortmentMinorRevisionAmendDistributionAction()
        {
            AssortmentMinorRevisionAmendDistributionAction item = new AssortmentMinorRevisionAmendDistributionAction();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentMinorRevisionAmendDistributionAction NewAssortmentMinorRevisionAmendDistributionAction(Int32 productId, String gtin, String name, Int32 priority)
        {
            AssortmentMinorRevisionAmendDistributionAction item = new AssortmentMinorRevisionAmendDistributionAction();
            item.Create(productId, gtin, name, priority);
            return item;
        }

        #endregion

        #region IAssortmentMinorRevisionAction Methods

        /// <summary>
        /// Implements the GetIAssortmentMinorRevisionActionLocations interface method.
        /// This gets the Location list as its IAssortmentMinorRevisionActionLocation interface collection
        /// so that the locations can be accessed when listing different Actions together.
        /// </summary>
        /// <returns></returns>   
        public ICollection<IAssortmentMinorRevisionActionLocation> GetIAssortmentMinorRevisionActionLocations()
        {
            //return the Location list object directly so that add/remove etc commands will take effect
            return Locations;
        }
        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<AssortmentMinorRevisionAmendDistributionActionLocationList>(LocationsProperty, AssortmentMinorRevisionAmendDistributionActionLocationList.NewAssortmentMinorRevisionAmendDistributionActionLocationList());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create(Int32 productId, String gtin, String name, Int32 priority)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<AssortmentMinorRevisionAmendDistributionActionLocationList>(LocationsProperty, AssortmentMinorRevisionAmendDistributionActionLocationList.NewAssortmentMinorRevisionAmendDistributionActionLocationList());
            this.LoadProperty<Int32?>(ProductIdProperty, productId);
            this.LoadProperty<String>(ProductGtinProperty, gtin);
            this.LoadProperty<String>(ProductNameProperty, name);
            this.LoadProperty<Int32>(PriorityProperty, priority);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion
    }
}
