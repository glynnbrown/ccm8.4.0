#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26076 : A.Silva ~ Removed Id property as identifier, user Grouping Property instead.

#endregion

#region Version History: (CCM 8.3)
// V8-31542 : L.Ineson
//  Added Id and updated implementation.
#endregion


#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public sealed partial class CustomColumnGroup
    {
        #region Constructor
        private CustomColumnGroup(){}//force use of factory methods.
        #endregion

        #region Factory Methods

        /// <summary>
        ///     Returns a existing <see cref="CustomColumnGroup" />.
        /// </summary>
        /// <param name="dalContext">DAL context to access the data.</param>
        /// <param name="dto"></param>
        /// <returns>
        ///     Returns an existing <see cref="CustomColumnGroup" />.
        /// </returns>
        public static CustomColumnGroup FetchCustomColumnGroup(IDalContext dalContext, CustomColumnGroupDto dto)
        {
            return DataPortal.FetchChild<CustomColumnGroup>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Returns a new <see cref="CustomColumnGroupDto" /> with this instance's data.
        /// </summary>
        /// <returns>A new dto</returns>
        private CustomColumnGroupDto GetDataTransferObject()
        {
            return new CustomColumnGroupDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                Grouping = ReadProperty(GroupingProperty),
                CustomColumnLayoutId = Parent.Id
            };
        }

        /// <summary>
        ///     Loads the data from a <see cref="CustomColumnGroupDto" /> in this instance.
        /// </summary>
        /// <param name="dalContext">DAL context to access the data.</param>
        /// <param name="dto">The <see cref="CustomColumnGroupDto" /> to load data from.</param>
        private void LoadDataTransferObject(IDalContext dalContext, CustomColumnGroupDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty(GroupingProperty, dto.Grouping);
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Called via reflection when retrieving a <see cref="CustomColumnGroup" />.
        /// </summary>
        /// <param name="dalContext">DAL context to access the data.</param>
        /// <param name="dto"><see cref="CustomColumnGroupDto" /> containing the data to load into this instance.</param>
        private void Child_Fetch(IDalContext dalContext, CustomColumnGroupDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Called via reflection when inserting a <see cref="CustomColumnGroup" /> in a data store.
        /// </summary>
        /// <param name="dalContext">DAL context to access the data.</param>
        private void Child_Insert(IDalContext dalContext)
        {
            using (ICustomColumnGroupDal dal = dalContext.GetDal<ICustomColumnGroupDal>())
            {
                CustomColumnGroupDto dto = GetDataTransferObject();
                dal.Insert(dto);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Update

        /// <summary>
        ///     Called via reflection when updating a <see cref="CustomColumnGroup" /> in a data store.
        /// </summary>
        /// <param name="dalContext">DAL context to access the data.</param>
        private void Child_Update(IDalContext dalContext)
        {
            using (ICustomColumnGroupDal dal = dalContext.GetDal<ICustomColumnGroupDal>())
            {
                CustomColumnGroupDto dto = GetDataTransferObject();
                dal.Update(dto);

                LoadProperty<Int32>(IdProperty, dto.Id);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Called via reflection when deleting the <see cref="CustomColumnGroup" /> from a data store.
        /// </summary>
        /// <param name="dalContext">DAL context to access the data.</param>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (ICustomColumnGroupDal dal = dalContext.GetDal<ICustomColumnGroupDal>())
            {
                dal.DeleteById(this.Id);
            }
        }

        #endregion

        #endregion
    }
}