﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
// V8-25886 : L.Ineson
//  Removed value, added values
#endregion
#region Version History: CCM810
// V8-29858 : M.Pettit
//  ParameterValues list property registration must be marked as lazy loaded.
//  Added OnCopy() method implementation
#endregion
#region Version History: CCM820
// V8-30596 : N.Foster
//  Resolved issue where duplicate tasks in same workflow resulted in exception
// V8-30771 : N.Foster
//  Add support for additional task parameter properties
#endregion
#region Version History: CCM820
// V8-31830 : A.Probyn
//   Added AddValues helper method
#endregion
#region Version History: CCM830
// V8-32133 : A.Probyn
//   Updated AddValues helper method
// V8-32639 : A.Silva
//  Pulled in ListRelatedParameters method from PerformanceSelectionNameTaskParameterHelper.

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class WorkpackagePlanogramParameter : ModelObject<WorkpackagePlanogramParameter>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent planogram
        /// </summary>
        public new WorkpackagePlanogram Parent
        {
            get { return ((WorkpackagePlanogramParameterList)base.Parent).Parent; }
        }
        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns unique parameter id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region WorkflowTaskId
        /// <summary>
        /// WorkflowTaskId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> WorkflowTaskIdProperty =
            RegisterModelProperty<Int32>(c => c.WorkflowTaskId);
        /// <summary>
        /// Returns the workflow task  id
        /// </summary>
        public Int32 WorkflowTaskId
        {
            get { return this.GetProperty<Int32>(WorkflowTaskIdProperty); }
        }
        #endregion

        #region WorkflowTaskParameterId
        /// <summary>
        /// WorkflowTaskParameterId property
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> WorkflowTaskParameterIdProperty =
            RegisterModelProperty<Int32>(c => c.WorkflowTaskParameterId);
        /// <summary>
        /// Returns the workflow task id property
        /// </summary>
        public Int32 WorkflowTaskParameterId
        {
            get { return this.GetProperty<Int32>(WorkflowTaskParameterIdProperty); }
        }
        #endregion

        #region TaskDetails
        /// <summary>
        /// TaskDetails property definition
        /// </summary>
        public static readonly ModelPropertyInfo<EngineTaskInfo> TaskDetailsProperty =
            RegisterModelProperty<EngineTaskInfo>(c => c.TaskDetails);
        /// <summary>
        /// Returns the task details
        /// </summary>
        public EngineTaskInfo TaskDetails
        {
            get { return this.GetProperty<EngineTaskInfo>(TaskDetailsProperty); }
        }
        #endregion

        #region ParameterDetails
        /// <summary>
        /// ParameterDetails property definition
        /// </summary>
        public static readonly ModelPropertyInfo<EngineTaskParameterInfo> ParameterDetailsProperty =
            RegisterModelProperty<EngineTaskParameterInfo>(c => c.ParameterDetails);
        /// <summary>
        /// Returns the task parameter details
        /// </summary>
        public EngineTaskParameterInfo ParameterDetails
        {
            get { return this.GetProperty<EngineTaskParameterInfo>(ParameterDetailsProperty); }
        }
        #endregion

        #region Values
        /// <summary>
        /// Values property definition
        /// </summary>
        public static readonly ModelPropertyInfo<WorkpackagePlanogramParameterValueList> ValuesProperty =
            RegisterModelProperty<WorkpackagePlanogramParameterValueList>(c => c.Values, RelationshipTypes.LazyLoad);
        /// <summary>
        /// Synchornously returns all parameter values
        /// </summary>
        public WorkpackagePlanogramParameterValueList Values
        {
            get
            {
                return this.GetPropertyLazy<WorkpackagePlanogramParameterValueList>(
                    ValuesProperty,
                    new WorkpackagePlanogramParameterValueList.FetchByWorkpackgePlanogramParameterIdCriteria(this.Id),
                    true);
            }
        }

        /// <summary>
        /// Asychronously returns all parameter values
        /// </summary>
        public WorkpackagePlanogramParameterValueList ValuesAsync
        {
            get
            {
                return this.GetPropertyLazy<WorkpackagePlanogramParameterValueList>(
                    ValuesProperty,
                    new WorkpackagePlanogramParameterValueList.FetchByWorkpackgePlanogramParameterIdCriteria(this.Id),
                    false);
            }
        }
        #endregion

        #region Helper Properties

        /// <summary>
        /// Helper property to Get/Set the value
        /// where this is a single value parameter type.
        /// </summary>
        public Object Value
        {
            get
            {
                if (this.Values.Count > 0)
                {
                    return this.Values.First().Value1;
                }
                return null;
            }

            set
            {
                this.Values.Clear();

                if (value != null)
                {
                    var paramValue = WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue();
                    paramValue.Value1 = value;

                    this.Values.Add(paramValue);
                }
                OnPropertyChanged("Value");
            }
        }

        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(WorkpackagePlanogramParameter), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramParameter), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.WorkpackageCreate.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramParameter), new IsInRole(AuthorizationActions.EditObject, DomainPermission.WorkpackageEdit.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramParameter), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.WorkpackageDelete.ToString()));
        }
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new workpackage planogram parameter
        /// </summary>
        internal static WorkpackagePlanogramParameter NewWorkpackagePlanogramParameter(WorkflowTaskParameter parameter)
        {
            WorkpackagePlanogramParameter item = new WorkpackagePlanogramParameter();
            item.Create(parameter);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected void Create(WorkflowTaskParameter parameter)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(WorkflowTaskIdProperty, parameter.Parent.Id);
            this.LoadProperty<Int32>(WorkflowTaskParameterIdProperty, parameter.Id);
            this.LoadProperty<EngineTaskInfo>(TaskDetailsProperty, parameter.TaskDetails.Copy());
            this.LoadProperty<EngineTaskParameterInfo>(ParameterDetailsProperty, parameter.ParameterDetails.Copy());
            this.LoadProperty<WorkpackagePlanogramParameterValueList>(ValuesProperty, WorkpackagePlanogramParameterValueList.NewWorkpackagePlanogramParameterValueList(parameter.Values));
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<WorkpackagePlanogramParameter>(oldId, newId);
        }

        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Returns the related parent workpackage parameter
        /// </summary>
        public WorkpackagePlanogramParameter GetParentParameter()
        {
            return this.Parent.Parameters.FirstOrDefault(p =>
                (p.WorkflowTaskId == this.WorkflowTaskId) &&
                (p.ParameterDetails.Id == this.ParameterDetails.ParentId));
        }

        /// <summary>
        /// Returns the related child workpackage parameters
        /// </summary>
        public IEnumerable<WorkpackagePlanogramParameter> GetChildParameters()
        {
            return this.Parent.Parameters.Where(p =>
                (p.WorkflowTaskId == this.WorkflowTaskId) &&
                (p.ParameterDetails.ParentId == this.ParameterDetails.Id));
        }

        /// <summary>
        /// Indicates if a value is required for this parameter. Returns null
        /// if a value is required, else returns a description of why the
        /// parameter is not required
        /// </summary>
        public String IsRequired(WorkflowTaskParameter workflowTaskParameter)
        {
            using (TaskBase task = this.TaskDetails.CreateTask())
            {
                task.InitializeParameters(workflowTaskParameter.Parent, this.Parent);
                TaskParameter taskParameter = task.Parameters.FirstOrDefault(p => p.Id == this.ParameterDetails.Id);
                if (taskParameter != null)
                {
                    return taskParameter.IsRequired;
                }
            }
            return null;
        }


        /// <summary>
        /// Indicates if this parameter is allowed to have a null value or not
        /// </summary>
        public Boolean IsNullAllowed(WorkflowTaskParameter workflowTaskParameter)
        {
            using (TaskBase task = this.TaskDetails.CreateTask())
            {
                task.InitializeParameters(workflowTaskParameter.Parent, this.Parent);
                TaskParameter taskParameter = task.Parameters.FirstOrDefault(p => p.Id == this.ParameterDetails.Id);
                if (taskParameter != null)
                {
                    return taskParameter.IsNullAllowed;
                }
            }
            return false;
        }

        /// <summary>
        /// Indicates if the parameter is valid of not. If valid then null is returned
        /// else a description is returned as to why the parameter is not valid
        /// </summary>
        public String IsValid(WorkflowTaskParameter workflowTaskParameter)
        {
            using (TaskBase task = this.TaskDetails.CreateTask())
            {
                task.InitializeParameters(workflowTaskParameter.Parent, this.Parent);
                TaskParameter taskParameter = task.Parameters.FirstOrDefault(p => p.Id == this.ParameterDetails.Id);
                if (taskParameter != null)
                {
                    return taskParameter.IsValid;
                }
            }
            return null;
        }

        /// <summary>
        /// Helper methods to add a series of new planogram parameter values, and triggered the property changed event
        /// on the value property.
        /// </summary>
        /// <param name="values"></param>
        /// <param name="triggerValueChanged">True if the OnValueChanged is required to be triggered as
        /// a result of this update as the 'Value' property changed is triggered. False if only the 'Values'
        /// property changed changed is required.</param>
        public void AddValues(IEnumerable<WorkpackagePlanogramParameterValue> values, Boolean triggerValueChanged)
        {
            this.Values.AddList(values);

            if (triggerValueChanged)
            {
                OnPropertyChanged("Value");
            }
            else
            {
                //Trigger values changed so any 'Values' related properties can be updated too. This
                //stops all the other updates as part of the 'Value' property changed.
                OnPropertyChanged("Values");
            }
        }

        /// <summary>
        ///     Enumerate the <see cref="WorkpackagePlanogramParameter"/> instances in the parent <see cref="WorkpackagePlanogram"/> that are related to the <paramref name="workflowTaskParameter"/>.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<WorkpackagePlanogramParameter> EnumerateRelatedParameters(WorkflowTaskParameter workflowTaskParameter)
        {
            if (workflowTaskParameter == null || workflowTaskParameter.Parent == null || Parent == null) return new List<WorkpackagePlanogramParameter>();

            Int32 myId = workflowTaskParameter.Parent.Parameters.IndexOf(workflowTaskParameter); 

            return Parent.Parameters.Where(p => 
            p.ParameterDetails.ParentId == myId
            && /*same task*/p.WorkflowTaskId == workflowTaskParameter.Parent.Id);
        }

        #endregion
    }
}
