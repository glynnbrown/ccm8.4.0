﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: CCM800
//// V8-26179  : I.George
////  Created
//#endregion
//#endregion

//using System;
//using System.Collections.Generic;
//using Galleria.Ccm.Resources.Language;
//namespace Galleria.Ccm.Model
//{
//    /// <summary>
//    /// Denotes the available angle units of measure
//    /// </summary>
//    public enum MetricElasticityDataModelType
//    {
//        None = 0,
//        BakerUrban = 1,
//        Linear = 2
//    }

//    public static class MetricElasticityDataModelTypeHelper
//    {
//        public static readonly Dictionary<MetricElasticityDataModelType, String> FriendlyNames =
//            new Dictionary<MetricElasticityDataModelType, String>()
//            {
//                {MetricElasticityDataModelType.None, Message.Enum_MetricElasticityDataModelType_None},
//                {MetricElasticityDataModelType.BakerUrban, Message.Enum_MetricElasticityDataModelType_BakerUrban },
//                {MetricElasticityDataModelType.Linear, Message.Enum_MetricElasticityDataModelType_Linear},
//            };
//    }
//}
