﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26123 : L.Ineson
//	Created (Auto-generated)
// V8-27548 : A.Kuszyk
//  Added FetchByEntityIdName.
#endregion
#region Version History: CCM810
// V8-29597 : N.Foster
//  Added IPlanogramMetricProfile
#endregion
#region Version History: CCM830
// V8-13996 : J.Pickup
//  Addedtwo new methods to normalise the order of metrics against the order from planogram performance data by matching on name.
#endregion
#endregion

using System;
using System.Reflection;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using System.Collections.Generic;
using System.Linq;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// MetricProfile model object
    /// </summary>
    [Serializable]
    public sealed partial class MetricProfile : ModelObject<MetricProfile>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The row version timestamp
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region EntityId

        /// <summary>
        /// EntityId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);

        /// <summary>
        /// Gets/Sets the EntityId value
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
            set { SetProperty<Int32>(EntityIdProperty, value); }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region Metric1MetricId

        /// <summary>
        /// Metric1MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Metric1MetricIdProperty =
            RegisterModelProperty<Int32?>(c => c.Metric1MetricId);

        /// <summary>
        /// Gets/Sets the Metric1MetricId value
        /// </summary>
        public Int32? Metric1MetricId
        {
            get { return GetProperty<Int32?>(Metric1MetricIdProperty); }
            set { SetProperty<Int32?>(Metric1MetricIdProperty, value); }
        }

        #endregion

        #region Metric1Ratio

        /// <summary>
        /// Metric1Ratio property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Metric1RatioProperty =
            RegisterModelProperty<Single?>(c => c.Metric1Ratio);

        /// <summary>
        /// Gets/Sets the Metric1Ratio value
        /// </summary>
        public Single? Metric1Ratio
        {
            get { return GetProperty<Single?>(Metric1RatioProperty); }
            set { SetProperty<Single?>(Metric1RatioProperty, value); }
        }

        #endregion

        #region Metric2MetricId

        /// <summary>
        /// Metric2MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Metric2MetricIdProperty =
            RegisterModelProperty<Int32?>(c => c.Metric2MetricId);

        /// <summary>
        /// Gets/Sets the Metric2MetricId value
        /// </summary>
        public Int32? Metric2MetricId
        {
            get { return GetProperty<Int32?>(Metric2MetricIdProperty); }
            set { SetProperty<Int32?>(Metric2MetricIdProperty, value); }
        }

        #endregion

        #region Metric2Ratio

        /// <summary>
        /// Metric2Ratio property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Metric2RatioProperty =
            RegisterModelProperty<Single?>(c => c.Metric2Ratio);

        /// <summary>
        /// Gets/Sets the Metric2Ratio value
        /// </summary>
        public Single? Metric2Ratio
        {
            get { return GetProperty<Single?>(Metric2RatioProperty); }
            set { SetProperty<Single?>(Metric2RatioProperty, value); }
        }

        #endregion

        #region Metric3MetricId

        /// <summary>
        /// Metric3MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Metric3MetricIdProperty =
            RegisterModelProperty<Int32?>(c => c.Metric3MetricId);

        /// <summary>
        /// Gets/Sets the Metric3MetricId value
        /// </summary>
        public Int32? Metric3MetricId
        {
            get { return GetProperty<Int32?>(Metric3MetricIdProperty); }
            set { SetProperty<Int32?>(Metric3MetricIdProperty, value); }
        }

        #endregion

        #region Metric3Ratio

        /// <summary>
        /// Metric3Ratio property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Metric3RatioProperty =
            RegisterModelProperty<Single?>(c => c.Metric3Ratio);

        /// <summary>
        /// Gets/Sets the Metric3Ratio value
        /// </summary>
        public Single? Metric3Ratio
        {
            get { return GetProperty<Single?>(Metric3RatioProperty); }
            set { SetProperty<Single?>(Metric3RatioProperty, value); }
        }

        #endregion

        #region Metric4MetricId

        /// <summary>
        /// Metric4MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Metric4MetricIdProperty =
            RegisterModelProperty<Int32?>(c => c.Metric4MetricId);

        /// <summary>
        /// Gets/Sets the Metric4MetricId value
        /// </summary>
        public Int32? Metric4MetricId
        {
            get { return GetProperty<Int32?>(Metric4MetricIdProperty); }
            set { SetProperty<Int32?>(Metric4MetricIdProperty, value); }
        }

        #endregion

        #region Metric4Ratio

        /// <summary>
        /// Metric4Ratio property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Metric4RatioProperty =
            RegisterModelProperty<Single?>(c => c.Metric4Ratio);

        /// <summary>
        /// Gets/Sets the Metric4Ratio value
        /// </summary>
        public Single? Metric4Ratio
        {
            get { return GetProperty<Single?>(Metric4RatioProperty); }
            set { SetProperty<Single?>(Metric4RatioProperty, value); }
        }

        #endregion

        #region Metric5MetricId

        /// <summary>
        /// Metric5MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Metric5MetricIdProperty =
            RegisterModelProperty<Int32?>(c => c.Metric5MetricId);

        /// <summary>
        /// Gets/Sets the Metric5MetricId value
        /// </summary>
        public Int32? Metric5MetricId
        {
            get { return GetProperty<Int32?>(Metric5MetricIdProperty); }
            set { SetProperty<Int32?>(Metric5MetricIdProperty, value); }
        }

        #endregion

        #region Metric5Ratio

        /// <summary>
        /// Metric5Ratio property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Metric5RatioProperty =
            RegisterModelProperty<Single?>(c => c.Metric5Ratio);

        /// <summary>
        /// Gets/Sets the Metric5Ratio value
        /// </summary>
        public Single? Metric5Ratio
        {
            get { return GetProperty<Single?>(Metric5RatioProperty); }
            set { SetProperty<Single?>(Metric5RatioProperty, value); }
        }

        #endregion

        #region Metric6MetricId

        /// <summary>
        /// Metric6MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Metric6MetricIdProperty =
            RegisterModelProperty<Int32?>(c => c.Metric6MetricId);

        /// <summary>
        /// Gets/Sets the Metric6MetricId value
        /// </summary>
        public Int32? Metric6MetricId
        {
            get { return GetProperty<Int32?>(Metric6MetricIdProperty); }
            set { SetProperty<Int32?>(Metric6MetricIdProperty, value); }
        }

        #endregion

        #region Metric6Ratio

        /// <summary>
        /// Metric6Ratio property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Metric6RatioProperty =
            RegisterModelProperty<Single?>(c => c.Metric6Ratio);

        /// <summary>
        /// Gets/Sets the Metric6Ratio value
        /// </summary>
        public Single? Metric6Ratio
        {
            get { return GetProperty<Single?>(Metric6RatioProperty); }
            set { SetProperty<Single?>(Metric6RatioProperty, value); }
        }

        #endregion

        #region Metric7MetricId

        /// <summary>
        /// Metric7MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Metric7MetricIdProperty =
            RegisterModelProperty<Int32?>(c => c.Metric7MetricId);

        /// <summary>
        /// Gets/Sets the Metric7MetricId value
        /// </summary>
        public Int32? Metric7MetricId
        {
            get { return GetProperty<Int32?>(Metric7MetricIdProperty); }
            set { SetProperty<Int32?>(Metric7MetricIdProperty, value); }
        }

        #endregion

        #region Metric7Ratio

        /// <summary>
        /// Metric7Ratio property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Metric7RatioProperty =
            RegisterModelProperty<Single?>(c => c.Metric7Ratio);

        /// <summary>
        /// Gets/Sets the Metric7Ratio value
        /// </summary>
        public Single? Metric7Ratio
        {
            get { return GetProperty<Single?>(Metric7RatioProperty); }
            set { SetProperty<Single?>(Metric7RatioProperty, value); }
        }

        #endregion

        #region Metric8MetricId

        /// <summary>
        /// Metric8MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Metric8MetricIdProperty =
            RegisterModelProperty<Int32?>(c => c.Metric8MetricId);

        /// <summary>
        /// Gets/Sets the Metric8MetricId value
        /// </summary>
        public Int32? Metric8MetricId
        {
            get { return GetProperty<Int32?>(Metric8MetricIdProperty); }
            set { SetProperty<Int32?>(Metric8MetricIdProperty, value); }
        }

        #endregion

        #region Metric8Ratio

        /// <summary>
        /// Metric8Ratio property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Metric8RatioProperty =
            RegisterModelProperty<Single?>(c => c.Metric8Ratio);

        /// <summary>
        /// Gets/Sets the Metric8Ratio value
        /// </summary>
        public Single? Metric8Ratio
        {
            get { return GetProperty<Single?>(Metric8RatioProperty); }
            set { SetProperty<Single?>(Metric8RatioProperty, value); }
        }

        #endregion

        #region Metric9MetricId

        /// <summary>
        /// Metric9MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Metric9MetricIdProperty =
            RegisterModelProperty<Int32?>(c => c.Metric9MetricId);

        /// <summary>
        /// Gets/Sets the Metric9MetricId value
        /// </summary>
        public Int32? Metric9MetricId
        {
            get { return GetProperty<Int32?>(Metric9MetricIdProperty); }
            set { SetProperty<Int32?>(Metric9MetricIdProperty, value); }
        }

        #endregion

        #region Metric9Ratio

        /// <summary>
        /// Metric9Ratio property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Metric9RatioProperty =
            RegisterModelProperty<Single?>(c => c.Metric9Ratio);

        /// <summary>
        /// Gets/Sets the Metric9Ratio value
        /// </summary>
        public Single? Metric9Ratio
        {
            get { return GetProperty<Single?>(Metric9RatioProperty); }
            set { SetProperty<Single?>(Metric9RatioProperty, value); }
        }

        #endregion

        #region Metric10MetricId

        /// <summary>
        /// Metric10MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Metric10MetricIdProperty =
            RegisterModelProperty<Int32?>(c => c.Metric10MetricId);

        /// <summary>
        /// Gets/Sets the Metric10MetricId value
        /// </summary>
        public Int32? Metric10MetricId
        {
            get { return GetProperty<Int32?>(Metric10MetricIdProperty); }
            set { SetProperty<Int32?>(Metric10MetricIdProperty, value); }
        }

        #endregion

        #region Metric10Ratio

        /// <summary>
        /// Metric10Ratio property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Metric10RatioProperty =
            RegisterModelProperty<Single?>(c => c.Metric10Ratio);

        /// <summary>
        /// Gets/Sets the Metric10Ratio value
        /// </summary>
        public Single? Metric10Ratio
        {
            get { return GetProperty<Single?>(Metric10RatioProperty); }
            set { SetProperty<Single?>(Metric10RatioProperty, value); }
        }

        #endregion

        #region Metric11MetricId

        /// <summary>
        /// Metric11MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Metric11MetricIdProperty =
            RegisterModelProperty<Int32?>(c => c.Metric11MetricId);

        /// <summary>
        /// Gets/Sets the Metric11MetricId value
        /// </summary>
        public Int32? Metric11MetricId
        {
            get { return GetProperty<Int32?>(Metric11MetricIdProperty); }
            set { SetProperty<Int32?>(Metric11MetricIdProperty, value); }
        }

        #endregion

        #region Metric11Ratio

        /// <summary>
        /// Metric11Ratio property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Metric11RatioProperty =
            RegisterModelProperty<Single?>(c => c.Metric11Ratio);

        /// <summary>
        /// Gets/Sets the Metric11Ratio value
        /// </summary>
        public Single? Metric11Ratio
        {
            get { return GetProperty<Single?>(Metric11RatioProperty); }
            set { SetProperty<Single?>(Metric11RatioProperty, value); }
        }

        #endregion

        #region Metric12MetricId

        /// <summary>
        /// Metric12MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Metric12MetricIdProperty =
            RegisterModelProperty<Int32?>(c => c.Metric12MetricId);

        /// <summary>
        /// Gets/Sets the Metric12MetricId value
        /// </summary>
        public Int32? Metric12MetricId
        {
            get { return GetProperty<Int32?>(Metric12MetricIdProperty); }
            set { SetProperty<Int32?>(Metric12MetricIdProperty, value); }
        }

        #endregion

        #region Metric12Ratio

        /// <summary>
        /// Metric12Ratio property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Metric12RatioProperty =
            RegisterModelProperty<Single?>(c => c.Metric12Ratio);

        /// <summary>
        /// Gets/Sets the Metric12Ratio value
        /// </summary>
        public Single? Metric12Ratio
        {
            get { return GetProperty<Single?>(Metric12RatioProperty); }
            set { SetProperty<Single?>(Metric12RatioProperty, value); }
        }

        #endregion

        #region Metric13MetricId

        /// <summary>
        /// Metric13MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Metric13MetricIdProperty =
            RegisterModelProperty<Int32?>(c => c.Metric13MetricId);

        /// <summary>
        /// Gets/Sets the Metric13MetricId value
        /// </summary>
        public Int32? Metric13MetricId
        {
            get { return GetProperty<Int32?>(Metric13MetricIdProperty); }
            set { SetProperty<Int32?>(Metric13MetricIdProperty, value); }
        }

        #endregion

        #region Metric13Ratio

        /// <summary>
        /// Metric13Ratio property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Metric13RatioProperty =
            RegisterModelProperty<Single?>(c => c.Metric13Ratio);

        /// <summary>
        /// Gets/Sets the Metric13Ratio value
        /// </summary>
        public Single? Metric13Ratio
        {
            get { return GetProperty<Single?>(Metric13RatioProperty); }
            set { SetProperty<Single?>(Metric13RatioProperty, value); }
        }

        #endregion

        #region Metric14MetricId

        /// <summary>
        /// Metric14MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Metric14MetricIdProperty =
            RegisterModelProperty<Int32?>(c => c.Metric14MetricId);

        /// <summary>
        /// Gets/Sets the Metric14MetricId value
        /// </summary>
        public Int32? Metric14MetricId
        {
            get { return GetProperty<Int32?>(Metric14MetricIdProperty); }
            set { SetProperty<Int32?>(Metric14MetricIdProperty, value); }
        }

        #endregion

        #region Metric14Ratio

        /// <summary>
        /// Metric14Ratio property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Metric14RatioProperty =
            RegisterModelProperty<Single?>(c => c.Metric14Ratio);

        /// <summary>
        /// Gets/Sets the Metric14Ratio value
        /// </summary>
        public Single? Metric14Ratio
        {
            get { return GetProperty<Single?>(Metric14RatioProperty); }
            set { SetProperty<Single?>(Metric14RatioProperty, value); }
        }

        #endregion

        #region Metric15MetricId

        /// <summary>
        /// Metric15MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Metric15MetricIdProperty =
            RegisterModelProperty<Int32?>(c => c.Metric15MetricId);

        /// <summary>
        /// Gets/Sets the Metric15MetricId value
        /// </summary>
        public Int32? Metric15MetricId
        {
            get { return GetProperty<Int32?>(Metric15MetricIdProperty); }
            set { SetProperty<Int32?>(Metric15MetricIdProperty, value); }
        }

        #endregion

        #region Metric15Ratio

        /// <summary>
        /// Metric15Ratio property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Metric15RatioProperty =
            RegisterModelProperty<Single?>(c => c.Metric15Ratio);

        /// <summary>
        /// Gets/Sets the Metric15Ratio value
        /// </summary>
        public Single? Metric15Ratio
        {
            get { return GetProperty<Single?>(Metric15RatioProperty); }
            set { SetProperty<Single?>(Metric15RatioProperty, value); }
        }

        #endregion

        #region Metric16MetricId

        /// <summary>
        /// Metric16MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Metric16MetricIdProperty =
            RegisterModelProperty<Int32?>(c => c.Metric16MetricId);

        /// <summary>
        /// Gets/Sets the Metric16MetricId value
        /// </summary>
        public Int32? Metric16MetricId
        {
            get { return GetProperty<Int32?>(Metric16MetricIdProperty); }
            set { SetProperty<Int32?>(Metric16MetricIdProperty, value); }
        }

        #endregion

        #region Metric16Ratio

        /// <summary>
        /// Metric16Ratio property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Metric16RatioProperty =
            RegisterModelProperty<Single?>(c => c.Metric16Ratio);

        /// <summary>
        /// Gets/Sets the Metric16Ratio value
        /// </summary>
        public Single? Metric16Ratio
        {
            get { return GetProperty<Single?>(Metric16RatioProperty); }
            set { SetProperty<Single?>(Metric16RatioProperty, value); }
        }

        #endregion

        #region Metric17MetricId

        /// <summary>
        /// Metric17MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Metric17MetricIdProperty =
            RegisterModelProperty<Int32?>(c => c.Metric17MetricId);

        /// <summary>
        /// Gets/Sets the Metric17MetricId value
        /// </summary>
        public Int32? Metric17MetricId
        {
            get { return GetProperty<Int32?>(Metric17MetricIdProperty); }
            set { SetProperty<Int32?>(Metric17MetricIdProperty, value); }
        }

        #endregion

        #region Metric17Ratio

        /// <summary>
        /// Metric17Ratio property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Metric17RatioProperty =
            RegisterModelProperty<Single?>(c => c.Metric17Ratio);

        /// <summary>
        /// Gets/Sets the Metric17Ratio value
        /// </summary>
        public Single? Metric17Ratio
        {
            get { return GetProperty<Single?>(Metric17RatioProperty); }
            set { SetProperty<Single?>(Metric17RatioProperty, value); }
        }

        #endregion

        #region Metric18MetricId

        /// <summary>
        /// Metric18MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Metric18MetricIdProperty =
            RegisterModelProperty<Int32?>(c => c.Metric18MetricId);

        /// <summary>
        /// Gets/Sets the Metric18MetricId value
        /// </summary>
        public Int32? Metric18MetricId
        {
            get { return GetProperty<Int32?>(Metric18MetricIdProperty); }
            set { SetProperty<Int32?>(Metric18MetricIdProperty, value); }
        }

        #endregion

        #region Metric18Ratio

        /// <summary>
        /// Metric18Ratio property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Metric18RatioProperty =
            RegisterModelProperty<Single?>(c => c.Metric18Ratio);

        /// <summary>
        /// Gets/Sets the Metric18Ratio value
        /// </summary>
        public Single? Metric18Ratio
        {
            get { return GetProperty<Single?>(Metric18RatioProperty); }
            set { SetProperty<Single?>(Metric18RatioProperty, value); }
        }

        #endregion

        #region Metric19MetricId

        /// <summary>
        /// Metric19MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Metric19MetricIdProperty =
            RegisterModelProperty<Int32?>(c => c.Metric19MetricId);

        /// <summary>
        /// Gets/Sets the Metric19MetricId value
        /// </summary>
        public Int32? Metric19MetricId
        {
            get { return GetProperty<Int32?>(Metric19MetricIdProperty); }
            set { SetProperty<Int32?>(Metric19MetricIdProperty, value); }
        }

        #endregion

        #region Metric19Ratio

        /// <summary>
        /// Metric19Ratio property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Metric19RatioProperty =
            RegisterModelProperty<Single?>(c => c.Metric19Ratio);

        /// <summary>
        /// Gets/Sets the Metric19Ratio value
        /// </summary>
        public Single? Metric19Ratio
        {
            get { return GetProperty<Single?>(Metric19RatioProperty); }
            set { SetProperty<Single?>(Metric19RatioProperty, value); }
        }

        #endregion

        #region Metric20MetricId

        /// <summary>
        /// Metric20MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Metric20MetricIdProperty =
            RegisterModelProperty<Int32?>(c => c.Metric20MetricId);

        /// <summary>
        /// Gets/Sets the Metric20MetricId value
        /// </summary>
        public Int32? Metric20MetricId
        {
            get { return GetProperty<Int32?>(Metric20MetricIdProperty); }
            set { SetProperty<Int32?>(Metric20MetricIdProperty, value); }
        }

        #endregion

        #region Metric20Ratio

        /// <summary>
        /// Metric20Ratio property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Metric20RatioProperty =
            RegisterModelProperty<Single?>(c => c.Metric20Ratio);

        /// <summary>
        /// Gets/Sets the Metric20Ratio value
        /// </summary>
        public Single? Metric20Ratio
        {
            get { return GetProperty<Single?>(Metric20RatioProperty); }
            set { SetProperty<Single?>(Metric20RatioProperty, value); }
        }

        #endregion

        #region DateCreated

        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        /// Gets/Sets the DateCreated value
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
            set { SetProperty<DateTime>(DateCreatedProperty, value); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        /// Gets/Sets the DateLastModified value
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
            set { SetProperty<DateTime>(DateLastModifiedProperty, value); }
        }

        #endregion

        #region DateDeleted

        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);

        /// <summary>
        /// Gets/Sets the DateDeleted value
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
            set { SetProperty<DateTime?>(DateDeletedProperty, value); }
        }

        #endregion


        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new MinValue<Int32>(EntityIdProperty, 1));

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));

        }

        #endregion

        #region Authorization Rules

        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(MetricProfile), new IsInRole(AuthorizationActions.GetObject, DomainPermission.MetricProfileGet.ToString()));
            BusinessRules.AddRule(typeof(MetricProfile), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.MetricProfileCreate.ToString()));
            BusinessRules.AddRule(typeof(MetricProfile), new IsInRole(AuthorizationActions.EditObject, DomainPermission.MetricProfileEdit.ToString()));
            BusinessRules.AddRule(typeof(MetricProfile), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.MetricProfileDelete.ToString()));
        }

        #endregion

        #region Criteria

        #region FetchByIdCriteria

        // <summary>
        /// Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> IdProperty =
                RegisterProperty<Int32>(c => c.Id);
            /// <summary>
            /// Returns the id
            /// </summary>
            public Int32 Id
            {
                get { return this.ReadProperty<Int32>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(Int32 id)
            {
                this.LoadProperty<Int32>(IdProperty, id);
            }
            #endregion
        }

        #endregion

        #region FetchByEntityIdNameCriteria

        // <summary>
        /// Criteria for the FetchByEntityIdName factory method
        /// </summary>
        [Serializable]
        public class FetchByEntityIdNameCriteria : CriteriaBase<FetchByEntityIdNameCriteria>
        {
            #region Properties

            #region EntityId
            /// <summary>
            /// EntityId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            /// <summary>
            /// Returns the EntityId
            /// </summary>
            public Int32 EntityId
            {
                get { return this.ReadProperty<Int32>(EntityIdProperty); }
            }
            #endregion

            #region Name
            /// <summary>
            /// Name property definition
            /// </summary>
            public static readonly PropertyInfo<String> NameProperty =
                RegisterProperty<String>(c => c.Name);
            /// <summary>
            /// Returns the Name
            /// </summary>
            public String Name
            {
                get { return this.ReadProperty<String>(NameProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByEntityIdNameCriteria(Int32 entityId, String name)
            {
                this.LoadProperty<Int32>(EntityIdProperty, entityId);
                this.LoadProperty<String>(NameProperty, name);
            }
            #endregion
        }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new MetricProfile
        /// </summary>
        public static MetricProfile NewMetricProfile(Int32 entityId)
        {
            MetricProfile item = new MetricProfile();
            item.Create(entityId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 entityId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Clears the value of all metrics in this profile.
        /// </summary>
        public void ClearMetrics()
        {
            this.Metric1MetricId = null;
            this.Metric1Ratio = null;

            this.Metric2MetricId = null;
            this.Metric2Ratio = null;

            this.Metric3MetricId = null;
            this.Metric3Ratio = null;

            this.Metric4MetricId = null;
            this.Metric4Ratio = null;

            this.Metric5MetricId = null;
            this.Metric5Ratio = null;

            this.Metric6MetricId = null;
            this.Metric6Ratio = null;

            this.Metric7MetricId = null;
            this.Metric7Ratio = null;

            this.Metric8MetricId = null;
            this.Metric8Ratio = null;

            this.Metric9MetricId = null;
            this.Metric9Ratio = null;

            this.Metric10MetricId = null;
            this.Metric10Ratio = null;

            this.Metric11MetricId = null;
            this.Metric11Ratio = null;

            this.Metric12MetricId = null;
            this.Metric12Ratio = null;

            this.Metric13MetricId = null;
            this.Metric13Ratio = null;

            this.Metric14MetricId = null;
            this.Metric14Ratio = null;

            this.Metric15MetricId = null;
            this.Metric15Ratio = null;

            this.Metric16MetricId = null;
            this.Metric16Ratio = null;

            this.Metric17MetricId = null;
            this.Metric17Ratio = null;

            this.Metric18MetricId = null;
            this.Metric18Ratio = null;

            this.Metric19MetricId = null;
            this.Metric19Ratio = null;

            this.Metric20MetricId = null;
            this.Metric20Ratio = null;
        }

        /// <summary>
        /// Sets the values of the metric at the given position number
        /// </summary>
        public void SetMetric(Int32 metricNumber, Int32? metricId, Single? metricRatio)
        {
            //set the metricid
            PropertyInfo prop = this.GetType().GetProperty(String.Format("Metric{0}MetricId", metricNumber), BindingFlags.Public | BindingFlags.Instance);
            if (null != prop && prop.CanWrite && prop.CanRead)
            {
                prop.SetValue(this, metricId, null);
            }

            //set the ratio
            prop = this.GetType().GetProperty(String.Format("Metric{0}Ratio", metricNumber), BindingFlags.Public | BindingFlags.Instance);
            if (null != prop && prop.CanWrite && prop.CanRead)
            {
                prop.SetValue(this, metricRatio, null);
            }
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<MetricProfile>(oldId, newId);
        }

        /// <summary>
        /// Aquires the ratio for the given Id. If unable find Id then the natural ratio of 0 is returned.
        /// </summary>
        /// <param name="idToAquireRatioFrom">Id of the ratio we need to aquire the ratio from</param>
        /// <returns>A float representation of the percentage ratio for the given id.</returns>
        public float GetRatioForMetricId(Int32 idToAquireRatioFrom)
        {
            if (this.Metric1MetricId == idToAquireRatioFrom) return this.Metric1Ratio.Value;
            if (this.Metric2MetricId == idToAquireRatioFrom) return this.Metric2Ratio.Value;
            if (this.Metric3MetricId == idToAquireRatioFrom) return this.Metric3Ratio.Value;
            if (this.Metric4MetricId == idToAquireRatioFrom) return this.Metric4Ratio.Value;
            if (this.Metric5MetricId == idToAquireRatioFrom) return this.Metric5Ratio.Value;
            if (this.Metric6MetricId == idToAquireRatioFrom) return this.Metric6Ratio.Value;
            if (this.Metric7MetricId == idToAquireRatioFrom) return this.Metric7Ratio.Value;
            if (this.Metric8MetricId == idToAquireRatioFrom) return this.Metric8Ratio.Value;
            if (this.Metric9MetricId == idToAquireRatioFrom) return this.Metric9Ratio.Value;
            if (this.Metric10MetricId == idToAquireRatioFrom) return this.Metric10Ratio.Value;
            if (this.Metric11MetricId == idToAquireRatioFrom) return this.Metric11Ratio.Value;
            if (this.Metric12MetricId == idToAquireRatioFrom) return this.Metric12Ratio.Value;
            if (this.Metric13MetricId == idToAquireRatioFrom) return this.Metric13Ratio.Value;
            if (this.Metric14MetricId == idToAquireRatioFrom) return this.Metric14Ratio.Value;
            if (this.Metric15MetricId == idToAquireRatioFrom) return this.Metric15Ratio.Value;
            if (this.Metric16MetricId == idToAquireRatioFrom) return this.Metric16Ratio.Value;
            if (this.Metric17MetricId == idToAquireRatioFrom) return this.Metric17Ratio.Value;
            if (this.Metric18MetricId == idToAquireRatioFrom) return this.Metric18Ratio.Value;
            if (this.Metric19MetricId == idToAquireRatioFrom) return this.Metric19Ratio.Value;
            if (this.Metric20MetricId == idToAquireRatioFrom) return this.Metric20Ratio.Value;

            return 0;
        }

        /// <summary>
        /// Enumerates through all applied metrics returning the global metric id 
        /// and the applied ratio.
        /// </summary>
        public IEnumerable<Tuple<Int32, Single>> EnumerateAppliedMetricIdsAndRatios()
        {
            if (this.Metric1MetricId.HasValue && this.Metric1Ratio.HasValue)
                yield return new Tuple<Int32, Single>(this.Metric1MetricId.Value, this.Metric1Ratio.Value);

            if (this.Metric2MetricId.HasValue && this.Metric2Ratio.HasValue)
                yield return new Tuple<Int32, Single>(this.Metric2MetricId.Value, this.Metric2Ratio.Value);

            if (this.Metric3MetricId.HasValue && this.Metric3Ratio.HasValue)
                yield return new Tuple<Int32, Single>(this.Metric3MetricId.Value, this.Metric3Ratio.Value);

            if (this.Metric4MetricId.HasValue && this.Metric4Ratio.HasValue)
                yield return new Tuple<Int32, Single>(this.Metric4MetricId.Value, this.Metric4Ratio.Value);

            if (this.Metric5MetricId.HasValue && this.Metric5Ratio.HasValue)
                yield return new Tuple<Int32, Single>(this.Metric5MetricId.Value, this.Metric5Ratio.Value);

            if (this.Metric6MetricId.HasValue && this.Metric6Ratio.HasValue)
                yield return new Tuple<Int32, Single>(this.Metric6MetricId.Value, this.Metric6Ratio.Value);

            if (this.Metric7MetricId.HasValue && this.Metric7Ratio.HasValue)
                yield return new Tuple<Int32, Single>(this.Metric7MetricId.Value, this.Metric7Ratio.Value);

            if (this.Metric8MetricId.HasValue && this.Metric8Ratio.HasValue)
                yield return new Tuple<Int32, Single>(this.Metric8MetricId.Value, this.Metric8Ratio.Value);

            if (this.Metric9MetricId.HasValue && this.Metric9Ratio.HasValue)
                yield return new Tuple<Int32, Single>(this.Metric9MetricId.Value, this.Metric9Ratio.Value);

            if (this.Metric10MetricId.HasValue && this.Metric10Ratio.HasValue)
                yield return new Tuple<Int32, Single>(this.Metric10MetricId.Value, this.Metric10Ratio.Value);

            if (this.Metric11MetricId.HasValue && this.Metric11Ratio.HasValue)
                yield return new Tuple<Int32, Single>(this.Metric11MetricId.Value, this.Metric11Ratio.Value);

            if (this.Metric12MetricId.HasValue && this.Metric12Ratio.HasValue)
                yield return new Tuple<Int32, Single>(this.Metric12MetricId.Value, this.Metric12Ratio.Value);

            if (this.Metric13MetricId.HasValue && this.Metric13Ratio.HasValue)
                yield return new Tuple<Int32, Single>(this.Metric13MetricId.Value, this.Metric13Ratio.Value);

            if (this.Metric14MetricId.HasValue && this.Metric14Ratio.HasValue)
                yield return new Tuple<Int32, Single>(this.Metric14MetricId.Value, this.Metric14Ratio.Value);

            if (this.Metric15MetricId.HasValue && this.Metric15Ratio.HasValue)
                yield return new Tuple<Int32, Single>(this.Metric15MetricId.Value, this.Metric15Ratio.Value);

            if (this.Metric16MetricId.HasValue && this.Metric16Ratio.HasValue)
                yield return new Tuple<Int32, Single>(this.Metric16MetricId.Value, this.Metric16Ratio.Value);

            if (this.Metric17MetricId.HasValue && this.Metric17Ratio.HasValue)
                yield return new Tuple<Int32, Single>(this.Metric17MetricId.Value, this.Metric17Ratio.Value);

            if (this.Metric18MetricId.HasValue && this.Metric18Ratio.HasValue)
                yield return new Tuple<Int32, Single>(this.Metric18MetricId.Value, this.Metric18Ratio.Value);

            if (this.Metric19MetricId.HasValue && this.Metric19Ratio.HasValue)
                yield return new Tuple<Int32, Single>(this.Metric19MetricId.Value, this.Metric19Ratio.Value);

            if (this.Metric20MetricId.HasValue && this.Metric20Ratio.HasValue)
                yield return new Tuple<Int32, Single>(this.Metric20MetricId.Value, this.Metric20Ratio.Value);


        }

        /// <summary>
        /// Returns a resolved planogram metric profile according to the metrics available in the planogram
        /// May return null if there is a metric mismatch.
        /// </summary>
        /// <param name="planogram">the planogram to resolve for</param>
        /// <param name="entityId">the entity id from which to retrieve global metrics.</param>
        /// <returns></returns>
        public IPlanogramMetricProfile ToPlanogramMetricProfile(PlanogramPerformance planogramPerformance, MetricList globalMetricList)
        {
            ResolvedPlanogramMetricProfile profile = new ResolvedPlanogramMetricProfile();

            foreach (Tuple<Int32, Single> appliedRatio in this.EnumerateAppliedMetricIdsAndRatios())
            {
                Int32 globalMetricId = appliedRatio.Item1;
                Single ratio = appliedRatio.Item2;

                //get the global metric
                Metric globalMetric = globalMetricList.FirstOrDefault(g => g.Id == globalMetricId);
                if (globalMetric == null)
                {
                    //if no global metric was found so we have a mismatch between the profile and the global list.
                    return null;
                }

                //confirm that the metric exists in the planogram matching by name (case insensitive)
                PlanogramPerformanceMetric planMetric =
                    planogramPerformance.Metrics
                    .FirstOrDefault(p => String.Compare(p.Name, globalMetric.Name, ignoreCase: true) == 0);

                if (planMetric == null)
                {
                    //if no plan metric was found then we have a mismatch between the profile and what is actually
                    // in the plan.
                    return null;
                }


                //add the resolved metric details
                profile.SetRatio(planMetric, ratio);
            }

            return profile;
        }

        #endregion

        #region Override
        public override string ToString()
        {
            return this.Name;
        }
        #endregion

        #region Nested Classes

        public sealed class ResolvedPlanogramMetricProfile : IPlanogramMetricProfile
        {
            #region Fields
            private Dictionary<Int32, PlanogramPerformanceMetric> _planMetrics = new Dictionary<Int32, PlanogramPerformanceMetric>();

            #endregion

            #region Properties
            public String Name { get; set; }
            public Single? Metric1Ratio { get; set; }
            public Single? Metric2Ratio { get; set; }
            public Single? Metric3Ratio { get; set; }
            public Single? Metric4Ratio { get; set; }
            public Single? Metric5Ratio { get; set; }
            public Single? Metric6Ratio { get; set; }
            public Single? Metric7Ratio { get; set; }
            public Single? Metric8Ratio { get; set; }
            public Single? Metric9Ratio { get; set; }
            public Single? Metric10Ratio { get; set; }
            public Single? Metric11Ratio { get; set; }
            public Single? Metric12Ratio { get; set; }
            public Single? Metric13Ratio { get; set; }
            public Single? Metric14Ratio { get; set; }
            public Single? Metric15Ratio { get; set; }
            public Single? Metric16Ratio { get; set; }
            public Single? Metric17Ratio { get; set; }
            public Single? Metric18Ratio { get; set; }
            public Single? Metric19Ratio { get; set; }
            public Single? Metric20Ratio { get; set; }
            #endregion

            internal ResolvedPlanogramMetricProfile() { }

            #region Methods

            public IEnumerable<Tuple<PlanogramPerformanceMetric, Single>> EnumerateAppliedMetricsAndRatios()
            {
                if (this.Metric1Ratio.HasValue)
                    yield return new Tuple<PlanogramPerformanceMetric, Single>(_planMetrics[1], this.Metric1Ratio.Value);

                if (this.Metric2Ratio.HasValue)
                    yield return new Tuple<PlanogramPerformanceMetric, Single>(_planMetrics[2], this.Metric2Ratio.Value);

                if (this.Metric3Ratio.HasValue)
                    yield return new Tuple<PlanogramPerformanceMetric, Single>(_planMetrics[3], this.Metric3Ratio.Value);

                if (this.Metric4Ratio.HasValue)
                    yield return new Tuple<PlanogramPerformanceMetric, Single>(_planMetrics[4], this.Metric4Ratio.Value);

                if (this.Metric5Ratio.HasValue)
                    yield return new Tuple<PlanogramPerformanceMetric, Single>(_planMetrics[5], this.Metric5Ratio.Value);

                if (this.Metric6Ratio.HasValue)
                    yield return new Tuple<PlanogramPerformanceMetric, Single>(_planMetrics[6], this.Metric6Ratio.Value);

                if (this.Metric7Ratio.HasValue)
                    yield return new Tuple<PlanogramPerformanceMetric, Single>(_planMetrics[7], this.Metric7Ratio.Value);

                if (this.Metric8Ratio.HasValue)
                    yield return new Tuple<PlanogramPerformanceMetric, Single>(_planMetrics[8], this.Metric8Ratio.Value);

                if (this.Metric9Ratio.HasValue)
                    yield return new Tuple<PlanogramPerformanceMetric, Single>(_planMetrics[9], this.Metric9Ratio.Value);

                if (this.Metric10Ratio.HasValue)
                    yield return new Tuple<PlanogramPerformanceMetric, Single>(_planMetrics[10], this.Metric10Ratio.Value);

                if (this.Metric11Ratio.HasValue)
                    yield return new Tuple<PlanogramPerformanceMetric, Single>(_planMetrics[11], this.Metric11Ratio.Value);

                if (this.Metric12Ratio.HasValue)
                    yield return new Tuple<PlanogramPerformanceMetric, Single>(_planMetrics[12], this.Metric12Ratio.Value);

                if (this.Metric13Ratio.HasValue)
                    yield return new Tuple<PlanogramPerformanceMetric, Single>(_planMetrics[13], this.Metric13Ratio.Value);

                if (this.Metric14Ratio.HasValue)
                    yield return new Tuple<PlanogramPerformanceMetric, Single>(_planMetrics[14], this.Metric14Ratio.Value);

                if (this.Metric15Ratio.HasValue)
                    yield return new Tuple<PlanogramPerformanceMetric, Single>(_planMetrics[15], this.Metric15Ratio.Value);

                if (this.Metric16Ratio.HasValue)
                    yield return new Tuple<PlanogramPerformanceMetric, Single>(_planMetrics[16], this.Metric16Ratio.Value);

                if (this.Metric17Ratio.HasValue)
                    yield return new Tuple<PlanogramPerformanceMetric, Single>(_planMetrics[17], this.Metric17Ratio.Value);

                if (this.Metric18Ratio.HasValue)
                    yield return new Tuple<PlanogramPerformanceMetric, Single>(_planMetrics[18], this.Metric18Ratio.Value);

                if (this.Metric19Ratio.HasValue)
                    yield return new Tuple<PlanogramPerformanceMetric, Single>(_planMetrics[19], this.Metric19Ratio.Value);

                if (this.Metric20Ratio.HasValue)
                    yield return new Tuple<PlanogramPerformanceMetric, Single>(_planMetrics[20], this.Metric20Ratio.Value);

            }

            internal void SetRatio(PlanogramPerformanceMetric metric, Single? value)
            {
                _planMetrics[metric.MetricId] = metric;

                switch (metric.MetricId)
                {
                    case 1:
                        this.Metric1Ratio = value;
                        break;

                    case 2:
                        this.Metric2Ratio = value;
                        break;

                    case 3:
                        this.Metric3Ratio = value;
                        break;

                    case 4:
                        this.Metric4Ratio = value;
                        break;

                    case 5:
                        this.Metric5Ratio = value;
                        break;

                    case 6:
                        this.Metric6Ratio = value;
                        break;

                    case 7:
                        this.Metric7Ratio = value;
                        break;

                    case 8:
                        this.Metric8Ratio = value;
                        break;

                    case 9:
                        this.Metric9Ratio = value;
                        break;

                    case 10:
                        this.Metric10Ratio = value;
                        break;

                    case 11:
                        this.Metric11Ratio = value;
                        break;

                    case 12:
                        this.Metric12Ratio = value;
                        break;

                    case 13:
                        this.Metric13Ratio = value;
                        break;

                    case 14:
                        this.Metric14Ratio = value;
                        break;

                    case 15:
                        this.Metric15Ratio = value;
                        break;

                    case 16:
                        this.Metric16Ratio = value;
                        break;

                    case 17:
                        this.Metric17Ratio = value;
                        break;

                    case 18:
                        this.Metric18Ratio = value;
                        break;

                    case 19:
                        this.Metric19Ratio = value;
                        break;

                    case 20:
                        this.Metric20Ratio = value;
                        break;

                }
            }

            #endregion
        }

        #endregion
    }
}