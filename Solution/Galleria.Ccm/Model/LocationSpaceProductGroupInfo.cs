﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM v8.0.2)
// V8-29026 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Rules;
using System.Collections.Generic;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a description of a type of location space product group info
    /// </summary>
    [Serializable]
    public sealed partial class LocationSpaceProductGroupInfo : ModelReadOnlyObject<LocationSpaceProductGroupInfo>
    {
        #region Properties
        /// <summary>
        /// The Location Space Product Group Id.
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// The Product Group Id.
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProductGroupIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductGroupId);
        public Int32 ProductGroupId
        {
            get { return GetProperty<Int32>(ProductGroupIdProperty); }
        }

        /// <summary>
        /// The Location Id.
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> LocationIdProperty =
            RegisterModelProperty<Int16>(c => c.LocationId);
        public Int16 LocationId
        {
            get { return GetProperty<Int16>(LocationIdProperty); }
        }

        /// <summary>
        /// The Bay Count.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BayCountProperty =
            RegisterModelProperty<Single>(c => c.BayCount, Message.LocationSpaceProductGroup_BayCount);
        public Single BayCount
        {
            get { return GetProperty<Single>(BayCountProperty); }
        }

        /// <summary>
        /// The Average Bay Width
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AverageBayWidthProperty =
            RegisterModelProperty<Single>(c => c.AverageBayWidth, Message.LocationSpaceProductGroup_AverageBayWidth, ModelPropertyDisplayType.Length);
        public Single AverageBayWidth
        {
            get { return GetProperty<Single>(AverageBayWidthProperty); }
        }

        /// <summary>
        /// The Product Count.
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> ProductCountProperty =
            RegisterModelProperty<Int32?>(c => c.ProductCount, Message.LocationSpaceProductGroup_ProductCount);
        public Int32? ProductCount
        {
            get { return GetProperty<Int32?>(ProductCountProperty); }
        }
        
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationSpaceProductGroupInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceProductGroupInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceProductGroupInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceProductGroupInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }

        #endregion
       
        #region Override

        public override string ToString()
        {
            return this.Id.ToString();
        }

        #endregion
    }
}