﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31934 : A.Heathcote
//      Created.
#endregion
#endregion
using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{   
    public partial class UserEditorSettingsSelectedColumn
    {
        #region Constructor 
        private UserEditorSettingsSelectedColumn(){ } //force use of the factory methods
        #endregion

        #region Factory Methods 

        internal static UserEditorSettingsSelectedColumn GetUserEditorSettingsSelectedColumn(IDalContext dalcontext, UserEditorSettingsSelectedColumnDto dto)
        {
            return DataPortal.FetchChild<UserEditorSettingsSelectedColumn>(dalcontext, dto);
        }

        #endregion

        #region DataAccess 
        #region Data Transfer 

        private UserEditorSettingsSelectedColumnDto GetDataTransferObjects()
        {
            return new UserEditorSettingsSelectedColumnDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                FieldPlaceHolder = ReadProperty<String>(FieldPlaceHolderProperty),
                ColumnType = (Byte)ReadProperty<SearchColumnType>(ColumnTypeProperty),
            };
        }


        private void LoadDataTransferObject(IDalContext dalcontext, UserEditorSettingsSelectedColumnDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<String>(FieldPlaceHolderProperty, dto.FieldPlaceHolder);
            LoadProperty<SearchColumnType>(ColumnTypeProperty, (SearchColumnType)dto.ColumnType);
        }
        #endregion

        #region Insert
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalcontext)
        {
            UserEditorSettingsSelectedColumnDto dto = GetDataTransferObjects();
            Int32 oldId = dto.Id;
            using (IUserEditorSettingsSelectedColumnDal dal = dalcontext.GetDal<IUserEditorSettingsSelectedColumnDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalcontext.RegisterId<UserEditorSettingsSelectedColumn>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalcontext, this);
        }

        #endregion

        #region Update
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            if (this.IsSelfDirty)
            {
                using (IUserEditorSettingsSelectedColumnDal dal = dalContext.GetDal<IUserEditorSettingsSelectedColumnDal>())
                {
                    dal.Update(this.GetDataTransferObjects());
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion
        #region Fetch 
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, UserEditorSettingsSelectedColumnDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion
        #region Delete
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IUserEditorSettingsSelectedColumnDal dal = dalContext.GetDal<IUserEditorSettingsSelectedColumnDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion
        #endregion

    }
}
