﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created (Auto-generated)
// V8-26401 : A.Silva ~ Modified the Create Method to allow instantiating with a default number of groups already in the collection.
// V8-27152 : A.Silva ~ Added factory method to create a new group list using the existing list in a given IValidationTemplate.

#endregion

#region Version History: (CCM v8.01)
// V8-28023 : L.Ineson
//  Added Add(String groupName)
#endregion

#endregion

using System;
using System.Collections.Generic;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Model representing a list of ValidationTemplateGroup objects.
    /// </summary>
    [Serializable]
    public sealed partial class ValidationTemplateGroupList : ModelList<ValidationTemplateGroupList, ValidationTemplateGroup>
    {

        #region Authorization Rules
        /// <summary>
        ///     Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ValidationTemplateGroupList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ValidationTemplateGet.ToString()));
            BusinessRules.AddRule(typeof(ValidationTemplateGroupList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ValidationTemplateCreate.ToString()));
            BusinessRules.AddRule(typeof(ValidationTemplateGroupList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ValidationTemplateEdit.ToString()));
            BusinessRules.AddRule(typeof(ValidationTemplateGroupList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ValidationTemplateDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        ///     Creates a new instance of this type
        /// </summary>
        public static ValidationTemplateGroupList NewValidationTemplateGroupList()
        {
            var item = new ValidationTemplateGroupList();
            item.Create();
            return item;
        }

        /// <summary>
        ///     Creates a new instance of <see cref="ValidationTemplateGroupList"/> adding the items already present in the given <paramref name="groups"/> list.
        /// </summary>
        /// <param name="groups">List of instances of <see cref="IValidationTemplateGroup"/> to be added to the newly created <see cref="ValidationTemplateGroupList"/>.</param>
        /// <returns>A new instance of <see cref="ValidationTemplateGroupList"/> intialized with the values from the given <paramref name="groups"/>.</returns>
        public static ValidationTemplateGroupList NewValidationTemplateGroupList(IEnumerable<IValidationTemplateGroup> groups)
        {
            var item = new ValidationTemplateGroupList();
            item.Create(groups);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        ///     Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }

        /// <summary>
        ///     Called when creating a new instance of this type
        /// </summary>
        private void Create(IEnumerable<IValidationTemplateGroup> groups)
        {
            foreach (var templateGroup in groups)
            {
                this.Add(ValidationTemplateGroup.NewValidationTemplateGroup(templateGroup));
            }

            this.Create();
        }

        #endregion

        #endregion

        #region Methods

        public new ValidationTemplateGroup this[int index]
        {
            get { return Count > index ? base[index] : null; }
        }


        /// <summary>
        /// Adds a new group with the given name
        /// </summary>
        public ValidationTemplateGroup Add(String groupName)
        {
            ValidationTemplateGroup newGroup = ValidationTemplateGroup.NewValidationTemplateGroup(groupName);
            this.Add(newGroup);
            return newGroup;
        }

        #endregion
    }
}