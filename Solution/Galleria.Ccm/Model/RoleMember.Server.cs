﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26234 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a role member within GFS
    /// </summary>
    public partial class RoleMember
    {
        #region Constructor
        private RoleMember() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a role member
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        /// <returns>The specified role member</returns>
        internal static RoleMember GetRoleMember(IDalContext dalContext, RoleMemberDto dto)
        {
            return DataPortal.FetchChild<RoleMember>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Creates a dto from this instance
        /// </summary>
        /// <param name="parent">The parent role</param>
        /// <returns>A role member dto</returns>
        private RoleMemberDto GetDataTransferObject(Role parent)
        {
            return new RoleMemberDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                RoleId = parent.Id,
                UserId = ReadProperty<Int32>(UserIdProperty)
            };
        }

        /// <summary>
        /// Creates a dto from this instance
        /// </summary>
        /// <param name="parent">The parent user</param>
        /// <returns>A role member dto</returns>
        private RoleMemberDto GetDataTransferObject(User parent)
        {
            return new RoleMemberDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                RoleId = ReadProperty<Int32>(RoleIdProperty),
                UserId = parent.Id
            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, RoleMemberDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            LoadProperty<Int32>(RoleIdProperty, dto.RoleId);
            LoadProperty<Int32>(UserIdProperty, dto.UserId);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when return FetchById
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, RoleMemberDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, Role parent)
        {
            RoleMemberDto dto = GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IRoleMemberDal dal = dalContext.GetDal<IRoleMemberDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            dalContext.RegisterId<RoleMember>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }

        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, User parent)
        {
            RoleMemberDto dto = GetDataTransferObject(parent);
            using (IRoleMemberDal dal = dalContext.GetDal<IRoleMemberDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this object in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, Role parent)
        {
            using (IRoleMemberDal dal = dalContext.GetDal<IRoleMemberDal>())
            {
                RoleMemberDto dto = GetDataTransferObject(parent);
                dal.Update(dto);
                LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }

        /// <summary>
        /// Called when updating this object in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, User parent)
        {
            using (IRoleMemberDal dal = dalContext.GetDal<IRoleMemberDal>())
            {
                RoleMemberDto dto = GetDataTransferObject(parent);
                dal.Update(dto);
                LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this instance is deleted
        /// </summary>
        /// <param name="dalContext"></param>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IRoleMemberDal dal = dalContext.GetDal<IRoleMemberDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion
    }
}
