﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26159 : L.Ineson
//      Created
// CCM : I.George
//      Added FetchByEntityIdIncludingDeletedCriteria
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// List of PerformanceSelectionInfo objects.
    /// </summary>
    [Serializable]
    public sealed partial class PerformanceSelectionInfoList : ModelReadOnlyList<PerformanceSelectionInfoList, PerformanceSelectionInfo>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PerformanceSelectionInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelectionInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelectionInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelectionInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByEntityId
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : Csla.CriteriaBase<FetchByEntityIdCriteria>
        {
            #region Properties

            public static readonly PropertyInfo<Int32> EntityIdProperty =
            RegisterProperty<Int32>(c => c.EntityId);

            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            #endregion

            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
            }
        }

        [Serializable]
        public class FetchByEntityIdIncludingDeletedCriteria : Csla.CriteriaBase<FetchByEntityIdIncludingDeletedCriteria>
        {
            #region Properties

            public static readonly PropertyInfo<Int32> EntityIdProperty =
            RegisterProperty<Int32>(c => c.EntityId);

            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }
            #endregion
             public FetchByEntityIdIncludingDeletedCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
            }
        }
        #endregion
    }
}
