﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
// V8-25757 : L.Ineson
//  Removed PlanogramGroup_Id link
//  Added Entity_Id link
// V8-27411 : M.Pettit
//  Added DateCompleted and UserId (owner) properties
// V8-28046 : N.Foster
//  Added WorkpackageType
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Made changes to improve performance of engine
#endregion
#region Version History: CCM810
// V8-29811 : A.Probyn
// Added PlanogramLocationType
// V8-29858 : M.Pettit
//  Planograms list property registration must be marked as lazy loaded.
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class Workpackage : ModelObject<Workpackage>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique worflow id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        internal static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// Returns the row version
        /// </summary>
        internal RowVersion RowVersion
        {
            get { return this.GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region EntityId
        /// <summary>
        /// EntityId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        /// <summary>
        /// Returns the planogram group id
        /// </summary>
        public Int32 EntityId
        {
            get { return this.GetProperty<Int32>(EntityIdProperty); }
            set { this.SetProperty<Int32>(EntityIdProperty, value); }
        }
        #endregion

        #region WorkflowId
        /// <summary>
        /// WorkflowId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> WorkflowIdProperty =
            RegisterModelProperty<Int32>(c => c.WorkflowId);
        /// <summary>
        /// Gets or set the workpackage workflow id
        /// </summary>
        public Int32 WorkflowId
        {
            get { return this.GetProperty<Int32>(WorkflowIdProperty); }
            set { this.SetProperty<Int32>(WorkflowIdProperty, value); }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Gets or sets the workflow name
        /// </summary>
        public String Name
        {
            get { return this.GetProperty<String>(NameProperty); }
            set { this.SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region WorkpackageType
        /// <summary>
        /// WorkpackageType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<WorkpackageType> WorkpackageTypeProperty =
            RegisterModelProperty<WorkpackageType>(c => c.WorkpackageType);
        /// <summary>
        /// Returns the workpackage type
        /// </summary>
        public WorkpackageType WorkpackageType
        {
            get { return this.ReadProperty<WorkpackageType>(WorkpackageTypeProperty); }
            set { this.SetProperty<WorkpackageType>(WorkpackageTypeProperty, value); }
        }
        #endregion

        #region PlanogramLocationType
        /// <summary>
        /// PlanogramLocationType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<WorkpackagePlanogramLocationType> PlanogramLocationTypeProperty =
            RegisterModelProperty<WorkpackagePlanogramLocationType>(c => c.PlanogramLocationType);
        /// <summary>
        /// Returns the PlanogramLocationType
        /// </summary>
        public WorkpackagePlanogramLocationType PlanogramLocationType
        {
            get { return this.ReadProperty<WorkpackagePlanogramLocationType>(PlanogramLocationTypeProperty); }
            set { this.SetProperty<WorkpackagePlanogramLocationType>(PlanogramLocationTypeProperty, value); }
        }
        #endregion

        #region UserId
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> UserIdProperty =
            RegisterModelProperty<Int32>(c => c.UserId);
        /// <summary>
        /// Gets or sets the workflow UserId
        /// </summary>
        public Int32 UserId
        {
            get { return this.GetProperty<Int32>(UserIdProperty); }
            set { this.SetProperty<Int32>(UserIdProperty, value); }
        }
        #endregion

        #region DateCreated
        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);
        /// <summary>
        /// Returns the date this workflow was created
        /// </summary>
        public DateTime DateCreated
        {
            get { return this.GetProperty<DateTime>(DateCreatedProperty); }
        }
        #endregion

        #region DateLastModified
        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        /// <summary>
        /// Returns the date this workflow was last modified
        /// </summary>
        public DateTime DateLastModified
        {
            get { return this.GetProperty<DateTime>(DateLastModifiedProperty); }
        }
        #endregion

        #region DateCompleted
        /// <summary>
        /// DateCompleted property definition
        /// </summary>
        public static ModelPropertyInfo<DateTime?> DateCompletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateCompleted);
        /// <summary>
        /// Returns the date this workflow was last completed
        /// </summary>
        public DateTime? DateCompleted
        {
            get { return this.GetProperty<DateTime?>(DateCompletedProperty); }
        }
        #endregion

        #region Planograms
        /// <summary>
        /// Planograms property definition
        /// </summary>
        public static readonly ModelPropertyInfo<WorkpackagePlanogramList> PlanogramsProperty =
            RegisterModelProperty<WorkpackagePlanogramList>(c => c.Planograms, RelationshipTypes.LazyLoad);
        /// <summary>
        /// Synchornously returns all planograms associated with this workpackage
        /// </summary>
        public WorkpackagePlanogramList Planograms
        {
            get
            {
                return this.GetPropertyLazy<WorkpackagePlanogramList>(
                    PlanogramsProperty,
                    new WorkpackagePlanogramList.FetchByWorkpackgeIdCriteria(this.Id, this.WorkflowId),
                    true);
            }
        }

        /// <summary>
        /// Asynchornously returns all planograms associated with this workpackage
        /// </summary>
        public WorkpackagePlanogramList PlanogramsAsync
        {
            get
            {
                return this.GetPropertyLazy<WorkpackagePlanogramList>(
                    PlanogramsProperty,
                    new WorkpackagePlanogramList.FetchByWorkpackgeIdCriteria(this.Id, this.WorkflowId),
                    true);
            }
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(Workpackage), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(Workpackage), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.WorkpackageCreate.ToString()));
            BusinessRules.AddRule(typeof(Workpackage), new IsInRole(AuthorizationActions.EditObject, DomainPermission.WorkpackageEdit.ToString()));
            BusinessRules.AddRule(typeof(Workpackage), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.WorkpackageDelete.ToString()));
        }
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
        }
        #endregion

        #region Criteria

        #region FetchByWorkpackageIdCriteria
        [Serializable]
        public class FetchByWorkpackageIdCriteria : CriteriaBase<FetchByWorkpackageIdCriteria>
        {
            #region Properties

            #region WorkpackageId
            /// <summary>
            /// WorkpackageId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> WorkpackageIdProperty =
                RegisterProperty<Int32>(c => c.WorkpackageId);
            /// <summary>
            /// Returns the workpackage id
            /// </summary>
            public Int32 WorkpackageId
            {
                get { return this.ReadProperty<Int32>(WorkpackageIdProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByWorkpackageIdCriteria(Int32 workpackageId)
            {
                this.LoadProperty<Int32>(WorkpackageIdProperty, workpackageId);
            }
            #endregion
        }
        #endregion

        #region FetchByWorkpackageIdCriteria
        [Serializable]
        public class FetchByWorkpackageIdDestinationPlanogramIdCriteria : CriteriaBase<FetchByWorkpackageIdDestinationPlanogramIdCriteria>
        {
            #region Properties

            #region WorkpackageId
            /// <summary>
            /// WorkpackageId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> WorkpackageIdProperty =
                RegisterProperty<Int32>(c => c.WorkpackageId);
            /// <summary>
            /// Returns the workpackage id
            /// </summary>
            public Int32 WorkpackageId
            {
                get { return this.ReadProperty<Int32>(WorkpackageIdProperty); }
            }
            #endregion

            #region DestinationPlanogramId
            /// <summary>
            /// DestinationPlanogramId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> DestinationPlanogramIdProperty =
                RegisterProperty<Int32>(c => c.DestinationPlanogramId);
            /// <summary>
            /// Returns the destination planogram id
            /// </summary>
            public Int32 DestinationPlanogramId
            {
                get { return this.ReadProperty<Int32>(DestinationPlanogramIdProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByWorkpackageIdDestinationPlanogramIdCriteria(Int32 workpackageId, Int32 destinationPlanogramId)
            {
                this.LoadProperty<Int32>(WorkpackageIdProperty, workpackageId);
                this.LoadProperty<Int32>(DestinationPlanogramIdProperty, destinationPlanogramId);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Factory Methods

        #region NewWorkpackage

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static Workpackage NewWorkpackage(Entity entity)
        {
            return NewWorkpackage(entity.Id);
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static Workpackage NewWorkpackage(Int32 entityId)
        {
            Workpackage item = new Workpackage();
            item.Create(entityId);
            return item;
        }

        #endregion

        #region DeleteById
        /// <summary>
        /// Deletes the workpackage with the given id.
        /// </summary>
        /// <param name="workpackageId"></param>
        public static void DeleteById(Int32 workpackageId)
        {
            DataPortal.Execute<DeleteByIdCommand>(new DeleteByIdCommand(workpackageId));
        }
        #endregion

        #region Execute
        /// <summary>
        /// Submits the specified workpackage
        /// for execution by the engine
        /// </summary>
        public static void Execute(Int32 entityId, Int32 workpackageId, String workpackageName, RowVersion workpackageRowVersion)
        {
            DataPortal.Execute<ExecuteCommand>(new ExecuteCommand(entityId, workpackageId, workpackageName, workpackageRowVersion));
        }
        #endregion

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected void Create(Int32 entityId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.LoadProperty<String>(NameProperty, Message.Workpackage_DefaultName);
            this.LoadProperty<WorkpackageType>(WorkpackageTypeProperty, WorkpackageType.Unknown);
            this.LoadProperty<Int32>(UserIdProperty, (Int32)DomainPrincipal.CurrentUserId);
            this.LoadProperty<WorkpackagePlanogramList>(PlanogramsProperty, WorkpackagePlanogramList.NewWorkpackagePlanogramList());
            this.LoadProperty<WorkpackagePlanogramLocationType>(PlanogramLocationTypeProperty, WorkpackagePlanogramLocationType.InNewSubFolder);
            base.Create();
        }
        #endregion

        #endregion

        #region Commands

        #region DeleteById

        /// <summary>
        /// Deletes the workpackage with the given id.
        /// </summary>
        [Serializable]
        private partial class DeleteByIdCommand : CommandBase<DeleteByIdCommand>
        {
            #region Authorization Rules
            /// <summary>
            /// Defines the authorization rules for this type
            /// </summary>
            private static void AddObjectAuthorizationRules()
            {
                BusinessRules.AddRule(typeof(DeleteByIdCommand), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(DeleteByIdCommand), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(DeleteByIdCommand), new IsInRole(AuthorizationActions.EditObject, DomainPermission.WorkpackageDelete.ToString()));
                BusinessRules.AddRule(typeof(DeleteByIdCommand), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
            }
            #endregion

            #region Properties

            #region WorkpackageId
            /// <summary>
            /// WorkpackageId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> WorkpackageIdProperty =
                RegisterProperty<Int32>(c => c.WorkpackageId);
            /// <summary>
            /// Returns the workpackage id
            /// </summary>
            public Int32 WorkpackageId
            {
                get { return this.ReadProperty<Int32>(WorkpackageIdProperty); }
            }
            #endregion


            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public DeleteByIdCommand(Int32 workpackageId)
            {
                this.LoadProperty<Int32>(WorkpackageIdProperty, workpackageId);
            }
            #endregion
        }

        #endregion

        #region Execute
        /// <summary>
        /// Submits this workpackage for execution
        /// by the engine
        /// </summary>
        [Serializable]
        public partial class ExecuteCommand : CommandBase<ExecuteCommand>
        {
            #region Authorization Rules
            /// <summary>
            /// Defines the authorization rules for this type
            /// </summary>
            private static void AddObjectAuthorizationRules()
            {
                BusinessRules.AddRule(typeof(ExecuteCommand), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(ExecuteCommand), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(ExecuteCommand), new IsInRole(AuthorizationActions.EditObject, DomainPermission.WorkpackageExecute.ToString()));
                BusinessRules.AddRule(typeof(ExecuteCommand), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
            }
            #endregion

            #region Properties

            #region EntityId
            /// <summary>
            /// EntityId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            /// <summary>
            /// Returns the entity id
            /// </summary>
            public Int32 EntityId
            {
                get { return this.ReadProperty<Int32>(EntityIdProperty); }
            }
            #endregion

            #region WorkpackageId
            /// <summary>
            /// WorkpackageId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> WorkpackageIdProperty =
                RegisterProperty<Int32>(c => c.WorkpackageId);
            /// <summary>
            /// Returns the workpackage id
            /// </summary>
            public Int32 WorkpackageId
            {
                get { return this.ReadProperty<Int32>(WorkpackageIdProperty); }
            }
            #endregion

            #region WorkpackageName
            /// <summary>
            /// Workpackage name definition
            /// </summary>
            public static readonly PropertyInfo<String> WorkpackageNameProperty =
                RegisterProperty<String>(c => c.WorkpackageName);
            /// <summary>
            /// Returns the workpackage name
            /// </summary>
            public String WorkpackageName
            {
                get { return this.ReadProperty<String>(WorkpackageNameProperty); }
            }
            #endregion

            #region WorkpackageRowVersion
            /// <summary>
            /// WorkpackageRowVersion property definition
            /// </summary>
            public static readonly PropertyInfo<RowVersion> WorkpackageRowVersionProperty =
                RegisterProperty<RowVersion>(c => c.WorkpackageRowVersion);
            /// <summary>
            /// Returns the workpackage row version
            /// </summary>
            public RowVersion WorkpackageRowVersion
            {
                get { return this.ReadProperty<RowVersion>(WorkpackageRowVersionProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public ExecuteCommand(Int32 entityId, Int32 workpackageId, String workpackageName, RowVersion workpackageRowVersion)
            {
                this.LoadProperty<Int32>(EntityIdProperty, entityId);
                this.LoadProperty<Int32>(WorkpackageIdProperty, workpackageId);
                this.LoadProperty<String>(WorkpackageNameProperty, workpackageName);
                this.LoadProperty<RowVersion>(WorkpackageRowVersionProperty, workpackageRowVersion);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Submits this workpackage to be
        /// executed by the engine
        /// </summary>
        public void Execute()
        {
            // the workpackage must have been
            // saved in order to execute
            if (this.IsNew) throw new InvalidOperationException();

            // execute the workpackage
            Execute(this.EntityId, this.Id, this.Name, this.RowVersion);
        }

        /// <summary>
        /// Increments the progress of a specified workpackage
        /// </summary>
        public void IncrementProgress()
        {
            // the workpackage must have been
            // saved in order to increment progress
            if (this.IsNew) throw new InvalidOperationException();

            WorkpackageProcessingStatus.IncrementProgress(this.Id, "TODO", DateTime.UtcNow);
        }
        #endregion
    }
}
