﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (GFS 1.0)
// GFS-27505 : J.Pickup
//  Created (Copied over from GFS)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Enum of values available for the ProductAttribute Type property
    /// </summary>
    public enum ProductAttributeDataType
    {
        Text = 1,
        Integer = 2,
        Decimal = 3,
        Currency = 4,
        Boolean = 5
    }


    public static class ProductAttributeDataTypeHelper
    {
        public static readonly Dictionary<ProductAttributeDataType, String> FriendlyNames =
            new Dictionary<ProductAttributeDataType, String>()
            {
                {ProductAttributeDataType.Boolean, Message.Enum_ProductAttributeType_Boolean},
                {ProductAttributeDataType.Currency, Message.Enum_ProductAttributeType_Currency},
                {ProductAttributeDataType.Decimal, Message.Enum_ProductAttributeType_Decimal},
                {ProductAttributeDataType.Integer, Message.Enum_ProductAttributeType_Integer},
                {ProductAttributeDataType.Text, Message.Enum_ProductAttributeType_Text},
            };

        public static ProductAttributeDataType? ProductAttributeTypeGetEnum(String description)
        {
            foreach (KeyValuePair<ProductAttributeDataType, String> keyPair in ProductAttributeDataTypeHelper.FriendlyNames)
            {
                if (keyPair.Value.Equals(description))
                {
                    return keyPair.Key;
                }
            }
            return null;
        }
    }
}
