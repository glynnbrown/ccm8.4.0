﻿#region Header Information

#region Version History:(CCM800)
//CCM-26124 : I.George
// Initial Version
#endregion

#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class InventoryProfileInfoList : ModelReadOnlyList<InventoryProfileInfoList, InventoryProfileInfo>
    {
        #region Authorization
        /// <summary>
        /// Defines the authorization rule for this type
        /// </summary>
        public static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(InventoryProfileInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(InventoryProfileInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(InventoryProfileInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(InventoryProfileInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region  Criteria
        /// <summary>
        /// Criteria for the Metric List FetchByEntityId
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : CriteriaBase<FetchByEntityIdCriteria>
        {
            #region Properties

            public static PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            #endregion

            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
            }
        }
        #endregion
    }
}
