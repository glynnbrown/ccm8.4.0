﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;
using System.IO;


namespace Galleria.Ccm.Model
{
    /// <summary>
    /// An enum which defines the available paper sizes for printing
    /// </summary>
    public enum PrintTemplatePaperSizeType
    {
        A3 = 0,
        A4 = 1,
        A5 = 2,
        B4 = 3,
        B5 = 4,
        Executive = 5,
        Legal = 6,
        Letter = 7
    }
}
