﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-V8-24863 : L.Hodson
//	Created
// CCM-24979 : L.Hodson
//  Added FixtureLibraryLocation property
// V8-25395 : A.Probyn
//  Updated IsLibraryPanelVisibleProperty to be false on default
//  Added ProductLibraryLocation & LastUsedProductLibraryLocation
// V8-27155 : L.Luong
//  Added ProductPlacement and Default Dimensions & Colours Properties
// V8-27411 : M.Pettit
//  Added LastSuccessfulSyncDate and LastFailedSyncDate
#endregion
#region Version History: CCM801
// V8-28679 : A.Silva
//  Added IRealImageProviderSettings implementation.
// V8-28951 : A.Silva
//  Amended default values for ProductImageSourceProperty (RealImageProviderType.Folder) and ProductImageAttributeProperty (now using the Gtin property from PlanogramProduct).
#endregion
#region Version History: CCM803
// V8-29373 : N.Foster
//  Added IPlanogramSettings interface
// V8-28662 : L.Ineson
//  Added planogram image settings.
#endregion
#region Version History: CCM810
// V8-29592 : L.Ineson
//  Added imperial defaults
#endregion
#region Version History: CCM820
// V8-30936 : M.Brumby
//  Slotwall defaults
#endregion
#region Version History: CCM830
// V8-32626 : A.Probyn
//  Added Annotations
// V8-32591 : L.Ineson
//  Added TextBox defaults.
#endregion
#endregion

using System;
using System.Globalization;
using System.IO;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Holds the system settings for the current user and current entity
    /// (Child of Entity)
    /// </summary>
    [Serializable]
    public sealed partial class SystemSettings : ModelObject<SystemSettings>,
        IRealImageProviderSettings,
        IPlanogramSettings
    {
        #region Parent

        public Entity ParentEntity
        {
            get { return base.Parent as Entity; }
        }

        #endregion

        #region Properties

        #region FoundationServicesEndpoint
        /// <summary>
        /// FoundationServicesEndpoint property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> FoundationServicesEndpointProperty =
            RegisterModelProperty<String>(c => c.FoundationServicesEndpoint);
        
        /// <summary>
        /// Holds the endpoint url for the galleria foundation web services
        /// </summary>
        public String FoundationServicesEndpoint
        {
            get { return this.GetProperty<String>(FoundationServicesEndpointProperty); }
            set { this.SetProperty<String>(FoundationServicesEndpointProperty, value); }
        }
        #endregion

        #region CompressionName
        
        /// <summary>
        /// CompressionName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CompressionNameProperty =
            RegisterModelProperty<String>(c => c.CompressionName);

        /// <summary>
        /// Holds the compression name for retrieving product images from GFS
        /// </summary>
        public String CompressionName
        {
            get { return this.GetProperty<String>(CompressionNameProperty); }
            set { this.SetProperty<String>(CompressionNameProperty, value); }
        }
        #endregion

        #region LengthUnitOfMeasure
        /// <summary>
        /// LengthUnitsOfMeasure Property definition
        /// </summary>
        public static readonly ModelPropertyInfo<UnitOfMeasureLengthType> LengthUnitOfMeasureProperty =
            RegisterModelProperty<UnitOfMeasureLengthType>(c => c.LengthUnitsOfMeasure, Message.Entity_LengthUnitOfMeasure);
        /// <summary>
        /// Indicates the units of measure for length values
        /// 0=unknown, 1 = Centimeters, 2 = Inches
        /// </summary>
        public UnitOfMeasureLengthType LengthUnitsOfMeasure
        {
            get { return this.GetProperty<UnitOfMeasureLengthType>(LengthUnitOfMeasureProperty); }
            set { this.SetProperty<UnitOfMeasureLengthType>(LengthUnitOfMeasureProperty, value); }
        }
        #endregion

        #region AreaUnitsOfMeasure
        /// <summary>
        /// Area Unit of Measure Area property
        /// </summary>
        public static readonly ModelPropertyInfo<UnitOfMeasureAreaType> AreaUnitsOfMeasureProperty =
            RegisterModelProperty<UnitOfMeasureAreaType>(c => c.AreaUnitsOfMeasure, Message.Entity_AreaUnitsOfMeasure);
        /// <summary>
        /// indicates the unit of measure for area value
        /// 0 = unknown, 1= Square Centimeters, 2= Square Inches
        /// </summary>
        public UnitOfMeasureAreaType AreaUnitsOfMeasure
        {
            get { return this.GetProperty<UnitOfMeasureAreaType>(AreaUnitsOfMeasureProperty); }
            set { this.SetProperty<UnitOfMeasureAreaType>(AreaUnitsOfMeasureProperty, value); }
        }

        #endregion

        #region AngleUnitsOfMeasure
        /// <summary>
        /// Angle units of measure property
        /// </summary>
        public static readonly ModelPropertyInfo<UnitOfMeasureAngleType> AngleUnitsOfMeasureProperty =
            RegisterModelProperty<UnitOfMeasureAngleType>(c => c.AngleUnitsOfMeasure, Message.Entity_AngleUnitsOfMeasure);
        /// <summary>
        /// indicates the unit of measure of the angles
        /// 0= Unknown, 1=Radians, 2=Degrees
        /// </summary>
        public UnitOfMeasureAngleType AngleUnitsOfMeasure
        {
            get { return this.GetProperty<UnitOfMeasureAngleType>(AngleUnitsOfMeasureProperty); }
            set { this.SetProperty<UnitOfMeasureAngleType>(AngleUnitsOfMeasureProperty, value); }
        }
        #endregion

        #region VolumeUnitsOfmeasure
        /// <summary>
        /// Volume Unit of measure Property
        /// </summary>
        public static readonly ModelPropertyInfo<UnitOfMeasureVolumeType> VolumeUnitsOfMeasureProperty =
            RegisterModelProperty<UnitOfMeasureVolumeType>(c => c.VolumeUnitsOfMeasure, Message.Entity_VolumeUnitsOfMeasure);

        /// <summary>
        ///  Indicates the units of measure for volume values
        ///  0 = Unknown, 1 = litres, 2 = Cubic Feet
        /// </summary>
        public UnitOfMeasureVolumeType VolumeUnitsOfMeasure
        {
            get { return this.GetProperty<UnitOfMeasureVolumeType>(VolumeUnitsOfMeasureProperty); }
            set { this.SetProperty<UnitOfMeasureVolumeType>(VolumeUnitsOfMeasureProperty, value); }
        }
        #endregion

        #region WeightUnitsOfMeasure
        /// <summary>
        /// Indicates the units of measure for Weight values
        ///  0 = Unknown, 1 = Kilograms , 2 = Pounds
        /// </summary>
        public static readonly ModelPropertyInfo<UnitOfMeasureWeightType> WeightUnitsOfMeasureProperty =
        RegisterModelProperty<UnitOfMeasureWeightType>(c => c.WeightUnitsOfMeasure, Message.Entity_WeightUnitsOfmeasure);

        public UnitOfMeasureWeightType WeightUnitsOfMeasure
        {
            get { return this.GetProperty<UnitOfMeasureWeightType>(WeightUnitsOfMeasureProperty); }
            set { this.SetProperty<UnitOfMeasureWeightType>(WeightUnitsOfMeasureProperty, value); }
        }

        #endregion

        #region CurrencyUnitsOfMeasure
        /// <summary>
        /// Currency Unit of measure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<UnitOfMeasureCurrencyType> CurrencyUnitsOfMesasureProperty =
            RegisterModelProperty<UnitOfMeasureCurrencyType>(c => c.CurrencyUnitsOfMeasure, Message.Entity_CurrencyUnitsOfMeasure);

        /// <summary>
        /// indicates the units of measure of the angle
        /// </summary>
        public UnitOfMeasureCurrencyType CurrencyUnitsOfMeasure
        {
            get { return this.GetProperty<UnitOfMeasureCurrencyType>(CurrencyUnitsOfMesasureProperty); }
            set { this.SetProperty<UnitOfMeasureCurrencyType>(CurrencyUnitsOfMesasureProperty, value); }
        }
        #endregion

        #region ProductPlacementX

        /// <summary>
        /// Product Placement X property definition
        /// </summary>
        public static readonly ModelPropertyInfo<ProductPlacementXType> ProductPlacementXProperty =
          RegisterModelProperty<ProductPlacementXType>(c => c.ProductPlacementX, Message.Entity_ProductPlacementX);

        /// <summary>
        /// Gets/Sets the default Product Placement X uom
        /// </summary>
        public ProductPlacementXType ProductPlacementX
        {
            get { return GetProperty<ProductPlacementXType>(ProductPlacementXProperty); }
            set { SetProperty<ProductPlacementXType>(ProductPlacementXProperty, value); }
        }

        #endregion

        #region ProductPlacementY

        /// <summary>
        /// Product Placement Y property definition
        /// </summary>
        public static readonly ModelPropertyInfo<ProductPlacementYType> ProductPlacementYProperty =
          RegisterModelProperty<ProductPlacementYType>(c => c.ProductPlacementY, Message.Entity_ProductPlacementY);

        /// <summary>
        /// Gets/Sets the default Product Placement Y uom
        /// </summary>
        public ProductPlacementYType ProductPlacementY
        {
            get { return GetProperty<ProductPlacementYType>(ProductPlacementYProperty); }
            set { SetProperty<ProductPlacementYType>(ProductPlacementYProperty, value); }
        }

        #endregion

        #region ProductPlacementZ

        /// <summary>
        /// Product Placement Z property definition
        /// </summary>
        public static readonly ModelPropertyInfo<ProductPlacementZType> ProductPlacementZProperty =
          RegisterModelProperty<ProductPlacementZType>(c => c.ProductPlacementZ, Message.Entity_ProductPlacementZ);

        /// <summary>
        /// Gets/Sets the default Product Placement Z uom
        /// </summary>
        public ProductPlacementZType ProductPlacementZ
        {
            get { return GetProperty<ProductPlacementZType>(ProductPlacementZProperty); }
            set { SetProperty<ProductPlacementZType>(ProductPlacementZProperty, value); }
        }

        #endregion

        #region Default Dimensions & Colours

        #region ProductHeight

        /// <summary>
        /// ProductHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ProductHeightProperty =
          RegisterModelProperty<Single>(c => c.ProductHeight);

        /// <summary>
        /// Gets/Sets the default ProductHeight
        /// </summary>
        public Single ProductHeight
        {
            get { return GetProperty<Single>(ProductHeightProperty); }
            set { SetProperty<Single>(ProductHeightProperty, value); }
        }

        #endregion

        #region ProductWidth

        /// <summary>
        /// ProductWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ProductWidthProperty =
          RegisterModelProperty<Single>(c => c.ProductWidth);

        /// <summary>
        /// Gets/Sets the default ProductWidth
        /// </summary>
        public Single ProductWidth
        {
            get { return GetProperty<Single>(ProductWidthProperty); }
            set { SetProperty<Single>(ProductWidthProperty, value); }
        }

        #endregion

        #region ProductDepth

        /// <summary>
        /// ProductDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ProductDepthProperty =
          RegisterModelProperty<Single>(c => c.ProductDepth);

        /// <summary>
        /// Gets/Sets the default ProductDepth
        /// </summary>
        public Single ProductDepth
        {
            get { return GetProperty<Single>(ProductDepthProperty); }
            set { SetProperty<Single>(ProductDepthProperty, value); }
        }

        #endregion

        #region FixtureHeight

        /// <summary>
        /// FixtureHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FixtureHeightProperty =
          RegisterModelProperty<Single>(c => c.FixtureHeight);

        /// <summary>
        /// Gets/Sets the default FixtureHeight
        /// </summary>
        public Single FixtureHeight
        {
            get { return GetProperty<Single>(FixtureHeightProperty); }
            set { SetProperty<Single>(FixtureHeightProperty, value); }
        }

        #endregion

        #region FixtureWidth

        /// <summary>
        /// FixtureWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FixtureWidthProperty =
          RegisterModelProperty<Single>(c => c.FixtureWidth);

        /// <summary>
        /// Gets/Sets the default FixtureWidth
        /// </summary>
        public Single FixtureWidth
        {
            get { return GetProperty<Single>(FixtureWidthProperty); }
            set { SetProperty<Single>(FixtureWidthProperty, value); }
        }

        #endregion

        #region FixtureDepth

        /// <summary>
        /// FixtureDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FixtureDepthProperty =
          RegisterModelProperty<Single>(c => c.FixtureDepth);

        /// <summary>
        /// Gets/Sets the default FixtureDepth
        /// </summary>
        public Single FixtureDepth
        {
            get { return GetProperty<Single>(FixtureDepthProperty); }
            set { SetProperty<Single>(FixtureDepthProperty, value); }
        }

        #endregion

        #region FixtureHasBackboard

        /// <summary>
        /// FixtureHasBackboard property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> FixtureHasBackboardProperty =
          RegisterModelProperty<Boolean>(c => c.FixtureHasBackboard);

        /// <summary>
        /// Gets/Sets the default FixtureHasBackboard
        /// </summary>
        public Boolean FixtureHasBackboard
        {
            get { return GetProperty<Boolean>(FixtureHasBackboardProperty); }
            set { SetProperty<Boolean>(FixtureHasBackboardProperty, value); }
        }

        #endregion

        #region BackboardDepth

        /// <summary>
        /// BackboardDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BackboardDepthProperty =
          RegisterModelProperty<Single>(c => c.BackboardDepth);

        /// <summary>
        /// Gets/Sets the default BackboardDepth
        /// </summary>
        public Single BackboardDepth
        {
            get { return GetProperty<Single>(BackboardDepthProperty); }
            set { SetProperty<Single>(BackboardDepthProperty, value); }
        }

        #endregion

        #region FixtureHasBase

        /// <summary>
        /// FixtureHasBase property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> FixtureHasBaseProperty =
          RegisterModelProperty<Boolean>(c => c.FixtureHasBase);

        /// <summary>
        /// Gets/Sets the default FixtureHasBase
        /// </summary>
        public Boolean FixtureHasBase
        {
            get { return GetProperty<Boolean>(FixtureHasBaseProperty); }
            set { SetProperty<Boolean>(FixtureHasBaseProperty, value); }
        }

        #endregion

        #region BaseHeight

        /// <summary>
        /// BaseHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BaseHeightProperty =
          RegisterModelProperty<Single>(c => c.BaseHeight);

        /// <summary>
        /// Gets/Sets the default BaseHeight
        /// </summary>
        public Single BaseHeight
        {
            get { return GetProperty<Single>(BaseHeightProperty); }
            set { SetProperty<Single>(BaseHeightProperty, value); }
        }

        #endregion

        #region ShelfHeight

        /// <summary>
        /// ShelfHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ShelfHeightProperty =
          RegisterModelProperty<Single>(c => c.ShelfHeight);

        /// <summary>
        /// Gets/Sets the default ShelfHeight
        /// </summary>
        public Single ShelfHeight
        {
            get { return GetProperty<Single>(ShelfHeightProperty); }
            set { SetProperty<Single>(ShelfHeightProperty, value); }
        }

        #endregion

        #region ShelfWidth

        /// <summary>
        /// ShelfWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ShelfWidthProperty =
          RegisterModelProperty<Single>(c => c.ShelfWidth);

        /// <summary>
        /// Gets/Sets the default ShelfWidth
        /// </summary>
        public Single ShelfWidth
        {
            get { return GetProperty<Single>(ShelfWidthProperty); }
            set { SetProperty<Single>(ShelfWidthProperty, value); }
        }

        #endregion

        #region ShelfDepth

        /// <summary>
        /// ShelfDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ShelfDepthProperty =
          RegisterModelProperty<Single>(c => c.ShelfDepth);

        /// <summary>
        /// Gets/Sets the default ShelfDepth
        /// </summary>
        public Single ShelfDepth
        {
            get { return GetProperty<Single>(ShelfDepthProperty); }
            set { SetProperty<Single>(ShelfDepthProperty, value); }
        }

        #endregion

        #region ShelfFillColour

        /// <summary>
        /// ShelfFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ShelfFillColourProperty =
          RegisterModelProperty<Int32>(c => c.ShelfFillColour);

        /// <summary>
        /// Gets/Sets the default ShelfFillColour
        /// </summary>
        public Int32 ShelfFillColour
        {
            get { return GetProperty<Int32>(ShelfFillColourProperty); }
            set { SetProperty<Int32>(ShelfFillColourProperty, value); }
        }

        #endregion

        #region PegboardHeight

        /// <summary>
        /// PegboardHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegboardHeightProperty =
          RegisterModelProperty<Single>(c => c.PegboardHeight);

        /// <summary>
        /// Gets/Sets the default PegboardHeight
        /// </summary>
        public Single PegboardHeight
        {
            get { return GetProperty<Single>(PegboardHeightProperty); }
            set { SetProperty<Single>(PegboardHeightProperty, value); }
        }

        #endregion

        #region PegboardWidth

        /// <summary>
        /// PegboardWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegboardWidthProperty =
          RegisterModelProperty<Single>(c => c.PegboardWidth);

        /// <summary>
        /// Gets/Sets the default PegboardWidth
        /// </summary>
        public Single PegboardWidth
        {
            get { return GetProperty<Single>(PegboardWidthProperty); }
            set { SetProperty<Single>(PegboardWidthProperty, value); }
        }

        #endregion

        #region PegboardDepth

        /// <summary>
        /// PegboardDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegboardDepthProperty =
          RegisterModelProperty<Single>(c => c.PegboardDepth);

        /// <summary>
        /// Gets/Sets the default PegboardDepth
        /// </summary>
        public Single PegboardDepth
        {
            get { return GetProperty<Single>(PegboardDepthProperty); }
            set { SetProperty<Single>(PegboardDepthProperty, value); }
        }

        #endregion

        #region PegboardFillColour

        /// <summary>
        /// PegboardFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PegboardFillColourProperty =
          RegisterModelProperty<Int32>(c => c.PegboardFillColour);

        /// <summary>
        /// Gets/Sets the default PegboardFillColour
        /// </summary>
        public Int32 PegboardFillColour
        {
            get { return GetProperty<Int32>(PegboardFillColourProperty); }
            set { SetProperty<Int32>(PegboardFillColourProperty, value); }
        }

        #endregion

        #region ChestHeight

        /// <summary>
        /// ChestHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ChestHeightProperty =
          RegisterModelProperty<Single>(c => c.ChestHeight);

        /// <summary>
        /// Gets/Sets the default ChestHeight
        /// </summary>
        public Single ChestHeight
        {
            get { return GetProperty<Single>(ChestHeightProperty); }
            set { SetProperty<Single>(ChestHeightProperty, value); }
        }

        #endregion

        #region ChestWidth

        /// <summary>
        /// ChestWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ChestWidthProperty =
          RegisterModelProperty<Single>(c => c.ChestWidth);

        /// <summary>
        /// Gets/Sets the default ChestWidth
        /// </summary>
        public Single ChestWidth
        {
            get { return GetProperty<Single>(ChestWidthProperty); }
            set { SetProperty<Single>(ChestWidthProperty, value); }
        }

        #endregion

        #region ChestDepth

        /// <summary>
        /// ChestDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ChestDepthProperty =
          RegisterModelProperty<Single>(c => c.ChestDepth);

        /// <summary>
        /// Gets/Sets the default ChestDepth
        /// </summary>
        public Single ChestDepth
        {
            get { return GetProperty<Single>(ChestDepthProperty); }
            set { SetProperty<Single>(ChestDepthProperty, value); }
        }

        #endregion

        #region ChestFillColour

        /// <summary>
        /// ChestFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ChestFillColourProperty =
          RegisterModelProperty<Int32>(c => c.ChestFillColour);

        /// <summary>
        /// Gets/Sets the default ChestFillColour
        /// </summary>
        public Int32 ChestFillColour
        {
            get { return GetProperty<Int32>(ChestFillColourProperty); }
            set { SetProperty<Int32>(ChestFillColourProperty, value); }
        }

        #endregion

        #region BarHeight

        /// <summary>
        /// BarHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BarHeightProperty =
          RegisterModelProperty<Single>(c => c.BarHeight);

        /// <summary>
        /// Gets/Sets the default BarHeight
        /// </summary>
        public Single BarHeight
        {
            get { return GetProperty<Single>(BarHeightProperty); }
            set { SetProperty<Single>(BarHeightProperty, value); }
        }

        #endregion

        #region BarWidth

        /// <summary>
        /// BarWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BarWidthProperty =
          RegisterModelProperty<Single>(c => c.BarWidth);

        /// <summary>
        /// Gets/Sets the default BarWidth
        /// </summary>
        public Single BarWidth
        {
            get { return GetProperty<Single>(BarWidthProperty); }
            set { SetProperty<Single>(BarWidthProperty, value); }
        }

        #endregion

        #region BarDepth

        /// <summary>
        /// BarDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BarDepthProperty =
          RegisterModelProperty<Single>(c => c.BarDepth);

        /// <summary>
        /// Gets/Sets the default BarDepth
        /// </summary>
        public Single BarDepth
        {
            get { return GetProperty<Single>(BarDepthProperty); }
            set { SetProperty<Single>(BarDepthProperty, value); }
        }

        #endregion

        #region BarFillColour

        /// <summary>
        /// BarFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> BarFillColourProperty =
          RegisterModelProperty<Int32>(c => c.BarFillColour);

        /// <summary>
        /// Gets/Sets the default BarFillColour
        /// </summary>
        public Int32 BarFillColour
        {
            get { return GetProperty<Int32>(BarFillColourProperty); }
            set { SetProperty<Int32>(BarFillColourProperty, value); }
        }

        #endregion

        #region RodHeight

        /// <summary>
        /// RodHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> RodHeightProperty =
          RegisterModelProperty<Single>(c => c.RodHeight);

        /// <summary>
        /// Gets/Sets the default RodHeight
        /// </summary>
        public Single RodHeight
        {
            get { return GetProperty<Single>(RodHeightProperty); }
            set { SetProperty<Single>(RodHeightProperty, value); }
        }

        #endregion

        #region RodWidth

        /// <summary>
        /// RodWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> RodWidthProperty =
          RegisterModelProperty<Single>(c => c.RodWidth);

        /// <summary>
        /// Gets/Sets the default RodWidth
        /// </summary>
        public Single RodWidth
        {
            get { return GetProperty<Single>(RodWidthProperty); }
            set { SetProperty<Single>(RodWidthProperty, value); }
        }

        #endregion

        #region RodDepth

        /// <summary>
        /// RodDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> RodDepthProperty =
          RegisterModelProperty<Single>(c => c.RodDepth);

        /// <summary>
        /// Gets/Sets the default RodDepth
        /// </summary>
        public Single RodDepth
        {
            get { return GetProperty<Single>(RodDepthProperty); }
            set { SetProperty<Single>(RodDepthProperty, value); }
        }

        #endregion

        #region RodFillColour

        /// <summary>
        /// RodFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> RodFillColourProperty =
          RegisterModelProperty<Int32>(c => c.RodFillColour);

        /// <summary>
        /// Gets/Sets the default RodFillColour
        /// </summary>
        public Int32 RodFillColour
        {
            get { return GetProperty<Int32>(RodFillColourProperty); }
            set { SetProperty<Int32>(RodFillColourProperty, value); }
        }

        #endregion

        #region ClipstripHeight

        /// <summary>
        /// ClipstripHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ClipStripHeightProperty =
          RegisterModelProperty<Single>(c => c.ClipStripHeight);

        /// <summary>
        /// Gets/Sets the default ClipstripHeight
        /// </summary>
        public Single ClipStripHeight
        {
            get { return GetProperty<Single>(ClipStripHeightProperty); }
            set { SetProperty<Single>(ClipStripHeightProperty, value); }
        }

        #endregion

        #region ClipstripWidth

        /// <summary>
        /// ClipstripWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ClipStripWidthProperty =
          RegisterModelProperty<Single>(c => c.ClipStripWidth);

        /// <summary>
        /// Gets/Sets the default ClipstripWidth
        /// </summary>
        public Single ClipStripWidth
        {
            get { return GetProperty<Single>(ClipStripWidthProperty); }
            set { SetProperty<Single>(ClipStripWidthProperty, value); }
        }

        #endregion

        #region ClipstripDepth

        /// <summary>
        /// ClipstripDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ClipStripDepthProperty =
          RegisterModelProperty<Single>(c => c.ClipStripDepth);

        /// <summary>
        /// Gets/Sets the default ClipstripDepth
        /// </summary>
        public Single ClipStripDepth
        {
            get { return GetProperty<Single>(ClipStripDepthProperty); }
            set { SetProperty<Single>(ClipStripDepthProperty, value); }
        }

        #endregion

        #region ClipstripFillColour

        /// <summary>
        /// ClipstripFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ClipStripFillColourProperty =
          RegisterModelProperty<Int32>(c => c.ClipStripFillColour);

        /// <summary>
        /// Gets/Sets the default ClipstripFillColour
        /// </summary>
        public Int32 ClipStripFillColour
        {
            get { return GetProperty<Int32>(ClipStripFillColourProperty); }
            set { SetProperty<Int32>(ClipStripFillColourProperty, value); }
        }

        #endregion

        #region PalletHeight

        /// <summary>
        /// PalletHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PalletHeightProperty =
          RegisterModelProperty<Single>(c => c.PalletHeight);

        /// <summary>
        /// Gets/Sets the default PalletHeight
        /// </summary>
        public Single PalletHeight
        {
            get { return GetProperty<Single>(PalletHeightProperty); }
            set { SetProperty<Single>(PalletHeightProperty, value); }
        }

        #endregion

        #region PalletWidth

        /// <summary>
        /// PalletWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PalletWidthProperty =
          RegisterModelProperty<Single>(c => c.PalletWidth);

        /// <summary>
        /// Gets/Sets the default PalletWidth
        /// </summary>
        public Single PalletWidth
        {
            get { return GetProperty<Single>(PalletWidthProperty); }
            set { SetProperty<Single>(PalletWidthProperty, value); }
        }

        #endregion

        #region PalletDepth

        /// <summary>
        /// PalletDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PalletDepthProperty =
          RegisterModelProperty<Single>(c => c.PalletDepth);

        /// <summary>
        /// Gets/Sets the default PalletDepth
        /// </summary>
        public Single PalletDepth
        {
            get { return GetProperty<Single>(PalletDepthProperty); }
            set { SetProperty<Single>(PalletDepthProperty, value); }
        }

        #endregion

        #region PalletFillColour

        /// <summary>
        /// PalletFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PalletFillColourProperty =
          RegisterModelProperty<Int32>(c => c.PalletFillColour);

        /// <summary>
        /// Gets/Sets the default PalletFillColour
        /// </summary>
        public Int32 PalletFillColour
        {
            get { return GetProperty<Int32>(PalletFillColourProperty); }
            set { SetProperty<Int32>(PalletFillColourProperty, value); }
        }

        #endregion

        #region SlotwallHeight
        /// <summary>
        /// SlotwallHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SlotwallHeightProperty =
            RegisterModelProperty<Single>(c => c.SlotwallHeight);
        /// <summary>
        /// Gets or sets the default Slotwall height
        /// </summary>
        public Single SlotwallHeight
        {
            get { return this.GetProperty<Single>(SlotwallHeightProperty); }
            set { this.SetProperty<Single>(SlotwallHeightProperty, value); }
        }
        #endregion

        #region SlotwallWidth
        /// <summary>
        /// SlotwallWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SlotwallWidthProperty =
            RegisterModelProperty<Single>(c => c.SlotwallWidth);
        /// <summary>
        /// Gets or sets the default Slotwall width
        /// </summary>
        public Single SlotwallWidth
        {
            get { return this.GetProperty<Single>(SlotwallWidthProperty); }
            set { this.SetProperty<Single>(SlotwallWidthProperty, value); }
        }
        #endregion

        #region SlotwallDepth
        /// <summary>
        /// SlotwallDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SlotwallDepthProperty =
            RegisterModelProperty<Single>(c => c.SlotwallDepth);
        /// <summary>
        /// Gets or sets the default Slotwall depth
        /// </summary>
        public Single SlotwallDepth
        {
            get { return this.GetProperty<Single>(SlotwallDepthProperty); }
            set { this.SetProperty<Single>(SlotwallDepthProperty, value); }
        }
        #endregion

        #region SlotwallFillColour
        /// <summary>
        /// SlotwallFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> SlotwallFillColourProperty =
            RegisterModelProperty<Int32>(c => c.SlotwallFillColour);
        /// <summary>
        /// Gets or sets the default Slotwall fill colour
        /// </summary>
        public Int32 SlotwallFillColour
        {
            get { return this.GetProperty<Int32>(SlotwallFillColourProperty); }
            set { this.SetProperty<Int32>(SlotwallFillColourProperty, value); }
        }
        #endregion

        #region TextBoxFont

        /// <summary>
        /// TextBoxFont property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> TextBoxFontProperty =
            RegisterModelProperty<String>(o => o.TextBoxFont);

        /// <summary>
        /// Gets/Sets the default textbox font
        /// </summary>
        public String TextBoxFont
        {
            get { return this.GetProperty<String>(TextBoxFontProperty); }
            set { this.SetProperty<String>(TextBoxFontProperty, value); }
        }

        #endregion

        #region TextBoxFontSize

        /// <summary>
        /// TextBoxFontSize property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Single> TextBoxFontSizeProperty =
            RegisterModelProperty<Single>(o => o.TextBoxFontSize);

        /// <summary>
        /// Returns the default textbox font size
        /// </summary>
        public Single TextBoxFontSize
        {
            get { return this.GetProperty<Single>(TextBoxFontSizeProperty); }
            set { this.SetProperty<Single>(TextBoxFontSizeProperty, value); }
        }

        #endregion

        #region TextBoxCanReduceToFit

        /// <summary>
        /// TextBoxCanReduceToFit property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> TextBoxCanReduceToFitProperty =
            RegisterModelProperty<Boolean>(o => o.TextBoxCanReduceToFit);


        /// <summary>
        /// Returns true of the textbox setting for reduce to fit is on.
        /// </summary>
        public Boolean TextBoxCanReduceToFit
        {
            get { return this.GetProperty<Boolean>(TextBoxCanReduceToFitProperty); }
            set { this.SetProperty<Boolean>(TextBoxCanReduceToFitProperty, value); }
        }

        #endregion

        #region TextBoxFontColour

        /// <summary>
        /// TextBoxFontColour property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> TextBoxFontColourProperty =
            RegisterModelProperty<Int32>(o => o.TextBoxFontColour);

        /// <summary>
        /// The default textbox font colour.
        /// </summary>
        public Int32 TextBoxFontColour
        {
            get { return this.GetProperty<Int32>(TextBoxFontColourProperty); }
            set { this.SetProperty<Int32>(TextBoxFontColourProperty, value); }
        }

        #endregion

        #region TextBoxBackgroundColour

        /// <summary>
        /// TextBoxBackgroundColour property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> TextBoxBackgroundColourProperty =
            RegisterModelProperty<Int32>(o => o.TextBoxBackgroundColour);

        /// <summary>
        /// The default textbox background colour
        /// </summary>
        public Int32 TextBoxBackgroundColour
        {
            get { return this.GetProperty<Int32>(TextBoxBackgroundColourProperty); }
            set { this.SetProperty<Int32>(TextBoxBackgroundColourProperty, value); }
        }


        #endregion

        #region TextBoxBorderColour

        /// <summary>
        /// TextBoxBorderColour property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> TextBoxBorderColourProperty =
            RegisterModelProperty<Int32>(o => o.TextBoxBorderColour);


        /// <summary>
        /// The default textbox border colour
        /// </summary>
        public Int32 TextBoxBorderColour
        {
            get { return this.GetProperty<Int32>(TextBoxBorderColourProperty); }
            set { this.SetProperty<Int32>(TextBoxBorderColourProperty, value); }
        }

        #endregion

        #region TextBoxBorderThickness

        /// <summary>
        /// TextBoxBorderThickness property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Single> TextBoxBorderThicknessProperty =
            RegisterModelProperty<Single>(o => o.TextBoxBorderThickness);

        /// <summary>
        /// The default textbox border thickness
        /// </summary>
        public Single TextBoxBorderThickness
        {
            get { return this.GetProperty<Single>(TextBoxBorderThicknessProperty); }
            set { this.SetProperty<Single>(TextBoxBorderThicknessProperty, value); }
        }

        #endregion

        #endregion

        #region Sync information

        #region LastSuccessfulSyncDateProperty

        /// <summary>
        /// LastSuccessfulSyncDate property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> LastSuccessfulSyncDateProperty =
          RegisterModelProperty<DateTime?>(c => c.LastSuccessfulSyncDate);

        /// <summary>
        /// Gets/Sets the Last Successful Sync Date
        /// </summary>
        public DateTime? LastSuccessfulSyncDate
        {
            get { return GetProperty<DateTime?>(LastSuccessfulSyncDateProperty); }
            set { SetProperty<DateTime?>(LastSuccessfulSyncDateProperty, value); }
        }

        #endregion

        #region LastFailedSyncDateProperty

        /// <summary>
        /// Last Sync Failed Date property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> LastFailedSyncDateProperty =
          RegisterModelProperty<DateTime?>(c => c.LastFailedSyncDate);

        /// <summary>
        /// Gets/Sets the Last Sync Failed Date
        /// </summary>
        public DateTime? LastFailedSyncDate
        {
            get { return GetProperty<DateTime?>(LastFailedSyncDateProperty); }
            set { SetProperty<DateTime?>(LastFailedSyncDateProperty, value); }
        }

        #endregion
        #endregion

        #region RealImageProviderSettings Properties

        #region ProductImageSource

        /// <summary>
        ///		Metadata for the <see cref="ProductImageSource"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<RealImageProviderType> ProductImageSourceProperty =
            RegisterModelProperty<RealImageProviderType>(o => o.ProductImageSource);

        /// <summary>
        ///		Gets or sets the currently set image source type.
        /// </summary>
        public RealImageProviderType ProductImageSource
        {
            get { return this.GetProperty<RealImageProviderType>(ProductImageSourceProperty); }
            set { this.SetProperty<RealImageProviderType>(ProductImageSourceProperty, value); }
        }

        #endregion

        #region ProductImageLocation

        /// <summary>
        ///		Metadata for the <see cref="ProductImageLocation"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> ProductImageLocationProperty =
            RegisterModelProperty<String>(o => o.ProductImageLocation);

        /// <summary>
        ///		Gets or sets the path to the root folder containing the product image files
        /// </summary>
        public String ProductImageLocation
        {
            get { return this.GetProperty<String>(ProductImageLocationProperty); }
            set { this.SetProperty<String>(ProductImageLocationProperty, value); }
        }

        #endregion

        #region ProductImageAttribute

        /// <summary>
        ///		Metadata for the <see cref="ProductImageAttribute"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> ProductImageAttributeProperty =
            RegisterModelProperty<String>(o => o.ProductImageAttribute);

        /// <summary>
        ///		Gets or sets the poduct attribute to use when finding image files.
        /// </summary>
        public String ProductImageAttribute
        {
            get { return this.GetProperty<String>(ProductImageAttributeProperty); }
            set { this.SetProperty<String>(ProductImageAttributeProperty, value); }
        }

        #endregion

        #region ProductImageIgnoreLeadingZeros

        /// <summary>
        ///		Metadata for the <see cref="ProductImageIgnoreLeadingZeros"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ProductImageIgnoreLeadingZerosProperty =
            RegisterModelProperty<Boolean>(o => o.ProductImageIgnoreLeadingZeros);

        /// <summary>
        ///		Gets or sets whether leading zeros in the file name should be ignored or not.
        /// </summary>
        public Boolean ProductImageIgnoreLeadingZeros
        {
            get { return this.GetProperty<Boolean>(ProductImageIgnoreLeadingZerosProperty); }
            set { this.SetProperty<Boolean>(ProductImageIgnoreLeadingZerosProperty, value); }
        }

        #endregion

        #region ProductImageMinFieldLength

        /// <summary>
        ///		Metadata for the <see cref="ProductImageMinFieldLength"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Byte> ProductImageMinFieldLengthProperty =
            RegisterModelProperty<Byte>(o => o.ProductImageMinFieldLength);

        /// <summary>
        ///		Gets or sets the minimum length for image file names. 
        /// </summary>
        public Byte ProductImageMinFieldLength
        {
            get { return this.GetProperty<Byte>(ProductImageMinFieldLengthProperty); }
            set { this.SetProperty<Byte>(ProductImageMinFieldLengthProperty, value); }
        }

        #endregion

        #region ProductImageFacingPostfixFront

        /// <summary>
        ///		Metadata for the <see cref="ProductImageFacingPostfixFront"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> ProductImageFacingPostfixFrontProperty =
            RegisterModelProperty<String>(o => o.ProductImageFacingPostfixFront);

        /// <summary>
        ///		Gets or sets the extension for Front <see cref="ProductImageImageFacingType"/> images.
        /// </summary>
        public String ProductImageFacingPostfixFront
        {
            get { return this.GetProperty<String>(ProductImageFacingPostfixFrontProperty); }
            set { this.SetProperty<String>(ProductImageFacingPostfixFrontProperty, value); }
        }

        #endregion

        #region ProductImageFacingPostfixLeft

        /// <summary>
        ///		Metadata for the <see cref="ProductImageFacingPostfixLeft"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> ProductImageFacingPostfixLeftProperty =
            RegisterModelProperty<String>(o => o.ProductImageFacingPostfixLeft);

        /// <summary>
        ///		Gets or sets the extension for Left <see cref="ProductImageImageFacingType"/> images.
        /// </summary>
        public String ProductImageFacingPostfixLeft
        {
            get { return this.GetProperty<String>(ProductImageFacingPostfixLeftProperty); }
            set { this.SetProperty<String>(ProductImageFacingPostfixLeftProperty, value); }
        }

        #endregion

        #region ProductImageFacingPostfixTop

        /// <summary>
        ///		Metadata for the <see cref="ProductImageFacingPostfixTop"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> ProductImageFacingPostfixTopProperty =
            RegisterModelProperty<String>(o => o.ProductImageFacingPostfixTop);

        /// <summary>
        ///		Gets or sets the extension for Top <see cref="ProductImageImageFacingType"/> images.
        /// </summary>
        public String ProductImageFacingPostfixTop
        {
            get { return this.GetProperty<String>(ProductImageFacingPostfixTopProperty); }
            set { this.SetProperty<String>(ProductImageFacingPostfixTopProperty, value); }
        }

        #endregion

        #region ProductImageFacingPostfixBack

        /// <summary>
        ///		Metadata for the <see cref="ProductImageFacingPostfixBack"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> ProductImageFacingPostfixBackProperty =
            RegisterModelProperty<String>(o => o.ProductImageFacingPostfixBack);

        /// <summary>
        ///		Gets or sets the extension for Back <see cref="ProductImageImageFacingType"/> images.
        /// </summary>
        public String ProductImageFacingPostfixBack
        {
            get { return this.GetProperty<String>(ProductImageFacingPostfixBackProperty); }
            set { this.SetProperty<String>(ProductImageFacingPostfixBackProperty, value); }
        }

        #endregion

        #region ProductImageFacingPostfixRight

        /// <summary>
        ///		Metadata for the <see cref="ProductImageFacingPostfixRight"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> ProductImageFacingPostfixRightProperty =
            RegisterModelProperty<String>(o => o.ProductImageFacingPostfixRight);

        /// <summary>
        ///		Gets or sets the extension for Right <see cref="ProductImageImageFacingType"/> images.
        /// </summary>
        public String ProductImageFacingPostfixRight
        {
            get { return this.GetProperty<String>(ProductImageFacingPostfixRightProperty); }
            set { this.SetProperty<String>(ProductImageFacingPostfixRightProperty, value); }
        }

        #endregion

        #region ProductImageFacingPostfixBottom

        /// <summary>
        ///		Metadata for the <see cref="ProductImageFacingPostfixBottom"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> ProductImageFacingPostfixBottomProperty =
            RegisterModelProperty<String>(o => o.ProductImageFacingPostfixBottom);

        /// <summary>
        ///		Gets or sets the extension for Bottom <see cref="ProductImageImageFacingType"/> images.
        /// </summary>
        public String ProductImageFacingPostfixBottom
        {
            get { return this.GetProperty<String>(ProductImageFacingPostfixBottomProperty); }
            set { this.SetProperty<String>(ProductImageFacingPostfixBottomProperty, value); }
        }

        #endregion

        #region ProductImageCompression

        /// <summary>
        ///		Metadata for the <see cref="ProductImageCompression"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<RealImageCompressionType> ProductImageCompressionProperty =
            RegisterModelProperty<RealImageCompressionType>(o => o.ProductImageCompression);

        /// <summary>
        ///		Gets or sets the compression ratio to achieve when getting images from file.
        /// </summary>
        public RealImageCompressionType ProductImageCompression
        {
            get { return this.GetProperty<RealImageCompressionType>(ProductImageCompressionProperty); }
            set { this.SetProperty<RealImageCompressionType>(ProductImageCompressionProperty, value); }
        }

        #endregion

        #region ProductImageColourDepth

        /// <summary>
        ///		Metadata for the <see cref="ProductImageColourDepth"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<RealImageColourDepthType> ProductImageColourDepthProperty =
            RegisterModelProperty<RealImageColourDepthType>(o => o.ProductImageColourDepth);

        /// <summary>
        ///		Gets or sets the depth of colour to use for images retrieved from files.
        /// </summary>
        public RealImageColourDepthType ProductImageColourDepth
        {
            get { return this.GetProperty<RealImageColourDepthType>(ProductImageColourDepthProperty); }
            set { this.SetProperty<RealImageColourDepthType>(ProductImageColourDepthProperty, value); }
        }

        #endregion

        #region ProductImageTransparency

        /// <summary>
        ///		Metadata for the <see cref="ProductImageTransparency"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ProductImageTransparencyProperty =
            RegisterModelProperty<Boolean>(o => o.ProductImageTransparency);

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageTransparency"/> property.
        /// </summary>
        public Boolean ProductImageTransparency
        {
            get { return this.GetProperty<Boolean>(ProductImageTransparencyProperty); }
            set { this.SetProperty<Boolean>(ProductImageTransparencyProperty, value); }
        }

        #endregion

        #endregion

        #region FixtureLabel

        /// <summary>
        /// FixtureLabel property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> FixtureLabelProperty =
           RegisterModelProperty<String>(c => c.FixtureLabel, Message.SystemSettings_FixtureLabel);

        /// <summary>
        /// Gets/Sets the name of the default fixture label
        /// </summary>
        public String FixtureLabel
        {
            get { return GetProperty<String>(FixtureLabelProperty); }
            set { SetProperty<String>(FixtureLabelProperty, value); }
        }

        #endregion

        #region ProductLabel

        /// <summary>
        /// ProductLabel property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ProductLabelProperty =
          RegisterModelProperty<String>(c => c.ProductLabel, Message.SystemSettings_ProductLabel);

        /// <summary>
        /// Gets/Sets the name of the default product label
        /// </summary>
        public String ProductLabel
        {
            get { return GetProperty<String>(ProductLabelProperty); }
            set { SetProperty<String>(ProductLabelProperty, value); }
        }

        #endregion

        #region Highlight

        /// <summary>
        /// Highlight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> HighlightProperty =
          RegisterModelProperty<String>(c => c.Highlight, Message.SystemSettings_Highlight);

        /// <summary>
        /// Gets/Sets the name of the default highlight
        /// </summary>
        public String Highlight
        {
            get { return GetProperty<String>(HighlightProperty); }
            set { SetProperty<String>(HighlightProperty, value); }
        }

        #endregion

        #region Positions

        /// <summary>
        /// Positions property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PositionsProperty =
            RegisterModelProperty<Boolean>(c => c.Positions, Message.SystemSettings_Positions);

        /// <summary>
        /// Gets/Sets the initial value for the Positions setting
        /// </summary>
        public Boolean Positions
        {
            get { return GetProperty<Boolean>(PositionsProperty); }
            set { SetProperty<Boolean>(PositionsProperty, value); }
        }

        #endregion

        #region PositionUnits

        /// <summary>
        /// PositionUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PositionUnitsProperty =
          RegisterModelProperty<Boolean>(c => c.PositionUnits, Message.SystemSettings_PositionUnits);

        /// <summary>
        /// Gets/Sets the initial value for the PositionUnits setting
        /// </summary>
        public Boolean PositionUnits
        {
            get { return GetProperty<Boolean>(PositionUnitsProperty); }
            set { SetProperty<Boolean>(PositionUnitsProperty, value); }
        }

        #endregion

        #region ProductImages

        /// <summary>
        /// ProductImages property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> ProductImagesProperty =
          RegisterModelProperty<Boolean>(c => c.ProductImages, Message.SystemSettings_ProductImages);

        /// <summary>
        /// Gets/Sets the initial value for the ProductImages setting
        /// </summary>
        public Boolean ProductImages
        {
            get { return GetProperty<Boolean>(ProductImagesProperty); }
            set { SetProperty<Boolean>(ProductImagesProperty, value); }
        }

        #endregion

        #region ProductShapes

        /// <summary>
        /// ProductShapes property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> ProductShapesProperty =
          RegisterModelProperty<Boolean>(c => c.ProductShapes, Message.SystemSettings_ProductShapes);

        /// <summary>
        /// Gets/Sets the initial value for the ProductShapes setting
        /// </summary>
        public Boolean ProductShapes
        {
            get { return GetProperty<Boolean>(ProductShapesProperty); }
            set { SetProperty<Boolean>(ProductShapesProperty, value); }
        }

        #endregion

        #region ProductFillPatterns

        /// <summary>
        /// ProductFillPatterns property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> ProductFillPatternsProperty =
          RegisterModelProperty<Boolean>(c => c.ProductFillPatterns, Message.SystemSettings_ProductFillPatterns);

        /// <summary>
        /// Gets/Sets the initial value for the ProductFillPatterns setting
        /// </summary>
        public Boolean ProductFillPatterns
        {
            get { return GetProperty<Boolean>(ProductFillPatternsProperty); }
            set { SetProperty<Boolean>(ProductFillPatternsProperty, value); }
        }

        #endregion

        #region FixtureImages

        /// <summary>
        /// FixtureImages property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> FixtureImagesProperty =
          RegisterModelProperty<Boolean>(c => c.FixtureImages, Message.SystemSettings_FixtureImages);

        /// <summary>
        /// Gets/Sets the initial value for the FixtureImages setting
        /// </summary>
        public Boolean FixtureImages
        {
            get { return GetProperty<Boolean>(FixtureImagesProperty); }
            set { SetProperty<Boolean>(FixtureImagesProperty, value); }
        }

        #endregion

        #region FixtureFillPatterns

        /// <summary>
        /// FixtureFillPatterns property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> FixtureFillPatternsProperty =
          RegisterModelProperty<Boolean>(c => c.FixtureFillPatterns, Message.SystemSettings_FixtureFillPatterns);

        /// <summary>
        /// Gets/Sets the initial value for the FixtureFillPatterns setting
        /// </summary>
        public Boolean FixtureFillPatterns
        {
            get { return GetProperty<Boolean>(FixtureFillPatternsProperty); }
            set { SetProperty<Boolean>(FixtureFillPatternsProperty, value); }
        }

        #endregion

        #region ChestWalls

        /// <summary>
        /// ChestWalls property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> ChestWallsProperty =
          RegisterModelProperty<Boolean>(c => c.ChestWalls, Message.SystemSettings_ChestWalls);

        /// <summary>
        /// Gets/Sets the initial value for the ChestWalls setting
        /// </summary>
        public Boolean ChestWalls
        {
            get { return GetProperty<Boolean>(ChestWallsProperty); }
            set { SetProperty<Boolean>(ChestWallsProperty, value); }
        }

        #endregion

        #region RotateTopDownComponents

        /// <summary>
        /// ChestsTopDown property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> RotateTopDownComponentsProperty =
          RegisterModelProperty<Boolean>(c => c.RotateTopDownComponents, Message.SystemSettings_RotateTopDownComponents);

        /// <summary>
        /// Gets/Sets the initial value for the RotateTopDownComponents setting
        /// </summary>
        public Boolean RotateTopDownComponents
        {
            get { return GetProperty<Boolean>(RotateTopDownComponentsProperty); }
            set { SetProperty<Boolean>(RotateTopDownComponentsProperty, value); }
        }

        #endregion

        #region ShelfRisers

        /// <summary>
        /// ShelfRisers property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> ShelfRisersProperty =
          RegisterModelProperty<Boolean>(c => c.ShelfRisers, Message.SystemSettings_ShelfRisers);

        /// <summary>
        /// Gets/Sets the initial value for the ShelfRisers setting
        /// </summary>
        public Boolean ShelfRisers
        {
            get { return GetProperty<Boolean>(ShelfRisersProperty); }
            set { SetProperty<Boolean>(ShelfRisersProperty, value); }
        }

        #endregion

        #region Notches

        /// <summary>
        /// Notches property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> NotchesProperty =
          RegisterModelProperty<Boolean>(c => c.Notches, Message.SystemSettings_Notches);

        /// <summary>
        /// Gets/Sets the initial value for the Notches setting
        /// </summary>
        public Boolean Notches
        {
            get { return GetProperty<Boolean>(NotchesProperty); }
            set { SetProperty<Boolean>(NotchesProperty, value); }
        }

        #endregion

        #region PegHoles

        /// <summary>
        /// Pegholes property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PegHolesProperty =
          RegisterModelProperty<Boolean>(c => c.PegHoles, Message.SystemSettings_PegHoles);

        /// <summary>
        /// Gets/Sets the initial value for the Pegholes setting
        /// </summary>
        public Boolean PegHoles
        {
            get { return GetProperty<Boolean>(PegHolesProperty); }
            set { SetProperty<Boolean>(PegHolesProperty, value); }
        }

        #endregion

        #region Pegs

        /// <summary>
        /// Pegs property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PegsProperty =
          RegisterModelProperty<Boolean>(c => c.Pegs, Message.SystemSettings_Pegs);

        /// <summary>
        /// Gets/Sets the initial value for the Pegs setting
        /// </summary>
        public Boolean Pegs
        {
            get { return GetProperty<Boolean>(PegsProperty); }
            set { SetProperty<Boolean>(PegsProperty, value); }
        }

        #endregion

        #region DividerLines

        /// <summary>
        /// DividerLines property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> DividerLinesProperty =
          RegisterModelProperty<Boolean>(c => c.DividerLines, Message.SystemSettings_DividerLines);

        /// <summary>
        /// Gets/Sets the initial value for the DividerLines setting
        /// </summary>
        public Boolean DividerLines
        {
            get { return GetProperty<Boolean>(DividerLinesProperty); }
            set { SetProperty<Boolean>(DividerLinesProperty, value); }
        }

        #endregion

        #region Dividers

        /// <summary>
        /// Dividers property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> DividersProperty =
          RegisterModelProperty<Boolean>(c => c.Dividers, Message.SystemSettings_Dividers);

        /// <summary>
        /// Gets/Sets the initial value for the Dividers setting
        /// </summary>
        public Boolean Dividers
        {
            get { return GetProperty<Boolean>(DividersProperty); }
            set { SetProperty<Boolean>(DividersProperty, value); }
        }

        #endregion
        
        #region Annotations

        /// <summary>
        /// Annotations property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> AnnotationsProperty =
          RegisterModelProperty<Boolean>(c => c.Annotations, Message.SystemSettings_Annotations);

        /// <summary>
        /// Gets/Sets the initial value for the Annotations setting
        /// </summary>
        public Boolean Annotations
        {
            get { return GetProperty<Boolean>(AnnotationsProperty); }
            set { SetProperty<Boolean>(AnnotationsProperty, value); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(SystemSettings), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(SystemSettings), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(SystemSettings), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(SystemSettings), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.CheckRules();
        }
        #endregion

        #region Factory Methods
        public static SystemSettings NewSystemSettings()
        {
            SystemSettings item = new SystemSettings();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new Object of this type
        /// </summary>
        private void Create()
        {
            LoadDefaults();
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #region Load Defaults

        /// <summary>
        /// In the normal course of execution, we always want to use the computer's locale settings to determine
        /// what defaults should be used.  However, for NUnit testing we need to be able to change this value on
        /// the fly in order to be able to test both paths through the code.  So, please don't reset the value of
        /// this field for any other use than unit testing.  If it turns out we need this kind of functionality for
        /// real, please use a CSLA property so it becomes web service-safe.
        /// </summary>
        internal static Boolean IsCurrentRegionMetric = RegionInfo.CurrentRegion.IsMetric; // ISO-13040

        /// <summary>
        /// Loads the defaults for this config Object
        /// </summary>
        private void LoadDefaults()
        {
            #region UOM

            if (IsCurrentRegionMetric)
            {
                this.LoadProperty<UnitOfMeasureLengthType>(LengthUnitOfMeasureProperty, UnitOfMeasureLengthType.Centimeters);
                this.LoadProperty<UnitOfMeasureAreaType>(AreaUnitsOfMeasureProperty, UnitOfMeasureAreaType.SquareCentimeter);
                this.LoadProperty<UnitOfMeasureVolumeType>(VolumeUnitsOfMeasureProperty, UnitOfMeasureVolumeType.Litres);
                this.LoadProperty<UnitOfMeasureWeightType>(WeightUnitsOfMeasureProperty, UnitOfMeasureWeightType.Kilograms);
                this.LoadProperty<UnitOfMeasureCurrencyType>(CurrencyUnitsOfMesasureProperty, UnitOfMeasureCurrencyType.GBP);
            }
            else
            {
                this.LoadProperty<UnitOfMeasureLengthType>(LengthUnitOfMeasureProperty, UnitOfMeasureLengthType.Inches);
                this.LoadProperty<UnitOfMeasureAreaType>(AreaUnitsOfMeasureProperty, UnitOfMeasureAreaType.SquareInches);
                this.LoadProperty<UnitOfMeasureVolumeType>(VolumeUnitsOfMeasureProperty, UnitOfMeasureVolumeType.CubicFeet);
                this.LoadProperty<UnitOfMeasureWeightType>(WeightUnitsOfMeasureProperty, UnitOfMeasureWeightType.Pounds);
                this.LoadProperty<UnitOfMeasureCurrencyType>(CurrencyUnitsOfMesasureProperty, UnitOfMeasureCurrencyType.USD);
            }

            this.LoadProperty<UnitOfMeasureAngleType>(AngleUnitsOfMeasureProperty, UnitOfMeasureAngleType.Degrees);
            

            #endregion

            #region Planogram default settings
            this.LoadProperty<ProductPlacementXType>(ProductPlacementXProperty, ProductPlacementXType.Manual);
            this.LoadProperty<ProductPlacementYType>(ProductPlacementYProperty, ProductPlacementYType.FillHigh);
            this.LoadProperty<ProductPlacementZType>(ProductPlacementZProperty, ProductPlacementZType.FillDeep);
            #endregion

            #region Planogram Part Sizes

            PlanogramSettings planogramSettings = PlanogramSettings.NewPlanogramSettings();

            if (IsCurrentRegionMetric)
            {
                LoadProperty<Single>(ProductHeightProperty, 10F);
                LoadProperty<Single>(ProductWidthProperty, 10F);
                LoadProperty<Single>(ProductDepthProperty, 10F);
            }
            else
            {
                LoadProperty<Single>(ProductHeightProperty, 6F);
                LoadProperty<Single>(ProductWidthProperty, 6F);
                LoadProperty<Single>(ProductDepthProperty, 6F);
            }

            LoadProperty<Single>(FixtureHeightProperty, planogramSettings.FixtureHeight);
            LoadProperty<Single>(FixtureWidthProperty, planogramSettings.FixtureWidth);
            LoadProperty<Single>(FixtureDepthProperty, planogramSettings.FixtureDepth);

            LoadProperty<Boolean>(FixtureHasBackboardProperty, true);
            LoadProperty<Single>(BackboardDepthProperty, planogramSettings.BackboardDepth);

            LoadProperty<Boolean>(FixtureHasBaseProperty, true);
            LoadProperty<Single>(BaseHeightProperty, planogramSettings.BaseHeight);

            LoadProperty<Single>(ShelfHeightProperty, planogramSettings.ShelfHeight);
            LoadProperty<Single>(ShelfWidthProperty, planogramSettings.ShelfWidth);
            LoadProperty<Single>(ShelfDepthProperty, planogramSettings.ShelfDepth);
            LoadProperty<Int32>(ShelfFillColourProperty, planogramSettings.ShelfFillColour);

            LoadProperty<Single>(PegboardHeightProperty, planogramSettings.PegboardHeight);
            LoadProperty<Single>(PegboardWidthProperty, planogramSettings.PegboardWidth);
            LoadProperty<Single>(PegboardDepthProperty, planogramSettings.PegboardDepth);
            LoadProperty<Int32>(PegboardFillColourProperty, planogramSettings.PegboardFillColour);

            LoadProperty<Single>(ChestHeightProperty, planogramSettings.ChestHeight);
            LoadProperty<Single>(ChestWidthProperty, planogramSettings.ChestWidth);
            LoadProperty<Single>(ChestDepthProperty, planogramSettings.ChestDepth);
            LoadProperty<Int32>(ChestFillColourProperty, planogramSettings.ChestFillColour);

            LoadProperty<Single>(BarHeightProperty, planogramSettings.BarHeight);
            LoadProperty<Single>(BarWidthProperty, planogramSettings.BarWidth);
            LoadProperty<Single>(BarDepthProperty, planogramSettings.BarDepth);
            LoadProperty<Int32>(BarFillColourProperty, planogramSettings.BarFillColour);

            LoadProperty<Single>(RodHeightProperty, planogramSettings.RodHeight);
            LoadProperty<Single>(RodWidthProperty, planogramSettings.RodWidth);
            LoadProperty<Single>(RodDepthProperty, planogramSettings.RodDepth);
            LoadProperty<Int32>(RodFillColourProperty, planogramSettings.RodFillColour);

            LoadProperty<Single>(ClipStripHeightProperty, planogramSettings.ClipStripHeight);
            LoadProperty<Single>(ClipStripWidthProperty, planogramSettings.ClipStripWidth);
            LoadProperty<Single>(ClipStripDepthProperty, planogramSettings.ClipStripDepth);
            LoadProperty<Int32>(ClipStripFillColourProperty, planogramSettings.ClipStripFillColour);

            LoadProperty<Single>(PalletHeightProperty, planogramSettings.PalletHeight);
            LoadProperty<Single>(PalletWidthProperty, planogramSettings.PalletWidth);
            LoadProperty<Single>(PalletDepthProperty, planogramSettings.PalletDepth);
            LoadProperty<Int32>(PalletFillColourProperty, planogramSettings.PalletFillColour);

            LoadProperty<Single>(SlotwallHeightProperty, planogramSettings.SlotwallHeight);
            LoadProperty<Single>(SlotwallWidthProperty, planogramSettings.SlotwallWidth);
            LoadProperty<Single>(SlotwallDepthProperty, planogramSettings.SlotwallDepth);
            LoadProperty<Int32>(SlotwallFillColourProperty, planogramSettings.SlotwallFillColour);

            LoadProperty<String>(TextBoxFontProperty, planogramSettings.TextBoxFont);
            LoadProperty<Single>(TextBoxFontSizeProperty, planogramSettings.TextBoxFontSize);
            LoadProperty<Boolean>(TextBoxCanReduceToFitProperty, planogramSettings.TextBoxCanReduceToFit);
            LoadProperty<Int32>(TextBoxFontColourProperty, planogramSettings.TextBoxFontColour);
            LoadProperty<Int32>(TextBoxBorderColourProperty, planogramSettings.TextBoxBorderColour);
            LoadProperty<Int32>(TextBoxBackgroundColourProperty, planogramSettings.TextBoxBackgroundColour);
            LoadProperty<Single>(TextBoxBorderThicknessProperty, planogramSettings.TextBoxBorderThickness);
            #endregion

            #region Sync Settings
            this.LoadProperty<DateTime?>(LastSuccessfulSyncDateProperty, null);
            this.LoadProperty<DateTime?>(LastFailedSyncDateProperty, null);
            #endregion

            #region Real Image Provider Settings
            String defaultDocsFolderPath = GetDefaultDocsFolderPath();

            LoadProperty(ProductImageSourceProperty, RealImageProviderType.Folder);
            LoadProperty(ProductImageLocationProperty, Path.Combine(defaultDocsFolderPath, "Images"));
            LoadProperty(ProductImageAttributeProperty,
                PlanogramProduct.EnumerateDisplayableFieldInfos(/*incMetadata*/false, /*inCustom*/true, /*incPerformance*/false)
                 .First(o => o.PropertyName == PlanogramProduct.GtinProperty.Name).FieldPlaceholder);
            LoadProperty(ProductImageMinFieldLengthProperty, (Byte)10);
            LoadProperty(ProductImageFacingPostfixFrontProperty, ((Int32)PlanogramImageFacingType.Front).ToString());
            LoadProperty(ProductImageFacingPostfixLeftProperty, ((Int32)PlanogramImageFacingType.Left).ToString());
            LoadProperty(ProductImageFacingPostfixTopProperty, ((Int32)PlanogramImageFacingType.Top).ToString());
            LoadProperty(ProductImageFacingPostfixBackProperty, ((Int32)PlanogramImageFacingType.Back).ToString());
            LoadProperty(ProductImageFacingPostfixRightProperty, ((Int32)PlanogramImageFacingType.Right).ToString());
            LoadProperty(ProductImageFacingPostfixBottomProperty, ((Int32)PlanogramImageFacingType.Bottom).ToString());
            LoadProperty(ProductImageCompressionProperty, RealImageCompressionType.None);
            LoadProperty(ProductImageColourDepthProperty, RealImageColourDepthType.Bit8);

            #endregion

            #region Planogram Image Settings

            LoadProperty<String>(ProductLabelProperty, String.Empty);
            LoadProperty<String>(FixtureLabelProperty, String.Empty);
            LoadProperty<String>(HighlightProperty, String.Empty);

            LoadProperty<Boolean>(PositionsProperty, true);
            LoadProperty<Boolean>(PositionUnitsProperty, true);
            LoadProperty<Boolean>(ProductImagesProperty, false);
            LoadProperty<Boolean>(ProductShapesProperty, false);
            LoadProperty<Boolean>(ProductFillPatternsProperty, false);
            LoadProperty<Boolean>(FixtureImagesProperty, false);
            LoadProperty<Boolean>(FixtureFillPatternsProperty, false);
            LoadProperty<Boolean>(ChestWallsProperty, false);
            LoadProperty<Boolean>(RotateTopDownComponentsProperty, false);
            LoadProperty<Boolean>(ShelfRisersProperty, true);
            LoadProperty<Boolean>(NotchesProperty, true);
            LoadProperty<Boolean>(PegHolesProperty, true);
            LoadProperty<Boolean>(PegsProperty, true);
            LoadProperty<Boolean>(DividerLinesProperty, false);
            LoadProperty<Boolean>(DividersProperty, true);
            LoadProperty<Boolean>(AnnotationsProperty, true);

            #endregion
        }

        #endregion

        #region Static Helper Methods

        /// <summary>
        ///     Determines the default documents folder based on the type of install.
        /// </summary>
        /// <returns>A <see cref="String"/> with the path to the default documents folder.</returns>
        private static String GetDefaultDocsFolderPath()
        {
            //  The default location of documents is defined by the type of install chosen.
            String defaultDocsFolderPath =
                Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments),
                    Constants.MyDocsAppFolderName);

            if (!Directory.Exists(defaultDocsFolderPath))
            {
                //  Set the user's documents as the common location failed.
                defaultDocsFolderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                    Constants.MyDocsAppFolderName);
            }

            return defaultDocsFolderPath;
        }

        #endregion

        #endregion
    }
}
