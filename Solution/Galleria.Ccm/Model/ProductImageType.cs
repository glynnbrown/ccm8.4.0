﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;
using System.Windows.Media.Imaging;

namespace Galleria.Ccm.Model
{
    public enum ProductImageMatchField
    {
        GTIN = 1,
        CorporateCode = 2,
    }

    public static class ProductImageMatchFieldHelper
    {
        public static Dictionary<ProductImageMatchField, String> FriendlyNames = new Dictionary<ProductImageMatchField, String>() 
        { 
            { ProductImageMatchField.GTIN, Message.Product_Gtin }, 
            { ProductImageMatchField.CorporateCode, Message.Product_CorporateCode }
        };
    }

    public enum ProductImageType
    {
        None = 0,
        Display = 1,
        PointOfPurchase = 2,
        Unit = 3,
        Tray = 4
    }

    public enum ProductImageFacing
    {
        None = 0,
        Front = 1,
        Left = 2,
        Top = 3,
        Back = 7,
        Right = 8,
        Bottom = 9,
    }

    public struct ProductImageImageFacingType
    {
        public ProductImageType ImageType;
        public ProductImageFacing FacingType;
    }

    public static class ProductImageTypeHelper
    {
        public static readonly Dictionary<ProductImageType, String> ImageTypeFriendlyNames =
            new Dictionary<ProductImageType, String>()
            {
                {ProductImageType.Display, Message.Enum_ProductImageType_Display},
                {ProductImageType.PointOfPurchase, Message.Enum_ProductImageType_PointOfPurchase},
                {ProductImageType.Unit, Message.Enum_ProductImageType_Unit},
                {ProductImageType.Tray, Message.Enum_ProductImageType_Tray}
            };

        public static readonly Dictionary<ProductImageFacing, String> ImageFacingFriendlyNames =
            new Dictionary<ProductImageFacing, String>()
            {
                {ProductImageFacing.Front, Message.Enum_ProductImageImageFacingType_Front},
                {ProductImageFacing.Back, Message.Enum_ProductImageImageFacingType_Back},
                {ProductImageFacing.Left, Message.Enum_ProductImageImageFacingType_Left},
                {ProductImageFacing.Right, Message.Enum_ProductImageImageFacingType_Right},
                {ProductImageFacing.Top, Message.Enum_ProductImageImageFacingType_Top},
                {ProductImageFacing.Bottom, Message.Enum_ProductImageImageFacingType_Bottom},
            };
    }

    public enum PostFixField
    {
        Display = 1,
        POP = 2,
        Unit = 3,
    }

    public static class PostFixFieldHelper
    {
        public static Dictionary<PostFixField, String> FriendlyNames = new Dictionary<PostFixField, String>() 
        {
            { PostFixField.Display, Message.Enum_PostFixField_Display }, 
            { PostFixField.Unit, Message.Enum_PostFixField_Unit },
            { PostFixField.POP, Message.Enum_PostFixField_POP }
        };
    }


    public class ProductImageContainer
    {
        private List<ProductImage> _productImages;

        #region Properties

        public ProductImageFacing FacingType { get; set; }
        public ProductImageType ImageType { get; set; }
        public List<ProductImage> ProductImages
        {
            get { return _productImages; }
            set
            {
                _productImages = value;
                //Set highest quality preview image.
                PreviewImage = _productImages
                        .OrderByDescending(p => p.ProductImageData.SizeInBytes)
                        .FirstOrDefault().ProductImageData.PreviewImage;
            }
        }

        public BitmapSource PreviewImage { get; private set; }

        #endregion
        public ProductImageContainer()
        { }
    }
}
