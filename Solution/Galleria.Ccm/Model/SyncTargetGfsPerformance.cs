﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using Csla.Rules.CommonRules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Rules;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a Gfs Performance Source Item to be synchronised when sync target
    /// </summary>
    [Serializable]
    public partial class SyncTargetGfsPerformance : ModelObject<SyncTargetGfsPerformance>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new SyncTarget Parent
        {
            get { return ((SyncTargetGfsPerformanceList)base.Parent).Parent; }
        }
        #endregion

        #region Properties

        /// <summary>
        /// The id property
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// The sync target id property
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> SyncTargetIdProperty =
            RegisterModelProperty<Int32>(c => c.SyncTargetId);
        public Int32 SyncTargetId
        {
            get { return GetProperty<Int32>(SyncTargetIdProperty); }
        }

        /// <summary>
        /// The row version property
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
            set { SetProperty<RowVersion>(RowVersionProperty, value); }
        }

        /// <summary>
        /// The entity name property
        /// </summary>
        public static readonly ModelPropertyInfo<String> EntityNameProperty =
            RegisterModelProperty<String>(c => c.EntityName);
        public String EntityName
        {
            get { return GetProperty<String>(EntityNameProperty); }
            //set { SetProperty<String>(EntityNameProperty, value); }
        }

        /// <summary>
        /// The source id property
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> SourceIdProperty =
            RegisterModelProperty<Int32>(c => c.SourceId);
        public Int32 SourceId
        {
            get { return GetProperty<Int32>(SourceIdProperty); }
        }

        /// <summary>
        /// The entity name property
        /// </summary>
        public static readonly ModelPropertyInfo<String> SourceNameProperty =
            RegisterModelProperty<String>(c => c.SourceName);
        public String SourceName
        {
            get { return GetProperty<String>(SourceNameProperty); }
            //set { SetProperty<String>(SourceNameProperty, value); }
        }

        /// <summary>
        /// The source type property
        /// </summary>
        public static readonly ModelPropertyInfo<GFSModel.PerformanceSourceType> SourceTypeProperty =
            RegisterModelProperty<GFSModel.PerformanceSourceType>(c => c.SourceType);
        public GFSModel.PerformanceSourceType SourceType
        {
            get { return GetProperty<GFSModel.PerformanceSourceType>(SourceTypeProperty); }
        }

        /// <summary>
        /// The source sync period as number of weeks
        /// </summary>
        public static readonly ModelPropertyInfo<Byte?> SourceSyncPeriodProperty =
            RegisterModelProperty<Byte?>(c => c.SourceSyncPeriod);
        public Byte? SourceSyncPeriod
        {
            get { return GetProperty<Byte?>(SourceSyncPeriodProperty); }
            set { SetProperty<Byte?>(SourceSyncPeriodProperty, value); }
        }

        #endregion

        #region Authorization Rules
        //this object is accessed without authentication and so cannot have rules
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(SyncTargetIdProperty));

            BusinessRules.AddRule(new Required(EntityNameProperty));
            BusinessRules.AddRule(new MaxLength(EntityNameProperty, 255));

            BusinessRules.AddRule(new Required(SourceIdProperty));
            BusinessRules.AddRule(new Required(SourceNameProperty));
            BusinessRules.AddRule(new MaxLength(SourceNameProperty, 255));

            BusinessRules.AddRule(new Required(SourceTypeProperty));

            BusinessRules.AddRule(new NullableMinValue<Byte>(SourceSyncPeriodProperty, 1));
            BusinessRules.AddRule(new NullableMaxValue<Byte>(SourceSyncPeriodProperty, 104)); //equals 2 years worth of data
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new synchronisation target Gfs Performance source
        /// </summary>
        /// <returns>A new synchornisation source</returns>
        public static SyncTargetGfsPerformance NewSyncTargetGfsPerformance(Int32 syncTargetId,
            GFSModel.PerformanceSource gfsPerformanceSource)
        {
            SyncTargetGfsPerformance item = new SyncTargetGfsPerformance();
            item.Create(syncTargetId, gfsPerformanceSource);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new sync target performance
        /// </summary>
        private void Create(Int32 syncTargetId, GFSModel.PerformanceSource gfsPerformanceSource)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(SyncTargetIdProperty, syncTargetId);
            this.LoadProperty<String>(EntityNameProperty, gfsPerformanceSource.EntityName);
            this.LoadProperty<Int32>(SourceIdProperty, gfsPerformanceSource.Id);
            this.LoadProperty<String>(SourceNameProperty, gfsPerformanceSource.Name);
            this.LoadProperty<GFSModel.PerformanceSourceType>(SourceTypeProperty, gfsPerformanceSource.Type);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}