﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-25628 : A.Kuszyk
//	Created (Auto-generated)
// CCM-25445 : L.Ineson
//  Added FetchByLocationGroupId
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public sealed partial class LocationList
    {
        #region Constructor
        public LocationList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of location objects for the given entity
        /// List is a root object
        /// </summary>
        /// <returns>A list of existing items</returns>
        public static LocationList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<LocationList>(new FetchByEntityIdCriteria(entityId));
        }

        /// <summary>
        /// Returns a list of location objects for the given location group
        /// List is a root object
        /// </summary>
        /// <returns>A list of existing items</returns>
        public static LocationList FetchByLocationGroupId(Int32 locationGroupId)
        {
            return DataPortal.Fetch<LocationList>(new FetchByLocationGroupIdCriteria(locationGroupId));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when loading this list from the entity
        /// </summary>
        /// <param name="criteria">Fetch criteria</param>
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                //fetch all locations for the entity
                IEnumerable<LocationDto> locationDtoList;
                using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                {
                    locationDtoList = dal.FetchByEntityId(criteria.EntityId);
                }

                //load the locations
                foreach (LocationDto dto in locationDtoList)
                {
                    this.Add(Location.FetchLocation(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true; 
        }

        /// <summary>
        /// Called when loading this list from the entity
        /// </summary>
        /// <param name="criteria">Fetch criteria</param>
        private void DataPortal_Fetch(FetchByLocationGroupIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                //fetch all locations for the entity
                IEnumerable<LocationDto> locationDtoList;
                using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                {
                    locationDtoList = dal.FetchByLocationGroupId(criteria.LocationGroupId);
                }

                //load the locations
                foreach (LocationDto dto in locationDtoList)
                {
                    this.Add(Location.FetchLocation(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #region Update
        /// <summary>
        /// Called when this list is being updated
        /// (Only when it is a root object)
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                Child_Update(dalContext);
                dalContext.Commit();
            }
        }
        #endregion

        #endregion
    }
}