﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a synchronisation target list
    /// </summary>
    [Serializable]
    public sealed partial class SyncTargetList : ModelList<SyncTargetList, SyncTarget>
    {
        #region Authorization Rules
        //this object is accessed without authentication and so cannot have rules
        #endregion
    }
}
