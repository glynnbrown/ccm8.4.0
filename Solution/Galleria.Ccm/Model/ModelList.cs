﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24801 : L.Hodson
//  Created
#endregion
#region Version History: CCM830
// CCM-32115 : L.Ineson
//  Added child changing event handler.
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Model;
using System.ComponentModel;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a base model list class from
    /// which the other model lists in
    /// this assembly will inherit
    /// </summary>
    [Serializable]
    public abstract partial class ModelList<TList, TItem> : Galleria.Framework.Model.ModelList<TList, TItem>, IModelList, INotifyChildChanging
        where TList : ModelList<TList, TItem>
        where TItem : ModelObject<TItem>
    {
        #region Properties

        #region DalFactoryName

        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        String IModelList.DalFactoryName
        {
            get { return this.DalFactoryName; }
        }

        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        protected virtual String DalFactoryName
        {
            get
            {
                // attempt to get the name from a parent model object
                IModelObject modelObject = this.Parent as IModelObject;
                if (modelObject != null) return modelObject.DalFactoryName;

                // attempt to get the name from a parent model list
                IModelList modelList = this.Parent as IModelList;
                if (modelList != null) return modelList.DalFactoryName;

                // throw an exception
                // NF - TODO
                throw new InvalidOperationException("The dal factory name could not be determined");
            }
        }

        #endregion

        #endregion

        #region Criteria

        /// <summary>
        /// Generic criteria used when fetching a list
        /// by its parent id
        /// </summary>
        [Serializable]
        public class FetchByParentIdCriteria : CriteriaBase<FetchByParentIdCriteria>
        {
            #region Properties

            #region DalFactoryName
            /// <summary>
            /// DalFactoryName property definition
            /// </summary>
            public static readonly PropertyInfo<String> DalFactoryNameProperty =
                RegisterProperty<String>(c => c.DalFactoryName);
            /// <summary>
            /// Returns the dal factory name
            /// </summary>
            public String DalFactoryName
            {
                get { return this.ReadProperty<String>(DalFactoryNameProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// ParentId property definition
            /// </summary>
            public static readonly PropertyInfo<Object> ParentIdProperty =
                RegisterProperty<Object>(c => c.ParentId);
            /// <summary>
            /// Retusn the parent id property
            /// </summary>
            public Object ParentId
            {
                get { return this.ReadProperty<Object>(ParentIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByParentIdCriteria(String dalFactoryName, Object parentId)
            {
                this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
                this.LoadProperty<Object>(ParentIdProperty, parentId);
            }
            #endregion
        }

        /// <summary>
        /// Generic criteria used when returning all items
        /// from the dalfactory.
        /// </summary>
        [Serializable]
        public class FetchAllCriteria : CriteriaBase<FetchAllCriteria>
        {
            #region Properties

            #region DalFactoryName
            /// <summary>
            /// DalFactoryName property definition
            /// </summary>
            public static readonly PropertyInfo<String> DalFactoryNameProperty =
                RegisterProperty<String>(c => c.DalFactoryName);
            /// <summary>
            /// Returns the dal factory name
            /// </summary>
            public String DalFactoryName
            {
                get { return this.ReadProperty<String>(DalFactoryNameProperty); }
            }
            #endregion

            #endregion

            #region Constructor

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchAllCriteria() { }

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchAllCriteria(String dalFactoryName)
            {
                this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            }
            #endregion
        }

        #endregion

        #region Methods

        /// <summary>
        /// Hook into child object events.
        /// </summary>
        protected override void OnAddEventHooks(TItem item)
        {
            base.OnAddEventHooks(item);

            INotifyPropertyChanging pc = item as INotifyPropertyChanging;
            if (pc != null)
            {
                pc.PropertyChanging += Child_PropertyChanging;
            }

            INotifyChildChanging cc = item as INotifyChildChanging;
            if (cc != null)
            {
                cc.ChildChanging += Child_Changing;
            }
        }

        /// <summary>
        /// Unhook child object events.
        /// </summary>
        protected override void OnRemoveEventHooks(TItem item)
        {
            base.OnRemoveEventHooks(item);

            INotifyPropertyChanging pc = item as INotifyPropertyChanging;
            if (pc != null)
            {
                pc.PropertyChanging -= Child_PropertyChanging;
            }

            INotifyChildChanging cc = item as INotifyChildChanging;
            if (cc != null)
            {
                cc.ChildChanging -= Child_Changing;
            }
        }

        /// <summary>
        /// Returns the correct dal factory
        /// </summary>
        protected virtual IDalFactory GetDalFactory()
        {
            return DalContainer.GetDalFactory(this.DalFactoryName);
        }

        /// <summary>
        /// Returns the specified dal factory
        /// </summary>
        protected virtual IDalFactory GetDalFactory(String dalName)
        {
            return DalContainer.GetDalFactory(dalName);
        }

        #endregion

        #region Child Changing Notification

        //TODO: This needs moving into the framework model object.

        [NonSerialized]
        [Csla.NotUndoable]
        private EventHandler<ChildChangingEventArgs> _childChangingHandlers;

        /// <summary>
        /// Event raised when a child object is about to change.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:ValidateArgumentsOfPublicMethods")]
        public event EventHandler<ChildChangingEventArgs> ChildChanging
        {
            add
            {
                _childChangingHandlers = (EventHandler<ChildChangingEventArgs>)
                  System.Delegate.Combine(_childChangingHandlers, value);
            }
            remove
            {
                _childChangingHandlers = (EventHandler<ChildChangingEventArgs>)
                  System.Delegate.Remove(_childChangingHandlers, value);
            }
        }

        /// <summary>
        /// Raises the child changing event to indicate that a child object is about to change.
        /// </summary>
        /// <param name="e">ChildChangingEventArgs object</param>
        protected virtual void OnChildChanging(ChildChangingEventArgs e)
        {
            if (_childChangingHandlers != null)
                _childChangingHandlers.Invoke(this, e);
        }

        /// <summary>
        /// Creates a ChildChangingEventArgs and raises the event. 
        /// </summary>
        private void RaiseChildChanging(object childObject, PropertyChangingEventArgs propertyArgs)
        {
            ChildChangingEventArgs args = new ChildChangingEventArgs(childObject, propertyArgs);
            OnChildChanging(args);
        }

        /// <summary>
        /// Raises the event
        /// </summary>
        private void RaiseChildChanging(ChildChangingEventArgs e)
        {
            OnChildChanging(e);
        }

        /// <summary>
        /// Handles any PropertyChanging event from a child object and echoes it up as a
        /// ChildChanging event.
        /// </summary>
        private void Child_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            RaiseChildChanging(sender, e);
        }

        /// <summary>
        /// Handles any ChildChanging event from a child object and echoes it up as a
        /// ChildChanging event.
        /// </summary>
        private void Child_Changing(object sender, ChildChangingEventArgs e)
        {
            RaiseChildChanging(e);
        }

        #endregion
    }
}
