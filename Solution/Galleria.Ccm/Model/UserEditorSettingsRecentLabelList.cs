﻿#region Header Information 
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31699 : A.Heathcote
//  Created.
// V8-31699 : A.Heathcote 
//  Expanded the Add method.
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// List of user setting Recentlabel
    /// </summary>
    [Serializable]
    public sealed partial class UserEditorSettingsRecentLabelList : ModelList<UserEditorSettingsRecentLabelList, UserEditorSettingsRecentLabel>
    {
        #region Authorization Rules 

        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Factory Methods 
        /// <summary>
        /// Called to create a new list
        /// </summary>
        /// <returns>A New List</returns>
        public static UserEditorSettingsRecentLabelList NewList()
        {
            UserEditorSettingsRecentLabelList list = new UserEditorSettingsRecentLabelList();
            list.Create();
            return list;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Adds new item to NewuserEditorSettingsRecentLabel
        /// </summary>
        /// <param name="label">The label being added</param>
        public void Add(Label label, Int32 maxRecentItems = 10)
        {
            AddLabelId(label.Id, label.Name, label.Type, maxRecentItems);
        }

        /// <summary>
        /// Adds new item to NewuserEditorSettingsRecentLabel
        /// </summary>
        public void AddLabelId(Object labelId, String labelName, LabelType labelType, Int32 maxRecentItems = 10)
        {
            if (labelId == null) return;

            //return out if the label is already in the list.
            //TODO: Should really add last accessed date a
            if (this.Any(l => Object.Equals(l.LabelId, labelId))) return;

            switch(labelType)
            {
                case LabelType.Product:
                    {
                        List<UserEditorSettingsRecentLabel> productLabels = this.Where(rl => rl.LabelType.Equals(LabelType.Product)).ToList();

                        if (productLabels.Count >= maxRecentItems)
                        {
                            this.Remove(productLabels[0]);
                        }

                        this.Add(UserEditorSettingsRecentLabel.NewUserEditorSettingsRecentLabel(labelId, labelName, labelType));
                    }
                    break;

                case LabelType.Fixture:
                    {
                        List<UserEditorSettingsRecentLabel> fixtureLabels = this.Where(rl => rl.LabelType.Equals(LabelType.Fixture)).ToList();
                        if (fixtureLabels.Count >= maxRecentItems)
                        {
                            this.Remove(fixtureLabels[0]);
                        }

                        this.Add(UserEditorSettingsRecentLabel.NewUserEditorSettingsRecentLabel(labelId, labelName, labelType));
                    }
                    break;

                default:
                    Debug.Fail("Label type not recognised");
                    break;
            }

        }


        #endregion
    }
}

