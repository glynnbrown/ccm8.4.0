﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// L.Hodson
//  Created
// V8-25669 : A.Kuszyk
//      Added EntityId property to DTO.
// CCM-25452 : N.Haywood
//		Readded product group id and missing fetch methods required for imports
// V8-26041 : A.Kuszyk
//      Changed use of CCM ProductStatusType to Framework PlanogramProductStatusType.
//      Changed use of CCM ProductOrientationType to Framework PlanogramProductOrientationType.
//      Introduced use of PlanogramProductMerchandisingStyle type for MerchandisingStyle property.
//      Renamed FillPattern property to FillPatternType
//      Added ShapeType and changed FillColour to Int32.
// CCM-26150 : L.Ineson
//	Added CustomAttributes property
// V8-27424 : L.Luong
//  Added FinancialGroupCode and FinancialGroupName
// V8-26777 : L.Ineson
//  Removed CanMerch properties
#endregion
#region Version History: CCM801
// V8-28827 : L.Ineson
//  Removed specific update call for customattributes object as this is no longer required.
#endregion
#region Version History: CCM820
// V8-31456 : N.Foster
//  Added method to fetch a batch of custom attribute data
#endregion
#region Version History: CCM830
// V8-31531 : A.Heathcote
//  Removed IsTrayProduct
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class Product
    {
        #region Constructors
        private Product() { } //force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing Product with the given id
        /// </summary>
        public static Product FetchById(Int32 id)
        {
            return DataPortal.Fetch<Product>(new FetchByIdCriteria(id));
        }

        /// <summary>
        /// Returns an existing object
        /// Any child data will be lazy loaded.
        /// </summary>
        /// <returns>An existing unit</returns>
        internal static Product GetProduct(IDalContext dalContext, ProductDto productDto, CustomAttributeDataDto customAttributeDataDto)
        {
            return DataPortal.FetchChild<Product>(dalContext, productDto, customAttributeDataDto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private ProductDto GetDataTransferObject()
        {
            return new ProductDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                ReplacementProductId = ReadProperty<Int32?>(ReplacementProductIdProperty),
                Gtin = ReadProperty<String>(GtinProperty),
                Name = ReadProperty<String>(NameProperty),
                Brand = ReadProperty<String>(BrandProperty),
                Height = ReadProperty<Single>(HeightProperty),
                Width = ReadProperty<Single>(WidthProperty),
                Depth = ReadProperty<Single>(DepthProperty),
                DisplayHeight = ReadProperty<Single>(DisplayHeightProperty),
                DisplayWidth = ReadProperty<Single>(DisplayWidthProperty),
                DisplayDepth = ReadProperty<Single>(DisplayDepthProperty),
                AlternateHeight = ReadProperty<Single>(AlternateHeightProperty),
                AlternateWidth = ReadProperty<Single>(AlternateWidthProperty),
                AlternateDepth = ReadProperty<Single>(AlternateDepthProperty),
                PointOfPurchaseHeight = ReadProperty<Single>(PointOfPurchaseHeightProperty),
                PointOfPurchaseWidth = ReadProperty<Single>(PointOfPurchaseWidthProperty),
                PointOfPurchaseDepth = ReadProperty<Single>(PointOfPurchaseDepthProperty),
                NumberOfPegHoles = ReadProperty<Byte>(NumberOfPegHolesProperty),
                PegX = ReadProperty<Single>(PegXProperty),
                PegX2 = ReadProperty<Single>(PegX2Property),
                PegX3 = ReadProperty<Single>(PegX3Property),
                PegY = ReadProperty<Single>(PegYProperty),
                PegY2 = ReadProperty<Single>(PegY2Property),
                PegY3 = ReadProperty<Single>(PegY3Property),
                PegProngOffsetX = ReadProperty<Single>(PegProngOffsetXProperty),
                PegProngOffsetY = ReadProperty<Single>(PegProngOffsetYProperty),
                PegDepth = ReadProperty<Single>(PegDepthProperty),
                SqueezeHeight = ReadProperty<Single>(SqueezeHeightProperty),
                SqueezeWidth = ReadProperty<Single>(SqueezeWidthProperty),
                SqueezeDepth = ReadProperty<Single>(SqueezeDepthProperty),
                NestingHeight = ReadProperty<Single>(NestingHeightProperty),
                NestingWidth = ReadProperty<Single>(NestingWidthProperty),
                NestingDepth = ReadProperty<Single>(NestingDepthProperty),
                CasePackUnits = ReadProperty<Int16>(CasePackUnitsProperty),
                CaseHigh = ReadProperty<Byte>(CaseHighProperty),
                CaseWide = ReadProperty<Byte>(CaseWideProperty),
                CaseDeep = ReadProperty<Byte>(CaseDeepProperty),
                CaseHeight = ReadProperty<Single>(CaseHeightProperty),
                CaseWidth = ReadProperty<Single>(CaseWidthProperty),
                CaseDepth = ReadProperty<Single>(CaseDepthProperty),
                MaxStack = ReadProperty<Byte>(MaxStackProperty),
                MaxTopCap = ReadProperty<Byte>(MaxTopCapProperty),
                MaxRightCap = ReadProperty<Byte>(MaxRightCapProperty),
                MinDeep = ReadProperty<Byte>(MinDeepProperty),
                MaxDeep = ReadProperty<Byte>(MaxDeepProperty),
                TrayPackUnits = ReadProperty<Int16>(TrayPackUnitsProperty),
                TrayHigh = ReadProperty<Byte>(TrayHighProperty),
                TrayWide = ReadProperty<Byte>(TrayWideProperty),
                TrayDeep = ReadProperty<Byte>(TrayDeepProperty),
                TrayHeight = ReadProperty<Single>(TrayHeightProperty),
                TrayWidth = ReadProperty<Single>(TrayWidthProperty),
                TrayDepth = ReadProperty<Single>(TrayDepthProperty),
                TrayThickHeight = ReadProperty<Single>(TrayThickHeightProperty),
                TrayThickWidth = ReadProperty<Single>(TrayThickWidthProperty),
                TrayThickDepth = ReadProperty<Single>(TrayThickDepthProperty),
                FrontOverhang = ReadProperty<Single>(FrontOverhangProperty),
                FingerSpaceAbove = ReadProperty<Single>(FingerSpaceAboveProperty),
                FingerSpaceToTheSide = ReadProperty<Single>(FingerSpaceToTheSideProperty),
                StatusType = (Byte)ReadProperty<PlanogramProductStatusType>(StatusTypeProperty),
                OrientationType = (Byte)ReadProperty<PlanogramProductOrientationType>(OrientationTypeProperty),
                MerchandisingStyle = (Byte)ReadProperty<PlanogramProductMerchandisingStyle>(MerchandisingStyleProperty),
                IsFrontOnly = ReadProperty<Boolean>(IsFrontOnlyProperty),
                IsPlaceHolderProduct = ReadProperty<Boolean>(IsPlaceHolderProductProperty),
                IsActive = ReadProperty<Boolean>(IsActiveProperty),
                CanBreakTrayUp = ReadProperty<Boolean>(CanBreakTrayUpProperty),
                CanBreakTrayDown = ReadProperty<Boolean>(CanBreakTrayDownProperty),
                CanBreakTrayBack = ReadProperty<Boolean>(CanBreakTrayBackProperty),
                CanBreakTrayTop = ReadProperty<Boolean>(CanBreakTrayTopProperty),
                ForceMiddleCap = ReadProperty<Boolean>(ForceMiddleCapProperty),
                ForceBottomCap = ReadProperty<Boolean>(ForceBottomCapProperty),
                Shape = ReadProperty<String>(ShapeProperty),
                ShapeType = (Byte)ReadProperty<PlanogramProductShapeType>(ShapeTypeProperty),
                FillColour = ReadProperty<Int32>(FillColourProperty),
                FillPatternType = (Byte)ReadProperty<PlanogramProductFillPatternType>(FillPatternTypeProperty),
                PointOfPurchaseDescription = ReadProperty<String>(PointOfPurchaseDescriptionProperty),
                ShortDescription = ReadProperty<String>(ShortDescriptionProperty),
                Subcategory = ReadProperty<String>(SubcategoryProperty),
                CustomerStatus = ReadProperty<String>(CustomerStatusProperty),
                Colour = ReadProperty<String>(ColourProperty),
                Flavour = ReadProperty<String>(FlavourProperty),
                PackagingShape = ReadProperty<String>(PackagingShapeProperty),
                PackagingType = ReadProperty<String>(PackagingTypeProperty),
                CountryOfOrigin = ReadProperty<String>(CountryOfOriginProperty),
                CountryOfProcessing = ReadProperty<String>(CountryOfProcessingProperty),
                ShelfLife = ReadProperty<Int16>(ShelfLifeProperty),
                DeliveryFrequencyDays = ReadProperty<Single?>(DeliveryFrequencyDaysProperty),
                DeliveryMethod = ReadProperty<String>(DeliveryMethodProperty),
                VendorCode = ReadProperty<String>(VendorCodeProperty),
                Vendor = ReadProperty<String>(VendorProperty),
                ManufacturerCode = ReadProperty<String>(ManufacturerCodeProperty),
                Manufacturer = ReadProperty<String>(ManufacturerProperty),
                Size = ReadProperty<String>(SizeProperty),
                UnitOfMeasure = ReadProperty<String>(UnitOfMeasureProperty),
                DateIntroduced = ReadProperty<DateTime?>(DateIntroducedProperty),
                DateDiscontinued = ReadProperty<DateTime?>(DateDiscontinuedProperty),
                DateEffective = ReadProperty<DateTime?>(DateEffectiveProperty),
                Health = ReadProperty<String>(HealthProperty),
                CorporateCode = ReadProperty<String>(CorporateCodeProperty),
                Barcode = ReadProperty<String>(BarcodeProperty),
                SellPrice = ReadProperty<Single?>(SellPriceProperty),
                SellPackCount = ReadProperty<Int16?>(SellPackCountProperty),
                SellPackDescription = ReadProperty<String>(SellPackDescriptionProperty),
                RecommendedRetailPrice = ReadProperty<Single?>(RecommendedRetailPriceProperty),
                ManufacturerRecommendedRetailPrice = ReadProperty<Single?>(ManufacturerRecommendedRetailPriceProperty),
                CostPrice = ReadProperty<Single?>(CostPriceProperty),
                CaseCost = ReadProperty<Single?>(CaseCostProperty),
                TaxRate = ReadProperty<Single?>(TaxRateProperty),
                ConsumerInformation = ReadProperty<String>(ConsumerInformationProperty),
                Texture = ReadProperty<String>(TextureProperty),
                StyleNumber = ReadProperty<Int16?>(StyleNumberProperty),
                Pattern = ReadProperty<String>(PatternProperty),
                Model = ReadProperty<String>(ModelProperty),
                GarmentType = ReadProperty<String>(GarmentTypeProperty),
                IsPrivateLabel = ReadProperty<Boolean>(IsPrivateLabelProperty),
                IsNewProduct = ReadProperty<Boolean>(IsNewProductProperty),
                FinancialGroupCode = ReadProperty<String>(FinancialGroupCodeProperty),
                FinancialGroupName = ReadProperty<String>(FinancialGroupNameProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty)
            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        /// <Param name="dalContext"> The current dal context </param>
        private void LoadDataTransferObject(IDalContext dalContext, ProductDto productDto, CustomAttributeDataDto customAttributeDataDto)
        {
            LoadProperty<Int32>(IdProperty, productDto.Id);
            LoadProperty<RowVersion>(RowVersionProperty, productDto.RowVersion);
            LoadProperty<Int32>(EntityIdProperty, productDto.EntityId);
            LoadProperty<Int32?>(ReplacementProductIdProperty, productDto.ReplacementProductId);
            LoadProperty<String>(GtinProperty, productDto.Gtin);
            LoadProperty<String>(NameProperty, productDto.Name);
            LoadProperty<String>(BrandProperty, productDto.Brand);
            LoadProperty<Single>(HeightProperty, productDto.Height);
            LoadProperty<Single>(WidthProperty, productDto.Width);
            LoadProperty<Single>(DepthProperty, productDto.Depth);
            LoadProperty<Single>(DisplayHeightProperty, productDto.DisplayHeight);
            LoadProperty<Single>(DisplayWidthProperty, productDto.DisplayWidth);
            LoadProperty<Single>(DisplayDepthProperty, productDto.DisplayDepth);
            LoadProperty<Single>(AlternateHeightProperty, productDto.AlternateHeight);
            LoadProperty<Single>(AlternateWidthProperty, productDto.AlternateWidth);
            LoadProperty<Single>(AlternateDepthProperty, productDto.AlternateDepth);
            LoadProperty<Single>(PointOfPurchaseHeightProperty, productDto.PointOfPurchaseHeight);
            LoadProperty<Single>(PointOfPurchaseWidthProperty, productDto.PointOfPurchaseWidth);
            LoadProperty<Single>(PointOfPurchaseDepthProperty, productDto.PointOfPurchaseDepth);
            LoadProperty<Byte>(NumberOfPegHolesProperty, productDto.NumberOfPegHoles);
            LoadProperty<Single>(PegXProperty, productDto.PegX);
            LoadProperty<Single>(PegX2Property, productDto.PegX2);
            LoadProperty<Single>(PegX3Property, productDto.PegX3);
            LoadProperty<Single>(PegYProperty, productDto.PegY);
            LoadProperty<Single>(PegY2Property, productDto.PegY2);
            LoadProperty<Single>(PegY3Property, productDto.PegY3);
            LoadProperty<Single>(PegProngOffsetXProperty, productDto.PegProngOffsetX);
            LoadProperty<Single>(PegProngOffsetYProperty, productDto.PegProngOffsetY);
            LoadProperty<Single>(PegDepthProperty, productDto.PegDepth);
            LoadProperty<Single>(SqueezeHeightProperty, productDto.SqueezeHeight);
            LoadProperty<Single>(SqueezeWidthProperty, productDto.SqueezeWidth);
            LoadProperty<Single>(SqueezeDepthProperty, productDto.SqueezeDepth);
            LoadProperty<Single>(NestingHeightProperty, productDto.NestingHeight);
            LoadProperty<Single>(NestingWidthProperty, productDto.NestingWidth);
            LoadProperty<Single>(NestingDepthProperty, productDto.NestingDepth);
            LoadProperty<Int16>(CasePackUnitsProperty, productDto.CasePackUnits);
            LoadProperty<Byte>(CaseHighProperty, productDto.CaseHigh);
            LoadProperty<Byte>(CaseWideProperty, productDto.CaseWide);
            LoadProperty<Byte>(CaseDeepProperty, productDto.CaseDeep);
            LoadProperty<Single>(CaseHeightProperty, productDto.CaseHeight);
            LoadProperty<Single>(CaseWidthProperty, productDto.CaseWidth);
            LoadProperty<Single>(CaseDepthProperty, productDto.CaseDepth);
            LoadProperty<Byte>(MaxStackProperty, productDto.MaxStack);
            LoadProperty<Byte>(MaxTopCapProperty, productDto.MaxTopCap);
            LoadProperty<Byte>(MaxRightCapProperty, productDto.MaxRightCap);
            LoadProperty<Byte>(MinDeepProperty, productDto.MinDeep);
            LoadProperty<Byte>(MaxDeepProperty, productDto.MaxDeep);
            LoadProperty<Int16>(TrayPackUnitsProperty, productDto.TrayPackUnits);
            LoadProperty<Byte>(TrayHighProperty, productDto.TrayHigh);
            LoadProperty<Byte>(TrayWideProperty, productDto.TrayWide);
            LoadProperty<Byte>(TrayDeepProperty, productDto.TrayDeep);
            LoadProperty<Single>(TrayHeightProperty, productDto.TrayHeight);
            LoadProperty<Single>(TrayWidthProperty, productDto.TrayWidth);
            LoadProperty<Single>(TrayDepthProperty, productDto.TrayDepth);
            LoadProperty<Single>(TrayThickHeightProperty, productDto.TrayThickHeight);
            LoadProperty<Single>(TrayThickWidthProperty, productDto.TrayThickWidth);
            LoadProperty<Single>(TrayThickDepthProperty, productDto.TrayThickDepth);
            LoadProperty<Single>(FrontOverhangProperty, productDto.FrontOverhang);
            LoadProperty<Single>(FingerSpaceAboveProperty, productDto.FingerSpaceAbove);
            LoadProperty<Single>(FingerSpaceToTheSideProperty, productDto.FingerSpaceToTheSide);
            LoadProperty<PlanogramProductStatusType>(StatusTypeProperty, (PlanogramProductStatusType)productDto.StatusType);
            LoadProperty<PlanogramProductOrientationType>(OrientationTypeProperty, (PlanogramProductOrientationType)productDto.OrientationType);
            LoadProperty<PlanogramProductMerchandisingStyle>(MerchandisingStyleProperty, (PlanogramProductMerchandisingStyle)productDto.MerchandisingStyle);
            LoadProperty<Boolean>(IsFrontOnlyProperty, productDto.IsFrontOnly);
            LoadProperty<Boolean>(IsPlaceHolderProductProperty, productDto.IsPlaceHolderProduct);
            LoadProperty<Boolean>(IsActiveProperty, productDto.IsActive);
            LoadProperty<Boolean>(CanBreakTrayUpProperty, productDto.CanBreakTrayUp);
            LoadProperty<Boolean>(CanBreakTrayDownProperty, productDto.CanBreakTrayDown);
            LoadProperty<Boolean>(CanBreakTrayBackProperty, productDto.CanBreakTrayBack);
            LoadProperty<Boolean>(CanBreakTrayTopProperty, productDto.CanBreakTrayTop);
            LoadProperty<Boolean>(ForceMiddleCapProperty, productDto.ForceMiddleCap);
            LoadProperty<Boolean>(ForceBottomCapProperty, productDto.ForceBottomCap);
            LoadProperty<String>(ShapeProperty, productDto.Shape);
            LoadProperty<PlanogramProductShapeType>(ShapeTypeProperty, (PlanogramProductShapeType)productDto.ShapeType);
            LoadProperty<Int32>(FillColourProperty, productDto.FillColour);
            LoadProperty<PlanogramProductFillPatternType>(FillPatternTypeProperty, (PlanogramProductFillPatternType)productDto.FillPatternType);
            LoadProperty<String>(PointOfPurchaseDescriptionProperty, productDto.PointOfPurchaseDescription);
            LoadProperty<String>(ShortDescriptionProperty, productDto.ShortDescription);
            LoadProperty<String>(SubcategoryProperty, productDto.Subcategory);
            LoadProperty<String>(CustomerStatusProperty, productDto.CustomerStatus);
            LoadProperty<String>(ColourProperty, productDto.Colour);
            LoadProperty<String>(FlavourProperty, productDto.Flavour);
            LoadProperty<String>(PackagingShapeProperty, productDto.PackagingShape);
            LoadProperty<String>(PackagingTypeProperty, productDto.PackagingType);
            LoadProperty<String>(CountryOfOriginProperty, productDto.CountryOfOrigin);
            LoadProperty<String>(CountryOfProcessingProperty, productDto.CountryOfProcessing);
            LoadProperty<Int16>(ShelfLifeProperty, productDto.ShelfLife);
            LoadProperty<Single?>(DeliveryFrequencyDaysProperty, productDto.DeliveryFrequencyDays);
            LoadProperty<String>(DeliveryMethodProperty, productDto.DeliveryMethod);
            LoadProperty<String>(BrandProperty, productDto.Brand);
            LoadProperty<String>(VendorCodeProperty, productDto.VendorCode);
            LoadProperty<String>(VendorProperty, productDto.Vendor);
            LoadProperty<String>(ManufacturerCodeProperty, productDto.ManufacturerCode);
            LoadProperty<String>(ManufacturerProperty, productDto.Manufacturer);
            LoadProperty<String>(SizeProperty, productDto.Size);
            LoadProperty<String>(UnitOfMeasureProperty, productDto.UnitOfMeasure);
            LoadProperty<DateTime?>(DateIntroducedProperty, productDto.DateIntroduced);
            LoadProperty<DateTime?>(DateDiscontinuedProperty, productDto.DateDiscontinued);
            LoadProperty<DateTime?>(DateEffectiveProperty, productDto.DateEffective);
            LoadProperty<String>(HealthProperty, productDto.Health);
            LoadProperty<String>(CorporateCodeProperty, productDto.CorporateCode);
            LoadProperty<String>(BarcodeProperty, productDto.Barcode);
            LoadProperty<Single?>(SellPriceProperty, productDto.SellPrice);
            LoadProperty<Int16?>(SellPackCountProperty, productDto.SellPackCount);
            LoadProperty<String>(SellPackDescriptionProperty, productDto.SellPackDescription);
            LoadProperty<Single?>(RecommendedRetailPriceProperty, productDto.RecommendedRetailPrice);
            LoadProperty<Single?>(ManufacturerRecommendedRetailPriceProperty, productDto.ManufacturerRecommendedRetailPrice);
            LoadProperty<Single?>(CostPriceProperty, productDto.CostPrice);
            LoadProperty<Single?>(CaseCostProperty, productDto.CaseCost);
            LoadProperty<Single?>(TaxRateProperty, productDto.TaxRate);
            LoadProperty<String>(ConsumerInformationProperty, productDto.ConsumerInformation);
            LoadProperty<String>(TextureProperty, productDto.Texture);
            LoadProperty<Int16?>(StyleNumberProperty, productDto.StyleNumber);
            LoadProperty<String>(PatternProperty, productDto.Pattern);
            LoadProperty<String>(ModelProperty, productDto.Model);
            LoadProperty<String>(GarmentTypeProperty, productDto.GarmentType);
            LoadProperty<Boolean>(IsPrivateLabelProperty, productDto.IsPrivateLabel);
            LoadProperty<Boolean>(IsNewProductProperty, productDto.IsNewProduct);
            LoadProperty<String>(FinancialGroupCodeProperty, productDto.FinancialGroupCode);
            LoadProperty<String>(FinancialGroupNameProperty, productDto.FinancialGroupName);
            LoadProperty<DateTime>(DateCreatedProperty, productDto.DateCreated);
            LoadProperty<DateTime>(DateLastModifiedProperty, productDto.DateLastModified);
            LoadProperty<DateTime?>(DateDeletedProperty, productDto.DateDeleted);

            if (customAttributeDataDto == null) customAttributeDataDto = new CustomAttributeDataDto();
            LoadProperty<CustomAttributeData>(CustomAttributesProperty, CustomAttributeData.Fetch(dalContext, customAttributeDataDto));
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning FetchById
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                // fetch the product dto
                ProductDto productDto;
                using (IProductDal dal = dalContext.GetDal<IProductDal>())
                {
                    productDto = dal.FetchById(criteria.Id);
                }

                // fetch the custom attribute dto
                CustomAttributeDataDto customAttributeDataDto;
                using (ICustomAttributeDataDal dal = dalContext.GetDal<ICustomAttributeDataDal>())
                {
                    customAttributeDataDto = dal.FetchByParentTypeParentId((Byte)CustomAttributeDataParentType.Product, criteria.Id);
                }

                // load the data transfer objects
                this.LoadDataTransferObject(dalContext, productDto, customAttributeDataDto);
            }
        }

        /// <summary>
        /// Called when returning an existing item
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ProductDto productDto, CustomAttributeDataDto customAttributeDataDto)
        {
            LoadDataTransferObject(dalContext, productDto, customAttributeDataDto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                ProductDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (IProductDal dal = dalContext.GetDal<IProductDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<Product>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this object in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                ProductDto dto = GetDataTransferObject();
                using (IProductDal dal = dalContext.GetDal<IProductDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IProductDal dal = dalContext.GetDal<IProductDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #endregion
    }
}
