﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class ClusterList
    {
        #region Constructor
        private ClusterList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns all clusters for a given cluster scheme
        /// </summary>
        /// <param name="clusterSchemeId">Cluster scheme id</param>
        /// <returns>Collection of clusters</returns>
        public static ClusterList FetchByClusterSchemeId(Int32 clusterSchemeId)
        {
            return DataPortal.Fetch<ClusterList>(new FetchByClusterSchemeIdCriteria(clusterSchemeId));
        }

        /// <summary>
        /// Returns all clusters for a given cluster scheme
        /// </summary>
        /// <param name="dalContext">DalContext</param>
        /// <param name="clusterSchemeId">Cluster scheme id</param>
        /// <returns>Collection of clusters</returns>
        internal static ClusterList FetchByClusterSchemeId(IDalContext dalContext, Int32 clusterSchemeId)
        {
            return DataPortal.FetchChild<ClusterList>(dalContext, new FetchByClusterSchemeIdCriteria(clusterSchemeId));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returning all clusters for a specific cluster scheme
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByClusterSchemeIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IClusterDal dal = dalContext.GetDal<IClusterDal>())
                {
                    IEnumerable<ClusterDto> dtoList = dal.FetchByClusterSchemeId(criteria.ClusterSchemeId);
                    foreach (ClusterDto dto in dtoList)
                    {
                        this.Add(Cluster.FetchCluster(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returning all clusters for a specific cluster scheme using the same IDalContext
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FetchByClusterSchemeIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            using (IClusterDal dal = dalContext.GetDal<IClusterDal>())
            {
                IEnumerable<ClusterDto> dtoList = dal.FetchByClusterSchemeId(criteria.ClusterSchemeId);
                foreach (ClusterDto dto in dtoList)
                {
                    this.Add(Cluster.FetchCluster(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #region Update
        /// <summary>
        /// Called when this list is being updated
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                Child_Update(dalContext);
                dalContext.Commit();
            }
        }
        #endregion

        #endregion
    }
}
