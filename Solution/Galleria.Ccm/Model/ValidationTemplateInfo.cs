﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26401 : A.Silva ~ Created.

#endregion

#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Readonly cutdown of a <see cref="ValidationTemplate"/> object.
    /// </summary>
    /// <remarks>This is a child object for <see cref="ValidationTemplateInfoList"/>.</remarks>
    [Serializable]
    public sealed partial class ValidationTemplateInfo : ModelReadOnlyObject<ValidationTemplateInfo>
    {
        #region Properties

        #region Id

        /// <summary>
        ///		Registers the <see cref="Id"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Object> IdProperty =
            RegisterModelProperty<Object>(o => o.Id);

        /// <summary>
        ///		Gets or sets the value for the <see cref="Id"/> property.
        /// </summary>
        public Object Id
        {
            get { return GetProperty(IdProperty); }
        }

        #endregion

        #region EntityId

        /// <summary>
        ///		Metadata for the <see cref="EntityId"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(o => o.EntityId);

        /// <summary>
        ///		Gets or sets the value for the <see cref="EntityId"/> property.
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty(EntityIdProperty); }
        }

        #endregion

        #region Name

        /// <summary>
        ///		Metadata for the <see cref="Name"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(o => o.Name);

        /// <summary>
        ///		Gets or sets the value for the <see cref="Name"/> property.
        /// </summary>
        public String Name
        {
            get { return GetProperty(NameProperty); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ValidationTemplateInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(ValidationTemplateInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ValidationTemplateInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ValidationTemplateInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Overrides

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:ValidationTemplateInfo"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:ValidationTemplateInfo"/>.
        /// </returns>
        public override string ToString()
        {
            return Name;
        }

        #endregion
    }
}
