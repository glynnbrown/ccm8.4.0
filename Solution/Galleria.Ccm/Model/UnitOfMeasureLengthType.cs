﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26179  : I.George
//  Created
// V8-28149 : M.Pettit
//  Ensured all types have friendly names for reporting
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Denotes the available length unit of measure types
    /// </summary>
    public enum UnitOfMeasureLengthType
    {
        Unknown = 0,
        Centimeters = 1,
        Inches = 2
    }

    /// <summary>
    /// UnitOfMeasureLengthType Helper class
    /// </summary>
    public static class UnitOfMeasureLengthTypeHelper
    {

        public static readonly Dictionary<UnitOfMeasureLengthType, String> FriendlyNames =
            new Dictionary<UnitOfMeasureLengthType, String>()
            {
                {UnitOfMeasureLengthType.Unknown, String.Empty},
                {UnitOfMeasureLengthType.Centimeters, Message.Enum_UnitOfMeasureLengthType_Centimeters},
                {UnitOfMeasureLengthType.Inches, Message.Enum_EntityUnitOfMeasureLengthType_Inches},
            };
        /// <summary>
        /// Dictionary with friendly value as key and friendly name value 
        /// </summary>
        public static readonly Dictionary<UnitOfMeasureLengthType, String> Abbreviations =
            new Dictionary<UnitOfMeasureLengthType, String>()
            {
                {UnitOfMeasureLengthType.Unknown, String.Empty},
                {UnitOfMeasureLengthType.Centimeters, Message.Enum_UnitOfMeasureLengthType_Centimeters_Abbrev},
                {UnitOfMeasureLengthType.Inches, Message.Enum_UnitOfMeasureLengthType_Inches_Abbrev},
            };

        public static String GetLengthCubedFriendlyName(UnitOfMeasureLengthType lengthType)
        {
            switch (lengthType)
            {
                default:
                case UnitOfMeasureLengthType.Unknown: return null;

                case UnitOfMeasureLengthType.Centimeters: return Message.Enum_UnitOfMeasureLengthType_CentimetersCubed;
                case UnitOfMeasureLengthType.Inches: return Message.Enum_UnitOfMeasureLengthType_InchesCubed;
            }
        }

        public static String GetLengthCubedAbbreviation(UnitOfMeasureLengthType lengthType)
        {
            switch (lengthType)
            {
                default:
                case UnitOfMeasureLengthType.Unknown: return null;

                case UnitOfMeasureLengthType.Centimeters: return Message.Enum_UnitOfMeasureLengthType_CentimetersCubed_Abbrev;
                case UnitOfMeasureLengthType.Inches: return Message.Enum_UnitOfMeasureLengthType_InchesCubed_Abbrev;
            }
        }
    }
}
