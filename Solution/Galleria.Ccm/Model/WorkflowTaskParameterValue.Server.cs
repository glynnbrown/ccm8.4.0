﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25886 : L.Ineson
//  Created
// V8-27426 : L.Ineson
//  Added 2 more values
#endregion

#region Version History : CCM 801
// V8-28835 : A.Kuszyk
//  Removed call to FieldManager.UpdateChildren() in delete method.
#endregion
#endregion

using System;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class WorkflowTaskParameterValue
    {
        #region Constructors
        private WorkflowTaskParameterValue() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a new instance from a dto
        /// </summary>
        internal static WorkflowTaskParameterValue GetWorkflowTaskParameterValue(IDalContext dalContext, WorkflowTaskParameterValueDto dto)
        {
            return DataPortal.FetchChild<WorkflowTaskParameterValue>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, WorkflowTaskParameterValueDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Object>(Value1Property, dto.Value1);
            this.LoadProperty<Object>(Value2Property, dto.Value2);
            this.LoadProperty<Object>(Value3Property, dto.Value3);
        }

        /// <summary>
        /// Gets a data transfer object from this instance
        /// </summary>
        private WorkflowTaskParameterValueDto GetDataTransferObject(WorkflowTaskParameter parent)
        {
            return new WorkflowTaskParameterValueDto()
            {
                Id = this.ReadProperty<Int32>(IdProperty),
                Value1 = this.ReadProperty<Object>(Value1Property),
                Value2 = this.ReadProperty<Object>(Value2Property),
                Value3 = this.ReadProperty<Object>(Value3Property),
                WorkflowTaskParameterId = parent.Id
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when loading a child instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, WorkflowTaskParameterValueDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting a child instance of this type
        /// </summary>
        private void Child_Insert(IDalContext dalContext, WorkflowTaskParameter parent)
        {
            WorkflowTaskParameterValueDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IWorkflowTaskParameterValueDal dal = dalContext.GetDal<IWorkflowTaskParameterValueDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<WorkflowTaskParameterValue>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating a child instance of this type
        /// </summary>
        private void Child_Update(IDalContext dalContext, WorkflowTaskParameter parent)
        {
            if (this.IsSelfDirty)
            {
                using (IWorkflowTaskParameterValueDal dal = dalContext.GetDal<IWorkflowTaskParameterValueDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this child is being deleted
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, WorkflowTaskParameter parent)
        {
            using (IWorkflowTaskParameterValueDal dal = dalContext.GetDal<IWorkflowTaskParameterValueDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion
    }
}
