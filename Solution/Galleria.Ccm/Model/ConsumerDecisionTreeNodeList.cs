﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8025632 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a list containing ConsumerDecisionTreeNode objects
    /// (Child of ConsumerDecisionTreeNode)
    /// </summary>
    [Serializable]
    public sealed partial class ConsumerDecisionTreeNodeList : ModelList<ConsumerDecisionTreeNodeList, ConsumerDecisionTreeNode>
    {
        #region Parent

        /// <summary>
        /// Returns the parent node
        /// </summary>
        public ConsumerDecisionTreeNode ParentNode
        {
            get { return base.Parent as ConsumerDecisionTreeNode; }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeNodeList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ConsumerDecisionTreeCreate.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeNodeList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ConsumerDecisionTreeGet.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeNodeList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ConsumerDecisionTreeEdit.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeNodeList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ConsumerDecisionTreeDelete.ToString()));
        }
        #endregion

        #region Properties

        /// <summary>
        /// Returns the total number of products
        /// contained within the nodes in this node list
        /// </summary>
        public Int32 TotalProductCount
        {
            get
            {
                Int32 totalProds = 0;
                foreach (ConsumerDecisionTreeNode node in this)
                {
                    totalProds += node.TotalProductCount;
                }
                return totalProds;
            }
        }

        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        public static ConsumerDecisionTreeNodeList NewConsumerDecisionTreeNodeList()
        {
            ConsumerDecisionTreeNodeList item = new ConsumerDecisionTreeNodeList();
            item.Create();
            return item;
        }

        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        public static ConsumerDecisionTreeNodeList NewConsumerDecisionTreeNodeList
            (ConsumerDecisionTreeNodeList nodeList, ConsumerDecisionTreeNode rootNode)
        {
            ConsumerDecisionTreeNodeList item = new ConsumerDecisionTreeNodeList();
            item.Create(nodeList, rootNode);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create()
        {
            MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(ConsumerDecisionTreeNodeList nodeList, ConsumerDecisionTreeNode rootNode)
        {
            foreach (ConsumerDecisionTreeNode node in nodeList)
            {
                this.Add(ConsumerDecisionTreeNode.NewConsumerDecisionTreeNode(node, rootNode));
            }

            MarkAsChild();
        }

        #endregion

        #endregion
    }
}
