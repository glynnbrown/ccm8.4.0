﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Csla;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentMinorRevisionAmendDistributionAction
    {
        #region Constructor
        private AssortmentMinorRevisionAmendDistributionAction() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static AssortmentMinorRevisionAmendDistributionAction FetchAssortmentMinorRevisionAmendDistributionAction(IDalContext dalContext, AssortmentMinorRevisionAmendDistributionActionDto dto)
        {
            return DataPortal.FetchChild<AssortmentMinorRevisionAmendDistributionAction>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private AssortmentMinorRevisionAmendDistributionActionDto GetDataTransferObject(AssortmentMinorRevision parent)
        {
            return new AssortmentMinorRevisionAmendDistributionActionDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                ProductGtin = ReadProperty<String>(ProductGtinProperty),
                ProductName = ReadProperty<String>(ProductNameProperty),
                ProductId = ReadProperty<Int32?>(ProductIdProperty),
                Priority = ReadProperty<Int32>(PriorityProperty),
                Comments = ReadProperty<String>(CommentsProperty),
                AssortmentMinorRevisionId = parent.Id
            };
        }

        /// <summary>
        /// Loads this object from the given dto
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, AssortmentMinorRevisionAmendDistributionActionDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<String>(ProductGtinProperty, dto.ProductGtin);
            LoadProperty<String>(ProductNameProperty, dto.ProductName);
            LoadProperty<Int32?>(ProductIdProperty, dto.ProductId);
            LoadProperty<Int32>(PriorityProperty, dto.Priority);
            LoadProperty<String>(CommentsProperty, dto.Comments);
            LoadProperty<AssortmentMinorRevisionAmendDistributionActionLocationList>(LocationsProperty, AssortmentMinorRevisionAmendDistributionActionLocationList.FetchByAssortmentMinorRevisionAmendDistributionActionId(dalContext, dto.Id));
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when this instance is being loaded
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, AssortmentMinorRevisionAmendDistributionActionDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when this instance is being inserted into the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent product universe</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, AssortmentMinorRevision parent)
        {
            AssortmentMinorRevisionAmendDistributionActionDto dto = GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IAssortmentMinorRevisionAmendDistributionActionDal dal = dalContext.GetDal<IAssortmentMinorRevisionAmendDistributionActionDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<Int32>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update

        /// <summary>
        /// Called when this instance is being updated in the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent product universe</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, AssortmentMinorRevision parent)
        {
            using (IAssortmentMinorRevisionAmendDistributionActionDal dal = dalContext.GetDal<IAssortmentMinorRevisionAmendDistributionActionDal>())
            {
                dal.Update(GetDataTransferObject(parent));
            }
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Delete

        /// <summary>
        /// Called when this instance is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IAssortmentMinorRevisionAmendDistributionActionDal dal = dalContext.GetDal<IAssortmentMinorRevisionAmendDistributionActionDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion
    }
}
