#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// CCM-25879 : Martin Shelley
//	Created (Auto-generated)
#endregion

#region Version History: (CCM 8.0.2)
// V8-28986 : Martin Shelley
//	Modified to allow planogram assignment by location
#endregion

#region Version History: (CCM 8.0.3)
// V8-29326 : M.Shelley
//  Added methods to fetch by cluster scheme id
#endregion

#endregion

using System;
using System.Collections.Generic;

using Csla;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
	public sealed partial class PlanAssignmentStoreSpaceInfoList
    {
        #region Constructor
        private PlanAssignmentStoreSpaceInfoList() { } // force use of factory methods
        #endregion

		#region Factory Methods

        /// <summary>
        /// Fetches the list by entity and product group id 
        /// </summary>
        public static PlanAssignmentStoreSpaceInfoList FetchByEntityIdProductGroupId(Int32 entityId, Int32 productGroupId)
        {
            return DataPortal.Fetch<PlanAssignmentStoreSpaceInfoList>(new FetchByEntityIdProductIdCriteria(entityId, productGroupId));
        }

        /// <summary>
        /// Fetches the list by entity, product group id and cluster scheme id
        /// </summary>
        public static PlanAssignmentStoreSpaceInfoList FetchByEntityIdProductGroupId(Int32 entityId, Int32 productGroupId, Int32? clusterSchemeId)
        {
            return DataPortal.Fetch<PlanAssignmentStoreSpaceInfoList>(new FetchByEntityIdProductIdCriteria(entityId, productGroupId, clusterSchemeId));
        }

        /// <summary>
        /// Fetches the list by entity and location id 
        /// </summary>
        public static PlanAssignmentStoreSpaceInfoList FetchByEntityIdLocationId(Int32 entityId, Int16 locationId)
        {
            return DataPortal.Fetch<PlanAssignmentStoreSpaceInfoList>(new FetchByEntityIdLocationIdCriteria(entityId, locationId));
        }

        /// <summary>
        /// Fetches the list by entity and location id 
        /// </summary>
        public static PlanAssignmentStoreSpaceInfoList FetchByEntityIdLocationId(Int32 entityId, Int16 locationId, Int32? clusterSchemeId)
        {
            return DataPortal.Fetch<PlanAssignmentStoreSpaceInfoList>(new FetchByEntityIdLocationIdCriteria(entityId, locationId, clusterSchemeId));
        }

        #endregion

        #region Data Access

        #region Fetch
        
		/// <summary>
        /// Called when returning all items for a entity and product group id
        /// </summary>
		[SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdProductIdCriteria criteria)
        {
            RaiseListChangedEvents = false;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanAssignmentStoreSpaceInfoDal dal = dalContext.GetDal<IPlanAssignmentStoreSpaceInfoDal>())
                {
                    IEnumerable<PlanAssignmentStoreSpaceInfoDto> dtoList =
                        dal.FetchByEntityIdProductGroupId(criteria.EntityId, criteria.ProductGroupId, criteria.ClusterSchemeId);

                    foreach (PlanAssignmentStoreSpaceInfoDto dto in dtoList)
                    {
                        this.Add(PlanAssignmentStoreSpaceInfo.GetPlanAssignmentStoreSpaceInfo(dalContext, dto));
                    }
                }
                RaiseListChangedEvents = true;
            }
        }

        /// <summary>
        /// Called when returning all items for a entity and location id
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdLocationIdCriteria criteria)
        {
            RaiseListChangedEvents = false;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanAssignmentStoreSpaceInfoDal dal = dalContext.GetDal<IPlanAssignmentStoreSpaceInfoDal>())
                {
                    IEnumerable<PlanAssignmentStoreSpaceInfoDto> dtoList =
                        dal.FetchByEntityIdLocationId(criteria.EntityId, criteria.LocationId, criteria.ClusterSchemeId);

                    foreach (PlanAssignmentStoreSpaceInfoDto dto in dtoList)
                    {
                        this.Add(PlanAssignmentStoreSpaceInfo.GetPlanAssignmentStoreSpaceInfo(dalContext, dto));
                    }
                }
                RaiseListChangedEvents = true;
            }
        }

        #endregion

        #endregion
    }
}