﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//	Created (Auto-generated)
// V8-25629 : A.Kuszyk
//  Added FetchAllChildGroups().
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;


namespace Galleria.Ccm.Model
{
    /// <summary>
    /// LocationGroup Model object
    /// </summary>
    [Serializable]
    public sealed partial class LocationGroup : ModelObject<LocationGroup>
    {
        #region Static Constructor
        static LocationGroup()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns the parent group of this
        /// </summary>
        public LocationGroup ParentGroup
        {
            get
            {
                LocationGroupList list = base.Parent as LocationGroupList;
                if (list != null)
                {
                    return list.Parent as LocationGroup;
                }
                return null;
            }
        }

        /// <summary>
        /// Returns the parent hierarchy of this
        /// </summary>
        public LocationHierarchy ParentHierarchy
        {
            get
            {
                LocationHierarchy parentHierarchy = base.Parent as LocationHierarchy;
                if (parentHierarchy == null)
                {
                    LocationGroup currentParent = this.ParentGroup;
                    while (currentParent != null)
                    {
                        if (currentParent.ParentGroup != null)
                        {
                            currentParent = currentParent.ParentGroup;
                        }
                        else
                        {
                            return currentParent.Parent as LocationHierarchy;
                        }
                    }
                }
                else
                {
                    return parentHierarchy;
                }

                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region LocationLevelId

        /// <summary>
        /// LocationLevelId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> LocationLevelIdProperty =
            RegisterModelProperty<Int32>(c => c.LocationLevelId);

        /// <summary>
        /// Gets/Sets the LocationLevelId value
        /// </summary>
        public Int32 LocationLevelId
        {
            get { return GetProperty<Int32>(LocationLevelIdProperty); }
            set { SetProperty<Int32>(LocationLevelIdProperty, value); }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region Code

        /// <summary>
        /// Code property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CodeProperty =
            RegisterModelProperty<String>(c => c.Code);

        /// <summary>
        /// Gets/Sets the Code value
        /// </summary>
        public String Code
        {
            get { return GetProperty<String>(CodeProperty); }
            set { SetProperty<String>(CodeProperty, value); }
        }

        #endregion

        #region AssignedLocationsCount

        /// <summary>
        /// AssignedLocationsCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> AssignedLocationsCountProperty =
            RegisterModelProperty<Int32>(c => c.AssignedLocationsCount);

        /// <summary>
        /// Gets/Sets the AssignedLocationsCount value
        /// </summary>
        public Int32 AssignedLocationsCount
        {
            get
            {
                if (FieldManager.GetFieldData(AssignedLocationsProperty) != null)
                {
                    return this.AssignedLocations.Count;
                }
                return GetProperty<Int32>(AssignedLocationsCountProperty);
            }
            internal set { SetProperty<Int32>(AssignedLocationsCountProperty, value); }
        }

        #endregion

        #region AssignedLocations

        /// <summary>
        /// AssignedLocations property definition
        /// </summary>
        public static readonly ModelPropertyInfo<LocationGroupLocationList> AssignedLocationsProperty =
            RegisterModelProperty<LocationGroupLocationList>(c => c.AssignedLocations, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Returns the AssignedLocations within this package
        /// </summary>
        public LocationGroupLocationList AssignedLocations
        {
            get
            {
                return this.GetPropertyLazy<LocationGroupLocationList>(
                    AssignedLocationsProperty,
                    new LocationGroupLocationList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }


        #endregion

        #region DateCreated

        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        /// Gets/Sets the DateCreated value
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
            set { SetProperty<DateTime>(DateCreatedProperty, value); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        /// Gets/Sets the DateLastModified value
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
            set { SetProperty<DateTime>(DateLastModifiedProperty, value); }
        }

        #endregion

        #region DateDeleted

        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);

        /// <summary>
        /// Gets/Sets the DateDeleted value
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
            set { SetProperty<DateTime?>(DateDeletedProperty, value); }
        }

        #endregion

        #region ChildList

        public static readonly ModelPropertyInfo<LocationGroupList> ChildListProperty =
        RegisterModelProperty<LocationGroupList>(c => c.ChildList);
        /// <summary>
        /// The list of child product groups
        /// </summary>
        public LocationGroupList ChildList
        {
            get
            {
                return GetProperty<LocationGroupList>(ChildListProperty);
            }
        }

        #endregion

        #endregion

        #region Helper Properties

        /// <summary>
        /// Returns true if this is the root group.
        /// </summary>
        /// <returns></returns>
        public Boolean IsRoot
        {
            get
            {
                return this.ParentGroup == null
                    && this.ParentHierarchy != null;
            }
        }


        public static readonly String GroupLabelPropertyName = "GroupLabel";

        /// <summary>
        /// Returns a string containing code : name
        /// </summary>
        public String GroupLabel
        {
            get
            {
                if (IsRoot)
                {
                    //don't include code for root.
                    return this.Name;
                }
                else
                {
                    return this.Code + " : " + this.Name;
                }
            }
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
            BusinessRules.AddRule(new Required(CodeProperty));
            BusinessRules.AddRule(new MaxLength(CodeProperty, 50));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationGroup), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationGroup), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(LocationGroup), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationGroup), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationHierarchyEdit.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static LocationGroup NewLocationGroup(Int32 levelId)
        {
            LocationGroup item = new LocationGroup();
            item.Create(levelId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(Int32 levelId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            this.LoadProperty<Int32>(LocationLevelIdProperty, levelId);
            this.LoadProperty<LocationGroupList>(ChildListProperty, LocationGroupList.NewLocationGroupList());
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// To String override
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Code + " : " + this.Name;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<LocationGroup>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes on this instance
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }

        /// <summary>
        /// Called when resolving ids on this instance
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // LocationLevelId
            Object locationLevelId = context.ResolveId<LocationLevel>(this.ReadProperty<Int32>(LocationLevelIdProperty));
            if (locationLevelId != null) this.LoadProperty<Int32>(LocationLevelIdProperty, (Int32)locationLevelId);
        }

        /// <summary>
        /// Enumerates through this group and  all child groups below
        /// </summary>
        /// <returns></returns>
        public IEnumerable<LocationGroup> EnumerateAllChildGroups()
        {
            yield return this;

            foreach (LocationGroup child in this.ChildList)
            {
                foreach (LocationGroup g in child.EnumerateAllChildGroups())
                {
                    yield return g;
                }
            }
        }

        /// <summary>
        /// Creates a list of groups representing a path down to the requested child
        /// </summary>
        /// <returns></returns>
        public List<LocationGroup> GetParentPath()
        {
            List<LocationGroup> inversePathList = new List<LocationGroup>();
            LocationGroup currentParent = this.ParentGroup;

            while (currentParent != null)
            {
                inversePathList.Add(currentParent);
                currentParent = currentParent.ParentGroup;
            }

            //invert the list so the top parent is the first item
            List<LocationGroup> pathList = new List<LocationGroup>();
            int currentIndex = inversePathList.Count - 1;
            while (currentIndex >= 0)
            {
                pathList.Add(inversePathList[currentIndex]);
                currentIndex--;
            }

            return pathList;
        }

        /// <summary>
        /// Gets the levle associated with this group
        /// </summary>
        public LocationLevel GetAssociatedLevel()
        {
            Int32 levelId = this.LocationLevelId;

            foreach (LocationLevel level in this.ParentHierarchy.EnumerateAllLevels())
            {
                if (levelId == level.Id)
                {
                    return level;
                }
            }

            return null;
        }

        /// <summary>
        /// Returns a list  of this group and  all child groups below
        /// </summary>
        /// <returns></returns>
        public IEnumerable<LocationGroup> FetchAllChildGroups()
        {
            IEnumerable<LocationGroup> returnList =
                new List<LocationGroup>() { this };

            foreach (LocationGroup child in this.ChildList)
            {
                returnList = returnList.Union<LocationGroup>(child.FetchAllChildGroups());
            }

            return returnList;
        }

        #endregion
    }

}