﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion
#region Version History : CCM811
// V8-30429  : J.Pickup
//  Introduced ParentId
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class EngineTaskParameterInfo : ModelReadOnlyObject<EngineTaskParameterInfo>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the property id
        /// </summary>
        public Int32 Id
        {
            get { return this.ReadProperty<Int32>(IdProperty); }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Returns the parameter name
        /// </summary>
        public String Name
        {
            get { return this.ReadProperty<String>(NameProperty); }
        }
        #endregion

        #region Description
        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);
        /// <summary>
        /// Returns the parameter description
        /// </summary>
        public String Description
        {
            get { return this.ReadProperty<String>(DescriptionProperty); }
        }
        #endregion

        #region Category
        /// <summary>
        /// Category property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CategoryProperty =
            RegisterModelProperty<String>(c => c.Category);
        /// <summary>
        /// Returns the parameter category
        /// </summary>
        public String Category
        {
            get { return this.GetProperty<String>(CategoryProperty); }
        }
        #endregion

        #region ParameterType
        /// <summary>
        /// PrimaryType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<TaskParameterType> ParameterTypeProperty =
            RegisterModelProperty<TaskParameterType>(c => c.ParameterType);
        /// <summary>
        /// Returns the parameters primary data type
        /// </summary>
        public TaskParameterType ParameterType
        {
            get { return this.ReadProperty<TaskParameterType>(ParameterTypeProperty); }
        }
        #endregion

        #region DefaultValue
        /// <summary>
        /// DefaultValue property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> DefaultValueProperty =
            RegisterModelProperty<Object>(c => c.DefaultValue);
        /// <summary>
        /// Returns the default value for the property
        /// </summary>
        public Object DefaultValue
        {
            get { return this.GetProperty<Object>(DefaultValueProperty); }
        }
        #endregion

        #region IsHidden
        /// <summary>
        /// IsHidden property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsHiddenByDefaultProperty =
            RegisterModelProperty<Boolean>(c => c.IsHiddenByDefault);
        /// <summary>
        /// Returns true if this parameter should be hidden on the
        /// bag grid
        /// </summary>
        public Boolean IsHiddenByDefault
        {
            get { return this.GetProperty<Boolean>(IsHiddenByDefaultProperty); }
        }
        #endregion

        #region IsReadOnly
        /// <summary>
        /// IsReadOnly property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsReadOnlyByDefaultProperty =
            RegisterModelProperty<Boolean>(c => c.IsReadOnlyByDefault);
        /// <summary>
        /// Returns true if this parameter should be readonly on the
        /// bag grid.
        /// </summary>
        public Boolean IsReadOnlyByDefault
        {
            get { return this.GetProperty<Boolean>(IsReadOnlyByDefaultProperty); }
        }
        #endregion

        #region EnumValues
        /// <summary>
        /// EnumValues property definition
        /// </summary>
        public static readonly ModelPropertyInfo<EngineTaskParameterEnumValueInfoList> EnumValuesProperty =
            RegisterModelProperty<EngineTaskParameterEnumValueInfoList>(c => c.EnumValues);
        /// <summary>
        /// Returns all the possible values for an enum type parameter
        /// </summary>
        public EngineTaskParameterEnumValueInfoList EnumValues
        {
            get { return this.ReadProperty<EngineTaskParameterEnumValueInfoList>(EnumValuesProperty); }
        }
        #endregion

        #region ParentId
        /// <summary>
        /// ParentId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> ParentIdProperty =
            RegisterModelProperty<Int32?>(c => c.ParentId);
        /// <summary>
        /// Returns the property parent id
        /// </summary>
        public Int32? ParentId
        {
            get { return this.ReadProperty<Int32?>(ParentIdProperty); }
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(EngineTaskParameterInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(EngineTaskParameterInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(EngineTaskParameterInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(EngineTaskParameterInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }   
}
