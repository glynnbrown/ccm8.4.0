﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : L.Luong
//   Created (Copied From GFS)
// V8-27404 : L.Luong
//   Changed LevelId to EntryType
#endregion

#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//	Extended for new WorkpackageName property
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing an Entity
    /// (Root object)
    /// </summary>
    [Serializable]
    public sealed partial class EventLogList : ModelList<EventLogList, EventLog>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(EventLogList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(EventLogList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.EventLogGet.ToString()));
            BusinessRules.AddRule(typeof(EventLogList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(EventLogList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        public static EventLogList NewList()
        {
            EventLogList item = new EventLogList();
            item.Create();
            return item;
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for LocationInfoList.FetchByEntityIdLocationCodes
        /// </summary>
        [Serializable]
        public class FetchByEventLogNameIdEntryTypeEntityIdCriteria : CriteriaBase<FetchByEventLogNameIdEntryTypeEntityIdCriteria>
        {
            #region Properties

            public static PropertyInfo<Int16> TopRowCountProperty =
                RegisterProperty<Int16>(c => c.TopRowCount);
            public Nullable<Int16> TopRowCount
            {
                get { return ReadProperty<Int16>(TopRowCountProperty); }
            }


            public static PropertyInfo<Nullable<Int32>> EventLogNameIdProperty =
                RegisterProperty<Nullable<Int32>>(c => c.EventLogNameId);
            public Nullable<Int32> EventLogNameId
            {
                get { return ReadProperty<Nullable<Int32>>(EventLogNameIdProperty); }
            }

            public static PropertyInfo<Nullable<Int16>> EntryTypeProperty =
                RegisterProperty<Nullable<Int16>>(c => c.EntryType);
            public Nullable<Int16> EntryType
            {
                get { return ReadProperty<Nullable<Int16>>(EntryTypeProperty); }
            }

            public static PropertyInfo<Nullable<Int32>> EntityIdProperty =
                RegisterProperty<Nullable<Int32>>(c => c.EntityId);
            public Nullable<Int32> EntityId
            {
                get { return ReadProperty<Nullable<Int32>>(EntityIdProperty); }
            }

            #endregion

            public FetchByEventLogNameIdEntryTypeEntityIdCriteria(Int16 topRowCount, Nullable<Int32> eventLogNameId, Nullable<Int16> EntryType, Nullable<Int32> entityId)
            {
                LoadProperty<Int16>(TopRowCountProperty, topRowCount);
                LoadProperty<Nullable<Int32>>(EventLogNameIdProperty, eventLogNameId);
                LoadProperty<Nullable<Int16>>(EntryTypeProperty, EntryType);
                LoadProperty<Nullable<Int32>>(EntityIdProperty, entityId);
            }
        }

        /// <summary>
        /// Criteria for EventLogList.FetchBySelectionOfCriteria
        /// </summary>
        [Serializable]
        public class FetchBySelectionOfCriteria : CriteriaBase<FetchBySelectionOfCriteria>
        {
            public static PropertyInfo<Int16> TopRowCountProperty =
               RegisterProperty<Int16>(c => c.TopRowCount);
            public Nullable<Int16> TopRowCount
            {
                get { return ReadProperty<Int16>(TopRowCountProperty); }
            }

            public static readonly PropertyInfo<Nullable<Int32>> EventLogNameIdProperty =
                RegisterProperty<Nullable<Int32>>(c => c.EventLogNameId);
            public Nullable<Int32> EventLogNameId
            {
                get { return ReadProperty<Nullable<Int32>>(EventLogNameIdProperty); }
            }

            public static readonly PropertyInfo<String> SourceProperty =
                RegisterProperty<String>(c => c.Source);
            public String Source
            {
                get { return ReadProperty<String>(SourceProperty); }
            }

            public static readonly PropertyInfo<Nullable<Int32>> EventIdProperty =
                RegisterProperty<Nullable<Int32>>(c => c.EventId);
            public Nullable<Int32> EventId
            {
                get { return ReadProperty<Nullable<Int32>>(EventIdProperty); }
            }

            public static readonly PropertyInfo<Int16?> EntryTypeProperty =
                RegisterProperty<Int16?>(c => c.EntryType);
            public Int16? EntryType
            {
                get { return ReadProperty<Int16?>(EntryTypeProperty); }
            }

            public static readonly PropertyInfo<Int32?> UserIdProperty =
                RegisterProperty<Int32?>(c => c.UserId);
            public Int32? UserId
            {
                get { return ReadProperty<Int32?>(UserIdProperty); }
            }

            public static readonly PropertyInfo<Int32?> EntityIdProperty =
                RegisterProperty<Int32?>(c => c.EntityId);
            public Int32? EntityId
            {
                get { return ReadProperty<Int32?>(EntityIdProperty); }
            }

            public static readonly PropertyInfo<DateTime?> DateTimeProperty =
                RegisterProperty<DateTime?>(c => c.DateTime);
            public DateTime? DateTime
            {
                get { return ReadProperty<DateTime?>(DateTimeProperty); }
            }

            public static readonly PropertyInfo<String> ProcessProperty =
                RegisterProperty<String>(c => c.Process);
            public String Process
            {
                get { return ReadProperty<String>(ProcessProperty); }
            }

            public static readonly PropertyInfo<String> ComputerNameProperty =
               RegisterProperty<String>(c => c.ComputerName);
            public String ComputerName
            {
                get { return ReadProperty<String>(ComputerNameProperty); }
            }

            public static readonly PropertyInfo<String> URLHelperLinkProperty =
                RegisterProperty<String>(c => c.URLHelperLink);
            public String URLHelperLink
            {
                get { return ReadProperty<String>(URLHelperLinkProperty); }
            }

            public static readonly PropertyInfo<String> DescriptionProperty =
               RegisterProperty<String>(c => c.Description);
            public String Description
            {
                get { return ReadProperty<String>(DescriptionProperty); }
            }

            public static readonly PropertyInfo<String> ContentProperty =
                RegisterProperty<String>(c => c.Content);
            public String Content
            {
                get { return ReadProperty<String>(ContentProperty); }
            }

            public static readonly PropertyInfo<String> GibraltarSessionIdProperty =
               RegisterProperty<String>(c => c.GibraltarSessionId);
            public String GibraltarSessionId
            {
                get { return ReadProperty<String>(GibraltarSessionIdProperty); }
            }

            public static readonly PropertyInfo<DateTime> StartDateProperty =
                RegisterProperty<DateTime>(c => c.StartDate);
            public DateTime StartDate
            {
                get { return ReadProperty<DateTime>(StartDateProperty); }
            }

            public static readonly PropertyInfo<DateTime> EndDateProperty =
                RegisterProperty<DateTime>(c => c.EndDate);
            public DateTime EndDate
            {
                get { return ReadProperty<DateTime>(EndDateProperty); }
            }

            public static readonly PropertyInfo<String> WorkpackageNameProperty =
               RegisterProperty<String>(c => c.WorkpackageName);
            public String WorkpackageName
            {
                get { return ReadProperty<String>(WorkpackageNameProperty); }
            }

            public static readonly PropertyInfo<Int32?> WorkpackageIdProperty =
               RegisterProperty<Int32?>(c => c.WorkpackageId);
            public Int32? WorkpackageId
            {
                get { return ReadProperty<Int32?>(WorkpackageIdProperty); }
            }

            public FetchBySelectionOfCriteria(Int16 topRowCount, Nullable<Int32> eventLogNameId, String source,
            Nullable<Int32> eventId, Nullable<Int16> entryType, Nullable<Int32> userId, Nullable<Int32> entityId, Nullable<DateTime> dateTime,
            String process, String computerName, String urlHelperLink, String description,
            String content, String gibraltarSessionId, DateTime startDate, DateTime endDate, String workpackageName, Int32? workpackageId)
            {
                LoadProperty<Int16>(TopRowCountProperty, topRowCount);
                LoadProperty<Nullable<Int32>>(EventLogNameIdProperty, eventLogNameId);
                LoadProperty<String>(SourceProperty, source);
                LoadProperty<Nullable<Int32>>(EventIdProperty, eventId);
                LoadProperty<Nullable<Int16>>(EntryTypeProperty, entryType);
                LoadProperty<Nullable<Int32>>(UserIdProperty, userId);
                LoadProperty<Nullable<Int32>>(EntityIdProperty, entityId); // GFS-16345
                LoadProperty<Nullable<DateTime>>(DateTimeProperty, dateTime);
                LoadProperty<String>(ProcessProperty, process);
                LoadProperty<String>(ComputerNameProperty, computerName);
                LoadProperty<String>(URLHelperLinkProperty, urlHelperLink);
                LoadProperty<String>(DescriptionProperty, description);
                LoadProperty<String>(ContentProperty, content);
                LoadProperty<String>(GibraltarSessionIdProperty, gibraltarSessionId);
                LoadProperty<DateTime>(StartDateProperty, startDate);
                LoadProperty<DateTime>(EndDateProperty, endDate);
                LoadProperty<String>(WorkpackageNameProperty, workpackageName);
                LoadProperty<Int32?>(WorkpackageIdProperty, workpackageId);
            }
        }

        /// <summary>
        /// Criteria for AmountOfRecordsToReturn
        /// </summary>
        [Serializable]
        public class FetchTopBySpecifiedAmountCriteria : Csla.CriteriaBase<FetchTopBySpecifiedAmountCriteria>
        {
            public static readonly PropertyInfo<Int32> AmountOfRecordsToReturnProperty =
            RegisterProperty<Int32>(c => c.AmountOfRecordsToReturn);
            public Int32 AmountOfRecordsToReturn
            {
                get { return ReadProperty<Int32>(AmountOfRecordsToReturnProperty); }
            }

            public FetchTopBySpecifiedAmountCriteria(Int32 amountOfRecordsToReturn)
            {
                LoadProperty<Int32>(AmountOfRecordsToReturnProperty, amountOfRecordsToReturn);
            }
        }

        /// <summary>
        /// Criteria for EventLogList.FetchByWorkpackageId
        /// </summary>
        [Serializable]
        public class FetchByWorkpackageIdCriteria : CriteriaBase<FetchByWorkpackageIdCriteria>
        {
            #region Properties

            public static PropertyInfo<Int32> WorkpackageIdProperty =
                RegisterProperty<Int32>(c => c.WorkpackageId);
            public Int32 WorkpackageId
            {
                get { return ReadProperty<Int32>(WorkpackageIdProperty); }
            }

            #endregion

            public FetchByWorkpackageIdCriteria(Int32 workpackageId)
            {
                LoadProperty<Int32>(WorkpackageIdProperty, workpackageId);
            }
        }


        #endregion
    }
}
