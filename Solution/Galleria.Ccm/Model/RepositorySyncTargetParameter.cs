﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using AutoMapper;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using Galleria.Framework2.Repository.Services.DataContracts;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model object representing a repository sync target parameter
    /// </summary>
    public partial class RepositorySyncTargetParameter : ModelObject<RepositorySyncTargetParameter>
    {
        #region Static Constructor
        static RepositorySyncTargetParameter()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the property id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Returns the parameter name
        /// </summary>
        public String Name
        {
            get { return this.GetProperty<String>(NameProperty); }
        }
        #endregion

        #region Value
        /// <summary>
        /// Value property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ValueProperty =
            RegisterModelProperty<String>(c => c.Value);
        /// <summary>
        /// Returns or sets the parameter value
        /// </summary>
        public String Value
        {
            get { return this.GetProperty<String>(ValueProperty); }
            set { this.SetProperty<String>(ValueProperty, value); }
        }
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            this.BusinessRules.AddRule(new Required(NameProperty));
            this.BusinessRules.AddRule(new MaxLength(NameProperty, 50));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(RepositorySyncTargetParameter), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(RepositorySyncTargetParameter), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(RepositorySyncTargetParameter), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(RepositorySyncTargetParameter), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Temporary.ToString()));
        }
        #endregion

        #region Mappings
        /// <summary>
        /// Initializes the mapper for this type
        /// </summary>
        private static void CreateMappings()
        {
            // model to data contract
            Mapper.CreateMap<RepositorySyncTargetParameter, RepositoryParameterDc>();

            // data contract to model
            Mapper.CreateMap<RepositoryParameterDc, RepositorySyncTargetParameter>();
        }
        #endregion
    }
}
