﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26159 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of PerformanceSelectionTimelineGroup objects.
    /// </summary>
    [Serializable]
    public sealed partial class PerformanceSelectionTimelineGroupList : ModelList<PerformanceSelectionTimelineGroupList, PerformanceSelectionTimelineGroup>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PerformanceSelectionTimelineGroupList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.PerformanceSelectionGet.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelectionTimelineGroupList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.PerformanceSelectionCreate.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelectionTimelineGroupList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.PerformanceSelectionEdit.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelectionTimelineGroupList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.PerformanceSelectionDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PerformanceSelectionTimelineGroupList NewPerformanceSelectionTimelineGroupList()
        {
            PerformanceSelectionTimelineGroupList item = new PerformanceSelectionTimelineGroupList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Creates a new item for the given code
        /// </summary>
        public PerformanceSelectionTimelineGroup Add(Int64 code)
        {
            PerformanceSelectionTimelineGroup item = this.Items.FirstOrDefault(t => t.Code == code);
            if (item == null)
            {
                item = PerformanceSelectionTimelineGroup.NewPerformanceSelectionTimelineGroup(code);
                this.Add(item);
            }
            return item;
        }
        #endregion
    }

}