#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.

#endregion

#endregion

using System.Linq;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public sealed partial class CustomColumnSortList
    {
        #region Constructor

        /// <summary>
        ///     Use the provided factory methods to create a new instance of <see cref="CustomColumnSortList"/>.
        /// </summary>
        private CustomColumnSortList()
        {
        }

        #endregion

        #region Data Access

        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using (var dalContext = GetDalFactory(criteria.DalFactoryName).CreateContext())
            using (var dal = dalContext.GetDal<ICustomColumnSortDal>())
            {
                dal.FetchByCustomColumnLayoutId(criteria.ParentId)
                    .ToList()
                    .ForEach(dto => Add(CustomColumnSort.FetchCustomColumnSort(dalContext, dto)));
            }

            MarkAsChild();
            RaiseListChangedEvents = true;
        }

        #endregion
    }
}