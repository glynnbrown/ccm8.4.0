﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8025632 : A.Kuszyk
//		Created (copied from SA).
// V8-25556 : J.Pickup
//		Added GFS Id property into dto Loading.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Csla.Core;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class ConsumerDecisionTreeNode
    {
        #region Constructors
        private ConsumerDecisionTreeNode() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <returns>An existing unit</returns>
        internal static ConsumerDecisionTreeNode FetchConsumerDecisionTreeNode(
            IDalContext dalContext, ConsumerDecisionTreeNodeDto dto,
            IEnumerable<ConsumerDecisionTreeNodeDto> masterNodeDtoList)
        {
            return DataPortal.FetchChild<ConsumerDecisionTreeNode>(dalContext, dto, masterNodeDtoList);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private ConsumerDecisionTreeNodeDto GetDataTransferObject()
        {
            return new ConsumerDecisionTreeNodeDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                GFSId = ReadProperty<Int32?>(GFSIdProperty),
                Name = ReadProperty<String>(NameProperty),
                ConsumerDecisionTreeLevelId = ReadProperty<Int32>(ConsumerDecisionTreeLevelIdProperty),
                ParentNodeId = (this.ParentNode != null) ? (Int32?)this.ParentNode.Id : null,
                ConsumerDecisionTreeId = this.ParentConsumerDecisionTree.Id
            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(
            IDalContext dalContext, ConsumerDecisionTreeNodeDto dto,
            IEnumerable<ConsumerDecisionTreeNodeDto> masterNodeDtoList)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Int32?>(GFSIdProperty, dto.GFSId);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<Int32>(ConsumerDecisionTreeLevelIdProperty, dto.ConsumerDecisionTreeLevelId);

            LoadProperty<ConsumerDecisionTreeNodeList>(ChildListProperty,
                ConsumerDecisionTreeNodeList.FetchListByParentNodeId(dalContext, dto.Id, masterNodeDtoList));

            LoadProperty<ConsumerDecisionTreeNodeProductList>(ProductsProperty,
                ConsumerDecisionTreeNodeProductList.FetchListByConsumerDecisionTreeNodeId(dalContext, dto.Id));
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning an existing item
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ConsumerDecisionTreeNodeDto dto, IEnumerable<ConsumerDecisionTreeNodeDto> masterNodeDtoList)
        {
            LoadDataTransferObject(dalContext, dto, masterNodeDtoList);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts a new object into the solution
        /// </summary>
        /// <param name="parent">The parent model</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext)
        {
            this.ResolveIds(dalContext);
            ConsumerDecisionTreeNodeDto dto = GetDataTransferObject();
            Int32 oldId = dto.Id;
            using (IConsumerDecisionTreeNodeDal dal = dalContext.GetDal<IConsumerDecisionTreeNodeDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<ConsumerDecisionTreeNode>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an object of this type
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent model</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            this.ResolveIds(dalContext);
            if (this.IsSelfDirty)
            {
                using (IConsumerDecisionTreeNodeDal dal = dalContext.GetDal<IConsumerDecisionTreeNodeDal>())
                {
                    dal.Update(this.GetDataTransferObject());
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IConsumerDecisionTreeNodeDal dal = dalContext.GetDal<IConsumerDecisionTreeNodeDal>())
            {
                dal.DeleteById(this.Id);
            }

            //GEM:14253
            //mark any child groups of this as deleted
            foreach (ConsumerDecisionTreeNode child in this.ChildList)
            {
                //forcibly mark the child as deleted
                ((IEditableBusinessObject)child).DeleteChild();
            }

            //make sure this updates its children so they get deleted if required.
            FieldManager.UpdateChildren(dalContext, this);

        }
        #endregion

        #endregion
    }
}
