﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-25664: A.Kuszyk
//		Created.
// V8-26240 : A.Silva ~ DataPortal_Fetch does nothing when the connection to the database cannot be established.

#endregion

#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class UserPlanogramGroupList
    {
        #region Constructor

        private UserPlanogramGroupList()
        {
        } // force use of factory methods

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Returns a UserPlanogramGroupList for the given User Id.
        /// </summary>
        /// <param name="userId">The </param>
        /// <returns></returns>
        public static UserPlanogramGroupList FetchByUserId(Int32 userId)
        {
            return DataPortal.Fetch<UserPlanogramGroupList>(new FetchByUserIdCriteria(userId));
        }

        /// <summary>
        ///     Returns a UserPlanogramGroupList for the current user.
        /// </summary>
        /// <returns></returns>
        public static UserPlanogramGroupList FetchForCurrentUser()
        {
            var userId = DomainPrincipal.CurrentUserId;
            if (userId != null)
            {
                return DataPortal.Fetch<UserPlanogramGroupList>(new FetchByUserIdCriteria((Int32) userId));
            }
            return null;
        }

        #endregion

        #region Data Access

        #region Fetch

        private void DataPortal_Fetch(FetchByUserIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            try
            {
                using (var dalContext = DalContainer.GetDalFactory().CreateContext())
                using (var dal = dalContext.GetDal<IUserPlanogramGroupDal>())
                {
                    foreach (var dto in dal.FetchByUserId(criteria.UserId))
                    {
                        Add(UserPlanogramGroup.GetUserPlanogramGroup(dalContext, dto));
                    }
                }
            }
            catch
            {
                // There is no connection or it was not set up.
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #region Update

        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                Child_Update(dalContext);
                dalContext.Commit();
            }
        }

        #endregion

        #endregion
    }
}