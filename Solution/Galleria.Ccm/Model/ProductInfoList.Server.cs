﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
// V8-25669 : A.Kuszyk
//  Implemented fetch methods in accordance with IProductInfoDal.
// CCM-25452 : N.Haywood
//  Added FetchByEntityId and removed fetch by search criteria
// V8-25453 : A.Kuszyk
//  Added FetchByEntityIdProductIds.
// CCM-25447 : N.Haywood
//  Added LocationProductIllegal
// CCM-26099 :I.George
//  Added FetchByMerchandisingGroupIdCriteria
#endregion
#region Version History: (CCm 8.3.0)
// V8-31835 : N.Haywood
//  Added Illegal and Legal MetaData and Validation
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class ProductInfoList
    {
        #region Constructors
        private ProductInfoList() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        public static ProductInfoList FetchByEntityIdSearchCriteria(Int32 entityId, String searchCriteria)
        {
            return DataPortal.Fetch<ProductInfoList>(new FetchByEntityIdSearchCriteriaCriteria(entityId,searchCriteria));
        }

        public static ProductInfoList FetchByEntityIdProductGtins(Int32 entityId, IEnumerable<String> productGtins)
        {
            return DataPortal.Fetch<ProductInfoList>(new FetchByEntityIdProductGtinsCriteria(entityId,productGtins));
        }

        public static ProductInfoList FetchByProductUniverseId(Int32 productUniverseId)
        {
            return DataPortal.Fetch<ProductInfoList>(new FetchByProductUniverseIdCriteria(productUniverseId));
        }

        public static ProductInfoList FetchByProductIds(IEnumerable<Int32> productIds)
        {
            return DataPortal.Fetch<ProductInfoList>(new FetchByProductIdsCriteria(productIds));
        }

        public static ProductInfoList FetchByMerchandisingGroupId(Int32 merchGroupId)
        {
            return DataPortal.Fetch<ProductInfoList>(new FetchByMerchandisingGroupIdCriteria(merchGroupId));
        }
        /// <summary>
        /// Return all product infos for the entity id and product ids
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="productIds"></param>
        /// <returns></returns>
        public static ProductInfoList FetchByEntityIdProductIds(Int32 entityId, IEnumerable<Int32> productIds)
        {
            return DataPortal.Fetch<ProductInfoList>(new FetchByEntityIdProductIdsCriteria(entityId, productIds));
        }

        /// <summary>
        /// Returns all product infos for the entity id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static ProductInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<ProductInfoList>(new FetchByEntityIdCriteria(entityId));
        }

        /// <summary>
        /// Returns all product infos for the specified
        /// entity including those marked as deleted
        /// </summary>
        public static ProductInfoList FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            return DataPortal.Fetch<ProductInfoList>(new FetchByEntityIdIncludingDeletedCriteria(entityId));
        }

        /// <summary>
        /// Returns all product infos
        /// that are illegal for a location and product group
        /// </summary>
        public static ProductInfoList FetchIllegalsByLocationCodeProductGroupCode(String locationCode, String productGroupCode)
        {
            return DataPortal.Fetch<ProductInfoList>(new FetchIllegalsByLocationCodeProductGroupCodeCriteria(locationCode, productGroupCode));
        }

        /// <summary>
        /// Returns all product infos
        /// that are Legal for a location and product group
        /// </summary>
        public static ProductInfoList FetchLegalsByLocationCodeProductGroupCode(String locationCode, String productGroupCode)
        {
            return DataPortal.Fetch<ProductInfoList>(new FetchLegalsByLocationCodeProductGroupCodeCriteria(locationCode, productGroupCode));
        }

        #endregion

        #region Data Access

        #region Fetch

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdSearchCriteriaCriteria criteria)
        {
            if (criteria.SearchCriteria == "")
            {
                return;
            }
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                {
                    IEnumerable<ProductInfoDto> dtoList = dal.FetchByEntityIdSearchCriteria(criteria.EntityId,criteria.SearchCriteria);

                    foreach (ProductInfoDto dto in dtoList)
                    {
                        this.Add(ProductInfo.GetProductInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdProductGtinsCriteria criteria)
        {
            //Don't bother doing anything if we have no gtins to fetch.
            if (!criteria.ProductGtins.Any()) return;

            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                {
                    IEnumerable<ProductInfoDto> dtoList = dal.FetchByEntityIdProductGtins(criteria.EntityId,criteria.ProductGtins);

                    foreach (ProductInfoDto dto in dtoList)
                    {
                        this.Add(ProductInfo.GetProductInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByProductUniverseIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                {
                    IEnumerable<ProductInfoDto> dtoList = dal.FetchByProductUniverseId(criteria.ProductUniverseId);

                    foreach (ProductInfoDto dto in dtoList)
                    {
                        this.Add(ProductInfo.GetProductInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByProductIdsCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                {
                    IEnumerable<ProductInfoDto> dtoList = dal.FetchByProductIds(criteria.ProductIds);

                    foreach (ProductInfoDto dto in dtoList)
                    {
                        this.Add(ProductInfo.GetProductInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when retrieving a list of all locations for an entity
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                {
                    IEnumerable<ProductInfoDto> dtoList = dal.FetchByEntityId(criteria.EntityId);

                    foreach (ProductInfoDto dto in dtoList)
                    {
                        this.Add(ProductInfo.GetProductInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when retrieving a list of all locations for an entity and product ids
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdProductIdsCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                {
                    IEnumerable<ProductInfoDto> dtoList = dal.FetchByEntityIdProductIds(
                        criteria.EntityId,
                        criteria.ProductIds);

                    foreach (ProductInfoDto dto in dtoList)
                    {
                        this.Add(ProductInfo.GetProductInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByMerchandisingGroupIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                {
                    IEnumerable<ProductInfoDto> dtoList = dal.FetchByMerchandisingGroupId(criteria.CategoryId);

                    foreach (ProductInfoDto dto in dtoList)
                    {
                        this.Add(ProductInfo.GetProductInfo(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when retrieving a list of all product infos
        /// for an entity, including those marked as deleted
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdIncludingDeletedCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                {
                    IEnumerable<ProductInfoDto> dtoList = dal.FetchByEntityIdIncludingDeleted(criteria.EntityId);
                    foreach (ProductInfoDto dto in dtoList)
                    {
                        this.Add(ProductInfo.GetProductInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when retrieving a list of all product infos
        /// that are illegal for a location and product group
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchIllegalsByLocationCodeProductGroupCodeCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                {
                    IEnumerable<ProductInfoDto> dtoList = dal.FetchIllegalsByLocationCodeProductGroupCode(criteria.LocationCode, criteria.ProductGroupCode);
                    foreach (ProductInfoDto dto in dtoList)
                    {
                        this.Add(ProductInfo.GetProductInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when retrieving a list of all product infos
        /// that are Legal for a location and product group
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchLegalsByLocationCodeProductGroupCodeCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                {
                    IEnumerable<ProductInfoDto> dtoList = dal.FetchLegalsByLocationCodeProductGroupCode(criteria.LocationCode, criteria.ProductGroupCode);
                    foreach (ProductInfoDto dto in dtoList)
                    {
                        this.Add(ProductInfo.GetProductInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}
