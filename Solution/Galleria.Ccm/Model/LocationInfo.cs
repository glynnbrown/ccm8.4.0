﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25628: A.Kuszyk
//		Created (Copied from SA)
#endregion
#region Version History: CCM811
// V8-30463 : L.Ineson
//  Added GetIdValue override
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model object representing a readonly cutdown of a location
    /// (child of LocationInfoList)
    /// </summary>
    [Serializable]
    public sealed partial class LocationInfo : ModelReadOnlyObject<LocationInfo>
    {
        #region Properties

        public static readonly ModelPropertyInfo<Int16> IdProperty =
            RegisterModelProperty<Int16>(c => c.Id);
        /// <summary>
        /// The unique object id
        /// </summary>
        public Int16 Id
        {
            get { return GetProperty<Int16>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        /// <summary>
        /// The id of the entity this belongs to
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> LocationGroupIdProperty =
            RegisterModelProperty<Int32>(c => c.LocationGroupId);
        /// <summary>
        /// The id of the group this location belongs to
        /// </summary>
        public Int32 LocationGroupId
        {
            get { return GetProperty<Int32>(LocationGroupIdProperty); }
        }

        /// <summary>
        /// The unique store code
        /// </summary>
        public static readonly ModelPropertyInfo<String> CodeProperty =
            RegisterModelProperty<String>(c => c.Code);
        public String Code
        {
            get { return GetProperty<String>(CodeProperty); }
        }

        /// <summary>
        /// Returns the store name
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        /// <summary>
        /// Returns the location region
        /// </summary>
        public static readonly ModelPropertyInfo<String> RegionProperty =
            RegisterModelProperty<String>(c => c.Region);
        public String Region
        {
            get { return GetProperty<String>(RegionProperty); }
        }

        /// <summary>
        /// Returns the location county
        /// </summary>
        public static readonly ModelPropertyInfo<String> CountyProperty =
            RegisterModelProperty<String>(c => c.County);
        public String County
        {
            get { return GetProperty<String>(CountyProperty); }
        }

        /// <summary>
        /// Returns the location tv region
        /// </summary>
        public static readonly ModelPropertyInfo<String> TVRegionProperty =
            RegisterModelProperty<String>(c => c.TVRegion);
        public String TVRegion
        {
            get { return GetProperty<String>(TVRegionProperty); }
        }

        /// <summary>
        /// Returns the location address 1
        /// </summary>
        public static readonly ModelPropertyInfo<String> Address1Property =
            RegisterModelProperty<String>(c => c.Address1);
        public String Address1
        {
            get { return GetProperty<String>(Address1Property); }
        }

        /// <summary>
        /// Returns the location address 2
        /// </summary>
        public static readonly ModelPropertyInfo<String> Address2Property =
            RegisterModelProperty<String>(c => c.Address2);
        public String Address2
        {
            get { return GetProperty<String>(Address2Property); }
        }

        /// <summary>
        /// Returns the location PostalCode
        /// </summary>
        public static readonly ModelPropertyInfo<String> PostalCodeProperty =
            RegisterModelProperty<String>(c => c.PostalCode);
        public String PostalCode
        {
            get { return GetProperty<String>(PostalCodeProperty); }
        }

        /// <summary>
        /// Returns the city
        /// </summary>
        public static readonly ModelPropertyInfo<String> CityProperty =
            RegisterModelProperty<String>(c => c.City);
        public String City
        {
            get { return GetProperty<String>(CityProperty); }
        }

        /// <summary>
        /// Returns the Country
        /// </summary>
        public static readonly ModelPropertyInfo<String> CountryProperty =
            RegisterModelProperty<String>(c => c.Country);
        public String Country
        {
            get { return GetProperty<String>(CountryProperty); }
        }

        /// <summary>
        /// The date when the item record was deleted 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(LocationInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(LocationInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(LocationInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Code + ' ' + this.Name;
        }

        protected override object GetIdValue()
        {
            return this.Id;
        }

        #endregion
    }
}
