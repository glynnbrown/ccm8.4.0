﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion


using System;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// PrintTemplateComponent Model object
    /// </summary>
    [Serializable]
    [DefaultNewMethod("NewPrintTemplateComponent", "Type")]
    public sealed partial class PrintTemplateComponent : ModelObject<PrintTemplateComponent>
    {
        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PrintTemplateSection Parent
        {
            get
            {
                PrintTemplateComponentList parentList = base.Parent as PrintTemplateComponentList;
                if (parentList != null)
                {
                    return parentList.Parent as PrintTemplateSection;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region Type

        /// <summary>
        /// Type property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PrintTemplateComponentType> TypeProperty =
            RegisterModelProperty<PrintTemplateComponentType>(c => c.Type);

        /// <summary>
        /// Gets/Sets the Type value
        /// </summary>
        public PrintTemplateComponentType Type
        {
            get { return GetProperty<PrintTemplateComponentType>(TypeProperty); }
            set { SetProperty<PrintTemplateComponentType>(TypeProperty, value); }
        }

        #endregion

        #region X

        /// <summary>
        /// X property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> XProperty =
            RegisterModelProperty<Single>(c => c.X);

        /// <summary>
        /// Gets/Sets the X value
        /// </summary>
        public Single X
        {
            get { return GetProperty<Single>(XProperty); }
            set { SetProperty<Single>(XProperty, value); }
        }

        #endregion

        #region Y

        /// <summary>
        /// Y property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> YProperty =
            RegisterModelProperty<Single>(c => c.Y);

        /// <summary>
        /// Gets/Sets the Y value
        /// </summary>
        public Single Y
        {
            get { return GetProperty<Single>(YProperty); }
            set { SetProperty<Single>(YProperty, value); }
        }

        #endregion

        #region Height

        /// <summary>
        /// Height property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HeightProperty =
            RegisterModelProperty<Single>(c => c.Height);

        /// <summary>
        /// Gets/Sets the Height value
        /// </summary>
        public Single Height
        {
            get { return GetProperty<Single>(HeightProperty); }
            set { SetProperty<Single>(HeightProperty, value); }
        }

        #endregion

        #region Width

        /// <summary>
        /// Width property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> WidthProperty =
            RegisterModelProperty<Single>(c => c.Width);

        /// <summary>
        /// Gets/Sets the Width value
        /// </summary>
        public Single Width
        {
            get { return GetProperty<Single>(WidthProperty); }
            set { SetProperty<Single>(WidthProperty, value); }
        }

        #endregion

        #region Component Details
        /// <summary>
        /// ComponentDetails property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PrintTemplateComponentDetail> ComponentDetailsProperty =
           RegisterModelProperty<PrintTemplateComponentDetail>(c => c.ComponentDetails);
        /// <summary>
        /// Gets the component Details of the print option component
        /// </summary>
        public PrintTemplateComponentDetail ComponentDetails
        {
            get { return GetProperty<PrintTemplateComponentDetail>(ComponentDetailsProperty); }
        }
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static PrintTemplateComponent NewPrintTemplateComponent(PrintTemplateComponentType componentType)
        {
            PrintTemplateComponent item = new PrintTemplateComponent();
            item.Create(componentType);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(PrintTemplateComponentType componentType)
        {
            LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty<PrintTemplateComponentType>(TypeProperty, componentType);

            LoadProperty<PrintTemplateComponentDetail>(ComponentDetailsProperty,
                PrintTemplateComponentDetail.NewPrintTemplateComponentDetail(componentType));

            switch (componentType)
            {
                default:
                    LoadProperty<Single>(HeightProperty, 50);
                    LoadProperty<Single>(WidthProperty, 50);
                    break;

                case PrintTemplateComponentType.TextBox:
                    LoadProperty<Single>(HeightProperty, 10);
                    LoadProperty<Single>(WidthProperty, 50);
                    break;

                case PrintTemplateComponentType.Planogram:
                    LoadProperty<Single>(HeightProperty, 150);
                    LoadProperty<Single>(WidthProperty, 175);
                    break;

                case PrintTemplateComponentType.DataSheet:
                    LoadProperty<Single>(HeightProperty, 150);
                    LoadProperty<Single>(WidthProperty, 175);
                    break;

                case PrintTemplateComponentType.Line:
                    break;
            }



            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<PrintTemplateComponent>(oldId, newId);
        }

        #endregion
    }
}