﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Csla;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class LocationSpaceBayList
    {
        #region Constructors
        private LocationSpaceBayList() { } // force use of factory methods
        #endregion

        #region Methods
        /// <summary>
        /// Called when adding a new object
        /// through data binding
        /// </summary>
        /// <returns>A new location space bay object</returns>
        protected override LocationSpaceBay AddNewCore()
        {
            LocationSpaceBay locationSpaceBay = LocationSpaceBay.NewLocationSpaceBay();
            Add(locationSpaceBay);
            return locationSpaceBay;
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns all location space Bays in the solution for a location space product group
        /// </summary>
        /// <returns>A list of all location space Bays</returns>
        internal static LocationSpaceBayList FetchByLocationSpaceProductGroupId(IDalContext dalContext, Int32 locationSpaceProductGroupId)
        {
            return DataPortal.FetchChild<LocationSpaceBayList>(dalContext, new FetchByLocationSpaceProductGroupIdCriteria(locationSpaceProductGroupId));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="productUniverseId">The parent product universe id id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FetchByLocationSpaceProductGroupIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            using (ILocationSpaceBayDal dal = dalContext.GetDal<ILocationSpaceBayDal>())
            {
                IEnumerable<LocationSpaceBayDto> dtoList = dal.FetchByLocationSpaceProductGroupId(criteria.LocationSpaceProductGroupId);
                foreach (LocationSpaceBayDto dto in dtoList)
                {
                    this.Add(LocationSpaceBay.GetLocationSpaceBay(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }



        #endregion

        #endregion

    }
}
