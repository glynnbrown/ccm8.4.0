﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26179  : I.George
//  Created
// V8-28149 : M.Pettit
//  Ensured all types have friendly names for reporting
#endregion

#region Version History: CCM801
// V8-28668 : L.Luong
//  Added HKD.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Denotes the available currency unit of measure types
    /// </summary>
    public enum UnitOfMeasureCurrencyType
    {
        Unknown = 0,
        GBP = 1,
        USD = 2,
        HKD = 3
    }

    /// <summary>
    /// UnitOfMeasureCurrencyType Helper Class
    /// </summary>
    public static class UnitOfMeasureCurrencyTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<UnitOfMeasureCurrencyType, String> FriendlyNames =
            new Dictionary<UnitOfMeasureCurrencyType, String>()
            {
                {UnitOfMeasureCurrencyType.Unknown, String.Empty},
                {UnitOfMeasureCurrencyType.GBP, Message.Enum_UnitOfMeasureCurrencyType_GBP},
                {UnitOfMeasureCurrencyType.USD, Message.Enum_UnitOfMeasureCurrencyType_USD},
                {UnitOfMeasureCurrencyType.HKD, Message.Enum_UnitOfMeasureCurrencyType_HKD},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<UnitOfMeasureCurrencyType, String> Abbreviations =
            new Dictionary<UnitOfMeasureCurrencyType, String>()
            {
                {UnitOfMeasureCurrencyType.Unknown, String.Empty},
                {UnitOfMeasureCurrencyType.GBP, Message.Enum_UnitOfMeasureCurrencyType_GBP_Abbrev},
                {UnitOfMeasureCurrencyType.USD, Message.Enum_UnitOfMeasureCurrencyType_USD_Abbrev},
                {UnitOfMeasureCurrencyType.HKD, Message.Enum_UnitOfMeasureCurrencyType_HKD_Abbrev},
            };
    }
}
