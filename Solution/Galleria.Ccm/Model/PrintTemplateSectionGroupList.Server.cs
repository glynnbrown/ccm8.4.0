﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public sealed partial class PrintTemplateSectionGroupList
    {
        #region Constructor
        private PrintTemplateSectionGroupList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Fetches the list by its parent id
        /// </summary>
        internal static PrintTemplateSectionGroupList FetchByParentId(IDalContext dalContext, Object parentId)
        {
            return DataPortal.FetchChild<PrintTemplateSectionGroupList>(dalContext, parentId);
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returing all items for a given parent id
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Object parentId)
        {
            RaiseListChangedEvents = false;
            using (IPrintTemplateSectionGroupDal dal = dalContext.GetDal<IPrintTemplateSectionGroupDal>())
            {
                IEnumerable<PrintTemplateSectionGroupDto> dtoList = dal.FetchByPrintTemplateId(parentId);
                foreach (PrintTemplateSectionGroupDto dto in dtoList)
                {
                    this.Add(PrintTemplateSectionGroup.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}