#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.

#endregion

#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Server side implementation of <see cref="CustomColumnLayoutInfo"/>.
    /// </summary>
    public sealed partial class CustomColumnLayoutInfo
    {
        #region Constructor

        /// <summary>
        ///     Use factory methods to obtain instances of <see cref="CustomColumnLayoutInfo"/> objects.
        /// </summary>
        private CustomColumnLayoutInfo()
        {
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Returns an existing <see cref="CustomColumnLayoutInfo"/> using the provided <paramref name="dalContext"/> and <paramref name="dto"/>.
        /// </summary>
        /// <param name="dalContext">DAL context to use when retrieving the instance.</param>
        /// <param name="dto"><see cref="CustomColumnLayoutInfoDto"/> to load from.</param>
        /// <returns>A new instance of <see cref="CustomColumnLayoutInfo"/>.</returns>
        internal static CustomColumnLayoutInfo GetCustomColumnLayoutInfo(IDalContext dalContext, CustomColumnLayoutInfoDto dto)
        {
            return DataPortal.FetchChild<CustomColumnLayoutInfo>(dalContext, dto);
        }

        internal static CustomColumnLayoutInfo GetCustomColumnLayoutInfo(IDalContext dalContext, CustomColumnLayoutInfoDto dto,
            String fileName)
        {
            return DataPortal.FetchChild<CustomColumnLayoutInfo>(dalContext, dto, fileName);
        }

        #endregion

        #region Data Access

        #region Data Transfer

        /// <summary>
        ///     Loads this instance's data from a dto.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">The <see cref="CustomColumnLayoutInfoDto"/> containing the data for this instance.</param>
        private void LoadDataTransferObject(IDalContext dalContext, CustomColumnLayoutInfoDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(TypeProperty,dto.Type);
            LoadProperty(NameProperty, dto.Name);
        }

        /// <summary>
        ///     Loads this instance's data from a dto.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">The <see cref="CustomColumnLayoutInfoDto"/> containing the data for this instance.</param>
        /// <param name="fileName">The filename where the data was retrieved from.</param>
        private void LoadDataTransferObject(IDalContext dalContext, CustomColumnLayoutInfoDto dto, String fileName)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(TypeProperty, dto.Type);
            LoadProperty(NameProperty, dto.Name);
            LoadProperty(FileNameProperty, fileName);
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Loads this instance from a dto.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">The <see cref="CustomColumnLayoutInfoDto"/> containing the data for this instance.</param>
        private void Child_Fetch(IDalContext dalContext, CustomColumnLayoutInfoDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        /// <summary>
        ///     Loads this instance from a dto.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">The <see cref="CustomColumnLayoutInfoDto"/> containing the data for this instance.</param>
        /// <param name="fileName">The filename where the data was retrieved from.</param>
        private void Child_Fetch(IDalContext dalContext, CustomColumnLayoutInfoDto dto, String fileName)
        {
            LoadDataTransferObject(dalContext, dto, fileName);
        }

        #endregion

        #endregion
    }
}