﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25664: A.Kuszyk
//		Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    public partial class UserPlanogramGroup
    {
        #region Constructor
        private UserPlanogramGroup() { }
        #endregion

        #region Factory Methods

        internal static UserPlanogramGroup GetUserPlanogramGroup(IDalContext dalContext, UserPlanogramGroupDto dto)
        {
            return DataPortal.FetchChild<UserPlanogramGroup>(dalContext, dto);
        }

        #endregion

        #region Data Access
        
        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, UserPlanogramGroupDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32>(UserIdProperty, dto.UserId);
            this.LoadProperty<Int32>(PlanogramGroupIdProperty, dto.PlanogramGroupId);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private UserPlanogramGroupDto GetDataTransferObject()
        {
            return new UserPlanogramGroupDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                UserId = ReadProperty<Int32>(UserIdProperty),
                PlanogramGroupId = ReadProperty<Int32>(PlanogramGroupIdProperty),
            };
        }

        #endregion 
        
        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, UserPlanogramGroupDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected void Child_Insert(IDalContext dalContext)
        {
            UserPlanogramGroupDto dto = GetDataTransferObject();
            Int32 oldId = dto.Id;
            using (IUserPlanogramGroupDal dal = dalContext.GetDal<IUserPlanogramGroupDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<UserPlanogramGroup>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this instance
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected void Child_Update(IDalContext dalContext)
        {
            if (this.IsSelfDirty)
            {
                using (IUserPlanogramGroupDal dal = dalContext.GetDal<IUserPlanogramGroupDal>())
                {
                    dal.Update(this.GetDataTransferObject());
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when this instance is being deleted
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IUserPlanogramGroupDal dal = dalContext.GetDal<IUserPlanogramGroupDal>())
            {
                dal.DeleteById(this.Id);
            }
        }

        #endregion

        #endregion
    }
}
