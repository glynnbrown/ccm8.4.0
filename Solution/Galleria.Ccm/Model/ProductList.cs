﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25452 : N.Haywood
//	Added from SA
// V8-25453 : A.Kuszyk
//  Added FetchByEntityIdProductIdsCriteria
#endregion
#region Version History: (CCM 8.3.0)
// V8-32356 : N.Haywood
//  Added FetchByEntityIdMultipleSearchText
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a list containing Product objects
    /// (root object or child of product group)
    /// </summary>
    [Serializable]
    public sealed partial class ProductList : ModelList<ProductList, Product>
    {
        #region Parent

        /// <summary>
        /// Gets the parent product group of this list
        /// </summary>
        public ProductGroup ParentProductGroup
        {
            get { return base.Parent as ProductGroup; }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ProductList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ProductCreate.ToString()));
            BusinessRules.AddRule(typeof(ProductList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ProductGet.ToString()));
            BusinessRules.AddRule(typeof(ProductList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ProductEdit.ToString()));
            BusinessRules.AddRule(typeof(ProductList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ProductDelete.ToString()));
        }
        #endregion

        #region Criteria

        #region FetchByProductGroupIdCriteria
        /// TODO:- Find out where this is used as we don't have financial hierarchy in CCM8
        /// <summary>
        /// Criteria for ProductList.FetchByProductGroupId
        /// </summary>
        [Serializable]
        internal class FetchByProductGroupIdCriteria : CriteriaBase<FetchByProductGroupIdCriteria>
        {
            #region Properties

            #region ProductGroupId
            public static PropertyInfo<Int32> ProductGroupIdProperty =
                RegisterProperty<Int32>(c => c.ProductGroupId);
            public Int32 ProductGroupId
            {
                get { return ReadProperty<Int32>(ProductGroupIdProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByProductGroupIdCriteria(Int32 productGroupId)
            {
                LoadProperty<Int32>(ProductGroupIdProperty, productGroupId);
            }
            #endregion
        }
        #endregion

        #region FetchByProductUniverseIdCriteria
        /// <summary>
        /// Criteria for ProductList.FetchByProductUniverseId
        /// </summary>
        [Serializable]
        public class FetchByProductUniverseIdCriteria : CriteriaBase<FetchByProductUniverseIdCriteria>
        {
            #region Properties

            #region ProductUniverseId
            public static PropertyInfo<Int32> ProductUniverseIdProperty =
                RegisterProperty<Int32>(c => c.ProductUniverseId);
            public Int32 ProductUniverseId
            {
                get { return ReadProperty<Int32>(ProductUniverseIdProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByProductUniverseIdCriteria(Int32 productUniverseId)
            {
                LoadProperty<Int32>(ProductUniverseIdProperty, productUniverseId);
            }
            #endregion
        }
        #endregion

        #region FetchByEntityIdSearchTextCriteria
        /// <summary>
        /// Criteria for use with ProductList.FetchByEntityIdSearchText
        /// </summary>
        [Serializable]
        public class FetchByEntityIdSearchTextCriteria : CriteriaBase<FetchByEntityIdSearchTextCriteria>
        {
            public static PropertyInfo<Int32> EntityIdProperty = RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static PropertyInfo<String> SearchTextProperty = RegisterProperty<String>(c => c.SearchText);
            public String SearchText
            {
                get { return ReadProperty<String>(SearchTextProperty); }
            }

            public FetchByEntityIdSearchTextCriteria(Int32 entityId, String searchCriteria)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<String>(SearchTextProperty, searchCriteria);
            }
        }
        #endregion

        #region FetchByMerchandisingGroupIdCriteria
        /// <summary>
        /// Criteria for ProductList.FetchByCategoryId
        /// </summary>
        [Serializable]
        public class FetchByMerchandisingGroupIdCriteria : CriteriaBase<FetchByMerchandisingGroupIdCriteria>
        {
            #region Properties

            #region ProductGroupId
            public static PropertyInfo<Int32> CategoryIdProperty =
                RegisterProperty<Int32>(c => c.CategoryId);
            public Int32 CategoryId
            {
                get { return ReadProperty<Int32>(CategoryIdProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByMerchandisingGroupIdCriteria(Int32 categoryId)
            {
                LoadProperty<Int32>(CategoryIdProperty, categoryId);
            }
            #endregion
        }
        #endregion
        
        #region FetchByEntityIdCriteria
        /// <summary>
        /// Criteria for ProductList.FetchByProductGroupId
        /// </summary>
        [Serializable]
        internal class FetchByEntityIdCriteria : CriteriaBase<FetchByEntityIdCriteria>
        {
            #region Properties

            #region EntityId
            public static PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }
            #endregion

            #region FetchProductAttributesProperty
            public static PropertyInfo<Boolean> FetchProductAttributesProperty =
                RegisterProperty<Boolean>(c => c.FetchProductAttributes);
            public Boolean FetchProductAttributes
            {
                get { return ReadProperty<Boolean>(FetchProductAttributesProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByEntityIdCriteria(Int32 entityId, Boolean fetchProductAttributes)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<Boolean>(FetchProductAttributesProperty, fetchProductAttributes);
            }
            #endregion
        }
        #endregion

        #region FetchByProductIdsCriteria
        /// <summary>
        /// Criteria for ProductList.FetchByEntityIdProductIds
        /// </summary>
        [Serializable]
        public class FetchByProductIdsCriteria : CriteriaBase<FetchByProductIdsCriteria>
        {
            #region Properties

            #region ProductIds
            public static PropertyInfo<IEnumerable<Int32>> ProductIdsProperty =
                RegisterProperty<IEnumerable<Int32>>(c => c.ProductIds);
            public IEnumerable<Int32> ProductIds
            {
                get { return ReadProperty<IEnumerable<Int32>>(ProductIdsProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByProductIdsCriteria(IEnumerable<Int32> productIds)
            {
                this.LoadProperty<IEnumerable<Int32>>(ProductIdsProperty, productIds);
            }
            #endregion
        }
        #endregion

        #region FetchByEntityIdMultpleSearchTextCriteria
        /// <summary>
        /// Criteria for use with ProductList.FetchByEntityIdMultipleSearchText
        /// </summary>
        [Serializable]
        public class FetchByEntityIdMultipleSearchTextCriteria : CriteriaBase<FetchByEntityIdMultipleSearchTextCriteria>
        {
            public static PropertyInfo<Int32> EntityIdProperty = RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static PropertyInfo<IEnumerable<String>> MultipleSearchTextProperty = RegisterProperty<IEnumerable<String>>(c => c.MultipleSearchText);
            public IEnumerable<String> MultipleSearchText
            {
                get { return ReadProperty<IEnumerable<String>>(MultipleSearchTextProperty); }
            }

            public FetchByEntityIdMultipleSearchTextCriteria(Int32 entityId, IEnumerable<String> searchCriteria)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<IEnumerable<String>>(MultipleSearchTextProperty, searchCriteria);
            }
        }
        #endregion

        #region FetchByEntityIdProductGtinsCriteria
        /// <summary>
        /// Criteria for ProductList.FetchByEntityIdProductGtinsCriteria
        /// </summary>
        [Serializable]
        public class FetchByEntityIdProductGtinsCriteria : CriteriaBase<FetchByEntityIdProductGtinsCriteria>
        {
            #region Properties

            #region EntityId
            /// <summary>
            /// EntityId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            /// <summary>
            /// Returns the entity id
            /// </summary>
            public Int32 EntityId
            {
                get { return this.ReadProperty<Int32>(EntityIdProperty); }
            }
            #endregion

            #region ProductGtins
            /// <summary>
            /// ProductGtins property definition
            /// </summary>
            public static PropertyInfo<IEnumerable<String>> ProductGtinsProperty =
                RegisterProperty<IEnumerable<String>>(c => c.ProductGtins);
            /// <summary>
            /// Returns the product gtins
            /// </summary>
            public IEnumerable<String> ProductGtins
            {
                get { return ReadProperty<IEnumerable<String>>(ProductGtinsProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByEntityIdProductGtinsCriteria(Int32 entityId, IEnumerable<String> productGtins)
            {
                this.LoadProperty<Int32>(EntityIdProperty, entityId);
                this.LoadProperty<IEnumerable<String>>(ProductGtinsProperty, productGtins);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <returns></returns>
        public static ProductList NewProductList()
        {
            ProductList item = new ProductList();
            item.Create();
            return item;
        }

        #endregion
    }
}

