﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26234 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a list of role entities
    /// </summary>
    [Serializable]
    public sealed partial class RoleEntityList : ModelList<RoleEntityList, RoleEntity>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(RoleEntityList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.RoleCreate.ToString()));
            BusinessRules.AddRule(typeof(RoleEntityList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.RoleGet.ToString()));
            BusinessRules.AddRule(typeof(RoleEntityList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.RoleEdit.ToString()));
            BusinessRules.AddRule(typeof(RoleEntityList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.RoleDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>a new list</returns>
        public static RoleEntityList NewList()
        {
            RoleEntityList list = new RoleEntityList();
            list.Create();
            return list;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            EntityInfoList entities = EntityInfoList.FetchAllEntityInfos(true);
            foreach (EntityInfo entity in entities)
            {
                this.Add(RoleEntity.NewRoleEntity(entity.Id));
            }
            base.Create();
        }
        #endregion

        #endregion
    }
}
