﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Csla;
using Csla.Data;
using Csla.Security;
using Csla.Serialization;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Cluster server model object
    /// </summary>
    public partial class ClusterLocation
    {
        #region Constructors
        private ClusterLocation() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the specified cluster object
        /// </summary>
        internal static ClusterLocation FetchClusterLocation(IDalContext dalContext, ClusterLocationDto dto)
        {
            return DataPortal.FetchChild<ClusterLocation>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Returns a dto for this object
        /// </summary>
        /// <returns>A new dto</returns>
        private ClusterLocationDto GetDataTransferObject(Cluster parent)
        {
            return new ClusterLocationDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                ClusterId = parent.Id,
                LocationId = ReadProperty<Int16>(LocationIdProperty),
                Address1 = ReadProperty<String>(Address1Property),
                Address2 = ReadProperty<String>(Address2Property),
                City = ReadProperty<String>(CityProperty),
                Code = ReadProperty<String>(CodeProperty),
                Country = ReadProperty<String>(CountryProperty),
                County = ReadProperty<String>(CountyProperty),
                Name = ReadProperty<String>(NameProperty),
                PostalCode = ReadProperty<String>(PostalCodeProperty),
                Region = ReadProperty<String>(RegionProperty),
                TVRegion = ReadProperty<String>(TVRegionProperty)
            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, ClusterLocationDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Int16>(LocationIdProperty, dto.LocationId);
            LoadProperty<String>(Address1Property, dto.Address1);
            LoadProperty<String>(Address2Property, dto.Address2);
            LoadProperty<String>(CityProperty, dto.City);
            LoadProperty<String>(CodeProperty, dto.Code);
            LoadProperty<String>(CountryProperty, dto.Country);
            LoadProperty<String>(CountyProperty, dto.County);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<String>(PostalCodeProperty, dto.PostalCode);
            LoadProperty<String>(RegionProperty, dto.Region);
            LoadProperty<String>(TVRegionProperty, dto.TVRegion);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when loading this instance
        /// from a data reader as part of a
        /// cluster list
        /// </summary>
        /// <param name="criteria">an object used to identify the desired review</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ClusterLocationDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when persisting this instance in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, Cluster parent)
        {
            ClusterLocationDto dto = GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IClusterLocationDal dal = dalContext.GetDal<IClusterLocationDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<ClusterLocation>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an existing instance
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, Cluster parent)
        {
            using (IClusterLocationDal dal = dalContext.GetDal<IClusterLocationDal>())
            {
                ClusterLocationDto dto = GetDataTransferObject(parent);
                dal.Update(dto);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IClusterLocationDal dal = dalContext.GetDal<IClusterLocationDal>())
            {
                dal.DeleteById(this.Id);
            }
        }

        #endregion

        #endregion
    }
}
