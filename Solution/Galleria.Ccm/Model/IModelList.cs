﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24801 : L.Hodson
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a model list within this assembly
    /// </summary>
    internal interface IModelList
    {
        #region Properties
        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        String DalFactoryName { get; }
        #endregion
    }
}
