﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a synchronisation target performance list
    /// </summary>
    [Serializable]
    public partial class SyncTargetGfsPerformanceList : ModelList<SyncTargetGfsPerformanceList, SyncTargetGfsPerformance>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent Sync Target
        /// </summary>
        public new SyncTarget Parent
        {
            get { return (SyncTarget)base.Parent; }
        }
        #endregion

        #region Authorization Rules
        //this object is accessed without authentication and so cannot have rules
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new sync target performances list
        /// </summary>
        /// <returns>A new synch target performance list</returns>
        internal static SyncTargetGfsPerformanceList NewSyncTargetGfsPerformanceList()
        {
            SyncTargetGfsPerformanceList syncTargetGfsPerformanceList = new SyncTargetGfsPerformanceList();
            syncTargetGfsPerformanceList.Create();
            return syncTargetGfsPerformanceList;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}