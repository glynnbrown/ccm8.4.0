﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Csla.Core;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    public partial class LocationGroup
    {
        #region Constructor
        private LocationGroup() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static LocationGroup Fetch(IDalContext dalContext, LocationGroupDto dto, IEnumerable<LocationGroupDto> productGroupDtoList)
        {
            return DataPortal.FetchChild<LocationGroup>(dalContext, dto, productGroupDtoList);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, LocationGroupDto dto, IEnumerable<LocationGroupDto> productGroupDtoList)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32>(LocationLevelIdProperty, dto.LocationLevelId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(CodeProperty, dto.Code);
            this.LoadProperty<Int32>(AssignedLocationsCountProperty, dto.AssignedLocationsCount);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);

            LoadProperty<LocationGroupList>(ChildListProperty,
                LocationGroupList.FetchByParentLocationGroupId(dalContext, dto.Id, productGroupDtoList));
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private LocationGroupDto GetDataTransferObject()
        {
            return new LocationGroupDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                LocationHierarchyId = this.ParentHierarchy.Id,
                LocationLevelId = ReadProperty<Int32>(LocationLevelIdProperty),
                Name = ReadProperty<String>(NameProperty),
                Code = ReadProperty<String>(CodeProperty),
                AssignedLocationsCount = this.AssignedLocationsCount,
                ParentGroupId = (this.ParentGroup != null) ? (Int32?)this.ParentGroup.Id : null,
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, LocationGroupDto dto, IEnumerable<LocationGroupDto> productGroupDtoList)
        {
            this.LoadDataTransferObject(dalContext, dto, productGroupDtoList);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext)
        {
            this.ResolveIds(dalContext);
            LocationGroupDto dto = this.GetDataTransferObject();
            Int32 oldId = dto.Id;
            using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            dalContext.RegisterId<LocationGroup>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            this.ResolveIds(dalContext);
            if (this.IsSelfDirty)
            {
                LocationGroupDto dto = this.GetDataTransferObject();
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
            {
                dal.DeleteById(this.Id);
            }

            //GEM:14253
            //mark any child groups of this as deleted
            foreach (LocationGroup child in this.ChildList)
            {
                //forcibly mark the child as deleted
                ((IEditableBusinessObject)child).DeleteChild();
            }

            //make sure this updates its children so they get deleted if required.
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #endregion

    }
}