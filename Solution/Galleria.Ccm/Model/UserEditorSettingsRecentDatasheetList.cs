﻿#region Header Information 
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31699 : A.Heathcote
//  Created.
#endregion 
#endregion 
using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using System.Linq;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// List of user setting RecentHighlight 
    /// </summary>
    [Serializable]
    public sealed partial class UserEditorSettingsRecentDatasheetList : ModelList<UserEditorSettingsRecentDatasheetList, UserEditorSettingsRecentDatasheet>
    {
        #region Authorization Rules

        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion
          
        #region Factory Methods
        /// <summary>
        /// Called to create a new list
        /// </summary>
        /// <returns>A New List</returns>
        public static UserEditorSettingsRecentDatasheetList NewList()
        {
            UserEditorSettingsRecentDatasheetList list = new UserEditorSettingsRecentDatasheetList();
            list.Create();
            return list;
        }
        #endregion

                #region Methods
       /// <summary>
       /// Adds a label of type Label to NewuserEditorSettingsRecentDatasheet
       /// </summary>
       /// <param name="label">The DataSheet is being added</param>
       public void Add(CustomColumnLayout datasheet, Int32 maxRecentItems = 10)
       {
           if (datasheet == null)
           {
               return;
           }
              foreach (UserEditorSettingsRecentDatasheet recentDatasheet in this)
           {
               if (datasheet.Id.Equals(recentDatasheet.DatasheetId)) return;
           }
            List<UserEditorSettingsRecentDatasheet> DatasheetList = this.Where(rD => rD.DatasheetId != null).ToList();

               if (DatasheetList.Count.Equals(maxRecentItems))
               {
                   
                   this.Remove(DatasheetList[0]);
                   this.Add(UserEditorSettingsRecentDatasheet.NewUserEditorSettingsRecentDatasheet(datasheet));
               }
               else
               {
                   this.Add(UserEditorSettingsRecentDatasheet.NewUserEditorSettingsRecentDatasheet(datasheet));
               }
          } 
         

       /// <summary>
       /// Removed the Label
       /// </summary>
       /// <param name="label">The label to be removed</param>
       public void Remove(CustomColumnLayout datasheet) 
       {
           UserEditorSettingsRecentDatasheet removeUserEditorSettingsRecentDatasheet =
               this.FirstOrDefault(D => D.DatasheetId.Equals(datasheet.Id));  
           if (removeUserEditorSettingsRecentDatasheet != null)
           {
               this.Remove(removeUserEditorSettingsRecentDatasheet);
           }
       }
       
        #endregion
    }
}
