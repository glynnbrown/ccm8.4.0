﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public sealed partial class LocationLevelList
    {
        #region Constructor
        private LocationLevelList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        /// <returns>A list of existing it</returns>
        internal static LocationLevelList FetchByParentLocationLevelId(
            IDalContext dalContext, Int32? parentId, IEnumerable<LocationLevelDto> productLevelDtoList)
        {
            return DataPortal.FetchChild<LocationLevelList>(dalContext, parentId, productLevelDtoList);
        }

        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returing all items for a given parent id
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32? parentId, IEnumerable<LocationLevelDto> productLevelDtoList)
        {
            RaiseListChangedEvents = false;

            //get a list of child dtos and add them to this
            foreach (LocationLevelDto childDto in productLevelDtoList)
            {
                if (childDto.ParentLevelId == parentId)
                {
                    this.Add(LocationLevel.Fetch(dalContext, childDto, productLevelDtoList));
                }
            }

            RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}