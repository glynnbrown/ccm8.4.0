using System;
using System.Collections.Generic;
using System.Linq;
using Csla;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class EntityComparisonAttributeList : ModelList<EntityComparisonAttributeList, EntityComparisonAttribute>
    {
        #region Properties

        #region Parent

        /// <summary>
        ///     Get a reference to the containing <see cref="Entity"/>.
        /// </summary>
        public new Entity Parent => (Entity)base.Parent;

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Create a new instance of <see cref="EntityComparisonAttributeList"/>.
        /// </summary>
        public static EntityComparisonAttributeList NewEntityComparisonAttributeList()
        {
            var item = new EntityComparisonAttributeList();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        ///     Initialize all default property values for this instance.
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Returns all specified products by their ids
        /// </summary>
        public static EntityComparisonAttributeList FetchByEntityIdComparisonAttribute(Int32 entityId)
        {
            return DataPortal.Fetch<EntityComparisonAttributeList>(new EntityComparisonAttributeList.FetchByEntityIdCriteria(entityId));
        }

        #endregion

        #endregion

        #region Methods

        public void AddRange(IEnumerable<EntityComparisonAttribute> sourceItems)
        {
            base.AddRange(sourceItems.Select(EntityComparisonAttribute.NewEntityComparisonAttribute));
        }

        #endregion

        #region FetchByEntityIdCriteria
        /// <summary>
        /// Criteria for ProductList.FetchByProductGroupId
        /// </summary>
        [Serializable]
        internal class FetchByEntityIdCriteria : CriteriaBase<FetchByEntityIdCriteria>
        {
            #region Properties

            #region EntityId
            public static PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId => ReadProperty<Int32>(EntityIdProperty);

            #endregion
            
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
            }
            #endregion
        }
        #endregion
    }
}