﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created (Auto-generated)
// V8-25526 : K.Pickup
//  Use RelationshipTypes.LazyLoad when registering lazy-loaded model properties.
// V8-25873 : A.Probyn
//  Added Custom Attribute 1 - 5 and added load of id in SaveAsFile
#endregion
#region Version History: (CCM CCM830)
// CCM-32524 : L.Ineson
//		Added Annotations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{

    /// <summary>
    /// Defines a fixture package.
    /// A package contains a group of fixtures and
    /// their associated data. A package could represent
    /// a file or group of fixtures within a database.
    /// </summary>
    [Serializable]
    public sealed partial class FixturePackage : ModelObject<FixturePackage>, IDisposable
    {
        #region Private Fields

        /// <summary>
        /// For the IDisposable pattern.
        /// </summary>
        private Boolean _isDisposed;

        #endregion

        #region Properties

        /// <summary>
        /// Returns the file extension to use when saving this to file. 
        /// </summary>
        public static String FileExtension
        {
            get { return Constants.FixtureLibraryFileExtension; }
        }

        #region DalFactoryName
        /// <summary>
        /// DalFactoryName property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> DalFactoryNameProperty =
            RegisterModelProperty<String>(c => c.DalFactoryName);
        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        protected override string DalFactoryName
        {
            get { return this.GetProperty<String>(DalFactoryNameProperty); }
        }
        #endregion

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Object> IdProperty =
            RegisterModelProperty<Object>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// For a filestystem this is the filepath.
        /// </summary>
        public Object Id
        {
            get { return this.GetProperty<Object>(IdProperty); }
            set { SetProperty<Object>(IdProperty, value); }
        }
        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region Description

        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);

        /// <summary>
        /// Gets/Sets the Description value
        /// </summary>
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
            set { SetProperty<String>(DescriptionProperty, value); }
        }

        #endregion

        #region Height

        /// <summary>
        /// Height property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HeightProperty =
            RegisterModelProperty<Single>(c => c.Height);

        /// <summary>
        /// Gets/Sets the Height value
        /// </summary>
        public Single Height
        {
            get { return GetProperty<Single>(HeightProperty); }
            set { SetProperty<Single>(HeightProperty, value); }
        }

        #endregion

        #region Width

        /// <summary>
        /// Width property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> WidthProperty =
            RegisterModelProperty<Single>(c => c.Width);

        /// <summary>
        /// Gets/Sets the Width value
        /// </summary>
        public Single Width
        {
            get { return GetProperty<Single>(WidthProperty); }
            set { SetProperty<Single>(WidthProperty, value); }
        }

        #endregion

        #region Depth

        /// <summary>
        /// Depth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DepthProperty =
            RegisterModelProperty<Single>(c => c.Depth);

        /// <summary>
        /// Gets/Sets the Depth value
        /// </summary>
        public Single Depth
        {
            get { return GetProperty<Single>(DepthProperty); }
            set { SetProperty<Single>(DepthProperty, value); }
        }

        #endregion

        #region FixtureCount

        /// <summary>
        /// FixtureCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FixtureCountProperty =
            RegisterModelProperty<Int32>(c => c.FixtureCount);

        /// <summary>
        /// Gets the FixtureCount value
        /// </summary>
        public Int32 FixtureCount
        {
            get { return GetProperty<Int32>(FixtureCountProperty); }
            private set { SetProperty<Int32>(FixtureCountProperty, value); }
        }

        #endregion

        #region AssemblyCount

        /// <summary>
        /// AssemblyCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> AssemblyCountProperty =
            RegisterModelProperty<Int32>(c => c.AssemblyCount);

        /// <summary>
        /// Gets the AssemblyCount value
        /// </summary>
        public Int32 AssemblyCount
        {
            get { return GetProperty<Int32>(AssemblyCountProperty); }
            private set { SetProperty<Int32>(AssemblyCountProperty, value); }
        }

        #endregion

        #region ComponentCount

        /// <summary>
        /// ComponentCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ComponentCountProperty =
            RegisterModelProperty<Int32>(c => c.ComponentCount);

        /// <summary>
        /// Gets the ComponentCount value
        /// </summary>
        public Int32 ComponentCount
        {
            get { return GetProperty<Int32>(ComponentCountProperty); }
            private set { SetProperty<Int32>(ComponentCountProperty, value); }
        }

        #endregion

        #region ThumbnailImageData

        /// <summary>
        /// ThumbnailImageData property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte[]> ThumbnailImageDataProperty =
            RegisterModelProperty<Byte[]>(c => c.ThumbnailImageData);

        /// <summary>
        /// Gets/Sets the ThumbnailImageData value
        /// </summary>
        public Byte[] ThumbnailImageData
        {
            get { return GetProperty<Byte[]>(ThumbnailImageDataProperty); }
            set { SetProperty<Byte[]>(ThumbnailImageDataProperty, value); }
        }

        #endregion

        #region LengthUnitsOfMeasure

        /// <summary>
        /// LengthUnitsOfMeasure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureLengthUnitOfMeasureType> LengthUnitsOfMeasureProperty =
            RegisterModelProperty<FixtureLengthUnitOfMeasureType>(c => c.LengthUnitsOfMeasure);

        /// <summary>
        /// Gets/Sets the LengthUnitsOfMeasure value
        /// </summary>
        public FixtureLengthUnitOfMeasureType LengthUnitsOfMeasure
        {
            get { return GetProperty<FixtureLengthUnitOfMeasureType>(LengthUnitsOfMeasureProperty); }
            set { SetProperty<FixtureLengthUnitOfMeasureType>(LengthUnitsOfMeasureProperty, value); }
        }

        #endregion

        #region AreaUnitsOfMeasure

        /// <summary>
        /// AreaUnitsOfMeasure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureAreaUnitOfMeasureType> AreaUnitsOfMeasureProperty =
            RegisterModelProperty<FixtureAreaUnitOfMeasureType>(c => c.AreaUnitsOfMeasure);

        /// <summary>
        /// Gets/Sets the AreaUnitsOfMeasure value
        /// </summary>
        public FixtureAreaUnitOfMeasureType AreaUnitsOfMeasure
        {
            get { return GetProperty<FixtureAreaUnitOfMeasureType>(AreaUnitsOfMeasureProperty); }
            set { SetProperty<FixtureAreaUnitOfMeasureType>(AreaUnitsOfMeasureProperty, value); }
        }

        #endregion

        #region VolumeUnitsOfMeasure

        /// <summary>
        /// VolumeUnitsOfMeasure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureVolumeUnitOfMeasureType> VolumeUnitsOfMeasureProperty =
            RegisterModelProperty<FixtureVolumeUnitOfMeasureType>(c => c.VolumeUnitsOfMeasure);

        /// <summary>
        /// Gets/Sets the VolumeUnitsOfMeasure value
        /// </summary>
        public FixtureVolumeUnitOfMeasureType VolumeUnitsOfMeasure
        {
            get { return GetProperty<FixtureVolumeUnitOfMeasureType>(VolumeUnitsOfMeasureProperty); }
            set { SetProperty<FixtureVolumeUnitOfMeasureType>(VolumeUnitsOfMeasureProperty, value); }
        }

        #endregion

        #region WeightUnitsOfMeasure

        /// <summary>
        /// WeightUnitsOfMeasure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureWeightUnitOfMeasureType> WeightUnitsOfMeasureProperty =
            RegisterModelProperty<FixtureWeightUnitOfMeasureType>(c => c.WeightUnitsOfMeasure);

        /// <summary>
        /// Gets/Sets the WeightUnitsOfMeasure value
        /// </summary>
        public FixtureWeightUnitOfMeasureType WeightUnitsOfMeasure
        {
            get { return GetProperty<FixtureWeightUnitOfMeasureType>(WeightUnitsOfMeasureProperty); }
            set { SetProperty<FixtureWeightUnitOfMeasureType>(WeightUnitsOfMeasureProperty, value); }
        }

        #endregion

        #region AngleUnitsOfMeasure

        /// <summary>
        /// AngleUnitsOfMeasure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureAngleUnitOfMeasureType> AngleUnitsOfMeasureProperty =
            RegisterModelProperty<FixtureAngleUnitOfMeasureType>(c => c.AngleUnitsOfMeasure);

        /// <summary>
        /// Gets/Sets the AngleUnitsOfMeasure value
        /// </summary>
        public FixtureAngleUnitOfMeasureType AngleUnitsOfMeasure
        {
            get { return GetProperty<FixtureAngleUnitOfMeasureType>(AngleUnitsOfMeasureProperty); }
            set { SetProperty<FixtureAngleUnitOfMeasureType>(AngleUnitsOfMeasureProperty, value); }
        }

        #endregion

        #region CurrencyUnitsOfMeasure

        /// <summary>
        /// CurrencyUnitsOfMeasure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureCurrencyUnitOfMeasureType> CurrencyUnitsOfMeasureProperty =
            RegisterModelProperty<FixtureCurrencyUnitOfMeasureType>(c => c.CurrencyUnitsOfMeasure);

        /// <summary>
        /// Gets/Sets the CurrencyUnitsOfMeasure value
        /// </summary>
        public FixtureCurrencyUnitOfMeasureType CurrencyUnitsOfMeasure
        {
            get { return GetProperty<FixtureCurrencyUnitOfMeasureType>(CurrencyUnitsOfMeasureProperty); }
            set { SetProperty<FixtureCurrencyUnitOfMeasureType>(CurrencyUnitsOfMeasureProperty, value); }
        }

        #endregion

        #region DateCreated

        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        /// Gets/Sets the DateCreated value
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
            set { SetProperty<DateTime>(DateCreatedProperty, value); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        /// Gets/Sets the DateLastModified value
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
            set { SetProperty<DateTime>(DateLastModifiedProperty, value); }
        }

        #endregion

        #region CreatedBy

        /// <summary>
        /// CreatedBy property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CreatedByProperty =
            RegisterModelProperty<String>(c => c.CreatedBy);

        /// <summary>
        /// Gets/Sets the CreatedBy value
        /// </summary>
        public String CreatedBy
        {
            get { return GetProperty<String>(CreatedByProperty); }
            set { SetProperty<String>(CreatedByProperty, value); }
        }

        #endregion

        #region LastModifiedBy

        /// <summary>
        /// LastModifiedBy property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> LastModifiedByProperty =
            RegisterModelProperty<String>(c => c.LastModifiedBy);

        /// <summary>
        /// Gets/Sets the LastModifiedBy value
        /// </summary>
        public String LastModifiedBy
        {
            get { return GetProperty<String>(LastModifiedByProperty); }
            set { SetProperty<String>(LastModifiedByProperty, value); }
        }

        #endregion

        #region Custom Attribute 1

        /// <summary>
        /// CustomAttribute1 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomAttribute1Property =
            RegisterModelProperty<String>(c => c.CustomAttribute1, Message.FixturePackage_CustomAttribute1);

        /// <summary>
        /// Gets/Sets the CustomAttribute1 value
        /// </summary>
        public String CustomAttribute1
        {
            get { return GetProperty<String>(CustomAttribute1Property); }
            set { SetProperty<String>(CustomAttribute1Property, value); }
        }

        #endregion

        #region Custom Attribute 2

        /// <summary>
        /// CustomAttribute2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomAttribute2Property =
            RegisterModelProperty<String>(c => c.CustomAttribute2, Message.FixturePackage_CustomAttribute2);

        /// <summary>
        /// Gets/Sets the CustomAttribute2 value
        /// </summary>
        public String CustomAttribute2
        {
            get { return GetProperty<String>(CustomAttribute2Property); }
            set { SetProperty<String>(CustomAttribute2Property, value); }
        }

        #endregion

        #region Custom Attribute 3

        /// <summary>
        /// CustomAttribute3 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomAttribute3Property =
            RegisterModelProperty<String>(c => c.CustomAttribute3, Message.FixturePackage_CustomAttribute3);

        /// <summary>
        /// Gets/Sets the CustomAttribute3 value
        /// </summary>
        public String CustomAttribute3
        {
            get { return GetProperty<String>(CustomAttribute3Property); }
            set { SetProperty<String>(CustomAttribute3Property, value); }
        }

        #endregion

        #region Custom Attribute 4

        /// <summary>
        /// CustomAttribute4 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomAttribute4Property =
            RegisterModelProperty<String>(c => c.CustomAttribute4, Message.FixturePackage_CustomAttribute4);

        /// <summary>
        /// Gets/Sets the CustomAttribute4 value
        /// </summary>
        public String CustomAttribute4
        {
            get { return GetProperty<String>(CustomAttribute4Property); }
            set { SetProperty<String>(CustomAttribute4Property, value); }
        }

        #endregion

        #region Custom Attribute 5

        /// <summary>
        /// CustomAttribute5 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomAttribute5Property =
            RegisterModelProperty<String>(c => c.CustomAttribute5, Message.FixturePackage_CustomAttribute5);

        /// <summary>
        /// Gets/Sets the CustomAttribute5 value
        /// </summary>
        public String CustomAttribute5
        {
            get { return GetProperty<String>(CustomAttribute5Property); }
            set { SetProperty<String>(CustomAttribute5Property, value); }
        }

        #endregion

        #region FixtureItems
        /// <summary>
        /// FixtureItems property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureItemList> FixtureItemsProperty =
            RegisterModelProperty<FixtureItemList>(c => c.FixtureItems, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Returns the FixtureItems within this package
        /// </summary>
        public FixtureItemList FixtureItems
        {
            get
            {
                return this.GetPropertyLazy<FixtureItemList>(
                    FixtureItemsProperty,
                    new FixtureItemList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// Asynchronously returns the FixtureItems within this package
        /// </summary>
        public FixtureItemList FixtureItemsAsync
        {
            get
            {
                return this.GetPropertyLazy<FixtureItemList>(
                    FixtureItemsProperty,
                    new FixtureItemList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region Fixtures
        /// <summary>
        /// Fixtures property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureList> FixturesProperty =
            RegisterModelProperty<FixtureList>(c => c.Fixtures, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Returns the Fixtures within this package
        /// </summary>
        public FixtureList Fixtures
        {
            get
            {
                return this.GetPropertyLazy<FixtureList>(
                    FixturesProperty,
                    new FixtureList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// Asynchronously returns the Fixtures within this package
        /// </summary>
        public FixtureList FixturesAsync
        {
            get
            {
                return this.GetPropertyLazy<FixtureList>(
                    FixturesProperty,
                    new FixtureList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region Assemblies
        /// <summary>
        /// Assemblies property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureAssemblyList> AssembliesProperty =
            RegisterModelProperty<FixtureAssemblyList>(c => c.Assemblies, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Returns the Assemblies within this package
        /// </summary>
        public FixtureAssemblyList Assemblies
        {
            get
            {
                return this.GetPropertyLazy<FixtureAssemblyList>(
                    AssembliesProperty,
                    new FixtureAssemblyList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// Asynchronously returns the Assemblies within this package
        /// </summary>
        public FixtureAssemblyList AssembliesAsync
        {
            get
            {
                return this.GetPropertyLazy<FixtureAssemblyList>(
                    AssembliesProperty,
                    new FixtureAssemblyList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region Components
        /// <summary>
        /// Components property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureComponentList> ComponentsProperty =
            RegisterModelProperty<FixtureComponentList>(c => c.Components, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Returns the Components within this package
        /// </summary>
        public FixtureComponentList Components
        {
            get
            {
                return this.GetPropertyLazy<FixtureComponentList>(
                    ComponentsProperty,
                    new FixtureComponentList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// Asynchronously returns the Components within this package
        /// </summary>
        public FixtureComponentList ComponentsAsync
        {
            get
            {
                return this.GetPropertyLazy<FixtureComponentList>(
                    ComponentsProperty,
                    new FixtureComponentList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region Images
        /// <summary>
        /// Images property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureImageList> ImagesProperty =
            RegisterModelProperty<FixtureImageList>(c => c.Images, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Returns the Images within this package
        /// </summary>
        public FixtureImageList Images
        {
            get
            {
                return this.GetPropertyLazy<FixtureImageList>(
                    ImagesProperty,
                    new FixtureImageList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// Asynchronously returns the Images within this package
        /// </summary>
        public FixtureImageList ImagesAsync
        {
            get
            {
                return this.GetPropertyLazy<FixtureImageList>(
                    ImagesProperty,
                    new FixtureImageList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region Annotations
        /// <summary>
        /// Annotations property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureAnnotationList> AnnotationsProperty =
            RegisterModelProperty<FixtureAnnotationList>(c => c.Annotations, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Returns the Annotations within this package
        /// </summary>
        public FixtureAnnotationList Annotations
        {
            get
            {
                return this.GetPropertyLazy<FixtureAnnotationList>(
                    AnnotationsProperty,
                    new FixtureAnnotationList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// Asynchronously returns the Annotations within this package
        /// </summary>
        public FixtureAnnotationList AnnotationsAsync
        {
            get
            {
                return this.GetPropertyLazy<FixtureAnnotationList>(
                    AnnotationsProperty,
                    new FixtureAnnotationList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region Helper Properties

        /// <summary>
        /// Returns the filename of this package
        /// </summary>
        public String FileName
        {
            get { return this.DalFactoryName; }
        }

        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
            BusinessRules.AddRule(new MaxLength(CreatedByProperty, 100));
            BusinessRules.AddRule(new MaxLength(LastModifiedByProperty, 100));

        }

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods

        /// <summary>
        /// Locks a package so that it cannot be
        /// edited or opened by another process
        /// </summary>
        internal static void LockFixturePackageByFileName(String fileName)
        {
            DataPortal.Execute<LockFixturePackageCommand>(new LockFixturePackageCommand(FixturePackageType.FileSystem, fileName));
        }

        /// <summary>
        /// Unlocks a package
        /// </summary>
        internal static void UnlockFixturePackageByFileName(String fileName)
        {
            DataPortal.Execute<UnlockFixturePackageCommand>(new UnlockFixturePackageCommand(FixturePackageType.FileSystem, fileName));
        }

        /// <summary>
        /// Creates a new FixturePackage
        /// </summary>
        public static FixturePackage NewFixturePackage()
        {
            FixturePackage item = new FixturePackage();
            item.Create();
            return item;
        }


        /// <summary>
        /// Creates a new FixturePackage
        /// </summary>
        public static FixturePackage NewFixturePackage(String fileName)
        {
            FixturePackage item = new FixturePackage();
            item.Create(fileName);
            return item;
        }

        /// <summary>
        /// Creates a new FixturePackage from the given plan fixture.
        /// </summary>
        public static FixturePackage NewFixturePackage(IEnumerable<PlanogramFixtureItem> planFixtureItems)
        {
            FixturePackage item = new FixturePackage();
            item.Create(planFixtureItems);
            return item;
        }

        /// <summary>
        /// Creates a new FixturePackage from the given plan component.
        /// </summary>
        public static FixturePackage NewFixturePackage(PlanogramFixtureComponent planFixtureComponent)
        {
            FixturePackage item = new FixturePackage();
            item.Create(planFixtureComponent);
            return item;
        }

        /// <summary>
        /// Creates a new FixturePackage from the given plan component.
        /// </summary>
        public static FixturePackage NewFixturePackage(PlanogramComponent planComponent)
        {
            FixturePackage item = new FixturePackage();
            item.Create(planComponent);
            return item;
        }

        /// <summary>
        /// Creates a new FixturePackage from the given planogram assembly
        /// </summary>
        /// <param name="fixtureAssembly"></param>
        /// <returns></returns>
        public static FixturePackage NewFixturePackage(PlanogramFixtureAssembly planFixtureAssembly)
        {
            FixturePackage item = new FixturePackage();
            item.Create(planFixtureAssembly);
            return item;
        }

        /// <summary>
        /// Creates a new FixturePackage from the given plan fixture.
        /// </summary>
        public static FixturePackage NewFixturePackage(PlanogramFixtureItem planFixtureItem)
        {
            FixturePackage item = new FixturePackage();
            item.Create(planFixtureItem);
            return item;
        }

        #endregion

        #region Data Access

        #region Criteria

        #region FetchCriteria
        /// <summary>
        /// Criteria for the Fetch factory method
        /// </summary>
        [Serializable]
        public class FetchCriteria : CriteriaBase<FetchCriteria>
        {
            #region Properties

            #region PackageType
            /// <summary>
            /// PackageType property definition
            /// </summary>
            public static readonly PropertyInfo<FixturePackageType> PackageTypeProperty =
                RegisterProperty<FixturePackageType>(c => c.PackageType);
            /// <summary>
            /// Returns the package type
            /// </summary>
            public FixturePackageType PackageType
            {
                get { return this.ReadProperty<FixturePackageType>(PackageTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchCriteria(FixturePackageType sourceType, Object id)
            {
                this.LoadProperty<FixturePackageType>(PackageTypeProperty, sourceType);
                this.LoadProperty<Object>(IdProperty, id);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<FixtureItemList>(FixtureItemsProperty, FixtureItemList.NewFixtureItemList());
            this.LoadProperty<FixtureList>(FixturesProperty, FixtureList.NewFixtureList());
            this.LoadProperty<FixtureAssemblyList>(AssembliesProperty, FixtureAssemblyList.NewFixtureAssemblyList());
            this.LoadProperty<FixtureComponentList>(ComponentsProperty, FixtureComponentList.NewFixtureComponentList());
            this.LoadProperty<FixtureImageList>(ImagesProperty, FixtureImageList.NewFixtureImageList());
            this.LoadProperty<FixtureAnnotationList>(AnnotationsProperty, FixtureAnnotationList.NewFixtureAnnotationList());
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(String fileName)
        {
            LockFixturePackageByFileName(fileName);
            this.LoadProperty<String>(DalFactoryNameProperty, fileName);
            this.LoadProperty<Object>(IdProperty, fileName);
            this.LoadProperty<FixtureItemList>(FixtureItemsProperty, FixtureItemList.NewFixtureItemList());
            this.LoadProperty<FixtureList>(FixturesProperty, FixtureList.NewFixtureList());
            this.LoadProperty<FixtureAssemblyList>(AssembliesProperty, FixtureAssemblyList.NewFixtureAssemblyList());
            this.LoadProperty<FixtureComponentList>(ComponentsProperty, FixtureComponentList.NewFixtureComponentList());
            this.LoadProperty<FixtureImageList>(ImagesProperty, FixtureImageList.NewFixtureImageList());
            this.LoadProperty<FixtureAnnotationList>(AnnotationsProperty, FixtureAnnotationList.NewFixtureAnnotationList());
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// from an existing planogram component
        /// </summary>
        private void Create(PlanogramFixtureComponent planFixtureComponent)
        {
            //get the component any any linked annotations to be added.
            PlanogramComponent planComponent = planFixtureComponent.GetPlanogramComponent();
            if (planComponent == null) throw new ArgumentException();

            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<FixtureItemList>(FixtureItemsProperty, FixtureItemList.NewFixtureItemList());
            this.LoadProperty<FixtureList>(FixturesProperty, FixtureList.NewFixtureList());
            this.LoadProperty<FixtureAssemblyList>(AssembliesProperty, FixtureAssemblyList.NewFixtureAssemblyList());
            this.LoadProperty<FixtureComponentList>(ComponentsProperty, FixtureComponentList.NewFixtureComponentList());
            this.LoadProperty<FixtureImageList>(ImagesProperty, FixtureImageList.NewFixtureImageList());
            this.LoadProperty<FixtureAnnotationList>(AnnotationsProperty, FixtureAnnotationList.NewFixtureAnnotationList());

            //load the component properties
            LoadProperty<String>(NameProperty, planComponent.Name);
            LoadProperty<Single>(HeightProperty, planComponent.Height);
            LoadProperty<Single>(WidthProperty,  planComponent.Width);
            LoadProperty<Single>(DepthProperty, planComponent.Depth);

            //create the fixture component - this will also copy subcomponents and images.
            AddPlanogramComponent(planComponent);

            //add any linked annotations
            foreach (PlanogramAnnotation planAnnotation in
                planComponent.Parent.Annotations.Where(a => 
                    a.PlanogramPositionId == null
                    && a.PlanogramAssemblyComponentId == null
                    && a.PlanogramFixtureAssemblyId == null
                    && Object.Equals(a.PlanogramFixtureComponentId, planFixtureComponent.Id)))
            {
                this.Annotations.Add(planAnnotation);
            }

            //null off ids from annotations which are higher than component level
            foreach (FixtureAnnotation anno in this.Annotations)
            {
                anno.FixtureItemId = null;
                anno.FixtureAssemblyItemId = null;
                anno.FixtureAssemblyComponentId = null;
                anno.FixtureComponentItemId = null;
            }


            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// from an existing planogram component
        /// </summary>
        private void Create(PlanogramComponent planComponent)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<FixtureItemList>(FixtureItemsProperty, FixtureItemList.NewFixtureItemList());
            this.LoadProperty<FixtureList>(FixturesProperty, FixtureList.NewFixtureList());
            this.LoadProperty<FixtureAssemblyList>(AssembliesProperty, FixtureAssemblyList.NewFixtureAssemblyList());
            this.LoadProperty<FixtureComponentList>(ComponentsProperty, FixtureComponentList.NewFixtureComponentList());
            this.LoadProperty<FixtureImageList>(ImagesProperty, FixtureImageList.NewFixtureImageList());
            this.LoadProperty<FixtureAnnotationList>(AnnotationsProperty, FixtureAnnotationList.NewFixtureAnnotationList());

            //load the component properties
            LoadProperty<String>(NameProperty, planComponent.Name);
            LoadProperty<Single>(HeightProperty, planComponent.Height);
            LoadProperty<Single>(WidthProperty, planComponent.Width);
            LoadProperty<Single>(DepthProperty, planComponent.Depth);

            //create the fixture component - this will also copy subcomponents and images.
            AddPlanogramComponent(planComponent);

            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// from an existing planogram assembly.
        /// </summary>
        private void Create(PlanogramFixtureAssembly planfixtureAssembly)
        {
            PlanogramAssembly planAssembly = planfixtureAssembly.GetPlanogramAssembly();
            if (planAssembly == null) throw new ArgumentException();

            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<FixtureItemList>(FixtureItemsProperty, FixtureItemList.NewFixtureItemList());
            this.LoadProperty<FixtureList>(FixturesProperty, FixtureList.NewFixtureList());
            this.LoadProperty<FixtureAssemblyList>(AssembliesProperty, FixtureAssemblyList.NewFixtureAssemblyList());
            this.LoadProperty<FixtureComponentList>(ComponentsProperty, FixtureComponentList.NewFixtureComponentList());
            this.LoadProperty<FixtureImageList>(ImagesProperty, FixtureImageList.NewFixtureImageList());
            this.LoadProperty<FixtureAnnotationList>(AnnotationsProperty, FixtureAnnotationList.NewFixtureAnnotationList());
            
            this.LoadProperty<String>(NameProperty, planAssembly.Name);
            this.LoadProperty<Single>(HeightProperty, planAssembly.Height);
            this.LoadProperty<Single>(WidthProperty, planAssembly.Width);
            this.LoadProperty<Single>(DepthProperty, planAssembly.Depth);

            //Add the new assembly
            AddPlanogramAssembly(planAssembly);


            //null off ids from annotations which are higher than assembly level
            foreach (FixtureAnnotation anno in this.Annotations)
            {
                anno.FixtureItemId = null;
                anno.FixtureAssemblyItemId = null;
            }

            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// from an existing planogram fixture.
        /// </summary>
        private void Create(PlanogramFixtureItem planFixtureItem)
        {
            PlanogramFixture planFixture = planFixtureItem.GetPlanogramFixture();
            if (planFixture == null) throw new ArgumentException();

            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<FixtureItemList>(FixtureItemsProperty, FixtureItemList.NewFixtureItemList());
            this.LoadProperty<FixtureList>(FixturesProperty, FixtureList.NewFixtureList());
            this.LoadProperty<FixtureAssemblyList>(AssembliesProperty, FixtureAssemblyList.NewFixtureAssemblyList());
            this.LoadProperty<FixtureComponentList>(ComponentsProperty, FixtureComponentList.NewFixtureComponentList());
            this.LoadProperty<FixtureImageList>(ImagesProperty, FixtureImageList.NewFixtureImageList());
            this.LoadProperty<FixtureAnnotationList>(AnnotationsProperty, FixtureAnnotationList.NewFixtureAnnotationList());
            
            this.LoadProperty<String>(NameProperty, planFixture.Name);
            this.LoadProperty<Single>(HeightProperty, planFixture.Height);
            this.LoadProperty<Single>(WidthProperty, planFixture.Width);
            this.LoadProperty<Single>(DepthProperty, planFixture.Depth);

            //Add the fixture
            AddPlanogramFixture(planFixture);


            //add linked annotations.
            foreach (PlanogramAnnotation planAnnotation in
                planFixture.Parent.Annotations
                .Where(a =>
                    a.PlanogramPositionId == null
                    && a.PlanogramSubComponentId == null
                    && a.PlanogramFixtureComponentId == null
                    && a.PlanogramFixtureAssemblyId == null
                    && a.PlanogramAssemblyComponentId == null
                    && Object.Equals(planFixtureItem.Id, a.PlanogramFixtureItemId)))
            {
                this.Annotations.Add(planAnnotation);
            }

            //null off ids from annotations which are higher than fixture level
            foreach (FixtureAnnotation anno in this.Annotations)
            {
                anno.FixtureItemId = null;
            }

            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// from an existing planogram assembly.
        /// </summary>
        private void Create(IEnumerable<PlanogramFixtureItem> planFixtureItems)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<FixtureItemList>(FixtureItemsProperty, FixtureItemList.NewFixtureItemList());
            this.LoadProperty<FixtureList>(FixturesProperty, FixtureList.NewFixtureList());
            this.LoadProperty<FixtureAssemblyList>(AssembliesProperty, FixtureAssemblyList.NewFixtureAssemblyList());
            this.LoadProperty<FixtureComponentList>(ComponentsProperty, FixtureComponentList.NewFixtureComponentList());
            this.LoadProperty<FixtureImageList>(ImagesProperty, FixtureImageList.NewFixtureImageList());
            this.LoadProperty<FixtureAnnotationList>(AnnotationsProperty, FixtureAnnotationList.NewFixtureAnnotationList());


            Dictionary<PlanogramFixtureItem, PlanogramFixture> fixtures = new Dictionary<PlanogramFixtureItem, PlanogramFixture>();
            foreach (PlanogramFixtureItem fi in planFixtureItems)
            {
                fixtures.Add(fi, fi.Parent.Fixtures.FindById(fi.PlanogramFixtureId));
            }

            Single minX = fixtures.Keys.Min(fi => fi.X);
            Single minY = fixtures.Keys.Min(fi => fi.Y);
            Single minZ = fixtures.Keys.Min(fi => fi.Z);

            LoadProperty<String>(NameProperty, fixtures.First().Value.Name);
            LoadProperty<Single>(HeightProperty, fixtures.Keys.Max(fi => fi.Y + fixtures[fi].Height) - minY);
            LoadProperty<Single>(WidthProperty, fixtures.Keys.Max(fi => fi.X + fixtures[fi].Width) - minX);
            LoadProperty<Single>(DepthProperty, fixtures.Keys.Max(fi => fi.Z + fixtures[fi].Depth) - minZ);

            foreach (PlanogramFixtureItem planFixtureItem in planFixtureItems)
            {
                FixtureItem fixtureItem = AddPlanogramFixtureItem(planFixtureItem);
                fixtureItem.X -= minX;
                fixtureItem.Y -= minY;
                fixtureItem.Z -= minZ;
            }

            base.Create();
        }

        #endregion

        #region Commands

        #region LockFixturePackageCommand
        /// <summary>
        /// Performs the locking of a package
        /// </summary>
        [Serializable]
        private partial class LockFixturePackageCommand : CommandBase<LockFixturePackageCommand>
        {
            #region Authorization Rules
            // no authentication rules required as this object
            // is accessed by the editor when not connected to
            // a repository
            #endregion

            #region Properties

            #region FixturePackageType
            /// <summary>
            /// FixturePackageType property definition
            /// </summary>
            public static readonly PropertyInfo<FixturePackageType> FixturePackageTypeProperty =
                RegisterProperty<FixturePackageType>(c => c.FixturePackageType);
            /// <summary>
            /// Returns the package source type
            /// </summary>
            public FixturePackageType FixturePackageType
            {
                get { return this.ReadProperty<FixturePackageType>(FixturePackageTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public LockFixturePackageCommand(FixturePackageType packageType, Object id)
            {
                this.LoadProperty<FixturePackageType>(FixturePackageTypeProperty, packageType);
                this.LoadProperty<Object>(IdProperty, id);
            }
            #endregion
        }
        #endregion

        #region UnlockFixturePackageCommand
        /// <summary>
        /// Performs the unlocking of a package
        /// </summary>
        [Serializable]
        private partial class UnlockFixturePackageCommand : CommandBase<UnlockFixturePackageCommand>
        {
            #region Authorization Rules
            // no authentication rules required as this object
            // is accessed by the editor when not connected to
            // a repository
            #endregion

            #region Properties

            #region FixturePackageType
            /// <summary>
            /// SourceType property definition
            /// </summary>
            public static readonly PropertyInfo<FixturePackageType> FixturePackageTypeProperty =
                RegisterProperty<FixturePackageType>(c => c.FixturePackageType);
            /// <summary>
            /// Returns the package source type
            /// </summary>
            public FixturePackageType FixturePackageType
            {
                get { return this.ReadProperty<FixturePackageType>(FixturePackageTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public UnlockFixturePackageCommand(FixturePackageType sourceType, Object id)
            {
                this.LoadProperty<FixturePackageType>(FixturePackageTypeProperty, sourceType);
                this.LoadProperty<Object>(IdProperty, id);
            }
            #endregion
        }
        #endregion

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Saves this FixturePackage to a file with a particular file name.
        /// </summary>
        public FixturePackage SaveAs(String fileName)
        {
            FixturePackage returnValue = null;
            String oldDalFactoryName = null;
            if ((!IsNew) && (DalFactoryName != fileName))
            {
                oldDalFactoryName = DalFactoryName;
                MarkGraphAsNew();
            }
            LockFixturePackageByFileName(fileName);
            LoadProperty<String>(DalFactoryNameProperty, fileName);
            LoadProperty<Object>(IdProperty, fileName);
            returnValue = Save();
            if (!String.IsNullOrEmpty(oldDalFactoryName))
            {
                UnlockFixturePackageByFileName(oldDalFactoryName);
            }
            //V8-25873 : Make sure id is the file name for file system, dal is returning 1
            LoadProperty<Object>(IdProperty, fileName);
            return returnValue;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<FixturePackage>(oldId, newId);
        }

        /// <summary>
        /// Copies the images from the given planogram to this package.
        /// </summary>
        /// <param name="planImageIds"></param>
        /// <param name="source"></param>
        internal void AddImages(IEnumerable<Int32?> planImageIds, Planogram source)
        {
            foreach (Int32? id in planImageIds)
            {
                if (id == null) continue;

                //check if the image already exists.
                FixtureImage fixtureImage = this.Images.FindById(id);
                if (fixtureImage != null) continue;

                PlanogramImage img = source.Images.FindById(id);
                if (img == null) continue;

                this.Images.Add(FixtureImage.NewFixtureImage(img));
            }
        }

        /// <summary>
        /// Adds copies of the given planogram fixture item and all of its dependants to this 
        /// fixture package.
        /// </summary>
        /// <param name="planFixtureItem">the planogram fixture item to add</param>
        /// <returns>the new fixture item</returns>
        private FixtureItem AddPlanogramFixtureItem(PlanogramFixtureItem planFixtureItem)
        {
            PlanogramFixture planFixture = planFixtureItem.GetPlanogramFixture();
            if (planFixture == null) return null;

            FixtureItem fixtureItem = FixtureItem.NewFixtureItem(planFixtureItem);
            this.FixtureItems.Add(fixtureItem);

            AddPlanogramFixture(planFixture);

            //add linked annotations.
            foreach (PlanogramAnnotation planAnnotation in
                planFixture.Parent.Annotations
                .Where(a =>
                    a.PlanogramPositionId == null
                    && a.PlanogramSubComponentId == null
                    && a.PlanogramFixtureComponentId == null
                    && a.PlanogramFixtureAssemblyId == null
                    && a.PlanogramAssemblyComponentId == null
                    && Object.Equals(planFixtureItem.Id, a.PlanogramFixtureItemId)))
            {
                this.Annotations.Add(planAnnotation);
            }

            return fixtureItem;
        }

        /// <summary>
        /// Adds copies of the given planogram fixture and all of its dependants to this 
        /// fixture package.
        /// </summary>
        /// <param name="planFixtureItem">the planogram fixture to add</param>
        /// <returns>the new fixture</returns>
        private Fixture AddPlanogramFixture(PlanogramFixture planFixture)
        {
            //Add the fixture
            Fixture fixture = Fixture.NewFixture(planFixture);
            this.Fixtures.Add(fixture);

            //copy any required assemblies
            foreach (PlanogramFixtureAssembly planFixtureAssembly in planFixture.Assemblies)
            {
                PlanogramAssembly planAssembly = planFixtureAssembly.GetPlanogramAssembly();
                if (planAssembly == null) continue;
                AddPlanogramAssembly(planAssembly);
            }

            foreach (PlanogramFixtureComponent planFc in planFixture.Components)
            {
                PlanogramComponent planComponent = planFc.GetPlanogramComponent();
                if (planComponent == null) continue;
                AddPlanogramComponent(planComponent);
            }

            //add component annotations
            List<Object> fixtureComponentIds = planFixture.Components.Select(a => a.Id).ToList();
            foreach (PlanogramAnnotation planAnnotation in
                planFixture.Parent.Annotations
                .Where(a =>
                    a.PlanogramPositionId == null
                    && a.PlanogramSubComponentId == null
                    && fixtureComponentIds.Contains(a.PlanogramFixtureComponentId)))
            {
                this.Annotations.Add(planAnnotation);
            }

            return fixture;
        }

        /// <summary>
        /// Adds copies of the given planogram assembly and all of its dependants to this 
        /// fixture package.
        /// </summary>
        /// <param name="planFixtureItem">the planogram assembly to add</param>
        /// <returns>the new fixture assembly</returns>
        private FixtureAssembly AddPlanogramAssembly(PlanogramAssembly planAssembly)
        {
            FixtureAssembly fixtureAssembly = FixtureAssembly.NewFixtureAssembly(planAssembly);
            this.Assemblies.Add(fixtureAssembly);

            //copy any required components.
            // this will also take subcomponents and images.
            foreach (PlanogramAssemblyComponent planAc in planAssembly.Components)
            {
                PlanogramComponent planComponent = planAc.GetPlanogramComponent();
                if (planComponent == null) continue;

                AddPlanogramComponent(planComponent);
            }

            //add component annotations
            List<Object> assemblyComponentIds = planAssembly.Components.Select(a => a.Id).ToList();
            foreach (PlanogramAnnotation planAnnotation in
                planAssembly.Parent.Annotations
                .Where(a =>
                    a.PlanogramPositionId == null
                    && a.PlanogramSubComponentId == null
                    && assemblyComponentIds.Contains(a.PlanogramAssemblyComponentId)))
            {
                this.Annotations.Add(planAnnotation);
            }

            return fixtureAssembly;
        }

        /// <summary>
        /// Adds copies of the given planogram component and all of its dependants to this 
        /// fixture package.
        /// </summary>
        /// <param name="planFixtureItem">the planogram component to add</param>
        /// <returns>the new fixture component</returns>
        private FixtureComponent AddPlanogramComponent(PlanogramComponent planComponent)
        {
            FixtureComponent fixtureComponent = FixtureComponent.NewFixtureComponent(planComponent, this);
            this.Components.Add(fixtureComponent);
            return fixtureComponent;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a child of this model changes.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnChildChanged(Csla.Core.ChildChangedEventArgs e)
        {
            base.OnChildChanged(e);

            if (e.CollectionChangedArgs != null)
            {
                //update count properties
                this.FixtureCount = (this.FixtureItems.Count > 0) ? this.FixtureItems.Count : this.Fixtures.Count;
                this.AssemblyCount = this.Assemblies.Count;
                this.ComponentCount = this.Components.Count;
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    if (!String.IsNullOrEmpty(DalFactoryName))
                    {
                        UnlockFixturePackageByFileName(DalFactoryName);
                    }
                }

                _isDisposed = true;
            }
        }

        #endregion

    }

}