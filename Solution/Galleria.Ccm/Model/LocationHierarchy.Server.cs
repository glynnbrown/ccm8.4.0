﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//		Created (Auto-generated)
// V8-25556 : D.Pleasance
//      Added FetchByEntityIdContext
#endregion
#endregion

using System;
using System.Linq;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    public partial class LocationHierarchy
    {
        #region Constructors
        private LocationHierarchy() { } //force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing LocationHierarchy with the given id
        /// </summary>
        public static LocationHierarchy FetchById(Int32 id)
        {
            return DataPortal.Fetch<LocationHierarchy>(new FetchByIdCriteria(id));
        }

        /// <summary>
        /// Returns the hierarchy for the given entity id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static LocationHierarchy FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<LocationHierarchy>(new FetchByEntityIdCriteria(entityId));
        }

        /// <summary>
        /// Returns the location hiearchy structure for the given entity id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static LocationHierarchy FetchByEntityIdContext(Int32 entityId, IDalContext context)
        {
            return DataPortal.Fetch<LocationHierarchy>(new FetchByEntityIdContextCriteria(entityId, context));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext,
            LocationHierarchyDto dto,
            IEnumerable<LocationLevelDto> childLocationLevelList,
            IEnumerable<LocationGroupDto> childLocationGroupList)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);

            //get the root level dto and load it
            LocationLevelDto rootLevelDto = childLocationLevelList.FirstOrDefault(l => Object.Equals(l.ParentLevelId, null));
            if (rootLevelDto != null)
            {
                LoadProperty<LocationLevel>(RootLevelProperty, LocationLevel.Fetch(dalContext, rootLevelDto, childLocationLevelList));
            }

            //get the root group dto and load
            LocationGroupDto rootGroupDto = childLocationGroupList.FirstOrDefault(l => Object.Equals(l.ParentGroupId, null));
            if (rootGroupDto != null)
            {
                LoadProperty<LocationGroup>(RootGroupProperty, LocationGroup.Fetch(dalContext, rootGroupDto, childLocationGroupList));
            }
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private LocationHierarchyDto GetDataTransferObject()
        {
            return new LocationHierarchyDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                Name = ReadProperty<String>(NameProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty),

            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning FetchById
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            Int32 hierarchyId = criteria.Id;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                // fetch the merchandising hierarch structure dto
                LocationHierarchyDto dto;
                using (ILocationHierarchyDal dal = dalContext.GetDal<ILocationHierarchyDal>())
                {
                    dto = dal.FetchById(hierarchyId);
                }

                //now use the hierarchy id to get all the levels in one go
                IEnumerable<LocationLevelDto> productLevelDtoList;
                using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                {
                    productLevelDtoList = dal.FetchByLocationHierarchyId(hierarchyId);
                }

                //and get all the groups in one go
                IEnumerable<LocationGroupDto> productGroupDtoList;
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    productGroupDtoList = dal.FetchByLocationHierarchyId(hierarchyId);
                }

                //load the hierarchy
                LoadDataTransferObject(dalContext, dto, productLevelDtoList, productGroupDtoList);
            }
        }

        /// <summary>
        /// Called when return FetchByEntityId
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            Int32 entityId = criteria.EntityId;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                // fetch the merchandising hierarch structure dto
                LocationHierarchyDto dto;
                using (ILocationHierarchyDal dal = dalContext.GetDal<ILocationHierarchyDal>())
                {
                    dto = dal.FetchByEntityId(entityId);
                }

                //now use the hierarchy id to get all the levels in one go
                IEnumerable<LocationLevelDto> productLevelDtoList;
                using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                {
                    productLevelDtoList = dal.FetchByLocationHierarchyId(dto.Id);
                }

                //and get all the groups in one go
                IEnumerable<LocationGroupDto> productGroupDtoList;
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    productGroupDtoList = dal.FetchByLocationHierarchyId(dto.Id);
                }

                //load the hierarchy
                LoadDataTransferObject(dalContext, dto, productLevelDtoList, productGroupDtoList);
            }
        }

        /// <summary>
        /// Called when return FetchByEntityIdContextCriteria
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(LocationHierarchy.FetchByEntityIdContextCriteria criteria)
        {
            Int32 entityId = criteria.EntityId;

            // fetch the location hierarch structure dto
            LocationHierarchyDto dto;
            using (ILocationHierarchyDal dal = criteria.Context.GetDal<ILocationHierarchyDal>())
            {
                dto = dal.FetchByEntityId(entityId);
            }

            //now use the hierarchy id to get all the levels in one go
            IEnumerable<LocationLevelDto> locationLevelDtoList;
            using (ILocationLevelDal dal = criteria.Context.GetDal<ILocationLevelDal>())
            {
                locationLevelDtoList = dal.FetchByLocationHierarchyId(dto.Id);
            }

            //and get all the groups in one go
            IEnumerable<LocationGroupDto> locationGroupDtoList;
            using (ILocationGroupDal dal = criteria.Context.GetDal<ILocationGroupDal>())
            {
                locationGroupDtoList = dal.FetchByLocationHierarchyId(dto.Id);
            }

            //load the hierarchy
            LoadDataTransferObject(criteria.Context, dto, locationLevelDtoList, locationGroupDtoList);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                LocationHierarchyDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (ILocationHierarchyDal dal = dalContext.GetDal<ILocationHierarchyDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<LocationHierarchy>(oldId, dto.Id);
                DataPortal.UpdateChild(this.RootLevel, dalContext, this);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this instance
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                LocationHierarchyDto dto = GetDataTransferObject();
                using (ILocationHierarchyDal dal = dalContext.GetDal<ILocationHierarchyDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                DataPortal.UpdateChild(this.RootLevel, dalContext, this);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when this instance is being deleted
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ILocationHierarchyDal dal = dalContext.GetDal<ILocationHierarchyDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #endregion

    }
}