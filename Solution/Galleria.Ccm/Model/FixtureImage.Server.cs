﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class FixtureImage
    {
        #region Constructor
        private FixtureImage() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static FixtureImage Fetch(IDalContext dalContext, FixtureImageDto dto)
        {
            return DataPortal.FetchChild<FixtureImage>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, FixtureImageDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<String>(FileNameProperty, dto.FileName);
            this.LoadProperty<String>(DescriptionProperty, dto.Description);
            this.LoadProperty<Byte[]>(ImageDataProperty, dto.ImageData);

        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private FixtureImageDto GetDataTransferObject(FixturePackage parent)
        {
            return new FixtureImageDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                FixturePackageId = parent.Id,
                FileName = ReadProperty<String>(FileNameProperty),
                Description = ReadProperty<String>(DescriptionProperty),
                ImageData = ReadProperty<Byte[]>(ImageDataProperty),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FixtureImageDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, FixturePackage parent)
        {
            FixtureImageDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IFixtureImageDal dal = dalContext.GetDal<IFixtureImageDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<FixtureImage>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, FixturePackage parent)
        {
            if (this.IsSelfDirty)
            {
                using (IFixtureImageDal dal = dalContext.GetDal<IFixtureImageDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, FixturePackage parent)
        {
            using (IFixtureImageDal dal = dalContext.GetDal<IFixtureImageDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion

    }
}