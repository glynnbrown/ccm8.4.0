﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25886 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class WorkpackagePlanogramParameterValueList : ModelList<WorkpackagePlanogramParameterValueList, WorkpackagePlanogramParameterValue>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(WorkpackagePlanogramParameterValueList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramParameterValueList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.WorkpackageCreate.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramParameterValueList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.WorkpackageEdit.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramParameterValueList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.WorkpackageDelete.ToString()));
        }
        #endregion

        #region Criteria

        #region FetchByWorkpackgeIdCriteria
        /// <summary>
        /// Criteria for FetchByWorkpackgePlanogramParameterId
        /// </summary>
        [Serializable]
        public class FetchByWorkpackgePlanogramParameterIdCriteria : CriteriaBase<FetchByWorkpackgePlanogramParameterIdCriteria>
        {
            #region Properties

            #region WorkpackagePlanogramParameterId
            /// <summary>
            /// WorkpackageId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> WorkpackagePlanogramParameterIdProperty =
                RegisterProperty<Int32>(c => c.WorkpackagePlanogramParameterId);
            /// <summary>
            /// Returns the workpackage id
            /// </summary>
            public Int32 WorkpackagePlanogramParameterId
            {
                get { return this.ReadProperty<Int32>(WorkpackagePlanogramParameterIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByWorkpackgePlanogramParameterIdCriteria(Int32 workpackagePlanogramParameterId)
            {
                this.LoadProperty<Int32>(WorkpackagePlanogramParameterIdProperty, workpackagePlanogramParameterId);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        internal static WorkpackagePlanogramParameterValueList NewWorkpackagePlanogramParameterValueList(IEnumerable<WorkflowTaskParameterValue> values)
        {
            WorkpackagePlanogramParameterValueList item = new WorkpackagePlanogramParameterValueList();
            item.Create(values);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new child instance of this type
        /// </summary>
        protected void Create(IEnumerable<WorkflowTaskParameterValue> values)
        {
            this.RaiseListChangedEvents = false;
            foreach (WorkflowTaskParameterValue workflowValue in values)
            {
                this.Add(WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue(workflowValue));
            }
            this.RaiseListChangedEvents = true;


            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

    }
}
