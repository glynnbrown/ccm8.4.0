﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31934 : A.Heathcote
//      Created.
#endregion
#endregion
using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using System.Collections.Generic;
using System.Linq;


namespace Galleria.Ccm.Model
{
    [Serializable]
   public sealed partial class UserEditorSettingsSelectedColumnList : ModelList<UserEditorSettingsSelectedColumnList, UserEditorSettingsSelectedColumn>
   {
       #region Authorisation Rules
        private static void AddObjectAuthorizationRules()
        {
        }

       #endregion

       #region Factory Methods 

        public static UserEditorSettingsSelectedColumnList NewList()
        {
            UserEditorSettingsSelectedColumnList list = new UserEditorSettingsSelectedColumnList();
            list.Create();
            return list;
        }
       #endregion

       #region methods 

        public void Add(UserEditorSettingsSelectedColumn column)
        {
            if(this.Count == 0 || !this.Contains(column))
            {
                base.Add(column);
            }

            //foreach (UserEditorSettingsSelectedColumn listedcolumn in this)
            //{
            //    if (column.FieldPlaceHolder.Equals(listedcolumn.FieldPlaceHolder)
            //        && column.ColumnType.Equals(listedcolumn.ColumnType))
            //    {
            //        return;
            //    }
                else
                {
                    return;
                }
            }

        //}

        public void Remove(UserEditorSettingsSelectedColumn column)
        {
            UserEditorSettingsSelectedColumn removeUserEditorSettingsSelectedColumn =
                this.FirstOrDefault(p => p.FieldPlaceHolder.Equals(column.FieldPlaceHolder));
            if (removeUserEditorSettingsSelectedColumn != null)
            {
                this.Remove(removeUserEditorSettingsSelectedColumn);
            }
        }
       #endregion
   }
}
