﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : Luong
//   Created (Copied From GFS)
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
#endregion

#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing an Entity
    /// (Root object)
    /// </summary>
    [Serializable]
    public partial class EventLogName : ModelObject<EventLogName>
    {
        #region Properties

        /// <summary>
        /// The unique object id
        /// </summary>
        /// 
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }

        /// <summary>
        /// The Name where the event log record came from.
        /// </summary>
        /// 
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(EventLogName), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(EventLogName), new IsInRole(AuthorizationActions.GetObject, DomainPermission.EventLogGet.ToString()));
            BusinessRules.AddRule(typeof(EventLogName), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(EventLogName), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new Event log name
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static EventLogName NewEventLogName()
        {
            EventLogName item = new EventLogName();
            item.Create();
            return item;
        }

        public static EventLogName NewEventLogName(Int32 id, String name)
        {
            EventLogName item = new EventLogName();
            item.Create(id, name);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, Message.EventLogName_DefaultName);
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 id, String name)
        {
            this.LoadProperty<Int32>(IdProperty, id);
            this.LoadProperty<String>(NameProperty, name);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Overrides
        /// <summary>
        /// Returns a string representation of this object
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }
        #endregion
    }
}
