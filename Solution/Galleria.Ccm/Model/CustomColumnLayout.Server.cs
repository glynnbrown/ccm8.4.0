﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0)
// V8-24700 : A.Silva 
//  Created.
// V8-26351 : A.Silva 
//  Some refactoring to tidy up Custom Column Layout classes.
// V8-27266 : L.Ineson
//  Made sure GetDalfactory methods set the dal factory type.
#endregion

#region Version History: (CCM 8.3)
// V8-31542 : L.Ineson
//  Added more properties to support new datasheet functionality
#endregion


#endregion

using System;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Server side implementation for the model for settings of a grid view.
    /// </summary>
    public sealed partial class CustomColumnLayout
    {
        #region Constructor

        /// <summary>
        ///     Private constructor. Obtain a new instance through the factory methods.
        /// </summary>
        private CustomColumnLayout()
        {
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given file path
        /// </summary>
        public static CustomColumnLayout FetchByFilename(String fileName)
        {
            return FetchByFilename(fileName, /*asReadOnly*/false);
        }

        /// <summary>
        /// Returns an existing item from the given file path
        /// </summary>
        public static CustomColumnLayout FetchByFilename(String fileName, Boolean asReadOnly)
        {
            return DataPortal.Fetch<CustomColumnLayout>(new FetchByIdCriteria(CustomColumnLayoutDalFactoryType.FileSystem, fileName, asReadOnly));
        }

        /// <summary>
        ///     Finds and returns the user <see cref="CustomColumnLayout" /> setting assigned to the provided
        ///     <paramref name="key" />.
        /// </summary>
        /// <param name="key">The key with which to retrieve the file name for the user <see cref="CustomColumnLayout" /> setting.</param>
        /// <returns>
        ///     The user <see cref="CustomColumnLayout" /> setting for the provided <paramref name="key" /> or <c>null</c> if
        ///     there was no user setting.
        /// </returns>
        public static CustomColumnLayout FetchUserColumnLayoutByKey(String key)
        {
            ColumnLayoutSetting userColumnLayoutSetting =
                UserSystemSetting.FetchUserSettings().ColumnLayouts.FirstOrDefault(setting => setting.ScreenKey == key);

            return userColumnLayoutSetting != null && System.IO.File.Exists(userColumnLayoutSetting.ColumnLayoutName)
                ? FetchByFilename(userColumnLayoutSetting.ColumnLayoutName)
                : null;
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Creates a data transfer object from this instance
        /// </summary>
        private CustomColumnLayoutDto GetDataTransferObject()
        {
            return new CustomColumnLayoutDto
            {
                Id = ReadProperty<Object>(IdProperty),
                Name = ReadProperty<String>(NameProperty),
                Description = ReadProperty<String>(DescriptionProperty),
                Type = (Byte)ReadProperty<CustomColumnLayoutType>(TypeProperty),
                HasTotals = ReadProperty(HasTotalsProperty),
                IsTitleShown = ReadProperty<Boolean>(IsTitleShownProperty),
                FrozenColumnCount = ReadProperty<Byte>(FrozenColumnCountProperty),
                IsGroupDetailHidden = ReadProperty<Boolean>(IsGroupDetailHiddenProperty),
                IsGroupBlankRowShown = ReadProperty<Boolean>(IsGroupBlankRowShownProperty),
                DataOrderType = (Byte)ReadProperty<DataSheetDataOrderType>(DataOrderTypeProperty),
                IncludeAllPlanograms = ReadProperty<Boolean>(IncludeAllPlanogramsProperty),
            };
        }

        /// <summary>
        ///     Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, CustomColumnLayoutDto dto,
            CustomColumnLayoutDalFactoryType dalFactoryType)
        {
            LoadProperty(DalFactoryTypeProperty, dalFactoryType);
            LoadProperty<Object>(IdProperty, dto.Id);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<String>(DescriptionProperty, dto.Description);
            LoadProperty<CustomColumnLayoutType>(TypeProperty, (CustomColumnLayoutType)dto.Type);
            LoadProperty<Boolean>(HasTotalsProperty, dto.HasTotals);
            LoadProperty<Boolean>(IsTitleShownProperty, dto.IsTitleShown);
            LoadProperty<Byte>(FrozenColumnCountProperty, dto.FrozenColumnCount);
            LoadProperty<Boolean>(IsGroupDetailHiddenProperty, dto.IsGroupDetailHidden);
            LoadProperty<Boolean>(IsGroupBlankRowShownProperty, dto.IsGroupBlankRowShown);
            LoadProperty<DataSheetDataOrderType>(DataOrderTypeProperty, (DataSheetDataOrderType)dto.DataOrderType);
            LoadProperty<Boolean>(IncludeAllPlanogramsProperty, dto.IncludeAllPlanograms);
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Called when fetching a <see cref="CustomColumnLayout" /> by its Id
        /// </summary>
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            if (criteria.DalFactoryType == CustomColumnLayoutDalFactoryType.FileSystem
                && !criteria.AsReadOnly)
            {
                LockCustomColumnLayoutByFileName((String)criteria.Id);
            }

            IDalFactory dalFactory = GetDalFactory(criteria.DalFactoryType);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ICustomColumnLayoutDal dal = dalContext.GetDal<ICustomColumnLayoutDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchById(criteria.Id), criteria.DalFactoryType);

                    //set readonly flag
                    this.LoadProperty<Boolean>(IsReadOnlyProperty, criteria.AsReadOnly);
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        ///     Called when inserting this item
        /// </summary>
        private new void DataPortal_Insert()
        {
            IDalFactory dalFactory = GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                CustomColumnLayoutDto dto = this.GetDataTransferObject();
                Object oldId = dto.Id;
                using (var dal = dalContext.GetDal<ICustomColumnLayoutDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty(IdProperty, dto.Id);
                dalContext.RegisterId<Object>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        ///     Called when updating an instance of this type
        /// </summary>
        private new void DataPortal_Update()
        {
            IDalFactory dalFactory = GetDalFactory();
            using (var dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                if (this.IsSelfDirty)
                {
                    CustomColumnLayoutDto dto = GetDataTransferObject();
                    using (var dal = dalContext.GetDal<ICustomColumnLayoutDal>())
                    {
                        dal.Update(dto);
                    }
                }
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        ///     Called when deleting an instance of this type
        /// </summary>
        private new void DataPortal_DeleteSelf()
        {
            var dalFactory = GetDalFactory();
            using (var dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (var dal = dalContext.GetDal<ICustomColumnLayoutDal>())
                {
                    dal.DeleteById(Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #endregion

        #region Commands

        #region LockCustomColumnLayoutCommand

        /// <summary>
        ///     Server side implementation of <see cref="LockCustomColumnLayoutCommand" />.
        /// </summary>
        private partial class LockCustomColumnLayoutCommand
        {
            /// <summary>
            ///     Code to execute whenever the dataportal is required to execute a command.
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                var dalFactory = GetDalFactory(this.DalFactoryType, out dalFactoryName);
                using (var dalContext = dalFactory.CreateContext())
                {
                    using (var dal = dalContext.GetDal<ICustomColumnLayoutDal>())
                    {
                        dal.LockById(Id);
                    }
                }
            }
        }

        #endregion

        #region UnlockCustomColumnLayoutCommand

        /// <summary>
        ///     Server side implementation of <see cref="UnlockCustomColumnLayoutCommand" />.
        /// </summary>
        private partial class UnlockCustomColumnLayoutCommand
        {
            /// <summary>
            ///     Code to execute whenever the dataportal is required to execute a command.
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                var dalFactory = GetDalFactory(this.DalFactoryType, out dalFactoryName);
                using (var dalContext = dalFactory.CreateContext())
                {
                    using (var dal = dalContext.GetDal<ICustomColumnLayoutDal>())
                    {
                        dal.UnlockById(Id);
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        ///     Returns the correct dal factory
        /// </summary>
        protected override IDalFactory GetDalFactory()
        {
            if (String.IsNullOrEmpty(this.DalFactoryName))
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.DalFactoryType, out dalFactoryName);
                this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            }

            return DalContainer.GetDalFactory(this.DalFactoryName);
        }

        /// <summary>
        ///     Selects and returns the correct DAL factory based on the given <paramref name="dalType" />.
        /// </summary>
        /// <param name="dalType">Type of DAL factory desired.</param>
        /// <returns>An instance of a class implementing the IDalFactory.</returns>
        private IDalFactory GetDalFactory(CustomColumnLayoutDalFactoryType dalType)
        {
            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(dalType, out dalFactoryName);
            this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            this.LoadProperty<CustomColumnLayoutDalFactoryType>(DalFactoryTypeProperty, dalType);
            return dalFactory;
        }

        /// <summary>
        ///     Selects and returns the correct DAL factory based on the given <paramref name="dalType" />.
        /// </summary>
        /// <param name="dalType">Type of DAL factory desired.</param>
        /// <param name="dalFactoryName"><c>OUT</c> Name of the DAL factory returned.</param>
        /// <returns>An instance of a class implementing the IDalFactory.</returns>
        private static IDalFactory GetDalFactory(CustomColumnLayoutDalFactoryType dalType, out String dalFactoryName)
        {
            switch (dalType)
            {
                case CustomColumnLayoutDalFactoryType.FileSystem:
                    dalFactoryName = Constants.UserDal;
                    return DalContainer.GetDalFactory(dalFactoryName);
                default:
                    dalFactoryName = DalContainer.DalName;
                    return DalContainer.GetDalFactory();
            }
        }

        #endregion
    }
}