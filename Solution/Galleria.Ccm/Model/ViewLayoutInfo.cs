﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Info object for ViewLayout
    /// </summary>
    [Serializable]
    public sealed partial class ViewLayoutInfo : ModelReadOnlyObject<ViewLayoutInfo>
    {
        #region Properties

        private static readonly ModelPropertyInfo<Int32> IdProperty =
           RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The unique object id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The name of the view layout
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ViewLayoutInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ViewLayoutInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(ViewLayoutInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ViewLayoutInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        #endregion

    }
}
