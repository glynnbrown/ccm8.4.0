﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion

#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentRegion
    {
        #region Constructor
        private AssortmentRegion() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating an instance from a dto
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new instance of this type</returns>
        internal static AssortmentRegion GetAssortmentRegion(IDalContext dalContext, AssortmentRegionDto dto)
        {
            return DataPortal.FetchChild<AssortmentRegion>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, AssortmentRegionDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<AssortmentRegionLocationList>(LocationsProperty, AssortmentRegionLocationList.GetAssortmentRegionLocationList(dalContext, dto.Id));
            LoadProperty<AssortmentRegionProductList>(ProductsProperty, AssortmentRegionProductList.GetAssortmentRegionProductList(dalContext, dto.Id));
        }

        /// <summary>
        /// Returns a dto created from this instance
        /// </summary>
        /// <returns>A data transfer object</returns>
        private AssortmentRegionDto GetDataTransferObject(Assortment parent)
        {
            return new AssortmentRegionDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                Name = ReadProperty<String>(NameProperty),
                AssortmentId = parent.Id,
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when this instance is being loaded
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, AssortmentRegionDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when this instance is being inserted into the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent assortment</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, Assortment parent)
        {
            using (IAssortmentRegionDal dal = dalContext.GetDal<IAssortmentRegionDal>())
            {
                AssortmentRegionDto dto = GetDataTransferObject(parent);
                dal.Insert(dto);
                this.LoadProperty<Int32>(IdProperty, dto.Id);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when this instance is being updated in the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent assortment</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, Assortment parent)
        {
            using (IAssortmentRegionDal dal = dalContext.GetDal<IAssortmentRegionDal>())
            {
                dal.Update(GetDataTransferObject(parent));
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this instance is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        private void Child_DeleteSelf(IDalContext dalContext, Assortment parent)
        {
            using (IAssortmentRegionDal dal = dalContext.GetDal<IAssortmentRegionDal>())
            {
                dal.DeleteById(this.Id);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #endregion
    }
}