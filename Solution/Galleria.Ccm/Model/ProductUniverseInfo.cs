﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630: A.Kuszyk
//  Created (copied from SA).
// V8-26520: J.Pickup
//  Added FriendlyProductGroupName property
#endregion
#region Version History: (CCM 8.0.3)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A class representing info on a product universe within GFS
    /// (child of ProductUniverseInfoList)
    /// </summary>
    [Serializable]
    public sealed partial class ProductUniverseInfo : ModelReadOnlyObject<ProductUniverseInfo>
    {
        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The unique object id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// Unique content reference property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Guid> UniqueContentReferenceProperty =
            RegisterModelProperty<Guid>(c => c.UniqueContentReference);
        /// <summary>
        /// Unique content reference
        /// </summary>
        public Guid UniqueContentReference
        {
            get { return GetProperty<Guid>(UniqueContentReferenceProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        /// <summary>
        /// The entity id
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
        }

        public static readonly ModelPropertyInfo<Int32?> ProductGroupIdProperty =
            RegisterModelProperty<Int32?>(c => c.ProductGroupId);
        /// <summary>
        /// The product group id
        /// </summary>
        public Int32? ProductGroupId
        {
            get { return GetProperty<Int32?>(ProductGroupIdProperty); }
        }

        public static readonly ModelPropertyInfo<String> ProductGroupCodeProperty =
            RegisterModelProperty<String>(c => c.ProductGroupCode);
        /// <summary>
        /// The code of the product group to which this info is assigned.
        /// </summary>
        public String ProductGroupCode
        {
            get { return GetProperty<String>(ProductGroupCodeProperty); }
        }


        public static readonly ModelPropertyInfo<String> ProductGroupNameProperty =
            RegisterModelProperty<String>(c => c.ProductGroupName);
        /// <summary>
        /// The name of the product group to which this info is assigned.
        /// </summary>
        public String ProductGroupName
        {
            get { return GetProperty<String>(ProductGroupNameProperty); }
        }


        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The product universe name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        public static readonly ModelPropertyInfo<Boolean> IsMasterProperty =
            RegisterModelProperty<Boolean>(c => c.IsMaster);
        /// <summary>
        /// Flag to indicate if the universe is the master
        /// </summary>
        public Boolean IsMaster
        {
            get { return GetProperty<Boolean>(IsMasterProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> ProductCountProperty =
            RegisterModelProperty<Int32>(c => c.ProductCount, Message.ProductUniverseInfo_ProductCount);
        /// <summary>
        /// The product universe product count
        /// </summary>
        public Int32 ProductCount
        {
            get { return GetProperty<Int32>(ProductCountProperty); }
        }

        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
        }

        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
        }

        /// <summary>
        /// Parent unique content reference property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Guid?> ParentUniqueContentReferenceProperty =
            RegisterModelProperty<Guid?>(c => c.ParentUniqueContentReference);
        /// <summary>
        /// Parent unique content reference
        /// </summary>
        public Guid? ParentUniqueContentReference
        {
            get { return GetProperty<Guid?>(ParentUniqueContentReferenceProperty); }
        }

        #endregion

        #region Helper Properties

        public static readonly ModelPropertyInfo<String> FriendlyProductGroupNameProperty =
            RegisterModelProperty<String>(c => c.FriendlyProductGroupName);
        /// <summary>
        /// A friendly combination of the proudct group code and product group name
        /// </summary>
        public String FriendlyProductGroupName
        {
            get { return (GetProperty<String>(ProductGroupCodeProperty) + " : " + GetProperty<String>(ProductGroupNameProperty)); }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ProductUniverseInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ProductUniverseInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(ProductUniverseInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ProductUniverseInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name + " (" + this.ProductGroupName + ")";
        }

        #endregion
    }
}
