﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26234 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class RoleInfo
    {
        #region Constructors
        private RoleInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the specified role information
        /// </summary>
        /// <param name="userId">The role id</param>
        /// <returns>The role info object</returns>
        public static RoleInfo GetRoleInfoById(Int32 roleId)
        {
            return DataPortal.Fetch<RoleInfo>(new SingleCriteria<Int32>(roleId));
        }

        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static RoleInfo GetRoleInfo(IDalContext dalContext, RoleInfoDto dto)
        {
            return DataPortal.FetchChild<RoleInfo>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, RoleInfoDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<String>(DescriptionProperty, dto.Description);
            LoadProperty<Boolean>(IsAdministratorRoleProperty, dto.IsAdministrator);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when loading this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        private void Child_Fetch(IDalContext dalContext, RoleInfoDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        /// <summary>
        /// Called when fetching a single role
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext context = dalFactory.CreateContext())
            {
                using (IRoleInfoDal dal = context.GetDal<IRoleInfoDal>())
                {
                    this.LoadDataTransferObject(context, dal.FetchById(criteria.Value));
                }
            }
        }

        #endregion

        #endregion
    }
}
