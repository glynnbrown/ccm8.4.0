﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
// V8-26182 : L.Ineson
//  Added NewPlanogramFixtureAssembly
#endregion
#region Version History: (CCM CCM830)
// V8-32524 : L.Ineson
//  Added FixtureAnnotations and simplified updated back to plan
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// FixtureAssemblyItem Model object
    /// </summary>
    [Serializable]
    public sealed partial class FixtureAssemblyItem : ModelObject<FixtureAssemblyItem>
    {
        #region Static Constructor
        static FixtureAssemblyItem()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Fixture Parent
        {
            get
            {
                FixtureAssemblyItemList parentList = base.Parent as FixtureAssemblyItemList;
                if (parentList != null)
                {
                    return parentList.Parent;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region FixtureAssemblyId

        /// <summary>
        /// FixtureAssemblyId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FixtureAssemblyIdProperty =
            RegisterModelProperty<Int32>(c => c.FixtureAssemblyId);

        /// <summary>
        /// Gets/Sets the FixtureAssemblyId value
        /// </summary>
        public Int32 FixtureAssemblyId
        {
            get { return GetProperty<Int32>(FixtureAssemblyIdProperty); }
            set { SetProperty<Int32>(FixtureAssemblyIdProperty, value); }
        }

        #endregion

        #region X

        /// <summary>
        /// X property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> XProperty =
            RegisterModelProperty<Single>(c => c.X);

        /// <summary>
        /// Gets/Sets the X value
        /// </summary>
        public Single X
        {
            get { return GetProperty<Single>(XProperty); }
            set { SetProperty<Single>(XProperty, value); }
        }

        #endregion

        #region Y

        /// <summary>
        /// Y property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> YProperty =
            RegisterModelProperty<Single>(c => c.Y);

        /// <summary>
        /// Gets/Sets the Y value
        /// </summary>
        public Single Y
        {
            get { return GetProperty<Single>(YProperty); }
            set { SetProperty<Single>(YProperty, value); }
        }

        #endregion

        #region Z

        /// <summary>
        /// Z property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ZProperty =
            RegisterModelProperty<Single>(c => c.Z);

        /// <summary>
        /// Gets/Sets the Z value
        /// </summary>
        public Single Z
        {
            get { return GetProperty<Single>(ZProperty); }
            set { SetProperty<Single>(ZProperty, value); }
        }

        #endregion

        #region Slope

        /// <summary>
        /// Slope property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SlopeProperty =
            RegisterModelProperty<Single>(c => c.Slope);

        /// <summary>
        /// Gets/Sets the Slope value
        /// </summary>
        public Single Slope
        {
            get { return GetProperty<Single>(SlopeProperty); }
            set { SetProperty<Single>(SlopeProperty, value); }
        }

        #endregion

        #region Angle

        /// <summary>
        /// Angle property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AngleProperty =
            RegisterModelProperty<Single>(c => c.Angle);

        /// <summary>
        /// Gets/Sets the Angle value
        /// </summary>
        public Single Angle
        {
            get { return GetProperty<Single>(AngleProperty); }
            set { SetProperty<Single>(AngleProperty, value); }
        }

        #endregion

        #region Roll

        /// <summary>
        /// Roll property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> RollProperty =
            RegisterModelProperty<Single>(c => c.Roll);

        /// <summary>
        /// Gets/Sets the Roll value
        /// </summary>
        public Single Roll
        {
            get { return GetProperty<Single>(RollProperty); }
            set { SetProperty<Single>(RollProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();


        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static FixtureAssemblyItem NewFixtureAssemblyItem()
        {
            FixtureAssemblyItem item = new FixtureAssemblyItem();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        internal static FixtureAssemblyItem NewFixtureAssemblyItem(PlanogramFixtureAssembly planAssemblyItem)
        {
            FixtureAssemblyItem item = new FixtureAssemblyItem();
            item.Create(planAssemblyItem);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }


        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create(PlanogramFixtureAssembly planAssemblyItem)
        {
            this.LoadProperty<Int32>(IdProperty, (Int32)planAssemblyItem.Id);
            this.LoadProperty<Int32>(FixtureAssemblyIdProperty, (Int32)planAssemblyItem.PlanogramAssemblyId);
            this.LoadProperty<Single>(XProperty, planAssemblyItem.X);
            this.LoadProperty<Single>(YProperty, planAssemblyItem.Y);
            this.LoadProperty<Single>(ZProperty, planAssemblyItem.Z);
            this.LoadProperty<Single>(SlopeProperty, planAssemblyItem.Slope);
            this.LoadProperty<Single>(AngleProperty, planAssemblyItem.Angle);
            this.LoadProperty<Single>(RollProperty, planAssemblyItem.Roll);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<FixtureAssemblyItem>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes on this instance
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }

        /// <summary>
        /// Called when a copy operation completes on this instance
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // FixtureAssemblyId
            Object fixtureAssemblyId = context.ResolveId<FixtureAssembly>(this.ReadProperty<Int32>(FixtureAssemblyIdProperty));
            if (fixtureAssemblyId != null) this.LoadProperty<Int32>(FixtureAssemblyIdProperty, (Int32)fixtureAssemblyId);
        }

        /// <summary>
        /// Updates this fixture assembly in the given planogram fixture.
        /// </summary>
        public PlanogramFixtureAssembly AddOrUpdatePlanogramFixtureAssembly(PlanogramFixture fixture)
        {
            //update the assembly first.
            FixtureAssembly fa = GetFixtureAssembly();
            if (fa != null) fa.AddOrUpdatePlanogramAssembly(fixture.Parent);


            PlanogramFixtureAssembly fixtureAssembly = fixture.Assemblies.FindById(this.Id);
            if (fixtureAssembly == null)
            {
                fixtureAssembly = PlanogramFixtureAssembly.NewPlanogramFixtureAssembly();
                fixtureAssembly.Id = this.Id;
                fixture.Assemblies.Add(fixtureAssembly);
            }

            fixtureAssembly.X = this.X;
            fixtureAssembly.Y = this.Y;
            fixtureAssembly.Z = this.Z;
            fixtureAssembly.Slope = this.Slope;
            fixtureAssembly.Angle = this.Angle;
            fixtureAssembly.Roll = this.Roll;
            fixtureAssembly.PlanogramAssemblyId = this.FixtureAssemblyId;

            

            return fixtureAssembly;
        }

        /// <summary>
        /// Returns the linked fixture assembly
        /// </summary>
        /// <returns></returns>
        public FixtureAssembly GetFixtureAssembly()
        {
            if (this.FixtureAssemblyId == null) return null;

            if (this.Parent == null) return null;
            FixturePackage package = this.Parent.Parent;
            if (package == null) return null;

            return package.Assemblies.FindById(this.FixtureAssemblyId);
        }

        #endregion
    }
}