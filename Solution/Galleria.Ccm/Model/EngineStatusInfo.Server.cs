﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-27411 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Engine.Dal.DataTransferObjects;
using Galleria.Framework.Engine.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class EngineStatusInfo
    {
        #region Constructors
        private EngineStatusInfo() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the EngineStatusInfo items for the current user
        /// </summary>
        /// <param name="entityId">Entity Id</param>
        /// <returns>SystemSettings Object</returns>
        public static EngineStatusInfo FetchEngineStatusInfo()
        {
            return DataPortal.Fetch<EngineStatusInfo>();
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, EngineMessageStatsDto dto)
        {
            this.LoadProperty<Int32>(MessageCountProperty, dto.MessageCount);
            this.LoadProperty<Int32>(ProcessedMessageCountProperty, dto.MessageProcessCount);

            this.LoadProperty<EngineInstanceInfoList>(EngineInstancesProperty, 
                EngineInstanceInfoList.GetEngineInstanceInfoList(dalContext));
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning FetchById
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                EngineMessageStatsDto dto;
                using (IEngineMessageDal dal = dalContext.GetDal<IEngineMessageDal>())
                {
                    dto = dal.FetchStats();
                }

                //Load the dto and list
                LoadDataTransferObject(dalContext, dto);
            }
        }

        #endregion

        #endregion
    }
}
