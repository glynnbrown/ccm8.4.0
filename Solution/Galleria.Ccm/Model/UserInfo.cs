﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-26222 : L.Luong
//		Created (Auto-generated)

#endregion
#region Version History: CCM811
// V8-30463 : L.Ineson
//  Added GetIdValue override
#endregion
#region Version History: CCM830
// V8-31514 : L.Ineson
//  Added FullName helper property
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using System.Globalization;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model object representing a readonly cutdown of a User
    /// (child of UserInfoList)
    /// </summary>
    [Serializable]
    public sealed partial class UserInfo : ModelReadOnlyObject<UserInfo>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(UserInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(UserInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(UserInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(UserInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The unique object id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<String> UserNameProperty =
            RegisterModelProperty<String>(c => c.UserName);
        /// <summary>
        /// The users username
        /// </summary>
        public String UserName
        {
            get { return GetProperty<String>(UserNameProperty); }
        }


        public static readonly ModelPropertyInfo<String> FirstNameProperty =
            RegisterModelProperty<String>(c => c.FirstName);
        /// <summary>
        /// The users first name
        /// </summary>
        public String FirstName
        {
            get { return GetProperty<String>(FirstNameProperty); }
        }


        public static readonly ModelPropertyInfo<String> LastNameProperty =
            RegisterModelProperty<String>(c => c.LastName);
        /// <summary>
        /// The users last name
        /// </summary>
        public String LastName
        {
            get { return GetProperty<String>(LastNameProperty); }
        }

        /// <summary>
        /// Helper property to return the full name
        /// of this user.
        /// </summary>
        public String FullName
        {
            get
            {
                return String.Format(CultureInfo.CurrentCulture, "{0} {1}", this.FirstName, this.LastName);
            }
        }

        #endregion

        #region Override

        public override String ToString()
        {
            return this.UserName;
        }

        protected override object GetIdValue()
        {
            return this.Id;
        }

        #endregion
    }
}
