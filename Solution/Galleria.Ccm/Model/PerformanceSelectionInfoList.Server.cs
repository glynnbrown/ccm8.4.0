﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26159 : L.Ineson
//      Created
// CCM-27172 : I.George
//      Added FetchByEntityIdIncludingDeletedCriteria
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class PerformanceSelectionInfoList
    {
        #region Constructors
        private PerformanceSelectionInfoList() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        public static PerformanceSelectionInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<PerformanceSelectionInfoList>(new FetchByEntityIdCriteria(entityId));
        }

        public static PerformanceSelectionInfoList FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            return DataPortal.Fetch<PerformanceSelectionInfoList>(new FetchByEntityIdIncludingDeletedCriteria(entityId));
        }
        #endregion

        #region Data Access

        /// <summary>
        /// Called when retrieving a list of all entity infos
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPerformanceSelectionInfoDal dal = dalContext.GetDal<IPerformanceSelectionInfoDal>())
                {
                    IEnumerable<PerformanceSelectionInfoDto> dtoList = dal.FetchByEntityId(criteria.EntityId);
                    foreach (PerformanceSelectionInfoDto dto in dtoList)
                    {
                        this.Add(PerformanceSelectionInfo.GetPerformanceSelectionInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when retrieeving a list of all PerformanceSelection for an Entity
        /// this procedure returns deleted items
        /// </summary>
        /// <param name="criteria"></param>
        private void DataPortal_Fetch(FetchByEntityIdIncludingDeletedCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPerformanceSelectionInfoDal dal = dalContext.GetDal<IPerformanceSelectionInfoDal>())
                {
                    IEnumerable<PerformanceSelectionInfoDto> dtoList = dal.FetchByEntityIdIncludingDeleted(criteria.EntityId);
                    foreach (PerformanceSelectionInfoDto dto in dtoList)
                    {
                        this.Add(PerformanceSelectionInfo.GetPerformanceSelectionInfo(dalContext, dto));
                    }
                }
            }

            this.RaiseListChangedEvents = true;
            this.IsReadOnly = true;
        }
        #endregion
    }
}
