﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  Added ProcessingStatusDescription, ProgressDateStarted, ProgressDateLastUpdated properties
//  Added DateCompleted and UserId, UserFullName properties
// V8-28493 : L.Ineson
//  Added planogram status counts
#endregion
#region Version History: CCM810
// V8-29811 : A.Probyn
// Added PlanogramLocationType
// V8-29842 : A.Probyn
//  Extended to include WorkpackageType
// V8-29590 : A.Probyn
//      Extended for new PlanogramWarningCount, PlanogramTotalErrorScore, PlanogramHighestErrorScore
// V8-30079 : L.Ineson
//  Ammended status property enum type.
#endregion
#region Version History: CCM820
// V8-30887 : L.Ineson
//  Added workflow name
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class WorkpackageInfo : ModelReadOnlyObject<WorkpackageInfo>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the worflow id
        /// </summary>
        public Int32 Id
        {
            get { return this.ReadProperty<Int32>(IdProperty); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        internal static ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// Returns the workflow row version
        /// </summary>
        internal RowVersion RowVersion
        {
            get { return this.ReadProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Returns the worflow name
        /// </summary>
        public String Name
        {
            get { return this.ReadProperty<String>(NameProperty); }
        }
        #endregion

        #region UserId
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> UserIdProperty =
            RegisterModelProperty<Int32>(c => c.UserId);
        /// <summary>
        /// Gets or sets the workflow UserId
        /// </summary>
        public Int32 UserId
        {
            get { return this.GetProperty<Int32>(UserIdProperty); }
        }
        #endregion

        #region UserFullName
        /// <summary>
        /// UserFullName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> UserFullNameProperty =
            RegisterModelProperty<String>(c => c.UserFullName);
        /// <summary>
        /// Returns the workpackage UserFullName (Workpackage owner)
        /// </summary>
        public String UserFullName
        {
            get { return this.ReadProperty<String>(UserFullNameProperty); }
        }
        #endregion

        #region PlanogramLocationType
        /// <summary>
        /// PlanogramLocationType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<WorkpackagePlanogramLocationType> PlanogramLocationTypeProperty =
            RegisterModelProperty<WorkpackagePlanogramLocationType>(c => c.PlanogramLocationType);
        /// <summary>
        /// Returns the PlanogramLocationType
        /// </summary>
        public WorkpackagePlanogramLocationType PlanogramLocationType
        {
            get { return this.ReadProperty<WorkpackagePlanogramLocationType>(PlanogramLocationTypeProperty); }
        }
        #endregion

        #region WorkpackageType
        /// <summary>
        /// WorkpackageType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<WorkpackageType> WorkpackageTypeProperty =
            RegisterModelProperty<WorkpackageType>(c => c.WorkpackageType);
        /// <summary>
        /// Returns the workpackage type
        /// </summary>
        public WorkpackageType WorkpackageType
        {
            get { return this.ReadProperty<WorkpackageType>(WorkpackageTypeProperty); }
        }
        #endregion
        
        #region PlanogramCount
        /// <summary>
        /// PlanogramCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanogramCountProperty =
            RegisterModelProperty<Int32>(c => c.PlanogramCount);
        /// <summary>
        /// Returns a count of the number of plans created by this workflow.
        /// NB- This does not include debug plans.
        /// </summary>
        public Int32 PlanogramCount
        {
            get { return this.ReadProperty<Int32>(PlanogramCountProperty); }
        }
        #endregion

        #region PlanogramPendingCount
        /// <summary>
        /// PlanogramPendingCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanogramPendingCountProperty =
            RegisterModelProperty<Int32>(c => c.PlanogramPendingCount);
        /// <summary>
        /// Returns a count of the number of plans created by this workflow.
        /// NB- This does not include debug plans.
        /// </summary>
        public Int32 PlanogramPendingCount
        {
            get { return this.ReadProperty<Int32>(PlanogramPendingCountProperty); }
        }
        #endregion

        #region PlanogramQueuedCount
        /// <summary>
        /// PlanogramQueuedCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanogramQueuedCountProperty =
            RegisterModelProperty<Int32>(c => c.PlanogramQueuedCount);
        /// <summary>
        /// Returns a count of the number of plans created by this workflow that have status of queued
        /// NB- This does not include debug plans.
        /// </summary>
        public Int32 PlanogramQueuedCount
        {
            get { return this.ReadProperty<Int32>(PlanogramQueuedCountProperty); }
        }
        #endregion

        #region PlanogramProcessingCount
        /// <summary>
        /// PlanogramProcessingCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanogramProcessingCountProperty =
            RegisterModelProperty<Int32>(c => c.PlanogramProcessingCount);
        /// <summary>
        /// Returns a count of the number of plans created by this workflow that have a status of processing
        /// NB- This does not include debug plans.
        /// </summary>
        public Int32 PlanogramProcessingCount
        {
            get { return this.ReadProperty<Int32>(PlanogramProcessingCountProperty); }
        }
        #endregion

        #region PlanogramCompleteCount
        /// <summary>
        /// PlanogramCompleteCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanogramCompleteCountProperty =
            RegisterModelProperty<Int32>(c => c.PlanogramCompleteCount);
        /// <summary>
        /// Returns a count of the number of plans created by this workflow that have a status of complete
        /// NB- This does not include debug plans.
        /// </summary>
        public Int32 PlanogramCompleteCount
        {
            get { return this.ReadProperty<Int32>(PlanogramCompleteCountProperty); }
        }
        #endregion

        #region PlanogramFailedCount
        /// <summary>
        /// PlanogramFailedCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanogramFailedCountProperty =
            RegisterModelProperty<Int32>(c => c.PlanogramFailedCount);
        /// <summary>
        /// Returns a count of the number of plans created by this workflow that have a status of failed.
        /// NB- This does not include debug plans.
        /// </summary>
        public Int32 PlanogramFailedCount
        {
            get { return this.ReadProperty<Int32>(PlanogramFailedCountProperty); }
        }
        #endregion

        #region DateCreated
        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);
        /// <summary>
        /// Returns the utc datetime at which the workpackage was created.
        /// </summary>
        public DateTime DateCreated
        {
            get { return this.ReadProperty<DateTime>(DateCreatedProperty); }
        }
        #endregion

        #region DateLastModified
        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        /// <summary>
        /// Returns the utc datetime at which the workpackage was last modified
        /// </summary>
        public DateTime DateLastModified
        {
            get { return this.ReadProperty<DateTime>(DateLastModifiedProperty); }
        }
        #endregion

        #region DateCompleted
        /// <summary>
        /// DateCompleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateCompletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateCompleted);
        /// <summary>
        /// Returns the utc datetime at which the workpackage was last modified
        /// </summary>
        public DateTime? DateCompleted
        {
            get 
            {
                WorkpackageProcessingStatusType status = this.ReadProperty<WorkpackageProcessingStatusType>(ProcessingStatusProperty);

                if (status == WorkpackageProcessingStatusType.Failed
                    ||status == WorkpackageProcessingStatusType.Complete
                    || status == WorkpackageProcessingStatusType.CompleteWithAllWarnings
                    || status == WorkpackageProcessingStatusType.CompleteWithSomeFailed
                    || status == WorkpackageProcessingStatusType.CompleteWithSomeWarnings)
                {
                    if (this.ReadProperty<DateTime?>(ProgressDateLastUpdatedProperty) != null)
                    {
                        return this.ReadProperty<DateTime?>(ProgressDateLastUpdatedProperty);
                    }
                }
                return null;
            }
        }
        #endregion

        #region ProcessingStatus
        /// <summary>
        /// ProcessingStatus property definition
        /// </summary>
        public static readonly ModelPropertyInfo<WorkpackageProcessingStatusType> ProcessingStatusProperty =
            RegisterModelProperty<WorkpackageProcessingStatusType>(c => c.ProcessingStatus);
        /// <summary>
        /// Returns the processing status for the workpackage
        /// </summary>
        public WorkpackageProcessingStatusType ProcessingStatus
        {
            get { return this.ReadProperty<WorkpackageProcessingStatusType>(ProcessingStatusProperty); }
        }
        #endregion

        #region ProcessingStatusDescription
        /// <summary>
        /// ProcessingStatusDescription property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ProcessingStatusDescriptionProperty =
            RegisterModelProperty<String>(c => c.ProcessingStatusDescription);
        /// <summary>
        /// Returns the  processing status description
        /// </summary>
        public String ProcessingStatusDescription
        {
            get { return this.ReadProperty<String>(ProcessingStatusDescriptionProperty); }
        }
        #endregion

        #region ProgressMax
        /// <summary>
        /// ProgressMax property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProgressMaxProperty =
            RegisterModelProperty<Int32>(c => c.ProgressMax);
        /// <summary>
        /// Returns the maximum progress value for this workpackage
        /// </summary>
        public Int32 ProgressMax
        {
            get { return this.ReadProperty<Int32>(ProgressMaxProperty); }
        }
        #endregion

        #region ProgressCurrent
        /// <summary>
        /// ProgressCurrent property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProgressCurrentProperty =
            RegisterModelProperty<Int32>(c => c.ProgressCurrent);
        /// <summary>
        /// Returns the current progress value for this workpackage
        /// </summary>
        public Int32 ProgressCurrent
        {
            get { return this.ReadProperty<Int32>(ProgressCurrentProperty); }
        }
        #endregion

        #region Progress
        /// <summary>
        /// Progress property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ProgressProperty =
            RegisterModelProperty<Single>(c => c.Progress);
        /// <summary>
        /// Returns the percentage progress of this workflow
        /// </summary>
        public Single Progress
        {
            get { return this.ReadProperty<Single>(ProgressProperty); }
        }
        #endregion

        #region ProgressDateStarted
        /// <summary>
        /// ProgressStartDate property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> ProgressDateStartedProperty =
            RegisterModelProperty<DateTime?>(c => c.ProgressDateStarted);
        /// <summary>
        /// Gets or sets the date the current  process was started
        /// </summary>
        public DateTime? ProgressDateStarted
        {
            get { return this.ReadProperty<DateTime?>(ProgressDateStartedProperty); }
        }
        #endregion

        #region ProgressDateLastUpdated
        /// <summary>
        /// Date Last Updated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> ProgressDateLastUpdatedProperty =
            RegisterModelProperty<DateTime?>(c => c.ProgressDateLastUpdated);
        /// <summary>
        /// Gets or sets the date the current  process was last updated
        /// </summary>
        public DateTime? ProgressDateLastUpdated
        {
            get { return this.ReadProperty<DateTime?>(ProgressDateLastUpdatedProperty); }
        }
        #endregion

        #region EntityId
        /// <summary>
        /// EntityId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        /// <summary>
        /// Gets the workpackage entity id
        /// </summary>
        public Int32 EntityId
        {
            get { return this.ReadProperty<Int32>(EntityIdProperty); }
        }
        #endregion
        
        #region PlanogramWarningCount
        /// <summary>
        /// PlanogramWarningCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanogramWarningCountProperty =
            RegisterModelProperty<Int32>(c => c.PlanogramWarningCount);
        /// <summary>
        /// Returns a count of the number of plans created by this workflow that have a status of Warning.
        /// NB- This does not include debug plans.
        /// </summary>
        public Int32 PlanogramWarningCount
        {
            get { return this.ReadProperty<Int32>(PlanogramWarningCountProperty); }
        }
        #endregion

        #region PlanogramTotalErrorScore
        /// <summary>
        /// PlanogramTotalErrorScore property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanogramTotalErrorScoreProperty =
            RegisterModelProperty<Int32>(c => c.PlanogramTotalErrorScore);
        /// <summary>
        /// Returns a the total error score for the workpackages planogram event logs
        /// </summary>
        public Int32 PlanogramTotalErrorScore
        {
            get { return this.ReadProperty<Int32>(PlanogramTotalErrorScoreProperty); }
        }
        #endregion

        #region PlanogramHighestErrorScore
        /// <summary>
        /// PlanogramHighestErrorScore property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanogramHighestErrorScoreProperty =
            RegisterModelProperty<Int32>(c => c.PlanogramHighestErrorScore);
        /// <summary>
        /// Returns a the highest error score for the workpackages planogram event logs
        /// </summary>
        public Int32 PlanogramHighestErrorScore
        {
            get { return this.ReadProperty<Int32>(PlanogramHighestErrorScoreProperty); }
        }
        #endregion

        #region WorkflowName

        /// <summary>
        /// WorkflowName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> WorkflowNameProperty =
            RegisterModelProperty<String>(c => c.WorkflowName);
        /// <summary>
        /// Gets the name of the workflow used by this workpackage
        /// </summary>
        public String WorkflowName
        {
            get { return this.ReadProperty<String>(WorkflowNameProperty); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(WorkpackageInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(WorkpackageInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkpackageInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(WorkpackageInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Returns the workflow name
        /// </summary>
        public override String ToString()
        {
            return this.Name;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Submits the workpackage for
        /// execution by the engine
        /// </summary>
        public void Execute()
        {
            Workpackage.Execute(this.EntityId, this.Id, this.Name, this.RowVersion);
        }
        #endregion
    }
}
