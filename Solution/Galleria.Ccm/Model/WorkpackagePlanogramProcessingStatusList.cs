﻿using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class WorkpackagePlanogramProcessingStatusList : ModelList<WorkpackagePlanogramProcessingStatusList, WorkpackagePlanogramProcessingStatus>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(WorkpackagePlanogramProcessingStatusList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramProcessingStatusList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramProcessingStatusList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramProcessingStatusList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Authenticated.ToString()));
        }
        #endregion


        #region Criteria
        /// <summary>
        /// Criteria for the FetchByPlanogramId
        /// </summary>
        [Serializable]
        public class FetchByPlanogramIdCriteria : CriteriaBase<FetchByPlanogramIdCriteria>
        {
            #region Properties

            public static PropertyInfo<Int32> PlanogramIdProperty =
                RegisterProperty<Int32>(c => c.PlanogramId);
            public Int32 PlanogramId
            {
                get { return ReadProperty<Int32>(PlanogramIdProperty); }
            }

            #endregion

            public FetchByPlanogramIdCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(PlanogramIdProperty, entityId);
            }
        }

        #endregion
    }
}
