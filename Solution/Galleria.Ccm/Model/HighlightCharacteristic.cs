﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-V8-24801 : L.Hodson
//	Created (Auto-generated)
// V8-25526 : K.Pickup
//  Use RelationshipTypes.LazyLoad when registering lazy-loaded model properties.
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
#endregion
#region Version History: (CCM 8.10)
// V8-28661 : L.Ineson
//  Implemented IPlanogramHighlight interface
#endregion
#region Version History: (CCM 8.20)
// V8-31308 : A.Probyn
//  Adjusted business rule to match schema. max length changed to 255 from 100.
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// HighlightCharacteristic Model object
    /// </summary>
    [Serializable]
    public sealed partial class HighlightCharacteristic : ModelObject<HighlightCharacteristic>, IPlanogramHighlightCharacteristic
    {
        #region Static Constructor
        static HighlightCharacteristic()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Highlight Parent
        {
            get
            {
                HighlightCharacteristicList parentList = base.Parent as HighlightCharacteristicList;
                if (parentList != null)
                {
                    return parentList.Parent;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            private set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region IsAndFilter

        /// <summary>
        /// IsAndFilter property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsAndFilterProperty =
            RegisterModelProperty<Boolean>(c => c.IsAndFilter);

        /// <summary>
        /// Gets/Sets the IsAndFilter value
        /// </summary>
        public Boolean IsAndFilter
        {
            get { return GetProperty<Boolean>(IsAndFilterProperty); }
            set { SetProperty<Boolean>(IsAndFilterProperty, value); }
        }

        #endregion

        #region Rules
        /// <summary>
        /// Rules property definition
        /// </summary>
        public static readonly ModelPropertyInfo<HighlightCharacteristicRuleList> RulesProperty =
            RegisterModelProperty<HighlightCharacteristicRuleList>(c => c.Rules, RelationshipTypes.LazyLoad);
        /// <summary>
        /// List of rules
        /// </summary>
        public HighlightCharacteristicRuleList Rules
        {
            get {return this.GetProperty<HighlightCharacteristicRuleList>(RulesProperty);}
        }

        IEnumerable<IPlanogramHighlightCharacteristicRule> IPlanogramHighlightCharacteristic.Rules
        {
            get { return this.GetProperty<HighlightCharacteristicRuleList>(RulesProperty); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 255));

        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static HighlightCharacteristic NewHighlightCharacteristic()
        {
            HighlightCharacteristic item = new HighlightCharacteristic();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Boolean>(IsAndFilterProperty, true);
            this.LoadProperty<HighlightCharacteristicRuleList>(RulesProperty, HighlightCharacteristicRuleList.NewHighlightCharacteristicRuleList());
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<HighlightCharacteristic>(oldId, newId);
        }

        /// <summary>
        /// ToString override.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }

        #endregion


        
    }
}