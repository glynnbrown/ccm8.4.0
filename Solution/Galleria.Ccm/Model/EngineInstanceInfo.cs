﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-27411 : M.Pettit
//  Created
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    public partial class EngineInstanceInfo : ModelReadOnlyObject<EngineInstanceInfo>
    {
        #region Constants
        // The maximum minutes between each heartbeat for an engine 
        // instance to be considered active and online
        public const Int32 MaxHeartbeatInterval = 5;
        #endregion
        
        #region Properties

        #region Id
        /// <summary>
        /// Instance Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Guid> IdProperty =
            RegisterModelProperty<Guid>(c => c.Id);
        /// <summary>
        /// Returns the Id
        /// </summary>
        public Guid Id
        {
            get { return this.ReadProperty<Guid>(IdProperty); }
        }
        #endregion

        #region ComputerName
        /// <summary>
        /// ComputerName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ComputerNameProperty =
            RegisterModelProperty<String>(c => c.ComputerName);
        /// <summary>
        /// Returns the Computer Name
        /// </summary>
        public String ComputerName
        {
            get { return this.ReadProperty<String>(ComputerNameProperty); }
        }
        #endregion

        #region WorkerCount
        /// <summary>
        /// WorkerCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> WorkerCountProperty =
            RegisterModelProperty<Int32>(c => c.WorkerCount);
        /// <summary>
        /// Returns the number of threads in the instance
        /// </summary>
        public Int32 WorkerCount
        {
            get { return this.ReadProperty<Int32>(WorkerCountProperty); }
        }
        #endregion

        #region Lifespan
        /// <summary>
        /// Lifespan property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> LifespanProperty =
            RegisterModelProperty<Int32>(c => c.Lifespan);
        /// <summary>
        /// Returns the lifespan (in minutes) of the instance. This is the time 
        /// before the instance will be deleted from the database
        /// </summary>
        public Int32 Lifespan
        {
            get { return this.ReadProperty<Int32>(LifespanProperty); }
        }
        #endregion

        #region DateLastActive
        /// <summary>
        /// DateLastActive property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastActiveProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastActive);
        /// <summary>
        /// Returns the heartbeat of the instance. This is the last time 
        /// the instance reported back. Instances should report back approx. every 15 minutes.
        /// </summary>
        public DateTime DateLastActive
        {
            get { return this.ReadProperty<DateTime>(DateLastActiveProperty); }
        }
        #endregion

        #endregion

        #region Helper Properties

        #region IsHealthy
        /// <summary>
        /// IsOnline property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsHealthyProperty =
            RegisterModelProperty<Boolean>(c => c.IsHealthy);
        /// <summary>
        /// Returns whether a heartbeat has been registered in the last X minutes from this instance
        /// </summary>
        public Boolean IsHealthy
        {
            get 
            { 
                TimeSpan timeSinceLastHeartbeat = 
                    DateTime.UtcNow.Subtract(this.ReadProperty<DateTime>(DateLastActiveProperty));
                return (timeSinceLastHeartbeat.TotalMinutes < MaxHeartbeatInterval);
            }
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(EngineInstanceInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(EngineInstanceInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(EngineInstanceInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(EngineInstanceInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Overrides

        /// <summary>
        ///     Overrides <see cref="ToString" /> to return the planogram name
        /// </summary>
        public override String ToString()
        {
            return ComputerName;
        }

        #endregion
    }
}
