﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24265 : N.Haywood
//  Created
// V8-27940 : L.Luong
//  added FetchByEntityId and FetchByEntityIdIncludingDeleted
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Readonly list holding LabelInfo objects.
    /// </summary>
    [Serializable]
    public sealed partial class LabelInfoList : ModelReadOnlyList<LabelInfoList, LabelInfo>
    {
        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Criteria

        #region FetchByPlanogramIdsCriteria

        /// <summary>
        /// Criteria for the Fetch factory method
        /// </summary>
        [Serializable]
        public class FetchByIdsCriteria : CriteriaBase<FetchByIdsCriteria>
        {
            #region Properties

            #region DalType
            /// <summary>
            /// DalType property definition
            /// </summary>
            public static readonly PropertyInfo<LabelDalFactoryType> DalTypeProperty =
                RegisterProperty<LabelDalFactoryType>(c => c.DalType);
            /// <summary>
            /// Returns the dal type
            /// </summary>
            public LabelDalFactoryType DalType
            {
                get { return this.ReadProperty<LabelDalFactoryType>(DalTypeProperty); }
            }
            #endregion

            #region Ids property

            /// <summary>
            /// Ids property definition
            /// </summary>
            public static readonly PropertyInfo<List<Object>> IdsProperty =
                RegisterProperty<List<Object>>(c => c.Ids);

            public List<Object> Ids
            {
                get { return this.ReadProperty<List<Object>>(IdsProperty); }
            }

            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdsCriteria(LabelDalFactoryType sourceType, List<Object> ids)
            {
                this.LoadProperty<LabelDalFactoryType>(DalTypeProperty, sourceType);
                this.LoadProperty<List<Object>>(IdsProperty, ids);
            }
            #endregion
        }

        #endregion

        #region FetchByEntityIdCriteria

        [Serializable]
        public class FetchByEntityIdCriteria : CriteriaBase<FetchByEntityIdCriteria>
        {
            #region EntityId

            /// <summary>
            ///		Metadata for the <see cref="EntityId"/> property.
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(o => o.EntityId);

            /// <summary>
            ///		Gets or sets the value for the <see cref="EntityId"/> property.
            /// </summary>
            public Int32 EntityId
            {
                get { return ReadProperty(EntityIdProperty); }
            }

            #endregion

            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty(EntityIdProperty, entityId);
            }
        }

        #endregion

        #region FetchByEntityIdIncludingDeletedCriteria

        [Serializable]
        public class FetchByEntityIdIncludingDeletedCriteria : CriteriaBase<FetchByEntityIdIncludingDeletedCriteria>
        {
            #region EntityId

            /// <summary>
            ///		Metadata for the <see cref="EntityId"/> property.
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(o => o.EntityId);

            /// <summary>
            ///		Gets or sets the value for the <see cref="EntityId"/> property.
            /// </summary>
            public Int32 EntityId
            {
                get { return ReadProperty(EntityIdProperty); }
            }

            #endregion

            public FetchByEntityIdIncludingDeletedCriteria(Int32 entityId)
            {
                LoadProperty(EntityIdProperty, entityId);
            }
        }

        #endregion

        #region FetchByTypeCriteria

        /// <summary>
        /// Criteria for the Fetch By Type factory method
        /// </summary>
        [Serializable]
        public class FetchByTypeCriteria : CriteriaBase<FetchByTypeCriteria>
        {
            #region Properties

            #region DalType
            /// <summary>
            /// DalType property definition
            /// </summary>
            public static readonly PropertyInfo<LabelDalFactoryType> DalTypeProperty =
                RegisterProperty<LabelDalFactoryType>(c => c.DalType);
            /// <summary>
            /// Returns the dal type
            /// </summary>
            public LabelDalFactoryType DalType
            {
                get { return this.ReadProperty<LabelDalFactoryType>(DalTypeProperty); }
            }
            #endregion

            #region Ids property

            /// <summary>
            /// Ids property definition
            /// </summary>
            public static readonly PropertyInfo<List<Object>> IdsProperty =
                RegisterProperty<List<Object>>(c => c.Ids);

            public List<Object> Ids
            {
                get { return this.ReadProperty<List<Object>>(IdsProperty); }
            }

            ///// <summary>
            /// Type property definition
            /// </summary>
            public static readonly PropertyInfo<LabelType> TypeProperty =
                RegisterProperty<LabelType>(c => c.Type);

            public LabelType Type
            {
                get { return this.ReadProperty<LabelType>(TypeProperty); }
            }

            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByTypeCriteria(LabelDalFactoryType sourceType, List<Object> ids, LabelType labelType)
            {
                this.LoadProperty<LabelDalFactoryType>(DalTypeProperty, sourceType);
                this.LoadProperty<List<Object>>(IdsProperty, ids);
                this.LoadProperty<LabelType>(TypeProperty, labelType);
            }
            #endregion
        }

        #endregion

        #endregion
    }
}
