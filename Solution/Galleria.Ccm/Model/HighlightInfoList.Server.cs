﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25436 L.Luong
//  Created
// V8-27491 J.Pickup
//  Introduced the Mssql factory type and fetchAll();
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class HighlightInfoList
    {
        #region Constants
        private const String _dalAssemblyName = "Galleria.Ccm.Dal.{0}.dll";
        #endregion

        #region Constructors
        private HighlightInfoList() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of all items from ids
        /// </summary>
        public static HighlightInfoList FetchByFileNames(List<Object> ids)
        {
            return DataPortal.Fetch<HighlightInfoList>(new FetchByIdsCriteria(HighlightDalFactoryType.FileSystem, ids));
        }

        /// <summary>
        /// Returns a list of all items from ids
        /// </summary>
        /// <param name="filenames"></param>
        /// <returns></returns>
        public static HighlightInfoList FetchByIds(List<Object> ids)
        {
            return DataPortal.Fetch<HighlightInfoList>(new FetchByIdsCriteria(HighlightDalFactoryType.Unknown, ids));
        }

        /// <summary>
        /// Returns a list of all items by entity id not including deleted. 
        /// </summary>
        /// <param name="filenames"></param>
        /// <returns></returns>
        public static HighlightInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<HighlightInfoList>(new FetchByEntityIdCriteria(entityId));
        }

        /// <summary>
        /// Returns a list of all items by entity id including deleted. 
        /// </summary>
        /// <param name="filenames"></param>
        /// <returns></returns>
        public static HighlightInfoList FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            return DataPortal.Fetch<HighlightInfoList>(new FetchByEntityIdIncludingDeletedCriteria(HighlightDalFactoryType.Unknown, entityId));
        }

        #endregion

        #region Data Access

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdsCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(criteria.DalType, out dalFactoryName);

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IHighlightInfoDal dal = dalContext.GetDal<IHighlightInfoDal>())
                {
                    foreach (HighlightInfoDto highlightDto in dal.FetchByIds(criteria.Ids))
                    {
                        if (criteria.DalType == HighlightDalFactoryType.FileSystem)
                        {
                            this.Add(HighlightInfo.GetHighlightInfo(dalContext, highlightDto, (String)highlightDto.Id));
                        }
                        else
                        {
                            this.Add(HighlightInfo.GetHighlightInfo(dalContext, highlightDto));
                        }
                    }
                }
            }

            this.RaiseListChangedEvents = true;
            this.IsReadOnly = true;
        }



        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(criteria.DalType, out dalFactoryName);

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IHighlightInfoDal dal = dalContext.GetDal<IHighlightInfoDal>())
                {
                    foreach (HighlightInfoDto highlightDto in dal.FetchByEntityId(criteria.EntityId))
                    {
                        if (criteria.DalType == HighlightDalFactoryType.FileSystem)
                        {
                            this.Add(HighlightInfo.GetHighlightInfo(dalContext, highlightDto, (String)highlightDto.Id));
                        }
                        else
                        {
                            this.Add(HighlightInfo.GetHighlightInfo(dalContext, highlightDto));
                        }
                    }
                }
            }

            this.RaiseListChangedEvents = true;
            this.IsReadOnly = true;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdIncludingDeletedCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(criteria.DalType, out dalFactoryName);

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IHighlightInfoDal dal = dalContext.GetDal<IHighlightInfoDal>())
                {
                    foreach (HighlightInfoDto highlightDto in dal.FetchByEntityIdIncludingDeleted(criteria.EntityId))
                    {
                        if (criteria.DalType == HighlightDalFactoryType.FileSystem)
                        {
                            this.Add(HighlightInfo.GetHighlightInfo(dalContext, highlightDto, (String)highlightDto.Id));
                        }
                        else
                        {
                            this.Add(HighlightInfo.GetHighlightInfo(dalContext, highlightDto));
                        }
                    }
                }
            }

            this.RaiseListChangedEvents = true;
            this.IsReadOnly = true;
        }



        #endregion

        #region Methods

        /// <summary>
        /// Returns the correct dal factory based on the given type and args.
        /// </summary>
        private static IDalFactory GetDalFactory(HighlightDalFactoryType dalType, out String dalFactoryName)
        {
            switch (dalType)
            {
                case HighlightDalFactoryType.FileSystem:
                    dalFactoryName = Constants.UserDal;
                    return DalContainer.GetDalFactory(dalFactoryName);

                default:
                    dalFactoryName = DalContainer.DalName;
                    return DalContainer.GetDalFactory();
            }

        }

        #endregion
    }
}
