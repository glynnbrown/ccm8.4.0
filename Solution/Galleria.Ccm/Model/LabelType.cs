﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// V8-24265 : J.Pickup
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    public enum LabelType
    {
        Product = 0,
        Fixture = 1
    }

    public static class LabelTypeHelper
    {
        public static readonly Dictionary<LabelType, String> FriendlyNames =
            new Dictionary<LabelType, String>()
            {
                {LabelType.Product, Message.LabelType_Product },
                {LabelType.Fixture, Message.LabelType_Fixture }
            };
    }
}
