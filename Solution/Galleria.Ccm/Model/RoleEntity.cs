﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26234 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a entity access permissions for a particular role
    /// </summary>
    [Serializable]
    public sealed partial class RoleEntity : ModelObject<RoleEntity>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The unique id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region Entity Id
        /// <summary>
        /// EntityId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        /// <summary>
        /// The entity id
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(RoleEntity), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.RoleCreate.ToString()));
            BusinessRules.AddRule(typeof(RoleEntity), new IsInRole(AuthorizationActions.GetObject, DomainPermission.RoleGet.ToString()));
            BusinessRules.AddRule(typeof(RoleEntity), new IsInRole(AuthorizationActions.EditObject, DomainPermission.RoleEdit.ToString()));
            BusinessRules.AddRule(typeof(RoleEntity), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.RoleDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new role entity
        /// </summary>
        /// <param name="entityId">The entity id</param>
        /// <returns></returns>
        public static RoleEntity NewRoleEntity(Int32 entityId)
        {
            RoleEntity item = new RoleEntity();
            item.Create(entityId);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        /// <param name="entityId">The entity id</param>
        private void Create(Int32 entityId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}
