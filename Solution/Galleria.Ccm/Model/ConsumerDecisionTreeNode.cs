﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8025632 : A.Kuszyk
//		Created (copied from SA).
// V8-25455 : J.Pickup
//		Added FullName property (helper)
// V8-27132 : A.Kuszyk
//  Implemented common interface.
// V8-25556 : J.Pickup
//		Added GFS Id property. 
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a node within a cdt
    /// (Child of ConsumerDecisionTree or ConsumerDecisionTreeNodeList)
    /// </summary>
    [Serializable]
    public sealed partial class ConsumerDecisionTreeNode : ModelObject<ConsumerDecisionTreeNode>, IPlanogramConsumerDecisionTreeNode
    {
        #region Parent

        /// <summary>
        /// Returns the parent node of this
        /// </summary>
        public ConsumerDecisionTreeNode ParentNode
        {
            get
            {
                ConsumerDecisionTreeNodeList list = base.Parent as ConsumerDecisionTreeNodeList;
                if (list != null)
                {
                    return list.ParentNode;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Returns the parent model of this
        /// </summary>
        public ConsumerDecisionTree ParentConsumerDecisionTree
        {
            get
            {
                ConsumerDecisionTree parentConsumerDecisionTree = base.Parent as ConsumerDecisionTree;
                if (parentConsumerDecisionTree == null)
                {
                    ConsumerDecisionTreeNode currentParent = this.ParentNode;
                    while (currentParent != null)
                    {
                        if (currentParent.ParentNode != null)
                        {
                            currentParent = currentParent.ParentNode;
                        }
                        else
                        {
                            return currentParent.Parent as ConsumerDecisionTree;
                        }
                    }
                }
                else
                {
                    return parentConsumerDecisionTree;
                }

                return null;
            }
        }

        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The unique object id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<Int32?> GFSIdProperty =
            RegisterModelProperty<Int32?>(c => c.GFSId);
        /// <summary>
        /// TheGFS Id for this specific item. 
        /// </summary>
        public Int32? GFSId
        {
            get { return GetProperty<Int32?>(GFSIdProperty); }
        }


        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The node name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32> ConsumerDecisionTreeLevelIdProperty =
        RegisterModelProperty<Int32>(c => c.ConsumerDecisionTreeLevelId);
        /// <summary>
        /// The id of the level this belongs to
        /// </summary>
        /// <remarks>Do not expose</remarks>
        public Int32 ConsumerDecisionTreeLevelId
        {
            get { return GetProperty<Int32>(ConsumerDecisionTreeLevelIdProperty); }
            set { SetProperty<Int32>(ConsumerDecisionTreeLevelIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<ConsumerDecisionTreeNodeList> ChildListProperty =
        RegisterModelProperty<ConsumerDecisionTreeNodeList>(c => c.ChildList);
        /// <summary>
        /// The list of child nodes
        /// </summary>
        public ConsumerDecisionTreeNodeList ChildList
        {
            get { return GetProperty<ConsumerDecisionTreeNodeList>(ChildListProperty); }
        }


        public static readonly ModelPropertyInfo<ConsumerDecisionTreeNodeProductList> ProductsProperty =
            RegisterModelProperty<ConsumerDecisionTreeNodeProductList>(c => c.Products);
        /// <summary>
        /// The list of products assigned to this node
        /// </summary>
        public ConsumerDecisionTreeNodeProductList Products
        {
            get { return GetProperty<ConsumerDecisionTreeNodeProductList>(ProductsProperty); }
        }

        #region Helper Properties

        /// <summary>
        /// Returns true if this is the root unit
        /// </summary>
        public Boolean IsRoot
        {
            get { return this.ParentNode == null; }
        }

        #region Total Product Count

        /// <summary>
        /// Returns the total number of products
        /// below this node in the mode
        /// </summary>
        public Int32 TotalProductCount
        {
            get
            {
                return this.ChildList.TotalProductCount + this.Products.Count;
            }
        }

        /// <summary>
        /// Notifies that the TotalProductCount property has changed
        /// and also raise the notification upwards
        /// </summary>
        public void RaiseTotalProductCountChanged()
        {
            OnPropertyChanged("TotalProductCount");

            if (this.ParentNode != null)
            {
                this.ParentNode.RaiseTotalProductCountChanged();
            }
        }

        #endregion

        /// <summary>
        /// Returns the full name of the node.
        /// This is a concatonation of the n
        /// </summary>
        public String FullName
        {
            get
            {
                String value = this.Name;

                ConsumerDecisionTreeNode node = this;
                while (node.ParentNode != null && !node.ParentNode.IsRoot)
                {
                    String parentName = node.ParentNode.Name;

                    //if ((value.Length + parentName.Length + /*spacing*/3) <= 300)
                    //{
                    value = String.Format(CultureInfo.CurrentCulture, "{0} - {1}", parentName, value);

                    node = node.ParentNode;
                    //}
                    //else { break; }
                }

                return value;
            }
        }


        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeNode), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ConsumerDecisionTreeCreate.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeNode), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ConsumerDecisionTreeGet.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeNode), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ConsumerDecisionTreeEdit.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeNode), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ConsumerDecisionTreeDelete.ToString()));
        }
        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <returns></returns>
        public static ConsumerDecisionTreeNode NewConsumerDecisionTreeNode(Object levelId)
        {
            ConsumerDecisionTreeNode item = new ConsumerDecisionTreeNode();
            item.Create(levelId);
            return item;
        }

        public static ConsumerDecisionTreeNode NewConsumerDecisionTreeNode
            (ConsumerDecisionTreeNode node, ConsumerDecisionTreeNode rootNode)
        {
            ConsumerDecisionTreeNode item = new ConsumerDecisionTreeNode();
            item.Create(node, rootNode);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Object levelId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(ConsumerDecisionTreeLevelIdProperty, (Int32)levelId);
            this.LoadProperty<ConsumerDecisionTreeNodeList>(ChildListProperty, ConsumerDecisionTreeNodeList.NewConsumerDecisionTreeNodeList());
            this.LoadProperty<ConsumerDecisionTreeNodeProductList>(ProductsProperty, ConsumerDecisionTreeNodeProductList.NewConsumerDecisionTreeNodeProductList());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(ConsumerDecisionTreeNode node, ConsumerDecisionTreeNode rootNode)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, node.Name);
            this.LoadProperty<Int32>(ConsumerDecisionTreeLevelIdProperty, node.ConsumerDecisionTreeLevelId);
            this.LoadProperty<ConsumerDecisionTreeNodeList>(ChildListProperty, ConsumerDecisionTreeNodeList.NewConsumerDecisionTreeNodeList(node.ChildList, rootNode));
            this.LoadProperty<ConsumerDecisionTreeNodeProductList>(ProductsProperty, ConsumerDecisionTreeNodeProductList.NewConsumerDecisionTreeNodeProductList(node.Products, rootNode));
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Overrides

        /// <summary>
        /// To String override
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<ConsumerDecisionTreeNode>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes on this instance
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }

        /// <summary>
        /// Called when resolving ids for this instance
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // ConsumerDecisionTreeLevelId
            Object consumerDecisionTreeLevelId = context.ResolveId<ConsumerDecisionTreeLevel>(this.ReadProperty<Int32>(ConsumerDecisionTreeLevelIdProperty));
            if (consumerDecisionTreeLevelId != null) this.LoadProperty<Int32>(ConsumerDecisionTreeLevelIdProperty, (Int32)consumerDecisionTreeLevelId);
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Returns a list  of this group and  all child groups below
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ConsumerDecisionTreeNode> FetchAllChildNodes()
        {
            IEnumerable<ConsumerDecisionTreeNode> returnList =
                new List<ConsumerDecisionTreeNode>() { this };

            foreach (ConsumerDecisionTreeNode child in this.ChildList)
            {
                returnList = returnList.Union(child.FetchAllChildNodes());
            }

            return returnList;
        }

        /// <summary>
        /// Returns a list of all products which sit under this node
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ConsumerDecisionTreeNodeProduct> FetchAllChildConsumerDecisionTreeNodeProducts()
        {
            IEnumerable<ConsumerDecisionTreeNodeProduct> returnList = this.Products;

            foreach (ConsumerDecisionTreeNode child in this.ChildList)
            {
                returnList = returnList.Union(child.FetchAllChildConsumerDecisionTreeNodeProducts());
            }

            return returnList;
        }

        /// <summary>
        /// Creates a list of nodes representing a path down to the requested child
        /// </summary>
        /// <returns></returns>
        public List<ConsumerDecisionTreeNode> FetchParentPath()
        {
            List<ConsumerDecisionTreeNode> inversePathList = new List<ConsumerDecisionTreeNode>();
            ConsumerDecisionTreeNode currentParent = this.ParentNode;

            while (currentParent != null)
            {
                inversePathList.Add(currentParent);
                currentParent = currentParent.ParentNode;
            }

            //invert the list so the top parent is the first item
            List<ConsumerDecisionTreeNode> pathList = new List<ConsumerDecisionTreeNode>();
            int currentIndex = inversePathList.Count - 1;
            while (currentIndex >= 0)
            {
                pathList.Add(inversePathList[currentIndex]);
                currentIndex--;
            }

            return pathList;
        }

        /// <summary>
        /// Returns the percentage of products held by this node in relation to its parent
        /// </summary>
        /// <returns></returns>
        public Double GetPercentOfParent()
        {
            if (this.ParentNode != null)
            {
                if (this.ParentNode.TotalProductCount == 0)
                {
                    //divide to parent size by the number of children as they all have 0 products
                    return 100 / (Double)this.ParentNode.ChildList.Count;
                }
                else if (this.TotalProductCount == 0)
                {
                    return 0;
                }
                else
                {
                    return ((Double)this.TotalProductCount /
                    (Double)this.ParentNode.TotalProductCount) * 100;
                }
            }
            else
            {
                return 100;
            }
        }

        ///// <summary>
        ///// Returns the number of levels (of nodes) in this structure, starting with this node.
        ///// </summary>
        //public Int32 GetLevelCount()
        //{
        //    Int32 maxChildLevelCount = 0;
        //    foreach (ConsumerDecisionTreeNode child in ChildList)
        //    {
        //        maxChildLevelCount = Math.Max(maxChildLevelCount, child.GetLevelCount());
        //    }
        //    return maxChildLevelCount + 1;
        //}

        /// <summary>
        /// Splits the node based on the given product related properties
        /// </summary>
        /// <param name="propertyDescriptionList"></param>
        /// <param name="masterProductsList"></param>
        public ConsumerDecisionTreeNode SplitByProductProperties(
            IEnumerable<ObjectFieldInfo> propertyDescriptionList,
            IEnumerable<Product> masterProductsList)
        {
            this.ChildList.RaiseListChangedEvents = false;
            this.Products.RaiseListChangedEvents = false;

            ConsumerDecisionTreeNode returnNullValueNode = null;

            //firstly remove all child nodes as we are going to re-split
            if (this.ChildList.Count > 0)
            {
                this.ChildList.Clear();
            }

            if (propertyDescriptionList.Any())
            {
                String splitDescriptionString = String.Empty;
                foreach (ObjectFieldInfo desc in propertyDescriptionList)
                {
                    if (!String.IsNullOrEmpty(splitDescriptionString))
                    {
                        splitDescriptionString = String.Format("{0} {1}", splitDescriptionString, desc.PropertyFriendlyName);
                    }
                    else
                    {
                        splitDescriptionString = desc.PropertyFriendlyName;
                    }
                }

                //create the property value groupings
                IEnumerable<Int32> prodIds = this.Products.Select(l => l.ProductId);
                IEnumerable<Product> referencedProds = masterProductsList.Where(l => prodIds.Contains(l.Id));

                IEnumerable<IGrouping<Object, Product>> groupings =
                    referencedProds.GroupBy(p => CreateCombinedPropertiesKey(p, propertyDescriptionList));

                //Check for a child level
                ConsumerDecisionTreeLevel childLevel;
                ConsumerDecisionTreeLevel parentLevel = this.ParentConsumerDecisionTree.FetchAllLevels().FirstOrDefault(p => p.Id.Equals(this.ConsumerDecisionTreeLevelId));
                if (parentLevel.ChildLevel == null)
                {
                    childLevel = ConsumerDecisionTreeLevel.NewConsumerDecisionTreeLevel();
                    parentLevel.ChildLevel = childLevel;
                }
                else
                {
                    childLevel = parentLevel.ChildLevel;
                }

                //cycle through creating the group nodes
                foreach (IGrouping<Object, Product> grp in groupings.OrderBy(g => g.Key))
                {
                    Boolean isNullValueNode = false;

                    Object value = grp.Key;
                    if (value == null || value.ToString() == String.Empty)
                    {
                        isNullValueNode = true;
                        value = Message.ConsumerDecisionTreeNode_NullNodeName;
                    }

                    //create a node
                    ConsumerDecisionTreeNode childNode = ConsumerDecisionTreeNode.NewConsumerDecisionTreeNode(childLevel.Id);
                    childNode.Name = value.ToString();
                    this.ChildList.Add(childNode);

                    //add in all the products
                    foreach (Product p in grp)
                    {
                        ConsumerDecisionTreeNodeProduct cdtProduct = this.Products.FirstOrDefault(q => q.ProductId == p.Id);
                        if (cdtProduct != null)
                        {
                            childNode.Products.Add(cdtProduct);
                        }
                        else
                        {
                            childNode.Products.Add(
                                ConsumerDecisionTreeNodeProduct.NewConsumerDecisionTreeNodeProduct(p.Id));
                        }
                    }

                    //If this is a null value node
                    if (isNullValueNode)
                    {
                        //Set the return value to be this node
                        returnNullValueNode = childNode;
                    }
                }

                //Update level name
                UpdateLevelName(childLevel, splitDescriptionString);
            }

            //remove all products from this node as we only need store them at the lowest level
            this.Products.Clear();

            this.ChildList.RaiseListChangedEvents = true;
            this.ChildList.Reset();
            this.Products.RaiseListChangedEvents = true;
            this.Products.Reset();

            //Return the null value node
            return returnNullValueNode;
        }

        public Int32 CalculateExpectedNodeCount(
            IEnumerable<ObjectFieldInfo> propertyDescriptionList,
            IEnumerable<Product> masterProductsList)
        {
            Int32 count = 0;

            if (propertyDescriptionList.Any())
            {
                List<IGrouping<Object, Product>> expectedNodes = new List<IGrouping<Object, Product>>();
                List<IGrouping<Object, Product>> parentNodes = new List<IGrouping<Object, Product>>();

                //Add a default group for the root
                expectedNodes.AddRange(masterProductsList.GroupBy(p => "Root"));
                parentNodes.AddRange(expectedNodes);
                foreach (ObjectFieldInfo property in propertyDescriptionList)
                {
                    IEnumerable<IGrouping<Object, Product>> oldParents = parentNodes.ToList();
                    parentNodes.Clear();

                    foreach (IGrouping<Object, Product> expectedNode in oldParents)
                    {
                        parentNodes.AddRange(GetExpectedChildNodes(new List<ObjectFieldInfo>() { property }, expectedNode));
                    }

                    expectedNodes.AddRange(parentNodes.ToList());
                }

                //Set the expected node count, -1 for the root
                count = expectedNodes.Count - 1;
            }

            return count;
        }

        private IEnumerable<IGrouping<Object, Product>> GetExpectedChildNodes(
            IEnumerable<ObjectFieldInfo> propertyDescriptionList,
            IEnumerable<Product> productsList)
        {
            IEnumerable<IGrouping<Object, Product>> groupings = null;

            if (propertyDescriptionList.Any())
            {
                groupings = productsList.GroupBy(p => CreateCombinedPropertiesKey(p, propertyDescriptionList));
            }

            return groupings;
        }

        /// <summary>
        /// Creates the value key for a product based on the given property descriptions
        /// </summary>
        /// <param name="prod"></param>
        /// <param name="propertyDescriptions"></param>
        /// <returns></returns>
        private Object CreateCombinedPropertiesKey(Product prod, IEnumerable<ObjectFieldInfo> propertyDescriptions)
        {
            Object propertyKey = null;

            foreach (ObjectFieldInfo p in propertyDescriptions)
            {
                if (p != null)
                {
                    Object value = null;
                    Object foundProperty = null;

                    if (p.OwnerType == typeof(Product))
                    {
                        foundProperty = p.GetValue(prod);
                        if (foundProperty != null)
                        {
                            value = foundProperty.ToString();
                        }
                    }
                    //else if (p.OwnerType == typeof(ProductAttributeData))
                    //{
                    //    foundProperty = p.GetInfo().GetValue(prod.AttributeData, null);
                    //    if (foundProperty != null)
                    //    {
                    //        value = foundProperty.ToString();
                    //    }
                    //}
                    else
                    {
                        Debug.WriteLine
                            (String.Format("Could not resolve property {0} {1}", p.PropertyName, p.OwnerType.ToString()));
                    }

                    if (value != null)
                    {
                        if (propertyKey != null)
                        {
                            propertyKey = String.Format("{0}, {1}", propertyKey, value);
                        }
                        else
                        {
                            //[20165]Added code so if data is empty string, flags up as null
                            if (value.ToString() == String.Empty)
                            {
                                value = null;
                            }

                            propertyKey = value;
                        }
                    }

                }
            }

            return propertyKey;
        }

        /// <summary>
        /// Creates a new child node using this node's products
        /// </summary>
        public ConsumerDecisionTreeNode AddNewChildNode()
        {
            this.ChildList.RaiseListChangedEvents = false;
            this.Products.RaiseListChangedEvents = false;

            ConsumerDecisionTreeNode childNode = null;

            //Get the parent level
            ConsumerDecisionTreeLevel parentLevel = this.ParentConsumerDecisionTree.GetLinkedLevel(this);

            if (parentLevel != null)
            {
                ConsumerDecisionTreeLevel newLevel;

                //A new level is required for the new node
                if (parentLevel.ChildLevel == null)
                {
                    newLevel = this.ParentConsumerDecisionTree.InsertNewChildLevel(parentLevel);
                }
                else
                {
                    //use the current child level
                    newLevel = parentLevel.ChildLevel;
                }

                //Create the child node
                childNode = ConsumerDecisionTreeNode.NewConsumerDecisionTreeNode(newLevel.Id);
                //set name
                //find out the other level names
                List<String> siblingNames = new List<String>();
                String name = Message.CdtMaintenance_NewNode;
                Int32 nameNumber = 1;


                foreach (ConsumerDecisionTreeNode node in ChildList)
                {
                    siblingNames.Add(node.Name.ToUpperInvariant());
                }
                if (siblingNames.Contains(name.ToUpperInvariant()))
                {
                    while (siblingNames.Contains(name.ToUpperInvariant() + " " + nameNumber))
                    {
                        nameNumber++;
                    }
                    childNode.Name = name + " " + nameNumber;
                }
                else
                {
                    childNode.Name = name;
                }

                childNode.Products.AddList(this.Products);
                this.ChildList.Add(childNode);

                //remove all products from this node as we only need store them at the lowest level
                this.Products.Clear();

                this.ChildList.RaiseListChangedEvents = true;
                this.ChildList.Reset();
                this.Products.RaiseListChangedEvents = true;
                this.Products.Reset();
            }
            return childNode;
        }

        /// <summary>
        /// Updates the details of this node from the given decision tree node.
        /// </summary>
        public void MergeFrom(ConsumerDecisionTreeNode node)
        {
            this.ChildList.RaiseListChangedEvents = false;
            this.Products.RaiseListChangedEvents = false;

            ConsumerDecisionTreeNode mergeRootNode = ConsumerDecisionTreeNode.NewConsumerDecisionTreeNode(node, this);
            mergeRootNode.Name = node.ParentConsumerDecisionTree.Name;

            this.ChildList.Add(mergeRootNode);

            //get a list of allocated products that are assigned to a child node
            IEnumerable<Int32> allocatedIds = new List<Int32>();
            foreach (ConsumerDecisionTreeNode child in this.ChildList)
            {
                allocatedIds = allocatedIds.Union(child.FetchAllChildConsumerDecisionTreeNodeProducts().Select(s => s.ProductId));
            }

            //remove all these from the root so we are just left with the root products and any that are unallocated.
            this.Products.RemoveList(
                this.Products.Where(p => allocatedIds.Contains(p.ProductId)).ToList());

            this.ChildList.RaiseListChangedEvents = true;
            this.ChildList.Reset();
            this.Products.RaiseListChangedEvents = true;
            this.Products.Reset();
        }

        /// <summary>
        /// Method to update the level name upon splitting a node
        /// </summary>
        /// <param name="level"></param>
        /// <param name="splitDescriptionString"></param>
        /// <remarks>This should only be used when splitting as there could be multiple split 
        /// by properties in a single level, requiring the custom name to be set.</remarks>
        public void UpdateLevelName(ConsumerDecisionTreeLevel level, String splitDescriptionString)
        {
            //Update level name if the split description is different
            if (level.Name != splitDescriptionString)
            {
                //If split description is valid, and string is not empty
                if (splitDescriptionString != "")
                {
                    //If name is empty - set to split description string
                    if (String.IsNullOrWhiteSpace(level.Name))
                    {
                        //Set it to split description
                        level.Name = splitDescriptionString;
                    }
                    else
                    {
                        //If the level name doesn't contain the split description
                        if (!level.Name.ToUpperInvariant().Contains(splitDescriptionString.ToUpperInvariant()))
                        {
                            //Append it to the end
                            level.Name = String.Format("{0} - {1}", level.Name, splitDescriptionString);
                        }
                    }
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Method to handle removing the node
        /// </summary>
        public void RemoveNode()
        {
            //Fetch levels
            IEnumerable<ConsumerDecisionTreeLevel> levels = this.ParentConsumerDecisionTree.FetchAllLevels();

            //Find node level
            ConsumerDecisionTreeLevel level = levels.FirstOrDefault(p => p.Id.Equals(this.Id));

            //Fetch nodes at this level
            IEnumerable<ConsumerDecisionTreeNode> levelNodes = this.ParentConsumerDecisionTree.FetchAllNodes().Where(p => p.Id.Equals(this.ConsumerDecisionTreeLevelId) && p != this);

            //Check if this is the last node at level, if so, remove level
            if (!levelNodes.Any())
            {
                if (level != null)
                {
                    //Remove level
                    this.ParentConsumerDecisionTree.RemoveLevel(level, true);
                }
            }
            else
            {
                //Otherwise remove node normally
                this.ParentNode.ChildList.Remove(this);

                //Update level name
                UpdateLevelName(level, "");
            }

            if (level != null)
            {
                //Get any child levels
                IEnumerable<ConsumerDecisionTreeLevel> childLevels = this.ParentConsumerDecisionTree.FetchAllLevels().Where(p => p.LevelNumber > level.LevelNumber);

                //If lower child levels exist, clear the ones up that dont have any nodes assigned
                if (childLevels.Any())
                {
                    //Get all nodes
                    IEnumerable<ConsumerDecisionTreeNode> currentNodes = this.ParentConsumerDecisionTree.FetchAllNodes();

                    //Enumerate through levels, starting with lowest
                    foreach (ConsumerDecisionTreeLevel childLevel in childLevels.OrderByDescending(p => p.LevelNumber))
                    {
                        //If there are no nodes at this level
                        if (!currentNodes.Any(p => p.Id.Equals(childLevel.Id)))
                        {
                            //Remove child level
                            this.ParentConsumerDecisionTree.RemoveLevel(childLevel, true);
                        }
                    }
                }
            }
        }

        #endregion

        #region IPlanogramConsumerDecisionTreeNode members
        IEnumerable<IPlanogramConsumerDecisionTreeNode> IPlanogramConsumerDecisionTreeNode.ChildList
        {
            get { return ChildList; }
        }

        IEnumerable<IPlanogramConsumerDecisionTreeNodeProduct> IPlanogramConsumerDecisionTreeNode.Products
        {
            get { return Products; }
        }
        #endregion
    }
}