﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentMinorRevisionReplaceActionLocationList
    {
        #region Constructors
        private AssortmentMinorRevisionReplaceActionLocationList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an existing Assortment minor revision replace action location list
        /// </summary>
        /// <param name="childData"></param>
        /// <returns>A new Assortment location list</returns>
        internal static AssortmentMinorRevisionReplaceActionLocationList FetchByAssortmentMinorRevisionReplaceActionId(IDalContext dalContext, Int32 assortmentMinorRevisionReplaceActionId)
        {
            return DataPortal.FetchChild<AssortmentMinorRevisionReplaceActionLocationList>(dalContext, assortmentMinorRevisionReplaceActionId);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="assortmentMinorRevisionActionId">The parent assortmentMinorRevisionAction id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 assortmentMinorRevisionActionId)
        {
            RaiseListChangedEvents = false;
            using (IAssortmentMinorRevisionReplaceActionLocationDal dal = dalContext.GetDal<IAssortmentMinorRevisionReplaceActionLocationDal>())
            {
                IEnumerable<AssortmentMinorRevisionReplaceActionLocationDto> dtoList = dal.FetchByAssortmentMinorRevisionReplaceActionId(assortmentMinorRevisionActionId);
                foreach (AssortmentMinorRevisionReplaceActionLocationDto dto in dtoList)
                {
                    this.Add(AssortmentMinorRevisionReplaceActionLocation.FetchAssortmentMinorRevisionReplaceActionLocation(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
