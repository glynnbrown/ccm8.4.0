﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
// V8-26182 : L.Ineson
//  Added NewPlanogramAssemblyComponent
#endregion
#region Version History: (CCM CCM830)
// V8-32524 : L.Ineson
//  Added FixtureAnnotations and simplified updated back to plan
#endregion
#endregion

using System;
using Galleria.Ccm.Helpers;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// FixtureAssemblyComponent Model object
    /// </summary>
    [Serializable]
    public sealed partial class FixtureAssemblyComponent : ModelObject<FixtureAssemblyComponent>
    {

        #region Static Constructor
        static FixtureAssemblyComponent()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new FixtureAssembly Parent
        {
            get
            {
                FixtureAssemblyComponentList parentList = base.Parent as FixtureAssemblyComponentList;
                if (parentList != null)
                {
                    return parentList.Parent;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region FixtureComponentId

        /// <summary>
        /// FixtureComponentId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FixtureComponentIdProperty =
            RegisterModelProperty<Int32>(c => c.FixtureComponentId);

        /// <summary>
        /// Gets/Sets the FixtureComponentId value
        /// </summary>
        public Int32 FixtureComponentId
        {
            get { return GetProperty<Int32>(FixtureComponentIdProperty); }
            set { SetProperty<Int32>(FixtureComponentIdProperty, value); }
        }

        #endregion

        #region X

        /// <summary>
        /// X property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> XProperty =
            RegisterModelProperty<Single>(c => c.X);

        /// <summary>
        /// Gets/Sets the X value
        /// </summary>
        public Single X
        {
            get { return GetProperty<Single>(XProperty); }
            set { SetProperty<Single>(XProperty, value); }
        }

        #endregion

        #region Y

        /// <summary>
        /// Y property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> YProperty =
            RegisterModelProperty<Single>(c => c.Y);

        /// <summary>
        /// Gets/Sets the Y value
        /// </summary>
        public Single Y
        {
            get { return GetProperty<Single>(YProperty); }
            set { SetProperty<Single>(YProperty, value); }
        }

        #endregion

        #region Z

        /// <summary>
        /// Z property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ZProperty =
            RegisterModelProperty<Single>(c => c.Z);

        /// <summary>
        /// Gets/Sets the Z value
        /// </summary>
        public Single Z
        {
            get { return GetProperty<Single>(ZProperty); }
            set { SetProperty<Single>(ZProperty, value); }
        }

        #endregion

        #region Slope

        /// <summary>
        /// Slope property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SlopeProperty =
            RegisterModelProperty<Single>(c => c.Slope);

        /// <summary>
        /// Gets/Sets the Slope value
        /// </summary>
        public Single Slope
        {
            get { return GetProperty<Single>(SlopeProperty); }
            set { SetProperty<Single>(SlopeProperty, value); }
        }

        #endregion

        #region Angle

        /// <summary>
        /// Angle property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AngleProperty =
            RegisterModelProperty<Single>(c => c.Angle);

        /// <summary>
        /// Gets/Sets the Angle value
        /// </summary>
        public Single Angle
        {
            get { return GetProperty<Single>(AngleProperty); }
            set { SetProperty<Single>(AngleProperty, value); }
        }

        #endregion

        #region Roll

        /// <summary>
        /// Roll property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> RollProperty =
            RegisterModelProperty<Single>(c => c.Roll);

        /// <summary>
        /// Gets/Sets the Roll value
        /// </summary>
        public Single Roll
        {
            get { return GetProperty<Single>(RollProperty); }
            set { SetProperty<Single>(RollProperty, value); }
        }

        #endregion


        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();


        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static FixtureAssemblyComponent NewFixtureAssemblyComponent()
        {
            FixtureAssemblyComponent item = new FixtureAssemblyComponent();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        internal static FixtureAssemblyComponent NewFixtureAssemblyComponent(PlanogramAssemblyComponent planAc)
        {
            FixtureAssemblyComponent item = new FixtureAssemblyComponent();
            item.Create(planAc);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(PlanogramAssemblyComponent planAc)
        {
            this.LoadProperty<Int32>(IdProperty, (Int32)planAc.Id);
            this.LoadProperty<Int32>(FixtureComponentIdProperty, (Int32)planAc.PlanogramComponentId);
            this.LoadProperty<Single>(XProperty, planAc.X);
            this.LoadProperty<Single>(YProperty, planAc.Y);
            this.LoadProperty<Single>(ZProperty, planAc.Z);
            this.LoadProperty<Single>(SlopeProperty, planAc.Slope);
            this.LoadProperty<Single>(AngleProperty, planAc.Angle);
            this.LoadProperty<Single>(RollProperty, planAc.Roll);
            
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<FixtureAssemblyComponent>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes on this instance
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }

        /// <summary>
        /// Called when resolving ids on this instance
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // FixtureComponentId
            Object fixtureComponentId = context.ResolveId<FixtureComponent>(this.ReadProperty<Int32>(FixtureComponentIdProperty));
            if (fixtureComponentId != null) this.LoadProperty<Int32>(FixtureComponentIdProperty, (Int32)fixtureComponentId);
        }

        /// <summary>
        /// Updates this assembly component to the given planogram assembly.
        /// </summary>
        public PlanogramAssemblyComponent AddOrUpdatePlanogramAssemblyComponent(PlanogramAssembly parentAssembly)
        {
            //update the child component first
            FixtureComponent component = GetFixtureComponent();
            if (component != null) component.AddOrUpdatePlanogramComponent(parentAssembly.Parent);



            PlanogramAssemblyComponent item = parentAssembly.Components.FindById(this.Id);
            if (item == null)
            {
                item = PlanogramAssemblyComponent.NewPlanogramAssemblyComponent();
                item.Id = this.Id;
                parentAssembly.Components.Add(item);
            }

            item.X = this.X;
            item.Y = this.Y;
            item.Z = this.Z;
            item.Slope = this.Slope;
            item.Angle = this.Angle;
            item.Roll = this.Roll;
            item.PlanogramComponentId = this.FixtureComponentId;

           

            return item;
        }

        /// <summary>
        /// Returns the linked fixture component.
        /// </summary>
        public FixtureComponent GetFixtureComponent()
        {
            if (this.Parent == null) return null;

            FixturePackage package = this.Parent.Parent;
            if (package == null) return null;

            return package.Components.FindById(this.FixtureComponentId);
        }

        #endregion
    }
}