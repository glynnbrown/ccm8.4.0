#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.
// V8-32621 : A.Silva
//  Added CreateDefaultPlanogramComparisonTemplate.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.IO;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Model representing a Planogram Comparison Template object.
    ///     (Root Object)
    ///     This class represents the configuration options used to perform Planogram Comparisons.
    ///     It contains a list of <see cref="ComparisonFields" /> used when comparing planograms along with
    ///     fields to identify the template.
    ///     This is a root object and has the following properties/actions:
    ///     - If this object is deleted, any child objects are deleted too.
    /// </summary>
    [Serializable, DefaultNewMethod("NewPlanogramComparisonTemplate", "EntityId")]
    public sealed partial class PlanogramComparisonTemplate : ModelObject<PlanogramComparisonTemplate>, IPlanogramComparisonSettings, IDisposable
    {
        #region Nested Enum: Source Type

        #endregion

        #region Commands

        #region LockPlanogramComparisonTemplateCommand

        /// <summary>
        ///     Command to lock a <see cref="PlanogramComparisonTemplate"/>.
        /// </summary>
        [Serializable]
        private partial class LockPlanogramComparisonTemplateCommand : CommandBase<LockPlanogramComparisonTemplateCommand>
        {
            #region Properties

            #region DalFactoryType

            /// <summary>
            ///     <see cref="PropertyInfo{T}" /> for the <see cref="DalFactoryType" /> property.
            /// </summary>
            public static readonly PropertyInfo<PlanogramComparisonTemplateDalType> DalFactoryTypeProperty =
                RegisterProperty<PlanogramComparisonTemplateDalType>(c => c.DalFactoryType);

            /// <summary>
            ///     Get the Dal Factory Type for this <see cref="UnlockPlanogramComparisonTemplateCommand" /> instance.
            /// </summary>
            public PlanogramComparisonTemplateDalType DalFactoryType { get { return ReadProperty(DalFactoryTypeProperty); } }

            #endregion

            #region Id

            /// <summary>
            ///     <see cref="PropertyInfo{T}" /> for the <see cref="Id" /> property.
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty = RegisterProperty<Object>(c => c.Id);

            /// <summary>
            ///     Get the Id for this <see cref="UnlockPlanogramComparisonTemplateCommand" /> instance.
            /// </summary>
            public Object Id { get { return ReadProperty(IdProperty); } }

            #endregion

            #endregion

            #region Constructor

            /// <summary>
            ///     Initialize a new instance of <see cref="LockPlanogramComparisonTemplateCommand"/>.
            /// </summary>
            /// <param name="dalType">The Dal Factory Type for the new instance.</param>
            /// <param name="id">The Id for the new instance.</param>
            public LockPlanogramComparisonTemplateCommand(PlanogramComparisonTemplateDalType dalType, Object id)
            {
                LoadProperty(DalFactoryTypeProperty, dalType);
                LoadProperty(IdProperty, id);
            }

            #endregion
        }

        #endregion

        #region UnlockPlanogramComparisonTemplateCommand

        /// <summary>
        ///     Command to unlock a <see cref="PlanogramComparisonTemplate"/>.
        /// </summary>
        [Serializable]
        private partial class UnlockPlanogramComparisonTemplateCommand : CommandBase<UnlockPlanogramComparisonTemplateCommand>
        {
            #region Properties

            #region DalFactoryType

            /// <summary>
            ///     <see cref="PropertyInfo{T}" /> for the <see cref="DalFactoryType" /> property.
            /// </summary>
            public static readonly PropertyInfo<PlanogramComparisonTemplateDalType> DalFactoryTypeProperty =
                RegisterProperty<PlanogramComparisonTemplateDalType>(c => c.DalFactoryType);

            /// <summary>
            ///     Get the Dal Factory Type for this <see cref="UnlockPlanogramComparisonTemplateCommand" /> instance.
            /// </summary>
            public PlanogramComparisonTemplateDalType DalFactoryType { get { return ReadProperty(DalFactoryTypeProperty); } }

            #endregion

            #region Id

            /// <summary>
            ///     <see cref="PropertyInfo{T}" /> for the <see cref="Id" /> property.
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty = RegisterProperty<Object>(c => c.Id);

            /// <summary>
            ///     Get the Id for this <see cref="UnlockPlanogramComparisonTemplateCommand" /> instance.
            /// </summary>
            public Object Id { get { return ReadProperty(IdProperty); } }

            #endregion

            #endregion

            #region Constructor

            /// <summary>
            ///     Initialize a new instance of <see cref="LockPlanogramComparisonTemplateCommand"/>.
            /// </summary>
            /// <param name="dalType">The Dal Factory Type for the new instance.</param>
            /// <param name="id">The Id for the new instance.</param>
            public UnlockPlanogramComparisonTemplateCommand(PlanogramComparisonTemplateDalType dalType, Object id)
            {
                LoadProperty(DalFactoryTypeProperty, dalType);
                LoadProperty(IdProperty, id);
            }

            #endregion
        }

        #endregion

        #endregion

        #region Static Constructor

        /// <summary>
        ///     Static constructor to set base properties.
        /// </summary>
        static PlanogramComparisonTemplate()
        {
            FriendlyName = Message.PlanogramComparisonTemplate_FriendlyName;
        }

        #endregion

        #region Properties

        #region DalFactoryType

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> for the <see cref="DalFactoryType" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramComparisonTemplateDalType> DalFactoryTypeProperty =
            RegisterModelProperty<PlanogramComparisonTemplateDalType>(c => c.DalFactoryType);

        /// <summary>
        ///     Get the Dal Factory Type for this <see cref="PlanogramComparisonTemplate" /> instance.
        /// </summary>
        private PlanogramComparisonTemplateDalType DalFactoryType { get { return GetProperty(DalFactoryTypeProperty); } }

        #endregion

        #region DalFactoryName

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> for the <see cref="DalFactoryName" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> DalFactoryNameProperty = RegisterModelProperty<String>(c => c.DalFactoryName);

        /// <summary>
        ///     Get the Dal Factory Name for this <see cref="PlanogramComparisonTemplate" /> instance.
        /// </summary>
        protected override String DalFactoryName { get { return GetProperty(DalFactoryNameProperty); } }

        #endregion

        #region IsReadOnly

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> for the <see cref="IsReadOnly" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> IsReadOnlyProperty = RegisterModelProperty<Boolean>(c => c.IsReadOnly);

        /// <summary>
        ///     Get whether this instance of <see cref="PlanogramComparisonTemplate" /> can be saved.
        /// </summary>
        public Boolean IsReadOnly { get { return GetProperty(IsReadOnlyProperty); } }

        #endregion

        #region Id

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> for the <see cref="Id" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<Object> IdProperty = RegisterModelProperty<Object>(o => o.Id);

        /// <summary>
        ///     Get/set the Id for this <see cref="PlanogramComparisonTemplate" /> instance.
        /// </summary>
        public Object Id { get { return GetProperty(IdProperty); } set { SetProperty(IdProperty, value); } }

        #endregion

        #region RowVersion

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> for the <see cref="RowVersion" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty = RegisterModelProperty<RowVersion>(o => o.RowVersion);

        /// <summary>
        ///     Get the row version timestamp for this <see cref="PlanogramComparisonTemplate" /> instance.
        /// </summary>
        public RowVersion RowVersion { get { return GetProperty(RowVersionProperty); } set { SetProperty(RowVersionProperty, value); } }

        #endregion

        #region DateCreated

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> for the <see cref="DateCreated" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty = RegisterModelProperty<DateTime>(o => o.DateCreated);

        /// <summary>
        ///     Get the date this <see cref="PlanogramComparisonTemplate" /> instance was created.
        /// </summary>
        public DateTime DateCreated { get { return GetProperty(DateCreatedProperty); } }

        #endregion

        #region DateLastModified

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> for the <see cref="DateLastModified" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty = RegisterModelProperty<DateTime>(o => o.DateLastModified);

        /// <summary>
        ///     Get the date this <see cref="PlanogramComparisonTemplate" /> instance was modefied last.
        /// </summary>
        public DateTime DateLastModified { get { return GetProperty(DateLastModifiedProperty); } }

        #endregion

        #region EntityId

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> for the <see cref="EntityId" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty = RegisterModelProperty<Int32>(o => o.EntityId);

        /// <summary>
        ///     Get Id for this <see cref="PlanogramComparisonTemplate" /> instance's assigned Entity.
        /// </summary>
        public Int32 EntityId { get { return GetProperty(EntityIdProperty); } set { SetProperty(EntityIdProperty, value); } }

        #endregion

        #region Name

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> for the <see cref="DateLastModified" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty = RegisterModelProperty<String>(o => o.Name);

        /// <summary>
        ///     Get/Set the name for this instance of <see cref="PlanogramComparisonTemplate" />.
        /// </summary>
        /// <remarks>This property is mandatory.</remarks>
        public String Name { get { return GetProperty(NameProperty); } set { SetProperty(NameProperty, value); } }

        #endregion

        #region Description

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> for the <see cref="DateLastModified" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty = RegisterModelProperty<String>(o => o.Description);

        /// <summary>
        ///     Get/Set the description text for this instance of <see cref="PlanogramComparisonTemplate" />.
        /// </summary>
        public String Description { get { return GetProperty(DescriptionProperty); } set { SetProperty(DescriptionProperty, value); } }

        #endregion

        #region DateLastCompared

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> for the <see cref="DateLastModified" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateLastComparedProperty = RegisterModelProperty<DateTime?>(o => o.DateLastCompared);

        /// <summary>
        ///     Get/Set the last date this instance of <see cref="PlanogramComparisonTemplate" /> was compared.
        /// </summary>
        /// <remarks>This property is only relevant for instances, it is here just to comply with the interface.</remarks>
        public DateTime? DateLastCompared { get { return GetProperty(DateLastComparedProperty); } set { SetProperty(DateLastComparedProperty, value); } }

        #endregion

        #region IgnoreNonPlacedProducts

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> for the <see cref="DateLastModified" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IgnoreNonPlacedProductsProperty = RegisterModelProperty<Boolean>(o => o.IgnoreNonPlacedProducts);

        /// <summary>
        ///     Get/Set the IgnoreNonPlacedProducts for this instance of <see cref="PlanogramComparisonTemplate" />.
        /// </summary>
        /// <remarks>This property is mandatory.</remarks>
        public Boolean IgnoreNonPlacedProducts { get { return GetProperty(IgnoreNonPlacedProductsProperty); } set { SetProperty(IgnoreNonPlacedProductsProperty, value); } }

        #endregion

        #region DataOrderType

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> for the <see cref="DateLastModified" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<DataOrderType> DataOrderTypeProperty = RegisterModelProperty<DataOrderType>(o => o.DataOrderType);

        /// <summary>
        ///     Get/Set the DataOrderType for this instance of <see cref="PlanogramComparisonTemplate" />.
        /// </summary>
        /// <remarks>This property is mandatory.</remarks>
        public DataOrderType DataOrderType { get { return GetProperty(DataOrderTypeProperty); } set { SetProperty(DataOrderTypeProperty, value); } }

        #endregion

        #region Comparison Fields

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> for the <see cref="ComparisonFields" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramComparisonTemplateFieldList> ComparisonFieldsProperty =
            RegisterModelProperty<PlanogramComparisonTemplateFieldList>(o => o.ComparisonFields);

        /// <summary>
        ///     Get the list of <see cref="PlanogramComparisonField" /> items used to compare different planograms.
        /// </summary>
        public PlanogramComparisonTemplateFieldList ComparisonFields { get { return GetProperty(ComparisonFieldsProperty); } }

        /// <summary>
        ///     Get the list of <see cref="PlanogramComparisonField" /> items used to compare different planograms.
        /// </summary>
        IEnumerable<IPlanogramComparisonSettingsField> IPlanogramComparisonSettings.ComparisonFields
        {
            get { return ComparisonFields; }
        }

        #endregion

        /// <summary>
        ///     Gets the file extension to use for planogram comparison templates.
        /// </summary>
        public static String FileExtension { get { return Constants.PlanogramComparisonTemplateFileExtension; } }

        #endregion

        #region Business Rules

        /// <summary>
        ///     Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
        }

        #endregion

        #region Authorization Rules

        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository

        /// <summary>
        ///     Returns a dictionary of the current role permissions allocated to the user for this object type.
        /// </summary>
        /// <returns></returns>
        public static IDictionary<AuthorizationActions, Boolean> GetUserPermissions()
        {
            return new Dictionary<AuthorizationActions, Boolean>()
            {
                {AuthorizationActions.CreateObject, ApplicationContext.User.IsInRole(DomainPermission.PlanogramComparisonTemplateCreate.ToString())},
                {AuthorizationActions.GetObject, ApplicationContext.User.IsInRole(DomainPermission.PlanogramComparisonTemplateGet.ToString())},
                {AuthorizationActions.EditObject, ApplicationContext.User.IsInRole(DomainPermission.PlanogramComparisonTemplateEdit.ToString())},
                {AuthorizationActions.DeleteObject, ApplicationContext.User.IsInRole(DomainPermission.PlanogramComparisonTemplateDelete.ToString())},
            };
        }

        #endregion

        #region Criteria

        #region FetchByIdCriteria

        /// <summary>
        ///     Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region Dal Factory Type

            /// <summary>
            ///     Metadata for the <see cref="DalFactoryType" /> property.
            /// </summary>
            private static readonly PropertyInfo<PlanogramComparisonTemplateDalType> SourceTypeCriteria =
                RegisterProperty<PlanogramComparisonTemplateDalType>(o => o.DalFactoryType);

            /// <summary>
            ///     Gets or sets the value for the <see cref="DalFactoryType" /> property.
            /// </summary>
            public PlanogramComparisonTemplateDalType DalFactoryType { get { return ReadProperty(SourceTypeCriteria); } }

            #endregion

            #region Id

            /// <summary>
            ///     Metadata for the <see cref="Id" /> property.
            /// </summary>
            private static readonly PropertyInfo<Object> IdCriteria =
                RegisterProperty<Object>(c => c.Id);

            /// <summary>
            ///     Gets the unique value used to identifiy the instance to be fetched.
            /// </summary>
            public Object Id { get { return ReadProperty(IdCriteria); } }

            #endregion

            #region Read Only

            /// <summary>
            ///     Metadata for the <see cref="ReadOnly" /> property.
            /// </summary>
            private static readonly PropertyInfo<Boolean> ReadOnlyCriteria =
                RegisterProperty<Boolean>(c => c.ReadOnly);

            /// <summary>
            ///     Get whether to open the file as read only or not.
            /// </summary>
            public Boolean ReadOnly { get { return ReadProperty(ReadOnlyCriteria); } }

            #endregion

            #endregion

            #region Constructor

            /// <summary>
            ///     Creates a new instance of <see cref="PlanogramComparisonTemplate.FetchByIdCriteria" />.
            /// </summary>
            /// <param name="sourceType">The source to fetch the instance from.</param>
            /// <param name="id">Unique id value identifying the instance to be fetched.</param>
            /// <param name="readOnly">Whether to open the file as read only or not (by default <c>false</c>).</param>
            public FetchByIdCriteria(PlanogramComparisonTemplateDalType sourceType, Object id, Boolean readOnly = false)
            {
                LoadProperty(SourceTypeCriteria, sourceType);
                LoadProperty(ReadOnlyCriteria, readOnly);
                LoadProperty(IdCriteria, id);
            }

            #endregion
        }

        #endregion

        #region FetchByEntityIdName Criteria

        [Serializable]
        public class FetchByEntityIdNameCriteria : CriteriaBase<FetchByEntityIdNameCriteria>
        {
            #region Properties

            #region Entity Id

            public static readonly PropertyInfo<Int32> CriteriaEntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);

            public Int32 EntityId { get { return ReadProperty(CriteriaEntityIdProperty); } }

            #endregion

            #region Name

            public static readonly PropertyInfo<String> CriteriaNameProperty =
                RegisterProperty<String>(c => c.Name);

            public String Name { get { return ReadProperty(CriteriaNameProperty); } }

            #endregion

            #endregion

            #region Constructor

            public FetchByEntityIdNameCriteria(Int32 entityId, String name)
            {
                LoadProperty(CriteriaEntityIdProperty, entityId);
                LoadProperty(CriteriaNameProperty, name);
            }

            #endregion
        }

        #endregion

        #endregion

        #region Factory Methods

        public static PlanogramComparisonTemplate NewPlanogramComparisonTemplate()
        {
            var item = new PlanogramComparisonTemplate();
            item.Create();

            return item;
        }

        /// <summary>
        ///     Creates, initializes and returns a new <see cref="PlanogramComparisonTemplate" /> instance for the
        ///     <see cref="Entity" />
        ///     with the provided <paramref name="entityId" />.
        /// </summary>
        /// <param name="entityId">Unique identifier for the <see cref="Entity" /> model that will contain this instance.</param>
        public static PlanogramComparisonTemplate NewPlanogramComparisonTemplate(Int32 entityId)
        {
            var item = new PlanogramComparisonTemplate();
            item.Create(entityId);

            return item;
        }

        /// <summary>
        ///     Creates, initializes and returns a new <see cref="PlanogramComparisonTemplate" /> instance using the given
        ///     <paramref name="source" /> to set all the initial values.
        /// </summary>
        /// <param name="entityId">Unique identifier for the <see cref="Entity" /> model that will contain this instance.</param>
        /// <param name="source">
        ///     An instance of a type implementing <see cref="IPlanogramComparisonSettings" /> to use for the
        ///     initial data of the new instance.
        /// </param>
        public static PlanogramComparisonTemplate NewPlanogramComparisonTemplate(Int32 entityId, IPlanogramComparisonSettings source)
        {
            var item = new PlanogramComparisonTemplate();
            item.Create(entityId, source);

            return item;
        }

        /// <summary>
        ///     Create a <see cref="PlanogramComparisonTemplate"/> instance with default values for new entities.
        /// </summary>
        /// <param name="entityId">The ID of the entity for which to create the default template.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonTemplate"/>.</returns>
        public static PlanogramComparisonTemplate CreateDefaultPlanogramComparisonTemplate(Int32 entityId)
        {
            PlanogramComparisonTemplate template = NewPlanogramComparisonTemplate(entityId);
            template.Name = Message.PlanogramComparisonTemplate_DefaultTemplate_Name;
            template.Description = Message.PlanogramComparisonTemplate_DefaultTemplate_Description;
            return template;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        ///     Called when creating a new object of <see cref="PlanogramComparisonTemplate" />.
        /// </summary>
        private new void Create()
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty(DateCreatedProperty, DateTime.UtcNow);
            LoadProperty(DateLastModifiedProperty, DateTime.UtcNow);

            LoadProperty(IgnoreNonPlacedProductsProperty, true);
            LoadProperty(DataOrderTypeProperty, DataOrderType.ByPlanogram);
            LoadProperty(ComparisonFieldsProperty, PlanogramComparisonTemplateFieldList.NewPlanogramComparisonTemplateFieldList());
            base.Create();
        }

        /// <summary>
        ///     Called when creating a new object of <see cref="PlanogramComparisonTemplate" />.
        /// </summary>
        private void Create(Int32 entityId)
        {
            LoadProperty(EntityIdProperty, entityId);
            Create();
        }

        /// <summary>
        ///     Creates a new <see cref="PlanogramComparisonTemplate" /> instance initializing it to the values in
        ///     <paramref name="source" />.
        /// </summary>
        /// <param name="entityId">Unique Id of the entity this validation template should be in.</param>
        /// <param name="source">
        ///     An instance of an object implemeting <see cref="IPlanogramComparisonSettings" /> from which to
        ///     initialize the new instance.
        /// </param>
        private void Create(Int32 entityId, IPlanogramComparisonSettings source)
        {
            PlanogramComparisonTemplateFieldList comparisonFields = PlanogramComparisonTemplateFieldList.NewPlanogramComparisonTemplateFieldList();
            comparisonFields.AddRange(source.ComparisonFields);
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty(EntityIdProperty, entityId);
            LoadProperty(DateCreatedProperty, DateTime.UtcNow);
            LoadProperty(DateLastModifiedProperty, DateTime.UtcNow);

            LoadProperty(NameProperty, source.Name);
            LoadProperty(DescriptionProperty, source.Description);
            LoadProperty(DateLastComparedProperty, source.DateLastCompared);
            LoadProperty(IgnoreNonPlacedProductsProperty, source.IgnoreNonPlacedProducts);
            LoadProperty(DataOrderTypeProperty, source.DataOrderType);
            LoadProperty(ComparisonFieldsProperty, comparisonFields);
            base.Create();
        }

        #endregion

        #region File Locking

        /// <summary>
        ///     Lock this instance's underlying file, 
        ///     so that it cannot be edited by another process.
        /// </summary>
        internal static void LockPlanogramComparisonTemplateByFileName(String fileName)
        {
            DataPortal.Execute(new LockPlanogramComparisonTemplateCommand(PlanogramComparisonTemplateDalType.FileSystem, fileName));
        }

        /// <summary>
        ///     Unlock this instance's underlying file,
        ///     so that it can be edited by other processes.
        /// </summary>
        public static void UnlockPlanogramComparisonTemplateByFileName(String fileName)
        {
            DataPortal.Execute(new UnlockPlanogramComparisonTemplateCommand(PlanogramComparisonTemplateDalType.FileSystem, fileName));
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        ///     Returns a <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </returns>
        public override String ToString()
        {
            return Name;
        }

        /// <summary>
        ///     Loads the data contained in the <paramref name="source" /> instance implementing
        ///     <see cref="IPlanogramComparisonSettings" /> into the current instance.
        /// </summary>
        /// <param name="source">
        ///     Instance containing the data to load, and that implements
        ///     <see cref="IPlanogramComparisonSettings" />.
        /// </param>
        public void Load(IPlanogramComparisonSettings source)
        {
            Name = source.Name;
            Description = source.Description;
            DateLastCompared = source.DateLastCompared;
            IgnoreNonPlacedProducts = source.IgnoreNonPlacedProducts;
            DataOrderType = source.DataOrderType;
            ResetComparisonFields();
            AddComparisonFields(source.ComparisonFields);
        }

        /// <summary>
        /// Saves this to a new file.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public PlanogramComparisonTemplate SaveAsFile(String path)
        {
            //  Assign the correct extension, overriding any possible choice by the user.
            String fileName = Path.ChangeExtension(path, FileExtension);

            //  Store the previous file name if there was one and 
            //  this model is not new (so it has been saved before).
            String oldFileName = null;
            if (!IsNew &&
                Id as String != fileName)
            {
                if (DalFactoryType == PlanogramComparisonTemplateDalType.FileSystem)
                {
                    oldFileName = Id as String;
                }
                MarkGraphAsNew();
            }

            //  Reset the Dal Factory settings and save.
            LoadProperty(DalFactoryTypeProperty, PlanogramComparisonTemplateDalType.FileSystem);
            LoadProperty(DalFactoryNameProperty, "");
            LoadProperty(IdProperty, fileName);
            Name = Path.GetFileNameWithoutExtension(fileName);
            PlanogramComparisonTemplate saveAsFile = Save();

            //  Unlock the old file name if there was one
            if (!String.IsNullOrEmpty(oldFileName)) UnlockPlanogramComparisonTemplateByFileName(oldFileName);

            //  Update the ID with the file name.
            LoadProperty(IdProperty, fileName);

            return saveAsFile;
        }

        /// <summary>
        ///     Add the fields in the enumeration to the current comparison fields in this instance.
        /// </summary>
        /// <param name="comparisonFields">The collection of <see cref="IPlanogramComparisonSettingsField"/> to add.</param>
        public void AddComparisonFields(IEnumerable<IPlanogramComparisonSettingsField> comparisonFields)
        {
            ComparisonFields.AddRange(comparisonFields);
        }

        /// <summary>
        ///     Add the fields in the enumeration to the current comparison fields in this instance.
        /// </summary>
        /// <param name="itemType">The <see cref="PlanogramItemType" /> that this range should be targeting.</param>
        /// <param name="comparisonFields">The collection of <see cref="IPlanogramComparisonSettingsField"/> to add.</param>
        public void AddComparisonFields(PlanogramItemType itemType, IEnumerable<IPlanogramComparisonSettingsField> comparisonFields)
        {
            ComparisonFields.AddRange(itemType, comparisonFields);
        }

        /// <summary>
        ///     Add a collection of <see cref="IPlanogramComparisonSettingsField" /> derived from an enumeration of
        ///     <see cref="ObjectFieldInfo" />.
        /// </summary>
        /// <param name="itemType">The <see cref="PlanogramItemType" /> that this range should be targeting.</param>
        /// <param name="fieldInfos">The enumeration of <see cref="ObjectFieldInfo" /> from which to derive the new items.</param>
        public void AddComparisonFields(PlanogramItemType itemType, Dictionary<ObjectFieldInfo, Boolean> fieldInfos)
        {
            ComparisonFields.AddRange(itemType, fieldInfos);
        }

        /// <summary>
        ///     Resets the internal collection, clearing all existing comparison fields in it.
        /// </summary>
        public void ResetComparisonFields()
        {
            ComparisonFields.Clear();
        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(Boolean disposing)
        {
            if (!disposing || _isDisposed) return;

            if (!IsNew)
            {
                if (DalFactoryType == PlanogramComparisonTemplateDalType.FileSystem &&
                    Id is String)
                {
                    UnlockPlanogramComparisonTemplateByFileName((String)Id);
                }
            }

            _isDisposed = true;
        }

        #endregion
    }
}