﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.IO;

namespace Galleria.Ccm.Model
{
    public partial class Folder
    {
        #region Constructors
        private Folder() { } //force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing folder at the given path.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static Folder FetchByDirectoryPath(String path)
        {
            return DataPortal.Fetch<Folder>(new FetchByIdCriteria(FolderDalFactoryType.FileSystem, path));
        }

        /// <summary>
        /// Returns an existing Folder with the given id
        /// </summary>
        public static Folder FetchById(Int32 id)
        {
            return DataPortal.Fetch<Folder>(id);
        }


        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static Folder Fetch(IDalContext dalContext, FolderDto dto)
        {
            return DataPortal.FetchChild<Folder>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, FolderDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(FileSystemPathProperty, dto.FileSystemPath);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private FolderDto GetDataTransferObject(Folder parent)
        {
            FolderDto dto = GetDataTransferObject();
            dto.ParentFolderId = parent.Id;

            //set the new filesystem path
            if (this.DalFactoryName == Constants.UserDal)
            {
                dto.FileSystemPath = Path.Combine((String)parent.Id, dto.Name);
            }

            return dto;
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private FolderDto GetDataTransferObject()
        {
            return new FolderDto
            {
                Id = ReadProperty<Object>(IdProperty),
                Name = ReadProperty<String>(NameProperty),
                FileSystemPath = ReadProperty<String>(FileSystemPathProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning FetchById
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            IDalFactory dalFactory = this.GetDalFactory(criteria.DalType);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IFolderDal dal = dalContext.GetDal<IFolderDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.Id));
                }
            }
        }


        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FolderDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, Folder parent)
        {
            FolderDto dto = this.GetDataTransferObject(parent);
            Object oldId = dto.Id;
            using (IFolderDal dal = dalContext.GetDal<IFolderDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<String>(FileSystemPathProperty, dto.FileSystemPath);
            dalContext.RegisterId<Folder>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this instance
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                FolderDto dto = GetDataTransferObject();
                using (IFolderDal dal = dalContext.GetDal<IFolderDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<String>(FileSystemPathProperty, dto.FileSystemPath);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }

        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, Folder parent)
        {
            FolderDto dto = this.GetDataTransferObject(parent);
            using (IFolderDal dal = dalContext.GetDal<IFolderDal>())
            {
                dal.Update(dto);
            }
            this.LoadProperty<String>(FileSystemPathProperty, dto.FileSystemPath);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        //Commented out as we should not be allowed to delete the root folder.
        ///// <summary>
        ///// Called when this instance is being deleted
        ///// </summary>
        //[SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        //protected override void DataPortal_DeleteSelf()
        //{
        //    IDalFactory dalFactory = GetDalFactory();
        //    using (IDalContext dalContext = dalFactory.CreateContext())
        //    {
        //        dalContext.Begin();
        //        using (IFolderDal dal = dalContext.GetDal<IFolderDal>())
        //        {
        //            dal.DeleteById(this.Id);
        //        }
        //        dalContext.Commit();
        //    }
        //}

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, Folder parent)
        {
            using (IFolderDal dal = dalContext.GetDal<IFolderDal>())
            {
                dal.DeleteById(this.Id);
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the correct dal factory based on the daltype and args.
        /// </summary>
        /// <param name="dalType"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private IDalFactory GetDalFactory(FolderDalFactoryType dalType)
        {
            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(dalType, out dalFactoryName);

            this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            //this.LoadProperty<FolderDalFactoryType>(DalFactoryTypeProperty, dalType);

            return dalFactory;
        }

        /// <summary>
        /// Returns the correct dal factory for this instance
        /// </summary>
        protected override IDalFactory GetDalFactory()
        {
            //if (String.IsNullOrEmpty(this.DalFactoryName))
            //{
            //    String dalFactoryName;
            //    IDalFactory dalFactory = GetDalFactory(this.DalFactoryType, out dalFactoryName);
            //    this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            //}

            return DalContainer.GetDalFactory(this.DalFactoryName);
        }

        /// <summary>
        /// Returns the correct dal factory based on the given type and args.
        /// </summary>
        private static IDalFactory GetDalFactory(FolderDalFactoryType dalType, out String dalFactoryName)
        {
            switch (dalType)
            {
                case FolderDalFactoryType.FileSystem:
                    dalFactoryName = Constants.UserDal;
                    return DalContainer.GetDalFactory(dalFactoryName);

                default:
                    dalFactoryName = DalContainer.DalName;
                    return DalContainer.GetDalFactory();
            }

        }
        #endregion

    }
}