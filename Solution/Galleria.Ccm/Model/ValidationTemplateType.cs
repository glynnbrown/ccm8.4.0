﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26815 : A.Silva ~ Created

#endregion

#endregion

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Defines the allowed values to indicate the source of a <see cref="ValidationTemplate"/>.
    /// </summary>
    public enum ValidationTemplateType
    {
        /// <summary>
        ///     The source for the <see cref="ValidationTemplate"/> instance has not been set and is unknown.
        /// </summary>
        Unknown = 0,
        /// <summary>
        ///     The source for the <see cref="ValidationTemplate"/> is the file system.
        /// </summary>
        FileSystem = 1
    }

    ///// <summary>
    /////     Contains methods to help using the <see cref="ValidationTemplateType"/> values.
    ///// </summary>
    //internal static class ValidationTemplateTypeHelper
    //{
        
    //}
}