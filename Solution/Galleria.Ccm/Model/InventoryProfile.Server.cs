﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26124 : I.George
//		Created (Auto-generated)
// V8-28013 : A.Kuszyk
//  Added FetchByEntityIdName.
#endregion
#region Version History: (CCM 820)
// CCM-30759 : J.Pickup
//		InventoryProfileType added.
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    public partial class InventoryProfile
    {
        #region Constructors
        private InventoryProfile() { } //force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing InventoryProfile with the given id
        /// </summary>
        public static InventoryProfile FetchById(Int32 id)
        {
            return DataPortal.Fetch<InventoryProfile>(new FetchByIdCriteria(id));
        }

        public static InventoryProfile FetchByEntityIdName(Int32 entityId, String name)
        {
            return DataPortal.Fetch<InventoryProfile>(new FetchByEntityIdNameCriteria(entityId, name));
        }

        internal static InventoryProfile FetchInventoryProfile(IDalContext dalContext, InventoryProfileDto dto)
        {
            return DataPortal.FetchChild<InventoryProfile>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, InventoryProfileDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Single>(CasePackProperty, dto.CasePack);
            this.LoadProperty<Single>(DaysOfSupplyProperty, dto.DaysOfSupply);
            this.LoadProperty<Single>(ShelfLifeProperty, dto.ShelfLife);
            this.LoadProperty<Single>(ReplenishmentDaysProperty, dto.ReplenishmentDays);
            this.LoadProperty<Single>(WasteHurdleUnitsProperty, dto.WasteHurdleUnits);
            this.LoadProperty<Single>(WasteHurdleCasePackProperty, dto.WasteHurdleCasePack);
            this.LoadProperty<Int32>(MinUnitsProperty, dto.MinUnits);
            this.LoadProperty<Int32>(MinFacingsProperty, dto.MinFacings);
            this.LoadProperty<PlanogramInventoryMetricType>(InventoryProfileTypeProperty, (PlanogramInventoryMetricType)dto.InventoryProfileType);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);
            

        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private InventoryProfileDto GetDataTransferObject()
        {
            return new InventoryProfileDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                Name = ReadProperty<String>(NameProperty),
                CasePack = ReadProperty<Single>(CasePackProperty),
                DaysOfSupply = ReadProperty<Single>(DaysOfSupplyProperty),
                ShelfLife = ReadProperty<Single>(ShelfLifeProperty),
                ReplenishmentDays = ReadProperty<Single>(ReplenishmentDaysProperty),
                WasteHurdleUnits = ReadProperty<Single>(WasteHurdleUnitsProperty),
                WasteHurdleCasePack = ReadProperty<Single>(WasteHurdleCasePackProperty),
                MinUnits = ReadProperty<Int32>(MinUnitsProperty),
                MinFacings = ReadProperty<Int32>(MinFacingsProperty),
                InventoryProfileType = (Byte)ReadProperty<PlanogramInventoryMetricType>(InventoryProfileTypeProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty),

            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning FetchById
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IInventoryProfileDal dal = dalContext.GetDal<IInventoryProfileDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.Id));
                }
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdNameCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IInventoryProfileDal dal = dalContext.GetDal<IInventoryProfileDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchByEntityIdName(criteria.EntityId, criteria.Name));
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                InventoryProfileDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (IInventoryProfileDal dal = dalContext.GetDal<IInventoryProfileDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<InventoryProfile>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this instance
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                InventoryProfileDto dto = GetDataTransferObject();
                using (IInventoryProfileDal dal = dalContext.GetDal<IInventoryProfileDal>())
                {
                    dal.Update(dto);

                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when this instance is being deleted
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IInventoryProfileDal dal = dalContext.GetDal<IInventoryProfileDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #endregion

    }
}
