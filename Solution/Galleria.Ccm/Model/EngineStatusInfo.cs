﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-27411 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Creates an instance of a read-only Engine Status model object, containing
    /// information regarding the current engine instances and message data
    /// </summary>
    public partial class EngineStatusInfo : ModelReadOnlyObject<EngineStatusInfo>
    {
        #region Properties

        #region Engine Instances
        /// <summary>
        /// Engine Instances property definition
        /// </summary>
        public static readonly ModelPropertyInfo<EngineInstanceInfoList> EngineInstancesProperty =
            RegisterModelProperty<EngineInstanceInfoList>(c => c.EngineInstances);
        /// <summary>
        /// Returns the engine instances stored in the database
        /// </summary>
        public EngineInstanceInfoList EngineInstances
        {
            get { return GetProperty<EngineInstanceInfoList>(EngineInstancesProperty); }
        }
        #endregion

        #region MessageCount
        /// <summary>
        /// Message Count property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> MessageCountProperty =
            RegisterModelProperty<Int32>(c => c.MessageCount);
        /// <summary>
        /// The number of messages in the queue
        /// </summary>
        public Int32 MessageCount
        {
            get { return GetProperty<Int32>(MessageCountProperty); }
        }
        #endregion

        #region ProcessedMessageCount
        /// <summary>
        /// Message Count property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProcessedMessageCountProperty =
            RegisterModelProperty<Int32>(c => c.ProcessedMessageCount);
        /// <summary>
        /// The number of processed messages in the queue
        /// </summary>
        public Int32 ProcessedMessageCount
        {
            get { return GetProperty<Int32>(ProcessedMessageCountProperty); }
        }
        #endregion

        #endregion

        #region Helper Properties

        #region EngineInstanceCount
        /// <summary>
        /// Message Count property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EngineInstanceCountProperty =
            RegisterModelProperty<Int32>(c => c.EngineInstanceCount);
        /// <summary>
        /// The number of Engine Instances
        /// </summary>
        public Int32 EngineInstanceCount
        {
            get 
            {
                if (GetProperty<EngineInstanceInfoList>(EngineInstancesProperty) != null)
                {
                    EngineInstanceInfoList instanceInfos = GetProperty<EngineInstanceInfoList>(EngineInstancesProperty);
                    return instanceInfos.Count;
                }
                return 0;
            }
        }
        #endregion

        #region HealthyEngineInstanceCount
        /// <summary>
        /// Online Engine Instance Count property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> HealthyEngineInstanceCountProperty =
            RegisterModelProperty<Int32>(c => c.HealthyEngineInstanceCount);
        /// <summary>
        /// The number of Engine Instances that have reported a heartbeat in the last 5 minutes
        /// </summary>
        public Int32 HealthyEngineInstanceCount
        {
            get
            {
                if (GetProperty<EngineInstanceInfoList>(EngineInstancesProperty) != null)
                {
                    EngineInstanceInfoList instanceInfos = GetProperty<EngineInstanceInfoList>(EngineInstancesProperty);
                    return (instanceInfos.Where(i => i.IsHealthy == true)).Count();
                }
                return 0;
            }
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(EngineStatusInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(EngineStatusInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(EngineStatusInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(EngineStatusInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
