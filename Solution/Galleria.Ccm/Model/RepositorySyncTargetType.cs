﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Denotes the available repository sync target types
    /// </summary>
    public enum RepositorySyncTargetType
    {
        Assembly = 0,
        Endpoint = 1
    }

    public static class RepositorySyncTargetTypeHelper
    {
        public static readonly Dictionary<RepositorySyncTargetType, String> FriendlyName =
            new Dictionary<RepositorySyncTargetType, String>()
            {
                { RepositorySyncTargetType.Assembly, Message.Enum_RepositorySyncTargetType_Assembly},
                { RepositorySyncTargetType.Endpoint, Message.Enum_RepositorySyncTargetType_Endpoint}
            };

        public static RepositorySyncTargetType? RepositorySyncTargetTypeGetEnum(String description)
        {
            foreach (KeyValuePair<RepositorySyncTargetType, String> keyPair in RepositorySyncTargetTypeHelper.FriendlyName)
            {
                if (keyPair.Value.Equals(description))
                {
                    return keyPair.Key;
                }
            }
            return null;
        }
    }
}
