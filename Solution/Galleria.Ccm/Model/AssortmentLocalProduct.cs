﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
// V8-26704 : A.Kuszyk
//  Implemented IPlanogramAssortmentLocalProduct.
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
#endregion

#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using AutoMapper;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A class representing an Assortment Region Product within CCM
    /// (Child of Assortment)
    /// This is a child Content object and so it has the following properties/actions:
    /// - This object does NOT have a DateDeleted property or DateDeleted field in its supporting database table
    /// - if its parent is deleted, it is not removed or marked as deleted (this allows for parent 'undeletes')
    /// - if it is deleted directly, it's associated record is removed from the database. 
    /// </summary>
    [Serializable]
    [DefaultNewMethod("NewAssortmentLocalProduct")]
    public partial class AssortmentLocalProduct : ModelObject<AssortmentLocalProduct>, IPlanogramAssortmentLocalProduct
    {
        #region Static Constructor
        static AssortmentLocalProduct()
        {
        }
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The AssortmentRegion id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<Int16> LocationIdProperty =
            RegisterModelProperty<Int16>(c => c.LocationId);
        /// <summary>
        /// The Location Id
        /// </summary>
        public Int16 LocationId
        {
            get { return GetProperty<Int16>(LocationIdProperty); }
            set { SetProperty<Int16>(LocationIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> LocationCodeProperty =
            RegisterModelProperty<String>(c => c.LocationCode);
        /// <summary>
        /// The Location Code
        /// </summary>
        public String LocationCode
        {
            get { return GetProperty<String>(LocationCodeProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> ProductIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductId);
        /// <summary>
        /// The Product Id
        /// </summary>
        public Int32 ProductId
        {
            get { return GetProperty<Int32>(ProductIdProperty); }
            set { SetProperty<Int32>(ProductIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> ProductGtinProperty =
            RegisterModelProperty<String>(c => c.ProductGtin);
        /// <summary>
        /// The Product GTIN
        /// </summary>
        public String ProductGtin
        {
            get { return GetProperty<String>(ProductGtinProperty); }
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new Required(ProductIdProperty));
            BusinessRules.AddRule(new Required(LocationIdProperty));
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentLocalProduct), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.AssortmentCreate.ToString()));
            BusinessRules.AddRule(typeof(AssortmentLocalProduct), new IsInRole(AuthorizationActions.GetObject, DomainPermission.AssortmentGet.ToString()));
            BusinessRules.AddRule(typeof(AssortmentLocalProduct), new IsInRole(AuthorizationActions.EditObject, DomainPermission.AssortmentEdit.ToString()));
            BusinessRules.AddRule(typeof(AssortmentLocalProduct), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.AssortmentDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentLocalProduct NewAssortmentLocalProduct()
        {
            AssortmentLocalProduct item = new AssortmentLocalProduct();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentLocalProduct NewAssortmentLocalProduct(Int16 locationId, Int32 productId)
        {
            AssortmentLocalProduct item = new AssortmentLocalProduct();
            item.Create(locationId, productId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create(Int16 locationId, Int32 productId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int16>(LocationIdProperty, locationId);
            this.LoadProperty<Int32>(ProductIdProperty, productId);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion
    }
}
