﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//      Initial version.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Helpers;



namespace Galleria.Ccm.Model
{
    public enum FixtureSubComponentYMerchStrategyType
    {
        Manual = 0,
        Bottom = 1,
        BottomStacked = 2,
        Top = 3,
        TopStacked = 4,
        Even = 5
    }

    /// <summary>
    /// PlanogramSubComponentYMerchStrategyType Helper Class
    /// </summary>
    public static class FixtureSubComponentYMerchStrategyTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<FixtureSubComponentYMerchStrategyType, String> FriendlyNames =
            new Dictionary<FixtureSubComponentYMerchStrategyType, String>()
            {
                {FixtureSubComponentYMerchStrategyType.Manual, Message.Enum_FixtureSubComponentYMerchStrategyType_Manual},
                {FixtureSubComponentYMerchStrategyType.Bottom, Message.Enum_FixtureSubComponentYMerchStrategyType_Bottom},
                {FixtureSubComponentYMerchStrategyType.BottomStacked, Message.Enum_FixtureSubComponentYMerchStrategyType_BottomStacked},
                {FixtureSubComponentYMerchStrategyType.Top, Message.Enum_FixtureSubComponentYMerchStrategyType_Top},
                {FixtureSubComponentYMerchStrategyType.TopStacked, Message.Enum_FixtureSubComponentYMerchStrategyType_TopStacked},
                {FixtureSubComponentYMerchStrategyType.Even, Message.Enum_FixtureSubComponentYMerchStrategyType_Even},
            };

        public static FixtureSubComponentYMerchStrategyType Parse(String enumName)
        {
            return EnumHelper.Parse<FixtureSubComponentYMerchStrategyType>(enumName, FixtureSubComponentYMerchStrategyType.Bottom);
        }
    }
}
