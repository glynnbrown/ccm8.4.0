﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentRegionLocationList
    {
        #region Constructor
        private AssortmentRegionLocationList() { } // Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a list of existing items based on their parent assortment region
        /// </summary>
        /// <returns>A list of existing it</returns>
        internal static AssortmentRegionLocationList GetAssortmentRegionLocationList(IDalContext dalContext, Int32 assortmentRegionId)
        {
            return DataPortal.FetchChild<AssortmentRegionLocationList>(dalContext, assortmentRegionId);
        }

        /// <summary>
        /// Returns all locations for the given assortment region
        /// </summary>
        /// <returns>A list of assortment regional locations</returns>
        public static AssortmentRegionLocationList FetchByRegionId(Int32 assortmentRegionId)
        {
            return DataPortal.Fetch<AssortmentRegionLocationList>(assortmentRegionId);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="assortmentId">The parent assortment region id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 assortmentRegionId)
        {
            RaiseListChangedEvents = false;
            using (IAssortmentRegionLocationDal dal = dalContext.GetDal<IAssortmentRegionLocationDal>())
            {
                IEnumerable<AssortmentRegionLocationDto> dtoList = dal.FetchByRegionId(assortmentRegionId);
                foreach (AssortmentRegionLocationDto dto in dtoList)
                {
                    this.Add(AssortmentRegionLocation.GetAssortmentRegionLocation(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when retrieving a list of all regions for an assortment
        /// </summary>
        private void DataPortal_Fetch(Int32 assortmentRegionId)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IAssortmentRegionLocationDal dal = dalContext.GetDal<IAssortmentRegionLocationDal>())
                {
                    IEnumerable<AssortmentRegionLocationDto> dtoList = dal.FetchByRegionId(assortmentRegionId);
                    foreach (AssortmentRegionLocationDto dto in dtoList)
                    {
                        this.Add(AssortmentRegionLocation.GetAssortmentRegionLocation(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}