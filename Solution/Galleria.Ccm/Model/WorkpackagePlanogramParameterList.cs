﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class WorkpackagePlanogramParameterList : ModelList<WorkpackagePlanogramParameterList, WorkpackagePlanogramParameter>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent workpackage planogram
        /// </summary>
        public new WorkpackagePlanogram Parent
        {
            get { return (WorkpackagePlanogram)base.Parent; }
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(WorkpackagePlanogramParameterList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramParameterList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.WorkpackageCreate.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramParameterList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.WorkpackageEdit.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramParameterList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.WorkpackageDelete.ToString()));
        }
        #endregion

        #region Criteria

        #region FetchByWorkpackgeIdCriteria
        /// <summary>
        /// Criteria for FetchByUniqueContentReference
        /// </summary>
        [Serializable]
        public class FetchByWorkpackgePlanogramIdCriteria : CriteriaBase<FetchByWorkpackgePlanogramIdCriteria>
        {
            #region Properties

            #region WorkpackagePlanogramId
            /// <summary>
            /// WorkpackageId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> WorkpackagePlanogramIdProperty =
                RegisterProperty<Int32>(c => c.WorkpackagePlanogramId);
            /// <summary>
            /// Returns the workpackage id
            /// </summary>
            public Int32 WorkpackagePlanogramId
            {
                get { return this.ReadProperty<Int32>(WorkpackagePlanogramIdProperty); }
            }
            #endregion

            #region WorkflowId
            /// <summary>
            /// Workflow id property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> WorkflowIdProperty =
                RegisterProperty<Int32>(c => c.WorkflowId);
            /// <summary>
            /// Returns the workflow id
            /// </summary>
            public Int32 WorkflowId
            {
                get { return this.ReadProperty<Int32>(WorkflowIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByWorkpackgePlanogramIdCriteria(Int32 workpackagePlanogramId, Int32 workflowId)
            {
                this.LoadProperty<Int32>(WorkpackagePlanogramIdProperty, workpackagePlanogramId);
                this.LoadProperty<Int32>(WorkflowIdProperty, workflowId);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        internal static WorkpackagePlanogramParameterList NewWorkpackagePlanogramParameterList()
        {
            WorkpackagePlanogramParameterList item = new WorkpackagePlanogramParameterList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new child instance of this type
        /// </summary>
        protected new void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Adds a new parameter
        /// </summary>
        /// <param name="workflowParameter"></param>
        /// <returns></returns>
        public WorkpackagePlanogramParameter Add(WorkflowTaskParameter workflowParameter)
        {
            WorkpackagePlanogramParameter item = WorkpackagePlanogramParameter.NewWorkpackagePlanogramParameter(workflowParameter);
            this.Add(item);
            return item;
        }

        #endregion
    }
}
