﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
//  V8-26140 : I.George
// Added FetchByEnityIdIncludingDeleteCriteria
#endregion
#region Version History: (CCM 8.0.3)
// V8-29214 : D.Pleasance
//  Removed FetchDeletedByEntityIdCriteria as items are now deleted outright
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla.Rules;
using Galleria.Framework.Model;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Csla;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class AssortmentMinorRevisionInfoList : ModelReadOnlyList<AssortmentMinorRevisionInfoList, AssortmentMinorRevisionInfo>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByEntityId
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : Csla.CriteriaBase<FetchByEntityIdCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
            RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
            }
        }

        /// <summary>
        /// Criteria for FetchByEntityIdChangeDate
        /// </summary>
        [Serializable]
        public class FetchByEntityIdChangeDateCriteria : CriteriaBase<FetchByEntityIdChangeDateCriteria>
        {
            #region Properties

            public static PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static PropertyInfo<DateTime> ChangeDateProperty =
                RegisterProperty<DateTime>(c => c.ChangeDate);
            public DateTime ChangeDate
            {
                get { return ReadProperty<DateTime>(ChangeDateProperty); }
            }

            #endregion

            public FetchByEntityIdChangeDateCriteria(Int32 entityId, DateTime changeDate)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<DateTime>(ChangeDateProperty, changeDate);
            }
        }
        
        /// <summary>
        /// Criteria for FetchAssortmentByEntityIdAssortmentSearchCriteria
        /// </summary>
        [Serializable]
        public class FetchAssortmentByEntityIdAssortmentMinorRevisionSearchCriteria : CriteriaBase<FetchAssortmentByEntityIdAssortmentMinorRevisionSearchCriteria>
        {
            #region Properties
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static readonly PropertyInfo<String> AssortmentSearchCriteriaProperty =
                RegisterProperty<String>(c => c.AssortmentSearchCriteria);
            public String AssortmentSearchCriteria
            {
                get { return ReadProperty<String>(AssortmentSearchCriteriaProperty); }
            }
            #endregion

            public FetchAssortmentByEntityIdAssortmentMinorRevisionSearchCriteria(Int32 entityId, String AssortmentSearchCriteria)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<String>(AssortmentSearchCriteriaProperty, AssortmentSearchCriteria);
            }
        }

        /// <summary>
        /// Criteria for FetchByProductGroupId
        /// </summary>
        [Serializable]
        public class FetchByProductGroupIdCriteria : Csla.CriteriaBase<FetchByProductGroupIdCriteria>
        {
            public static readonly PropertyInfo<Int32> ProductGroupIdProperty =
            RegisterProperty<Int32>(c => c.ProductGroupId);
            public Int32 ProductGroupId
            {
                get { return ReadProperty<Int32>(ProductGroupIdProperty); }
            }

            public FetchByProductGroupIdCriteria(Int32 productGroupId)
            {
                LoadProperty<Int32>(ProductGroupIdProperty, productGroupId);
            }
        }

        #endregion
    }
}