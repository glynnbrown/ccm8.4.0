﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
// V8-35460 : L.Ineson
//  Change TaskType to load from the AssemblyQualifiedName rather than just Name.
// V8-25839: L.Ineson
//  Added Category
#endregion

#region Version History: CCM8.1.1
// V8-28224 : M.Shelley
//  Added Description 
#endregion

#endregion

using System;
using Csla;
using Galleria.Ccm.Engine;

namespace Galleria.Ccm.Model
{
    public partial class EngineTaskInfo
    {
        #region Constructors
        private EngineTaskInfo() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a task info object
        /// </summary>
        internal static EngineTaskInfo GetEngineTaskInfo(String taskType)
        {
            return DataPortal.FetchChild<EngineTaskInfo>(taskType);
        }

        #endregion

        #region Data Access
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(String taskType)
        {
            // create an instance of the task type
            TaskBase task = TaskContainer.CreateTask(taskType);

            // decant the default details into this info object
            this.LoadProperty<String>(TaskTypeProperty, task.TaskType);
            this.LoadProperty<String>(NameProperty, task.Name);
            this.LoadProperty<String>(DescriptionProperty, task.Description);
            this.LoadProperty<String>(CategoryProperty, task.Category);
            this.LoadProperty<Byte>(MajorVersionProperty, task.MajorVersion);
            this.LoadProperty<Byte>(MinorVersionProperty, task.MinorVersion);
            this.LoadProperty<Boolean>(HasPreStepProperty, task.HasPreStep);
            this.LoadProperty<Boolean>(HasPostStepProperty, task.HasPostStep);
            this.LoadProperty<EngineTaskParameterInfoList>(ParametersProperty, EngineTaskParameterInfoList.GetEngineTaskParameterInfoList(task.Parameters));
        }
        #endregion
    }
}
