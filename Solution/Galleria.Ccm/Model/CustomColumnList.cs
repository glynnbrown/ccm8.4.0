#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-24124 : A.Silva ~ Added Available columns for PlanogramProduct.
// V8-26076 : A.Silva ~ Added base group to default columns.
// V8-25797 : A.Silva ~ Modified the Available columns for PlanogramProduct.
//                    ~ No longer do available columns modify the path on their own. FormatPath can be used to format the paths.
// V8-26351 : A.Silva ~ Refactored some code to make it more standard.

#endregion
#region Version History: (CCM 8.3)
// V8-31541 : L.Ineson
//  Added new Add method.
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a list of CustomColumns
    /// </summary>
    [Serializable]
    public sealed partial class CustomColumnList : ModelList<CustomColumnList, CustomColumn>
    {
        #region Parent

        /// <summary>
        /// Returns the parent layout.
        /// </summary>
        public new CustomColumnLayout Parent
        {
            get { return base.Parent as CustomColumnLayout; }
        }

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods

        /// <summary>
        ///     Initializes and returns a new instance of <see cref="CustomColumnList"/> for the provided <paramref name="layoutType"/>.
        /// </summary>
        /// <param name="layoutType">The <see cref="CustomColumnLayoutType"/> for this instance. It will determine which columns are created.</param>
        /// <returns>A new instance of <see cref="CustomColumnList"/>.</returns>
        public static CustomColumnList NewCustomColumnList()
        {
            var columnList = new CustomColumnList();
            columnList.Create();
            return columnList;
        }

        /// <summary>
        ///     Initializes and returns a new instance of <see cref="CustomColumnList"/> for the provided <paramref name="layoutType"/>.
        /// </summary>
        /// <param name="layoutType">The <see cref="CustomColumnLayoutType"/> for this instance. It will determine which columns are created.</param>
        /// <returns>A new instance of <see cref="CustomColumnList"/>.</returns>
        public static CustomColumnList NewCustomColumnList(IEnumerable<CustomColumn> cols)
        {
            var columnList = new CustomColumnList();
            columnList.Create(cols);
            return columnList;
        }

        #endregion

        #region Data Access
      
        /// <summary>
        ///     Creates a new instance of <see cref="CustomColumnList" />.
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create(IEnumerable<CustomColumn> cols)
        {
            this.RaiseListChangedEvents = false;

            foreach (CustomColumn c in cols)
            {
                this.Add(c);
            }

            this.RaiseListChangedEvents = true;

            this.MarkAsChild();
            base.Create();
        }


        #endregion

        #region Methods

        /// <summary>
        /// Adds a new custom column to the end of this list.
        /// </summary>
        public CustomColumn Add(String path)
        {
            Int32 maxNo = (this.Any())? this.Max(c=> c.Number) :0;
            CustomColumn column = CustomColumn.NewCustomColumn(path, maxNo + 1);
            Add(column);
            return column;
        }

        #endregion
    }
}