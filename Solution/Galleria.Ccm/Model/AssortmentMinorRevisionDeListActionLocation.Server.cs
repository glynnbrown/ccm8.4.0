﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentMinorRevisionDeListActionLocation
    {
        #region Constructor
        private AssortmentMinorRevisionDeListActionLocation() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static AssortmentMinorRevisionDeListActionLocation FetchAssortmentMinorRevisionDeListActionLocation(IDalContext dalContext, AssortmentMinorRevisionDeListActionLocationDto dto)
        {
            return DataPortal.FetchChild<AssortmentMinorRevisionDeListActionLocation>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private AssortmentMinorRevisionDeListActionLocationDto GetDataTransferObject(AssortmentMinorRevisionDeListAction parent)
        {
            return new AssortmentMinorRevisionDeListActionLocationDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                LocationCode = ReadProperty<String>(LocationCodeProperty),
                LocationName = ReadProperty<String>(LocationNameProperty),
                LocationId = ReadProperty<Int16?>(LocationIdProperty),
                AssortmentMinorRevisionDeListActionId = parent.Id
            };
        }

        /// <summary>
        /// Loads this object from the given dto
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, AssortmentMinorRevisionDeListActionLocationDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<String>(LocationCodeProperty, dto.LocationCode);
            LoadProperty<String>(LocationNameProperty, dto.LocationName);
            LoadProperty<Int16?>(LocationIdProperty, dto.LocationId);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when this instance is being loaded
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, AssortmentMinorRevisionDeListActionLocationDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when this instance is being inserted into the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent product universe</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, AssortmentMinorRevisionDeListAction parent)
        {
            AssortmentMinorRevisionDeListActionLocationDto dto = GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IAssortmentMinorRevisionDeListActionLocationDal dal = dalContext.GetDal<IAssortmentMinorRevisionDeListActionLocationDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<AssortmentMinorRevisionDeListActionLocation>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when this instance is being updated in the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent product universe</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, AssortmentMinorRevisionDeListAction parent)
        {
            if (this.IsSelfDirty)
            {
                using (IAssortmentMinorRevisionDeListActionLocationDal dal = dalContext.GetDal<IAssortmentMinorRevisionDeListActionLocationDal>())
                {
                    dal.Update(GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when this instance is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IAssortmentMinorRevisionDeListActionLocationDal dal = dalContext.GetDal<IAssortmentMinorRevisionDeListActionLocationDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion
    }
}
