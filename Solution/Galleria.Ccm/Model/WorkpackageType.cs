﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-28046 : N.Foster
//  Created
#endregion
#region Version History: CCM810
// V8-29587 : A.Probyn
//  Added missing friendly names
#endregion
#region Version History: CCM810
// V8-30445 : M.Pettit
//  Removed enforced Byte datatype for reporting purposes 
#endregion
#endregion

using System.Collections.Generic;
using System;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Enum that defines a workpackage type
    /// </summary>
    public enum WorkpackageType
    {
        Unknown = 0,
        NewFromExisting = 1,
        NewFromFile = 2,
        LoadFromWorkpackage = 3,
        ModifyExisting = 4,
        StoreSpecific = 5,
        //UniqueStoreSpecific = 5
    }

    /// <summary>
    ///     WorkpackageType Helper Class
    /// </summary>
    public static class WorkpackageTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<WorkpackageType, String> FriendlyNames =
            new Dictionary<WorkpackageType, String>
            {
                {WorkpackageType.LoadFromWorkpackage, Message.Enum_WorkpackageType_LoadFromWorkpackage},
                {WorkpackageType.ModifyExisting, Message.Enum_WorkpackageType_ModifyExisting},
                {WorkpackageType.NewFromExisting, Message.Enum_WorkpackageType_NewFromExisting},
                {WorkpackageType.NewFromFile, Message.Enum_WorkpackageType_NewFromFile},
                {WorkpackageType.StoreSpecific, Message.Enum_WorkpackageType_StoreSpecific},
                {WorkpackageType.Unknown, Message.Enum_WorkpackageType_Unknown},
            };
    }
}
