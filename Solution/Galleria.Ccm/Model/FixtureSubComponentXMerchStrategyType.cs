﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//      Initial version.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    public enum FixtureSubComponentXMerchStrategyType
    {
        Manual = 0,
        Left = 1,
        LeftStacked = 2,
        Right = 3,
        RightStacked = 4,
        Even = 5,
    }

    /// <summary>
    /// PlanogramSubComponentXMerchStrategyType Helper Class
    /// </summary>
    public static class FixtureSubComponentXMerchStrategyTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<FixtureSubComponentXMerchStrategyType, String> FriendlyNames =
            new Dictionary<FixtureSubComponentXMerchStrategyType, String>()
            {
                {FixtureSubComponentXMerchStrategyType.Manual, Message.Enum_FixtureSubComponentXMerchStrategyType_Manual},
                {FixtureSubComponentXMerchStrategyType.Left, Message.Enum_FixtureSubComponentXMerchStrategyType_Left},
                {FixtureSubComponentXMerchStrategyType.LeftStacked, Message.Enum_FixtureSubComponentXMerchStrategyType_LeftStacked},
                {FixtureSubComponentXMerchStrategyType.Right, Message.Enum_FixtureSubComponentXMerchStrategyType_Right},
                {FixtureSubComponentXMerchStrategyType.RightStacked, Message.Enum_FixtureSubComponentXMerchStrategyType_RightStacked},
                {FixtureSubComponentXMerchStrategyType.Even, Message.Enum_FixtureSubComponentXMerchStrategyType_Even},
            };

        public static FixtureSubComponentXMerchStrategyType Parse(String enumName)
        {
            return EnumHelper.Parse<FixtureSubComponentXMerchStrategyType>(enumName, FixtureSubComponentXMerchStrategyType.LeftStacked);
        }
    }
}
