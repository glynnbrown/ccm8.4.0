﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//		Created (Auto-generated)
// V8-25556 : D.Pleasance
//      Added FetchProductHierarchyByEntityIdContextCriteria, FetchAllGroups
// V8-27997 : A.Probyn
//      Updated delete authorization to be valid for Administrator
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{

    /// <summary>
    /// Model representing a product hierarchy structure header
    /// (Root object)
    /// </summary>
    [Serializable]
    public sealed partial class ProductHierarchy : ModelObject<ProductHierarchy>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The row version timestamp
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region EntityId

        /// <summary>
        /// EntityId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);

        /// <summary>
        /// Gets/Sets the EntityId value
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
            set { SetProperty<Int32>(EntityIdProperty, value); }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region DateCreated

        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        /// Gets/Sets the DateCreated value
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
            set { SetProperty<DateTime>(DateCreatedProperty, value); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        /// Gets/Sets the DateLastModified value
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
            set { SetProperty<DateTime>(DateLastModifiedProperty, value); }
        }

        #endregion

        #region DateDeleted

        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);

        /// <summary>
        /// Gets/Sets the DateDeleted value
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
            set { SetProperty<DateTime?>(DateDeletedProperty, value); }
        }

        #endregion

        #region Root Level

        public static readonly ModelPropertyInfo<ProductLevel> RootLevelProperty =
            RegisterModelProperty<ProductLevel>(c => c.RootLevel);
        /// <summary>
        /// The root level of the hierarchy
        /// </summary>
        public ProductLevel RootLevel
        {
            get { return GetProperty<ProductLevel>(RootLevelProperty); }
        }

        #endregion

        #region Root Group

        public static readonly ModelPropertyInfo<ProductGroup> RootGroupProperty =
            RegisterModelProperty<ProductGroup>(c => c.RootGroup);
        /// <summary>
        /// The root level of the hierarchy
        /// </summary>
        public ProductGroup RootGroup
        {
            get { return GetProperty<ProductGroup>(RootGroupProperty); }
        }

        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(EntityIdProperty));
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));

        }

        #endregion

        #region Authorization Rules

        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ProductHierarchy), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(ProductHierarchy), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ProductHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(ProductHierarchy), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ProductHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(ProductHierarchy), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Administrator.ToString()));
        }

        #endregion

        #region Criteria

        #region FetchByIdCriteria

        /// <summary>
        /// Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> IdProperty =
                RegisterProperty<Int32>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Int32 Id
            {
                get { return this.ReadProperty<Int32>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(Int32 id)
            {
                this.LoadProperty<Int32>(IdProperty, id);
            }
            #endregion
        }

        #endregion

        #region FetchByEntityIdCriteria

        /// <summary>
        /// Criteria for the FetchByEntityId factory method
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : CriteriaBase<FetchByEntityIdCriteria>
        {
            #region Properties

            #region EntityId
            /// <summary>
            /// EntityId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Int32 EntityId
            {
                get { return this.ReadProperty<Int32>(EntityIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByEntityIdCriteria(Int32 id)
            {
                this.LoadProperty<Int32>(EntityIdProperty, id);
            }
            #endregion
        }

        #endregion

        #region FetchProductHierarchyByEntityIdContextCriteria

        /// <summary>
        /// Criteria for FetchProductHierarchyByEntityIdContext
        /// </summary>
        [Serializable]
        public class FetchProductHierarchyByEntityIdContextCriteria :
            Csla.CriteriaBase<FetchProductHierarchyByEntityIdContextCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
            RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static readonly PropertyInfo<IDalContext> ContextProperty =
            RegisterProperty<IDalContext>(c => c.Context);
            public IDalContext Context
            {
                get { return ReadProperty<IDalContext>(ContextProperty); }
            }

            public FetchProductHierarchyByEntityIdContextCriteria(Int32 entityId, IDalContext context)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<IDalContext>(ContextProperty, context);
            }
        }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new ProductHierarchy
        /// </summary>
        public static ProductHierarchy NewProductHierarchy(Int32 entityId)
        {
            ProductHierarchy item = new ProductHierarchy();
            item.Create(entityId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 entityId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);

            //add in a new root level
            this.LoadProperty<ProductLevel>(RootLevelProperty, ProductLevel.NewProductLevel());
            this.RootLevel.Name = Message.ProductHierarchy_RootLevel_DefaultName;

            //add in a new root group
            this.LoadProperty<ProductGroup>(RootGroupProperty, ProductGroup.NewProductGroup(this.RootLevel.Id));
            this.RootGroup.Code = "0";
            this.RootGroup.Name = Message.ProductHierarchy_RootGroup_DefaultName;

            //Set entity id
            this.LoadProperty<Int32>(EntityIdProperty, entityId);

            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// ToString override
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<ProductHierarchy>(oldId, newId);
        }

        /// <summary>
        /// Enumerates through all levels of this hierarchy in order
        /// from root to bottom.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProductLevel> EnumerateAllLevels()
        {
            //move down through the levels adding each one
            ProductLevel currentLevel = this.RootLevel;
            while (currentLevel != null)
            {
                yield return currentLevel;

                currentLevel = currentLevel.ChildLevel;
            }
        }

        /// <summary>
        /// Enumerates through all groups in the structure 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProductGroup> EnumerateAllGroups()
        {
            foreach(ProductGroup g in this.RootGroup.EnumerateAllChildGroups())
            {
                yield return g;
            }
        }

        /// <summary>
        /// Returns the next available default level name
        /// </summary>
        /// <returns></returns>
        public String GetNextAvailableLevelName(ProductLevel parentLevel)
        {
            String name = String.Empty;

            List<ProductLevel> existingLevels = this.EnumerateAllLevels().ToList();

            IEnumerable<String> takenNames = existingLevels.Select(l => l.Name);
            Int32 newChildLevelNo = existingLevels.IndexOf(parentLevel) + 2;
            do
            {
                name = String.Format(Message.LocationLevel_DefaultName, newChildLevelNo);
                newChildLevelNo++;
            }
            while (takenNames.Contains(name));

            return name;
        }

        /// <summary>
        /// Inserts a new child under the parent and moves its children down
        /// </summary>
        /// <param name="parentLevel"></param>
        /// <returns>the new child</returns>
        public ProductLevel InsertNewChildLevel(ProductLevel parentLevel)
        {
            //create the new child
            ProductLevel newChildLevel = ProductLevel.NewProductLevel();

            //give it a unqiue name
            if (parentLevel.ParentHierarchy != null)
            {
                newChildLevel.Name = parentLevel.ParentHierarchy.GetNextAvailableLevelName(parentLevel);
            }

            //get the parents current child
            ProductLevel currentParentChild = parentLevel.ChildLevel;
            if (currentParentChild != null)
            {
                //need to move the child to be a child of the new level instead
                newChildLevel.ChildLevel = currentParentChild;
                //remove from its old parent
                parentLevel.ChildLevel = null;

                //now set the new child and the child of the parent
                parentLevel.ChildLevel = newChildLevel;


                //need to insert an extra unit underneath all units on the parent level
                Int32 parentLevelId = parentLevel.Id;
                IEnumerable<ProductGroup> parentLevelUnits =
                    this.EnumerateAllGroups().Where(u => u.ProductLevelId == parentLevelId);

                //cycle through all the parents
                Int32 addedGroupNumber = 1;
                foreach (ProductGroup unit in parentLevelUnits)
                {
                    //get a list of child units belonging to the parent
                    List<ProductGroup> parentChildUnits = unit.ChildList.ToList();

                    if (parentChildUnits.Count > 0)
                    {
                        //create a new child unit on the new level
                        ProductGroup newChildUnit = ProductGroup.NewProductGroup(newChildLevel.Id);
                        newChildUnit.Code = GetNextAvailableDefaultGroupCode();
                        newChildUnit.Name = String.Format(Message.LocationGroup_DefaultName, newChildLevel.Name, addedGroupNumber);

                        //add the children to this new unit
                        newChildUnit.ChildList.AddList(parentChildUnits);

                        //clear the parent child collection and add the new child unit
                        unit.ChildList.Clear();
                        unit.ChildList.Add(newChildUnit);
                    }

                    addedGroupNumber++;
                }
            }
            else
            {
                parentLevel.ChildLevel = newChildLevel;
            }

            return newChildLevel;
        }

        /// <summary>
        /// Method to constrcut a default hierarchy
        /// </summary>
        public void ConstructDefaultHierarchy()
        {
            ProductLevel rootLevel = this.RootLevel;
            rootLevel.ShapeNo = 0;
            rootLevel.Colour = 1027916543;

            ProductLevel firstLevel = this.InsertNewChildLevel(this.RootLevel);
            firstLevel.Name = Message.ProductHierarchy_DepartmentLevel_DefaultName;
            firstLevel.ShapeNo = 10;
            firstLevel.Colour = 1270125823;

            ProductLevel secondLevel = this.InsertNewChildLevel(firstLevel);
            secondLevel.Name = Message.ProductHierarchy_CategoryLevel_DefaultName;
            secondLevel.ShapeNo = 7;
            secondLevel.Colour = -1587642625;
        }

        /// <summary>
        /// Returns the next available code integer 
        /// that may be used by a group in this hierarchy
        /// </summary>
        /// <returns></returns>
        public String GetNextAvailableDefaultGroupCode()
        {
            List<String> takenCodes = EnumerateAllGroups().Select(g => g.Code).ToList();
            Int32 codeInt = 1;

            while (takenCodes.Contains(codeInt.ToString()))
            {
                codeInt++;
            }

            return codeInt.ToString();
        }

        #region Action Specific Helpers

        /// <summary>
        /// Returns an empty string if the given group may have a child group added
        /// otherwise returns the appropriate error.
        /// </summary>
        /// <param name="parentGroup"></param>
        /// <returns></returns>
        public static String IsAddGroupAllowed(ProductGroup parentGroup)
        {
            //must have a parent unit selected
            if (parentGroup == null)
            {
                return Message.Hierarchy_Error_PleaseSelectValidGroup;
            }

            ProductHierarchy parentHierarchy = parentGroup.ParentHierarchy;

            //the level on which the parent resides must not be the bottom one.
            if (parentHierarchy != null
                && parentHierarchy.EnumerateAllLevels().Last().Id == parentGroup.ProductLevelId)
            {
                return Message.Hierarchy_Error_AddGroupNoAvailableLevel;
            }


            return String.Empty;
        }

        /// <summary>
        /// Returns an empty string if the move is valid
        /// Otherwise a friendly display error is given
        /// </summary>
        /// <param name="groupToMove"></param>
        /// <param name="newParentGroup"></param>
        /// <returns></returns>
        public static String IsMoveGroupAllowed(ProductGroup groupToMove, ProductGroup newParent)
        {
            //must have a group selected
            if (groupToMove == null)
            {
                return Message.Hierarchy_Error_PleaseSelectValidGroup;
            }

            //must not be the root node
            if (groupToMove.IsRoot)
            {
                return Message.Hierarchy_Error_NotRootGroup;
            }

            //param must be valid
            if (newParent == null)
            {
                return Message.Hierarchy_Error_PleaseSelectValidGroup;
            }

            //selected node and move to node must not be the same
            if (newParent == groupToMove)
            {
                return Message.Hierarchy_Error_ActionInvalid;
            }


            //selected node is already be a child of the node to move to -
            // will not return an error for this as whilst pointless it is valid.


            //the new parent associated level must not be the lowest level
            //i.e we must have a level available to move to.
            if (newParent.GetAssociatedLevel().ChildLevel == null)
            {
                return Message.Hierarchy_Error_ActionInvalid;
            }


            return String.Empty;
        }

        /// <summary>
        /// Returns an empty string if the given group may be removed from the hierarchy
        /// otherwise returns the appropriate error.
        /// </summary>
        /// <param name="parentGroup"></param>
        /// <returns></returns>
        public static String IsRemoveGroupAllowed(ProductGroup groupToRemove)
        {
            //must have a unit selected
            if (groupToRemove == null)
            {
                return Message.Hierarchy_Error_PleaseSelectValidGroup;
            }

            //must not have the root selected
            if (groupToRemove.IsRoot)
            {
                return Message.Hierarchy_Error_NotRootGroup;
            }


            return String.Empty;
        }

        /// <summary>
        /// Returns an empty string if the given level may have a new child added
        /// otherwise returns the appropriate error.
        /// </summary>
        /// <param name="parentGroup"></param>
        /// <returns></returns>
        public static String IsAddLevelAllowed(ProductLevel parentLevel)
        {
            //must have a level selected
            if (parentLevel == null)
            {
                return Message.Hierarchy_Error_LowestLevelOnly;
            }

            // Must be the lowest level
            if (parentLevel.ChildLevel != null)
            {
                return Message.Hierarchy_Error_LowestLevelOnly;
            }


            return String.Empty;
        }

        /// <summary>
        /// Returns an empty string if the given level may be removed from the hierarchy
        /// otherwise returns the appropriate error.
        /// </summary>
        /// <param name="parentGroup"></param>
        /// <returns></returns>
        public static String IsRemoveLevelAllowed(ProductLevel levelToRemove)
        {
            //must have a level selected
            if (levelToRemove == null)
            {
                return Message.Hierarchy_Error_LowestLevelOnly;
            }

            //must not have the root selected
            if (levelToRemove.ParentLevel == null)
            {
                return Message.Hierarchy_Error_LowestLevelOnly;
            }

            // must be the lowest level
            if (levelToRemove.ChildLevel != null)
            {
                return Message.Hierarchy_Error_LowestLevelOnly;
            }


            return String.Empty;
        }

        /// <summary>
        /// Safetly assigns the given child group to the proposed parent if legal.
        /// Carries out any required product re-assignment actions.
        /// </summary>
        /// <param name="childGroup">the group to set as the child</param>
        /// <param name="parentToAssign">the new parent to assign the child group to.</param>
        /// <returns>The product group instance belonging to the given parent 
        /// - this may not be the child group instance if a move action was performed</returns>
        public static ProductGroup AssignGroupParent(ProductGroup childGroup, ProductGroup parentToAssign)
        {
            ProductGroup addedReturnGroup = childGroup;

            //new group to be added
            if (childGroup.ParentGroup == null)
            {
                if (String.IsNullOrEmpty(IsAddGroupAllowed(parentToAssign)))
                {
                    //add the group to the parent's list
                    parentToAssign.ChildList.Add(childGroup);
                }
            }

            //group is to be moved
            else if (childGroup.ParentGroup != parentToAssign)
            {
                if (String.IsNullOrEmpty(IsMoveGroupAllowed(childGroup, parentToAssign)))
                {
                    ProductGroup origParent = childGroup.ParentGroup;
                    Int32 groupId = childGroup.Id;

                    //move the group
                    parentToAssign.ChildList.Add(childGroup);
                    origParent.ChildList.Remove(childGroup);

                    //Do not need to reassign products as the move would not have been allowed
                    // if the parent had any.

                    //refind the group in the parent collection as it will be a different instance to the one passed in.
                    addedReturnGroup = parentToAssign.ChildList.FirstOrDefault(c => c.Id == groupId);
                }
            }


            //update the level id of the return group
            if (addedReturnGroup != null)
            {
                ProductLevel newLevel = parentToAssign.GetAssociatedLevel();
                if (newLevel != null) { newLevel = newLevel.ChildLevel; }

                if (newLevel != null
                    && addedReturnGroup.ProductLevelId != newLevel.Id)
                {
                    addedReturnGroup.ProductLevelId = newLevel.Id;
                }
            }


            return addedReturnGroup;
        }

        /// <summary>
        /// Adds the given level to the structure under the given parent
        /// </summary>
        /// <param name="levelToAdd"></param>
        /// <param name="parentLevel"></param>
        public void AddLevel(ProductLevel levelToAdd, ProductLevel parentLevel)
        {
            //get the parents current child
            ProductLevel currentParentChild = parentLevel.ChildLevel;
            if (currentParentChild != null)
            {
                //need to move the child to be a child of the new level instead
                levelToAdd.ChildLevel = currentParentChild;
                //remove from its old parent
                parentLevel.ChildLevel = null;

                //now set the new child and the child of the parent
                parentLevel.ChildLevel = levelToAdd;


                //need to insert an extra unit underneath all units on the parent level
                Int32 parentLevelId = parentLevel.Id;
                IEnumerable<ProductGroup> parentLevelUnits =
                    this.EnumerateAllGroups().Where(u => u.ProductLevelId == parentLevelId);

                //cycle through all the parents
                Int32 addedGroupNumber = 1;
                foreach (ProductGroup unit in parentLevelUnits)
                {
                    //get a list of child units belonging to the parent
                    List<ProductGroup> parentChildUnits = unit.ChildList.ToList();

                    if (parentChildUnits.Count > 0)
                    {
                        //create a new child unit on the new level
                        ProductGroup newChildUnit = ProductGroup.NewProductGroup(levelToAdd.Id);
                        newChildUnit.Code = GetNextAvailableDefaultGroupCode();
                        newChildUnit.Name = String.Format(Message.LocationGroup_DefaultName, levelToAdd.Name, addedGroupNumber);

                        //add the children to this new unit
                        newChildUnit.ChildList.AddList(parentChildUnits);

                        //clear the parent child collection and add the new child unit
                        unit.ChildList.Clear();
                        unit.ChildList.Add(newChildUnit);
                    }

                    addedGroupNumber++;
                }
            }
            else
            {
                parentLevel.ChildLevel = levelToAdd;
            }
        }

        /// <summary>
        /// Removes the level from the structure and moves all its children up
        /// </summary>
        /// <param name="levelToRemove"></param>
        public void RemoveLevel(ProductLevel levelToRemove)
        {
            ProductLevel levelParent = levelToRemove.ParentLevel;
            ProductLevel levelChild = levelToRemove.ChildLevel;

            //add the level child to the parent
            levelParent.ChildLevel = levelChild;

            //now null it off from the level that is being removed
            levelToRemove.ChildLevel = null;


            //cycle through the parent units deleting all direct child units
            //as they will be on the removed level and shifting all their children up
            Int32 parentLevelRef = levelParent.Id;
            IEnumerable<ProductGroup> parentLevelUnits =
                this.EnumerateAllGroups().Where(u => u.ProductLevelId == parentLevelRef);

            foreach (ProductGroup parentUnit in parentLevelUnits)
            {
                List<ProductGroup> childUnitsToRemove = parentUnit.ChildList.ToList();
                foreach (ProductGroup childToRemove in childUnitsToRemove)
                {
                    //move any child groups to the parent
                    if (childToRemove.ChildList.Count > 0)
                    {
                        List<ProductGroup> removeUnitChildren = childToRemove.ChildList.ToList();
                        //add to the list of the parent
                        parentUnit.ChildList.AddList(removeUnitChildren);
                        //remove from the original unit that is being deleted
                        childToRemove.ChildList.Clear();
                    }

                    //remove the child from its parent
                    parentUnit.ChildList.Remove(childToRemove);
                }
            }

        }

        #endregion

        #endregion

        #region Helpers
        
        /// <summary>
        /// Returns a list of all groups in the structure 
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<ProductGroup> FetchAllGroups()
        {
            return this.RootGroup.FetchAllChildGroups().ToList().AsReadOnly();
        }

        #endregion
    }
}