﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Initial Version
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Sub Component Notch Style Type Enum
    /// </summary>
    public enum FixtureSubComponentNotchStyleType
    {
        SingleSquare = 0,
        SingleRectangle = 1,
        SingleCircle = 2,
        DoubleSquare = 3,
        DoubleRectangle = 4,
        DoubleCircle = 5,
        SingleH = 6
    }

    /// <summary>
    /// PlanogramSubComponentNotchStyleType Helper Class
    /// </summary>
    public static class FixtureSubComponentNotchStyleTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<FixtureSubComponentNotchStyleType, String> FriendlyNames =
            new Dictionary<FixtureSubComponentNotchStyleType, String>()
            {
                {FixtureSubComponentNotchStyleType.SingleSquare, Message.Enum_FixtureSubComponentNotchStyleType_SingleSquare},
                {FixtureSubComponentNotchStyleType.SingleRectangle, Message.Enum_FixtureSubComponentNotchStyleType_SingleRectangle},
                {FixtureSubComponentNotchStyleType.SingleCircle, Message.Enum_FixtureSubComponentNotchStyleType_SingleCircle},
                {FixtureSubComponentNotchStyleType.DoubleSquare, Message.Enum_FixtureSubComponentNotchStyleType_DoubleSquare},
                {FixtureSubComponentNotchStyleType.DoubleRectangle, Message.Enum_FixtureSubComponentNotchStyleType_DoubleRectangle},
                {FixtureSubComponentNotchStyleType.DoubleCircle, Message.Enum_FixtureSubComponentNotchStyleType_DoubleCircle},
                {FixtureSubComponentNotchStyleType.SingleH, Message.Enum_FixtureSubComponentNotchStyleType_SingleH},
            };

        public static FixtureSubComponentNotchStyleType Parse(String value)
        {
            return EnumHelper.Parse<FixtureSubComponentNotchStyleType>(value, FixtureSubComponentNotchStyleType.SingleSquare);
        }

    }
}
