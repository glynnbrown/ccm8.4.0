﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//	Created (Auto-generated)
// V8-27476 : L.Luong
//  Added BlockPlacementXType and BlockPlacementYType
#endregion

#region Version History : CCM 802
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information
// V8-28995 : A.Kuszyk
//	Removed BlockPlacementX/YType and added Primary/Secondary/Tertiary type.
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    public partial class BlockingGroup
    {
        #region Constructor
        private BlockingGroup() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static BlockingGroup Fetch(IDalContext dalContext, BlockingGroupDto dto)
        {
            return DataPortal.FetchChild<BlockingGroup>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, BlockingGroupDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Int32>(ColourProperty, dto.Colour);
            this.LoadProperty<PlanogramBlockingFillPatternType>(FillPatternTypeProperty, (PlanogramBlockingFillPatternType)dto.FillPatternType);
            this.LoadProperty<Boolean>(CanCompromiseSequenceProperty, dto.CanCompromiseSequence);
            this.LoadProperty<Boolean>(IsRestrictedByComponentTypeProperty, dto.IsRestrictedByComponentType);
            this.LoadProperty<Boolean>(CanOptimiseProperty, dto.CanOptimise);
            this.LoadProperty<Boolean>(CanMergeProperty, dto.CanMerge);
            this.LoadProperty<PlanogramBlockingGroupPlacementType>(
                BlockPlacementPrimaryTypeProperty, (PlanogramBlockingGroupPlacementType)dto.BlockPlacementPrimaryType);
            this.LoadProperty<PlanogramBlockingGroupPlacementType>(
                BlockPlacementSecondaryTypeProperty, (PlanogramBlockingGroupPlacementType)dto.BlockPlacementSecondaryType);
            this.LoadProperty<PlanogramBlockingGroupPlacementType>(
                BlockPlacementTertiaryTypeProperty, (PlanogramBlockingGroupPlacementType)dto.BlockPlacementTertiaryType);
            this.LoadProperty<Single>(TotalSpacePercentageProperty, dto.TotalSpacePercentage);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private BlockingGroupDto GetDataTransferObject(Blocking parent)
        {
            return new BlockingGroupDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                BlockingId = parent.Id,
                Name = ReadProperty<String>(NameProperty),
                Colour = ReadProperty<Int32>(ColourProperty),
                FillPatternType = (Byte)ReadProperty<PlanogramBlockingFillPatternType>(FillPatternTypeProperty),
                CanCompromiseSequence = ReadProperty<Boolean>(CanCompromiseSequenceProperty),
                IsRestrictedByComponentType = ReadProperty<Boolean>(IsRestrictedByComponentTypeProperty),
                CanOptimise = ReadProperty<Boolean>(CanOptimiseProperty),
                CanMerge = ReadProperty<Boolean>(CanMergeProperty),
                BlockPlacementPrimaryType = (Byte)ReadProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementPrimaryTypeProperty),
                BlockPlacementSecondaryType = (Byte)ReadProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementSecondaryTypeProperty),
                BlockPlacementTertiaryType = (Byte)ReadProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementTertiaryTypeProperty),
                TotalSpacePercentage = ReadProperty<Single>(TotalSpacePercentageProperty),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, BlockingGroupDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, Blocking parent)
        {
            BlockingGroupDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IBlockingGroupDal dal = dalContext.GetDal<IBlockingGroupDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<BlockingGroup>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, Blocking parent)
        {
            if (this.IsSelfDirty)
            {
                using (IBlockingGroupDal dal = dalContext.GetDal<IBlockingGroupDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, Blocking parent)
        {
            using (IBlockingGroupDal dal = dalContext.GetDal<IBlockingGroupDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion

    }
}