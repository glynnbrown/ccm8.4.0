﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26895 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  Altered names of existing properties in PlanogramProcessingStatusDto
//  Renamed Increment to AutomationIncrement
//  Added metadata, validation increment commands
//  Added metadata and validation status properties
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramProcessingStatus
    {
        #region Constructors
        private PlanogramProcessingStatus() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the specified workpackage processing status
        /// </summary>
        public static PlanogramProcessingStatus FetchByPlanogramId(Int32 planogramId)
        {
            return DataPortal.Fetch<PlanogramProcessingStatus>(new SingleCriteria<Int32>(planogramId));
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramProcessingStatusDto dto)
        {
            this.LoadProperty<Int32>(PlanogramIdProperty, dto.PlanogramId);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);

            this.LoadProperty<ProcessingStatus>(MetaDataStatusProperty, (ProcessingStatus)dto.MetaDataStatus);
            this.LoadProperty<String>(MetaDataStatusDescriptionProperty, dto.MetaDataStatusDescription);
            this.LoadProperty<Int32>(MetaDataProgressMaxProperty, dto.MetaDataProgressMax);
            this.LoadProperty<Int32>(MetaDataProgressCurrentProperty, dto.MetaDataProgressCurrent);
            this.LoadProperty<DateTime?>(MetaDataDateStartedProperty, dto.MetaDataDateStarted);
            this.LoadProperty<DateTime?>(MetaDataDateLastUpdatedProperty, dto.MetaDataDateLastUpdated);

            this.LoadProperty<ProcessingStatus>(ValidationStatusProperty, (ProcessingStatus)dto.ValidationStatus);
            this.LoadProperty<String>(ValidationStatusDescriptionProperty, dto.ValidationStatusDescription);
            this.LoadProperty<Int32>(ValidationProgressMaxProperty, dto.ValidationProgressMax);
            this.LoadProperty<Int32>(ValidationProgressCurrentProperty, dto.ValidationProgressCurrent);
            this.LoadProperty<DateTime?>(ValidationDateStartedProperty, dto.ValidationDateStarted);
            this.LoadProperty<DateTime?>(ValidationDateLastUpdatedProperty, dto.ValidationDateLastUpdated);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private PlanogramProcessingStatusDto GetDataTransferObject()
        {
            return new PlanogramProcessingStatusDto()
            {
                PlanogramId = this.ReadProperty<Int32>(PlanogramIdProperty),
                RowVersion = this.ReadProperty<RowVersion>(RowVersionProperty),
                MetaDataStatus = (Byte)this.ReadProperty<ProcessingStatus>(MetaDataStatusProperty),
                MetaDataStatusDescription = this.ReadProperty<String>(MetaDataStatusDescriptionProperty),
                MetaDataProgressMax = this.ReadProperty<Int32>(MetaDataProgressMaxProperty),
                MetaDataProgressCurrent = this.ReadProperty<Int32>(MetaDataProgressCurrentProperty),
                MetaDataDateStarted = this.ReadProperty<DateTime?>(MetaDataDateStartedProperty),
                MetaDataDateLastUpdated = this.ReadProperty<DateTime?>(MetaDataDateLastUpdatedProperty),
                ValidationStatus = (Byte)this.ReadProperty<ProcessingStatus>(ValidationStatusProperty),
                ValidationStatusDescription = this.ReadProperty<String>(ValidationStatusDescriptionProperty),
                ValidationProgressMax = this.ReadProperty<Int32>(ValidationProgressMaxProperty),
                ValidationProgressCurrent = this.ReadProperty<Int32>(ValidationProgressCurrentProperty),
                ValidationDateStarted = this.ReadProperty<DateTime?>(ValidationDateStartedProperty),
                ValidationDateLastUpdated = this.ReadProperty <DateTime?> (ValidationDateLastUpdatedProperty)
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching a single instance by its id
        /// </summary>
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramProcessingStatusDal dal = dalContext.GetDal<IPlanogramProcessingStatusDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchByPlanogramId(criteria.Value));
                }
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this instance into the data store
        /// </summary>
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                PlanogramProcessingStatusDto dto = this.GetDataTransferObject();
                using (IPlanogramProcessingStatusDal dal = dalContext.GetDal<IPlanogramProcessingStatusDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #endregion

        #region Commands

        #region Increments

        /// <summary>
        /// Increments the MetaData progress of a planogram
        /// </summary>
        private partial class MetaDataIncrementCommand
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this command on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IPlanogramProcessingStatusDal dal = dalContext.GetDal<IPlanogramProcessingStatusDal>())
                    {
                        dal.MetaDataStatusIncrement(this.PlanogramId, this.StatusDescription, this.DateLastUpdated);
                    }
                }
            }
            #endregion

            #endregion
        }

        /// <summary>
        /// Increments the Validation progress of a planogram
        /// </summary>
        private partial class ValidationIncrementCommand
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this command on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IPlanogramProcessingStatusDal dal = dalContext.GetDal<IPlanogramProcessingStatusDal>())
                    {
                        dal.ValidationStatusIncrement(this.PlanogramId, this.StatusDescription, this.DateLastUpdated);
                    }
                }
            }
            #endregion

            #endregion
        }

        #endregion

        #endregion
    }
}
