﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    public partial class BlockingDivider
    {
        #region Constructor
        private BlockingDivider() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static BlockingDivider Fetch(IDalContext dalContext, BlockingDividerDto dto)
        {
            return DataPortal.FetchChild<BlockingDivider>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, BlockingDividerDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<PlanogramBlockingDividerType>(TypeProperty, (PlanogramBlockingDividerType)dto.Type);
            this.LoadProperty<Byte>(LevelProperty, dto.Level);
            this.LoadProperty<Single>(XProperty, dto.X);
            this.LoadProperty<Single>(YProperty, dto.Y);
            this.LoadProperty<Single>(LengthProperty, dto.Length);
            this.LoadProperty<Boolean>(IsSnappedProperty, dto.IsSnapped);

        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private BlockingDividerDto GetDataTransferObject(Blocking parent)
        {
            return new BlockingDividerDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                BlockingId = parent.Id,
                Type = (Byte)ReadProperty<PlanogramBlockingDividerType>(TypeProperty),
                Level = ReadProperty<Byte>(LevelProperty),
                X = ReadProperty<Single>(XProperty),
                Y = ReadProperty<Single>(YProperty),
                Length = ReadProperty<Single>(LengthProperty),
                IsSnapped = ReadProperty<Boolean>(IsSnappedProperty)

            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, BlockingDividerDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, Blocking parent)
        {
            BlockingDividerDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IBlockingDividerDal dal = dalContext.GetDal<IBlockingDividerDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<BlockingDivider>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, Blocking parent)
        {
            if (this.IsSelfDirty)
            {
                using (IBlockingDividerDal dal = dalContext.GetDal<IBlockingDividerDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, Blocking parent)
        {
            using (IBlockingDividerDal dal = dalContext.GetDal<IBlockingDividerDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion

    }
}