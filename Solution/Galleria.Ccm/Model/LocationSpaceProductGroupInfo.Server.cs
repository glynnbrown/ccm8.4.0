﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// V8-29026 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Diagnostics.CodeAnalysis;
using Galleria.Framework.Dal;
using Csla;

namespace Galleria.Ccm.Model
{
    public partial class LocationSpaceProductGroupInfo
    {
        #region Constructor
        private LocationSpaceProductGroupInfo() { }//force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates the item from the given dto
        /// </summary>
        /// <param name="dalContext">Current dal context</param>
        /// <param name="dto"></param>
        /// <returns>Object of this type</returns>
        internal static LocationSpaceProductGroupInfo GetLocationSpaceProductGroupInfo(IDalContext dalContext, LocationSpaceProductGroupInfoDto dto)
        {
            return DataPortal.FetchChild<LocationSpaceProductGroupInfo>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, LocationSpaceProductGroupInfoDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Int32>(ProductGroupIdProperty, dto.ProductGroupId);
            LoadProperty<Int16>(LocationIdProperty, dto.LocationId);
            LoadProperty<Single>(BayCountProperty, dto.BayCount);
            LoadProperty<Int32?>(ProductCountProperty, dto.ProductCount);
            LoadProperty<Single>(AverageBayWidthProperty, dto.AverageBayWidth);
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning an existing object 
        /// </summary>
        /// <param name="dalContext">current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, LocationSpaceProductGroupInfoDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #endregion
    }
}