﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// V8-29010 : D.Pleasance
//  Created
#endregion
#region Version History: (CCM 8.1.1)
// V8-30261 : M.Pettit
//  Renamed PlanogramNameTemplateSeparatorTypeHelper Underline to Underscore
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    public enum PlanogramNameTemplateSeparatorType
    {
        Underscore,
        Colon,
        Dash,
        Space
    }

    public static class PlanogramNameTemplateSeparatorTypeHelper
    {
        public static readonly Dictionary<PlanogramNameTemplateSeparatorType, String> FriendlyNames =
            new Dictionary<PlanogramNameTemplateSeparatorType, String>()
            {
                {PlanogramNameTemplateSeparatorType.Underscore, Message.PlanogramNameTemplateSeparatorType_Underscore },
                {PlanogramNameTemplateSeparatorType.Colon, Message.PlanogramNameTemplateSeparatorType_Colon },
                {PlanogramNameTemplateSeparatorType.Dash, Message.PlanogramNameTemplateSeparatorType_Dash },
                {PlanogramNameTemplateSeparatorType.Space, Message.PlanogramNameTemplateSeparatorType_Space },
            };

        public static readonly Dictionary<PlanogramNameTemplateSeparatorType, String> FriendlyDescriptions =
            new Dictionary<PlanogramNameTemplateSeparatorType, String>()
            {
                {PlanogramNameTemplateSeparatorType.Underscore, Message.PlanogramNameTemplateSeparatorType_Underscore_Desc },
                {PlanogramNameTemplateSeparatorType.Colon, Message.PlanogramNameTemplateSeparatorType_Colon_Desc },
                {PlanogramNameTemplateSeparatorType.Dash, Message.PlanogramNameTemplateSeparatorType_Dash_Desc},
                {PlanogramNameTemplateSeparatorType.Space, Message.PlanogramNameTemplateSeparatorType_Space_Desc},
            };

        public static readonly Dictionary<PlanogramNameTemplateSeparatorType, String> Values =
            new Dictionary<PlanogramNameTemplateSeparatorType, String>()
            {
                {PlanogramNameTemplateSeparatorType.Underscore, Message.PlanogramNameTemplateSeparatorType_Underscore_Value},
                {PlanogramNameTemplateSeparatorType.Colon, Message.PlanogramNameTemplateSeparatorType_Colon_Value },
                {PlanogramNameTemplateSeparatorType.Dash, Message.PlanogramNameTemplateSeparatorType_Dash_Value},
                {PlanogramNameTemplateSeparatorType.Space, Message.PlanogramNameTemplateSeparatorType_Space_Value},
            };
    }
}