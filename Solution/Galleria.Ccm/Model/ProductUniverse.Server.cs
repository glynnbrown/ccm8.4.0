﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630: A.Kuszyk
//  Created (copied from SA).
// V8-26520: J.Pickup
//  Added missing datelastmodfied to get and load dto methods.
#endregion
#region Version History: (CCM 8.0.3)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#region Version History: (CCM 8.1.0)
// V8-29936 : N.Haywood
//  Update will set a new ucr
#endregion
#region Version History : CCM820
// V8-30840 : A.Kuszyk
//  Added FetchByEntityIdName.
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;

using Csla;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    public partial class ProductUniverse
    {
        #region Constructor
        private ProductUniverse() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the specified product universe
        /// </summary>
        /// <param name="id">The id of the product universe</param>
        /// <returns></returns>
        public static ProductUniverse FetchById(Int32 id)
        {
            return DataPortal.Fetch<ProductUniverse>(new SingleCriteria<Int32>(id));
        }

        /// <summary>
        /// Returns the specified product universe
        /// </summary>
        /// <param name="id">The id of the product universe</param>
        /// <returns></returns>
        public static ProductUniverse FetchByEntityIdName(Int32 entityId, String name)
        {
            return DataPortal.Fetch<ProductUniverse>(new FetchByEntityIdNameCriteria(entityId, name));
        }

        /// <summary>
        /// returns the specified product universe
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        internal static ProductUniverse FetchProductUniverse(IDalContext dalContext, ProductUniverseDto dto)
        {
            return DataPortal.FetchChild<ProductUniverse>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this object from a data transfer object
        /// </summary>
        /// <param name="context">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext context, ProductUniverseDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            LoadProperty<Guid>(UniqueContentReferenceProperty, dto.UniqueContentReference);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<Int32?>(ProductGroupIdProperty, dto.ProductGroupId);
            LoadProperty<Boolean>(IsMasterProperty, dto.IsMaster);
            LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);

            LoadProperty<ProductUniverseProductList>(ProductsProperty,
                ProductUniverseProductList.FetchByProductUniverseId(context, dto.Id));
            LoadProperty<Guid?>(ParentUniqueContentReferenceProperty, dto.ParentUniqueContentReference);

        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        /// <returns>A new data transfer object</returns>
        private ProductUniverseDto GetDataTransferObject()
        {
            return new ProductUniverseDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                UniqueContentReference = ReadProperty<Guid>(UniqueContentReferenceProperty),
                Name = ReadProperty<String>(NameProperty),
                ProductGroupId = ReadProperty<Int32?>(ProductGroupIdProperty),
                IsMaster = ReadProperty<Boolean>(IsMasterProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                ParentUniqueContentReference = ReadProperty<Guid?>(ParentUniqueContentReferenceProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning FetchById
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                ProductUniverseDto dto;
                using (IProductUniverseDal dal = dalContext.GetDal<IProductUniverseDal>())
                {
                    dto = dal.FetchById(criteria.Value);
                }

                LoadDataTransferObject(dalContext, dto);
            }
        }

        /// <summary>
        /// Called when returning FetchByEntityIdName
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdNameCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                ProductUniverseDto dto;
                using (IProductUniverseDal dal = dalContext.GetDal<IProductUniverseDal>())
                {
                    dto = dal.FetchByEntityIdName(criteria.EntityId, criteria.Name);
                }

                LoadDataTransferObject(dalContext, dto);
            }
        }

        /// <summary>
        /// Called when fetching a product universe by dto
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto"></param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ProductUniverseDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this instance into the database
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                ProductUniverseDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (IProductUniverseDal dal = dalContext.GetDal<IProductUniverseDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                dalContext.RegisterId<ProductUniverse>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when this instance is being updated
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                ProductUniverseDto dto = GetDataTransferObject();
                dto.UniqueContentReference = Guid.NewGuid();
                using (IProductUniverseDal dal = dalContext.GetDal<IProductUniverseDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this instance is being deleted
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IProductUniverseDal dal = dalContext.GetDal<IProductUniverseDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }
        #endregion

        #endregion
    }
}
