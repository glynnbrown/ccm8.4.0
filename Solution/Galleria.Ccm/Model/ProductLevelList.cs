﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of ProductLevel objects.
    /// </summary>
    [Serializable]
    public sealed partial class ProductLevelList : ModelList<ProductLevelList, ProductLevel>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ProductLevelList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(ProductLevelList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ProductHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(ProductLevelList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ProductHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(ProductLevelList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ProductHierarchyEdit.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static ProductLevelList NewProductLevelList()
        {
            ProductLevelList item = new ProductLevelList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }

}