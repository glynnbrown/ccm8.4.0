﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    public enum ImagePixelFormat
    {
        pf8bit = 0,
        pf16bit,
        pf32bit,
        pf24bit
    }

    /// <summary>
    /// Represents a single image.
    /// (child of ProductImage)
    /// 
    /// This is a root MasterData object and so it has the following properties/actions:
    /// - This object has a DateDeleted property and associated field in its supporting database table
    /// - If this object is deleted, it is marked as deleted (this allows for 'undeletes') and any child objects are left unchanged
    /// </summary>
    [Serializable]
    public partial class Image : ModelObject<Image>
    {
        #region Helpers

        public Dictionary<ImagePixelFormat, PixelFormat> PixelFormatLookup = new Dictionary<ImagePixelFormat, PixelFormat>()
            {
                {ImagePixelFormat.pf8bit, PixelFormats.Indexed8},
                {ImagePixelFormat.pf16bit, PixelFormats.Bgr555},
                {ImagePixelFormat.pf24bit, PixelFormats.Rgb24},
                {ImagePixelFormat.pf32bit, PixelFormats.Pbgra32}
            };

        #endregion

        #region Locks
        private Object BitmapLock;
        private Object OriginalFileLock;
        private Object BlobLock;
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
                RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }


        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
                RegisterModelProperty<RowVersion>(c => c.RowVersion);
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }


        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
                RegisterModelProperty<Int32>(c => c.EntityId);
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> OriginalFileIdProperty =
            RegisterModelProperty<Int32>(c => c.OriginalFileId);
        public Int32 OriginalFileId
        {
            get { return GetProperty<Int32>(OriginalFileIdProperty); }
        }

        public static readonly ModelPropertyInfo<File> OriginalFileProperty =
            RegisterModelProperty<File>(c => c.OriginalFile);
        public File OriginalFile
        {
            get
            {
#if !SILVERLIGHT
                if (GetProperty<File>(OriginalFileProperty) == null)
                {
                    if (OriginalFileLock == null)
                    {
                        OriginalFileLock = new object();
                    }

                    lock (OriginalFileLock)
                    {
                        if (GetProperty<File>(OriginalFileProperty) == null)
                        {
                            LoadProperty<File>(OriginalFileProperty,
                                File.GetById(ReadProperty(OriginalFileIdProperty)));
                            OnPropertyChanged(OriginalFileProperty);
                        }
                    }

                }
                return GetProperty<File>(OriginalFileProperty);
#else
                if (ReadProperty<File>(OriginalFileProperty) == null)
                {
                    this.MarkBusy();
                    DataPortal.BeginFetch<File>((o, e) =>
                    {
                        if(e.Error != null)
                        {
                            throw e.Error;
                        }
                        LoadProperty<File>(OriginalFileProperty, e.Object);
                        OnPropertyChanged(OriginalFileProperty);
                        MarkIdle();
                    });
                }
                return GetProperty<File>(OriginalFileProperty);
#endif
            }
            set
            {
                SetProperty<File>(OriginalFileProperty, value);
                FileUpdate();
            }
        }

        public static readonly ModelPropertyInfo<Int32> BlobIdProperty =
            RegisterModelProperty<Int32>(c => c.BlobID);
        public Int32 BlobID
        {
            get { return GetProperty<Int32>(BlobIdProperty); }
        }

        public static readonly ModelPropertyInfo<Blob> ImageBlobProperty =
                RegisterModelProperty<Blob>(c => c.ImageBlob);
        public Blob ImageBlob
        {
            get
            {
#if !SILVERLIGHT
                if (GetProperty<Blob>(ImageBlobProperty) == null)
                {
                    if (BlobLock == null)
                    {
                        BlobLock = new object();
                    }

                    lock (BlobLock)
                    {
                        if (GetProperty<Blob>(ImageBlobProperty) == null)
                        {
                            LoadProperty<Blob>(ImageBlobProperty,
                                Blob.GetById(ReadProperty(BlobIdProperty)));
                            OnPropertyChanged(ImageBlobProperty);
                        }

                    }

                }
                return GetProperty<Blob>(ImageBlobProperty);
#else
                if (ReadProperty<Blob>(ImageBlobProperty) == null)
                {
                    this.MarkBusy();
                    DataPortal.BeginFetch<Blob>((o, e) =>
                    {
                        if(e.Error != null)
                        {
                            throw e.Error;
                        }
                        LoadProperty<Blob>(ImageBlobProperty, e.Object);
                        OnPropertyChanged(ImageBlobProperty);
                        MarkIdle();
                    });
                }
                return GetProperty<Blob>(ImageBlobProperty);
#endif
            }
        }

        public static readonly ModelPropertyInfo<Int64> SizeInBytesProperty =
                RegisterModelProperty<Int64>(c => c.SizeInBytes);
        public Int64 SizeInBytes
        {
            get { return GetProperty<Int64>(SizeInBytesProperty); }
        }


        public static readonly ModelPropertyInfo<Int16> HeightProperty =
                RegisterModelProperty<Int16>(c => c.Height);
        public Int16 Height
        {
            get { return GetProperty<Int16>(HeightProperty); }
        }


        public static readonly ModelPropertyInfo<Int16> WidthProperty =
                RegisterModelProperty<Int16>(c => c.Width);
        public Int16 Width
        {
            get { return GetProperty<Int16>(WidthProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> CompressionIdProperty =
                RegisterModelProperty<Int32>(c => c.CompressionId);
        public Int32 CompressionId
        {
            get { return GetProperty<Int32>(CompressionIdProperty); }
            set { SetProperty<Int32>(CompressionIdProperty, value); }
        }

        /// <summary>
        /// The date when the item was created 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
        }

        /// <summary>
        /// The date when the item was modified 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
        }

        /// <summary>
        /// The date when the item was deleted 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
        }

        #endregion

        #region Helper Properties

        [NonSerialized]
        BitmapSource _bitmapImage;

        public BitmapSource BitmapImage
        {
            get
            {
                if (_bitmapImage == null)
                {
                    _bitmapImage = GetBitmapImage();
                }
                return _bitmapImage;
            }
        }

        [NonSerialized]
        BitmapSource _previewImage;

        public BitmapSource PreviewImage
        {
            get
            {
                if (_previewImage == null)
                {
                    _previewImage = BitmapHelper.ReSize(BitmapImage, new Size(96, 96), true);

                }
                return _previewImage;
            }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(Image), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(Image), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(Image), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(Image), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Authenticated.ToString()));
        }
        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new image for the current entity with no compression
        /// </summary>
        /// <param name="filePath">full file path to an image</param>
        /// <returns></returns>
        public static Image NewImage(Int32 entityId, String filePath)
        {
            return NewImage(entityId, filePath, (Compression)null);
        }

        /// <summary>
        /// Creates a new image for the current entity with no compression
        /// </summary>
        /// <param name="file">file containing image data</param>
        /// <returns></returns>
        public static Image NewImage(File file)
        {
            return NewImage(file.EntityId, file, (Compression)null);
        }

        /// <summary>
        /// Creates a new image for the current entity
        /// </summary>
        /// <param name="file">file containing image data</param>
        /// <returns></returns>
        public static Image NewImage(File file, Compression compressionType)
        {
            return NewImage(file.EntityId, file, compressionType);
        }
        /// <summary>
        /// Creates a new image for the given entity
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="filePath">full file path to an image</param>
        /// <returns></returns>
        public static Image NewImage(Int32 entityId, String filePath, Compression compressionType)
        {
            Image item = new Image();
            item.Create(entityId, filePath, compressionType);
            return item;
        }

        /// <summary>
        /// Creates a new image for the given entity
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="file">file containing image data</param>
        /// <returns></returns>
        public static Image NewImage(Int32 entityId, File file, Compression compressionType)
        {
            Image item = new Image();
            item.Create(entityId, file, compressionType);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 entityId, String Path, Compression compressionType)
        {
            File file = File.NewFile(entityId, Path, null);
            this.Create(entityId, file, compressionType);
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 entityId, File imageFile, Compression compressionType)
        {
            BitmapSource processedImage;
            Size processedSize = Size.Empty;
            PixelFormat processedPixelFormat = PixelFormats.Pbgra32;
            Boolean processedMaintainAspectRatio = true;
            if (compressionType != null)
            {
                processedSize = new Size(compressionType.Width, compressionType.Height);
                processedPixelFormat = PixelFormatLookup[compressionType.ColourDepth];
                processedMaintainAspectRatio = compressionType.MaintainAspectRatio;
                using (MemoryStream dataStream = new MemoryStream(imageFile.Data))
                {
                    processedImage = BitmapHelper.CreateBitmap(
                        dataStream,
                        processedSize,
                        processedPixelFormat,
                        processedMaintainAspectRatio);
                    Byte[] processedBlob = BitmapHelper.CreateBlob(processedImage);
                    LoadProperty<Int64>(SizeInBytesProperty, processedBlob.Length);
                    LoadProperty<Blob>(ImageBlobProperty, Blob.NewBlob(processedBlob));
                    LoadProperty<Int32>(CompressionIdProperty, compressionType.Id);
                }
            }
            else
            {   //Create Original Image
                //LoadProperty<Int32>(BlobIdProperty, imageFile.BlobID);
                LoadProperty<Int64>(SizeInBytesProperty, imageFile.SizeInBytes);
                LoadProperty<Blob>(ImageBlobProperty, imageFile.FileBlob);
                processedImage = BitmapHelper.GetBitmapImage(imageFile.Data);
                LoadProperty<Int32>(CompressionIdProperty, Compression.OriginalImageId);
            }

            LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty<Int32>(EntityIdProperty, entityId);
            LoadProperty<File>(OriginalFileProperty, imageFile);
            LoadProperty<Int32>(OriginalFileIdProperty, imageFile.Id);
            LoadProperty<Int16>(WidthProperty, (Int16)processedImage.PixelWidth);
            LoadProperty<Int16>(HeightProperty, (Int16)processedImage.PixelHeight);
            LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);


            #region Debug code - Save Ouput to file
#if DEBUG
            //String TESTPATH = Path.Combine(OriginalFile.SourceFilePath, "CREATE");
            //Directory.CreateDirectory(TESTPATH);
            //String Compress01TestOut = Path.Combine(TESTPATH, "TEST - Compress01 - " + Path.GetFileNameWithoutExtension(OriginalFile.Name) + ".png");
            //PngBitmapEncoder png = new PngBitmapEncoder();
            //png.Frames.Add(BitmapFrame.Create(GetBitmapImage(ImageBlob.Data)));
            //using (Stream fileStream = System.IO.File.Open(Compress01TestOut, FileMode.Create))
            //{ png.Save(fileStream); } 
#endif
            #endregion

            base.Create();
        }

        #endregion

        #endregion

        #region Methods
        
        #region FileUpdate

        private void FileUpdate()
        {
            SetProperty<Int32>(OriginalFileIdProperty, OriginalFile.Id);
            ReProcessBlob();
        }

        public void ReProcessBlob()
        {
            BitmapSource processedImage;
            Size processedSize = Size.Empty;
            PixelFormat processedPixelFormat = PixelFormats.Pbgra32;
            Boolean processedMaintainAspectRatio = true;
            if (CompressionId != Compression.OriginalImageId)
            {
                Compression compressionType = CompressionList.FetchAll().FirstOrDefault(c => c.Id == CompressionId);
                processedSize = new Size(compressionType.Width, compressionType.Height);
                processedPixelFormat = PixelFormatLookup[compressionType.ColourDepth];
                processedMaintainAspectRatio = compressionType.MaintainAspectRatio;
                using (MemoryStream dataStream = new MemoryStream(OriginalFile.Data))
                {
                    processedImage = BitmapHelper.CreateBitmap(
                        dataStream,
                        processedSize,
                        processedPixelFormat,
                        processedMaintainAspectRatio);
                    Byte[] processedBlob = BitmapHelper.CreateBlob(processedImage);
                    ImageBlob.Data = processedBlob;
                    LoadProperty<Int64>(SizeInBytesProperty, processedBlob.Length);
                }
            }
            else
            {
                LoadProperty<Int64>(SizeInBytesProperty, OriginalFile.SizeInBytes);
                LoadProperty<Blob>(ImageBlobProperty, OriginalFile.FileBlob);
                processedImage = BitmapHelper.GetBitmapImage(OriginalFile.Data);
                LoadProperty<Int32>(CompressionIdProperty, Compression.OriginalImageId);
            }
            LoadProperty<Int16>(WidthProperty, (Int16)processedImage.PixelWidth);
            LoadProperty<Int16>(HeightProperty, (Int16)processedImage.PixelHeight);
            #region Debug code - Save Ouput to file
#if DEBUG
            //String TESTPATH = Path.Combine(OriginalFile.SourceFilePath, "REPROCESS");
            //Directory.CreateDirectory(TESTPATH);
            //String Compress01TestOut = Path.Combine(TESTPATH, "TEST - Compress01 - " + Path.GetFileNameWithoutExtension(OriginalFile.Name) + ".png");
            //PngBitmapEncoder png = new PngBitmapEncoder();
            //png.Frames.Add(BitmapFrame.Create(GetBitmapImage(ImageBlob.Data)));
            //using (Stream fileStream = System.IO.File.Open(Compress01TestOut, FileMode.Create))
            //{ png.Save(fileStream); } 

#endif
            #endregion
        }

        #endregion

        /// <summary>
        /// Converts ImageBlob data into a bitmap image
        /// </summary>
        /// <returns></returns>
        public BitmapImage GetBitmapImage()
        {
            return BitmapHelper.GetBitmapImage(ImageBlob.Data);
        }

        #endregion

        #region Overrides

        /// <summary>
        /// To String override
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Id.ToString();
        }

        #endregion

    }
}
