﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25886 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class WorkpackagePlanogramParameterValueList
    {
        #region Constructor
        private WorkpackagePlanogramParameterValueList() { } // force use of factory methods
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning all child parameters for a specified workpackage planogram
        /// </summary>
        private void DataPortal_Fetch(FetchByWorkpackgePlanogramParameterIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IWorkpackagePlanogramParameterValueDal dal = dalContext.GetDal<IWorkpackagePlanogramParameterValueDal>())
                {
                    IEnumerable<WorkpackagePlanogramParameterValueDto> dtoList = dal.FetchByWorkpackagePlanogramParameterId(criteria.WorkpackagePlanogramParameterId);
                    foreach (WorkpackagePlanogramParameterValueDto dto in dtoList)
                    {
                        this.Add(WorkpackagePlanogramParameterValue.GetWorkpackagePlanogramParameterValue(dalContext, dto));
                    }
                }
            }
            this.MarkAsChild();
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
