﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26895 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  Altered names of existing status properties
//  Renamed Increment to AutomationIncrement
//  Added metadata and validation status properties
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class PlanogramProcessingStatus : ModelObject<PlanogramProcessingStatus>
    {
        #region Properties

        #region PlanogramId
        /// <summary>
        /// PlanogramId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanogramIdProperty =
            RegisterModelProperty<Int32>(c => c.PlanogramId);
        /// <summary>
        /// Returns the planogram id
        /// </summary>
        public Int32 PlanogramId
        {
            get { return this.GetProperty<Int32>(PlanogramIdProperty); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// Returns the row version
        /// </summary>
        public RowVersion RowVersion
        {
            get { return this.GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region MetaDataStatus
        /// <summary>
        /// MetaDataStatus property definition
        /// </summary>
        public static readonly ModelPropertyInfo<ProcessingStatus> MetaDataStatusProperty =
            RegisterModelProperty<ProcessingStatus>(c => c.MetaDataStatus);
        /// <summary>
        /// Gets or sets the processing status of the workpackage
        /// </summary>
        public ProcessingStatus MetaDataStatus
        {
            get { return this.GetProperty<ProcessingStatus>(MetaDataStatusProperty); }
            set { this.SetProperty<ProcessingStatus>(MetaDataStatusProperty, value); }
        }
        #endregion

        #region MetaDataStatusDescription
        /// <summary>
        /// MetaData Status Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> MetaDataStatusDescriptionProperty =
            RegisterModelProperty<String>(c => c.MetaDataStatusDescription);
        /// <summary>
        /// Gets or sets the processing status description of the planogram
        /// </summary>
        public String MetaDataStatusDescription
        {
            get { return this.GetProperty<String>(MetaDataStatusDescriptionProperty); }
            set { this.SetProperty<String>(MetaDataStatusDescriptionProperty, value); }
        }
        #endregion

        #region MetaDataProgressMax
        /// <summary>
        /// MetaDataProgressMax property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> MetaDataProgressMaxProperty =
            RegisterModelProperty<Int32>(c => c.MetaDataProgressMax);
        /// <summary>
        /// Gets or sets the MetaData's max progress value
        /// </summary>
        public Int32 MetaDataProgressMax
        {
            get { return this.ReadProperty<Int32>(MetaDataProgressMaxProperty); }
            set { this.SetProperty<Int32>(MetaDataProgressMaxProperty, value); }
        }
        #endregion

        #region MetaDataProgressCurrent
        /// <summary>
        /// ProgressCurrent property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> MetaDataProgressCurrentProperty =
            RegisterModelProperty<Int32>(c => c.MetaDataProgressCurrent);
        /// <summary>
        /// Gets or sets the current MetaData progress
        /// </summary>
        public Int32 MetaDataProgressCurrent
        {
            get { return this.ReadProperty<Int32>(MetaDataProgressCurrentProperty); }
            set { this.SetProperty<Int32>(MetaDataProgressCurrentProperty, value); }
        }
        #endregion

        #region MetaDataDateStarted
        /// <summary>
        /// StartDate property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> MetaDataDateStartedProperty =
            RegisterModelProperty<DateTime?>(c => c.MetaDataDateStarted);
        /// <summary>
        /// Gets or sets the date the current MetaData process was started
        /// </summary>
        public DateTime? MetaDataDateStarted
        {
            get { return this.ReadProperty<DateTime?>(MetaDataDateStartedProperty); }
            set { this.SetProperty<DateTime?>(MetaDataDateStartedProperty, value); }
        }
        #endregion

        #region MetaDataDateLastUpdated
        /// <summary>
        /// Date Last Updated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> MetaDataDateLastUpdatedProperty =
            RegisterModelProperty<DateTime?>(c => c.MetaDataDateLastUpdated);
        /// <summary>
        /// Gets or sets the date the current MetaData process was last updated
        /// </summary>
        public DateTime? MetaDataDateLastUpdated
        {
            get { return this.ReadProperty<DateTime?>(MetaDataDateLastUpdatedProperty); }
            set { this.SetProperty<DateTime?>(MetaDataDateLastUpdatedProperty, value); }
        }
        #endregion

        #region ValidationStatus
        /// <summary>
        /// ValidationStatus property definition
        /// </summary>
        public static readonly ModelPropertyInfo<ProcessingStatus> ValidationStatusProperty =
            RegisterModelProperty<ProcessingStatus>(c => c.ValidationStatus);
        /// <summary>
        /// Gets or sets the processing status of the workpackage
        /// </summary>
        public ProcessingStatus ValidationStatus
        {
            get { return this.GetProperty<ProcessingStatus>(ValidationStatusProperty); }
            set { this.SetProperty<ProcessingStatus>(ValidationStatusProperty, value); }
        }
        #endregion

        #region ValidationStatusDescription
        /// <summary>
        /// Validation Status Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ValidationStatusDescriptionProperty =
            RegisterModelProperty<String>(c => c.ValidationStatusDescription);
        /// <summary>
        /// Gets or sets the processing status description of the planogram
        /// </summary>
        public String ValidationStatusDescription
        {
            get { return this.GetProperty<String>(ValidationStatusDescriptionProperty); }
            set { this.SetProperty<String>(ValidationStatusDescriptionProperty, value); }
        }
        #endregion

        #region ValidationProgressMax
        /// <summary>
        /// ValidationProgressMax property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ValidationProgressMaxProperty =
            RegisterModelProperty<Int32>(c => c.ValidationProgressMax);
        /// <summary>
        /// Gets or sets the Validation's max progress value
        /// </summary>
        public Int32 ValidationProgressMax
        {
            get { return this.ReadProperty<Int32>(ValidationProgressMaxProperty); }
            set { this.SetProperty<Int32>(ValidationProgressMaxProperty, value); }
        }
        #endregion

        #region ValidationProgressCurrent
        /// <summary>
        /// ProgressCurrent property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ValidationProgressCurrentProperty =
            RegisterModelProperty<Int32>(c => c.ValidationProgressCurrent);
        /// <summary>
        /// Gets or sets the current Validation progress
        /// </summary>
        public Int32 ValidationProgressCurrent
        {
            get { return this.ReadProperty<Int32>(ValidationProgressCurrentProperty); }
            set { this.SetProperty<Int32>(ValidationProgressCurrentProperty, value); }
        }
        #endregion

        #region ValidationDateStarted
        /// <summary>
        /// StartDate property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> ValidationDateStartedProperty =
            RegisterModelProperty<DateTime?>(c => c.ValidationDateStarted);
        /// <summary>
        /// Gets or sets the date the current Validation process was started
        /// </summary>
        public DateTime? ValidationDateStarted
        {
            get { return this.ReadProperty<DateTime?>(ValidationDateStartedProperty); }
            set { this.SetProperty<DateTime?>(ValidationDateStartedProperty, value); }
        }
        #endregion

        #region ValidationDateLastUpdated
        /// <summary>
        /// Date Last Updated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> ValidationDateLastUpdatedProperty =
            RegisterModelProperty<DateTime?>(c => c.ValidationDateLastUpdated);
        /// <summary>
        /// Gets or sets the date the current Validation process was last updated
        /// </summary>
        public DateTime? ValidationDateLastUpdated
        {
            get { return this.ReadProperty<DateTime?>(ValidationDateLastUpdatedProperty); }
            set { this.SetProperty<DateTime?>(ValidationDateLastUpdatedProperty, value); }
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PlanogramProcessingStatus), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(PlanogramProcessingStatus), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(PlanogramProcessingStatus), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(PlanogramProcessingStatus), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Authenticated.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Increments the MetaData progress for the specified planogram
        /// </summary>
        public static void IncrementMetaDataProgress(Int32 planogramId, String statusDescription, DateTime dateLastUpdated)
        {
            DataPortal.Execute<MetaDataIncrementCommand>(new MetaDataIncrementCommand(planogramId, statusDescription, dateLastUpdated));
        }

        /// <summary>
        /// Increments the Validation progress for the specified planogram
        /// </summary>
        public static void IncrementValidationProgress(Int32 planogramId, String statusDescription, DateTime dateLastUpdated)
        {
            DataPortal.Execute<ValidationIncrementCommand>(new ValidationIncrementCommand(planogramId, statusDescription, dateLastUpdated));
        }
        #endregion

        #region Commands

        #region MetaData Increment Command
        /// <summary>
        /// Increments the MetaData progess for a planogram
        /// </summary>
        [Serializable]
        private partial class MetaDataIncrementCommand : CommandBase<MetaDataIncrementCommand>
        {
            #region Authorization Rules
            /// <summary>
            /// Defines the authorization rules for this type
            /// </summary>
            private static void AddObjectAuthorizationRules()
            {
                BusinessRules.AddRule(typeof(MetaDataIncrementCommand), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(MetaDataIncrementCommand), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(MetaDataIncrementCommand), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
                BusinessRules.AddRule(typeof(MetaDataIncrementCommand), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
            }
            #endregion

            #region Properties

            #region PlanogramId
            /// <summary>
            /// PlanogramId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> PlanogramIdProperty =
                RegisterProperty<Int32>(c => c.PlanogramId);
            /// <summary>
            /// Returns the planogram id
            /// </summary>
            public Int32 PlanogramId
            {
                get { return this.ReadProperty<Int32>(PlanogramIdProperty); }
            }
            #endregion

            #region Status Description
            /// <summary>
            /// Status Description property definition
            /// </summary>
            public static readonly PropertyInfo<String> StatusDescriptionProperty =
                RegisterProperty<String>(c => c.StatusDescription);
            /// <summary>
            /// Returns the Status Description
            /// </summary>
            public String StatusDescription
            {
                get { return this.ReadProperty<String>(StatusDescriptionProperty); }
            }
            #endregion

            #region Date Last Updated
            /// <summary>
            /// Date Last Updated property definition
            /// </summary>
            public static readonly PropertyInfo<DateTime> DateLastUpdatedProperty =
                RegisterProperty<DateTime>(c => c.DateLastUpdated);
            /// <summary>
            /// Returns the Date Last Updated
            /// </summary>
            public DateTime DateLastUpdated
            {
                get { return this.ReadProperty<DateTime>(DateLastUpdatedProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public MetaDataIncrementCommand(Int32 planogramId, String statusDescription, DateTime dateLastUpdated)
            {
                this.LoadProperty<Int32>(PlanogramIdProperty, planogramId);
                this.LoadProperty<String>(StatusDescriptionProperty, statusDescription);
                this.LoadProperty<DateTime>(DateLastUpdatedProperty, dateLastUpdated);
            }
            #endregion
        }
        #endregion

        #region Validation Increment Command
        /// <summary>
        /// Increments the validation progess for a planogram
        /// </summary>
        [Serializable]
        private partial class ValidationIncrementCommand : CommandBase<ValidationIncrementCommand>
        {
            #region Authorization Rules
            /// <summary>
            /// Defines the authorization rules for this type
            /// </summary>
            private static void AddObjectAuthorizationRules()
            {
                BusinessRules.AddRule(typeof(ValidationIncrementCommand), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(ValidationIncrementCommand), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(ValidationIncrementCommand), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
                BusinessRules.AddRule(typeof(ValidationIncrementCommand), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
            }
            #endregion

            #region Properties

            #region PlanogramId
            /// <summary>
            /// PlanogramId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> PlanogramIdProperty =
                RegisterProperty<Int32>(c => c.PlanogramId);
            /// <summary>
            /// Returns the planogram id
            /// </summary>
            public Int32 PlanogramId
            {
                get { return this.ReadProperty<Int32>(PlanogramIdProperty); }
            }
            #endregion

            #region Status Description
            /// <summary>
            /// Status Description property definition
            /// </summary>
            public static readonly PropertyInfo<String> StatusDescriptionProperty =
                RegisterProperty<String>(c => c.StatusDescription);
            /// <summary>
            /// Returns the Status Description
            /// </summary>
            public String StatusDescription
            {
                get { return this.ReadProperty<String>(StatusDescriptionProperty); }
            }
            #endregion

            #region Date Last Updated
            /// <summary>
            /// Date Last Updated property definition
            /// </summary>
            public static readonly PropertyInfo<DateTime> DateLastUpdatedProperty =
                RegisterProperty<DateTime>(c => c.DateLastUpdated);
            /// <summary>
            /// Returns the Date Last Updated
            /// </summary>
            public DateTime DateLastUpdated
            {
                get { return this.ReadProperty<DateTime>(DateLastUpdatedProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public ValidationIncrementCommand(Int32 planogramId, String statusDescription, DateTime dateLastUpdated)
            {
                this.LoadProperty<Int32>(PlanogramIdProperty, planogramId);
                this.LoadProperty<String>(StatusDescriptionProperty, statusDescription);
                this.LoadProperty<DateTime>(DateLastUpdatedProperty, dateLastUpdated);
            }
            #endregion
        }
        #endregion

        #endregion
    }
}
