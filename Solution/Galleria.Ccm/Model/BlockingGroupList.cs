﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26891 : L.Ineson
//	Created (Auto-generated)
// V8-27957 : N.Foster
//  Implement CCM security
#endregion
#endregion

using System;
using System.Globalization;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of BlockingGroup objects.
    /// </summary>
    [Serializable]
    public sealed partial class BlockingGroupList : ModelList<BlockingGroupList, BlockingGroup>
    {
        #region Parent

        public new Blocking Parent
        {
            get { return base.Parent as Blocking; }
   
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(BlockingGroupList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.BlockingGet.ToString()));
            BusinessRules.AddRule(typeof(BlockingGroupList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.BlockingCreate.ToString()));
            BusinessRules.AddRule(typeof(BlockingGroupList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.BlockingEdit.ToString()));
            BusinessRules.AddRule(typeof(BlockingGroupList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.BlockingDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static BlockingGroupList NewBlockingGroupList()
        {
            BlockingGroupList item = new BlockingGroupList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        protected override BlockingGroup AddNewCore()
        {
            Int32 groupNumber = Items.Count +1;
            
            //get the next available group colour
            Int32 colour = (this.Parent != null)? this.Parent.GetNextBlockingGroupColour() : 0;

            //create the group
            BlockingGroup newGroup = BlockingGroup.NewBlockingGroup(
                String.Format(CultureInfo.CurrentCulture, Message.BlockingGroup_DefaultName, groupNumber),
                colour);

            //add it.
            this.Add(newGroup);

            return newGroup;
        }

        #endregion
    }
}