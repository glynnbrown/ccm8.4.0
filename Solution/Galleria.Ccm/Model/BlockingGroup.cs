﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26891 : L.Ineson
//	Created (Auto-generated)
// V8-26986 : A.Kuszyk
//  Implemented IPlanogramBlockingGroup.
// V8-27392 : L.Ineson
//  CanCompromiseSequenceProperty now defaults to false
// V8-27476 : L.Luong
//  Added BlockPlacementXType and BlockPlacementYType
// V8-27957 : N.Foster
//  Implement CCM security
#endregion

#region Version History : CCM 802
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information
// V8-28995 : A.Kuszyk
//	Removed BlockPlacementX/YType and added Primary/Secondary/Tertiary type.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// BlockingGroup Model object
    /// </summary>
    [Serializable]
    [DefaultNewMethod("NewBlockingGroup", "Name", "Colour")]
    public sealed partial class BlockingGroup : ModelObject<BlockingGroup>, IPlanogramBlockingGroup
    {
        #region Static Constructor
        static BlockingGroup()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Blocking Parent
        {
            get
            {
                BlockingGroupList parentList = base.Parent as BlockingGroupList;
                if (parentList != null)
                {
                    return parentList.Parent as Blocking;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// The unique Object id
        /// </summary>
        Object IPlanogramBlockingGroup.Id
        {
            get { return Id; }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region Colour

        /// <summary>
        /// Colour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ColourProperty =
            RegisterModelProperty<Int32>(c => c.Colour);

        /// <summary>
        /// Gets/Sets the Colour value
        /// </summary>
        public Int32 Colour
        {
            get { return GetProperty<Int32>(ColourProperty); }
            set { SetProperty<Int32>(ColourProperty, value); }
        }

        #endregion

        #region FillPatternType

        /// <summary>
        /// FillPatternType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramBlockingFillPatternType> FillPatternTypeProperty =
            RegisterModelProperty<PlanogramBlockingFillPatternType>(c => c.FillPatternType);

        /// <summary>
        /// Gets/Sets the FillPatternType value
        /// </summary>
        public PlanogramBlockingFillPatternType FillPatternType
        {
            get { return GetProperty<PlanogramBlockingFillPatternType>(FillPatternTypeProperty); }
            set { SetProperty<PlanogramBlockingFillPatternType>(FillPatternTypeProperty, value); }
        }

        #endregion

        #region CanCompromiseSequence

        /// <summary>
        /// CanCompromiseSequence property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> CanCompromiseSequenceProperty =
            RegisterModelProperty<Boolean>(c => c.CanCompromiseSequence);

        /// <summary>
        /// Gets/Sets the CanCompromiseSequence value
        /// </summary>
        public Boolean CanCompromiseSequence
        {
            get { return GetProperty<Boolean>(CanCompromiseSequenceProperty); }
            set { SetProperty<Boolean>(CanCompromiseSequenceProperty, value); }
        }

        #endregion

        #region IsRestrictedByComponentType

        /// <summary>
        /// IsRestrictedByComponentType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsRestrictedByComponentTypeProperty =
            RegisterModelProperty<Boolean>(c => c.IsRestrictedByComponentType);

        /// <summary>
        /// Gets/Sets the IsRestrictedByComponentType value
        /// </summary>
        public Boolean IsRestrictedByComponentType
        {
            get { return GetProperty<Boolean>(IsRestrictedByComponentTypeProperty); }
            set { SetProperty<Boolean>(IsRestrictedByComponentTypeProperty, value); }
        }

        #endregion

        #region CanOptimise

        /// <summary>
        /// CanOptimise property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> CanOptimiseProperty =
            RegisterModelProperty<Boolean>(c => c.CanOptimise);

        /// <summary>
        /// Gets/Sets the CanOptimise value
        /// </summary>
        public Boolean CanOptimise
        {
            get { return GetProperty<Boolean>(CanOptimiseProperty); }
            set { SetProperty<Boolean>(CanOptimiseProperty, value); }
        }

        #endregion

        #region CanMerge

        /// <summary>
        /// CanMerge property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> CanMergeProperty =
            RegisterModelProperty<Boolean>(c => c.CanMerge);

        /// <summary>
        /// Gets/Sets the CanMerge value
        /// </summary>
        public Boolean CanMerge
        {
            get { return GetProperty<Boolean>(CanMergeProperty); }
            set { SetProperty<Boolean>(CanMergeProperty, value); }
        }

        #endregion

        #region IsLimited

        /// <summary>
        /// IsLimited property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsLimitedProperty =
            RegisterModelProperty<Boolean>(c => c.IsLimited);

        /// <summary>
        /// Gets/Sets the IsLimited value
        /// </summary>
        public Boolean IsLimited
        {
            get { return GetProperty<Boolean>(IsLimitedProperty); }
            set { SetProperty<Boolean>(IsLimitedProperty, value); }
        }

        #endregion

        #region LimitedPercentage

        /// <summary>
        /// LimitedPercentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> LimitedPercentageProperty =
            RegisterModelProperty<Single>(c => c.LimitedPercentage);

        /// <summary>
        /// Gets/Sets the LimitedPercentage value
        /// </summary>
        public Single LimitedPercentage
        {
            get { return GetProperty<Single>(LimitedPercentageProperty); }
            set { SetProperty<Single>(LimitedPercentageProperty, value); }
        }

        #endregion

        #region BlockPlacementPrimaryType

        /// <summary>
        /// BlockPlacementPrimaryType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramBlockingGroupPlacementType> BlockPlacementPrimaryTypeProperty =
            RegisterModelProperty<PlanogramBlockingGroupPlacementType>(c => c.BlockPlacementPrimaryType);

        /// <summary>
        /// Gets/Sets the BlockPlacementPrimaryType value
        /// </summary>
        public PlanogramBlockingGroupPlacementType BlockPlacementPrimaryType
        {
            get { return GetProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementPrimaryTypeProperty); }
            set { SetProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementPrimaryTypeProperty, value); }
        }

        #endregion

        #region BlockPlacementSecondaryType

        /// <summary>
        /// BlockPlacementSecondaryType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramBlockingGroupPlacementType> BlockPlacementSecondaryTypeProperty =
            RegisterModelProperty<PlanogramBlockingGroupPlacementType>(c => c.BlockPlacementSecondaryType);

        /// <summary>
        /// Gets/Sets the BlockPlacementSecondaryType value
        /// </summary>
        public PlanogramBlockingGroupPlacementType BlockPlacementSecondaryType
        {
            get { return GetProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementSecondaryTypeProperty); }
            set { SetProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementSecondaryTypeProperty, value); }
        }

        #endregion

        #region BlockPlacementTertiaryType

        /// <summary>
        /// BlockPlacementTertiaryType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramBlockingGroupPlacementType> BlockPlacementTertiaryTypeProperty =
            RegisterModelProperty<PlanogramBlockingGroupPlacementType>(c => c.BlockPlacementTertiaryType);

        /// <summary>
        /// Gets/Sets the BlockPlacementTertiaryType value
        /// </summary>
        public PlanogramBlockingGroupPlacementType BlockPlacementTertiaryType
        {
            get { return GetProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementTertiaryTypeProperty); }
            set { SetProperty<PlanogramBlockingGroupPlacementType>(BlockPlacementTertiaryTypeProperty, value); }
        }

        #endregion

        #region TotalSpacePercentage

        /// <summary>
        /// TotalSpacePercentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> TotalSpacePercentageProperty =
            RegisterModelProperty<Single>(c => c.TotalSpacePercentage);

        /// <summary>
        /// Returns the total percentage of space allocated to this group.
        /// This is a percentage value between 0 and 1
        /// </summary>
        public Single TotalSpacePercentage
        {
            get { return GetProperty<Single>(TotalSpacePercentageProperty); }
            private set { SetProperty<Single?>(TotalSpacePercentageProperty, value); }
        }

        /// <summary>
        /// Updates the value of the total space percentage property.
        /// </summary>
        /// <returns></returns>
        private Single CalculateTotalSpacePercentage()
        {
            Blocking parentBlocking = this.Parent;
            if (parentBlocking == null) return 0;

            return parentBlocking.Locations
                .Where(l => Object.Equals(l.BlockingGroupId, this.Id))
                .Sum(l => l.SpacePercentage);
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));

            // Mark the block placement types as inter-dependant.
            BusinessRules.AddRule(new Dependency(
                BlockPlacementPrimaryTypeProperty, BlockPlacementSecondaryTypeProperty, BlockPlacementTertiaryTypeProperty));
            BusinessRules.AddRule(new Dependency(
                BlockPlacementSecondaryTypeProperty, BlockPlacementPrimaryTypeProperty, BlockPlacementTertiaryTypeProperty));
            BusinessRules.AddRule(new Dependency(
                BlockPlacementTertiaryTypeProperty, BlockPlacementPrimaryTypeProperty, BlockPlacementSecondaryTypeProperty));

            // Ensure there are no duplicates for each dimension.
            BusinessRules.AddRule(new PlanogramBlockingGroupPlacementTypeRule(
                BlockPlacementPrimaryTypeProperty, BlockPlacementSecondaryTypeProperty, BlockPlacementTertiaryTypeProperty));
            BusinessRules.AddRule(new PlanogramBlockingGroupPlacementTypeRule(
                BlockPlacementSecondaryTypeProperty, BlockPlacementPrimaryTypeProperty, BlockPlacementTertiaryTypeProperty));
            BusinessRules.AddRule(new PlanogramBlockingGroupPlacementTypeRule(
                BlockPlacementTertiaryTypeProperty, BlockPlacementPrimaryTypeProperty, BlockPlacementSecondaryTypeProperty));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(BlockingGroup), new IsInRole(AuthorizationActions.GetObject, DomainPermission.BlockingGet.ToString()));
            BusinessRules.AddRule(typeof(BlockingGroup), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.BlockingCreate.ToString()));
            BusinessRules.AddRule(typeof(BlockingGroup), new IsInRole(AuthorizationActions.EditObject, DomainPermission.BlockingEdit.ToString()));
            BusinessRules.AddRule(typeof(BlockingGroup), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.BlockingDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static BlockingGroup NewBlockingGroup(String name, Int32 colour)
        {
            BlockingGroup item = new BlockingGroup();
            item.Create(name, colour);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(String name, Int32 colour)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<PlanogramBlockingFillPatternType>(FillPatternTypeProperty, PlanogramBlockingFillPatternType.Solid);
            this.LoadProperty<Boolean>(CanCompromiseSequenceProperty, false);
            this.LoadProperty<Boolean>(IsRestrictedByComponentTypeProperty, true);
            this.LoadProperty<Boolean>(CanOptimiseProperty, true);
            this.LoadProperty<Boolean>(CanMergeProperty, true);
            this.LoadProperty<PlanogramBlockingGroupPlacementType>(
                BlockPlacementPrimaryTypeProperty, PlanogramBlockingGroupPlacementType.LeftToRight);
            this.LoadProperty<PlanogramBlockingGroupPlacementType>(
                BlockPlacementSecondaryTypeProperty, PlanogramBlockingGroupPlacementType.BottomToTop);
            this.LoadProperty<PlanogramBlockingGroupPlacementType>(
                BlockPlacementTertiaryTypeProperty, PlanogramBlockingGroupPlacementType.FrontToBack);
            this.LoadProperty<String>(NameProperty, name);
            this.LoadProperty<Int32>(ColourProperty, colour);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<BlockingGroup>(oldId, newId);
        }

        /// <summary>
        /// Called whenever the root object of this tree notifies that a model within the tree has changed.
        /// </summary>
        protected override void OnObjectGraphMemberChanged(object graphMember, 
            System.ComponentModel.PropertyChangedEventArgs propertyChangedArgs, 
            System.Collections.Specialized.NotifyCollectionChangedEventArgs collectionChangedArgs)
        {
            base.OnObjectGraphMemberChanged(graphMember, propertyChangedArgs, collectionChangedArgs);

            //TotalAllocatedSpace
            if (propertyChangedArgs != null && 
                (propertyChangedArgs.PropertyName == BlockingLocation.SpacePercentageProperty.Name
                || propertyChangedArgs.PropertyName == BlockingLocation.BlockingGroupIdProperty.Name))
            {
                BlockingLocation location = graphMember as BlockingLocation;
                if (location != null)
                {
                    if (location.BlockingGroupId == this.Id)
                    {
                        this.TotalSpacePercentage = CalculateTotalSpacePercentage();
                    }
                }
            }
        }

        /// <summary>
        /// Returns a list of blocking locations for this group.
        /// </summary>
        /// <returns></returns>
        public List<BlockingLocation> GetBlockingLocations()
        {
            List<BlockingLocation> returnList = new List<BlockingLocation>();
            if (this.Parent != null)
            {
                foreach (BlockingLocation loc in this.Parent.Locations)
                {
                    if (loc.BlockingGroupId == this.Id)
                    {
                        returnList.Add(loc);
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Ensures that the given locations and only the given locations
        /// are associated with this group.
        /// </summary>
        public void UpdateAssociatedLocations(IEnumerable<BlockingLocation> locations)
        {
            Blocking parentBlocking = this.Parent;
            if (parentBlocking == null) return;

            //get the list of locations currently associated with this group.
            List<BlockingLocation> currentAssociated = GetBlockingLocations();

            //first unassociate any locations that are no longer in the group
            foreach (BlockingLocation current in currentAssociated.ToList())
            {
                if (!locations.Contains(current))
                {
                    BlockingGroup newGroup = parentBlocking.Groups.AddNew();
                    current.BlockingGroupId = newGroup.Id;

                    currentAssociated.Remove(current);
                }
            }

            //next, add in any locations that are missing
            //List<BlockingLocation> remainingLocs = locations.Except(currentAssociated).ToList();

            Queue<BlockingLocation> remainingLocs = new Queue<BlockingLocation>(locations.Except(currentAssociated));
            while (remainingLocs.Count > 0
                && remainingLocs.Any(l => currentAssociated.Any(c => c.IsAdjacent(l))))
            {
                BlockingLocation nextLoc = remainingLocs.Dequeue();
                if (currentAssociated.Any(c => c.IsAdjacent(nextLoc)))
                {
                    //associate the location
                    nextLoc.BlockingGroupId = this.Id;
                }
                else
                {
                    //add back in the queue
                    remainingLocs.Enqueue(nextLoc);
                }

            }


            //clean up any unused groups
            foreach (BlockingGroup group in parentBlocking.Groups.ToList())
            {
                if (group.GetBlockingLocations().Count == 0)
                {
                    parentBlocking.Groups.Remove(group);
                }
            }

        }


        #region IPlanogramBlockingGroup Members
        void IPlanogramBlockingGroup.UpdateAssociatedLocations(IEnumerable<IPlanogramBlockingLocation> locations)
        {
            UpdateAssociatedLocations(locations.Cast<BlockingLocation>());
        }

        IEnumerable<IPlanogramBlockingLocation> IPlanogramBlockingGroup.GetBlockingLocations()
        {
            return GetBlockingLocations();
        }

        #endregion

        #endregion
    }
}