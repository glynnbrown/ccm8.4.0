﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-32524 : L.Ineson
//  Created
// V8-32636 : M.Brumby
//  Resolve Id Fix
#endregion
#endregion

using System;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using PlanogramMessage = Galleria.Framework.Planograms.Resources.Language.Message;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a Planogram Annotation object
    /// </summary>
    [Serializable]
    public sealed partial class FixtureAnnotation : ModelObject<FixtureAnnotation>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new FixturePackage Parent
        {
            get 
            {
                FixtureAnnotationList parentList = base.Parent as FixtureAnnotationList;
                if (parentList == null) return null;
                return parentList.Parent as FixturePackage;
            }
        }
        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region FixtureItemId
        /// <summary>
        /// FixtureItemId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> FixtureItemIdProperty =
            RegisterModelProperty<Int32?>(c => c.FixtureItemId);
        /// <summary>
        /// The FixturePosition FixtureItemId
        /// </summary>
        public Int32? FixtureItemId
        {
            get { return GetProperty<Int32?>(FixtureItemIdProperty); }
            set { SetProperty<Int32?>(FixtureItemIdProperty, value); }
        }
        #endregion

        #region FixtureAssemblyItemId
        /// <summary>
        /// FixtureAssemblyId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> FixtureAssemblyItemIdProperty =
            RegisterModelProperty<Int32?>(c => c.FixtureAssemblyItemId);
        /// <summary>
        /// The FixturePosition FixtureAssemblyId
        /// </summary>
        public Int32? FixtureAssemblyItemId
        {
            get { return GetProperty<Int32?>(FixtureAssemblyItemIdProperty); }
            set { SetProperty<Int32?>(FixtureAssemblyItemIdProperty, value); }
        }
        #endregion

        #region FixtureComponentItemId
        /// <summary>
        /// FixtureComponentId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> FixtureComponentItemIdProperty =
            RegisterModelProperty<Int32?>(c => c.FixtureComponentItemId);
        /// <summary>
        /// The FixturePosition FixtureComponentId
        /// </summary>
        public Int32? FixtureComponentItemId
        {
            get { return GetProperty<Int32?>(FixtureComponentItemIdProperty); }
            set { SetProperty<Int32?>(FixtureComponentItemIdProperty, value); }
        }
        #endregion

        #region FixtureAssemblyComponentId
        /// <summary>
        /// FixtureAssemblyComponentId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> FixtureAssemblyComponentIdProperty =
            RegisterModelProperty<Int32?>(c => c.FixtureAssemblyComponentId);
        /// <summary>
        /// The FixturePosition FixtureAssemblyComponentId
        /// </summary>
        public Int32? FixtureAssemblyComponentId
        {
            get { return GetProperty<Int32?>(FixtureAssemblyComponentIdProperty); }
            set { SetProperty<Int32?>(FixtureAssemblyComponentIdProperty, value); }
        }
        #endregion

        #region FixtureSubComponentId
        /// <summary>
        /// FixtureSubComponentId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> FixtureSubComponentIdProperty =
            RegisterModelProperty<Int32?>(c => c.FixtureSubComponentId);
        /// <summary>
        /// The FixturePosition FixtureSubComponentId
        /// </summary>
        public Int32? FixtureSubComponentId
        {
            get { return GetProperty<Int32?>(FixtureSubComponentIdProperty); }
            set { SetProperty<Int32?>(FixtureSubComponentIdProperty, value); }
        }
        #endregion

        #region AnnotationType
        /// <summary>
        /// AnnotationType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramAnnotationType> AnnotationTypeProperty =
            RegisterModelProperty<PlanogramAnnotationType>(c => c.AnnotationType);
        /// <summary>
        /// The type of textbox this is
        /// </summary>
        public PlanogramAnnotationType AnnotationType
        {
            get { return GetProperty<PlanogramAnnotationType>(AnnotationTypeProperty); }
            set { SetProperty<PlanogramAnnotationType>(AnnotationTypeProperty, value); }
        }
        #endregion

        #region Text
        /// <summary>
        /// Text property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> TextProperty =
            RegisterModelProperty<String>(c => c.Text, PlanogramMessage.PlanogramAnnotation_Text);
        /// <summary>
        /// Annotation Text
        /// </summary>
        public String Text
        {
            get { return GetProperty<String>(TextProperty); }
            set { SetProperty<String>(TextProperty, value); }
        }
        #endregion

        #region X
        /// <summary>
        /// X property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> XProperty =
            RegisterModelProperty<Single?>(c => c.X, PlanogramMessage.Generic_X, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Annotation X
        /// </summary>
        public Single? X
        {
            get { return GetProperty<Single?>(XProperty); }
            set { SetProperty<Single?>(XProperty, value); }
        }
        #endregion

        #region Y
        /// <summary>
        /// Y property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> YProperty =
            RegisterModelProperty<Single?>(c => c.Y, PlanogramMessage.Generic_Y, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Annotation Y
        /// </summary>
        public Single? Y
        {
            get { return GetProperty<Single?>(YProperty); }
            set { SetProperty<Single?>(YProperty, value); }
        }
        #endregion

        #region Z
        /// <summary>
        /// Z property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> ZProperty =
            RegisterModelProperty<Single?>(c => c.Z, PlanogramMessage.Generic_Z, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Annotation Z
        /// </summary>
        public Single? Z
        {
            get { return GetProperty<Single?>(ZProperty); }
            set { SetProperty<Single?>(ZProperty, value); }
        }
        #endregion

        #region Height
        /// <summary>
        /// Height property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HeightProperty =
            RegisterModelProperty<Single>(c => c.Height, PlanogramMessage.Generic_Height, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Annotation Height
        /// </summary>
        public Single Height
        {
            get { return GetProperty<Single>(HeightProperty); }
            set { SetProperty<Single>(HeightProperty, value); }
        }
        #endregion

        #region Width
        /// <summary>
        /// Width property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> WidthProperty =
            RegisterModelProperty<Single>(c => c.Width, PlanogramMessage.Generic_Width, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Annotation Width
        /// </summary>
        public Single Width
        {
            get { return GetProperty<Single>(WidthProperty); }
            set { SetProperty<Single>(WidthProperty, value); }
        }
        #endregion

        #region Depth
        /// <summary>
        /// Depth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DepthProperty =
            RegisterModelProperty<Single>(c => c.Depth, PlanogramMessage.Generic_Depth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Annotation Depth
        /// </summary>
        public Single Depth
        {
            get { return GetProperty<Single>(DepthProperty); }
            set { SetProperty<Single>(DepthProperty, value); }
        }
        #endregion

        #region BorderColour
        /// <summary>
        /// BorderColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> BorderColourProperty =
            RegisterModelProperty<Int32>(c => c.BorderColour, PlanogramMessage.PlanogramAnnotation_BorderColour);
        /// <summary>
        /// The colour of the annotation border
        /// </summary>
        public Int32 BorderColour
        {
            get { return GetProperty<Int32>(BorderColourProperty); }
            set { SetProperty<Int32>(BorderColourProperty, value); }
        }
        #endregion

        #region BorderThickness
        /// <summary>
        /// BorderThickness property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BorderThicknessProperty =
            RegisterModelProperty<Single>(c => c.BorderThickness, PlanogramMessage.PlanogramAnnotation_BorderThickness);
        /// <summary>
        /// The thickness of the annotation border
        /// </summary>
        public Single BorderThickness
        {
            get { return GetProperty<Single>(BorderThicknessProperty); }
            set { SetProperty<Single>(BorderThicknessProperty, value); }
        }
        #endregion

        #region BackgroundColour
        /// <summary>
        /// BackgroundColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> BackgroundColourProperty =
            RegisterModelProperty<Int32>(c => c.BackgroundColour, PlanogramMessage.PlanogramAnnotation_BackgroundColour);
        /// <summary>
        /// The colour of the annotation background
        /// </summary>
        public Int32 BackgroundColour
        {
            get { return GetProperty<Int32>(BackgroundColourProperty); }
            set { SetProperty<Int32>(BackgroundColourProperty, value); }
        }
        #endregion

        #region FontColour
        /// <summary>
        /// FontColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FontColourProperty =
            RegisterModelProperty<Int32>(c => c.FontColour, PlanogramMessage.PlanogramAnnotation_FontColour);
        /// <summary>
        /// The annotation text foreground colour
        /// </summary>
        public Int32 FontColour
        {
            get { return GetProperty<Int32>(FontColourProperty); }
            set { SetProperty<Int32>(FontColourProperty, value); }
        }
        #endregion

        #region FontSize
        /// <summary>
        /// FontSize property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FontSizeProperty =
            RegisterModelProperty<Single>(c => c.FontSize, PlanogramMessage.PlanogramAnnotation_FontSize);
        /// <summary>
        /// The size of the annotation text
        /// </summary>
        public Single FontSize
        {
            get { return GetProperty<Single>(FontSizeProperty); }
            set { SetProperty<Single>(FontSizeProperty, value); }
        }
        #endregion

        #region FontName
        /// <summary>
        /// FontName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> FontNameProperty =
            RegisterModelProperty<String>(c => c.FontName, PlanogramMessage.PlanogramAnnotation_FontName);
        /// <summary>
        /// The name of the font family to render text as
        /// </summary>
        public String FontName
        {
            get { return GetProperty<String>(FontNameProperty); }
            set { SetProperty<String>(FontNameProperty, value); }
        }
        #endregion

        #region CanReduceFontToFit
        /// <summary>
        /// CanReduceFontToFit property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> CanReduceFontToFitProperty =
            RegisterModelProperty<Boolean>(c => c.CanReduceFontToFit, PlanogramMessage.PlanogramAnnotation_CanReduceFontToFit);
        /// <summary>
        /// Indicates if the font size can be reduced if too large
        /// </summary>
        public Boolean CanReduceFontToFit
        {
            get { return GetProperty<Boolean>(CanReduceFontToFitProperty); }
            set { SetProperty<Boolean>(CanReduceFontToFitProperty, value); }
        }
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new MaxLength(TextProperty, 255));
            BusinessRules.AddRule(new MaxLength(FontNameProperty, 100));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            // NF - TODO
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static FixtureAnnotation NewFixtureAnnotation()
        {
            FixtureAnnotation item = new FixtureAnnotation();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static FixtureAnnotation NewFixtureAnnotation(FixtureItem fixtureItem)
        {
            FixtureAnnotation item = new FixtureAnnotation();
            item.Create(fixtureItem);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static FixtureAnnotation NewFixtureAnnotation(FixtureComponentItem fixtureComponent, FixtureItem fixtureItem)
        {
            FixtureAnnotation item = new FixtureAnnotation();
            item.Create(fixtureComponent, fixtureItem);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type from the given plan annotation
        /// </summary>
        internal static FixtureAnnotation NewFixtureAnnotation(PlanogramAnnotation planAnnotation)
        {
            FixtureAnnotation item = new FixtureAnnotation();
            item.Create(planAnnotation);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        protected override void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(FixtureComponentItem fixtureComponentItem, FixtureItem fixtureItem)
        {
            this.LoadProperty<Int32?>(FixtureComponentItemIdProperty, fixtureComponentItem.Id);
            Create(fixtureItem);
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(FixtureItem fixtureItem)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Single?>(XProperty, 0);
            this.LoadProperty<Single?>(YProperty, 0);
            this.LoadProperty<Single?>(ZProperty, 0);
            this.LoadProperty<PlanogramAnnotationType>(AnnotationTypeProperty, PlanogramAnnotationType.TextBox);
            this.LoadProperty<String>(FontNameProperty, "Arial");
            this.LoadProperty<Single>(FontSizeProperty, 12);
            this.LoadProperty<Int32>(FontColourProperty, -16777216);
            this.LoadProperty<Boolean>(CanReduceFontToFitProperty, true);
            this.LoadProperty<Int32>(BackgroundColourProperty, -1);
            this.LoadProperty<Int32>(BorderColourProperty, -16777216);
            this.LoadProperty<Single>(BorderThicknessProperty, 1);
            if (fixtureItem != null) this.LoadProperty<Int32?>(FixtureItemIdProperty, fixtureItem.Id);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(PlanogramAnnotation planAnnotation)
        {
            this.LoadProperty<Int32>(IdProperty, (Int32)planAnnotation.Id);
            this.LoadProperty<Single?>(XProperty, planAnnotation.X);
            this.LoadProperty<Single?>(YProperty, planAnnotation.Y);
            this.LoadProperty<Single?>(ZProperty, planAnnotation.Z);
            this.LoadProperty<Single>(HeightProperty, planAnnotation.Height);
            this.LoadProperty<Single>(WidthProperty, planAnnotation.Width);
            this.LoadProperty<Single>(DepthProperty, planAnnotation.Depth);
            this.LoadProperty<String>(TextProperty, planAnnotation.Text);
            this.LoadProperty<PlanogramAnnotationType>(AnnotationTypeProperty, planAnnotation.AnnotationType);
            this.LoadProperty<String>(FontNameProperty, planAnnotation.FontName);
            this.LoadProperty<Single>(FontSizeProperty, planAnnotation.FontSize);
            this.LoadProperty<Int32>(FontColourProperty, planAnnotation.FontColour);
            this.LoadProperty<Boolean>(CanReduceFontToFitProperty, planAnnotation.CanReduceFontToFit);
            this.LoadProperty<Int32>(BackgroundColourProperty, planAnnotation.BackgroundColour);
            this.LoadProperty<Int32>(BorderColourProperty, planAnnotation.BorderColour);
            this.LoadProperty<Single>(BorderThicknessProperty, planAnnotation.BorderThickness);

            this.LoadProperty<Int32?>(FixtureItemIdProperty, (Int32?)planAnnotation.PlanogramFixtureItemId);
            this.LoadProperty<Int32?>(FixtureAssemblyItemIdProperty, (Int32?)planAnnotation.PlanogramFixtureAssemblyId);
            this.LoadProperty<Int32?>(FixtureAssemblyComponentIdProperty, (Int32?)planAnnotation.PlanogramAssemblyComponentId);
            this.LoadProperty<Int32?>(FixtureComponentItemIdProperty, (Int32?)planAnnotation.PlanogramFixtureComponentId);
            this.LoadProperty<Int32?>(FixtureSubComponentIdProperty, (Int32?)planAnnotation.PlanogramSubComponentId);

            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<FixtureAnnotation>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }

        /// <summary>
        /// Called when ids need resolving
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // FixtureItemId
            Int32? fixtureItemId = (Int32?)context.ResolveId<FixtureItem>(this.ReadProperty<Int32?>(FixtureItemIdProperty));
            if (fixtureItemId != null) this.LoadProperty<Int32?>(FixtureItemIdProperty, fixtureItemId);

            // FixtureAssemblyId
            Int32? planogramFixtureAssemblyId = (Int32?)context.ResolveId<FixtureAssembly>(this.ReadProperty<Int32?>(FixtureAssemblyItemIdProperty));
            if (planogramFixtureAssemblyId != null) this.LoadProperty<Int32?>(FixtureAssemblyItemIdProperty, planogramFixtureAssemblyId);

            // FixtureComponentId
            Int32? fixtureComponentId = (Int32?)context.ResolveId<FixtureComponentItem>(this.ReadProperty<Int32?>(FixtureComponentItemIdProperty));
            if (fixtureComponentId != null) this.LoadProperty<Int32?>(FixtureComponentItemIdProperty, fixtureComponentId);

            // FixtureAssemblyComponentId
            Int32? fixtureAssemblyComponentId = (Int32?)context.ResolveId<FixtureAssemblyComponent>(this.ReadProperty<Int32?>(FixtureAssemblyComponentIdProperty));
            if (fixtureAssemblyComponentId != null) this.LoadProperty<Int32?>(FixtureAssemblyComponentIdProperty, fixtureAssemblyComponentId);

            // FixtureSubComponentId
            Int32? fixtureSubComponentId = (Int32?)context.ResolveId<FixtureSubComponent>(this.ReadProperty<Int32?>(FixtureSubComponentIdProperty));
            if (fixtureSubComponentId != null) this.LoadProperty<Int32?>(FixtureSubComponentIdProperty, fixtureSubComponentId);

        }

        /// <summary>
        /// Creates a new planogram annoation from this.
        /// </summary>
        public PlanogramAnnotation NewPlanogramAnnotation()
        {
            PlanogramAnnotation anno = PlanogramAnnotation.NewPlanogramAnnotation();
            anno.Id = this.Id;
           
            anno.X = this.X;
            anno.Y = this.Y;
            anno.Z = this.Z;
            anno.AnnotationType = this.AnnotationType;
            anno.FontName = this.FontName;
            anno.FontSize = this.FontSize;
            anno.FontColour = this.FontColour;
            anno.CanReduceFontToFit = this.CanReduceFontToFit;
            anno.BackgroundColour = this.BackgroundColour;
            anno.BorderColour = this.BorderColour;
            anno.BorderThickness = this.BorderThickness;
            anno.Height = this.Height;
            anno.Width = this.Width;
            anno.Depth = this.Depth;
            anno.Text = this.Text;

            anno.PlanogramFixtureItemId = this.FixtureItemId;
            anno.PlanogramAssemblyComponentId = this.FixtureAssemblyComponentId;
            anno.PlanogramFixtureComponentId = this.FixtureComponentItemId;
            anno.PlanogramSubComponentId = this.FixtureSubComponentId;


            return anno;
        }

        #endregion
    }
}
