﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
// V8-25757 : L.Ineson
//  Removed PlanogramGroup_Id link
//  Added Entity_Id link
// V8-27411 : M.Pettit
//  Added DateCompleted and UserId (owner) properties
// V8-28046 : N.Foster
//  Added WorkpackageType
// V8-28232 : N.Foster
//  Added support for batch operations
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Made changes to improve performance of engine
#endregion
#region Version History: CCM810
// V8-29811 : A.Probyn
// Added PlanogramLocationType
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    public partial class Workpackage
    {
        #region Constructors
        private Workpackage() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the specified workpackage
        /// </summary>
        public static Workpackage FetchById(Int32 workpackageId)
        {
            return DataPortal.Fetch<Workpackage>(new FetchByWorkpackageIdCriteria(workpackageId));
        }

        /// <summary>
        /// Returns the specified workpackage but
        /// only loads data for the specified planogram
        /// </summary>
        public static Workpackage FetchByWorkpackageIdDestinationPlanogramId(Int32 workpackageId, Int32 destinationPlanogramId)
        {
            return DataPortal.Fetch<Workpackage>(new FetchByWorkpackageIdDestinationPlanogramIdCriteria(workpackageId, destinationPlanogramId));
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, WorkpackageDto dto, Int32? destinationPlanogramId)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<Int32>(WorkflowIdProperty, dto.WorkflowId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<WorkpackageType>(WorkpackageTypeProperty, (WorkpackageType)dto.WorkpackageType);
            this.LoadProperty<Int32>(UserIdProperty, dto.UserId);
            this.LoadProperty<WorkpackagePlanogramLocationType>(PlanogramLocationTypeProperty, (WorkpackagePlanogramLocationType)dto.PlanogramLocationType);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<DateTime?>(DateCompletedProperty, dto.DateCompleted);
            if (destinationPlanogramId != null)
            {
                this.LoadProperty<WorkpackagePlanogramList>(PlanogramsProperty, WorkpackagePlanogramList.FetchByWorkpackageIdDestinationPlanogramId(dalContext, dto.Id, (Int32)destinationPlanogramId, dto.WorkflowId));
            }
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private WorkpackageDto GetDataTransferObject()
        {
            return new WorkpackageDto()
            {
                Id = this.ReadProperty<Int32>(IdProperty),
                RowVersion = this.ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = this.ReadProperty<Int32>(EntityIdProperty),
                WorkflowId = this.ReadProperty<Int32>(WorkflowIdProperty),
                Name = this.ReadProperty<String>(NameProperty),
                WorkpackageType = (Byte)this.ReadProperty<WorkpackageType>(WorkpackageTypeProperty),
                UserId = this.ReadProperty<Int32>(UserIdProperty),
                PlanogramLocationType = (Byte)this.ReadProperty<WorkpackagePlanogramLocationType>(PlanogramLocationTypeProperty),
                DateCreated = this.ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = this.ReadProperty<DateTime>(DateLastModifiedProperty),
                DateCompleted = this.ReadProperty<DateTime?>(DateCompletedProperty)
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void DataPortal_Fetch(FetchByWorkpackageIdCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IWorkpackageDal dal = dalContext.GetDal<IWorkpackageDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.WorkpackageId), null);
                }
            }
        }

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void DataPortal_Fetch(FetchByWorkpackageIdDestinationPlanogramIdCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IWorkpackageDal dal = dalContext.GetDal<IWorkpackageDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.WorkpackageId), criteria.DestinationPlanogramId);
                }
            }
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this instance into the data store
        /// </summary>
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (BatchSaveContext batchContext = new BatchSaveContext(dalContext))
                {
                    // register batch handlers in order they will be committed
                    batchContext.Register<WorkpackageDto, IWorkpackageDal>();
                    batchContext.Register<WorkpackagePlanogramDto, IWorkpackagePlanogramDal>();
                    batchContext.Register<WorkpackagePlanogramParameterDto, IWorkpackagePlanogramParameterDal>();
                    batchContext.Register<WorkpackagePlanogramParameterValueDto, IWorkpackagePlanogramParameterValueDal>();

                    // insert the workpackage
                    Int32 oldId = 0;
                    batchContext.Insert<WorkpackageDto>(
                    (dc) =>
                    {
                        WorkpackageDto dto = this.GetDataTransferObject();
                        oldId = dto.Id;
                        return dto;
                    },
                    (dc, dto) =>
                    {
                        this.LoadProperty<Int32>(IdProperty, dto.Id);
                        this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                        this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                        this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                        dc.RegisterId<Workpackage>(oldId, dto.Id);
                    });

                    // update children
                    FieldManager.UpdateChildren(batchContext, this);

                    // commit the batch
                    batchContext.Commit();
                }
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this instance into the data store
        /// </summary>
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (BatchSaveContext batchContext = new BatchSaveContext(dalContext))
                {
                    // register batch handlers in order they will be committed
                    batchContext.Register<WorkpackageDto, IWorkpackageDal>();
                    batchContext.Register<WorkpackagePlanogramDto, IWorkpackagePlanogramDal>();
                    batchContext.Register<WorkpackagePlanogramParameterDto, IWorkpackagePlanogramParameterDal>();
                    batchContext.Register<WorkpackagePlanogramParameterValueDto, IWorkpackagePlanogramParameterValueDal>();

                    // update the workpackage
                    batchContext.Update<WorkpackageDto>(
                        this.GetDataTransferObject(),
                        (dc, dto) =>
                        {
                            this.LoadProperty<Int32>(IdProperty, dto.Id);
                            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                        });

                    // update children
                    FieldManager.UpdateChildren(batchContext, this);

                    // commit the batch
                    batchContext.Commit();
                }
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this instance is being deleted
        /// </summary>
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IWorkpackageDal dal = dalContext.GetDal<IWorkpackageDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }
        #endregion

        #endregion

        #region Commands

        #region DeleteById
        /// <summary>
        /// Deletes the workpackage with the given id
        /// </summary>
        private partial class DeleteByIdCommand
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                // upsert the dtos
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IWorkpackageDal dal = dalContext.GetDal<IWorkpackageDal>())
                    {
                        dal.DeleteById(this.WorkpackageId);
                    }
                }
            }
            #endregion

            #endregion
        }
        #endregion

        #region Execute
        /// <summary>
        /// Submits this workpackage for
        /// execution by the engine
        /// </summary>
        public partial class ExecuteCommand
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this command on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                Engine.Engine.EnqueueMessage(
                    new Engine.Processes.Workpackage.Initialization.Message(
                        this.EntityId,
                        this.WorkpackageId,
                        this.WorkpackageName,
                        this.WorkpackageRowVersion));
            }
            #endregion

            #endregion
        }
        #endregion

        #endregion
    }
}
