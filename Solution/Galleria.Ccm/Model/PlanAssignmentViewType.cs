﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Model
{
    #region Enums

    public enum PlanAssignmentViewType
    {
        ViewCategory,
        ViewLocation,
    }

    #endregion
}
