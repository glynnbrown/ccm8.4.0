﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-27411 : M.Pettit
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Engine;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using System.Collections.Generic;
using Galleria.Framework.Engine.Dal.DataTransferObjects;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public partial class EngineInstanceInfoList
    {
        #region Constructors
        private EngineInstanceInfoList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all registers engine instances
        /// </summary>
        public static EngineInstanceInfoList GetEngineInstanceInfoList(IDalContext dalContext)
        {
            return DataPortal.FetchChild<EngineInstanceInfoList>(dalContext);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning all registered engine instances
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IEngineInstanceDal dal = dalContext.GetDal<IEngineInstanceDal>())
            {
                IEnumerable<EngineInstanceDto> dtoList = dal.FetchAll();
                foreach (EngineInstanceDto dto in dtoList)
                {
                    this.Add(EngineInstanceInfo.GetEngineInstanceInfo(dalContext, dto));
                }
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}

