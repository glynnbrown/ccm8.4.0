﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25395 : A.Probyn
//		Created 
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;

namespace Galleria.Ccm.Model
{
    public partial class ProductLibraryInfoList
    {
        #region Constants
        private const String _dalAssemblyName = "Galleria.Ccm.Dal.{0}.dll";
        #endregion

        #region Constructors
        private ProductLibraryInfoList() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Fetches a list of infos for the givne product libraries
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <returns></returns>
        public static ProductLibraryInfoList FetchByFileNames(IEnumerable<String> fileNames)
        {
            return DataPortal.Fetch<ProductLibraryInfoList>(new FetchByIdsCriteria(ProductLibraryDalFactoryType.FileSystem, fileNames.Cast<Object>().ToList()));
        }

        /// <summary>
        /// Returns the package with the specified ID from the default DAL.  Created for the purpose of unit testing
        /// with the Mock DAL--not sure if this is something you'd need in real life or not.
        /// </summary>
        public static ProductLibraryInfoList FetchByIds(IEnumerable<Object> ids)
        {
            return DataPortal.Fetch<ProductLibraryInfoList>(new FetchByIdsCriteria(ProductLibraryDalFactoryType.Unknown, ids.ToList()));
        }

        /// <summary>
        /// Returns a list of all items by entity id not including deleted. 
        /// </summary>
        /// <param name="filenames"></param>
        /// <returns></returns>
        public static ProductLibraryInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<ProductLibraryInfoList>(new FetchByEntityIdCriteria(entityId));
        }


        #endregion

        #region Data Access

        #region Fetch

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdsCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(criteria.DalType, out dalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductLibraryInfoDal dal = dalContext.GetDal<IProductLibraryInfoDal>())
                {
                    foreach (ProductLibraryInfoDto dto in dal.FetchByIds(criteria.Ids))
                    {
                        this.Add(ProductLibraryInfo.GetProductLibraryInfo(dalContext, dto));
                    }
                }
            }


            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(criteria.DalType, out dalFactoryName);

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductLibraryInfoDal dal = dalContext.GetDal<IProductLibraryInfoDal>())
                {
                    foreach (ProductLibraryInfoDto dto in dal.FetchByEntityId(criteria.EntityId))
                    {
                        if (criteria.DalType == ProductLibraryDalFactoryType.FileSystem)
                        {
                            this.Add(ProductLibraryInfo.GetProductLibraryInfo(dalContext, dto));
                        }
                        else
                        {
                            this.Add(ProductLibraryInfo.GetProductLibraryInfo(dalContext, dto));
                        }
                    }
                }
            }

            this.RaiseListChangedEvents = true;
            this.IsReadOnly = true;
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the correct dal factory based on the given type and args.
        /// </summary>
        private static IDalFactory GetDalFactory(ProductLibraryDalFactoryType dalType, out String dalFactoryName)
        {
            switch (dalType)
            {
                case ProductLibraryDalFactoryType.FileSystem:
                    dalFactoryName = Constants.UserDal;
                    return DalContainer.GetDalFactory(dalFactoryName);

                default:
                    dalFactoryName = DalContainer.DalName;
                    return DalContainer.GetDalFactory();
            }

        }

        #endregion
    }
}
