﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion
#region Version History: CCM830
// V8-32357 : M.Brumby
//  [PCR01561] added DisplayName and DisplayDescription
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class WorkflowTask : ModelObject<WorkflowTask>
    {
        #region Parent
        /// <summary>
        /// Reutrns a reference to the parent workflow
        /// </summary>
        public new Workflow Parent
        {
            get { return ((WorkflowTaskList)base.Parent).Parent; }
        }
        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the workflow task id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region SequenceId
        /// <summary>
        /// SequenceId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> SequenceIdProperty =
            RegisterModelProperty<Byte>(c => c.SequenceId);
        /// <summary>
        /// Returns the position in the workflow for the task
        /// </summary>
        public Byte SequenceId
        {
            get { return this.GetProperty<Byte>(SequenceIdProperty); }
            internal set { this.SetProperty<Byte>(SequenceIdProperty, value); }
        }
        #endregion

        #region Details
        /// <summary>
        /// Details property definition
        /// </summary>
        public static readonly ModelPropertyInfo<EngineTaskInfo> DetailsProperty =
            RegisterModelProperty<EngineTaskInfo>(c => c.Details);
        /// <summary>
        /// Returns the workflow task type details
        /// </summary>
        public EngineTaskInfo Details
        {
            get { return this.GetProperty<EngineTaskInfo>(DetailsProperty); }
        }
        #endregion

        #region Parameters
        /// <summary>
        /// Parameters property definition
        /// </summary>
        public static readonly ModelPropertyInfo<WorkflowTaskParameterList> ParametersProperty =
            RegisterModelProperty<WorkflowTaskParameterList>(c => c.Parameters);
        /// <summary>
        /// Returns all task parameters
        /// </summary>
        public WorkflowTaskParameterList Parameters
        {
            get { return this.GetProperty<WorkflowTaskParameterList>(ParametersProperty); }
        }
        #endregion

        #region User Display Properties

        /// <summary>
        /// DisplayName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DisplayNameProperty =
            RegisterModelProperty<String>(c => c.DisplayName);
        /// <summary>
        /// Returns the DisplayName for the workflow for the task
        /// </summary>
        public String DisplayName
        {
            get { return this.GetProperty<String>(DisplayNameProperty); }
            set { this.SetProperty<String>(DisplayNameProperty, value);
            OnPropertyChanged("VisibleName");
            }
        }

        /// <summary>
        /// Display Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DisplayDescriptionProperty =
            RegisterModelProperty<String>(c => c.DisplayDescription);
        /// <summary>
        /// Returns the Display Description for the workflow for the task
        /// </summary>
        public String DisplayDescription
        {
            get { return this.GetProperty<String>(DisplayDescriptionProperty); }
            set { this.SetProperty<String>(DisplayDescriptionProperty, value);
            OnPropertyChanged("VisibleDescription");
            }
        }

        #endregion

        #region Helper Properties

        public String VisibleName
        {
            get
            {
                return String.IsNullOrWhiteSpace(DisplayName) ?
                            Details.Name : DisplayName;
            }
        }

        public String VisibleDescription
        {
            get
            {
                return String.IsNullOrWhiteSpace(DisplayDescription) ?
                            Details.Description : DisplayDescription;
            }
        }

        #endregion
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(WorkflowTask), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTask), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.WorkflowCreate.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTask), new IsInRole(AuthorizationActions.EditObject, DomainPermission.WorkflowEdit.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTask), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.WorkflowDelete.ToString()));
        }
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new Entity
        /// </summary>
        internal static WorkflowTask NewWorkflowTask(EngineTaskInfo task, Byte sequenceId)
        {
            WorkflowTask item = new WorkflowTask();
            item.Create(task, sequenceId);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected void Create(EngineTaskInfo task, Byte sequenceId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<EngineTaskInfo>(DetailsProperty, task);
            this.LoadProperty<Byte>(SequenceIdProperty, sequenceId);
            this.LoadProperty<WorkflowTaskParameterList>(ParametersProperty, WorkflowTaskParameterList.NewWorkflowTaskParameterList(task));
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// ToString override
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Details.Name;
        }

        /// <summary>
        /// Moves the task to specified position within the workflow sequence
        /// </summary>
        public void MoveTo(Byte sequenceId)
        {
            IEnumerable<WorkflowTask> tasks = this.Parent.Tasks.OrderBy(item => item.SequenceId);
            Byte sequence = 0;
            foreach (WorkflowTask task in tasks)
            {
                if (task.SequenceId != this.SequenceId)
                {
                    if (sequence == sequenceId) sequence++;
                    task.SequenceId = sequence;
                    sequence++;
                }
            }
            this.SequenceId = sequenceId;
        }

        /// <summary>
        /// Moves the item up the sequence, moving other items around
        /// </summary>
        public void MoveUp()
        {
            if (this.SequenceId > Byte.MinValue)
            {
                this.MoveTo((Byte)(this.SequenceId - 1));
            }
        }

        /// <summary>
        /// Moves an item up the sequence
        /// </summary>
        public void MoveDown()
        {
            if (this.SequenceId < Byte.MaxValue)
            {
                this.MoveTo((Byte)(this.SequenceId + 1));
            }
        }

        /// <summary>
        /// Creates an instance of this task
        /// </summary>
        public TaskBase CreateTask()
        {
            return this.Details.CreateTask();
        }
        #endregion
    }
}
