﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created (Auto-generated)
// V8-26812 : A.Silva ~ Added ValidationType Property.

#endregion

#region Version History: (CCM v8.20)
// V8-31533 : A.Probyn
//  Updated lazy loaded properties to not be lazy loaded as they were causing serialization exceptions
//  when used in the workflow client.
#endregion

#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    public partial class ValidationTemplateGroup
    {
        #region Constructor

        private ValidationTemplateGroup()
        {
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Returns an existing item from the given dto
        /// </summary>
        internal static ValidationTemplateGroup Fetch(IDalContext dalContext, ValidationTemplateGroupDto dto)
        {
            return DataPortal.FetchChild<ValidationTemplateGroup>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Creates a dto from this object
        /// </summary>
        private ValidationTemplateGroupDto GetDataTransferObject(ValidationTemplate parent)
        {
            return new ValidationTemplateGroupDto
            {
                Id = ReadProperty(IdProperty),
                ValidationTemplateId = parent.Id,
                Name = ReadProperty(NameProperty),
                Threshold1 = ReadProperty(Threshold1Property),
                Threshold2 = ReadProperty(Threshold2Property),
                ValidationType = (Byte) ReadProperty(ValidationTypeProperty)
            };
        }

        /// <summary>
        ///     Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, ValidationTemplateGroupDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(NameProperty, dto.Name);
            LoadProperty(Threshold1Property, dto.Threshold1);
            LoadProperty(Threshold2Property, dto.Threshold2);
            LoadProperty(ValidationTypeProperty, (PlanogramValidationType) dto.ValidationType);
            LoadProperty<ValidationTemplateGroupMetricList>(MetricListProperty, ValidationTemplateGroupMetricList.FetchByParentId(dalContext, dto.Id));
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ValidationTemplateGroupDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        ///     Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, ValidationTemplate parent)
        {
            ValidationTemplateGroupDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (var dal = dalContext.GetDal<IValidationTemplateGroupDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty(IdProperty, dto.Id);
            dalContext.RegisterId<ValidationTemplateGroup>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        ///     Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, ValidationTemplate parent)
        {
            if (this.IsSelfDirty)
            {
                using (var dal = dalContext.GetDal<IValidationTemplateGroupDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        ///     Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, ValidationTemplate parent)
        {
            using (var dal = dalContext.GetDal<IValidationTemplateGroupDal>())
            {
                dal.DeleteById(Id);
            }
        }

        #endregion

        #endregion
    }
}