﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
// V8-26704 : A.Kuszyk
//  Implemented IPlanogramAssortmentRegion.
// GFS-26739 : J.Pickup
//  Removed tempId and introduced an override of OnCopy method to use Resolve Id.
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
#endregion

#endregion

using System;
using System.Linq;
using AutoMapper;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using System.Collections.Generic;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A class representing an Assortment Region within CCM
    /// (Child of Assortment)
    /// This is a child Content object and so it has the following properties/actions:
    /// - This object does NOT have a DateDeleted property or DateDeleted field in its supporting database table
    /// - if its parent is deleted, it is not removed or marked as deleted (this allows for parent 'undeletes')
    /// - if it is deleted directly, it's associated record is removed from the database. 
    [Serializable]
    [DefaultNewMethod("NewAssortmentRegion")]
    public partial class AssortmentRegion : ModelObject<AssortmentRegion>, IPlanogramAssortmentRegion
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent Scenario
        /// </summary>
        public Assortment ParentAssortment
        {
            get { return ((AssortmentRegionList)base.Parent).ParentAssortment; }
        }
        #endregion

        #region Static Constructor
        static AssortmentRegion()
        {
        }
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The AssortmentRegion id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// RegionId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32>RegionIdProperty =
            RegisterModelProperty<Int32>(c => c.RegionId);

        /// <summary>
        /// Gets/Sets the RegionId value
        /// </summary>
        public Int32 RegionId
        {
            get { return GetProperty<Int32>(RegionIdProperty); }
            set { SetProperty<Int32>(RegionIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The Region Name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        public static readonly ModelPropertyInfo<AssortmentRegionLocationList> LocationsProperty =
            RegisterModelProperty<AssortmentRegionLocationList>(c => c.Locations);
        /// <summary>
        /// The Assortment Region's Locations. This is a list of stores in a region where 
        /// regional products will be swapped in
        /// </summary>
        public AssortmentRegionLocationList Locations
        {
            get { return GetProperty<AssortmentRegionLocationList>(LocationsProperty); }
        }

        public static readonly ModelPropertyInfo<AssortmentRegionProductList> ProductsProperty =
            RegisterModelProperty<AssortmentRegionProductList>(c => c.Products);
        /// <summary>
        /// The Assortment Region's Products. This is a list of products in a region and their 
        /// regional replacements
        /// </summary>
        public AssortmentRegionProductList Products
        {
            get { return GetProperty<AssortmentRegionProductList>(ProductsProperty); }
        }

        #region IPlanogramAssortmentRegion Members

        /// <summary>
        /// IPlanogramAssortmentRegion Locations member.
        /// </summary>
        IEnumerable<IPlanogramAssortmentRegionLocation> IPlanogramAssortmentRegion.Locations
        {
            get { return Locations.Select(o => (IPlanogramAssortmentRegionLocation)o); }
        }

        /// <summary>
        /// IPlanogramAssortmentRegion Products member.
        /// </summary>
        IEnumerable<IPlanogramAssortmentRegionProduct> IPlanogramAssortmentRegion.Products
        {
            get { return Products.Select(o => (IPlanogramAssortmentRegionProduct)o); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));

            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentRegion), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.AssortmentCreate.ToString()));
            BusinessRules.AddRule(typeof(AssortmentRegion), new IsInRole(AuthorizationActions.GetObject, DomainPermission.AssortmentGet.ToString()));
            BusinessRules.AddRule(typeof(AssortmentRegion), new IsInRole(AuthorizationActions.EditObject, DomainPermission.AssortmentEdit.ToString()));
            BusinessRules.AddRule(typeof(AssortmentRegion), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.AssortmentDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentRegion NewAssortmentRegion(String regionName)
        {
            AssortmentRegion item = new AssortmentRegion();
            item.Create(regionName);
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentRegion NewAssortmentRegion()
        {
            AssortmentRegion item = new AssortmentRegion();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<AssortmentRegionLocationList>(LocationsProperty, AssortmentRegionLocationList.NewAssortmentRegionLocationList());
            this.LoadProperty<AssortmentRegionProductList>(ProductsProperty, AssortmentRegionProductList.NewAssortmentRegionProductList());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create(String regionName)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, regionName);
            this.LoadProperty<AssortmentRegionLocationList>(LocationsProperty, AssortmentRegionLocationList.NewAssortmentRegionLocationList());
            this.LoadProperty<AssortmentRegionProductList>(ProductsProperty, AssortmentRegionProductList.NewAssortmentRegionProductList());
            this.Create();
        }

        #endregion

        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<AssortmentRegion>(oldId, newId);
        }
        #endregion
    }
}