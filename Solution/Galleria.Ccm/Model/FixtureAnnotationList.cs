﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// CCM-32524 : L.Ineson
// Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A list of Annotations contained within a planogram
    /// </summary>
    [Serializable]
    public sealed partial class FixtureAnnotationList : ModelList<FixtureAnnotationList, FixtureAnnotation>
    {

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static FixtureAnnotationList NewFixtureAnnotationList()
        {
            FixtureAnnotationList item = new FixtureAnnotationList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Creates and adds a new item to this collection
        /// </summary>
        public FixtureAnnotation Add(FixtureItem fixtureItem)
        {
            FixtureAnnotation annotation = FixtureAnnotation.NewFixtureAnnotation(fixtureItem);
            this.Add(annotation);
            return annotation;
        }

        /// <summary>
        /// Creates and adds a new item to this collection
        /// </summary>
        public FixtureAnnotation Add(FixtureComponentItem fixtureComponentItem, FixtureItem fixtureItem)
        {
            FixtureAnnotation annotation = FixtureAnnotation.NewFixtureAnnotation(fixtureComponentItem, fixtureItem);
            this.Add(annotation);
            return annotation;
        }

        /// <summary>
        /// Creates and adds a new item to this collection
        /// </summary>
        public FixtureAnnotation Add(PlanogramAnnotation planAnnotation)
        {
            FixtureAnnotation annotation = FixtureAnnotation.NewFixtureAnnotation(planAnnotation);
            this.Add(annotation);
            return annotation;
        }

        #endregion
    }
}
