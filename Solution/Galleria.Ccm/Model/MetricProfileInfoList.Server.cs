﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26123 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class MetricProfileInfoList
    {
        #region Constructors
        private MetricProfileInfoList() { } // force the use of factory methods
        #endregion

        #region FactoryMethods

        public static MetricProfileInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<MetricProfileInfoList>(new FetchByEntityIdCriteria(entityId));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when retrieving a list of all locations for an entity
        /// Note: This procedure should NOT return deleted items
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IMetricProfileInfoDal dal = dalContext.GetDal<IMetricProfileInfoDal>())
                {
                    IEnumerable<MetricProfileInfoDto> dtoList = dal.FetchByEntityId(criteria.EntityId);

                    foreach (MetricProfileInfoDto dto in dtoList)
                    {
                        this.Add(MetricProfileInfo.FetchMetricProfileInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }


        #endregion

        #endregion
    }
}
