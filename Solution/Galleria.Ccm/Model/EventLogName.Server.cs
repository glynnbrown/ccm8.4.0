﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : Luong
//   Created (Copied From GFS)
#endregion

#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class EventLogName
    {
        #region Constructor
        private EventLogName() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates the item from the given dto
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal static EventLogName GetEventLogName(IDalContext dalContext, EventLogNameDto dto)
        {
            return DataPortal.FetchChild<EventLogName>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private EventLogNameDto GetDataTransferObject()
        {
            return new EventLogNameDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                Name = ReadProperty<String>(NameProperty)
            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, EventLogNameDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<String>(NameProperty, dto.Name);
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Called when return FetchById
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(Int32 id)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IEventLogNameDal dal = dalContext.GetDal<IEventLogNameDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchById(id));
                }
            }
        }

        /// <summary>
        /// Called when return FetchById
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(String name)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IEventLogNameDal dal = dalContext.GetDal<IEventLogNameDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchByName(name));
                }
            }
        }

        /// <summary>
        /// Called when return Child
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, EventLogNameDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            //insert the Event Log itself
            EventLogNameDto eventLogNameDto = GetDataTransferObject();
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IEventLogNameDal dal = dalContext.GetDal<IEventLogNameDal>())
                {
                    dal.Insert(eventLogNameDto);
                    LoadProperty<Int32>(IdProperty, eventLogNameDto.Id);
                    //LoadProperty<RowVersion>(RowVersionProperty, eventLogDto.RowVersion);
                }
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }

        /// <summary>
        /// Inserts a new object into the solution
        /// </summary>
        /// <param name="parent">The parent model node</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext/*, LocationTypeList parent*/)
        {
            using (IEventLogNameDal dal = dalContext.GetDal<IEventLogNameDal>())
            {
                // get the dto
                EventLogNameDto dto = GetDataTransferObject();
                dal.Insert(dto);

                //update this.Id from the dto id
                LoadProperty<Int32>(IdProperty, dto.Id);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #endregion
    }
}
