﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : Luong
//   Created (Copied From GFS)
#endregion

#endregion

using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class EventLogNameList
    {
        #region Constructor
        private EventLogNameList() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an entity info from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        /// <returns>An entity info object</returns>
        public static EventLogNameList FetchAll()
        {
            return DataPortal.Fetch<EventLogNameList>();
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when return Fetch
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch()
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IEventLogNameDal dal = dalContext.GetDal<IEventLogNameDal>())
                {
                    IEnumerable<EventLogNameDto> dtoList = dal.FetchAll();

                    foreach (EventLogNameDto dto in dtoList)
                    {
                        this.Add(EventLogName.GetEventLogName(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;
            MarkAsChild();
        }

        #endregion

        #endregion
    }
}
