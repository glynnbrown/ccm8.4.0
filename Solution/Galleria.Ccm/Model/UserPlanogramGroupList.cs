﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25664: A.Kuszyk
//		Created.
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of UserPlanogramGroup objects.
    /// </summary>
    [Serializable]
    public partial class UserPlanogramGroupList : ModelList<UserPlanogramGroupList,UserPlanogramGroup>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(UserPlanogramGroupList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(UserPlanogramGroupList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(UserPlanogramGroupList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(UserPlanogramGroupList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Authenticated.ToString()));
        }
        #endregion

        #region Criteria

        [Serializable]
        public class FetchByUserIdCriteria : CriteriaBase<FetchByUserIdCriteria>
        {
            #region Properties
            public static readonly PropertyInfo<Int32> UserIdProperty =
                RegisterProperty<Int32>(c => c.UserId);
            public Int32 UserId
            {
                get { return ReadProperty<Int32>(UserIdProperty); }
            } 
            #endregion

            #region Constructors
            public FetchByUserIdCriteria(Int32 userId)
            {
                LoadProperty<Int32>(UserIdProperty, userId);
            } 
            #endregion
        }

        #endregion

        #region Factory Methods

        public static UserPlanogramGroupList NewUserPlanogramGroupList()
        {
            var model = new UserPlanogramGroupList();
            model.Create();
            return model;
        }

        #endregion
    }
}
