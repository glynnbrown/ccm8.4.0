﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
// V8-25454 : J.Pickup
//      Added a helper property : ProductGroupFriendlyName.
// V8-25455 : J.Pickup
//		Added FetchByPlanogramIdsCriteria
#endregion
#region Version History: (CCM 8.0.3)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines an entity info within the solution
    /// </summary>
    [Serializable]
    public sealed partial class ConsumerDecisionTreeInfo : ModelReadOnlyObject<ConsumerDecisionTreeInfo>
    {
        #region Properties

        /// <summary>
        /// The entity id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }
        /// <summary>
        /// The entity name
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        /// <summary>
        /// Unique content reference property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Guid> UniqueContentReferenceProperty =
            RegisterModelProperty<Guid>(c => c.UniqueContentReference, Message.UniqueContentReference);
        /// <summary>
        /// Unique content reference
        /// </summary>
        public Guid UniqueContentReference
        {
            get { return GetProperty<Guid>(UniqueContentReferenceProperty); }
        }

        public static readonly ModelPropertyInfo<Int32?> ProductGroupIdProperty =
            RegisterModelProperty<Int32?>(c => c.ProductGroupId);
        /// <summary>
        /// The product group id
        /// </summary>
        public Int32? ProductGroupId
        {
            get { return GetProperty<Int32?>(ProductGroupIdProperty); }
        }

        /// <summary>
        /// The entity description
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
        }

        public static readonly ModelPropertyInfo<String> ProductGroupCodeProperty =
            RegisterModelProperty<String>(c => c.ProductGroupCode);
        /// <summary>
        /// The code of the product group to which this info is assigned.
        /// </summary>
        public String ProductGroupCode
        {
            get { return GetProperty<String>(ProductGroupCodeProperty); }
        }


        public static readonly ModelPropertyInfo<String> ProductGroupNameProperty =
            RegisterModelProperty<String>(c => c.ProductGroupName);
        /// <summary>
        /// The name of the product group to which this info is assigned.
        /// </summary>
        public String ProductGroupName
        {
            get { return GetProperty<String>(ProductGroupNameProperty); }
        }

        /// <summary>
        /// Parent unique content reference property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Guid?> ParentUniqueContentReferenceProperty =
            RegisterModelProperty<Guid?>(c => c.ParentUniqueContentReference);
        /// <summary>
        /// Parent unique content reference
        /// </summary>
        public Guid? ParentUniqueContentReference
        {
            get { return GetProperty<Guid?>(ParentUniqueContentReferenceProperty); }
        }

        #endregion

        #region Helper Properties

        public static readonly ModelPropertyInfo<String> ProductGroupFriendlyNameProperty =
            RegisterModelProperty<String>(c => c.ProductGroupFriendlyName);
        /// <summary>
        /// Concat of product group code & name.
        /// </summary>
        public String ProductGroupFriendlyName
        {
            get { return String.Concat(GetProperty<String>(ProductGroupCodeProperty), " ", GetProperty<String>(ProductGroupNameProperty)); }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchById
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            public static readonly PropertyInfo<Int32> IdProperty =
            RegisterProperty<Int32>(c => c.Id);
            public Int32 Id
            {
                get { return ReadProperty<Int32>(IdProperty); }
            }

            public FetchByIdCriteria(Int32 id)
            {
                LoadProperty<Int32>(IdProperty, id);
            }
        }

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        #endregion
    }
}
