﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Reperesents a list of repository sync target parameters
    /// </summary>
    public partial class RepositorySyncTargetParameterList : ModelList<RepositorySyncTargetParameterList, RepositorySyncTargetParameter>
    {
        #region Static Constructor
        static RepositorySyncTargetParameterList()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(RepositorySyncTargetParameterList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(RepositorySyncTargetParameterList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(RepositorySyncTargetParameterList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(RepositorySyncTargetParameterList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Temporary.ToString()));
        }
        #endregion
    }
}
