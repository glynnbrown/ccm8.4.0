﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25879 : N.Haywood
//	Created (Auto-generated)
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of LocationPlanAssignment objects.
    /// </summary>
    [Serializable]
    public sealed partial class LocationPlanAssignmentList : ModelList<LocationPlanAssignmentList, LocationPlanAssignment>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationPlanAssignmentList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(LocationPlanAssignmentList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationPlanAssignmentCreate.ToString()));
            BusinessRules.AddRule(typeof(LocationPlanAssignmentList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationPlanAssignmentEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationPlanAssignmentList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationPlanAssignmentDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static LocationPlanAssignmentList NewLocationPlanAssignmentList()
        {
            LocationPlanAssignmentList item = new LocationPlanAssignmentList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            base.Create();
        }
        #endregion

        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByCategoryReviewId
        /// </summary>
        [Serializable]
        public class FetchByEntityIdProductGroupIdCriteria : Csla.CriteriaBase<FetchByEntityIdProductGroupIdCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static readonly PropertyInfo<Int32> ProductGroupIdProperty =
                RegisterProperty<Int32>(c => c.ProductGroupId);
            public Int32 ProductGroupId
            {
                get { return ReadProperty<Int32>(ProductGroupIdProperty); }
            }

            public FetchByEntityIdProductGroupIdCriteria(Int32 entityId, Int32 productGroupId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<Int32>(ProductGroupIdProperty, productGroupId);
            }
        }

        /// <summary>
        /// Criteria for 
        /// </summary>
        [Serializable]
        public class FetchByEntityIdLocationIdCriteria : Csla.CriteriaBase<FetchByEntityIdLocationIdCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static readonly PropertyInfo<Int16> LocationIdProperty =
                RegisterProperty<Int16>(c => c.LocationId);
            public Int16 LocationId
            {
                get { return ReadProperty<Int16>(LocationIdProperty); }
            }

            public FetchByEntityIdLocationIdCriteria(Int32 entityId, Int16 locationId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<Int16>(LocationIdProperty, locationId);
            }
        }

        /// <summary>
        /// Criteria for FetchByCategoryReviewId
        /// </summary>
        [Serializable]
        public class FetchByPlanogramIdCriteria : Csla.CriteriaBase<FetchByPlanogramIdCriteria>
        {

            public static readonly PropertyInfo<Int32> PlanIdProperty =
                RegisterProperty<Int32>(c => c.PlanId);
            public Int32 PlanId
            {
                get { return ReadProperty<Int32>(PlanIdProperty); }
            }

            public FetchByPlanogramIdCriteria(Int32 planId)
            {
                LoadProperty<Int32>(PlanIdProperty, planId);
            }
        }
        #endregion
    }
}