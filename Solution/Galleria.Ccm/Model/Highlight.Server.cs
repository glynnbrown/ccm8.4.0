﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// CCM-V8-24801 : L.Hodson
//	Created (Auto-generated)
// V8-25436 : L.Luong
//  Changed to fit new design and no longer child class
// V8-27941 J.Pickup
//  Introduced new properties for respository & GetDalFactory can also now return mssql dalFactory.
// V8-28362 : L.Ineson
//  Added the ability to open a file as readonly.
#endregion

#region Version History: (CCM 8.1.0)
// V8-28661 : L.Ineson
//  Added FetchByEntityIdName
// V8-30194 : M.Shelley
//  Added InsertUsingExistingContext method to allow inserting of Highlights using the the DAL Context.
#endregion

#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    public partial class Highlight
    {
        #region Constructor
        private Highlight() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing Highlight with the given string
        /// </summary>
        public static Highlight FetchByFilename(String fileName)
        {
            return FetchByFilename(fileName, /*asReadOnly*/false);
        }

        /// <summary>
        /// Returns an existing Highlight with the given string
        /// </summary>
        public static Highlight FetchByFilename(String fileName, Boolean asReadOnly)
        {
            return DataPortal.Fetch<Highlight>(new FetchByIdCriteria(HighlightDalFactoryType.FileSystem, fileName, asReadOnly));
        }

        /// <summary>
        /// Returns an existing Highlight with the given id
        /// </summary>
        public static Highlight FetchById(Object id)
        {
            return DataPortal.Fetch<Highlight>(new FetchByIdCriteria(HighlightDalFactoryType.Unknown, id));
        }

        /// <summary>
        /// Returns an existing Highlight with the given id
        /// </summary>
        public static Highlight FetchById(Object id, Boolean asReadOnly)
        {
            return DataPortal.Fetch<Highlight>(new FetchByIdCriteria(HighlightDalFactoryType.Unknown, id, asReadOnly));
        }

        /// <summary>
        /// Returns an existing Highlight with the given id
        /// For unit testing purposes.
        /// </summary>
        public static Highlight FetchById(Object id, HighlightDalFactoryType dalFactoryType)
        {
            return DataPortal.Fetch<Highlight>(new FetchByIdCriteria(dalFactoryType, id));
        }

        /// <summary>
        /// Return the highlight w
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Highlight FetchByEntityIdName(Int32 entityId, String name)
        {
            return DataPortal.Fetch<Highlight>(new FetchByEntityIdNameCriteria(entityId, name));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, HighlightDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<HighlightType>(TypeProperty, (HighlightType)dto.Type);
            this.LoadProperty<HighlightMethodType>(MethodTypeProperty, (HighlightMethodType)dto.MethodType);
            this.LoadProperty<Boolean>(IsFilterEnabledProperty, dto.IsFilterEnabled);
            this.LoadProperty<Boolean>(IsPreFilterProperty, dto.IsPreFilter);
            this.LoadProperty<Boolean>(IsAndFilterProperty, dto.IsAndFilter);
            this.LoadProperty<String>(Field1Property, dto.Field1);
            this.LoadProperty<String>(Field2Property, dto.Field2);
            this.LoadProperty<HighlightQuadrantType>(QuadrantXTypeProperty, (HighlightQuadrantType)dto.QuadrantXType);
            this.LoadProperty<Single>(QuadrantXConstantProperty, dto.QuadrantXConstant);
            this.LoadProperty<HighlightQuadrantType>(QuadrantYTypeProperty, (HighlightQuadrantType)dto.QuadrantYType);
            this.LoadProperty<Single>(QuadrantYConstantProperty, dto.QuadrantYConstant);
            this.LoadProperty<HighlightGroupList>(GroupsProperty, HighlightGroupList.FetchByParentId(dalContext, dto.Id));
            this.LoadProperty<HighlightFilterList>(FiltersProperty, HighlightFilterList.FetchByParentId(dalContext, dto.Id));
            this.LoadProperty<HighlightCharacteristicList>(CharacteristicsProperty, HighlightCharacteristicList.FetchByParentId(dalContext, dto.Id));
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private HighlightDto GetDataTransferObject()
        {
            return new HighlightDto()
            {
                Id = ReadProperty<Object>(IdProperty),
                Name = ReadProperty<String>(NameProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                Type = (Byte)ReadProperty<HighlightType>(TypeProperty),
                MethodType = (Byte)ReadProperty<HighlightMethodType>(MethodTypeProperty),
                IsFilterEnabled = ReadProperty<Boolean>(IsFilterEnabledProperty),
                IsPreFilter = ReadProperty<Boolean>(IsPreFilterProperty),
                IsAndFilter = ReadProperty<Boolean>(IsAndFilterProperty),
                Field1 = ReadProperty<String>(Field1Property),
                Field2 = ReadProperty<String>(Field2Property),
                QuadrantXType = (Byte)ReadProperty<HighlightQuadrantType>(QuadrantXTypeProperty),
                QuadrantXConstant = ReadProperty<Single>(QuadrantXConstantProperty),
                QuadrantYType = (Byte)ReadProperty<HighlightQuadrantType>(QuadrantYTypeProperty),
                QuadrantYConstant = ReadProperty<Single>(QuadrantYConstantProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching a package by its Id
        /// </summary>
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            if (criteria.HighlightDalType == HighlightDalFactoryType.FileSystem
                && !criteria.AsReadOnly)
            {
                //lock the file first
                LockHighlightByFileName((String)criteria.Id);
            }

            IDalFactory dalFactory = this.GetDalFactory(criteria.HighlightDalType);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IHighlightDal dal = dalContext.GetDal<IHighlightDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.Id));

                    //set readonly flag
                    this.LoadProperty<Boolean>(IsReadOnlyProperty, criteria.AsReadOnly);
                }
            }
        }

        /// <summary>
        /// Called when fetching a highlight by entity id and name
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdNameCriteria criteria)
        {
            IDalFactory dalFactory = this.GetDalFactory(HighlightDalFactoryType.Unknown);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IHighlightDal dal = dalContext.GetDal<IHighlightDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchByEntityIdName(criteria.EntityId, criteria.Name));
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Insert()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                HighlightDto dto = this.GetDataTransferObject();
                Object oldId = dto.Id;
                
                using (IHighlightDal dal = dalContext.GetDal<IHighlightDal>())
                {
                    dal.Insert(dto);
                }
                
                this.LoadProperty<Object>(IdProperty, dto.Id);
                this.LoadProperty(RowVersionProperty, dto.RowVersion);
                this.LoadProperty(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<Highlight>(oldId, dto.Id);
                
                FieldManager.UpdateChildren(dalContext, this);
                
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Update()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                HighlightDto dto = GetDataTransferObject();
                if (this.IsSelfDirty)
                {
                    using (IHighlightDal dal = dalContext.GetDal<IHighlightDal>())
                    {
                        dal.Update(dto);
                    }
                    this.LoadProperty(RowVersionProperty, dto.RowVersion);
                    this.LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
                }
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IHighlightDal dal = dalContext.GetDal<IHighlightDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #endregion

        #region Commands

        #region LockHighlightCommand
        /// <summary>
        /// Performs the locking of a highlight
        /// </summary>
        private partial class LockHighlightCommand : CommandBase<LockHighlightCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.HighlightDalFactoryType, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IHighlightDal dal = dalContext.GetDal<IHighlightDal>())
                    {
                        dal.LockById(this.Id);
                    }
                }
            }
            #endregion

            #endregion
        }
        #endregion

        #region UnlockHighlightCommand
        /// <summary>
        /// Performs the unlocking of a Highlight
        /// </summary>
        private partial class UnlockHighlightCommand : CommandBase<UnlockHighlightCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.HighlightDalFactoryType, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IHighlightDal dal = dalContext.GetDal<IHighlightDal>())
                    {
                        dal.UnlockById(this.Id);
                    }
                }
            }
            #endregion

            #endregion
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the correct dal factory based on the daltype and args.
        /// </summary>
        /// <param name="dalType"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private IDalFactory GetDalFactory(HighlightDalFactoryType dalType)
        {
            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(dalType, out dalFactoryName);
            this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            this.LoadProperty<HighlightDalFactoryType>(DalFactoryTypeProperty, dalType);

            return dalFactory;
        }

        /// <summary>
        /// Returns the correct dal factory for this instance
        /// </summary>
        protected override IDalFactory GetDalFactory()
        {
            if (String.IsNullOrEmpty(this.DalFactoryName))
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.DalFactoryType, out dalFactoryName);
                this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            }

            return DalContainer.GetDalFactory(this.DalFactoryName);
        }

        /// <summary>
        /// Returns the correct dal factory based on the given type and args.
        /// </summary>
        private static IDalFactory GetDalFactory(HighlightDalFactoryType dalType, out String dalFactoryName)
        {
            switch (dalType)
            {
                case HighlightDalFactoryType.FileSystem:
                    dalFactoryName = Constants.UserDal;
                    return DalContainer.GetDalFactory(dalFactoryName);

                default:
                    dalFactoryName = DalContainer.DalName;
                    return DalContainer.GetDalFactory();
            }
        }

        /// <summary>
        /// Called when inserting this object and its children using an existing dal context.
        /// </summary>
        ///<remarks>used to insert default by sync process.</remarks>
        internal void InsertUsingExistingContext(IDalContext dalContext)
        {
            HighlightDto dto = GetDataTransferObject();
            Object oldId = dto.Id;

            using (IHighlightDal dal = dalContext.GetDal<IHighlightDal>())
            {
                dal.Insert(dto);
            }

            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty(RowVersionProperty, dto.RowVersion);
            this.LoadProperty(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
            dalContext.RegisterId<Highlight>(oldId, dto.Id);

            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion
    }
}