﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Used by the InventoryRules and InventoryObjectives screens
    /// </summary>
    public enum AssortmentInventoryRuleForceType
    {
        Units,
        Facings
    }
    public static class AssortmentInventoryRuleForceTypeHelper
    {
        public static readonly Dictionary<AssortmentInventoryRuleForceType, String> FriendlyNames =
            new Dictionary<AssortmentInventoryRuleForceType, String>()
            {
                {AssortmentInventoryRuleForceType.Units, Message.Enum_AssortmentInventoryRuleForceType_Units},
                {AssortmentInventoryRuleForceType.Facings, Message.Enum_AssortmentInventoryRuleForceType_Facings},
            };
    }
}
