﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model object representing a repository sync target
    /// </summary>
    public partial class RepositorySyncTarget : ModelObject<RepositorySyncTarget>
    {
        #region Static Constructor
        static RepositorySyncTarget()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the property id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region RepositoryType
        /// <summary>
        /// RepositoryType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RepositorySyncTargetType> RepositoryTypeProperty =
            RegisterModelProperty<RepositorySyncTargetType>(c => c.RepositoryType);
        /// <summary>
        /// Returns or sets the repository target type
        /// </summary>
        public RepositorySyncTargetType RepositoryType
        {
            get { return this.GetProperty<RepositorySyncTargetType>(RepositoryTypeProperty); }
            set { this.SetProperty<RepositorySyncTargetType>(RepositoryTypeProperty, value); }
        }
        #endregion

        #region Parameters
        /// <summary>
        /// Parameters property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RepositorySyncTargetParameterList> ParametersProperty =
            RegisterModelProperty<RepositorySyncTargetParameterList>(c => c.Parameters);
        /// <summary>
        /// Returns the sync target parameters
        /// </summary>
        public RepositorySyncTargetParameterList Parameters
        {
            get { return this.GetProperty<RepositorySyncTargetParameterList>(ParametersProperty); }
        }
        #endregion

        #region AssmemblyName
        /// <summary>
        /// AssemblyName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> AssemblyNameProperty =
            RegisterModelProperty<String>(c => c.AssemblyName);
        /// <summary>
        /// Gets or sets the assembly name
        /// </summary>
        public String AssemblyName
        {
            get { return this.GetProperty<String>(AssemblyNameProperty); }
            set { this.SetProperty<String>(AssemblyNameProperty, value); }
        }
        #endregion

        #region TypeName
        /// <summary>
        /// TypeName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> TypeNameProperty =
            RegisterModelProperty<String>(c => c.TypeName);
        /// <summary>
        /// Gets or sets the type name
        /// </summary>
        public String TypeName
        {
            get { return this.GetProperty<String>(TypeNameProperty); }
            set { this.SetProperty<String>(TypeNameProperty, value); }
        }
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            this.BusinessRules.AddRule(new MaxLength(AssemblyNameProperty, 50));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(RepositorySyncTarget), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(RepositorySyncTarget), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(RepositorySyncTarget), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(RepositorySyncTarget), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Temporary.ToString()));
        }
        #endregion
    }
}
