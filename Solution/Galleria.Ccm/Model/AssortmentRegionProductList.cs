﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion

#endregion

using System;
using System.Linq;
using Csla;
using Galleria.Framework.Model;
using System.Collections.Generic;
using System.Collections.Specialized;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A list of region products contained within an assortment
    /// </summary>
    [Serializable]
    public partial class AssortmentRegionProductList : ModelList<AssortmentRegionProductList, AssortmentRegionProduct>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentRegionProductList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.AssortmentCreate.ToString()));
            BusinessRules.AddRule(typeof(AssortmentRegionProductList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.AssortmentGet.ToString()));
            BusinessRules.AddRule(typeof(AssortmentRegionProductList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.AssortmentEdit.ToString()));
            BusinessRules.AddRule(typeof(AssortmentRegionProductList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.AssortmentDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A new list</returns>
        public static AssortmentRegionProductList NewAssortmentRegionProductList()
        {
            AssortmentRegionProductList item = new AssortmentRegionProductList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}