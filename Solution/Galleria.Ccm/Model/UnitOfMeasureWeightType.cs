﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-26179  : I.George
//  Created
// V8-26640 : A.Silva ~ Corrected Enum_UnitOfMeasureWeightType_Kilograms to match the other settings.
// V8-28149 : M.Pettit
//  Ensured all types have friendly names for reporting
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Denotes the available weight unit of measure types
    /// </summary>
    public enum UnitOfMeasureWeightType
    {
        Unknown = 0,
        Kilograms = 1,
        Pounds = 2
    }

    /// <summary>
    ///     UnitOfMeasureWeightType Helper Class
    /// </summary>
    public static class UnitOfMeasureWeightTypeHelper
    {
        /// <summary>
        ///     Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<UnitOfMeasureWeightType, String> Abbreviations =
            new Dictionary<UnitOfMeasureWeightType, String>
            {
                {UnitOfMeasureWeightType.Unknown, String.Empty},
                {UnitOfMeasureWeightType.Kilograms, Message.Enum_UnitOfMeasureWeightType_Kilogram_Abbrev},
                {UnitOfMeasureWeightType.Pounds, Message.Enum_UnitOfMeasureWeightType_Pounds_Abbrev},
            };

        public static readonly Dictionary<UnitOfMeasureWeightType, String> FriendlyNames =
            new Dictionary<UnitOfMeasureWeightType, String>
            {
                {UnitOfMeasureWeightType.Unknown, String.Empty},
                {UnitOfMeasureWeightType.Kilograms, Message.Enum_UnitOfMeasureWeightType_Kilograms},
                {UnitOfMeasureWeightType.Pounds, Message.Enum_UnitOfMeasureWeightType_Pounds},
            };
    }
}