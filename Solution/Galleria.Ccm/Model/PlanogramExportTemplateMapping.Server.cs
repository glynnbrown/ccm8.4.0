﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramExportTemplateMapping
    {
        #region Constructor
        private PlanogramExportTemplateMapping() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static PlanogramExportTemplateMapping Fetch(IDalContext dalContext, PlanogramExportTemplateMappingDto dto)
        {
            return DataPortal.FetchChild<PlanogramExportTemplateMapping>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramExportTemplateMappingDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<PlanogramFieldMappingType>(FieldTypeProperty, (PlanogramFieldMappingType)dto.FieldType);
            this.LoadProperty<String>(FieldProperty, dto.Field);
            this.LoadProperty<String>(ExternalFieldProperty, dto.ExternalField);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private PlanogramExportTemplateMappingDto GetDataTransferObject(PlanogramExportTemplate parent)
        {
            return new PlanogramExportTemplateMappingDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                PlanogramExportTemplateId = parent.Id,
                FieldType = (Byte)ReadProperty<PlanogramFieldMappingType>(FieldTypeProperty),
                Field = ReadProperty<String>(FieldProperty),
                ExternalField = ReadProperty<String>(ExternalFieldProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, PlanogramExportTemplateMappingDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, PlanogramExportTemplate parent)
        {
            PlanogramExportTemplateMappingDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IPlanogramExportTemplateMappingDal dal = dalContext.GetDal<IPlanogramExportTemplateMappingDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<PlanogramExportTemplateMapping>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, PlanogramExportTemplate parent)
        {
            if (this.IsSelfDirty)
            {
                using (IPlanogramExportTemplateMappingDal dal = dalContext.GetDal<IPlanogramExportTemplateMappingDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, PlanogramExportTemplate parent)
        {
            using (IPlanogramExportTemplateMappingDal dal = dalContext.GetDal<IPlanogramExportTemplateMappingDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion
    }
}
