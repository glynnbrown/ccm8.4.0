﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.2.0)
// V8-30508 : M.Shelley
//  Moved the PlanAssignmentPublishStatusType enum definition to the Galleria.Ccm.Model namespace
//  and left the PlanAssignmentPublishStatusTypeHelper class in the Galleria.Ccm.Common.Wpf project
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Resources;

namespace Galleria.Ccm.Model
{
    public enum PlanAssignmentPublishStatusType
    {
        Failed = 0,
        ProjectLinkExceptions = 1,
        Successful = 2,
        ProjectLinkNotPublished = 3, // to GFS
        PlanNotPublished = 4,
        NoPlan = 5,
        ProjectLinkNotPublishedExternally = 6,  // not marked as published as considered by external apps
        PlanogramAltered = 7,   // The planogram has been changed after publishing
    }
}
