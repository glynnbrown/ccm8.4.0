﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a product image
    /// (Child of ProductImageList)
    /// 
    /// This is a child MasterData object and so it has the following properties/actions:
    /// - This object does NOT have a DateDeleted property or DateDeleted field in its supporting database table
    /// - if its parent is deleted, it is not removed or marked as deleted (this allows for parent 'undeletes')
    /// - if it is deleted directly, it's associated record is removed from the database. 
    /// </summary>
    [Serializable]
    public partial class ProductImage : ModelObject<ProductImage>
    {
        #region Fields
        private Object productImageDataLock;
        #endregion

        #region Parent

        public ProductImageList ParentProductImageList
        {
            get { return base.Parent as ProductImageList; }
        }

        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
        }

        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }
        public static readonly ModelPropertyInfo<Int32> ProductIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductId);
        public Int32 ProductId
        {
            get { return GetProperty<Int32>(ProductIdProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> ImageIdProperty =
            RegisterModelProperty<Int32>(c => c.ImageId);
        public Int32 ImageId
        {
            get { return GetProperty<Int32>(ImageIdProperty); }
        }
        public static readonly ModelPropertyInfo<Image> ProductImageDataProperty =
            RegisterModelProperty<Image>(c => c.ProductImageData);
        public Image ProductImageData
        {
            get
            {
#if !SILVERLIGHT
                if (GetProperty<Image>(ProductImageDataProperty) == null)
                {
                    if (productImageDataLock == null) productImageDataLock = new Object();

                    lock (productImageDataLock)
                    {
                        if (GetProperty<Image>(ProductImageDataProperty) == null)
                        {
                            LoadProperty<Image>(ProductImageDataProperty, Image.GetImageById(ImageId));
                            OnPropertyChanged(ProductImageDataProperty);
                        }
                    }
                }
                return GetProperty<Image>(ProductImageDataProperty);
#else
                if (ReadProperty<Image>(ProductImageDataProperty) == null)
                {
                    this.MarkBusy();
                    DataPortal.BeginFetch<Image>((o, e) =>
                    {
                        if(e.Error != null)
                        {
                            throw e.Error;
                        }
                        LoadProperty<Image>(ProductImageDataProperty, e.Object);
                        OnPropertyChanged(ProductImageDataProperty);
                        MarkIdle();
                    });
                }
                return GetProperty<Image>(ProductImageDataProperty);
#endif

            }
        }

        public static readonly ModelPropertyInfo<ProductImageType> ImageTypeProperty =
            RegisterModelProperty<ProductImageType>(c => c.ImageType);
        public ProductImageType ImageType
        {
            get { return GetProperty<ProductImageType>(ImageTypeProperty); }
            set { SetProperty<ProductImageType>(ImageTypeProperty, value); }
        }

        public static readonly ModelPropertyInfo<ProductImageFacing> FacingTypeProperty =
            RegisterModelProperty<ProductImageFacing>(c => c.FacingType);
        public ProductImageFacing FacingType
        {
            get { return GetProperty<ProductImageFacing>(FacingTypeProperty); }
            set { SetProperty<ProductImageFacing>(FacingTypeProperty, value); }
        }

        /// <summary>
        /// The date when the item was created 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
        }

        /// <summary>
        /// The date when the item was modified 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ProductImage), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ProductCreate.ToString()));
            BusinessRules.AddRule(typeof(ProductImage), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ProductGet.ToString()));
            BusinessRules.AddRule(typeof(ProductImage), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ProductEdit.ToString()));
            BusinessRules.AddRule(typeof(ProductImage), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ProductDelete.ToString()));
        }
        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

        }

        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByProductIdImageTypeFacingTypeCompressionId
        /// </summary>
        [Serializable]
        public class FetchByProductIdImageTypeFacingTypeCompressionIdCriteria : CriteriaBase<FetchByProductIdImageTypeFacingTypeCompressionIdCriteria>
        {
            public static readonly PropertyInfo<Int32> ProductIdProperty =
                RegisterProperty<Int32>(c => c.ProductId);
            public Int32 ProductId
            {
                get { return ReadProperty<Int32>(ProductIdProperty); }
            }


            public static readonly PropertyInfo<ProductImageType> ImageTypeProperty =
                RegisterProperty<ProductImageType>(c => c.ImageType);
            public ProductImageType ImageType
            {
                get { return ReadProperty<ProductImageType>(ImageTypeProperty); }
            }


            public static readonly PropertyInfo<ProductImageFacing> FacingTypeProperty =
                RegisterProperty<ProductImageFacing>(c => c.FacingType);
            public ProductImageFacing FacingType
            {
                get { return ReadProperty<ProductImageFacing>(FacingTypeProperty); }
            }

            public static readonly PropertyInfo<Int32> CompressionIdProperty =
                RegisterProperty<Int32>(c => c.CompressionId);
            public Int32 CompressionId
            {
                get { return ReadProperty<Int32>(CompressionIdProperty); }
            }

            public FetchByProductIdImageTypeFacingTypeCompressionIdCriteria
                (Int32 productId, ProductImageType imageType, ProductImageFacing facingType, Int32 compressionId)
            {
                LoadProperty<Int32>(ProductIdProperty, productId);
                LoadProperty<ProductImageType>(ImageTypeProperty, imageType);
                LoadProperty<ProductImageFacing>(FacingTypeProperty, facingType);
                LoadProperty<Int32>(CompressionIdProperty, compressionId);
            }
        }

        #endregion

        #region Factory Methods

        public static ProductImage NewProductImage(Image image, ProductImageImageFacingType imageFacing, Int32 productID)
        {
            ProductImage item = new ProductImage();
            item.Create(image, imageFacing, productID);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        private void Create(Image image, ProductImageImageFacingType imageFacing, Int32 productID)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(ProductIdProperty, productID);
            this.LoadProperty<Int32>(EntityIdProperty, image.EntityId);
            this.LoadProperty<Int32>(ImageIdProperty, image.Id);
            this.LoadProperty<ProductImageType>(ImageTypeProperty, imageFacing.ImageType);
            this.LoadProperty<ProductImageFacing>(FacingTypeProperty, imageFacing.FacingType);
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            if (image.IsNew) this.LoadProperty<Image>(ProductImageDataProperty, image);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion
    }
}
