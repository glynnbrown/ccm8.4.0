﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : Luong
//   Created (Copied From GFS)
#endregion

#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing an Entity
    /// (Root object)
    /// </summary>
    [Serializable]
    public sealed partial class EventLogNameList : ModelList<EventLogNameList, EventLogName>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(EventLogNameList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(EventLogNameList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.EventLogGet.ToString()));
            BusinessRules.AddRule(typeof(EventLogNameList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(EventLogNameList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        public static EventLogNameList NewList()
        {
            EventLogNameList item = new EventLogNameList();
            item.Create();
            return item;
        }
        #endregion
    }
}
