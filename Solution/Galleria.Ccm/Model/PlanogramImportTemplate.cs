﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27804 : L.Ineson
//  Created
// V8-24779 : L.Luong
//  Added EntityId, RowVersion and Date properties
// V8-28219 : N.Haywood
//  changed PlanogramImportFileType.Spaceman to SpacemanV9
// V8-28420 : D.Pleasance
//  Added ApplySpacemanV9Mappings
#endregion
#region Version History: (CCM 8.0.1)
// V8-28622 \ V8-29132 : D.Pleasance
//  Added PerformanceMetrics
//  Amended so that ApplySpacemanV9Mappings is called on the NewPlanogramImportTemplate method.
// V8-28852 : D.Pleasance
//  Amended default metric SalesVolumeso that SpecialType set to PlanogramMetricSpecialType.RegularSalesUnits.
#endregion
#region Version History: (CCM 8.03)
// V8-29220 : L.Ineson
//Removed date deleted
#endregion
#region Version History: (CCM 8.1.0)
//V8-29830 : M.Pettit
//  Added CasePackUnits to mapping
//V8-29678 : D.Pleasance
//  Amended ApplySpacemanV9Mappings to apply additional default mappings.
//V8-30215 : M.Brumby
//  Added use of metric aggregation types
#endregion
#region Version History: CCM820

// V8-30756 : A.Silva
//  Added ProSpace and Apollo extensions.
// V8-30725 : M.Brumby
//  PCR01417 Import from Apollo
// V8-30754 : A.Silva
//  Amended name of ProSpace extension in the file selection window.
//  Added GetTypeFromFileName() to get the type of file from the file name (still based on the extension).
// V8-31073 : A.Silva
//  Added Default Metric mappings for ProSpace.
//  Added reinitialization of the mappings if the file type changes.
//  Added Lookup dictionaries for max version and default name for import file types.
// V8-31189 : A.Silva
//  Amended template names for Apollo and JDA Space Planning imports.

#endregion

#region Version History: (CCM 8.30)
// V8-32360 : M.Brumby
//  [PCR01564] Auto split bays by a fixed value
// V8-32480 : M.Brumby
//  [PCR01564] Make split by fixed with default if there is no split option set
// V8-32725 : M.Pettit
//  Added SaveAs method
//  SaveAsFile method was not correctly clearing the DalFacotryname when saving a db item to a file
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Enums;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Planograms.External;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model containing settings on how a planogram should be imported from an external file type.
    /// </summary>
    [DefaultNewMethod("NewPlanogramImportTemplate", "FileType", "FileVersion")]
    [Serializable]
    public sealed partial class PlanogramImportTemplate : ModelObject<PlanogramImportTemplate>, IDisposable
    {
        #region Nested Classes
        /// <summary>
        /// Denotes the different dal factory types that may be used
        /// for this model.
        /// </summary>
        public enum PlanogramImportTemplateDalType
        {
            Unknown = 0,
            FileSystem = 1
        }
        #endregion

        #region Public Static

        /// <summary>
        /// Returns the file extension to use for this model.
        /// </summary>
        public static String FileExtension
        {
            get { return Constants.PlanogramImportTemplateFileExtension; }
        }

        #endregion

        #region Properties

        #region DalFactoryType

        /// <summary>
        /// DalFactoryType property definition
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramImportTemplateDalType> DalFactoryTypeProperty =
            RegisterModelProperty<PlanogramImportTemplateDalType>(c => c.DalFactoryType);

        /// <summary>
        /// Returns the dal factory type
        /// </summary>
        private PlanogramImportTemplateDalType DalFactoryType
        {
            get { return this.GetProperty<PlanogramImportTemplateDalType>(DalFactoryTypeProperty); }
        }

        #endregion

        #region DalFactoryName
        /// <summary>
        /// DalFactoryName property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> DalFactoryNameProperty =
            RegisterModelProperty<String>(c => c.DalFactoryName);
        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        protected override String DalFactoryName
        {
            get { return this.GetProperty<String>(DalFactoryNameProperty); }
        }
        #endregion

        #region IsReadOnly

        private static readonly ModelPropertyInfo<Boolean> IsReadOnlyProperty =
             RegisterModelProperty<Boolean>(c => c.IsReadOnly);

        /// <summary>
        /// Returns true if this model has been loaded as readonly.
        /// </summary>
        public Boolean IsReadOnly
        {
            get { return this.GetProperty<Boolean>(IsReadOnlyProperty); }
        }

        #endregion

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> IdProperty =
            RegisterModelProperty<Object>(c => c.Id);
        /// <summary>
        /// Returns the unique identifier for this item.
        /// </summary>
        public Object Id
        {
            get { return this.GetProperty<Object>(IdProperty); }
        }
        #endregion

        #region RowVersion

        /// <summary>
        ///     Metadata for the <see cref="RowVersion" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);

        /// <summary>
        ///     Gets the row version timestamp.
        /// </summary>
        private RowVersion RowVersion
        {
            get { return GetProperty(RowVersionProperty); }
        }

        #endregion

        #region EntityId

        /// <summary>
        ///     Metadata for the <see cref="EntityId" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<int>(c => c.EntityId);

        /// <summary>
        ///     Gets or sets the unique Id value for the <see cref="Entity" /> model this instance belongs to.
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty(EntityIdProperty); }
            set { SetProperty(EntityIdProperty, value); }
        }

        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Gets or sets the workflow name
        /// </summary>
        public String Name
        {
            get { return this.GetProperty<String>(NameProperty); }
            set { this.SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region FileType
        /// <summary>
        /// FileType property definition.
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramImportFileType> FileTypeProperty =
            RegisterModelProperty<PlanogramImportFileType>(c => c.FileType);

        /// <summary>
        /// Gets/Sets the type of file this template is for.
        /// </summary>
        public PlanogramImportFileType FileType
        {
            get { return this.GetProperty<PlanogramImportFileType>(FileTypeProperty); }
            set
            {
                PlanogramImportFileType fileType = FileType;
                this.SetProperty<PlanogramImportFileType>( FileTypeProperty, value);
                if (!Equals(fileType, value))
                {
                    InitializeMappings(value);
                }
            }
        }
        #endregion

        #region FileVersion
        /// <summary>
        /// FileVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> FileVersionProperty =
            RegisterModelProperty<String>(c => c.FileVersion);

        /// <summary>
        /// Gets/Sets the file version that this template is for.
        /// </summary>
        public String FileVersion
        {
            get { return this.GetProperty<String>(FileVersionProperty); }
            set { SetProperty<String>(FileVersionProperty, value); }
        }
        #endregion

        #region Mappings
        /// <summary>
        /// Mappings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramImportTemplateMappingList> MappingsProperty =
            RegisterModelProperty<PlanogramImportTemplateMappingList>(c => c.Mappings);
        /// <summary>
        /// Returns the list of mappings for this template.
        /// </summary>
        public PlanogramImportTemplateMappingList Mappings
        {
            get { return this.GetProperty<PlanogramImportTemplateMappingList>(MappingsProperty); }
        }
        #endregion

        #region PerformanceMetrics
        /// <summary>
        /// PerformanceMetrics property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramImportTemplatePerformanceMetricList> PerformanceMetricsProperty =
            RegisterModelProperty<PlanogramImportTemplatePerformanceMetricList>(c => c.PerformanceMetrics);
        /// <summary>
        /// Returns the list of performance metrics for this template.
        /// </summary>
        public PlanogramImportTemplatePerformanceMetricList PerformanceMetrics
        {
            get { return this.GetProperty<PlanogramImportTemplatePerformanceMetricList>(PerformanceMetricsProperty); }
        }
        #endregion

        #region DateCreated

        /// <summary>
        ///     Metadata for the <see cref="DateCreated" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        ///     Gets or sets the date this instance was created originally.
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty(DateCreatedProperty); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        ///     Metadata for the <see cref="DateLastModified" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        ///     Gets or sets the last date this instance was modified (and persisted).
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty(DateLastModifiedProperty); }
        }

        #endregion

        #region Advanced Settings
        #region AllowAdvancedBaySplitting
        /// <summary>
        /// AllowAdvancedBaySplitting property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> AllowAdvancedBaySplittingProperty =
            RegisterModelProperty<Boolean>(c => c.AllowAdvancedBaySplitting);
        /// <summary>
        /// Gets or sets the AllowAdvancedBaySplitting value
        /// </summary>
        public Boolean AllowAdvancedBaySplitting
        {
            get { return this.GetProperty<Boolean>(AllowAdvancedBaySplittingProperty); }
            set
            {
                this.SetProperty<Boolean>(AllowAdvancedBaySplittingProperty, value);
                UpdateHelperProperties();
            }
        }
        #endregion

        #region CanSplitComponents
        /// <summary>
        /// CanSplitComponents property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> CanSplitComponentsProperty =
            RegisterModelProperty<Boolean>(c => c.CanSplitComponents);
        /// <summary>
        /// Gets or sets the CanSplitComponents value
        /// </summary>
        public Boolean CanSplitComponents
        {
            get { return this.GetProperty<Boolean>(CanSplitComponentsProperty); }
            set
            {
                this.SetProperty<Boolean>(CanSplitComponentsProperty, value);
                UpdateHelperProperties();
            }
        }
        #endregion

        #region SplitBaysByComponentStart
        /// <summary>
        /// SplitBaysByComponentStart property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> SplitBaysByComponentStartProperty =
            RegisterModelProperty<Boolean>(c => c.SplitBaysByComponentStart);
        /// <summary>
        /// Gets or sets the SplitBaysByComponentStart value
        /// </summary>
        public Boolean SplitBaysByComponentStart
        {
            get { return this.GetProperty<Boolean>(SplitBaysByComponentStartProperty); }
            set
            {
                this.SetProperty<Boolean>(SplitBaysByComponentStartProperty, value);
                UpdateHelperProperties();
            }
        }
        #endregion

        #region SplitBaysByComponentEnd
        /// <summary>
        /// SplitBaysByComponentEnd property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> SplitBaysByComponentEndProperty =
            RegisterModelProperty<Boolean>(c => c.SplitBaysByComponentEnd);
        /// <summary>
        /// Gets or sets the SplitBaysByComponentEnd value
        /// </summary>
        public Boolean SplitBaysByComponentEnd
        {
            get { return this.GetProperty<Boolean>(SplitBaysByComponentEndProperty); }
            set 
            { 
                this.SetProperty<Boolean>(SplitBaysByComponentEndProperty, value);
                UpdateHelperProperties();
            }
        }
        #endregion

        #region SplitByFixedSize
        /// <summary>
        /// SplitByFixedSize property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> SplitByFixedSizeProperty =
            RegisterModelProperty<Boolean>(c => c.SplitByFixedSize);
        /// <summary>
        /// Gets or sets the SplitByFixedSize value
        /// </summary>
        public Boolean SplitByFixedSize
        {
            get { return this.GetProperty<Boolean>(SplitByFixedSizeProperty); }
            set
            {
                this.SetProperty<Boolean>(SplitByFixedSizeProperty, value);
                UpdateHelperProperties();
            }
        }
        #endregion

        #region SplitByRecurringPattern
        /// <summary>
        /// SplitByRecurringPattern property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> SplitByRecurringPatternProperty =
            RegisterModelProperty<Boolean>(c => c.SplitByRecurringPattern);
        /// <summary>
        /// Gets or sets the SplitByRecurringPattern value
        /// </summary>
        public Boolean SplitByRecurringPattern
        {
            get { return this.GetProperty<Boolean>(SplitByRecurringPatternProperty); }
            set 
            { 
                this.SetProperty<Boolean>(SplitByRecurringPatternProperty, value);
                UpdateHelperProperties();
            }
        }
        #endregion

        #region SplitByBayCount
        /// <summary>
        /// SplitByBayCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> SplitByBayCountProperty =
            RegisterModelProperty<Boolean>(c => c.SplitByBayCount);
        /// <summary>
        /// Gets or sets the SplitByBayCount value
        /// </summary>
        public Boolean SplitByBayCount
        {
            get { return this.GetProperty<Boolean>(SplitByBayCountProperty); }
            set 
            { 
                this.SetProperty<Boolean>(SplitByBayCountProperty, value);
                UpdateHelperProperties();
            }
        }
        #endregion

        #region SplitFixedSize
        /// <summary>
        /// SplitFixedSize property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Double> SplitFixedSizeProperty =
            RegisterModelProperty<Double>(c => c.SplitFixedSize);
        /// <summary>
        /// Gets or sets the SplitFixedSize value
        /// </summary>
        public Double SplitFixedSize
        {
            get { return this.GetProperty<Double>(SplitFixedSizeProperty); }
            set { this.SetProperty<Double>(SplitFixedSizeProperty, value); }
        }
        #endregion

        #region SplitRecurringPattern
        /// <summary>
        /// SplitRecurringPattern property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> SplitRecurringPatternProperty =
            RegisterModelProperty<String>(c => c.SplitRecurringPattern);
        /// <summary>
        /// Gets or sets the SplitRecurringPattern value
        /// </summary>
        public String SplitRecurringPattern
        {            
            get { return this.GetProperty<String>(SplitRecurringPatternProperty); }
            set { this.SetProperty<String>(SplitRecurringPatternProperty, value); }
        }
        #endregion

        #region SplitBayCount
        /// <summary>
        /// SplitBayCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> SplitBayCountProperty =
            RegisterModelProperty<Int32>(c => c.SplitBayCount);
        /// <summary>
        /// Gets or sets the SplitBayCount value
        /// </summary>
        public Int32 SplitBayCount
        {
            get { return this.GetProperty<Int32>(SplitBayCountProperty); }
            set { this.SetProperty<Int32>(SplitBayCountProperty, value); }
        }
        #endregion
				
        #endregion Advanced Settings
        #endregion

        #region Helper Properties
        public Boolean EnableSplitComponents
        {
            get
            {
                return AllowAdvancedBaySplitting;
            }
        }
        public Boolean EnableSplitBayByComponent
        {
            get
            {
                return AllowAdvancedBaySplitting && !SplitByBayCount;
            }
        }
        public Boolean EnableSplitBayByFixedSize
        {
            get
            {
                return AllowAdvancedBaySplitting && !SplitByBayCount && !SplitByRecurringPattern;
            }
        }
        public Boolean EnableSplitByRecurringPattern
        {
            get
            {
                return AllowAdvancedBaySplitting && !SplitByBayCount && !SplitByFixedSize;
            }
        }
        public Boolean EnableSplitByBayCount
        {
            get
            {
                return AllowAdvancedBaySplitting && !SplitByRecurringPattern && !SplitByFixedSize;
            }
        }

        private void UpdateHelperProperties()
        {
            OnPropertyChanged("EnableSplitComponents");
            OnPropertyChanged("EnableSplitBayByComponent");
            OnPropertyChanged("EnableSplitBayByFixedSize");
            OnPropertyChanged("EnableSplitByRecurringPattern");
            OnPropertyChanged("EnableSplitByBayCount");
        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository


        /// <summary>
        /// Returns a dictionary of the current role permissions allocated to the user for this object type.
        /// </summary>
        /// <returns></returns>
        public static IDictionary<AuthorizationActions, Boolean> GetUserPermissions()
        {
            return new Dictionary<AuthorizationActions, Boolean>()
            {
                {AuthorizationActions.CreateObject, ApplicationContext.User.IsInRole(DomainPermission.PlanogramImportTemplateCreate.ToString())},
                {AuthorizationActions.GetObject, ApplicationContext.User.IsInRole(DomainPermission.PlanogramImportTemplateGet.ToString())},
                {AuthorizationActions.EditObject, ApplicationContext.User.IsInRole(DomainPermission.PlanogramImportTemplateEdit.ToString())},
                {AuthorizationActions.DeleteObject, ApplicationContext.User.IsInRole(DomainPermission.PlanogramImportTemplateDelete.ToString())},
            };
        }

        #endregion

        #region Criteria

        #region FetchByIdCriteria

        /// <summary>
        /// Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region DalType
            /// <summary>
            /// DalType property definition
            /// </summary>
            public static readonly PropertyInfo<PlanogramImportTemplateDalType> DalTypeProperty =
                RegisterProperty<PlanogramImportTemplateDalType>(c => c.DalType);
            /// <summary>
            /// Returns the  dal type
            /// </summary>
            public PlanogramImportTemplateDalType DalType
            {
                get { return this.ReadProperty<PlanogramImportTemplateDalType>(DalTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #region AsReadOnly

            /// <summary>
            /// AsReadOnly property definition
            /// </summary>
            public static readonly PropertyInfo<Boolean> AsReadOnlyProperty =
                RegisterProperty<Boolean>(c => c.AsReadOnly);
            /// <summary>
            /// If true the model will be loaded as readonly.
            /// </summary>
            public Boolean AsReadOnly
            {
                get { return this.ReadProperty<Boolean>(AsReadOnlyProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(PlanogramImportTemplateDalType type, Object id)
            {
                this.LoadProperty<PlanogramImportTemplateDalType>(DalTypeProperty, type);
                this.LoadProperty<Object>(IdProperty, id);
                this.LoadProperty<Boolean>(AsReadOnlyProperty, false);
            }

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(PlanogramImportTemplateDalType type, Object id, Boolean asReadOnly)
            {
                this.LoadProperty<PlanogramImportTemplateDalType>(DalTypeProperty, type);
                this.LoadProperty<Object>(IdProperty, id);
                this.LoadProperty<Boolean>(AsReadOnlyProperty, asReadOnly);
            }
            #endregion
        }

        #endregion

        #region FetchByEntityIdNameCriteria

        [Serializable]
        public class FetchByEntityIdNameCriteria : CriteriaBase<FetchByEntityIdNameCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static readonly PropertyInfo<String> NameProperty =
                RegisterProperty<String>(c => c.Name);
            public String Name
            {
                get { return ReadProperty<String>(NameProperty); }
            }

            public FetchByEntityIdNameCriteria(Int32 entityId, String name)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<String>(NameProperty, name);
            }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
            //BusinessRules.AddRule(new Required(FileVersionProperty));
            BusinessRules.AddRule(new MaxLength(FileVersionProperty, 50));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="fileType"></param>
        /// <param name="fileVersion"></param>
        public static PlanogramImportTemplate NewPlanogramImportTemplate(PlanogramImportFileType fileType, String fileVersion)
        {
            var item = new PlanogramImportTemplate();
            item.Create(fileType, fileVersion);
            item.InitializeMappings(fileType);

            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramImportTemplate NewPlanogramImportTemplate(Int32 entityId, PlanogramImportFileType fileType)
        {
            return NewPlanogramImportTemplate(entityId, fileType, PlanogramImportFileTypeHelper.MaxSupportedVersion[fileType]);
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramImportTemplate NewPlanogramImportTemplate(Int32 entityId, PlanogramImportFileType fileType, String fileVersion)
        {
            var item = new PlanogramImportTemplate();
            item.Create(entityId, fileType, fileVersion);
            item.InitializeMappings(fileType);

            return item;
        }
        
        /// <summary>
        /// Locks a model so that it cannot be
        /// edited or opened by another process
        /// </summary>
        internal static void LockPlanogramImportTemplateByFileName(String fileName)
        {
            DataPortal.Execute<LockPlanogramImportTemplateCommand>(new LockPlanogramImportTemplateCommand(PlanogramImportTemplateDalType.FileSystem, fileName));
        }

        /// <summary>
        /// Unlocks a model
        /// </summary>
        public static void UnlockPlanogramImportTemplateByFileName(String fileName)
        {
            DataPortal.Execute<UnlockPlanogramImportTemplateCommand>(new UnlockPlanogramImportTemplateCommand(PlanogramImportTemplateDalType.FileSystem, fileName));
        }


        #endregion

        #region Data Access

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(PlanogramImportFileType fileType, String fileVersion)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<PlanogramImportFileType>(FileTypeProperty, fileType);
            this.LoadProperty<String>(FileVersionProperty, fileVersion);
            this.LoadProperty<PlanogramImportTemplateMappingList>(MappingsProperty, PlanogramImportTemplateMappingList.NewPlanogramImportTemplateMappingList());
            this.LoadProperty<PlanogramImportTemplatePerformanceMetricList>(PerformanceMetricsProperty, PlanogramImportTemplatePerformanceMetricList.NewPlanogramImportTemplatePerformanceMetricList());
            this.LoadProperty(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty(DateLastModifiedProperty, DateTime.UtcNow);
            this.LoadProperty(SplitByFixedSizeProperty, true);
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(Int32 entityId, PlanogramImportFileType fileType, String fileVersion)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.LoadProperty<PlanogramImportFileType>(FileTypeProperty, fileType);
            this.LoadProperty<String>(FileVersionProperty, fileVersion);
            this.LoadProperty<PlanogramImportTemplateMappingList>(MappingsProperty, PlanogramImportTemplateMappingList.NewPlanogramImportTemplateMappingList());
            this.LoadProperty<PlanogramImportTemplatePerformanceMetricList>(PerformanceMetricsProperty, PlanogramImportTemplatePerformanceMetricList.NewPlanogramImportTemplatePerformanceMetricList());
            this.LoadProperty(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty(DateLastModifiedProperty, DateTime.UtcNow);
            this.LoadProperty(SplitByFixedSizeProperty, true);
            base.Create();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<PlanogramImportTemplate>(oldId, newId);
        }

        /// <summary>
        /// ToString override.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Provide a SaveAs method to allow saving to the repository.
        /// This resets DalFactoryType and DalFactoryName to allow saving saving to the repository,
        /// otherwise a PlanogramExportTemplate instance loaded from a file tries to save back to a file.
        /// </summary>
        /// <returns></returns>
        public PlanogramImportTemplate SaveAs()
        {
            this.LoadProperty<PlanogramImportTemplateDalType>(DalFactoryTypeProperty, PlanogramImportTemplateDalType.Unknown);
            this.LoadProperty<String>(DalFactoryNameProperty, DalContainer.DalName);

            return this.Save();
        }

        /// <summary>
        /// Saves this to a new file.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public PlanogramImportTemplate SaveAsFile(String path)
        {
            PlanogramImportTemplate returnValue = null;

            String fileName = path;

            //make sure the file extension is correct.
            fileName = Path.ChangeExtension(path, FileExtension);

            //if this model is not new and was fecthed from another file
            // take a note of the old file name.
            String oldFileName = null;
            if ((!IsNew) && (this.Id as String != fileName))
            {
                if (DalFactoryType == PlanogramImportTemplateDalType.FileSystem)
                {
                    oldFileName = this.Id as String;
                }
                MarkGraphAsNew();
            }

            LoadProperty<PlanogramImportTemplateDalType>(DalFactoryTypeProperty, PlanogramImportTemplateDalType.FileSystem);
            LoadProperty<String>(DalFactoryNameProperty, "");
            LoadProperty<Object>(IdProperty, fileName);
            this.Name = Path.GetFileNameWithoutExtension(fileName);
            returnValue = Save();

            //unlock the old file.
            if (!String.IsNullOrEmpty(oldFileName))
            {
                UnlockPlanogramImportTemplateByFileName(oldFileName);
            }

            if (System.IO.File.Exists(fileName))
            {
                UnlockPlanogramImportTemplateByFileName(fileName);
            }

            //V8-25873 : Make sure id is the file name for file system, dal is returning 1
            LoadProperty<Object>(IdProperty, fileName);
            return returnValue;
        }

        /// <summary>
        /// Updates this mapping list for the list of mappable ccm fields
        /// </summary>
        public void UpdateFromCcmFieldList()
        {
            var ccmFieldList = PlanogramImportTemplateFieldInfoList.NewPlanogramImportTemplateFieldInfoList(null, null);

            if (this.Mappings.Count == 0)
            {
                //just load in all fields
                foreach (PlanogramImportTemplateFieldInfo field in ccmFieldList)
                {
                    this.Mappings.Add(PlanogramImportTemplateMapping.NewPlanogramImportTemplateMapping(field.Field, field.FieldType));
                }
            }
            else
            {
                //need to update existing list
                List<String> ccmFields = ccmFieldList.Select(c=> c.Field).ToList();

                //remove any mappings for fields which no longer exist
                this.Mappings.RemoveList(this.Mappings.Where(m =>!ccmFields.Contains(m.Field)).ToList());

                //add in any new ones
                ccmFields = ccmFields.Except(this.Mappings.Select(m => m.Field)).ToList();

                foreach (PlanogramImportTemplateFieldInfo field in ccmFieldList)
                {
                    if (ccmFields.Contains(field.Field))
                    {
                        this.Mappings.Add(PlanogramImportTemplateMapping.NewPlanogramImportTemplateMapping(field.Field, field.FieldType));
                    }
                }
            }
        }

        /// <summary>
        /// Returns a list of planogram field mappings from this template.
        /// </summary>
        public List<PlanogramFieldMapping> GetPlanogramFieldMappings(Boolean isImport = true)
        {
            List<PlanogramFieldMapping> planMappings = new List<PlanogramFieldMapping>();

            foreach (PlanogramImportTemplateMapping mapping in this.Mappings)
            {
                if (!String.IsNullOrEmpty(mapping.ExternalField))
                {

                    if (isImport)
                    {
                        planMappings.Add(PlanogramFieldMapping.NewPlanogramFieldMapping(mapping.FieldType, mapping.Field, mapping.ExternalField));
                    }
                    else
                    {
                        //flip
                        planMappings.Add(PlanogramFieldMapping.NewPlanogramFieldMapping(mapping.FieldType, mapping.ExternalField, mapping.Field));
                    }
                }
            }

            return planMappings;
        }

        /// <summary>
        /// Returns a list of planogram metric mappings from this template.
        /// </summary>
        public List<PlanogramMetricMapping> GetPlanogramMetricMappings()
        {
            List<PlanogramMetricMapping> planMappings = new List<PlanogramMetricMapping>();

            foreach (PlanogramImportTemplatePerformanceMetric mapping in this.PerformanceMetrics)
            {
                if (!String.IsNullOrEmpty(mapping.ExternalField))
                {
                    planMappings.Add(PlanogramMetricMapping.NewPlanogramMetricMapping(mapping.Name, mapping.Description, mapping.Direction, 
                                    mapping.SpecialType,mapping.MetricType, mapping.AggregationType, mapping.MetricId, mapping.ExternalField));
                }
            }

            return planMappings;
        }

        private void InitializeMappings(PlanogramImportFileType fileType)
        {
            if (Mappings.Count > 0) Mappings.Clear();
            if (PerformanceMetrics.Any()) PerformanceMetrics.Clear();
            UpdateFromCcmFieldList();

            switch (fileType)
            {
                case PlanogramImportFileType.SpacemanV9:
                    ApplySpacemanV9Mappings();
                    break;
                case PlanogramImportFileType.ProSpace:
                    ApplySpacePlanningMappings();
                    break;
                case PlanogramImportFileType.Apollo:
                    ApplyApolloMappings();
                    break;
                default:
                    Debug.Fail(String.Format("Unknown PlanogramImportFileType ({0}) when calling InitializeMappings", fileType));
                    break;
            }
        }

        #region Default Spaceman V9 mappings

        private void ApplySpacemanV9Mappings()
        {
            IEnumerable<PlanogramImportTemplateMapping> planogramMappings = Mappings.Where(m => m.FieldType == PlanogramFieldMappingType.Planogram).ToList();
            foreach (Tuple<String, String> fieldMapping in EnumerateSpacemanV9DefaultMappingsForPlanogram())
            {
                PlanogramImportTemplateMapping match = planogramMappings.FirstOrDefault(m => m.Field == fieldMapping.Item1);
                if (match != null) match.ExternalField = fieldMapping.Item2;
            }

            planogramMappings = Mappings.Where(m => m.FieldType == PlanogramFieldMappingType.Fixture).ToList();
            foreach (Tuple<String, String> fieldMapping in EnumerateSpacemanV9DefaultMappingsForFixture())
            {
                PlanogramImportTemplateMapping match = planogramMappings.FirstOrDefault(m => m.Field == fieldMapping.Item1);
                if (match != null) match.ExternalField = fieldMapping.Item2;
            }

            planogramMappings = Mappings.Where(m => m.FieldType == PlanogramFieldMappingType.Component).ToList();
            foreach (Tuple<String, String> fieldMapping in EnumerateSpacemanV9DefaultMappingsForComponent())
            {
                PlanogramImportTemplateMapping match = planogramMappings.FirstOrDefault(m => m.Field == fieldMapping.Item1);
                if (match != null) match.ExternalField = fieldMapping.Item2;
            }

            planogramMappings = Mappings.Where(m => m.FieldType == PlanogramFieldMappingType.Product).ToList();
            foreach (Tuple<String, String> fieldMapping in EnumerateSpacemanV9DefaultMappingsForProduct())
            {
                PlanogramImportTemplateMapping match = planogramMappings.FirstOrDefault(m => m.Field == fieldMapping.Item1);
                if (match != null) match.ExternalField = fieldMapping.Item2;
            }

            #region Performance Metrics
            PerformanceMetrics.Add(PlanogramImportTemplatePerformanceMetric.NewPlanogramImportTemplatePerformanceMetric(Message.PlanogramPerformance_DefaultMetric1_SalesValue, Message.PlanogramPerformance_DefaultMetric1_RegularSalesValue, SpacemanFieldHelper.ProductSales, 1));
            PerformanceMetrics[0].MetricType = MetricType.Decimal;
            PerformanceMetrics.Add(PlanogramImportTemplatePerformanceMetric.NewPlanogramImportTemplatePerformanceMetric(Message.PlanogramPerformance_DefaultMetric2_SalesVolume, Message.PlanogramPerformance_DefaultMetric2_RegularSalesVolume, SpacemanFieldHelper.ProductUnitMovement, 2));
            PerformanceMetrics[1].MetricType = MetricType.Integer;
            PerformanceMetrics[1].SpecialType = MetricSpecialType.RegularSalesUnits;
            PerformanceMetrics.Add(PlanogramImportTemplatePerformanceMetric.NewPlanogramImportTemplatePerformanceMetric(Message.PlanogramPerformance_DefaultMetric3_SalesMargin, Message.PlanogramPerformance_DefaultMetric3_RegularSalesMargin, SpacemanFieldHelper.ProductUnitProfit, 3));
            PerformanceMetrics[2].MetricType = MetricType.Decimal;
            #endregion
        }

        private static IEnumerable<Tuple<String, String>> EnumerateSpacemanV9DefaultMappingsForPlanogram()
        {
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text1Property.Name),
                                          SpacemanFieldHelper.SectionString1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text2Property.Name),
                                          SpacemanFieldHelper.SectionString2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text3Property.Name),
                                          SpacemanFieldHelper.SectionString3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text4Property.Name),
                                          SpacemanFieldHelper.SectionString4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text5Property.Name),
                                          SpacemanFieldHelper.SectionString5);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value1Property.Name),
                                          SpacemanFieldHelper.SectionDouble1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value2Property.Name),
                                          SpacemanFieldHelper.SectionDouble2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value3Property.Name),
                                          SpacemanFieldHelper.SectionDouble3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value4Property.Name),
                                          SpacemanFieldHelper.SectionDouble4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value5Property.Name),
                                          SpacemanFieldHelper.SectionDouble5);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value6Property.Name),
                                          SpacemanFieldHelper.SectionInteger1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value7Property.Name),
                                          SpacemanFieldHelper.SectionInteger2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value8Property.Name),
                                          SpacemanFieldHelper.SectionInteger3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value9Property.Name),
                                          SpacemanFieldHelper.SectionInteger4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value10Property.Name),
                                          SpacemanFieldHelper.SectionInteger5);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value11Property.Name),
                                          SpacemanFieldHelper.SectionLong1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value12Property.Name),
                                          SpacemanFieldHelper.SectionLong2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value13Property.Name),
                                          SpacemanFieldHelper.SectionLong3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value14Property.Name),
                                          SpacemanFieldHelper.SectionLong4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value15Property.Name),
                                          SpacemanFieldHelper.SectionLong5);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Date1Property.Name),
                                          SpacemanFieldHelper.SectionDate);
        }

        private static IEnumerable<Tuple<String, String>> EnumerateSpacemanV9DefaultMappingsForFixture()
        {
            yield return
                new Tuple<String, String>(PlanogramFixture.NameProperty.Name,
                                          SpacemanFieldHelper.FixelName);
        }

        private static IEnumerable<Tuple<String, String>> EnumerateSpacemanV9DefaultMappingsForComponent()
        {
            yield return
                new Tuple<String, String>(PlanogramComponent.NameProperty.Name,
                                          SpacemanFieldHelper.FixelName);
            yield return
                new Tuple<String, String>(PlanogramComponent.ManufacturerProperty.Name,
                                          SpacemanFieldHelper.FixelManufacturer);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text1Property.Name),
                                          SpacemanFieldHelper.FixelString1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text2Property.Name),
                                          SpacemanFieldHelper.FixelString2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text3Property.Name),
                                          SpacemanFieldHelper.FixelString3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text4Property.Name),
                                          SpacemanFieldHelper.FixelString4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text5Property.Name),
                                          SpacemanFieldHelper.FixelString5);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value1Property.Name),
                                          SpacemanFieldHelper.FixelDouble1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value2Property.Name),
                                          SpacemanFieldHelper.FixelDouble2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value3Property.Name),
                                          SpacemanFieldHelper.FixelDouble3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value4Property.Name),
                                          SpacemanFieldHelper.FixelDouble4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value5Property.Name),
                                          SpacemanFieldHelper.FixelDouble5);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value6Property.Name),
                                          SpacemanFieldHelper.FixelInteger1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value7Property.Name),
                                          SpacemanFieldHelper.FixelInteger2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value8Property.Name),
                                          SpacemanFieldHelper.FixelInteger3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value9Property.Name),
                                          SpacemanFieldHelper.FixelInteger4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value10Property.Name),
                                          SpacemanFieldHelper.FixelInteger5);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value11Property.Name)
                                          ,
                                          SpacemanFieldHelper.FixelLong1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value12Property.Name),
                                          SpacemanFieldHelper.FixelLong2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value13Property.Name),
                                          SpacemanFieldHelper.FixelLong3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value14Property.Name),
                                          SpacemanFieldHelper.FixelLong4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value15Property.Name),
                                          SpacemanFieldHelper.FixelLong5);
        }

        private static IEnumerable<Tuple<String, String>> EnumerateSpacemanV9DefaultMappingsForProduct()
        {
            yield return
                new Tuple<String, String>(PlanogramProduct.GtinProperty.Name,
                                          SpacemanFieldHelper.ProductGTIN);
            yield return
                new Tuple<String, String>(PlanogramProduct.NameProperty.Name,
                                          SpacemanFieldHelper.ProductName);
            yield return
                new Tuple<String, String>(PlanogramProduct.BrandProperty.Name,
                                          SpacemanFieldHelper.ProductBrand);
            yield return
                new Tuple<String, String>(PlanogramProduct.SubcategoryProperty.Name,
                                          SpacemanFieldHelper.ProductSubcategory);
            yield return
                new Tuple<String, String>(PlanogramProduct.ColourProperty.Name,
                                          SpacemanFieldHelper.ProductColour);
            yield return
                new Tuple<String, String>(PlanogramProduct.ManufacturerProperty.Name,
                                          SpacemanFieldHelper.ProductManufacturer);
            yield return
                new Tuple<String, String>(PlanogramProduct.SizeProperty.Name,
                                          SpacemanFieldHelper.ProductSize);
            yield return
                new Tuple<String, String>(PlanogramProduct.UnitOfMeasureProperty.Name,
                                          SpacemanFieldHelper.ProductUOM);
            yield return
                new Tuple<String, String>(PlanogramProduct.SellPriceProperty.Name,
                                          SpacemanFieldHelper.ProductPrice);
            yield return
                new Tuple<String, String>(PlanogramProduct.CostPriceProperty.Name,
                                          SpacemanFieldHelper.ProductCost);
            yield return
                new Tuple<String, String>(PlanogramProduct.CaseCostProperty.Name,
                                          SpacemanFieldHelper.ProductCaseCost);
            yield return
                new Tuple<String, String>(PlanogramProduct.TaxRateProperty.Name,
                                          SpacemanFieldHelper.ProductTax_Code);
            yield return
                new Tuple<String, String>(PlanogramProduct.CasePackUnitsProperty.Name,
                                          SpacemanFieldHelper.ProductUnitsCase);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text1Property.Name),
                                          SpacemanFieldHelper.ProductDescA);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text2Property.Name),
                                          SpacemanFieldHelper.ProductDescB);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text3Property.Name),
                                          SpacemanFieldHelper.ProductDescC);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text4Property.Name),
                                          SpacemanFieldHelper.ProductDescD);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text5Property.Name),
                                          SpacemanFieldHelper.ProductDescE);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text6Property.Name),
                                          SpacemanFieldHelper.ProductDescF);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text7Property.Name),
                                          SpacemanFieldHelper.ProductDescG);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text8Property.Name),
                                          SpacemanFieldHelper.ProductDescH);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text9Property.Name),
                                          SpacemanFieldHelper.ProductDescI);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text10Property.Name),
                                          SpacemanFieldHelper.ProductDescJ);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text11Property.Name),
                                          SpacemanFieldHelper.ProductString1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text12Property.Name),
                                          SpacemanFieldHelper.ProductString2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text13Property.Name),
                                          SpacemanFieldHelper.ProductString3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text14Property.Name),
                                          SpacemanFieldHelper.ProductString4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Text15Property.Name),
                                          SpacemanFieldHelper.ProductString5);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value1Property.Name),
                                          SpacemanFieldHelper.ProductDouble1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value2Property.Name),
                                          SpacemanFieldHelper.ProductDouble2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value3Property.Name),
                                          SpacemanFieldHelper.ProductDouble3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value4Property.Name),
                                          SpacemanFieldHelper.ProductDouble4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value5Property.Name),
                                          SpacemanFieldHelper.ProductDouble5);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value6Property.Name),
                                          SpacemanFieldHelper.ProductInteger1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value7Property.Name),
                                          SpacemanFieldHelper.ProductInteger2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value8Property.Name),
                                          SpacemanFieldHelper.ProductInteger3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value9Property.Name),
                                          SpacemanFieldHelper.ProductInteger4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value10Property.Name),
                                          SpacemanFieldHelper.ProductInteger5);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value11Property.Name),
                                          SpacemanFieldHelper.ProductLong1);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value12Property.Name),
                                          SpacemanFieldHelper.ProductLong2);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value13Property.Name),
                                          SpacemanFieldHelper.ProductLong3);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value14Property.Name),
                                          SpacemanFieldHelper.ProductLong4);
            yield return
                new Tuple<String, String>(FormatCustomAttributePropertyName(CustomAttributeData.Value15Property.Name),
                                          SpacemanFieldHelper.ProductLong5);

        }

        #endregion

        private void ApplySpacePlanningMappings()
        {
            foreach (PlanogramFieldMappingType value in Enum.GetValues(typeof (PlanogramFieldMappingType)))
            {
                IEnumerable<PlanogramImportTemplateMapping> planogramMappings = Mappings.Where(m => m.FieldType == value).ToList();
                foreach (Tuple<String, String> fieldMapping in ProSpaceImportHelper.EnumerateDefaultMappings(value))
                {
                    PlanogramImportTemplateMapping match = planogramMappings.FirstOrDefault(m => m.Field == fieldMapping.Item1);
                    if (match != null) match.ExternalField = fieldMapping.Item2;
                }
            }

            PerformanceMetrics.AddRange(ProSpaceImportHelper.EnumerateDefaultMetrics().Select(AsPlanogramImportTemplatePerformanceMetric));
        }

        private void ApplyApolloMappings()
        {
            foreach (PlanogramFieldMappingType value in Enum.GetValues(typeof(PlanogramFieldMappingType)))
            {
                IEnumerable<PlanogramImportTemplateMapping> planogramMappings = Mappings.Where(m => m.FieldType == value).ToList();
                foreach (Tuple<String, String> fieldMapping in ApolloImportHelper.EnumerateDefaultMappings(value))
                {
                    PlanogramImportTemplateMapping match = planogramMappings.FirstOrDefault(m => m.Field == fieldMapping.Item1);
                    if (match != null) match.ExternalField = fieldMapping.Item2;
                }
            }

            #region Performance Metrics
            PerformanceMetrics.Add(PlanogramImportTemplatePerformanceMetric.NewPlanogramImportTemplatePerformanceMetric(Message.PlanogramPerformance_DefaultMetric1_SalesValue, Message.PlanogramPerformance_DefaultMetric1_RegularSalesValue, ApolloImportHelper.PositionSales, 1));
            PerformanceMetrics[0].MetricType = MetricType.Decimal;
            PerformanceMetrics.Add(PlanogramImportTemplatePerformanceMetric.NewPlanogramImportTemplatePerformanceMetric(Message.PlanogramPerformance_DefaultMetric2_SalesVolume, Message.PlanogramPerformance_DefaultMetric2_RegularSalesVolume, ApolloImportHelper.ProductRealMovement, 2));
            PerformanceMetrics[1].MetricType = MetricType.Integer;
            PerformanceMetrics[1].SpecialType = MetricSpecialType.RegularSalesUnits;
            PerformanceMetrics.Add(PlanogramImportTemplatePerformanceMetric.NewPlanogramImportTemplatePerformanceMetric(Message.PlanogramPerformance_DefaultMetric3_SalesMargin, Message.PlanogramPerformance_DefaultMetric3_RegularSalesMargin, ApolloImportHelper.PositionPROFIT, 3));
            PerformanceMetrics[2].MetricType = MetricType.Decimal;
            #endregion
        }

        private static String FormatCustomAttributePropertyName(String customPropertyName)
        {
            return String.Format("{0}.{1}", Planogram.CustomAttributesProperty.Name, customPropertyName);
        }

        private static PlanogramImportTemplatePerformanceMetric AsPlanogramImportTemplatePerformanceMetric(SpacePlanningImportHelper.MetricData value)
        {
            PlanogramImportTemplatePerformanceMetric metric = PlanogramImportTemplatePerformanceMetric
                .NewPlanogramImportTemplatePerformanceMetric(value.Name,
                                                             value.Description,
                                                             value.ExternalField,
                                                             value.MetricId);
            metric.MetricType = value.MetricType;
            metric.SpecialType = value.SpecialType;
            return metric;
        }

        public RestructurePlanogramSettings GetRestructurePlanogramSettings()
        {
            RestructurePlanogramSettings output = new RestructurePlanogramSettings();

            output.Enabled = this.AllowAdvancedBaySplitting;
            output.BayCount = this.SplitBayCount;
            output.FixedSize = this.SplitFixedSize;
            output.RecurringPattern = this.SplitRecurringPattern;
            output.SplitByComponentStart = this.SplitBaysByComponentStart;
            output.SplitByComponentEnd = this.SplitBaysByComponentEnd;
            output.SplitByFixedSize = this.SplitByFixedSize;
            output.SplitByRecurringPattern = this.SplitByRecurringPattern;
            output.CanSplitComponents = this.CanSplitComponents;
            output.SplitByBayCount = this.SplitByBayCount;

            return output;
        }
        #endregion

        #region Commands

        #region LockPlanogramImportTemplateCommand
        /// <summary>
        /// Performs the locking of a PlanogramImportTemplate
        /// </summary>
        [Serializable]
        private partial class LockPlanogramImportTemplateCommand : CommandBase<LockPlanogramImportTemplateCommand>
        {
            #region Properties

            #region DalFactoryType
            /// <summary>
            /// DalFactoryType property definition
            /// </summary>
            public static readonly PropertyInfo<PlanogramImportTemplateDalType> DalFactoryTypeProperty =
                RegisterProperty<PlanogramImportTemplateDalType>(c => c.DalFactoryType);
            /// <summary>
            /// Returns the dal factory type
            /// </summary>
            public PlanogramImportTemplateDalType DalFactoryType
            {
                get { return this.ReadProperty<PlanogramImportTemplateDalType>(DalFactoryTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public LockPlanogramImportTemplateCommand(PlanogramImportTemplateDalType dalType, Object id)
            {
                this.LoadProperty<PlanogramImportTemplateDalType>(DalFactoryTypeProperty, dalType);
                this.LoadProperty<Object>(IdProperty, id);
            }
            #endregion
        }
        #endregion

        #region UnlockPlanogramImportTemplateCommand
        /// <summary>
        /// Performs the unlocking of an object
        /// </summary>
        [Serializable]
        private partial class UnlockPlanogramImportTemplateCommand : CommandBase<UnlockPlanogramImportTemplateCommand>
        {
            #region Properties

            #region DalFactoryType
            /// <summary>
            /// DalFactoryType property definition
            /// </summary>
            public static readonly PropertyInfo<PlanogramImportTemplateDalType> DalFactoryTypeProperty =
                RegisterProperty<PlanogramImportTemplateDalType>(c => c.DalFactoryType);
            /// <summary>
            /// Returns the dal factory type
            /// </summary>
            public PlanogramImportTemplateDalType DalFactoryType
            {
                get { return this.ReadProperty<PlanogramImportTemplateDalType>(DalFactoryTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public UnlockPlanogramImportTemplateCommand(PlanogramImportTemplateDalType sourceType, Object id)
            {
                this.LoadProperty<PlanogramImportTemplateDalType>(DalFactoryTypeProperty, sourceType);
                this.LoadProperty<Object>(IdProperty, id);
            }
            #endregion
        }
        #endregion

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    if (!IsNew)
                    {
                        if (this.DalFactoryType == PlanogramImportTemplateDalType.FileSystem
                            && this.Id is String)
                        {
                            UnlockPlanogramImportTemplateByFileName((String)Id);
                        }
                    }
                }
                _isDisposed = true;
            }
        }

        #endregion
    }

    public enum PlanogramImportFileType
    {
        SpacemanV9 = 0,
        ProSpace = 1,
        Apollo = 2
    }
    public static class PlanogramImportFileTypeHelper
    {
        public static readonly Dictionary<PlanogramImportFileType, String> FriendlyNames =
            new Dictionary<PlanogramImportFileType, String>()
            {
                {PlanogramImportFileType.SpacemanV9, "Spaceman v9"},
                {PlanogramImportFileType.ProSpace, "JDA Space Planning v8"},
                {PlanogramImportFileType.Apollo, "Apollo v11"}
            };

        public static readonly Dictionary<PlanogramImportFileType, String> Extension =
            new Dictionary<PlanogramImportFileType, String>
            {
                {PlanogramImportFileType.SpacemanV9, ".pln"},
                {PlanogramImportFileType.ProSpace, ".psa"},
                {PlanogramImportFileType.Apollo, ".xmz"}
            };

        public static PlanogramImportFileType GetTypeFromExtension(String extension)
        {
            KeyValuePair<PlanogramImportFileType, String> match = Extension.FirstOrDefault(pair => String.Equals(extension.ToLowerInvariant(), pair.Value));
            Boolean isValidMatch = !match.Equals(default(KeyValuePair<PlanogramImportFileType, String>));
            return isValidMatch ? match.Key : PlanogramImportFileType.SpacemanV9;
        }

        /// <summary>
        ///     Get the <see cref="PlanogramImportFileType"/> corresponding to the extension of the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The full name of the file to get the <see cref="PlanogramImportFileType"/> from.</param>
        /// <returns>The type matching the extension of the <paramref name="fileName"/>.</returns>
        public static PlanogramImportFileType GetTypeFromFileName(String fileName)
        {
            if (!String.IsNullOrWhiteSpace(fileName)) return GetTypeFromExtension(Path.GetExtension(fileName));

            Debug.Fail("Invalid file name. The provided file name was either null or empty.");
            return PlanogramImportFileType.SpacemanV9;
        }

        /// <summary>
        ///     Lookup of max supported versions per planogram import file type.
        /// </summary>
        public static readonly Dictionary<PlanogramImportFileType, String> MaxSupportedVersion =
            new Dictionary<PlanogramImportFileType, String>
            {
                {PlanogramImportFileType.SpacemanV9, "9"},
                {PlanogramImportFileType.ProSpace, "8"},
                {PlanogramImportFileType.Apollo, "11"},
            };

        /// <summary>
        ///     Lookup of default names per planogram import file type.
        /// </summary>
        public static readonly Dictionary<PlanogramImportFileType, String> DefaultTemplateName =
            new Dictionary<PlanogramImportFileType, String>
            {
                {PlanogramImportFileType.SpacemanV9, Resources.Language.Message.PlanogramImportTemplate_SpacemanDefaultName},
                {PlanogramImportFileType.Apollo, Resources.Language.Message.PlanogramImportTemplate_ApolloDefaultName},
                {PlanogramImportFileType.ProSpace, Resources.Language.Message.PlanogramImportTemplate_ProSpaceDefaultName},
            };
    }
}