﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class FixtureAssemblyComponent
    {
        #region Constructor
        private FixtureAssemblyComponent() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static FixtureAssemblyComponent Fetch(IDalContext dalContext, FixtureAssemblyComponentDto dto)
        {
            return DataPortal.FetchChild<FixtureAssemblyComponent>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, FixtureAssemblyComponentDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32>(FixtureComponentIdProperty, dto.FixtureComponentId);
            this.LoadProperty<Single>(XProperty, dto.X);
            this.LoadProperty<Single>(YProperty, dto.Y);
            this.LoadProperty<Single>(ZProperty, dto.Z);
            this.LoadProperty<Single>(SlopeProperty, dto.Slope);
            this.LoadProperty<Single>(AngleProperty, dto.Angle);
            this.LoadProperty<Single>(RollProperty, dto.Roll);

        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private FixtureAssemblyComponentDto GetDataTransferObject(FixtureAssembly parent)
        {
            return new FixtureAssemblyComponentDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                FixtureAssemblyId = parent.Id,
                FixtureComponentId = ReadProperty<Int32>(FixtureComponentIdProperty),
                X = ReadProperty<Single>(XProperty),
                Y = ReadProperty<Single>(YProperty),
                Z = ReadProperty<Single>(ZProperty),
                Slope = ReadProperty<Single>(SlopeProperty),
                Angle = ReadProperty<Single>(AngleProperty),
                Roll = ReadProperty<Single>(RollProperty),
 
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FixtureAssemblyComponentDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, FixtureAssembly parent)
        {
            this.ResolveIds(dalContext);
            FixtureAssemblyComponentDto dto = this.GetDataTransferObject(parent);
            Object oldId = dto.Id;
            using (IFixtureAssemblyComponentDal dal = dalContext.GetDal<IFixtureAssemblyComponentDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<FixtureAssemblyComponent>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, FixtureAssembly parent)
        {
            this.ResolveIds(dalContext);
            if (this.IsSelfDirty)
            {
                using (IFixtureAssemblyComponentDal dal = dalContext.GetDal<IFixtureAssemblyComponentDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, FixtureAssembly parent)
        {
            using (IFixtureAssemblyComponentDal dal = dalContext.GetDal<IFixtureAssemblyComponentDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion

    }
}