﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
// V8-26491 : A.Kuszyk
//  Added Assortment.
// V8-26956 : L.Luong
//  Added ConsumerDecisionTree
// V8-26891 : L.Ineson
//  Added Blocking
// V8-27938 : N.Haywood
//  Added Data Sheets
#endregion

#region Version History: CCM830

// V8-31742 : A.Silva
//  Added PlanogramComparison.
//V8-31834 : L.Ineson
//  Added Category Insight
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    public enum DocumentType
    {
        None = 0,
        Design = 1,
        Front = 2,
        Left = 3,
        Right = 4,
        Top = 5,
        Bottom = 6,
        Back = 7,
        Perspective = 8,
        ProductList = 9,
        FixtureList = 10,
        Assortment = 11,
        ConsumerDecisionTree = 12,
        Blocking = 13,
        DataSheet = 14,
        PlanogramComparison = 15,
        CategoryInsight = 16
    }

    public static class DocumentTypeHelper
    {
        public static readonly Dictionary<DocumentType, String> FriendlyNames =
            new Dictionary<DocumentType, String>
            {
                {DocumentType.Front, Message.PlanDocumentType_OrthFront },
                {DocumentType.Left, Message.PlanDocumentType_OrthLeft },
                {DocumentType.Right, Message.PlanDocumentType_OrthRight },
                {DocumentType.Top, Message.PlanDocumentType_OrthTop },
                {DocumentType.Bottom, Message.PlanDocumentType_OrthBottom },
                {DocumentType.Back, Message.PlanDocumentType_OrthBack },
                {DocumentType.Perspective, Message.PlanDocumentType_ThreeDimensional },
                {DocumentType.ProductList, Message.PlanDocumentType_ProductList },
                {DocumentType.FixtureList, Message.PlanDocumentType_FixtureList },
                {DocumentType.Design, Message.PlanDocumentType_Design },
                {DocumentType.Assortment, Message.PlanDocumentType_Assortment},
                {DocumentType.ConsumerDecisionTree, Message.PlanDocumentType_ConsumerDecisionTree},
                {DocumentType.Blocking, Message.PlanDocumentType_Blocking},
                {DocumentType.DataSheet, Message.PlanDocumentType_DataSheet},
                {DocumentType.PlanogramComparison, Message.PlanDocumentType_PlanogramCompare},
                {DocumentType.CategoryInsight, Message.PlanDocumentType_CategoryInsight},
            };
    }
}
