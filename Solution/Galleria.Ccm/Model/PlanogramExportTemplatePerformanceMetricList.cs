﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of PlanogramExportTemplatePerformanceMetric objects.
    /// </summary>
    [Serializable]
    public partial class PlanogramExportTemplatePerformanceMetricList : ModelList<PlanogramExportTemplatePerformanceMetricList, PlanogramExportTemplatePerformanceMetric>
    {
        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramExportTemplatePerformanceMetricList NewPlanogramExportTemplatePerformanceMetricList()
        {
            PlanogramExportTemplatePerformanceMetricList item = new PlanogramExportTemplatePerformanceMetricList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}
