﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26234 : L.Ineson
//	Copied from GFS
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a role within the application domain
    /// </summary>
    [Serializable]
    public sealed partial class Role : ModelObject<Role>
    {
        #region Static Constructor

        static Role()
        {
            FriendlyName = Message.Role;
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The role id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region Row Version
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The role row version
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The role name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region Description
        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);
        /// <summary>
        /// The role description
        /// </summary>
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
            set { SetProperty<String>(DescriptionProperty, value); }
        }
        #endregion

        #region Is Administrator Role
        /// <summary>
        /// IsAdministratorRole property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsAdministratorRoleProperty =
            RegisterModelProperty<Boolean>(c => c.IsAdministratorRole);
        /// <summary>
        /// Indicates if this role represents the built-in administrators role
        /// </summary>
        public Boolean IsAdministratorRole
        {
            get { return GetProperty<Boolean>(IsAdministratorRoleProperty); }
        }
        #endregion

        #region Role Entities
        /// <summary>
        /// RoleEntities property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RoleEntityList> EntitiesProperty =
            RegisterModelProperty<RoleEntityList>(c => c.Entities);
        /// <summary>
        /// Returns the entities that this role has access too
        /// </summary>
        public RoleEntityList Entities
        {
            get { return GetProperty<RoleEntityList>(EntitiesProperty); }
        }
        #endregion

        #region Role Permissions
        /// <summary>
        /// RolePermissions property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RolePermissionList> PermissionsProperty =
            RegisterModelProperty<RolePermissionList>(c => c.Permissions);
        /// <summary>
        /// Returns the permissions for this role
        /// </summary>
        public RolePermissionList Permissions
        {
            get { return GetProperty<RolePermissionList>(PermissionsProperty); }
        }
        #endregion

        #region Role Members
        /// <summary>
        /// RoleMembers property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RoleMemberList> MembersProperty =
            RegisterModelProperty<RoleMemberList>(c => c.Members);
        /// <summary>
        /// Returns the members of this role
        /// </summary>
        public RoleMemberList Members
        {
            get { return GetProperty<RoleMemberList>(MembersProperty); }
        }
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            // name
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));

            // description
            BusinessRules.AddRule(new Required(DescriptionProperty));
            BusinessRules.AddRule(new MaxLength(DescriptionProperty, 100));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(Role), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.RoleCreate.ToString()));
            BusinessRules.AddRule(typeof(Role), new IsInRole(AuthorizationActions.GetObject, DomainPermission.RoleGet.ToString()));
            BusinessRules.AddRule(typeof(Role), new IsInRole(AuthorizationActions.EditObject, DomainPermission.RoleEdit.ToString()));
            BusinessRules.AddRule(typeof(Role), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.RoleDelete.ToString()));
        }
        #endregion

        #region Criteria

        #region FetchByIdCriteria

        /// <summary>
        /// Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> IdProperty =
                RegisterProperty<Int32>(c => c.Id);
            /// <summary>
            /// Returns the id
            /// </summary>
            public Int32 Id
            {
                get { return this.ReadProperty<Int32>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(Int32 id)
            {
                this.LoadProperty<Int32>(IdProperty, id);
            }
            #endregion
        }

        #endregion

        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static Role NewRole()
        {
            Role item = new Role();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<RoleEntityList>(EntitiesProperty, RoleEntityList.NewList());
            this.LoadProperty<RolePermissionList>(PermissionsProperty, RolePermissionList.NewList());
            this.LoadProperty<RoleMemberList>(MembersProperty, RoleMemberList.NewList());
            base.Create();
        }
        #endregion

        #endregion

        #region Overrides
        /// <summary>
        /// Returns a string representation of this object
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }
        #endregion
    }
}
