﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24779 : L.Luong
//  Created.

#endregion
#region Version History: (CCM 8.03)
// V8-29220 : L.Ineson
//Removed date deleted
#endregion

#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Core;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramImportTemplateInfoList
    {
        #region Constructors
        private PlanogramImportTemplateInfoList() { } // force the use of factory methods
        #endregion

        #region FactoryMethods

        public static PlanogramImportTemplateInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<PlanogramImportTemplateInfoList>(new FetchByEntityIdCriteria(entityId));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when retrieving a list of all PlanogramImportTemplates for an entity
        /// Note: This procedure should NOT return deleted items
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramImportTemplateInfoDal dal = dalContext.GetDal<IPlanogramImportTemplateInfoDal>())
                {
                    IEnumerable<PlanogramImportTemplateInfoDto> dtoList = dal.FetchByEntityId(criteria.EntityId);

                    foreach (PlanogramImportTemplateInfoDto dto in dtoList)
                    {
                        this.Add(PlanogramImportTemplateInfo.FetchPlanogramImportTemplateInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion

        #endregion

    }
}
