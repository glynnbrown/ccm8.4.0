﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
// V8-24264 : A.Probyn
//      Added ModelPropertyDisplayType to BayAverageWidth
#endregion
#region Version History: (CCM v8.0.2)
// V8-29010 : D.Pleasance
//  Added EnumerateDisplayableFieldInfos
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Rules;
using System.Collections.Generic;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a description of a type of location space product group 
    /// (child of location space )
    /// </summary>
    [Serializable]
    public sealed partial class LocationSpaceProductGroup : ModelObject<LocationSpaceProductGroup>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new LocationSpace Parent
        {
            get { return ((LocationSpaceProductGroupList)base.Parent).Parent; }
        }
        #endregion

        #region Properties
        /// <summary>
        /// The Location Space Product Group Id.
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }

        /// <summary>
        /// The Product Group Id.
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProductGroupIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductGroupId);
        public Int32 ProductGroupId
        {
            get { return GetProperty<Int32>(ProductGroupIdProperty); }
            set { SetProperty<Int32>(ProductGroupIdProperty, value); }
        }

        /// <summary>
        /// The Bay Count.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BayCountProperty =
            RegisterModelProperty<Single>(c => c.BayCount, Message.LocationSpaceProductGroup_BayCount);
        public Single BayCount
        {
            get { return GetProperty<Single>(BayCountProperty); }
            set { SetProperty<Single>(BayCountProperty, value); }
        }

        /// <summary>
        /// The Average Bay Width
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AverageBayWidthProperty =
            RegisterModelProperty<Single>(c => c.AverageBayWidth, Message.LocationSpaceProductGroup_AverageBayWidth, ModelPropertyDisplayType.Length);
        public Single AverageBayWidth
        {
            get { return GetProperty<Single>(AverageBayWidthProperty); }
            set { SetProperty<Single>(AverageBayWidthProperty, value); }
        }

        /// <summary>
        /// The Product Count.
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> ProductCountProperty =
            RegisterModelProperty<Int32?>(c => c.ProductCount, Message.LocationSpaceProductGroup_ProductCount);
        public Int32? ProductCount
        {
            get { return GetProperty<Int32?>(ProductCountProperty); }
            set { SetProperty<Int32?>(ProductCountProperty, value); }
        }



        /// <summary>
        /// Aisle Name.
        /// </summary>
        public static readonly ModelPropertyInfo<String> AisleNameProperty =
            RegisterModelProperty<String>(c => c.AisleName, Message.LocationSpaceProductGroup_AisleName);
        public String AisleName
        {
            get { return GetProperty<String>(AisleNameProperty); }
            set { SetProperty<String>(AisleNameProperty, value); }
        }

        /// <summary>
        /// Valley Name.
        /// </summary>
        public static readonly ModelPropertyInfo<String> ValleyNameProperty =
            RegisterModelProperty<String>(c => c.ValleyName, Message.LocationSpaceProductGroup_ValleyName);
        public String ValleyName
        {
            get { return GetProperty<String>(ValleyNameProperty); }
            set { SetProperty<String>(ValleyNameProperty, value); }
        }

        /// <summary>
        /// Zone Name.
        /// </summary>
        public static readonly ModelPropertyInfo<String> ZoneNameProperty =
            RegisterModelProperty<String>(c => c.ZoneName, Message.LocationSpaceProductGroup_ZoneName);
        public String ZoneName
        {
            get { return GetProperty<String>(ZoneNameProperty); }
            set { SetProperty<String>(ZoneNameProperty, value); }
        }

        /// <summary>
        /// CustomAttribute01.
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomAttribute01Property =
            RegisterModelProperty<String>(c => c.CustomAttribute01, Message.LocationSpaceProductGroup_CustomAttribute01);
        public String CustomAttribute01
        {
            get { return GetProperty<String>(CustomAttribute01Property); }
            set { SetProperty<String>(CustomAttribute01Property, value); }
        }

        /// <summary>
        /// CustomAttribute02.
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomAttribute02Property =
            RegisterModelProperty<String>(c => c.CustomAttribute02, Message.LocationSpaceProductGroup_CustomAttribute02);
        public String CustomAttribute02
        {
            get { return GetProperty<String>(CustomAttribute02Property); }
            set { SetProperty<String>(CustomAttribute02Property, value); }
        }

        /// <summary>
        /// CustomAttribute03.
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomAttribute03Property =
            RegisterModelProperty<String>(c => c.CustomAttribute03, Message.LocationSpaceProductGroup_CustomAttribute03);
        public String CustomAttribute03
        {
            get { return GetProperty<String>(CustomAttribute03Property); }
            set { SetProperty<String>(CustomAttribute03Property, value); }
        }

        /// <summary>
        /// CustomAttribute04.
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomAttribute04Property =
            RegisterModelProperty<String>(c => c.CustomAttribute04, Message.LocationSpaceProductGroup_CustomAttribute04);
        public String CustomAttribute04
        {
            get { return GetProperty<String>(CustomAttribute04Property); }
            set { SetProperty<String>(CustomAttribute04Property, value); }
        }

        /// <summary>
        /// CustomAttribute04.
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomAttribute05Property =
            RegisterModelProperty<String>(c => c.CustomAttribute05, Message.LocationSpaceProductGroup_CustomAttribute05);
        public String CustomAttribute05
        {
            get { return GetProperty<String>(CustomAttribute05Property); }
            set { SetProperty<String>(CustomAttribute05Property, value); }
        }

        /// <summary>
        /// The Location Space Bay
        /// </summary>
        public static readonly ModelPropertyInfo<LocationSpaceBayList> BaysProperty =
            RegisterModelProperty<LocationSpaceBayList>(c => c.Bays);

        public LocationSpaceBayList Bays
        {
            get { return GetProperty<LocationSpaceBayList>(BaysProperty); }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationSpaceProductGroup), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationSpaceCreate.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceProductGroup), new IsInRole(AuthorizationActions.GetObject, DomainPermission.LocationSpaceGet.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceProductGroup), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationSpaceEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceProductGroup), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationSpaceDelete.ToString()));
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new MinValue<Int32>(ProductGroupIdProperty, 1));

            BusinessRules.AddRule(new Required(BayCountProperty));
            BusinessRules.AddRule(new MinValue<Single>(BayCountProperty, 0));
            BusinessRules.AddRule(new MaxValue<Single>(BayCountProperty, 100));

            BusinessRules.AddRule(new NullableMinValue<Int32>(ProductCountProperty, 0));
            BusinessRules.AddRule(new NullableMaxValue<Int32>(ProductCountProperty, 9999));

            BusinessRules.AddRule(new Required(AverageBayWidthProperty));
            BusinessRules.AddRule(new MinValue<Single>(AverageBayWidthProperty, 0));
            BusinessRules.AddRule(new MaxValue<Single>(AverageBayWidthProperty, 99999.99f));
        }

        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a new location space object
        /// </summary>
        /// <param name="createDefaultBay">Create default bay parameter, defaulted to true</param>
        /// <returns></returns>
        public static LocationSpaceProductGroup NewLocationSpaceProductGroup(Boolean createDefaultBay = true)
        {
            LocationSpaceProductGroup item = new LocationSpaceProductGroup();
            item.Create(createDefaultBay);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Boolean createDefaultBay = true)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<LocationSpaceBayList>(BaysProperty, LocationSpaceBayList.NewLocationSpaceBayList(createDefaultBay));
            this.LoadProperty<Single>(BayCountProperty, Convert.ToSingle(this.Bays.Count));
            this.MarkAsChild();
            this.MarkGraphAsInitialized();
            this.BusinessRules.CheckRules();
        }


        #endregion

        #endregion

        #region Override

        public override string ToString()
        {
            return this.Id.ToString();
        }

        /// <summary>
        /// Override for the child changed event
        /// </summary>
        /// <param name="e"></param>
        protected override void OnChildChanged(Csla.Core.ChildChangedEventArgs e)
        {
            //If bay list is changing
            if (e.ChildObject.GetType() == typeof(LocationSpaceBayList))
            {
                //Update the bay count property
                if (this.Bays != null)
                {
                    //Set bay count to match the bays count
                    this.BayCount = this.Bays.Count;
                }
            }

            base.OnChildChanged(e);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos()
        {
            Type type = typeof(LocationSpaceProductGroup);
            String typeFriendly = LocationSpaceProductGroup.FriendlyName;

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationSpaceProductGroup.BayCountProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationSpaceProductGroup.AverageBayWidthProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationSpaceProductGroup.ProductCountProperty);            
        }
        #endregion
    }
}
