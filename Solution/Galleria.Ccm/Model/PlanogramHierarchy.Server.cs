﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25587 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Linq;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using System.Collections.Generic;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramHierarchy
    {
        #region Constructors
        private PlanogramHierarchy() { } //force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing PlanogramHierarchy with the given id
        /// </summary>
        public static PlanogramHierarchy FetchById(Int32 id)
        {
            return DataPortal.Fetch<PlanogramHierarchy>(new FetchByIdCriteria(id));
        }

        /// <summary>
        /// Returns the hierarchy for the given entity id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static PlanogramHierarchy FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<PlanogramHierarchy>(new FetchByEntityIdCriteria(entityId));
        }

        public static Dictionary<Guid, Guid> FetchPlanogramPlanogramGroupGroupings(Int32 entityId)
        {
            FetchPlanogramPlanogramGroupGroupingsCommand cmd = new FetchPlanogramPlanogramGroupGroupingsCommand()
            {
                EntityId = entityId
            };

            cmd = DataPortal.Execute<FetchPlanogramPlanogramGroupGroupingsCommand>(cmd);

            return cmd.PlanogramGroupings;
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramHierarchyDto dto,
            IEnumerable<PlanogramGroupDto> childProductGroupList)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);

            //get the root group dto and load
            PlanogramGroupDto rootGroupDto = childProductGroupList.FirstOrDefault(l => !l.ParentGroupId.HasValue);
            if (rootGroupDto != null)
            {
                LoadProperty<PlanogramGroup>(RootGroupProperty,
                    PlanogramGroup.Fetch(dalContext, rootGroupDto, childProductGroupList));
            }
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private PlanogramHierarchyDto GetDataTransferObject()
        {
            return new PlanogramHierarchyDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                Name = ReadProperty<String>(NameProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty),

            };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Called when returning FetchById
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            Int32 hierarchyId = criteria.Id;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                // fetch the merchandising hierarch structure dto
                PlanogramHierarchyDto dto;
                using (IPlanogramHierarchyDal dal = dalContext.GetDal<IPlanogramHierarchyDal>())
                {
                    dto = dal.FetchById(hierarchyId);
                }


                //and get all the groups in one go
                IEnumerable<PlanogramGroupDto> groupDtoList;
                using (IPlanogramGroupDal dal = dalContext.GetDal<IPlanogramGroupDal>())
                {
                    groupDtoList = dal.FetchByPlanogramHierarchyId(hierarchyId);
                }

                //load the hierarchy
                LoadDataTransferObject(dalContext, dto, groupDtoList);
            }
        }

        /// <summary>
        /// Called when return FetchByEntityId
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            Int32 entityId = criteria.EntityId;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                // fetch the merchandising hierarch structure dto
                PlanogramHierarchyDto dto;
                using (IPlanogramHierarchyDal dal = dalContext.GetDal<IPlanogramHierarchyDal>())
                {
                    dto = dal.FetchByEntityId(entityId);
                }

                //and get all the groups in one go
                IEnumerable<PlanogramGroupDto> groupDtoList;
                using (IPlanogramGroupDal dal = dalContext.GetDal<IPlanogramGroupDal>())
                {
                    groupDtoList = dal.FetchByPlanogramHierarchyId(dto.Id);
                }

                //load the hierarchy
                LoadDataTransferObject(dalContext, dto, groupDtoList);
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                PlanogramHierarchyDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (IPlanogramHierarchyDal dal = dalContext.GetDal<IPlanogramHierarchyDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<PlanogramHierarchy>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this instance
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                PlanogramHierarchyDto dto = GetDataTransferObject();
                using (IPlanogramHierarchyDal dal = dalContext.GetDal<IPlanogramHierarchyDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when this instance is being deleted
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IPlanogramHierarchyDal dal = dalContext.GetDal<IPlanogramHierarchyDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #endregion

        #region commands

        [Serializable]
        private class FetchPlanogramPlanogramGroupGroupingsCommand : CommandBase<FetchPlanogramPlanogramGroupGroupingsCommand>
        {
            public Dictionary<Guid, Guid> PlanogramGroupings { get; private set; }
            public Int32 EntityId { get; set; }

            /// <summary>
            /// Defines the authorization rules for this type
            /// Execute will use the edit action to perform this exists action. 
            /// Remaining actions should never be executed hence the 'restricted' domain permission on those. 
            /// </summary>
            private static void AddObjectAuthorizationRules()
            {
                BusinessRules.AddRule(typeof(FetchPlanogramPlanogramGroupGroupingsCommand), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(FetchPlanogramPlanogramGroupGroupingsCommand), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(FetchPlanogramPlanogramGroupGroupingsCommand), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
                BusinessRules.AddRule(typeof(FetchPlanogramPlanogramGroupGroupingsCommand), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
            }

            protected override void DataPortal_Execute()
            {
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IPlanogramHierarchyDal dal = dalContext.GetDal<IPlanogramHierarchyDal>())
                    {
                        try
                        {
                            PlanogramGroupings = dal.FetchPlanogramPlanogramGroupGroupings(EntityId);
                        }
                        catch (Exception e)
                        {

                        }
                    }
                }
            }
        }

        #endregion

    }
}