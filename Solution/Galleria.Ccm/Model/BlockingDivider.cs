﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//	Created (Auto-generated)
// V8-27546 : A.Kuszyk
//  Added GetOverlap method.
#endregion
#endregion


using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Helpers;
using System.Diagnostics;


namespace Galleria.Ccm.Model
{
    /// <summary>
    /// BlockingDivider Model object
    /// </summary>
    [Serializable]
    public sealed partial class BlockingDivider : ModelObject<BlockingDivider>, IPlanogramBlockingDivider
    {

        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Blocking Parent
        {
            get
            {
                BlockingDividerList parentList = base.Parent as BlockingDividerList;
                if (parentList != null)
                {
                    return parentList.Parent as Blocking;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// The unique Object id
        /// </summary>
        Object IPlanogramBlockingDivider.Id
        {
            get { return Id; }
        }

        #endregion

        #region Type

        /// <summary>
        /// Type property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramBlockingDividerType> TypeProperty =
            RegisterModelProperty<PlanogramBlockingDividerType>(c => c.Type);

        /// <summary>
        /// Gets/Sets the Type value
        /// </summary>
        public PlanogramBlockingDividerType Type
        {
            get { return GetProperty<PlanogramBlockingDividerType>(TypeProperty); }
            set { SetProperty<PlanogramBlockingDividerType>(TypeProperty, value); }
        }

        #endregion

        #region Level

        /// <summary>
        /// Level property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> LevelProperty =
            RegisterModelProperty<Byte>(c => c.Level);

        /// <summary>
        /// Gets/Sets the Level value
        /// </summary>
        public Byte Level
        {
            get { return GetProperty<Byte>(LevelProperty); }
            set { SetProperty<Byte>(LevelProperty, value); }
        }

        #endregion

        #region X

        /// <summary>
        /// X property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> XProperty =
            RegisterModelProperty<Single>(c => c.X);

        /// <summary>
        /// Gets/Sets the X value
        /// </summary>
        public Single X
        {
            get { return GetProperty<Single>(XProperty); }
            private set //force use of MoveTo method
            {
                Single currentValue = this.ReadProperty<Single>(XProperty);
                if (value != currentValue)
                {
                    SetProperty<Single>(XProperty, value);
                    NotifyObjectGraph(XProperty);
                }
            } 
        }

        #endregion

        #region Y

        /// <summary>
        /// Y property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> YProperty =
            RegisterModelProperty<Single>(c => c.Y);

        /// <summary>
        /// Gets/Sets the Y value
        /// </summary>
        public Single Y
        {
            get { return GetProperty<Single>(YProperty); }
            private set  //force use of MoveTo method
            {
                Single currentValue = this.ReadProperty<Single>(YProperty);
                if (value != currentValue)
                {
                    SetProperty<Single>(YProperty, value);
                    NotifyObjectGraph(YProperty);
                }
            }
        }

        #endregion

        #region Length

        /// <summary>
        /// Length property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> LengthProperty =
            RegisterModelProperty<Single>(c => c.Length);

        /// <summary>
        /// Gets/Sets the Length value
        /// </summary>
        public Single Length
        {
            get { return GetProperty<Single>(LengthProperty); }
            private set {  SetProperty<Single>(LengthProperty, value); }
        }

        #endregion

        #region IsSnapped

        /// <summary>
        /// IsSnapped property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsSnappedProperty =
            RegisterModelProperty<Boolean>(c => c.IsSnapped);

        /// <summary>
        /// Gets/Sets the IsSnapped value
        /// </summary>
        public Boolean IsSnapped
        {
            get { return GetProperty<Boolean>(IsSnappedProperty); }
            set { SetProperty<Boolean>(IsSnappedProperty, value); }
        }

        #endregion

        #region IsLimited

        /// <summary>
        /// IsLimited property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsLimitedProperty =
            RegisterModelProperty<Boolean>(c => c.IsLimited);

        /// <summary>
        /// Gets/Sets the IsLimited value
        /// </summary>
        public Boolean IsLimited
        {
            get { return GetProperty<Boolean>(IsLimitedProperty); }
            set { SetProperty<Boolean>(IsLimitedProperty, value); }
        }

        #endregion

        #region LimitedPercentage

        /// <summary>
        /// LimitedPercentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> LimitedPercentageProperty =
            RegisterModelProperty<Single>(c => c.LimitedPercentage);

        /// <summary>
        /// Gets/Sets the LimitedPercentage value
        /// </summary>
        public Single LimitedPercentage
        {
            get { return GetProperty<Single>(LimitedPercentageProperty); }
            set { SetProperty<Single>(LimitedPercentageProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new MinValue<Byte>(LevelProperty, 1));

            BusinessRules.AddRule(new MinValue<Single>(XProperty, 0));
            BusinessRules.AddRule(new MaxValue<Single>(XProperty, 1));

            BusinessRules.AddRule(new MinValue<Single>(YProperty, 0));
            BusinessRules.AddRule(new MaxValue<Single>(YProperty, 1));

            BusinessRules.AddRule(new MinValue<Single>(LengthProperty, 0));
            BusinessRules.AddRule(new MaxValue<Single>(LengthProperty, 1));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(BlockingDivider), new IsInRole(AuthorizationActions.GetObject, DomainPermission.BlockingGet.ToString()));
            BusinessRules.AddRule(typeof(BlockingDivider), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.BlockingCreate.ToString()));
            BusinessRules.AddRule(typeof(BlockingDivider), new IsInRole(AuthorizationActions.EditObject, DomainPermission.BlockingEdit.ToString()));
            BusinessRules.AddRule(typeof(BlockingDivider), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.BlockingDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static BlockingDivider NewBlockingDivider()
        {
            BlockingDivider item = new BlockingDivider();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static BlockingDivider NewBlockingDivider(
            Byte levelNo, PlanogramBlockingDividerType type, Single x, Single y, Single length)
        {
            BlockingDivider item = new BlockingDivider();
            item.Create(levelNo, type, x, y, length);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(Byte levelNo, PlanogramBlockingDividerType type, Single x, Single y, Single length)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Byte>(LevelProperty, levelNo);
            this.LoadProperty<PlanogramBlockingDividerType>(TypeProperty, type);
            this.LoadProperty<Single>(XProperty, x);
            this.LoadProperty<Single>(YProperty, y);
            this.LoadProperty<Single>(LengthProperty, length);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<BlockingDivider>(oldId, newId);
        }

        /// <summary>
        /// Moves this divider by the given delta in the correct direction according to its
        /// type. Also ensures that other dividers in the blocking are correctly updated.
        /// </summary>
        /// <param name="delta"></param>
        public void MoveTo(Single newPosition)
        {
            if (this.Type == PlanogramBlockingDividerType.Vertical)
            {
                MoveVerticalDividerTo(this, newPosition);
            }
            else
            {
                MoveHorizontalDividerTo(this, newPosition);
            }
        }

        private static void MoveVerticalDividerTo(BlockingDivider divider, Single newDivX)
        {
            Debug.Assert(divider.Type == PlanogramBlockingDividerType.Vertical);

            Single oldX = divider.X;
            if (newDivX == oldX) return;

            //get a list of adjacent dividers
            List<BlockingDivider> adjacentDividers = divider.GetAdjacentDividers();

            //get the min and max based on dividers either side
            BlockingDivider minParallel, maxParallel;
            divider.GetParallelDividers(out minParallel, out maxParallel);

            Single minX = Convert.ToSingle(Math.Round((minParallel != null) ? minParallel.X + 0.01F : 0.01F, 5));
            Single maxX = Convert.ToSingle(Math.Round((maxParallel != null) ? maxParallel.X - 0.01F : 0.99F, 5));

            //Clamp the delta and apply
            newDivX = Math.Max(minX, newDivX);
            newDivX = Math.Min(maxX, newDivX);
            divider.X = newDivX;

            //update all affected dividers
            foreach (BlockingDivider div in adjacentDividers)
            {
                if (div.X == oldX)
                {
                    div.X = divider.X;
                }
                div.UpdateLength();
            }

        }

        private static void MoveHorizontalDividerTo(BlockingDivider divider, Single newDivY)
        {
            Debug.Assert(divider.Type == PlanogramBlockingDividerType.Horizontal);

            Single oldY = divider.Y;
            if (newDivY == oldY) return;

            //get a list of adjacent dividers
            List<BlockingDivider> adjacentDividers = divider.GetAdjacentDividers();

            //get the min and max based on dividers either side
            BlockingDivider minParallel, maxParallel;
            divider.GetParallelDividers(out minParallel, out maxParallel);

            Single minY = Convert.ToSingle(Math.Round((minParallel != null) ? minParallel.Y + 0.01F : 0.01F, 5));
            Single maxY = Convert.ToSingle(Math.Round((maxParallel != null) ? maxParallel.Y - 0.01F : 0.99F, 5));


            //Clamp the delta and apply
            newDivY = Math.Max(minY, newDivY);
            newDivY = Math.Min(maxY, newDivY);
            divider.Y = newDivY;

            //update all affected dividers
            foreach (BlockingDivider div in adjacentDividers)
            {
                if (div.Y == oldY)
                {
                    div.Y = divider.Y;
                }
                div.UpdateLength();
            }

        }


        /// <summary>
        /// Updates the length value of this divider
        /// </summary>
        internal void UpdateLength()
        {
            Blocking parentBlocking = this.Parent;
            if (parentBlocking == null)
            {
                this.Length = 1;
                return;
            }
            else
            {
                Single max = 0;
                Boolean crossFound = false;
                if (this.Type == PlanogramBlockingDividerType.Vertical)
                {
                    foreach (BlockingLocation l in parentBlocking.Locations)
                    {
                        if (l.BlockingDividerLeftId == this.Id)
                        {
                            max = Math.Max(max, l.Y + l.Height);
                            crossFound = true;
                        }
                    }

                    this.Length = Convert.ToSingle(Math.Round((crossFound) ? max - this.Y : 1, 5));
                }
                else
                {
                    foreach (BlockingLocation l in parentBlocking.Locations)
                    {
                        if (l.BlockingDividerBottomId == this.Id)
                        {
                            max = Math.Max(max, l.X + l.Width);
                            crossFound = true;
                        }
                    }

                    this.Length = Convert.ToSingle(Math.Round((crossFound) ? max - this.X : 1, 5));
                }
                
            }
        }

        /// <summary>
        /// Returns all dividers which are adjacent to this one.
        /// </summary>
        public List<BlockingDivider> GetAdjacentDividers()
        {
           return BlockingHelper.GetAdjacentDividers(this, this.Parent);
        }

        /// <summary>
        /// Returns an the nearest parallel dividers before and after this one
        /// </summary>
        /// <returns></returns>
        public void GetParallelDividers(out BlockingDivider nearestMin, out BlockingDivider nearestMax)
        {
            BlockingHelper.GetParallelDividers(this, this.Parent, out nearestMin, out nearestMax);
        }

        /// <summary>
        /// Returns the percentage by which the given divider overlaps with this divider, 
        /// in the dimension of the divider's orientation.
        /// </summary>
        /// <param name="divider">The divider against which the overlap is to be evaluated.</param>
        /// <returns>A value between 0 and 1 representing the amount of overlap.</returns>
        public Single GetOverlap(BlockingDivider divider)
        {
            return BlockingHelper.GetOverlap(this, divider);
        }

        #endregion
    }
}