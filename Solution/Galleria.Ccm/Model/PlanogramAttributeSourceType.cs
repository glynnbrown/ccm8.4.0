﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM803
// V8-29643 : D.Pleasance
//  Created
#endregion
#region Version History: CCM803
// V8-30063 : D.Pleasance
//  Added Inventory
#endregion
#region Version History: CCM820
// V8-31128 : D.Pleasance
//  Added Info
#endregion
#region Version History: CCM840
// V8-19069 : J.Mendes
//  Added DataNote
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Model
{
    public enum PlanogramAttributeSourceType
    {
        General = 0,
        CustomDataText = 1,
        CustomDataValue = 2,
        CustomDataFlag = 3,
        CustomDataDate = 4,
        Settings = 5,
        Inventory = 6,
        Info = 7,
        CustomDataNote = 8
    }
}