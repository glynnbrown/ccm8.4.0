﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using System.Collections.Generic;
using Csla.Core;

namespace Galleria.Ccm.Model
{
    public partial class ProductGroup
    {
        #region Constructor
        private ProductGroup() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static ProductGroup Fetch(IDalContext dalContext, ProductGroupDto dto, IEnumerable<ProductGroupDto> productGroupDtoList)
        {
            return DataPortal.FetchChild<ProductGroup>(dalContext, dto, productGroupDtoList);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, ProductGroupDto dto, IEnumerable<ProductGroupDto> productGroupDtoList)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32>(ProductLevelIdProperty, dto.ProductLevelId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(CodeProperty, dto.Code);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted); 

            LoadProperty<ProductGroupList>(ChildListProperty,
                ProductGroupList.FetchByParentProductGroupId(dalContext, dto.Id, productGroupDtoList));
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private ProductGroupDto GetDataTransferObject()
        {
            return new ProductGroupDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                ProductHierarchyId = this.ParentHierarchy.Id,
                ProductLevelId = ReadProperty<Int32>(ProductLevelIdProperty),
                Name = ReadProperty<String>(NameProperty),
                Code = ReadProperty<String>(CodeProperty),
                ParentGroupId = (this.ParentGroup != null) ? (Int32?)this.ParentGroup.Id : null,
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty), 
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ProductGroupDto dto, IEnumerable<ProductGroupDto> productGroupDtoList)
        {
            this.LoadDataTransferObject(dalContext, dto, productGroupDtoList);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext)
        {
            this.ResolveIds(dalContext);
            ProductGroupDto dto = this.GetDataTransferObject();
            Int32 oldId = dto.Id;
            using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            dalContext.RegisterId<ProductGroup>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            this.ResolveIds(dalContext);
            if (this.IsSelfDirty)
            {
                ProductGroupDto dto = this.GetDataTransferObject();
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
            {
                dal.DeleteById(this.Id);
            }

            //GEM:14253
            //mark any child groups of this as deleted
            foreach (ProductGroup child in this.ChildList)
            {
                //forcibly mark the child as deleted
                ((IEditableBusinessObject)child).DeleteChild();
            }

            //make sure this updates its children so they get deleted if required.
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #endregion

    }
}