﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//	Created
// CCM-25452 : N.Haywood
//  Added ToString override
#endregion
#region Version History: CCM811
// V8-30463 : L.Ineson
//  Added GetIdValue override
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// ProductGroup Model object
    /// </summary>
    [Serializable]
    public sealed partial class ProductGroupInfo : ModelReadOnlyObject<ProductGroupInfo>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        #endregion

        #region Code

        /// <summary>
        /// Code property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CodeProperty =
            RegisterModelProperty<String>(c => c.Code);

        /// <summary>
        /// Gets/Sets the Code value
        /// </summary>
        public String Code
        {
            get { return GetProperty<String>(CodeProperty); }
        }

        #endregion

        #region DateDeleted

        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);

        /// <summary>
        /// Gets/Sets the DateDeleted value
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ProductGroupInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(ProductGroupInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ProductGroupInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ProductGroupInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new info of the given product group.
        /// </summary>
        /// <param name="productGroup"></param>
        /// <returns></returns>
        public static ProductGroupInfo NewProductGroupInfo(ProductGroup productGroup)
        {
            ProductGroupInfo item = new ProductGroupInfo();
            item.Create(productGroup);
            return item;
        }

        /// <summary>
        /// Creates a new info of the given details.
        /// </summary>
        /// <param name="productGroup"></param>
        /// <returns></returns>
        public static ProductGroupInfo NewProductGroupInfo(Int32 id, String code, String name)
        {
            ProductGroupInfo item = new ProductGroupInfo();
            item.Create(id, code, name);
            return item;
        }

        #endregion

        #region Methods

        private void Create(ProductGroup productGroup)
        {
            LoadProperty<Int32>(IdProperty, productGroup.Id);
            LoadProperty<String>(CodeProperty, productGroup.Code);
            LoadProperty<String>(NameProperty, productGroup.Name);
            LoadProperty<DateTime?>(DateDeletedProperty, null);
        }

        private void Create(Int32 id, String code, String name)
        {
            LoadProperty<Int32>(IdProperty, id);
            LoadProperty<String>(CodeProperty, code);
            LoadProperty<String>(NameProperty, name);
            LoadProperty<DateTime?>(DateDeletedProperty, null);
        }

        /// <summary>
        /// To String override
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Code + " : " + this.Name;
        }

        protected override object GetIdValue()
        {
            return this.Id;
        }

        #endregion
    }
}
