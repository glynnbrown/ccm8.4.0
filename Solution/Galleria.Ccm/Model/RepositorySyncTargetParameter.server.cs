﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model object that represents a repository sync target parameter
    /// </summary>
    public partial class RepositorySyncTargetParameter
    {
        #region Constructor
        private RepositorySyncTargetParameter() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the specified repository sync target parameter
        /// </summary>
        internal static RepositorySyncTargetParameter FetchRepositorySyncTarget(IDalContext dalContext, RepositorySyncTargetParameterDto dto)
        {
            return DataPortal.FetchChild<RepositorySyncTargetParameter>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private RepositorySyncTargetParameterDto GetDataTransferObject(RepositorySyncTarget parent)
        {
            return new RepositorySyncTargetParameterDto
            {
                Id = this.ReadProperty<Int32>(IdProperty),
                Name = this.ReadProperty<String>(NameProperty),
                Value = this.ReadProperty<String>(ValueProperty),
                RepositorySyncTargetId = parent.Id
            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        /// <Param name="dalContext"> The current dal context </param>
        private void LoadDataTransferObject(IDalContext dalContext, RepositorySyncTargetParameterDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(ValueProperty, dto.Value);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when loading a parameter from a dto
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="id">The id of the </param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, RepositorySyncTargetParameterDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #endregion
    }
}
