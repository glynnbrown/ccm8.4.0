﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//	Created (Auto-generated)
// V8-25556 : D.Pleasance
//  Added FetchAllChildGroups
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// ProductGroup Model object
    /// </summary>
    [Serializable]
    public sealed partial class ProductGroup : ModelObject<ProductGroup>
    {

        #region Static Constructor
        static ProductGroup()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns the parent group of this
        /// </summary>
        public ProductGroup ParentGroup
        {
            get
            {
                ProductGroupList list = base.Parent as ProductGroupList;
                if (list != null)
                {
                    return list.Parent as ProductGroup;
                }
                return null;
            }
        }

        /// <summary>
        /// Returns the parent hierarchy of this
        /// </summary>
        public ProductHierarchy ParentHierarchy
        {
            get
            {
                ProductHierarchy parentHierarchy = base.Parent as ProductHierarchy;
                if (parentHierarchy == null)
                {
                    ProductGroup currentParent = this.ParentGroup;
                    while (currentParent != null)
                    {
                        if (currentParent.ParentGroup != null)
                        {
                            currentParent = currentParent.ParentGroup;
                        }
                        else
                        {
                            return currentParent.Parent as ProductHierarchy;
                        }
                    }
                }
                else
                {
                    return parentHierarchy;
                }

                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region ProductLevelId

        /// <summary>
        /// ProductLevelId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProductLevelIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductLevelId);

        /// <summary>
        /// Gets/Sets the ProductLevelId value
        /// </summary>
        public Int32 ProductLevelId
        {
            get { return GetProperty<Int32>(ProductLevelIdProperty); }
            set { SetProperty<Int32>(ProductLevelIdProperty, value); }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set 
            { 
                SetProperty<String>(NameProperty, value);

                OnPropertyChanged(GroupLabelPropertyName);
            }
        }

        #endregion

        #region Code

        /// <summary>
        /// Code property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CodeProperty =
            RegisterModelProperty<String>(c => c.Code);

        /// <summary>
        /// Gets/Sets the Code value
        /// </summary>
        public String Code
        {
            get { return GetProperty<String>(CodeProperty); }
            set 
            { 
                SetProperty<String>(CodeProperty, value);

                OnPropertyChanged(GroupLabelPropertyName);
            }
        }

        #endregion

        #region DateCreated

        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        /// Gets/Sets the DateCreated value
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
            set { SetProperty<DateTime>(DateCreatedProperty, value); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        /// Gets/Sets the DateLastModified value
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
            set { SetProperty<DateTime>(DateLastModifiedProperty, value); }
        }

        #endregion

        #region DateDeleted

        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);

        /// <summary>
        /// Gets/Sets the DateDeleted value
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
            set { SetProperty<DateTime?>(DateDeletedProperty, value); }
        }

        #endregion

        #region ChildList

        public static readonly ModelPropertyInfo<ProductGroupList> ChildListProperty =
        RegisterModelProperty<ProductGroupList>(c => c.ChildList);
        /// <summary>
        /// The list of child product groups
        /// </summary>
        public ProductGroupList ChildList
        {
            get
            {
                return GetProperty<ProductGroupList>(ChildListProperty);
            }
        }

        #endregion

        #endregion

        #region Helper Properties

        /// <summary>
        /// Returns true if this is the root group.
        /// </summary>
        /// <returns></returns>
        public Boolean IsRoot
        {
            get
            {
                return this.ParentGroup == null
                    && this.ParentHierarchy != null;
            }
        }


        public static readonly String GroupLabelPropertyName = "GroupLabel";
        
        /// <summary>
        /// Returns a string containing code : name
        /// </summary>
        public String GroupLabel
        {
            get
            {
                if (IsRoot)
                {
                    //don't include code for root.
                    return this.Name;
                }
                else
                {
                    return this.Code + " : " + this.Name;
                }
            }
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));

            BusinessRules.AddRule(new Required(CodeProperty));
            BusinessRules.AddRule(new MaxLength(CodeProperty, 50));

        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ProductGroup), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(ProductGroup), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ProductHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(ProductGroup), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ProductHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(ProductGroup), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ProductHierarchyEdit.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static ProductGroup NewProductGroup(Int32 productLevelId)
        {
            ProductGroup item = new ProductGroup();
            item.Create(productLevelId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(Int32 productLevelId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            this.LoadProperty<Int32>(ProductLevelIdProperty, productLevelId);
            this.LoadProperty<ProductGroupList>(ChildListProperty, ProductGroupList.NewProductGroupList());
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// To String override
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Code + " : " + this.Name;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<ProductGroup>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes on this instance
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }

        /// <summary>
        /// Called when resolving ids on this instance
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // ProductLevelId
            Object productLevelId = context.ResolveId<ProductLevel>(this.ReadProperty<Int32>(ProductLevelIdProperty));
            if (productLevelId != null) this.LoadProperty<Int32>(ProductLevelIdProperty, (Int32)productLevelId);
        }

        /// <summary>
        /// Enumerates through this group and  all child groups below
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProductGroup> EnumerateAllChildGroups()
        {
            yield return this;

            foreach (ProductGroup child in this.ChildList)
            {
                foreach (ProductGroup g in child.EnumerateAllChildGroups())
                {
                    yield return g;
                }
            }
        }

        /// <summary>
        /// Creates a list of groups representing a path down to the requested child
        /// </summary>
        /// <returns></returns>
        public List<ProductGroup> GetParentPath()
        {
            List<ProductGroup> inversePathList = new List<ProductGroup>();
            ProductGroup currentParent = this.ParentGroup;

            while (currentParent != null)
            {
                inversePathList.Add(currentParent);
                currentParent = currentParent.ParentGroup;
            }

            //invert the list so the top parent is the first item
            List<ProductGroup> pathList = new List<ProductGroup>();
            int currentIndex = inversePathList.Count - 1;
            while (currentIndex >= 0)
            {
                pathList.Add(inversePathList[currentIndex]);
                currentIndex--;
            }

            return pathList;
        }

        /// <summary>
        /// Gets the levle associated with this group
        /// </summary>
        public ProductLevel GetAssociatedLevel()
        {
            Int32 levelId = this.ProductLevelId;

            foreach (ProductLevel level in this.ParentHierarchy.EnumerateAllLevels())
            {
                if (levelId == level.Id)
                {
                    return level;
                }
            }

            return null;
        }

        

        #endregion

        #region Helper Methods

        /// <summary>
        /// Returns a list  of this group and  all child groups below
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProductGroup> FetchAllChildGroups()
        {
            IEnumerable<ProductGroup> returnList =
                new List<ProductGroup>() { this };

            foreach (ProductGroup child in this.ChildList)
            {
                returnList = returnList.Union(child.FetchAllChildGroups());
            }

            return returnList;
        }

                /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos()
        {
            Type type = typeof(ProductGroup);
            String typeFriendly = Message.ProductGroup_FriendlyName;

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ProductGroup.CodeProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ProductGroup.NameProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ProductGroup.DateCreatedProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ProductGroup.DateLastModifiedProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ProductGroup.ProductLevelIdProperty);
        }

        #endregion
    }
}