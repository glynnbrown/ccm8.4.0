﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class WorkflowTaskList
    {
        #region Constructor
        private WorkflowTaskList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all tasks within the specified workflow
        /// </summary>
        internal static WorkflowTaskList FetchByWorkflowId(IDalContext dalContext, Int32 workflowId)
        {
            return DataPortal.FetchChild<WorkflowTaskList>(dalContext, workflowId);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returing all items for a given parent id
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, Int32 workflowId)
        {
            this.RaiseListChangedEvents = false;
            using (IWorkflowTaskDal dal = dalContext.GetDal<IWorkflowTaskDal>())
            {
                IEnumerable<WorkflowTaskDto> dtoList = dal.FetchByWorkflowId(workflowId);
                foreach (WorkflowTaskDto dto in dtoList)
                {
                    this.Add(WorkflowTask.GetWorkflowTask(dalContext, dto));
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
