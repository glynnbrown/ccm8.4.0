﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26159 : L.Ineson
//      Created
#endregion
#region Version History: (CCM 8.3.0)
// V8-31830 : A.Probyn
//  Updated to include GFSPerformanceSourceId, SelectionType, TimeType, DynamicValue
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Info object for PerformanceSelection
    /// </summary>
    [Serializable]
    public sealed partial class PerformanceSelectionInfo : ModelReadOnlyObject<PerformanceSelectionInfo>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The unique object id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The name of the selection
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }
        #endregion

        #region GFSPerformanceSourceId

        /// <summary>
        /// GFSPerformanceSourceId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> GFSPerformanceSourceIdProperty =
            RegisterModelProperty<Int32>(c => c.GFSPerformanceSourceId);

        /// <summary>
        /// Gets/Sets the GFSPerformanceSourceId value
        /// </summary>
        public Int32 GFSPerformanceSourceId
        {
            get { return GetProperty<Int32>(GFSPerformanceSourceIdProperty); }
        }

        #endregion

        #region TimeType

        /// <summary>
        /// TimeType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PerformanceSelectionTimeType> TimeTypeProperty =
            RegisterModelProperty<PerformanceSelectionTimeType>(c => c.TimeType);

        /// <summary>
        /// Gets/Sets the TimeType value
        /// </summary>
        public PerformanceSelectionTimeType TimeType
        {
            get { return GetProperty<PerformanceSelectionTimeType>(TimeTypeProperty); }
        }

        #endregion

        #region SelectionType

        /// <summary>
        /// SelectionType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PerformanceSelectionType> SelectionTypeProperty =
            RegisterModelProperty<PerformanceSelectionType>(c => c.SelectionType);

        /// <summary>
        /// Gets/Sets the SelectionType value
        /// </summary>
        public PerformanceSelectionType SelectionType
        {
            get { return GetProperty<PerformanceSelectionType>(SelectionTypeProperty); }
        }

        #endregion

        #region DynamicValue

        /// <summary>
        /// DynamicValue property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> DynamicValueProperty =
            RegisterModelProperty<Int32>(c => c.DynamicValue);

        /// <summary>
        /// Gets/Sets the DynamicValue value
        /// </summary>
        public Int32 DynamicValue
        {
            get { return GetProperty<Int32>(DynamicValueProperty); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PerformanceSelectionInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelectionInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelectionInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelectionInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Overrides

        public override String ToString()
        {
            return Name;
        }

        #endregion
    }
}
