﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-V8-24801 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class HighlightCharacteristic
    {
        #region Constructor
        private HighlightCharacteristic() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static HighlightCharacteristic Fetch(IDalContext dalContext, HighlightCharacteristicDto dto)
        {
            return DataPortal.FetchChild<HighlightCharacteristic>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, HighlightCharacteristicDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Boolean>(IsAndFilterProperty, dto.IsAndFilter);
            this.LoadProperty<HighlightCharacteristicRuleList>(RulesProperty, HighlightCharacteristicRuleList.FetchByParentId(dalContext, dto.Id));
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private HighlightCharacteristicDto GetDataTransferObject(Highlight parent)
        {
            return new HighlightCharacteristicDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                HighlightId = parent.Id,
                Name = ReadProperty<String>(NameProperty),
                IsAndFilter = ReadProperty<Boolean>(IsAndFilterProperty),

            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, HighlightCharacteristicDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, Highlight parent)
        {
            HighlightCharacteristicDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IHighlightCharacteristicDal dal = dalContext.GetDal<IHighlightCharacteristicDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<HighlightCharacteristic>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, Highlight parent)
        {
            if (this.IsSelfDirty)
            {
                using (IHighlightCharacteristicDal dal = dalContext.GetDal<IHighlightCharacteristicDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, Highlight parent)
        {
            using (IHighlightCharacteristicDal dal = dalContext.GetDal<IHighlightCharacteristicDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion

    }
}