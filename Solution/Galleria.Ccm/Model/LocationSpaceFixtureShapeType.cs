﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    public enum LocationSpaceFixtureShapeType
    {
        Rectangle,
        Irregular,
        Square,
        TriangleRightAngled,
        TriangleIsosceles,
        TriangleScalene,
        TriangleEquilateral,
        Circle,
        HalfCircle,
        Oval,
        Pentagon,
        Hexagon,
        Heptagon,
        Octagon,
        Decagon,
        Dodecagon
    }

    public static class LocationSpaceFixtureShapeTypeHelper
    {
        public static readonly Dictionary<LocationSpaceFixtureShapeType, String> FriendlyNames =
           new Dictionary<LocationSpaceFixtureShapeType, String>()
            {
                {LocationSpaceFixtureShapeType.Rectangle, Message.Enum_LocationSpaceFixtureShapeType_Rectangle},
                {LocationSpaceFixtureShapeType.Irregular, Message.Enum_LocationSpaceFixtureShapeType_Irregular},
                {LocationSpaceFixtureShapeType.Square, Message.Enum_LocationSpaceFixtureShapeType_Square},
                {LocationSpaceFixtureShapeType.TriangleRightAngled, Message.Enum_LocationSpaceFixtureShapeType_TriangleRightAngled},
                {LocationSpaceFixtureShapeType.TriangleIsosceles, Message.Enum_LocationSpaceFixtureShapeType_TriangleIsosceles},
                {LocationSpaceFixtureShapeType.TriangleScalene, Message.Enum_LocationSpaceFixtureShapeType_TriangleScalene},
                {LocationSpaceFixtureShapeType.TriangleEquilateral, Message.Enum_LocationSpaceFixtureShapeType_TriangleEquilateral},
                {LocationSpaceFixtureShapeType.Circle, Message.Enum_LocationSpaceFixtureShapeType_Circle},
                {LocationSpaceFixtureShapeType.HalfCircle, Message.Enum_LocationSpaceFixtureShapeType_HalfCircle},
                {LocationSpaceFixtureShapeType.Oval, Message.Enum_LocationSpaceFixtureShapeType_Oval},
                {LocationSpaceFixtureShapeType.Pentagon, Message.Enum_LocationSpaceFixtureShapeType_Pentagon},
                {LocationSpaceFixtureShapeType.Hexagon, Message.Enum_LocationSpaceFixtureShapeType_Hexagon},
                {LocationSpaceFixtureShapeType.Heptagon, Message.Enum_LocationSpaceFixtureShapeType_Heptagon},
                {LocationSpaceFixtureShapeType.Octagon, Message.Enum_LocationSpaceFixtureShapeType_Octagon},
                {LocationSpaceFixtureShapeType.Decagon, Message.Enum_LocationSpaceFixtureShapeType_Decagon},
                {LocationSpaceFixtureShapeType.Dodecagon, Message.Enum_LocationSpaceFixtureShapeType_Dodecagon}                
            };

        /// <summary>
        /// Dictionary with Friendly Name as key and FixtureShapeType as value
        /// </summary>
        public static readonly Dictionary<String, LocationSpaceFixtureShapeType> EnumFromFriendlyName =
            new Dictionary<String, LocationSpaceFixtureShapeType>()
        {
            {Message.Enum_LocationSpaceFixtureShapeType_Rectangle.ToLowerInvariant(), LocationSpaceFixtureShapeType.Rectangle},
            {Message.Enum_LocationSpaceFixtureShapeType_Irregular.ToLowerInvariant(), LocationSpaceFixtureShapeType.Irregular},
            {Message.Enum_LocationSpaceFixtureShapeType_Square.ToLowerInvariant(), LocationSpaceFixtureShapeType.Square},
            {Message.Enum_LocationSpaceFixtureShapeType_TriangleRightAngled.ToLowerInvariant(), LocationSpaceFixtureShapeType.TriangleRightAngled},
            {Message.Enum_LocationSpaceFixtureShapeType_TriangleIsosceles.ToLowerInvariant(), LocationSpaceFixtureShapeType.TriangleIsosceles},
            {Message.Enum_LocationSpaceFixtureShapeType_TriangleScalene.ToLowerInvariant(), LocationSpaceFixtureShapeType.TriangleScalene},
            {Message.Enum_LocationSpaceFixtureShapeType_TriangleEquilateral.ToLowerInvariant(), LocationSpaceFixtureShapeType.TriangleEquilateral},
            {Message.Enum_LocationSpaceFixtureShapeType_Circle.ToLowerInvariant(), LocationSpaceFixtureShapeType.Circle},
            {Message.Enum_LocationSpaceFixtureShapeType_HalfCircle.ToLowerInvariant(), LocationSpaceFixtureShapeType.HalfCircle},
            {Message.Enum_LocationSpaceFixtureShapeType_Oval.ToLowerInvariant(), LocationSpaceFixtureShapeType.Oval},
            {Message.Enum_LocationSpaceFixtureShapeType_Pentagon.ToLowerInvariant(), LocationSpaceFixtureShapeType.Pentagon},
            {Message.Enum_LocationSpaceFixtureShapeType_Hexagon.ToLowerInvariant(), LocationSpaceFixtureShapeType.Hexagon},
            {Message.Enum_LocationSpaceFixtureShapeType_Heptagon.ToLowerInvariant(), LocationSpaceFixtureShapeType.Heptagon},
            {Message.Enum_LocationSpaceFixtureShapeType_Octagon.ToLowerInvariant(), LocationSpaceFixtureShapeType.Octagon},
            {Message.Enum_LocationSpaceFixtureShapeType_Decagon.ToLowerInvariant(), LocationSpaceFixtureShapeType.Decagon},
            {Message.Enum_LocationSpaceFixtureShapeType_Dodecagon.ToLowerInvariant(), LocationSpaceFixtureShapeType.Dodecagon}
        };
    }
}
