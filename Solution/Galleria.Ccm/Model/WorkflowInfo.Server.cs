﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
// V8-25868 : L.Ineson
//  Added Description
// V8-25787 : N.Foster
//  Added RowVersion property
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class WorkflowInfo
    {
        #region Constructor
        private WorkflowInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an instance from a dto
        /// </summary>
        internal static WorkflowInfo GetWorkflowInfo(IDalContext dalContext, WorkflowInfoDto dto)
        {
            return DataPortal.FetchChild<WorkflowInfo>(dalContext, dto);
        }

        /// <summary>
        /// Returns the specified workflow info
        /// </summary>
        /// <param name="workflowId"></param>
        /// <returns></returns>
        public static WorkflowInfo FetchWorkflowInfoById(Int32 workflowId)
        {
            return DataPortal.Fetch<WorkflowInfo>(new SingleCriteria<Int32>(workflowId));
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, WorkflowInfoDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(DescriptionProperty, dto.Description);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, WorkflowInfoDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IWorkflowInfoDal dal = dalContext.GetDal<IWorkflowInfoDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.Value));
                }
            }
        }
        #endregion

        #endregion
    }
}
