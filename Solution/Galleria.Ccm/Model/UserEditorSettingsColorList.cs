﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30791 : D.Pleasance
//	Created
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using System.Linq;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// List of user setting colors
    /// </summary>
    [Serializable]
    public sealed partial class UserEditorSettingsColorList : ModelList<UserEditorSettingsColorList, UserEditorSettingsColor>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>a new list</returns>
        public static UserEditorSettingsColorList NewList()
        {
            UserEditorSettingsColorList list = new UserEditorSettingsColorList();
            list.Create();
            return list;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Adds the recent user color
        /// </summary>
        /// <param name="product">The color to add</param>
        public void Add(Int32 color)
        {
            this.Add(UserEditorSettingsColor.NewUserEditorSettingsColor(color));
        }

        /// <summary>
        /// Removes the user color
        /// </summary>
        /// <param name="color">The color to remove</param>
        public void Remove(Int32 color)
        {
            UserEditorSettingsColor removeUserEditorSettingsColor =
                this.FirstOrDefault(p => p.Color == color);

            if (removeUserEditorSettingsColor != null)
            {
                this.Remove(removeUserEditorSettingsColor);
            }
        }

        #endregion
    }
}
