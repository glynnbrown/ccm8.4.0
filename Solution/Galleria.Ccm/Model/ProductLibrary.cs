﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25395 : A.Probyn
//		Created (Auto-generated)
// V8-25526 : K.Pickup
//  Use RelationshipTypes.LazyLoad when registering lazy-loaded model properties.
// V8-27986 : A.Probyn ~ Set StartAtRow to 1 on Create
#endregion

#region Version History: CCM 801
// V8-27897 : A.Kuszyk
//  Copied Populate method from view model.
#endregion

#region Version History: CCM 830
//V8-32361 : L.Ineson
//  Added entityid and name.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Csla;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Imports;
using Galleria.Framework.Model;
using Csla.Rules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{

    /// <summary>
    /// ProductLibrary model object
    /// </summary>
    [Serializable]
    public sealed partial class ProductLibrary : ModelObject<ProductLibrary>
    {
        #region Properties

        /// <summary>
        /// Returns the file extension to use for print templates.
        /// </summary>
        public static String FileExtension
        {
            get { return Constants.ProductLibraryFileExtension; }
        }

        #region DalFactoryName
        /// <summary>
        /// DalFactoryName property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> DalFactoryNameProperty =
            RegisterModelProperty<String>(c => c.DalFactoryName);
        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        protected override string DalFactoryName
        {
            get { return this.GetProperty<String>(DalFactoryNameProperty); }
        }
        #endregion

        #region DalFactoryType

        // This property exists to tell the model object which dal type it and its children
        // are using.

        /// <summary>
        /// DalFactoryType property definition
        /// </summary>
        private static readonly ModelPropertyInfo<ProductLibraryDalFactoryType> DalFactoryTypeProperty =
            RegisterModelProperty<ProductLibraryDalFactoryType>(c => c.DalFactoryType);

        /// <summary>
        /// Returns the dal factory type
        /// </summary>
        private ProductLibraryDalFactoryType DalFactoryType
        {
            get { return this.GetProperty<ProductLibraryDalFactoryType>(DalFactoryTypeProperty); }
        }

        #endregion

        #region IsReadOnly

        private static readonly ModelPropertyInfo<Boolean> IsReadOnlyProperty =
             RegisterModelProperty<Boolean>(c => c.IsReadOnly);

        /// <summary>
        /// Returns true if this model has been loaded as readonly.
        /// </summary>
        public Boolean IsReadOnly
        {
            get { return this.GetProperty<Boolean>(IsReadOnlyProperty); }
        }

        #endregion

        #region IsFile

        private static readonly ModelPropertyInfo<Boolean> IsFileProperty =
             RegisterModelProperty<Boolean>(c => c.IsFile);

        /// <summary>
        /// Returns true if this model has been loaded from a file.
        /// </summary>
        public Boolean IsFile
        {
            get { return this.GetProperty<Boolean>(IsFileProperty); }
        }

        #endregion



        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Object> IdProperty =
            RegisterModelProperty<Object>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Object Id
        {
            get { return this.GetProperty<Object>(IdProperty); }
            set { SetProperty<Object>(IdProperty, value); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The row version timestamp
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region EntityId

        /// <summary>
        /// EntityId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);

        /// <summary>
        /// Gets/Sets the EntityId value
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
            set { SetProperty<Int32>(EntityIdProperty, value); }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name, Message.Generic_Name);

        /// <summary>
        /// Gets/Sets the name of the template
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region FileType

        /// <summary>
        /// FileType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<ImportDefinitionFileType> FileTypeProperty =
            RegisterModelProperty<ImportDefinitionFileType>(c => c.FileType);

        /// <summary>
        /// Gets/Sets the FileType value
        /// </summary>
        public ImportDefinitionFileType FileType
        {
            get { return GetProperty<ImportDefinitionFileType>(FileTypeProperty); }
            set { SetProperty<ImportDefinitionFileType>(FileTypeProperty, value); }
        }

        #endregion

        #region ExcelWorkbookSheet

        /// <summary>
        /// ExcelWorkbookSheet property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ExcelWorkbookSheetProperty =
            RegisterModelProperty<String>(c => c.ExcelWorkbookSheet);

        /// <summary>
        /// Gets/Sets the ExcelWorkbookSheet value
        /// </summary>
        public String ExcelWorkbookSheet
        {
            get { return GetProperty<String>(ExcelWorkbookSheetProperty); }
            set { SetProperty<String>(ExcelWorkbookSheetProperty, value); }
        }

        #endregion

        #region StartAtRow

        /// <summary>
        /// StartAtRow property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> StartAtRowProperty =
            RegisterModelProperty<Int32>(c => c.StartAtRow);

        /// <summary>
        /// Gets/Sets the StartAtRow value
        /// </summary>
        public Int32 StartAtRow
        {
            get { return GetProperty<Int32>(StartAtRowProperty); }
            set { SetProperty<Int32>(StartAtRowProperty, value); }
        }

        #endregion

        #region IsFirstRowHeaders

        /// <summary>
        /// IsFirstRowHeaders property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsFirstRowHeadersProperty =
            RegisterModelProperty<Boolean>(c => c.IsFirstRowHeaders);

        /// <summary>
        /// Gets/Sets the IsFirstRowHeaders value
        /// </summary>
        public Boolean IsFirstRowHeaders
        {
            get { return GetProperty<Boolean>(IsFirstRowHeadersProperty); }
            set { SetProperty<Boolean>(IsFirstRowHeadersProperty, value); }
        }

        #endregion

        #region ColumnMappings

        /// <summary>
        /// ColumnMappings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<ProductLibraryColumnMappingList> ColumnMappingsProperty =
            RegisterModelProperty<ProductLibraryColumnMappingList>(c => c.ColumnMappings);

        /// <summary>
        /// Gets the collection of child ColumnMappings
        /// </summary>
        public ProductLibraryColumnMappingList ColumnMappings
        {
            get { return GetProperty<ProductLibraryColumnMappingList>(ColumnMappingsProperty); }
        }

        #endregion

        #region TextDataFormat

        /// <summary>
        /// The format of the data to be imported
        /// (Text file only)
        /// </summary>
        public static readonly ModelPropertyInfo<ImportDefinitionTextDataFormatType?> TextDataFormatProperty =
            RegisterModelProperty<ImportDefinitionTextDataFormatType?>(c => c.TextDataFormat);
        public ImportDefinitionTextDataFormatType? TextDataFormat
        {
            get { return GetProperty<ImportDefinitionTextDataFormatType?>(TextDataFormatProperty); }
            set { SetProperty<ImportDefinitionTextDataFormatType?>(TextDataFormatProperty, value); }
        }

        #endregion

        #region TextDelimiterType

        /// <summary>
        /// The type of the delimiter used within the import file to separate data
        /// (Delimited text file only)
        /// </summary>
        public static readonly ModelPropertyInfo<ImportDefinitionTextDelimiterType?> TextDelimiterTypeProperty =
            RegisterModelProperty<ImportDefinitionTextDelimiterType?>(c => c.TextDelimiterType);
        public ImportDefinitionTextDelimiterType? TextDelimiterType
        {
            get { return GetProperty<ImportDefinitionTextDelimiterType?>(TextDelimiterTypeProperty); }
            set { SetProperty<ImportDefinitionTextDelimiterType?>(TextDelimiterTypeProperty, value); }
        }

        #endregion

        #region DateCreated

        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        /// Gets/Sets the DateCreated value
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
            set { SetProperty<DateTime>(DateCreatedProperty, value); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        /// Gets/Sets the DateLastModified value
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
            set { SetProperty<DateTime>(DateLastModifiedProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository

        /// <summary>
        /// Returns a dictionary of the current role permissions allocated to the user for this object type.
        /// </summary>
        /// <returns></returns>
        public static IDictionary<AuthorizationActions, Boolean> GetUserPermissions()
        {
            return new Dictionary<AuthorizationActions, Boolean>()
            {
                {AuthorizationActions.CreateObject, ApplicationContext.User.IsInRole(DomainPermission.Authenticated.ToString())},
                {AuthorizationActions.GetObject, ApplicationContext.User.IsInRole(DomainPermission.Authenticated.ToString())},
                {AuthorizationActions.EditObject, ApplicationContext.User.IsInRole(DomainPermission.Authenticated.ToString())},
                {AuthorizationActions.DeleteObject, ApplicationContext.User.IsInRole(DomainPermission.Authenticated.ToString())},
            };
        }

        #endregion

        #region Criteria

        #region FetchByIdCriteria

        // <summary>
        /// Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region DalType
            /// <summary>
            /// DalType property definition
            /// </summary>
            public static readonly PropertyInfo<ProductLibraryDalFactoryType> DalTypeProperty =
                RegisterProperty<ProductLibraryDalFactoryType>(c => c.DalType);
            /// <summary>
            /// Returns the dal type
            /// </summary>
            public ProductLibraryDalFactoryType DalType
            {
                get { return this.ReadProperty<ProductLibraryDalFactoryType>(DalTypeProperty); }
            }
            #endregion

            #region Id property

            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);

            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }

            #endregion

            #region AsReadOnly

            /// <summary>
            /// AsReadOnly property definition
            /// </summary>
            public static readonly PropertyInfo<Boolean> AsReadOnlyProperty =
                RegisterProperty<Boolean>(c => c.AsReadOnly);
            /// <summary>
            /// If true the model will be loaded as readonly.
            /// </summary>
            public Boolean AsReadOnly
            {
                get { return this.ReadProperty<Boolean>(AsReadOnlyProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(ProductLibraryDalFactoryType sourceType, Object id, Boolean asReadOnly)
            {
                this.LoadProperty<ProductLibraryDalFactoryType>(DalTypeProperty, sourceType);
                this.LoadProperty<Object>(IdProperty, id);
                this.LoadProperty<Boolean>(AsReadOnlyProperty, asReadOnly);
            }
            #endregion
        }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new ProductLibrary
        /// </summary>
        public static ProductLibrary NewProductLibrary()
        {
            ProductLibrary item = new ProductLibrary();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new ProductLibrary
        /// </summary>
        public static ProductLibrary NewProductLibrary(Int32 entityId)
        {
            ProductLibrary item = new ProductLibrary();
            item.Create(entityId);
            return item;
        }

        /// <summary>
        /// Locks a ProductLibrary so that it cannot be
        /// edited or opened by another process
        /// </summary>
        internal static void LockProductLibraryByFileName(String fileName)
        {
            DataPortal.Execute<LockProductLibraryCommand>(new LockProductLibraryCommand(ProductLibraryDalFactoryType.FileSystem, fileName));
        }

        /// <summary>
        /// Unlocks a ProductLibrary
        /// </summary>
        public static void UnlockProductLibraryByFileName(String fileName)
        {
            DataPortal.Execute<UnlockByIdCommand>(new UnlockByIdCommand(ProductLibraryDalFactoryType.FileSystem, fileName));
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 entityId)
        {
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            Create();
        }


        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            
            this.LoadProperty<Int32>(StartAtRowProperty, 1);
            this.LoadProperty<ImportDefinitionFileType>(FileTypeProperty, ImportDefinitionFileType.Excel);
            this.LoadProperty<ProductLibraryColumnMappingList>(ColumnMappingsProperty, ProductLibraryColumnMappingList.NewProductLibraryColumnMappingList());


            base.Create();
        }

        #endregion

        #endregion

        #region Commands

        #region LockProductLibraryCommand
        /// <summary>
        /// Performs the locking of a ProductLibrary
        /// </summary>
        [Serializable]
        private partial class LockProductLibraryCommand : CommandBase<LockProductLibraryCommand>
        {
            #region Properties

            #region DalType
            /// <summary>
            /// DalType property definition
            /// </summary>
            public static readonly PropertyInfo<ProductLibraryDalFactoryType> DalTypeProperty =
                RegisterProperty<ProductLibraryDalFactoryType>(c => c.DalType);
            /// <summary>
            /// Returns the dal factory type
            /// </summary>
            public ProductLibraryDalFactoryType DalType
            {
                get { return this.ReadProperty<ProductLibraryDalFactoryType>(DalTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public LockProductLibraryCommand(ProductLibraryDalFactoryType dalType, Object id)
            {
                this.LoadProperty<ProductLibraryDalFactoryType>(DalTypeProperty, dalType);
                this.LoadProperty<Object>(IdProperty, id);
            }
            #endregion
        }
        #endregion

        #region UnlockByIdCommand
        /// <summary>
        /// Performs the unlocking of a product library
        /// </summary>
        [Serializable]
        private partial class UnlockByIdCommand : CommandBase<UnlockByIdCommand>
        {
            #region Properties

            #region Type
            /// <summary>
            /// SourceType property definition
            /// </summary>
            public static readonly PropertyInfo<ProductLibraryDalFactoryType> TypeProperty =
                RegisterProperty<ProductLibraryDalFactoryType>(c => c.Type);
            /// <summary>
            /// Returns the package source type
            /// </summary>
            public ProductLibraryDalFactoryType Type
            {
                get { return this.ReadProperty<ProductLibraryDalFactoryType>(TypeProperty); }
            }
            #endregion

            #region Ids property

            /// <summary>
            /// Ids property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);

            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }

            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public UnlockByIdCommand(ProductLibraryDalFactoryType sourceType, Object id)
            {
                this.LoadProperty<ProductLibraryDalFactoryType>(TypeProperty, sourceType);
                this.LoadProperty<Object>(IdProperty, id);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns a string representation of this object
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            //Only show id if its been reesolved
            if (!this.IsNew)
            {
                return String.Format("{0}", this.Id);
            }
            return Message.ProductLibrary_DefaultName;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<ProductLibrary>(oldId, newId);
        }


        public override ProductLibrary Save()
        {
            if (this.IsReadOnly)
            {
                throw new InvalidOperationException();
            }

            return base.Save();
        }

        /// <summary>
        /// Provide a SaveAs method to allow saving to the repository.
        /// This resets DalFactoryType and DalFactoryName to allow saving saving to the repository,
        /// otherwise a ProductLibrary instance loaded from a file tries to save back to a file.
        /// </summary>
        public ProductLibrary SaveAsToRepository(Int32 entityId)
        {
            ProductLibrary returnValue = null;

            //if this model is not new and was fetched from another file
            // take a note of the old file name so we can unlock it after
            String oldFileName = null;
            if (!IsNew && DalFactoryType == ProductLibraryDalFactoryType.FileSystem)
            {
                oldFileName = this.Id as String;
            }

            //Mark this as a new item.
            if (!IsNew) MarkGraphAsNew();

            //Load the new details
            LoadProperty<ProductLibraryDalFactoryType>(DalFactoryTypeProperty, ProductLibraryDalFactoryType.Unknown);
            LoadProperty<String>(DalFactoryNameProperty, DalContainer.DalName);
            LoadProperty<Int32>(EntityIdProperty, entityId);
            LoadProperty<Boolean>(IsReadOnlyProperty, false);
            LoadProperty<Boolean>(IsFileProperty, false);


            //save
            returnValue = Save();

            //unlock the old file.
            if (!String.IsNullOrEmpty(oldFileName))
            {
                UnlockProductLibraryByFileName(oldFileName);
            }

            return returnValue;
        }

        /// <summary>
        /// Saves this to a new file.
        /// </summary>
        public ProductLibrary SaveAsFile(String path)
        {
            ProductLibrary returnValue = null;
            String fileName = path;

            //make sure the file extension is correct.
            fileName = Path.ChangeExtension(path, FileExtension);

            //if this model is not new and was fetched from another file
            // take a note of the old file name.
            String oldFileName = null;
            if ((!IsNew) && (this.Id as String != fileName))
            {
                if (DalFactoryType == ProductLibraryDalFactoryType.FileSystem)
                {
                    oldFileName = this.Id as String;
                }
            }

            if (!IsNew) MarkGraphAsNew();

            LoadProperty<ProductLibraryDalFactoryType>(DalFactoryTypeProperty, ProductLibraryDalFactoryType.FileSystem);
            LoadProperty<String>(DalFactoryNameProperty, "");
            LoadProperty<Object>(IdProperty, fileName);
            LoadProperty<String>(NameProperty, Path.GetFileNameWithoutExtension(fileName));
            LoadProperty<Boolean>(IsReadOnlyProperty, false);
            LoadProperty<Boolean>(IsFileProperty, true);
            returnValue = Save();

            //unlock the old file.
            if (!String.IsNullOrEmpty(oldFileName))
            {
                UnlockProductLibraryByFileName(oldFileName);
            }

            //Make sure id is the file name for file system, dal is returning 1
            this.LoadProperty<Object>(IdProperty, fileName);
            return returnValue;
        }

        /// <summary>
        /// Populates the product library from the given parameters.
        /// </summary>
        /// <param name="exists">True indicates that this is an existing product library.</param>
        /// <param name="selectedWorksheet"></param>
        /// <param name="importFileData">The column separated file data.</param>
        /// <param name="isFixedWidth">True indiciates that fixed width is selected.</param>
        public void Populate(Boolean exists, String selectedWorksheet, ImportFileData importFileData, Boolean isFixedWidth)
        {
            this.ExcelWorkbookSheet = selectedWorksheet;
            if (this.FileType == ImportDefinitionFileType.Text)
            {
                this.ExcelWorkbookSheet = String.Empty;
                //text data format populated only for text file types
                this.TextDataFormat = isFixedWidth
                    ? ImportDefinitionTextDataFormatType.Fixed : ImportDefinitionTextDataFormatType.Delimited;

                if (isFixedWidth)
                {
                    this.TextDelimiterType = null;
                }
            }
            else if (this.FileType == ImportDefinitionFileType.Excel)
            {
                this.TextDataFormat = null;
                this.TextDelimiterType = null;
            }

            //Retrieve mapped columns
            IEnumerable<ImportMapping> mappedColumnsList = 
                importFileData.MappingList.Where(
                    m => !String.IsNullOrEmpty(m.ColumnReference) // customer source column was mapped
                        || (m.CanDefaultData == true && m.PropertyDefault != null)); // no source but our destination should be defaulted

            if (!exists)
            {
                //GFS-23788 : Clear any old column mappings if they exist - possibly during a SaveAs where the original file's mappings
                //are already added. We must clear these otherwise 2 versions of each mapping will be stored, one with the original template's
                //saved values and one with any changes made since the last save.
                this.ColumnMappings.Clear();
            }

            //Populate column mappings
            foreach (ImportMapping mapping in mappedColumnsList)
            {
                ProductLibraryColumnMapping importColumnMapping = null;
                Boolean willRequireAddingAsNew = true;
                if (exists)
                {
                    importColumnMapping = this.ColumnMappings.Where(p => p.Destination == mapping.PropertyName).FirstOrDefault();
                    willRequireAddingAsNew = importColumnMapping == null;
                }

                if (!exists || importColumnMapping == null) //fresh definition or could not find mapping
                {
                    importColumnMapping = ProductLibraryColumnMapping.NewProductLibraryColumnMapping();
                }

                importColumnMapping.CanDefault = mapping.CanDefaultData;
                importColumnMapping.Default = mapping.PropertyDefault != null ? mapping.PropertyDefault.ToString() : null;
                importColumnMapping.Destination = mapping.PropertyName; // destination = model object property
                importColumnMapping.Max = mapping.PropertyMax;
                importColumnMapping.Min = mapping.PropertyMin;
                importColumnMapping.Source = mapping.ColumnReference; //source = customer data column
                //format property type 
                if (mapping.PropertyType.ToString().Contains("System.Nullable`1"))
                {
                    importColumnMapping.Type = mapping.PropertyType.ToString().Substring(25).TrimEnd(']') + "?";
                }
                else
                {
                    importColumnMapping.Type = mapping.PropertyType.Name;
                }

                //Only mapp fixed width column start/end if column is also set
                if (mapping.IsColumnReferenceSet)
                {
                    //temporary workaround the fact that the galleria framework has FixedWidthColumnStart noted as nullable
                    if (mapping.FixedWidthColumnStart != null)
                    {
                        importColumnMapping.TextFixedWidthColumnStart = (Int32)mapping.FixedWidthColumnStart;
                    }
                    else
                    {
                        importColumnMapping.TextFixedWidthColumnStart = 0;
                    }

                    //temporary workaround the fact that the galleria framework has FixedWidthColumnStart noted as nullable
                    if (mapping.FixedWidthColumnEnd != null)
                    {
                        importColumnMapping.TextFixedWidthColumnEnd = (Int32)mapping.FixedWidthColumnEnd;
                    }
                    else
                    {
                        importColumnMapping.TextFixedWidthColumnEnd = 0;
                    }
                }

                if (willRequireAddingAsNew) //!exists)
                {
                    //add column to ImportColumnMappings
                    this.ColumnMappings.Add(importColumnMapping);
                }
            }
        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    if (!IsNew)
                    {
                        if (this.DalFactoryType == ProductLibraryDalFactoryType.FileSystem
                            && this.Id is String)
                        {
                            UnlockProductLibraryByFileName((String)Id);
                            LoadProperty<Boolean>(IsReadOnlyProperty, true);
                        }
                    }
                }
                _isDisposed = true;
            }
        }

        #endregion

    }

    /// <summary>
    /// Denotes the available dalfactories types.
    /// </summary>
    public enum ProductLibraryDalFactoryType
    {
        Unknown = 0,
        FileSystem = 1,
    }

    /// <summary>
    /// Denotes the type of default product library source
    /// </summary>
    public enum DefaultProductLibrarySourceType
    {
        Unknown = 0,
        ProductLibrary = 1,
        ProductUniverse = 2
    }

    /// <summary>
    /// DefaultProductLibrarySourceType Helper Class
    /// </summary>
    public static class DefaultProductLibrarySourceTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<DefaultProductLibrarySourceType, String> FriendlyNames =
            new Dictionary<DefaultProductLibrarySourceType, String>()
            {
                {DefaultProductLibrarySourceType.Unknown, String.Empty},
                {DefaultProductLibrarySourceType.ProductLibrary, Message.Enum_DefaultProductLibrarySourceType_ProductLibrary},
                {DefaultProductLibrarySourceType.ProductUniverse, Message.Enum_DefaultProductLibrarySourceType_ProductUniverse},
            };
    }
}