﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25886 : L.Ineson
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Engine;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    public partial class EngineTaskParameterEnumValueInfoList
    {
        #region Constructors
        private EngineTaskParameterEnumValueInfoList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all parameters for the specified task
        /// </summary>
        public static EngineTaskParameterEnumValueInfoList GetEngineTaskParameterEnumValueInfoList(IEnumerable<TaskParameterEnumValue> parameterValues)
        {
            return DataPortal.FetchChild<EngineTaskParameterEnumValueInfoList>(parameterValues);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when fetching a list of this type
        /// </summary>
        private void Child_Fetch(IEnumerable<TaskParameterEnumValue> parameterValues)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            foreach (TaskParameterEnumValue parameterValue in parameterValues)
            {
                this.Add(EngineTaskParameterEnumValueInfo.GetEngineTaskParameterEnumValueInfo(parameterValue));
            }
            this.IsReadOnly = false;
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
