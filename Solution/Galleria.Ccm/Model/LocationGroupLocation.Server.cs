﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GFS 1.0)
// CCM-25445 : L.Ineson
//		Copied from GFS 
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Csla.Core;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    public partial class LocationGroupLocation
    {
        #region Constructors
        private LocationGroupLocation() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <returns>An existing unit</returns>
        internal static LocationGroupLocation Fetch(IDalContext dalContext, LocationGroupLocationDto dto)
        {
            return DataPortal.FetchChild<LocationGroupLocation>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private LocationGroupLocationDto GetDataTransferObject()
        {
            return new LocationGroupLocationDto
            {
                LocationId = ReadProperty<Int16>(LocationIdProperty),
                Name = ReadProperty<String>(NameProperty),
                Code = ReadProperty<String>(CodeProperty),
                LocationGroupId = this.ParentLocationGroupLocationList.ParentLocationGroup.Id
            };
        }


        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, LocationGroupLocationDto dto)
        {
            LoadProperty<Int16>(LocationIdProperty, dto.LocationId);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<String>(CodeProperty, dto.Code);
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning an existing item from a provided dto
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, LocationGroupLocationDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert

        //INSERT IS NOT ALLOWED! 
        //This item is inserted as a Location object.

        #endregion

        #region Update
        /// <summary>
        /// Called when updating an object of this type
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent model</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            if (this.IsSelfDirty)
            {
                using (ILocationGroupLocationDal dal = dalContext.GetDal<ILocationGroupLocationDal>())
                {
                    dal.Update(this.GetDataTransferObject());
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this object is deleting itself
        /// NOTE - THIS WILL CALL THE LOCATION DELETE
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
            {
                dal.DeleteById(this.LocationId);
            }
        }
        #endregion

        #endregion
    }
}
