﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
// V8-25868 : L.Ineson
//  Added Description property
#endregion
#region Verison History : CCM811
// V8-30538 : M.Brumby
//  Save dirty fetches
#endregion
#region Verison History : CCM820
// V8-30830 : L.Luong
//  Added try catch when saving dirty fetches as it conflicts with readonly rights
// V8-31246 : A.Kuszyk
//  Added InsertUsingExistingContext to allow saving with a specific dal context.
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class Workflow
    {
        #region Constructors
        private Workflow() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the specified workflow
        /// </summary>
        public static Workflow FetchById(Int32 id)
        {            
            Workflow output = DataPortal.Fetch<Workflow>(new SingleCriteria<Int32>(id));
            //Missing parameters are now loaded in during the fetch.
            //If this is the case then we need to save our workflow
            //and pass out the result so we have a non dirty fetch.
            if (output.IsDirty)
            {
                try
                {
                    return output.Save();
                }
                catch { }
            }
            return output;
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, WorkflowDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(DescriptionProperty, dto.Description);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);
            this.LoadProperty<WorkflowTaskList>(TasksProperty, WorkflowTaskList.FetchByWorkflowId(dalContext, dto.Id));
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private WorkflowDto GetDataTransferObject()
        {
            return new WorkflowDto()
            {
                Id = this.ReadProperty<Int32>(IdProperty),
                RowVersion = this.ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = this.ReadProperty<Int32>(EntityIdProperty),
                Name = this.ReadProperty<String>(NameProperty),
                Description = this.ReadProperty<String>(DescriptionProperty),
                DateCreated = this.ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = this.ReadProperty<DateTime>(DateLastModifiedProperty),
                DateDeleted = this.ReadProperty<DateTime?>(DateDeletedProperty)
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching a single instance by its id
        /// </summary>
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IWorkflowDal dal = dalContext.GetDal<IWorkflowDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.Value));
                }
            }
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this instance into the data store
        /// </summary>
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                WorkflowDto dto = this.GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (IWorkflowDal dal = dalContext.GetDal<IWorkflowDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<Workflow>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }

        /// <summary>
        /// Called when inserting this object and its children using an existing dal context.
        /// </summary>
        ///<remarks>used to insert default by sync process.</remarks>
        internal void InsertUsingExistingContext(IDalContext dalContext)
        {
            WorkflowDto dto = GetDataTransferObject();
            Object oldId = dto.Id;

            using (IWorkflowDal dal = dalContext.GetDal<IWorkflowDal>())
            {
                dal.Insert(dto);
            }

            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty(RowVersionProperty, dto.RowVersion);
            this.LoadProperty(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
            dalContext.RegisterId<Workflow>(oldId, dto.Id);

            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this instance into the data store
        /// </summary>
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                WorkflowDto dto = this.GetDataTransferObject();
                using (IWorkflowDal dal = dalContext.GetDal<IWorkflowDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this instance is being deleted
        /// </summary>
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IWorkflowDal dal = dalContext.GetDal<IWorkflowDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }
        #endregion

        #endregion
    }
}
