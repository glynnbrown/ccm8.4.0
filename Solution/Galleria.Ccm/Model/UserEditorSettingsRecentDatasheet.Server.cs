﻿  #region HeaderInformation
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31699 : A.heathcote
//  Created.
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;


namespace Galleria.Ccm.Model
{
    public partial class UserEditorSettingsRecentDatasheet
    {
        #region Constructor
        private UserEditorSettingsRecentDatasheet() { }   //force use of factory methods
        #endregion

        #region Factory Methods

        internal static UserEditorSettingsRecentDatasheet GetUserEditorSettingsRecentDatasheet(IDalContext dalContext, UserEditorSettingsRecentDatasheetDto dto)
        {
            return DataPortal.FetchChild<UserEditorSettingsRecentDatasheet>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object

        private UserEditorSettingsRecentDatasheetDto GetDataTransferObjects()
        {
            return new UserEditorSettingsRecentDatasheetDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                DatasheetId = ReadProperty<Object>(DatasheetIdProperty)
            };
        }

        private void LoadDataTransferObject(IDalContext dalcontext, UserEditorSettingsRecentDatasheetDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Object>(DatasheetIdProperty, dto.DatasheetId);

        }
        #endregion

        #region Fetch

        private void Child_Fetch(IDalContext dalcontext, UserEditorSettingsRecentDatasheetDto dto)
        {
            LoadDataTransferObject(dalcontext, dto);
        }

        #endregion

        #region Insert

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalcontext)
        {
            UserEditorSettingsRecentDatasheetDto dto = GetDataTransferObjects();
            Int32 oldId = dto.Id;
            using (IUserEditorSettingsRecentDatasheetDal dal = dalcontext.GetDal<IUserEditorSettingsRecentDatasheetDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalcontext.RegisterId<UserEditorSettingsRecentDatasheet>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalcontext, this);
        }

        #region Update
        /// <summary>
        /// Called when updating this object in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            if (this.IsSelfDirty)
            {
                using (IUserEditorSettingsRecentDatasheetDal dal = dalContext.GetDal<IUserEditorSettingsRecentDatasheetDal>())
                {
                    dal.Update(this.GetDataTransferObjects());
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IUserEditorSettingsRecentDatasheetDal dal = dalContext.GetDal<IUserEditorSettingsRecentDatasheetDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion
    }
        #endregion
}
