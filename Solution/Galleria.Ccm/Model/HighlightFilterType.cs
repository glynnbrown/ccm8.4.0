﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24801 L.Hodson
//  Created
#endregion

#region Version History: (CCM 8.01)
// V8-28181 : L.Luong
//  Added Contains
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Model
{
    public enum HighlightFilterType
    {
        Equals,
        NotEquals,
        LessThan,
        LessThanOrEqualTo,
        MoreThan,
        MoreThanOrEqualTo,
        Contains,
    }

    public static class HighlightFilterTypeHelper
    {
        public static readonly Dictionary<HighlightFilterType, String> FriendlyNames =
            new Dictionary<HighlightFilterType, String>()
            {
                {HighlightFilterType.Equals, "=" },
                {HighlightFilterType.NotEquals, "<>" },
                {HighlightFilterType.LessThan, "<" },
                {HighlightFilterType.LessThanOrEqualTo, "<=" },
                {HighlightFilterType.MoreThan, ">" },
                {HighlightFilterType.MoreThanOrEqualTo, ">=" },
                {HighlightFilterType.Contains, Message.HighlightType_Contains },
            };

        public static PlanogramHighlightFilterType ToPlanogramHighlightFilterType(this HighlightFilterType value)
        {
            switch (value)
            {
                default:
                case HighlightFilterType.Equals: return PlanogramHighlightFilterType.Equals;
                case HighlightFilterType.Contains: return PlanogramHighlightFilterType.Contains;
                case HighlightFilterType.LessThan: return PlanogramHighlightFilterType.LessThan;
                case HighlightFilterType.LessThanOrEqualTo: return PlanogramHighlightFilterType.LessThanOrEqualTo;
                case HighlightFilterType.MoreThan: return PlanogramHighlightFilterType.MoreThan;
                case HighlightFilterType.MoreThanOrEqualTo: return PlanogramHighlightFilterType.MoreThanOrEqualTo;
                case HighlightFilterType.NotEquals: return PlanogramHighlightFilterType.NotEquals;
            }
        }
    }
}
