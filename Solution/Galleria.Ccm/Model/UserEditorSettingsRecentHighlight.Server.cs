﻿#region HeaderInformation
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31699 : A.heathcote
//  Created.
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;


namespace Galleria.Ccm.Model
{
    public partial class UserEditorSettingsRecentHighlight
    {
        #region Constructor
        private UserEditorSettingsRecentHighlight() { }   //force use of factory methods
        #endregion

        #region Factory Methods

        internal static UserEditorSettingsRecentHighlight GetUserEditorSettingsRecentHighlight(IDalContext dalContext, UserEditorSettingsRecentHighlightDto dto)
        {
            return DataPortal.FetchChild<UserEditorSettingsRecentHighlight>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object

        private UserEditorSettingsRecentHighlightDto GetDataTransferObjects()
        {
            return new UserEditorSettingsRecentHighlightDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                SaveType = (Byte)ReadProperty<HighlightDalFactoryType>(SaveTypeProperty),
                HighlightId = ReadProperty<Object>(HighlightIdProperty)
            };
        }

        private void LoadDataTransferObject(IDalContext dalcontext, UserEditorSettingsRecentHighlightDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<HighlightDalFactoryType>(SaveTypeProperty, (HighlightDalFactoryType)dto.SaveType);
            LoadProperty<Object>(HighlightIdProperty, dto.HighlightId);

        }
        #endregion

        #region Fetch

        private void Child_Fetch(IDalContext dalcontext, UserEditorSettingsRecentHighlightDto dto)
        {
            LoadDataTransferObject(dalcontext, dto);
        }

        #endregion

        #region Insert

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalcontext)
        {
            UserEditorSettingsRecentHighlightDto dto = GetDataTransferObjects();
            Int32 oldId = dto.Id;
            using (IUserEditorSettingsRecentHighlightDal dal = dalcontext.GetDal<IUserEditorSettingsRecentHighlightDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalcontext.RegisterId<UserEditorSettingsRecentHighlight>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalcontext, this);
        }

        #region Update
        /// <summary>
        /// Called when updating this object in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            if (this.IsSelfDirty)
            {
                using (IUserEditorSettingsRecentHighlightDal dal = dalContext.GetDal<IUserEditorSettingsRecentHighlightDal>())
                {
                    dal.Update(this.GetDataTransferObjects());
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IUserEditorSettingsRecentHighlightDal dal = dalContext.GetDal<IUserEditorSettingsRecentHighlightDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion
    }
        #endregion
}
