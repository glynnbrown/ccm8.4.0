﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentMinorRevisionAmendDistributionActionList
    {
        #region Constructors
        private AssortmentMinorRevisionAmendDistributionActionList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an existing Assortment minor revision action list
        /// </summary>
        /// <param name="childData"></param>
        /// <returns>A new Assortment minor revision action list</returns>
        internal static AssortmentMinorRevisionAmendDistributionActionList FetchByAssortmentMinorRevisionId(IDalContext dalContext, Int32 AssortmentMinorRevisionId)
        {
            return DataPortal.FetchChild<AssortmentMinorRevisionAmendDistributionActionList>(dalContext, AssortmentMinorRevisionId);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="reviewId">The parent Assortment id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 AssortmentMinorRevisionContentId)
        {
            RaiseListChangedEvents = false;
            using (IAssortmentMinorRevisionAmendDistributionActionDal dal = dalContext.GetDal<IAssortmentMinorRevisionAmendDistributionActionDal>())
            {
                IEnumerable<AssortmentMinorRevisionAmendDistributionActionDto> dtoList = dal.FetchByAssortmentMinorRevisionId(AssortmentMinorRevisionContentId);
                foreach (AssortmentMinorRevisionAmendDistributionActionDto dto in dtoList)
                {
                    this.Add(AssortmentMinorRevisionAmendDistributionAction.FetchAssortmentMinorRevisionAmendDistributionAction(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
