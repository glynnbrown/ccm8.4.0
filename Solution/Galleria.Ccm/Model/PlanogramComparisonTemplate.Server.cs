﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.
// V8-32621 : A.Silva
//  Changed the visibility of InsertUsingExistingContext to Internal.

#endregion

#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramComparisonTemplate
    {
        #region Constructors

        /// <summary>
        ///     Private constructor. Use public factory methods to initialize new instances.
        /// </summary>
        private PlanogramComparisonTemplate() {}

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Fetch the corresponding <see cref="PlanogramComparisonTemplate"/> for the given <paramref name="id"/>.
        /// </summary>
        /// <returns>A new instance of <see cref="PlanogramComparisonTemplate"/> with the data contained assigned to the given <paramref name="id"/>.</returns>
        public static PlanogramComparisonTemplate FetchById(Object id)
        {
            return DataPortal.Fetch<PlanogramComparisonTemplate>(new FetchByIdCriteria(PlanogramComparisonTemplateDalType.Unknown, id));
        }

        /// <summary>
        ///     Fetch the corresponding <see cref="PlanogramComparisonTemplate"/> for the given <paramref name="id"/>.
        /// </summary>
        /// <returns>A new instance of <see cref="PlanogramComparisonTemplate"/> with the data contained assigned to the given <paramref name="id"/>.</returns>
        public static PlanogramComparisonTemplate FetchById(Object id, Boolean asReadOnly)
        {
            return DataPortal.Fetch<PlanogramComparisonTemplate>(new FetchByIdCriteria(PlanogramComparisonTemplateDalType.Unknown, id, asReadOnly));
        }

        /// <summary>
        ///     Fetch an existing <see cref="PlanogramComparisonTemplate"/> from the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">the path to the file containing the <see cref="PlanogramComparisonTemplate"/> to fetch.</param>
        /// <param name="readOnly">[Optional] Whether to open the file in read only mode or not (by default <c>false</c>).</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonTemplate"/> with the data contained in the specified file.</returns>
        public static PlanogramComparisonTemplate FetchByFilename(String fileName, Boolean readOnly = false)
        {
            return DataPortal.Fetch<PlanogramComparisonTemplate>(new FetchByIdCriteria(PlanogramComparisonTemplateDalType.FileSystem, fileName, readOnly));
        }

        /// <summary>
        ///     Fetch an existing <see cref="PlanogramComparisonTemplate"/> with the given entity id and name.
        /// </summary>
        public static PlanogramComparisonTemplate FetchByEntityIdName(Int32 entityId, String name)
        {
            return DataPortal.Fetch<PlanogramComparisonTemplate>(new FetchByEntityIdNameCriteria(entityId, name));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Creates a data transfer object from this instance
        /// </summary>
        private PlanogramComparisonTemplateDto GetDataTransferObject()
        {
            return new PlanogramComparisonTemplateDto
                   {
                       Id = ReadProperty(IdProperty),
                       EntityId = ReadProperty(EntityIdProperty),
                       RowVersion = ReadProperty(RowVersionProperty),
                       DateCreated = ReadProperty(DateCreatedProperty),
                       DateLastModified = ReadProperty(DateLastModifiedProperty),

                       Name = ReadProperty(NameProperty),
                       Description = ReadProperty(DescriptionProperty),
                       IgnoreNonPlacedProducts = ReadProperty(IgnoreNonPlacedProductsProperty),
                       DataOrderType = (Byte) ReadProperty(DataOrderTypeProperty)
                   };
        }

        /// <summary>
        ///     Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramComparisonTemplateDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(EntityIdProperty, dto.EntityId);
            LoadProperty(RowVersionProperty, dto.RowVersion);
            LoadProperty(DateCreatedProperty, dto.DateCreated);
            LoadProperty(DateLastModifiedProperty, dto.DateLastModified);

            LoadProperty(NameProperty, dto.Name);
            LoadProperty(DescriptionProperty, dto.Description);
            LoadProperty(IgnoreNonPlacedProductsProperty, dto.IgnoreNonPlacedProducts);
            LoadProperty(DataOrderTypeProperty, dto.DataOrderType);
            LoadProperty(ComparisonFieldsProperty, PlanogramComparisonTemplateFieldList.FetchByParentId(dalContext, dto.Id));
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Called when returning FetchById
        /// </summary>
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            if (criteria.DalFactoryType == PlanogramComparisonTemplateDalType.FileSystem
                && !criteria.ReadOnly)
            {
                //lock the file first
                LockPlanogramComparisonTemplateByFileName((String)criteria.Id);
            }

            IDalFactory dalFactory = GetDalFactory(criteria.DalFactoryType);
            using (IDalContext dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateDal>())
            {
                LoadDataTransferObject(dalContext, dal.FetchById(criteria.Id));

                //set readonly flag
                LoadProperty(IsReadOnlyProperty, criteria.ReadOnly);
            }
        }

        private void DataPortal_Fetch(FetchByEntityIdNameCriteria criteria)
        {
            IDalFactory dalFactory = GetDalFactory(PlanogramComparisonTemplateDalType.Unknown);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchByEntityIdName(criteria.EntityId, criteria.Name));
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        ///     Called when inserting an instance of this type
        /// </summary>
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                PlanogramComparisonTemplateDto dto = GetDataTransferObject();
                Object oldId = dto.Id;
                using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateDal>())
                {
                    dal.Insert(dto);
                }
                LoadProperty(IdProperty, dto.Id);
                LoadProperty(RowVersionProperty, dto.RowVersion);
                LoadProperty(DateCreatedProperty, dto.DateCreated);
                LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<PlanogramComparisonTemplate>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }

        /// <summary>
        /// Called when inserting this object and its children using an existing dal context.
        /// </summary>
        ///<remarks>Used to insert default by sync process.</remarks>
        internal void InsertUsingExistingContext(IDalContext dalContext)
        {
            PlanogramComparisonTemplateDto dto = GetDataTransferObject();
            Object oldId = dto.Id;
            using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateDal>())
            {
                dal.Insert(dto);
            }
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(RowVersionProperty, dto.RowVersion);
            LoadProperty(DateCreatedProperty, dto.DateCreated);
            LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
            dalContext.RegisterId<PlanogramComparisonTemplate>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Update
        /// <summary>
        ///     Called when updating this instance
        /// </summary>
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                PlanogramComparisonTemplateDto dto = GetDataTransferObject();
                using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateDal>())
                {
                    dal.Update(dto);
                }
                LoadProperty(RowVersionProperty, dto.RowVersion);
                LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        ///     Called when this instance is being deleted
        /// </summary>
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateDal>())
                {
                    dal.DeleteById(Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #endregion

        #region Commands

        #region LockPlanogramComparisonTemplateCommand

        /// <summary>
        ///     Server side implementation of <see cref="LockPlanogramComparisonTemplateCommand"/>.
        /// </summary>
        private partial class LockPlanogramComparisonTemplateCommand
        {
            #region Data Access

            #region Execute

            /// <summary>
            ///     Server side execution of the command.
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(DalFactoryType, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateDal>())
                {
                    dal.LockById(Id);
                }
            }

            #endregion

            #endregion
        }
        #endregion

        #region UnlockPlanogramComparisonTemplateCommand

        /// <summary>
        ///     Server side implementation of <see cref="UnlockPlanogramComparisonTemplateCommand"/>.
        /// </summary>
        private partial class UnlockPlanogramComparisonTemplateCommand
        {
            #region Data Access

            #region Execute

            /// <summary>
            ///     Server side execution of the command.
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(DalFactoryType, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateDal>())
                {
                    dal.UnlockById(Id);
                }
            }

            #endregion

            #endregion
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        ///     Get the correct Dal Factory for the given <paramref name="dalType"/>.
        /// </summary>
        /// <param name="dalType">The type of Dal Factory to get.</param>
        /// <returns>The corresponding Dal Factory.</returns>
        /// <remarks>Loads the Dal Factory details into this instance before returning it.</remarks>
        private IDalFactory GetDalFactory(PlanogramComparisonTemplateDalType dalType)
        {
            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(dalType, out dalFactoryName);
            LoadProperty(DalFactoryNameProperty, dalFactoryName);
            LoadProperty(DalFactoryTypeProperty, dalType);

            return dalFactory;
        }

        /// <summary>
        ///     Get the correct Dal Factory for this instance.
        /// </summary>
        /// <returns>The corresponding Dal Factory.</returns>
        /// <remarks>Loads the Dal Factory details into this instance before returning it.</remarks>
        protected override IDalFactory GetDalFactory()
        {
            if (!String.IsNullOrEmpty(DalFactoryName)) return DalContainer.GetDalFactory(DalFactoryName);

            String dalFactoryName;
            GetDalFactory(DalFactoryType, out dalFactoryName);
            LoadProperty(DalFactoryNameProperty, dalFactoryName);

            return DalContainer.GetDalFactory(DalFactoryName);
        }

        /// <summary>
        ///     Get the correct Dal Factory for this instance, and the name of it as an OUT parameter.
        /// </summary>
        /// <param name="dalType">The type of Dal Factory to get.</param>
        /// <param name="dalFactoryName">[OUT] Parameter to contain the name of the returned Dal Factory.</param>
        /// <returns>The corresponding Dal Factory.</returns>
        /// <remarks>Loads the Dal Factory details into this instance before returning it.</remarks>
        private static IDalFactory GetDalFactory(PlanogramComparisonTemplateDalType dalType, out String dalFactoryName)
        {
            //  Get the User Dal Factory if the type is FileSystem.
            if (dalType == PlanogramComparisonTemplateDalType.FileSystem)
            {
                dalFactoryName = Constants.UserDal;
                return DalContainer.GetDalFactory(dalFactoryName);
            }
            
            //  Get the default Dal Factory for this instance.
            dalFactoryName = DalContainer.DalName;
            return DalContainer.GetDalFactory();
        }

        #endregion
    }
}