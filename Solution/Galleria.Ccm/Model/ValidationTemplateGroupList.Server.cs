﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created (Auto-generated)
// V8-26815 : A.silva ~ Modified the type for the validationTemplateId parameter in GetListByValidationTemplateId.

#endregion

#region Version History: (CCM v8.20)
// V8-31533 : A.Probyn
//  Updated lazy loaded properties to not be lazy loaded as they were causing serialization exceptions
//  when used in the workflow client.
#endregion

#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Server implementation of <see cref="ValidationTemplateGroupList"/>.
    /// </summary>
    public sealed partial class ValidationTemplateGroupList
    {
        #region Constructor

        /// <summary>
        ///     Private constructor. Use Factory Methods to obtain new instances of this class.
        /// </summary>
        private ValidationTemplateGroupList() { } 

        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        internal static ValidationTemplateGroupList FetchByParentId(IDalContext dalContext, Object parentId)
        {
            return DataPortal.FetchChild<ValidationTemplateGroupList>(dalContext, new FetchByParentIdCriteria(null, parentId));
        }
        #endregion

        #region Data Access

        #region Fetch


        #region Fetch
        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FetchByParentIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            using (IValidationTemplateGroupDal dal = dalContext.GetDal<IValidationTemplateGroupDal>())
            {
                IEnumerable<ValidationTemplateGroupDto> dtoList = dal.FetchByValidationTemplateId(criteria.ParentId);
                foreach (ValidationTemplateGroupDto dto in dtoList)
                {
                    this.Add(ValidationTemplateGroup.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion

        #endregion
    }
}