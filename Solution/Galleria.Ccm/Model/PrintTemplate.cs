﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created (Auto-generated)
// V8-31200 : A.Probyn
//      Added new CreateDefaultPlanogramPrintTemplate to create a default print template
#endregion
#region Version History: (CCM 830)
// CCM-18454 : L.Ineson
//  Added RequiresPlanogramImages helper
#endregion
#endregion

using System;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;
using System.IO;
using System.Collections.Generic;
using System.Windows;

namespace Galleria.Ccm.Model
{

    /// <summary>
    /// PrintTemplate model object
    /// </summary>
    [Serializable]
    public sealed partial class PrintTemplate : ModelObject<PrintTemplate>, IDisposable
    {
        #region Nested Classes
        /// <summary>
        /// An enumeration that defines the dal factory type available for this item.
        /// </summary>
        public enum PrintTemplateDalFactoryType
        {
            Unknown = 0,
            FileSystem = 1,
        }
        #endregion

        #region Properties

        /// <summary>
        /// Returns the file extension to use for print templates.
        /// </summary>
        public static String FileExtension
        {
            get { return Constants.PrintTemplateFileExtension; }
        }

        #region DalFactoryType

        /// <summary>
        /// DalFactoryType property definition
        /// </summary>
        private static readonly ModelPropertyInfo<PrintTemplateDalFactoryType> DalFactoryTypeProperty =
            RegisterModelProperty<PrintTemplateDalFactoryType>(c => c.DalFactoryType);

        /// <summary>
        /// Returns the dal factory type
        /// </summary>
        private PrintTemplateDalFactoryType DalFactoryType
        {
            get { return this.GetProperty<PrintTemplateDalFactoryType>(DalFactoryTypeProperty); }
        }

        /// <summary>
        /// Returns true if this object
        /// is currently pointing at the file dal.
        /// </summary>
        public Boolean IsFile
        {
            get { return this.DalFactoryType == PrintTemplateDalFactoryType.FileSystem; }
        }

        #endregion

        #region DalFactoryName
        /// <summary>
        /// DalFactoryName property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> DalFactoryNameProperty =
            RegisterModelProperty<String>(c => c.DalFactoryName);
        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        protected override String DalFactoryName
        {
            get { return this.GetProperty<String>(DalFactoryNameProperty); }
        }
        #endregion

        #region IsReadOnly

        private static readonly ModelPropertyInfo<Boolean> IsReadOnlyProperty =
             RegisterModelProperty<Boolean>(c => c.IsReadOnly);

        /// <summary>
        /// Returns true if this model has been loaded as readonly.
        /// </summary>
        public Boolean IsReadOnly
        {
            get { return this.GetProperty<Boolean>(IsReadOnlyProperty); }
        }

        #endregion


        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Object> IdProperty =
            RegisterModelProperty<Object>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Object Id
        {
            get { return this.GetProperty<Object>(IdProperty); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The row version timestamp
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region EntityId

        /// <summary>
        /// EntityId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);

        /// <summary>
        /// Gets/Sets the EntityId value
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
            set { SetProperty<Int32>(EntityIdProperty, value); }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name, Message.Generic_Name);

        /// <summary>
        /// Gets/Sets the name of the template
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region Description

        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);

        /// <summary>
        /// Gets/Sets the Description value
        /// </summary>
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
            set { SetProperty<String>(DescriptionProperty, value); }
        }

        #endregion

        #region PaperSize

        /// <summary>
        /// PaperSize property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PrintTemplatePaperSizeType> PaperSizeProperty =
            RegisterModelProperty<PrintTemplatePaperSizeType>(c => c.PaperSize);

        /// <summary>
        /// Gets/Sets the size of the paper in this template.
        /// </summary>
        public PrintTemplatePaperSizeType PaperSize
        {
            get { return GetProperty<PrintTemplatePaperSizeType>(PaperSizeProperty); }
            set 
            { 
                SetProperty<PrintTemplatePaperSizeType>(PaperSizeProperty, value);

                //Ripple this change down.
                UpdateChildPageSizes();
            }
        }

        #endregion

        #region IsPlanMirrored

        /// <summary>
        /// IsPlanMirrored property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsPlanMirroredProperty =
            RegisterModelProperty<Boolean>(c => c.IsPlanMirrored);

        /// <summary>
        /// Gets/Sets whether the planogram will be displayed as mirrored by
        /// this template.
        /// </summary>
        public Boolean IsPlanMirrored
        {
            get { return GetProperty<Boolean>(IsPlanMirroredProperty); }
            set { SetProperty<Boolean>(IsPlanMirroredProperty, value); }
        }

        #endregion

        #region DateCreated

        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        /// Gets/Sets the DateCreated value
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
            set { SetProperty<DateTime>(DateCreatedProperty, value); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        /// Gets/Sets the DateLastModified value
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
            set { SetProperty<DateTime>(DateLastModifiedProperty, value); }
        }

        #endregion

        #region SectionGroups

        /// <summary>
        /// SectionGroups property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PrintTemplateSectionGroupList> SectionGroupsProperty =
            RegisterModelProperty<PrintTemplateSectionGroupList>(c => c.SectionGroups);

        /// <summary>
        /// Gets the collection of child SectionGroups
        /// </summary>
        public PrintTemplateSectionGroupList SectionGroups
        {
            get { return GetProperty<PrintTemplateSectionGroupList>(SectionGroupsProperty); }
        }

        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 255));
            BusinessRules.AddRule(new MaxLength(DescriptionProperty, 255));
        }

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository

        /// <summary>
        /// Returns a dictionary of the current role permissions allocated to the user for this object type.
        /// </summary>
        /// <returns></returns>
        public static IDictionary<AuthorizationActions, Boolean> GetUserPermissions()
        {
            return new Dictionary<AuthorizationActions, Boolean>()
            {
                {AuthorizationActions.CreateObject, ApplicationContext.User.IsInRole(DomainPermission.PrintTemplateCreate.ToString())},
                {AuthorizationActions.GetObject, ApplicationContext.User.IsInRole(DomainPermission.PrintTemplateGet.ToString())},
                {AuthorizationActions.EditObject, ApplicationContext.User.IsInRole(DomainPermission.PrintTemplateEdit.ToString())},
                {AuthorizationActions.DeleteObject, ApplicationContext.User.IsInRole(DomainPermission.PrintTemplateDelete.ToString())},
            };
        }

        #endregion

        #region Criteria

        #region FetchByIdCriteria

        /// <summary>
        /// Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region DalType
            /// <summary>
            /// DalType property definition
            /// </summary>
            public static readonly PropertyInfo<PrintTemplateDalFactoryType> DalTypeProperty =
                RegisterProperty<PrintTemplateDalFactoryType>(c => c.DalType);
            /// <summary>
            /// Returns the dal type
            /// </summary>
            public PrintTemplateDalFactoryType DalType
            {
                get { return this.ReadProperty<PrintTemplateDalFactoryType>(DalTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #region AsReadOnly

            /// <summary>
            /// AsReadOnly property definition
            /// </summary>
            public static readonly PropertyInfo<Boolean> AsReadOnlyProperty =
                RegisterProperty<Boolean>(c => c.AsReadOnly);
            /// <summary>
            /// If true the model will be loaded as readonly.
            /// </summary>
            public Boolean AsReadOnly
            {
                get { return this.ReadProperty<Boolean>(AsReadOnlyProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(PrintTemplateDalFactoryType dalType, Object id, Boolean asReadOnly)
            {
                this.LoadProperty<PrintTemplateDalFactoryType>(DalTypeProperty, dalType);
                this.LoadProperty<Object>(IdProperty, id);
                this.LoadProperty<Boolean>(AsReadOnlyProperty, asReadOnly);
            }
            #endregion
        }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new PrintTemplate
        /// </summary>
        public static PrintTemplate NewPrintTemplate()
        {
            PrintTemplate item = new PrintTemplate();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new PrintTemplate
        /// </summary>
        public static PrintTemplate NewPrintTemplate(Int32 entityId)
        {
            PrintTemplate item = new PrintTemplate();
            item.Create(entityId);
            return item;
        }

        /// <summary>
        /// Locks a PrintTemplate so that it cannot be
        /// edited or opened by another process
        /// </summary>
        internal static void LockPrintTemplateByFileName(String fileName)
        {
            DataPortal.Execute<LockPrintTemplateCommand>(new LockPrintTemplateCommand(PrintTemplateDalFactoryType.FileSystem, fileName));
        }

        /// <summary>
        /// Unlocks a PrintTemplate
        /// </summary>
        public static void UnlockPrintTemplateByFileName(String fileName)
        {
            DataPortal.Execute<UnlockPrintTemplateCommand>(new UnlockPrintTemplateCommand(PrintTemplateDalFactoryType.FileSystem, fileName));
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 entityId)
        {
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<PrintTemplateDalFactoryType>(DalFactoryTypeProperty, PrintTemplateDalFactoryType.Unknown);

            this.LoadProperty<PrintTemplateSectionGroupList>(SectionGroupsProperty, PrintTemplateSectionGroupList.NewPrintTemplateSectionGroupList());

            this.LoadProperty<PrintTemplatePaperSizeType>(PaperSizeProperty, PrintTemplatePaperSizeType.A4);
            this.LoadProperty<Boolean>(IsPlanMirroredProperty, false);
            
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            
            base.Create();
        }

        #endregion

        #region Commands

        #region LockPrintTemplateCommand
        /// <summary>
        /// Performs the locking of a PrintTemplate
        /// </summary>
        [Serializable]
        private partial class LockPrintTemplateCommand : CommandBase<LockPrintTemplateCommand>
        {
            #region Properties

            #region DalType
            /// <summary>
            /// DalType property definition
            /// </summary>
            public static readonly PropertyInfo<PrintTemplateDalFactoryType> DalTypeProperty =
                RegisterProperty<PrintTemplateDalFactoryType>(c => c.DalType);
            /// <summary>
            /// Returns the dal factory type
            /// </summary>
            public PrintTemplateDalFactoryType DalType
            {
                get { return this.ReadProperty<PrintTemplateDalFactoryType>(DalTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public LockPrintTemplateCommand(PrintTemplateDalFactoryType dalType, Object id)
            {
                this.LoadProperty<PrintTemplateDalFactoryType>(DalTypeProperty, dalType);
                this.LoadProperty<Object>(IdProperty, id);
            }
            #endregion
        }
        #endregion

        #region UnlockPrintTemplateCommand
        /// <summary>
        /// Performs the unlocking of a highlight
        /// </summary>
        [Serializable]
        private partial class UnlockPrintTemplateCommand : CommandBase<UnlockPrintTemplateCommand>
        {
            #region Properties

            #region DalType
            /// <summary>
            /// DalType property definition
            /// </summary>
            public static readonly PropertyInfo<PrintTemplateDalFactoryType> DalTypeProperty =
                RegisterProperty<PrintTemplateDalFactoryType>(c => c.DalType);
            /// <summary>
            /// Returns the PrintTemplate source type
            /// </summary>
            public PrintTemplateDalFactoryType DalType
            {
                get { return this.ReadProperty<PrintTemplateDalFactoryType>(DalTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public UnlockPrintTemplateCommand(PrintTemplateDalFactoryType sourceType, Object id)
            {
                this.LoadProperty<PrintTemplateDalFactoryType>(DalTypeProperty, sourceType);
                this.LoadProperty<Object>(IdProperty, id);
            }
            #endregion
        }
        #endregion

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<PrintTemplate>(oldId, newId);
        }

        /// <summary>
        /// ToString override.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Provide a SaveAs method to allow saving to the repository.
        /// This resets DalFactoryType and DalFactoryName to allow saving saving to the repository,
        /// otherwise a PrintTemplate instance loaded from a file tries to save back to a file.
        /// </summary>
        public PrintTemplate SaveAsToRepository(Int32 entityId)
        {
            PrintTemplate returnValue = null;

            //if this model is not new and was fetched from another file
            // take a note of the old file name so we can unlock it after
            String oldFileName = null;
            if (!IsNew && DalFactoryType == PrintTemplateDalFactoryType.FileSystem)
            {
                oldFileName = this.Id as String;
            }

            //Mark this as a new item.
            if (!IsNew) MarkGraphAsNew();

            //Load the new details
            this.LoadProperty<PrintTemplateDalFactoryType>(DalFactoryTypeProperty, PrintTemplateDalFactoryType.Unknown);
            this.LoadProperty<String>(DalFactoryNameProperty, DalContainer.DalName);
            this.LoadProperty<Int32>(EntityIdProperty, entityId);

            //save
            returnValue = Save();

            //unlock the old file.
            if (!String.IsNullOrEmpty(oldFileName))
            {
                UnlockPrintTemplateByFileName(oldFileName);
            }

            return returnValue;
        }

        /// <summary>
        /// Saves this to a new file.
        /// </summary>
        public PrintTemplate SaveAsFile(String path)
        {
            PrintTemplate returnValue = null;
            String fileName = path;

            //make sure the file extension is correct.
            fileName = Path.ChangeExtension(path, FileExtension);

            //if this model is not new and was fetched from another file
            // take a note of the old file name.
            String oldFileName = null;
            if ((!IsNew) && (this.Id as String != fileName))
            {
                if (DalFactoryType == PrintTemplateDalFactoryType.FileSystem)
                {
                    oldFileName = this.Id as String;
                }
            }

            if (!IsNew) MarkGraphAsNew();

            LoadProperty<PrintTemplateDalFactoryType>(DalFactoryTypeProperty, PrintTemplateDalFactoryType.FileSystem);
            LoadProperty<String>(DalFactoryNameProperty, "");
            LoadProperty<Object>(IdProperty, fileName);
            LoadProperty<String>(NameProperty, Path.GetFileNameWithoutExtension(fileName));
            returnValue = Save();

            //unlock the old file.
            if (!String.IsNullOrEmpty(oldFileName))
            {
                UnlockPrintTemplateByFileName(oldFileName);
            }

            //Make sure id is the file name for file system, dal is returning 1
            this.LoadProperty<Object>(IdProperty, fileName);
            return returnValue;
        }


        #region Section Group Methods

        /// <summary>
        /// Moves the section group within the print option
        /// </summary>
        public PrintTemplateSectionGroup MoveSectionGroup(PrintTemplateSectionGroup sectionGroupToMove, Byte targetSectionGroupNumber)
        {
            //TODO: This is taking a copy of the section to move it but im not sure why its not just changing the number..

            //loop through all the section groups within the print option
            foreach (PrintTemplateSectionGroup sectionGroup in this.SectionGroups)
            {
                //check to see if the section group is being moved is going up or down the list
                if (sectionGroupToMove.Number > targetSectionGroupNumber)
                {
                    //if the section group number is less than the one being moved and greater than or equal to the target then increase the sections group number
                    if (sectionGroup.Number < sectionGroupToMove.Number && sectionGroup.Number >= targetSectionGroupNumber)
                    {
                        sectionGroup.Number = (Byte)(sectionGroup.Number + 1);
                    }
                }
                else
                {
                    //if the section group number is greater than the one being moved and less than or equal to the target then decrease the section group number
                    if (sectionGroup.Number > sectionGroupToMove.Number && sectionGroup.Number <= targetSectionGroupNumber)
                    {
                        sectionGroup.Number = (Byte)(sectionGroup.Number - 1);
                    }
                }
            }

            //finally set the section group number of the moved section group
            sectionGroupToMove.Number = targetSectionGroupNumber;

            //move the section group
            PrintTemplateSectionGroup groupCopy = sectionGroupToMove.Copy();
            this.SectionGroups.Remove(sectionGroupToMove);
            this.SectionGroups.Insert(sectionGroupToMove.Number - 1, groupCopy);

            return groupCopy;
        }

        #endregion

        /// <summary>
        /// Updates all page sizes of child section groups and sections
        /// according to the current value.
        /// </summary>
        public void UpdateChildPageSizes()
        {
            PrintTemplatePaperSizeType paperSize = this.PaperSize;

            foreach (PrintTemplateSectionGroup sectionGroup in this.SectionGroups)
            {
                foreach (PrintTemplateSection section in sectionGroup.Sections)
                {
                    section.UpdatePageSize(paperSize);
                }
            }
        }

        /// <summary>
        /// Create default planogram print template
        /// </summary>
        public static PrintTemplate CreateDefaultPlanogramPrintTemplate(Int32 entityId, Boolean duringSync)
        {
            //Create new print option
            PrintTemplate planogramPrintTemplate = PrintTemplate.NewPrintTemplate(entityId);
            planogramPrintTemplate.PaperSize = PrintTemplatePaperSizeType.A4;
            planogramPrintTemplate.IsPlanMirrored = false;
            planogramPrintTemplate.Name = Message.PrintTemplateSetup_DefaultTemplate_Name;

            //Create section Group 1 - Overview
            PrintTemplateSectionGroup overviewSectionGroup = PrintTemplateSectionGroup.NewEmpty();
            overviewSectionGroup.Name = Message.PrintTemplateSetup_DefaultTemplate_SectionGroup1_Name;
            overviewSectionGroup.BaysPerPage = 0;

            //Create section 1 - Overview
            PrintTemplateSection overviewSection = PrintTemplateSection.NewEmpty();

            //Create Planogram Component For Overview Section
            PrintTemplateComponent overviewPlanogramComponent = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.Planogram);
            overviewPlanogramComponent.X = 2f;
            overviewPlanogramComponent.Y = 13.40f;
            overviewPlanogramComponent.Height = 180.68f;
            overviewPlanogramComponent.Width = 292.81f;
            //overviewPlanogramComponent.ComponentDetails.PlanogramProductLabelId
            overviewSection.Components.Add(overviewPlanogramComponent);

            //Create TextBox Component For Overview Section
            PrintTemplateComponent overviewTextBox1Component = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.TextBox);
            overviewTextBox1Component.X = 2f;
            overviewTextBox1Component.Y = 1f;
            overviewTextBox1Component.Height = 10f;
            overviewTextBox1Component.Width = 177.69f;
            overviewTextBox1Component.ComponentDetails.TextBoxFontSize = 12;
            overviewTextBox1Component.ComponentDetails.TextBoxIsFontBold = true;
            overviewTextBox1Component.ComponentDetails.TextBoxText = Message.PrintTemplateSetup_Default_ProductGroupTextBoxText;
            overviewSection.Components.Add(overviewTextBox1Component);

            //Create TextBox Component For Overview Section
            PrintTemplateComponent overviewTextBox2Component = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.TextBox);
            overviewTextBox2Component.X = 181f;
            overviewTextBox2Component.Y = 1;
            overviewTextBox2Component.Height = 10;
            overviewTextBox2Component.Width = 114.20f;
            overviewTextBox2Component.ComponentDetails.TextBoxFontSize = 12;
            overviewTextBox2Component.ComponentDetails.TextBoxIsFontBold = true;
            overviewTextBox2Component.ComponentDetails.TextBoxText = Message.PrintTemplateSetup_Default_LiveDateTextBoxText;
            overviewTextBox2Component.ComponentDetails.TextBoxTextAlignment = TextAlignment.Right;
            overviewSection.Components.Add(overviewTextBox2Component);

            //Create TextBox Component For Overview Section
            PrintTemplateComponent overviewTextBox3Component = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.TextBox);
            overviewTextBox3Component.X = 2f;
            overviewTextBox3Component.Y = 196.12f;
            overviewTextBox3Component.Height = 12.82f;
            overviewTextBox3Component.Width = 188.62f;
            overviewTextBox3Component.ComponentDetails.TextBoxFontSize = 10;
            overviewTextBox3Component.ComponentDetails.TextBoxText = Message.PrintTemplateSetup_Default_LocationTextBoxText;
            overviewSection.Components.Add(overviewTextBox3Component);

            //Create TextBox Component For Overview Section
            PrintTemplateComponent overviewTextBox4Component = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.TextBox);
            overviewTextBox4Component.X = 196f;
            overviewTextBox4Component.Y = 196f;
            overviewTextBox4Component.Height = 12.82f;
            overviewTextBox4Component.Width = 99.02f;
            overviewTextBox4Component.ComponentDetails.TextBoxFontSize = 10;
            overviewTextBox4Component.ComponentDetails.TextBoxText = Message.PrintTemplateSetup_Default_PageNumberTextBoxText;
            overviewTextBox4Component.ComponentDetails.TextBoxTextAlignment = TextAlignment.Right;
            overviewSection.Components.Add(overviewTextBox4Component);

            //Create Line Component For Overview Section
            PrintTemplateComponent overviewLine1Component = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.Line);
            overviewLine1Component.X = 1;
            overviewLine1Component.Y = 12;
            overviewLine1Component.Height = 0;
            overviewLine1Component.Width = 0;
            overviewLine1Component.ComponentDetails.LineEndX = 295.23;
            overviewLine1Component.ComponentDetails.LineEndY = 12;
            overviewSection.Components.Add(overviewLine1Component);

            //Create Line Component For Overview Section
            PrintTemplateComponent overviewLine2Component = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.Line);
            overviewLine2Component.X = 2;
            overviewLine2Component.Y = 195.76f;
            overviewLine2Component.Height = 0;
            overviewLine2Component.Width = 0;
            overviewLine2Component.ComponentDetails.LineEndX = 295.23;
            overviewLine2Component.ComponentDetails.LineEndY = 195.76;
            overviewSection.Components.Add(overviewLine2Component);

            //Add overview section to section group
            overviewSectionGroup.Sections.Add(overviewSection);

            //Add section group to print option
            planogramPrintTemplate.SectionGroups.Add(overviewSectionGroup);

            //Create section Group 2 - 1 Bay Per Page
            PrintTemplateSectionGroup BayPerPageSectionGroup = PrintTemplateSectionGroup.NewEmpty();
            BayPerPageSectionGroup.Name = Message.PrintTemplateSetup_DefaultTemplate_SectionGroup2_Name;
            BayPerPageSectionGroup.Number = 2;
            BayPerPageSectionGroup.BaysPerPage = 1;

            //Create section 1 - Planogram
            PrintTemplateSection planogramSection = PrintTemplateSection.NewEmpty();

            //Create Planogram Component For 1 Bay Per Page Section
            PrintTemplateComponent bayPerPagePlanogramComponent = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.Planogram);
            bayPerPagePlanogramComponent.X = 2f;
            bayPerPagePlanogramComponent.Y = 13.40f;
            bayPerPagePlanogramComponent.Height = 180.68f;
            bayPerPagePlanogramComponent.Width = 292.81f;
            planogramSection.Components.Add(bayPerPagePlanogramComponent);

            //Create TextBox Component For 1 Bay Per Page Section
            PrintTemplateComponent bayPerPageTextBox1Component = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.TextBox);
            bayPerPageTextBox1Component.X = 2;
            bayPerPageTextBox1Component.Y = 1;
            bayPerPageTextBox1Component.Height = 10;
            bayPerPageTextBox1Component.Width = 177.69f;
            bayPerPageTextBox1Component.ComponentDetails.TextBoxFontSize = 12;
            bayPerPageTextBox1Component.ComponentDetails.TextBoxIsFontBold = true;
            bayPerPageTextBox1Component.ComponentDetails.TextBoxText = Message.PrintTemplateSetup_Default_ProductGroupTextBoxText;
            planogramSection.Components.Add(bayPerPageTextBox1Component);

            //Create TextBox Component For 1 Bay Per Page Section
            PrintTemplateComponent bayPerPageTextBox2Component = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.TextBox);
            bayPerPageTextBox2Component.X = 181f;
            bayPerPageTextBox2Component.Y = 1;
            bayPerPageTextBox2Component.Height = 10;
            bayPerPageTextBox2Component.Width = 114.20f;
            bayPerPageTextBox2Component.ComponentDetails.TextBoxFontSize = 12;
            bayPerPageTextBox2Component.ComponentDetails.TextBoxIsFontBold = true;
            bayPerPageTextBox2Component.ComponentDetails.TextBoxText = Message.PrintTemplateSetup_Default_LiveDateTextBoxText;
            bayPerPageTextBox2Component.ComponentDetails.TextBoxTextAlignment = TextAlignment.Right;
            planogramSection.Components.Add(bayPerPageTextBox2Component);

            //Create TextBox Component For 1 Bay Per Page Section
            PrintTemplateComponent bayPerPageTextBox3Component = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.TextBox);
            bayPerPageTextBox3Component.X = 2f;
            bayPerPageTextBox3Component.Y = 196.12f;
            bayPerPageTextBox3Component.Height = 12.82f;
            bayPerPageTextBox3Component.Width = 188.62f;
            bayPerPageTextBox3Component.ComponentDetails.TextBoxFontSize = 10;
            bayPerPageTextBox3Component.ComponentDetails.TextBoxText = Message.PrintTemplateSetup_Default_LocationTextBoxText;
            planogramSection.Components.Add(bayPerPageTextBox3Component);

            //Create TextBox Component For 1 Bay Per Page Section
            PrintTemplateComponent bayPerPageTextBox4Component = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.TextBox);
            bayPerPageTextBox4Component.X = 196f;
            bayPerPageTextBox4Component.Y = 196;
            bayPerPageTextBox4Component.Height = 12.82f;
            bayPerPageTextBox4Component.Width = 99.02f;
            bayPerPageTextBox4Component.ComponentDetails.TextBoxFontSize = 10;
            bayPerPageTextBox4Component.ComponentDetails.TextBoxText = Message.PrintTemplateSetup_Default_PageNumberTextBoxText;
            bayPerPageTextBox4Component.ComponentDetails.TextBoxTextAlignment = TextAlignment.Right;
            planogramSection.Components.Add(bayPerPageTextBox4Component);

            //Create Line Component For 1 Bay Per Page Section
            PrintTemplateComponent bayPerPageLine1Component = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.Line);
            bayPerPageLine1Component.X = 1;
            bayPerPageLine1Component.Y = 12;
            bayPerPageLine1Component.Height = 0;
            bayPerPageLine1Component.Width = 0;
            bayPerPageLine1Component.ComponentDetails.LineEndX = 295.23;
            bayPerPageLine1Component.ComponentDetails.LineEndY = 12;
            planogramSection.Components.Add(bayPerPageLine1Component);

            //Create Line Component For 1 Bay Per Page Section
            PrintTemplateComponent bayPerPageLine2Component = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.Line);
            bayPerPageLine2Component.X = 1;
            bayPerPageLine2Component.Y = 195.76f;
            bayPerPageLine2Component.Height = 0;
            bayPerPageLine2Component.Width = 0;
            bayPerPageLine2Component.ComponentDetails.LineEndX = 295.23;
            bayPerPageLine2Component.ComponentDetails.LineEndY = 195.76;
            planogramSection.Components.Add(bayPerPageLine2Component);

            //Add Planogram section to section group
            BayPerPageSectionGroup.Sections.Add(planogramSection);

            //Create section 2 - Merch List
            PrintTemplateSection merchListSection = PrintTemplateSection.NewEmpty();
            merchListSection.Number = 2;

            //Get data sheet we want to use
            UserEditorSettings settings = null;
            if (duringSync)
            {
                settings = UserEditorSettings.NewUserEditorSettings();
            }
            else
            {
                settings = UserEditorSettings.FetchUserEditorSettings();
            }

            //Check we can find default datasheet we want to use
            String dataSheetPath = String.Format("{0}\\Product Inventory Data Sheet.pogds", settings.DataSheetLayoutLocation);
            if (System.IO.File.Exists(dataSheetPath))
            {
                //Create Planogram Component For Merch List Section
                PrintTemplateComponent merchListComponent = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.DataSheet);
                merchListComponent.X = 2f;
                merchListComponent.Y = 13.40f;
                merchListComponent.Height = 180.68f;
                merchListComponent.Width = 292.81f;
                merchListComponent.ComponentDetails.DataSheetHeaderFontSize = 9;
                merchListComponent.ComponentDetails.DataSheetHeaderIsFontBold = true;
                merchListComponent.ComponentDetails.DataSheetRowFontSize = 8;
                merchListComponent.ComponentDetails.DataSheetId = dataSheetPath;
                merchListSection.Components.Add(merchListComponent);

                //Create TextBox Component For Merch List Section
                PrintTemplateComponent merchListTextBox1Component = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.TextBox);
                merchListTextBox1Component.X = 2;
                merchListTextBox1Component.Y = 1;
                merchListTextBox1Component.Height = 10;
                merchListTextBox1Component.Width = 177.69f;
                merchListTextBox1Component.ComponentDetails.TextBoxFontSize = 12;
                merchListTextBox1Component.ComponentDetails.TextBoxIsFontBold = true;
                merchListTextBox1Component.ComponentDetails.TextBoxText = Message.PrintTemplateSetup_Default_ProductGroupTextBoxText;
                merchListSection.Components.Add(merchListTextBox1Component);

                //Create TextBox Component For Merch List Section
                PrintTemplateComponent merchListTextBox2Component = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.TextBox);
                merchListTextBox2Component.X = 181f;
                merchListTextBox2Component.Y = 1;
                merchListTextBox2Component.Height = 10;
                merchListTextBox2Component.Width = 114.20f;
                merchListTextBox2Component.ComponentDetails.TextBoxFontSize = 12;
                merchListTextBox2Component.ComponentDetails.TextBoxIsFontBold = true;
                merchListTextBox2Component.ComponentDetails.TextBoxText = Message.PrintTemplateSetup_Default_LiveDateTextBoxText;
                merchListTextBox2Component.ComponentDetails.TextBoxTextAlignment = TextAlignment.Right;
                merchListSection.Components.Add(merchListTextBox2Component);

                //Create TextBox Component For Merch List Section
                PrintTemplateComponent merchListTextBox3Component = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.TextBox);
                merchListTextBox3Component.X = 2f;
                merchListTextBox3Component.Y = 196.12f;
                merchListTextBox3Component.Height = 12.82f;
                merchListTextBox3Component.Width = 188.62f;
                merchListTextBox3Component.ComponentDetails.TextBoxFontSize = 10;
                merchListTextBox3Component.ComponentDetails.TextBoxText = Message.PrintTemplateSetup_Default_LocationTextBoxText;
                merchListSection.Components.Add(merchListTextBox3Component);

                //Create TextBox Component For Merch List Section
                PrintTemplateComponent merchListTextBox4Component = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.TextBox);
                merchListTextBox4Component.X = 196f;
                merchListTextBox4Component.Y = 196;
                merchListTextBox4Component.Height = 12.82f;
                merchListTextBox4Component.Width = 99.02f;
                merchListTextBox4Component.ComponentDetails.TextBoxFontSize = 10;
                merchListTextBox4Component.ComponentDetails.TextBoxText = Message.PrintTemplateSetup_Default_PageNumberTextBoxText;
                merchListTextBox4Component.ComponentDetails.TextBoxTextAlignment = TextAlignment.Right;
                merchListSection.Components.Add(merchListTextBox4Component);

                //Create Line Component For Merch List Section
                PrintTemplateComponent merchListLine1Component = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.Line);
                merchListLine1Component.X = 1;
                merchListLine1Component.Y = 12;
                merchListLine1Component.Height = 0;
                merchListLine1Component.Width = 0;
                merchListLine1Component.ComponentDetails.LineEndX = 295.23;
                merchListLine1Component.ComponentDetails.LineEndY = 12;
                merchListSection.Components.Add(merchListLine1Component);

                //Create Line Component For Merch List Section
                PrintTemplateComponent merchListLine2Component = PrintTemplateComponent.NewPrintTemplateComponent(PrintTemplateComponentType.Line);
                merchListLine2Component.X = 1;
                merchListLine2Component.Y = 195.76f;
                merchListLine2Component.Height = 0;
                merchListLine2Component.Width = 0;
                merchListLine2Component.ComponentDetails.LineEndX = 295.23;
                merchListLine2Component.ComponentDetails.LineEndY = 195.76;
                merchListSection.Components.Add(merchListLine2Component);

                //Add Merch List section to section group
                BayPerPageSectionGroup.Sections.Add(merchListSection);
            }

            //Add section group to print option
            planogramPrintTemplate.SectionGroups.Add(BayPerPageSectionGroup);

            return planogramPrintTemplate;
        }

        /// <summary>
        /// Returns true if any of the components in this template require
        /// the load of the planogram images.
        /// </summary>
        /// <returns></returns>
        public Boolean RequiresPlanogramImages()
        {
            return this.SectionGroups
                    .Any(sg => sg.Sections
                        .Any(s => s.Components
                            .Any(c => c.Type == PrintTemplateComponentType.Planogram
                                    && (c.ComponentDetails.PlanogramProductImages || c.ComponentDetails.PlanogramFixtureImages)
                                    )
                             )
                        );
        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    if (!IsNew)
                    {
                        if (this.DalFactoryType == PrintTemplateDalFactoryType.FileSystem
                            && this.Id is String)
                        {
                            UnlockPrintTemplateByFileName((String)Id);
                        }
                    }
                }
                _isDisposed = true;
            }
        }

        #endregion
    }

}