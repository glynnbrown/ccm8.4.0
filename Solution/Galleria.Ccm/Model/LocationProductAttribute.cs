﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
// V8-26041 : A.Kuszyk
//      Changed use of CCM ProductStatusType to Framework PlanogramProductStatusType.
// V8-24264 : A.Probyn
//      Added missing ModelPropertyType
// V8-27630 : I.George
//      Added defaultNewMethod
#endregion
#region Version History: (CCM 8.03)
//V8-29267 : L.Ineson
//  Removed date deleted.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a locationProductAttribute 
    /// (child of LocationProductAttributeList)
    /// 
    /// This is a root MasterData object and so it has the following properties/actions:
    /// - The object should be deleted outright. This behaviour is different from GFS.
    /// </summary>
    [Serializable]
    [DefaultNewMethod("NewLocationProductAttribute","LocationId" ,"ProductId","EntityId")]
    public partial class LocationProductAttribute : ModelObject<LocationProductAttribute>
    {
        #region Static Constructor
        /// <summary>
        /// Static Constructor
        /// </summary>
        static LocationProductAttribute()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Properties


        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
       RegisterModelProperty<RowVersion>(c => c.RowVersion);
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }


        public static readonly ModelPropertyInfo<Int32> ProductIdProperty =
       RegisterModelProperty<Int32>(c => c.ProductId);
        public Int32 ProductId
        {
            get { return GetProperty<Int32>(ProductIdProperty); }
        }


        public static readonly ModelPropertyInfo<Int16> LocationIdProperty =
       RegisterModelProperty<Int16>(c => c.LocationId);
        public Int16 LocationId
        {
            get { return GetProperty<Int16>(LocationIdProperty); }
        }


        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
       RegisterModelProperty<Int32>(c => c.EntityId);
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
        }


        public static readonly ModelPropertyInfo<String> GTINProperty =
       RegisterModelProperty<String>(c => c.GTIN);
        public String GTIN
        {
            get { return GetProperty<String>(GTINProperty); }
            set { SetProperty<String>(GTINProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> DescriptionProperty =
       RegisterModelProperty<String>(c => c.Description);
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
            set { SetProperty<String>(DescriptionProperty, value); }
        }


        public static readonly ModelPropertyInfo<Single?> HeightProperty =
       RegisterModelProperty<Single?>(c => c.Height, Message.LocationProductAttribute_Height, ModelPropertyDisplayType.Length);
        public Single? Height
        {
            get { return GetProperty<Single?>(HeightProperty); }
            set { SetProperty<Single?>(HeightProperty, value); }
        }


        public static readonly ModelPropertyInfo<Single?> WidthProperty =
       RegisterModelProperty<Single?>(c => c.Width, Message.LocationProductAttribute_Width, ModelPropertyDisplayType.Length);
        public Single? Width
        {
            get { return GetProperty<Single?>(WidthProperty); }
            set { SetProperty<Single?>(WidthProperty, value); }
        }


        public static readonly ModelPropertyInfo<Single?> DepthProperty =
       RegisterModelProperty<Single?>(c => c.Depth, Message.LocationProductAttribute_Depth, ModelPropertyDisplayType.Length);
        public Single? Depth
        {
            get { return GetProperty<Single?>(DepthProperty); }
            set { SetProperty<Single?>(DepthProperty, value); }
        }


        public static readonly ModelPropertyInfo<Int16?> CasePackUnitsProperty =
       RegisterModelProperty<Int16?>(c => c.CasePackUnits, Message.LocationProductAttribute_CasePackUnits);
        public Int16? CasePackUnits
        {
            get { return GetProperty<Int16?>(CasePackUnitsProperty); }
            set { SetProperty<Int16?>(CasePackUnitsProperty, value); }
        }


        public static readonly ModelPropertyInfo<Byte?> CaseHighProperty =
        RegisterModelProperty<Byte?>(c => c.CaseHigh, Message.LocationProductAttribute_CaseHigh);
        /// <summary>
        /// The product CaseHigh
        /// </summary>
        public Byte? CaseHigh
        {
            get { return GetProperty<Byte?>(CaseHighProperty); }
            set
            {
                SetProperty<Byte?>(CaseHighProperty, value);
            }
        }

        public static readonly ModelPropertyInfo<Byte?> CaseWideProperty =
        RegisterModelProperty<Byte?>(c => c.CaseWide, Message.LocationProductAttribute_CaseWide);
        /// <summary>
        /// The product CaseWide
        /// </summary>
        public Byte? CaseWide
        {
            get { return GetProperty<Byte?>(CaseWideProperty); }
            set
            {
                SetProperty<Byte?>(CaseWideProperty, value);
            }
        }

        public static readonly ModelPropertyInfo<Byte?> CaseDeepProperty =
        RegisterModelProperty<Byte?>(c => c.CaseDeep, Message.LocationProductAttribute_CaseDeep);
        /// <summary>
        /// The product CaseDeep
        /// </summary>
        public Byte? CaseDeep
        {
            get { return GetProperty<Byte?>(CaseDeepProperty); }
            set
            {
                SetProperty<Byte?>(CaseDeepProperty, value);
            }
        }

        public static readonly ModelPropertyInfo<Single?> CaseHeightProperty =
        RegisterModelProperty<Single?>(c => c.CaseHeight, Message.LocationProductAttribute_CaseHeight, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The product CaseHeight
        /// </summary>
        public Single? CaseHeight
        {
            get { return GetProperty<Single?>(CaseHeightProperty); }
            set { SetProperty<Single?>(CaseHeightProperty, value); }
        }

        public static readonly ModelPropertyInfo<Single?> CaseWidthProperty =
        RegisterModelProperty<Single?>(c => c.CaseWidth, Message.LocationProductAttribute_CaseWidth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The product CaseWidth
        /// </summary>
        public Single? CaseWidth
        {
            get { return GetProperty<Single?>(CaseWidthProperty); }
            set { SetProperty<Single?>(CaseWidthProperty, value); }
        }


        public static readonly ModelPropertyInfo<Single?> CaseDepthProperty =
        RegisterModelProperty<Single?>(c => c.CaseDepth, Message.LocationProductAttribute_CaseDepth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The product CaseDepth
        /// </summary>
        public Single? CaseDepth
        {
            get { return GetProperty<Single?>(CaseDepthProperty); }
            set { SetProperty<Single?>(CaseDepthProperty, value); }
        }

        public static readonly ModelPropertyInfo<Byte?> DaysOfSupplyProperty =
       RegisterModelProperty<Byte?>(c => c.DaysOfSupply, Message.LocationProductAttribute_DaysOfSupply);
        public Byte? DaysOfSupply
        {
            get { return GetProperty<Byte?>(DaysOfSupplyProperty); }
            set { SetProperty<Byte?>(DaysOfSupplyProperty, value); }
        }


        public static readonly ModelPropertyInfo<Byte?> MinDeepProperty =
       RegisterModelProperty<Byte?>(c => c.MinDeep, Message.LocationProductAttribute_MinDeep);
        public Byte? MinDeep
        {
            get { return GetProperty<Byte?>(MinDeepProperty); }
            set { SetProperty<Byte?>(MinDeepProperty, value); }
        }


        public static readonly ModelPropertyInfo<Byte?> MaxDeepProperty =
       RegisterModelProperty<Byte?>(c => c.MaxDeep, Message.LocationProductAttribute_MaxDeep);
        public Byte? MaxDeep
        {
            get { return GetProperty<Byte?>(MaxDeepProperty); }
            set { SetProperty<Byte?>(MaxDeepProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int16?> TrayPackUnitsProperty =
       RegisterModelProperty<Int16?>(c => c.TrayPackUnits, Message.LocationProductAttribute_TrayPackUnit);
        /// <summary>
        /// The product TrayPackUnits
        /// </summary>
        public Int16? TrayPackUnits
        {
            get { return GetProperty<Int16?>(TrayPackUnitsProperty); }
            set { SetProperty<Int16?>(TrayPackUnitsProperty, value); }
        }

        public static readonly ModelPropertyInfo<Byte?> TrayHighProperty =
       RegisterModelProperty<Byte?>(c => c.TrayHigh, Message.LocationProductAttribute_TrayHigh);
        public Byte? TrayHigh
        {
            get { return GetProperty<Byte?>(TrayHighProperty); }
            set
            {
                SetProperty<Byte?>(TrayHighProperty, value);
            }
        }


        public static readonly ModelPropertyInfo<Byte?> TrayWideProperty =
       RegisterModelProperty<Byte?>(c => c.TrayWide, Message.LocationProductAttribute_TrayWide);
        public Byte? TrayWide
        {
            get { return GetProperty<Byte?>(TrayWideProperty); }
            set
            {
                SetProperty<Byte?>(TrayWideProperty, value);
            }
        }


        public static readonly ModelPropertyInfo<Byte?> TrayDeepProperty =
       RegisterModelProperty<Byte?>(c => c.TrayDeep, Message.LocationProductAttribute_TrayDeep);
        public Byte? TrayDeep
        {
            get { return GetProperty<Byte?>(TrayDeepProperty); }
            set
            {
                SetProperty<Byte?>(TrayDeepProperty, value);
            }
        }

        public static readonly ModelPropertyInfo<Single?> TrayHeightProperty =
        RegisterModelProperty<Single?>(c => c.TrayHeight, Message.LocationProductAttribute_TrayHeight, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The product TrayHeight
        /// </summary>
        public Single? TrayHeight
        {
            get { return GetProperty<Single?>(TrayHeightProperty); }
            set { SetProperty<Single?>(TrayHeightProperty, value); }
        }

        public static readonly ModelPropertyInfo<Single?> TrayWidthProperty =
        RegisterModelProperty<Single?>(c => c.TrayWidth, Message.LocationProductAttribute_TrayWidth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The product TrayWidth
        /// </summary>
        public Single? TrayWidth
        {
            get { return GetProperty<Single?>(TrayWidthProperty); }
            set { SetProperty<Single?>(TrayWidthProperty, value); }
        }


        public static readonly ModelPropertyInfo<Single?> TrayDepthProperty =
        RegisterModelProperty<Single?>(c => c.TrayDepth, Message.LocationProductAttribute_TrayDepth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// The product TrayDepth
        /// </summary>
        public Single? TrayDepth
        {
            get { return GetProperty<Single?>(TrayDepthProperty); }
            set { SetProperty<Single?>(TrayDepthProperty, value); }
        }

        public static readonly ModelPropertyInfo<Single?> TrayThickHeightProperty =
       RegisterModelProperty<Single?>(c => c.TrayThickHeight, Message.LocationProductAttribute_TrayThickHeight, ModelPropertyDisplayType.Length);
        public Single? TrayThickHeight
        {
            get { return GetProperty<Single?>(TrayThickHeightProperty); }
            set { SetProperty<Single?>(TrayThickHeightProperty, value); }
        }


        public static readonly ModelPropertyInfo<Single?> TrayThickDepthProperty =
       RegisterModelProperty<Single?>(c => c.TrayThickDepth, Message.LocationProductAttribute_TrayThickDepth, ModelPropertyDisplayType.Length);
        public Single? TrayThickDepth
        {
            get { return GetProperty<Single?>(TrayThickDepthProperty); }
            set { SetProperty<Single?>(TrayThickDepthProperty, value); }
        }


        public static readonly ModelPropertyInfo<Single?> TrayThickWidthProperty =
       RegisterModelProperty<Single?>(c => c.TrayThickWidth, Message.LocationProductAttribute_TrayThickWidth, ModelPropertyDisplayType.Length);
        public Single? TrayThickWidth
        {
            get { return GetProperty<Single?>(TrayThickWidthProperty); }
            set { SetProperty<Single?>(TrayThickWidthProperty, value); }
        }


        public static readonly ModelPropertyInfo<PlanogramProductStatusType?> StatusTypeProperty =
            RegisterModelProperty<PlanogramProductStatusType?>(c => c.StatusType, Message.LocationProductAttribute_StatusType,
            Message.LocationProductAttribute_StatusType, null);
        public PlanogramProductStatusType? StatusType
        {
            get { return GetProperty<PlanogramProductStatusType?>(StatusTypeProperty); }
            set { SetProperty<PlanogramProductStatusType?>(StatusTypeProperty, value); }
        }


        public static readonly ModelPropertyInfo<Int16?> ShelfLifeProperty =
       RegisterModelProperty<Int16?>(c => c.ShelfLife, Message.LocationProductAttribute_ShelfLife);
        public Int16? ShelfLife
        {
            get { return GetProperty<Int16?>(ShelfLifeProperty); }
            set { SetProperty<Int16?>(ShelfLifeProperty, value); }
        }


        public static readonly ModelPropertyInfo<Single?> DeliveryFrequencyDaysProperty =
       RegisterModelProperty<Single?>(c => c.DeliveryFrequencyDays, Message.LocationProductAttribute_DeliveryFrequencyDays);
        public Single? DeliveryFrequencyDays
        {
            get { return GetProperty<Single?>(DeliveryFrequencyDaysProperty); }
            set { SetProperty<Single?>(DeliveryFrequencyDaysProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> DeliveryMethodProperty =
       RegisterModelProperty<String>(c => c.DeliveryMethod, Message.LocationProductAttribute_DeliveryMethod);
        public String DeliveryMethod
        {
            get { return GetProperty<String>(DeliveryMethodProperty); }
            set { SetProperty<String>(DeliveryMethodProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> VendorCodeProperty =
       RegisterModelProperty<String>(c => c.VendorCode, Message.LocationProductAttribute_VendorCode);
        public String VendorCode
        {
            get { return GetProperty<String>(VendorCodeProperty); }
            set { SetProperty<String>(VendorCodeProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> VendorProperty =
       RegisterModelProperty<String>(c => c.Vendor, Message.LocationProductAttribute_Vendor);
        public String Vendor
        {
            get { return GetProperty<String>(VendorProperty); }
            set { SetProperty<String>(VendorProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> ManufacturerCodeProperty =
       RegisterModelProperty<String>(c => c.ManufacturerCode, Message.LocationProductAttribute_ManufacturerCode);
        public String ManufacturerCode
        {
            get { return GetProperty<String>(ManufacturerCodeProperty); }
            set { SetProperty<String>(ManufacturerCodeProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> ManufacturerProperty =
       RegisterModelProperty<String>(c => c.Manufacturer, Message.LocationProductAttribute_Manufacturer);
        public String Manufacturer
        {
            get { return GetProperty<String>(ManufacturerProperty); }
            set { SetProperty<String>(ManufacturerProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> SizeProperty =
       RegisterModelProperty<String>(c => c.Size, Message.LocationProductAttribute_Size);
        public String Size
        {
            get { return GetProperty<String>(SizeProperty); }
            set { SetProperty<String>(SizeProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> UnitOfMeasureProperty =
       RegisterModelProperty<String>(c => c.UnitOfMeasure, Message.LocationProductAttribute_UnitOfMeasure);
        public String UnitOfMeasure
        {
            get { return GetProperty<String>(UnitOfMeasureProperty); }
            set { SetProperty<String>(UnitOfMeasureProperty, value); }
        }


        public static readonly ModelPropertyInfo<Single?> SellPriceProperty =
       RegisterModelProperty<Single?>(c => c.SellPrice, Message.LocationProductAttribute_SellPrice, ModelPropertyDisplayType.Currency);
        public Single? SellPrice
        {
            get { return GetProperty<Single?>(SellPriceProperty); }
            set { SetProperty<Single?>(SellPriceProperty, value); }
        }


        public static readonly ModelPropertyInfo<Int16?> SellPackCountProperty =
       RegisterModelProperty<Int16?>(c => c.SellPackCount, Message.LocationProductAttribute_SellPackCount);
        public Int16? SellPackCount
        {
            get { return GetProperty<Int16?>(SellPackCountProperty); }
            set { SetProperty<Int16?>(SellPackCountProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> SellPackDescriptionProperty =
       RegisterModelProperty<String>(c => c.SellPackDescription, Message.LocationProductAttribute_SellPackDescription);
        public String SellPackDescription
        {
            get { return GetProperty<String>(SellPackDescriptionProperty); }
            set { SetProperty<String>(SellPackDescriptionProperty, value); }
        }


        public static readonly ModelPropertyInfo<Single?> RecommendedRetailPriceProperty =
       RegisterModelProperty<Single?>(c => c.RecommendedRetailPrice, Message.LocationProductAttribute_RecommendedRetailPrice, ModelPropertyDisplayType.Currency);
        public Single? RecommendedRetailPrice
        {
            get { return GetProperty<Single?>(RecommendedRetailPriceProperty); }
            set { SetProperty<Single?>(RecommendedRetailPriceProperty, value); }
        }


        public static readonly ModelPropertyInfo<Single?> CostPriceProperty =
       RegisterModelProperty<Single?>(c => c.CostPrice, Message.LocationProductAttribute_CostPrice, ModelPropertyDisplayType.Currency);
        public Single? CostPrice
        {
            get { return GetProperty<Single?>(CostPriceProperty); }
            set { SetProperty<Single?>(CostPriceProperty, value); }
        }


        public static readonly ModelPropertyInfo<Single?> CaseCostProperty =
       RegisterModelProperty<Single?>(c => c.CaseCost, Message.LocationProductAttribute_CaseCost, ModelPropertyDisplayType.Currency);
        public Single? CaseCost
        {
            get { return GetProperty<Single?>(CaseCostProperty); }
            set { SetProperty<Single?>(CaseCostProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> ConsumerInformationProperty =
       RegisterModelProperty<String>(c => c.ConsumerInformation, Message.LocationProductAttribute_ConsumerInformation);
        public String ConsumerInformation
        {
            get { return GetProperty<String>(ConsumerInformationProperty); }
            set { SetProperty<String>(ConsumerInformationProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> PatternProperty =
       RegisterModelProperty<String>(c => c.Pattern, Message.LocationProductAttribute_Pattern);
        public String Pattern
        {
            get { return GetProperty<String>(PatternProperty); }
            set { SetProperty<String>(PatternProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> ModelProperty =
       RegisterModelProperty<String>(c => c.Model, Message.LocationProductAttribute_Model);
        public String Model
        {
            get { return GetProperty<String>(ModelProperty); }
            set { SetProperty<String>(ModelProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> CorporateCodeProperty =
            RegisterModelProperty<String>(c => c.CorporateCode, Message.LocationProductAttribute_CorporateProductCode);
        public String CorporateCode
        {
            get { return GetProperty<String>(CorporateCodeProperty); }
            set { SetProperty<String>(CorporateCodeProperty, value); }
        }

        /// <summary>
        /// The date when the item was created 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
        }

        /// <summary>
        /// The date when the item was modified 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
        }


        //#region Custom Attributes

        //public static readonly ModelPropertyInfo<object> CustomAttribute01Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute01);
        ///// <summary>
        ///// CustomAttribute01
        ///// </summary>
        //public object CustomAttribute01
        //{
        //    get { return GetProperty<object>(CustomAttribute01Property); }
        //    set { SetProperty<object>(CustomAttribute01Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute02Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute02);
        ///// <summary>
        ///// CustomAttribute02
        ///// </summary>
        //public object CustomAttribute02
        //{
        //    get { return GetProperty<object>(CustomAttribute02Property); }
        //    set { SetProperty<object>(CustomAttribute02Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute03Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute03);
        ///// <summary>
        ///// CustomAttribute03
        ///// </summary>
        //public object CustomAttribute03
        //{
        //    get { return GetProperty<object>(CustomAttribute03Property); }
        //    set { SetProperty<object>(CustomAttribute03Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute04Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute04);
        ///// <summary>
        ///// CustomAttribute04
        ///// </summary>
        //public object CustomAttribute04
        //{
        //    get { return GetProperty<object>(CustomAttribute04Property); }
        //    set { SetProperty<object>(CustomAttribute04Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute05Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute05);
        ///// <summary>
        ///// CustomAttribute05
        ///// </summary>
        //public object CustomAttribute05
        //{
        //    get { return GetProperty<object>(CustomAttribute05Property); }
        //    set { SetProperty<object>(CustomAttribute05Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute06Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute06);
        ///// <summary>
        ///// CustomAttribute06
        ///// </summary>
        //public object CustomAttribute06
        //{
        //    get { return GetProperty<object>(CustomAttribute06Property); }
        //    set { SetProperty<object>(CustomAttribute06Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute07Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute07);
        ///// <summary>
        ///// CustomAttribute07
        ///// </summary>
        //public object CustomAttribute07
        //{
        //    get { return GetProperty<object>(CustomAttribute07Property); }
        //    set { SetProperty<object>(CustomAttribute07Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute08Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute08);
        ///// <summary>
        ///// CustomAttribute08
        ///// </summary>
        //public object CustomAttribute08
        //{
        //    get { return GetProperty<object>(CustomAttribute08Property); }
        //    set { SetProperty<object>(CustomAttribute08Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute09Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute09);
        ///// <summary>
        ///// CustomAttribute09
        ///// </summary>
        //public object CustomAttribute09
        //{
        //    get { return GetProperty<object>(CustomAttribute09Property); }
        //    set { SetProperty<object>(CustomAttribute09Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute10Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute10);
        ///// <summary>
        ///// CustomAttribute10
        ///// </summary>
        //public object CustomAttribute10
        //{
        //    get { return GetProperty<object>(CustomAttribute10Property); }
        //    set { SetProperty<object>(CustomAttribute10Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute11Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute11);
        ///// <summary>
        ///// CustomAttribute11
        ///// </summary>
        //public object CustomAttribute11
        //{
        //    get { return GetProperty<object>(CustomAttribute11Property); }
        //    set { SetProperty<object>(CustomAttribute11Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute12Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute12);
        ///// <summary>
        ///// CustomAttribute12
        ///// </summary>
        //public object CustomAttribute12
        //{
        //    get { return GetProperty<object>(CustomAttribute12Property); }
        //    set { SetProperty<object>(CustomAttribute12Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute13Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute13);
        ///// <summary>
        ///// CustomAttribute13
        ///// </summary>
        //public object CustomAttribute13
        //{
        //    get { return GetProperty<object>(CustomAttribute13Property); }
        //    set { SetProperty<object>(CustomAttribute13Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute14Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute14);
        ///// <summary>
        ///// CustomAttribute14
        ///// </summary>
        //public object CustomAttribute14
        //{
        //    get { return GetProperty<object>(CustomAttribute14Property); }
        //    set { SetProperty<object>(CustomAttribute14Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute15Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute15);
        ///// <summary>
        ///// CustomAttribute15
        ///// </summary>
        //public object CustomAttribute15
        //{
        //    get { return GetProperty<object>(CustomAttribute15Property); }
        //    set { SetProperty<object>(CustomAttribute15Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute16Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute16);
        ///// <summary>
        ///// CustomAttribute16
        ///// </summary>
        //public object CustomAttribute16
        //{
        //    get { return GetProperty<object>(CustomAttribute16Property); }
        //    set { SetProperty<object>(CustomAttribute16Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute17Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute17);
        ///// <summary>
        ///// CustomAttribute17
        ///// </summary>
        //public object CustomAttribute17
        //{
        //    get { return GetProperty<object>(CustomAttribute17Property); }
        //    set { SetProperty<object>(CustomAttribute17Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute18Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute18);
        ///// <summary>
        ///// CustomAttribute18
        ///// </summary>
        //public object CustomAttribute18
        //{
        //    get { return GetProperty<object>(CustomAttribute18Property); }
        //    set { SetProperty<object>(CustomAttribute18Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute19Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute19);
        ///// <summary>
        ///// CustomAttribute19
        ///// </summary>
        //public object CustomAttribute19
        //{
        //    get { return GetProperty<object>(CustomAttribute19Property); }
        //    set { SetProperty<object>(CustomAttribute19Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute20Property =
        // RegisterModelProperty<object>(c => c.CustomAttribute20);
        ///// <summary>
        ///// CustomAttribute20
        ///// </summary>
        //public object CustomAttribute20
        //{
        //    get { return GetProperty<object>(CustomAttribute20Property); }
        //    set { SetProperty<object>(CustomAttribute20Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute21Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute21);
        ///// <summary>
        ///// CustomAttribute21
        ///// </summary>
        //public object CustomAttribute21
        //{
        //    get { return GetProperty<object>(CustomAttribute21Property); }
        //    set { SetProperty<object>(CustomAttribute21Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute22Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute22);
        ///// <summary>
        ///// CustomAttribute22
        ///// </summary>
        //public object CustomAttribute22
        //{
        //    get { return GetProperty<object>(CustomAttribute22Property); }
        //    set { SetProperty<object>(CustomAttribute22Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute23Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute23);
        ///// <summary>
        ///// CustomAttribute23
        ///// </summary>
        //public object CustomAttribute23
        //{
        //    get { return GetProperty<object>(CustomAttribute23Property); }
        //    set { SetProperty<object>(CustomAttribute23Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute24Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute24);
        ///// <summary>
        ///// CustomAttribute24
        ///// </summary>
        //public object CustomAttribute24
        //{
        //    get { return GetProperty<object>(CustomAttribute24Property); }
        //    set { SetProperty<object>(CustomAttribute24Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute25Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute25);
        ///// <summary>
        ///// CustomAttribute25
        ///// </summary>
        //public object CustomAttribute25
        //{
        //    get { return GetProperty<object>(CustomAttribute25Property); }
        //    set { SetProperty<object>(CustomAttribute25Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute26Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute26);
        ///// <summary>
        ///// CustomAttribute26
        ///// </summary>
        //public object CustomAttribute26
        //{
        //    get { return GetProperty<object>(CustomAttribute26Property); }
        //    set { SetProperty<object>(CustomAttribute26Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute27Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute27);
        ///// <summary>
        ///// CustomAttribute27
        ///// </summary>
        //public object CustomAttribute27
        //{
        //    get { return GetProperty<object>(CustomAttribute27Property); }
        //    set { SetProperty<object>(CustomAttribute27Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute28Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute28);
        ///// <summary>
        ///// CustomAttribute28
        ///// </summary>
        //public object CustomAttribute28
        //{
        //    get { return GetProperty<object>(CustomAttribute28Property); }
        //    set { SetProperty<object>(CustomAttribute28Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute29Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute29);
        ///// <summary>
        ///// CustomAttribute29
        ///// </summary>
        //public object CustomAttribute29
        //{
        //    get { return GetProperty<object>(CustomAttribute29Property); }
        //    set { SetProperty<object>(CustomAttribute29Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute30Property =
        // RegisterModelProperty<object>(c => c.CustomAttribute30);
        ///// <summary>
        ///// CustomAttribute30
        ///// </summary>
        //public object CustomAttribute30
        //{
        //    get { return GetProperty<object>(CustomAttribute30Property); }
        //    set { SetProperty<object>(CustomAttribute30Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute31Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute31);
        ///// <summary>
        ///// CustomAttribute31
        ///// </summary>
        //public object CustomAttribute31
        //{
        //    get { return GetProperty<object>(CustomAttribute31Property); }
        //    set { SetProperty<object>(CustomAttribute31Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute32Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute32);
        ///// <summary>
        ///// CustomAttribute32
        ///// </summary>
        //public object CustomAttribute32
        //{
        //    get { return GetProperty<object>(CustomAttribute32Property); }
        //    set { SetProperty<object>(CustomAttribute32Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute33Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute33);
        ///// <summary>
        ///// CustomAttribute33
        ///// </summary>
        //public object CustomAttribute33
        //{
        //    get { return GetProperty<object>(CustomAttribute33Property); }
        //    set { SetProperty<object>(CustomAttribute33Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute34Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute34);
        ///// <summary>
        ///// CustomAttribute34
        ///// </summary>
        //public object CustomAttribute34
        //{
        //    get { return GetProperty<object>(CustomAttribute34Property); }
        //    set { SetProperty<object>(CustomAttribute34Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute35Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute35);
        ///// <summary>
        ///// CustomAttribute35
        ///// </summary>
        //public object CustomAttribute35
        //{
        //    get { return GetProperty<object>(CustomAttribute35Property); }
        //    set { SetProperty<object>(CustomAttribute35Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute36Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute36);
        ///// <summary>
        ///// CustomAttribute36
        ///// </summary>
        //public object CustomAttribute36
        //{
        //    get { return GetProperty<object>(CustomAttribute36Property); }
        //    set { SetProperty<object>(CustomAttribute36Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute37Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute37);
        ///// <summary>
        ///// CustomAttribute37
        ///// </summary>
        //public object CustomAttribute37
        //{
        //    get { return GetProperty<object>(CustomAttribute37Property); }
        //    set { SetProperty<object>(CustomAttribute37Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute38Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute38);
        ///// <summary>
        ///// CustomAttribute38
        ///// </summary>
        //public object CustomAttribute38
        //{
        //    get { return GetProperty<object>(CustomAttribute38Property); }
        //    set { SetProperty<object>(CustomAttribute38Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute39Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute39);
        ///// <summary>
        ///// CustomAttribute39
        ///// </summary>
        //public object CustomAttribute39
        //{
        //    get { return GetProperty<object>(CustomAttribute39Property); }
        //    set { SetProperty<object>(CustomAttribute39Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute40Property =
        // RegisterModelProperty<object>(c => c.CustomAttribute40);
        ///// <summary>
        ///// CustomAttribute40
        ///// </summary>
        //public object CustomAttribute40
        //{
        //    get { return GetProperty<object>(CustomAttribute40Property); }
        //    set { SetProperty<object>(CustomAttribute40Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute41Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute41);
        ///// <summary>
        ///// CustomAttribute44
        ///// </summary>
        //public object CustomAttribute41
        //{
        //    get { return GetProperty<object>(CustomAttribute41Property); }
        //    set { SetProperty<object>(CustomAttribute41Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute42Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute42);
        ///// <summary>
        ///// CustomAttribute44
        ///// </summary>
        //public object CustomAttribute42
        //{
        //    get { return GetProperty<object>(CustomAttribute42Property); }
        //    set { SetProperty<object>(CustomAttribute42Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute43Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute43);
        ///// <summary>
        ///// CustomAttribute44
        ///// </summary>
        //public object CustomAttribute43
        //{
        //    get { return GetProperty<object>(CustomAttribute43Property); }
        //    set { SetProperty<object>(CustomAttribute43Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute44Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute44);
        ///// <summary>
        ///// CustomAttribute44
        ///// </summary>
        //public object CustomAttribute44
        //{
        //    get { return GetProperty<object>(CustomAttribute44Property); }
        //    set { SetProperty<object>(CustomAttribute44Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute45Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute45);
        ///// <summary>
        ///// CustomAttribute45
        ///// </summary>
        //public object CustomAttribute45
        //{
        //    get { return GetProperty<object>(CustomAttribute45Property); }
        //    set { SetProperty<object>(CustomAttribute45Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute46Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute46);
        ///// <summary>
        ///// CustomAttribute46
        ///// </summary>
        //public object CustomAttribute46
        //{
        //    get { return GetProperty<object>(CustomAttribute46Property); }
        //    set { SetProperty<object>(CustomAttribute46Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute47Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute47);
        ///// <summary>
        ///// CustomAttribute47
        ///// </summary>
        //public object CustomAttribute47
        //{
        //    get { return GetProperty<object>(CustomAttribute47Property); }
        //    set { SetProperty<object>(CustomAttribute47Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute48Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute48);
        ///// <summary>
        ///// CustomAttribute48
        ///// </summary>
        //public object CustomAttribute48
        //{
        //    get { return GetProperty<object>(CustomAttribute48Property); }
        //    set { SetProperty<object>(CustomAttribute48Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute49Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute49);
        ///// <summary>
        ///// CustomAttribute49
        ///// </summary>
        //public object CustomAttribute49
        //{
        //    get { return GetProperty<object>(CustomAttribute49Property); }
        //    set { SetProperty<object>(CustomAttribute49Property, value); }
        //}

        //public static readonly ModelPropertyInfo<object> CustomAttribute50Property =
        //  RegisterModelProperty<object>(c => c.CustomAttribute50);
        ///// <summary>
        ///// CustomAttribute50
        ///// </summary>
        //public object CustomAttribute50
        //{
        //    get { return GetProperty<object>(CustomAttribute50Property); }
        //    set { SetProperty<object>(CustomAttribute50Property, value); }
        //}

        //#endregion

        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            //BusinessRules.AddRule(new Required(GTINProperty)); // (GFS-16886) No particular property here is required
            //BusinessRules.AddRule(new Required(DescriptionProperty));
            // GFS-22179 : CasePackUnitsProperty no longer required ...
            //BusinessRules.AddRule(new Required(CasePackUnitsProperty)); // case pack value already populated and must stay as required GFS-16415
            //BusinessRules.AddRule(new Required(TrayPackUnitsProperty));
            //BusinessRules.AddRule(new Required(DaysOfSupplyProperty));

            #region Case

            // GFS-22179 : CasePackUnits are now allowed to be null
            //BusinessRules.AddRule(new MinValue<Int16>(CasePackUnitsProperty, 1));
            //BusinessRules.AddRule(new MaxValue<Int16>(CasePackUnitsProperty, 9999));
            //BusinessRules.AddRule(new Framework.Rules.NullableMinValue<Byte>(CaseHighProperty, 0));
            //BusinessRules.AddRule(new Framework.Rules.NullableMinValue<Byte>(CaseWideProperty, 0));
            //BusinessRules.AddRule(new Framework.Rules.NullableMinValue<Byte>(CaseDeepProperty, 0));

            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMinValue<Int16>(CasePackUnitsProperty, 1));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMaxValue<Int16>(CasePackUnitsProperty, 9999));

            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMinValue<Byte>(CaseHighProperty, 1));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMinValue<Byte>(CaseWideProperty, 1));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMinValue<Byte>(CaseDeepProperty, 1));

            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMinValue<Single>(CaseHeightProperty, 0.0f));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMaxValue<Single>(CaseHeightProperty, 9999.99f));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMinValue<Single>(CaseWidthProperty, 0.0f));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMaxValue<Single>(CaseWidthProperty, 9999.99f));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMinValue<Single>(CaseDepthProperty, 0.0f));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMaxValue<Single>(CaseDepthProperty, 9999.99f));

            #endregion

            #region Tray
            // GFS-22179 : TrayPackUnits are now allowed to be null
            //BusinessRules.AddRule(new MinValue<Int16>(TrayPackUnitsProperty, 0));
            //BusinessRules.AddRule(new MaxValue<Int16>(TrayPackUnitsProperty, 9999));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMinValue<Int16>(TrayPackUnitsProperty, 1));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMaxValue<Int16>(TrayPackUnitsProperty, 9999));

            //BusinessRules.AddRule(new Framework.Rules.NullableMinValue<Byte>(TrayHighProperty, 0));
            //BusinessRules.AddRule(new Framework.Rules.NullableMinValue<Byte>(TrayWideProperty, 0));
            //BusinessRules.AddRule(new Framework.Rules.NullableMinValue<Byte>(TrayDeepProperty, 0));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMinValue<Byte>(TrayHighProperty, 1));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMinValue<Byte>(TrayWideProperty, 1));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMinValue<Byte>(TrayDeepProperty, 1));

            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMinValue<Single>(TrayHeightProperty, 0.0f));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMaxValue<Single>(TrayHeightProperty, 9999.99f));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMinValue<Single>(TrayWidthProperty, 0.0f));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMaxValue<Single>(TrayWidthProperty, 9999.99f));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMinValue<Single>(TrayDepthProperty, 0.0f));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMaxValue<Single>(TrayDepthProperty, 9999.99f));

            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMinValue<Single>(TrayThickHeightProperty, 0.0f));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMaxValue<Single>(TrayThickHeightProperty, 10.0f));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMinValue<Single>(TrayThickWidthProperty, 0.0f));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMaxValue<Single>(TrayThickWidthProperty, 10.0f));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMinValue<Single>(TrayThickDepthProperty, 0.0f));
            BusinessRules.AddRule(new Galleria.Framework.Rules.NullableMaxValue<Single>(TrayThickDepthProperty, 10.0f));

            #endregion

            BusinessRules.AddRule(new MaxLength(GTINProperty, 14));
            BusinessRules.AddRule(new MaxLength(DescriptionProperty, 50));
            BusinessRules.AddRule(new MaxLength(DeliveryMethodProperty, 20));
            BusinessRules.AddRule(new MaxLength(VendorCodeProperty, 20));
            BusinessRules.AddRule(new MaxLength(VendorProperty, 50));
            BusinessRules.AddRule(new MaxLength(ManufacturerCodeProperty, 20));
            BusinessRules.AddRule(new MaxLength(ManufacturerProperty, 50));
            BusinessRules.AddRule(new MaxLength(SizeProperty, 20));
            BusinessRules.AddRule(new MaxLength(UnitOfMeasureProperty, 20));
            BusinessRules.AddRule(new MaxLength(SellPackDescriptionProperty, 100));
            BusinessRules.AddRule(new MaxLength(ConsumerInformationProperty, 50));
            BusinessRules.AddRule(new MaxLength(PatternProperty, 20));
            BusinessRules.AddRule(new MaxLength(ModelProperty, 20));
            BusinessRules.AddRule(new MaxLength(CorporateCodeProperty, 20));
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationProductAttribute), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationProductAttributeCreate.ToString()));
            BusinessRules.AddRule(typeof(LocationProductAttribute), new IsInRole(AuthorizationActions.GetObject, DomainPermission.LocationProductAttributeGet.ToString()));
            BusinessRules.AddRule(typeof(LocationProductAttribute), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationProductAttributeEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationProductAttribute), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationProductAttributeDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static LocationProductAttribute NewLocationProductAttributeAsChild(Int16 locationId, Int32 productId, Int32 entityId)
        {
            LocationProductAttribute item = new LocationProductAttribute();
            item.Create(locationId, productId, entityId);
            item.MarkAsChild();
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static LocationProductAttribute NewLocationProductAttribute(Int16 locationId, Int32 productId, Int32 entityId)
        {
            LocationProductAttribute item = new LocationProductAttribute();
            item.Create(locationId, productId, entityId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int16 locationId, Int32 productId, Int32 entityId)
        {
            LoadProperty<Int32>(EntityIdProperty, entityId);
            LoadProperty<Int16>(LocationIdProperty, locationId);
            LoadProperty<Int32>(ProductIdProperty, productId);
            //LoadProperty<Int16>(CasePackUnitsProperty, 1);
            //LoadProperty<Byte?>(CaseHighProperty, 1);
            //LoadProperty<Byte?>(CaseWideProperty, 1);
            //LoadProperty<Byte?>(CaseDeepProperty, 1);
            //LoadProperty<Byte?>(TrayHighProperty, 1);
            //LoadProperty<Byte?>(TrayWideProperty, 1);
            // LoadProperty<Byte?>(TrayDeepProperty, 1);
            LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            base.Create();
        }
        #endregion

        #endregion

        #region Overrides

        public override String ToString()
        {
            return String.Format("{0} {1}", this.GTIN, this.Description);
        }

        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByLocationIdProductIdCombinations
        /// </summary>
        [Serializable]
        public class FetchByLocationIdProductIdCriteria : Csla.CriteriaBase<FetchByLocationIdProductIdCriteria>
        {
            #region Properties

            #region EntityId
            /// <summary>
            /// Entity id property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            /// <summary>
            /// Entity id
            /// </summary>
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }
            #endregion

            #region LocationId
            /// <summary>
            /// LocationId property definition
            /// </summary>
            public static readonly PropertyInfo<Int16> LocationIdProperty =
                RegisterProperty<Int16>(c => c.LocationId);
            /// <summary>
            /// LocationId
            /// </summary>
            public Int16 LocationId
            {
                get { return ReadProperty<Int16>(LocationIdProperty); }
            }
            #endregion

            #region ProductId
            /// <summary>
            /// ProductId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> ProductIdProperty =
                RegisterProperty<Int32>(c => c.ProductId);
            /// <summary>
            /// ProductId
            /// </summary>
            public Int32 ProductId
            {
                get { return ReadProperty<Int32>(ProductIdProperty); }
            }
            #endregion

            #endregion


            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByLocationIdProductIdCriteria(Int16 locationId, Int32 productId)
            {
                //LoadProperty<Int32>(EntityIdProperty, this.EntityId);
                LoadProperty<Int16>(LocationIdProperty, locationId);
                LoadProperty<Int32>(ProductIdProperty, productId);
            }
            #endregion
        }

        #endregion

        //#region Mapping
        ///// <summary>
        ///// Creates all mappings for this type
        ///// </summary>        
        //internal static void CreateMappings()
        //{
        //    Mapper.CreateMap<LocationProductAttribute, LocationProductAttributeDc>()
        //        .ForMember(dest => dest.LocationCode, map => map.ResolveUsing<LocationIdToCodeResolver>().FromMember(src => src.LocationId))
        //        .ForMember(dest => dest.CasePackUnits, map => map.NullSubstitute(default(Int16)))
        //        .ForMember(dest => dest.DaysOfSupply, map => map.NullSubstitute(default(Byte)))
        //        .ForMember(dest => dest.TrayPackUnits, map => map.NullSubstitute(default(Int16)));

        //}
        //#endregion

        #region Methods

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos()
        {
            Type type = typeof(LocationProductAttribute);
            String typeFriendly = Product.FriendlyName;

            String attributesGroup = Message.Product_PropertyGroup_Attributes;

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.CaseCostProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.CaseDeepProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.CaseDepthProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.CaseHeightProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.CaseHighProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.CasePackUnitsProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.CaseWideProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.CaseWidthProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.ConsumerInformationProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.CorporateCodeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.CostPriceProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.DeliveryFrequencyDaysProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.DeliveryMethodProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.DepthProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.DescriptionProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.GTINProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.HeightProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.ManufacturerCodeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.ManufacturerProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.MaxDeepProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.MinDeepProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.ModelProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.PatternProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.RecommendedRetailPriceProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.SellPackCountProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.SellPackDescriptionProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.SellPriceProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.ShelfLifeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.SizeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.StatusTypeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.TrayDeepProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.TrayDepthProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.TrayHeightProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.TrayHighProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.TrayPackUnitsProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.TrayThickDepthProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.TrayThickHeightProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.TrayThickWidthProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.TrayWideProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.TrayWidthProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.UnitOfMeasureProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.VendorCodeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.VendorProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, LocationProductAttribute.WidthProperty, attributesGroup);

        }

        #endregion

        #region Helpers

        /// <summary>
        /// Returns true if the equivalent product property would be an 
        /// attribute data one.
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static Boolean IsProductAttributeDataProperty(String propertyName)
        {
            if (propertyName == LocationProductAttribute.ShelfLifeProperty.Name
                || propertyName == LocationProductAttribute.DeliveryFrequencyDaysProperty.Name
                || propertyName == LocationProductAttribute.DeliveryMethodProperty.Name
                || propertyName == LocationProductAttribute.VendorCodeProperty.Name
                || propertyName == LocationProductAttribute.VendorProperty.Name
                || propertyName == LocationProductAttribute.ManufacturerCodeProperty.Name
                || propertyName == LocationProductAttribute.ManufacturerProperty.Name
                || propertyName == LocationProductAttribute.SizeProperty.Name
                || propertyName == LocationProductAttribute.UnitOfMeasureProperty.Name
                || propertyName == LocationProductAttribute.SellPriceProperty.Name
                || propertyName == LocationProductAttribute.SellPackCountProperty.Name
                || propertyName == LocationProductAttribute.SellPackDescriptionProperty.Name
                || propertyName == LocationProductAttribute.RecommendedRetailPriceProperty.Name
                || propertyName == LocationProductAttribute.CostPriceProperty.Name
                || propertyName == LocationProductAttribute.CaseCostProperty.Name
                || propertyName == LocationProductAttribute.ConsumerInformationProperty.Name
                || propertyName == LocationProductAttribute.PatternProperty.Name
                || propertyName == LocationProductAttribute.ModelProperty.Name
                || propertyName == LocationProductAttribute.CorporateCodeProperty.Name)
            {
                return true;
            }

            return false;
        }

        #endregion
    }
}
