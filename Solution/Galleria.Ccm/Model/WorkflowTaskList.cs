﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
// V8-25460 : L.Ineson
//  Added MoveUp and MoveDown methods.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class WorkflowTaskList : ModelList<WorkflowTaskList, WorkflowTask>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent workflow
        /// </summary>
        public new Workflow Parent
        {
            get { return (Workflow)base.Parent; }
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(WorkflowTaskList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.WorkflowCreate.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.WorkflowEdit.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.WorkflowDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        internal static WorkflowTaskList NewWorkflowTaskList()
        {
            WorkflowTaskList item = new WorkflowTaskList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Creates a new task based on the specified engine task type
        /// </summary>
        public WorkflowTask Add(EngineTaskInfo task)
        {
            WorkflowTask item = WorkflowTask.NewWorkflowTask(task, (Byte)this.Count);
            this.Add(item);
            return item;
        }

        /// <summary>
        /// Called when an item is removed from this workflow
        /// </summary>
        protected override void RemoveItem(int index)
        {
            // remove the item
            base.RemoveItem(index);

            // re-sequence the remaining items
            Byte sequenceId = 0;
            IEnumerable<WorkflowTask> items = this.OrderBy(item => item.SequenceId);
            foreach (WorkflowTask item in items)
            {
                item.SequenceId = sequenceId;
                sequenceId++;
            }
        }

        /// <summary>
        /// Moves the task up in the sequence ordering.
        /// </summary>
        /// <param name="task"></param>
        public void MoveUp(WorkflowTask task)
        {
            if(Contains(task) && task.SequenceId != 0)
            {
                //determine the new order
                List<WorkflowTask> items = this.OrderBy(item => item.SequenceId).ToList();
                Int32 oldIdx = items.IndexOf(task);
                items.Remove(task);
                items.Insert(oldIdx - 1, task);

                //re-sequence
                Byte sequenceId = 0;
                foreach (WorkflowTask item in items)
                {
                    item.SequenceId = sequenceId;
                    sequenceId++;
                }
            }
        }

        /// <summary>
        /// Moves the task down in the sequence ordering.
        /// </summary>
        /// <param name="task"></param>
        public void MoveDown(WorkflowTask task)
        {
            if (Contains(task) && task.SequenceId != (Byte)this.Count-1)
            {
                //determine the new order
                List<WorkflowTask> items = this.OrderBy(item => item.SequenceId).ToList();
                Int32 oldIdx = items.IndexOf(task);
                items.Remove(task);
                items.Insert(oldIdx + 1, task);

                //re-sequence
                Byte sequenceId = 0;
                foreach (WorkflowTask item in items)
                {
                    item.SequenceId = sequenceId;
                    sequenceId++;
                }
            }
        }

        #endregion
    }
}
