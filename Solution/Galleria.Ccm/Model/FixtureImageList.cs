﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Linq;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of FixtureImage objects.
    /// </summary>
    [Serializable]
    public sealed partial class FixtureImageList : ModelList<FixtureImageList, FixtureImage>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new FixturePackage Parent
        {
            get { return (FixturePackage)base.Parent; }
        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static FixtureImageList NewFixtureImageList()
        {
            FixtureImageList item = new FixtureImageList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        public FixtureImage FindById(Object id)
        {
            return this.FirstOrDefault(f=> Object.Equals(f.Id, id));
        }

        #endregion
    }
}