﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 800)

// CCM-V8-24863 : L.Hodson
//		Created
// CCM-24979 : L.Hodson
//      Added FixtureLibraryLocation property
// V8-25395 : A.Probyn
//      Updated IsLibraryPanelVisibleProperty to be false on default
//      Added ProductLibraryLocation & LastUsedProductLibraryLocation
// V8-26306 : L.Ineson
//      Copied over
// V8-26787 : L.Ineson
//  Updated fields to use square brackets
// V8-27154 : L.Luong
//      Added ProductPlacement X,Y,Z
// V8-27239 : I.George
//  Added CanSelectComponents and CanSelectPosition Properties
// V8-27266 : L.Ineson
//  Added folder locations for labels and highlights.
//  Renamed slotlines to divider lines
//  Renamed show chests top down to rotate top down components.
// V8-27694 : L.Ineson
//  Uom properties are now enums.
// V8-27625 : A.Silva
//      Added IsRenumberOnSaveEnabled property, false by default.
// V8-27804 : L.Ineson
//  Added PlanogramFileTemplateLocationProperty
// V8-27938 : N.Haywood
//  Added Data Sheet location and favourites
// V8-28051 : J.Pickup
//  LoadDefaults() Now takes into account the type of installshield instalation used and sets default folders accordingly.
// V8-28220 : D.Pleasance
//  Added default for DefaultSpacemanFileTemplateProperty

#endregion

#region Version History: CCM801

// V8-28676 : A.Silva
//      Added product image settings releated properties.
// V8-28951 : A.Silva
//      Amended default value for ProductImageAttributeProperty (now using the Gtin property from PlanogramProduct).

#endregion

#region Version History: CCM802

// V8-27828 : J.Pickup
//      Introduced IsShowSelectionAsBlinkingEnabled
// V8-29054 : M.Pettit
//  Added Validation warning visibility settings
// V8-28766 : J.Pickup
//  ProductPlacementType refactored from single to Manual. 
// V8-27775 : L.Ineson
//  Added new setting IsDynamicTextScalingEnabled

#endregion

#region Version History: CCM803

// V8-29751 : L.Ineson
//  Added highlight setting.

#endregion

#region Version History: CCM810

//V8-29455 : L.Ineson
// Defaults for Component and Position properties now get populated.
// V8-29592 : L.Ineson
//  Added imperial defaults

#endregion

#region Version History: CCM811

// V8-30263 : A.Silva
//  Set default value 'false' for ShowComponentIsOverFilledWarning.

#endregion

#region Version History: CCM820

// V8-30791 : D.Pleasance
//  Added User Colors
//V8-30738 : L.Ineson
//  Added DefaultPrintTemplateId setting.
// V8-30932 : L.Ineson
//  Removed CanUserSelectPositions and CanUserSelectComponents.
// V8-30705 : A.Probyn
//  Added new properties to support showing assortment warnings.
// V8-30936 : M.Brumby
//  Slotwall defaults
// V8-29439 : J.Pickup
//  lots of instant warnings introduced.
//V8-30193 : L.Ineson
//  Removed IsRenumberOnSaveEnabled.
// V8-31078 : L.Ineson
//  Added PrintTemplateLocation
// V8-31057 - M.Brumby
//  PCR01417 Default Apollo Tempate
// V8-31111 : A.Silva
//  Added Default template file for ProSpace.

#endregion

#region Version History: CCM830
// V8-31546 : M.Pettit
//  Added DefaultExportApolloFileTemplate,DefaultExportJDAFileTemplate,DefaultExportSpacemanFileTemplate
//  Added PlanogramExportFileTemplateLocation
// V8-31699 : A.Heathcote 
//  Added Recent Label, Recent Highlight and Recent Datasheet to properties.
// V8-31835 : N.Haywood
//  Added Illegal and Legal MetaData and Validation
// V8-31945 : A.Silva
//  Added PlanogramComparisonTemplateLocation.
// V8-32085 : N.Haywood
//  Added StatusBarTextFontSize
// V8-31934 : A.Heathcote
//  Added SelectedSearchColumns (Name soon to be refactored)
// V8-32141 : M.Pettit
//  Changed default export folder path in LoadDefaults() method
// V8-32361 : L.Ineson
//  Added default product universe template id.
// V8-32361 : J.Pickup
//  Added IsComponentOutsideOfFixtureAreaWarnings
// V8-32733 : A.Silva
//  Added Default Planogram Comparison Template Id.
// V8-32626 : A.Probyn
//  Added Annotations
// V8-32591 : L.Ineson
//  Added textbox defaults.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.External;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Holds config settings for the application
    /// </summary>
    [Serializable]
    public sealed partial class UserEditorSettings :
        ModelObject<UserEditorSettings>,
        IPlanogramSettings,
        IRealImageProviderSettings
    {
        #region Properties

        #region DalFactoryName
        /// <summary>
        /// DalFactoryName property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> DalFactoryNameProperty =
           RegisterModelProperty<String>(c => c.DalFactoryName);

        /// <summary>
        /// Returns the name of the dal factory to use.
        /// </summary>
        public new String DalFactoryName
        {
            get { return GetProperty<String>(DalFactoryNameProperty); }
        }
        #endregion

        #region DisplayLanguageCode

        /// <summary>
        /// DisplayLanguageCode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DisplayLanguageCodeProperty =
           RegisterModelProperty<String>(c => c.DisplayLanguageCode);

        /// <summary>
        /// Gets/Sets the display language code to use.
        /// </summary>
        public String DisplayLanguageCode
        {
            get { return GetProperty<String>(DisplayLanguageCodeProperty); }
            set { SetProperty<String>(DisplayLanguageCodeProperty, value); }
        }

        #endregion

        #region IsAutosaveEnabled

        /// <summary>
        /// IsAutosaveEnabled property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsAutosaveEnabledProperty =
            RegisterModelProperty<Boolean>(c => c.IsAutosaveEnabled);

        /// <summary>
        /// Gets/Sets whether autosave is enabled
        /// </summary>
        public Boolean IsAutosaveEnabled
        {
            get { return GetProperty<Boolean>(IsAutosaveEnabledProperty); }
            set { SetProperty<Boolean>(IsAutosaveEnabledProperty, value); }
        }

        #endregion

        #region AutosaveInterval

        /// <summary>
        /// AutosaveInterval property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> AutosaveIntervalProperty =
            RegisterModelProperty<Int32>(c => c.AutosaveInterval);

        /// <summary>
        /// Gets/Sets the interval between each autosave in minutes.
        /// </summary>
        public Int32 AutosaveInterval
        {
            get { return GetProperty<Int32>(AutosaveIntervalProperty); }
            set { SetProperty<Int32>(AutosaveIntervalProperty, value); }
        }

        #endregion

        #region Locations

        #region AutosaveLocation

        /// <summary>
        /// AutosaveLocation property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> AutosaveLocationProperty =
            RegisterModelProperty<String>(c => c.AutosaveLocation);

        /// <summary>
        /// Gets/Sets the location to which items should be autosaved
        /// </summary>
        public String AutosaveLocation
        {
            get { return GetProperty<String>(AutosaveLocationProperty); }
            set { SetProperty<String>(AutosaveLocationProperty, value); }
        }

        #endregion

        #region ColumnLayoutsLocation

        /// <summary>
        /// ColumnLayoutsLocation property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ColumnLayoutLocationProperty =
            RegisterModelProperty<String>(c => c.ColumnLayoutLocation);

        /// <summary>
        /// Get/Sets the default column layouts directory.
        /// </summary>
        public String ColumnLayoutLocation
        {
            get { return GetProperty<String>(ColumnLayoutLocationProperty); }
            set { SetProperty(ColumnLayoutLocationProperty, value); }
        }

        #endregion

        #region FixtureLibraryLocation

        /// <summary>
        /// FixtureLibraryLocation property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> FixtureLibraryLocationProperty =
            RegisterModelProperty<String>(c => c.FixtureLibraryLocation);

        /// <summary>
        /// Gets/Sets the default fixtures directory.
        /// </summary>
        public String FixtureLibraryLocation
        {
            get { return GetProperty<String>(FixtureLibraryLocationProperty); }
            set { SetProperty<String>(FixtureLibraryLocationProperty, value); }
        }

        #endregion

        #region HighlightLocation

        /// <summary>
        /// HighlightLocation property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> HighlightLocationProperty =
            RegisterModelProperty<String>(c => c.HighlightLocation);

        /// <summary>
        /// Get/Sets the default highlights directory.
        /// </summary>
        public String HighlightLocation
        {
            get { return GetProperty<String>(HighlightLocationProperty); }
            set { SetProperty<String>(HighlightLocationProperty, value); }
        }

        #endregion

        #region ImageLocation

        /// <summary>
        /// ImageLocation property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ImageLocationProperty =
            RegisterModelProperty<String>(c => c.ImageLocation);

        /// <summary>
        /// Gets/Sets the default images directory.
        /// </summary>
        public String ImageLocation
        {
            get { return GetProperty<String>(ImageLocationProperty); }
            set { SetProperty<String>(ImageLocationProperty, value); }
        }

        #endregion

        #region LabelLocation

        /// <summary>
        /// LabelLocation property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> LabelLocationProperty =
            RegisterModelProperty<String>(c => c.LabelLocation);
        /// <summary>
        /// Gets/Sets the default labels directory
        /// </summary>
        public String LabelLocation
        {
            get { return GetProperty<String>(LabelLocationProperty); }
            set { SetProperty<String>(LabelLocationProperty, value); }
        }
        #endregion

        #region PlanogramLocation

        /// <summary>
        /// PlanogramLocation property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> PlanogramLocationProperty =
            RegisterModelProperty<String>(c => c.PlanogramLocation);

        /// <summary>
        /// Gets/Sets the default planograms directory.
        /// </summary>
        public String PlanogramLocation
        {
            get { return GetProperty<String>(PlanogramLocationProperty); }
            set { SetProperty<String>(PlanogramLocationProperty, value); }
        }

        #endregion

        #region PlanogramComparisonLocation

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}"/> for the <see cref="PlanogramComparisonTemplateLocation"/> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> PlanogramComparisonTemplateLocationProperty =
            RegisterModelProperty<String>(c => c.PlanogramComparisonTemplateLocation);

        /// <summary>
        ///     Get/Sets the default Planogram Comparison template directory.
        /// </summary>
        public String PlanogramComparisonTemplateLocation
        {
            get { return GetProperty(PlanogramComparisonTemplateLocationProperty); }
            set { SetProperty(PlanogramComparisonTemplateLocationProperty, value); }
        }

        #endregion

        #region PlanogramExportFileTemplateLocation
        /// <summary>
        /// PlanogramExportFileTemplateLocation property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> PlanogramExportFileTemplateLocationProperty =
            RegisterModelProperty<String>(c => c.PlanogramExportFileTemplateLocation);
        /// <summary>
        /// Gets/Sets the default export file template directory.
        /// </summary>
        public String PlanogramExportFileTemplateLocation
        {
            get { return GetProperty<String>(PlanogramExportFileTemplateLocationProperty); }
            set { SetProperty<String>(PlanogramExportFileTemplateLocationProperty, value); }
        }
        #endregion

        #region PlanogramFileTemplateLocation
        /// <summary>
        /// PlanogramFileTemplateLocation property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> PlanogramFileTemplateLocationProperty =
            RegisterModelProperty<String>(c => c.PlanogramFileTemplateLocation);
        /// <summary>
        /// Gets/Sets the default file template directory.
        /// </summary>
        public String PlanogramFileTemplateLocation
        {
            get { return GetProperty<String>(PlanogramFileTemplateLocationProperty); }
            set { SetProperty<String>(PlanogramFileTemplateLocationProperty, value); }
        }
        #endregion

        #region PrintTemplateLocation

        /// <summary>
        /// PrintTemplateLocation property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> PrintTemplateLocationProperty =
            RegisterModelProperty<String>(c => c.PrintTemplateLocation);

        /// <summary>
        /// Gets/Sets the default print templates directory.
        /// </summary>
        public String PrintTemplateLocation
        {
            get { return GetProperty<String>(PrintTemplateLocationProperty); }
            set { SetProperty<String>(PrintTemplateLocationProperty, value); }
        }

        #endregion

        #region ValidationTemplateLocation

        /// <summary>
        /// ValidationTemplateLocation property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ValidationTemplateLocationProperty =
             RegisterModelProperty<String>(c => c.ValidationTemplateLocation);

        /// <summary>
        /// Get/Sets the default validation template directory.
        /// </summary>
        public String ValidationTemplateLocation
        {
            get { return GetProperty<String>(ValidationTemplateLocationProperty); }
            set { SetProperty(ValidationTemplateLocationProperty, value); }
        }

        #endregion

        #endregion

        #region Data Sheets

        /// <summary>
        /// DataSheetLayoutLocation property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DataSheetLayoutLocationProperty =
            RegisterModelProperty<String>(c => c.DataSheetLayoutLocation);

        /// <summary>
        /// Get/Sets the default datasheet directory.
        /// </summary>
        public String DataSheetLayoutLocation
        {
            get { return GetProperty<String>(DataSheetLayoutLocationProperty); }
            set { SetProperty(DataSheetLayoutLocationProperty, value); }
        }

        /// <summary>
        /// DataSheetFavourite1 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DataSheetFavourite1Property =
            RegisterModelProperty<String>(c => c.DataSheetFavourite1);

        /// <summary>
        /// Get/Sets DataSheetFavourite1
        /// </summary>
        public String DataSheetFavourite1
        {
            get { return GetProperty<String>(DataSheetFavourite1Property); }
            set { SetProperty(DataSheetFavourite1Property, value); }
        }

        /// <summary>
        /// DataSheetFavourite2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DataSheetFavourite2Property =
            RegisterModelProperty<String>(c => c.DataSheetFavourite2);

        /// <summary>
        /// Get/Sets DataSheetFavourite2
        /// </summary>
        public String DataSheetFavourite2
        {
            get { return GetProperty<String>(DataSheetFavourite2Property); }
            set { SetProperty(DataSheetFavourite2Property, value); }
        }

        /// <summary>
        /// DataSheetFavourite3 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DataSheetFavourite3Property =
            RegisterModelProperty<String>(c => c.DataSheetFavourite3);

        /// <summary>
        /// Get/Sets DataSheetFavourite3
        /// </summary>
        public String DataSheetFavourite3
        {
            get { return GetProperty<String>(DataSheetFavourite3Property); }
            set { SetProperty(DataSheetFavourite3Property, value); }
        }

        /// <summary>
        /// DataSheetFavourite4 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DataSheetFavourite4Property =
            RegisterModelProperty<String>(c => c.DataSheetFavourite4);

        /// <summary>
        /// Get/Sets DataSheetFavourite4
        /// </summary>
        public String DataSheetFavourite4
        {
            get { return GetProperty<String>(DataSheetFavourite4Property); }
            set { SetProperty(DataSheetFavourite4Property, value); }
        }

        /// <summary>
        /// DataSheetFavourite5 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DataSheetFavourite5Property =
            RegisterModelProperty<String>(c => c.DataSheetFavourite5);

        /// <summary>
        /// Get/Sets DataSheetFavourite5
        /// </summary>
        public String DataSheetFavourite5
        {
            get { return GetProperty<String>(DataSheetFavourite5Property); }
            set { SetProperty(DataSheetFavourite5Property, value); }
        }

        #endregion

        #region Default File Templates

        #region DefaultSpacemanFileTemplate

        /// <summary>
        /// DefaultSpacemanFileTemplate property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DefaultSpacemanFileTemplateProperty =
            RegisterModelProperty<String>(c => c.DefaultSpacemanFileTemplate);

        /// <summary>
        /// Gets/Sets the default template to use when importing a spaceman file.
        /// </summary>
        public String DefaultSpacemanFileTemplate
        {
            get { return GetProperty<String>(DefaultSpacemanFileTemplateProperty); }
            set { SetProperty<String>(DefaultSpacemanFileTemplateProperty, value); }
        }

        #endregion

        #region DefaultApolloFileTemplate

        /// <summary>
        /// DefaultApolloFileTemplate property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DefaultApolloFileTemplateProperty =
            RegisterModelProperty<String>(c => c.DefaultApolloFileTemplate);

        /// <summary>
        /// Gets/Sets the default template to use when importing a Apollo file.
        /// </summary>
        public String DefaultApolloFileTemplate
        {
            get { return GetProperty<String>(DefaultApolloFileTemplateProperty); }
            set { SetProperty<String>(DefaultApolloFileTemplateProperty, value); }
        }

        #endregion

        #region DefaultProspaceFileTemplate

        /// <summary>
        /// DefaultProspaceFileTemplate property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DefaultProspaceFileTemplateProperty =
            RegisterModelProperty<String>(c => c.DefaultProspaceFileTemplate);

        /// <summary>
        /// Gets/Sets the default template to use when importing a Prospace file.
        /// </summary>
        public String DefaultProspaceFileTemplate
        {
            get { return GetProperty<String>(DefaultProspaceFileTemplateProperty); }
            set { SetProperty<String>(DefaultProspaceFileTemplateProperty, value); }
        }

        #endregion

        #region DefaultExportSpacemanFileTemplate

        /// <summary>
        /// DefaultExportSpacemanFileTemplate property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DefaultExportSpacemanFileTemplateProperty =
            RegisterModelProperty<String>(c => c.DefaultExportSpacemanFileTemplate);

        /// <summary>
        /// Gets/Sets the default template to use when exporting a spaceman file.
        /// </summary>
        public String DefaultExportSpacemanFileTemplate
        {
            get { return GetProperty<String>(DefaultExportSpacemanFileTemplateProperty); }
            set { SetProperty<String>(DefaultExportSpacemanFileTemplateProperty, value); }
        }

        #endregion

        #region DefaultExportApolloFileTemplate

        /// <summary>
        /// DefaultExportApolloFileTemplate property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DefaultExportApolloFileTemplateProperty =
            RegisterModelProperty<String>(c => c.DefaultExportApolloFileTemplate);

        /// <summary>
        /// Gets/Sets the default template to use when exporting an Apollo file.
        /// </summary>
        public String DefaultExportApolloFileTemplate
        {
            get { return GetProperty<String>(DefaultExportApolloFileTemplateProperty); }
            set { SetProperty<String>(DefaultExportApolloFileTemplateProperty, value); }
        }

        #endregion

        #region DefaultExportJDAFileTemplate

        /// <summary>
        /// DefaultExportJDAFileTemplate property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DefaultExportJDAFileTemplateProperty =
            RegisterModelProperty<String>(c => c.DefaultExportJDAFileTemplate);

        /// <summary>
        /// Gets/Sets the default template to use when exporting a JDA file.
        /// </summary>
        public String DefaultExportJDAFileTemplate
        {
            get { return GetProperty<String>(DefaultExportJDAFileTemplateProperty); }
            set { SetProperty<String>(DefaultExportJDAFileTemplateProperty, value); }
        }

        #endregion

        #endregion

        #region DefaultProductLibrarySource

        /// <summary>
        /// DefaultProductLibrarySource property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DefaultProductLibrarySourceProperty =
            RegisterModelProperty<String>(c => c.DefaultProductLibrarySource);

        /// <summary>
        /// Gets/Sets the default product library directory.
        /// </summary>
        public String DefaultProductLibrarySource
        {
            get { return GetProperty<String>(DefaultProductLibrarySourceProperty); }
            set { SetProperty<String>(DefaultProductLibrarySourceProperty, value); }
        }

        #endregion

        #region DefaultProductLibrarySourceType

        /// <summary>
        /// DefaultProductLibrarySourceType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DefaultProductLibrarySourceType> DefaultProductLibrarySourceTypeProperty =
            RegisterModelProperty<DefaultProductLibrarySourceType>(c => c.DefaultProductLibrarySourceType);

        /// <summary>
        /// Gets/Sets the default product library source type.
        /// </summary>
        public DefaultProductLibrarySourceType DefaultProductLibrarySourceType
        {
            get { return GetProperty<DefaultProductLibrarySourceType>(DefaultProductLibrarySourceTypeProperty); }
            set { SetProperty<DefaultProductLibrarySourceType>(DefaultProductLibrarySourceTypeProperty, value); }
        }

        #endregion

        #region DefaultProductUniverseTemplateId

        /// <summary>
        /// DefaultProductUniverseTemplateId property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> DefaultProductUniverseTemplateIdProperty =
            RegisterModelProperty<String>(o => o.DefaultProductUniverseTemplateId);

        /// <summary>
        ///Gets/Sets the id of the default product universe template to use when the file does
        ///not have a specific one of its own.
        /// </summary>
        public String DefaultProductUniverseTemplateId
        {
            get { return this.GetProperty<String>(DefaultProductUniverseTemplateIdProperty); }
            set { this.SetProperty<String>(DefaultProductUniverseTemplateIdProperty, value); }
        }

        #endregion

        #region DefaultPlanogramComparisonTemplateId

        /// <summary>
        /// DefaultPlanogramComparisonTemplateId property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> DefaultPlanogramComparisonTemplateIdProperty =
            RegisterModelProperty<String>(o => o.DefaultPlanogramComparisonTemplateId);

        /// <summary>
        ///Gets/Sets the id of the default planogram comparison template to use when the plan does
        ///not have a specific one of its own.
        /// </summary>
        public String DefaultPlanogramComparisonTemplateId
        {
            get { return GetProperty(DefaultPlanogramComparisonTemplateIdProperty); }
            set { SetProperty(DefaultPlanogramComparisonTemplateIdProperty, value); }
        }

        #endregion

        #region NewPlanViewLayout

        /// <summary>
        /// NewPlanViewLayout property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NewPlanViewLayoutProperty =
            RegisterModelProperty<String>(c => c.NewPlanViewLayout);

        /// <summary>
        /// Gets/Sets the name of the layout to use when creating a new planogram.
        /// </summary>
        public String NewPlanViewLayout
        {
            get { return GetProperty<String>(NewPlanViewLayoutProperty); }
            set { SetProperty<String>(NewPlanViewLayoutProperty, value); }
        }

        #endregion

        #region ComponentHoverStatusBarText

        /// <summary>
        /// ComponentHoverStatusBarText property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ComponentHoverStatusBarTextProperty =
           RegisterModelProperty<String>(c => c.ComponentHoverStatusBarText);

        /// <summary>
        /// Gets/Sets the format string used to display text in the status 
        /// bar when a component is hovered over.
        /// </summary>
        public String ComponentHoverStatusBarText
        {
            get { return GetProperty<String>(ComponentHoverStatusBarTextProperty); }
            set { SetProperty<String>(ComponentHoverStatusBarTextProperty, value); }
        }

        #endregion

        #region PositionHoverStatusBarText

        /// <summary>
        /// PositionHoverStatusBarText property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> PositionHoverStatusBarTextProperty =
           RegisterModelProperty<String>(c => c.PositionHoverStatusBarText);

        /// <summary>
        /// Gets/Sets the format string used to display text in the status bar when a position is hovered over.
        /// </summary>
        public String PositionHoverStatusBarText
        {
            get { return GetProperty<String>(PositionHoverStatusBarTextProperty); }
            set { SetProperty<String>(PositionHoverStatusBarTextProperty, value); }
        }

        #endregion

        #region StatusBarTextFontSize

        /// <summary>
        /// StatusBarTextFontSize property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> StatusBarTextFontSizeProperty =
           RegisterModelProperty<Byte>(c => c.StatusBarTextFontSize);

        /// <summary>
        /// Gets/Sets font size used in the status bar
        /// </summary>
        public Byte StatusBarTextFontSize
        {
            get { return GetProperty<Byte>(StatusBarTextFontSizeProperty); }
            set { SetProperty<Byte>(StatusBarTextFontSizeProperty, value); }
        }

        #endregion

        #region FixtureLabel

        /// <summary>
        /// FixtureLabel property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> FixtureLabelProperty =
           RegisterModelProperty<String>(c => c.FixtureLabel);

        /// <summary>
        /// Gets/Sets the name of the default fixture label
        /// </summary>
        public String FixtureLabel
        {
            get { return GetProperty<String>(FixtureLabelProperty); }
            set { SetProperty<String>(FixtureLabelProperty, value); }
        }

        #endregion

        #region ProductLabel

        /// <summary>
        /// ProductLabel property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ProductLabelProperty =
          RegisterModelProperty<String>(c => c.ProductLabel);

        /// <summary>
        /// Gets/Sets the name of the default product label
        /// </summary>
        public String ProductLabel
        {
            get { return GetProperty<String>(ProductLabelProperty); }
            set { SetProperty<String>(ProductLabelProperty, value); }
        }

        #endregion

        #region Highlight

        /// <summary>
        /// Highlight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> HighlightProperty =
          RegisterModelProperty<String>(c => c.Highlight, Message.SystemSettings_Highlight);

        /// <summary>
        /// Gets/Sets the name of the default product label
        /// </summary>
        public String Highlight
        {
            get { return GetProperty<String>(HighlightProperty); }
            set { SetProperty<String>(HighlightProperty, value); }
        }

        #endregion

        #region PositionUnits

        /// <summary>
        /// PositionUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PositionUnitsProperty =
          RegisterModelProperty<Boolean>(c => c.PositionUnits);

        /// <summary>
        /// Gets/Sets the initial value for the PositionUnits setting
        /// </summary>
        public Boolean PositionUnits
        {
            get { return GetProperty<Boolean>(PositionUnitsProperty); }
            set { SetProperty<Boolean>(PositionUnitsProperty, value); }
        }

        #endregion

        #region ProductImages

        /// <summary>
        /// ProductImages property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> ProductImagesProperty =
          RegisterModelProperty<Boolean>(c => c.ProductImages);

        /// <summary>
        /// Gets/Sets the initial value for the ProductImages setting
        /// </summary>
        public Boolean ProductImages
        {
            get { return GetProperty<Boolean>(ProductImagesProperty); }
            set { SetProperty<Boolean>(ProductImagesProperty, value); }
        }

        #endregion

        #region ProductShapes

        /// <summary>
        /// ProductShapes property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> ProductShapesProperty =
          RegisterModelProperty<Boolean>(c => c.ProductShapes);

        /// <summary>
        /// Gets/Sets the initial value for the ProductShapes setting
        /// </summary>
        public Boolean ProductShapes
        {
            get { return GetProperty<Boolean>(ProductShapesProperty); }
            set { SetProperty<Boolean>(ProductShapesProperty, value); }
        }

        #endregion

        #region ProductFillPatterns

        /// <summary>
        /// ProductFillPatterns property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> ProductFillPatternsProperty =
          RegisterModelProperty<Boolean>(c => c.ProductFillPatterns);

        /// <summary>
        /// Gets/Sets the initial value for the ProductFillPatterns setting
        /// </summary>
        public Boolean ProductFillPatterns
        {
            get { return GetProperty<Boolean>(ProductFillPatternsProperty); }
            set { SetProperty<Boolean>(ProductFillPatternsProperty, value); }
        }

        #endregion

        #region FixtureImages

        /// <summary>
        /// FixtureImages property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> FixtureImagesProperty =
          RegisterModelProperty<Boolean>(c => c.FixtureImages);

        /// <summary>
        /// Gets/Sets the initial value for the FixtureImages setting
        /// </summary>
        public Boolean FixtureImages
        {
            get { return GetProperty<Boolean>(FixtureImagesProperty); }
            set { SetProperty<Boolean>(FixtureImagesProperty, value); }
        }

        #endregion

        #region FixtureFillPatterns

        /// <summary>
        /// FixtureFillPatterns property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> FixtureFillPatternsProperty =
          RegisterModelProperty<Boolean>(c => c.FixtureFillPatterns);

        /// <summary>
        /// Gets/Sets the initial value for the FixtureFillPatterns setting
        /// </summary>
        public Boolean FixtureFillPatterns
        {
            get { return GetProperty<Boolean>(FixtureFillPatternsProperty); }
            set { SetProperty<Boolean>(FixtureFillPatternsProperty, value); }
        }

        #endregion

        #region ChestWalls

        /// <summary>
        /// ChestWalls property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> ChestWallsProperty =
          RegisterModelProperty<Boolean>(c => c.ChestWalls);

        /// <summary>
        /// Gets/Sets the initial value for the ChestWalls setting
        /// </summary>
        public Boolean ChestWalls
        {
            get { return GetProperty<Boolean>(ChestWallsProperty); }
            set { SetProperty<Boolean>(ChestWallsProperty, value); }
        }

        #endregion

        #region RotateTopDownComponents

        /// <summary>
        /// ChestsTopDown property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> RotateTopDownComponentsProperty =
          RegisterModelProperty<Boolean>(c => c.RotateTopDownComponents);

        /// <summary>
        /// Gets/Sets the initial value for the RotateTopDownComponents setting
        /// </summary>
        public Boolean RotateTopDownComponents
        {
            get { return GetProperty<Boolean>(RotateTopDownComponentsProperty); }
            set { SetProperty<Boolean>(RotateTopDownComponentsProperty, value); }
        }

        #endregion

        #region ShelfRisers

        /// <summary>
        /// ShelfRisers property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> ShelfRisersProperty =
          RegisterModelProperty<Boolean>(c => c.ShelfRisers);

        /// <summary>
        /// Gets/Sets the initial value for the ShelfRisers setting
        /// </summary>
        public Boolean ShelfRisers
        {
            get { return GetProperty<Boolean>(ShelfRisersProperty); }
            set { SetProperty<Boolean>(ShelfRisersProperty, value); }
        }

        #endregion

        #region Notches

        /// <summary>
        /// Notches property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> NotchesProperty =
          RegisterModelProperty<Boolean>(c => c.Notches);

        /// <summary>
        /// Gets/Sets the initial value for the Notches setting
        /// </summary>
        public Boolean Notches
        {
            get { return GetProperty<Boolean>(NotchesProperty); }
            set { SetProperty<Boolean>(NotchesProperty, value); }
        }

        #endregion

        #region PegHoles

        /// <summary>
        /// Pegholes property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PegHolesProperty =
          RegisterModelProperty<Boolean>(c => c.PegHoles);

        /// <summary>
        /// Gets/Sets the initial value for the Pegholes setting
        /// </summary>
        public Boolean PegHoles
        {
            get { return GetProperty<Boolean>(PegHolesProperty); }
            set { SetProperty<Boolean>(PegHolesProperty, value); }
        }

        #endregion

        #region Pegs

        /// <summary>
        /// Pegs property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PegsProperty =
          RegisterModelProperty<Boolean>(c => c.Pegs);

        /// <summary>
        /// Gets/Sets the initial value for the Pegs setting
        /// </summary>
        public Boolean Pegs
        {
            get { return GetProperty<Boolean>(PegsProperty); }
            set { SetProperty<Boolean>(PegsProperty, value); }
        }

        #endregion

        #region DividerLines

        /// <summary>
        /// DividerLines property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> DividerLinesProperty =
          RegisterModelProperty<Boolean>(c => c.DividerLines);

        /// <summary>
        /// Gets/Sets the initial value for the DividerLines setting
        /// </summary>
        public Boolean DividerLines
        {
            get { return GetProperty<Boolean>(DividerLinesProperty); }
            set { SetProperty<Boolean>(DividerLinesProperty, value); }
        }

        #endregion

        #region Dividers

        /// <summary>
        /// Dividers property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> DividersProperty =
          RegisterModelProperty<Boolean>(c => c.Dividers);

        /// <summary>
        /// Gets/Sets the initial value for the Dividers setting
        /// </summary>
        public Boolean Dividers
        {
            get { return GetProperty<Boolean>(DividersProperty); }
            set { SetProperty<Boolean>(DividersProperty, value); }
        }

        #endregion
        
        #region Annotations

        /// <summary>
        /// Annotations property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> AnnotationsProperty =
          RegisterModelProperty<Boolean>(c => c.Annotations);

        /// <summary>
        /// Gets/Sets the initial value for the Annotations setting
        /// </summary>
        public Boolean Annotations
        {
            get { return GetProperty<Boolean>(AnnotationsProperty); }
            set { SetProperty<Boolean>(AnnotationsProperty, value); }
        }

        #endregion

        #region LengthUnitOfMeasure

        /// <summary>
        /// UnitOfMeasure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramLengthUnitOfMeasureType> LengthUnitOfMeasureProperty =
          RegisterModelProperty<PlanogramLengthUnitOfMeasureType>(c => c.LengthUnitOfMeasure);

        /// <summary>
        /// Gets/Sets the default uom
        /// </summary>
        public PlanogramLengthUnitOfMeasureType LengthUnitOfMeasure
        {
            get { return GetProperty<PlanogramLengthUnitOfMeasureType>(LengthUnitOfMeasureProperty); }
            set { SetProperty<PlanogramLengthUnitOfMeasureType>(LengthUnitOfMeasureProperty, value); }
        }

        #endregion

        #region CurrencyUnitOfMeasure

        /// <summary>
        /// Currency property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramCurrencyUnitOfMeasureType> CurrencyUnitOfMeasureProperty =
          RegisterModelProperty<PlanogramCurrencyUnitOfMeasureType>(c => c.CurrencyUnitOfMeasure);

        /// <summary>
        /// Gets/Sets the default currency
        /// </summary>
        public PlanogramCurrencyUnitOfMeasureType CurrencyUnitOfMeasure
        {
            get { return GetProperty<PlanogramCurrencyUnitOfMeasureType>(CurrencyUnitOfMeasureProperty); }
            set { SetProperty<PlanogramCurrencyUnitOfMeasureType>(CurrencyUnitOfMeasureProperty, value); }
        }

        #endregion

        #region VolumeUnitOfMeasure

        /// <summary>
        /// Volume property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramVolumeUnitOfMeasureType> VolumeUnitOfMeasureProperty =
          RegisterModelProperty<PlanogramVolumeUnitOfMeasureType>(c => c.VolumeUnitOfMeasure);

        /// <summary>
        /// Gets/Sets the default volume uom
        /// </summary>
        public PlanogramVolumeUnitOfMeasureType VolumeUnitOfMeasure
        {
            get { return GetProperty<PlanogramVolumeUnitOfMeasureType>(VolumeUnitOfMeasureProperty); }
            set { SetProperty<PlanogramVolumeUnitOfMeasureType>(VolumeUnitOfMeasureProperty, value); }
        }

        #endregion

        #region WeightUnitOfMeasure

        /// <summary>
        /// Weight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramWeightUnitOfMeasureType> WeightUnitOfMeasureProperty =
          RegisterModelProperty<PlanogramWeightUnitOfMeasureType>(c => c.WeightUnitOfMeasure);

        /// <summary>
        /// Gets/Sets the default volume uom
        /// </summary>
        public PlanogramWeightUnitOfMeasureType WeightUnitOfMeasure
        {
            get { return GetProperty<PlanogramWeightUnitOfMeasureType>(WeightUnitOfMeasureProperty); }
            set { SetProperty<PlanogramWeightUnitOfMeasureType>(WeightUnitOfMeasureProperty, value); }
        }

        #endregion

        #region ProductPlacementX

        /// <summary>
        /// Product Placement X property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramProductPlacementXType> ProductPlacementXProperty =
          RegisterModelProperty<PlanogramProductPlacementXType>(c => c.ProductPlacementX);

        /// <summary>
        /// Gets/Sets the default Product Placement X uom
        /// </summary>
        public PlanogramProductPlacementXType ProductPlacementX
        {
            get { return GetProperty<PlanogramProductPlacementXType>(ProductPlacementXProperty); }
            set { SetProperty<PlanogramProductPlacementXType>(ProductPlacementXProperty, value); }
        }

        #endregion

        #region ProductPlacementY

        /// <summary>
        /// Product Placement Y property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramProductPlacementYType> ProductPlacementYProperty =
          RegisterModelProperty<PlanogramProductPlacementYType>(c => c.ProductPlacementY);

        /// <summary>
        /// Gets/Sets the default Product Placement Y uom
        /// </summary>
        public PlanogramProductPlacementYType ProductPlacementY
        {
            get { return GetProperty<PlanogramProductPlacementYType>(ProductPlacementYProperty); }
            set { SetProperty<PlanogramProductPlacementYType>(ProductPlacementYProperty, value); }
        }

        #endregion

        #region ProductPlacementZ

        /// <summary>
        /// Product Placement Z property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramProductPlacementZType> ProductPlacementZProperty =
          RegisterModelProperty<PlanogramProductPlacementZType>(c => c.ProductPlacementZ);

        /// <summary>
        /// Gets/Sets the default Product Placement Z uom
        /// </summary>
        public PlanogramProductPlacementZType ProductPlacementZ
        {
            get { return GetProperty<PlanogramProductPlacementZType>(ProductPlacementZProperty); }
            set { SetProperty<PlanogramProductPlacementZType>(ProductPlacementZProperty, value); }
        }

        #endregion

        #region Default Dimensions & Colours

        #region ProductHeight

        /// <summary>
        /// ProductHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ProductHeightProperty =
          RegisterModelProperty<Single>(c => c.ProductHeight);

        /// <summary>
        /// Gets/Sets the default ProductHeight
        /// </summary>
        public Single ProductHeight
        {
            get { return GetProperty<Single>(ProductHeightProperty); }
            set { SetProperty<Single>(ProductHeightProperty, value); }
        }

        #endregion

        #region ProductWidth

        /// <summary>
        /// ProductWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ProductWidthProperty =
          RegisterModelProperty<Single>(c => c.ProductWidth);

        /// <summary>
        /// Gets/Sets the default ProductWidth
        /// </summary>
        public Single ProductWidth
        {
            get { return GetProperty<Single>(ProductWidthProperty); }
            set { SetProperty<Single>(ProductWidthProperty, value); }
        }

        #endregion

        #region ProductDepth

        /// <summary>
        /// ProductDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ProductDepthProperty =
          RegisterModelProperty<Single>(c => c.ProductDepth);

        /// <summary>
        /// Gets/Sets the default ProductDepth
        /// </summary>
        public Single ProductDepth
        {
            get { return GetProperty<Single>(ProductDepthProperty); }
            set { SetProperty<Single>(ProductDepthProperty, value); }
        }

        #endregion

        #region FixtureHeight

        /// <summary>
        /// FixtureHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FixtureHeightProperty =
          RegisterModelProperty<Single>(c => c.FixtureHeight);

        /// <summary>
        /// Gets/Sets the default FixtureHeight
        /// </summary>
        public Single FixtureHeight
        {
            get { return GetProperty<Single>(FixtureHeightProperty); }
            set { SetProperty<Single>(FixtureHeightProperty, value); }
        }

        #endregion

        #region FixtureWidth

        /// <summary>
        /// FixtureWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FixtureWidthProperty =
          RegisterModelProperty<Single>(c => c.FixtureWidth);

        /// <summary>
        /// Gets/Sets the default FixtureWidth
        /// </summary>
        public Single FixtureWidth
        {
            get { return GetProperty<Single>(FixtureWidthProperty); }
            set { SetProperty<Single>(FixtureWidthProperty, value); }
        }

        #endregion

        #region FixtureDepth

        /// <summary>
        /// FixtureDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FixtureDepthProperty =
          RegisterModelProperty<Single>(c => c.FixtureDepth);

        /// <summary>
        /// Gets/Sets the default FixtureDepth
        /// </summary>
        public Single FixtureDepth
        {
            get { return GetProperty<Single>(FixtureDepthProperty); }
            set { SetProperty<Single>(FixtureDepthProperty, value); }
        }

        #endregion

        #region FixtureHasBackboard

        /// <summary>
        /// FixtureHasBackboard property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> FixtureHasBackboardProperty =
          RegisterModelProperty<Boolean>(c => c.FixtureHasBackboard);

        /// <summary>
        /// Gets/Sets the default FixtureHasBackboard
        /// </summary>
        public Boolean FixtureHasBackboard
        {
            get { return GetProperty<Boolean>(FixtureHasBackboardProperty); }
            set { SetProperty<Boolean>(FixtureHasBackboardProperty, value); }
        }

        #endregion

        #region BackboardDepth

        /// <summary>
        /// BackboardDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BackboardDepthProperty =
          RegisterModelProperty<Single>(c => c.BackboardDepth);

        /// <summary>
        /// Gets/Sets the default BackboardDepth
        /// </summary>
        public Single BackboardDepth
        {
            get { return GetProperty<Single>(BackboardDepthProperty); }
            set { SetProperty<Single>(BackboardDepthProperty, value); }
        }

        #endregion

        #region FixtureHasBase

        /// <summary>
        /// FixtureHasBase property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> FixtureHasBaseProperty =
          RegisterModelProperty<Boolean>(c => c.FixtureHasBase);

        /// <summary>
        /// Gets/Sets the default FixtureHasBase
        /// </summary>
        public Boolean FixtureHasBase
        {
            get { return GetProperty<Boolean>(FixtureHasBaseProperty); }
            set { SetProperty<Boolean>(FixtureHasBaseProperty, value); }
        }

        #endregion

        #region BaseHeight

        /// <summary>
        /// BaseHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BaseHeightProperty =
          RegisterModelProperty<Single>(c => c.BaseHeight);

        /// <summary>
        /// Gets/Sets the default BaseHeight
        /// </summary>
        public Single BaseHeight
        {
            get { return GetProperty<Single>(BaseHeightProperty); }
            set { SetProperty<Single>(BaseHeightProperty, value); }
        }

        #endregion

        #region ShelfHeight

        /// <summary>
        /// ShelfHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ShelfHeightProperty =
          RegisterModelProperty<Single>(c => c.ShelfHeight);

        /// <summary>
        /// Gets/Sets the default ShelfHeight
        /// </summary>
        public Single ShelfHeight
        {
            get { return GetProperty<Single>(ShelfHeightProperty); }
            set { SetProperty<Single>(ShelfHeightProperty, value); }
        }

        #endregion

        #region ShelfWidth

        /// <summary>
        /// ShelfWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ShelfWidthProperty =
          RegisterModelProperty<Single>(c => c.ShelfWidth);

        /// <summary>
        /// Gets/Sets the default ShelfWidth
        /// </summary>
        public Single ShelfWidth
        {
            get { return GetProperty<Single>(ShelfWidthProperty); }
            set { SetProperty<Single>(ShelfWidthProperty, value); }
        }

        #endregion

        #region ShelfDepth

        /// <summary>
        /// ShelfDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ShelfDepthProperty =
          RegisterModelProperty<Single>(c => c.ShelfDepth);

        /// <summary>
        /// Gets/Sets the default ShelfDepth
        /// </summary>
        public Single ShelfDepth
        {
            get { return GetProperty<Single>(ShelfDepthProperty); }
            set { SetProperty<Single>(ShelfDepthProperty, value); }
        }

        #endregion

        #region ShelfFillColour

        /// <summary>
        /// ShelfFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ShelfFillColourProperty =
          RegisterModelProperty<Int32>(c => c.ShelfFillColour);

        /// <summary>
        /// Gets/Sets the default ShelfFillColour
        /// </summary>
        public Int32 ShelfFillColour
        {
            get { return GetProperty<Int32>(ShelfFillColourProperty); }
            set { SetProperty<Int32>(ShelfFillColourProperty, value); }
        }

        #endregion

        #region PegboardHeight

        /// <summary>
        /// PegboardHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegboardHeightProperty =
          RegisterModelProperty<Single>(c => c.PegboardHeight);

        /// <summary>
        /// Gets/Sets the default PegboardHeight
        /// </summary>
        public Single PegboardHeight
        {
            get { return GetProperty<Single>(PegboardHeightProperty); }
            set { SetProperty<Single>(PegboardHeightProperty, value); }
        }

        #endregion

        #region PegboardWidth

        /// <summary>
        /// PegboardWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegboardWidthProperty =
          RegisterModelProperty<Single>(c => c.PegboardWidth);

        /// <summary>
        /// Gets/Sets the default PegboardWidth
        /// </summary>
        public Single PegboardWidth
        {
            get { return GetProperty<Single>(PegboardWidthProperty); }
            set { SetProperty<Single>(PegboardWidthProperty, value); }
        }

        #endregion

        #region PegboardDepth

        /// <summary>
        /// PegboardDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PegboardDepthProperty =
          RegisterModelProperty<Single>(c => c.PegboardDepth);

        /// <summary>
        /// Gets/Sets the default PegboardDepth
        /// </summary>
        public Single PegboardDepth
        {
            get { return GetProperty<Single>(PegboardDepthProperty); }
            set { SetProperty<Single>(PegboardDepthProperty, value); }
        }

        #endregion

        #region PegboardFillColour

        /// <summary>
        /// PegboardFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PegboardFillColourProperty =
          RegisterModelProperty<Int32>(c => c.PegboardFillColour);

        /// <summary>
        /// Gets/Sets the default PegboardFillColour
        /// </summary>
        public Int32 PegboardFillColour
        {
            get { return GetProperty<Int32>(PegboardFillColourProperty); }
            set { SetProperty<Int32>(PegboardFillColourProperty, value); }
        }

        #endregion

        #region ChestHeight

        /// <summary>
        /// ChestHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ChestHeightProperty =
          RegisterModelProperty<Single>(c => c.ChestHeight);

        /// <summary>
        /// Gets/Sets the default ChestHeight
        /// </summary>
        public Single ChestHeight
        {
            get { return GetProperty<Single>(ChestHeightProperty); }
            set { SetProperty<Single>(ChestHeightProperty, value); }
        }

        #endregion

        #region ChestWidth

        /// <summary>
        /// ChestWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ChestWidthProperty =
          RegisterModelProperty<Single>(c => c.ChestWidth);

        /// <summary>
        /// Gets/Sets the default ChestWidth
        /// </summary>
        public Single ChestWidth
        {
            get { return GetProperty<Single>(ChestWidthProperty); }
            set { SetProperty<Single>(ChestWidthProperty, value); }
        }

        #endregion

        #region ChestDepth

        /// <summary>
        /// ChestDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ChestDepthProperty =
          RegisterModelProperty<Single>(c => c.ChestDepth);

        /// <summary>
        /// Gets/Sets the default ChestDepth
        /// </summary>
        public Single ChestDepth
        {
            get { return GetProperty<Single>(ChestDepthProperty); }
            set { SetProperty<Single>(ChestDepthProperty, value); }
        }

        #endregion

        #region ChestFillColour

        /// <summary>
        /// ChestFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ChestFillColourProperty =
          RegisterModelProperty<Int32>(c => c.ChestFillColour);

        /// <summary>
        /// Gets/Sets the default ChestFillColour
        /// </summary>
        public Int32 ChestFillColour
        {
            get { return GetProperty<Int32>(ChestFillColourProperty); }
            set { SetProperty<Int32>(ChestFillColourProperty, value); }
        }

        #endregion

        #region BarHeight

        /// <summary>
        /// BarHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BarHeightProperty =
          RegisterModelProperty<Single>(c => c.BarHeight);

        /// <summary>
        /// Gets/Sets the default BarHeight
        /// </summary>
        public Single BarHeight
        {
            get { return GetProperty<Single>(BarHeightProperty); }
            set { SetProperty<Single>(BarHeightProperty, value); }
        }

        #endregion

        #region BarWidth

        /// <summary>
        /// BarWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BarWidthProperty =
          RegisterModelProperty<Single>(c => c.BarWidth);

        /// <summary>
        /// Gets/Sets the default BarWidth
        /// </summary>
        public Single BarWidth
        {
            get { return GetProperty<Single>(BarWidthProperty); }
            set { SetProperty<Single>(BarWidthProperty, value); }
        }

        #endregion

        #region BarDepth

        /// <summary>
        /// BarDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BarDepthProperty =
          RegisterModelProperty<Single>(c => c.BarDepth);

        /// <summary>
        /// Gets/Sets the default BarDepth
        /// </summary>
        public Single BarDepth
        {
            get { return GetProperty<Single>(BarDepthProperty); }
            set { SetProperty<Single>(BarDepthProperty, value); }
        }

        #endregion

        #region BarFillColour

        /// <summary>
        /// BarFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> BarFillColourProperty =
          RegisterModelProperty<Int32>(c => c.BarFillColour);

        /// <summary>
        /// Gets/Sets the default BarFillColour
        /// </summary>
        public Int32 BarFillColour
        {
            get { return GetProperty<Int32>(BarFillColourProperty); }
            set { SetProperty<Int32>(BarFillColourProperty, value); }
        }

        #endregion

        #region RodHeight

        /// <summary>
        /// RodHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> RodHeightProperty =
          RegisterModelProperty<Single>(c => c.RodHeight);

        /// <summary>
        /// Gets/Sets the default RodHeight
        /// </summary>
        public Single RodHeight
        {
            get { return GetProperty<Single>(RodHeightProperty); }
            set { SetProperty<Single>(RodHeightProperty, value); }
        }

        #endregion

        #region RodWidth

        /// <summary>
        /// RodWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> RodWidthProperty =
          RegisterModelProperty<Single>(c => c.RodWidth);

        /// <summary>
        /// Gets/Sets the default RodWidth
        /// </summary>
        public Single RodWidth
        {
            get { return GetProperty<Single>(RodWidthProperty); }
            set { SetProperty<Single>(RodWidthProperty, value); }
        }

        #endregion

        #region RodDepth

        /// <summary>
        /// RodDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> RodDepthProperty =
          RegisterModelProperty<Single>(c => c.RodDepth);

        /// <summary>
        /// Gets/Sets the default RodDepth
        /// </summary>
        public Single RodDepth
        {
            get { return GetProperty<Single>(RodDepthProperty); }
            set { SetProperty<Single>(RodDepthProperty, value); }
        }

        #endregion

        #region RodFillColour

        /// <summary>
        /// RodFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> RodFillColourProperty =
          RegisterModelProperty<Int32>(c => c.RodFillColour);

        /// <summary>
        /// Gets/Sets the default RodFillColour
        /// </summary>
        public Int32 RodFillColour
        {
            get { return GetProperty<Int32>(RodFillColourProperty); }
            set { SetProperty<Int32>(RodFillColourProperty, value); }
        }

        #endregion

        #region ClipstripHeight

        /// <summary>
        /// ClipstripHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ClipStripHeightProperty =
          RegisterModelProperty<Single>(c => c.ClipStripHeight);

        /// <summary>
        /// Gets/Sets the default ClipstripHeight
        /// </summary>
        public Single ClipStripHeight
        {
            get { return GetProperty<Single>(ClipStripHeightProperty); }
            set { SetProperty<Single>(ClipStripHeightProperty, value); }
        }

        #endregion

        #region ClipstripWidth

        /// <summary>
        /// ClipstripWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ClipStripWidthProperty =
          RegisterModelProperty<Single>(c => c.ClipStripWidth);

        /// <summary>
        /// Gets/Sets the default ClipstripWidth
        /// </summary>
        public Single ClipStripWidth
        {
            get { return GetProperty<Single>(ClipStripWidthProperty); }
            set { SetProperty<Single>(ClipStripWidthProperty, value); }
        }

        #endregion

        #region ClipstripDepth

        /// <summary>
        /// ClipstripDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ClipStripDepthProperty =
          RegisterModelProperty<Single>(c => c.ClipStripDepth);

        /// <summary>
        /// Gets/Sets the default ClipstripDepth
        /// </summary>
        public Single ClipStripDepth
        {
            get { return GetProperty<Single>(ClipStripDepthProperty); }
            set { SetProperty<Single>(ClipStripDepthProperty, value); }
        }

        #endregion

        #region ClipstripFillColour

        /// <summary>
        /// ClipstripFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ClipStripFillColourProperty =
          RegisterModelProperty<Int32>(c => c.ClipStripFillColour);

        /// <summary>
        /// Gets/Sets the default ClipstripFillColour
        /// </summary>
        public Int32 ClipStripFillColour
        {
            get { return GetProperty<Int32>(ClipStripFillColourProperty); }
            set { SetProperty<Int32>(ClipStripFillColourProperty, value); }
        }

        #endregion

        #region PalletHeight

        /// <summary>
        /// PalletHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PalletHeightProperty =
          RegisterModelProperty<Single>(c => c.PalletHeight);

        /// <summary>
        /// Gets/Sets the default PalletHeight
        /// </summary>
        public Single PalletHeight
        {
            get { return GetProperty<Single>(PalletHeightProperty); }
            set { SetProperty<Single>(PalletHeightProperty, value); }
        }

        #endregion

        #region PalletWidth

        /// <summary>
        /// PalletWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PalletWidthProperty =
          RegisterModelProperty<Single>(c => c.PalletWidth);

        /// <summary>
        /// Gets/Sets the default PalletWidth
        /// </summary>
        public Single PalletWidth
        {
            get { return GetProperty<Single>(PalletWidthProperty); }
            set { SetProperty<Single>(PalletWidthProperty, value); }
        }

        #endregion

        #region PalletDepth

        /// <summary>
        /// PalletDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> PalletDepthProperty =
          RegisterModelProperty<Single>(c => c.PalletDepth);

        /// <summary>
        /// Gets/Sets the default PalletDepth
        /// </summary>
        public Single PalletDepth
        {
            get { return GetProperty<Single>(PalletDepthProperty); }
            set { SetProperty<Single>(PalletDepthProperty, value); }
        }

        #endregion

        #region PalletFillColour

        /// <summary>
        /// PalletFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PalletFillColourProperty =
          RegisterModelProperty<Int32>(c => c.PalletFillColour);

        /// <summary>
        /// Gets/Sets the default PalletFillColour
        /// </summary>
        public Int32 PalletFillColour
        {
            get { return GetProperty<Int32>(PalletFillColourProperty); }
            set { SetProperty<Int32>(PalletFillColourProperty, value); }
        }

        #endregion

        #region SlotwallHeight
        /// <summary>
        /// SlotwallHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SlotwallHeightProperty =
            RegisterModelProperty<Single>(c => c.SlotwallHeight);
        /// <summary>
        /// Gets or sets the default Slotwall height
        /// </summary>
        public Single SlotwallHeight
        {
            get { return this.GetProperty<Single>(SlotwallHeightProperty); }
            set { this.SetProperty<Single>(SlotwallHeightProperty, value); }
        }
        #endregion

        #region SlotwallWidth
        /// <summary>
        /// SlotwallWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SlotwallWidthProperty =
            RegisterModelProperty<Single>(c => c.SlotwallWidth);
        /// <summary>
        /// Gets or sets the default Slotwall width
        /// </summary>
        public Single SlotwallWidth
        {
            get { return this.GetProperty<Single>(SlotwallWidthProperty); }
            set { this.SetProperty<Single>(SlotwallWidthProperty, value); }
        }
        #endregion

        #region SlotwallDepth
        /// <summary>
        /// SlotwallDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SlotwallDepthProperty =
            RegisterModelProperty<Single>(c => c.SlotwallDepth);
        /// <summary>
        /// Gets or sets the default Slotwall depth
        /// </summary>
        public Single SlotwallDepth
        {
            get { return this.GetProperty<Single>(SlotwallDepthProperty); }
            set { this.SetProperty<Single>(SlotwallDepthProperty, value); }
        }
        #endregion

        #region SlotwallFillColour
        /// <summary>
        /// SlotwallFillColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> SlotwallFillColourProperty =
            RegisterModelProperty<Int32>(c => c.SlotwallFillColour);
        /// <summary>
        /// Gets or sets the default Slotwall fill colour
        /// </summary>
        public Int32 SlotwallFillColour
        {
            get { return this.GetProperty<Int32>(SlotwallFillColourProperty); }
            set { this.SetProperty<Int32>(SlotwallFillColourProperty, value); }
        }
        #endregion
        #endregion

        #region IsLibraryPanelVisible

        /// <summary>
        /// IsLibraryPanelVisible property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsLibraryPanelVisibleProperty =
            RegisterModelProperty<Boolean>(c => c.IsLibraryPanelVisible);

        /// <summary>
        /// Gets/Sets whether the library panel should be shown on startup.
        /// </summary>
        public Boolean IsLibraryPanelVisible
        {
            get { return GetProperty<Boolean>(IsLibraryPanelVisibleProperty); }
            set { SetProperty<Boolean>(IsLibraryPanelVisibleProperty, value); }
        }

        #endregion

        #region StatusBarHeight

        /// <summary>
        /// StatusBarHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> StatusBarHeightProperty =
            RegisterModelProperty<Int32>(c => c.StatusBarHeight);

        /// <summary>
        /// Gets/Sets the height of the status bar.
        /// </summary>
        public Int32 StatusBarHeight
        {
            get { return GetProperty<Int32>(StatusBarHeightProperty); }
            set { SetProperty<Int32>(StatusBarHeightProperty, value); }
        }

        #endregion

        #region PositionProperties

        /// <summary>
        /// PositionProperties property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> PositionPropertiesProperty =
            RegisterModelProperty<String>(c => c.PositionProperties);

        /// <summary>
        /// Gets/Sets the fields to by displayed in the position properties panel.
        /// </summary>
        public String PositionProperties
        {
            get { return GetProperty<String>(PositionPropertiesProperty); }
            set { SetProperty<String>(PositionPropertiesProperty, value); }
        }

        #endregion

        #region ComponentProperties

        /// <summary>
        /// ComponentProperties property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ComponentPropertiesProperty =
            RegisterModelProperty<String>(c => c.ComponentProperties);

        public String ComponentProperties
        {
            get { return GetProperty<String>(ComponentPropertiesProperty); }
            set { SetProperty<String>(ComponentPropertiesProperty, value); }
        }
        #endregion

        #region IsShowSelectionAsBlinkingEnabled

        /// <summary>
        ///		Metadata for the <see cref="IsShowSelectionAsBlinkingEnabled"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> IsShowSelectionAsBlinkingEnabledProperty =
            RegisterModelProperty<Boolean>(o => o.IsShowSelectionAsBlinkingEnabled);

        /// <summary>
        ///		Gets or sets whether selected items highlighting should 'blink'.
        /// </summary>
        public Boolean IsShowSelectionAsBlinkingEnabled
        {
            get { return this.GetProperty<Boolean>(IsShowSelectionAsBlinkingEnabledProperty); }
            set { this.SetProperty<Boolean>(IsShowSelectionAsBlinkingEnabledProperty, value); }
        }

        #endregion

        #region BlinkSpeed
        /// <summary>
        ///		Metadata for the <see cref="BlinkSpeed"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> BlinkSpeedProperty =
            RegisterModelProperty<Int32>(o => o.BlinkSpeed);

        /// <summary>
        ///		Gets or sets whether selected items highlighting should 'blink'.
        /// </summary>
        public Int32 BlinkSpeed
        {
            get { return this.GetProperty<Int32>(BlinkSpeedProperty); }
            set { this.SetProperty<Int32>(BlinkSpeedProperty, value); }
        }

        #endregion

        #region ProductImageSource

        /// <summary>
        ///		Metadata for the <see cref="ProductImageSource"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<RealImageProviderType> ProductImageSourceProperty =
            RegisterModelProperty<RealImageProviderType>(o => o.ProductImageSource);

        /// <summary>
        ///		Gets or sets the currently set image source type.
        /// </summary>
        public RealImageProviderType ProductImageSource
        {
            get { return this.GetProperty<RealImageProviderType>(ProductImageSourceProperty); }
            set { this.SetProperty<RealImageProviderType>(ProductImageSourceProperty, value); }
        }

        #endregion

        #region ProductImageLocation

        /// <summary>
        ///		Metadata for the <see cref="ProductImageLocation"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> ProductImageLocationProperty =
            RegisterModelProperty<String>(o => o.ProductImageLocation);

        /// <summary>
        ///		Gets or sets the value for the Product Images folder.
        /// </summary>
        public String ProductImageLocation
        {
            get { return this.GetProperty<String>(ProductImageLocationProperty); }
            set { this.SetProperty<String>(ProductImageLocationProperty, value); }
        }

        #endregion

        #region ProductImageAttribute

        /// <summary>
        ///		Metadata for the <see cref="ProductImageAttribute"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> ProductImageAttributeProperty =
            RegisterModelProperty<String>(o => o.ProductImageAttribute);

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageAttribute"/> property.
        /// </summary>
        public String ProductImageAttribute
        {
            get { return this.GetProperty<String>(ProductImageAttributeProperty); }
            set { this.SetProperty<String>(ProductImageAttributeProperty, value); }
        }

        #endregion

        #region ProductImageIgnoreLeadingZeros

        /// <summary>
        ///		Metadata for the <see cref="ProductImageIgnoreLeadingZeros"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ProductImageIgnoreLeadingZerosProperty =
            RegisterModelProperty<Boolean>(o => o.ProductImageIgnoreLeadingZeros);

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageIgnoreLeadingZeros"/> property.
        /// </summary>
        public Boolean ProductImageIgnoreLeadingZeros
        {
            get { return this.GetProperty<Boolean>(ProductImageIgnoreLeadingZerosProperty); }
            set { this.SetProperty<Boolean>(ProductImageIgnoreLeadingZerosProperty, value); }
        }

        #endregion

        #region ProductImageMinFieldLength

        /// <summary>
        ///		Metadata for the <see cref="ProductImageMinFieldLength"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Byte> ProductImageMinFieldLengthProperty =
            RegisterModelProperty<Byte>(o => o.ProductImageMinFieldLength);

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageMinFieldLength"/> property.
        /// </summary>
        public Byte ProductImageMinFieldLength
        {
            get { return this.GetProperty<Byte>(ProductImageMinFieldLengthProperty); }
            set { this.SetProperty<Byte>(ProductImageMinFieldLengthProperty, value); }
        }

        #endregion

        #region ProductImageFacingPostfixFront

        /// <summary>
        ///		Metadata for the <see cref="ProductImageFacingPostfixFront"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> ProductImageFacingPostfixFrontProperty =
            RegisterModelProperty<String>(o => o.ProductImageFacingPostfixFront);

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageFacingPostfixFront"/> property.
        /// </summary>
        public String ProductImageFacingPostfixFront
        {
            get { return this.GetProperty<String>(ProductImageFacingPostfixFrontProperty); }
            set { this.SetProperty<String>(ProductImageFacingPostfixFrontProperty, value); }
        }

        #endregion

        #region ProductImageFacingPostfixLeft

        /// <summary>
        ///		Metadata for the <see cref="ProductImageFacingPostfixLeft"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> ProductImageFacingPostfixLeftProperty =
            RegisterModelProperty<String>(o => o.ProductImageFacingPostfixLeft);

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageFacingPostfixLeft"/> property.
        /// </summary>
        public String ProductImageFacingPostfixLeft
        {
            get { return this.GetProperty<String>(ProductImageFacingPostfixLeftProperty); }
            set { this.SetProperty<String>(ProductImageFacingPostfixLeftProperty, value); }
        }

        #endregion

        #region ProductImageFacingPostfixTop

        /// <summary>
        ///		Metadata for the <see cref="ProductImageFacingPostfixTop"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> ProductImageFacingPostfixTopProperty =
            RegisterModelProperty<String>(o => o.ProductImageFacingPostfixTop);

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageFacingPostfixTop"/> property.
        /// </summary>
        public String ProductImageFacingPostfixTop
        {
            get { return this.GetProperty<String>(ProductImageFacingPostfixTopProperty); }
            set { this.SetProperty<String>(ProductImageFacingPostfixTopProperty, value); }
        }

        #endregion

        #region ProductImageFacingPostfixBack

        /// <summary>
        ///		Metadata for the <see cref="ProductImageFacingPostfixBack"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> ProductImageFacingPostfixBackProperty =
            RegisterModelProperty<String>(o => o.ProductImageFacingPostfixBack);

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageFacingPostfixBack"/> property.
        /// </summary>
        public String ProductImageFacingPostfixBack
        {
            get { return this.GetProperty<String>(ProductImageFacingPostfixBackProperty); }
            set { this.SetProperty<String>(ProductImageFacingPostfixBackProperty, value); }
        }

        #endregion

        #region ProductImageFacingPostfixRight

        /// <summary>
        ///		Metadata for the <see cref="ProductImageFacingPostfixRight"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> ProductImageFacingPostfixRightProperty =
            RegisterModelProperty<String>(o => o.ProductImageFacingPostfixRight);

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageFacingPostfixRight"/> property.
        /// </summary>
        public String ProductImageFacingPostfixRight
        {
            get { return this.GetProperty<String>(ProductImageFacingPostfixRightProperty); }
            set { this.SetProperty<String>(ProductImageFacingPostfixRightProperty, value); }
        }

        #endregion

        #region ProductImageFacingPostfixBottom

        /// <summary>
        ///		Metadata for the <see cref="ProductImageFacingPostfixBottom"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> ProductImageFacingPostfixBottomProperty =
            RegisterModelProperty<String>(o => o.ProductImageFacingPostfixBottom);

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageFacingPostfixBottom"/> property.
        /// </summary>
        public String ProductImageFacingPostfixBottom
        {
            get { return this.GetProperty<String>(ProductImageFacingPostfixBottomProperty); }
            set { this.SetProperty<String>(ProductImageFacingPostfixBottomProperty, value); }
        }

        #endregion

        #region ProductImageCompression

        /// <summary>
        ///		Metadata for the <see cref="ProductImageCompression"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<RealImageCompressionType> ProductImageCompressionProperty =
            RegisterModelProperty<RealImageCompressionType>(o => o.ProductImageCompression);

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageCompression"/> property.
        /// </summary>
        public RealImageCompressionType ProductImageCompression
        {
            get { return this.GetProperty<RealImageCompressionType>(ProductImageCompressionProperty); }
            set { this.SetProperty<RealImageCompressionType>(ProductImageCompressionProperty, value); }
        }

        #endregion

        #region ProductImageColourDepth

        /// <summary>
        ///		Metadata for the <see cref="ProductImageColourDepth"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<RealImageColourDepthType> ProductImageColourDepthProperty =
            RegisterModelProperty<RealImageColourDepthType>(o => o.ProductImageColourDepth);

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageColourDepth"/> property.
        /// </summary>
        public RealImageColourDepthType ProductImageColourDepth
        {
            get { return this.GetProperty<RealImageColourDepthType>(ProductImageColourDepthProperty); }
            set { this.SetProperty<RealImageColourDepthType>(ProductImageColourDepthProperty, value); }
        }

        #endregion

        #region ProductImageTransparency

        /// <summary>
        ///		Metadata for the <see cref="ProductImageTransparency"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ProductImageTransparencyProperty =
            RegisterModelProperty<Boolean>(o => o.ProductImageTransparency);

        /// <summary>
        ///		Gets or sets the value for the <see cref="ProductImageTransparency"/> property.
        /// </summary>
        public Boolean ProductImageTransparency
        {
            get { return this.GetProperty<Boolean>(ProductImageTransparencyProperty); }
            set { this.SetProperty<Boolean>(ProductImageTransparencyProperty, value); }
        }

        #endregion

        #region Validation Tab

        #region ShowComponentHasCollisionsWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowComponentHasCollisionsWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowComponentHasCollisionsWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowComponentHasCollisionsWarnings);

        /// <summary>
        ///		Gets or sets whether ComponentHasCollisions validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowComponentHasCollisionsWarnings
        {
            get { return this.GetProperty<Boolean>(ShowComponentHasCollisionsWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowComponentHasCollisionsWarningsProperty, value); }
        }

        #endregion

        #region ShowComponentIsOverfilledWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowComponentIsOverfilledWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowComponentIsOverfilledWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowComponentIsOverfilledWarnings);

        /// <summary>
        ///		Gets or sets whether ComponentIsOverfilled validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowComponentIsOverfilledWarnings
        {
            get { return this.GetProperty<Boolean>(ShowComponentIsOverfilledWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowComponentIsOverfilledWarningsProperty, value); }
        }

        #endregion

        #region ShowComponentIsOutsideOfFixtureAreaWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowComponentIsOutsideOfFixtureAreaWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowComponentIsOutsideOfFixtureAreaWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowComponentIsOutsideOfFixtureAreaWarnings);

        /// <summary>
        ///		Gets or sets whether ShowComponentIsOutsideOfFixtureAreaWarnings validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowComponentIsOutsideOfFixtureAreaWarnings
        {
            get { return this.GetProperty<Boolean>(ShowComponentIsOutsideOfFixtureAreaWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowComponentIsOutsideOfFixtureAreaWarningsProperty, value); }
        }

        #endregion

        #region ShowComponentSlopeWithNoRiserWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowComponentSlopeWithNoRiserWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowComponentSlopeWithNoRiserWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowComponentSlopeWithNoRiserWarnings);

        /// <summary>
        ///		Gets or sets whether ComponentSlopeWithNoRiser validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowComponentSlopeWithNoRiserWarnings
        {
            get { return this.GetProperty<Boolean>(ShowComponentSlopeWithNoRiserWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowComponentSlopeWithNoRiserWarningsProperty, value); }
        }

        #endregion

        #region ShowPegIsOverfilledWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowPegIsOverfilledWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowPegIsOverfilledWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowPegIsOverfilledWarnings);

        /// <summary>
        ///		Gets or sets whether PegIsOverfilled validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowPegIsOverfilledWarnings
        {
            get { return this.GetProperty<Boolean>(ShowPegIsOverfilledWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowPegIsOverfilledWarningsProperty, value); }
        }

        #endregion

        #region ShowPositionCannotBreakTrayBackWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowPositionCannotBreakTrayBackWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowPositionCannotBreakTrayBackWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowPositionCannotBreakTrayBackWarnings);

        /// <summary>
        ///		Gets or sets whether PositionCannotBreakTrayBack validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowPositionCannotBreakTrayBackWarnings
        {
            get { return this.GetProperty<Boolean>(ShowPositionCannotBreakTrayBackWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowPositionCannotBreakTrayBackWarningsProperty, value); }
        }

        #endregion

        #region ShowPositionCannotBreakTrayDownWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowPositionCannotBreakTrayDownWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowPositionCannotBreakTrayDownWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowPositionCannotBreakTrayDownWarnings);

        /// <summary>
        ///		Gets or sets whether PositionCannotBreakTrayDown validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowPositionCannotBreakTrayDownWarnings
        {
            get { return this.GetProperty<Boolean>(ShowPositionCannotBreakTrayDownWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowPositionCannotBreakTrayDownWarningsProperty, value); }
        }

        #endregion

        #region ShowPositionCannotBreakTrayTopWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowPositionCannotBreakTrayTopWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowPositionCannotBreakTrayTopWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowPositionCannotBreakTrayTopWarnings);

        /// <summary>
        ///		Gets or sets whether PositionCannotBreakTrayTop validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowPositionCannotBreakTrayTopWarnings
        {
            get { return this.GetProperty<Boolean>(ShowPositionCannotBreakTrayTopWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowPositionCannotBreakTrayTopWarningsProperty, value); }
        }

        #endregion

        #region ShowPositionCannotBreakTrayUpWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowPositionCannotBreakTrayUpWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowPositionCannotBreakTrayUpWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowPositionCannotBreakTrayUpWarnings);

        /// <summary>
        ///		Gets or sets whether PositionCannotBreakTrayUp validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowPositionCannotBreakTrayUpWarnings
        {
            get { return this.GetProperty<Boolean>(ShowPositionCannotBreakTrayUpWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowPositionCannotBreakTrayUpWarningsProperty, value); }
        }

        #endregion

        #region ShowPositionDoesNotAchieveMinDeepWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowPositionDoesNotAchieveMinDeepWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowPositionDoesNotAchieveMinDeepWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowPositionDoesNotAchieveMinDeepWarnings);

        /// <summary>
        ///		Gets or sets whether PositionDoesNotAchieveMinDeep validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowPositionDoesNotAchieveMinDeepWarnings
        {
            get { return this.GetProperty<Boolean>(ShowPositionDoesNotAchieveMinDeepWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowPositionDoesNotAchieveMinDeepWarningsProperty, value); }
        }

        #endregion

        #region ShowPositionExceedsMaxDeepWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowPositionExceedsMaxDeepWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowPositionExceedsMaxDeepWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowPositionExceedsMaxDeepWarnings);

        /// <summary>
        ///		Gets or sets whether PositionExceedsMaxDeep validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowPositionExceedsMaxDeepWarnings
        {
            get { return this.GetProperty<Boolean>(ShowPositionExceedsMaxDeepWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowPositionExceedsMaxDeepWarningsProperty, value); }
        }

        #endregion

        #region ShowPositionExceedsMaxRightCapWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowPositionExceedsMaxRightCapWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowPositionExceedsMaxRightCapWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowPositionExceedsMaxRightCapWarnings);

        /// <summary>
        ///		Gets or sets whether PositionExceedsMaxRightCap validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowPositionExceedsMaxRightCapWarnings
        {
            get { return this.GetProperty<Boolean>(ShowPositionExceedsMaxRightCapWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowPositionExceedsMaxRightCapWarningsProperty, value); }
        }

        #endregion

        #region ShowPositionExceedsMaxStackWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowPositionExceedsMaxStackWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowPositionExceedsMaxStackWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowPositionExceedsMaxStackWarnings);

        /// <summary>
        ///		Gets or sets whether PositionExceedsMaxStack validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowPositionExceedsMaxStackWarnings
        {
            get { return this.GetProperty<Boolean>(ShowPositionExceedsMaxStackWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowPositionExceedsMaxStackWarningsProperty, value); }
        }

        #endregion

        #region ShowPositionExceedsMaxTopCapWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowPositionExceedsMaxTopCapWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowPositionExceedsMaxTopCapWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowPositionExceedsMaxTopCapWarnings);

        /// <summary>
        ///		Gets or sets whether PositionExceedsMaxTopCap validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowPositionExceedsMaxTopCapWarnings
        {
            get { return this.GetProperty<Boolean>(ShowPositionExceedsMaxTopCapWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowPositionExceedsMaxTopCapWarningsProperty, value); }
        }

        #endregion

        #region ShowPositionHasCollisionsWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowPositionHasCollisionsWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowPositionHasCollisionsWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowPositionHasCollisionsWarnings);

        /// <summary>
        ///		Gets or sets whether PositionHasCollisions validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowPositionHasCollisionsWarnings
        {
            get { return this.GetProperty<Boolean>(ShowPositionHasCollisionsWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowPositionHasCollisionsWarningsProperty, value); }
        }

        #endregion

        #region ShowPositionPegHolesAreInvalid
        /// <summary>
        ///		Metadata for the <see cref="ShowPositionPegHolesAreInvalidWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowPositionPegHolesAreInvalidWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowPositionPegHolesAreInvalidWarnings);

        /// <summary>
        ///		Gets or sets whether Position peg holes validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowPositionPegHolesAreInvalidWarnings
        {
            get { return this.GetProperty<Boolean>(ShowPositionPegHolesAreInvalidWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowPositionPegHolesAreInvalidWarningsProperty, value); }
        }

        #endregion

        #region ShowPositionHasInvalidMerchStyleSettingsWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowPositionHasInvalidMerchStyleSettingsWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowPositionHasInvalidMerchStyleSettingsWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowPositionHasInvalidMerchStyleSettingsWarnings);

        /// <summary>
        ///		Gets or sets whether PositionHasInvalidMerchStyleSettings validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowPositionHasInvalidMerchStyleSettingsWarnings
        {
            get { return this.GetProperty<Boolean>(ShowPositionHasInvalidMerchStyleSettingsWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowPositionHasInvalidMerchStyleSettingsWarningsProperty, value); }
        }

        #endregion

        #region ShowPositionIsHangingTrayWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowPositionIsHangingTrayWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowPositionIsHangingTrayWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowPositionIsHangingTrayWarnings);

        /// <summary>
        ///		Gets or sets whether PositionIsHangingTray validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowPositionIsHangingTrayWarnings
        {
            get { return this.GetProperty<Boolean>(ShowPositionIsHangingTrayWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowPositionIsHangingTrayWarningsProperty, value); }
        }

        #endregion

        #region ShowPositionIsHangingWithoutPegWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowPositionIsHangingWithoutPegWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowPositionIsHangingWithoutPegWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowPositionIsHangingWithoutPegWarnings);

        /// <summary>
        ///		Gets or sets whether PositionIsHangingWithoutPeg validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowPositionIsHangingWithoutPegWarnings
        {
            get { return this.GetProperty<Boolean>(ShowPositionIsHangingWithoutPegWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowPositionIsHangingWithoutPegWarningsProperty, value); }
        }

        #endregion

        #region ShowPositionIsOutsideMerchandisingSpaceWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowPositionIsOutsideMerchandisingSpaceWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowPositionIsOutsideMerchandisingSpaceWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowPositionIsOutsideMerchandisingSpaceWarnings);

        /// <summary>
        ///		Gets or sets whether PositionIsOutsideMerchandisingSpace validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowPositionIsOutsideMerchandisingSpaceWarnings
        {
            get { return this.GetProperty<Boolean>(ShowPositionIsOutsideMerchandisingSpaceWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowPositionIsOutsideMerchandisingSpaceWarningsProperty, value); }
        }

        #endregion

        #region ShowProductDoesNotAchieveMinCasesWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowProductDoesNotAchieveMinCasesWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowProductDoesNotAchieveMinCasesWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowProductDoesNotAchieveMinCasesWarnings);

        /// <summary>
        ///		Gets or sets whether ProductDoesNotAchieveMinCases validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowProductDoesNotAchieveMinCasesWarnings
        {
            get { return this.GetProperty<Boolean>(ShowProductDoesNotAchieveMinCasesWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowProductDoesNotAchieveMinCasesWarningsProperty, value); }
        }

        #endregion

        #region ShowProductDoesNotAchieveMinDeliveriesWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowProductDoesNotAchieveMinDeliveriesWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowProductDoesNotAchieveMinDeliveriesWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowProductDoesNotAchieveMinDeliveriesWarnings);

        /// <summary>
        ///		Gets or sets whether ProductDoesNotAchieveMinDeliveries validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowProductDoesNotAchieveMinDeliveriesWarnings
        {
            get { return this.GetProperty<Boolean>(ShowProductDoesNotAchieveMinDeliveriesWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowProductDoesNotAchieveMinDeliveriesWarningsProperty, value); }
        }

        #endregion

        #region ShowProductDoesNotAchieveMinDosWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowProductDoesNotAchieveMinDosWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowProductDoesNotAchieveMinDosWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowProductDoesNotAchieveMinDosWarnings);

        /// <summary>
        ///		Gets or sets whether ProductDoesNotAchieveMinDos validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowProductDoesNotAchieveMinDosWarnings
        {
            get { return this.GetProperty<Boolean>(ShowProductDoesNotAchieveMinDosWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowProductDoesNotAchieveMinDosWarningsProperty, value); }
        }

        #endregion

        #region ShowProductDoesNotAchieveMinShelfLifeWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowProductDoesNotAchieveMinShelfLifeWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowProductDoesNotAchieveMinShelfLifeWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowProductDoesNotAchieveMinShelfLifeWarnings);

        /// <summary>
        ///		Gets or sets whether ProductDoesNotAchieveMinShelfLife validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowProductDoesNotAchieveMinShelfLifeWarnings
        {
            get { return this.GetProperty<Boolean>(ShowProductDoesNotAchieveMinShelfLifeWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowProductDoesNotAchieveMinShelfLifeWarningsProperty, value); }
        }

        #endregion

        #region ShowProductHasDuplicateGtinWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowProductHasDuplicateGtinWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowProductHasDuplicateGtinWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowProductHasDuplicateGtinWarnings);

        /// <summary>
        ///		Gets or sets whether ProductHasDuplicateGtin validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowProductHasDuplicateGtinWarnings
        {
            get { return this.GetProperty<Boolean>(ShowProductHasDuplicateGtinWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowProductHasDuplicateGtinWarningsProperty, value); }
        }

        #endregion

        #region ShowAssortmentLocalLineRulesWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowAssortmentLocalLineRulesWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowAssortmentLocalLineRulesWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowAssortmentLocalLineRulesWarnings);

        /// <summary>
        ///		Gets or sets whether ShowAssortmentLocalLineRules validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowAssortmentLocalLineRulesWarnings
        {
            get { return this.GetProperty<Boolean>(ShowAssortmentLocalLineRulesWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowAssortmentLocalLineRulesWarningsProperty, value); }
        }

        #endregion

        #region ShowAssortmentDistributionRulesWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowAssortmentDistributionRulesWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowAssortmentDistributionRulesWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowAssortmentDistributionRulesWarnings);

        /// <summary>
        ///		Gets or sets whether ShowAssortmentLocalLineRules validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowAssortmentDistributionRulesWarnings
        {
            get { return this.GetProperty<Boolean>(ShowAssortmentDistributionRulesWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowAssortmentDistributionRulesWarningsProperty, value); }
        }

        #endregion

        #region ShowAssortmentProductRulesWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowAssortmentProductRulesWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowAssortmentProductRulesWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowAssortmentProductRulesWarnings);

        /// <summary>
        ///		Gets or sets whether ShowAssortmentLocalLineRules validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowAssortmentProductRulesWarnings
        {
            get { return this.GetProperty<Boolean>(ShowAssortmentProductRulesWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowAssortmentProductRulesWarningsProperty, value); }
        }

        #endregion

        #region ShowAssortmentFamilyRulesWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowAssortmentFamilyRulesWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowAssortmentFamilyRulesWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowAssortmentFamilyRulesWarnings);

        /// <summary>
        ///		Gets or sets whether ShowAssortmentLocalLineRules validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowAssortmentFamilyRulesWarnings
        {
            get { return this.GetProperty<Boolean>(ShowAssortmentFamilyRulesWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowAssortmentFamilyRulesWarningsProperty, value); }
        }

        #endregion

        #region ShowAssortmentInheritanceRulesWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowAssortmentInheritanceRulesWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowAssortmentInheritanceRulesWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowAssortmentInheritanceRulesWarnings);

        /// <summary>
        ///		Gets or sets whether ShowAssortmentLocalLineRules validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowAssortmentInheritanceRulesWarnings
        {
            get { return this.GetProperty<Boolean>(ShowAssortmentInheritanceRulesWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowAssortmentInheritanceRulesWarningsProperty, value); }
        }
        #endregion

        #region ShowAssortmentCoreRulesWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowAssortmentCoreRulesWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowAssortmentCoreRulesWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowAssortmentCoreRulesWarnings);

        /// <summary>
        ///		Gets or sets whether ShowAssortmentLocalLineRules validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowAssortmentCoreRulesWarnings
        {
            get { return this.GetProperty<Boolean>(ShowAssortmentCoreRulesWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowAssortmentCoreRulesWarningsProperty, value); }
        }
        #endregion

        #region ShowLocationProductIllegalWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowLocationProductIllegalWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowLocationProductIllegalWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowLocationProductIllegalWarnings);

        /// <summary>
        ///		Gets or sets whether ShowAssortmentLocalLineRules validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowLocationProductIllegalWarnings
        {
            get { return this.GetProperty<Boolean>(ShowLocationProductIllegalWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowLocationProductIllegalWarningsProperty, value); }
        }
        #endregion

        #region ShowLocationProductLegalWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowLocationProductLegalWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowLocationProductLegalWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowLocationProductLegalWarnings);

        /// <summary>
        ///		Gets or sets whether ShowAssortmentLocalLineRules validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowLocationProductLegalWarnings
        {
            get { return this.GetProperty<Boolean>(ShowLocationProductLegalWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowLocationProductLegalWarningsProperty, value); }
        }
        #endregion

        #endregion

        #region Instant Validation

        #region ShowInstantComponentHasCollisionsWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantComponentHasCollisionsWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantComponentHasCollisionsWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantComponentHasCollisionsWarnings);

        /// <summary>
        ///		Gets or sets whether ComponentHasCollisions validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantComponentHasCollisionsWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantComponentHasCollisionsWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantComponentHasCollisionsWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantComponentIsOverfilledWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantComponentIsOverfilledWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantComponentIsOverfilledWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantComponentIsOverfilledWarnings);

        /// <summary>
        ///		Gets or sets whether ComponentIsOverfilled validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantComponentIsOverfilledWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantComponentIsOverfilledWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantComponentIsOverfilledWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantComponentSlopeWithNoRiserWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantComponentSlopeWithNoRiserWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantComponentSlopeWithNoRiserWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantComponentSlopeWithNoRiserWarnings);

        /// <summary>
        ///		Gets or sets whether ComponentSlopeWithNoRiser validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantComponentSlopeWithNoRiserWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantComponentSlopeWithNoRiserWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantComponentSlopeWithNoRiserWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantComponentIsOutsideOfFixtureAreaWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantComponentIsOutsideOfFixtureAreaWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantComponentIsOutsideOfFixtureAreaWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantComponentIsOutsideOfFixtureAreaWarnings);

        /// <summary>
        ///		Gets or sets whether ShowInstantComponentIsOutsideOfFixtureAreaWarnings validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantComponentIsOutsideOfFixtureAreaWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantComponentIsOutsideOfFixtureAreaWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantComponentIsOutsideOfFixtureAreaWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantPegIsOverfilledWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantPegIsOverfilledWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantPegIsOverfilledWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantPegIsOverfilledWarnings);

        /// <summary>
        ///		Gets or sets whether PegIsOverfilled validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantPegIsOverfilledWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantPegIsOverfilledWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantPegIsOverfilledWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantPositionCannotBreakTrayBackWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantPositionCannotBreakTrayBackWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantPositionCannotBreakTrayBackWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantPositionCannotBreakTrayBackWarnings);

        /// <summary>
        ///		Gets or sets whether PositionCannotBreakTrayBack validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantPositionCannotBreakTrayBackWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantPositionCannotBreakTrayBackWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantPositionCannotBreakTrayBackWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantPositionPegHolesAreInvalid
        /// <summary>
        ///		Metadata for the <see cref="ShowInstantPositionPegHolesAreInvalidWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantPositionPegHolesAreInvalidWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantPositionPegHolesAreInvalidWarnings);

        /// <summary>
        ///		Gets or sets whether PositionPegHolesAreInvalid validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantPositionPegHolesAreInvalidWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantPositionPegHolesAreInvalidWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantPositionPegHolesAreInvalidWarningsProperty, value); }
        }
        #endregion 

        #region ShowInstantPositionCannotBreakTrayDownWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantPositionCannotBreakTrayDownWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantPositionCannotBreakTrayDownWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantPositionCannotBreakTrayDownWarnings);

        /// <summary>
        ///		Gets or sets whether PositionCannotBreakTrayDown validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantPositionCannotBreakTrayDownWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantPositionCannotBreakTrayDownWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantPositionCannotBreakTrayDownWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantPositionCannotBreakTrayTopWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantPositionCannotBreakTrayTopWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantPositionCannotBreakTrayTopWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantPositionCannotBreakTrayTopWarnings);

        /// <summary>
        ///		Gets or sets whether PositionCannotBreakTrayTop validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantPositionCannotBreakTrayTopWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantPositionCannotBreakTrayTopWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantPositionCannotBreakTrayTopWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantPositionCannotBreakTrayUpWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantPositionCannotBreakTrayUpWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantPositionCannotBreakTrayUpWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantPositionCannotBreakTrayUpWarnings);

        /// <summary>
        ///		Gets or sets whether PositionCannotBreakTrayUp validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantPositionCannotBreakTrayUpWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantPositionCannotBreakTrayUpWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantPositionCannotBreakTrayUpWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantPositionDoesNotAchieveMinDeepWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantPositionDoesNotAchieveMinDeepWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantPositionDoesNotAchieveMinDeepWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantPositionDoesNotAchieveMinDeepWarnings);

        /// <summary>
        ///		Gets or sets whether PositionDoesNotAchieveMinDeep validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantPositionDoesNotAchieveMinDeepWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantPositionDoesNotAchieveMinDeepWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantPositionDoesNotAchieveMinDeepWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantPositionExceedsMaxDeepWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantPositionExceedsMaxDeepWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantPositionExceedsMaxDeepWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantPositionExceedsMaxDeepWarnings);

        /// <summary>
        ///		Gets or sets whether PositionExceedsMaxDeep validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantPositionExceedsMaxDeepWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantPositionExceedsMaxDeepWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantPositionExceedsMaxDeepWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantPositionExceedsMaxRightCapWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantPositionExceedsMaxRightCapWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantPositionExceedsMaxRightCapWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantPositionExceedsMaxRightCapWarnings);

        /// <summary>
        ///		Gets or sets whether PositionExceedsMaxRightCap validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantPositionExceedsMaxRightCapWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantPositionExceedsMaxRightCapWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantPositionExceedsMaxRightCapWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantPositionExceedsMaxStackWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantPositionExceedsMaxStackWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantPositionExceedsMaxStackWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantPositionExceedsMaxStackWarnings);

        /// <summary>
        ///		Gets or sets whether PositionExceedsMaxStack validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantPositionExceedsMaxStackWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantPositionExceedsMaxStackWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantPositionExceedsMaxStackWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantPositionExceedsMaxTopCapWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantPositionExceedsMaxTopCapWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantPositionExceedsMaxTopCapWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantPositionExceedsMaxTopCapWarnings);

        /// <summary>
        ///		Gets or sets whether PositionExceedsMaxTopCap validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantPositionExceedsMaxTopCapWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantPositionExceedsMaxTopCapWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantPositionExceedsMaxTopCapWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantPositionHasCollisionsWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantPositionHasCollisionsWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantPositionHasCollisionsWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantPositionHasCollisionsWarnings);

        /// <summary>
        ///		Gets or sets whether PositionHasCollisions validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantPositionHasCollisionsWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantPositionHasCollisionsWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantPositionHasCollisionsWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantPositionHasInvalidMerchStyleSettingsWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantPositionHasInvalidMerchStyleSettingsWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantPositionHasInvalidMerchStyleSettingsWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantPositionHasInvalidMerchStyleSettingsWarnings);

        /// <summary>
        ///		Gets or sets whether PositionHasInvalidMerchStyleSettings validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantPositionHasInvalidMerchStyleSettingsWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantPositionHasInvalidMerchStyleSettingsWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantPositionHasInvalidMerchStyleSettingsWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantPositionIsHangingTrayWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantPositionIsHangingTrayWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantPositionIsHangingTrayWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantPositionIsHangingTrayWarnings);

        /// <summary>
        ///		Gets or sets whether PositionIsHangingTray validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantPositionIsHangingTrayWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantPositionIsHangingTrayWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantPositionIsHangingTrayWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantPositionIsHangingWithoutPegWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantPositionIsHangingWithoutPegWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantPositionIsHangingWithoutPegWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantPositionIsHangingWithoutPegWarnings);

        /// <summary>
        ///		Gets or sets whether PositionIsHangingWithoutPeg validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantPositionIsHangingWithoutPegWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantPositionIsHangingWithoutPegWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantPositionIsHangingWithoutPegWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantPositionIsOutsideMerchandisingSpaceWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantPositionIsOutsideMerchandisingSpaceWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantPositionIsOutsideMerchandisingSpaceWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantPositionIsOutsideMerchandisingSpaceWarnings);

        /// <summary>
        ///		Gets or sets whether PositionIsOutsideMerchandisingSpace validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantPositionIsOutsideMerchandisingSpaceWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantPositionIsOutsideMerchandisingSpaceWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantPositionIsOutsideMerchandisingSpaceWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantProductDoesNotAchieveMinCasesWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantProductDoesNotAchieveMinCasesWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantProductDoesNotAchieveMinCasesWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantProductDoesNotAchieveMinCasesWarnings);

        /// <summary>
        ///		Gets or sets whether ProductDoesNotAchieveMinCases validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantProductDoesNotAchieveMinCasesWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantProductDoesNotAchieveMinCasesWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantProductDoesNotAchieveMinCasesWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantProductDoesNotAchieveMinDeliveriesWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantProductDoesNotAchieveMinDeliveriesWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantProductDoesNotAchieveMinDeliveriesWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantProductDoesNotAchieveMinDeliveriesWarnings);

        /// <summary>
        ///		Gets or sets whether ProductDoesNotAchieveMinDeliveries validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantProductDoesNotAchieveMinDeliveriesWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantProductDoesNotAchieveMinDeliveriesWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantProductDoesNotAchieveMinDeliveriesWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantProductDoesNotAchieveMinDosWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantProductDoesNotAchieveMinDosWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantProductDoesNotAchieveMinDosWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantProductDoesNotAchieveMinDosWarnings);

        /// <summary>
        ///		Gets or sets whether ProductDoesNotAchieveMinDos validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantProductDoesNotAchieveMinDosWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantProductDoesNotAchieveMinDosWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantProductDoesNotAchieveMinDosWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantProductDoesNotAchieveMinShelfLifeWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantProductDoesNotAchieveMinShelfLifeWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantProductDoesNotAchieveMinShelfLifeWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantProductDoesNotAchieveMinShelfLifeWarnings);

        /// <summary>
        ///		Gets or sets whether ProductDoesNotAchieveMinShelfLife validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantProductDoesNotAchieveMinShelfLifeWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantProductDoesNotAchieveMinShelfLifeWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantProductDoesNotAchieveMinShelfLifeWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantProductHasDuplicateGtinWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantProductHasDuplicateGtinWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantProductHasDuplicateGtinWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantProductHasDuplicateGtinWarnings);

        /// <summary>
        ///		Gets or sets whether ProductHasDuplicateGtin validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantProductHasDuplicateGtinWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantProductHasDuplicateGtinWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantProductHasDuplicateGtinWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantAssortmentLocalLineRulesWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantAssortmentLocalLineRulesWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantAssortmentLocalLineRulesWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantAssortmentLocalLineRulesWarnings);

        /// <summary>
        ///		Gets or sets whether ShowInstantAssortmentLocalLineRules validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantAssortmentLocalLineRulesWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantAssortmentLocalLineRulesWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantAssortmentLocalLineRulesWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantAssortmentDistributionRulesWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantAssortmentDistributionRulesWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantAssortmentDistributionRulesWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantAssortmentDistributionRulesWarnings);

        /// <summary>
        ///		Gets or sets whether ShowInstantAssortmentLocalLineRules validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantAssortmentDistributionRulesWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantAssortmentDistributionRulesWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantAssortmentDistributionRulesWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantAssortmentProductRulesWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantAssortmentProductRulesWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantAssortmentProductRulesWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantAssortmentProductRulesWarnings);

        /// <summary>
        ///		Gets or sets whether ShowInstantAssortmentLocalLineRules validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantAssortmentProductRulesWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantAssortmentProductRulesWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantAssortmentProductRulesWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantAssortmentFamilyRulesWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantAssortmentFamilyRulesWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantAssortmentFamilyRulesWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantAssortmentFamilyRulesWarnings);

        /// <summary>
        ///		Gets or sets whether ShowInstantAssortmentLocalLineRules validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantAssortmentFamilyRulesWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantAssortmentFamilyRulesWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantAssortmentFamilyRulesWarningsProperty, value); }
        }

        #endregion

        #region ShowInstantAssortmentInheritanceRulesWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantAssortmentInheritanceRulesWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantAssortmentInheritanceRulesWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantAssortmentInheritanceRulesWarnings);

        /// <summary>
        ///		Gets or sets whether ShowInstantAssortmentLocalLineRules validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantAssortmentInheritanceRulesWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantAssortmentInheritanceRulesWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantAssortmentInheritanceRulesWarningsProperty, value); }
        }
        #endregion

        #region ShowInstantAssortmentCoreRulesWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantAssortmentCoreRulesWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantAssortmentCoreRulesWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantAssortmentCoreRulesWarnings);

        /// <summary>
        ///		Gets or sets whether ShowInstantAssortmentLocalLineRules validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantAssortmentCoreRulesWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantAssortmentCoreRulesWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantAssortmentCoreRulesWarningsProperty, value); }
        }
        #endregion

        #region ShowInstantLocationProductIllegalWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantLocationProductIllegalWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantLocationProductIllegalWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantLocationProductIllegalWarnings);

        /// <summary>
        ///		Gets or sets whether ShowAssortmentLocalLineRules validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantLocationProductIllegalWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantLocationProductIllegalWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantLocationProductIllegalWarningsProperty, value); }
        }
        #endregion

        #region ShowInstantLocationProductLegalWarnings

        /// <summary>
        ///		Metadata for the <see cref="ShowInstantLocationProductLegalWarnings"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> ShowInstantLocationProductLegalWarningsProperty =
            RegisterModelProperty<Boolean>(o => o.ShowInstantLocationProductLegalWarnings);

        /// <summary>
        ///		Gets or sets whether ShowAssortmentLocalLineRules validation warnings are displayed to the user.
        /// </summary>
        public Boolean ShowInstantLocationProductLegalWarnings
        {
            get { return this.GetProperty<Boolean>(ShowInstantLocationProductLegalWarningsProperty); }
            set { this.SetProperty<Boolean>(ShowInstantLocationProductLegalWarningsProperty, value); }
        }
        #endregion

        #endregion

        #region IsDynamicTextScalingEnabled

        /// <summary>
        /// IsDynamicTextScalingEnabled property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> IsDynamicTextScalingEnabledProperty =
            RegisterModelProperty<Boolean>(o => o.IsDynamicTextScalingEnabled);

        /// <summary>
        ///Gets/Sets whether text on a planogram should be resized dynamically according to the camera zoom.
        /// </summary>
        public Boolean IsDynamicTextScalingEnabled
        {
            get { return this.GetProperty<Boolean>(IsDynamicTextScalingEnabledProperty); }
            set { this.SetProperty<Boolean>(IsDynamicTextScalingEnabledProperty, value); }
        }

        #endregion

        #region Colors
        /// <summary>
        /// UserEditorSettingsColorList property definition
        /// </summary>
        public static readonly ModelPropertyInfo<UserEditorSettingsColorList> ColorsProperty =
            RegisterModelProperty<UserEditorSettingsColorList>(c => c.Colors);
        /// <summary>
        /// Returns the user colors
        /// </summary>
        public UserEditorSettingsColorList Colors
        {
            get { return GetProperty<UserEditorSettingsColorList>(ColorsProperty); }
        }
        #endregion

        #region Recent Label
        /// <summary>
        /// UserEditorSettingsRecentLabelList property defined.
        /// </summary>
        public static readonly ModelPropertyInfo<UserEditorSettingsRecentLabelList> RecentLabelProperty = 
            RegisterModelProperty<UserEditorSettingsRecentLabelList>(c => c.RecentLabels);
        /// <summary>
        /// returns recent label 
        /// </summary>
        public UserEditorSettingsRecentLabelList RecentLabels
        {
            get { return GetProperty<UserEditorSettingsRecentLabelList>(RecentLabelProperty); }
        }
        #endregion

        #region Selected Search Columns

        /// <summary>
        /// UserEditorSettingsSelectedColumnsList property defined
        /// </summary>
        public static readonly ModelPropertyInfo<UserEditorSettingsSelectedColumnList> SelectedColumnsProperty =
            RegisterModelProperty<UserEditorSettingsSelectedColumnList>(c => c.SelectedColumns);

        public UserEditorSettingsSelectedColumnList SelectedColumns
        {
            get { return GetProperty<UserEditorSettingsSelectedColumnList>(SelectedColumnsProperty); }
        }

        #endregion

        #region Recent Highlight
        /// <summary>
        /// UserEditorSettingsRecentHighlightList property defined.
        /// </summary>
        public static readonly ModelPropertyInfo<UserEditorSettingsRecentHighlightList> RecentHighlightProperty =
            RegisterModelProperty<UserEditorSettingsRecentHighlightList>(c => c.RecentHighlight);
        /// <summary>
        /// returns recent highlight 
        /// </summary>
        public UserEditorSettingsRecentHighlightList RecentHighlight
        {
            get { return GetProperty<UserEditorSettingsRecentHighlightList>(RecentHighlightProperty); }
        }
        #endregion

        #region Recent Datasheet
        /// <summary>
        /// UserEditorSettingsRecentDatasheetList property defined.
        /// </summary>
        public static readonly ModelPropertyInfo<UserEditorSettingsRecentDatasheetList> RecentDatasheetProperty =
            RegisterModelProperty<UserEditorSettingsRecentDatasheetList>(d => d.RecentDatasheet);
        /// <summary>
        /// returns recent highlight 
        /// </summary>
        public UserEditorSettingsRecentDatasheetList RecentDatasheet
        {
            get { return GetProperty<UserEditorSettingsRecentDatasheetList>(RecentDatasheetProperty); }
        }
        #endregion 

        #region DefaultPrintTemplateId

        /// <summary>
        /// DefaultPrintTemplateId property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> DefaultPrintTemplateIdProperty =
            RegisterModelProperty<String>(o => o.DefaultPrintTemplateId);

        /// <summary>
        ///Gets/Sets the id of the default print template to load.
        /// </summary>
        public String DefaultPrintTemplateId
        {
            get { return this.GetProperty<String>(DefaultPrintTemplateIdProperty); }
            set { this.SetProperty<String>(DefaultPrintTemplateIdProperty, value); }
        }

        #endregion

        #region TextBoxFont

        /// <summary>
        /// TextBoxFont property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> TextBoxFontProperty =
            RegisterModelProperty<String>(o => o.TextBoxFont);

        /// <summary>
        /// Gets/Sets the default textbox font
        /// </summary>
        public String TextBoxFont
        {
            get { return this.GetProperty<String>(TextBoxFontProperty); }
            set { this.SetProperty<String>(TextBoxFontProperty, value); }
        }

        #endregion

        #region TextBoxFontSize

        /// <summary>
        /// TextBoxFontSize property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Single> TextBoxFontSizeProperty =
            RegisterModelProperty<Single>(o => o.TextBoxFontSize);

        /// <summary>
        /// Returns the default textbox font size
        /// </summary>
        public Single TextBoxFontSize
        {
            get { return this.GetProperty<Single>(TextBoxFontSizeProperty); }
            set { this.SetProperty<Single>(TextBoxFontSizeProperty, value); }
        }

        #endregion

        #region TextBoxCanReduceToFit

        /// <summary>
        /// TextBoxCanReduceToFit property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> TextBoxCanReduceToFitProperty =
            RegisterModelProperty<Boolean>(o => o.TextBoxCanReduceToFit);


        /// <summary>
        /// Returns true of the textbox setting for reduce to fit is on.
        /// </summary>
        public Boolean TextBoxCanReduceToFit
        {
            get { return this.GetProperty<Boolean>(TextBoxCanReduceToFitProperty); }
            set { this.SetProperty<Boolean>(TextBoxCanReduceToFitProperty, value); }
        }

        #endregion

        #region TextBoxFontColour

        /// <summary>
        /// TextBoxFontColour property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> TextBoxFontColourProperty =
            RegisterModelProperty<Int32>(o => o.TextBoxFontColour);

        /// <summary>
        /// The default textbox font colour.
        /// </summary>
        public Int32 TextBoxFontColour
        {
            get { return this.GetProperty<Int32>(TextBoxFontColourProperty); }
            set { this.SetProperty<Int32>(TextBoxFontColourProperty, value); }
        }

        #endregion

        #region TextBoxBackgroundColour

        /// <summary>
        /// TextBoxBackgroundColour property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> TextBoxBackgroundColourProperty =
            RegisterModelProperty<Int32>(o => o.TextBoxBackgroundColour);

        /// <summary>
        /// The default textbox background colour
        /// </summary>
        public Int32 TextBoxBackgroundColour
        {
            get { return this.GetProperty<Int32>(TextBoxBackgroundColourProperty); }
            set { this.SetProperty<Int32>(TextBoxBackgroundColourProperty, value); }
        }


        #endregion

        #region TextBoxBorderColour

        /// <summary>
        /// TextBoxBorderColour property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> TextBoxBorderColourProperty =
            RegisterModelProperty<Int32>(o => o.TextBoxBorderColour);


        /// <summary>
        /// The default textbox border colour
        /// </summary>
        public Int32 TextBoxBorderColour
        {
            get { return this.GetProperty<Int32>(TextBoxBorderColourProperty); }
            set { this.SetProperty<Int32>(TextBoxBorderColourProperty, value); }
        }

        #endregion

        #region TextBoxBorderThickness

        /// <summary>
        /// TextBoxBorderThickness property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Single> TextBoxBorderThicknessProperty =
            RegisterModelProperty<Single>(o => o.TextBoxBorderThickness);

        /// <summary>
        /// The default textbox border thickness
        /// </summary>
        public Single TextBoxBorderThickness
        {
            get { return this.GetProperty<Single>(TextBoxBorderThicknessProperty); }
            set { this.SetProperty<Single>(TextBoxBorderThicknessProperty, value); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new SystemSetting
        /// </summary>
        internal static UserEditorSettings NewUserEditorSettings()
        {
            UserEditorSettings item = new UserEditorSettings();
            item.Create();
            return item;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies default property values.
        /// </summary>
        private void LoadDefaults()
        {
            Boolean isMetricRegion = RegionInfo.CurrentRegion.IsMetric;

            #region Set directories
            //+ Default directories
            String editorAppDataFolder =
                Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Constants.EditorAppDataFolderName);

            //The default location of documents will be defined by the type of install chosen.
            String myDocsFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), Constants.MyDocsAppFolderName);
            String myPublicDocsFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments), Constants.MyDocsAppFolderName);
            String calculatedDefaultDocsFolder;

            if (Directory.Exists(myPublicDocsFolder))
            {
                //Sample files were installed in public... (All users)
                //Set default folder to public
                calculatedDefaultDocsFolder = myPublicDocsFolder;
            }
            else
            {
                if (Directory.Exists(myDocsFolder))
                {
                    //Sample files were installed in MyDocuments... (Just current user)
                    //Set default folder to documents
                    calculatedDefaultDocsFolder = myDocsFolder;
                }
                else
                {
                    //No Sample files were installed... (User elected not to install sample files).
                    //We choose documents as default location anyway
                    calculatedDefaultDocsFolder = myDocsFolder;
                }
            }
            String fileTemplatesFolder = Path.Combine(calculatedDefaultDocsFolder, Constants.FileTemplatesFolderName);
            String exportFileTemplatesFolder = Path.Combine(calculatedDefaultDocsFolder, Constants.ExportFileTemplatesFolderName);

            #endregion

            #region Load General Default values

            LoadProperty<String>(PlanogramLocationProperty, Environment.GetFolderPath(Environment.SpecialFolder.UserProfile));
            LoadProperty<String>(ImageLocationProperty, Path.Combine(calculatedDefaultDocsFolder, "Images"));
            LoadProperty<String>(FixtureLibraryLocationProperty, Path.Combine(calculatedDefaultDocsFolder, "Fixtures"));
            LoadProperty<String>(LabelLocationProperty, Path.Combine(calculatedDefaultDocsFolder, "Labels"));
            LoadProperty<String>(HighlightLocationProperty, Path.Combine(calculatedDefaultDocsFolder, "Highlights"));
            LoadProperty<String>(ColumnLayoutLocationProperty, Path.Combine(calculatedDefaultDocsFolder, "Column Layouts"));
            LoadProperty<String>(DataSheetLayoutLocationProperty, Path.Combine(calculatedDefaultDocsFolder, "Data Sheets"));
            LoadProperty<String>(ValidationTemplateLocationProperty, Path.Combine(calculatedDefaultDocsFolder, "Validation Templates"));
            LoadProperty<String>(PlanogramFileTemplateLocationProperty, fileTemplatesFolder);
            LoadProperty<String>(PlanogramExportFileTemplateLocationProperty, exportFileTemplatesFolder);
            LoadProperty<String>(PrintTemplateLocationProperty, Path.Combine(calculatedDefaultDocsFolder, "Print Templates"));
            LoadProperty<String>(PlanogramComparisonTemplateLocationProperty, Path.Combine(calculatedDefaultDocsFolder, Constants.PlanogramComparisonTemplatesFolderName));
            LoadProperty<String>(DataSheetFavourite1Property, "");
            LoadProperty<String>(DataSheetFavourite2Property, "");
            LoadProperty<String>(DataSheetFavourite3Property, "");
            LoadProperty<String>(DataSheetFavourite4Property, "");
            LoadProperty<String>(DataSheetFavourite5Property, "");

            LoadProperty<String>(DefaultSpacemanFileTemplateProperty, VerifiedPathOrEmpty(fileTemplatesFolder, SpacemanFieldHelper.DefaultPogft));
            LoadProperty<String>(DefaultApolloFileTemplateProperty, VerifiedPathOrEmpty(fileTemplatesFolder, ApolloImportHelper.DefaultPogft));
            LoadProperty<String>(DefaultProspaceFileTemplateProperty, VerifiedPathOrEmpty(fileTemplatesFolder, ProSpaceImportHelper.DefaultPogft));

            LoadProperty<String>(DefaultExportSpacemanFileTemplateProperty, VerifiedPathOrEmpty(exportFileTemplatesFolder, SpacemanExportHelper.DefaultPogeft));
            LoadProperty<String>(DefaultExportApolloFileTemplateProperty, VerifiedPathOrEmpty(exportFileTemplatesFolder, ApolloExportHelper.DefaultPogeft));
            LoadProperty<String>(DefaultExportJDAFileTemplateProperty, VerifiedPathOrEmpty(exportFileTemplatesFolder, JDAExportHelper.DefaultPogeft));

            //+ AutoSave
            LoadProperty<Boolean>(IsAutosaveEnabledProperty, false);
            LoadProperty<Int32>(AutosaveIntervalProperty, 20);
            LoadProperty<String>(AutosaveLocationProperty, Path.Combine(calculatedDefaultDocsFolder, "Autosaves"));


            //+ Product library
            LoadProperty<String>(DefaultProductLibrarySourceProperty, String.Empty);
            LoadProperty<DefaultProductLibrarySourceType>(DefaultProductLibrarySourceTypeProperty, DefaultProductLibrarySourceType.Unknown);

            #endregion

            #region ViewSettings

            LoadProperty<String>(DisplayLanguageCodeProperty, "en-US");
            LoadProperty<Boolean>(IsShowSelectionAsBlinkingEnabledProperty, true);
            LoadProperty<Int32>(BlinkSpeedProperty, 500);
            LoadProperty<Boolean>(IsDynamicTextScalingEnabledProperty, false);

            if (isMetricRegion)
            {
                LoadProperty<PlanogramLengthUnitOfMeasureType>(LengthUnitOfMeasureProperty, PlanogramLengthUnitOfMeasureType.Centimeters);
                LoadProperty<PlanogramCurrencyUnitOfMeasureType>(CurrencyUnitOfMeasureProperty, PlanogramCurrencyUnitOfMeasureType.GBP);
                LoadProperty<PlanogramVolumeUnitOfMeasureType>(VolumeUnitOfMeasureProperty, PlanogramVolumeUnitOfMeasureType.Litres);
                LoadProperty<PlanogramWeightUnitOfMeasureType>(WeightUnitOfMeasureProperty, PlanogramWeightUnitOfMeasureType.Kilograms);
            }
            else
            {
                LoadProperty<PlanogramLengthUnitOfMeasureType>(LengthUnitOfMeasureProperty, PlanogramLengthUnitOfMeasureType.Inches);
                LoadProperty<PlanogramCurrencyUnitOfMeasureType>(CurrencyUnitOfMeasureProperty, PlanogramCurrencyUnitOfMeasureType.USD);
                LoadProperty<PlanogramVolumeUnitOfMeasureType>(VolumeUnitOfMeasureProperty, PlanogramVolumeUnitOfMeasureType.CubicFeet);
                LoadProperty<PlanogramWeightUnitOfMeasureType>(WeightUnitOfMeasureProperty, PlanogramWeightUnitOfMeasureType.Pounds);
            }

            
            LoadProperty<Boolean>(PositionUnitsProperty, true);
            LoadProperty<Boolean>(ProductImagesProperty, false);
            LoadProperty<Boolean>(ProductShapesProperty, false);
            LoadProperty<Boolean>(ProductFillPatternsProperty, false);
            LoadProperty<Boolean>(FixtureImagesProperty, false);
            LoadProperty<Boolean>(FixtureFillPatternsProperty, false);
            LoadProperty<Boolean>(ChestWallsProperty, false);
            LoadProperty<Boolean>(RotateTopDownComponentsProperty, false);
            LoadProperty<Boolean>(ShelfRisersProperty, true);
            LoadProperty<Boolean>(NotchesProperty, true);
            LoadProperty<Boolean>(PegHolesProperty, true);
            LoadProperty<Boolean>(PegsProperty, true);
            LoadProperty<Boolean>(DividerLinesProperty, false);
            LoadProperty<Boolean>(DividersProperty, true);
            LoadProperty<Boolean>(AnnotationsProperty, true);

            LoadProperty<String>(ComponentHoverStatusBarTextProperty,
                "[PlanogramComponent.Name],  " +
                "([PlanogramComponent.MetaWorldX], [PlanogramComponent.MetaWorldY], [PlanogramComponent.MetaWorldZ]),  " +
                "([PlanogramComponent.Height] x [PlanogramComponent.Width] x [PlanogramComponent.Depth])");

            LoadProperty<String>(PositionHoverStatusBarTextProperty, "[PlanogramProduct.Gtin] [PlanogramProduct.Name]");

            LoadProperty<Byte>(StatusBarTextFontSizeProperty, 9);

            LoadProperty<Boolean>(IsLibraryPanelVisibleProperty, false);
            LoadProperty<Int32>(StatusBarHeightProperty, 25);

            LoadProperty<String>(ProductLabelProperty, String.Empty);
            LoadProperty<String>(FixtureLabelProperty, String.Empty);
            LoadProperty<String>(HighlightProperty, String.Empty);


            LoadProperty<String>(PositionPropertiesProperty, GetDefaultPositionProperties());
            LoadProperty<String>(ComponentPropertiesProperty, GetDefaultComponentProperties());


            #endregion

            #region Load Plan Creation Default values

            //+ Plan creation defaults
            PlanogramSettings planogramSettings = PlanogramSettings.NewPlanogramSettings();

            if (isMetricRegion)
            {
                LoadProperty<Single>(ProductHeightProperty, 10F);
                LoadProperty<Single>(ProductWidthProperty, 10F);
                LoadProperty<Single>(ProductDepthProperty, 10F);
            }
            else
            {
                LoadProperty<Single>(ProductHeightProperty, 6F);
                LoadProperty<Single>(ProductWidthProperty, 6F);
                LoadProperty<Single>(ProductDepthProperty, 6F);
            }

            LoadProperty<Single>(FixtureHeightProperty, planogramSettings.FixtureHeight);
            LoadProperty<Single>(FixtureWidthProperty, planogramSettings.FixtureWidth);
            LoadProperty<Single>(FixtureDepthProperty, planogramSettings.FixtureDepth);

            LoadProperty<Boolean>(FixtureHasBackboardProperty, true);
            LoadProperty<Single>(BackboardDepthProperty, planogramSettings.BackboardDepth);

            LoadProperty<Boolean>(FixtureHasBaseProperty, true);
            LoadProperty<Single>(BaseHeightProperty, planogramSettings.BaseHeight);

            LoadProperty<Single>(ShelfHeightProperty, planogramSettings.ShelfHeight);
            LoadProperty<Single>(ShelfWidthProperty, planogramSettings.ShelfWidth);
            LoadProperty<Single>(ShelfDepthProperty, planogramSettings.ShelfDepth);
            LoadProperty<Int32>(ShelfFillColourProperty, planogramSettings.ShelfFillColour);

            LoadProperty<Single>(PegboardHeightProperty, planogramSettings.PegboardHeight);
            LoadProperty<Single>(PegboardWidthProperty, planogramSettings.PegboardWidth);
            LoadProperty<Single>(PegboardDepthProperty, planogramSettings.PegboardDepth);
            LoadProperty<Int32>(PegboardFillColourProperty, planogramSettings.PegboardFillColour);

            LoadProperty<Single>(ChestHeightProperty, planogramSettings.ChestHeight);
            LoadProperty<Single>(ChestWidthProperty, planogramSettings.ChestWidth);
            LoadProperty<Single>(ChestDepthProperty, planogramSettings.ChestDepth);
            LoadProperty<Int32>(ChestFillColourProperty, planogramSettings.ChestFillColour);

            LoadProperty<Single>(BarHeightProperty, planogramSettings.BarHeight);
            LoadProperty<Single>(BarWidthProperty, planogramSettings.BarWidth);
            LoadProperty<Single>(BarDepthProperty, planogramSettings.BarDepth);
            LoadProperty<Int32>(BarFillColourProperty, planogramSettings.BarFillColour);

            LoadProperty<Single>(RodHeightProperty, planogramSettings.RodHeight);
            LoadProperty<Single>(RodWidthProperty, planogramSettings.RodWidth);
            LoadProperty<Single>(RodDepthProperty, planogramSettings.RodDepth);
            LoadProperty<Int32>(RodFillColourProperty, planogramSettings.RodFillColour);

            LoadProperty<Single>(ClipStripHeightProperty, planogramSettings.ClipStripHeight);
            LoadProperty<Single>(ClipStripWidthProperty, planogramSettings.ClipStripWidth);
            LoadProperty<Single>(ClipStripDepthProperty, planogramSettings.ClipStripDepth);
            LoadProperty<Int32>(ClipStripFillColourProperty, planogramSettings.ClipStripFillColour);

            LoadProperty<Single>(PalletHeightProperty, planogramSettings.PalletHeight);
            LoadProperty<Single>(PalletWidthProperty, planogramSettings.PalletWidth);
            LoadProperty<Single>(PalletDepthProperty, planogramSettings.PalletDepth);
            LoadProperty<Int32>(PalletFillColourProperty, planogramSettings.PalletFillColour);

            LoadProperty<Single>(SlotwallHeightProperty, planogramSettings.SlotwallHeight);
            LoadProperty<Single>(SlotwallWidthProperty, planogramSettings.SlotwallWidth);
            LoadProperty<Single>(SlotwallDepthProperty, planogramSettings.SlotwallDepth);
            LoadProperty<Int32>(SlotwallFillColourProperty, planogramSettings.SlotwallFillColour);

            LoadProperty<PlanogramProductPlacementXType>(ProductPlacementXProperty, PlanogramProductPlacementXType.Manual);
            LoadProperty<PlanogramProductPlacementYType>(ProductPlacementYProperty, PlanogramProductPlacementYType.FillHigh);
            LoadProperty<PlanogramProductPlacementZType>(ProductPlacementZProperty, PlanogramProductPlacementZType.FillDeep);


            LoadProperty<String>(TextBoxFontProperty, planogramSettings.TextBoxFont);
            LoadProperty<Single>(TextBoxFontSizeProperty, planogramSettings.TextBoxFontSize);
            LoadProperty<Boolean>(TextBoxCanReduceToFitProperty, planogramSettings.TextBoxCanReduceToFit);
            LoadProperty<Int32>(TextBoxFontColourProperty, planogramSettings.TextBoxFontColour);
            LoadProperty<Int32>(TextBoxBorderColourProperty, planogramSettings.TextBoxBorderColour);
            LoadProperty<Int32>(TextBoxBackgroundColourProperty, planogramSettings.TextBoxBackgroundColour);
            LoadProperty<Single>(TextBoxBorderThicknessProperty, planogramSettings.TextBoxBorderThickness);


            #endregion

            #region Product Images
            LoadProperty<RealImageProviderType>(ProductImageSourceProperty, RealImageProviderType.Repository);
            LoadProperty<String>(ProductImageLocationProperty, Path.Combine(calculatedDefaultDocsFolder, "Images"));
            LoadProperty<String>(ProductImageAttributeProperty,
                PlanogramProduct.EnumerateDisplayableFieldInfos(/*incMetadata*/false, /*inCustom*/true, /*incPerformance*/false)
                .First(o => o.PropertyName == PlanogramProduct.GtinProperty.Name).FieldPlaceholder);
            LoadProperty<Boolean>(ProductImageIgnoreLeadingZerosProperty, false);
            LoadProperty<Byte>(ProductImageMinFieldLengthProperty, (Byte)10);
            LoadProperty<String>(ProductImageFacingPostfixFrontProperty, ((Int32)PlanogramImageFacingType.Front).ToString());
            LoadProperty<String>(ProductImageFacingPostfixLeftProperty, ((Int32)PlanogramImageFacingType.Left).ToString());
            LoadProperty<String>(ProductImageFacingPostfixTopProperty, ((Int32)PlanogramImageFacingType.Top).ToString());
            LoadProperty<String>(ProductImageFacingPostfixBackProperty, ((Int32)PlanogramImageFacingType.Back).ToString());
            LoadProperty<String>(ProductImageFacingPostfixRightProperty, ((Int32)PlanogramImageFacingType.Right).ToString());
            LoadProperty<String>(ProductImageFacingPostfixBottomProperty, ((Int32)PlanogramImageFacingType.Bottom).ToString());
            LoadProperty<RealImageCompressionType>(ProductImageCompressionProperty, RealImageCompressionType.None);
            LoadProperty<RealImageColourDepthType>(ProductImageColourDepthProperty, RealImageColourDepthType.Bit8);
            LoadProperty<Boolean>(ProductImageTransparencyProperty, false);
            #endregion

            #region Load Validation Default Settings

            //+ Validation Settings
            LoadProperty<Boolean>(ShowComponentHasCollisionsWarningsProperty, true);
            LoadProperty<Boolean>(ShowComponentIsOverfilledWarningsProperty, false);
            LoadProperty<Boolean>(ShowComponentSlopeWithNoRiserWarningsProperty, true);
            LoadProperty<Boolean>(ShowComponentIsOutsideOfFixtureAreaWarningsProperty, true);
            LoadProperty<Boolean>(ShowPegIsOverfilledWarningsProperty, true);
            LoadProperty<Boolean>(ShowPositionCannotBreakTrayBackWarningsProperty, true);
            LoadProperty<Boolean>(ShowPositionCannotBreakTrayDownWarningsProperty, true);
            LoadProperty<Boolean>(ShowPositionCannotBreakTrayTopWarningsProperty, true);
            LoadProperty<Boolean>(ShowPositionCannotBreakTrayUpWarningsProperty, true);
            LoadProperty<Boolean>(ShowPositionDoesNotAchieveMinDeepWarningsProperty, true);
            LoadProperty<Boolean>(ShowPositionExceedsMaxDeepWarningsProperty, true);
            LoadProperty<Boolean>(ShowPositionExceedsMaxRightCapWarningsProperty, true);
            LoadProperty<Boolean>(ShowPositionExceedsMaxStackWarningsProperty, true);
            LoadProperty<Boolean>(ShowPositionExceedsMaxTopCapWarningsProperty, true);
            LoadProperty<Boolean>(ShowPositionHasCollisionsWarningsProperty, true);
            LoadProperty<Boolean>(ShowPositionHasInvalidMerchStyleSettingsWarningsProperty, true);
            LoadProperty<Boolean>(ShowPositionIsHangingTrayWarningsProperty, true);
            LoadProperty<Boolean>(ShowPositionIsHangingWithoutPegWarningsProperty, true);
            LoadProperty<Boolean>(ShowPositionIsOutsideMerchandisingSpaceWarningsProperty, true);
            LoadProperty<Boolean>(ShowProductDoesNotAchieveMinCasesWarningsProperty, true);
            LoadProperty<Boolean>(ShowProductDoesNotAchieveMinDeliveriesWarningsProperty, true);
            LoadProperty<Boolean>(ShowProductDoesNotAchieveMinDosWarningsProperty, true);
            LoadProperty<Boolean>(ShowProductDoesNotAchieveMinShelfLifeWarningsProperty, true);
            LoadProperty<Boolean>(ShowProductHasDuplicateGtinWarningsProperty, true);
            LoadProperty<Boolean>(ShowAssortmentLocalLineRulesWarningsProperty, true);
            LoadProperty<Boolean>(ShowAssortmentDistributionRulesWarningsProperty, true);
            LoadProperty<Boolean>(ShowAssortmentProductRulesWarningsProperty, true);
            LoadProperty<Boolean>(ShowAssortmentFamilyRulesWarningsProperty, true);
            LoadProperty<Boolean>(ShowAssortmentInheritanceRulesWarningsProperty, true);
            LoadProperty<Boolean>(ShowAssortmentCoreRulesWarningsProperty, true);
            LoadProperty<Boolean>(ShowLocationProductIllegalWarningsProperty, true);
            LoadProperty<Boolean>(ShowLocationProductLegalWarningsProperty, false);

            //+ Instant Validaton Settings
            LoadProperty<Boolean>(ShowInstantComponentHasCollisionsWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantComponentIsOverfilledWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantComponentSlopeWithNoRiserWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantComponentIsOutsideOfFixtureAreaWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantPegIsOverfilledWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantPositionCannotBreakTrayBackWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantPositionCannotBreakTrayDownWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantPositionCannotBreakTrayTopWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantPositionCannotBreakTrayUpWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantPositionDoesNotAchieveMinDeepWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantPositionExceedsMaxDeepWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantPositionExceedsMaxRightCapWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantPositionExceedsMaxStackWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantPositionExceedsMaxTopCapWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantPositionHasCollisionsWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantPositionHasInvalidMerchStyleSettingsWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantPositionIsHangingTrayWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantPositionIsHangingWithoutPegWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantPositionIsOutsideMerchandisingSpaceWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantProductDoesNotAchieveMinCasesWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantProductDoesNotAchieveMinDeliveriesWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantProductDoesNotAchieveMinDosWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantProductDoesNotAchieveMinShelfLifeWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantProductHasDuplicateGtinWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantAssortmentLocalLineRulesWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantAssortmentDistributionRulesWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantAssortmentProductRulesWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantAssortmentFamilyRulesWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantAssortmentInheritanceRulesWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantAssortmentCoreRulesWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantLocationProductIllegalWarningsProperty, false);
            LoadProperty<Boolean>(ShowInstantLocationProductLegalWarningsProperty, false);

            #endregion
        }

        /// <summary>
        /// Returns the default position properties string.
        /// </summary>
        private String GetDefaultPositionProperties()
        {
            StringBuilder sb = new StringBuilder();


            String[] productProperties =
                new String[]
                {
                    PlanogramProduct.GtinProperty.Name,
                    PlanogramProduct.NameProperty.Name,
                    PlanogramProduct.BrandProperty.Name,
                    PlanogramProduct.HeightProperty.Name,
                    PlanogramProduct.WidthProperty.Name,
                    PlanogramProduct.DepthProperty.Name,
                };

            IEnumerable<ObjectFieldInfo> allFields = PlanogramProduct.EnumerateDisplayableFieldInfos(true, true, true);
            foreach (String property in productProperties)
            {
                ObjectFieldInfo field = allFields.FirstOrDefault(f => f.PropertyName == property);
                if (field != null) sb.AppendFormat("{0},", field.FieldPlaceholder);
            }

            String[] positionProperties =
                new String[]
                {
                    PlanogramPosition.FacingsHighProperty.Name,
                    PlanogramPosition.FacingsWideProperty.Name,
                    PlanogramPosition.FacingsDeepProperty.Name,
                    PlanogramPosition.OrientationTypeProperty.Name,
                    PlanogramPosition.MerchandisingStyleProperty.Name,
                    PlanogramPosition.UnitsHighProperty.Name,
                    PlanogramPosition.UnitsWideProperty.Name,
                    PlanogramPosition.UnitsDeepProperty.Name,
                    PlanogramPosition.TotalUnitsProperty.Name,
                };

            allFields = PlanogramPosition.EnumerateDisplayableFieldInfos();
            foreach (String property in positionProperties)
            {
                ObjectFieldInfo field = allFields.FirstOrDefault(f => f.PropertyName == property);
                if (field != null) sb.AppendFormat("{0},", field.FieldPlaceholder);
            }

            return sb.ToString().TrimEnd(',');
        }

        /// <summary>
        /// Returns the default component properties string.
        /// </summary>
        private String GetDefaultComponentProperties()
        {
            StringBuilder sb = new StringBuilder();


            String[] defaultFieldNames =
                new String[]
                {
                    PlanogramComponent.NameProperty.Name,
                    PlanogramComponent.ComponentTypeProperty.Name,
                    PlanogramComponent.HeightProperty.Name,
                    PlanogramComponent.WidthProperty.Name,
                    PlanogramComponent.DepthProperty.Name,
                };

            IEnumerable<ObjectFieldInfo> allFields = PlanogramComponent.EnumerateDisplayableFieldInfos(true);
            foreach (String property in defaultFieldNames)
            {
                ObjectFieldInfo field = allFields.FirstOrDefault(f => f.PropertyName == property);
                if (field != null) sb.AppendFormat("{0},", field.FieldPlaceholder);
            }

            defaultFieldNames =
                new String[]
                {
                    PlanogramFixtureComponent.MetaWorldXProperty.Name,
                    PlanogramFixtureComponent.MetaWorldYProperty.Name,
                    PlanogramFixtureComponent.MetaWorldZProperty.Name
                };

            allFields = PlanogramFixtureComponent.EnumerateDisplayableFieldInfos();
            foreach (String property in defaultFieldNames)
            {
                ObjectFieldInfo field = allFields.FirstOrDefault(f => f.PropertyName == property);
                if (field != null) sb.AppendFormat("{0},", field.FieldPlaceholder);
            }

            defaultFieldNames =
            new String[]
            {
                PlanogramSubComponent.MerchandisingStrategyXProperty.Name,
                PlanogramSubComponent.MerchandisingStrategyYProperty.Name,
                PlanogramSubComponent.MerchandisingStrategyZProperty.Name,
                PlanogramSubComponent.MerchandisableHeightProperty.Name,
                PlanogramSubComponent.MerchandisableDepthProperty.Name,
                PlanogramSubComponent.CombineTypeProperty.Name,

            };

            allFields = PlanogramSubComponent.EnumerateDisplayableFieldInfos();
            foreach (String property in defaultFieldNames)
            {
                ObjectFieldInfo field = allFields.FirstOrDefault(f => f.PropertyName == property);
                if (field != null) sb.AppendFormat("{0},", field.FieldPlaceholder);
            }

            return sb.ToString().TrimEnd(',');
        }

        /// <summary>
        ///     Returns the combined path if it exists or an empty string if not.
        /// </summary>
        /// <param name="folderPath">The folder path.</param>
        /// <param name="file">The file name</param>
        /// <returns></returns>
        private static String VerifiedPathOrEmpty(String folderPath, String file)
        {
            String path = Path.Combine(folderPath, file);
            return System.IO.File.Exists(path) ? path : String.Empty;
        }

        #endregion

        #region Criteria

        /// <summary>
        /// Generic criteria used when returning this item
        /// from the dalfactory.
        /// </summary>
        [Serializable]
        public class FetchCriteria : CriteriaBase<FetchCriteria>
        {
            #region Properties

            #region DalFactoryName
            /// <summary>
            /// DalFactoryName property definition
            /// </summary>
            public static readonly PropertyInfo<String> DalFactoryNameProperty =
                RegisterProperty<String>(c => c.DalFactoryName);
            /// <summary>
            /// Returns the dal factory name
            /// </summary>
            public String DalFactoryName
            {
                get { return this.ReadProperty<String>(DalFactoryNameProperty); }
            }
            #endregion

            #endregion

            #region Constructor

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchCriteria()
            {
                this.LoadProperty<String>(DalFactoryNameProperty, Constants.UserDal);
            }

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchCriteria(String dalFactoryName)
            {
                this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            }
            #endregion
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<UserEditorSettingsColorList>(ColorsProperty, UserEditorSettingsColorList.NewList());
            this.LoadProperty<UserEditorSettingsRecentLabelList>(RecentLabelProperty, UserEditorSettingsRecentLabelList.NewList());
            this.LoadProperty<UserEditorSettingsRecentHighlightList>(RecentHighlightProperty, UserEditorSettingsRecentHighlightList.NewList());
            this.LoadProperty<UserEditorSettingsRecentDatasheetList>(RecentDatasheetProperty, UserEditorSettingsRecentDatasheetList.NewList());
            this.LoadProperty<UserEditorSettingsSelectedColumnList>(SelectedColumnsProperty, UserEditorSettingsSelectedColumnList.NewList());
            
            LoadDefaults();
            
            base.Create();
        }

        #endregion

        #endregion
    }
}