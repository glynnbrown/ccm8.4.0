﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26234 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a security role within GFS
    /// </summary>
    public partial class Role
    {
        #region Constructor
        private Role() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the entity with the given id
        /// </summary>
        /// <param name="id">The role id</param>
        /// <returns>The specified role</returns>
        public static Role FetchById(Int32 id)
        {
            return DataPortal.Fetch<Role>(new FetchByIdCriteria(id));
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, RoleDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<String>(DescriptionProperty, dto.Description);
            LoadProperty<Boolean>(IsAdministratorRoleProperty, dto.IsAdministrator);
            LoadProperty<RoleEntityList>(EntitiesProperty, RoleEntityList.GetListByParentId(dalContext, dto.Id));
            LoadProperty<RoleMemberList>(MembersProperty, RoleMemberList.FetchByRoleId(dalContext, dto.Id));
            LoadProperty<RolePermissionList>(PermissionsProperty, RolePermissionList.GetRolePermissions(dalContext, dto.Id));
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private RoleDto GetDataTransferObject()
        {
            return new RoleDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                Name = ReadProperty<String>(NameProperty),
                Description = ReadProperty<String>(DescriptionProperty),
                IsAdministrator = ReadProperty<Boolean>(IsAdministratorRoleProperty)
            };
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Called when return FetchById
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IRoleDal dal = dalContext.GetDal<IRoleDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchById(criteria.Id));
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                RoleDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (IRoleDal dal = dalContext.GetDal<IRoleDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                dalContext.RegisterId<Role>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this object in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                RoleDto dto = GetDataTransferObject();
                using (IRoleDal dal = dalContext.GetDal<IRoleDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IRoleDal dal = dalContext.GetDal<IRoleDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }
        #endregion

        #endregion
    }
}
