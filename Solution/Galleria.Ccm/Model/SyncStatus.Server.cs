﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using System.ServiceProcess;
using System.Linq;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Object used to return the current
    /// status of the synchronisation service
    /// </summary>
    public partial class SyncStatus
    {
        #region Constants
        /// <summary>
        /// The name of the synchronisation service
        /// </summary>
        private const string _serviceName = "CCM Sync Service"; // This needs to match the sync service name
        #endregion

        #region Constructors
        private SyncStatus() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the current sync status object
        /// </summary>
        /// <returns>The current sync status</returns>
        public static SyncStatus GetCurrentStatus()
        {
            return DataPortal.Fetch<SyncStatus>();
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when retrieving an object of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch()
        {
            // determine the status of the synchornisation service
            ServiceController[] serviceControllers = ServiceController.GetServices();
            ServiceController serviceController = serviceControllers.FirstOrDefault(o => o.ServiceName == _serviceName);
            if (serviceController != null)
            {
                switch (serviceController.Status)
                {
                    case ServiceControllerStatus.Running:
                    case ServiceControllerStatus.PausePending:
                    case ServiceControllerStatus.StopPending:
                        this.LoadProperty<SyncServiceStatus>(ServiceStatusProperty, SyncServiceStatus.Idle);
                        break;
                    case ServiceControllerStatus.Stopped:
                    case ServiceControllerStatus.ContinuePending:
                    case ServiceControllerStatus.StartPending:
                        this.LoadProperty<SyncServiceStatus>(ServiceStatusProperty, SyncServiceStatus.Stopped);
                        break;
                    case ServiceControllerStatus.Paused:
                        this.LoadProperty<SyncServiceStatus>(ServiceStatusProperty, SyncServiceStatus.Paused);
                        break;
                }
            }
            else
            {
                this.LoadProperty<SyncServiceStatus>(ServiceStatusProperty, SyncServiceStatus.Unknown);
            }

            // if the service is stopped or paused then none of the sync targets can be running
            if (this.ServiceStatus != SyncServiceStatus.Stopped && this.ServiceStatus != SyncServiceStatus.Paused)
            {
                SyncTarget syncTargetError = null;
                SyncTarget syncTargetRunning = null;

                // get the sync targets
                SyncTargetList syncTargets = SyncTargetList.GetAllSyncTargets(true);

                // determine if any of the sync targets is currently running
                syncTargetRunning = syncTargets.FirstOrDefault(o => (o.Status == SyncTargetStatus.Running) || (o.Status == SyncTargetStatus.Cancelling));
                if (syncTargetRunning != null)
                {
                    this.LoadProperty<SyncServiceStatus>(ServiceStatusProperty, SyncServiceStatus.Syncing);
                }
                else
                {
                    // if none are running then determine if any of the sync targets had a problem
                    syncTargetError = syncTargets.FirstOrDefault(o => (o.Status == SyncTargetStatus.Errored) || (o.Status == SyncTargetStatus.Failed));
                    if (syncTargetError != null)
                    {
                        this.LoadProperty<SyncServiceStatus>(ServiceStatusProperty, SyncServiceStatus.Error);
                    }
                }
            }

            // load the service description based on the status
            this.LoadProperty<String>(ServiceStatusDescriptionProperty, SyncServiceStatusHelper.FriendlyDescriptions[this.ServiceStatus]);
        }
        #endregion

        #endregion
    }
}