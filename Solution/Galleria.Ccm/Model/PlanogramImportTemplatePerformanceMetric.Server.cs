﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.1)
//V8-28622 : D.Pleasance
//  Created
#endregion
#region Version History: (CCM 8.0.3)
// V8-29682 : A.Probyn
//  Added AggregationType
#endregion
#region Version History: (CCM 8.1.0)
// V8-30215 : M.Brumby
//  Changed AggregationType to use PlanogramPerformanceMetricAggregationType for consitancey
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Enums;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramImportTemplatePerformanceMetric
    {
        #region Constructor
        private PlanogramImportTemplatePerformanceMetric() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static PlanogramImportTemplatePerformanceMetric Fetch(IDalContext dalContext, PlanogramImportTemplatePerformanceMetricDto dto)
        {
            return DataPortal.FetchChild<PlanogramImportTemplatePerformanceMetric>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramImportTemplatePerformanceMetricDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(DescriptionProperty, dto.Description);
            this.LoadProperty<MetricDirectionType>(DirectionProperty, (MetricDirectionType)dto.Direction);
            this.LoadProperty<MetricSpecialType>(SpecialTypeProperty, (MetricSpecialType)dto.SpecialType);
            this.LoadProperty<MetricType>(MetricTypeProperty, (MetricType)dto.MetricType);
            this.LoadProperty<Byte>(MetricIdProperty, dto.MetricId);
            this.LoadProperty<String>(ExternalFieldProperty, dto.ExternalField);
            this.LoadProperty<AggregationType>(AggregationTypeProperty, (AggregationType)dto.AggregationType);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private PlanogramImportTemplatePerformanceMetricDto GetDataTransferObject(PlanogramImportTemplate parent)
        {
            return new PlanogramImportTemplatePerformanceMetricDto()
            {
                Id = ReadProperty(IdProperty),
                PlanogramImportTemplateId = parent.Id,
                Name = this.ReadProperty<String>(NameProperty),
                Description = this.ReadProperty<String>(DescriptionProperty),
                Direction = (Byte)this.ReadProperty<MetricDirectionType>(DirectionProperty),
                SpecialType = (Byte)this.ReadProperty<MetricSpecialType>(SpecialTypeProperty),
                MetricType = (Byte)this.ReadProperty<MetricType>(MetricTypeProperty),
                MetricId = this.ReadProperty<Byte>(MetricIdProperty),
                ExternalField = ReadProperty(ExternalFieldProperty),
                AggregationType = (Byte)this.ReadProperty<AggregationType>(AggregationTypeProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, PlanogramImportTemplatePerformanceMetricDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, PlanogramImportTemplate parent)
        {
            PlanogramImportTemplatePerformanceMetricDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IPlanogramImportTemplatePerformanceMetricDal dal = dalContext.GetDal<IPlanogramImportTemplatePerformanceMetricDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<PlanogramImportTemplatePerformanceMetric>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, PlanogramImportTemplate parent)
        {
            if (this.IsSelfDirty)
            {
                using (IPlanogramImportTemplatePerformanceMetricDal dal = dalContext.GetDal<IPlanogramImportTemplatePerformanceMetricDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, PlanogramImportTemplate parent)
        {
            using (IPlanogramImportTemplatePerformanceMetricDal dal = dalContext.GetDal<IPlanogramImportTemplatePerformanceMetricDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion
    }
}