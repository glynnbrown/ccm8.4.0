﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created 
// V8-25873 : A.Probyn
//  Added Custom Attribute 1 - 5
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class FixturePackageInfo
    {

        #region Constructors
        private FixturePackageInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static FixturePackageInfo GetFixturePackageInfo(IDalContext dalContext, FixturePackageInfoDto dto)
        {
            return DataPortal.FetchChild<FixturePackageInfo>(dalContext, dto);
        }

        /// <summary>
        /// Moves the fixture package to a new folder.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="newDirectoryPath"></param>
        public static void MoveFixturePackage(String fileName, String newDirectoryPath)
        {
            DataPortal.Execute<SetFolderIdCommand>(new SetFolderIdCommand(FixturePackageType.FileSystem, fileName, newDirectoryPath));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, FixturePackageInfoDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<Object>(FolderIdProperty, dto.FolderId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(DescriptionProperty, dto.Description);
            this.LoadProperty<Single>(HeightProperty, dto.Height);
            this.LoadProperty<Single>(WidthProperty, dto.Width);
            this.LoadProperty<Single>(DepthProperty, dto.Depth);
            this.LoadProperty<Int32>(FixtureCountProperty, dto.FixtureCount);
            this.LoadProperty<Int32>(AssemblyCountProperty, dto.AssemblyCount);
            this.LoadProperty<Int32>(ComponentCountProperty, dto.ComponentCount);
            this.LoadProperty<Byte[]>(ThumbnailImageDataProperty, dto.ThumbnailImageData);
            this.LoadProperty<String>(CustomAttribute1Property, dto.CustomAttribute1);
            this.LoadProperty<String>(CustomAttribute2Property, dto.CustomAttribute2);
            this.LoadProperty<String>(CustomAttribute3Property, dto.CustomAttribute3);
            this.LoadProperty<String>(CustomAttribute4Property, dto.CustomAttribute4);
            this.LoadProperty<String>(CustomAttribute5Property, dto.CustomAttribute5);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when loading this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FixturePackageInfoDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #endregion

        #region Commands

        #region SetFolderIdCommand

        /// <summary>
        /// Sets the folder id of the given package id
        /// </summary>
        private partial class SetFolderIdCommand : CommandBase<SetFolderIdCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.FixturePackageType, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IFixturePackageInfoDal dal = dalContext.GetDal<IFixturePackageInfoDal>())
                    {
                        dal.SetFolderId(this.Id, this.FolderId);
                    }
                }
            }
            #endregion

            #endregion
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the correct dal factory based on the given type and args.
        /// </summary>
        private static IDalFactory GetDalFactory(FixturePackageType dalType, out String dalFactoryName)
        {
            switch (dalType)
            {
                case FixturePackageType.FileSystem:
                    dalFactoryName = Constants.UserDal;
                    return DalContainer.GetDalFactory(dalFactoryName);

                default:
                    dalFactoryName = DalContainer.DalName;
                    return DalContainer.GetDalFactory();
            }

        }

        #endregion

    }
}
