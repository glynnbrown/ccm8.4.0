﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
#endregion

#endregion

using System;
using AutoMapper;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Model;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// This is a root MasterData object and so it has the following properties/actions:
    ///- This object has a DateDeleted property and associated field in its supporting database table
    ///- If this object is deleted, it is marked as deleted (this allows for 'undeletes') and any child objects are left unchanged
    /// </summary>
    [Serializable]
    public partial class Blob : ModelObject<Blob>
    {
        #region Static Constructor
        static Blob()
        {
        }
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
                RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
        RegisterModelProperty<RowVersion>(c => c.RowVersion);
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }

        public static readonly ModelPropertyInfo<Byte[]> DataProperty =
        RegisterModelProperty<Byte[]>(c => c.Data);
        public Byte[] Data
        {
            get { return GetProperty<Byte[]>(DataProperty); }
            set { SetProperty<Byte[]>(DataProperty, value); }
        }

        /// <summary>
        /// The date when the item was created 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
        }

        /// <summary>
        /// The date when the item was modified 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
        }

        /// <summary>
        /// The date when the item was deleted 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(Blob), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(Blob), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(Blob), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(Blob), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Authenticated.ToString()));
        }
        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }

        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        /// <returns>New Blob</returns>
        public static Blob NewBlob()
        {
            Blob item = new Blob();
            item.Create();
            return item;
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        /// <param name="fileNameAndPath">full file path and name</param>
        /// <returns>New Blob</returns>
        public static Blob NewBlob(String fileNameAndPath)
        {
            Blob item = new Blob();
            item.Create(fileNameAndPath);
            return item;
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        /// <param name="blobData">Blob data to set to new blob model</param>
        /// <returns>New Blob</returns>
        public static Blob NewBlob(Byte[] blobData)
        {
            Blob item = new Blob();
            item.Create(blobData);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        /// <param name="fileNameAndPath">full file path and name</param>
        private void Create(String fileNameAndPath)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Byte[]>(DataProperty, System.IO.File.ReadAllBytes(fileNameAndPath));
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        /// <param name="blobData">Blob data to set to new blob model</param>
        private void Create(Byte[] blobData)
        {
            this.LoadProperty<Byte[]>(DataProperty, blobData);
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}
