﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion

#endregion


using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Csla.Serialization;
using Galleria.Framework.Model;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a File Info within the solution
    /// </summary>
    [Serializable]
    public partial class FileInfo : ModelReadOnlyObject<FileInfo>
    {
        #region Properties

        /// <summary>
        /// The File id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// The entity Id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
        }

        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The file name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        public static readonly ModelPropertyInfo<String> SourceFilePathProperty =
            RegisterModelProperty<String>(c => c.SourceFilePath);
        /// <summary>
        /// The file source path where the file was loaded from
        /// </summary>
        public String SourceFilePath
        {
            get { return GetProperty<String>(SourceFilePathProperty); }
        }

        public static readonly ModelPropertyInfo<Int64> SizeInBytesProperty =
            RegisterModelProperty<Int64>(c => c.SizeInBytes);
        /// <summary>
        /// The file SizeInBytes
        /// </summary>
        public Int64 SizeInBytes
        {
            get { return GetProperty<Int64>(SizeInBytesProperty); }
        }

        public static readonly ModelPropertyInfo<DateTime> DateModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        /// <summary>
        /// The file DateModified
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateModifiedProperty); }
        }

        public static readonly ModelPropertyInfo<Int32?> UserIdProperty =
            RegisterModelProperty<Int32?>(c => c.UserId);
        /// <summary>
        /// The user who created the file
        /// </summary>
        public Int32? UserId
        {
            get { return GetProperty<Int32?>(UserIdProperty); }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(FileInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(FileInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(FileInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(FileInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.Name;
        }

        #endregion
    }
}
