﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//	Created
// CCM-25444 : N.Haywood
//      Added FetchByLocationGroupIds
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// LocationGroup Model object
    /// </summary>
    [Serializable]
    public sealed partial class LocationGroupInfoList : ModelReadOnlyList<LocationGroupInfoList, LocationGroupInfo>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationGroupInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(LocationGroupInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(LocationGroupInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(LocationGroupInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Criteria


        #region FetchDeletedByLocationHierarchyIdCriteria

        /// <summary>
        /// Criteria for the FetchByEntityId factory method
        /// </summary>
        [Serializable]
        public class FetchDeletedByLocationHierarchyIdCriteria : CriteriaBase<FetchDeletedByLocationHierarchyIdCriteria>
        {
            #region Properties

            #region LocationHierarchyId
            /// <summary>
            /// LocationHierarchyId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> LocationHierarchyIdProperty =
                RegisterProperty<Int32>(c => c.LocationHierarchyId);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Int32 LocationHierarchyId
            {
                get { return this.ReadProperty<Int32>(LocationHierarchyIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchDeletedByLocationHierarchyIdCriteria(Int32 hierarchyId)
            {
                this.LoadProperty<Int32>(LocationHierarchyIdProperty, hierarchyId);
            }
            #endregion
        }

        #endregion

        /// <summary>
        /// Criteria for LocationGroupInfoList.FetchByLocationGroupIds
        /// </summary>
        [Serializable]
        public class FetchByLocationGroupIdsCriteria : CriteriaBase<FetchByLocationGroupIdsCriteria>
        {
            #region Properties

            public static PropertyInfo<IEnumerable<Int32>> LocationGroupIdsProperty =
                RegisterProperty<IEnumerable<Int32>>(c => c.LocationGroupIds);
            public IEnumerable<Int32> LocationGroupIds
            {
                get { return ReadProperty<IEnumerable<Int32>>(LocationGroupIdsProperty); }
            }

            #endregion

            public FetchByLocationGroupIdsCriteria(IEnumerable<Int32> locationIds)
            {
                LoadProperty<IEnumerable<Int32>>(LocationGroupIdsProperty, locationIds);
            }
        }

        #endregion
    }
}
