﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History 830
// V8-31699 : A.Heathcote
//  Created.


#endregion
#endregion
  
using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    ///  <summary>
    ///  A Recent Datasheet
    /// </summary>
    [Serializable]
    public partial class UserEditorSettingsRecentDatasheet : ModelObject<UserEditorSettingsRecentDatasheet>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Defines Property 
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Unique Id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region DatasheetID

        public static readonly ModelPropertyInfo<Object> DatasheetIdProperty =
            RegisterModelProperty<Object>(c => c.DatasheetId);
        public Object DatasheetId
        {
            get
            { return GetProperty<Object>(DatasheetIdProperty); }

        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// defines the autherization rules
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Factory Methods

        public static UserEditorSettingsRecentDatasheet NewUserEditorSettingsRecentDatasheet(CustomColumnLayout datasheet)
        {
            UserEditorSettingsRecentDatasheet item = new UserEditorSettingsRecentDatasheet();
            item.Create(datasheet);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        private void Create(CustomColumnLayout datasheet)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Object>(DatasheetIdProperty, datasheet.Id);  //String FileExtension
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}
