﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a synchronisation target for
    /// the sychronisation service
    /// </summary>
    [Serializable]
    public sealed partial class SyncTarget : ModelObject<SyncTarget>
    {
        #region Properties
        /// <summary>
        /// The id property
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// The target type property
        /// </summary>
        public static readonly ModelPropertyInfo<SyncTargetType> TargetTypeProperty =
            RegisterModelProperty<SyncTargetType>(c => c.TargetType);
        public SyncTargetType TargetType
        {
            get { return GetProperty<SyncTargetType>(TargetTypeProperty); }
            set { SetProperty<SyncTargetType>(TargetTypeProperty, value); }
        }

        /// <summary>
        /// The connection string property
        /// </summary>
        public static readonly ModelPropertyInfo<String> ConnectionStringProperty =
            RegisterModelProperty<String>(c => c.ConnectionString);
        public String ConnectionString
        {
            get { return GetProperty<String>(ConnectionStringProperty); }
            set { SetProperty<String>(ConnectionStringProperty, value); }
        }

        /// <summary>
        /// The target status property
        /// </summary>
        public static readonly ModelPropertyInfo<SyncTargetStatus> StatusProperty =
            RegisterModelProperty<SyncTargetStatus>(c => c.Status);
        public SyncTargetStatus Status
        {
            get { return GetProperty<SyncTargetStatus>(StatusProperty); }
            set { SetProperty<SyncTargetStatus>(StatusProperty, value); }
        }

        /// <summary>
        /// The frequency property
        /// </summary>
        public static readonly ModelPropertyInfo<SyncTargetFrequency> FrequencyProperty =
            RegisterModelProperty<SyncTargetFrequency>(c => c.Frequency);
        public SyncTargetFrequency Frequency
        {
            get { return GetProperty<SyncTargetFrequency>(FrequencyProperty); }
            set { SetProperty<SyncTargetFrequency>(FrequencyProperty, value); }
        }

        /// <summary>
        /// The last sync property
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> LastSyncProperty =
            RegisterModelProperty<DateTime?>(c => c.LastSync);
        public DateTime? LastSync
        {
            get { return GetProperty<DateTime?>(LastSyncProperty); }
            set { SetProperty<DateTime?>(LastSyncProperty, value); }
        }

        /// <summary>
        /// The last sync error property
        /// </summary>
        public static readonly ModelPropertyInfo<SyncTargetError> ErrorProperty =
            RegisterModelProperty<SyncTargetError>(c => c.Error);
        public SyncTargetError Error
        {
            get { return GetProperty<SyncTargetError>(ErrorProperty); }
            set { SetProperty<SyncTargetError>(ErrorProperty, value); }
        }

        /// <summary>
        /// The is active property
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsActiveProperty =
            RegisterModelProperty<Boolean>(c => c.IsActive);
        public Boolean IsActive
        {
            get { return GetProperty<Boolean>(IsActiveProperty); }
            set { SetProperty<Boolean>(IsActiveProperty, value); }
        }

        /// <summary>
        /// The entity id property
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
            set { SetProperty<Int32>(EntityIdProperty, value); }
        }

        /// <summary>
        /// The retry count property
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> RetryCountProperty =
            RegisterModelProperty<Int32>(c => c.RetryCount);
        public Int32 RetryCount
        {
            get { return GetProperty<Int32>(RetryCountProperty); }
            set { SetProperty<Int32>(RetryCountProperty, value); }
        }

        /// <summary>
        /// The failed count property
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FailedCountProperty =
            RegisterModelProperty<Int32>(c => c.FailedCount);
        public Int32 FailedCount
        {
            get { return GetProperty<Int32>(FailedCountProperty); }
            set { SetProperty<Int32>(FailedCountProperty, value); }
        }

        /// <summary>
        /// The last run property
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> LastRunProperty =
            RegisterModelProperty<DateTime?>(c => c.LastRun);
        public DateTime? LastRun
        {
            get { return GetProperty<DateTime?>(LastRunProperty); }
            set { SetProperty<DateTime?>(LastRunProperty, value); }
        }

        public static readonly ModelPropertyInfo<SyncTargetGfsPerformanceList> SyncTargetGfsPerformancesProperty =
            RegisterModelProperty<SyncTargetGfsPerformanceList>(c => c.SyncTargetGfsPerformances);
        /// <summary>
        /// The list of GFS Performance Sources to be synchronised when synchronising this target
        /// </summary>
        public SyncTargetGfsPerformanceList SyncTargetGfsPerformances
        {
            get { return GetProperty<SyncTargetGfsPerformanceList>(SyncTargetGfsPerformancesProperty); }
        }

        #endregion

        #region Authorization Rules
        //this object is accessed without authentication and so cannot have rules
        #endregion

        #region Helper Property
        /// <summary>
        /// This property states whether the sync tool ui or the sync service owns the object
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> UISourceProperty =
            RegisterModelProperty<Boolean>(c => c.UISource);
        public Boolean UISource
        {
            get { return GetProperty<Boolean>(UISourceProperty); }
            set { SetProperty<Boolean>(UISourceProperty, value); }
        }
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(TargetTypeProperty));
            BusinessRules.AddRule(new Required(ConnectionStringProperty));
            BusinessRules.AddRule(new MaxLength(ConnectionStringProperty, 255));
            BusinessRules.AddRule(new Required(EntityIdProperty));
            BusinessRules.AddRule(new Required(FrequencyProperty));
            BusinessRules.AddRule(new Required(StatusProperty));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new synchornisation target
        /// </summary>
        /// <returns>A new synchirnisation target</returns>
        public static SyncTarget NewSyncTarget()
        {
            SyncTarget item = new SyncTarget();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<SyncTargetType>(TargetTypeProperty, SyncTargetType.Mssql);
            this.LoadProperty<SyncTargetFrequency>(FrequencyProperty, SyncTargetFrequency.FifteenMinutes);
            this.LoadProperty<SyncTargetStatus>(StatusProperty, SyncTargetStatus.Idle);
            this.LoadProperty<SyncTargetGfsPerformanceList>(SyncTargetGfsPerformancesProperty, SyncTargetGfsPerformanceList.NewSyncTargetGfsPerformanceList());
            base.Create();
        }

        #endregion

    }
}