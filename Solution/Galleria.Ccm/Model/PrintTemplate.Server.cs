﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created (Auto-generated)
// CCM-31200 : A.Probyn
//      Added new InsertUsingExistingContext
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    public partial class PrintTemplate
    {
        #region Constructors
        private PrintTemplate() { } //force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing PrintTemplate with the given file path
        /// </summary>
        public static PrintTemplate FetchByFilename(String fileName)
        {
            return FetchByFilename(fileName, /*asReadOnly*/false);
        }

        /// <summary>
        /// Returns an existing PrintTemplate with the given file path
        /// </summary>
        public static PrintTemplate FetchByFilename(String fileName, Boolean asReadOnly)
        {
            return DataPortal.Fetch<PrintTemplate>(new FetchByIdCriteria(PrintTemplateDalFactoryType.FileSystem, fileName, asReadOnly));
        }

        /// <summary>
        /// Returns an existing PrintTemplate with the given id
        /// </summary>
        public static PrintTemplate FetchById(Object id)
        {
            return DataPortal.Fetch<PrintTemplate>(new FetchByIdCriteria(PrintTemplateDalFactoryType.Unknown, id, /*asReadOnly*/false));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PrintTemplateDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(DescriptionProperty, dto.Description);
            this.LoadProperty<PrintTemplatePaperSizeType>(PaperSizeProperty, (PrintTemplatePaperSizeType)dto.PaperSize);
            this.LoadProperty<Boolean>(IsPlanMirroredProperty, dto.IsPlanMirrored);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<PrintTemplateSectionGroupList>(SectionGroupsProperty, PrintTemplateSectionGroupList.FetchByParentId(dalContext, dto.Id));
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private PrintTemplateDto GetDataTransferObject()
        {
            return new PrintTemplateDto
            {
                Id = ReadProperty<Object>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                Name = ReadProperty<String>(NameProperty),
                Description = ReadProperty<String>(DescriptionProperty),
                PaperSize = (Byte)ReadProperty<PrintTemplatePaperSizeType>(PaperSizeProperty),
                IsPlanMirrored = ReadProperty<Boolean>(IsPlanMirroredProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning FetchById
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            //lock the file first if required.
            if (criteria.DalType == PrintTemplateDalFactoryType.FileSystem
                && !criteria.AsReadOnly)
            {
                LockPrintTemplateByFileName((String)criteria.Id);
            }


            IDalFactory dalFactory = this.GetDalFactory(criteria.DalType);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPrintTemplateDal dal = dalContext.GetDal<IPrintTemplateDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.Id));

                    //set readonly flag
                    this.LoadProperty<Boolean>(IsReadOnlyProperty, criteria.AsReadOnly);

                    //Update the page sizes of all child objects
                    UpdateChildPageSizes();
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                PrintTemplateDto dto = GetDataTransferObject();
                Object oldId = dto.Id;
               
                using (IPrintTemplateDal dal = dalContext.GetDal<IPrintTemplateDal>())
                {
                    dal.Insert(dto);
                }

                this.LoadProperty<Object>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<PrintTemplate>(oldId, dto.Id);
                
                FieldManager.UpdateChildren(dalContext, this);
                
                dalContext.Commit();
            }
        }

        /// <summary>
        /// Called when inserting this object and its children using an existing dal context.
        /// </summary>
        ///<remarks>used to insert default by sync process.</remarks>
        internal void InsertUsingExistingContext(IDalContext dalContext)
        {
            PrintTemplateDto dto = GetDataTransferObject();
            Object oldId = dto.Id;

            using (IPrintTemplateDal dal = dalContext.GetDal<IPrintTemplateDal>())
            {
                dal.Insert(dto);
            }

            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty(RowVersionProperty, dto.RowVersion);
            this.LoadProperty(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
            dalContext.RegisterId<PrintTemplate>(oldId, dto.Id);

            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Update

        /// <summary>
        /// Called when updating this instance
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                PrintTemplateDto dto = GetDataTransferObject();
                if (this.IsSelfDirty)
                {
                    using (IPrintTemplateDal dal = dalContext.GetDal<IPrintTemplateDal>())
                    {
                        dal.Update(dto);
                    }

                    this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                    this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                }

                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when this instance is being deleted
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IPrintTemplateDal dal = dalContext.GetDal<IPrintTemplateDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #region Commands

        /// <summary>
        /// Performs the locking of a highlight
        /// </summary>
        private partial class LockPrintTemplateCommand : CommandBase<LockPrintTemplateCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.DalType, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IPrintTemplateDal dal = dalContext.GetDal<IPrintTemplateDal>())
                    {
                        dal.LockById(this.Id);
                    }
                }
            }
            #endregion

            #endregion
        }

        /// <summary>
        /// Performs the unlocking of a PrintTemplate
        /// </summary>
        private partial class UnlockPrintTemplateCommand : CommandBase<UnlockPrintTemplateCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.DalType, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IPrintTemplateDal dal = dalContext.GetDal<IPrintTemplateDal>())
                    {
                        dal.UnlockById(this.Id);
                    }
                }
            }
            #endregion

            #endregion
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the correct dal factory based on the daltype and args.
        /// </summary>
        private IDalFactory GetDalFactory(PrintTemplateDalFactoryType dalType)
        {
            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(dalType, out dalFactoryName);
            this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            this.LoadProperty<PrintTemplateDalFactoryType>(DalFactoryTypeProperty, dalType);

            return dalFactory;
        }

        /// <summary>
        /// Returns the correct dal factory for this instance
        /// </summary>
        protected override IDalFactory GetDalFactory()
        {
            if (String.IsNullOrEmpty(this.DalFactoryName))
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.DalFactoryType, out dalFactoryName);
                this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            }

            return DalContainer.GetDalFactory(this.DalFactoryName);
        }

        /// <summary>
        /// Returns the correct dal factory based on the given type and args.
        /// </summary>
        private static IDalFactory GetDalFactory(PrintTemplateDalFactoryType dalType, out String dalFactoryName)
        {
            switch (dalType)
            {
                case PrintTemplateDalFactoryType.FileSystem:
                    dalFactoryName = Constants.UserDal;
                    return DalContainer.GetDalFactory(dalFactoryName);

                default:
                    dalFactoryName = DalContainer.DalName;
                    return DalContainer.GetDalFactory();
            }
        }

        #endregion
    }
}