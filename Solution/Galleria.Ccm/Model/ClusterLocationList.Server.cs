﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class ClusterLocationList
    {
        #region Constructor
        private ClusterLocationList() { } // force use of factory methods
        #endregion

        #region Criteria
        [Serializable]
        private class FetchByClusterIdCriteria : CriteriaBase<FetchByClusterIdCriteria>
        {
            #region Properties
            /// <summary>
            /// Cluster Id
            /// </summary>
            private static readonly PropertyInfo<Int32> _clusterIdProperty = RegisterProperty<Int32>(c => c.ClusterId);
            public Int32 ClusterId
            {
                get { return ReadProperty<Int32>(_clusterIdProperty); }
            }
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="peerGroupId">The cluster id</param>
            public FetchByClusterIdCriteria(Int32 clusterId)
            {
                LoadProperty<Int32>(_clusterIdProperty, clusterId);
            }
            #endregion
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns all cluster locations for a given cluster
        /// </summary>
        /// <param name="clusterSchemeId">Cluster id</param>
        /// <returns>Collection of cluster locations</returns>
        public static ClusterLocationList FetchByClusterId(Int32 clusterId)
        {
            return DataPortal.Fetch<ClusterLocationList>(new FetchByClusterIdCriteria(clusterId));
        }

        /// <summary>
        /// Returns all cluster locations for a given cluster 
        /// </summary>
        /// <param name="dalContext">DalContext</param>
        /// <param name="clusterSchemeId">Cluster id</param>
        /// <returns>Collection of cluster locations</returns>
        internal static ClusterLocationList FetchByClusterId(IDalContext dalContext, Int32 clusterId)
        {
            return DataPortal.FetchChild<ClusterLocationList>(dalContext, new FetchByClusterIdCriteria(clusterId));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returning all cluster locations for a specific cluster
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByClusterIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IClusterLocationDal dal = dalContext.GetDal<IClusterLocationDal>())
                {
                    IEnumerable<ClusterLocationDto> dtoList = dal.FetchByClusterId(criteria.ClusterId);
                    foreach (ClusterLocationDto dto in dtoList)
                    {
                        this.Add(ClusterLocation.FetchClusterLocation(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returning all cluster locations for a specific cluster using the same IDalContext
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FetchByClusterIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            using (IClusterLocationDal dal = dalContext.GetDal<IClusterLocationDal>())
            {
                IEnumerable<ClusterLocationDto> dtoList = dal.FetchByClusterId(criteria.ClusterId);
                foreach (ClusterLocationDto dto in dtoList)
                {
                    this.Add(ClusterLocation.FetchClusterLocation(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #region Update
        /// <summary>
        /// Called when this list is being updated
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                Child_Update(dalContext);
                dalContext.Commit();
            }
        }
        #endregion

        #endregion
    }
}
