﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History : (CCM CCM800)

// CCM800-26953 : I.George
//   Initial Version

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Csla;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Enums;

namespace Galleria.Ccm.Model
{
    public sealed partial class PerformanceSelectionMetric
    {
        #region Constructor
        private PerformanceSelectionMetric() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static PerformanceSelectionMetric Fetch(IDalContext dalContext, PerformanceSelectionMetricDto dto)
        {
            return DataPortal.FetchChild<PerformanceSelectionMetric>(dalContext, dto);
        }

        #endregion

        #region DataAccess
        
        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PerformanceSelectionMetricDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32>(GFSPerformanceSourceMetricIdProperty, dto.GFSPerformanceSourceMetricId);
            this.LoadProperty<AggregationType>(AggregationTypeProperty, (AggregationType)dto.AggregationType);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private PerformanceSelectionMetricDto GetDataTransferObject(PerformanceSelection parent)
        {
            return new PerformanceSelectionMetricDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                PerformanceSelectionId = parent.Id,
                GFSPerformanceSourceMetricId = ReadProperty<Int32>(GFSPerformanceSourceMetricIdProperty),
                AggregationType = (Byte)this.ReadProperty<AggregationType>(AggregationTypeProperty)
            };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, PerformanceSelectionMetricDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, PerformanceSelection parent)
        {
            PerformanceSelectionMetricDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IPerformanceSelectionMetricDal dal = dalContext.GetDal<IPerformanceSelectionMetricDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<PerformanceSelectionMetric>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, PerformanceSelection parent)
        {
            if (this.IsSelfDirty)
            {
                using (IPerformanceSelectionMetricDal dal = dalContext.GetDal<IPerformanceSelectionMetricDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IPerformanceSelectionMetricDal dal = dalContext.GetDal<IPerformanceSelectionMetricDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion
    }
}
