﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History : (CCM CCM800)

// CCM800-26953 : I.George
//   Initial Version

#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of performanceSelectionMetric Objects
    /// </summary>
    [Serializable]
    public sealed partial class PerformanceSelectionMetricList : ModelList<PerformanceSelectionMetricList, PerformanceSelectionMetric>
    {
        #region Authorization
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PerformanceSelectionMetricList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.PerformanceSelectionGet.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelectionMetricList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.PerformanceSelectionCreate.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelectionMetricList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.PerformanceSelectionEdit.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelectionMetricList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.PerformanceSelectionDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PerformanceSelectionMetricList NewPerformanceSelectionMetricList()
        {
            PerformanceSelectionMetricList item = new PerformanceSelectionMetricList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion
        #endregion
    }
}
