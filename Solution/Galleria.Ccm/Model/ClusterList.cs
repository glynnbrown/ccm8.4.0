﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629: A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Model;
using Galleria.Ccm.Security;
using Csla;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class ClusterList : ModelList<ClusterList, Cluster>
    {
        #region Authorisation Rules
        /// <summary>
        /// Defines the authorisation rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ClusterList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ClusterSchemeCreate.ToString()));
            BusinessRules.AddRule(typeof(ClusterList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ClusterSchemeGet.ToString()));
            BusinessRules.AddRule(typeof(ClusterList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ClusterSchemeEdit.ToString()));
            BusinessRules.AddRule(typeof(ClusterList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ClusterSchemeDelete.ToString()));
        }

        #endregion

        #region Criteria

        [Serializable]
        internal class FetchByClusterSchemeIdCriteria : CriteriaBase<FetchByClusterSchemeIdCriteria>
        {
            #region Properties
            /// <summary>
            /// Cluster Scheme Id
            /// </summary>
            private static readonly PropertyInfo<Int32> _clusterSchemeIdProperty = RegisterProperty<Int32>(c => c.ClusterSchemeId);
            public Int32 ClusterSchemeId
            {
                get { return ReadProperty<Int32>(_clusterSchemeIdProperty); }
            }
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="peerGroupId">The cluster scheme id</param>
            public FetchByClusterSchemeIdCriteria(Int32 clusterSchemeId)
            {
                LoadProperty<Int32>(_clusterSchemeIdProperty, clusterSchemeId);
            }
            #endregion
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new cluster list
        /// </summary>
        /// <returns>A new cluster list</returns>
        internal static ClusterList NewClusterList()
        {
            ClusterList item = new ClusterList();
            item.Create();
            return item;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Called whenever a new item is added through data-binding
        /// </summary>
        /// <returns></returns>
        protected override Cluster AddNewCore()
        {
            Cluster newItem = Cluster.NewCluster();
            this.Add(newItem);
            return newItem;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private new void Create()
        {
            MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

    }
}
