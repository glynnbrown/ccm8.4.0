﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// CCM-25587 : L.Ineson
//	Created (Auto-generated)
// CCM-25608 : N.Foster
//  Added associate planogram command
// V8-27964 : A.Silva
//      Added ProductGroupId.
// V8-27966 : A.Silva
//      Added InsertPlanogramGroup and UpdatePlanogramGroup Commands.
// V8-28334 : A.Silva
//      Added DeletePlanogramGroup Command.

#endregion
#region Version History : CCM 802
// V8-27433 : A.Kuszyk
//  Added GetFriendlyFullPath().
// V8-29230 : N.Foster
//  Added overload for AssociatePlanograms
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// PlanogramGroup Model object
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramGroup : ModelObject<PlanogramGroup>
    {
        #region Static Constructor
        static PlanogramGroup()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns the parent group of this
        /// </summary>    
        public PlanogramGroup ParentGroup
        {
            get
            {
                PlanogramGroupList list = base.Parent as PlanogramGroupList;
                if (list != null)
                {
                    return list.Parent as PlanogramGroup;
                }
                return null;
            }
        }

        /// <summary>
        /// Returns the parent hierarchy of this
        /// </summary>
        public PlanogramHierarchy ParentHierarchy
        {
            get
            {
                PlanogramHierarchy parentHierarchy = base.Parent as PlanogramHierarchy;
                if (parentHierarchy == null)
                {
                    PlanogramGroup currentParent = this.ParentGroup;
                    while (currentParent != null)
                    {
                        if (currentParent.ParentGroup != null)
                        {
                            currentParent = currentParent.ParentGroup;
                        }
                        else
                        {
                            return currentParent.Parent as PlanogramHierarchy;
                        }
                    }
                }
                else
                {
                    return parentHierarchy;
                }

                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region DateCreated

        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        /// Gets/Sets the DateCreated value
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
            set { SetProperty<DateTime>(DateCreatedProperty, value); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        /// Gets/Sets the DateLastModified value
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
            set { SetProperty<DateTime>(DateLastModifiedProperty, value); }
        }

        #endregion

        #region DateDeleted

        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);

        /// <summary>
        /// Gets/Sets the DateDeleted value
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
            set { SetProperty<DateTime?>(DateDeletedProperty, value); }
        }

        #endregion

        #region ChildList

        public static readonly ModelPropertyInfo<PlanogramGroupList> ChildListProperty =
        RegisterModelProperty<PlanogramGroupList>(c => c.ChildList);
        /// <summary>
        /// The list of child product groups
        /// </summary>
        public PlanogramGroupList ChildList
        {
            get
            {
                return GetProperty<PlanogramGroupList>(ChildListProperty);
            }
        }

        #endregion

        #region ProductGroupId

        /// <summary>
        ///		Metadata for the <see cref="ProductGroupId"/> property.
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> ProductGroupIdProperty =
            RegisterModelProperty<Int32?>(o => o.ProductGroupId, "Merchandising Group");

        /// <summary>
        ///		Gets or sets the Id for the product group that this planogram group is lonked to, if any.
        /// </summary>
        public Int32? ProductGroupId
        {
            get { return this.GetProperty<Int32?>(ProductGroupIdProperty); }
            set { this.SetProperty<Int32?>(ProductGroupIdProperty, value); }
        }

        #endregion

        #region Code

        /// <summary>
        ///		Metadata for the <see cref="ProductGroupId"/> property.
        /// </summary>
        public static readonly ModelPropertyInfo<Guid> CodeProperty =
            RegisterModelProperty<Guid>(o => o.Code, "Code");

        /// <summary>
        ///		Gets the unique code for the planogram group - only used for the sync to the cloud
        /// </summary>
        public Guid Code
        {
            get { return this.GetProperty<Guid>(CodeProperty); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PlanogramGroup), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(PlanogramGroup), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.PlanogramHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(PlanogramGroup), new IsInRole(AuthorizationActions.EditObject, DomainPermission.PlanogramHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(PlanogramGroup), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.PlanogramHierarchyEdit.ToString()));
        }
        #endregion

        #region Factory Methods

        #region NewPlanogramGroup
        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static PlanogramGroup NewPlanogramGroup()
        {
            PlanogramGroup item = new PlanogramGroup();
            item.Create();
            return item;
        }
        #endregion

        #region AssociatePlanograms
        /// <summary>
        /// Associates a set of planograms to a product group
        /// </summary>
        public static void AssociatePlanograms(PlanogramGroup planogramGroup, PlanogramInfoList planograms)
        {
            AssociatePlanograms(planogramGroup.Id, planograms.Select(item => item.Id));
        }

        /// <summary>
        /// Associates a set of planograms to a product group
        /// </summary>
        public static void AssociatePlanograms(PlanogramGroup planogramGroup, IEnumerable<Int32> planogramIds)
        {
            AssociatePlanograms(planogramGroup.Id, planogramIds);
        }

        /// <summary>
        /// Associates a set of planograms to a product group
        /// </summary>
        public static void AssociatePlanograms(PlanogramGroup planogramGroup, IEnumerable<Planogram> planograms)
        {
            AssociatePlanograms(planogramGroup.Id, planograms.Select(item => (Int32)item.Id));
        }

        /// <summary>
        /// Associates a set of planograms to a product group
        /// </summary>
        public static void AssociatePlanograms(Int32 planogramGroupId, IEnumerable<Int32> planogramIds)
        {
            DataPortal.Execute<AssociatePlanogramsCommand>(new AssociatePlanogramsCommand(planogramGroupId, planogramIds));
        }
        #endregion

        #region InsertPlanogramGroup

        /// <summary>
        ///     Directly inserts a new <see cref="PlanogramGroup"/> into the data repository.
        /// </summary>
        /// <param name="planogramHierarchyId"></param>
        /// <param name="parentPlanogramGroupId">Id of the parent planogram group.</param>
        /// <param name="name">Name of the new planogram group to insert</param>
        /// <param name="productGroupId"></param>
        public static Int32 InsertPlanogramGroup(Int32 planogramHierarchyId, Int32 parentPlanogramGroupId, String name, Int32? productGroupId)
        {
            InsertPlanogramGroupCommand command = new InsertPlanogramGroupCommand(planogramHierarchyId, parentPlanogramGroupId, name, productGroupId);
            DataPortal.Execute<InsertPlanogramGroupCommand>(command);
            return command.Id;
        }

        #endregion

        #region UpdatePlanogramGroup

        /// <summary>
        ///     Directly update the <see cref="PlanogramGroup"/> in the data repository.
        /// </summary>
        /// <param name="planogramGroupId"></param>
        /// <param name="planogramHierarchyId"></param>
        /// <param name="parentPlanogramGroupId"></param>
        /// <param name="name"></param>
        /// <param name="productGroupId"></param>
        /// <param name="dateDeleted"></param>
        public static void UpdatePlanogramGroup(Int32 planogramGroupId, Int32 planogramHierarchyId, Int32 parentPlanogramGroupId, String name, Int32? productGroupId)
        {
            DataPortal.Execute<UpdatePlanogramGroupCommand>(new UpdatePlanogramGroupCommand(planogramGroupId, planogramHierarchyId, parentPlanogramGroupId, name, productGroupId));
        }

        #endregion

        #region DeletePlanogramGroup

        /// <summary>
        ///     Directly delete the <see cref="PlanogramGroup"/> in the data repository.
        /// </summary>
        /// <param name="planogramGroupId"></param>
        public static void DeletePlanogramGroup(Int32 planogramGroupId)
        {
            DataPortal.Execute<DeletePlanogramGroupCommand>(new DeletePlanogramGroupCommand(planogramGroupId));
        }

        #endregion

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            this.LoadProperty<PlanogramGroupList>(ChildListProperty, PlanogramGroupList.NewPlanogramGroupList());
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Commands

        #region AssociatePlanograms
        /// <summary>
        /// Associates a set of planograms with a product group
        /// </summary>
        [Serializable]
        private partial class AssociatePlanogramsCommand : CommandBase<AssociatePlanogramsCommand>
        {
            #region Authorization Rules
            /// <summary>
            /// Defines the authorization rules for this type
            /// </summary>
            private static void AddObjectAuthorizationRules()
            {
                BusinessRules.AddRule(typeof(AssociatePlanogramsCommand), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(AssociatePlanogramsCommand), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(AssociatePlanogramsCommand), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
                BusinessRules.AddRule(typeof(AssociatePlanogramsCommand), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
            }
            #endregion

            #region Properties

            #region PlanogramGroupId
            /// <summary>
            /// PlanogramGroupId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> PlanogramGroupIdProperty =
                RegisterProperty<Int32>(c => c.PlanogramGroupId);
            /// <summary>
            /// Returns the planogram group id
            /// </summary>
            public Int32 PlanogramGroupId
            {
                get { return this.ReadProperty<Int32>(PlanogramGroupIdProperty); }
            }
            #endregion

            #region PlanogramIds
            /// <summary>
            /// PlanogramIds property definition
            /// </summary>
            public static readonly PropertyInfo<List<Int32>> PlanogramIdsProperty =
                RegisterProperty<List<Int32>>(c => c.PlanogramIds);
            /// <summary>
            /// The list of planograms to associate
            /// </summary>
            public List<Int32> PlanogramIds
            {
                get { return this.ReadProperty<List<Int32>>(PlanogramIdsProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public AssociatePlanogramsCommand(Int32 planogramGroupId, IEnumerable<Int32> planogramIds)
            {
                this.LoadProperty<Int32>(PlanogramGroupIdProperty, planogramGroupId);
                this.LoadProperty<List<Int32>>(PlanogramIdsProperty, planogramIds.ToList());
            }
            #endregion
        }
        #endregion

        #region InsertPlanogramGroup
        /// <summary>
        /// Associates a set of planograms with a product group
        /// </summary>
        [Serializable]
        private partial class InsertPlanogramGroupCommand : CommandBase<InsertPlanogramGroupCommand>
        {
            #region Authorization Rules
            /// <summary>
            /// Defines the authorization rules for this type
            /// </summary>
            private static void AddObjectAuthorizationRules()
            {
                BusinessRules.AddRule(typeof(InsertPlanogramGroupCommand), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(InsertPlanogramGroupCommand), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(InsertPlanogramGroupCommand), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
                BusinessRules.AddRule(typeof(InsertPlanogramGroupCommand), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
            }
            #endregion

            #region Properties

            #region Id

            /// <summary>
            ///		Metadata for the <see cref="Id"/> property.
            /// </summary>
            private static readonly PropertyInfo<Int32> IdProperty =
                RegisterProperty<Int32>(o => o.Id);

            /// <summary>
            ///		Gets or sets the value for the <see cref="Id"/> property.
            /// </summary>
            public Int32 Id
            {
                get { return ReadProperty(IdProperty); }
            }

            #endregion

            #region PlanogramHierarchyId
            /// <summary>
            /// PlanogramHierarchyId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> PlanogramHierarchyIdProperty =
                RegisterProperty<Int32>(c => c.PlanogramHierarchyId);
            /// <summary>
            /// Returns the parent planogram hierarchy id
            /// </summary>
            public Int32 PlanogramHierarchyId
            {
                get { return this.ReadProperty<Int32>(PlanogramHierarchyIdProperty); }
            }
            #endregion

            #region ParentPlanogramGroupId
            /// <summary>
            /// ParentPlanogramGroupId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> ParentPlanogramGroupIdProperty =
                RegisterProperty<Int32>(c => c.ParentPlanogramGroupId);
            /// <summary>
            /// Returns the parent planogram group id
            /// </summary>
            public Int32 ParentPlanogramGroupId
            {
                get { return this.ReadProperty<Int32>(ParentPlanogramGroupIdProperty); }
            }
            #endregion

            #region Name
            /// <summary>
            /// Name property definition
            /// </summary>
            public static readonly PropertyInfo<String> NameProperty =
                RegisterProperty<String>(c => c.Name);
            /// <summary>
            /// The name of the planogram group.
            /// </summary>
            public String Name
            {
                get { return this.ReadProperty<String>(NameProperty); }
            }
            #endregion

            #region ProductGroupId
            /// <summary>
            /// ProductGroupId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32?> ProductGroupIdProperty =
                RegisterProperty<Int32?>(c => c.ProductGroupId);
            /// <summary>
            /// Returns the planogram group related prodcut group id
            /// </summary>
            public Int32? ProductGroupId
            {
                get { return this.ReadProperty<Int32?>(ProductGroupIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public InsertPlanogramGroupCommand(Int32 planogramHierarchyId, Int32 parentPlanogramGroupId, String name, Int32? productGroupId)
            {
                this.LoadProperty<Int32>(PlanogramHierarchyIdProperty, planogramHierarchyId);
                this.LoadProperty<Int32>(ParentPlanogramGroupIdProperty, parentPlanogramGroupId);
                this.LoadProperty<String>(NameProperty, name);
                this.LoadProperty<Int32?>(ProductGroupIdProperty, productGroupId);
            }
            #endregion
        }
        #endregion

        #region UpdatePlanogramGroup
        /// <summary>
        ///     Directly updates a <see cref="PlanogramGroup"/> in the data repository.
        /// </summary>
        [Serializable]
        private partial class UpdatePlanogramGroupCommand : CommandBase<UpdatePlanogramGroupCommand>
        {
            #region Authorization Rules
            /// <summary>
            /// Defines the authorization rules for this type
            /// </summary>
            private static void AddObjectAuthorizationRules()
            {
                BusinessRules.AddRule(typeof(UpdatePlanogramGroupCommand), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(UpdatePlanogramGroupCommand), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(UpdatePlanogramGroupCommand), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
                BusinessRules.AddRule(typeof(UpdatePlanogramGroupCommand), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
            }
            #endregion

            #region Properties

            #region PlanogramGroupId
            /// <summary>
            /// PlanogramGroupId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> PlanogramGroupIdProperty =
                RegisterProperty<Int32>(c => c.PlanogramGroupId);
            /// <summary>
            /// Returns the planogram group id
            /// </summary>
            public Int32 PlanogramGroupId
            {
                get { return this.ReadProperty<Int32>(PlanogramGroupIdProperty); }
            }
            #endregion

            #region PlanogramHierarchyId
            /// <summary>
            /// PlanogramHierarchyId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> PlanogramHierarchyIdProperty =
                RegisterProperty<Int32>(c => c.PlanogramHierarchyId);
            /// <summary>
            /// Returns the parent planogram hierarchy id
            /// </summary>
            public Int32 PlanogramHierarchyId
            {
                get { return this.ReadProperty<Int32>(PlanogramHierarchyIdProperty); }
            }
            #endregion

            #region ParentPlanogramGroupId
            /// <summary>
            /// ParentPlanogramGroupId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> ParentPlanogramGroupIdProperty =
                RegisterProperty<Int32>(c => c.ParentPlanogramGroupId);
            /// <summary>
            /// Returns the parent planogram group id
            /// </summary>
            public Int32 ParentPlanogramGroupId
            {
                get { return this.ReadProperty<Int32>(ParentPlanogramGroupIdProperty); }
            }
            #endregion

            #region Name
            /// <summary>
            /// Name property definition
            /// </summary>
            public static readonly PropertyInfo<String> NameProperty =
                RegisterProperty<String>(c => c.Name);
            /// <summary>
            /// The name of the planogram group.
            /// </summary>
            public String Name
            {
                get { return this.ReadProperty<String>(NameProperty); }
            }
            #endregion

            #region ProductGroupId
            /// <summary>
            /// ProductGroupId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32?> ProductGroupIdProperty =
                RegisterProperty<Int32?>(c => c.ProductGroupId);
            /// <summary>
            /// Returns the planogram group related prodcut group id
            /// </summary>
            public Int32? ProductGroupId
            {
                get { return this.ReadProperty<Int32?>(ProductGroupIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public UpdatePlanogramGroupCommand(Int32 planogramGroupId, Int32 planogramHierarchyId, Int32 parentPlanogramGroupId, String name, Int32? productGroupId)
            {
                this.LoadProperty<Int32>(PlanogramGroupIdProperty, planogramGroupId);
                this.LoadProperty<Int32>(PlanogramHierarchyIdProperty, planogramHierarchyId);
                this.LoadProperty<Int32>(ParentPlanogramGroupIdProperty, parentPlanogramGroupId);
                this.LoadProperty<String>(NameProperty, name);
                this.LoadProperty<Int32?>(ProductGroupIdProperty, productGroupId);
            }
            #endregion
        }
        #endregion

        #region DeletePlanogramGroup
        /// <summary>
        ///     Directly deletes a <see cref="PlanogramGroup"/> in the data repository.
        /// </summary>
        [Serializable]
        private partial class DeletePlanogramGroupCommand : CommandBase<DeletePlanogramGroupCommand>
        {
            #region Authorization Rules
            /// <summary>
            /// Defines the authorization rules for this type
            /// </summary>
            private static void AddObjectAuthorizationRules()
            {
                BusinessRules.AddRule(typeof(DeletePlanogramGroupCommand), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(DeletePlanogramGroupCommand), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(DeletePlanogramGroupCommand), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
                BusinessRules.AddRule(typeof(DeletePlanogramGroupCommand), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
            }
            #endregion

            #region Properties

            #region PlanogramGroupId
            /// <summary>
            /// PlanogramGroupId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> PlanogramGroupIdProperty =
                RegisterProperty<Int32>(c => c.PlanogramGroupId);
            /// <summary>
            /// Returns the planogram group id
            /// </summary>
            public Int32 PlanogramGroupId
            {
                get { return this.ReadProperty<Int32>(PlanogramGroupIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public DeletePlanogramGroupCommand(Int32 planogramGroupId)
            {
                this.LoadProperty<Int32>(PlanogramGroupIdProperty, planogramGroupId);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns a String describing the hierarchial path of this Planogram Group.
        /// </summary>
        public String GetFriendlyFullPath()
        {
            StringBuilder fullPathBuilder = new StringBuilder();
            foreach(PlanogramGroup group in FetchFullPath())
            {
                fullPathBuilder.AppendFormat(@"{0}\", group.Name);
            }
            return fullPathBuilder.ToString();
        }

        /// <summary>
        /// Associates a set of planograms with this product group
        /// </summary>
        public void AssociatePlanograms(PlanogramInfoList planograms)
        {
            AssociatePlanograms(this, planograms);
        }

        /// <summary>
        /// Associates a set of planograms with this product group
        /// </summary>
        /// <param name="planogramIds"></param>
        public void AssociatePlanograms(IEnumerable<Int32> planogramIds)
        {
            AssociatePlanograms(this, planogramIds);
        }

        /// <summary>
        /// Associates a set of planograms with this product group
        /// </summary>
        /// <param name="planograms">The planograms to associate</param>
        public void AssociatePlanograms(IEnumerable<Planogram> planograms)
        {
            AssociatePlanograms(this, planograms);
        }

        /// <summary>
        ///     Directly inserts this instance as a new <see cref="PlanogramGroup"/> into the data repository.
        /// </summary>
        public void InsertPlanogramGroup()
        {
            Int32 planogramGroupId = InsertPlanogramGroup(this.ParentHierarchy.Id, this.ParentGroup.Id, this.Name, this.ProductGroupId);
            this.Id = planogramGroupId;
            foreach (PlanogramGroup childGroup in ChildList)
            {
                childGroup.InsertPlanogramGroup();
            }
        }

        /// <summary>
        ///     Directly update this instance in the data repository.
        /// </summary>
        public void UpdatePlanogramGroup()
        {
            UpdatePlanogramGroup(this.Id, this.ParentHierarchy.Id, this.ParentGroup.Id, this.Name, this.ProductGroupId);
        }

        /// <summary>
        ///     Directly delete this instance in the data repository.
        /// </summary>
        public void DeletePlanogramGroup()
        {
            DeletePlanogramGroup(this.Id);
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<PlanogramGroup>(oldId, newId);
        }

        /// <summary>
        /// ToString override
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Enumerates through this group and  all child groups below
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PlanogramGroup> EnumerateAllChildGroups()
        {
            yield return this;

            foreach (PlanogramGroup child in this.ChildList)
            {
                foreach (PlanogramGroup g in child.EnumerateAllChildGroups())
                {
                    yield return g;
                }
            }
        }

        /// <summary>
        /// Creates a list of groups representing a path down to the requested child
        /// </summary>
        /// <returns></returns>
        public List<PlanogramGroup> FetchParentPath()
        {
            List<PlanogramGroup> inversePathList = new List<PlanogramGroup>();
            PlanogramGroup currentParent = this.ParentGroup;

            while (currentParent != null)
            {
                inversePathList.Add(currentParent);
                currentParent = currentParent.ParentGroup;
            }

            //invert the list so the top parent is the first item
            List<PlanogramGroup> pathList = new List<PlanogramGroup>();
            int currentIndex = inversePathList.Count - 1;
            while (currentIndex >= 0)
            {
                pathList.Add(inversePathList[currentIndex]);
                currentIndex--;
            }

            return pathList;
        }

        /// <summary>
        /// Returns the full path to this group including the group itself.
        /// </summary>
        /// <returns></returns>
        public List<PlanogramGroup> FetchFullPath()
        {
            List<PlanogramGroup> path = FetchParentPath();
            path.Add(this);
            return path;
        }

        /// <summary>
        /// Returns a count of how many levels deep
        /// this group resides.
        /// </summary>
        /// <returns></returns>
        public Int32 LevelNo()
        {
            Int32 levelCount = 1;

            PlanogramGroup currentParent = this.ParentGroup;
            while (currentParent != null)
            {
                currentParent = currentParent.ParentGroup;
                levelCount++;
            }
            return levelCount;
        }

        #endregion
    }
}