﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-27804 : L.Ineson
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model info representing a field used by a planogram import template
    /// </summary>
    [Serializable]
    public sealed class PlanogramImportTemplateFieldInfo : ModelReadOnlyObject<PlanogramImportTemplateFieldInfo>
    {
        #region Properties

        #region Field
        /// <summary>
        /// Field property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> FieldProperty =
            RegisterModelProperty<String>(c => c.Field);
        /// <summary>
        /// Gets/Sets the ccm field
        /// </summary>
        public String Field
        {
            get { return this.GetProperty<String>(FieldProperty); }
        }
        #endregion

        #region DisplayName
        /// <summary>
        /// DisplayName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DisplayNameProperty =
            RegisterModelProperty<String>(c => c.DisplayName);
        /// <summary>
        /// Gets/Sets the friendly name to use when displaying the field.
        /// </summary>
        public String DisplayName
        {
            get { return this.GetProperty<String>(DisplayNameProperty); }
        }
        #endregion

        #region FieldType
        /// <summary>
        /// FieldType property definition.
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramFieldMappingType> FieldTypeProperty =
            RegisterModelProperty<PlanogramFieldMappingType>(c => c.FieldType);

        /// <summary>
        /// Gets/Sets the FieldType of field this mapping is for
        /// </summary>
        public PlanogramFieldMappingType FieldType
        {
            get { return this.GetProperty<PlanogramFieldMappingType>(FieldTypeProperty); }
        }
        #endregion

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Constructor
        private PlanogramImportTemplateFieldInfo() { }// force use of factory methods.
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type.
        /// </summary>
        public static PlanogramImportTemplateFieldInfo NewPlanogramImportTemplateFieldInfo(
            String field, String displayName, PlanogramFieldMappingType fieldType)
        {
            PlanogramImportTemplateFieldInfo item = new PlanogramImportTemplateFieldInfo();
            item.Create(field, displayName, fieldType);
            return item;
        }

        /// <summary>
        /// Creates a new object of this type.
        /// </summary>
        public static PlanogramImportTemplateFieldInfo NewPlanogramImportTemplateFieldInfo(
            ObjectFieldInfo fieldInfo, PlanogramFieldMappingType fieldType)
        {
            PlanogramImportTemplateFieldInfo item = new PlanogramImportTemplateFieldInfo();
            item.Create(fieldInfo.PropertyName, fieldInfo.FieldFriendlyName, fieldType);
            return item;
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Called when a new item is created.
        /// </summary>
        private void Create(String field, String displayName, PlanogramFieldMappingType fieldType)
        {
            this.LoadProperty<String>(FieldProperty, field);
            this.LoadProperty<String>(DisplayNameProperty, displayName);
            this.LoadProperty<PlanogramFieldMappingType>(FieldTypeProperty, fieldType);
        }


        #endregion
    }
}
