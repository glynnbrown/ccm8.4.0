﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Object used to return the current
    /// status of the synchronisation service
    /// </summary>
    [Serializable]
    public sealed partial class SyncStatus : ModelReadOnlyObject<SyncStatus>
    {
        #region Authorization Rules
        //this object is accessed without authentication and so cannot have rules
        #endregion

        #region Properties
        /// <summary>
        /// The service status property
        /// </summary>
        public static readonly ModelPropertyInfo<SyncServiceStatus> ServiceStatusProperty =
            RegisterModelProperty<SyncServiceStatus>(c => c.ServiceStatus);
        public SyncServiceStatus ServiceStatus
        {
            get { return GetProperty<SyncServiceStatus>(ServiceStatusProperty); }
        }

        /// <summary>
        /// The service status description property
        /// </summary>
        public static readonly ModelPropertyInfo<String> ServiceStatusDescriptionProperty =
            RegisterModelProperty<String>(c => c.ServiceStatusDescription);
        public String ServiceStatusDescription
        {
            get { return GetProperty<String>(ServiceStatusDescriptionProperty); }
        }
        #endregion
    }
}