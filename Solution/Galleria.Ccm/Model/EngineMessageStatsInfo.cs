﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-27411 : M.Pettit
//  Created
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class EngineMessageStatsInfo : ModelReadOnlyObject<EngineMessageStatsInfo>
    {
        #region Properties

        /// <summary>
        /// MessageCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> MessageCountProperty =
            RegisterModelProperty<Int32>(c => c.MessageCount);
        /// <summary>
        /// Returns the number of messages in the queue
        /// </summary>
        public Int32 MessageCount
        {
            get { return this.ReadProperty<Int32>(MessageCountProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> MessageProcessCountProperty =
            RegisterModelProperty<Int32>(c => c.MessageProcessCount);
        /// <summary>
        /// Returns the number of processed messages in the queue
        /// </summary>
        public Int32 MessageProcessCount
        {
            get { return GetProperty<Int32>(MessageProcessCountProperty); }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(EngineMessageStatsInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(EngineMessageStatsInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(EngineMessageStatsInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(EngineMessageStatsInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
