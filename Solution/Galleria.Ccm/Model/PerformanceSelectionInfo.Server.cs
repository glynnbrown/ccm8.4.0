﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26159 : L.Ineson
//      Created
#endregion
#region Version History: (CCM 8.3.0)
// V8-31830 : A.Probyn
//  Updated to include GFSPerformanceSourceId, SelectionType, TimeType, DynamicValue
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class PerformanceSelectionInfo
    {
        #region Constructor
        private PerformanceSelectionInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns from a dto
        /// </summary>
        internal static PerformanceSelectionInfo GetPerformanceSelectionInfo(IDalContext dalContext, PerformanceSelectionInfoDto dto)
        {
            return DataPortal.FetchChild<PerformanceSelectionInfo>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, PerformanceSelectionInfoDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<Int32>(GFSPerformanceSourceIdProperty, dto.GFSPerformanceSourceId);
            LoadProperty<PerformanceSelectionTimeType>(TimeTypeProperty, (PerformanceSelectionTimeType)dto.TimeType);
            LoadProperty<PerformanceSelectionType>(SelectionTypeProperty, (PerformanceSelectionType)dto.SelectionType);
            LoadProperty<Int32>(DynamicValueProperty, dto.DynamicValue);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, PerformanceSelectionInfoDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #endregion
    }
}
