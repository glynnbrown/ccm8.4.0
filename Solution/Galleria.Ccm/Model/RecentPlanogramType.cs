﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-V8-24261 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Enum denoting the available planogram types
    /// </summary>
    public enum RecentPlanogramType
    {
        Unknown = 0,
        Pog = 1,
        Repository = 2,
    }

}
