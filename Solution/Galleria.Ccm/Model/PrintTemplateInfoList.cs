﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//	Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of PrintTemplateInfo objects.
    /// </summary>
    [Serializable]
    public sealed partial class PrintTemplateInfoList : ModelReadOnlyList<PrintTemplateInfoList, PrintTemplateInfo>
    {
        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Criteria

        #region FetchByEntityIdCriteria
        /// <summary>
        /// Criteria for FetchByEntityId
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : Csla.CriteriaBase<FetchByEntityIdCriteria>
        {
            #region Properties

            #region DalType
            /// <summary>
            /// DalType property definition
            /// </summary>
            public static readonly PropertyInfo<PrintTemplate.PrintTemplateDalFactoryType> DalTypeProperty =
                RegisterProperty<PrintTemplate.PrintTemplateDalFactoryType>(c => c.DalType);
            /// <summary>
            /// Returns the dal type
            /// </summary>
            public PrintTemplate.PrintTemplateDalFactoryType DalType
            {
                get { return this.ReadProperty<PrintTemplate.PrintTemplateDalFactoryType>(DalTypeProperty); }
            }
            #endregion

            #region EntityId

            /// <summary>
            /// EntityId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
            RegisterProperty<Int32>(c => c.EntityId);
            /// <summary>
            /// Returns the entity id to fetch for.
            /// </summary>
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty<PrintTemplate.PrintTemplateDalFactoryType>(DalTypeProperty, PrintTemplate.PrintTemplateDalFactoryType.Unknown);
                LoadProperty<Int32>(EntityIdProperty, entityId);
            }
            #endregion
        }
        #endregion

        #region FetchByIdsCriteria

        /// <summary>
        /// Criteria for the Fetch factory method
        /// </summary>
        [Serializable]
        public class FetchByIdsCriteria : CriteriaBase<FetchByIdsCriteria>
        {
            #region Properties

            #region DalType
            /// <summary>
            /// DalType property definition
            /// </summary>
            public static readonly PropertyInfo<PrintTemplate.PrintTemplateDalFactoryType> DalTypeProperty =
                RegisterProperty<PrintTemplate.PrintTemplateDalFactoryType>(c => c.DalType);
            /// <summary>
            /// Returns the dal type
            /// </summary>
            public PrintTemplate.PrintTemplateDalFactoryType DalType
            {
                get { return this.ReadProperty<PrintTemplate.PrintTemplateDalFactoryType>(DalTypeProperty); }
            }
            #endregion

            #region Ids property

            /// <summary>
            /// Ids property definition
            /// </summary>
            public static readonly PropertyInfo<List<Object>> IdsProperty =
                RegisterProperty<List<Object>>(c => c.Ids);

            public List<Object> Ids
            {
                get { return this.ReadProperty<List<Object>>(IdsProperty); }
            }

            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdsCriteria(PrintTemplate.PrintTemplateDalFactoryType sourceType, List<Object> ids)
            {
                this.LoadProperty<PrintTemplate.PrintTemplateDalFactoryType>(DalTypeProperty, sourceType);
                this.LoadProperty<List<Object>>(IdsProperty, ids);
            }
            #endregion
        }

        #endregion

        #endregion
    }

}