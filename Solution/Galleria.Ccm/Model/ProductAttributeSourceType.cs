﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM801
// V8-27494 : A.Kuszyk
//  Created
#endregion
#region Version History: CCM802
// V8-29030 : J.Pickup
//  Added ProductPerformanceData
#endregion
#region Version History : CCM820
// V8-31246 : A.Kuszyk
//  Added helper class to get enum from field info.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Denotes different sources of product attribute data.
    /// </summary>
    public enum ProductAttributeSourceType
    {
        /// <summary>
        /// Attribute data sourcing from the Product itself.
        /// </summary>
        ProductAttributes = 0,
        /// <summary>
        /// Attribute data sourcing from the Product's Custom Attribute Data.
        /// </summary>
        ProductCustomAttributes = 1,
        /// <summary>
        /// Attribute data sourcing from the Product's associated Performance Data.
        /// </summary>
        ProductPerformanceData = 2
    }

    /// <summary>
    /// A helper class for the <see cref="ProductAttributeSourceType"/> enum.
    /// </summary>
    public static class ProductAttributeSourceTypeHelper
    {
        /// <summary>
        /// Gets a <see cref="ProductAttributeSourceType"/> that represents the source of the given <paramref name="fieldInfo"/>
        /// by evaluating the Group Name of the field.
        /// </summary>
        /// <param name="fieldInfo"></param>
        /// <returns></returns>
        public static ProductAttributeSourceType GetSourceType(ObjectFieldInfo fieldInfo)
        {
            if (fieldInfo.GroupName.Equals(Galleria.Framework.Planograms.Resources.Language.Message.PlanogramProduct_PropertyGroup_Custom))
            {
                return ProductAttributeSourceType.ProductCustomAttributes;
            }
            else if (fieldInfo.GroupName.Equals(Galleria.Framework.Planograms.Resources.Language.Message.PlanogramProduct_PropertyGroup_Performance))
            {
                return ProductAttributeSourceType.ProductPerformanceData;
            }
            else
            {
                return ProductAttributeSourceType.ProductAttributes;
            }
        }
    }
}
