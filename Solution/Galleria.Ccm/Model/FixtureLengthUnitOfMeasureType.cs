﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Denotes the available length unit of measure types
    /// </summary>
    public enum FixtureLengthUnitOfMeasureType
    {
        Unknown = 0,
        Centimeters = 1,
        Inches = 2
    }

    /// <summary>
    /// FixtureLengthUnitOfMeasureType Helper Class
    /// </summary>
    public static class FixtureLengthUnitOfMeasureTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<FixtureLengthUnitOfMeasureType, String> FriendlyNames =
            new Dictionary<FixtureLengthUnitOfMeasureType, String>()
            {
                {FixtureLengthUnitOfMeasureType.Unknown, String.Empty},
                {FixtureLengthUnitOfMeasureType.Centimeters, Message.Enum_FixtureLengthUnitOfMeasureType_Centimeters},
                {FixtureLengthUnitOfMeasureType.Inches, Message.Enum_FixtureAnnotationType_Inches},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<FixtureLengthUnitOfMeasureType, String> Abbreviations =
            new Dictionary<FixtureLengthUnitOfMeasureType, String>()
            {
                {FixtureLengthUnitOfMeasureType.Unknown, String.Empty},
                {FixtureLengthUnitOfMeasureType.Centimeters, Message.Enum_FixtureLengthUnitOfMeasureType_Centimeters_Abbrev},
                {FixtureLengthUnitOfMeasureType.Inches, Message.Enum_FixtureAnnotationType_Inches_Abbrev},
            };

        public static FixtureLengthUnitOfMeasureType Parse(String value)
        {
            return EnumHelper.Parse<FixtureLengthUnitOfMeasureType>(value, FixtureLengthUnitOfMeasureType.Unknown);
        }

        public static PlanogramLengthUnitOfMeasureType ToPlanogramLengthUnitOfMeasureType(FixtureLengthUnitOfMeasureType type)
        {
            switch (type)
            {
                case FixtureLengthUnitOfMeasureType.Centimeters: return PlanogramLengthUnitOfMeasureType.Centimeters;
                case FixtureLengthUnitOfMeasureType.Inches: return PlanogramLengthUnitOfMeasureType.Inches;
                
                default:
                case FixtureLengthUnitOfMeasureType.Unknown: 
                    return PlanogramLengthUnitOfMeasureType.Unknown;
            }
                    
        }

    }
}
