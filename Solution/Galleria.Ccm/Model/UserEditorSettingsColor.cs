﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30791 : D.Pleasance
//	Created
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A user color
    /// </summary>
    [Serializable]
    public partial class UserEditorSettingsColor : ModelObject<UserEditorSettingsColor>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The unique id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region Permission Type
        /// <summary>
        /// Color property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ColorProperty =
            RegisterModelProperty<Int32>(c => c.Color);
        /// <summary>
        /// The color
        /// </summary>
        public Int32 Color
        {
            get { return GetProperty<Int32>(ColorProperty); }
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new user color
        /// </summary>
        /// <param name="permissionType">The permission type</param>
        /// <returns>A new role permission</returns>
        public static UserEditorSettingsColor NewUserEditorSettingsColor(Int32 color)
        {
            UserEditorSettingsColor item = new UserEditorSettingsColor();
            item.Create(color);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="permissionType"></param>
        private void Create(Int32 color)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(ColorProperty, color);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}