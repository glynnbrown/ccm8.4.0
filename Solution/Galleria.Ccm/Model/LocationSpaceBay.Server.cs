﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Csla;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class LocationSpaceBay
    {
        #region Constructor
        private LocationSpaceBay() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new location space Bay obect from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new location space Bay object</returns>
        internal static LocationSpaceBay GetLocationSpaceBay(IDalContext dalContext, LocationSpaceBayDto dto)
        {
            return DataPortal.FetchChild<LocationSpaceBay>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a new dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private LocationSpaceBayDto GetDataTransferObject(LocationSpaceProductGroup parent)
        {
            return new LocationSpaceBayDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                Order = ReadProperty<Byte>(OrderProperty),
                Height = ReadProperty<Single>(HeightProperty),
                Width = ReadProperty<Single>(WidthProperty),
                Depth = ReadProperty<Single>(DepthProperty),
                BaseHeight = ReadProperty<Single>(BaseHeightProperty),
                BaseWidth = ReadProperty<Single>(BaseWidthProperty),
                BaseDepth = ReadProperty<Single>(BaseDepthProperty),
                LocationSpaceProductGroupId = parent.Id,
                BayType = (Byte)ReadProperty<LocationSpaceBayType>(BayTypeProperty),
                BayTypeCalculation = ReadProperty<Single?>(BayTypeCalculationProperty),
                NotchPitch = ReadProperty<Single?>(NotchPitchProperty),
                FixtureName = ReadProperty<String>(FixtureNameProperty),
                BayTypePostFix = ReadProperty<String>(BayTypePostFixProperty),
                FixtureType = (Byte)ReadProperty<LocationSpaceFixtureType>(FixtureTypeProperty),
                IsPromotional = ReadProperty<Boolean>(IsPromotionalProperty),
                IsAisleStart = ReadProperty<Boolean>(IsAisleStartProperty),
                IsAisleEnd = ReadProperty<Boolean>(IsAisleEndProperty),
                IsAisleRight = ReadProperty<Boolean>(IsAisleRightProperty),
                IsAisleLeft = ReadProperty<Boolean>(IsAisleLeftProperty),
                ManufacturerName = ReadProperty<String>(ManufacturerProperty),
                Barcode = ReadProperty<String>(BarcodeProperty),
                HasPower = ReadProperty<Boolean>(HasPowerProperty),
                AssetNumber = ReadProperty<String>(AssetProperty),
                Temperature = ReadProperty<Single?>(TemperatureProperty),
                Colour = ReadProperty<String>(ColourProperty),
                BayLocationRef = ReadProperty<String>(BayLocationRefProperty),
                BayFloorDrawingNo = ReadProperty<String>(BayFloorDrawingNoProperty),
                NotchStartY = ReadProperty<Single>(NotchStartYProperty),
                FixtureShape = (Byte)ReadProperty<LocationSpaceFixtureShapeType>(FixtureShapeTypeProperty),


            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, LocationSpaceBayDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Byte>(OrderProperty, dto.Order);
            LoadProperty<Single>(HeightProperty, dto.Height);
            LoadProperty<Single>(WidthProperty, dto.Width);
            LoadProperty<Single>(DepthProperty, dto.Depth);
            LoadProperty<Single>(BaseHeightProperty, dto.BaseHeight);
            LoadProperty<Single>(BaseWidthProperty, dto.BaseWidth);
            LoadProperty<Single>(BaseDepthProperty, dto.BaseDepth);
            LoadProperty<LocationSpaceElementList>(ElementsProperty,
            LocationSpaceElementList.FetchByLocationSpaceBayId(dalContext, dto.Id));
            LoadProperty<LocationSpaceBayType>(BayTypeProperty, (LocationSpaceBayType)dto.BayType);
            LoadProperty<String>(BayTypePostFixProperty, dto.BayTypePostFix);
            LoadProperty<Single?>(BayTypeCalculationProperty, dto.BayTypeCalculation);
            LoadProperty<Single?>(NotchPitchProperty, dto.NotchPitch);
            LoadProperty<String>(FixtureNameProperty, dto.FixtureName);
            LoadProperty<LocationSpaceFixtureType>(FixtureTypeProperty, (LocationSpaceFixtureType)dto.FixtureType);
            LoadProperty<LocationSpaceFixtureShapeType>(FixtureShapeTypeProperty, (LocationSpaceFixtureShapeType)dto.FixtureShape);
            LoadProperty<Boolean>(IsPromotionalProperty, dto.IsPromotional);
            LoadProperty<Boolean>(IsAisleStartProperty, dto.IsAisleStart);
            LoadProperty<Boolean>(IsAisleEndProperty, dto.IsAisleEnd);
            LoadProperty<Boolean>(IsAisleRightProperty, dto.IsAisleRight);
            LoadProperty<Boolean>(IsAisleLeftProperty, dto.IsAisleLeft);
            LoadProperty<String>(ManufacturerProperty, dto.ManufacturerName);
            LoadProperty<String>(BarcodeProperty, dto.Barcode);
            LoadProperty<Boolean>(HasPowerProperty, dto.HasPower);
            LoadProperty<String>(AssetProperty, dto.AssetNumber);
            LoadProperty<Single?>(TemperatureProperty, dto.Temperature);
            LoadProperty<String>(ColourProperty, dto.Colour);
            LoadProperty<String>(BayLocationRefProperty, dto.BayLocationRef);
            LoadProperty<String>(BayFloorDrawingNoProperty, dto.BayFloorDrawingNo);
            LoadProperty<Single>(NotchStartYProperty, dto.NotchStartY);
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Called when returning an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, LocationSpaceBayDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting a child
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, LocationSpaceProductGroup parent)
        {
            LocationSpaceBayDto dto = GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (ILocationSpaceBayDal dal = dalContext.GetDal<ILocationSpaceBayDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<LocationSpaceBay>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region update
        /// <summary>
        /// Called when updating an existing child
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, LocationSpaceProductGroup parent)
        {
            if (this.IsSelfDirty)
            {
                using (ILocationSpaceBayDal dal = dalContext.GetDal<ILocationSpaceBayDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this instance is deleting itself
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (ILocationSpaceBayDal dal = dalContext.GetDal<ILocationSpaceBayDal>())
            {
                dal.DeleteById(this.Id);
            }
        }

        #endregion

        #endregion
    }
}
