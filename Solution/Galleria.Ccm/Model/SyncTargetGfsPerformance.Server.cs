﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class SyncTargetGfsPerformance
    {
        #region Constructors
        private SyncTargetGfsPerformance() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the specified sync target performance
        /// </summary>
        /// <param name="id">The sync source id</param>
        public static SyncTargetGfsPerformance GetSyncTargetGfsPerformanceById(Int32 id)
        {
            return DataPortal.Fetch<SyncTargetGfsPerformance>(new SingleCriteria<SyncTargetGfsPerformance, Int32>(id));
        }

        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        /// <returns>An existing sync target object</returns>
        internal static SyncTargetGfsPerformance FetchSyncTargetGfsPerformanceAsChild(IDalContext dalContext, SyncTargetGfsPerformanceDto dto)
        {
            return DataPortal.FetchChild<SyncTargetGfsPerformance>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Returns a dto for this object
        /// </summary>
        /// <returns>A dto created from this instance</returns>
        private SyncTargetGfsPerformanceDto GetDataTransferObject()
        {
            return new SyncTargetGfsPerformanceDto()
            {
                Id = this.ReadProperty<Int32>(IdProperty),
                RowVersion = this.ReadProperty<RowVersion>(RowVersionProperty),
                EntityName = this.ReadProperty<String>(EntityNameProperty),
                SourceId = this.ReadProperty<Int32>(SourceIdProperty),
                SourceName = this.ReadProperty<String>(SourceNameProperty),
                SourceType = (Byte)this.ReadProperty<GFSModel.PerformanceSourceType>(SourceTypeProperty),
                SourceSyncPeriod = this.ReadProperty<Byte?>(SourceSyncPeriodProperty),
                SyncTargetId = this.Parent.Id
            };
        }

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, SyncTargetGfsPerformanceDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32>(SyncTargetIdProperty, dto.SyncTargetId);
            this.LoadProperty<String>(EntityNameProperty, dto.EntityName);
            this.LoadProperty<Int32>(SourceIdProperty, dto.SourceId);
            this.LoadProperty<String>(SourceNameProperty, dto.SourceName);
            this.LoadProperty<GFSModel.PerformanceSourceType>(SourceTypeProperty, (GFSModel.PerformanceSourceType)dto.SourceType);
            this.LoadProperty<Byte?>(SourceSyncPeriodProperty, dto.SourceSyncPeriod);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when loading this instance
        /// from a data reader as part of a
        /// review list
        /// </summary>
        /// <param name="criteria">an object used to identify the desired review</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(SingleCriteria<SyncTargetGfsPerformance, Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory(Constants.SyncDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ISyncTargetGfsPerformanceDal dal = dalContext.GetDal<ISyncTargetGfsPerformanceDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchById(criteria.Value));
                }
            }
        }
        /// <summary>
        /// Called when loading this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, SyncTargetGfsPerformanceDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this object into the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory(Constants.SyncDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                SyncTargetGfsPerformanceDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (ISyncTargetGfsPerformanceDal dal = dalContext.GetDal<ISyncTargetGfsPerformanceDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                dalContext.RegisterId<SyncTargetGfsPerformance>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }

        /// <summary>
        /// Called when inserting this object into the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext)
        {
            SyncTargetGfsPerformanceDto dto = GetDataTransferObject();
            Int32 oldId = dto.Id;
            using (ISyncTargetGfsPerformanceDal dal = dalContext.GetDal<ISyncTargetGfsPerformanceDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            dalContext.RegisterId<SyncTargetGfsPerformance>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this object in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory(Constants.SyncDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                SyncTargetGfsPerformanceDto dto = GetDataTransferObject();
                using (ISyncTargetGfsPerformanceDal dal = dalContext.GetDal<ISyncTargetGfsPerformanceDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }

        /// <summary>
        /// Called when updating this object in the data store
        /// </summary>
        /// <param name="parent"></param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            SyncTargetGfsPerformanceDto dto = GetDataTransferObject();
            using (ISyncTargetGfsPerformanceDal dal = dalContext.GetDal<ISyncTargetGfsPerformanceDal>())
            {
                dal.Update(dto);
            }
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory(Constants.SyncDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ISyncTargetGfsPerformanceDal dal = dalContext.GetDal<ISyncTargetGfsPerformanceDal>())
                {
                    dalContext.Begin();
                    dal.DeleteById(ReadProperty<int>(IdProperty));
                    dalContext.Commit();
                }
            }
        }

        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (ISyncTargetGfsPerformanceDal dal = dalContext.GetDal<ISyncTargetGfsPerformanceDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion
    }
}