﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
// CCM-26322 : L.Ineson
//  Made ProductGroupId not null to match table script.
// V8-26764 : N.Foster
//  Added FetchByName
// V8-27241 : A.Kuszyk
//  Changed FetchByName to FetchByEntityIdName.
#endregion
#region Version History: (CCM CCM803)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#region Version History: (CCM 8.1.0)
// V8-29936 : N.Haywood
//  Update will set a new ucr
#endregion
#region Version History: (CCM CCM830)
// V8-31550 : A.Probyn
//  Added ProductBuddys & LocationBuddys
// V8-31551 : A.Probyn
//  Added InventoryRules
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    public partial class Assortment
    {
        #region Constructor
        private Assortment() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the specified Assortment
        /// </summary>
        public static Assortment GetById(Int32 id)
        {
            return DataPortal.Fetch<Assortment>(new SingleCriteria<Int32>(id));
        }

        /// <summary>
        /// Returns the specified assortment
        /// </summary>
        public static Assortment FetchByEntityIdName(Int32 entityId, String name)
        {
            return DataPortal.Fetch<Assortment>(new FetchByEntityIdNameCriteria(entityId, name));
        }

        /// <summary>
        /// Returns the latest Assortment with the specified name
        /// </summary>
        /// <param name="name">The Assortment name</param>
        /// <returns>The latest version of the Assortment</returns>
        public static Assortment FetchLatestVersionByEntityIdName(Int32 entityId, String name)
        {
            return DataPortal.Fetch<Assortment>(new FetchByEntityIdNameCriteria(entityId, name));
        }

        /// <summary>
        /// returns the specified Assortment
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static Assortment GetAssortment(IDalContext dalContext, AssortmentDto dto)
        {
            return DataPortal.FetchChild<Assortment>(dalContext, dto);
        }

        /// <summary>
        /// Deletes all Assortment with the given id 
        /// </summary>
        /// <param name="parentContentId"></param>
        public static void DeleteById(Int32 Id)
        {
            DataPortal.Delete<Assortment>(new DeleteByAssortmentIdCriteria(Id));
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this object from a data transfer object
        /// </summary>
        /// <param name="context">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext context, AssortmentDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            LoadProperty<Int32>(ProductGroupIdProperty, dto.ProductGroupId);
            LoadProperty<Int32?>(ConsumerDecisionTreeIdProperty, dto.ConsumerDecisionTreeId);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<Guid>(UniqueContentReferenceProperty, dto.UniqueContentReference);
            LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            
            LoadProperty<AssortmentProductList>(ProductsProperty, AssortmentProductList.GetAssortmentProductList(context, dto.Id));            
            LoadProperty<AssortmentLocationList>(LocationsProperty, AssortmentLocationList.GetAssortmentLocationList(context, dto.Id));
            LoadProperty<AssortmentLocalProductList>(LocalProductsProperty, AssortmentLocalProductList.GetAssortmentLocalProductList(context, dto.Id));
            LoadProperty<AssortmentRegionList>(RegionsProperty, AssortmentRegionList.GetAssortmentRegionList(context, dto.Id));
            LoadProperty<AssortmentFileList>(FilesProperty, AssortmentFileList.GetAssortmentFileList(context, dto.Id));
            LoadProperty<AssortmentInventoryRuleList>(InventoryRulesProperty, AssortmentInventoryRuleList.GetAssortmentInventoryRuleList(context, dto.Id));
            LoadProperty<AssortmentLocationBuddyList>(LocationBuddiesProperty, AssortmentLocationBuddyList.GetAssortmentLocationBuddyList(context, dto.Id));
            LoadProperty<AssortmentProductBuddyList>(ProductBuddiesProperty, AssortmentProductBuddyList.GetAssortmentProductBuddyList(context, dto.Id));
            LoadProperty<Guid?>(ParentUniqueContentReferenceProperty, dto.ParentUniqueContentReference);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        /// <returns>A new data transfer object</returns>
        private AssortmentDto GetDataTransferObject()
        {
            AssortmentDto dto = new AssortmentDto();
            dto.Id = ReadProperty<Int32>(IdProperty);
            dto.RowVersion = ReadProperty<RowVersion>(RowVersionProperty);
            dto.EntityId = ReadProperty<Int32>(EntityIdProperty);
            dto.ProductGroupId = ReadProperty<Int32>(ProductGroupIdProperty);
            dto.ConsumerDecisionTreeId = ReadProperty<Int32?>(ConsumerDecisionTreeIdProperty);
            dto.Name = ReadProperty<String>(NameProperty);
            dto.UniqueContentReference = ReadProperty<Guid>(UniqueContentReferenceProperty);
            dto.DateCreated = ReadProperty<DateTime>(DateCreatedProperty);
            dto.DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty);
            dto.ParentUniqueContentReference = ReadProperty<Guid?>(ParentUniqueContentReferenceProperty);

            return dto;
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching a Assortment by id
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IAssortmentDal dal = dalContext.GetDal<IAssortmentDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchById(criteria.Value));
                }
            }
        }

        /// <summary>
        /// Called when fetching a Assortment by name
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdNameCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IAssortmentDal dal = dalContext.GetDal<IAssortmentDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchByEntityIdName(criteria.EntityId, criteria.Name));
                }
            }
        }

        /// <summary>
        /// Called when fetching a product universe by dto
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto"></param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, AssortmentDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this instance into the database
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                AssortmentDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (IAssortmentDal dal = dalContext.GetDal<IAssortmentDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<Assortment>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when this instance is being updated
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                AssortmentDto dto = GetDataTransferObject();
                dto.UniqueContentReference = Guid.NewGuid();
                using (IAssortmentDal dal = dalContext.GetDal<IAssortmentDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this instance is being deleted
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IAssortmentDal dal = dalContext.GetDal<IAssortmentDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }


        /// <summary>
        ///Called when deleting by ParentContentId
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Delete(DeleteByAssortmentIdCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IAssortmentDal dal = dalContext.GetDal<IAssortmentDal>())
                {
                    dal.DeleteById(criteria.AssortmentId);
                }
            }
        }

        #endregion

        #endregion
    }
}