﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26124 : I.George
//		Created (Auto-generated)
// V8-28013 : A.Kuszyk
//  Added FetchByEntityIdName.
#endregion

#region Version History: CCM810

// V8-29768 : A.Silva
//  Added CalculateMaxUnits and a new Create from PlanogramInventory method.

#endregion

#region Version History: (CCM 820)
// CCM-30759 : J.Pickup
//		InventoryProfileType added.
#endregion

#endregion

using System;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{

    /// <summary>
    /// InventoryProfile model object
    /// </summary>
    [Serializable]
    public sealed partial class InventoryProfile : ModelObject<InventoryProfile>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<int> IdProperty =
            RegisterModelProperty<int>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The row version timestamp
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region EntityId

        /// <summary>
        /// EntityId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<int> EntityIdProperty =
            RegisterModelProperty<int>(c => c.EntityId);

        /// <summary>
        /// Gets/Sets the EntityId value
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
            set { SetProperty<Int32>(EntityIdProperty, value); }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<string> NameProperty =
            RegisterModelProperty<string>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region CasePack

        /// <summary>
        /// CasePack property definition
        /// </summary>
        public static readonly ModelPropertyInfo<float> CasePackProperty =
            RegisterModelProperty<float>(c => c.CasePack);

        /// <summary>
        /// Gets/Sets the CasePack value
        /// </summary>
        public Single CasePack
        {
            get { return GetProperty<Single>(CasePackProperty); }
            set { SetProperty<Single>(CasePackProperty, value); }
        }

        #endregion

        #region DaysOfSupply

        /// <summary>
        /// DaysOfSupply property definition
        /// </summary>
        public static readonly ModelPropertyInfo<float> DaysOfSupplyProperty =
            RegisterModelProperty<float>(c => c.DaysOfSupply);

        /// <summary>
        /// Gets/Sets the DaysOfSupply value
        /// </summary>
        public Single DaysOfSupply
        {
            get { return GetProperty<Single>(DaysOfSupplyProperty); }
            set { SetProperty<Single>(DaysOfSupplyProperty, value); }
        }

        #endregion

        #region ShelfLife

        /// <summary>
        /// ShelfLife property definition
        /// </summary>
        public static readonly ModelPropertyInfo<float> ShelfLifeProperty =
            RegisterModelProperty<float>(c => c.ShelfLife);

        /// <summary>
        /// Gets/Sets the ShelfLife value
        /// </summary>
        public Single ShelfLife
        {
            get { return GetProperty<Single>(ShelfLifeProperty); }
            set { SetProperty<Single>(ShelfLifeProperty, value); }
        }

        #endregion

        #region ReplenishmentDays

        /// <summary>
        /// ReplenishmentDays property definition
        /// </summary>
        public static readonly ModelPropertyInfo<float> ReplenishmentDaysProperty =
            RegisterModelProperty<float>(c => c.ReplenishmentDays);

        /// <summary>
        /// Gets/Sets the ReplenishmentDays value
        /// </summary>
        public Single ReplenishmentDays
        {
            get { return GetProperty<Single>(ReplenishmentDaysProperty); }
            set { SetProperty<Single>(ReplenishmentDaysProperty, value); }
        }

        #endregion

        #region WasteHurdleUnits

        /// <summary>
        /// WasteHurdleUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<float> WasteHurdleUnitsProperty =
            RegisterModelProperty<float>(c => c.WasteHurdleUnits);

        /// <summary>
        /// Gets/Sets the WasteHurdleUnits value
        /// </summary>
        public Single WasteHurdleUnits
        {
            get { return GetProperty<Single>(WasteHurdleUnitsProperty); }
            set { SetProperty<Single>(WasteHurdleUnitsProperty, value); }
        }

        #endregion

        #region WasteHurdleCasePack

        /// <summary>
        /// WasteHurdleWastePack property definition
        /// </summary>
        public static readonly ModelPropertyInfo<float> WasteHurdleCasePackProperty =
            RegisterModelProperty<float>(c => c.WasteHurdleCasePack);

        /// <summary>
        /// Gets/Sets the WasteHurdleWastePack value
        /// </summary>
        public Single WasteHurdleCasePack
        {
            get { return GetProperty<Single>(WasteHurdleCasePackProperty); }
            set { SetProperty<Single>(WasteHurdleCasePackProperty, value); }
        }

        #endregion

        #region MinUnits

        /// <summary>
        /// MinUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<int> MinUnitsProperty =
            RegisterModelProperty<int>(c => c.MinUnits);

        /// <summary>
        /// Gets/Sets the MinUnits value
        /// </summary>
        public Int32 MinUnits
        {
            get { return GetProperty<Int32>(MinUnitsProperty); }
            set { SetProperty<Int32>(MinUnitsProperty, value); }
        }

        #endregion

        #region MinFacings

        /// <summary>
        /// MinFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<int> MinFacingsProperty =
            RegisterModelProperty<int>(c => c.MinFacings);

        /// <summary>
        /// Gets/Sets the MinFacings value
        /// </summary>
        public Int32 MinFacings
        {
            get { return GetProperty<Int32>(MinFacingsProperty); }
            set { SetProperty<Int32>(MinFacingsProperty, value); }
        }

        #endregion

        #region InventoryProfileType

        /// <summary>
        /// Inventory Profile Type property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramInventoryMetricType> InventoryProfileTypeProperty =
            RegisterModelProperty<PlanogramInventoryMetricType>(c => c.InventoryProfileType);

        /// <summary>
        /// Gets/Sets the Inventory Profile Type  value
        /// </summary>
        public PlanogramInventoryMetricType InventoryProfileType
        {
            get { return GetProperty<PlanogramInventoryMetricType>(InventoryProfileTypeProperty); }
            set { SetProperty<PlanogramInventoryMetricType>(InventoryProfileTypeProperty, value); }
        }

        #endregion

        #region DateCreated

        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        /// Gets/Sets the DateCreated value
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
            set { SetProperty<DateTime>(DateCreatedProperty, value); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        /// Gets/Sets the DateLastModified value
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
            set { SetProperty<DateTime>(DateLastModifiedProperty, value); }
        }

        #endregion

        #region DateDeleted

        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);

        /// <summary>
        /// Gets/Sets the DateDeleted value
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
            set { SetProperty<DateTime?>(DateDeletedProperty, value); }
        }

        #endregion


        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new MinValue<int>(EntityIdProperty, 1));
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(InventoryProfile), new IsInRole(AuthorizationActions.GetObject, DomainPermission.InventoryProfileGet.ToString()));
            BusinessRules.AddRule(typeof(InventoryProfile), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.InventoryProfileCreate.ToString()));
            BusinessRules.AddRule(typeof(InventoryProfile), new IsInRole(AuthorizationActions.EditObject, DomainPermission.InventoryProfileEdit.ToString()));
            BusinessRules.AddRule(typeof(InventoryProfile), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.InventoryProfileDelete.ToString()));
        }
        #endregion

        #region Criteria

        #region FetchByIdCriteria

        // <summary>
        /// Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<int> IdProperty =
                RegisterProperty<int>(c => c.Id);
            /// <summary>
            /// Returns the id
            /// </summary>
            public Int32 Id
            {
                get { return this.ReadProperty<Int32>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(Int32 id)
            {
                this.LoadProperty<Int32>(IdProperty, id);
            }
            #endregion
        }

        [Serializable]
        public class FetchByEntityIdNameCriteria : CriteriaBase<FetchByEntityIdNameCriteria>
        {
            #region Properties

            #region EntityId
            /// <summary>
            /// EntityId property definition
            /// </summary>
            public static readonly PropertyInfo<int> EntityIdProperty =
                RegisterProperty<int>(c => c.EntityId);
            /// <summary>
            /// Returns the EntityId
            /// </summary>
            public Int32 EntityId
            {
                get { return this.ReadProperty<Int32>(EntityIdProperty); }
            }
            #endregion

            #region Name
            /// <summary>
            /// Name property definition
            /// </summary>
            public static readonly PropertyInfo<string> NameProperty =
                RegisterProperty<string>(c => c.Name);
            /// <summary>
            /// Returns the Name
            /// </summary>
            public String Name
            {
                get { return this.ReadProperty<String>(NameProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByEntityIdNameCriteria(Int32 entityId, String name)
            {
                this.LoadProperty<Int32>(EntityIdProperty, entityId);
                this.LoadProperty<String>(NameProperty, name);
            }
            #endregion
        }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new InventoryProfile
        /// </summary>
        public static InventoryProfile NewInventoryProfile(Int32 entityId)
        {
            InventoryProfile item = new InventoryProfile();
            item.Create(entityId);
            return item;
        }

        public static InventoryProfile NewInventoryProfile(PlanogramInventory planogram)
        {
            var item = new InventoryProfile();
            item.Create(planogram);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 entityId)
        {
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            base.Create();
        }

        /// <summary>
        ///     Called when creating a new instance of <see cref="InventoryProfile"/> from data in an <see cref="PlanogramInventory"/>.
        /// </summary>
        private void Create(PlanogramInventory inventory)
        {
            this.LoadProperty<Int32>(EntityIdProperty, 0);
            this.LoadProperty<Single>(ReplenishmentDaysProperty, inventory.MinDeliveries);
            this.LoadProperty<Single>(ShelfLifeProperty, inventory.MinShelfLife);
            this.LoadProperty<Single>(DaysOfSupplyProperty, inventory.MinDos);
            this.LoadProperty<Single>(CasePackProperty, inventory.MinCasePacks);
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            base.Create();
        }

        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<InventoryProfile>(oldId, newId);
        }

        /// <summary>
        ///     Calculates the max units that the given <paramref name="product"/> should achieve considering the <paramref name="performanceData"/> and this <see cref="InventoryProfile"/>.
        /// </summary>
        /// <param name="product">The product to calculate max units for.</param>
        /// <param name="performanceData">the performance data to use when calculating the max units.</param>
        public Int16 CalculateMaxUnits(IPlanogramProduct product, IPlanogramPerformanceData performanceData)
        {
            // First, calculate values dependant on Units sold per day which might be null.
            Single maxShelfLifeUnits;
            Single minDaysOfSupply;
            Single minReplenishmentDays;
            if (performanceData.UnitsSoldPerDay.HasValue && performanceData.UnitsSoldPerDay.Value.GreaterThan(0))
            // If we have a value.
            {
                // Determine if we need to remove this product due to the less than the number of units or case packs
                // per week indicated in the inventory profile. If we do, just return 0.
                if (WasteHurdleUnits > 0 || WasteHurdleCasePack > 0)
                {
                    Boolean removeProduct =
                        (performanceData.UnitsSoldPerDay.Value*7).LessThan(WasteHurdleUnits) ||
                        (performanceData.UnitsSoldPerDay.Value*7/product.CasePackUnits).LessThan(
                            WasteHurdleCasePack);
                    if (removeProduct) return 0;
                }
                // Shelf life - this is the percentage of shelf life from the inventory profile multiplied
                // by the days of shelf life for the product multiplied by the units sold per day.
                maxShelfLifeUnits = Convert.ToSingle(
                    Math.Floor(ShelfLife * product.ShelfLife * performanceData.UnitsSoldPerDay.Value));

                // Days of supply - simply the inventory profile days of supply multiplied by the units
                // sold per day for the product.
                minDaysOfSupply = DaysOfSupply * performanceData.UnitsSoldPerDay.Value;

                // Min. number of deliveries/replenishment days - this is the number deliveries that
                // there must be units for from the inventory profile, multiplied by the days between
                // deliveries for the product, multiplied by the units sold per day for the product.
                Single deliveryFrequency = product.DeliveryFrequencyDays ?? 0;
                minReplenishmentDays = ReplenishmentDays * deliveryFrequency *
                                       performanceData.UnitsSoldPerDay.Value;
            }
            else // if we don't have a value, set the minima to zero and the maximum to one.
            {
                minDaysOfSupply = 0;
                minReplenishmentDays = 0;
                maxShelfLifeUnits = 1;
            }

            // Now, continue to calculate values that aren't dependant on Units Sold Per Day.

            // Minimum units - this is simply taken from the inventory profile.
            Single minUnits = MinUnits;

            // Minimum number of case packs - we just convert the inventory profile case packs
            // value into units by multiplying it by the units per case pack for the product.
            Single minCasePacks = CasePack * product.CasePackUnits;

            // Return maxmimum units out of the calculated minima.
            Single maximumValue = new[] { minCasePacks, minDaysOfSupply, minReplenishmentDays, minUnits }.Max();
            if (maxShelfLifeUnits == 0) maxShelfLifeUnits = maximumValue;
            try
            {
                // Restrict the maximum by the maxShelfLifeUnits, to ensure we don't exceed the maximum
                // percentage of shelf life units that should appear.
                return Convert.ToInt16(Math.Ceiling(Math.Min(maximumValue, maxShelfLifeUnits)));
            }
            catch (OverflowException)
            {
                return Int16.MaxValue;
            }
        }
        #endregion

        #region Override

        public override string ToString()
        {
            return this.Name;
        }
        #endregion
    }

}
