﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-26222 : L.Luong
//		Created (Auto-generated)

#endregion
#endregion

using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class UserInfoList
    {
        #region Constructors
        private UserInfoList() { } // force the use of factory methods
        #endregion

        #region FactoryMethods

        public static UserInfoList FetchAll()
        {
            return DataPortal.Fetch<UserInfoList>(new FetchAllCriteria());
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when retrieving a list of all entity infos
        /// </summary>
        private void DataPortal_Fetch(FetchAllCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IUserInfoDal dal = dalContext.GetDal<IUserInfoDal>())
                {
                    IEnumerable<UserInfoDto> dtoList = dal.FetchAll();
                    foreach (UserInfoDto dto in dtoList)
                    {
                        this.Add(UserInfo.FetchUserInfo(dalContext, dto));
                    }
                }
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion

        #endregion

    }
}

