﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8025632 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class ConsumerDecisionTreeNodeProductList
    {
        #region Constructor
        private ConsumerDecisionTreeNodeProductList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        /// <returns>A list of existing it</returns>
        internal static ConsumerDecisionTreeNodeProductList FetchListByConsumerDecisionTreeNodeId(
            IDalContext dalContext, Int32 parentNodeId)
        {
            return DataPortal.FetchChild<ConsumerDecisionTreeNodeProductList>(dalContext, parentNodeId);
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parentNodeId">The parent node id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 parentId)
        {
            RaiseListChangedEvents = false;

            using (IConsumerDecisionTreeNodeProductDal dal =
               dalContext.GetDal<IConsumerDecisionTreeNodeProductDal>())
            {
                IEnumerable<ConsumerDecisionTreeNodeProductDto> dtoList =
                    dal.FetchByConsumerDecisionTreeNodeId(parentId);

                foreach (ConsumerDecisionTreeNodeProductDto childDto in dtoList)
                {
                    this.Add(ConsumerDecisionTreeNodeProduct.FetchConsumerDecisionTreeNodeProduct(dalContext, childDto));
                }
            }

            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}
