﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-27499 : N.Foster
//  Created
#endregion
#endregion

namespace Galleria.Ccm.Model
{
    public enum EngineMetricType : byte
    {
        Counter = Galleria.Framework.Engine.EngineMetricType.Counter, // 0
        Process = Galleria.Framework.Engine.EngineMetricType.Process, // 1
        TaskExecution = 2,
        TaskPreExecution = 3,
        TaskPostExecution = 4
    }
}
