﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27197 : L.Ineson
//  Moved out from CustomColumn
#endregion

#endregion

using System.Collections.Generic;
using System;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    public enum CustomColumnTotalType
    {
        None = 0,
        Count = 1,
        CountTrue = 2,
        CountFalse = 3,
        Sum = 4,
        Average = 5,
        StandardDeviation = 6,
        Min = 7,
        Max = 8,
    }

    public static class CustomColumnTotalTypeHelper
    {
        public static readonly Dictionary<CustomColumnTotalType, String> FriendlyNames = 
            new Dictionary <CustomColumnTotalType, String>
            {
                {CustomColumnTotalType.None, Message.CustomColumnTotalType_None},
                {CustomColumnTotalType.Average, Message.CustomColumnTotalType_Average},
                {CustomColumnTotalType.Count, Message.CustomColumnTotalType_Count},
                {CustomColumnTotalType.CountTrue, Message.CustomColumnTotalType_CountTrue},
                {CustomColumnTotalType.CountFalse, Message.CustomColumnTotalType_CountFalse},
                {CustomColumnTotalType.Max, Message.CustomColumnTotalType_Max},
                {CustomColumnTotalType.Min, Message.CustomColumnTotalType_Min},
                {CustomColumnTotalType.StandardDeviation, Message.CustomColumnTotalType_StandardDeviation},
                {CustomColumnTotalType.Sum, Message.CustomColumnTotalType_Sum}
            };
    }
}
