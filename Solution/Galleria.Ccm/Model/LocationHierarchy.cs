﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//		Created (Auto-generated)
// CCM-25444 : N.Haywood
//      Added methods used for location maintenance
// V8-25556 : D.Pleasance
//      Added FetchByEntityIdContextCriteria
// V8-27997 : A.Probyn
//      Updated delete authorization to be valid for Administrator
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{

    /// <summary>
    /// LocationHierarchy model object
    /// </summary>
    [Serializable]
    public sealed partial class LocationHierarchy : ModelObject<LocationHierarchy>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The row version timestamp
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region EntityId

        /// <summary>
        /// EntityId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);

        /// <summary>
        /// Gets/Sets the EntityId value
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
            set { SetProperty<Int32>(EntityIdProperty, value); }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region DateCreated

        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        /// Gets/Sets the DateCreated value
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
            set { SetProperty<DateTime>(DateCreatedProperty, value); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        /// Gets/Sets the DateLastModified value
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
            set { SetProperty<DateTime>(DateLastModifiedProperty, value); }
        }

        #endregion

        #region DateDeleted

        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);

        /// <summary>
        /// Gets/Sets the DateDeleted value
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
            set { SetProperty<DateTime?>(DateDeletedProperty, value); }
        }

        #endregion

        #region Root Level

        public static readonly ModelPropertyInfo<LocationLevel> RootLevelProperty =
            RegisterModelProperty<LocationLevel>(c => c.RootLevel);
        /// <summary>
        /// The root level of the hierarchy
        /// </summary>
        public LocationLevel RootLevel
        {
            get { return GetProperty<LocationLevel>(RootLevelProperty); }
        }

        #endregion

        #region Root Group

        public static readonly ModelPropertyInfo<LocationGroup> RootGroupProperty =
            RegisterModelProperty<LocationGroup>(c => c.RootGroup);
        /// <summary>
        /// The root level of the hierarchy
        /// </summary>
        public LocationGroup RootGroup
        {
            get { return GetProperty<LocationGroup>(RootGroupProperty); }
        }

        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new MinValue<Int32>(EntityIdProperty, 1));
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));

        }

        #endregion

        #region Authorization Rules

        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationHierarchy), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(LocationHierarchy), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationHierarchy), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationHierarchy), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Administrator.ToString()));
        }

        #endregion

        #region Criteria

        #region FetchByIdCriteria

        // <summary>
        /// Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> IdProperty =
                RegisterProperty<Int32>(c => c.Id);
            /// <summary>
            /// Returns the id
            /// </summary>
            public Int32 Id
            {
                get { return this.ReadProperty<Int32>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(Int32 id)
            {
                this.LoadProperty<Int32>(IdProperty, id);
            }
            #endregion
        }

        #endregion

        #region FetchByEntityIdCriteria

        /// <summary>
        /// Criteria for the FetchByEntityId factory method
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : CriteriaBase<FetchByEntityIdCriteria>
        {
            #region Properties

            #region EntityId
            /// <summary>
            /// EntityId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Int32 EntityId
            {
                get { return this.ReadProperty<Int32>(EntityIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByEntityIdCriteria(Int32 id)
            {
                this.LoadProperty<Int32>(EntityIdProperty, id);
            }
            #endregion
        }

        #endregion

        #region FetchByEntityIdContextCriteria

        /// <summary>
        /// Criteria for FetchHierarchyByEntityIdContext
        /// </summary>
        [Serializable]
        public class FetchByEntityIdContextCriteria :
            Csla.CriteriaBase<FetchByEntityIdContextCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
            RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static readonly PropertyInfo<IDalContext> ContextProperty =
            RegisterProperty<IDalContext>(c => c.Context);
            public IDalContext Context
            {
                get { return ReadProperty<IDalContext>(ContextProperty); }
            }

            public FetchByEntityIdContextCriteria(Int32 entityId, IDalContext context)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<IDalContext>(ContextProperty, context);
            }
        }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new LocationHierarchy
        /// </summary>
        public static LocationHierarchy NewLocationHierarchy(Int32 entityId)
        {
            LocationHierarchy item = new LocationHierarchy();
            item.Create(entityId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 entityId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, Message.Entity_LocationHierarchyDefaultName);
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);

            //add in a new root level
            this.LoadProperty<LocationLevel>(RootLevelProperty, LocationLevel.NewLocationLevel());
            this.RootLevel.Name = Message.LocationHierarchy_RootLevel_DefaultName;

            //add in a new root group
            this.LoadProperty<LocationGroup>(RootGroupProperty, LocationGroup.NewLocationGroup(this.RootLevel.Id));
            this.RootGroup.Code = "0";
            this.RootGroup.Name = Message.LocationHierarchy_RootGroup_DefaultName;

            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// ToString override
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<LocationHierarchy>(oldId, newId);
        }

        /// <summary>
        /// Enumerates through all levels of this hierarchy in order
        /// from root to bottom.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<LocationLevel> EnumerateAllLevels()
        {
            //move down through the levels adding each one
            LocationLevel currentLevel = this.RootLevel;
            while (currentLevel != null)
            {
                yield return currentLevel;

                currentLevel = currentLevel.ChildLevel;
            }
        }

        /// <summary>
        /// Enumerates through all groups in the structure 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<LocationGroup> EnumerateAllGroups()
        {
            foreach (LocationGroup g in this.RootGroup.EnumerateAllChildGroups())
            {
                yield return g;
            }
        }

        /// <summary>
        /// Inserts a new child under the parent and moves its children down
        /// </summary>
        /// <param name="parentLevel"></param>
        /// <returns>the new child</returns>
        public LocationLevel InsertNewChildLevel(LocationLevel parentLevel)
        {
            //create the new child
            LocationLevel newChildLevel = LocationLevel.NewLocationLevel();

            //[SA-15074] give it a unqiue name
            if (parentLevel.ParentHierarchy != null)
            {
                List<LocationLevel> existingLevels = parentLevel.ParentHierarchy.EnumerateAllLevels().ToList();
                IEnumerable<String> takenNames = existingLevels.Select(l => l.Name);
                Int32 newChildLevelNo = existingLevels.IndexOf(parentLevel) + 2;
                do
                {
                    newChildLevel.Name = String.Format(Message.LocationLevel_DefaultName, newChildLevelNo);
                    newChildLevelNo++;
                }
                while (takenNames.Contains(newChildLevel.Name));
            }


            //get the parents current child
            LocationLevel currentParentChild = parentLevel.ChildLevel;

            if (currentParentChild != null)
            {
                //need to move the child to be a child of the new level instead
                newChildLevel.ChildLevel = currentParentChild;
                //remove from its old parent
                parentLevel.ChildLevel = null;

                //now set the new child and the child of the parent
                parentLevel.ChildLevel = newChildLevel;


                //need to insert an extra unit underneath all units on the parent level

                //cycle through all the parents
                Int32 addedGroupNumber = 1;
                foreach (LocationGroup unit in EnumerateAllGroups())
                {
                    if (Object.Equals(unit.LocationLevelId, parentLevel.Id))
                    {

                        //get a list of child units belonging to the parent
                        List<LocationGroup> parentChildUnits = unit.ChildList.ToList();

                        if (parentChildUnits.Count > 0)
                        {
                            //create a new child unit on the new level
                            LocationGroup newChildUnit = LocationGroup.NewLocationGroup(newChildLevel.Id);
                            newChildUnit.Code = GetNextAvailableDefaultGroupCode();
                            newChildUnit.Name = String.Format(Message.LocationGroup_DefaultName, newChildLevel.Name, addedGroupNumber);

                            //add the children to this new unit
                            newChildUnit.ChildList.AddList(parentChildUnits);

                            //clear the parent child collection and add the new child unit
                            unit.ChildList.Clear();
                            unit.ChildList.Add(newChildUnit);
                        }

                        addedGroupNumber++;
                    }
                }
            }
            else
            {
                parentLevel.ChildLevel = newChildLevel;
            }

            return newChildLevel;
        }

        /// <summary>
        /// Method to constrcut a default hierarchy
        /// </summary>
        public void ConstructDefaultHierarchy()
        {
            LocationLevel rootLevel = this.RootLevel;
            rootLevel.ShapeNo = 0;
            rootLevel.Colour = 1027916543;

            LocationLevel firstLevel = this.InsertNewChildLevel(this.RootLevel);
            firstLevel.Name = Message.LocationHierarchy_BannerLevel_DefaultName;
            firstLevel.ShapeNo = 10;
            firstLevel.Colour = 1270125823;

            LocationLevel secondLevel = this.InsertNewChildLevel(firstLevel);
            secondLevel.Name = Message.LocationHierarchy_RegionLevel_DefaultName;
            secondLevel.ShapeNo = 7;
            secondLevel.Colour = -1587642625;
        }

        /// <summary>
        /// Returns the next available code integer 
        /// that may be used by a group in this hierarchy
        /// </summary>
        /// <returns></returns>
        public String GetNextAvailableDefaultGroupCode()
        {
            IEnumerable<String> takenCodes = EnumerateAllGroups().Select(g => g.Code);
            Int32 codeInt = 1;

            while (takenCodes.Contains(codeInt.ToString()))
            {
                codeInt++;
            }

            return codeInt.ToString();
        }

        /// <summary>
        /// Returns the next available default level name
        /// </summary>
        /// <returns></returns>
        public String GetNextAvailableLevelName(LocationLevel parentLevel)
        {
            String name = String.Empty;

            List<LocationLevel> existingLevels = EnumerateAllLevels().ToList();

            IEnumerable<String> takenNames = existingLevels.Select(l => l.Name);
            Int32 newChildLevelNo = existingLevels.IndexOf(parentLevel) + 2;
            do
            {
                name = String.Format(Message.LocationLevel_DefaultName, newChildLevelNo);
                newChildLevelNo++;
            }
            while (takenNames.Contains(name));

            return name;
        }

        #region Action Specific Helpers

        /// <summary>
        /// Returns an empty string if the given group may have a child group added
        /// otherwise returns the appropriate error.
        /// </summary>
        /// <param name="parentGroup"></param>
        /// <returns></returns>
        public static String IsAddGroupAllowed(LocationGroup parentGroup)
        {
            //must have a parent unit selected
            if (parentGroup == null)
            {
                return Message.Hierarchy_Error_PleaseSelectValidGroup;
            }

            LocationHierarchy parentHierarchy = parentGroup.ParentHierarchy;

            //the level on which the parent resides must not be the bottom one.
            if (parentHierarchy != null
                && parentHierarchy.EnumerateAllLevels().Last().Id == parentGroup.LocationLevelId)
            {
                return Message.Hierarchy_Error_AddGroupNoAvailableLevel;
            }


            return String.Empty;
        }

        /// <summary>
        /// Returns an empty string if the move is valid
        /// Otherwise a friendly display error is given
        /// </summary>
        /// <param name="groupToMove"></param>
        /// <param name="newParentGroup"></param>
        /// <returns></returns>
        public static String IsMoveGroupAllowed(LocationGroup groupToMove, LocationGroup newParent)
        {
            //must have a group selected
            if (groupToMove == null)
            {
                return Message.Hierarchy_Error_PleaseSelectValidGroup;
            }

            //must not be the root node
            if (groupToMove.IsRoot)
            {
                return Message.Hierarchy_Error_NotRootGroup;
            }

            //param must be valid
            if (newParent == null)
            {
                return Message.Hierarchy_Error_PleaseSelectValidGroup;
            }

            //selected node and move to node must not be the same
            if (newParent == groupToMove)
            {
                return Message.Hierarchy_Error_ActionInvalid;
            }


            //selected node is already be a child of the node to move to -
            // will not return an error for this as whilst pointless it is valid.

            //new parent must not have any direct locations
            if (newParent.AssignedLocationsCount > 0)
            {
                return Message.Hierarchy_Error_ActionInvalid;
            }

            //the new parent associated level must not be the lowest level
            //i.e we must have a level available to move to.
            if (newParent.GetAssociatedLevel().ChildLevel == null)
            {
                return Message.Hierarchy_Error_ActionInvalid;
            }


            return String.Empty;
        }

        /// <summary>
        /// Returns an empty string if the given group may be removed from the hierarchy
        /// otherwise returns the appropriate error.
        /// </summary>
        /// <param name="parentGroup"></param>
        /// <returns></returns>
        public static String IsRemoveGroupAllowed(LocationGroup groupToRemove)
        {
            //must have a unit selected
            if (groupToRemove == null)
            {
                return Message.Hierarchy_Error_PleaseSelectValidGroup;
            }

            //must not have the root selected
            if (groupToRemove.IsRoot)
            {
                return Message.Hierarchy_Error_NotRootGroup;
            }


            return String.Empty;
        }

        /// <summary>
        /// Returns an empty string if the given level may have a new child added
        /// otherwise returns the appropriate error.
        /// </summary>
        /// <param name="parentGroup"></param>
        /// <returns></returns>
        public static String IsAddLevelAllowed(LocationLevel parentLevel)
        {
            //must have a level selected
            if (parentLevel == null)
            {
                return Message.Hierarchy_Error_LowestLevelOnly;
            }

            // Must be the lowest level
            if (parentLevel.ChildLevel != null)
            {
                return Message.Hierarchy_Error_LowestLevelOnly;
            }


            return String.Empty;
        }

        /// <summary>
        /// Returns an empty string if the given level may be removed from the hierarchy
        /// otherwise returns the appropriate error.
        /// </summary>
        /// <param name="parentGroup"></param>
        /// <returns></returns>
        public static String IsRemoveLevelAllowed(LocationLevel levelToRemove)
        {
            //must have a level selected
            if (levelToRemove == null)
            {
                return Message.Hierarchy_Error_LowestLevelOnly;
            }

            //must not have the root selected
            if (levelToRemove.ParentLevel == null)
            {
                return Message.Hierarchy_Error_LowestLevelOnly;
            }

            // must be the lowest level
            if (levelToRemove.ChildLevel != null)
            {
                return Message.Hierarchy_Error_LowestLevelOnly;
            }

            // Can only remove if no groups reside at this level.
            LocationHierarchy parentHierarchy = levelToRemove.ParentHierarchy;
            if (parentHierarchy != null &&
                parentHierarchy.EnumerateAllGroups().Any(g =>
                    g.AssignedLocationsCount > 0 && g.LocationLevelId == levelToRemove.Id))
            {
                return Message.Hierarchy_Error_LevelHasLocations;
            }

            return String.Empty;
        }

        /// <summary>
        /// Safetly assigns the given child group to the proposed parent if legal.
        /// Carries out any required product re-assignment actions.
        /// </summary>
        /// <param name="childGroup">the group to set as the child</param>
        /// <param name="parentToAssign">the new parent to assign the child group to.</param>
        /// <returns>The loc group instance belonging to the given parent 
        /// - this may not be the child group instance if a move action was performed</returns>
        public static LocationGroup AssignGroupParent(LocationGroup childGroup, LocationGroup parentToAssign)
        {
            LocationGroup addedReturnGroup = childGroup;

            //new group to be added
            if (childGroup.ParentGroup == null)
            {
                if (String.IsNullOrEmpty(IsAddGroupAllowed(parentToAssign)))
                {
                    //add the group to the parent's list
                    parentToAssign.ChildList.Add(childGroup);

                    //if the parent has locations then move them
                    if (parentToAssign.AssignedLocationsCount > 0)
                    {
                        List<LocationGroupLocation> parentLocations = parentToAssign.AssignedLocations.ToList();
                        childGroup.AssignedLocations.AddList(parentLocations);
                        parentToAssign.AssignedLocations.RemoveList(parentLocations);
                    }
                }
            }

            //group is to be moved
            else if (childGroup.ParentGroup != parentToAssign)
            {
                if (String.IsNullOrEmpty(IsMoveGroupAllowed(childGroup, parentToAssign)))
                {
                    LocationGroup origParent = childGroup.ParentGroup;
                    String groupCode = childGroup.Code;

                    //move the group
                    parentToAssign.ChildList.Add(childGroup);
                    origParent.ChildList.Remove(childGroup);

                    //Do not need to reassign locations as the move would not have been allowed
                    // if the parent had any.

                    //refind the group in the parent collection as it will be a different instance to the one passed in.
                    addedReturnGroup = parentToAssign.ChildList.FirstOrDefault(c => c.Code.Equals(groupCode));
                }
            }

            //update the level id of the return group
            if (addedReturnGroup != null)
            {
                LocationLevel newLevel = parentToAssign.GetAssociatedLevel();
                if (newLevel != null) { newLevel = newLevel.ChildLevel; }

                if (newLevel != null && addedReturnGroup.LocationLevelId != newLevel.Id)
                {
                    addedReturnGroup.LocationLevelId = newLevel.Id;
                }
            }


            return addedReturnGroup;
        }

        /// <summary>
        /// Adds the given level to the structure underneath the given parent
        /// </summary>
        /// <param name="levelToAdd"></param>
        /// <param name="parentLevel"></param>
        public void AddLevel(LocationLevel levelToAdd, LocationLevel parentLevel)
        {
            //get the parents current child
            LocationLevel currentParentChild = parentLevel.ChildLevel;
            if (currentParentChild != null)
            {
                //need to move the child to be a child of the new level instead
                levelToAdd.ChildLevel = currentParentChild;
                //remove from its old parent
                parentLevel.ChildLevel = null;

                //now set the new child and the child of the parent
                parentLevel.ChildLevel = levelToAdd;


                //need to insert an extra unit underneath all units on the parent level
                Int32 parentLevelId = parentLevel.Id;
                IEnumerable<LocationGroup> parentLevelUnits = this.EnumerateAllGroups().Where(u => u.LocationLevelId == parentLevelId);

                //cycle through all the parents
                Int32 addedGroupNumber = 1;
                foreach (LocationGroup unit in parentLevelUnits)
                {
                    //get a list of child units belonging to the parent
                    List<LocationGroup> parentChildUnits = unit.ChildList.ToList();

                    if (parentChildUnits.Count > 0)
                    {
                        //create a new child unit on the new level
                        LocationGroup newChildUnit = LocationGroup.NewLocationGroup(levelToAdd.Id);
                        newChildUnit.Code = GetNextAvailableDefaultGroupCode();
                        newChildUnit.Name = String.Format(Message.LocationGroup_DefaultName, levelToAdd.Name, addedGroupNumber);

                        //add the children to this new unit
                        newChildUnit.ChildList.AddList(parentChildUnits);

                        //clear the parent child collection and add the new child unit
                        unit.ChildList.Clear();
                        unit.ChildList.Add(newChildUnit);
                    }

                    addedGroupNumber++;
                }
            }
            else
            {
                parentLevel.ChildLevel = levelToAdd;
            }
        }

        /// <summary>
        /// Removes the level from the structure and moves all its children up
        /// </summary>
        /// <param name="levelToRemove"></param>
        public void RemoveLevel(LocationLevel levelToRemove)
        {
            LocationLevel levelParent = levelToRemove.ParentLevel;
            LocationLevel levelChild = levelToRemove.ChildLevel;

            //add the level child to the parent
            levelParent.ChildLevel = levelChild;

            //now null it off from the level that is being removed
            levelToRemove.ChildLevel = null;

            //cycle through the parent units deleting all direct child units
            //as they will be on the removed level
            //and shifting all their children up
            Int32 parentLevelId = levelParent.Id;
            IEnumerable<LocationGroup> parentLevelUnits =
                this.EnumerateAllGroups().Where(u => u.LocationLevelId == parentLevelId);

            foreach (LocationGroup parentUnit in parentLevelUnits)
            {
                List<LocationGroup> childUnitsToRemove = parentUnit.ChildList.ToList();
                foreach (LocationGroup childToRemove in childUnitsToRemove)
                {
                    //move any locations from the actual unit being removed to the parent
                    List<LocationGroupLocation> childAssignedLocations = childToRemove.AssignedLocations.ToList();
                    parentUnit.AssignedLocations.AddList(childAssignedLocations);
                    childToRemove.AssignedLocations.RemoveList(childAssignedLocations);

                    //move any child groups to the parent
                    if (childToRemove.ChildList.Count > 0)
                    {
                        List<LocationGroup> removeUnitChildren = childToRemove.ChildList.ToList();

                        //add to the list of the parent
                        parentUnit.ChildList.AddList(removeUnitChildren);

                        //remove from the original unit that is being deleted
                        childToRemove.ChildList.Clear();
                    }

                    //remove the child from its parent
                    parentUnit.ChildList.Remove(childToRemove);
                }
            }

        }

        #endregion

        #endregion

        #region Helpers

        /// <summary>
        /// Returns a list of levels in the structure
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<LocationLevel> FetchAllLevels()
        {
            List<LocationLevel> returnList = new List<LocationLevel>();

            //move down through the levels adding each one
            LocationLevel currentLevel = this.RootLevel;
            while (currentLevel != null)
            {
                returnList.Add(currentLevel);
                currentLevel = currentLevel.ChildLevel;
            }

            return returnList.AsReadOnly();
        }

        /// <summary>
        /// Returns a list of all groups in the structure 
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<LocationGroup> FetchAllGroups()
        {
            return this.RootGroup.FetchAllChildGroups().ToList().AsReadOnly();
        }

        #endregion

    }

}