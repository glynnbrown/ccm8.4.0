﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//	Created (Auto-generated)
// V8-31105 : L.Ineson
//  Added PlanogramIsProportional property
#endregion
#region Version History: (CCM 830)
// V8-32636 : A.Probyn
//  Added PlanogramAnnotations
#endregion
#region Version History: (CCM 832)
// CCM-18477 : G.Richards
//  Defaulted the font to Arial Unicode MS.
#endregion
#endregion


using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using System.Windows;
using Galleria.Ccm.Resources.Language;
using System.Windows.Media;
using Galleria.Framework.ViewModel;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// PrintTemplateComponentDetail Model object
    /// </summary>
    [Serializable]
    public sealed partial class PrintTemplateComponentDetail : ModelObject<PrintTemplateComponentDetail>
    {
        #region Constants
        private const String _defaultFontName = "Arial Unicode MS";

        public const String SpecialFieldNameDate = "Date";
        public const String SpecialFieldNameDateTime = "DateTime";
        public const String SpecialFieldNamePageNumber = "PageNumber";
        public const String SpecialFieldNameTotalPages = "TotalPages";
        #endregion

        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PrintTemplateComponent Parent
        {
            get {return base.Parent as PrintTemplateComponent;}
        }

        #endregion

        #region Properties

        #region Planogram Component Properties

        #region PlanogramViewType

        /// <summary>
        /// PlanogramViewType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PrintTemplatePlanogramViewType> PlanogramViewTypeProperty =
          RegisterModelProperty<PrintTemplatePlanogramViewType>(c => c.PlanogramViewType, Message.PrintTemplateComponentDetail_PlanogramViewType);
        /// <summary>
        /// Gets/Sets the view to display for a planogram component
        /// </summary>
        public PrintTemplatePlanogramViewType PlanogramViewType
        {
            get { return GetProperty<PrintTemplatePlanogramViewType>(PlanogramViewTypeProperty); }
            set { SetProperty<PrintTemplatePlanogramViewType>(PlanogramViewTypeProperty, value); }
        }
        #endregion

        #region PlanogramPositions

        /// <summary>
        /// PlanogramPositionUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramPositionsProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramPositions, Message.SystemSettings_Positions);

        /// <summary>
        /// Gets/Sets whether the plan view should show positions
        /// </summary>
        public Boolean PlanogramPositions
        {
            get { return GetProperty<Boolean>(PlanogramPositionsProperty); }
            set { SetProperty<Boolean>(PlanogramPositionsProperty, value); }
        }

        #endregion

        #region PlanogramPositionUnits

        /// <summary>
        /// PlanogramPositionUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramPositionUnitsProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramPositionUnits, Message.SystemSettings_PositionUnits);

        /// <summary>
        /// Gets/Sets whether the plan view should show position units
        /// </summary>
        public Boolean PlanogramPositionUnits
        {
            get { return GetProperty<Boolean>(PlanogramPositionUnitsProperty); }
            set { SetProperty<Boolean>(PlanogramPositionUnitsProperty, value); }
        }

        #endregion

        #region PlanogramProductImages

        /// <summary>
        /// PlanogramProductImages property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramProductImagesProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramProductImages, Message.SystemSettings_ProductImages);

        /// <summary>
        /// Gets/Sets whether the plan view should show product images.
        /// </summary>
        public Boolean PlanogramProductImages
        {
            get { return GetProperty<Boolean>(PlanogramProductImagesProperty); }
            set { SetProperty<Boolean>(PlanogramProductImagesProperty, value); }
        }

        #endregion

        #region PlanogramProductShapes

        /// <summary>
        /// PlanogramProductShapes property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramProductShapesProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramProductShapes, Message.SystemSettings_ProductShapes);

        /// <summary>
        /// Gets/Sets whether the plan view should show product shapes
        /// </summary>
        public Boolean PlanogramProductShapes
        {
            get { return GetProperty<Boolean>(PlanogramProductShapesProperty); }
            set { SetProperty<Boolean>(PlanogramProductShapesProperty, value); }
        }

        #endregion

        #region PlanogramProductFillColours

        /// <summary>
        /// PlanogramProductFillColours property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramProductFillColoursProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramProductFillColours, Message.SystemSettings_ProductFillColours);

        /// <summary>
        /// Gets/Sets whether the plan view should show product fill patterns
        /// </summary>
        public Boolean PlanogramProductFillColours
        {
            get { return GetProperty<Boolean>(PlanogramProductFillColoursProperty); }
            set { SetProperty<Boolean>(PlanogramProductFillColoursProperty, value); }
        }

        #endregion

        #region PlanogramProductFillPatterns

        /// <summary>
        /// PlanogramProductFillPatterns property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramProductFillPatternsProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramProductFillPatterns, Message.SystemSettings_ProductFillPatterns);

        /// <summary>
        /// Gets/Sets whether the plan view should show product fill patterns
        /// </summary>
        public Boolean PlanogramProductFillPatterns
        {
            get { return GetProperty<Boolean>(PlanogramProductFillPatternsProperty); }
            set { SetProperty<Boolean>(PlanogramProductFillPatternsProperty, value); }
        }

        #endregion

        #region PlanogramFixtureImages

        /// <summary>
        /// PlanogramFixtureImages property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramFixtureImagesProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramFixtureImages, Message.SystemSettings_FixtureImages);

        /// <summary>
        /// Gets/Sets whether the plan view should show fixture images
        /// </summary>
        public Boolean PlanogramFixtureImages
        {
            get { return GetProperty<Boolean>(PlanogramFixtureImagesProperty); }
            set { SetProperty<Boolean>(PlanogramFixtureImagesProperty, value); }
        }

        #endregion

        #region PlanogramFixtureFillPatterns

        /// <summary>
        /// PlanogramFixtureFillPatterns property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramFixtureFillPatternsProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramFixtureFillPatterns, Message.SystemSettings_FixtureFillPatterns);

        /// <summary>
        /// Gets/Sets whether the plan view should show fixture fill patterns
        /// </summary>
        public Boolean PlanogramFixtureFillPatterns
        {
            get { return GetProperty<Boolean>(PlanogramFixtureFillPatternsProperty); }
            set { SetProperty<Boolean>(PlanogramFixtureFillPatternsProperty, value); }
        }

        #endregion

        #region PlanogramChestWalls

        /// <summary>
        /// PlanogramChestWalls property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramChestWallsProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramChestWalls, Message.SystemSettings_ChestWalls);

        /// <summary>
        /// Gets/Sets whether the plan view should show chest walls
        /// </summary>
        public Boolean PlanogramChestWalls
        {
            get { return GetProperty<Boolean>(PlanogramChestWallsProperty); }
            set { SetProperty<Boolean>(PlanogramChestWallsProperty, value); }
        }

        #endregion

        #region PlanogramRotateTopDownComponents

        /// <summary>
        /// ChestsTopDown property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramRotateTopDownComponentsProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramRotateTopDownComponents, Message.SystemSettings_RotateTopDownComponents);

        /// <summary>
        /// Gets/Sets whether the plan view should show top down components as rotated
        /// </summary>
        public Boolean PlanogramRotateTopDownComponents
        {
            get { return GetProperty<Boolean>(PlanogramRotateTopDownComponentsProperty); }
            set { SetProperty<Boolean>(PlanogramRotateTopDownComponentsProperty, value); }
        }

        #endregion

        #region PlanogramShelfRisers

        /// <summary>
        /// ShelfRisers property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramShelfRisersProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramShelfRisers, Message.SystemSettings_ShelfRisers);

        /// <summary>
        /// Gets/Sets whether the plan view should show shelf risers
        /// </summary>
        public Boolean PlanogramShelfRisers
        {
            get { return GetProperty<Boolean>(PlanogramShelfRisersProperty); }
            set { SetProperty<Boolean>(PlanogramShelfRisersProperty, value); }
        }

        #endregion

        #region PlanogramNotches

        /// <summary>
        /// PlanogramNotches property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramNotchesProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramNotches, Message.SystemSettings_Notches);

        /// <summary>
        ///Gets/Sets whether the plan view should show notches
        /// </summary>
        public Boolean PlanogramNotches
        {
            get { return GetProperty<Boolean>(PlanogramNotchesProperty); }
            set { SetProperty<Boolean>(PlanogramNotchesProperty, value); }
        }

        #endregion

        #region PlanogramPegHoles

        /// <summary>
        /// PlanogramPegholes property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramPegHolesProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramPegHoles, Message.SystemSettings_PegHoles);

        /// <summary>
        /// Gets/Sets whether the plan view should show peg holes
        /// </summary>
        public Boolean PlanogramPegHoles
        {
            get { return GetProperty<Boolean>(PlanogramPegHolesProperty); }
            set { SetProperty<Boolean>(PlanogramPegHolesProperty, value); }
        }

        #endregion

        #region PlanogramPegs

        /// <summary>
        /// PlanogramPegs property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramPegsProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramPegs, Message.SystemSettings_Pegs);

        /// <summary>
        /// Gets/Sets whether the plan view should show pegs
        /// </summary>
        public Boolean PlanogramPegs
        {
            get { return GetProperty<Boolean>(PlanogramPegsProperty); }
            set { SetProperty<Boolean>(PlanogramPegsProperty, value); }
        }

        #endregion

        #region PlanogramDividerLines

        /// <summary>
        /// PlanogramDividerLines property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramDividerLinesProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramDividerLines, Message.SystemSettings_DividerLines);

        /// <summary>
        /// Gets/Sets whether the plan view should show divider lines
        /// </summary>
        public Boolean PlanogramDividerLines
        {
            get { return GetProperty<Boolean>(PlanogramDividerLinesProperty); }
            set { SetProperty<Boolean>(PlanogramDividerLinesProperty, value); }
        }

        #endregion

        #region PlanogramDividers

        /// <summary>
        /// PlanogramDividers property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramDividersProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramDividers, Message.SystemSettings_Dividers);

        /// <summary>
        /// Gets/Sets whether the plan view should show dividers
        /// </summary>
        public Boolean PlanogramDividers
        {
            get { return GetProperty<Boolean>(PlanogramDividersProperty); }
            set { SetProperty<Boolean>(PlanogramDividersProperty, value); }
        }

        #endregion

        #region PlanogramTextBoxes

        /// <summary>
        /// PlanogramTextBoxes property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramTextBoxesProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramTextBoxes, Message.PrintTemplateComponentDetail_TextBoxes);

        /// <summary>
        /// Gets/Sets whether the plan view should show textboxes
        /// </summary>
        public Boolean PlanogramTextBoxes
        {
            get { return GetProperty<Boolean>(PlanogramTextBoxesProperty); }
            set { SetProperty<Boolean>(PlanogramTextBoxesProperty, value); }
        }

        #endregion

        #region PlanogramAnnotations

        /// <summary>
        /// PlanogramAnnotations property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramAnnotationsProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramAnnotations, Message.SystemSettings_Annotations);

        /// <summary>
        /// Gets/Sets whether the plan view should show Annotations
        /// </summary>
        public Boolean PlanogramAnnotations
        {
            get { return GetProperty<Boolean>(PlanogramAnnotationsProperty); }
            set { SetProperty<Boolean>(PlanogramAnnotationsProperty, value); }
        }

        #endregion

        #region PlanogramProductLabelId
        /// <summary>
        /// PlanogramProductLabelId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramProductLabelIdProperty =
          RegisterModelProperty<Object>(c => c.PlanogramProductLabelId);
        /// <summary>
        /// Gets/Sets the id of the default product label
        /// </summary>
        public Object PlanogramProductLabelId
        {
            get { return GetProperty<Object>(PlanogramProductLabelIdProperty); }
            set { SetProperty<Object>(PlanogramProductLabelIdProperty, value); }
        }
        #endregion

        #region PlanogramUseLiveProductLabel
        /// <summary>
        /// PlanogramUseLiveProductLabel property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramUseLiveProductLabelProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramUseLiveProductLabel);
        /// <summary>
        /// Gets/Sets whether the plan view should try to use the live label on the current plan
        /// before falling back on the default.
        /// </summary>
        public Boolean PlanogramUseLiveProductLabel
        {
            get { return GetProperty<Boolean>(PlanogramUseLiveProductLabelProperty); }
            set { SetProperty<Boolean>(PlanogramUseLiveProductLabelProperty, value); }
        }
        #endregion

        #region PlanogramFixtureLabelId
        /// <summary>
        /// PlanogramFixtureLabelId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramFixtureLabelIdProperty =
          RegisterModelProperty<Object>(c => c.PlanogramFixtureLabelId);
        /// <summary>
        /// Gets/Sets the id of the default fixture label
        /// </summary>
        public Object PlanogramFixtureLabelId
        {
            get { return GetProperty<Object>(PlanogramFixtureLabelIdProperty); }
            set { SetProperty<Object>(PlanogramFixtureLabelIdProperty, value); }
        }
        #endregion

        #region PlanogramUseLiveFixtureLabel
        /// <summary>
        /// PlanogramUseLiveFixtureLabel property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramUseLiveFixtureLabelProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramUseLiveFixtureLabel);
        /// <summary>
        /// Gets/Sets whether the plan view should try to use the live label on the current plan
        /// before falling back on the default.
        /// </summary>
        public Boolean PlanogramUseLiveFixtureLabel
        {
            get { return GetProperty<Boolean>(PlanogramUseLiveFixtureLabelProperty); }
            set { SetProperty<Boolean>(PlanogramUseLiveFixtureLabelProperty, value); }
        }
        #endregion

        #region PlanogramHighlightId
        /// <summary>
        /// PlanogramHighlightId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> PlanogramHighlightIdProperty =
          RegisterModelProperty<Object>(c => c.PlanogramHighlightId);
        /// <summary>
        /// Gets/Sets the id of the default highlight
        /// </summary>
        public Object PlanogramHighlightId
        {
            get { return GetProperty<Object>(PlanogramHighlightIdProperty); }
            set { SetProperty<Object>(PlanogramHighlightIdProperty, value); }
        }
        #endregion

        #region PlanogramUseLiveHighlight
        /// <summary>
        /// PlanogramUseLiveHighlight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramUseLiveHighlightProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramUseLiveHighlight);
        /// <summary>
        /// Gets/Sets whether the plan view should try to use the live highlight on the current plan
        /// before falling back on the default.
        /// </summary>
        public Boolean PlanogramUseLiveHighlight
        {
            get { return GetProperty<Boolean>(PlanogramUseLiveHighlightProperty); }
            set { SetProperty<Boolean>(PlanogramUseLiveHighlightProperty, value); }
        }
        #endregion

        #region PlanogramCameraLookDirection

        /// <summary>
        /// PlanogramCameraLookDirection property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> PlanogramCameraLookDirectionProperty =
         RegisterModelProperty<String>(c => c.PlanogramCameraLookDirection);

        /// <summary>
        /// Gets/Sets the look direction of the planogram camera
        /// 3d view only.
        /// </summary>
        public String PlanogramCameraLookDirection
        {
            get { return GetProperty<String>(PlanogramCameraLookDirectionProperty); }
            set { SetProperty<String>(PlanogramCameraLookDirectionProperty, value); }
        }

        #endregion

        #region PlanogramCameraPosition

        /// <summary>
        /// PlanogramCameraPosition property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> PlanogramCameraPositionProperty =
         RegisterModelProperty<String>(c => c.PlanogramCameraPosition);

        /// <summary>
        /// Gets/Sets the position of the planogram camera
        /// 3d view only.
        /// </summary>
        public String PlanogramCameraPosition
        {
            get { return GetProperty<String>(PlanogramCameraPositionProperty); }
            set { SetProperty<String>(PlanogramCameraPositionProperty, value); }
        }

        #endregion

        #region PlanogramIsProportional

        /// <summary>
        /// PlanogramIsProportional property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> PlanogramIsProportionalProperty =
          RegisterModelProperty<Boolean>(c => c.PlanogramIsProportional);

        /// <summary>
        /// Gets/Sets whether the plan view should show pegs
        /// </summary>
        public Boolean PlanogramIsProportional
        {
            get { return GetProperty<Boolean>(PlanogramIsProportionalProperty); }
            set { SetProperty<Boolean>(PlanogramIsProportionalProperty, value); }
        }

        #endregion

        #endregion

        #region TextBox Component Properties

        #region TextBoxText
        /// <summary>
        /// TextBoxText property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> TextBoxTextProperty =
           RegisterModelProperty<String>(c => c.TextBoxText);
        /// <summary>
        /// Gets/Sets the textbox Text
        /// </summary>
        public String TextBoxText
        {
            get { return GetProperty<String>(TextBoxTextProperty); }
            set { SetProperty<String>(TextBoxTextProperty, value); }
        }
        #endregion

        #region TextBoxFontSize
        /// <summary>
        /// TextBoxFontSize property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> TextBoxFontSizeProperty =
           RegisterModelProperty<Byte>(c => c.TextBoxFontSize);
        /// <summary>
        /// Gets/Sets the textbox font size
        /// </summary>
        public Byte TextBoxFontSize
        {
            get { return GetProperty<Byte>(TextBoxFontSizeProperty); }
            set { SetProperty<Byte>(TextBoxFontSizeProperty, value); }
        }
        #endregion

        #region TextBoxIsFontBold

        /// <summary>
        /// TextBoxIsFontBold property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> TextBoxIsFontBoldProperty =
           RegisterModelProperty<Boolean>(c => c.TextBoxIsFontBold);
        /// <summary>
        /// Gets/Sets the textbox font Bold
        /// </summary>
        public Boolean TextBoxIsFontBold
        {
            get { return GetProperty<Boolean>(TextBoxIsFontBoldProperty); }
            set  {  SetProperty<Boolean>(TextBoxIsFontBoldProperty, value);}
        }

        #endregion

        #region TextBoxFontName
        /// <summary>
        /// TextBoxFontName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> TextBoxFontNameProperty =
           RegisterModelProperty<String>(c => c.TextBoxFontName);
        /// <summary>
        /// Gets/Sets the textbox font
        /// </summary>
        public String TextBoxFontName
        {
            get { return GetProperty<String>(TextBoxFontNameProperty); }
            set { SetProperty<String>(TextBoxFontNameProperty, value); }
        }

        #endregion

        #region TextBoxForeground
        /// <summary>
        /// TextBoxForeground property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> TextBoxForegroundProperty =
           RegisterModelProperty<Int32>(c => c.TextBoxForeground);
        /// <summary>
        /// Gets/Sets the textbox foreground colour
        /// </summary>
        public Int32 TextBoxForeground
        {
            get { return GetProperty<Int32>(TextBoxForegroundProperty); }
            set {SetProperty<Int32>(TextBoxForegroundProperty, value);}
        }

        #endregion

        #region TextBoxTextAlignment
        /// <summary>
        /// TextBoxTextAlignment property definition
        /// </summary>
        public static readonly ModelPropertyInfo<TextAlignment> TextBoxTextAlignmentProperty =
           RegisterModelProperty<TextAlignment>(c => c.TextBoxTextAlignment);
        /// <summary>
        /// Gets/Sets the textbox Text Alignment
        /// </summary>
        public TextAlignment TextBoxTextAlignment
        {
            get { return GetProperty<TextAlignment>(TextBoxTextAlignmentProperty); }
            set { SetProperty<TextAlignment>(TextBoxTextAlignmentProperty, value); }
        }
        #endregion

        #region TextBoxIsFontItalic
        /// <summary>
        /// TextBoxIsFontItalic property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> TextBoxIsFontItalicProperty =
           RegisterModelProperty<Boolean>(c => c.TextBoxIsFontItalic);
        /// <summary>
        /// Gets/Sets the textbox font italic
        /// </summary>
        public Boolean TextBoxIsFontItalic
        {
            get { return GetProperty<Boolean>(TextBoxIsFontItalicProperty); }
            set {SetProperty<Boolean>(TextBoxIsFontItalicProperty, value);}
        }
        #endregion

        #region TextBoxIsFontUnderlined
        /// <summary>
        /// TextBoxIsFontUnderlined property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> TextBoxIsFontUnderlinedProperty =
           RegisterModelProperty<Boolean>(c => c.TextBoxIsFontUnderlined);
        /// <summary>
        /// Gets/Sets the textbox font underlined
        /// </summary>
        public Boolean TextBoxIsFontUnderlined
        {
            get { return GetProperty<Boolean>(TextBoxIsFontUnderlinedProperty); }
            set { SetProperty<Boolean>(TextBoxIsFontUnderlinedProperty, value);}
        }
        #endregion

       

        ///// <summary>
        ///// Gets/Sets the textbox text for the UI
        ///// </summary>
        //String _textBoxTextUI = String.Empty;
        //public String TextBoxTextUI
        //{
        //    get
        //    {
        //        String textBoxText = GetProperty<String>(TextBoxTextProperty);
        //        _textBoxTextUI = String.Empty;

        //        //Convert textbox text string to replace any field locations with placeholders
        //        if (!String.IsNullOrEmpty(textBoxText))
        //        {
        //            if (textBoxText.Contains("<") && textBoxText.Contains(">"))
        //            {
        //                _textBoxTextUI = PrintTemplateComponentInsertTextHelper.ConvertEnumToPlaceholder(textBoxText);
        //            }
        //            else
        //            {
        //                _textBoxTextUI = textBoxText;
        //            }
        //        }

        //        return _textBoxTextUI;
        //    }
        //    set
        //    {
        //        //Convert UI string to replace any placeholders with field locations
        //        this._textBoxTextUI = value;
        //        String textBoxText = String.Empty;

        //        if (!String.IsNullOrEmpty(_textBoxTextUI))
        //        {
        //            if (_textBoxTextUI.Contains("<") && _textBoxTextUI.Contains(">"))
        //            {
        //                textBoxText = PrintOptionComponentInsertTextHelper.ConvertPlaceholderToEnum(_textBoxTextUI);
        //            }
        //            else
        //            {
        //                textBoxText = _textBoxTextUI;
        //            }
        //        }

        //        this.TextBoxText = textBoxText;
        //    }
        //}


        #endregion

        #region DataSheet Component Properties

        #region DataSheetId
        /// <summary>
        /// DataSheetId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> DataSheetIdProperty =
           RegisterModelProperty<Object>(c => c.DataSheetId);
        /// <summary>
        /// Gets/Sets the id of the datasheet to include.
        /// </summary>
        public Object DataSheetId
        {
            get { return GetProperty<Object>(DataSheetIdProperty); }
            set { SetProperty<Object>(DataSheetIdProperty, value); }
        }
        #endregion

        #region DataSheetStyle
        /// <summary>
        /// DataSheetStyle property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PrintTemplateComponentGridStyle> DataSheetStyleProperty =
           RegisterModelProperty<PrintTemplateComponentGridStyle>(c => c.DataSheetStyle);
        /// <summary>
        /// Gets/Sets the report style for the print option component.
        /// </summary>
        public PrintTemplateComponentGridStyle DataSheetStyle
        {
            get { return GetProperty<PrintTemplateComponentGridStyle>(DataSheetStyleProperty); }
            set { SetProperty<PrintTemplateComponentGridStyle>(DataSheetStyleProperty, value); }
        }
        #endregion

        #region DataSheetHeaderFontSize
        /// <summary>
        /// DataSheetHeaderFontSize property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> DataSheetHeaderFontSizeProperty =
           RegisterModelProperty<Byte>(c => c.DataSheetHeaderFontSize);
        /// <summary>
        /// Gets/Sets the textbox font size
        /// </summary>
        public Byte DataSheetHeaderFontSize
        {
            get { return GetProperty<Byte>(DataSheetHeaderFontSizeProperty); }
            set { SetProperty<Byte>(DataSheetHeaderFontSizeProperty, value); }
        }
        #endregion

        #region DataSheetHeaderIsFontBold
        /// <summary>
        /// DataSheetHeaderIsFontBold property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> DataSheetHeaderIsFontBoldProperty =
           RegisterModelProperty<Boolean>(c => c.DataSheetHeaderIsFontBold);
        /// <summary>
        /// Gets/Sets the textbox font Bold
        /// </summary>
        public Boolean DataSheetHeaderIsFontBold
        {
            get { return GetProperty<Boolean>(DataSheetHeaderIsFontBoldProperty); }
            set { SetProperty<Boolean>(DataSheetHeaderIsFontBoldProperty, value); }
        }
        #endregion

        #region DataSheetHeaderFontName
        /// <summary>
        /// DataSheetHeaderFontName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DataSheetHeaderFontNameProperty =
           RegisterModelProperty<String>(c => c.DataSheetHeaderFontName);
        /// <summary>
        /// Gets/Sets the textbox font
        /// </summary>
        public String DataSheetHeaderFontName
        {
            get { return GetProperty<String>(DataSheetHeaderFontNameProperty); }
            set { SetProperty<String>(DataSheetHeaderFontNameProperty, value); }
        }
        #endregion

        #region DataSheetHeaderForeground
        /// <summary>
        /// DataSheetHeaderForeground property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> DataSheetHeaderForegroundProperty =
           RegisterModelProperty<Int32>(c => c.DataSheetHeaderForeground);
        /// <summary>
        /// Gets/Sets the textbox foreground colour
        /// </summary>
        public Int32 DataSheetHeaderForeground
        {
            get { return GetProperty<Int32>(DataSheetHeaderForegroundProperty); }
            set { SetProperty<Int32>(DataSheetHeaderForegroundProperty, value); }
        }
        #endregion

        #region DataSheetHeaderTextAlignment
        /// <summary>
        /// DataSheetHeaderTextAlignment property definition
        /// </summary>
        public static readonly ModelPropertyInfo<TextAlignment> DataSheetHeaderTextAlignmentProperty =
           RegisterModelProperty<TextAlignment>(c => c.DataSheetHeaderTextAlignment);
        /// <summary>
        /// Gets/Sets the textbox Text Alignment
        /// </summary>
        public TextAlignment DataSheetHeaderTextAlignment
        {
            get { return GetProperty<TextAlignment>(DataSheetHeaderTextAlignmentProperty); }
            set { SetProperty<TextAlignment>(DataSheetHeaderTextAlignmentProperty, value); }
        }
        #endregion

        #region DataSheetHeaderIsFontItalic
        /// <summary>
        /// DataSheetHeaderIsFontItalic property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> DataSheetHeaderIsFontItalicProperty =
           RegisterModelProperty<Boolean>(c => c.DataSheetHeaderIsFontItalic);
        /// <summary>
        /// Gets/Sets the textbox font italic
        /// </summary>
        public Boolean DataSheetHeaderIsFontItalic
        {
            get { return GetProperty<Boolean>(DataSheetHeaderIsFontItalicProperty); }
            set { SetProperty<Boolean>(DataSheetHeaderIsFontItalicProperty, value); }
        }
        #endregion

        #region DataSheetHeaderIsFontUnderlined
        /// <summary>
        /// DataSheetHeaderIsFontUnderlined property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> DataSheetHeaderIsFontUnderlinedProperty =
           RegisterModelProperty<Boolean>(c => c.DataSheetHeaderIsFontUnderlined);
        /// <summary>
        /// Gets/Sets the textbox font underlined
        /// </summary>
        public Boolean DataSheetHeaderIsFontUnderlined
        {
            get { return GetProperty<Boolean>(DataSheetHeaderIsFontUnderlinedProperty); }
            set { SetProperty<Boolean>(DataSheetHeaderIsFontUnderlinedProperty, value); }
        }
        #endregion

        #region DataSheetRowFontSize
        /// <summary>
        /// DataSheetRowFontSize property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> DataSheetRowFontSizeProperty =
           RegisterModelProperty<Byte>(c => c.DataSheetRowFontSize);
        /// <summary>
        /// Gets/Sets the textbox font size
        /// </summary>
        public Byte DataSheetRowFontSize
        {
            get { return GetProperty<Byte>(DataSheetRowFontSizeProperty); }
            set { SetProperty<Byte>(DataSheetRowFontSizeProperty, value); }
        }
        #endregion

        #region DataSheetRowIsFontBold
        /// <summary>
        /// DataSheetRowIsFontBold property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> DataSheetRowIsFontBoldProperty =
           RegisterModelProperty<Boolean>(c => c.DataSheetRowIsFontBold);
        /// <summary>
        /// Gets/Sets the textbox font Bold
        /// </summary>
        public Boolean DataSheetRowIsFontBold
        {
            get { return GetProperty<Boolean>(DataSheetRowIsFontBoldProperty); }
            set { SetProperty<Boolean>(DataSheetRowIsFontBoldProperty, value); }
        }
        #endregion

        #region DataSheetRowFontName
        /// <summary>
        /// DataSheetRowFontName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DataSheetRowFontNameProperty =
           RegisterModelProperty<String>(c => c.DataSheetRowFontName);
        /// <summary>
        /// Gets/Sets the textbox font
        /// </summary>
        public String DataSheetRowFontName
        {
            get { return GetProperty<String>(DataSheetRowFontNameProperty); }
            set { SetProperty<String>(DataSheetRowFontNameProperty, value); }
        }
        #endregion

        #region DataSheetRowForeground
        /// <summary>
        /// DataSheetRowForeground property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> DataSheetRowForegroundProperty =
           RegisterModelProperty<Int32>(c => c.DataSheetRowForeground);
        /// <summary>
        /// Gets/Sets the textbox foreground colour
        /// </summary>
        public Int32 DataSheetRowForeground
        {
            get { return GetProperty<Int32>(DataSheetRowForegroundProperty); }
            set { SetProperty<Int32>(DataSheetRowForegroundProperty, value); }
        }
        #endregion

        #region DataSheetRowTextAlignment
        /// <summary>
        /// DataSheetRowTextAlignment property definition
        /// </summary>
        public static readonly ModelPropertyInfo<TextAlignment> DataSheetRowTextAlignmentProperty =
           RegisterModelProperty<TextAlignment>(c => c.DataSheetRowTextAlignment);
        /// <summary>
        /// Gets/Sets the textbox Text Alignment
        /// </summary>
        public TextAlignment DataSheetRowTextAlignment
        {
            get { return GetProperty<TextAlignment>(DataSheetRowTextAlignmentProperty); }
            set { SetProperty<TextAlignment>(DataSheetRowTextAlignmentProperty, value); }
        }
        #endregion

        #region DataSheetRowIsFontItalic
        /// <summary>
        /// DataSheetRowIsFontItalic property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> DataSheetRowIsFontItalicProperty =
           RegisterModelProperty<Boolean>(c => c.DataSheetRowIsFontItalic);
        /// <summary>
        /// Gets/Sets the textbox font italic
        /// </summary>
        public Boolean DataSheetRowIsFontItalic
        {
            get { return GetProperty<Boolean>(DataSheetRowIsFontItalicProperty); }
            set { SetProperty<Boolean>(DataSheetRowIsFontItalicProperty, value); }
        }
        #endregion

        #region DataSheetRowIsFontUnderlined
        /// <summary>
        /// DataSheetRowIsFontUnderlined property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> DataSheetRowIsFontUnderlinedProperty =
           RegisterModelProperty<Boolean>(c => c.DataSheetRowIsFontUnderlined);
        /// <summary>
        /// Gets/Sets the textbox font underlined
        /// </summary>
        public Boolean DataSheetRowIsFontUnderlined
        {
            get { return GetProperty<Boolean>(DataSheetRowIsFontUnderlinedProperty); }
            set { SetProperty<Boolean>(DataSheetRowIsFontUnderlinedProperty, value); }
        }
        #endregion

        #endregion

        #region Line Component Properties

        #region LineEndX
        /// <summary>
        /// LineEndX property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Double> LineEndXProperty =
           RegisterModelProperty<Double>(c => c.LineEndX);
        /// <summary>
        /// Gets/Sets the line end x position
        /// </summary>
        public Double LineEndX
        {
            get { return GetProperty<Double>(LineEndXProperty); }
            set { SetProperty<Double>(LineEndXProperty, value); }
        }
        #endregion

        #region LineEndY
        /// <summary>
        /// LineEndY property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Double> LineEndYProperty =
           RegisterModelProperty<Double>(c => c.LineEndY);
        /// <summary>
        /// Gets/Sets the line end Y position
        /// </summary>
        public Double LineEndY
        {
            get { return GetProperty<Double>(LineEndYProperty); }
            set { SetProperty<Double>(LineEndYProperty, value); }
        }
        #endregion

        #region LineForeground
        /// <summary>
        /// LineForeground property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> LineForegroundProperty =
           RegisterModelProperty<Int32>(c => c.LineForeground);
        /// <summary>
        /// Gets/Sets the Line foreground colour
        /// </summary>
        public Int32 LineForeground
        {
            get { return GetProperty<Int32>(LineForegroundProperty); }
            set
            {
                SetProperty<Int32>(LineForegroundProperty, value);
                OnPropertyChanged("ResolvedLineColour");
            }
        }
        #endregion

        #region LineWeight
        /// <summary>
        /// LineWeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Double> LineWeightProperty =
           RegisterModelProperty<Double>(c => c.LineWeight);
        /// <summary>
        /// Gets/Sets the Line weight
        /// </summary>
        public Double LineWeight
        {
            get { return GetProperty<Double>(LineWeightProperty); }
            set { SetProperty<Double>(LineWeightProperty, value); }
        }
        #endregion

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static PrintTemplateComponentDetail NewPrintTemplateComponentDetail(PrintTemplateComponentType componentType)
        {
            PrintTemplateComponentDetail item = new PrintTemplateComponentDetail();
            item.Create(componentType);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(PrintTemplateComponentType componentType)
        {
            LoadDefaults(componentType);

            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Loads the defaults for this component
        /// </summary>
        private void LoadDefaults(PrintTemplateComponentType componentType)
        {

            switch (componentType)
            {
                case PrintTemplateComponentType.Planogram:
                    this.LoadProperty<PrintTemplatePlanogramViewType>(PlanogramViewTypeProperty, PrintTemplatePlanogramViewType.Design);
                    this.LoadProperty<Boolean>(PlanogramPositionsProperty, true);
                    this.LoadProperty<Boolean>(PlanogramPositionUnitsProperty, true);
                    this.LoadProperty<Boolean>(PlanogramProductImagesProperty, false);
                    this.LoadProperty<Boolean>(PlanogramProductShapesProperty, false);
                    this.LoadProperty<Boolean>(PlanogramProductFillColoursProperty, true);
                    this.LoadProperty<Boolean>(PlanogramProductFillPatternsProperty, false);
                    this.LoadProperty<Boolean>(PlanogramFixtureImagesProperty, false);
                    this.LoadProperty<Boolean>(PlanogramFixtureFillPatternsProperty, false);
                    this.LoadProperty<Boolean>(PlanogramChestWallsProperty, true);
                    this.LoadProperty<Boolean>(PlanogramRotateTopDownComponentsProperty, true);
                    this.LoadProperty<Boolean>(PlanogramShelfRisersProperty, true);
                    this.LoadProperty<Boolean>(PlanogramNotchesProperty, true);
                    this.LoadProperty<Boolean>(PlanogramPegHolesProperty, true);
                    this.LoadProperty<Boolean>(PlanogramPegsProperty, true);
                    this.LoadProperty<Boolean>(PlanogramDividerLinesProperty, false);
                    this.LoadProperty<Boolean>(PlanogramDividersProperty, true);
                    this.LoadProperty<Boolean>(PlanogramTextBoxesProperty, true);
                    this.LoadProperty<Boolean>(PlanogramAnnotationsProperty, true);
                    this.LoadProperty<Object>(PlanogramProductLabelIdProperty, null);
                    this.LoadProperty<Boolean>(PlanogramUseLiveProductLabelProperty, false);
                    this.LoadProperty<Object>(PlanogramFixtureLabelIdProperty, null);
                    this.LoadProperty<Boolean>(PlanogramUseLiveFixtureLabelProperty, false);
                    this.LoadProperty<Object>(PlanogramHighlightIdProperty, null);
                    this.LoadProperty<Boolean>(PlanogramUseLiveHighlightProperty, false);
                    this.LoadProperty<Boolean>(PlanogramIsProportionalProperty, true);
                    break;


                case PrintTemplateComponentType.TextBox:
                    this.LoadProperty<Byte>(TextBoxFontSizeProperty, 9);
                    this.LoadProperty<Boolean>(TextBoxIsFontBoldProperty, false);
                    this.LoadProperty<String>(TextBoxFontNameProperty, _defaultFontName);
                    this.LoadProperty<Int32>(TextBoxForegroundProperty, /*Black*/-16777216);
                    this.LoadProperty<Boolean>(TextBoxIsFontItalicProperty, false);
                    this.LoadProperty<Boolean>(TextBoxIsFontUnderlinedProperty, false);
                    this.LoadProperty<TextAlignment>(TextBoxTextAlignmentProperty, TextAlignment.Left);
                    break;

                case PrintTemplateComponentType.DataSheet:
                    this.LoadProperty<Object>(DataSheetIdProperty, null);
                    this.LoadProperty<Byte>(DataSheetHeaderFontSizeProperty, 9);
                    this.LoadProperty<Boolean>(DataSheetHeaderIsFontBoldProperty, false);
                    this.LoadProperty<String>(DataSheetHeaderFontNameProperty, _defaultFontName);
                    this.LoadProperty<Int32>(DataSheetHeaderForegroundProperty, /*Black*/-16777216);
                    this.LoadProperty<Boolean>(DataSheetHeaderIsFontItalicProperty, false);
                    this.LoadProperty<Boolean>(DataSheetHeaderIsFontUnderlinedProperty, false);
                    this.LoadProperty<Byte>(DataSheetRowFontSizeProperty, 8);
                    this.LoadProperty<Boolean>(DataSheetRowIsFontBoldProperty, false);
                    this.LoadProperty<String>(DataSheetRowFontNameProperty, _defaultFontName);
                    this.LoadProperty<Int32>(DataSheetRowForegroundProperty, /*Black*/-16777216);
                    this.LoadProperty<Boolean>(DataSheetRowIsFontItalicProperty, false);
                    this.LoadProperty<Boolean>(DataSheetRowIsFontUnderlinedProperty, false);
                    this.LoadProperty<PrintTemplateComponentGridStyle>(DataSheetStyleProperty, PrintTemplateComponentGridStyle.Standard);
                    break;

                case PrintTemplateComponentType.Line:
                    this.LoadProperty<Double>(LineWeightProperty, 1);
                    this.LoadProperty<Int32>(LineForegroundProperty, /*Black*/-16777216);
                    break;
            }
        }

        /// <summary>
        /// Enumerates through all of the special field that can be used when inserting a textbox component.
        /// </summary>
        public static IEnumerable<ObjectFieldInfo> EnumerateSpecialTextFields()
        {
            Type ownerType = typeof(PrintTemplate);
            String ownerFriendlyName= String.Empty;
            String groupName = Message.PrintTemplateComponentDetail_TextBoxSpecialFieldsGroup;

            //Date
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName,
                SpecialFieldNameDate, Message.PrintTemplateComponentDetail_TextBoxDateFieldName, typeof(DateTime), ModelPropertyDisplayType.Date,
                /*isSpecial*/true, groupName);

            //DateTime
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName,
               SpecialFieldNameDateTime, Message.PrintTemplateComponentDetail_TextBoxDateTimeFieldName, typeof(DateTime), ModelPropertyDisplayType.DateTime,
                /*isSpecial*/true, groupName);

            //PageNumber
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName,
               SpecialFieldNamePageNumber, Message.PrintTemplateComponentDetail_TextBoxPageNumberFieldName, typeof(Int32), ModelPropertyDisplayType.None,
                /*isSpecial*/true, groupName);

            //TotalPages
            yield return ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName,
               SpecialFieldNameTotalPages, Message.PrintTemplateComponentDetail_TextBoxTotalPagesFieldName, typeof(Int32), ModelPropertyDisplayType.None,
                /*isSpecial*/true, groupName);
        }

        #endregion
    }

    /// <summary>
    /// Enum denoting the available planogram views for printing.
    /// </summary>
    public enum PrintTemplatePlanogramViewType
    {
        Design = 0,
        Perspective = 1,
        Front = 2,
        Back = 3,
        Top = 4,
        Bottom = 5,
        Left = 6,
        Right = 7
    }

    /// <summary>
    /// An enum which defines the style of the Print Option Component Grid
    /// </summary>
    public enum PrintTemplateComponentGridStyle
    {
        Standard = 0,   // Standard is Shaded header row and border
        Full = 1,       // Full is shaded header row and alternate grid rows lightly shaded 
        Light = 2,      // Light is alternate grid rows lightly shaded. No shading on header row
        None = 3
    }
}