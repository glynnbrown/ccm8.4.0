﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    public enum LocationSpaceFixtureType
    {
        Standard,
        Special
    }

    public static class LocationSpaceFixtureTypeHelper
    {
        public static readonly Dictionary<LocationSpaceFixtureType, String> FriendlyNames =
           new Dictionary<LocationSpaceFixtureType, String>()
            {
                {LocationSpaceFixtureType.Standard, Message.Enum_LocationSpaceFixtureType_Standard},
                {LocationSpaceFixtureType.Special, Message.Enum_LocationSpaceFixtureType_Special},
            };

        /// <summary>
        /// Dictionary with Friendly Name as key and FixtureType as value
        /// </summary>
        public static readonly Dictionary<String, LocationSpaceFixtureType> EnumFromFriendlyName =
            new Dictionary<String, LocationSpaceFixtureType>()
        {
            {Message.Enum_LocationSpaceFixtureType_Standard.ToLowerInvariant(), LocationSpaceFixtureType.Standard},
            {Message.Enum_LocationSpaceFixtureType_Special.ToLowerInvariant(), LocationSpaceFixtureType.Special}
        };
    }
}
