﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27411 : M.Pettit
//	Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    ///// <summary>
    ///// Enum representing the different thypes of user that may apply
    ///// a lock for a Planogram
    ///// </summary>
    //public enum PlanogramLockType : byte
    //{
    //    User = 0,
    //    System = 1
    //}

    ///// <summary>
    ///// Planogram Lock Type Helper Class.
    ///// </summary>
    //public static class PlanogramLockTypeHelper
    //{
    //    /// <summary>
    //    /// Holds the dictionary mapping <see cref="PlanogramLockType" /> type values to friendly names.
    //    /// </summary>
    //    public static readonly Dictionary<PlanogramLockType, String> FriendlyNames = new Dictionary
    //        <PlanogramLockType, String>
    //    {
    //        {PlanogramLockType.User, Message.Enum_PlanogramLockType_User},
    //        {PlanogramLockType.System, Message.Enum_PlanogramLockType_System},
    //    };
    //}
}
