﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM810)
// V8-29861 : A.Probyn
//	Created
#endregion

#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramEventLogInfo
    {
        #region Constructor
        private PlanogramEventLogInfo() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an item from a dto
        /// </summary>
        internal static PlanogramEventLogInfo GetPlanogramEventLogInfo(IDalContext dalContext, PlanogramEventLogInfoDto dto)
        {
            return DataPortal.FetchChild<PlanogramEventLogInfo>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramEventLogInfoDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<PlanogramEventLogEventType>(EventTypeProperty, (PlanogramEventLogEventType)dto.EventType);
            this.LoadProperty<PlanogramEventLogEntryType>(EntryTypeProperty, (PlanogramEventLogEntryType)dto.EntryType);
            this.LoadProperty<Object>(AffectedIdProperty, dto.AffectedId);
            this.LoadProperty<PlanogramEventLogAffectedType?>(AffectedTypeProperty, (PlanogramEventLogAffectedType?)dto.AffectedType);
            this.LoadProperty<String>(WorkpackageSourceProperty, dto.WorkpackageSource);
            this.LoadProperty<String>(TaskSourceProperty, dto.TaskSource);
            this.LoadProperty<Int32>(EventIdProperty, dto.EventId);
            this.LoadProperty<DateTime>(DateTimeProperty, dto.DateTime);
            this.LoadProperty<String>(DescriptionProperty, dto.Description);
            this.LoadProperty<String>(ContentProperty, dto.Content);
            this.LoadProperty<Byte>(ScoreProperty, dto.Score);
            this.LoadProperty<Int32>(PlanogramIdProperty, dto.PlanogramId);
            this.LoadProperty<String>(PlanogramNameProperty, dto.PlanogramName);
            this.LoadProperty<Int64>(ExecutionOrderProperty, dto.ExecutionOrder);
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramEventLogInfoDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion
        
        #endregion
    }
}
