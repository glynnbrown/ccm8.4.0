﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
#endregion
#region Version History: (CCM 8.0.3)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Csla;
using Csla.Data;
using Csla.Security;
using Csla.Serialization;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines an entity info within the solution
    /// </summary>
    public partial class ConsumerDecisionTreeInfo
    {
        #region Constructor
        private ConsumerDecisionTreeInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an entity info from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        /// <returns>An entity info object</returns>
        internal static ConsumerDecisionTreeInfo FetchConsumerDecisionTreeInfo(IDalContext dalContext, ConsumerDecisionTreeInfoDto dto)
        {
            return DataPortal.FetchChild<ConsumerDecisionTreeInfo>(dalContext, dto);
        }
        #endregion

        /// <summary>
        /// Returns the consumer decision tree info with the given id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ConsumerDecisionTreeInfo FetchById(Int32 id)
        {
            return DataPortal.Fetch<ConsumerDecisionTreeInfo>(new FetchByIdCriteria(id));
        }

        #region Data Access

        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, ConsumerDecisionTreeInfoDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Guid>(UniqueContentReferenceProperty, dto.UniqueContentReference);
            this.LoadProperty<Int32?>(ProductGroupIdProperty, dto.ProductGroupId);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<String>(ProductGroupCodeProperty, dto.ProductGroupCode);
            this.LoadProperty<String>(ProductGroupNameProperty, dto.ProductGroupName);
            this.LoadProperty<Guid?>(ParentUniqueContentReferenceProperty, dto.ParentUniqueContentReference);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, ConsumerDecisionTreeInfoDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        /// <summary>
        /// Called when return FetchById
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(ConsumerDecisionTreeInfo.FetchByIdCriteria criteria)
        {
            Int32 treeId = criteria.Id;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                // fetch the structure dto
                ConsumerDecisionTreeInfoDto dto;
                using (IConsumerDecisionTreeInfoDal dal = dalContext.GetDal<IConsumerDecisionTreeInfoDal>())
                {
                    dto = dal.FetchById(criteria.Id);
                }

                LoadDataTransferObject(dalContext, dto);
            }
        }
        

        #endregion

        #endregion
    }
}
