﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25879 : N.Haywood
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public sealed partial class LocationPlanAssignmentList
    {
        #region Constructor
        private LocationPlanAssignmentList() { } // force use of factory methods
        #endregion

        #region FactoryMethods

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        /// <param name="categoryReviewId"></param>
        /// <returns></returns>
        public static LocationPlanAssignmentList FetchByEntityIdProductGroupId(Int32 entityId, Int32 productGroupId)
        {
            return DataPortal.Fetch<LocationPlanAssignmentList>(new FetchByEntityIdProductGroupIdCriteria(entityId, productGroupId));
        }

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        /// <param name="categoryReviewId"></param>
        /// <returns></returns>
        public static LocationPlanAssignmentList FetchByEntityIdLocationId(Int32 entityId, Int16 locationId)
        {
            return DataPortal.Fetch<LocationPlanAssignmentList>(new FetchByEntityIdLocationIdCriteria(entityId, locationId));
        }

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        /// <param name="categoryReviewId"></param>
        /// <returns></returns>
        public static LocationPlanAssignmentList FetchByPlanogramId(Int32 planogramId)
        {
            return DataPortal.Fetch<LocationPlanAssignmentList>(new FetchByPlanogramIdCriteria(planogramId));
        }

        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returing all items for a given parent id
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdProductGroupIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationPlanAssignmentDal dal = dalContext.GetDal<ILocationPlanAssignmentDal>())
                {
                    IEnumerable<LocationPlanAssignmentDto> dtoList = 
                        dal.FetchByProductGroupId( (Int32)criteria.ProductGroupId);
                    foreach (LocationPlanAssignmentDto dto in dtoList)
                    {
                        this.Add(LocationPlanAssignment.Fetch(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returing all items for a given parent id
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdLocationIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationPlanAssignmentDal dal = dalContext.GetDal<ILocationPlanAssignmentDal>())
                {
                    IEnumerable<LocationPlanAssignmentDto> dtoList =
                        dal.FetchByLocationId((Int16)criteria.LocationId);
                    foreach (LocationPlanAssignmentDto dto in dtoList)
                    {
                        this.Add(LocationPlanAssignment.Fetch(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returing all items for a given parent id
        /// </summary>
        private void DataPortal_Fetch(FetchByPlanogramIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationPlanAssignmentDal dal = dalContext.GetDal<ILocationPlanAssignmentDal>())
                {
                    IEnumerable<LocationPlanAssignmentDto> dtoList =
                        dal.FetchByPlanogramId(criteria.PlanId);
                    foreach (LocationPlanAssignmentDto dto in dtoList)
                    {
                        this.Add(LocationPlanAssignment.Fetch(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #region Update

        /// <summary>
        /// Called when this list is being updated
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                Child_Update(dalContext);
                dalContext.Commit();
            }
        }

        #endregion

        #endregion
    }
}