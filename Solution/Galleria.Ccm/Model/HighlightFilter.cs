﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24801 L.Hodson
//  Created
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
#endregion
#region Version History: (CCM 8.10)
// V8-28661 : L.Ineson
//  Implemented IPlanogramHighlight interface
#endregion
#region Version History: (CCM 8.20)
// V8-31308 : A.Probyn
//  Removed field business rule on field
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a HighlightFilter object.
    /// </summary>
    [Serializable]
    public sealed partial class HighlightFilter : ModelObject<HighlightFilter>, IPlanogramHighlightFilter
    {
        #region Static Constructor
        static HighlightFilter()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Highlight Parent
        {
            get 
            {
                HighlightFilterList parentList = base.Parent as HighlightFilterList;
                if (parentList != null)
                {
                    return parentList.Parent;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            private set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region Field

        /// <summary>
        /// Field property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> FieldProperty =
            RegisterModelProperty<String>(c => c.Field);
        
        /// <summary>
        /// Gets/Sets the filter field
        /// </summary>
        public String Field
        {
            get { return GetProperty<String>(FieldProperty); }
            set { SetProperty<String>(FieldProperty, value); }
        }

        #endregion

        #region Type

        /// <summary>
        /// Type property definition
        /// </summary>
        public static readonly ModelPropertyInfo<HighlightFilterType> TypeProperty =
            RegisterModelProperty<HighlightFilterType>(c => c.Type);
        
        /// <summary>
        /// Gets/Sets the filter type
        /// </summary>
        /// <returns></returns>
        public HighlightFilterType Type
        {
            get { return GetProperty<HighlightFilterType>(TypeProperty); }
            set { SetProperty<HighlightFilterType>(TypeProperty, value); }
        }

        PlanogramHighlightFilterType IPlanogramHighlightFilter.Type
        {
            get { return Type.ToPlanogramHighlightFilterType(); }
        }

        #endregion

        #region Value

        /// <summary>
        /// Value property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ValueProperty =
            RegisterModelProperty<String>(c => c.Value);
        
        /// <summary>
        /// Gets/Sets the filter value
        /// </summary>
        /// <returns></returns>
        public String Value
        {
            get { return GetProperty<String>(ValueProperty); }
            set { SetProperty<String>(ValueProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <returns></returns>
        public static HighlightFilter NewHighlightFilter()
        {
            HighlightFilter item = new HighlightFilter();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<HighlightFilterType>(TypeProperty, HighlightFilterType.Equals);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<HighlightFilter>(oldId, newId);
        }
        #endregion
        
    }
}
