﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using Csla;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class ViewLayout
    {
        #region Constructors
        private ViewLayout() { } //force the use of factory methods
        #endregion

        #region Factory Methods

        public static ViewLayout FetchById(Int32 id)
        {
            return DataPortal.Fetch<ViewLayout>(id);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        private void LoadDataTransferObject(IDalContext dalContext, ViewLayoutDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<Boolean>(IsVerticalProperty, dto.IsVertical);

            LoadProperty<ViewLayoutItemList>(ItemsProperty,
                ViewLayoutItemList.FetchByViewLayoutId(dalContext, dto.Id));
        }

        private ViewLayoutDto GetDataTransferObject()
        {
            return new ViewLayoutDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                Name = ReadProperty<String>(NameProperty),
                IsVertical = ReadProperty<Boolean>(IsVerticalProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning FetchById
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(Int32 id)
        {
            //TODO:
            LoadDataTransferObject(null, MockDataHelper.FetchViewLayoutById(id));

            //IDalFactory dalFactory = DalContainer.GetDalFactory();
            //using (IDalContext dalContext = dalFactory.CreateContext())
            //{
            //    FixtureDto dto;
            //    using (IFixtureDal dal = dalContext.GetDal<IFixtureDal>())
            //    {
            //        dto = dal.FetchById(id);
            //    }

            //    LoadDataTransferObject(dalContext, dto);
            //}
        }


        #endregion

        #region Insert

        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            //TODO:
            ViewLayoutDto dto = GetDataTransferObject();
            MockDataHelper.InsertViewLayout(dto);
            LoadProperty<Int32>(IdProperty, dto.Id);

            //IDalFactory dalFactory = DalContainer.GetDalFactory();
            //using (IDalContext dalContext = dalFactory.CreateContext())
            //{
            //    dalContext.Begin();

            //    //insert this
            //    using (IProductDal dal = dalContext.GetDal<IProductDal>())
            //    {
            //        ProductDto dto = GetDataTransferObject();
            //        dal.Insert(dto);

            //        LoadProperty<Int32>(IdProperty, dto.Id);
            //        LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            //        LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            //        LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            //    }

            //    //insert any remaining children
            //    FieldManager.UpdateChildren(dalContext, this);

            //    dalContext.Commit();
            //}

        }

        #endregion

        #region Update

        /// <summary>
        /// Called when updating this object in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            //TODO:
            MockDataHelper.UpdateViewLayout(GetDataTransferObject());

            //IDalFactory dalFactory = DalContainer.GetDalFactory();
            //using (IDalContext dalContext = dalFactory.CreateContext())
            //{
            //    dalContext.Begin();

            //    //update the dto for this
            //    using (IProductDal dal = dalContext.GetDal<IProductDal>())
            //    {
            //        ProductDto dto = GetDataTransferObject();
            //        dal.Update(dto);

            //        LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            //        LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            //    }

            //    //insert any remaining children
            //    FieldManager.UpdateChildren(dalContext, this);

            //    dalContext.Commit();
            //}
        }


        #endregion

        #region Delete

        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            //TODO:
            MockDataHelper.DeleteViewLayout(this.Id);
            

            //IDalFactory dalFactory = DalContainer.GetDalFactory();
            //using (IDalContext dalContext = dalFactory.CreateContext())
            //{
            //    dalContext.Begin();
            //    using (IProductDal dal = dalContext.GetDal<IProductDal>())
            //    {
            //        dal.DeleteById(this.Id);
            //    }
            //    dalContext.Commit();
            //}
        }

        #endregion

        #endregion
    }
}
