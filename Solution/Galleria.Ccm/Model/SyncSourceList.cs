﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;
using Galleria.Framework.Model;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a synchronisation source list
    /// </summary>
    [Serializable]
    public sealed partial class SyncSourceList : ModelList<SyncSourceList, SyncSource>
    {
        #region Authorization Rules
        //this object is accessed without authentication and so cannot have rules
        #endregion
    }
}