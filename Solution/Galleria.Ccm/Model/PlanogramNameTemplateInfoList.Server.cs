﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// CCM-29010 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramNameTemplateInfoList
    {
        #region Constructors
        private PlanogramNameTemplateInfoList() { } // force the use of factory methods
        #endregion

        #region FactoryMethods

        public static PlanogramNameTemplateInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<PlanogramNameTemplateInfoList>(new FetchByEntityIdCriteria(entityId));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when retrieving a list of all PlanogramNameTemplate for an entity
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramNameTemplateInfoDal dal = dalContext.GetDal<IPlanogramNameTemplateInfoDal>())
                {
                    IEnumerable<PlanogramNameTemplateInfoDto> dtoList = dal.FetchByEntityId(criteria.EntityId);

                    foreach (PlanogramNameTemplateInfoDto dto in dtoList)
                    {
                        this.Add(PlanogramNameTemplateInfo.FetchPlanogramNameTemplateInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}