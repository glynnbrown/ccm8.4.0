﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class BlockingLocation
    {
        #region Constructor
        private BlockingLocation() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static BlockingLocation Fetch(IDalContext dalContext, BlockingLocationDto dto)
        {
            return DataPortal.FetchChild<BlockingLocation>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, BlockingLocationDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32>(BlockingGroupIdProperty, dto.BlockingGroupId);
            this.LoadProperty<Int32?>(BlockingDividerTopIdProperty, dto.BlockingDividerTopId);
            this.LoadProperty<Int32?>(BlockingDividerBottomIdProperty, dto.BlockingDividerBottomId);
            this.LoadProperty<Int32?>(BlockingDividerLeftIdProperty, dto.BlockingDividerLeftId);
            this.LoadProperty<Int32?>(BlockingDividerRightIdProperty, dto.BlockingDividerRightId);
            this.LoadProperty<Single>(SpacePercentageProperty, dto.SpacePercentage);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private BlockingLocationDto GetDataTransferObject(Blocking parent)
        {
            return new BlockingLocationDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                BlockingId = parent.Id,
                BlockingGroupId = ReadProperty<Int32>(BlockingGroupIdProperty),
                BlockingDividerTopId = ReadProperty<Int32?>(BlockingDividerTopIdProperty),
                BlockingDividerBottomId = ReadProperty<Int32?>(BlockingDividerBottomIdProperty),
                BlockingDividerLeftId = ReadProperty<Int32?>(BlockingDividerLeftIdProperty),
                BlockingDividerRightId = ReadProperty<Int32?>(BlockingDividerRightIdProperty),
                SpacePercentage = ReadProperty<Single>(SpacePercentageProperty),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, BlockingLocationDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, Blocking parent)
        {
            this.ResolveIds(dalContext);
            BlockingLocationDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IBlockingLocationDal dal = dalContext.GetDal<IBlockingLocationDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<BlockingLocation>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, Blocking parent)
        {
            this.ResolveIds(dalContext);
            if (this.IsSelfDirty)
            {
                using (IBlockingLocationDal dal = dalContext.GetDal<IBlockingLocationDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, Blocking parent)
        {
            using (IBlockingLocationDal dal = dalContext.GetDal<IBlockingLocationDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion

    }
}