﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Server side implementation of <see cref="ColumnLayoutSetting"/>.
    /// </summary>
    public partial class ColumnLayoutSetting
    {
        #region Constructors
        private ColumnLayoutSetting() { }
        #endregion

        #region Factory Methods

        /// <summary>
        ///     Fetches a ColumnLayoutSetting from the DAL.
        /// </summary>
        /// <returns>A new instance of the requested ColumnLayoutSetting.</returns>
        internal static ColumnLayoutSetting GetColumnLayoutSetting(IDalContext dalContext, ColumnLayoutSettingDto dto)
        {
            return DataPortal.FetchChild<ColumnLayoutSetting>(dalContext, dto);
        }

        #endregion

        #region Methods

        public static void UpdateUserSetting(String screenKey, String fileName)
        {
            var userSettings = UserSystemSetting.FetchUserSettings();
            var userColumnLayoutSetting = userSettings.ColumnLayouts
                .FirstOrDefault(setting => setting.ScreenKey == screenKey);
            if (userColumnLayoutSetting == null)
            {
                userColumnLayoutSetting = NewColumnLayoutSetting();
                userColumnLayoutSetting.ScreenKey = screenKey;
                userSettings.ColumnLayouts.Add(userColumnLayoutSetting);
            }
            userColumnLayoutSetting.ColumnLayoutName = fileName;
            userSettings.Save();
        }

        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        ///     Creates and intializes a new <see cref="ColumnLayoutSettingDto"/> instance with the data in this instance.
        /// </summary>
        /// <returns>A new instance of <see cref="ColumnLayoutSettingDto"/> with the information of this instance.</returns>
        private ColumnLayoutSettingDto GetDataTransferObject()
        {
            return new ColumnLayoutSettingDto
            {
                Id = ReadProperty(IdProperty),
                ScreenKey = ReadProperty(ScreenKeyProperty),
                ColumnLayoutName = ReadProperty(ColumnLayoutNameProperty),
            };
        }
        /// <summary>
        ///     Copies into this instance the data from the specified <see cref="ColumnLayoutSettingDto"/>.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">The instance of <see cref="ColumnLayoutSettingDto"/> containing the data to load into this instance.</param>
        private void LoadDataTransferObject(IDalContext dalContext, ColumnLayoutSettingDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(ScreenKeyProperty, dto.ScreenKey);
            LoadProperty(ColumnLayoutNameProperty, dto.ColumnLayoutName);
        }

        #endregion

        #region Fetch
        /// <summary>
        ///     Called by reflection by CSLA whenever fetching this instance as a child.
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, ColumnLayoutSettingDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        ///     Called by reflection by CSLA whenever inserting this instance as a child.
        /// </summary>
        private void Child_Insert(IDalContext dalContext)
        {
            ColumnLayoutSettingDto dto = this.GetDataTransferObject();
            Int32 oldId = dto.Id;
            using (IColumnLayoutSettingDal dal = dalContext.GetDal<IColumnLayoutSettingDal>())
            {
                dal.Insert(dto);
            }
            dalContext.RegisterId<ColumnLayoutSetting>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        ///     Called by reflection by CSLA whenever updating this instance as a child.
        /// </summary>
        /// <param name="dalContext"></param>
        private void Child_Update(IDalContext dalContext)
        {
            if (this.IsSelfDirty)
            {
                using (var dal = dalContext.GetDal<IColumnLayoutSettingDal>())
                {
                    dal.Update(GetDataTransferObject());
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #endregion
    }
}
