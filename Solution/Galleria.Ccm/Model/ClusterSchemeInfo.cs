﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629 : A.Kuszyk
//		Created (copied from SA).
#endregion
#region Version History: (CCM 8.0.3)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#region Version History: (CCM 8.1.0)
// V8-29598 : L.Luong
//  Added ProductGroupId and DateLastModified
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;
using Csla.Data;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Csla.Security;
using Csla.Serialization;

using Galleria.Ccm.Resources;
using Galleria.Framework.Model;
using Galleria.Ccm.Security;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class ClusterSchemeInfo : ModelReadOnlyObject<ClusterSchemeInfo>
    {
        #region Properties
        /// <summary>
        /// The unique cluster scheme id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// Unique content reference property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Guid> UniqueContentReferenceProperty =
            RegisterModelProperty<Guid>(c => c.UniqueContentReference);
        /// <summary>
        /// Unique content reference
        /// </summary>
        public Guid UniqueContentReference
        {
            get { return GetProperty<Guid>(UniqueContentReferenceProperty); }
        }

        /// <summary>
        /// The cluster scheme name
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        /// <summary>
        /// The cluster schme isDefault value
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsDefaultProperty =
            RegisterModelProperty<Boolean>(c => c.IsDefault);
        public Boolean IsDefault
        {
            get { return GetProperty<Boolean>(IsDefaultProperty); }
        }

        /// <summary>
        /// A count of clusters contained within the cluster scheme
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ClusterCountProperty =
            RegisterModelProperty<Int32>(c => c.ClusterCount);
        public Int32 ClusterCount
        {
            get { return GetProperty<Int32>(ClusterCountProperty); }
        }

        /// <summary>
        /// The entity Id.
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
        }

        /// <summary>
        /// The Product Group Id.
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> ProductGroupIdProperty =
            RegisterModelProperty<Int32?>(c => c.ProductGroupId);
        public Int32? ProductGroupId
        {
            get { return GetProperty<Int32?>(ProductGroupIdProperty); }
        }

        /// <summary>
        /// The Product Group Id.
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified); 
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
        }

        /// <summary>
        /// Parent unique content reference property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Guid?> ParentUniqueContentReferenceProperty =
            RegisterModelProperty<Guid?>(c => c.ParentUniqueContentReference);
        /// <summary>
        /// Parent unique content reference
        /// </summary>
        public Guid? ParentUniqueContentReference
        {
            get { return GetProperty<Guid?>(ParentUniqueContentReferenceProperty); }
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ClusterSchemeInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ClusterSchemeInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(ClusterSchemeInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ClusterSchemeInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        #endregion
    }
}
