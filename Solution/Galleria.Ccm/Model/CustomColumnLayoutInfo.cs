#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.

#endregion

#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Readonly model for the basic information about a <see cref="CustomColumnLayout"/> object.
    /// </summary>
    [Serializable]
    public sealed partial class CustomColumnLayoutInfo : ModelReadOnlyObject<CustomColumnLayoutInfo>, IDisposable
    {
        #region Properties

        #region Id Property

        /// <summary>
        ///     The <see cref="ModelPropertyInfo{T}"/> for the <see cref="Id"/> property.
        /// </summary>
        public static readonly ModelPropertyInfo<Object> IdProperty = RegisterModelProperty<Object>(o => o.Id);

        /// <summary>
        ///     Gets the unique <see cref="Id"/> of this <see cref="CustomColumnLayoutInfo"/>.
        /// </summary>
        public Object Id
        {
            get { return GetProperty(IdProperty); }
        }

        #endregion

        #region FileName Property

        /// <summary>
        ///     The <see cref="ModelPropertyInfo{T}"/> for the <see cref="FileName"/> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> FileNameProperty = RegisterModelProperty<String>(o => o.FileName);

        /// <summary>
        ///     Gets the name of the file this <see cref="CustomColumnLayoutInfo"/> represents.
        /// </summary>
        public String FileName
        {
            get { return GetProperty(FileNameProperty); }
        }

        #endregion

        #region Type Property

        /// <summary>
        ///     The <see cref="ModelPropertyInfo{T}"/> for the <see cref="Type"/> property.
        /// </summary>
        public static readonly ModelPropertyInfo<CustomColumnLayoutType> TypeProperty = RegisterModelProperty<CustomColumnLayoutType>(o => o.Type);

        /// <summary>
        ///     Gets the type of the <see cref="CustomColumnLayout"/> this <see cref="CustomColumnLayoutInfo"/> represents.
        /// </summary>
        public CustomColumnLayoutType Type
        {
            get { return GetProperty(TypeProperty); }
        }

        #endregion

        #region Name Property

        /// <summary>
        ///     The <see cref="ModelPropertyInfo{T}"/> for the <see cref="Name"/> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty = RegisterModelProperty<String>(o => o.Name);

        /// <summary>
        ///     Gets the name of the <see cref="CustomColumnLayout"/> this <see cref="CustomColumnLayoutInfo"/> represents.
        /// </summary>
        public String Name
        {
            get { return GetProperty(NameProperty); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region IDisposable Support

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(Boolean disposing)
        {
            if (_isDisposed) return;

            if (disposing)
            {
                
            }

            _isDisposed = true;
        }
        #endregion
    }
}