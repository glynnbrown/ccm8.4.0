﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created 
// V8-25873 : A.Probyn
//  Added Custom Attribute 1 - 5 and friendly names
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Info object for a FixturePackage
    /// </summary>
    [Serializable]
    public sealed partial class FixturePackageInfo : ModelReadOnlyObject<FixturePackageInfo>
    {
        #region Static Constructor
        /// <summary>
        /// Static Constructor
        /// </summary>
        static FixturePackageInfo()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Object> IdProperty =
            RegisterModelProperty<Object>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Object Id
        {
            get { return this.GetProperty<Object>(IdProperty); }
        }
        #endregion

        #region FolderId

        /// <summary>
        /// FolderId property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Object> FolderIdProperty =
            RegisterModelProperty<Object>(c => c.FolderId);
        /// <summary>
        /// Returns the name of the file this info represents
        /// if of a file system type.
        /// </summary>
        public Object FolderId
        {
            get { return this.GetProperty<Object>(FolderIdProperty); }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        #endregion

        #region Description

        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);

        /// <summary>
        /// Gets/Sets the Description value
        /// </summary>
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
        }

        #endregion

        #region Height

        /// <summary>
        /// Height property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HeightProperty =
            RegisterModelProperty<Single>(c => c.Height);

        /// <summary>
        /// Gets/Sets the Height value
        /// </summary>
        public Single Height
        {
            get { return GetProperty<Single>(HeightProperty); }
        }

        #endregion

        #region Width

        /// <summary>
        /// Width property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> WidthProperty =
            RegisterModelProperty<Single>(c => c.Width);

        /// <summary>
        /// Gets/Sets the Width value
        /// </summary>
        public Single Width
        {
            get { return GetProperty<Single>(WidthProperty); }
        }

        #endregion

        #region Depth

        /// <summary>
        /// Depth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DepthProperty =
            RegisterModelProperty<Single>(c => c.Depth);

        /// <summary>
        /// Gets/Sets the Depth value
        /// </summary>
        public Single Depth
        {
            get { return GetProperty<Single>(DepthProperty); }
        }

        #endregion

        #region FixtureCount

        /// <summary>
        /// FixtureCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FixtureCountProperty =
            RegisterModelProperty<Int32>(c => c.FixtureCount, Message.FixturePackageInfo_FixtureCount);

        /// <summary>
        /// Gets the FixtureCount value
        /// </summary>
        public Int32 FixtureCount
        {
            get { return GetProperty<Int32>(FixtureCountProperty); }
        }

        #endregion

        #region AssemblyCount

        /// <summary>
        /// AssemblyCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> AssemblyCountProperty =
            RegisterModelProperty<Int32>(c => c.AssemblyCount, Message.FixturePackageInfo_AssemblyCount);

        /// <summary>
        /// Gets the AssemblyCount value
        /// </summary>
        public Int32 AssemblyCount
        {
            get { return GetProperty<Int32>(AssemblyCountProperty); }
        }

        #endregion

        #region ComponentCount

        /// <summary>
        /// ComponentCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ComponentCountProperty =
            RegisterModelProperty<Int32>(c => c.ComponentCount, Message.FixturePackageInfo_ComponentCount);

        /// <summary>
        /// Gets the ComponentCount value
        /// </summary>
        public Int32 ComponentCount
        {
            get { return GetProperty<Int32>(ComponentCountProperty); }
        }

        #endregion

        #region ThumbnailImageData

        /// <summary>
        /// ThumbnailImageData property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte[]> ThumbnailImageDataProperty =
            RegisterModelProperty<Byte[]>(c => c.ThumbnailImageData);

        /// <summary>
        /// Gets/Sets the ThumbnailImageData value
        /// </summary>
        public Byte[] ThumbnailImageData
        {
            get { return GetProperty<Byte[]>(ThumbnailImageDataProperty); }
        }

        #endregion

        #region Custom Attribute 1

        /// <summary>
        /// CustomAttribute1 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomAttribute1Property =
            RegisterModelProperty<String>(c => c.CustomAttribute1);

        /// <summary>
        /// Gets/Sets the CustomAttribute1 value
        /// </summary>
        public String CustomAttribute1
        {
            get { return GetProperty<String>(CustomAttribute1Property); }
        }

        #endregion

        #region Custom Attribute 2

        /// <summary>
        /// CustomAttribute2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomAttribute2Property =
            RegisterModelProperty<String>(c => c.CustomAttribute2);

        /// <summary>
        /// Gets/Sets the CustomAttribute2 value
        /// </summary>
        public String CustomAttribute2
        {
            get { return GetProperty<String>(CustomAttribute2Property); }
        }

        #endregion

        #region Custom Attribute 3

        /// <summary>
        /// CustomAttribute3 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomAttribute3Property =
            RegisterModelProperty<String>(c => c.CustomAttribute3);

        /// <summary>
        /// Gets/Sets the CustomAttribute3 value
        /// </summary>
        public String CustomAttribute3
        {
            get { return GetProperty<String>(CustomAttribute3Property); }
        }

        #endregion

        #region Custom Attribute 4

        /// <summary>
        /// CustomAttribute4 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomAttribute4Property =
            RegisterModelProperty<String>(c => c.CustomAttribute4);

        /// <summary>
        /// Gets/Sets the CustomAttribute4 value
        /// </summary>
        public String CustomAttribute4
        {
            get { return GetProperty<String>(CustomAttribute4Property); }
        }

        #endregion

        #region Custom Attribute 5

        /// <summary>
        /// CustomAttribute5 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomAttribute5Property =
            RegisterModelProperty<String>(c => c.CustomAttribute5);

        /// <summary>
        /// Gets/Sets the CustomAttribute5 value
        /// </summary>
        public String CustomAttribute5
        {
            get { return GetProperty<String>(CustomAttribute5Property); }
        }

        #endregion 

        /// <summary>
        /// Returns the package type
        /// </summary>
        public FixturePackageType PackageType
        {
            get
            {
                if (this.FolderId is String)
                {
                    return FixturePackageType.FileSystem;
                }
                return FixturePackageType.Unknown;
            }
        }

        public String FileName
        {
            get
            {
                if (Id is String)
                {
                    return (String)Id;
                }
                return null;
            }
        }

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        #endregion

        #region Commands

        #region SetFolderIdCommand

        /// <summary>
        /// Sets the folder id of the given package id
        /// </summary>
        [Serializable]
        private partial class SetFolderIdCommand : CommandBase<SetFolderIdCommand>
        {
            #region Authorization Rules
            // no authentication rules required as this object
            // is accessed by the editor when not connected to
            // a repository
            #endregion

            #region Properties

            #region FixturePackageType
            /// <summary>
            /// FixturePackageType property definition
            /// </summary>
            public static readonly PropertyInfo<FixturePackageType> FixturePackageTypeProperty =
                RegisterProperty<FixturePackageType>(c => c.FixturePackageType);
            /// <summary>
            /// Returns the package source type
            /// </summary>
            public FixturePackageType FixturePackageType
            {
                get { return this.ReadProperty<FixturePackageType>(FixturePackageTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #region FolderId

            /// <summary>
            /// FolderId property definition
            /// </summary>
            public static readonly PropertyInfo<Object> FolderIdProperty =
                RegisterProperty<Object>(c => c.FolderId);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object FolderId
            {
                get { return this.ReadProperty<Object>(FolderIdProperty); }
            }


            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public SetFolderIdCommand(FixturePackageType packageType, Object id, Object newFolderId)
            {
                this.LoadProperty<FixturePackageType>(FixturePackageTypeProperty, packageType);
                this.LoadProperty<Object>(IdProperty, id);
                this.LoadProperty<Object>(FolderIdProperty, newFolderId);
            }
            #endregion
        }

        #endregion

        #endregion
    }
}
