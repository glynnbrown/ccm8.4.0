﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using System.Diagnostics.CodeAnalysis;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentMinorRevisionAmendDistributionActionLocationList
    {
        #region Constructors
        private AssortmentMinorRevisionAmendDistributionActionLocationList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an existing Assortment location list
        /// </summary>
        /// <param name="childData"></param>
        /// <returns>A new Assortment location list</returns>
        internal static AssortmentMinorRevisionAmendDistributionActionLocationList FetchByAssortmentMinorRevisionAmendDistributionActionId(IDalContext dalContext, Int32 assortmentMinorRevisionAmendDistributionActionId)
        {
            return DataPortal.FetchChild<AssortmentMinorRevisionAmendDistributionActionLocationList>(dalContext, assortmentMinorRevisionAmendDistributionActionId);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="reviewId">The parent Assortment id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 AssortmentMinorRevisionAmendDistributionActionId)
        {
            RaiseListChangedEvents = false;
            using (IAssortmentMinorRevisionAmendDistributionActionLocationDal dal = dalContext.GetDal<IAssortmentMinorRevisionAmendDistributionActionLocationDal>())
            {
                IEnumerable<AssortmentMinorRevisionAmendDistributionActionLocationDto> dtoList = dal.FetchByAssortmentMinorRevisionAmendDistributionActionId(AssortmentMinorRevisionAmendDistributionActionId);
                foreach (AssortmentMinorRevisionAmendDistributionActionLocationDto dto in dtoList)
                {
                    this.Add(AssortmentMinorRevisionAmendDistributionActionLocation.FetchAssortmentMinorRevisionAmendDistributionActionLocation(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
