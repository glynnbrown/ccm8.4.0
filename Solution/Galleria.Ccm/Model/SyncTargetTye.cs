﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public enum SyncTargetType
    {
        Mssql = 0,
        //VistaDb = 1
    }

    public static class SyncTargetTypeHelper
    {
        public static readonly Dictionary<SyncTargetType, String> FriendlyNames =
            new Dictionary<SyncTargetType, String>()
            {
                {SyncTargetType.Mssql, Message.Enum_SyncTargetType_Mssql},
                //{SyncTargetType.VistaDb, Message.Enum_SyncTargetType_VistaDb}
            };

        public static readonly Dictionary<SyncTargetType, String> FriendlyDescriptions =
            new Dictionary<SyncTargetType, String>()
            {
                {SyncTargetType.Mssql, Message.Enum_SyncTargetType_Mssql_Description},
                //{SyncTargetType.VistaDb, Message.Enum_SyncTargetType_VistaDb_Description}
            };

        /// <summary>
        /// Returns the matching enum value from the specifed description
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static SyncTargetType? SyncTargetTypeGetEnum(String description)
        {
            foreach (KeyValuePair<SyncTargetType, String> keyPair in SyncTargetTypeHelper.FriendlyNames)
            {
                if (keyPair.Value.Equals(description))
                {
                    return keyPair.Key;
                }
            }
            return null;
        }
    }
}
