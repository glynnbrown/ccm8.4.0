﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// CCM-25879 : N.Haywood
//	Created (Auto-generated)
#endregion

#region Version History: (CCM 8.1.1)
// V8-30463 : L.Ineson
//  Ammended user properties to be ids for performance.
#endregion

#region Version History: (CCM 8.2.0)
// V8-30732 : M.Shelley
//  Added 2 new properties : OutputDestination and PlanPublishType
// V8-30508 : M.Shelley
//  Added a new property : PublishStatus
#endregion

#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using System.Collections.Generic;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    public partial class LocationPlanAssignment
    {
        #region Constructor
        private LocationPlanAssignment() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static LocationPlanAssignment Fetch(IDalContext dalContext, LocationPlanAssignmentDto dto)
        {
            return DataPortal.FetchChild<LocationPlanAssignment>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, LocationPlanAssignmentDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32>(PlanogramIdProperty, dto.PlanogramId);
            this.LoadProperty<Int32>(ProductGroupIdProperty, dto.ProductGroupId);
            this.LoadProperty<Int16>(LocationIdProperty, dto.LocationId);
            this.LoadProperty<Int32?>(AssignedByUserIdProperty, dto.AssignedByUserId);
            this.LoadProperty<Int32?>(PublishedByUserIdProperty, dto.PublishUserId);
            this.LoadProperty<DateTime?>(DateAssignedProperty, dto.DateAssigned);
            this.LoadProperty<DateTime?>(DatePublishedProperty, dto.DatePublished);
            this.LoadProperty<DateTime?>(DateCommunicatedProperty, dto.DateCommunicated);
            this.LoadProperty<DateTime?>(DateLiveProperty, dto.DateLive);
            this.LoadProperty<PlanPublishType?>(PublishTypeProperty, (PlanPublishType?)dto.PublishType);
            this.LoadProperty<String>(OutputDestinationProperty, dto.OutputDestination);
            this.LoadProperty<PlanAssignmentPublishStatusType?>(PublishStatusProperty, (PlanAssignmentPublishStatusType?) dto.PublishStatus);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private LocationPlanAssignmentDto GetDataTransferObject()
        {
            return new LocationPlanAssignmentDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                PlanogramId = ReadProperty<Int32>(PlanogramIdProperty),
                ProductGroupId = ReadProperty<Int32>(ProductGroupIdProperty),
                LocationId = ReadProperty<Int16>(LocationIdProperty),
                AssignedByUserId = ReadProperty<Int32?>(AssignedByUserIdProperty),
                PublishUserId = ReadProperty<Int32?>(PublishedByUserIdProperty),
                DateAssigned = ReadProperty<DateTime?>(DateAssignedProperty),
                DatePublished = ReadProperty<DateTime?>(DatePublishedProperty),
                DateCommunicated = ReadProperty<DateTime?>(DateCommunicatedProperty),
                DateLive = ReadProperty<DateTime?>(DateLiveProperty),
                PublishType = (Byte?)ReadProperty<PlanPublishType?>(PublishTypeProperty),
                OutputDestination = ReadProperty<String>(OutputDestinationProperty),
                PublishStatus = (Byte?) ReadProperty<PlanAssignmentPublishStatusType?>(PublishStatusProperty),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, LocationPlanAssignmentDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext)
        {
            LocationPlanAssignmentDto dto = this.GetDataTransferObject();
            Int32 oldId = dto.Id;
            using (ILocationPlanAssignmentDal dal = dalContext.GetDal<ILocationPlanAssignmentDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<LocationPlanAssignment>(oldId, dto.Id);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            if (this.IsSelfDirty)
            {
                using (ILocationPlanAssignmentDal dal = dalContext.GetDal<ILocationPlanAssignmentDal>())
                {
                    dal.Update(this.GetDataTransferObject());
                }
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (ILocationPlanAssignmentDal dal = dalContext.GetDal<ILocationPlanAssignmentDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion

    }
}