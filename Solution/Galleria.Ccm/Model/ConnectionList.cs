﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25559 : L.Ineson
//	Copied from SA
#endregion
#endregion

using System;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model implementation of ConnectionList (Child of UserSettings)
    /// </summary>
    [Serializable]
    public sealed partial class ConnectionList : ModelList<ConnectionList, Connection>
    {
        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new ConnectionList 
        /// </summary>
        /// <returns>A new ConnectionList</returns>
        public static ConnectionList NewConnectionList()
        {
            ConnectionList metricList = new ConnectionList();
            metricList.Create();
            return metricList;
        }

        #endregion

        #region Methods

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create()
        {
            MarkAsChild();
            AllowNew = true;
        }

        #endregion

        #endregion

    }
}
