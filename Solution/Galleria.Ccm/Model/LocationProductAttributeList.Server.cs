﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
// V8-27630 : I.George
// Removed Unused Factory methods
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class LocationProductAttributeList
    {
        #region Constructor
        private LocationProductAttributeList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        public static LocationProductAttributeList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<LocationProductAttributeList>(new FetchByEntityIdCriteria(entityId));
        }

        /// <summary>
        /// Returns all items for the given combinations
        /// </summary>
        /// <param name="combinationsList"></param>
        /// <returns></returns>
        public static LocationProductAttributeList FetchByLocationIdProductIdCombinations(Int32 entityId, IEnumerable<Tuple<Int16, Int32>> combinationsList)
        {
            return DataPortal.Fetch<LocationProductAttributeList>(new FetchByLocationIdProductIdCombinationsCriteria(entityId, combinationsList));
        }

        /// <summary>
        /// Returns all items for the given combinations. Allows option to create model instances of a locationproductattribute if it does not already exist
        /// </summary>
        /// <param name="combinationsList"></param>
        /// <returns></returns>
        public static LocationProductAttributeList FetchByLocationIdProductIdCombinations(Int32 entityId, IEnumerable<Tuple<Int16, Int32>> combinationsList, Boolean allowCreate)
        {
            return DataPortal.Fetch<LocationProductAttributeList>(new FetchByLocationIdProductIdCombinationsCriteria(entityId, combinationsList, allowCreate));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returning all library types
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
                {
                    IEnumerable<LocationProductAttributeDto> dtoList = dal.FetchByEntityId(criteria.EntityId);
                    foreach (LocationProductAttributeDto dto in dtoList)
                    {
                        this.Add(LocationProductAttribute.GetLocationProductAttribute(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returning all library types
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByLocationIdProductIdCombinationsCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
                {
                    IEnumerable<LocationProductAttributeDto> dtoList = dal.FetchByLocationIdProductIdCombinations(criteria.Combinations);
                    foreach (Tuple<Int16, Int32> combination in criteria.Combinations)
                    {
                        LocationProductAttributeDto combinationDto =
                            dtoList.FirstOrDefault(d => d.LocationId == combination.Item1 && d.ProductId == combination.Item2);
                        if (combinationDto != null)
                        {
                            this.Add(LocationProductAttribute.GetLocationProductAttribute(dalContext, combinationDto));
                        }
                        else
                        {
                            if (criteria.AllowCreate)
                            {
                                this.Add(LocationProductAttribute.NewLocationProductAttributeAsChild(combination.Item1, combination.Item2, criteria.EntityId));
                            }
                        }
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #region Update
        /// <summary>
        /// Called when this list is being updated
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                Child_Update(dalContext);
                dalContext.Commit();
            }
        }
        #endregion

        #endregion

    }
}
