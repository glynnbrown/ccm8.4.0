﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25436 L.Luong
//  Created
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class HighlightInfo
    {
        #region Constructors
        private HighlightInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static HighlightInfo GetHighlightInfo(IDalContext dalContext, HighlightInfoDto dto)
        {
            return DataPortal.FetchChild<HighlightInfo>(dalContext, dto);
        }

        /// <summary>
        /// Returns an info of an existing object
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        internal static HighlightInfo GetHighlightInfo(IDalContext dalContext, HighlightInfoDto dto, String fileName)
        {
            return DataPortal.FetchChild<HighlightInfo>(dalContext, dto, fileName);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, HighlightInfoDto dto)
        {
            LoadProperty<Object>(IdProperty, dto.Id);
            LoadProperty<String>(NameProperty, dto.Name);
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, HighlightInfoDto dto, String fileName)
        {
            LoadProperty<Object>(IdProperty, dto.Id);
            LoadProperty<String>(FileNameProperty, fileName);
            LoadProperty<String>(NameProperty, dto.Name);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when loading this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, HighlightInfoDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        /// <summary>
        /// Called when loading this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, HighlightInfoDto dto, String fileName)
        {
            LoadDataTransferObject(dalContext, dto, fileName);
        }

        #endregion

        #endregion
    }
}
