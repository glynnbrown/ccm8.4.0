﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History 830
// V8-31699 : A.Heathcote
//  Created.


#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    ///  <summary>
    ///  A Recent highlight Label
    /// </summary>
    [Serializable]
    public partial class UserEditorSettingsRecentHighlight : ModelObject<UserEditorSettingsRecentHighlight>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Unique Id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region SaveType

        public static readonly ModelPropertyInfo<HighlightDalFactoryType> SaveTypeProperty =
            RegisterModelProperty<HighlightDalFactoryType>(c => c.SaveType);

        public HighlightDalFactoryType SaveType
        {
            get { return GetProperty<HighlightDalFactoryType>(SaveTypeProperty); }
        }

        #endregion

        #region HighlightID

        public static readonly ModelPropertyInfo<Object> HighlightIdProperty =
            RegisterModelProperty<Object>(c => c.HighlightId);
        public Object HighlightId
        {
            get { return GetProperty<Object>(HighlightIdProperty); }
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// defines the autherization rules
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new item of this type
        /// </summary>
        /// <param name="highlightId">the highlight id</param>
        /// <returns></returns>
        internal static UserEditorSettingsRecentHighlight NewUserEditorSettingsRecentHighlight(Object highlightId)
        {
            UserEditorSettingsRecentHighlight item = new UserEditorSettingsRecentHighlight();
            item.Create(highlightId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        private void Create(Object highlightId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Object>(HighlightIdProperty, highlightId);

            this.LoadProperty<HighlightDalFactoryType>(SaveTypeProperty, 
                (highlightId is String)? HighlightDalFactoryType.FileSystem: HighlightDalFactoryType.Unknown);


            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion
    }
}
