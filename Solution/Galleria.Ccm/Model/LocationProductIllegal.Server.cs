﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25447 : N.Haywood
//  Copied over from GFS
#endregion
#region Version History: (CCM 8.03)
// V8-29268 : L.Ineson
//  Removed date deleted
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;
using Csla;

namespace Galleria.Ccm.Model
{
    public partial class LocationProductIllegal
    {
        #region Constructor
        private LocationProductIllegal() { }//force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates the item from the given dto
        /// </summary>
        /// <param name="dalContext">Current dal context</param>
        /// <param name="dto"></param>
        /// <returns>Object of this type</returns>
        internal static LocationProductIllegal GetLocationProductIllegal(IDalContext dalContext, LocationProductIllegalDto dto)
        {
            return DataPortal.FetchChild<LocationProductIllegal>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns></returns>
        private LocationProductIllegalDto GetDataTransferObject()
        {
            return new LocationProductIllegalDto
            {
                ProductId = ReadProperty<Int32>(ProductIdProperty),
                LocationId = ReadProperty<Int16>(LocationIdProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
            };
        }
        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dalContext">current dal context</param>
        /// <param name="dto">dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, LocationProductIllegalDto dto)
        {
            LoadProperty<Int32>(ProductIdProperty, dto.ProductId);
            LoadProperty<Int16>(LocationIdProperty, dto.LocationId);
            LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning an existing object 
        /// </summary>
        /// <param name="dalContext">current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, LocationProductIllegalDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts a new object into the solution
        /// </summary>
        /// <param name="dalContext">current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext)
        {
            using (ILocationProductIllegalDal dal = dalContext.GetDal<ILocationProductIllegalDal>())
            {
                //get the dto
                LocationProductIllegalDto dto = GetDataTransferObject();
                dal.Insert(dto);

                //update the rowversion from the dto
                LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);

                LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Update

        /// <summary>
        /// Called when updating an object of this type
        /// </summary>
        /// <param name="dalContext">the current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            using (ILocationProductIllegalDal dal = dalContext.GetDal<ILocationProductIllegalDal>())
            {
                //get the dto
                LocationProductIllegalDto dto = GetDataTransferObject();
                dal.Update(dto);

                //update the rowversion from the dto
                LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Delete

        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (ILocationProductIllegalDal dal = dalContext.GetDal<ILocationProductIllegalDal>())
            {
                dal.DeleteById(this.LocationId, this.ProductId);
            }
        }

        #endregion

        #endregion
    }
}
