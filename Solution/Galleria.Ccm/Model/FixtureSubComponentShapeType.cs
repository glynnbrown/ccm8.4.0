﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Intiial Version
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Sub Component Shape Type Enum
    /// </summary>
    public enum FixtureSubComponentShapeType
    {
        Box = 0,
        //Mesh = 1
    }

    /// <summary>
    /// Sub Component Shape Type Helper Class
    /// </summary>
    public static class FixtureSubComponentShapeTypeHelper
    {
        public static readonly Dictionary<FixtureSubComponentShapeType, String> FriendlyNames =
            new Dictionary<FixtureSubComponentShapeType, String>()
            {
                {FixtureSubComponentShapeType.Box, Message.Enum_FixtureSubComponentShapeType_Box },
                //{FixtureSubComponentShapeType.Mesh, Message.Enum_FixtureSubComponentShapeType_Mesh }
            };


        public static FixtureSubComponentShapeType Parse(String value)
        {
            return EnumHelper.Parse<FixtureSubComponentShapeType>(value, FixtureSubComponentShapeType.Box);
        }
    }
}
