﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630: A.Kuszyk
//  Created (copied from SA).
// V8-26520: J.Pickup
//  Added missing DateLastModified Property
#endregion
#region Version History: (CCM 8.0.3)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#region Version History : CCM820
// V8-30840 : A.Kuszyk
//  Added FetchByEntityIdName.
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Csla;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A class representing a product universe within GFS
    /// </summary>
    [Serializable]
    [DefaultNewMethod("NewProductUniverse", "EntityId")]
    public sealed partial class ProductUniverse : ModelObject<ProductUniverse>
    {
        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
           RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The unique object id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
           RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The row version timestamp
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        /// <summary>
        /// The entity id
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
        }

        /// <summary>
        /// Unique content reference property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Guid> UniqueContentReferenceProperty =
            RegisterModelProperty<Guid>(c => c.UniqueContentReference);
        /// <summary>
        /// Unique content reference
        /// </summary>
        public Guid UniqueContentReference
        {
            get { return GetProperty<Guid>(UniqueContentReferenceProperty); }
        }

        /// <summary>
        /// Parent unique content reference property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Guid?> ParentUniqueContentReferenceProperty =
            RegisterModelProperty<Guid?>(c => c.ParentUniqueContentReference);
        /// <summary>
        /// Parent unique content reference
        /// </summary>
        public Guid? ParentUniqueContentReference
        {
            get { return GetProperty<Guid?>(ParentUniqueContentReferenceProperty); }
        }

        public static readonly ModelPropertyInfo<Int32?> ProductGroupIdProperty =
            RegisterModelProperty<Int32?>(c => c.ProductGroupId);
        public Int32? ProductGroupId
        {
            get { return GetProperty<Int32?>(ProductGroupIdProperty); }
            set { SetProperty<Int32?>(ProductGroupIdProperty, value); }
        }

        /// <summary>
        /// Whether this product universe is the master product universe for its product group, i.e. the one that gets
        /// automatically created and updated on product import.
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsMasterProperty =
            RegisterModelProperty<Boolean>(c => c.IsMaster);
        public Boolean IsMaster
        {
            get { return GetProperty<Boolean>(IsMasterProperty); }
            set { SetProperty<Boolean>(IsMasterProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// The product universe name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }


        public static readonly ModelPropertyInfo<ProductUniverseProductList> ProductsProperty =
            RegisterModelProperty<ProductUniverseProductList>(c => c.Products);
        /// <summary>
        /// The product universe products
        /// </summary>
        public ProductUniverseProductList Products
        {
            get { return GetProperty<ProductUniverseProductList>(ProductsProperty); }
        }

        /// <summary>
        /// The date when the item was modified 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));

        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ProductUniverse), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ProductUniverseCreate.ToString()));
            BusinessRules.AddRule(typeof(ProductUniverse), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ProductUniverseGet.ToString()));
            BusinessRules.AddRule(typeof(ProductUniverse), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ProductUniverseEdit.ToString()));
            BusinessRules.AddRule(typeof(ProductUniverse), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ProductUniverseDelete.ToString()));
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByEntityIdName
        /// </summary>
        [Serializable]
        public class FetchByEntityIdNameCriteria : CriteriaBase<FetchByEntityIdNameCriteria>
        {
            #region EntityId
		    public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            } 
	        #endregion

            #region Name
		    public static readonly PropertyInfo<String> NameProperty =
                RegisterProperty<String>(c => c.Name);
            public String Name
            {
                get { return ReadProperty<String>(NameProperty); }
            } 
	        #endregion

            public FetchByEntityIdNameCriteria(Int32 entityId, String name)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<String>(NameProperty, name);
            }
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <param name="entityId">The entityid to which this belongs</param>
        /// <returns></returns>
        public static ProductUniverse NewProductUniverse(Int32 entityId)
        {
            ProductUniverse item = new ProductUniverse();
            item.Create(entityId);
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <param name="entityId">The entityid to which this belongs</param>
        /// <param name="productGroupId">The product group id</param>
        /// <returns>A new object</returns>
        public static ProductUniverse NewProductUniverse(Int32 entityId, Int32 productGroupId)
        {
            ProductUniverse item = new ProductUniverse();
            item.Create(entityId, productGroupId);
            return item;
        }
        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when this instance is being created
        /// </summary>
        private void Create(Int32 entityId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.LoadProperty<Guid>(UniqueContentReferenceProperty, Guid.NewGuid());
            this.LoadProperty<Boolean>(IsMasterProperty, false);
            this.LoadProperty<ProductUniverseProductList>(ProductsProperty, ProductUniverseProductList.NewProductUniverseProductList());
            //this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            base.Create();
        }

        /// <summary>
        /// Called when this instance is being created
        /// </summary>
        private void Create(Int32 entityId, Int32 productGroupId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.LoadProperty<Int32?>(ProductGroupIdProperty, productGroupId);
            this.LoadProperty<Guid>(UniqueContentReferenceProperty, Guid.NewGuid());
            this.LoadProperty<Boolean>(IsMasterProperty, false);
            this.LoadProperty<ProductUniverseProductList>(ProductsProperty, ProductUniverseProductList.NewProductUniverseProductList());
            //this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            base.Create();
        }

        #endregion

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        protected override void MarkNew()
        {
            base.MarkNew();

            //load a new guid.
            LoadProperty<Guid>(UniqueContentReferenceProperty, Guid.NewGuid());
        }

        #endregion
    }
}
