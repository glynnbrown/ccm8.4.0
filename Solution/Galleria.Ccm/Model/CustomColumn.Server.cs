﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26076 : A.Silva ~ Removed Id property, use Path to identify.
// V8-26472 : A.Probyn
//      Removed obsolete CustomColumn fields.
//      Consolodated ColumnTotalType and TotalType which were being used in Model Object and user cache differently.
// V8-27504 : A.Silva
//      Amended to account for HeaderGroupNumber property.

#endregion
#region Version History: (CCM 8.2)
// V8-30870 : L.Ineson
//  Added Width and DisplayName
#endregion

#region Version History: (CCM 8.3)
// V8-31542 : L.Ineson
//  Added Id and updated implementation.
//V8-32122 : L.Ineson
//  Removed HeaderGroupNumber
#endregion

#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Server side implementation of <see cref="CustomColumn" />.
    /// </summary>
    public sealed partial class CustomColumn
    {
        #region Constructor
        private CustomColumn(){} //Force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        ///     Fetches an existing <see cref="CustomColumn" /> from the DAL using the <paramref name="dalContext" />.
        /// </summary>
        /// <param name="dalContext">DAL context to access the data.</param>
        /// <param name="dto">The data transfer object to use when fetching.</param>
        /// <returns>Returns an instance with the <see cref="CustomColumn" />.</returns>
        internal static CustomColumn FetchCustomColumn(IDalContext dalContext, CustomColumnDto dto)
        {
            return DataPortal.FetchChild<CustomColumn>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Creates a data transfer object from this instance
        /// </summary>
        /// <returns>A new instance of <see cref="CustomColumnDto" /> with the data from this instance.</returns>
        private CustomColumnDto GetDataTransferObject()
        {
            return new CustomColumnDto
            {
                Id =ReadProperty<Int32>(IdProperty),
                CustomColumnLayoutId = Parent.Id,
                Path = ReadProperty<String>(PathProperty),
                Number = ReadProperty<Int32>(NumberProperty),
                TotalType = (Byte)ReadProperty<CustomColumnTotalType>(TotalTypeProperty),
                DisplayName = ReadProperty<String>(DisplayNameProperty),
                Width = ReadProperty<Int32>(WidthProperty),
            };
        }

        /// <summary>
        ///     Loads the data contained in the <paramref name="dto" /> into this instance.
        /// </summary>
        /// <param name="dalContext">Not used, kept as standard signature.</param>
        /// <param name="dto">Data transfer object containing the data to be loaded.</param>
        private void LoadDataTransferObject(IDalContext dalContext, CustomColumnDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<String>(PathProperty, dto.Path);
            LoadProperty<Int32>(NumberProperty, dto.Number);
            LoadProperty<CustomColumnTotalType>(TotalTypeProperty, (CustomColumnTotalType)dto.TotalType);
            LoadProperty<String>(DisplayNameProperty, dto.DisplayName);
            LoadProperty<Int32>(WidthProperty, dto.Width);
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Loads this instance as a child with data from the <paramref name="dto" />.
        /// </summary>
        /// <param name="dalContext">Not used, kept as standard signature.</param>
        /// <param name="dto">Data transfer object containing the data to be loaded.</param>
        /// <remarks>Called by CSLA via reflection.</remarks>
        private void Child_Fetch(IDalContext dalContext, CustomColumnDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts this instance as a child into the DAL indicated by the <paramref name="dalContext" />.
        /// </summary>
        /// <param name="dalContext">Context to obtain a DAL object from.</param>
        /// <remarks>Called by CSLA via reflection.</remarks>
        private void Child_Insert(IDalContext dalContext)
        {
            using (ICustomColumnDal dal = dalContext.GetDal<ICustomColumnDal>())
            {
                CustomColumnDto dto = GetDataTransferObject();
                dal.Insert(dto);

                LoadProperty<Int32>(IdProperty, dto.Id);
            }
            FieldManager.UpdateChildren(dalContext,this); 
        }

        #endregion

        #region Update

        /// <summary>
        ///     Updates this instance as a child into the DAL indicated by the <paramref name="dalContext" />.
        /// </summary>
        /// <param name="dalContext">Context to obtain a DAL object from.</param>
        /// <remarks>Called by CSLA via reflection.</remarks>
        private void Child_Update(IDalContext dalContext)
        {
            using (ICustomColumnDal dal = dalContext.GetDal<ICustomColumnDal>())
            {
                dal.Update(GetDataTransferObject());
            }
            FieldManager.UpdateChildren(dalContext,this);
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes this instance as a child from the DAL indicated by the <paramref name="dalContext" />.
        /// </summary>
        /// <param name="dalContext">Context to obtain a DAL object from.</param>
        /// <remarks>
        ///     Called by CSLA via reflection.
        ///     It uses the <see cref="Path" /> property to identify the <see cref="CustomColumn" /> in the collection of columns.
        ///     It works well with just one <see cref="CustomColumnLayout" /> in the cache, but with more, it will need to be
        ///     modified to discriminate the parent.
        /// </remarks>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (ICustomColumnDal dal = dalContext.GetDal<ICustomColumnDal>())
            {
                dal.DeleteById(this.Id);
            }
        }

        #endregion

        #endregion
    }
}