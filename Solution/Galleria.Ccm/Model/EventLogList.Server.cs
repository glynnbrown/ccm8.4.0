﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : L.Luong
//   Created (Copied From GFS)
// V8-27404 : L.Luong
//   Changed LevelId to EntryType
#endregion

#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//	Extended for new WorkpackageName property
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class EventLogList
    {
        #region Constructor
        private EventLogList() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an entity info from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        /// <returns>An entity info object</returns>
        public static EventLogList FetchAll()
        {
            return DataPortal.Fetch<EventLogList>();
        }

        /// <summary>
        /// Returns the entity with the given id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static EventLogList FetchByEventLogNameId(Int32 Id)
        {
            return DataPortal.Fetch<EventLogList>(Id);
        }

        /// <summary>
        /// Returns the entity with the given id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static EventLogList FetchAllOccuredAfterDateTime(DateTime? lastUpdatedDateTime)
        {
            return DataPortal.Fetch<EventLogList>(lastUpdatedDateTime);
        }

        /// <summary>
        /// Returns the entity with the given id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static EventLogList FetchTopSpecifiedAmountIncludingDeleted(Int32 amountOfRecordsToReturn)
        {
            return DataPortal.Fetch<EventLogList>(new FetchTopBySpecifiedAmountCriteria(amountOfRecordsToReturn));
        }

        /// <summary>
        /// Returns the entity with the given id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static EventLogList FetchByEventLogNameIdEntryTypeEntityId(Int16 topRowCount, Nullable<Int32> eventLogName_Id, Nullable<Int16> entryType, Nullable<Int32> Entity_Id)
        {
            return DataPortal.Fetch<EventLogList>(new FetchByEventLogNameIdEntryTypeEntityIdCriteria(topRowCount, eventLogName_Id, entryType, Entity_Id));
        }

        /// <summary>
        /// Returns a list of event logs fulfilling given mixed set of criteria
        /// </summary>
        /// <returns></returns>
        public static EventLogList FetchByMixedCriteria(Int16 topRowCount, Nullable<Int32> eventLogNameId, String source,
            Nullable<Int32> eventId, Nullable<Int16> entryType, Nullable<Int32> userId, Nullable<Int32> entityId, Nullable<DateTime> dateTime,
            String process, String computerName, String urlHelperLink, String description,
            String content, String gibraltarSessionId, DateTime startDate, DateTime endDate, String workpackageName,
            Int32? workpackageId)
        {
            return DataPortal.Fetch<EventLogList>(new FetchBySelectionOfCriteria(topRowCount, eventLogNameId, source, eventId,
                entryType, userId, entityId, dateTime, process, computerName, urlHelperLink, description, content, gibraltarSessionId, startDate, endDate, workpackageName, workpackageId));
        }

        /// <summary>
        /// Returns the event logs for the specified workpackage id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static EventLogList FetchByWorkPackageId(Int32 workpackageId)
        {
            return DataPortal.Fetch<EventLogList>(new FetchByWorkpackageIdCriteria(workpackageId));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when return FetchById
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch()
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
                {
                    IEnumerable<EventLogDto> dtoList = dal.FetchAll();
                    foreach (EventLogDto dto in dtoList)
                    {
                        this.Add(EventLog.GetEventLog(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;
            MarkAsChild();
        }

        /// <summary>
        /// Called when loading this list from the entity
        /// </summary>
        /// <param name="criteria">Fetch criteria</param>
        private void DataPortal_Fetch(Int32 Id)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                //fetch all locations for the entity
                IEnumerable<EventLogDto> EventLogDtoList;
                using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
                {
                    EventLogDtoList = dal.FetchByEventLogNameId(Id);
                }

                //load the locations
                foreach (EventLogDto dto in EventLogDtoList)
                {
                    this.Add(EventLog.GetEventLog(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
            MarkAsChild();
        }


        /// <summary>
        /// Called when loading event logs from datetime
        /// </summary>
        /// <param name="criteria">Fetch criteria</param>
        private void DataPortal_Fetch(DateTime dateLastUpdated)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                //fetch all events
                IEnumerable<EventLogDto> EventLogDtoList;
                using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
                {
                    EventLogDtoList = dal.FetchAllOccuredAfterDateTime(dateLastUpdated);
                }

                //load the locations
                foreach (EventLogDto dto in EventLogDtoList)
                {
                    this.Add(EventLog.GetEventLog(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
            MarkAsChild();
        }

        /// <summary>
        /// Called when loading this list from the entity
        /// </summary>
        /// <param name="criteria">Fetch criteria</param>
        private void DataPortal_Fetch(FetchByEventLogNameIdEntryTypeEntityIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                //fetch all locations for the entity
                IEnumerable<EventLogDto> EventLogDtoList;
                using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
                {
                    EventLogDtoList = dal.FetchByEventLogNameIdEntryTypeEntityId((short)criteria.TopRowCount, criteria.EventLogNameId, criteria.EntryType, criteria.EntityId);
                }

                //load the locations
                foreach (EventLogDto dto in EventLogDtoList)
                {
                    this.Add(EventLog.GetEventLog(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
            MarkAsChild();
        }

        /// <summary>
        /// Called when return Fetch my selection of criteria
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchBySelectionOfCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                //fetch all locations for the entity
                IEnumerable<EventLogDto> EventLogDtoList;
                using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
                {
                    EventLogDtoList = dal.FetchByMixedCriteria((short)criteria.TopRowCount, criteria.EventLogNameId,
                        criteria.Source, criteria.EventId, criteria.EntryType, criteria.UserId, criteria.EntityId,
                        criteria.DateTime, criteria.Process, criteria.ComputerName, criteria.URLHelperLink, criteria.Description,
                        criteria.Content, criteria.GibraltarSessionId, criteria.StartDate, criteria.EndDate, criteria.WorkpackageName,
                        criteria.WorkpackageId);
                }

                //load the locations
                foreach (EventLogDto dto in EventLogDtoList)
                {
                    this.Add(EventLog.GetEventLog(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
            MarkAsChild();
        }


        /// <summary>
        /// Called when loading top specified amount of records
        /// </summary>
        /// <param name="criteria">Fetch criteria</param>
        private void DataPortal_Fetch(FetchTopBySpecifiedAmountCriteria amountOfRecordsToReturn)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                //fetch all events
                IEnumerable<EventLogDto> EventLogDtoList;
                using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
                {
                    EventLogDtoList = dal.FetchTopSpecifiedAmountIncludingDeleted(amountOfRecordsToReturn.AmountOfRecordsToReturn);
                }

                //load the locations
                foreach (EventLogDto dto in EventLogDtoList)
                {
                    this.Add(EventLog.GetEventLog(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
            MarkAsChild();
        }


        /// <summary>
        /// Called when loading this list from the entity
        /// </summary>
        /// <param name="criteria">Fetch criteria</param>
        private void DataPortal_Fetch(FetchByWorkpackageIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                //fetch all event logs for the workpackage id
                using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
                {
                    IEnumerable<EventLogDto> dtoList = dal.FetchByWorkpackageId(criteria.WorkpackageId);
                    foreach (EventLogDto dto in dtoList)
                    {
                        this.Add(EventLog.GetEventLog(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;
            MarkAsChild();
        }


        #endregion

        #endregion
    }
}
