﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8025632 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Linq;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    public partial class ConsumerDecisionTreeLevel
    {
        #region Constructor
        private ConsumerDecisionTreeLevel() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static ConsumerDecisionTreeLevel FetchConsumerDecisionTreeLevel(
            IDalContext dalContext,
            ConsumerDecisionTreeLevelDto dto,
            IEnumerable<ConsumerDecisionTreeLevelDto> treeLevelList)
        {
            return DataPortal.FetchChild<ConsumerDecisionTreeLevel>(dalContext, dto, treeLevelList);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private ConsumerDecisionTreeLevelDto GetDataTransferObject()
        {
            return new ConsumerDecisionTreeLevelDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                ConsumerDecisionTreeId = this.ParentConsumerDecisionTree.Id,
                Name = ReadProperty<String>(NameProperty),
                ParentLevelId = (this.ParentLevel != null) ? (Int32?)this.ParentLevel.Id : null
            };
        }

        /// <summary>
        /// Loads this object from the given dto
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, ConsumerDecisionTreeLevelDto dto,
            IEnumerable<ConsumerDecisionTreeLevelDto> treeLevelList)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<String>(NameProperty, dto.Name);

            //check if there is a child level to this
            ConsumerDecisionTreeLevelDto childLevelDto =
                treeLevelList.FirstOrDefault(l => l.ParentLevelId.Equals(this.Id));
            if (childLevelDto != null)
            {
                this.ChildLevel = ConsumerDecisionTreeLevel.FetchConsumerDecisionTreeLevel(dalContext, childLevelDto, treeLevelList);
                MarkClean();
            }
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when this instance is being loaded
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ConsumerDecisionTreeLevelDto dto,
            IEnumerable<ConsumerDecisionTreeLevelDto> treeLevelList)
        {
            this.LoadDataTransferObject(dalContext, dto, treeLevelList);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when this instance is being inserted into the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent product universe</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext)
        {
            ConsumerDecisionTreeLevelDto dto = GetDataTransferObject();
            Int32 oldId = dto.Id;
            using (IConsumerDecisionTreeLevelDal dal = dalContext.GetDal<IConsumerDecisionTreeLevelDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<ConsumerDecisionTreeLevel>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when this instance is being updated in the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent product universe</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            if (this.IsSelfDirty)
            {
                using (IConsumerDecisionTreeLevelDal dal = dalContext.GetDal<IConsumerDecisionTreeLevelDal>())
                {
                    dal.Update(this.GetDataTransferObject());
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when this instance is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IConsumerDecisionTreeLevelDal dal = dalContext.GetDal<IConsumerDecisionTreeLevelDal>())
            {
                dal.DeleteById(this.Id);
            }

            //mark any child levels of this as deleted
            foreach (ConsumerDecisionTreeLevel child in this.ChildLevelList)
            {
                //forcibly mark the child as deleted
                ((Csla.Core.IEditableBusinessObject)child).DeleteChild();
            }

            //make sure this updates its children so they get deleted if required.
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #endregion

    }
}