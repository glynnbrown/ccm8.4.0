﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25559 : L.Ineson
//		Copied from SA
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model server implementation of the Connection
    /// </summary>
    public partial class Connection
    {
        #region Constructors
        private Connection() { }//Force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing Connection
        /// </summary>
        /// <returns>Existing Connection</returns>
        internal static Connection GetConnection(IDalContext dalContext, ConnectionDto dto)
        {
            return DataPortal.FetchChild<Connection>(dalContext, dto);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Method to compare connection against another
        /// </summary>
        /// <returns></returns>
        public Boolean Compare(Connection comparisonConnection)
        {
            if (this.ServerName == comparisonConnection.ServerName &&
                this.DatabaseFileName == comparisonConnection.DatabaseFileName &&
                    this.DatabaseName == comparisonConnection.DatabaseName &&
                        this.HostName == comparisonConnection.HostName &&
                            this.PortNumber == comparisonConnection.PortNumber)
            {
                return true;
            }
            return false;
        }

        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Returns a new dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private ConnectionDto GetDataTransferObject()
        {
            return new ConnectionDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                //Type = (Byte)ReadProperty<ConnectionType>(TypeProperty),
                ServerName = ReadProperty<String>(ServerNameProperty),
                DatabaseName = ReadProperty<String>(DatabaseNameProperty),
                DatabaseFileName = ReadProperty<String>(DatabaseFileNameProperty),
                HostName = ReadProperty<String>(HostNameProperty),
                PortNumber = (Byte)ReadProperty<Byte>(PortNumberProperty),
                BusinessEntityId = ReadProperty<Int32>(BusinessEntityIdProperty),
                IsAutoConnect = ReadProperty<Boolean>(IsAutoConnectProperty),
                IsCurrentConnection = ReadProperty<Boolean>(IsCurrentConnectionProperty),
            };
        }
        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dalContext">The dalcontext to use</param>
        /// <param name="dto">The Dto to Load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, ConnectionDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            //LoadProperty<ConnectionType>(TypeProperty, (ConnectionType)dto.Type);
            LoadProperty<String>(ServerNameProperty, dto.ServerName);
            LoadProperty<String>(DatabaseNameProperty, dto.DatabaseName);
            LoadProperty<String>(DatabaseFileNameProperty, dto.DatabaseFileName);
            LoadProperty<String>(HostNameProperty, dto.HostName);
            LoadProperty<Byte>(PortNumberProperty, dto.PortNumber);
            LoadProperty<Int32>(BusinessEntityIdProperty, dto.BusinessEntityId);
            LoadProperty<Boolean>(IsAutoConnectProperty, dto.IsAutoConnect);
            LoadProperty<Boolean>(IsCurrentConnectionProperty, dto.IsCurrentConnection);
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Called when returning an existing Object
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ConnectionDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this object Int32o a data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext)
        {
            ConnectionDto dto = this.GetDataTransferObject();
            Int32 oldId = dto.Id;
            using (IConnectionDal dal = dalContext.GetDal<IConnectionDal>())
            {
                dal.Insert(dto);
            }
            dalContext.RegisterId<Connection>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// caled when updating this object in the data store
        /// </summary>
        /// <param name="dalContext"></param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            if (this.IsSelfDirty)
            {
                using (IConnectionDal dal = dalContext.GetDal<IConnectionDal>())
                {
                    dal.Update(this.GetDataTransferObject());
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #endregion
    }
}
