﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26944 : A.Silva ~ Created.
// V8-27004 : A.Silva ~ Properly implemented.

#endregion

#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     PlanogramValidationMetric List of Infos.
    /// </summary>
    /// <remarks>This class is a child object.</remarks>
    [Serializable]
    public sealed partial class PlanogramValidationMetricInfoList : ModelReadOnlyList<PlanogramValidationMetricInfoList, PlanogramValidationMetricInfo>
    {
        #region Authorization Rules

        /// <summary>
        ///     Defines the authorization rules for the <see cref="PlanogramValidationMetricInfoList"/> type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PlanogramValidationMetricInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanogramValidationMetricInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(PlanogramValidationMetricInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanogramValidationMetricInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }

        #endregion
    }
}