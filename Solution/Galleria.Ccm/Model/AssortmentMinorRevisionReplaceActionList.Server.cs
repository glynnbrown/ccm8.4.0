﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Csla;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentMinorRevisionReplaceActionList
    {
        #region Constructors
        private AssortmentMinorRevisionReplaceActionList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an existing Assortment minor revision action list
        /// </summary>
        /// <param name="childData"></param>
        /// <returns>A new Assortment minor revision action list</returns>
        internal static AssortmentMinorRevisionReplaceActionList FetchByAssortmentMinorRevisionId(IDalContext dalContext, Int32 assortmentMinorRevisionId)
        {
            return DataPortal.FetchChild<AssortmentMinorRevisionReplaceActionList>(dalContext, assortmentMinorRevisionId);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="assortmentMinorRevisionId">The parent assortment minor revision id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 assortmentMinorRevisionId)
        {
            RaiseListChangedEvents = false;
            using (IAssortmentMinorRevisionReplaceActionDal dal = dalContext.GetDal<IAssortmentMinorRevisionReplaceActionDal>())
            {
                IEnumerable<AssortmentMinorRevisionReplaceActionDto> dtoList = dal.FetchByAssortmentMinorRevisionId(assortmentMinorRevisionId);
                foreach (AssortmentMinorRevisionReplaceActionDto dto in dtoList)
                {
                    this.Add(AssortmentMinorRevisionReplaceAction.FetchAssortmentMinorRevisionReplaceAction(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
