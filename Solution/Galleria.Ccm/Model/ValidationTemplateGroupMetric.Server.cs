﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created (Auto-generated)
// V8-26514 : A.Silva ~ Added Score Properties.
// V8-26814 : A.Silva ~ Corrected the loading of AggregationTypeProperty to be of the correct type.
// V8-26812 : A.Silva ~ Added ValidationType Property.

#endregion

#region Version History: CCM803

// V8-29596 : L.Luong
//  Added Criteria

#endregion

#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    public partial class ValidationTemplateGroupMetric
    {
        #region Constructor

        private ValidationTemplateGroupMetric()
        {
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Returns an existing item from the given dto
        /// </summary>
        internal static ValidationTemplateGroupMetric Fetch(IDalContext dalContext, ValidationTemplateGroupMetricDto dto)
        {
            return DataPortal.FetchChild<ValidationTemplateGroupMetric>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Creates a dto from this object
        /// </summary>
        private ValidationTemplateGroupMetricDto GetDataTransferObject(ValidationTemplateGroup parent)
        {
            return new ValidationTemplateGroupMetricDto
            {
                Id = ReadProperty(IdProperty),
                ValidationTemplateGroupId = parent.Id,
                Field = ReadProperty(FieldProperty),
                Threshold1 = ReadProperty(Threshold1Property),
                Threshold2 = ReadProperty(Threshold2Property),
                Score1 = ReadProperty(Score1Property),
                Score2 = ReadProperty(Score2Property),
                Score3 = ReadProperty(Score3Property),
                AggregationType = (Byte)ReadProperty(AggregationTypeProperty),
                ValidationType = (Byte) ReadProperty(ValidationTypeProperty),
                Criteria = ReadProperty(CriteriaProperty)
            };
        }

        /// <summary>
        ///     Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, ValidationTemplateGroupMetricDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(FieldProperty, dto.Field);
            LoadProperty(Threshold1Property, dto.Threshold1);
            LoadProperty(Threshold2Property, dto.Threshold2);
            LoadProperty(Score1Property, dto.Score1);
            LoadProperty(Score2Property, dto.Score2);
            LoadProperty(Score3Property, dto.Score3);
            LoadProperty(AggregationTypeProperty, (PlanogramValidationAggregationType) dto.AggregationType);
            LoadProperty(ValidationTypeProperty, (PlanogramValidationType) dto.ValidationType);
            LoadProperty(CriteriaProperty, dto.Criteria);
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ValidationTemplateGroupMetricDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        ///     Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, ValidationTemplateGroup parent)
        {
            ValidationTemplateGroupMetricDto dto = GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (var dal = dalContext.GetDal<IValidationTemplateGroupMetricDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty(IdProperty, dto.Id);
            dalContext.RegisterId<ValidationTemplateGroupMetric>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        ///     Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, ValidationTemplateGroup parent)
        {
            if (this.IsSelfDirty)
            {
                using (IValidationTemplateGroupMetricDal dal = dalContext.GetDal<IValidationTemplateGroupMetricDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        ///     Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, ValidationTemplateGroup parent)
        {
            using (var dal = dalContext.GetDal<IValidationTemplateGroupMetricDal>())
            {
                dal.DeleteById(Id);
            }
        }

        #endregion

        #endregion
    }
}