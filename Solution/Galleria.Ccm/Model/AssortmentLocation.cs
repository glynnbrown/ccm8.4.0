﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
#endregion

#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using AutoMapper;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A class representing an Assortment Location within CCM
    /// (Child of Assortment)
    /// This is a child Content object and so it has the following properties/actions:
    /// - This object does NOT have a DateDeleted property or DateDeleted field in its supporting database table
    /// - if its parent is deleted, it is not removed or marked as deleted (this allows for parent 'undeletes')
    /// - if it is deleted directly, it's associated record is removed from the database. 
    /// </summary>
    [Serializable]
    [DefaultNewMethod("NewAssortmentLocation")]
    public partial class AssortmentLocation : ModelObject<AssortmentLocation>
    {
        #region Static Constructor
        static AssortmentLocation()
        {
        }
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The AssortmentLocation id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<Int16> LocationIdProperty =
            RegisterModelProperty<Int16>(c => c.LocationId);
        /// <summary>
        /// The Location Id
        /// </summary>
        public Int16 LocationId
        {
            get { return GetProperty<Int16>(LocationIdProperty); }
            set { SetProperty<Int16>(LocationIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> CodeProperty =
            RegisterModelProperty<String>(c => c.Code);
        /// <summary>
        /// The location Code
        /// </summary>
        public String Code
        {
            get { return GetProperty<String>(CodeProperty); }
            set { SetProperty<String>(CodeProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The location name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new Required(LocationIdProperty));
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentLocation), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.AssortmentCreate.ToString()));
            BusinessRules.AddRule(typeof(AssortmentLocation), new IsInRole(AuthorizationActions.GetObject, DomainPermission.AssortmentGet.ToString()));
            BusinessRules.AddRule(typeof(AssortmentLocation), new IsInRole(AuthorizationActions.EditObject, DomainPermission.AssortmentEdit.ToString()));
            BusinessRules.AddRule(typeof(AssortmentLocation), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.AssortmentDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentLocation NewAssortmentLocation()
        {
            AssortmentLocation item = new AssortmentLocation();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentLocation NewAssortmentLocation(Int16 locationId)
        {
            AssortmentLocation item = new AssortmentLocation();
            item.Create(locationId);
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentLocation NewAssortmentLocation(Location location)
        {
            AssortmentLocation item = new AssortmentLocation();
            item.Create(location);
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentLocation NewAssortmentLocation(LocationInfo locationInfo)
        {
            AssortmentLocation item = new AssortmentLocation();
            item.Create(locationInfo);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create(Int16 locationId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int16>(LocationIdProperty, locationId);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create(Location location)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int16>(LocationIdProperty, location.Id);
            this.LoadProperty<String>(CodeProperty, location.Code);
            this.LoadProperty<String>(NameProperty, location.Name);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create(LocationInfo locationInfo)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int16>(LocationIdProperty, locationInfo.Id);
            this.LoadProperty<String>(CodeProperty, locationInfo.Code);
            this.LoadProperty<String>(NameProperty, locationInfo.Name);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Overrides

        public override string ToString()
        {
            return String.Format("{0} {1}", this.Code, this.Name);
        }

        #endregion
    }
}