﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.1)
//V8-28622 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public sealed partial class PlanogramImportTemplatePerformanceMetricList
    {
        #region Constructor
        private PlanogramImportTemplatePerformanceMetricList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        internal static PlanogramImportTemplatePerformanceMetricList FetchByParentId(IDalContext dalContext, Object parentId)
        {
            return DataPortal.FetchChild<PlanogramImportTemplatePerformanceMetricList>(dalContext, new FetchByParentIdCriteria(null, parentId));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FetchByParentIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            using (IPlanogramImportTemplatePerformanceMetricDal dal = dalContext.GetDal<IPlanogramImportTemplatePerformanceMetricDal>())
            {
                IEnumerable<PlanogramImportTemplatePerformanceMetricDto> dtoList = dal.FetchByPlanogramImportTemplateId(criteria.ParentId);
                foreach (PlanogramImportTemplatePerformanceMetricDto dto in dtoList)
                {
                    this.Add(PlanogramImportTemplatePerformanceMetric.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}