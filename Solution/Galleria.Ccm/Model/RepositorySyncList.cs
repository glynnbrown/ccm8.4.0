﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a list of repository sync operations
    /// </summary>
    public partial class RepositorySyncList : ModelList<RepositorySyncList, RepositorySync>
    {
        #region Static Constructor
        static RepositorySyncList()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(RepositorySyncList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(RepositorySyncList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(RepositorySyncList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(RepositorySyncList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Temporary.ToString()));
        }
        #endregion

        #region Methods
        /// <summary>
        /// Indicates if this collection contains an item with the specified id
        /// </summary>
        /// <param name="id">The id to check for</param>
        /// <returns>True if the item exists, else false</returns>
        public Boolean Contains(Int32 id)
        {
            foreach (RepositorySync item in this)
            {
                if (item.Id == id) return true;
            }
            return false;
        }
        #endregion
    }
}
