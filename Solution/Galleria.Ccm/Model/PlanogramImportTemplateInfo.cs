﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24779 : L.Luong
//  Created.

#endregion
#region Version History: (CCM 8.03)
// V8-29220 : L.Ineson
//Removed date deleted
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class PlanogramImportTemplateInfo : ModelReadOnlyObject<PlanogramImportTemplateInfo>, IDisposable
    {
        #region Properties

        #region Id
       
        public static readonly ModelPropertyInfo<Object> IdProperty =
         RegisterModelProperty<Object>(c => c.Id);
        /// <summary>
        /// The unique object id
        /// </summary>
        public Object Id
        {
            get { return GetProperty<Object>(IdProperty); }
        }

        #endregion

        #region EntityId

        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(o => o.EntityId);
        /// <summary>
        ///		Gets or sets the value for the <see cref="EntityId"/> property.
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty(EntityIdProperty); }
        }

        #endregion

        #region Name

        public static readonly ModelPropertyInfo<String> NameProperty =
           RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The product name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Overrides

        public override String ToString()
        {
            return Name;
        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    //Unlock();
                }

                _isDisposed = true;
            }
        }

        #endregion
    }
}
