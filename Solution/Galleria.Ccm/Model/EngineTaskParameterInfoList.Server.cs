﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion
#endregion

using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Engine;

namespace Galleria.Ccm.Model
{
    public partial class EngineTaskParameterInfoList
    {
        #region Constructors
        private EngineTaskParameterInfoList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all parameters for the specified task
        /// </summary>
        public static EngineTaskParameterInfoList GetEngineTaskParameterInfoList(IEnumerable<TaskParameter> parameters)
        {
            return DataPortal.FetchChild<EngineTaskParameterInfoList>(parameters);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when fetching a list of this type
        /// </summary>
        private void Child_Fetch(IEnumerable<TaskParameter> parameters)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            foreach (TaskParameter parameter in parameters)
            {
                this.Add(EngineTaskParameterInfo.GetEngineTaskParameterInfo(parameter));
            }
            this.IsReadOnly = false;
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
