﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
// V8-26704 : A.Kuszyk
//  Implemented IPlanogramAssortmentRegionLocation
#endregion

#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentRegionLocation
    {
        #region Constructor
        private AssortmentRegionLocation() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating an instance from a dto
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new instance of this type</returns>
        internal static AssortmentRegionLocation GetAssortmentRegionLocation(IDalContext dalContext, AssortmentRegionLocationDto dto)
        {
            return DataPortal.FetchChild<AssortmentRegionLocation>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, AssortmentRegionLocationDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Int16>(LocationIdProperty, dto.LocationId);
            LoadProperty<String>(LocationCodeProperty, dto.LocationCode);
        }

        /// <summary>
        /// Returns a dto created from this instance
        /// </summary>
        /// <param name="parent">The Assortment Region</param>
        /// <returns>A data transfer object</returns>
        private AssortmentRegionLocationDto GetDataTransferObject(AssortmentRegion parent)
        {
            return new AssortmentRegionLocationDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                LocationId = ReadProperty<Int16>(LocationIdProperty),
                AssortmentRegionId = parent.Id,
                LocationCode = ReadProperty<String>(LocationCodeProperty)
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when this instance is being loaded
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, AssortmentRegionLocationDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when this instance is being inserted into the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent assortment</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, AssortmentRegion parent)
        {
            AssortmentRegionLocationDto dto = GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IAssortmentRegionLocationDal dal = dalContext.GetDal<IAssortmentRegionLocationDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<AssortmentRegionLocation>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when this instance is being updated in the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent assortment</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, AssortmentRegion parent)
        {
            if (this.IsSelfDirty)
            {
                using (IAssortmentRegionLocationDal dal = dalContext.GetDal<IAssortmentRegionLocationDal>())
                {
                    dal.Update(GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this instance is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        private void Child_DeleteSelf(IDalContext dalContext, AssortmentRegion parent)
        {
            using (IAssortmentRegionLocationDal dal = dalContext.GetDal<IAssortmentRegionLocationDal>())
            {
                dal.DeleteById(this.Id);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #endregion
    }
}