﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentRegionProductList
    {
        #region Constructor
        private AssortmentRegionProductList() { } // Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a list of existing items based on their parent assortment region
        /// </summary>
        /// <returns>A list of existing it</returns>
        internal static AssortmentRegionProductList GetAssortmentRegionProductList(IDalContext dalContext, Int32 assortmentRegionId)
        {
            return DataPortal.FetchChild<AssortmentRegionProductList>(dalContext, assortmentRegionId);
        }

        /// <summary>
        /// Returns all products for the given assortment region
        /// </summary>
        /// <returns>A list of assortment regional products</returns>
        public static AssortmentRegionProductList FetchByRegionId(Int32 assortmentRegionId)
        {
            return DataPortal.Fetch<AssortmentRegionProductList>(assortmentRegionId);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="assortmentId">The parent assortment region id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 assortmentRegionId)
        {
            RaiseListChangedEvents = false;
            using (IAssortmentRegionProductDal dal = dalContext.GetDal<IAssortmentRegionProductDal>())
            {
                IEnumerable<AssortmentRegionProductDto> dtoList = dal.FetchByRegionId(assortmentRegionId);
                foreach (AssortmentRegionProductDto dto in dtoList)
                {
                    this.Add(AssortmentRegionProduct.GetAssortmentRegionProduct(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when retrieving a list of all regions for an assortment
        /// </summary>
        private void DataPortal_Fetch(Int32 assortmentRegionId)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IAssortmentRegionProductDal dal = dalContext.GetDal<IAssortmentRegionProductDal>())
                {
                    IEnumerable<AssortmentRegionProductDto> dtoList = dal.FetchByRegionId(assortmentRegionId);
                    foreach (AssortmentRegionProductDto dto in dtoList)
                    {
                        this.Add(AssortmentRegionProduct.GetAssortmentRegionProduct(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}