﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25886 : L.Ineson
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Engine;

namespace Galleria.Ccm.Model
{
    public partial class EngineTaskParameterEnumValueInfo
    {
        #region Constructors
        private EngineTaskParameterEnumValueInfo() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a task parameter info object
        /// </summary>
        internal static EngineTaskParameterEnumValueInfo GetEngineTaskParameterEnumValueInfo(TaskParameterEnumValue parameterValue)
        {
            return DataPortal.FetchChild<EngineTaskParameterEnumValueInfo>(parameterValue);
        }
        #endregion

        #region Data Access
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(TaskParameterEnumValue parameterValue)
        {
            this.LoadProperty<Object>(ValueProperty, parameterValue.Value);
            this.LoadProperty<String>(NameProperty, parameterValue.Name);
            this.LoadProperty<String>(DescriptionProperty, parameterValue.Description);
        }
        #endregion
    }
}
