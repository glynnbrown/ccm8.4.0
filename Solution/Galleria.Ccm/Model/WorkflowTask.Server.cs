﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion

#region Version History : CCM 801
// V8-28835 : A.Kuszyk
//  Removed call to FieldManager.UpdateChildren() in delete method.
#endregion

#region Version History: CCM830
// V8-32357 : M.Brumby
//  [PCR01561] added DisplayName and DisplayDescription
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class WorkflowTask
    {
        #region Constructors
        private WorkflowTask() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a new instance loaded from a dto
        /// </summary>
        internal static WorkflowTask GetWorkflowTask(IDalContext dalContext, WorkflowTaskDto dto)
        {
            return DataPortal.FetchChild<WorkflowTask>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, WorkflowTaskDto dto)
        {
            EngineTaskInfo task = EngineTaskInfo.GetEngineTaskInfo(dto.TaskType);
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Byte>(SequenceIdProperty, dto.SequenceId);
            this.LoadProperty<EngineTaskInfo>(DetailsProperty, task);
            this.LoadProperty<String>(DisplayNameProperty, dto.DisplayName);
            this.LoadProperty<String>(DisplayDescriptionProperty, dto.DisplayDescription);
            this.LoadProperty<WorkflowTaskParameterList>(ParametersProperty, WorkflowTaskParameterList.FetchByWorkflowTaskId(dalContext, dto.Id, task));
        }

        /// <summary>
        /// Gets a data transfer object from this instance
        /// </summary>
        private WorkflowTaskDto GetDataTransferObject(Workflow parent)
        {
            return new WorkflowTaskDto()
            {
                Id = this.ReadProperty<Int32>(IdProperty),
                SequenceId = this.ReadProperty<Byte>(SequenceIdProperty),
                TaskType = this.Details.TaskType,
                WorkflowId = parent.Id,
                DisplayName = this.ReadProperty<String>(DisplayNameProperty),
                DisplayDescription = this.ReadProperty<String>(DisplayDescriptionProperty)
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when loading this instance from a dto
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, WorkflowTaskDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(IDalContext dalContext, Workflow parent)
        {
            WorkflowTaskDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IWorkflowTaskDal dal = dalContext.GetDal<IWorkflowTaskDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<WorkflowTask>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(IDalContext dalContext, Workflow parent)
        {
            if (this.IsSelfDirty)
            {
                using (IWorkflowTaskDal dal = dalContext.GetDal<IWorkflowTaskDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this child is being deleted
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, Workflow parent)
        {
            using (IWorkflowTaskDal dal = dalContext.GetDal<IWorkflowTaskDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion
    }
}
