﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
// V8-32396 : A.Probyn
//  Removed obsolete properties
#endregion

#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using AutoMapper;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Rules;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A class representing an Assortment Location Buddy within CCM
    /// (Child of Assortment)
    /// This is a child Content object and so it has the following properties/actions:
    /// - This object does NOT have a DateDeleted property or DateDeleted field in its supporting database table
    /// - if its parent is deleted, it is not removed or marked as deleted (this allows for parent 'undeletes')
    /// - if it is deleted directly, it's associated record is removed from the database. 
    /// </summary>
    [Serializable]
    [DefaultNewMethod("NewAssortmentLocationBuddy")]
    public partial class AssortmentLocationBuddy : ModelObject<AssortmentLocationBuddy>, IPlanogramAssortmentLocationBuddy
    {
        #region Static Constructor
        static AssortmentLocationBuddy()
        {
        }
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The AssortmentLocationBuddy id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<Int16> LocationIdProperty =
            RegisterModelProperty<Int16>(c => c.LocationId);
        /// <summary>
        /// The Location Id
        /// </summary>
        public Int16 LocationId
        {
            get { return GetProperty<Int16>(LocationIdProperty); }
            set { SetProperty<Int16>(LocationIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> LocationCodeProperty =
            RegisterModelProperty<String>(c => c.LocationCode);
        /// <summary>
        /// The Location Code
        /// </summary>
        public String LocationCode
        {
            get { return GetProperty<String>(LocationCodeProperty); }
            set { SetProperty<String>(LocationCodeProperty, value); }
        }

        public static ModelPropertyInfo<PlanogramAssortmentLocationBuddyTreatmentType> TreatmentTypeProperty =
        RegisterModelProperty<PlanogramAssortmentLocationBuddyTreatmentType>(c => c.TreatmentType);
        public PlanogramAssortmentLocationBuddyTreatmentType TreatmentType
        {
            get { return GetProperty<PlanogramAssortmentLocationBuddyTreatmentType>(TreatmentTypeProperty); }
            set { SetProperty<PlanogramAssortmentLocationBuddyTreatmentType>(TreatmentTypeProperty, value); }
        }

        #region S1

        public static readonly ModelPropertyInfo<Int16?> S1LocationIdProperty =
            RegisterModelProperty<Int16?>(c => c.S1LocationId);
        /// <summary>
        /// The LocationId of the source location 1
        /// </summary>
        public Int16? S1LocationId
        {
            get { return GetProperty<Int16?>(S1LocationIdProperty); }
            set { SetProperty<Int16?>(S1LocationIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> S1LocationCodeProperty =
           RegisterModelProperty<String>(c => c.S1LocationCode);
        /// <summary>
        /// The source 1 location code
        /// </summary>
        public String S1LocationCode
        {
            get { return GetProperty<String>(S1LocationCodeProperty); }
            set { SetProperty<String>(S1LocationCodeProperty, value); }
        }

        public static readonly ModelPropertyInfo<Single?> S1PercentageProperty =
        RegisterModelProperty<Single?>(c => c.S1Percentage);
        public Single? S1Percentage
        {
            get { return GetProperty<Single?>(S1PercentageProperty); }
            set { SetProperty<Single?>(S1PercentageProperty, value); }
        }

        #endregion

        #region S2

        public static readonly ModelPropertyInfo<Int16?> S2LocationIdProperty =
            RegisterModelProperty<Int16?>(c => c.S2LocationId);
        /// <summary>
        /// The LocationId of the source location 2
        /// </summary>
        public Int16? S2LocationId
        {
            get { return GetProperty<Int16?>(S2LocationIdProperty); }
            set { SetProperty<Int16?>(S2LocationIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> S2LocationCodeProperty =
           RegisterModelProperty<String>(c => c.S2LocationCode);
        /// <summary>
        /// The source 2 location code
        /// </summary>
        public String S2LocationCode
        {
            get { return GetProperty<String>(S2LocationCodeProperty); }
            set { SetProperty<String>(S2LocationCodeProperty, value); }
        }

        public static readonly ModelPropertyInfo<Single?> S2PercentageProperty =
        RegisterModelProperty<Single?>(c => c.S2Percentage);
        public Single? S2Percentage
        {
            get { return GetProperty<Single?>(S2PercentageProperty); }
            set { SetProperty<Single?>(S2PercentageProperty, value); }
        }

        #endregion

        #region S3

        public static readonly ModelPropertyInfo<Int16?> S3LocationIdProperty =
            RegisterModelProperty<Int16?>(c => c.S3LocationId);
        /// <summary>
        /// The LocationId of the source location 3
        /// </summary>
        public Int16? S3LocationId
        {
            get { return GetProperty<Int16?>(S3LocationIdProperty); }
            set { SetProperty<Int16?>(S3LocationIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> S3LocationCodeProperty =
           RegisterModelProperty<String>(c => c.S3LocationCode);
        /// <summary>
        /// The source 3 location code
        /// </summary>
        public String S3LocationCode
        {
            get { return GetProperty<String>(S3LocationCodeProperty); }
            set { SetProperty<String>(S3LocationCodeProperty, value); }
        }

        public static readonly ModelPropertyInfo<Single?> S3PercentageProperty =
        RegisterModelProperty<Single?>(c => c.S3Percentage);
        public Single? S3Percentage
        {
            get { return GetProperty<Single?>(S3PercentageProperty); }
            set { SetProperty<Single?>(S3PercentageProperty, value); }
        }

        #endregion

        #region S4

        public static readonly ModelPropertyInfo<Int16?> S4LocationIdProperty =
            RegisterModelProperty<Int16?>(c => c.S4LocationId);
        /// <summary>
        /// The LocationId of the source location 4
        /// </summary>
        public Int16? S4LocationId
        {
            get { return GetProperty<Int16?>(S4LocationIdProperty); }
            set { SetProperty<Int16?>(S4LocationIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> S4LocationCodeProperty =
           RegisterModelProperty<String>(c => c.S4LocationCode);
        /// <summary>
        /// The source 4 location code
        /// </summary>
        public String S4LocationCode
        {
            get { return GetProperty<String>(S4LocationCodeProperty); }
            set { SetProperty<String>(S4LocationCodeProperty, value); }
        }

        public static readonly ModelPropertyInfo<Single?> S4PercentageProperty =
        RegisterModelProperty<Single?>(c => c.S4Percentage);
        public Single? S4Percentage
        {
            get { return GetProperty<Single?>(S4PercentageProperty); }
            set { SetProperty<Single?>(S4PercentageProperty, value); }
        }

        #endregion

        #region S5

        public static readonly ModelPropertyInfo<Int16?> S5LocationIdProperty =
            RegisterModelProperty<Int16?>(c => c.S5LocationId);
        /// <summary>
        /// The LocationId of the source location 5
        /// </summary>
        public Int16? S5LocationId
        {
            get { return GetProperty<Int16?>(S5LocationIdProperty); }
            set { SetProperty<Int16?>(S5LocationIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> S5LocationCodeProperty =
           RegisterModelProperty<String>(c => c.S5LocationCode);
        /// <summary>
        /// The source 5 location code
        /// </summary>
        public String S5LocationCode
        {
            get { return GetProperty<String>(S5LocationCodeProperty); }
            set { SetProperty<String>(S5LocationCodeProperty, value); }
        }

        public static readonly ModelPropertyInfo<Single?> S5PercentageProperty =
        RegisterModelProperty<Single?>(c => c.S5Percentage);
        public Single? S5Percentage
        {
            get { return GetProperty<Single?>(S5PercentageProperty); }
            set { SetProperty<Single?>(S5PercentageProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {

            base.AddBusinessRules();

            BusinessRules.AddRule(new NullableMaxValue<Single>(S1PercentageProperty, 10));
            BusinessRules.AddRule(new NullableMaxValue<Single>(S2PercentageProperty, 10));
            BusinessRules.AddRule(new NullableMaxValue<Single>(S3PercentageProperty, 10));
            BusinessRules.AddRule(new NullableMaxValue<Single>(S4PercentageProperty, 10));
            BusinessRules.AddRule(new NullableMaxValue<Single>(S5PercentageProperty, 10));
            BusinessRules.AddRule(new NullableMinValue<Single>(S1PercentageProperty, 0.01f));
            BusinessRules.AddRule(new NullableMinValue<Single>(S2PercentageProperty, 0.01f));
            BusinessRules.AddRule(new NullableMinValue<Single>(S3PercentageProperty, 0.01f));
            BusinessRules.AddRule(new NullableMinValue<Single>(S4PercentageProperty, 0.01f));
            BusinessRules.AddRule(new NullableMinValue<Single>(S5PercentageProperty, 0.01f));
            BusinessRules.AddRule(new MaxLength(S1PercentageProperty, 4));
            BusinessRules.AddRule(new MaxLength(S2PercentageProperty, 4));
            BusinessRules.AddRule(new MaxLength(S3PercentageProperty, 4));
            BusinessRules.AddRule(new MaxLength(S4PercentageProperty, 4));
            BusinessRules.AddRule(new MaxLength(S5PercentageProperty, 4));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentLocationBuddy), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.AssortmentCreate.ToString()));
            BusinessRules.AddRule(typeof(AssortmentLocationBuddy), new IsInRole(AuthorizationActions.GetObject, DomainPermission.AssortmentGet.ToString()));
            BusinessRules.AddRule(typeof(AssortmentLocationBuddy), new IsInRole(AuthorizationActions.EditObject, DomainPermission.AssortmentEdit.ToString()));
            BusinessRules.AddRule(typeof(AssortmentLocationBuddy), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.AssortmentDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentLocationBuddy NewAssortmentLocationBuddy()
        {
            AssortmentLocationBuddy item = new AssortmentLocationBuddy();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentLocationBuddy NewAssortmentLocationBuddy(AssortmentLocation location)
        {
            AssortmentLocationBuddy item = new AssortmentLocationBuddy();
            item.Create(location);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create(AssortmentLocation assortmentLocation)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int16>(LocationIdProperty, assortmentLocation.LocationId);
            this.LoadProperty<String>(LocationCodeProperty, assortmentLocation.Code);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion
    }
}
