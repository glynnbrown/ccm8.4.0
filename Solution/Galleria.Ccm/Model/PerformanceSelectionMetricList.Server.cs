﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History : (CCM CCM800)

// CCM800-26953 : I.George
//   Initial Version

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public sealed partial class PerformanceSelectionMetricList
    {
        #region Constructor
        private PerformanceSelectionMetricList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a list of existing items based on their parent location space
        /// </summary>
        /// <returns>A list of existing it</returns>
        internal static PerformanceSelectionMetricList FetchByPerformanceSelectionId(IDalContext dalContext, Int32 parentId)
        {
            return DataPortal.FetchChild<PerformanceSelectionMetricList>(dalContext, parentId);
        }
        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="productUniverseId">The parent location space id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 parentId)
        {
            RaiseListChangedEvents = false;
            using (IPerformanceSelectionMetricDal dal = dalContext.GetDal<IPerformanceSelectionMetricDal>())
            {
                IEnumerable<PerformanceSelectionMetricDto> dtoList = dal.FetchByPerformanceSelectionId(parentId);
                foreach (PerformanceSelectionMetricDto dto in dtoList)
                {
                    this.Add(PerformanceSelectionMetric.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true; 
        }
        #endregion

        #endregion
    }
}
