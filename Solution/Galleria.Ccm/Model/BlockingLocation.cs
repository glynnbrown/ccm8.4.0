﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
//	Created (Auto-generated)
// CCM-27420 : L.Ineson
//  Update helper properties now has option to 
//  use LoadProperty on first call so that this doesn't immediately get marked dirty.
// V8-27957 : N.Foster
//  Implement CCM security
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// BlockingLocation Model object
    /// </summary>
    [Serializable]
    public sealed partial class BlockingLocation : ModelObject<BlockingLocation>, IPlanogramBlockingLocation
    {
        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Blocking Parent
        {
            get
            {
                BlockingLocationList parentList = base.Parent as BlockingLocationList;
                if (parentList != null)
                {
                    return parentList.Parent as Blocking;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region BlockingGroupId

        /// <summary>
        /// BlockingGroupId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> BlockingGroupIdProperty =
            RegisterModelProperty<Int32>(c => c.BlockingGroupId);

        /// <summary>
        /// Gets/Sets the BlockingGroupId value
        /// </summary>
        public Int32 BlockingGroupId
        {
            get { return GetProperty<Int32>(BlockingGroupIdProperty); }
            set 
            {
                Int32 currentValue = this.ReadProperty<Int32>(BlockingGroupIdProperty);
                if (value != currentValue)
                {
                    SetProperty<Int32>(BlockingGroupIdProperty, value);
                    NotifyObjectGraph(BlockingGroupIdProperty);
                    OnPropertyChanged("PlanogramBlockingGroupId");
                }
            }
        }

        Object IPlanogramBlockingLocation.PlanogramBlockingGroupId
        {
            get { return BlockingGroupId; }
            set { BlockingGroupId = (Int32)value; }
        }

        #endregion

        #region BlockingDividerTopId

        /// <summary>
        /// BlockingDividerTopId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> BlockingDividerTopIdProperty =
            RegisterModelProperty<Int32?>(c => c.BlockingDividerTopId);

        /// <summary>
        /// Gets/Sets the BlockingDividerTopId value
        /// </summary>
        public Int32? BlockingDividerTopId
        {
            get { return GetProperty<Int32?>(BlockingDividerTopIdProperty); }
            set 
            {
                SetProperty<Int32?>(BlockingDividerTopIdProperty, value);

                //Update the affected helper properties
                UpdateHelperProperties();

                //also notify that the instance property value has changed too.
                OnPropertyChanged("PlanogramBlockingDividerTopId");
            }
        }

        Object IPlanogramBlockingLocation.PlanogramBlockingDividerTopId
        {
            get { return BlockingDividerTopId; }
            set { BlockingDividerTopId = (Int32)value; }
        }

        #endregion

        #region BlockingDividerBottomId

        /// <summary>
        /// BlockingDividerBottomId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> BlockingDividerBottomIdProperty =
            RegisterModelProperty<Int32?>(c => c.BlockingDividerBottomId);

        /// <summary>
        /// Gets/Sets the BlockingDividerBottomId value
        /// </summary>
        public Int32? BlockingDividerBottomId
        {
            get { return GetProperty<Int32?>(BlockingDividerBottomIdProperty); }
            set 
            { 
                SetProperty<Int32?>(BlockingDividerBottomIdProperty, value);

                //Update the affected helper properties
                UpdateHelperProperties();

                //also notify that the instance property value has changed too.
                OnPropertyChanged("PlanogramBlockingDividerBottomId");
            }
        }

        Object IPlanogramBlockingLocation.PlanogramBlockingDividerBottomId
        {
            get { return BlockingDividerBottomId; }
            set { BlockingDividerBottomId = (Int32)value; }
        }

        #endregion

        #region BlockingDividerLeftId

        /// <summary>
        /// BlockingDividerLeftId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> BlockingDividerLeftIdProperty =
            RegisterModelProperty<Int32?>(c => c.BlockingDividerLeftId);

        /// <summary>
        /// Gets/Sets the BlockingDividerLeftId value
        /// </summary>
        public Int32? BlockingDividerLeftId
        {
            get { return GetProperty<Int32?>(BlockingDividerLeftIdProperty); }
            set 
            { 
                SetProperty<Int32?>(BlockingDividerLeftIdProperty, value);

                //Update the affected helper properties
                UpdateHelperProperties();

                //also notify that the instance property value has changed too.
                OnPropertyChanged("PlanogramBlockingDividerLeftId");
            }
        }

        Object IPlanogramBlockingLocation.PlanogramBlockingDividerLeftId
        {
            get { return BlockingDividerLeftId; }
            set { BlockingDividerLeftId = (Int32)value; }
        }

        #endregion

        #region BlockingDividerRightId

        /// <summary>
        /// BlockingDividerRightId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> BlockingDividerRightIdProperty =
            RegisterModelProperty<Int32?>(c => c.BlockingDividerRightId);

        /// <summary>
        /// Gets/Sets the BlockingDividerRightId value
        /// </summary>
        public Int32? BlockingDividerRightId
        {
            get { return GetProperty<Int32?>(BlockingDividerRightIdProperty); }
            set 
            { 
                SetProperty<Int32?>(BlockingDividerRightIdProperty, value);

                //Update the affected helper properties
                UpdateHelperProperties();

                //also notify that the instance property value has changed too.
                OnPropertyChanged("PlanogramBlockingDividerRightId");
            }
        }

        Object IPlanogramBlockingLocation.PlanogramBlockingDividerRightId
        {
            get { return BlockingDividerRightId; }
            set { BlockingDividerRightId = (Int32)value; }
        }

        #endregion

        #region SpacePercentage

        /// <summary>
        /// SpacePercentage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SpacePercentageProperty =
            RegisterModelProperty<Single>(c => c.SpacePercentage);

        /// <summary>
        /// Returns the percentage of space allocated to this location.
        /// This is a percentage value between 0 and 1
        /// </summary>
        public Single SpacePercentage
        {
            get { return GetProperty<Single>(SpacePercentageProperty); }
            private set
            {
                Single currentValue = this.ReadProperty<Single>(SpacePercentageProperty);
                if (value != currentValue)
                {
                    SetProperty<Single?>(SpacePercentageProperty, value);
                    NotifyObjectGraph(SpacePercentageProperty);
                }
            }
        }

        #endregion
       

        #region X (Calculated)

        public static readonly ModelPropertyInfo<Single?>XProperty =
             RegisterModelProperty<Single?>(c => c.X);

        /// <summary>
        /// Returns the x coordiate of this location
        /// as a percentage between 0 and 1
        /// </summary>
        public Single X
        {
            get
            {
                Single? value = GetProperty<Single?>(XProperty);
                if (value == null)
                {
                    UpdateHelperProperties(/*initialLoad*/true);
                    return GetProperty<Single?>(XProperty).Value;
                }
                return value.Value;
            }
            private set { SetProperty<Single?>(XProperty, value); }
        }

        #endregion

        #region Y (Calculated)

        public static readonly ModelPropertyInfo<Single?> YProperty =
            RegisterModelProperty<Single?>(c => c.Y);

        /// <summary>
        /// Returns the x coordiate of this location
        /// as a percentage between 0 and 1
        /// </summary>
        public Single Y
        {
            get
            {
                Single? value = GetProperty<Single?>(YProperty);
                if (value == null)
                {
                    UpdateHelperProperties(/*initialLoad*/true);
                    return GetProperty<Single?>(YProperty).Value;
                }
                return value.Value;
            }
            private set { SetProperty<Single?>(YProperty, value); }
        }

        #endregion

        #region Width (Calculated)

        public static readonly ModelPropertyInfo<Single?> WidthProperty =
            RegisterModelProperty<Single?>(c => c.Width);

        /// <summary>
        /// Returns the x coordiate of this location
        /// as a percentage between 0 and 1
        /// </summary>
        public Single Width
        {
            get
            {
                Single? value = GetProperty<Single?>(WidthProperty);
                if (value == null)
                {
                    UpdateHelperProperties(/*initialLoad*/true);
                    return GetProperty<Single?>(WidthProperty).Value;
                }
                return value.Value;
            }
            private set { SetProperty<Single?>(WidthProperty, value); }
        }


        #endregion

        #region Height (Calculated)

        public static readonly ModelPropertyInfo<Single?> HeightProperty =
             RegisterModelProperty<Single?>(c => c.Height);

        /// <summary>
        /// Returns the x coordiate of this location
        /// as a percentage between 0 and 1
        /// </summary>
        public Single Height
        {
            get
            {
                Single? value = GetProperty<Single?>(HeightProperty);
                if (value == null)
                {
                    UpdateHelperProperties(/*initialLoad*/true);
                    return GetProperty<Single?>(HeightProperty).Value;
                }
                return value.Value;
            }
            private set { SetProperty<Single?>(HeightProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();


        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(BlockingLocation), new IsInRole(AuthorizationActions.GetObject, DomainPermission.BlockingGet.ToString()));
            BusinessRules.AddRule(typeof(BlockingLocation), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.BlockingCreate.ToString()));
            BusinessRules.AddRule(typeof(BlockingLocation), new IsInRole(AuthorizationActions.EditObject, DomainPermission.BlockingEdit.ToString()));
            BusinessRules.AddRule(typeof(BlockingLocation), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.BlockingDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static BlockingLocation NewBlockingLocation()
        {
            BlockingLocation item = new BlockingLocation();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static BlockingLocation NewBlockingLocation(BlockingGroup group)
        {
            BlockingLocation item = new BlockingLocation();
            item.Create(group);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(BlockingGroup group)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(BlockingGroupIdProperty, group.Id);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<BlockingLocation>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes on this instance
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }

        /// <summary>
        /// Resolves ids for this instance
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // BlockingGroupId
            Object blockingGroupId = context.ResolveId<BlockingGroup>(this.ReadProperty<Int32>(BlockingGroupIdProperty));
            if (blockingGroupId != null) this.LoadProperty<Int32>(BlockingGroupIdProperty, (Int32)blockingGroupId);

            // BlockingDividerTopId
            Object blockingDividerTopId = context.ResolveId<BlockingDivider>(this.ReadProperty<Int32?>(BlockingDividerTopIdProperty));
            if (blockingDividerTopId != null) this.LoadProperty<Int32?>(BlockingDividerTopIdProperty, (Int32?)blockingDividerTopId);

            // BlockingDividerBottomId
            Object blockingDividerBottomId = context.ResolveId<BlockingDivider>(this.ReadProperty<Int32?>(BlockingDividerBottomIdProperty));
            if (blockingDividerBottomId != null) this.LoadProperty<Int32?>(BlockingDividerBottomIdProperty, (Int32?)blockingDividerBottomId);

            // BlockingDividerLeftId
            Object blockingDividerLeftId = context.ResolveId<BlockingDivider>(this.ReadProperty<Int32?>(BlockingDividerLeftIdProperty));
            if (blockingDividerLeftId != null) this.LoadProperty<Int32?>(BlockingDividerLeftIdProperty, (Int32?)blockingDividerLeftId);

            // BlockingDividerRightId
            Object blockingDividerRightId = context.ResolveId<BlockingDivider>(this.ReadProperty<Int32?>(BlockingDividerRightIdProperty));
            if (blockingDividerRightId != null) this.LoadProperty<Int32?>(BlockingDividerRightIdProperty, (Int32?)blockingDividerRightId);
        }

        /// <summary>
        /// Called whenever the root object of this tree notifies that a model within the tree has changed.
        /// </summary>
        protected override void OnObjectGraphMemberChanged(object graphMember, 
            System.ComponentModel.PropertyChangedEventArgs propertyChangedArgs, 
            System.Collections.Specialized.NotifyCollectionChangedEventArgs collectionChangedArgs)
        {
            base.OnObjectGraphMemberChanged(graphMember, propertyChangedArgs, collectionChangedArgs);

            //AllocatedSpace
            BlockingDivider divider = graphMember as BlockingDivider;
            if (divider == null) return;

            if (IsAssociated(divider))
            {
                UpdateHelperProperties();
            }

        }

        /// <summary>
        /// Updates the helper properties of this location
        /// </summary>
        private void UpdateHelperProperties(Boolean initialLoad = false)
        {
            BlockingDivider bottomDivider = GetBottomDivider();
            BlockingDivider topDivider = GetTopDivider();
            BlockingDivider leftDivider = GetLeftDivider();
            BlockingDivider rightDivider = GetRightDivider();

            Single leftX = (leftDivider != null) ? leftDivider.X : 0;
            Single rightX = (rightDivider != null) ? rightDivider.X : 1;
            Single bottomY = (bottomDivider != null) ? bottomDivider.Y : 0;
            Single topY = (topDivider != null) ? topDivider.Y : 1;

            Single newX = leftX;
            Single newY = bottomY;
            Single newWidth = Convert.ToSingle(Math.Round(rightX - leftX, 5));
            Single newHeight = Convert.ToSingle(Math.Round(topY - bottomY, 5));
            Single newSpacePercent = Convert.ToSingle(Math.Round(newWidth * newHeight, 5));

            if (initialLoad)
            {
                LoadProperty(XProperty, newX);
                LoadProperty(YProperty, newY);
                LoadProperty(WidthProperty, newWidth);
                LoadProperty(HeightProperty, newHeight);
                LoadProperty(SpacePercentageProperty, newSpacePercent);
            }
            else
            {
                this.X = newX;
                this.Y = newY;
                this.Width = newWidth;
                this.Height = newHeight;
                this.SpacePercentage = newSpacePercent;
            }
        }

        /// <summary>
        /// Returns the blocking group to which this location
        /// is associated.
        /// </summary>
        /// <returns></returns>
        public BlockingGroup GetBlockingGroup()
        {
            if (this.Parent == null) return null;
            return this.Parent.Groups.FirstOrDefault(g => g.Id == this.BlockingGroupId);
        }

        /// <summary>
        /// Returns the divider adjacent to the 
        /// bottom of this block
        /// </summary>
        /// <returns></returns>
        public BlockingDivider GetBottomDivider()
        {
            if (!this.BlockingDividerBottomId.HasValue) return null;
            if (this.Parent == null) return null;

            return this.Parent.Dividers.FirstOrDefault(g => g.Id == this.BlockingDividerBottomId.Value);
        }

        /// <summary>
        /// Returns the divider adjacent to the 
        /// left of this block
        /// </summary>
        /// <returns></returns>
        public BlockingDivider GetLeftDivider()
        {
            if (!this.BlockingDividerLeftId.HasValue) return null;
            if (this.Parent == null) return null;

            return this.Parent.Dividers.FirstOrDefault(g => g.Id == this.BlockingDividerLeftId.Value);
        }

        /// <summary>
        /// Returns the divider adjacent to the 
        /// right of this block
        /// </summary>
        /// <returns></returns>
        public BlockingDivider GetRightDivider()
        {
            if (!this.BlockingDividerRightId.HasValue) return null;
            if (this.Parent == null) return null;

            return this.Parent.Dividers.FirstOrDefault(g => g.Id == this.BlockingDividerRightId.Value);
        }

        /// <summary>
        /// Returns the divider adjacent to the 
        /// top of this block
        /// </summary>
        /// <returns></returns>
        public BlockingDivider GetTopDivider()
        {
            if (!this.BlockingDividerTopId.HasValue) return null;
            if (this.Parent == null) return null;

            return this.Parent.Dividers.FirstOrDefault(g => g.Id == this.BlockingDividerTopId.Value);
        }

        /// <summary>
        /// Returns true if this blocking is associated with the given divider 
        /// </summary>
        /// <param name="divider"></param>
        /// <returns></returns>
        public Boolean IsAssociated(IPlanogramBlockingDivider divider)
        {
            if (divider == null) return false;

            BlockingDivider d = divider as BlockingDivider;
            if (d == null) return false;

            //return true if any of the ids here link to the divider given.
            return (this.BlockingDividerLeftId == d.Id
                || this.BlockingDividerRightId == d.Id
                || this.BlockingDividerTopId == d.Id
                || this.BlockingDividerBottomId == d.Id);
        }

        /// <summary>
        /// Returns true if this location can be combined with the one given
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        public Boolean CanCombineWith(BlockingLocation location)
        {
            return BlockingHelper.CanCombine(location, this);
        }

        /// <summary>
        /// Combine this location with the one given.
        /// </summary>
        /// <param name="location"></param>
        public void CombineWith(BlockingLocation location)
        {
            //check we have a parent blocking.
            Blocking parentBlocking = this.Parent;
            if (parentBlocking == null) return;

            //return out if we cant combine
            if (!CanCombineWith(location)) return;


            BlockingGroup groupToRemove = GetBlockingGroup();

            //update the group id of this block to the same and the loc
            // we are combining with.
            this.BlockingGroupId = location.BlockingGroupId;

            //remove the old group if it is now unused.
            if (groupToRemove.GetBlockingLocations().Count == 0)
            {
                parentBlocking.Groups.Remove(groupToRemove);
            }
        }

        /// <summary>
        /// Returns true if this location is adjacent.
        /// </summary>
        /// <param name="loc"></param>
        /// <returns></returns>
        public Boolean IsAdjacent(BlockingLocation loc)
        {
            if (loc == this) return false;

            return (loc.BlockingDividerLeftId == this.BlockingDividerRightId
                        || loc.BlockingDividerRightId == this.BlockingDividerLeftId
                        || loc.BlockingDividerTopId == this.BlockingDividerBottomId
                        || loc.BlockingDividerBottomId == this.BlockingDividerTopId);
        }

        /// <summary>
        /// Returns a list of locations that are adjacent to this one.
        /// </summary>
        /// <returns></returns>
        public List<BlockingLocation> GetAdjacentLocations()
        {
            List<BlockingLocation> adjacentLocations = new List<BlockingLocation>();

            Blocking parentBlocking = this.Parent;
            if (parentBlocking != null)
            {
                foreach (BlockingLocation loc in parentBlocking.Locations)
                {
                    if (IsAdjacent(loc))
                    {
                        adjacentLocations.Add(loc);
                    }
                }
            }

            return adjacentLocations;
        }

        #region IPlanogramBlockingLocation Members

        /// <summary>
        /// Returns the blocking group that this location is associated with.
        /// </summary>
        /// <returns></returns>
        IPlanogramBlockingGroup IPlanogramBlockingLocation.GetBlockingGroup()
        {
            return GetBlockingGroup();
        }

        Boolean IPlanogramBlockingLocation.CanCombineWith(IPlanogramBlockingLocation location)
        {
            return CanCombineWith(location as BlockingLocation);
        }

        void IPlanogramBlockingLocation.CombineWith(IPlanogramBlockingLocation location)
        {
            CombineWith(location as BlockingLocation);
        }

        #endregion

        #endregion

    }
}