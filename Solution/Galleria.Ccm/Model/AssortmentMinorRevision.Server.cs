﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#region Version History: (CCM 8.0.3)
// V8-29214 : D.Pleasance
//  Removed DateDeleted as items are now deleted outright
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#region Version History: (CCM 8.1.0)
// V8-29936 : N.Haywood
//  Update will set a new ucr
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentMinorRevision
    {
        #region Constructor
        private AssortmentMinorRevision() { }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the specified AssortmentMinorRevision
        /// </summary>
        /// <param name="id">The id of the Assortment</param>
        /// <returns></returns>
        public static AssortmentMinorRevision GetById(Int32 id)
        {
            return DataPortal.Fetch<AssortmentMinorRevision>(new SingleCriteria<Int32>(id));
        }

        /// <summary>
        /// Returns the latest AssortmentMinorRevision with the specified name
        /// </summary>
        /// <param name="name">The AssortmentMinorRevision name</param>
        /// <returns>The latest version of the Assortment</returns>
        public static AssortmentMinorRevision FetchLatestVersionByEntityIdName(Int32 entityId, String name)
        {
            return DataPortal.Fetch<AssortmentMinorRevision>(new FetchByEntityIdNameCriteria(entityId, name));
        }

        /// <summary>
        /// returns the specified AssortmentMinorRevision
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static AssortmentMinorRevision GetAssortmentMinorRevision(IDalContext dalContext, AssortmentMinorRevisionDto dto)
        {
            return DataPortal.FetchChild<AssortmentMinorRevision>(dalContext, dto);
        }

        /// <summary>
        /// Deletes all Assortment with the given parent content id 
        /// and the parent content itself
        /// </summary>
        /// <param name="parentContentId"></param>
        public static void DeleteById(Int32 Id)
        {
            DataPortal.Delete<AssortmentMinorRevision>(new DeleteByAssortmentMinorRevisonIdCriteria(Id));
        }

        /// <summary>
        /// Returns the specified assortmentMinorRevision name
        /// </summary>
        public static AssortmentMinorRevision FetchByEntityIdName(Int32 entityId, String name)
        {
            return DataPortal.Fetch<AssortmentMinorRevision>(new FetchByEntityIdNameCriteria(entityId, name));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this object from a data transfer object
        /// </summary>
        /// <param name="context">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext context, AssortmentMinorRevisionDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Int32?>(ConsumerDecisionTreeIdProperty, dto.ConsumerDecisionTreeId);
            LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            LoadProperty<Guid>(UniqueContentReferenceProperty, dto.UniqueContentReference);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            LoadProperty<Int32>(ProductGroupIdProperty, dto.ProductGroupId); 
            LoadProperty<Guid?>(ParentUniqueContentReferenceProperty, dto.ParentUniqueContentReference);


            LoadProperty<AssortmentMinorRevisionDeListActionList>(DeListActionsProperty, AssortmentMinorRevisionDeListActionList.FetchByAssortmentMinorRevisionId(context, dto.Id));
            LoadProperty<AssortmentMinorRevisionListActionList>(ListActionsProperty, AssortmentMinorRevisionListActionList.FetchByAssortmentMinorRevisionContentId(context, dto.Id));
            LoadProperty<AssortmentMinorRevisionAmendDistributionActionList>(AmendDistributionActionsProperty, AssortmentMinorRevisionAmendDistributionActionList.FetchByAssortmentMinorRevisionId(context, dto.Id));
            LoadProperty<AssortmentMinorRevisionReplaceActionList>(ReplaceActionsProperty, AssortmentMinorRevisionReplaceActionList.FetchByAssortmentMinorRevisionId(context, dto.Id));

        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        /// <returns>A new data transfer object</returns>
        private AssortmentMinorRevisionDto GetDataTransferObject()
        {
            AssortmentMinorRevisionDto dto = new AssortmentMinorRevisionDto();
            dto.Id = ReadProperty<Int32>(IdProperty);
            dto.ConsumerDecisionTreeId = ReadProperty<Int32?>(ConsumerDecisionTreeIdProperty);
            dto.RowVersion = ReadProperty<RowVersion>(RowVersionProperty);
            dto.UniqueContentReference = ReadProperty<Guid>(UniqueContentReferenceProperty);
            dto.Name = ReadProperty<String>(NameProperty);
            dto.EntityId = ReadProperty<Int32>(EntityIdProperty);
            dto.DateCreated = ReadProperty<DateTime>(DateCreatedProperty);
            dto.DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty);
            dto.ProductGroupId = ReadProperty<Int32>(ProductGroupIdProperty);
            dto.ParentUniqueContentReference = ReadProperty<Guid?>(ParentUniqueContentReferenceProperty);
            return dto;
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching a Assortment by id
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IAssortmentMinorRevisionDal dal = dalContext.GetDal<IAssortmentMinorRevisionDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchById(criteria.Value));
                }
            }
        }

        /// <summary>
        /// Called when fetching a MacroSpace by name
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdNameCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IAssortmentMinorRevisionDal dal = dalContext.GetDal<IAssortmentMinorRevisionDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchByEntityIdName(criteria.EntityId, criteria.Name));
                }
            }
        }


        /// <summary>
        /// Called when fetching a product universe by dto
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto"></param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, AssortmentMinorRevisionDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this instance into the database
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                AssortmentMinorRevisionDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (IAssortmentMinorRevisionDal dal = dalContext.GetDal<IAssortmentMinorRevisionDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<Int32?>(ConsumerDecisionTreeIdProperty, dto.ConsumerDecisionTreeId);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<Guid>(UniqueContentReferenceProperty, dto.UniqueContentReference);
                this.LoadProperty<String>(NameProperty, dto.Name);
                this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
                this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<AssortmentMinorRevision>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when this instance is being updated
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                AssortmentMinorRevisionDto dto = GetDataTransferObject();
                dto.UniqueContentReference = Guid.NewGuid();
                using (IAssortmentMinorRevisionDal dal = dalContext.GetDal<IAssortmentMinorRevisionDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this instance is being deleted
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IAssortmentMinorRevisionDal dal = dalContext.GetDal<IAssortmentMinorRevisionDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }


        /// <summary>
        ///Called when deleting by ParentContentId
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Delete(DeleteByAssortmentMinorRevisonIdCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IAssortmentMinorRevisionDal dal = dalContext.GetDal<IAssortmentMinorRevisionDal>())
                {
                    dal.DeleteById(criteria.AssortmentId);
                }
            }
        }


        #endregion

        #endregion
    }
}
