﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Read only cutdown version of a <see cref="RenumberingStrategy"/> object.
    /// </summary>
    /// <remarks>This is a child object for <see cref="RenumberingStrategyInfoList"/>.</remarks>
    [Serializable]
    public partial class RenumberingStrategyInfo : ModelReadOnlyObject<RenumberingStrategyInfo>
    {
        #region Properties

        #region Id

        /// <summary>
        ///		Metadata for the <see cref="Id"/> property.
        /// </summary>
        private static readonly PropertyInfo<Int32> IdProperty =
            RegisterProperty<Int32>(o => o.Id);

        /// <summary>
        ///		Gets or sets the value for the <see cref="Id"/> property.
        /// </summary>
        public Int32 Id
        {
            get { return ReadProperty(IdProperty); }
        }

        #endregion

        #region EntityId

        /// <summary>
        ///		Metadata for the <see cref="EntityId"/> property.
        /// </summary>
        private static readonly PropertyInfo<Int32> EntityIdProperty =
            RegisterProperty<Int32>(o => o.EntityId);

        /// <summary>
        ///		Gets or sets the value for the <see cref="EntityId"/> property.
        /// </summary>
        public Int32 EntityId
        {
            get { return ReadProperty(EntityIdProperty); }
        }

        #endregion

        #region Name

        /// <summary>
        ///		Metadata for the <see cref="Name"/> property.
        /// </summary>
        public static readonly PropertyInfo<String> NameProperty =
            RegisterProperty<String>(o => o.Name);

        /// <summary>
        ///		Gets or sets the value for the <see cref="Name"/> property.
        /// </summary>
        public String Name
        {
            get { return ReadProperty(NameProperty); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(RenumberingStrategyInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(RenumberingStrategyInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(RenumberingStrategyInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(RenumberingStrategyInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Overrides

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:RenumberingStrategyInfo"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:RenumberingStrategyInfo"/>.
        /// </returns>
        public override string ToString()
        {
            return Name;
        }

        #endregion
    }
}
