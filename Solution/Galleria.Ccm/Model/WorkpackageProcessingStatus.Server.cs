﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26895 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  Aded description, dateStarted, dateLastUpdated properties to increment command
#endregion

#region Version History: CCM810
// V8-30079 : L.Ineson
//  Ammended status property enum type.
#endregion

#region Version History: CCM8.2.0
// V8-30646 : M.Shelley
//  Added SetPendingCommand 
#endregion

#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    public partial class WorkpackageProcessingStatus
    {
        #region Constructors
        private WorkpackageProcessingStatus() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the specified workpackage processing status
        /// </summary>
        public static WorkpackageProcessingStatus FetchByWorkpackageId(Int32 workpackageId)
        {
            return DataPortal.Fetch<WorkpackageProcessingStatus>(new SingleCriteria<Int32>(workpackageId));
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, WorkpackageProcessingStatusDto dto)
        {
            this.LoadProperty<Int32>(WorkpackageIdProperty, dto.WorkpackageId);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<WorkpackageProcessingStatusType>(StatusProperty, (WorkpackageProcessingStatusType)dto.Status);
            this.LoadProperty<String>(StatusDescriptionProperty, dto.StatusDescription);
            this.LoadProperty<Int32>(ProgressMaxProperty, dto.ProgressMax);
            this.LoadProperty<Int32>(ProgressCurrentProperty, dto.ProgressCurrent);
            this.LoadProperty<DateTime?>(DateStartedProperty, dto.DateStarted);
            this.LoadProperty<DateTime?>(DateLastUpdatedProperty, dto.DateLastUpdated);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private WorkpackageProcessingStatusDto GetDataTransferObject()
        {
            return new WorkpackageProcessingStatusDto()
            {
                WorkpackageId = this.ReadProperty<Int32>(WorkpackageIdProperty),
                RowVersion = this.ReadProperty<RowVersion>(RowVersionProperty),
                Status = (Byte)this.ReadProperty<WorkpackageProcessingStatusType>(StatusProperty),
                StatusDescription = this.ReadProperty<String>(StatusDescriptionProperty),
                ProgressMax = this.ReadProperty<Int32>(ProgressMaxProperty),
                ProgressCurrent = this.ReadProperty<Int32>(ProgressCurrentProperty),
                DateStarted = this.ReadProperty<DateTime?>(DateStartedProperty),
                DateLastUpdated = this.ReadProperty<DateTime?>(DateLastUpdatedProperty)
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching a single instance by its id
        /// </summary>
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IWorkpackageProcessingStatusDal dal = dalContext.GetDal<IWorkpackageProcessingStatusDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchByWorkpackageId(criteria.Value));
                }
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this instance into the data store
        /// </summary>
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                WorkpackageProcessingStatusDto dto = this.GetDataTransferObject();
                using (IWorkpackageProcessingStatusDal dal = dalContext.GetDal<IWorkpackageProcessingStatusDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #endregion

        #region Commands

        #region Increments
        /// <summary>
        /// Increments the progress of a workpackage
        /// </summary>
        private partial class IncrementCommand
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this command on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IWorkpackageProcessingStatusDal dal = dalContext.GetDal<IWorkpackageProcessingStatusDal>())
                    {
                        dal.Increment(this.WorkpackageId, this.StatusDescription, this.DateLastUpdated);
                    }
                }
            }
            #endregion

            #endregion
        }

        /// <summary>
        /// Increments the progress of a workpackage
        /// </summary>
        private partial class SetPendingCommand
        {
            #region Data Access

            #region Execute

            /// <summary>
            /// Called when executing this command on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IWorkpackageProcessingStatusDal dal = dalContext.GetDal<IWorkpackageProcessingStatusDal>())
                    {
                        dal.SetPending(this.WorkpackageId, (Byte) this.ProcessingStatus, this.StatusDescription);
                    }
                }
            }

            #endregion

            #endregion
        }

        #endregion

        #endregion
    }
}
