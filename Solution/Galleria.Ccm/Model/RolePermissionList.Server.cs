﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26234 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;
using Csla.Data;

using Galleria.Framework.Dal;

using Galleria.Ccm.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// List of permissions for a role
    /// </summary>
    public partial class RolePermissionList
    {
        #region Constructor
        private RolePermissionList() { } //Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all permissions for the specified role
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="roleId">The role id</param>
        /// <returns>A list of permissions for the specified role</returns>
        internal static RolePermissionList GetRolePermissions(IDalContext dalContext, Int32 roleId)
        {
            return DataPortal.FetchChild<RolePermissionList>(dalContext, roleId);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when populating this list for a role
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="roleId">The role id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 roleId)
        {
            RaiseListChangedEvents = false;
            using (IRolePermissionDal dal = dalContext.GetDal<IRolePermissionDal>())
            {
                foreach (RolePermissionDto dto in dal.FetchByRoleId(roleId))
                {
                    this.Add(RolePermission.GetRolePermission(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
