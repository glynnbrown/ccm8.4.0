﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-28258 : N.Foster
//	Created
#endregion
#endregion

using System;
using System.IO;
using System.Reflection;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Engine;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public static partial class TaskLibrary
    {
        #region Commands

        #region Register
        /// <summary>
        /// Registers an assembly within the solution
        /// </summary>
        private partial class RegisterCommand
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this command
            /// </summary>
            protected override void DataPortal_Execute()
            {
                // in order to load the assembly, we must first
                // create an app domain, so the assembly can be unloaded afterwards
                AppDomain appDomain = null;
                try
                {
                    // create a new app domain and load our assembly into it
                    appDomain = AppDomain.CreateDomain(Guid.NewGuid().ToString(), null, AppDomain.CurrentDomain.SetupInformation);
                    Assembly assembly = appDomain.Load(this.Data);
                    AssemblyName assemblyName = assembly.GetName();

                    // get the task library details
                    String fullName = assemblyName.Name;
                    Int32 major = assemblyName.Version.Major;
                    Int32 minor = assemblyName.Version.Minor;
                    Int32 revision = assemblyName.Version.Build; // NOTE - The way galleria tracks revision and build numbers is the opposite of how assemblies are tracked
                    Int32 build = assemblyName.Version.Revision;

                    // verify that the assembly has at least
                    // one task contained within it
                    Boolean taskLibraryContainsTasks = false;
                    foreach (Type assemblyType in assembly.GetTypes())
                    {
                        if (assemblyType.IsSubclassOf(typeof(TaskBase)))
                        {
                            taskLibraryContainsTasks = true;
                            break;
                        }
                    }

                    // if the task library contains no tasks
                    // then set the result and return
                    if (!taskLibraryContainsTasks)
                    {
                        this.Result = TaskLibraryRegisterResult.AssemblyContainsNoTasks;
                        return;
                    }

                    // update the database
                    IDalFactory dalFactory = DalContainer.GetDalFactory();
                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        using (ITaskLibraryDal dal = dalContext.GetDal<ITaskLibraryDal>())
                        {
                            // attempt to fetch the existing task library
                            TaskLibraryDto taskLibraryDto = null;
                            try
                            {
                                taskLibraryDto = dal.FetchByFullName(fullName);
                            }
                            catch (DtoDoesNotExistException) { }

                            // determine if the task library already exists
                            if (taskLibraryDto == null)
                            {
                                // insert the task library details into the database
                                dal.Insert(new TaskLibraryDto()
                                {
                                    FullName = fullName,
                                    Major = major,
                                    Minor = minor,
                                    Build = build,
                                    Revision = revision,
                                    Data = this.Data
                                });
                            }
                            else
                            {
                                // determine if the existing library is newer
                                // then the one we are trying to register
                                if (((taskLibraryDto.Major > major)) ||
                                    ((taskLibraryDto.Major == major) && (taskLibraryDto.Minor > minor)) ||
                                    ((taskLibraryDto.Major == major) && (taskLibraryDto.Minor == minor) && (taskLibraryDto.Revision > revision)) ||
                                    ((taskLibraryDto.Major == major) && (taskLibraryDto.Minor == minor) && (taskLibraryDto.Revision == revision) && (taskLibraryDto.Build >= build)))
                                {
                                    this.Result = TaskLibraryRegisterResult.AssemblyOutOfDate;
                                    return;
                                }

                                // update the dto
                                taskLibraryDto.Major = major;
                                taskLibraryDto.Minor = minor;
                                taskLibraryDto.Revision = revision;
                                taskLibraryDto.Build = build;
                                taskLibraryDto.Data = Data;

                                // update the database
                                dal.Update(taskLibraryDto);
                            }
                        }
                    }

                    // return success
                    this.Result = TaskLibraryRegisterResult.Successful;
                }
                catch (ConcurrencyException)
                {
                    this.Result = TaskLibraryRegisterResult.AssemblyOutOfDate;
                }
                catch
                {
                    this.Result = TaskLibraryRegisterResult.InvalidAssembly;
                }
                finally
                {
                    if (appDomain != null) AppDomain.Unload(appDomain);
                }
            }
            #endregion

            #endregion
        }
        #endregion

        #region DeleteById
        /// <summary>
        /// Deletes the specified assembly from the solution
        /// </summary>
        private partial class DeleteByIdCommand
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this command
            /// </summary>
            protected override void DataPortal_Execute()
            {
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (ITaskLibraryDal dal = dalContext.GetDal<ITaskLibraryDal>())
                    {
                        dal.DeleteById(this.Id);
                    }
                }
            }
            #endregion

            #endregion
        }
        #endregion

        #endregion
    }
}
