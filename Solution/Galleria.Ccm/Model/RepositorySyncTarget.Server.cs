﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model object that represents a repository sync target
    /// </summary>
    public partial class RepositorySyncTarget
    {
        #region Constants
        private const String _assemblyName = "AssemblyName";
        private const String _typeName = "TypeName";
        #endregion

        #region Constructor
        private RepositorySyncTarget() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the specified repository sync operation
        /// </summary>
        internal static RepositorySyncTarget FetchRepositorySyncTarget(IDalContext dalContext, Int32 id)
        {
            return DataPortal.FetchChild<RepositorySyncTarget>(dalContext, id);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private RepositorySyncTargetDto GetDataTransferObject(RepositorySync parent)
        {
            return new RepositorySyncTargetDto
            {
                Id = this.ReadProperty<Int32>(IdProperty),
                RepositoryType = (Byte)this.ReadProperty<RepositorySyncTargetType>(RepositoryTypeProperty)
            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        /// <Param name="dalContext"> The current dal context </param>
        private void LoadDataTransferObject(IDalContext dalContext, RepositorySyncTargetDto dto)
        {
            // load this model object
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RepositorySyncTargetType>(RepositoryTypeProperty, (RepositorySyncTargetType)dto.RepositoryType);
            this.LoadProperty<RepositorySyncTargetParameterList>(ParametersProperty, RepositorySyncTargetParameterList.FetchByRepositorySyncTargetId(dalContext, dto.Id));

            // now we have loaded the model object
            // parse the parameters for the expected parameters
            foreach (RepositorySyncTargetParameter parameter in this.Parameters)
            {
                switch (parameter.Name)
                {
                    case _assemblyName:
                        this.LoadProperty<String>(AssemblyNameProperty, parameter.Value);
                        break;
                    case _typeName:
                        this.LoadProperty<String>(TypeNameProperty, parameter.Value);
                        break;
                    default:
                        break;
                }
            }
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when loading a parameter from a dto
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="id">The id of the </param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 id)
        {
            using (IRepositorySyncTargetDal dal = dalContext.GetDal<IRepositorySyncTargetDal>())
            {
                this.LoadDataTransferObject(dalContext, dal.FetchById(id));
            }
        }
        #endregion

        #endregion
    }
}
