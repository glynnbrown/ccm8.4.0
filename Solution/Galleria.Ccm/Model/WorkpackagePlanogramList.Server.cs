﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Made changes to improve performance of engine
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class WorkpackagePlanogramList
    {
        #region Constructor
        private WorkpackagePlanogramList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an instance of this type
        /// </summary>
        internal static WorkpackagePlanogramList FetchByWorkpackageIdDestinationPlanogramId(IDalContext dalContext, Int32 workpackageId, Int32 destinationPlanogramId, Int32 workflowId)
        {
            return DataPortal.FetchChild<WorkpackagePlanogramList>(dalContext, workpackageId, destinationPlanogramId, workflowId);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning all items for the given parent id
        /// </summary>
        private void DataPortal_Fetch(FetchByWorkpackgeIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                // get the workpackage planogram dto list
                IEnumerable<WorkpackagePlanogramDto> planogramDtoList;
                using (IWorkpackagePlanogramDal dal = dalContext.GetDal<IWorkpackagePlanogramDal>())
                {
                    planogramDtoList = dal.FetchByWorkpackageId(criteria.WorkpackageId);
                    foreach (WorkpackagePlanogramDto planogramDto in planogramDtoList)
                    {
                        this.Add(WorkpackagePlanogram.GetWorkpackagePlanogram(dalContext, planogramDto, criteria.WorkflowId));
                    }
                }
            }
            this.MarkAsChild();
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when fetching a child instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, Int32 workpackageId, Int32 destinationPlanogramId, Int32 workflowId)
        {
            this.RaiseListChangedEvents = false;
            using (IWorkpackagePlanogramDal dal = dalContext.GetDal<IWorkpackagePlanogramDal>())
            {
                this.Add(WorkpackagePlanogram.GetWorkpackagePlanogram(
                    dalContext,
                    dal.FetchByWorkpackageIdDestinationPlanogramId(workpackageId, destinationPlanogramId),
                    workflowId));
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
