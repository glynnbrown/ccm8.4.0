﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8025632 : A.Kuszyk
//		Created (copied from SA).
// V8-25455 : J.Pickup
//		Added GetAllNodes helper method
// V8-27132 : A.Kuszyk
//  Implemented common interface.
// V8-27892 : J.Pickup
//      Introduced ProductGroupId Required buisness rule. 
#endregion
#region Version History: (CCM 8.0.3)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Model
{
    public enum ConsumerDecisionTreeType
    {
        Manual = 1,
        Automatic = 2
    }

    /// <summary>
    /// Header for the consumer decision tree structure
    /// (root object)
    /// </summary>
    [Serializable]
    public sealed partial class ConsumerDecisionTree : ModelObject<ConsumerDecisionTree>, IPlanogramConsumerDecisionTree
    {
        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
           RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The unique object id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The row version timestamp
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }

        /// <summary>
        /// The Entity Id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
        }

        /// <summary>
        /// Unique content reference property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Guid> UniqueContentReferenceProperty =
            RegisterModelProperty<Guid>(c => c.UniqueContentReference, Message.UniqueContentReference);
        /// <summary>
        /// Unique content reference
        /// </summary>
        public Guid UniqueContentReference
        {
            get { return GetProperty<Guid>(UniqueContentReferenceProperty); }
        }


        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The hierarchy name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32?> ProductGroupIdProperty =
            RegisterModelProperty<Int32?>(c => c.ProductGroupId);
        public Int32? ProductGroupId
        {
            get { return GetProperty<Int32?>(ProductGroupIdProperty); }
            set { SetProperty<Int32?>(ProductGroupIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<ConsumerDecisionTreeType> TypeProperty =
            RegisterModelProperty<ConsumerDecisionTreeType>(c => c.Type);
        /// <summary>
        /// The tree type
        /// </summary>
        public ConsumerDecisionTreeType Type
        {
            get { return GetProperty<ConsumerDecisionTreeType>(TypeProperty); }
            set { SetProperty<ConsumerDecisionTreeType>(TypeProperty, value); }
        }

        public static readonly ModelPropertyInfo<ConsumerDecisionTreeNode> RootNodeProperty =
            RegisterModelProperty<ConsumerDecisionTreeNode>(c => c.RootNode);
        /// <summary>
        /// The root node of the structure
        /// </summary>
        public ConsumerDecisionTreeNode RootNode
        {
            get { return GetProperty<ConsumerDecisionTreeNode>(RootNodeProperty); }
        }

        public static readonly ModelPropertyInfo<ConsumerDecisionTreeLevel> RootLevelProperty =
            RegisterModelProperty<ConsumerDecisionTreeLevel>(c => c.RootLevel);
        /// <summary>
        /// The root level of the hierarchy
        /// </summary>
        public ConsumerDecisionTreeLevel RootLevel
        {
            get { return GetProperty<ConsumerDecisionTreeLevel>(RootLevelProperty); }
        }

        /// <summary>
        /// Parent unique content reference property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Guid?> ParentUniqueContentReferenceProperty =
            RegisterModelProperty<Guid?>(c => c.ParentUniqueContentReference);
        /// <summary>
        /// Parent unique content reference
        /// </summary>
        public Guid? ParentUniqueContentReference
        {
            get { return GetProperty<Guid?>(ParentUniqueContentReferenceProperty); }
        }

        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
            BusinessRules.AddRule(new MinValue<Int32>(EntityIdProperty, 1));
            BusinessRules.AddRule(new Required(ProductGroupIdProperty));
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ConsumerDecisionTree), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ConsumerDecisionTreeCreate.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTree), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ConsumerDecisionTreeGet.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTree), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ConsumerDecisionTreeEdit.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTree), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ConsumerDecisionTreeDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object based on entity id
        /// </summary>
        /// <returns>A new object</returns>
        public static ConsumerDecisionTree NewConsumerDecisionTree(Int32 entityId)
        {
            ConsumerDecisionTree item = new ConsumerDecisionTree();
            item.Create(entityId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 entityId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.LoadProperty<Guid>(UniqueContentReferenceProperty, Guid.NewGuid());
            this.LoadProperty<ConsumerDecisionTreeType>(TypeProperty, ConsumerDecisionTreeType.Automatic);
            this.LoadProperty<ConsumerDecisionTreeLevel>(RootLevelProperty, ConsumerDecisionTreeLevel.NewConsumerDecisionTreeLevel());
            this.RootLevel.Name = Message.LocationHierarchy_RootLevel_DefaultName;
            this.LoadProperty<ConsumerDecisionTreeNode>(RootNodeProperty, ConsumerDecisionTreeNode.NewConsumerDecisionTreeNode(this.RootLevel.Id));
            this.RootNode.Name = Message.ConsumerDecisionTreeNode_RootNode_DefaultName;
            base.Create();
        }

        #endregion

        #endregion

        #region Overrides

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            this.LoadProperty<Guid>(UniqueContentReferenceProperty, Guid.NewGuid());
            context.RegisterId<ConsumerDecisionTree>(oldId, newId);
        }

        //protected override void MarkNew()
        //{
        //    //load a new ucr
        //    LoadProperty<Guid>(UniqueContentReferenceProperty, Guid.NewGuid());

        //    //Fetch all the levels
        //    List<ConsumerDecisionTreeLevel> levels = this.FetchAllLevels().ToList();
        //    if (levels.Count() > 0)
        //    {
        //        //Dictionary<Int32, Guid> resetCdtLevelIdDict = ConsumerDecisionTreeLevel.ResetTemporaryIds(levels);

        //        ////resolve ConsumerDecisionTreeNode
        //        //ConsumerDecisionTreeNode.ResetScenarioConsumerDecisionTreeLevelReferences(this.FetchAllNodes(), resetCdtLevelIdDict);
        //    }

        //    base.MarkNew();
        //}

        public override String ToString()
        {
            return this.Name;
        }

        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchById
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            public static readonly PropertyInfo<Int32> IdProperty =
            RegisterProperty<Int32>(c => c.Id);
            public Int32 Id
            {
                get { return ReadProperty<Int32>(IdProperty); }
            }

            public FetchByIdCriteria(Int32 id)
            {
                LoadProperty<Int32>(IdProperty, id);
            }
        }

        [Serializable]
        public class FetchByEntityIdNameCriteria : CriteriaBase<FetchByEntityIdNameCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
            RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static readonly PropertyInfo<String> NameProperty =
            RegisterProperty<String>(c => c.Name);
            public String Name
            {
                get { return ReadProperty<String>(NameProperty); }
            }

            public FetchByEntityIdNameCriteria(Int32 entityId, String name)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<String>(NameProperty, name);
            }
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Returns a list of all child nodes in the tree
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<ConsumerDecisionTreeNode> GetAllNodes()
        {
            return this.RootNode.FetchAllChildNodes().ToList().AsReadOnly();
        }

        /// <summary>
        /// Returns a list of levels in the structure
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<ConsumerDecisionTreeLevel> FetchAllLevels()
        {
            List<ConsumerDecisionTreeLevel> returnList = new List<ConsumerDecisionTreeLevel>();

            //move down through the levels adding each one
            ConsumerDecisionTreeLevel currentLevel = this.RootLevel;
            while (currentLevel != null)
            {
                returnList.Add(currentLevel);
                currentLevel = currentLevel.ChildLevel;
            }

            return returnList.AsReadOnly();
        }

        /// <summary>
        /// Returns a list of all groups in the structure 
        /// </summary>
        /// <returns></returns>
        public ReadOnlyCollection<ConsumerDecisionTreeNode> FetchAllNodes()
        {
            return this.RootNode.FetchAllChildNodes().ToList().AsReadOnly();
        }

        /// <summary>
        /// Inserts a new child under the parent and moves its children down
        /// </summary>
        /// <param name="parentLevel"></param>
        /// <returns>the new child</returns>
        public ConsumerDecisionTreeLevel InsertNewChildLevel(ConsumerDecisionTreeLevel parentLevel)
        {
            //create the new child
            ConsumerDecisionTreeLevel newChildLevel = ConsumerDecisionTreeLevel.NewConsumerDecisionTreeLevel();

            //[SA-15072] give it a unqiue name
            if (parentLevel.ParentConsumerDecisionTree != null)
            {
                List<ConsumerDecisionTreeLevel> existingLevels = parentLevel.ParentConsumerDecisionTree.FetchAllLevels().ToList();
                IEnumerable<String> takenNames = existingLevels.Select(l => l.Name);

                Int32 newChildLevelNo = existingLevels.IndexOf(parentLevel) + 2;
                String levelName = String.Empty;
                do
                {
                    /*newChildLevel.Name*/
                    levelName = String.Format(Message.CdtMaintenance_NewLevel, newChildLevelNo);
                    newChildLevelNo++;
                }
                while (takenNames.Contains(levelName));

                newChildLevel = ConsumerDecisionTreeLevel.NewConsumerDecisionTreeLevel(levelName);
            }

            //get the parents current child
            ConsumerDecisionTreeLevel currentParentChild = parentLevel.ChildLevel;

            if (currentParentChild != null)
            {
                //need to move the child to be a child of the new level instead
                newChildLevel.ChildLevel = currentParentChild;
                //remove from its old parent
                parentLevel.ChildLevel = null;

                //now set the new child and the child of the parent
                parentLevel.ChildLevel = newChildLevel;


                //need to insert an extra unit underneath all units on the parent level
                Object parentLevelId = parentLevel.Id;
                IEnumerable<ConsumerDecisionTreeNode> parentLevelUnits =
                    this.FetchAllNodes().Where(u => u.ConsumerDecisionTreeLevelId.Equals(parentLevelId));

                //cycle through all the parents
                Int32 addedGroupNumber = 1;
                foreach (ConsumerDecisionTreeNode unit in parentLevelUnits)
                {
                    //get a list of child units belonging to the parent
                    List<ConsumerDecisionTreeNode> parentChildUnits = unit.ChildList.ToList();

                    if (parentChildUnits.Count > 0)
                    {
                        //create a new child unit on the new level
                        ConsumerDecisionTreeNode newChildUnit = ConsumerDecisionTreeNode.NewConsumerDecisionTreeNode(newChildLevel.Id);
                        newChildUnit.Name = String.Format(Message.ConsumerDecisionTreeNode_Name_DefaultValue, newChildLevel.Name, addedGroupNumber);

                        //add the children to this new unit
                        newChildUnit.ChildList.AddList(parentChildUnits);

                        unit.ChildList.RaiseListChangedEvents = false;

                        //clear the parent child collection and add the new child unit
                        unit.ChildList.Clear();
                        unit.ChildList.Add(newChildUnit);

                        unit.ChildList.RaiseListChangedEvents = true;
                        unit.ChildList.Reset();
                    }

                    addedGroupNumber++;
                }
            }
            else
            {
                parentLevel.ChildLevel = newChildLevel;
            }

            return newChildLevel;
        }

        /// <summary>
        /// Removes the level from the structure and all its children
        /// </summary>
        /// <param name="levelToRemove"></param>
        public void RemoveLevel(ConsumerDecisionTreeLevel levelToRemove, Boolean moveProductToParent)
        {
            //need to find the bottom level
            ConsumerDecisionTreeLevel lowestLevel = levelToRemove;
            if (lowestLevel != null && !lowestLevel.IsRoot)
            {
                while (lowestLevel.ChildLevel != null)
                {
                    lowestLevel = lowestLevel.ChildLevel;
                }

                //go through each parent of the lowest level
                while (lowestLevel != levelToRemove.ParentLevel)
                {
                    //cycle through the parent units deleting all direct child units
                    //as they will be on the removed level
                    Object parentLevelRef = lowestLevel.ParentLevel.Id;
                    IEnumerable<ConsumerDecisionTreeNode> parentLevelUnits =
                        this.FetchAllNodes().Where(u => u.ConsumerDecisionTreeLevelId.Equals(parentLevelRef));

                    foreach (ConsumerDecisionTreeNode parentUnit in parentLevelUnits)
                    {
                        parentUnit.ChildList.RaiseListChangedEvents = false;

                        List<ConsumerDecisionTreeNode> childUnitsToRemove = parentUnit.ChildList.ToList();
                        foreach (ConsumerDecisionTreeNode childToRemove in childUnitsToRemove)
                        {
                            if (moveProductToParent)
                            {
                                //move any prodcuts from the actual unit being removed to the parent
                                List<ConsumerDecisionTreeNodeProduct> childAssignedProducts = childToRemove.Products.ToList();
                                parentUnit.Products.AddList(childAssignedProducts);
                                childToRemove.Products.RemoveList(childAssignedProducts);
                            }

                            //remove the child from its parent
                            parentUnit.ChildList.Remove(childToRemove);
                        }
                        parentUnit.ChildList.RaiseListChangedEvents = true;
                        parentUnit.ChildList.Reset();
                    }

                    if (lowestLevel.ChildLevel != null) 
                    {
                        lowestLevel.RemoveLevel(lowestLevel.ChildLevel); 
                    }
                    lowestLevel = lowestLevel.ParentLevel;
                }

                //set the actual level to null
                levelToRemove.ParentLevel.RemoveLevel(levelToRemove);
                //levelToRemove.ParentLevel.ChildLevel = null;
            }
        }

        /// <summary>
        /// Returns the parent level of the given node
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public ConsumerDecisionTreeLevel GetLinkedLevel(ConsumerDecisionTreeNode node)
        {
            return this.FetchAllLevels().FirstOrDefault(u => u.Id.Equals(node.ConsumerDecisionTreeLevelId));
        }

        ///// <summary>
        ///// Returns the number of levels (of nodes) in this structure.
        ///// </summary>
        //public Int32 GetLevelCount()
        //{
        //    return RootNode.GetLevelCount();
        //}

        /// <summary>
        /// Returns a list of all product ids assigned in the cdt
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Int32> GetAssignedProductIds()
        {
            return
                this.RootNode.FetchAllChildConsumerDecisionTreeNodeProducts().Select(p => p.ProductId)
                .ToList();
        }

        /// <summary>
        /// Adds the provided tree's node under this tree's root
        /// </summary>
        /// <param name="tree"></param>
        public void MergeTree(ConsumerDecisionTree tree)
        {
            //Get/create the level to add the tree to
            ConsumerDecisionTreeLevel cdtMergeRootLevel = this.RootLevel.ChildLevel;
            if (cdtMergeRootLevel == null)
            {
                cdtMergeRootLevel = this.InsertNewChildLevel(this.RootLevel);
                cdtMergeRootLevel.Name = Message.CdtMaintenance_CDTMergeLevel;
            }

            //Create the levels
            Dictionary<Int32, Int32> levelMatchedIds = cdtMergeRootLevel.MergeFrom(tree.RootLevel);

            //Create the nodes
            this.RootNode.MergeFrom(tree.RootNode);

            //Resolve the ids
            ConsumerDecisionTreeNode cdtMergeRootNode = this.RootNode.ChildList.LastOrDefault();
            if (cdtMergeRootNode != null)
            {
                List<ConsumerDecisionTreeNode> nodesToResolve = cdtMergeRootNode.FetchAllChildNodes().ToList();
                foreach (ConsumerDecisionTreeNode node in nodesToResolve)
                {
                    Int32 levelId = 0;
                    levelMatchedIds.TryGetValue(node.ConsumerDecisionTreeLevelId, out levelId);

                    if (levelId != 0)
                    {
                        node.ConsumerDecisionTreeLevelId = levelId;
                    }
                }
            }
        }

        #endregion

        #region IPlanogramConsumerDecisionTree members
        IPlanogramConsumerDecisionTreeLevel IPlanogramConsumerDecisionTree.RootLevel
        {
            get { return RootLevel; }
        }

        IPlanogramConsumerDecisionTreeNode IPlanogramConsumerDecisionTree.RootNode
        {
            get { return RootNode; }
        } 
        #endregion
    }

    public static class ConsumerDecisionTreeTypeHelper
    {
        public static readonly Dictionary<ConsumerDecisionTreeType, String> FriendlyNames =
            new Dictionary<ConsumerDecisionTreeType, String>()
            {
                {ConsumerDecisionTreeType.Automatic, ConsumerDecisionTreeType.Automatic.ToString()},
                {ConsumerDecisionTreeType.Manual, ConsumerDecisionTreeType.Manual.ToString()}
            };

        public static ConsumerDecisionTreeType? ConsumerDecisionTreeTypeGetEnum(String description)
        {
            foreach (KeyValuePair<ConsumerDecisionTreeType, String> keyPair in ConsumerDecisionTreeTypeHelper.FriendlyNames)
            {
                if (keyPair.Value.Equals(description))
                {
                    return keyPair.Key;
                }
            }
            return null;
        }
    }
}
