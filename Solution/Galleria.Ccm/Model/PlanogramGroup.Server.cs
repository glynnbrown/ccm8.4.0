﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)

// CCM-25587 : L.Ineson
//	Created (Auto-generated)
// V8-27964 : A.Silva
//      Added ProductGroupId to DataTransferObject methods.
// V8-27966 : A.Silva
//      Added InsertPlanogramGroup and UpdatePlanogramGroup Commands.
// V8-28334 : A.Silva
//      Added DeletePlanogram Group Command.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramGroup
    {
        #region Constructor
        private PlanogramGroup() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static PlanogramGroup Fetch(IDalContext dalContext, PlanogramGroupDto dto, IEnumerable<PlanogramGroupDto> groupDtoList)
        {
            return DataPortal.FetchChild<PlanogramGroup>(dalContext, dto, groupDtoList);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramGroupDto dto, IEnumerable<PlanogramGroupDto> groupDtoList)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);
            this.LoadProperty<Guid>(CodeProperty, dto.Code);

            this.LoadProperty<PlanogramGroupList>(ChildListProperty,
               PlanogramGroupList.FetchByParentPlanogramGroupId(dalContext, dto.Id, groupDtoList));

            this.LoadProperty<Int32?>(ProductGroupIdProperty, dto.ProductGroupId);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private PlanogramGroupDto GetDataTransferObject()
        {
            return new PlanogramGroupDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                PlanogramHierarchyId = this.ParentHierarchy.Id,
                Name = ReadProperty<String>(NameProperty),
                ParentGroupId = (this.ParentGroup != null)? (Int32?)this.ParentGroup.Id : null,
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty),
                ProductGroupId = ReadProperty<Int32?>(ProductGroupIdProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, PlanogramGroupDto dto, IEnumerable<PlanogramGroupDto> groupDtoList)
        {
            this.LoadDataTransferObject(dalContext, dto, groupDtoList);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext)
        {
            PlanogramGroupDto dto = this.GetDataTransferObject();
            Int32 oldId = dto.Id;
            using (IPlanogramGroupDal dal = dalContext.GetDal<IPlanogramGroupDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            dalContext.RegisterId<PlanogramGroup>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            if (this.IsSelfDirty)
            {
                PlanogramGroupDto dto = this.GetDataTransferObject();
                using (IPlanogramGroupDal dal = dalContext.GetDal<IPlanogramGroupDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IPlanogramGroupDal dal = dalContext.GetDal<IPlanogramGroupDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion

        #region Commands

        #region AssociatePlanograms
        /// <summary>
        /// Associates a set of planograms with a product group
        /// </summary>
        private partial class AssociatePlanogramsCommand : CommandBase<AssociatePlanogramsCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                // build the list of dtos to insert
                List<PlanogramGroupPlanogramDto> dtoList = new List<PlanogramGroupPlanogramDto>();
                foreach (Int32 planogramId in this.PlanogramIds)
                {
                    dtoList.Add(new PlanogramGroupPlanogramDto()
                    {
                        PlanogramGroupId = this.PlanogramGroupId,
                        PlanogramId = planogramId
                    });
                }

                // upsert the dtos
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IPlanogramGroupPlanogramDal dal = dalContext.GetDal<IPlanogramGroupPlanogramDal>())
                    {
                        dal.Upsert(dtoList);
                    }
                }
            }
            #endregion

            #endregion
        }
        #endregion

        #region InsertPlanogramGroup
        /// <summary>
        ///     Directly inserts a new <see cref="PlanogramGroup"/> into the data repository.
        /// </summary>
        private partial class InsertPlanogramGroupCommand : CommandBase<InsertPlanogramGroupCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                // Create the dto.
                PlanogramGroupDto dto = new PlanogramGroupDto
                {
                    PlanogramHierarchyId = this.PlanogramHierarchyId,
                    ParentGroupId = this.ParentPlanogramGroupId,
                    Name = this.Name,
                    ProductGroupId = this.ProductGroupId,
                };

                // Insert the dto.
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IPlanogramGroupDal dal = dalContext.GetDal<IPlanogramGroupDal>())
                    {
                        dal.Insert(dto);
                    }
                    this.LoadProperty<Int32>(IdProperty, dto.Id);
                }
            }
            #endregion

            #endregion
        }
        #endregion

        #region UpdatePlanogramGroup
        /// <summary>
        ///     Directly updates a new <see cref="PlanogramGroup"/> in the data repository.
        /// </summary>
        private partial class UpdatePlanogramGroupCommand : CommandBase<UpdatePlanogramGroupCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                // Create the dto.
                PlanogramGroupDto dto = new PlanogramGroupDto
                {
                    Id = this.PlanogramGroupId,
                    PlanogramHierarchyId = this.PlanogramHierarchyId,
                    ParentGroupId = this.ParentPlanogramGroupId,
                    Name = this.Name,
                    ProductGroupId = this.ProductGroupId
                };

                // Update the dto.
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IPlanogramGroupDal dal = dalContext.GetDal<IPlanogramGroupDal>())
                    {
                        dal.Update(dto);
                    }
                    //this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                }
            }
            #endregion

            #endregion
        }
        #endregion

        #region DeletePlanogramGroup
        /// <summary>
        ///     Directly deletes a new <see cref="PlanogramGroup"/> in the data repository.
        /// </summary>
        private partial class DeletePlanogramGroupCommand : CommandBase<DeletePlanogramGroupCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IPlanogramGroupDal dal = dalContext.GetDal<IPlanogramGroupDal>())
                    {
                        dal.DeleteById(this.PlanogramGroupId);
                    }
                }
            }
            #endregion

            #endregion
        }
        #endregion

        #endregion
    }
}