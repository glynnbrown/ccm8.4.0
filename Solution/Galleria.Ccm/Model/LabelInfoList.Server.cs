﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24265 N.Haywood
//  Created
// V8-27940 : L.Luong
//  Added FetchByEntityId and FetchByEntityIdIncludingDeleted
#endregion
#region Version History CCM830
// V8-31699 : A.Heathcote
//  added to the "public static LabelInfoList" method
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.IO;
using Galleria.Framework.Dal.Configuration;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class LabelInfoList
    {
        #region Constants
        private const String _dalAssemblyName = "Galleria.Ccm.Dal.{0}.dll";
        #endregion

        #region Constructors
        private LabelInfoList() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        ///// <summary>
        ///// Fetches a list of infos for the givne fixture packages
        ///// </summary>
        ///// <param name="searchCriteria"></param>
        ///// <returns></returns>
        //public static LabelInfoList FetchByFileNames(IEnumerable<String> fileNames)
        //{
        //    return DataPortal.Fetch<LabelInfoList>(new FetchByPlanogramIdsCriteria(LabelDalFactoryType.FileSystem, fileNames.Cast<Object>().ToList()));
        //}

        /// <summary>
        /// Returns a list of all items from ids
        /// </summary>
        /// <param name="filenames"></param>
        /// <returns></returns>
        public static LabelInfoList FetchByIds(List<Object> ids, LabelDalFactoryType dalFactoryType = LabelDalFactoryType.Unknown)
        {
            return DataPortal.Fetch<LabelInfoList>(
                new FetchByIdsCriteria(dalFactoryType, ids));
        }

        /// <summary>
        /// returns all the labels from entityId
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static LabelInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<LabelInfoList>(new FetchByEntityIdCriteria(entityId));
        }

        public static LabelInfoList FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            return DataPortal.Fetch<LabelInfoList>(new FetchByEntityIdIncludingDeletedCriteria(entityId));
        }

        #endregion

        #region Data Access

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdsCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(criteria.DalType, out dalFactoryName);

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILabelInfoDal dal = dalContext.GetDal<ILabelInfoDal>())
                {
                    foreach (LabelInfoDto labelDto in dal.FetchByIds(criteria.Ids))
                    {
                        if (criteria.DalType == LabelDalFactoryType.FileSystem)
                        {
                            this.Add(LabelInfo.GetLabelInfo(dalContext, labelDto, (String)labelDto.Id));
                        }
                        else
                        {
                            this.Add(LabelInfo.GetLabelInfo(dalContext, labelDto));
                        }
                    }
                }
            }
            
            this.RaiseListChangedEvents = true;
            this.IsReadOnly = true;
        }

        /// <summary>
        ///     Called when retrieving a list of all labels for an entity.
        /// </summary>
        /// <param name="criteria">The criteria to select valid items with.</param>
        /// <remarks>This procedure should NOT return <c>deleted</c> items.</remarks>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IsReadOnly = false;
            var dalFactory = DalContainer.GetDalFactory();
            using (var dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<ILabelInfoDal>())
            {
                var dtoList = dal.FetchByEntityId(criteria.EntityId);
                foreach (var dto in dtoList)
                {
                    Add(LabelInfo.GetLabelInfo(dalContext, dto));
                }
            }
            IsReadOnly = true;
            RaiseListChangedEvents = true;
        }

        /// <summary>
        ///     Called when retrieving a list of all Validation templates for an entity.
        /// </summary>
        /// <param name="criteria">The criteria to select valid items with.</param>
        private void DataPortal_Fetch(FetchByEntityIdIncludingDeletedCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IsReadOnly = false;
            var dalFactory = DalContainer.GetDalFactory();
            using (var dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<ILabelInfoDal>())
            {
                var dtoList = dal.FetchByEntityIdIncludingDeleted(criteria.EntityId);
                foreach (var dto in dtoList)
                {
                    Add(LabelInfo.GetLabelInfo(dalContext, dto));
                }
            }
            IsReadOnly = true;
            RaiseListChangedEvents = true;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByTypeCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(criteria.DalType, out dalFactoryName);

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILabelInfoDal dal = dalContext.GetDal<ILabelInfoDal>())
                {
                    foreach (LabelInfoDto labelDto in dal.FetchByIds(criteria.Ids))
                    {
                        if (criteria.DalType == LabelDalFactoryType.FileSystem &&
                            criteria.Type == (LabelType)labelDto.Type)
                        {

                            this.Add(LabelInfo.GetLabelInfo(dalContext, labelDto, (String)labelDto.Id));
                        }
                        if (criteria.DalType == LabelDalFactoryType.Unknown &&
                            criteria.Type == (LabelType)labelDto.Type)
                        {
                            this.Add(LabelInfo.GetLabelInfo(dalContext, labelDto));
                        }
                    }
                }
            }
            
            this.RaiseListChangedEvents = true;
            this.IsReadOnly = true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the correct dal factory based on the given type and args.
        /// </summary>
        private static IDalFactory GetDalFactory(LabelDalFactoryType dalType, out String dalFactoryName)
        {
            switch (dalType)
            {
                case LabelDalFactoryType.FileSystem:
                    dalFactoryName = Constants.UserDal;
                    return DalContainer.GetDalFactory(dalFactoryName);

                default:
                    dalFactoryName = DalContainer.DalName;
                    return DalContainer.GetDalFactory();
            }

        }

        #endregion
    }
}
