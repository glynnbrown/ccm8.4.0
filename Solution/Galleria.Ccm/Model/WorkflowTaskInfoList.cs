﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25919 : L.Ineson
//  Created
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;


namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class WorkflowTaskInfoList : ModelReadOnlyList<WorkflowTaskInfoList, WorkflowTaskInfo>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(WorkflowTaskInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Criteria

        #region FetchByWorkflowId
        [Serializable]
        public class FetchByWorkflowIdCriteria : CriteriaBase<FetchByWorkflowIdCriteria>
        {
            #region Properties

            #region EntityId
            /// <summary>
            /// WorkflowId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> WorkflowIdProperty =
                RegisterProperty<Int32>(c => c.WorkflowId);
            /// <summary>
            /// Returns the workflow id
            /// </summary>
            public Int32 WorkflowId
            {
                get { return this.ReadProperty<Int32>(WorkflowIdProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByWorkflowIdCriteria(Int32 workflowId)
            {
                this.LoadProperty<Int32>(WorkflowIdProperty, workflowId);
            }
            #endregion

        }
        #endregion

        #region FetchByWorkpackageId
        [Serializable]
        public class FetchByWorkpackageIdCriteria : CriteriaBase<FetchByWorkpackageIdCriteria>
        {
            #region Properties

            #region EntityId
            /// <summary>
            /// WorkpackageId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> WorkpackageIdProperty =
                RegisterProperty<Int32>(c => c.WorkpackageId);
            /// <summary>
            /// Returns the workflow id
            /// </summary>
            public Int32 WorkpackageId
            {
                get { return this.ReadProperty<Int32>(WorkpackageIdProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByWorkpackageIdCriteria(Int32 workpackageId)
            {
                this.LoadProperty<Int32>(WorkpackageIdProperty, workpackageId);
            }
            #endregion

        }
        #endregion

        #endregion

    }
}
