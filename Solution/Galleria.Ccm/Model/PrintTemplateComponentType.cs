﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;
using System.IO;


namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Enum denoting the different component types.
    /// </summary>
    public enum PrintTemplateComponentType
    {
        Unknown = 0,
        TextBox = 1,
        Planogram = 4,
        DataSheet = 5,
        Line = 7,
    }
}
