﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM833
// CCM-19130 : J.Mendes
//  Added a new paramenter to the UpdatePlanogramLocationSpace which allows the user to ignore Location Space Element Attributes 
//    If the user does not ignore any attributtes the shelf will be updated with the Location Space Element attributtes
//    If the user ignores all attributes(with exception of the width attribute), the values of each attribute will remain the same as the original shelf.
//    If the user ignores the width, the shelf width will be same as the bay width
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    public enum LocationSpaceElementFillPatternType
    {
        Solid = 0,
        Border = 1,
        DiagonalUp = 2,
        DiagonalDown = 3,
        Crosshatch = 4,
        Horizontal = 5,
        Vertical = 6,
        Dotted = 7
    }

    /// <summary>
    /// ProductFillPatternType Helper Class
    /// </summary>
    public static class LocationSpaceElementFillPatternTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<LocationSpaceElementFillPatternType, String> FriendlyNames =
            new Dictionary<LocationSpaceElementFillPatternType, String>()
            {
                {LocationSpaceElementFillPatternType.Solid, Message.Enum_LocationSpaceElementFillPatternType_Solid},
                {LocationSpaceElementFillPatternType.Border, Message.Enum_LocationSpaceElementFillPatternType_Border},
                {LocationSpaceElementFillPatternType.DiagonalUp, Message.Enum_LocationSpaceElementFillPatternType_DiagonalUp},
                {LocationSpaceElementFillPatternType.DiagonalDown, Message.Enum_LocationSpaceElementFillPatternType_DiagonalDown},
                {LocationSpaceElementFillPatternType.Crosshatch, Message.Enum_LocationSpaceElementFillPatternType_Crosshatch},
                {LocationSpaceElementFillPatternType.Horizontal, Message.Enum_LocationSpaceElementFillPatternType_Horizontal},
                {LocationSpaceElementFillPatternType.Vertical, Message.Enum_LocationSpaceElementFillPatternType_Vertical},
                {LocationSpaceElementFillPatternType.Dotted, Message.Enum_LocationSpaceElementFillPatternType_Dotted}
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly description as value.
        /// </summary>
        public static readonly Dictionary<LocationSpaceElementFillPatternType, String> FriendlyDescriptions =
            new Dictionary<LocationSpaceElementFillPatternType, String>()
            {
                {LocationSpaceElementFillPatternType.Solid, Message.Enum_LocationSpaceElementFillPatternType_Solid},
                {LocationSpaceElementFillPatternType.Border, Message.Enum_LocationSpaceElementFillPatternType_Border},
                {LocationSpaceElementFillPatternType.DiagonalUp, Message.Enum_LocationSpaceElementFillPatternType_DiagonalUp},
                {LocationSpaceElementFillPatternType.DiagonalDown, Message.Enum_LocationSpaceElementFillPatternType_DiagonalDown},
                {LocationSpaceElementFillPatternType.Crosshatch, Message.Enum_LocationSpaceElementFillPatternType_Crosshatch},
                {LocationSpaceElementFillPatternType.Horizontal, Message.Enum_LocationSpaceElementFillPatternType_Horizontal},
                {LocationSpaceElementFillPatternType.Vertical, Message.Enum_LocationSpaceElementFillPatternType_Vertical},
                {LocationSpaceElementFillPatternType.Dotted, Message.Enum_LocationSpaceElementFillPatternType_Dotted}
            };

        /// <summary>
        /// Dictionary with friendly name in all lower case as key and enum value as value.
        /// </summary>
        public static readonly Dictionary<String, LocationSpaceElementFillPatternType> EnumFromFriendlyName =
            new Dictionary<String, LocationSpaceElementFillPatternType>()
            {
                {Message.Enum_LocationSpaceElementFillPatternType_Solid.ToLowerInvariant(), LocationSpaceElementFillPatternType.Solid},
                {Message.Enum_LocationSpaceElementFillPatternType_Border.ToLowerInvariant(), LocationSpaceElementFillPatternType.Border},
                {Message.Enum_LocationSpaceElementFillPatternType_DiagonalUp.ToLowerInvariant(), LocationSpaceElementFillPatternType.DiagonalUp},
                {Message.Enum_LocationSpaceElementFillPatternType_DiagonalDown.ToLowerInvariant(), LocationSpaceElementFillPatternType.DiagonalDown},
                {Message.Enum_LocationSpaceElementFillPatternType_Crosshatch.ToLowerInvariant(),LocationSpaceElementFillPatternType.Crosshatch},
                {Message.Enum_LocationSpaceElementFillPatternType_Horizontal.ToLowerInvariant(), LocationSpaceElementFillPatternType.Horizontal},
                {Message.Enum_LocationSpaceElementFillPatternType_Vertical.ToLowerInvariant(), LocationSpaceElementFillPatternType.Vertical},
                {Message.Enum_LocationSpaceElementFillPatternType_Dotted.ToLowerInvariant(), LocationSpaceElementFillPatternType.Dotted}
            };

        /// <summary>
        /// Returns the matching enum value from the specifed friendly name
        /// Returns null if no match found.
        /// </summary>
        public static LocationSpaceElementFillPatternType? LocationSpaceElementFillPatternTypeGetEnum(String friendlyName)
        {
            LocationSpaceElementFillPatternType? returnValue = null;
            LocationSpaceElementFillPatternType enumValue;
            if (EnumFromFriendlyName.TryGetValue(friendlyName, out enumValue))
            {
                returnValue = enumValue;
            }
            return returnValue;
        }
    }
}