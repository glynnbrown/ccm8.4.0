﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
#endregion

#endregion

using System;
using Galleria.Framework.Model;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     This class holds represents the values used to set a particular Column Layout. Child of <see cref="ColumnLayoutSettingList"/>.
    /// </summary>
    [Serializable]
    public sealed partial class ColumnLayoutSetting : ModelObject<ColumnLayoutSetting>
    {
        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Properties

        #region Id

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}"/> definition for the <see cref="Id"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty = RegisterModelProperty<Int32>(c => c.Id);

        /// <summary>
        ///     Unique identifier for the <see cref="ColumnLayoutSetting"/>.
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty(IdProperty); }
            set { SetProperty(IdProperty, value); }
        }

        #endregion

        #region ScreenKey

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}"/> definition for the <see cref="ScreenKey"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> ScreenKeyProperty = RegisterModelProperty<String>(c => c.ScreenKey);

        /// <summary>
        ///     Unique identifier for the screen on which to apply the <see cref="CustomColumnLayout"/>.
        /// </summary>
        public String ScreenKey
        {
            get { return GetProperty(ScreenKeyProperty); }
            set { SetProperty(ScreenKeyProperty, value); }
        }

        #endregion

        #region ColumnLayoutName

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}"/> definition for the <see cref="ColumnLayoutName"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> ColumnLayoutNameProperty = RegisterModelProperty<String>(c => c.ColumnLayoutName);

        /// <summary>
        ///     Unique identifier for the <see cref="ColumnLayoutSetting"/>.
        /// </summary>
        public String ColumnLayoutName
        {
            get { return GetProperty(ColumnLayoutNameProperty); }
            set { SetProperty(ColumnLayoutNameProperty, value); }
        }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new metric
        /// </summary>
        /// <returns>Returns the new metric</returns>
        public static ColumnLayoutSetting NewColumnLayoutSetting()
        {
            var connection = new ColumnLayoutSetting();
            connection.Create();
            return connection;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        ///     Overrides the base Create method used when creating a new instance of this type.
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty(ScreenKeyProperty, String.Empty);
            this.LoadProperty(ColumnLayoutNameProperty, String.Empty);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion
    }
}
