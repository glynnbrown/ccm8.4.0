﻿#region Header Information

#region Version History:(CCM800)
//CCM-26124 : I.George
// Initial Version
#endregion
#region Version History:(CCM820)
//CCM-30836 : J.Pickup
// Introduced IPtype.
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines an inventoryProfile info within the solution
    /// </summary>
    public sealed partial class InventoryProfileInfo
    {
        #region Constructor
        private InventoryProfileInfo() { } //force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// returns an inventoryProfile from a Dto
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">The dto to load</param>
        /// <returns>An entityInfo Object</returns>
        internal static InventoryProfileInfo FetchInventoryProfileInfo(IDalContext dalContext, InventoryProfileInfoDto dto)
        {
            return DataPortal.FetchChild<InventoryProfileInfo>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, InventoryProfileInfoDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<PlanogramInventoryMetricType>(InventoryProfileTypeProperty, (PlanogramInventoryMetricType)dto.InventoryProfileType);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto"></param>
        private void Child_Fetch(IDalContext dalContext, InventoryProfileInfoDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion 

        #endregion
    }
}
