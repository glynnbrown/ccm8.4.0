﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26179  : I.George
//  Created
// V8-28149 : M.Pettit
//  Ensured all types have friendly names for reporting
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Denotes the available volume unit of measure types
    /// </summary>
    public enum UnitOfMeasureVolumeType
    {
        Unknown = 0,
        Litres = 1,
        CubicFeet = 2
    }
    /// <summary>
    /// UnitOfMeasureVolumeType Helper Class
    /// </summary>
    public static class UnitOfMeasureVolumeTypeHelper
    {
        public static readonly Dictionary<UnitOfMeasureVolumeType, String> FriendlyNames =
            new Dictionary<UnitOfMeasureVolumeType, String>()
            {
                {UnitOfMeasureVolumeType.Unknown, String.Empty},
                {UnitOfMeasureVolumeType.Litres, Message.Enum_UnitOfMeasureVolumeType__Liters},
                {UnitOfMeasureVolumeType.CubicFeet, Message.Enum_UnitOfMeasureVolumeType_CubicFeet},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<UnitOfMeasureVolumeType, String> Abbreviations =
            new Dictionary<UnitOfMeasureVolumeType, String>()
            {
                {UnitOfMeasureVolumeType.Unknown, String.Empty},
                {UnitOfMeasureVolumeType.Litres, Message.Enum_UnitOfMeasureVolumeType_Liters_Abbrev},
                {UnitOfMeasureVolumeType.CubicFeet, Message.Enum_UnitOfMeasureVolumeType_CubicFeet_Abbrev},
            };
    }
}
