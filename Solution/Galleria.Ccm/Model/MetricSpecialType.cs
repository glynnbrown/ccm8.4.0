﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: CCM800
//// V8-26179  : I.George
////  Created
//#endregion

//#region Version History: CCM820
//// V8-30752   : J.Pickup
////  PromotionalSalesUnits added
//#endregion
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Galleria.Ccm.Resources.Language;

//namespace Galleria.Ccm.Model
//{
//    public enum MetricSpecialType
//    {
//       None = 0,
//       RegularSalesUnits = 1,
//       ReplenishmentDays = 2,
//       PromotionalSalesUnits = 3
//    }

//    public static class MetricSpecialTypeHelper
//    {
//        public static readonly Dictionary<MetricSpecialType, String> FriendlyNames =
//            new Dictionary<MetricSpecialType, String>()
//            {
//                {MetricSpecialType.None, Message.Enum_MetricSpecialType_None},
//                {MetricSpecialType.RegularSalesUnits, Message.Enum_MetricSpecialTypes_RegularSalesUnits},
//                {MetricSpecialType.ReplenishmentDays, Message.Enum_MetricSpecialTypes_ReplenishmentDays},
//                {MetricSpecialType.PromotionalSalesUnits, Message.Enum_MetricSpecialTypes_PromotionalSalesUnits},
//            };
//    }
//}
