﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class FixtureAssemblyItem
    {
        #region Constructor
        private FixtureAssemblyItem() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static FixtureAssemblyItem Fetch(IDalContext dalContext, FixtureAssemblyItemDto dto)
        {
            return DataPortal.FetchChild<FixtureAssemblyItem>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, FixtureAssemblyItemDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32>(FixtureAssemblyIdProperty, dto.FixtureAssemblyId);
            this.LoadProperty<Single>(XProperty, dto.X);
            this.LoadProperty<Single>(YProperty, dto.Y);
            this.LoadProperty<Single>(ZProperty, dto.Z);
            this.LoadProperty<Single>(SlopeProperty, dto.Slope);
            this.LoadProperty<Single>(AngleProperty, dto.Angle);
            this.LoadProperty<Single>(RollProperty, dto.Roll);

        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private FixtureAssemblyItemDto GetDataTransferObject(Fixture parent)
        {
            return new FixtureAssemblyItemDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                FixtureId = parent.Id,
                FixtureAssemblyId = ReadProperty<Int32>(FixtureAssemblyIdProperty),
                X = ReadProperty<Single>(XProperty),
                Y = ReadProperty<Single>(YProperty),
                Z = ReadProperty<Single>(ZProperty),
                Slope = ReadProperty<Single>(SlopeProperty),
                Angle = ReadProperty<Single>(AngleProperty),
                Roll = ReadProperty<Single>(RollProperty),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FixtureAssemblyItemDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, Fixture parent)
        {
            this.ResolveIds(dalContext);
            FixtureAssemblyItemDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IFixtureAssemblyItemDal dal = dalContext.GetDal<IFixtureAssemblyItemDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<FixtureAssemblyItem>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, Fixture parent)
        {
            this.ResolveIds(dalContext);
            if (this.IsSelfDirty)
            {
                using (IFixtureAssemblyItemDal dal = dalContext.GetDal<IFixtureAssemblyItemDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, Fixture parent)
        {
            using (IFixtureAssemblyItemDal dal = dalContext.GetDal<IFixtureAssemblyItemDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion

    }
}