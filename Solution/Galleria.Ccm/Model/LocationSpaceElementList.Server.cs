﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Csla;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class LocationSpaceElementList
    {
        #region Constructors
        public LocationSpaceElementList() { } // force use of factory methods
        #endregion

        #region Methods
        /// <summary>
        /// Called when adding a new object
        /// through data binding
        /// </summary>
        /// <returns>A new location space element object</returns>
        protected override LocationSpaceElement AddNewCore()
        {
            LocationSpaceElement locationSpaceElement = LocationSpaceElement.NewLocationSpaceElement();
            Add(locationSpaceElement);
            return locationSpaceElement;
        }
        #endregion

        #region Factory Methods

        internal static LocationSpaceElementList FetchByLocationSpaceBayId(IDalContext dalContext, Int32 locationSpaceBayId)
        {
            return DataPortal.FetchChild<LocationSpaceElementList>(dalContext, new FetchByLocationSpaceBayIdCriteria(locationSpaceBayId));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="productUniverseId">The parent product universe id id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FetchByLocationSpaceBayIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            using (ILocationSpaceElementDal dal = dalContext.GetDal<ILocationSpaceElementDal>())
            {
                IEnumerable<LocationSpaceElementDto> dtoList = dal.FetchByLocationSpaceBayId(criteria.LocationSpaceBayId);
                foreach (LocationSpaceElementDto dto in dtoList)
                {
                    this.Add(LocationSpaceElement.GetLocationSpaceElement(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returning all location space Elements for an entity
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByLocationSpaceBayIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationSpaceElementDal dal = dalContext.GetDal<ILocationSpaceElementDal>())
                {
                    IEnumerable<LocationSpaceElementDto> dtoList = dal.FetchByLocationSpaceBayId(criteria.LocationSpaceBayId);
                    foreach (LocationSpaceElementDto dto in dtoList)
                    {
                        this.Add(LocationSpaceElement.GetLocationSpaceElement(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #region Update
        /// <summary>
        /// Called when this list is being updated
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                Child_Update(dalContext);
                dalContext.Commit();
            }
        }
        #endregion

        #endregion

    }
}
