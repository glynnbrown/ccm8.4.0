﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class PlanogramComparisonTemplateInfo : ModelReadOnlyObject<PlanogramComparisonTemplateInfo>
    {
        #region Properties

        #region Id

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}"/> for the <see cref="Id"/> property.
        /// </summary>
        public static readonly ModelPropertyInfo<Object> IdProperty = RegisterModelProperty<Object>(c => c.Id);

        /// <summary>
        ///     Unique Id for this instance.
        /// </summary>
        public Object Id { get { return GetProperty(IdProperty); } }

        #endregion

        #region FileName

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}"/> for the <see cref="FileName"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> FileNameProperty = RegisterModelProperty<String>(c => c.FileName);

        /// <summary>
        ///     Get the name of the file represented by this info if there is one.
        /// </summary>
        public String FileName { get { return GetProperty(FileNameProperty); } }

        #endregion

        #region Name

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}"/> for the <see cref="Name"/> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty = RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        ///     Get the name assigned to this instance of <see cref="PlanogramComparisonTemplateInfo"/>.
        /// </summary>
        public String Name { get { return GetProperty(NameProperty); } }

        #endregion

        #endregion

        #region Authorization Rules

        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository

        #endregion

        #region Methods

        public override String ToString()
        {
            return Name;
        }

        #endregion
    }
}