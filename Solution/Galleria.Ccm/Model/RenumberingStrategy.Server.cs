﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva
//      Created.
// V8-27562 : A.Silva
//      Added FetchByEntityIdName

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Server-side implementation of <see cref="RenumberingStrategy"/>.
    /// </summary>
    public partial class RenumberingStrategy
    {
        #region Constructor

        /// <summary>
        ///     Private constructor; use factory methods to create new instances of <see cref="RenumberingStrategy"/>.
        /// </summary>
        private RenumberingStrategy()
        {
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Fetches the <see cref="RenumberingStrategy"/> instance matching the provided <paramref name="id"/>.
        /// </summary>
        /// <param name="id">Unique identifier for the <see cref="RenumberingStrategy"/> to be fetched.</param>
        /// <returns>A new instance with the data belonging to the provided <paramref name="id"/>.</returns>
        public static RenumberingStrategy FetchById(Int32 id)
        {
            return DataPortal.Fetch<RenumberingStrategy>(new FetchByIdCriteria(id));
        }

        /// <summary>
        ///     Fetches the <see cref="RenumberingStrategy"/> instance matching the provided <paramref name="entityId"/> and <paramref name="name"/>.
        /// </summary>
        /// <param name="entityId">Unique identifier for the <see cref="RenumberingStrategy"/> to be fetched.</param>
        /// <param name="name">THe name of the renumbering strategy to be fetched.</param>
        /// <returns>A new instance with the data belonging to the provided <paramref name="entityId"/> and matching the given <paramref name="name"/>.</returns>
        public static RenumberingStrategy FetchByEntityIdName(Int32 entityId, String name)
        {
            return DataPortal.Fetch<RenumberingStrategy>(new FetchByEntityIdNameCriteria(entityId, name));
        }

        /// <summary>
        ///     Gets the <see cref="RenumberingStrategy"/> instance from the data contained in the provided <paramref name="dto"/>.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">Instance of <see cref="RenumberingStrategyDto"/> containing the values to be loaded.</param>
        /// <returns>A new child instance created from the provided <paramref name="dto"/>.</returns>
        internal static RenumberingStrategy GetRenumberingStrategy(IDalContext dalContext, RenumberingStrategyDto dto)
        {
            return DataPortal.FetchChild<RenumberingStrategy>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Creates a <see cref="RenumberingStrategyDto"/> with the values in this instance.
        /// </summary>
        /// <returns>New instance of <see cref="RenumberingStrategyDto"/> with the values in this instance.</returns>
        private RenumberingStrategyDto GetDataTransferObject()
        {
            return new RenumberingStrategyDto
            {
                Id = ReadProperty(IdProperty),
                RowVersion = ReadProperty(RowVersionProperty),
                EntityId = ReadProperty(EntityIdProperty),
                Name = ReadProperty(NameProperty),
                BayComponentXRenumberStrategyPriority = ReadProperty(BayComponentXRenumberStrategyPriorityProperty),
                BayComponentYRenumberStrategyPriority = ReadProperty(BayComponentYRenumberStrategyPriorityProperty),
                BayComponentZRenumberStrategyPriority = ReadProperty(BayComponentZRenumberStrategyPriorityProperty),
                PositionXRenumberStrategyPriority = ReadProperty(PositionXRenumberStrategyPriorityProperty),
                PositionYRenumberStrategyPriority = ReadProperty(PositionYRenumberStrategyPriorityProperty),
                PositionZRenumberStrategyPriority = ReadProperty(PositionZRenumberStrategyPriorityProperty),
                BayComponentXRenumberStrategy = (Byte) ReadProperty(BayComponentXRenumberStrategyProperty),
                BayComponentYRenumberStrategy = (Byte) ReadProperty(BayComponentYRenumberStrategyProperty),
                BayComponentZRenumberStrategy = (Byte) ReadProperty(BayComponentZRenumberStrategyProperty),
                PositionXRenumberStrategy = (Byte) ReadProperty(PositionXRenumberStrategyProperty),
                PositionYRenumberStrategy = (Byte) ReadProperty(PositionYRenumberStrategyProperty),
                PositionZRenumberStrategy = (byte) ReadProperty(PositionZRenumberStrategyProperty),
                RestartComponentRenumberingPerBay = ReadProperty(RestartComponentRenumberingPerBayProperty),
                IgnoreNonMerchandisingComponents = ReadProperty(IgnoreNonMerchandisingComponentsProperty),
                RestartPositionRenumberingPerComponent = ReadProperty(RestartPositionRenumberingPerComponentProperty),
                UniqueNumberMultiPositionProductsPerComponent = ReadProperty(UniqueNumberMultiPositionProductsPerComponentProperty),
                ExceptAdjacentPositions = ReadProperty(ExceptAdjacentPositionsProperty),
                DateCreated = ReadProperty(DateCreatedProperty),
                DateLastModified = ReadProperty(DateLastModifiedProperty),
                DateDeleted = ReadProperty(DateDeletedProperty),
                RestartPositionRenumberingPerBay = ReadProperty(RestartPositionRenumberingPerBayProperty),
                RestartComponentRenumberingPerComponentType = ReadProperty(RestartComponentRenumberingPerComponentTypeProperty)
            };
        }

        /// <summary>
        ///     Loads the values from the provided <paramref name="dto"/> into this instance.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">Instance of <see cref="RenumberingStrategyDto"/> containing the values to be loaded.</param>
        private void LoadDataTransferObject(IDalContext dalContext, RenumberingStrategyDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(RowVersionProperty, dto.RowVersion);
            LoadProperty(EntityIdProperty, dto.EntityId);
            LoadProperty(NameProperty, dto.Name);
            LoadProperty(BayComponentXRenumberStrategyPriorityProperty, dto.BayComponentXRenumberStrategyPriority);
            LoadProperty(BayComponentYRenumberStrategyPriorityProperty, dto.BayComponentYRenumberStrategyPriority);
            LoadProperty(BayComponentZRenumberStrategyPriorityProperty, dto.BayComponentZRenumberStrategyPriority);
            LoadProperty(PositionXRenumberStrategyPriorityProperty, dto.PositionXRenumberStrategyPriority);
            LoadProperty(PositionYRenumberStrategyPriorityProperty, dto.PositionYRenumberStrategyPriority);
            LoadProperty(PositionZRenumberStrategyPriorityProperty, dto.PositionZRenumberStrategyPriority);
            LoadProperty(BayComponentXRenumberStrategyProperty, dto.BayComponentXRenumberStrategy);
            LoadProperty(BayComponentYRenumberStrategyProperty, dto.BayComponentYRenumberStrategy);
            LoadProperty(BayComponentZRenumberStrategyProperty, dto.BayComponentZRenumberStrategy);
            LoadProperty(PositionXRenumberStrategyProperty, dto.PositionXRenumberStrategy);
            LoadProperty(PositionYRenumberStrategyProperty, dto.PositionYRenumberStrategy);
            LoadProperty(PositionZRenumberStrategyProperty, dto.PositionZRenumberStrategy);
            LoadProperty(RestartComponentRenumberingPerBayProperty, dto.RestartComponentRenumberingPerBay);
            LoadProperty(IgnoreNonMerchandisingComponentsProperty, dto.IgnoreNonMerchandisingComponents);
            LoadProperty(RestartPositionRenumberingPerComponentProperty, dto.RestartPositionRenumberingPerComponent);
            LoadProperty(UniqueNumberMultiPositionProductsPerComponentProperty, dto.UniqueNumberMultiPositionProductsPerComponent);
            LoadProperty(ExceptAdjacentPositionsProperty, dto.ExceptAdjacentPositions);
            LoadProperty(DateCreatedProperty, dto.DateCreated);
            LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
            LoadProperty(DateDeletedProperty, dto.DateDeleted);
            LoadProperty(RestartPositionRenumberingPerBayProperty, dto.RestartPositionRenumberingPerBay);
            LoadProperty(RestartComponentRenumberingPerComponentTypeProperty, dto.RestartComponentRenumberingPerComponentType);
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Invoked by CSLA via reflection when calling DataPortal.Fetch on this instance.
        /// </summary>
        /// <param name="criteria">The criteria object to perform the fetch.</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            var dalFactory = GetDalFactory();
            using (var dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IRenumberingStrategyDal>())
            {
                LoadDataTransferObject(dalContext, dal.FetchById(criteria.Id));
            }
        }

        /// <summary>
        ///     Invoked by CSLA via reflection when calling DataPortal.Fetch on this instance.
        /// </summary>
        /// <param name="criteria">The criteria object to perform the fetch.</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdNameCriteria criteria)
        {
            var dalFactory = GetDalFactory();
            using (var dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IRenumberingStrategyDal>())
            {
                LoadDataTransferObject(dalContext, dal.FetchByEntityIdName(criteria.EnitityId, criteria.Name));
            }
        }

        /// <summary>
        ///     Invoked by CSLA via reflection when calling DataPortal.FetchChild in this instance.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">Instance of <see cref="RenumberingStrategyDto"/> containing the values to be loaded.</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(IDalContext dalContext, RenumberingStrategyDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        ///     Invoked by CSLA via reflection when calling Save on this instance if it is new.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            var dalFactory = GetDalFactory();
            using (var dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                RenumberingStrategyDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (var dal = dalContext.GetDal<IRenumberingStrategyDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty(IdProperty, dto.Id);
                this.LoadProperty(RowVersionProperty, dto.RowVersion);
                this.LoadProperty(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<RenumberingStrategy>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        ///     Invoked by CSLA via reflection when calling Save on this instance if it is not new.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            var dalFactory = GetDalFactory();
            using (var dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                RenumberingStrategyDto dto = GetDataTransferObject();
                using (IRenumberingStrategyDal dal = dalContext.GetDal<IRenumberingStrategyDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty(RowVersionProperty, dto.RowVersion);
                this.LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        ///     Invoked by CSLA via reflection when calling Delete on this instance if it is not new.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            var dalFactory = GetDalFactory();
            using (var dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (var dal = dalContext.GetDal<IRenumberingStrategyDal>())
                {
                    dal.DeleteById(Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #endregion
    }
}