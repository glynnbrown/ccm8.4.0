﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
// V8-26140 : I.George
// Added FetchByEntityIdIncludingDeleted
#endregion
#region Version History: (CCM 8.0.3)
// V8-29214 : D.Pleasance
//  Removed FetchDeletedByEntityIdCriteria as items are now deleted outright
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// AssortmentMinorRevisionInfoList
    /// </summary>
    public partial class AssortmentMinorRevisionInfoList
    {
        #region Constructors
        private AssortmentMinorRevisionInfoList() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all items for the given entity id
        /// </summary>
        /// <returns>A list of entity infos</returns>
        public static AssortmentMinorRevisionInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<AssortmentMinorRevisionInfoList>(new FetchByEntityIdCriteria(entityId));
        }

        /// <summary>
        /// Returns all items for the given product group id
        /// </summary>
        /// <returns>A list of entity infos</returns>
        public static AssortmentMinorRevisionInfoList FetchByProductGroupId(Int32 productGroupId)
        {
            return DataPortal.Fetch<AssortmentMinorRevisionInfoList>(new FetchByProductGroupIdCriteria(productGroupId));
        }
        
        /// <summary>
        /// Returns all items for the given entity id and change date
        /// </summary>
        /// <returns>A list of entity infos</returns>
        public static AssortmentMinorRevisionInfoList FetchByEntityIdChangeDate(Int32 entityId, DateTime changeDate)
        {
            return DataPortal.Fetch<AssortmentMinorRevisionInfoList>(new FetchByEntityIdChangeDateCriteria(entityId, changeDate));
        }

        /// <summary>
        /// Returns all items for the given entity id and search criteria
        /// </summary>
        /// <returns>A list of entity infos</returns>
        public static AssortmentMinorRevisionInfoList FetchByEntityIdAssortmentMinorRevisionSearchCriteria(Int32 entityId, String assortmentSearchCriteria)
        {
            return DataPortal.Fetch<AssortmentMinorRevisionInfoList>(new FetchAssortmentByEntityIdAssortmentMinorRevisionSearchCriteria(entityId, assortmentSearchCriteria));
        }
        #endregion

        #region Data Access
        /// <summary>
        /// Called when retrieving a list of all entity infos
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IAssortmentMinorRevisionInfoDal dal = dalContext.GetDal<IAssortmentMinorRevisionInfoDal>())
                {
                    IEnumerable<AssortmentMinorRevisionInfoDto> dtoList = dal.FetchByEntityId(criteria.EntityId);
                    foreach (AssortmentMinorRevisionInfoDto dto in dtoList)
                    {
                        this.Add(AssortmentMinorRevisionInfo.GetAssortmentMinorRevisionInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when retrieving a list of assortments that have been modified/created after change date
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdChangeDateCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IAssortmentMinorRevisionInfoDal dal = dalContext.GetDal<IAssortmentMinorRevisionInfoDal>())
                {
                    IEnumerable<AssortmentMinorRevisionInfoDto> dtoList = dal.FetchByEntityIdChangeDate(criteria.EntityId, criteria.ChangeDate);
                    foreach (AssortmentMinorRevisionInfoDto dto in dtoList)
                    {
                        this.Add(AssortmentMinorRevisionInfo.GetAssortmentMinorRevisionInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }
        
        /// <summary>
        /// Called when retrieving a list of assortments wit search critiera
        /// </summary>
        private void DataPortal_Fetch(FetchAssortmentByEntityIdAssortmentMinorRevisionSearchCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IAssortmentMinorRevisionInfoDal dal = dalContext.GetDal<IAssortmentMinorRevisionInfoDal>())
                {
                    IEnumerable<AssortmentMinorRevisionInfoDto> dtoList = dal.FetchByEntityIdAssortmentMinorRevisionSearchCriteria(criteria.EntityId, criteria.AssortmentSearchCriteria);
                    foreach (AssortmentMinorRevisionInfoDto dto in dtoList)
                    {
                        this.Add(AssortmentMinorRevisionInfo.GetAssortmentMinorRevisionInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }
        
        /// <summary>
        /// Called when retrieving a list of all assortment minor revision infos
        /// </summary>
        private void DataPortal_Fetch(FetchByProductGroupIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IAssortmentMinorRevisionInfoDal dal = dalContext.GetDal<IAssortmentMinorRevisionInfoDal>())
                {
                    IEnumerable<AssortmentMinorRevisionInfoDto> dtoList = dal.FetchByProductGroupId(criteria.ProductGroupId);
                    foreach (AssortmentMinorRevisionInfoDto dto in dtoList)
                    {
                        this.Add(AssortmentMinorRevisionInfo.GetAssortmentMinorRevisionInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }
        #endregion
    }
}

