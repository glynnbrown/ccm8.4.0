﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//	Created (Auto-generated)
// V8-31105 : L.Ineson
//  Added PlanogramIsProportional property
#endregion
#region Version History: (CCM 830)
// V8-32636 : A.Probyn
//  Added PlanogramAnnotations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Windows;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    public partial class PrintTemplateComponentDetail
    {
        #region Constants

        private const String _planogramViewType = "PlanogramViewType";
        private const String _planogramPositions = "PlanogramPositions";
        private const String _planogramPositionUnits = "PlanogramPositionUnits";
        private const String _planogramProductImages = "PlanogramProductImages";
        private const String _planogramProductShapes = "PlanogramProductShapes";
        private const String _planogramProductFillColours = "PlanogramProductFillColours";
        private const String _planogramProductFillPatterns = "PlanogramProductFillPatterns";
        private const String _planogramFixtureImages = "PlanogramFixtureImages";
        private const String _planogramFixtureFillPatterns = "PlanogramFixtureFillPatterns";
        private const String _planogramChestWalls = "PlanogramChestWalls";
        private const String _planogramRotateTopDownComponents = "PlanogramRotateTopDownComponents";
        private const String _planogramShelfRisers = "PlanogramShelfRisers";
        private const String _planogramNotches = "PlanogramNotches";
        private const String _planogramPegHoles = "PlanogramPegHoles";
        private const String _planogramPegs = "PlanogramPegs";
        private const String _planogramDividerLines = "PlanogramDividerLines";
        private const String _planogramDividers = "PlanogramDividers";
        private const String _planogramTextBoxes = "PlanogramTextBoxes";
        private const String _planogramAnnotations = "PlanogramAnnotations";
        private const String _planogramProductLabelId = "PlanogramProductLabelId";
        private const String _planogramUseLiveProductLabel = "PlanogramUseLiveProductLabel";
        private const String _planogramFixtureLabelId = "PlanogramFixtureLabelId";
        private const String _planogramUseLiveFixtureLabel = "PlanogramUseLiveFixtureLabel";
        private const String _planogramHighlightId = "PlanogramHighlightId";
        private const String _planogramUseLiveHighlight = "PlanogramUseLiveHighlight";
        private const String _planogramCameraLookDirection = "PlanogramCameraLookDirection";
        private const String _planogramCameraPosition = "PlanogramCameraPosition";
        private const String _planogramIsProportional = "PlanogramIsProportional";

        private const String _textBoxText = "TextBoxText";
        private const String _textBoxFontSize = "TextBoxFontSize";
        private const String _textBoxIsFontBold = "TextBoxIsFontBold";
        private const String _textBoxFontName = "TextBoxFontName";
        private const String _textBoxForeground = "TextBoxForeground";
        private const String _textBoxTextAlignment = "TextBoxTextAlignment";
        private const String _textBoxIsFontItalic = "TextBoxIsFontItalic";
        private const String _textBoxIsFontUnderlined = "TextBoxIsFontUnderlined";

        private const String _dataSheetId = "DataSheetId";
        private const String _dataSheetStyle = "DataSheetStyle";
        private const String _dataSheetHeaderFontSize = "DataSheetHeaderFontSize";
        private const String _dataSheetHeaderFontName = "DataSheetHeaderFontName";
        private const String _dataSheetHeaderIsFontBold = "DataSheetHeaderIsFontBold";
        private const String _dataSheetHeaderForeground = "DataSheetHeaderForeground";
        private const String _dataSheetHeaderTextAlignment = "DataSheetHeaderTextAlignment";
        private const String _dataSheetHeaderIsFontItalic = "DataSheetHeaderIsFontItalic";
        private const String _dataSheetHeaderIsFontUnderlined = "DataSheetHeaderIsFontUnderlined";
        private const String _dataSheetRowFontSize = "DataSheetRowFontSize";
        private const String _dataSheetRowFontName = "DataSheetRowFontName";
        private const String _dataSheetRowIsFontBold = "DataSheetRowIsFontBold";
        private const String _dataSheetRowForeground = "DataSheetRowForeground";
        private const String _dataSheetRowTextAlignment = "DataSheetRowTextAlignment";
        private const String _dataSheetRowIsFontItalic = "DataSheetRowIsFontItalic";
        private const String _dataSheetRowIsFontUnderlined = "DataSheetRowIsFontUnderlined";

        private const String _lineEndX = "LineEndX";
        private const String _lineEndY = "LineEndY";
        private const String _lineForeground = "LineForeground";
        private const String _lineWeight = "LineWeight";

        #endregion

        #region Constructor
        private PrintTemplateComponentDetail() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static PrintTemplateComponentDetail FetchByParentId(IDalContext dalContext, Int32 printOptionComponentId, PrintTemplateComponentType componentType)
        {
            return DataPortal.FetchChild<PrintTemplateComponentDetail>(dalContext, printOptionComponentId, componentType);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto with the provided details
        /// </summary>
        /// <param name="name">The config value name</param>
        /// <param name="value">The config value</param>
        /// <returns>A new dto</returns>
        private static PrintTemplateComponentDetailDto GetDataTransferObject(String key, Object value, Int32 parentId)
        {
            return new PrintTemplateComponentDetailDto()
            {
                Key = key,
                Value = (value != null) ? Convert.ToString(value, CultureInfo.InvariantCulture) : null,
                PrintTemplateComponentId = parentId
            };
        }

        /// <summary>
        /// Returns a list of dtos for this item.
        /// </summary>
        private List<PrintTemplateComponentDetailDto> GetDataTransferObjects(Int32 parentId)
        {
            List<PrintTemplateComponentDetailDto> dtoList = new List<PrintTemplateComponentDetailDto>();

            dtoList.Add(GetDataTransferObject(_planogramViewType, ReadProperty<PrintTemplatePlanogramViewType>(PlanogramViewTypeProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramPositions, ReadProperty<Boolean>(PlanogramPositionsProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramPositionUnits, ReadProperty<Boolean>(PlanogramPositionUnitsProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramProductImages, ReadProperty<Boolean>(PlanogramProductImagesProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramProductShapes, ReadProperty<Boolean>(PlanogramProductShapesProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramProductFillColours, ReadProperty<Boolean>(PlanogramProductFillColoursProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramProductFillPatterns, ReadProperty<Boolean>(PlanogramProductFillPatternsProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramFixtureImages, ReadProperty<Boolean>(PlanogramFixtureImagesProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramFixtureFillPatterns, ReadProperty<Boolean>(PlanogramFixtureFillPatternsProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramChestWalls, ReadProperty<Boolean>(PlanogramChestWallsProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramRotateTopDownComponents, ReadProperty<Boolean>(PlanogramRotateTopDownComponentsProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramShelfRisers, ReadProperty<Boolean>(PlanogramShelfRisersProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramNotches, ReadProperty<Boolean>(PlanogramNotchesProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramPegHoles, ReadProperty<Boolean>(PlanogramPegHolesProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramPegs, ReadProperty<Boolean>(PlanogramPegsProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramDividerLines, ReadProperty<Boolean>(PlanogramDividerLinesProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramDividers, ReadProperty<Boolean>(PlanogramDividersProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramTextBoxes, ReadProperty<Boolean>(PlanogramTextBoxesProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramAnnotations, ReadProperty<Boolean>(PlanogramAnnotationsProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramProductLabelId, ReadProperty<Object>(PlanogramProductLabelIdProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramUseLiveProductLabel, ReadProperty<Boolean>(PlanogramUseLiveProductLabelProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramFixtureLabelId, ReadProperty<Object>(PlanogramFixtureLabelIdProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramUseLiveFixtureLabel, ReadProperty<Boolean>(PlanogramUseLiveFixtureLabelProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramHighlightId, ReadProperty<Object>(PlanogramHighlightIdProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramUseLiveHighlight, ReadProperty<Boolean>(PlanogramUseLiveHighlightProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramCameraLookDirection, ReadProperty<String>(PlanogramCameraLookDirectionProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramCameraPosition, ReadProperty<String>(PlanogramCameraPositionProperty), parentId));
            dtoList.Add(GetDataTransferObject(_planogramIsProportional, ReadProperty<Boolean>(PlanogramIsProportionalProperty), parentId));

            dtoList.Add(GetDataTransferObject(_textBoxText, ReadProperty<String>(TextBoxTextProperty), parentId));
            dtoList.Add(GetDataTransferObject(_textBoxFontSize, ReadProperty<Byte>(TextBoxFontSizeProperty), parentId));
            dtoList.Add(GetDataTransferObject(_textBoxIsFontBold, ReadProperty<Boolean>(TextBoxIsFontBoldProperty), parentId));
            dtoList.Add(GetDataTransferObject(_textBoxFontName, ReadProperty<String>(TextBoxFontNameProperty), parentId));
            dtoList.Add(GetDataTransferObject(_textBoxForeground, ReadProperty<Int32>(TextBoxForegroundProperty), parentId));
            dtoList.Add(GetDataTransferObject(_textBoxTextAlignment, ReadProperty<TextAlignment>(TextBoxTextAlignmentProperty), parentId));
            dtoList.Add(GetDataTransferObject(_textBoxIsFontItalic, ReadProperty<Boolean>(TextBoxIsFontItalicProperty), parentId));
            dtoList.Add(GetDataTransferObject(_textBoxIsFontUnderlined, ReadProperty<Boolean>(TextBoxIsFontUnderlinedProperty), parentId));

            dtoList.Add(GetDataTransferObject(_dataSheetId, ReadProperty<Object>(DataSheetIdProperty), parentId));
            dtoList.Add(GetDataTransferObject(_dataSheetStyle, ReadProperty<PrintTemplateComponentGridStyle>(DataSheetStyleProperty), parentId));
            dtoList.Add(GetDataTransferObject(_dataSheetHeaderFontSize, ReadProperty<Byte>(DataSheetHeaderFontSizeProperty), parentId));
            dtoList.Add(GetDataTransferObject(_dataSheetHeaderFontName, ReadProperty<String>(DataSheetHeaderFontNameProperty), parentId));
            dtoList.Add(GetDataTransferObject(_dataSheetHeaderIsFontBold, ReadProperty<Boolean>(DataSheetHeaderIsFontBoldProperty), parentId));
            dtoList.Add(GetDataTransferObject(_dataSheetHeaderForeground, ReadProperty<Int32>(DataSheetHeaderForegroundProperty), parentId));
            dtoList.Add(GetDataTransferObject(_dataSheetHeaderTextAlignment, ReadProperty<TextAlignment>(DataSheetHeaderTextAlignmentProperty), parentId));
            dtoList.Add(GetDataTransferObject(_dataSheetHeaderIsFontItalic, ReadProperty<Boolean>(DataSheetHeaderIsFontItalicProperty), parentId));
            dtoList.Add(GetDataTransferObject(_dataSheetHeaderIsFontUnderlined, ReadProperty<Boolean>(DataSheetHeaderIsFontUnderlinedProperty), parentId));
            dtoList.Add(GetDataTransferObject(_dataSheetRowFontSize, ReadProperty<Byte>(DataSheetRowFontSizeProperty), parentId));
            dtoList.Add(GetDataTransferObject(_dataSheetRowFontName, ReadProperty<String>(DataSheetRowFontNameProperty), parentId));
            dtoList.Add(GetDataTransferObject(_dataSheetRowIsFontBold, ReadProperty<Boolean>(DataSheetRowIsFontBoldProperty), parentId));
            dtoList.Add(GetDataTransferObject(_dataSheetRowForeground, ReadProperty<Int32>(DataSheetRowForegroundProperty), parentId));
            dtoList.Add(GetDataTransferObject(_dataSheetRowTextAlignment, ReadProperty<TextAlignment>(DataSheetRowTextAlignmentProperty), parentId));
            dtoList.Add(GetDataTransferObject(_dataSheetRowIsFontItalic, ReadProperty<Boolean>(DataSheetRowIsFontItalicProperty), parentId));
            dtoList.Add(GetDataTransferObject(_dataSheetRowIsFontUnderlined, ReadProperty<Boolean>(DataSheetRowIsFontUnderlinedProperty), parentId));

            dtoList.Add(GetDataTransferObject(_lineEndX, ReadProperty<Double>(LineEndXProperty), parentId));
            dtoList.Add(GetDataTransferObject(_lineEndY, ReadProperty<Double>(LineEndYProperty), parentId));
            dtoList.Add(GetDataTransferObject(_lineForeground, ReadProperty<Int32>(LineForegroundProperty), parentId));
            dtoList.Add(GetDataTransferObject(_lineWeight, ReadProperty<Double>(LineWeightProperty), parentId));


            return dtoList;
        }

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObjects(IEnumerable<PrintTemplateComponentDetailDto> dtoList, PrintTemplateComponentType componentType)
        {
            //load the defaults first in case any values are missing.
            LoadDefaults(componentType);

            foreach (PrintTemplateComponentDetailDto dto in dtoList)
            {
                switch (dto.Key)
                {
                    case _planogramViewType:
                        LoadProperty<PrintTemplatePlanogramViewType>(PlanogramViewTypeProperty,
                            EnumHelper.Parse<PrintTemplatePlanogramViewType>(dto.Value, PrintTemplatePlanogramViewType.Design));
                        break;
                    case _planogramPositions: LoadProperty<Boolean>(PlanogramPositionsProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramPositionUnits: LoadProperty<Boolean>(PlanogramPositionUnitsProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramProductImages: LoadProperty<Boolean>(PlanogramProductImagesProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramProductShapes: LoadProperty<Boolean>(PlanogramProductShapesProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramProductFillColours: LoadProperty<Boolean>(PlanogramProductFillColoursProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramProductFillPatterns: LoadProperty<Boolean>(PlanogramProductFillPatternsProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramFixtureImages: LoadProperty<Boolean>(PlanogramFixtureImagesProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramFixtureFillPatterns: LoadProperty<Boolean>(PlanogramFixtureFillPatternsProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramChestWalls: LoadProperty<Boolean>(PlanogramChestWallsProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramRotateTopDownComponents: LoadProperty<Boolean>(PlanogramRotateTopDownComponentsProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramShelfRisers: LoadProperty<Boolean>(PlanogramShelfRisersProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramNotches: LoadProperty<Boolean>(PlanogramNotchesProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramPegHoles: LoadProperty<Boolean>(PlanogramPegHolesProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramPegs: LoadProperty<Boolean>(PlanogramPegsProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramDividerLines: LoadProperty<Boolean>(PlanogramDividerLinesProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramDividers: LoadProperty<Boolean>(PlanogramDividersProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramTextBoxes: LoadProperty<Boolean>(PlanogramTextBoxesProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramAnnotations: LoadProperty<Boolean>(PlanogramAnnotationsProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramProductLabelId: LoadProperty<Object>(PlanogramProductLabelIdProperty, dto.Value); break;
                    case _planogramUseLiveProductLabel: LoadProperty<Boolean>(PlanogramUseLiveProductLabelProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramFixtureLabelId: LoadProperty<Object>(PlanogramFixtureLabelIdProperty, dto.Value); break;
                    case _planogramUseLiveFixtureLabel: LoadProperty<Boolean>(PlanogramUseLiveFixtureLabelProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramHighlightId: LoadProperty<Object>(PlanogramHighlightIdProperty, dto.Value); break;
                    case _planogramUseLiveHighlight: LoadProperty<Boolean>(PlanogramUseLiveHighlightProperty, Boolean.Parse(dto.Value)); break;
                    case _planogramCameraLookDirection: LoadProperty<String>(PlanogramCameraLookDirectionProperty, dto.Value); break;
                    case _planogramCameraPosition: LoadProperty<String>(PlanogramCameraPositionProperty, dto.Value); break;
                    case _planogramIsProportional: LoadProperty<Boolean>(PlanogramIsProportionalProperty, Boolean.Parse(dto.Value)); break;

                    case _textBoxText: LoadProperty<String>(TextBoxTextProperty, dto.Value); break;
                    case _textBoxFontSize: LoadProperty<Byte>(TextBoxFontSizeProperty, Byte.Parse(dto.Value, CultureInfo.InvariantCulture)); break;
                    case _textBoxIsFontBold: LoadProperty<Boolean>(TextBoxIsFontBoldProperty, Boolean.Parse(dto.Value)); break;
                    case _textBoxFontName: LoadProperty<String>(TextBoxFontNameProperty, dto.Value); break;
                    case _textBoxForeground: LoadProperty<Int32>(TextBoxForegroundProperty, Int32.Parse(dto.Value, CultureInfo.InvariantCulture)); break;
                    case _textBoxTextAlignment: LoadProperty<TextAlignment>(TextBoxTextAlignmentProperty, EnumHelper.Parse<TextAlignment>(dto.Value, TextAlignment.Left)); break;
                    case _textBoxIsFontItalic: LoadProperty<Boolean>(TextBoxIsFontItalicProperty, Boolean.Parse(dto.Value)); break;
                    case _textBoxIsFontUnderlined: LoadProperty<Boolean>(TextBoxIsFontUnderlinedProperty, Boolean.Parse(dto.Value)); break;

                    case _dataSheetId: LoadProperty<Object>(DataSheetIdProperty, dto.Value); break;
                    case _dataSheetStyle:
                        LoadProperty<PrintTemplateComponentGridStyle>(DataSheetStyleProperty,
                        EnumHelper.Parse<PrintTemplateComponentGridStyle>(dto.Value, PrintTemplateComponentGridStyle.Standard));
                        break;
                    case _dataSheetHeaderFontSize: LoadProperty<Byte>(DataSheetHeaderFontSizeProperty, Byte.Parse(dto.Value, CultureInfo.InvariantCulture)); break;
                    case _dataSheetHeaderFontName: LoadProperty<String>(DataSheetHeaderFontNameProperty, dto.Value); break;
                    case _dataSheetHeaderIsFontBold: LoadProperty<Boolean>(DataSheetHeaderIsFontBoldProperty, Boolean.Parse(dto.Value)); break;
                    case _dataSheetHeaderForeground: LoadProperty<Int32>(DataSheetHeaderForegroundProperty, Int32.Parse(dto.Value, CultureInfo.InvariantCulture)); break;
                    case _dataSheetHeaderTextAlignment: LoadProperty<TextAlignment>(DataSheetHeaderTextAlignmentProperty, EnumHelper.Parse<TextAlignment>(dto.Value, TextAlignment.Left)); break;
                    case _dataSheetHeaderIsFontItalic: LoadProperty<Boolean>(DataSheetHeaderIsFontItalicProperty, Boolean.Parse(dto.Value)); break;
                    case _dataSheetHeaderIsFontUnderlined: LoadProperty<Boolean>(DataSheetHeaderIsFontUnderlinedProperty, Boolean.Parse(dto.Value)); break;
                    case _dataSheetRowFontSize: LoadProperty<Byte>(DataSheetRowFontSizeProperty, Byte.Parse(dto.Value, CultureInfo.InvariantCulture)); break;
                    case _dataSheetRowFontName: LoadProperty<String>(DataSheetRowFontNameProperty, dto.Value); break;
                    case _dataSheetRowIsFontBold: LoadProperty<Boolean>(DataSheetRowIsFontBoldProperty, Boolean.Parse(dto.Value)); break;
                    case _dataSheetRowForeground: LoadProperty<Int32>(DataSheetRowForegroundProperty, Int32.Parse(dto.Value, CultureInfo.InvariantCulture)); break;
                    case _dataSheetRowTextAlignment: LoadProperty<TextAlignment>(DataSheetRowTextAlignmentProperty, EnumHelper.Parse<TextAlignment>(dto.Value, TextAlignment.Left)); break;
                    case _dataSheetRowIsFontItalic: LoadProperty<Boolean>(DataSheetRowIsFontItalicProperty, Boolean.Parse(dto.Value)); break;
                    case _dataSheetRowIsFontUnderlined: LoadProperty<Boolean>(DataSheetRowIsFontUnderlinedProperty, Boolean.Parse(dto.Value)); break;

                    case _lineEndX: LoadProperty<Double>(LineEndXProperty, Double.Parse(dto.Value, CultureInfo.InvariantCulture)); break;
                    case _lineEndY: LoadProperty<Double>(LineEndYProperty, Double.Parse(dto.Value, CultureInfo.InvariantCulture)); break;
                    case _lineForeground: LoadProperty<Int32>(LineForegroundProperty, Int32.Parse(dto.Value, CultureInfo.InvariantCulture)); break;
                    case _lineWeight: LoadProperty<Double>(LineWeightProperty, Double.Parse(dto.Value, CultureInfo.InvariantCulture)); break;

                }
            }
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 parentId, PrintTemplateComponentType componentType)
        {
            using (IPrintTemplateComponentDetailDal dal = dalContext.GetDal<IPrintTemplateComponentDetailDal>())
            {
                LoadDataTransferObjects(dal.FetchByPrintTemplateComponentId(parentId), componentType);
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, PrintTemplateComponent parent)
        {
            using (IPrintTemplateComponentDetailDal dal = dalContext.GetDal<IPrintTemplateComponentDetailDal>())
            {
                UpdateDtos(dal, parent);
            }

            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, PrintTemplateComponent parent)
        {
            if (this.IsSelfDirty)
            {
                using (IPrintTemplateComponentDetailDal dal = dalContext.GetDal<IPrintTemplateComponentDetailDal>())
                {
                    UpdateDtos(dal, parent);
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, PrintTemplateComponent parent)
        {
            using (IPrintTemplateComponentDetailDal dal = dalContext.GetDal<IPrintTemplateComponentDetailDal>())
            {
                dal.DeleteByPrintTemplateComponentId(parent.Id);
            }
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Method to update or insert config values for the current entity id
        /// as required. This is used by both the child_insert & child_update
        /// </summary>
        /// <param name="dal"></param>
        private void UpdateDtos(IPrintTemplateComponentDetailDal dal, PrintTemplateComponent parent)
        {
            Int32 parentId = parent.Id;
            IEnumerable<PrintTemplateComponentDetailDto> oldDtoList = dal.FetchByPrintTemplateComponentId(parentId).ToList();

            UpdateDtoItem(dal, oldDtoList, _planogramViewType, ReadProperty(PlanogramViewTypeProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramPositions, ReadProperty(PlanogramPositionsProperty), parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramPositionUnits, ReadProperty(PlanogramPositionUnitsProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramProductImages, ReadProperty(PlanogramProductImagesProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramProductShapes, ReadProperty(PlanogramProductShapesProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramProductFillColours, ReadProperty(PlanogramProductFillColoursProperty), parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramProductFillPatterns, ReadProperty(PlanogramProductFillPatternsProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramFixtureImages, ReadProperty(PlanogramFixtureImagesProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramFixtureFillPatterns, ReadProperty(PlanogramFixtureFillPatternsProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramChestWalls, ReadProperty(PlanogramChestWallsProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramRotateTopDownComponents, ReadProperty(PlanogramRotateTopDownComponentsProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramShelfRisers, ReadProperty(PlanogramShelfRisersProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramNotches, ReadProperty(PlanogramNotchesProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramPegHoles, ReadProperty(PlanogramPegHolesProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramPegs, ReadProperty(PlanogramPegsProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramDividerLines, ReadProperty(PlanogramDividerLinesProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramDividers, ReadProperty(PlanogramDividersProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramTextBoxes, ReadProperty(PlanogramTextBoxesProperty), parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramAnnotations, ReadProperty(PlanogramAnnotationsProperty), parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramProductLabelId, ReadProperty(PlanogramProductLabelIdProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramUseLiveProductLabel, ReadProperty(PlanogramUseLiveProductLabelProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramFixtureLabelId, ReadProperty(PlanogramFixtureLabelIdProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramUseLiveFixtureLabel, ReadProperty(PlanogramUseLiveFixtureLabelProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramHighlightId, ReadProperty(PlanogramHighlightIdProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramUseLiveHighlight, ReadProperty(PlanogramUseLiveHighlightProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramCameraLookDirection, ReadProperty(PlanogramCameraLookDirectionProperty), parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramCameraPosition, ReadProperty(PlanogramCameraPositionProperty), parentId);
            UpdateDtoItem(dal, oldDtoList, _planogramIsProportional, ReadProperty(PlanogramIsProportionalProperty), parentId);

            UpdateDtoItem(dal, oldDtoList, _textBoxText, ReadProperty(TextBoxTextProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _textBoxFontSize, ReadProperty(TextBoxFontSizeProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _textBoxIsFontBold, ReadProperty(TextBoxIsFontBoldProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _textBoxFontName, ReadProperty(TextBoxFontNameProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _textBoxForeground, ReadProperty(TextBoxForegroundProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _textBoxTextAlignment, ReadProperty(TextBoxTextAlignmentProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _textBoxIsFontItalic, ReadProperty(TextBoxIsFontItalicProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _textBoxIsFontUnderlined, ReadProperty(TextBoxIsFontUnderlinedProperty),parentId);

            UpdateDtoItem(dal, oldDtoList, _dataSheetId, ReadProperty(DataSheetIdProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _dataSheetStyle, ReadProperty(DataSheetStyleProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _dataSheetHeaderFontSize, ReadProperty(DataSheetHeaderFontSizeProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _dataSheetHeaderFontName, ReadProperty(DataSheetHeaderFontNameProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _dataSheetHeaderIsFontBold, ReadProperty(DataSheetHeaderIsFontBoldProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _dataSheetHeaderForeground, ReadProperty(DataSheetHeaderForegroundProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _dataSheetHeaderTextAlignment, ReadProperty(DataSheetHeaderTextAlignmentProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _dataSheetHeaderIsFontItalic, ReadProperty(DataSheetHeaderIsFontItalicProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _dataSheetHeaderIsFontUnderlined, ReadProperty(DataSheetHeaderIsFontUnderlinedProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _dataSheetRowFontSize, ReadProperty(DataSheetRowFontSizeProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _dataSheetRowFontName, ReadProperty(DataSheetRowFontNameProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _dataSheetRowIsFontBold, ReadProperty(DataSheetRowIsFontBoldProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _dataSheetRowForeground, ReadProperty(DataSheetRowForegroundProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _dataSheetRowTextAlignment, ReadProperty(DataSheetRowTextAlignmentProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _dataSheetRowIsFontItalic, ReadProperty(DataSheetRowIsFontItalicProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _dataSheetRowIsFontUnderlined, ReadProperty(DataSheetRowIsFontUnderlinedProperty),parentId);

            UpdateDtoItem(dal, oldDtoList, _lineEndX, ReadProperty(LineEndXProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _lineEndY, ReadProperty(LineEndYProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _lineForeground, ReadProperty(LineForegroundProperty),parentId);
            UpdateDtoItem(dal, oldDtoList, _lineWeight, ReadProperty(LineWeightProperty),parentId);

        }

        /// <summary>
        /// Updates or inserts based on the given key.
        /// </summary>
        private static void UpdateDtoItem(IPrintTemplateComponentDetailDal dal, IEnumerable<PrintTemplateComponentDetailDto> oldDtoList, 
            String key, Object value, Int32 parentId)
        {
            PrintTemplateComponentDetailDto newDto = GetDataTransferObject(key, value, parentId);
            PrintTemplateComponentDetailDto oldDto = oldDtoList.FirstOrDefault(c => c.Key == key);

            if (oldDto == null)
            {
                dal.Insert(newDto);
            }
            else
            {
                oldDto.Value = newDto.Value;
                dal.Update(oldDto);
            }
        }

        #endregion

    }
}