﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-26520 J.Pickup
//  Created
#endregion
#region Version History: (CCM 830)
// V8-31831 : A.Probyn
//  Updated to include PlanogramNameTemplate
// V8-32810 : M.Pettit
//  Updated to include PrintTemplate
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// ContentLookupItemType Enum
    /// </summary>
    public enum ContentLookupItemType
    {
        ProductUniverse = 0,
        Assortment = 1,
        MetricProfile = 2,
        InventoryProfile = 3,
        ConsumerDecisionTree = 4, 
        MinorAssortment = 5, 
        PerformanceSelections = 6,
        Sequence = 7,
        Blocking = 8,
        Cluster = 9,
        RenumberingStrategy = 10,
        ValidationTemplate = 11,
        PlanogramNameTemplate = 12,
        PrintTemplate = 13
    }

    /// <summary>
    /// ContentLookupItemType Helper Class
    /// </summary>
    public static class ContentLookupItemTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<ContentLookupItemType, String> FriendlyNames =
            new Dictionary<ContentLookupItemType, String>()
            {
                {ContentLookupItemType.MetricProfile, Message.Enum_ContentLookupItemType_MetricProfile},
                {ContentLookupItemType.InventoryProfile, Message.Enum_ContentLookupItemType_InventoryProfile},
                {ContentLookupItemType.Assortment, Message.Enum_ContentLookupItemType_Assortment},
                {ContentLookupItemType.ProductUniverse, Message.Enum_ContentLookupItemType_ProductUniverse},
                {ContentLookupItemType.ConsumerDecisionTree, Message.Enum_ContentLookupItemType_ConsumerDecisionTree},
                {ContentLookupItemType.MinorAssortment, Message.Enum_ContentLookupItemType_MinorAssortment},
                {ContentLookupItemType.PerformanceSelections, Message.Enum_ContentLookupItemType_PerformanceSelections},
                {ContentLookupItemType.Sequence, Message.Enum_ContentLookupItemType_Sequence},
                {ContentLookupItemType.Blocking, Message.Enum_ContentLookupItemType_Blocking},
                {ContentLookupItemType.Cluster, Message.Enum_ContentLookupItemType_Cluster},
                {ContentLookupItemType.RenumberingStrategy, Message.Enum_ContentLookupItemType_RenumberingStrategy},
                {ContentLookupItemType.ValidationTemplate, Message.Enum_ContentLookupItemType_ValidationTemplate},
                {ContentLookupItemType.PlanogramNameTemplate, Message.Enum_ContentLookupItemType_PlanogramNameTemplate},
                {ContentLookupItemType.PrintTemplate, Message.Enum_ContentLookupItemType_PrintTemplate},
            };
    }
}

