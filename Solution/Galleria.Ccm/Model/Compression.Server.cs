﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.DataStructures;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class Compression
    {
        #region Constructor
        private Compression() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        ///  Returns the Compression with the given id
        /// </summary>
        /// <returns></returns>
        public static Compression GetCompressionById(Int32 id)
        {
            return DataPortal.Fetch<Compression>(new SingleCriteria<Int32>(id));
        }

        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <returns>An existing unit</returns>
        internal static Compression GetCompression(IDalContext dalContext, CompressionDto dto)
        {
            return DataPortal.FetchChild<Compression>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private CompressionDto GetDataTransferObject()
        {
            return new CompressionDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                Name = ReadProperty<String>(NameProperty),
                Width = ReadProperty<Int32>(WidthProperty),
                Height = ReadProperty<Int32>(HeightProperty),
                ColourDepth = (Int32)ReadProperty<ImagePixelFormat>(ColourDepthProperty),
                MaintainAspectRatio = ReadProperty<Boolean>(MaintainAspectRatioProperty),
                Enabled = ReadProperty<Boolean>(EnabledProperty),
            };
        }


        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, CompressionDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<Int32>(WidthProperty, dto.Width);
            LoadProperty<Int32>(HeightProperty, dto.Height);
            LoadProperty<ImagePixelFormat>(ColourDepthProperty, (ImagePixelFormat)dto.ColourDepth);
            LoadProperty<Boolean>(MaintainAspectRatioProperty, dto.MaintainAspectRatio);
            LoadProperty<Boolean>(EnabledProperty, dto.Enabled);
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Called when return FetchById
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ICompressionDal dal = dalContext.GetDal<ICompressionDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchById(criteria.Value));
                }
            }
        }

        /// <summary>
        /// Called when returning an existing item
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, CompressionDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts a new object into the solution
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent model</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext)
        {
            CompressionDto dto = GetDataTransferObject();
            Int32 oldId = dto.Id;
            using (ICompressionDal dal = dalContext.GetDal<ICompressionDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            dalContext.RegisterId<Compression>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an object of this type
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent model</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            CompressionDto dto = GetDataTransferObject();
            using (ICompressionDal dal = dalContext.GetDal<ICompressionDal>())
            {
                dal.Update(dto);
            }
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (ICompressionDal dal = dalContext.GetDal<ICompressionDal>())
            {
                dal.DeleteById(this.Id);
            }

        }
        #endregion

        #endregion
    }
}
