﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-27411 : M.Pettit
//  Created
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;
using Galleria.Framework.Engine.Dal.DataTransferObjects;
using Galleria.Framework.Engine.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class EngineMessageStatsInfo
    {
        #region Constructors
        private EngineMessageStatsInfo() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an Engine Message Stats Info from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        /// <returns>An Info object</returns>
        public static EngineMessageStatsInfo GetEngineMessageStatsInfo()
        {
            return DataPortal.Fetch<EngineMessageStatsInfo>();
        }
        #endregion

        #region Data Access

        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, EngineMessageStatsDto dto)
        {
            this.LoadProperty<Int32>(MessageCountProperty, dto.MessageCount);
            this.LoadProperty<Int32>(MessageProcessCountProperty, dto.MessageProcessCount);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void DataPortal_Fetch()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IEngineMessageDal dal = dalContext.GetDal<IEngineMessageDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchStats());
                }
            }
        }
        #endregion

        #endregion

    }
}
