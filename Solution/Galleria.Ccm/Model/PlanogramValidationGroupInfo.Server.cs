﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26944 : A.Silva ~ Created.
// V8-27004 : A.Silva ~ Properly implemented.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Server-side implementation for <see cref="PlanogramValidationGroupInfo"/>.
    /// </summary>
    public partial class PlanogramValidationGroupInfo
    {
        #region Constructor

        /// <summary>
        ///     Private constructor. Use factory methods to retrieve new instances.
        /// </summary>
        private PlanogramValidationGroupInfo() {}

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Gets a <see cref="PlanogramValidationGroupInfo"/> from a given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dalContext">Not used.</param>
        /// <param name="dto">Source <see cref="PlanogramValidationGroupInfoDto"/> to load in the new instance.</param>
        /// <param name="validationMetricInfoDtos">Collection of <see cref="PlanogramValidationMetricInfoDto"/> instances to get this instance's metrics from.</param>
        /// <returns>A new instance of <see cref="PlanogramValidationGroupInfo"/> loaded with the data in the given <paramref name="dto"/>.</returns>
        /// <remarks>CSLA will invoke <see cref="Child_Fetch"/> on the server via reflection.</remarks>
        public static PlanogramValidationGroupInfo GetPlanogramValidationGroupInfo(IDalContext dalContext,
            PlanogramValidationGroupInfoDto dto, IEnumerable<PlanogramValidationMetricInfoDto> validationMetricInfoDtos)
        {
            return DataPortal.FetchChild<PlanogramValidationGroupInfo>(dalContext, dto, validationMetricInfoDtos);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Loads the data from the given <paramref name="dto"/> into this instance.
        /// </summary>
        /// <param name="dalContext">Not used.</param>
        /// <param name="dto">The source <see cref="PlanogramValidationGroupInfoDto"/>.</param>
        /// <param name="validationMetricInfoDtos">The collection of <see cref="PlanogramValidationMetricInfoDto"/> containing the metrics for this instance.</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramValidationGroupInfoDto dto,
            IEnumerable<PlanogramValidationMetricInfoDto> validationMetricInfoDtos)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32>(PlanogramValidationIdProperty, dto.PlanogramValidationId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<PlanogramValidationTemplateResultType>(ResultTypeProperty, (PlanogramValidationTemplateResultType) dto.ResultType);
            var groupMetricDtos = validationMetricInfoDtos.Where(o => o.PlanogramValidationGroupId == dto.Id);
            this.LoadProperty<PlanogramValidationMetricInfoList>(MetricsProperty, PlanogramValidationMetricInfoList.Fetch(dalContext, groupMetricDtos));
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Invoked by CSLA via reflection when calling <see cref="GetPlanogramValidationGroupInfo" />.
        /// </summary>
        /// <param name="dalContext">Not used.</param>
        /// <param name="dto">
        ///     Instance of <see cref="PlanogramValidationGroupInfo" /> containing the data to initialize the new
        ///     instance.
        /// </param>
        /// <param name="validationMetricInfoDtos">The collection of <see cref="PlanogramValidationMetricInfoDto"/> containing the metrics for this instance.</param>
        private void Child_Fetch(IDalContext dalContext, PlanogramValidationGroupInfoDto dto,
            IEnumerable<PlanogramValidationMetricInfoDto> validationMetricInfoDtos)
        {
            this.LoadDataTransferObject(dalContext, dto, validationMetricInfoDtos);
        }
        #endregion

        #endregion
    }
}
