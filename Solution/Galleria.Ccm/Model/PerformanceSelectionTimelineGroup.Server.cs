﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26159 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class PerformanceSelectionTimelineGroup
    {
        #region Constructor
        private PerformanceSelectionTimelineGroup() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static PerformanceSelectionTimelineGroup Fetch(IDalContext dalContext, PerformanceSelectionTimelineGroupDto dto)
        {
            return DataPortal.FetchChild<PerformanceSelectionTimelineGroup>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PerformanceSelectionTimelineGroupDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int64>(CodeProperty, dto.Code);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private PerformanceSelectionTimelineGroupDto GetDataTransferObject(PerformanceSelection parent)
        {
            return new PerformanceSelectionTimelineGroupDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                PerformanceSelectionId = parent.Id,
                Code = ReadProperty<Int64>(CodeProperty),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, PerformanceSelectionTimelineGroupDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, PerformanceSelection parent)
        {
            PerformanceSelectionTimelineGroupDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IPerformanceSelectionTimelineGroupDal dal = dalContext.GetDal<IPerformanceSelectionTimelineGroupDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<PerformanceSelectionTimelineGroup>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, PerformanceSelection parent)
        {
            if (this.IsSelfDirty)
            {
                using (IPerformanceSelectionTimelineGroupDal dal = dalContext.GetDal<IPerformanceSelectionTimelineGroupDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IPerformanceSelectionTimelineGroupDal dal = dalContext.GetDal<IPerformanceSelectionTimelineGroupDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion

    }
}