﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a synchronisation target for
    /// the sychronisation service
    /// </summary>
    public partial class SyncTarget
    {
        #region Constructors
        public SyncTarget() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the specified sync target
        /// </summary>
        /// <param name="id">The sync target id</param>
        public static SyncTarget GetSyncTargetById(Int32 id)
        {
            return DataPortal.Fetch<SyncTarget>(new SingleCriteria<SyncTarget, Int32>(id));
        }

        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        /// <returns>An existing sync target object</returns>
        internal static SyncTarget GetSyncTarget(IDalContext dalContext, SyncTargetDto dto)
        {
            return DataPortal.FetchChild<SyncTarget>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Returns a dto for this object
        /// </summary>
        /// <returns>A dto created from this instance</returns>
        private SyncTargetDto GetDataTransferObject()
        {
            return new SyncTargetDto()
            {
                Id = this.ReadProperty<Int32>(IdProperty),
                TargetType = (Byte)this.ReadProperty<SyncTargetType>(TargetTypeProperty),
                ConnectionString = this.ReadProperty<String>(ConnectionStringProperty),
                Status = (Byte)this.ReadProperty<SyncTargetStatus>(StatusProperty),
                Frequency = (Byte)this.ReadProperty<SyncTargetFrequency>(FrequencyProperty),
                LastSync = this.ReadProperty<DateTime?>(LastSyncProperty),
                Error = (Byte)this.ReadProperty<SyncTargetError>(ErrorProperty),
                IsActive = this.ReadProperty<Boolean>(IsActiveProperty),
                EntityId = this.ReadProperty<Int32>(EntityIdProperty),
                RetryCount = this.ReadProperty<Int32>(RetryCountProperty),
                FailedCount = this.ReadProperty<Int32>(FailedCountProperty),
                LastRun = this.ReadProperty<DateTime?>(LastRunProperty),
            };
        }

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, SyncTargetDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<SyncTargetType>(TargetTypeProperty, (SyncTargetType)dto.TargetType);
            this.LoadProperty<String>(ConnectionStringProperty, dto.ConnectionString);
            this.LoadProperty<SyncTargetStatus>(StatusProperty, (SyncTargetStatus)dto.Status);
            this.LoadProperty<SyncTargetFrequency>(FrequencyProperty, (SyncTargetFrequency)dto.Frequency);
            this.LoadProperty<DateTime?>(LastSyncProperty, dto.LastSync);
            this.LoadProperty<SyncTargetError>(ErrorProperty, (SyncTargetError)dto.Error);
            this.LoadProperty<Boolean>(IsActiveProperty, dto.IsActive);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<Int32>(RetryCountProperty, dto.RetryCount);
            this.LoadProperty<Int32>(FailedCountProperty, dto.FailedCount);
            this.LoadProperty<DateTime?>(LastRunProperty, dto.LastRun);

            LoadProperty<SyncTargetGfsPerformanceList>(SyncTargetGfsPerformancesProperty,
                SyncTargetGfsPerformanceList.FetchBySyncTargetIdAsChild(dalContext, dto.Id));
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when loading this instance
        /// from a data reader as part of a
        /// review list
        /// </summary>
        /// <param name="criteria">an object used to identify the desired review</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(SingleCriteria<SyncTarget, Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory(Constants.SyncDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ISyncTargetDal dal = dalContext.GetDal<ISyncTargetDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchById(criteria.Value));
                }
            }
        }

        /// <summary>
        /// Called when loading this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, SyncTargetDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this object into the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory(Constants.SyncDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ISyncTargetDal dal = dalContext.GetDal<ISyncTargetDal>())
                {
                    SyncTargetDto dto = GetDataTransferObject();
                    dal.Insert(dto);
                    LoadProperty<Int32>(IdProperty, dto.Id);
                }
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this object in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            SyncTargetIsSetDto IsSetDto = new SyncTargetIsSetDto();
            SetIsSetProperties(IsSetDto, this.UISource);

            IDalFactory dalFactory = DalContainer.GetDalFactory(Constants.SyncDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ISyncTargetDal dal = dalContext.GetDal<ISyncTargetDal>())
                {
                    SyncTargetDto dto = GetDataTransferObject();
                    dal.Update(dto, IsSetDto);
                }
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }

        /// <summary>
        /// Set the sync target is set properties based on what is the owner of the object.
        /// This is either the sync tool ui or the sync service
        /// </summary>
        internal static void SetIsSetProperties(SyncTargetIsSetDto IsSetDto, Boolean uiSource)
        {
            if (uiSource)
            {
                IsSetDto.IsTargetTypeSet = true;
                IsSetDto.IsConnectionStringSet = true;
                IsSetDto.IsFrequencySet = true;
                IsSetDto.IsLastRunSet = true;
                IsSetDto.IsIsActiveSet = true;
                IsSetDto.IsEntityIdSet = true;
                IsSetDto.IsLastSyncSet = false;
                IsSetDto.IsStatusSet = false;
                IsSetDto.IsErrorSet = false;
                IsSetDto.IsRetryCountSet = false;
                IsSetDto.IsFailedCountSet = false;
            }
            else
            {
                IsSetDto.IsLastRunSet = true;
                IsSetDto.IsLastSyncSet = true;
                IsSetDto.IsStatusSet = true;
                IsSetDto.IsErrorSet = true;
                IsSetDto.IsRetryCountSet = true;
                IsSetDto.IsFailedCountSet = true;
                IsSetDto.IsTargetTypeSet = false;
                IsSetDto.IsConnectionStringSet = false;
                IsSetDto.IsFrequencySet = false;
                IsSetDto.IsIsActiveSet = false;
                IsSetDto.IsEntityIdSet = false;
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory(Constants.SyncDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ISyncTargetDal dal = dalContext.GetDal<ISyncTargetDal>())
                {
                    dalContext.Begin();
                    dal.DeleteById(ReadProperty<int>(IdProperty));
                    dalContext.Commit();
                }
            }
        }
        #endregion

        #endregion
    }
}