﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History : (CCM CCM800)

//// CCM800-26953 : I.George
////   Initial Version

//#endregion
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Galleria.Ccm.Resources.Language;

//namespace Galleria.Ccm.Model
//{
//    public enum  PerformanceSelectionMetricAggregationType
//    {
//        Sum = 0,
//        Avg = 1,
//        Min = 2,
//        Max = 3,
//        Count = 4
//    }

//    public static class PerformanceSelectionMetricAggregationTypeHelper
//    {
//        public static readonly Dictionary<PerformanceSelectionMetricAggregationType, String> FriendlyNames =
//            new Dictionary<PerformanceSelectionMetricAggregationType, String>()
//            {
//                {PerformanceSelectionMetricAggregationType.Sum,Message.Enum_PerformanceSelectionMetricAggregationType_Sum },
//                {PerformanceSelectionMetricAggregationType.Avg, Message.Enum_PerformanceSelectionMetricAggregationType_Avg},
//                {PerformanceSelectionMetricAggregationType.Min, Message.Enum_PerformanceSelectionMetricAggregationType_Min},
//                {PerformanceSelectionMetricAggregationType.Max, Message.Enum_PerformanceSelectionMetricAggregationType_Max},
//                {PerformanceSelectionMetricAggregationType.Count, Message.Enum_PerformanceSelectionMetricAggregationType_Count},
//            };
//    }
//}
