﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public enum SyncTargetFrequency
    {
        FifteenMinutes = 0,
        ThirtyMinutes = 1,
        OneHour = 2,
        TwoHours = 3,
        FourHours = 4,
        EightHours = 5,
        TwelveHours = 6,
        TwentyFourHours = 7,
        FortyEightHours = 8,
        FiveMinutes = 9,
        OneMinute = 10
    }

    public static class SyncTargetFrequencyHelper
    {
        public static readonly Dictionary<SyncTargetFrequency, string> FriendlyNames =
            new Dictionary<SyncTargetFrequency, string>()
            {
                {SyncTargetFrequency.FortyEightHours, Message.Enum_SyncTargetFrequency_FortyEightHours},
                {SyncTargetFrequency.TwentyFourHours, Message.Enum_SyncTargetFrequency_TwentyFourHours},
                {SyncTargetFrequency.TwelveHours, Message.Enum_SyncTargetFrequency_TwelveHours},
                {SyncTargetFrequency.EightHours, Message.Enum_SyncTargetFrequency_EightHours},
                {SyncTargetFrequency.FourHours, Message.Enum_SyncTargetFrequency_FourHours},
                {SyncTargetFrequency.TwoHours, Message.Enum_SyncTargetFrequency_TwoHours},
                {SyncTargetFrequency.OneHour, Message.Enum_SyncTargetFrequency_OneHour},
                {SyncTargetFrequency.ThirtyMinutes, Message.Enum_SyncTargetFrequency_ThirtyMinutes},
                {SyncTargetFrequency.FifteenMinutes, Message.Enum_SyncTargetFrequency_FifteenMinutes},
                {SyncTargetFrequency.FiveMinutes, Message.Enum_SyncTargetFrequency_FiveMinutes},
                {SyncTargetFrequency.OneMinute, Message.Enum_SyncTargetFrequency_OneMinute}
            };

        public static readonly Dictionary<SyncTargetFrequency, string> FriendlyDescriptions =
            new Dictionary<SyncTargetFrequency, string>()
            {
                {SyncTargetFrequency.FortyEightHours, Message.Enum_SyncTargetFrequency_FortyEightHours},
                {SyncTargetFrequency.TwentyFourHours, Message.Enum_SyncTargetFrequency_TwentyFourHours},
                {SyncTargetFrequency.TwelveHours, Message.Enum_SyncTargetFrequency_TwelveHours},
                {SyncTargetFrequency.EightHours, Message.Enum_SyncTargetFrequency_EightHours},
                {SyncTargetFrequency.FourHours, Message.Enum_SyncTargetFrequency_FourHours},
                {SyncTargetFrequency.TwoHours, Message.Enum_SyncTargetFrequency_TwoHours},
                {SyncTargetFrequency.OneHour, Message.Enum_SyncTargetFrequency_OneHour},
                {SyncTargetFrequency.ThirtyMinutes, Message.Enum_SyncTargetFrequency_ThirtyMinutes},
                {SyncTargetFrequency.FifteenMinutes, Message.Enum_SyncTargetFrequency_FifteenMinutes},
                {SyncTargetFrequency.FiveMinutes, Message.Enum_SyncTargetFrequency_FiveMinutes},
                {SyncTargetFrequency.OneMinute, Message.Enum_SyncTargetFrequency_OneMinute}
            };

        /// <summary>
        /// Returns the matching enum value from the specifed description
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static SyncTargetFrequency? SyncTargetFrequencyGetEnum(string description)
        {
            foreach (KeyValuePair<SyncTargetFrequency, string> keyPair in SyncTargetFrequencyHelper.FriendlyNames)
            {
                if (keyPair.Value.Equals(description))
                {
                    return keyPair.Key;
                }
            }
            return null;
        }


        /// <summary>
        /// Returns the matching enum value from the specifed enum
        /// Returns 0 if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static Int32 SyncTargetFrequencyGetValue(SyncTargetFrequency syncTargetFrequency)
        {
            switch (syncTargetFrequency)
            {
                case SyncTargetFrequency.OneMinute:
                    return 1;
                case SyncTargetFrequency.FiveMinutes:
                    return 5;
                case SyncTargetFrequency.FifteenMinutes:
                    return 15;
                case SyncTargetFrequency.ThirtyMinutes:
                    return 30;
                case SyncTargetFrequency.OneHour:
                    return 60;
                case SyncTargetFrequency.TwoHours:
                    return 120;
                case SyncTargetFrequency.FourHours:
                    return 240;
                case SyncTargetFrequency.EightHours:
                    return 480;
                case SyncTargetFrequency.TwelveHours:
                    return 720;
                case SyncTargetFrequency.TwentyFourHours:
                    return 1440;
                case SyncTargetFrequency.FortyEightHours:
                    return 2880;
            }

            return 240;
        }
    }
}