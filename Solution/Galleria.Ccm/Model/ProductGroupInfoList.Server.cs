﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//	Created
#endregion

#region Version History : CCM 802
// V8-29078 : A.Kuszyk
//  Added FetchByProductGroupCodes.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public partial class ProductGroupInfoList
    {
        #region Constructors
        private ProductGroupInfoList() { } // force the use of factory methods
        #endregion

        #region FactoryMethods

        /// <summary>
        /// Returns a list all deleted ProductGroupinfos for the given hierarchy id
        /// </summary>
        /// <param name="locationIds"></param>
        /// <returns></returns>
        public static ProductGroupInfoList FetchDeletedByProductHierarchyId(Int32 hierarchyId)
        {
            return DataPortal.Fetch<ProductGroupInfoList>(new FetchDeletedByProductHierarchyIdCriteria(hierarchyId));
        }

        /// <summary>
        /// Returns a list of all product group infos for the given ids
        /// </summary>
        /// <param name="locationIds"></param>
        /// <returns></returns>
        public static ProductGroupInfoList FetchByProductGroupIds(IEnumerable<Int32> productGroupIds)
        {
            return DataPortal.Fetch<ProductGroupInfoList>(new FetchByProductGroupIdsCriteria(productGroupIds));
        }
        
        /// <summary>
        /// Fetches a list of Product Group infos from the given list of Product Group codes.
        /// </summary>
        /// <param name="productGroupCodes"></param>
        /// <returns></returns>
        public static ProductGroupInfoList FetchByProductGroupCodes(IEnumerable<String> productGroupCodes)
        {
            return DataPortal.Fetch<ProductGroupInfoList>(new FetchByProductGroupCodesCriteria(productGroupCodes));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when fetching by location ids
        /// </summary>
        /// <param name="criteria"></param>
        private void DataPortal_Fetch(FetchDeletedByProductHierarchyIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductGroupInfoDal dal = dalContext.GetDal<IProductGroupInfoDal>())
                {
                    foreach (ProductGroupInfoDto dto in
                        dal.FetchDeletedByProductHierarchyId(criteria.ProductHierarchyId))
                    {
                        this.Add(ProductGroupInfo.Fetch(dalContext, dto));
                    }
                }
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when fetching by location ids
        /// </summary>
        /// <param name="criteria"></param>
        private void DataPortal_Fetch(FetchByProductGroupIdsCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductGroupInfoDal dal = dalContext.GetDal<IProductGroupInfoDal>())
                {
                    IEnumerable<ProductGroupInfoDto> dtoList = dal.FetchByProductGroupIds(criteria.ProductGroupIds);

                    foreach (ProductGroupInfoDto dto in dtoList)
                    {
                        this.Add(ProductGroupInfo.FetchProductGroupInfo(dalContext, dto));
                    }
                }
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when fetching by Product Group codes
        /// </summary>
        /// <param name="criteria"></param>
        private void DataPortal_Fetch(FetchByProductGroupCodesCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductGroupInfoDal dal = dalContext.GetDal<IProductGroupInfoDal>())
                {
                    IEnumerable<ProductGroupInfoDto> dtoList = dal.FetchByProductGroupCodes(criteria.ProductGroupCodes);

                    foreach (ProductGroupInfoDto dto in dtoList)
                    {
                        this.Add(ProductGroupInfo.FetchProductGroupInfo(dalContext, dto));
                    }
                }
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}
