﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#endregion

using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Server side implementation for <see cref="RenumberingStrategyInfo"/>.
    /// </summary>
    public partial class RenumberingStrategyInfo
    {
        #region Constructor

        /// <summary>
        ///     Private constructor. User Factory Methods to get new instances of <see cref="RenumberingStrategyInfo"/>.
        /// </summary>
        private RenumberingStrategyInfo() { }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Gets a new instance of <see cref="RenumberingStrategyInfo"/> from the provided <paramref name="dto"/>.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">The <see cref="RenumberingStrategyInfoDto"/> with the data to initalize the new <see cref="RenumberingStrategyInfo"/>.</param>
        /// <returns>A new instance of <see cref="RenumberingStrategyInfo"/> created from the provided <paramref name="dto"/>.</returns>
        internal static RenumberingStrategyInfo GetRenumberingStrategyInfo(IDalContext dalContext,
            RenumberingStrategyInfoDto dto)
        {
            return DataPortal.FetchChild<RenumberingStrategyInfo>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Loads the data from the given <paramref name="dto"/> into this instance.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">Instance of <see cref="RenumberingStrategyInfoDto"/> with the data to load.</param>
        private void LoadDataTransferObject(IDalContext dalContext, RenumberingStrategyInfoDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(EntityIdProperty, dto.EntityId);
            LoadProperty(NameProperty, dto.Name);
        }
        #endregion

        #region Fetch

        /// <summary>
        ///     The data portal invokes this method via reflection when loading this object from a dto.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">The dto to load from.</param>
        private void Child_Fetch(IDalContext dalContext, RenumberingStrategyInfoDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }


        #endregion

        #endregion
    }
}
