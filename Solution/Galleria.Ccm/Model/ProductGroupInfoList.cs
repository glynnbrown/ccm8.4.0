﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//	Created (Auto-generated)
#endregion

#region Version History : CCM 802
// V8-29078 : A.Kuszyk
//  Added FetchByProductGroupCodesCriteria.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// ProductGroup Model object
    /// </summary>
    [Serializable]
    public sealed partial class ProductGroupInfoList : ModelReadOnlyList<ProductGroupInfoList, ProductGroupInfo>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ProductGroupInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(ProductGroupInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ProductGroupInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ProductGroupInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Criteria


        #region FetchDeletedByProductHierarchyIdCriteria

        /// <summary>
        /// Criteria for the FetchByEntityId factory method
        /// </summary>
        [Serializable]
        public class FetchDeletedByProductHierarchyIdCriteria : CriteriaBase<FetchDeletedByProductHierarchyIdCriteria>
        {
            #region Properties

            #region ProductHierarchyId
            /// <summary>
            /// ProductHierarchyId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> ProductHierarchyIdProperty =
                RegisterProperty<Int32>(c => c.ProductHierarchyId);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Int32 ProductHierarchyId
            {
                get { return this.ReadProperty<Int32>(ProductHierarchyIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchDeletedByProductHierarchyIdCriteria(Int32 hierarchyId)
            {
                this.LoadProperty<Int32>(ProductHierarchyIdProperty, hierarchyId);
            }
            #endregion
        }

        #endregion

        #region FetchByProductGroupIdsCriteria 

        /// <summary>
        /// Criteria for ProductGroupInfoList.FetchByProductGroupIds
        /// </summary>
        [Serializable]
        public class FetchByProductGroupIdsCriteria : CriteriaBase<FetchByProductGroupIdsCriteria>
        {
            #region Properties

            public static PropertyInfo<IEnumerable<Int32>> ProductGroupIdsProperty =
                RegisterProperty<IEnumerable<Int32>>(c => c.ProductGroupIds);
            public IEnumerable<Int32> ProductGroupIds
            {
                get { return ReadProperty<IEnumerable<Int32>>(ProductGroupIdsProperty); }
            }

            #endregion

            public FetchByProductGroupIdsCriteria(IEnumerable<Int32> locationIds)
            {
                LoadProperty<IEnumerable<Int32>>(ProductGroupIdsProperty, locationIds);
            }
        }

        #endregion

        #region FetchByProductGroupCodesCriteria

        /// <summary>
        /// Criteria for ProductGroupInfoList.FetchByProductGroupCodes
        /// </summary>
        [Serializable]
        public class FetchByProductGroupCodesCriteria : CriteriaBase<FetchByProductGroupCodesCriteria>
        {
            #region Properties

            public static PropertyInfo<IEnumerable<String>> ProductGroupCodesProperty =
                RegisterProperty<IEnumerable<String>>(c => c.ProductGroupCodes);
            public IEnumerable<String> ProductGroupCodes
            {
                get { return ReadProperty<IEnumerable<String>>(ProductGroupCodesProperty); }
            }

            #endregion

            public FetchByProductGroupCodesCriteria(IEnumerable<String> productGroupCodes)
            {
                LoadProperty<IEnumerable<String>>(ProductGroupCodesProperty, productGroupCodes);
            }
        }

        #endregion

        #endregion
    }
}
