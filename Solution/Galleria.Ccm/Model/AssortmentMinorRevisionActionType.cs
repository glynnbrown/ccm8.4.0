﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Assortment Minor Revision Action Types
    /// </summary>
    public enum AssortmentMinorRevisionActionType
    {
        List = 0,
        DeList = 1,
        AmendDistribution = 2,
        Replace = 3
    }

    /// <summary>
    /// Helper class to provide internationaalized strings for each ScenarioMinorRevisionActionType
    /// </summary>
    public static class AssortmentMinorRevisionActionTypeHelper
    {
        public static readonly Dictionary<AssortmentMinorRevisionActionType, String> FriendlyNames =
            new Dictionary<AssortmentMinorRevisionActionType, String>()
            {
                {AssortmentMinorRevisionActionType.List, Message.Enum_AssortmentMinorRevisionActionType_List},
                {AssortmentMinorRevisionActionType.DeList, Message.Enum_AssortmentMinorRevisionActionType_DeList},
                {AssortmentMinorRevisionActionType.AmendDistribution, Message.Enum_AssortmentMinorRevisionActionType_AmendDistribution},
                {AssortmentMinorRevisionActionType.Replace, Message.Enum_AssortmentMinorRevisionActionType_Replace}
            };
    }
}

