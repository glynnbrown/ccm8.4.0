﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0)
// V8-24700 : A.Silva 
//  Created.
// V8-24124 : A.Silva 
//  Added support for Default Column Layout for PlanogramProduct.
//  Corrected the default layout for Planogram Products which was incorrectly identified as a Planogram layout.
// V8-26351 : A.Silva 
//  Some refactoring to tidy up Custom Column Layout classes.
// V8-26472 : A.Probyn
//      Removed obsolete CustomColumn fields.
// V8-27266 : L.Ineson
//  Added SaveAsFile
// V8-27938 : N.Haywood
//  Added Data Sheets
#endregion

#region Version History: (CCM 8.3)
// V8-31542 : L.Ineson
//  Added more properties to support new datasheet functionality
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.IO;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Model for the custom column layouts in data grids.
    /// </summary>
    [Serializable]
    public sealed partial class CustomColumnLayout : ModelObject<CustomColumnLayout>, IDisposable
    {
        #region Properties

        #region File Extensions

        /// <summary>
        /// Returns the file extension to use for custom columns.
        /// </summary>
        public static String FileExtension
        {
            get { return Constants.CustomColumnLayoutFileExtension; }
        }

        /// <summary>
        /// Returns the file extension to use for datasheets.
        /// </summary>
        public static String DataSheetFileExtension
        {
            get { return Constants.DataSheetFileExtension; }
        }

        #endregion

        #region DalFactoryType

        /// <summary>
        ///  DalFactoryType property definition
        /// </summary>
        private static readonly ModelPropertyInfo<CustomColumnLayoutDalFactoryType> DalFactoryTypeProperty =
            RegisterModelProperty<CustomColumnLayoutDalFactoryType>(c => c.DalFactoryType);

        /// <summary>
        /// Returns the dal factory type
        /// </summary>
        public CustomColumnLayoutDalFactoryType DalFactoryType
        {
            get { return GetProperty(DalFactoryTypeProperty); }
        }

        #endregion

        #region DalFactoryName

        /// <summary>
        /// DalFactoryName property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> DalFactoryNameProperty =
            RegisterModelProperty<String>(c => c.DalFactoryName);

        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        protected override String DalFactoryName
        {
            get { return GetProperty(DalFactoryNameProperty); }
        }

        #endregion

        #region IsReadOnly

        /// <summary>
        /// IsReadOnly property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Boolean> IsReadOnlyProperty =
             RegisterModelProperty<Boolean>(c => c.IsReadOnly);

        /// <summary>
        /// Returns true if this model has been loaded as readonly.
        /// </summary>
        public Boolean IsReadOnly
        {
            get { return this.GetProperty<Boolean>(IsReadOnlyProperty); }
        }

        #endregion

        #region Id

        /// <summary>
        ///  Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<object> IdProperty = RegisterModelProperty<object>(c=> c.Id);

        /// <summary>
        ///     Gets or sets the unique id value
        /// </summary>
        public Object Id
        {
            get { return GetProperty(IdProperty); }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c=> c.Name, Message.Generic_Name);

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public String Name
        {
            get { return GetProperty(NameProperty); }
            set { SetProperty(NameProperty, value); }
        }

        #endregion

        #region Description

        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c=> c.Description, Message.CustomColumnLayout_Description);

        /// <summary>
        ///  Gets or sets the description
        /// </summary>
        public String Description
        {
            get { return GetProperty(DescriptionProperty); }
            set { SetProperty(DescriptionProperty, value); }
        }

        #endregion

        #region Type

        /// <summary>
        /// Type property definition
        /// </summary>
        public static readonly ModelPropertyInfo<CustomColumnLayoutType> TypeProperty =
            RegisterModelProperty<CustomColumnLayoutType>(c=> c.Type, Message.CustomColumnLayout_Type);

        /// <summary>
        ///  Gets or sets the type of layout
        /// </summary>
        public CustomColumnLayoutType Type
        {
            get { return GetProperty(TypeProperty); }
            set { SetProperty(TypeProperty, value); }
        }

        #endregion

        #region Columns

        /// <summary>
        /// Columns property definition
        /// </summary>
        public static readonly ModelPropertyInfo<CustomColumnList> ColumnsProperty =
            RegisterModelProperty<CustomColumnList>(c=> c.Columns, RelationshipTypes.LazyLoad);

        /// <summary>
        ///   Gets the list of columns
        /// </summary>
        public CustomColumnList Columns
        {
            get
            {
                return GetPropertyLazy(ColumnsProperty,
                    new ModelList<CustomColumnList, CustomColumn>.FetchByParentIdCriteria(DalFactoryName, Id), true);
            }
        }

        #endregion

        #region Groups

        /// <summary>
        ///  Groups property definition
        /// </summary>
        public static readonly ModelPropertyInfo<CustomColumnGroupList> GroupListProperty =
            RegisterModelProperty<CustomColumnGroupList>(c=> c.GroupList, RelationshipTypes.LazyLoad);

        /// <summary>
        ///  Gets the list of column groups
        /// </summary>
        public CustomColumnGroupList GroupList
        {
            get
            {
                return GetPropertyLazy(GroupListProperty,
                    new ModelList<CustomColumnGroupList, CustomColumnGroup>.FetchByParentIdCriteria(DalFactoryName, Id),
                    true);
            }
        }

        #endregion

        #region SortList

        /// <summary>
        /// SortList property definition
        /// </summary>
        public static readonly ModelPropertyInfo<CustomColumnSortList> SortListProperty =
            RegisterModelProperty<CustomColumnSortList>(c=> c.SortList, RelationshipTypes.LazyLoad);

        /// <summary>
        ///  Gets the list of column sorts
        /// </summary>
        public CustomColumnSortList SortList
        {
            get
            {
                return GetPropertyLazy(SortListProperty,
                    new ModelList<CustomColumnSortList, CustomColumnSort>.FetchByParentIdCriteria(DalFactoryName, Id),
                    true);
            }
        }

        #endregion

        #region FilterList

        /// <summary>
        ///  FilterList property definition
        /// </summary>
        public static readonly ModelPropertyInfo<CustomColumnFilterList> FilterListProperty =
            RegisterModelProperty<CustomColumnFilterList>(c=> c.FilterList, RelationshipTypes.LazyLoad);

        /// <summary>
        ///  Gets the list of filters.
        /// </summary>
        public CustomColumnFilterList FilterList
        {
            get
            {
                return GetPropertyLazy(FilterListProperty,
                    new ModelList<CustomColumnFilterList, CustomColumnFilter>.FetchByParentIdCriteria(DalFactoryName, Id),
                    true);
            }
        }

        #endregion

        #region HasTotals

        /// <summary>
        ///  HasTotals property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasTotalsProperty =
            RegisterModelProperty<Boolean>(c=> c.HasTotals);

        /// <summary>
        /// Gets or sets whether to show total columns or not.
        /// </summary>
        public Boolean HasTotals
        {
            get { return GetProperty(HasTotalsProperty); }
            set { SetProperty(HasTotalsProperty, value); }
        }

        #endregion

        #region FrozenColumnCount

        /// <summary>
        ///  FrozenColumnCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> FrozenColumnCountProperty =
            RegisterModelProperty<Byte>(c=> c.FrozenColumnCount, Message.CustomColumnLayout_FrozenColumnCount);
        
        /// <summary>
        /// Gets/Sets the number of columns to be frozen.
        /// </summary>
        public Byte FrozenColumnCount
        {
            get { return GetProperty<Byte>(FrozenColumnCountProperty); }
            set { SetProperty<Byte>(FrozenColumnCountProperty, value); }
        }
        #endregion

        #region IsTitleShown

        /// <summary>
        ///  IsTitleShown property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsTitleShownProperty =
            RegisterModelProperty<Boolean>(c=> c.IsTitleShown, Message.CustomColumnLayout_IsTitleShown);

        /// <summary>
        /// Gets/Sets whether the data sheet title should be displayed.
        /// </summary>
        public Boolean IsTitleShown
        {
            get { return GetProperty<Boolean>(IsTitleShownProperty); }
            set { SetProperty<Boolean>(IsTitleShownProperty, value); }
        }

        #endregion

        #region IsGroupDetailHidden

        /// <summary>
        ///  IsGroupDetailHidden property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsGroupDetailHiddenProperty =
            RegisterModelProperty<Boolean>(c => c.IsGroupDetailHidden, Message.CustomColumnLayout_IsGroupDetailHidden);

        /// <summary>
        /// Gets/Sets whether group details should be collapsed to a single row.
        /// </summary>
        public Boolean IsGroupDetailHidden
        {
            get { return GetProperty<Boolean>(IsGroupDetailHiddenProperty); }
            set { SetProperty<Boolean>(IsGroupDetailHiddenProperty, value); }
        }

        #endregion

        #region IsGroupBlankRowShown

        /// <summary>
        ///  IsGroupBlankRowShown property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsGroupBlankRowShownProperty =
            RegisterModelProperty<Boolean>(c => c.IsGroupBlankRowShown, Message.CustomColumnLayout_IsGroupBlankRowShown);

        /// <summary>
        /// Gets/Sets whether a blank row should be added after each group
        /// </summary>
        public Boolean IsGroupBlankRowShown
        {
            get { return GetProperty<Boolean>(IsGroupBlankRowShownProperty); }
            set { SetProperty<Boolean>(IsGroupBlankRowShownProperty, value); }
        }

        #endregion

        #region IncludeAllPlanograms
        /// <summary>
        /// IncludeAllPlanograms property definition.
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IncludeAllPlanogramsProperty =
           RegisterModelProperty<Boolean>(c => c.IncludeAllPlanograms);
        /// <summary>
        /// Gets/Sets whether this datasheet should show data for all open planograms.
        /// </summary>
        public Boolean IncludeAllPlanograms
        {
            get { return GetProperty<Boolean>(IncludeAllPlanogramsProperty); }
            set { SetProperty<Boolean>(IncludeAllPlanogramsProperty, value); }
        }
        #endregion

        #region DataOrderType

        /// <summary>
        ///  DataOrderType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DataSheetDataOrderType> DataOrderTypeProperty =
            RegisterModelProperty<DataSheetDataOrderType>(c => c.DataOrderType, Message.CustomColumnLayout_DataOrderType);

        /// <summary>
        /// Gets/Sets how data should be ordered if the datasheet is displaying multiple planograms.
        /// </summary>
        public DataSheetDataOrderType DataOrderType
        {
            get { return GetProperty<DataSheetDataOrderType>(DataOrderTypeProperty); }
            set { SetProperty<DataSheetDataOrderType>(DataOrderTypeProperty, value); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Business Rules

        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="layoutType"></param>
        /// <returns></returns>
        public static CustomColumnLayout NewCustomColumnLayout(CustomColumnLayoutType layoutType)
        {
            var item = new CustomColumnLayout();
            item.Create(layoutType);
            return item;
        }

        /// <summary>
        ///  Creates a new instance of this type.
        /// </summary>
        /// <param name="layoutType">The type of custom column layout to be created.</param>
        /// <param name="customColumnLayoutFactory">Helper class for Custom Column Layouts.</param>
        /// <returns>A new initialized instance of <see cref="CustomColumnLayout" />.</returns>
        public static CustomColumnLayout NewCustomColumnLayout(CustomColumnLayoutType layoutType, IEnumerable<CustomColumn> columns)
        {
            var item = new CustomColumnLayout();
            item.Create(layoutType, columns);
            return item;
        }

        /// <summary>
        ///     Locks the file containing the <see cref="CustomColumnLayout" /> so that it <c>cannot</c> be edited or opened by
        ///     another process.
        /// </summary>
        /// <param name="fileName">Name of the file to unlock.</param>
        public static void LockCustomColumnLayoutByFileName(String fileName)
        {
            DataPortal.Execute(new LockCustomColumnLayoutCommand(CustomColumnLayoutDalFactoryType.FileSystem, fileName));
        }

        /// <summary>
        ///     Unlocks the file containing the <see cref="CustomColumnLayout" /> so that it <c>can</c> be edited or opened by
        ///     another process.
        /// </summary>
        /// <param name="fileName">Name of the file to unlock.</param>
        public static void UnlockCustomColumnLayoutByFileName(String fileName)
        {
            DataPortal.Execute(new UnlockCustomColumnLayoutCommand(CustomColumnLayoutDalFactoryType.FileSystem, fileName));
        }

        #endregion

        #region Data Access

        private void Create(
            CustomColumnLayoutType layoutType,
            IEnumerable<CustomColumn> columns = null)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());

            if (layoutType == CustomColumnLayoutType.DataSheet)
            {
                this.LoadProperty<String>(NameProperty, Message.CustomColumnLayout_DataSheetDefaultName);
                this.LoadProperty<String>(DescriptionProperty, Message.CustomColumnLayout_DataSheetDefaultDescription);
                this.LoadProperty<Boolean>(IsGroupBlankRowShownProperty, true);
            }
            else
            {
                this.LoadProperty<String>(NameProperty, Message.CustomColumnLayout_DefaultName);
                this.LoadProperty<String>(DescriptionProperty, Message.CustomColumnLayout_DefaultDescription);
            }

            this.LoadProperty<CustomColumnLayoutType>(TypeProperty, layoutType);

            this.LoadProperty<CustomColumnList>(ColumnsProperty, 
                (columns != null)? CustomColumnList.NewCustomColumnList(columns):CustomColumnList.NewCustomColumnList());
            
            this.LoadProperty<CustomColumnGroupList>(GroupListProperty, CustomColumnGroupList.NewCustomColumnGroupList());
            this.LoadProperty<CustomColumnSortList>(SortListProperty, CustomColumnSortList.NewCustomColumnSortList());
            this.LoadProperty<CustomColumnFilterList>(FilterListProperty, CustomColumnFilterList.NewCustomColumnFilterList());

            base.Create();
        }


        #endregion

        #region Criteria

        #region FetchByIdCriteria

        /// <summary>
        /// Criteria used for FetchById
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region DalFactoryType Property

            public static readonly PropertyInfo<CustomColumnLayoutDalFactoryType> DalFactoryTypeProperty =
                RegisterProperty<CustomColumnLayoutDalFactoryType>(c=> c.DalFactoryType);

            public CustomColumnLayoutDalFactoryType DalFactoryType
            {
                get { return ReadProperty(DalFactoryTypeProperty); }
            }

            #endregion

            #region Id Property

            public static readonly PropertyInfo<Object> IdProperty = RegisterProperty<Object>(c=> c.Id);

            public Object Id
            {
                get { return ReadProperty(IdProperty); }
            }

            #endregion

            #region AsReadOnly

            /// <summary>
            /// AsReadOnly property definition
            /// </summary>
            public static readonly PropertyInfo<Boolean> AsReadOnlyProperty =
                RegisterProperty<Boolean>(c => c.AsReadOnly);
            /// <summary>
            /// If true the model will be loaded as readonly.
            /// </summary>
            public Boolean AsReadOnly
            {
                get { return this.ReadProperty<Boolean>(AsReadOnlyProperty); }
            }
            #endregion

            #endregion

            #region Constructor

            public FetchByIdCriteria(CustomColumnLayoutDalFactoryType dalFactoryType, Object id)
            {
                LoadProperty(IdProperty, id);
                LoadProperty(DalFactoryTypeProperty, dalFactoryType);
                this.LoadProperty<Boolean>(AsReadOnlyProperty, false);
            }

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(CustomColumnLayoutDalFactoryType dalFactoryType, Object id, Boolean asReadOnly)
            {
                this.LoadProperty(IdProperty, id);
                this.LoadProperty(DalFactoryTypeProperty, dalFactoryType);
                this.LoadProperty<Boolean>(AsReadOnlyProperty, asReadOnly);
            }

            #endregion
        }

        #endregion

        #endregion

        #region Commands

        #region LockCustomColumnLayoutCommand

        /// <summary>
        ///     Performs the locking of the file containing a <see cref="CustomColumnLayout" />.
        /// </summary>
        [Serializable]
        private partial class LockCustomColumnLayoutCommand : CommandBase<LockCustomColumnLayoutCommand>
        {
            #region Properties

            #region DalFactoryType Property

            /// <summary>
            ///     <see cref="ModelPropertyInfo{T}" /> for the <see cref="DalFactoryType" /> property.
            /// </summary>
            public static readonly PropertyInfo<CustomColumnLayoutDalFactoryType> DalFactoryTypeProperty =
                RegisterProperty<CustomColumnLayoutDalFactoryType>(c=> c.DalFactoryType);

            /// <summary>
            ///     Gets the type of DAL factory used in this <see cref="LockCustomColumnLayoutCommand" /> instance.
            /// </summary>
            public CustomColumnLayoutDalFactoryType DalFactoryType
            {
                get { return ReadProperty(DalFactoryTypeProperty); }
            }

            #endregion

            #region Id Property

            /// <summary>
            ///     <see cref="ModelPropertyInfo{T}" /> for the <see cref="Id" /> property.
            /// </summary>
            public static readonly PropertyInfo<object> IdProperty = RegisterProperty<object>(c=> c.Id);

            /// <summary>
            ///     Gets the unique Id for this <see cref="LockCustomColumnLayoutCommand" /> instance.
            /// </summary>
            public Object Id
            {
                get { return ReadProperty(IdProperty); }
            }

            #endregion

            #endregion

            #region Constructor

            /// <summary>
            ///     Initializes and returns a new instance of a <see cref="LockCustomColumnLayoutCommand" /> object.
            /// </summary>
            /// <param name="dalType"></param>
            /// <param name="id"></param>
            public LockCustomColumnLayoutCommand(CustomColumnLayoutDalFactoryType dalType, Object id)
            {
                LoadProperty(DalFactoryTypeProperty, dalType);
                LoadProperty(IdProperty, id);
            }

            #endregion
        }

        #endregion

        #region UnlockCustomColumnLayoutCommand

        /// <summary>
        ///     Performs the unlocking of the file containing a <see cref="CustomColumnLayout" />.
        /// </summary>
        [Serializable]
        private partial class UnlockCustomColumnLayoutCommand : CommandBase<UnlockCustomColumnLayoutCommand>
        {
            #region Properties

            #region DalFactoryType Property

            /// <summary>
            ///     <see cref="ModelPropertyInfo{T}" /> for the <see cref="DalFactoryType" /> property.
            /// </summary>
            public static readonly PropertyInfo<CustomColumnLayoutDalFactoryType> DalFactoryTypeProperty =
                RegisterProperty<CustomColumnLayoutDalFactoryType>(c=> c.DalFactoryType);

            /// <summary>
            ///     Gets the type of DAL factory used in this <see cref="LockCustomColumnLayoutCommand" /> instance.
            /// </summary>
            public CustomColumnLayoutDalFactoryType DalFactoryType
            {
                get { return ReadProperty(DalFactoryTypeProperty); }
            }

            #endregion

            #region Id Property

            /// <summary>
            ///     <see cref="ModelPropertyInfo{T}" /> for the <see cref="Id" /> property.
            /// </summary>
            public static readonly PropertyInfo<object> IdProperty = RegisterProperty<object>(c=> c.Id);

            /// <summary>
            ///     Gets the unique Id for this <see cref="LockCustomColumnLayoutCommand" /> instance.
            /// </summary>
            public Object Id
            {
                get { return ReadProperty(IdProperty); }
            }

            #endregion

            #endregion

            #region Constructor

            /// <summary>
            ///     Initializes and returns a new instance of a <see cref="LockCustomColumnLayoutCommand" /> object.
            /// </summary>
            /// <param name="dalType"></param>
            /// <param name="id"></param>
            public UnlockCustomColumnLayoutCommand(CustomColumnLayoutDalFactoryType dalType, Object id)
            {
                LoadProperty(DalFactoryTypeProperty, dalType);
                LoadProperty(IdProperty, id);
            }

            #endregion
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// ToString override.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<CustomColumnLayout>(oldId, newId);
        }

        /// <summary>
        /// Saves this to a new file.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public CustomColumnLayout SaveAsFile(String path)
        {
            CustomColumnLayout returnValue = null;

            String fileName = path;

            //make sure the file extension is correct.
            if (this.Type != CustomColumnLayoutType.DataSheet)
            {
                fileName = Path.ChangeExtension(path, FileExtension);
            }
            else
            {
                fileName = Path.ChangeExtension(path, DataSheetFileExtension);
            }

            //if this model is not new and was fecthed from another file
            // take a note of the old file name.
            String oldFileName = null;
            if ((!IsNew) && (this.Id as String != fileName))
            {
                if (DalFactoryType == CustomColumnLayoutDalFactoryType.FileSystem)
                {
                    oldFileName = this.Id as String;
                }
            }

            //Mark this item as new.
            if (!this.IsNew) MarkGraphAsNew();

            LoadProperty<CustomColumnLayoutDalFactoryType>(DalFactoryTypeProperty, CustomColumnLayoutDalFactoryType.FileSystem);
            LoadProperty<Object>(IdProperty, fileName);
            this.Name = Path.GetFileNameWithoutExtension(fileName);
            returnValue = Save();

            //unlock the old file.
            if (!String.IsNullOrEmpty(oldFileName))
            {
                UnlockCustomColumnLayoutByFileName(oldFileName);
            }

            //V8-25873 : Make sure id is the file name for file system, dal is returning 1
            LoadProperty<Object>(IdProperty, fileName);
            return returnValue;
        }

        #endregion

        #region IDisposable support

        private Boolean _isDisposed;

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(Boolean disposing)
        {
            if (_isDisposed) return;
            if (disposing)
            {
                var fileName = Id as String;
                if (!String.IsNullOrEmpty(fileName))
                {
                    UnlockCustomColumnLayoutByFileName(fileName);
                }
            }

            _isDisposed = true;
        }

        #endregion
    }

    #region CustomColumnLayoutDalFactoryType

    /// <summary>
    ///  Types of DAL factories available to <see cref="CustomColumnLayout" />.
    /// </summary>
    public enum CustomColumnLayoutDalFactoryType
    {
        Unknown,
        FileSystem
    }

    #endregion

    #region DataSheetDataOrderType

    /// <summary>
    /// Denotes the different ways the datasheet can 
    /// order columns when displaying data for multiple planograms.
    /// </summary>
    public enum DataSheetDataOrderType
    {
        ByPlanogram = 0,
        ByAttribute = 1,
    }

    public static class DataSheetDataOrderTypeHelper
    {
        public static readonly Dictionary<DataSheetDataOrderType, String> FriendlyNames =
            new Dictionary<DataSheetDataOrderType, String>()
            {
                {DataSheetDataOrderType.ByPlanogram, Message.Enum_DataSheetDataOrderType_ByPlanogram},
                {DataSheetDataOrderType.ByAttribute, Message.Enum_DataSheetDataOrderType_ByAttribute},
            };
    }

    #endregion
}