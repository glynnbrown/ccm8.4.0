﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630: A.Kuszyk
//  Created (copied from SA).
// V8-26560 : A.Probyn
//  Added FetchByProductUniverseIds
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;
using Csla.Data;

using Galleria.Framework.Dal;

using Galleria.Ccm.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// ProductUniverseInfoList
    /// </summary>
    public partial class ProductUniverseInfoList
    {
        #region Constructors
        private ProductUniverseInfoList() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all items for the given entity id
        /// </summary>
        /// <returns>A list of entity infos</returns>
        public static ProductUniverseInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<ProductUniverseInfoList>(new FetchByEntityIdCriteria(entityId));
        }

        /// <summary>
        /// Returns a list of all product universe infos for the given ids
        /// </summary>
        /// <param name="locationIds"></param>
        /// <returns></returns>
        public static ProductUniverseInfoList FetchByProductUniverseIds(IEnumerable<Int32> productUniverseIds)
        {
            return DataPortal.Fetch<ProductUniverseInfoList>(new FetchByProductUniverseIdsCriteria(productUniverseIds));
        }

        /// <summary>
        /// Returns all items for the product group id
        /// </summary>
        /// <returns>A list of entity infos</returns>
        public static ProductUniverseInfoList FetchByProductGroupId(Int32 productGroupId)
        {
            return DataPortal.Fetch<ProductUniverseInfoList>(new FetchByProductGroupIdCriteria(productGroupId));
        }

        #endregion

        #region Data Access
        /// <summary>
        /// Called when retrieving a list of all entity infos
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductUniverseInfoDal dal = dalContext.GetDal<IProductUniverseInfoDal>())
                {
                    IEnumerable<ProductUniverseInfoDto> dtoList = dal.FetchByEntityId(criteria.EntityId);
                    foreach (ProductUniverseInfoDto dto in dtoList)
                    {
                        this.Add(ProductUniverseInfo.FetchProductUniverseInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when retrieving a list of all entity infos
        /// </summary>
        private void DataPortal_Fetch(FetchByProductUniverseIdsCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductUniverseInfoDal dal = dalContext.GetDal<IProductUniverseInfoDal>())
                {
                    IEnumerable<ProductUniverseInfoDto> dtoList = dal.FetchByProductUniverseIds(criteria.ProductUniverseIds);
                    foreach (ProductUniverseInfoDto dto in dtoList)
                    {
                        this.Add(ProductUniverseInfo.FetchProductUniverseInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }
        

        /// <summary>
        /// Called when retrieving a list of all product group ids
        /// </summary>
        private void DataPortal_Fetch(FetchByProductGroupIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductUniverseInfoDal dal = dalContext.GetDal<IProductUniverseInfoDal>())
                {
                    IEnumerable<ProductUniverseInfoDto> dtoList = dal.FetchByProductGroupId(criteria.ProductGroupId);
                    foreach (ProductUniverseInfoDto dto in dtoList)
                    {
                        this.Add(ProductUniverseInfo.FetchProductUniverseInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion
    }
}
