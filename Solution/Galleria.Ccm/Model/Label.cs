﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24265 J.Pickup
//  Created
// CCM-24265 : N.Haywood
//  Added more properties
// V8-26248 : L.Luong
//  Edited Dispose
// V8-27614 : L.Ineson
//  Added SaveAsFile method.
// Removed id setter
// V8-27940 : L.Luong
//  Added RowVersion, EntityId, DateCreated, DateModified  and DateDeleted 
// V8-28362 : L.Ineson
//  Added the ability to open a file as readonly.
#endregion
#region Version History: (CCM 8.0)
//V8-29526 J.Pickup
//  ShowOverImagesProperty is now true by deafult. 
#endregion
#region Version History: (CCM 8.03)
// V8-28662 : L.Ineson
//  Now implements IPlanogramLabel 
#endregion
#region Version History: (CCM 8.1.0)
// V8-30202 : M.Shelley
//  Added SaveAs method to explicitly save to the repository
#endregion
#region Version History CCM830
// V8-31699 : A.Heathcote
//  Changed DalfactoryType region to public from private (e.g. public LabelDalFactoryType)  
//  then changed them back again.
#endregion

#endregion

using System;
using System.IO;
using System.Windows;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using System.Collections.Generic;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{

    /// <summary>
    /// Model representing a label object.
    /// </summary>
    [Serializable]
    public sealed partial class Label : ModelObject<Label>, IDisposable, IPlanogramLabel
    {
        #region Private Fields

        /// <summary>
        /// For the IDisposable pattern.
        /// </summary>
        private Boolean _isDisposed;

        #endregion

        #region Static Constructor
        static Label()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets the file extension to use for labels.
        /// </summary>
        public static String FileExtension
        {
            get { return Constants.LabelFileExtension; }
        }

        #region DalFactoryType

        /// <summary>
        /// DalFactoryType property definition
        /// </summary>
        private static readonly ModelPropertyInfo<LabelDalFactoryType> DalFactoryTypeProperty =
            RegisterModelProperty<LabelDalFactoryType>(c => c.DalFactoryType);

        /// <summary>
        /// Returns the dal factory type
        /// </summary>
        private LabelDalFactoryType DalFactoryType
        {
            get { return this.GetProperty<LabelDalFactoryType>(DalFactoryTypeProperty); }
        }
        

        #endregion

        #region DalFactoryName
        /// <summary>
        /// DalFactoryName property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> DalFactoryNameProperty =
            RegisterModelProperty<String>(c => c.DalFactoryName);
        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        protected override string DalFactoryName
        {
            get { return this.GetProperty<String>(DalFactoryNameProperty); }
        }
        #endregion

        #region IsReadOnly

        private static readonly ModelPropertyInfo<Boolean> IsReadOnlyProperty =
             RegisterModelProperty<Boolean>(c => c.IsReadOnly);

        /// <summary>
        /// Returns true if this model has been loaded as readonly.
        /// </summary>
        public Boolean IsReadOnly
        {
            get { return this.GetProperty<Boolean>(IsReadOnlyProperty); }
        }

        #endregion

        #region Id
        private static readonly ModelPropertyInfo<Object> IdProperty =
            RegisterModelProperty<Object>(c => c.Id);
        /// <summary>
        /// The unique id
        /// </summary>
        public Object Id
        {
            get { return GetProperty<Object>(IdProperty); }
        }
        #endregion

        #region RowVersion

        /// <summary>
        ///     Metadata for the <see cref="RowVersion" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);

        /// <summary>
        ///     Gets the row version timestamp.
        /// </summary>
        private RowVersion RowVersion
        {
            get { return GetProperty(RowVersionProperty); }
        }

        #endregion

        #region EntityId

        /// <summary>
        ///     Metadata for the <see cref="EntityId" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<int>(c => c.EntityId);

        /// <summary>
        ///     Gets or sets the unique Id value for the <see cref="Entity" /> model this instance belongs to.
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty(EntityIdProperty); }
            set { SetProperty(EntityIdProperty, value); }
        }

        #endregion

        #region Name
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name, Message.Generic_Name);
        /// <summary>
        /// The setting name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region Type
        public static readonly ModelPropertyInfo<LabelType> TypeProperty =
            RegisterModelProperty<LabelType>(c => c.Type);
        /// <summary>
        /// The type of label setting this is
        /// e.g. product or fixture
        /// </summary>
        public LabelType Type
        {
            get { return GetProperty<LabelType>(TypeProperty); }
            set { SetProperty<LabelType>(TypeProperty, value); }
        }
        #endregion

        #region BackgroundColour
        public static readonly ModelPropertyInfo<Int32> BackgroundColourProperty =
            RegisterModelProperty<Int32>(c => c.BackgroundColour, Message.LabelSetting_BackgroundColour);
        /// <summary>
        /// The colour of the label background
        /// </summary>
        public Int32 BackgroundColour
        {
            get { return GetProperty<Int32>(BackgroundColourProperty); }
            set { SetProperty<Int32>(BackgroundColourProperty, value); }
        }
        #endregion

        #region BackgroundTransparency
        public static readonly ModelPropertyInfo<float> BackgroundTransparencyProperty =
            RegisterModelProperty<float>(c => c.BackgroundTransparency, Message.LabelSetting_BackgroundTransparency);
        /// <summary>
        /// The colour of the label background
        /// </summary>
        public float BackgroundTransparency
        {
            get { return GetProperty<float>(BackgroundTransparencyProperty); }
            set { SetProperty<float>(BackgroundTransparencyProperty, value); }
        }
        #endregion

        #region BorderColourProperty
        public static readonly ModelPropertyInfo<Int32> BorderColourProperty =
            RegisterModelProperty<Int32>(c => c.BorderColour, Message.LabelSetting_BorderColour);
        /// <summary>
        /// The colour label border brush
        /// </summary>
        public Int32 BorderColour
        {
            get { return GetProperty<Int32>(BorderColourProperty); }
            set { SetProperty<Int32>(BorderColourProperty, value); }
        }
        #endregion

        #region BorderThickness
        public static readonly ModelPropertyInfo<Int32> BorderThicknessProperty =
          RegisterModelProperty<Int32>(c => c.BorderThickness, Message.LabelSetting_BorderThickness);
        /// <summary>
        /// The thickness of the border
        /// </summary>
        public Int32 BorderThickness
        {
            get { return GetProperty<Int32>(BorderThicknessProperty); }
            set { SetProperty<Int32>(BorderThicknessProperty, value); }
        }
        #endregion

        #region Text
        public static readonly ModelPropertyInfo<String> TextProperty =
            RegisterModelProperty<String>(c => c.Text, Message.LabelSetting_Text);
        /// <summary>
        /// The label text which determines what the label should contain.
        /// </summary>
        public String Text
        {
            get { return GetProperty<String>(TextProperty); }
            set { SetProperty<String>(TextProperty, value); }
        }
        #endregion

        #region TextColour
        public static readonly ModelPropertyInfo<Int32> TextColourProperty =
            RegisterModelProperty<Int32>(c => c.TextColour, Message.LabelSetting_TextColor);
        /// <summary>
        /// The color of the text
        /// </summary>
        public Int32 TextColour
        {
            get { return GetProperty<Int32>(TextColourProperty); }
            set { SetProperty<Int32>(TextColourProperty, value); }
        }
        #endregion

        #region Font
        public static readonly ModelPropertyInfo<String> FontProperty =
            RegisterModelProperty<String>(c => c.Font, Message.LabelSetting_Font);
        /// <summary>
        /// The name of the text font
        /// </summary>
        public String Font
        {
            get { return GetProperty<String>(FontProperty); }
            set { SetProperty<String>(FontProperty, value); }
        }
        #endregion

        #region FontSize
        public static readonly ModelPropertyInfo<Int32> FontSizeProperty =
            RegisterModelProperty<Int32>(c => c.FontSize, Message.LabelSetting_FontSize);
        /// <summary>
        /// The font size
        /// </summary>
        public Int32 FontSize
        {
            get { return GetProperty<Int32>(FontSizeProperty); }
            set { SetProperty<Int32>(FontSizeProperty, value); }
        }
        #endregion

        #region TextBoxfontBold
        public static readonly ModelPropertyInfo<Boolean> TextBoxFontBoldProperty =
            RegisterModelProperty<Boolean>(c => c.TextBoxFontBold, Message.LabelSetting_TextBoxFontBold);
        /// <summary>
        /// Gets/Sets the textbox font Bold
        /// </summary>
        public Boolean TextBoxFontBold
        {
            get { return GetProperty<Boolean>(TextBoxFontBoldProperty); }
            set
            {
                SetProperty<Boolean>(TextBoxFontBoldProperty, value);
                OnPropertyChanged("ResolvedTextBoxFontWeight");
            }
        }
        #endregion

        #region TextBoxfontItalic
        public static readonly ModelPropertyInfo<Boolean> TextBoxFontItalicProperty =
            RegisterModelProperty<Boolean>(c => c.TextBoxFontItalic, Message.LabelSetting_TextBoxFontItalic);
        /// <summary>
        /// Gets/Sets the textbox font italic
        /// </summary>
        public Boolean TextBoxFontItalic
        {
            get { return GetProperty<Boolean>(TextBoxFontItalicProperty); }
            set
            {
                SetProperty<Boolean>(TextBoxFontItalicProperty, value);
                OnPropertyChanged("ResolvedTextBoxFontStyle");
            }
        }
        #endregion

        #region TextBoxFontUnderlined
        public static readonly ModelPropertyInfo<Boolean> TextBoxFontUnderlinedProperty =
            RegisterModelProperty<Boolean>(c => c.TextBoxFontUnderlined, Message.LabelSetting_TextBoxFontUnderlined);
        /// <summary>
        /// Gets/Sets the textbox font underlined
        /// </summary>
        public Boolean TextBoxFontUnderlined
        {
            get { return GetProperty<Boolean>(TextBoxFontUnderlinedProperty); }
            set
            {
                SetProperty<Boolean>(TextBoxFontUnderlinedProperty, value);
                OnPropertyChanged("ResolvedTextBoxFontDecoration");
            }
        }
        #endregion

        #region LabelHorizontalPlacement
        public static readonly ModelPropertyInfo<LabelHorizontalPlacementType> LabelHorizontalPlacementProperty =
            RegisterModelProperty<LabelHorizontalPlacementType>(c => c.LabelHorizontalPlacement, Message.LabelSetting_HorizontalPlacement);
        /// <summary>
        /// Gets/Sets the LabelHorizontalPlacement
        /// </summary>
        public LabelHorizontalPlacementType LabelHorizontalPlacement
        {
            get { return GetProperty<LabelHorizontalPlacementType>(LabelHorizontalPlacementProperty); }
            set
            {
                SetProperty<LabelHorizontalPlacementType>(LabelHorizontalPlacementProperty, value);
                OnPropertyChanged("LabelHorizontalPlacement");
            }
        }

        PlanogramLabelHorizontalAlignment IPlanogramLabel.LabelHorizontalPlacement
        {
            get { return GetProperty<LabelHorizontalPlacementType>(LabelHorizontalPlacementProperty).ToPlanogramLabelHorizontalAlignment(); }
        }

        #endregion

        #region LabelVeticalPlacement
        public static readonly ModelPropertyInfo<LabelVerticalPlacementType> LabelVerticalPlacementProperty =
            RegisterModelProperty<LabelVerticalPlacementType>(c => c.LabelVerticalPlacement, Message.LabelSetting_VerticalPlacement);
        /// <summary>
        /// Gets/Sets the LabelVerticalPlacement
        /// </summary>
        public LabelVerticalPlacementType LabelVerticalPlacement
        {
            get { return GetProperty<LabelVerticalPlacementType>(LabelVerticalPlacementProperty); }
            set
            {
                SetProperty<LabelVerticalPlacementType>(LabelVerticalPlacementProperty, value);
            }
        }

        PlanogramLabelVerticalAlignment IPlanogramLabel.LabelVerticalPlacement
        {
            get { return GetProperty<LabelVerticalPlacementType>(LabelVerticalPlacementProperty).ToPlanogramLabelVerticalAlignment(); }
        }

        #endregion

        #region TextDirection
        public static readonly ModelPropertyInfo<LabelTextDirectionType> TextDirectionProperty =
            RegisterModelProperty<LabelTextDirectionType>(c => c.TextDirection, Message.LabelSetting_TextDirection);
        /// <summary>
        /// Gets/Sets the textbox Text Direction
        /// </summary>
        public LabelTextDirectionType TextDirection
        {
            get { return GetProperty<LabelTextDirectionType>(TextDirectionProperty); }
            set { SetProperty<LabelTextDirectionType>(TextDirectionProperty, value); }
        }

        PlanogramLabelTextDirection IPlanogramLabel.TextDirection
        {
            get { return GetProperty<LabelTextDirectionType>(TextDirectionProperty).ToPlanogramLabelTextDirection(); }
        }

        #endregion

        #region TextBoxAlignment
        public static readonly ModelPropertyInfo<TextAlignment> TextBoxTextAlignmentProperty =
            RegisterModelProperty<TextAlignment>(c => c.TextBoxTextAlignment, Message.LabelSetting_TextBoxAlignment);
        /// <summary>
        /// Gets/Sets the textbox Text Alignment
        /// </summary>
        public TextAlignment TextBoxTextAlignment
        {
            get { return GetProperty<TextAlignment>(TextBoxTextAlignmentProperty); }
            set { SetProperty<TextAlignment>(TextBoxTextAlignmentProperty, value); }
        }

        PlanogramLabelTextAlignment IPlanogramLabel.TextBoxTextAlignment
        {
            get 
            { 
                TextAlignment value = GetProperty<TextAlignment>(TextBoxTextAlignmentProperty);
                switch(value)
                {
                    default:
                    case TextAlignment.Center: return PlanogramLabelTextAlignment.Center;
                    case TextAlignment.Justify: return PlanogramLabelTextAlignment.Justified;
                    case TextAlignment.Left: return PlanogramLabelTextAlignment.Left;
                    case TextAlignment.Right: return PlanogramLabelTextAlignment.Right;
                }
            }
        }

        #endregion

        #region TextWrapping
        public static readonly ModelPropertyInfo<LabelTextWrapping> TextWrappingProperty =
            RegisterModelProperty<LabelTextWrapping>(c => c.TextWrapping, Message.LabelSetting_TextWrapping);
        /// <summary>
        /// Gets/Sets the textbox Text Wrapping
        /// </summary>
        public LabelTextWrapping TextWrapping
        {
            get { return GetProperty<LabelTextWrapping>(TextWrappingProperty); }
            set { SetProperty<LabelTextWrapping>(TextWrappingProperty, value); }
        }

        PlanogramLabelTextWrapping IPlanogramLabel.TextWrapping
        {
            get { return GetProperty<LabelTextWrapping>(TextWrappingProperty).ToPlanogramLabelTextWrapping(); }
        }

        #endregion

        #region IsRotateToFitOn
        public static readonly ModelPropertyInfo<Boolean> IsRotateToFitOnProperty =
            RegisterModelProperty<Boolean>(c => c.IsRotateToFitOn, Message.LabelSetting_IsRotateToFitOn);
        /// <summary>
        /// If true, the text may rotate to fit within the label
        /// </summary>
        public Boolean IsRotateToFitOn
        {
            get { return GetProperty<Boolean>(IsRotateToFitOnProperty); }
            set { SetProperty<Boolean>(IsRotateToFitOnProperty, value); }
        }
        #endregion

        #region IsShrinkToFitOn
        public static readonly ModelPropertyInfo<Boolean> IsShrinkToFitOnProperty =
            RegisterModelProperty<Boolean>(c => c.IsShrinkToFitOn, Message.LabelSetting_IsShrinkToFitOn);
        /// <summary>
        /// If true, the text may shrink to fit within the label
        /// </summary>
        public Boolean IsShrinkToFitOn
        {
            get { return GetProperty<Boolean>(IsShrinkToFitOnProperty); }
            set { SetProperty<Boolean>(IsShrinkToFitOnProperty, value); }
        }
        #endregion

        #region ShowOverImages
        public static readonly ModelPropertyInfo<Boolean> ShowOverImagesProperty =
            RegisterModelProperty<Boolean>(c => c.ShowOverImages, Message.LabelSetting_ShowOverImages);
        /// <summary>
        /// If true, the text will show over images
        /// </summary>
        public Boolean ShowOverImages
        {
            get { return GetProperty<Boolean>(ShowOverImagesProperty); }
            set { SetProperty<Boolean>(ShowOverImagesProperty, value); }
        }
        #endregion

        #region ShowLabelPerFacing
        public static readonly ModelPropertyInfo<Boolean> ShowLabelPerFacingProperty =
            RegisterModelProperty<Boolean>(c => c.ShowLabelPerFacing, Message.LabelSetting_ShowLabelPerFacing);
        /// <summary>
        /// If true, the text will show over images
        /// </summary>
        public Boolean ShowLabelPerFacing
        {
            get { return GetProperty<Boolean>(ShowLabelPerFacingProperty); }
            set { SetProperty<Boolean>(ShowLabelPerFacingProperty, value); }
        }
        #endregion

        #region DateCreated

        /// <summary>
        ///     Metadata for the <see cref="DateCreated" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        ///     Gets or sets the date this instance was created originally.
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty(DateCreatedProperty); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        ///     Metadata for the <see cref="DateLastModified" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        ///     Gets or sets the last date this instance was modified (and persisted).
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty(DateLastModifiedProperty); }
        }

        #endregion

        #region DateDeleted

        /// <summary>
        ///     Metadata for the <see cref="DateDeleted" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);

        /// <summary>
        ///     Gets or sets the date this instance was marked for deletion.
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return GetProperty(DateDeletedProperty); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));
        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository

        /// <summary>
        /// Returns a dictionary of the current role permissions allocated to the user for this object type.
        /// </summary>
        /// <returns></returns>
        public static IDictionary<AuthorizationActions, Boolean> GetUserPermissions()
        {
            return new Dictionary<AuthorizationActions, Boolean>()
            {
                {AuthorizationActions.CreateObject, ApplicationContext.User.IsInRole(DomainPermission.LabelCreate.ToString())},
                {AuthorizationActions.GetObject, ApplicationContext.User.IsInRole(DomainPermission.LabelGet.ToString())},
                {AuthorizationActions.EditObject, ApplicationContext.User.IsInRole(DomainPermission.LabelEdit.ToString())},
                {AuthorizationActions.DeleteObject, ApplicationContext.User.IsInRole(DomainPermission.LabelDelete.ToString())},
            };
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Locks a label so that it cannot be
        /// edited or opened by another process
        /// </summary>
        public static void LockLabelByFileName(String fileName)
        {
            DataPortal.Execute<LockLabelCommand>(new LockLabelCommand(LabelDalFactoryType.FileSystem, fileName));
        }

        /// <summary>
        /// Unlocks a label
        /// </summary>
        public static void UnlockLabelByFileName(String fileName)
        {
            DataPortal.Execute<UnlockLabelCommand>(new UnlockLabelCommand(LabelDalFactoryType.FileSystem, fileName));
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <returns></returns>
        public static Label NewLabel(LabelType labelType)
        {
            Label item = new Label();
            item.Create(labelType);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <returns></returns>
        public static Label NewLabel(Int32 entityId, LabelType labelType)
        {
            Label item = new Label();
            item.Create(entityId, labelType);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(LabelType labelType)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());

            this.LoadProperty<Int32>(BackgroundColourProperty, -1);//White
            this.LoadProperty<Int32>(TextColourProperty, -16777216);//black
            this.LoadProperty<Int32>(BorderColourProperty, -16777216);//black
            this.LoadProperty<Int32>(BorderThicknessProperty, 1);
            this.LoadProperty<String>(FontProperty, "Arial");
            this.LoadProperty<Int32>(FontSizeProperty, 20);
            this.LoadProperty<Boolean>(IsRotateToFitOnProperty, true);
            this.LoadProperty<Boolean>(IsShrinkToFitOnProperty, true);
            this.LoadProperty<LabelType>(TypeProperty, labelType);

            this.LoadProperty<Boolean>(TextBoxFontBoldProperty, false);
            this.LoadProperty<Boolean>(TextBoxFontItalicProperty, false);
            this.LoadProperty<Boolean>(TextBoxFontUnderlinedProperty, false);
            this.LoadProperty<LabelHorizontalPlacementType>(LabelHorizontalPlacementProperty, LabelHorizontalPlacementType.Center);
            this.LoadProperty<LabelVerticalPlacementType>(LabelVerticalPlacementProperty, LabelVerticalPlacementType.Center);
            this.LoadProperty<TextAlignment>(TextBoxTextAlignmentProperty, TextAlignment.Center);
            this.LoadProperty<LabelTextDirectionType>(TextDirectionProperty, LabelTextDirectionType.Horizontal);
            this.LoadProperty<LabelTextWrapping>(TextWrappingProperty, LabelTextWrapping.Word);
            this.LoadProperty<float>(BackgroundTransparencyProperty, 1.0f);
            this.LoadProperty<Boolean>(ShowOverImagesProperty, true);
            this.LoadProperty<Boolean>(ShowLabelPerFacingProperty, false);
            this.LoadProperty(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty(DateLastModifiedProperty, DateTime.UtcNow);

            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 entityId, LabelType labelType)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);

            this.LoadProperty<Int32>(BackgroundColourProperty, -1);//White
            this.LoadProperty<Int32>(TextColourProperty, -16777216);//black
            this.LoadProperty<Int32>(BorderColourProperty, -16777216);//black
            this.LoadProperty<Int32>(BorderThicknessProperty, 1);
            this.LoadProperty<String>(FontProperty, "Arial");
            this.LoadProperty<Int32>(FontSizeProperty, 8);
            this.LoadProperty<Boolean>(IsRotateToFitOnProperty, true);
            this.LoadProperty<Boolean>(IsShrinkToFitOnProperty, true);
            this.LoadProperty<LabelType>(TypeProperty, labelType);

            this.LoadProperty<Boolean>(TextBoxFontBoldProperty, false);
            this.LoadProperty<Boolean>(TextBoxFontItalicProperty, false);
            this.LoadProperty<Boolean>(TextBoxFontUnderlinedProperty, false);
            this.LoadProperty<LabelHorizontalPlacementType>(LabelHorizontalPlacementProperty, LabelHorizontalPlacementType.Center);
            this.LoadProperty<LabelVerticalPlacementType>(LabelVerticalPlacementProperty, LabelVerticalPlacementType.Center);
            this.LoadProperty<TextAlignment>(TextBoxTextAlignmentProperty, TextAlignment.Center);
            this.LoadProperty<LabelTextDirectionType>(TextDirectionProperty, LabelTextDirectionType.Horizontal);
            this.LoadProperty<LabelTextWrapping>(TextWrappingProperty, LabelTextWrapping.Word);
            this.LoadProperty<float>(BackgroundTransparencyProperty, 1.0f);
            this.LoadProperty<Boolean>(ShowOverImagesProperty, true);
            this.LoadProperty<Boolean>(ShowLabelPerFacingProperty, false);
            this.LoadProperty(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty(DateLastModifiedProperty, DateTime.UtcNow);

            base.Create();
        }

        #endregion

        #endregion

        #region Criteria

        #region FetchByIdCriteria

        // <summary>
        /// Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region PackageType
            /// <summary>
            /// LabelDalType property definition
            /// </summary>
            public static readonly PropertyInfo<LabelDalFactoryType> LabelDalTypeProperty =
                RegisterProperty<LabelDalFactoryType>(c => c.LabelDalType);
            /// <summary>
            /// Returns the label dal type
            /// </summary>
            public LabelDalFactoryType LabelDalType
            {
                get { return this.ReadProperty<LabelDalFactoryType>(LabelDalTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #region AsReadOnly

            /// <summary>
            /// AsReadOnly property definition
            /// </summary>
            public static readonly PropertyInfo<Boolean> AsReadOnlyProperty =
                RegisterProperty<Boolean>(c => c.AsReadOnly);
            /// <summary>
            /// If true the model will be loaded as readonly.
            /// </summary>
            public Boolean AsReadOnly
            {
                get { return this.ReadProperty<Boolean>(AsReadOnlyProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(LabelDalFactoryType type, Object id)
            {
                this.LoadProperty<LabelDalFactoryType>(LabelDalTypeProperty, type);
                this.LoadProperty<Object>(IdProperty, id);
                this.LoadProperty<Boolean>(AsReadOnlyProperty, false);
            }

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(LabelDalFactoryType type, Object id, Boolean asReadOnly)
            {
                this.LoadProperty<LabelDalFactoryType>(LabelDalTypeProperty, type);
                this.LoadProperty<Object>(IdProperty, id);
                this.LoadProperty<Boolean>(AsReadOnlyProperty, asReadOnly);
            }
            #endregion
        }

        #endregion

        #region FetchByEntityIdName Criteria

        [Serializable]
        public class FetchByEntityIdNameCriteria : CriteriaBase<FetchByEntityIdNameCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static readonly PropertyInfo<String> NameProperty =
                RegisterProperty<String>(c => c.Name);
            public String Name
            {
                get { return ReadProperty<String>(NameProperty); }
            }

            public FetchByEntityIdNameCriteria(Int32 entityId, String name)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<String>(NameProperty, name);
            }
        }

        #endregion

        #endregion

        #region Commands

        #region LockLabelCommand
        /// <summary>
        /// Performs the locking of a label
        /// </summary>
        [Serializable]
        private partial class LockLabelCommand : CommandBase<LockLabelCommand>
        {
            #region Properties

            #region LabelDalFactoryType
            /// <summary>
            /// LabelDalFactoryType property definition
            /// </summary>
            public static readonly PropertyInfo<LabelDalFactoryType> LabelDalFactoryTypeProperty =
                RegisterProperty<LabelDalFactoryType>(c => c.LabelDalFactoryType);
            /// <summary>
            /// Returns the label dal factory type
            /// </summary>
            public LabelDalFactoryType LabelDalFactoryType
            {
                get { return this.ReadProperty<LabelDalFactoryType>(LabelDalFactoryTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public LockLabelCommand(LabelDalFactoryType dalType, Object id)
            {
                this.LoadProperty<LabelDalFactoryType>(LabelDalFactoryTypeProperty, dalType);
                this.LoadProperty<Object>(IdProperty, id);
            }
            #endregion
        }
        #endregion

        #region UnlockLabelCommand
        /// <summary>
        /// Performs the unlocking of a label
        /// </summary>
        [Serializable]
        private partial class UnlockLabelCommand : CommandBase<UnlockLabelCommand>
        {
            #region Properties

            #region LabelDalFactoryType
            /// <summary>
            /// SourceType property definition
            /// </summary>
            public static readonly PropertyInfo<LabelDalFactoryType> LabelDalFactoryTypeProperty =
                RegisterProperty<LabelDalFactoryType>(c => c.LabelDalFactoryType);
            /// <summary>
            /// Returns the label source type
            /// </summary>
            public LabelDalFactoryType LabelDalFactoryType
            {
                get { return this.ReadProperty<LabelDalFactoryType>(LabelDalFactoryTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public UnlockLabelCommand(LabelDalFactoryType sourceType, Object id)
            {
                this.LoadProperty<LabelDalFactoryType>(LabelDalFactoryTypeProperty, sourceType);
                this.LoadProperty<Object>(IdProperty, id);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();

            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<Label>(oldId, newId);
        }

        /// <summary>
        /// ToString override.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Provide a SaveAs method to allow saving to the repository.
        /// This resets DalFactoryType and DalFactoryName to allow saving saving to the repository,
        /// otherwise a Highlight instance loaded from a file tries to save back to a file.
        /// </summary>
        /// <returns></returns>
        public Label SaveAs()
        {
            this.LoadProperty<LabelDalFactoryType>(DalFactoryTypeProperty, LabelDalFactoryType.Unknown);
            this.LoadProperty<String>(DalFactoryNameProperty, DalContainer.DalName);

            return this.Save();
        }

        /// <summary>
        /// Saves this to a new file.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public Label SaveAsFile(String path)
        {
            Label returnValue = null;
            String fileName = path;

            //make sure the file extension is correct.
            fileName = Path.ChangeExtension(path, FileExtension);

            // if this model is not new and was fetched from another file
            // take a note of the old file name.
            String oldFileName = null;
            if ((!IsNew) && (this.Id as String != fileName))
            {
                if (DalFactoryType == LabelDalFactoryType.FileSystem)
                {
                    oldFileName = this.Id as String;
                }
                MarkGraphAsNew();
            }

            LoadProperty<LabelDalFactoryType>(DalFactoryTypeProperty, LabelDalFactoryType.FileSystem);
            LoadProperty<String>(DalFactoryNameProperty, "");
            LoadProperty<Object>(IdProperty, fileName);
            this.Name = Path.GetFileNameWithoutExtension(fileName);
            returnValue = Save();

            //unlock the old file.
            if (!String.IsNullOrEmpty(oldFileName))
            {
                UnlockLabelByFileName(oldFileName);
            }

            //V8-25873 : Make sure id is the file name for file system, dal is returning 1
            LoadProperty<Object>(IdProperty, fileName);
            return returnValue;
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    if (!IsNew)
                    {
                        if (Id is String)
                        {
                            UnlockLabelByFileName((String)Id);
                            LoadProperty<Boolean>(IsReadOnlyProperty, true);
                        }
                    }
                }

                _isDisposed = true;
            }
        }

        #endregion

    }
}
