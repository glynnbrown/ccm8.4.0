﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentProductBuddyList
    {
        #region Constructor
        private AssortmentProductBuddyList() { } // Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a list of existing items based on their parent assortment
        /// </summary>
        /// <returns>A list of existing it</returns>
        internal static AssortmentProductBuddyList GetAssortmentProductBuddyList(IDalContext dalContext, Int32 assortmentId)
        {
            return DataPortal.FetchChild<AssortmentProductBuddyList>(dalContext, assortmentId);
        }

        /// <summary>
        /// Returns all local assortment product items for the given assortment
        /// </summary>
        /// <returns>A list of assortment local products for an assortment</returns>
        public static AssortmentProductBuddyList FetchByAssortmentId(Int32 assortmentId)
        {
            return DataPortal.Fetch<AssortmentProductBuddyList>(assortmentId);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="assortmentId">The parent assortment id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 assortmentId)
        {
            RaiseListChangedEvents = false;
            using (IAssortmentProductBuddyDal dal = dalContext.GetDal<IAssortmentProductBuddyDal>())
            {
                IEnumerable<AssortmentProductBuddyDto> dtoList = dal.FetchByAssortmentId(assortmentId);
                foreach (AssortmentProductBuddyDto dto in dtoList)
                {
                    this.Add(AssortmentProductBuddy.GetAssortmentProductBuddy(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when retrieving a list of all local products for assortment
        /// </summary>
        private void DataPortal_Fetch(Int32 assortmentId)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IAssortmentProductBuddyDal dal = dalContext.GetDal<IAssortmentProductBuddyDal>())
                {
                    IEnumerable<AssortmentProductBuddyDto> dtoList = dal.FetchByAssortmentId(assortmentId);
                    foreach (AssortmentProductBuddyDto dto in dtoList)
                    {
                        this.Add(AssortmentProductBuddy.GetAssortmentProductBuddy(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}