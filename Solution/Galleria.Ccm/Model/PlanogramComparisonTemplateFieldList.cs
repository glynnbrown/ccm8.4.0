﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Model representing a collection of <see cref="PlanogramComparisonTemplateField"/> objects.
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramComparisonTemplateFieldList : ModelList<PlanogramComparisonTemplateFieldList, PlanogramComparisonTemplateField>
    {
        #region Properties

        #region Parent

        /// <summary>
        ///     Get a reference to the containing <see cref="PlanogramComparisonTemplate"/>.
        /// </summary>
        public new PlanogramComparisonTemplate Parent { get { return (PlanogramComparisonTemplate) base.Parent; } }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Create a new instance of <see cref="PlanogramComparisonTemplateFieldList"/>.
        /// </summary>
        public static PlanogramComparisonTemplateFieldList NewPlanogramComparisonTemplateFieldList()
        {
            var item = new PlanogramComparisonTemplateFieldList();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        ///     Initialize all default property values for this instance.
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        public void AddRange(IEnumerable<IPlanogramComparisonSettingsField> sourceItems)
        {
            base.AddRange(sourceItems.Select(PlanogramComparisonTemplateField.NewPlanogramComparisonTemplateField));
        }

        public void AddRange(PlanogramItemType itemType, IEnumerable<IPlanogramComparisonSettingsField> sourceItem)
        {
            base.AddRange(sourceItem.Select(field => PlanogramComparisonTemplateField.NewPlanogramComparisonTemplateField(itemType, field)));
        }

        /// <summary>
        ///     Add a collection of <see cref="PlanogramComparisonTemplateField"/> derived from an enumeration of <see cref="ObjectFieldInfo"/>.
        /// </summary>
        /// <param name="itemType">The <see cref="PlanogramItemType"/> that this range should be targeting.</param>
        /// <param name="range">The enumeration of <see cref="ObjectFieldInfo"/> from which to derive the new items.</param>
        public void AddRange(PlanogramItemType itemType, Dictionary<ObjectFieldInfo, Boolean> range)
        {
            base.AddRange(range.Select(info => PlanogramComparisonTemplateField.NewPlanogramComparisonTemplateField(itemType, info.Key, info.Value)));
        }

        /// <summary>
        ///     Add a new <see cref="PlanogramComparisonTemplateField"/> derived from a <see cref="ObjectFieldInfo"/>.
        /// </summary>
        /// <param name="itemType">The <see cref="PlanogramItemType"/> that this item should be targeting.</param>
        /// <param name="item">The instance of <see cref="ObjectFieldInfo"/> from which to derive the new item.</param>
        /// <param name="display"></param>
        public PlanogramComparisonTemplateField Add(PlanogramItemType itemType, ObjectFieldInfo item, Boolean display)
        {
            PlanogramComparisonTemplateField newItem = PlanogramComparisonTemplateField.NewPlanogramComparisonTemplateField(itemType, item, display);
            Add(newItem);
            return newItem;
        }

        /// <summary>
        ///     Enumerate all fields that are of the given <paramref name="planItemType" />.
        /// </summary>
        /// <param name="planItemType">Type of planogram item that the comparison field needs to be of.</param>
        /// <returns>An enumeration of all contained fields that are of the given <paramref name="planItemType" />.</returns>
        public IEnumerable<PlanogramComparisonTemplateField> OfItemType(PlanogramItemType planItemType)
        {
            return this.Where(field => field.ItemType == planItemType);
        }

        #endregion
    }
}