﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    [Serializable, DefaultNewMethod("NewPlanogramComparisonTemplate", "EntityId")]
    public partial class EntityComparisonAttribute : ModelObject<EntityComparisonAttribute>
    {

        #region Parent

        ///// <summary>
        /////     <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="Parent" /> property.
        ///// </summary>
        //public static readonly ModelPropertyInfo<Entity> ParentProperty =
        //    RegisterModelProperty<Entity>(c => c.Parent);

        public new Entity Parent
        {
            get
            {
                var currentParent = ((EntityComparisonAttributeList) base.Parent);

               return currentParent.Parent;
            }  
        }

        #endregion

        #region Id

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="Id" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);

        /// <summary>
        ///     Get the Id for this instance in the data base.
        /// </summary>
        public Int32 Id => GetProperty(IdProperty);

        #endregion

        #region PropertyName

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="PropertyName" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> PropertyNameProperty =
            RegisterModelProperty<String>(c => c.PropertyName);

        /// <summary>
        /// Gets or sets the Property Name
        /// </summary>
        public String PropertyName
        {
            get { return this.ReadProperty<String>(PropertyNameProperty); }
            set { this.SetProperty<String>(PropertyNameProperty, value); }
        }

        #endregion

        #region PropertyDisplayName

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="PropertyDisplayName" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<String> PropertyDisplayNameProperty =
            RegisterModelProperty<String>(c => c.PropertyDisplayName);

        /// <summary>
        /// Gets or sets the Property Display Name
        /// </summary>
        public String PropertyDisplayName
        {
            get { return this.ReadProperty<String>(PropertyDisplayNameProperty); }
            set { this.SetProperty<String>(PropertyDisplayNameProperty, value); }
        }

        #endregion

        #region ItemType

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}" /> definition for the <see cref="ItemType" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramItemType> ItemTypeProperty =
            RegisterModelProperty<PlanogramItemType>(c => c.ItemType);

        /// <summary>
        ///     The <see cref="PlanogramItemType"/> that this instance's field is.
        /// </summary>
        public PlanogramItemType ItemType { get { return GetProperty(ItemTypeProperty); } set { SetProperty(ItemTypeProperty, value); } }

        #endregion

        /// <summary>
        /// Create a new instance of <see cref="EntityComparisonAttribute"/>>
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static EntityComparisonAttribute NewEntityComparisonAttribute(EntityComparisonAttribute attribute)
        {
            var item = new EntityComparisonAttribute();
            item.Create(attribute);
            return item;
        }

        public static EntityComparisonAttribute NewEntityComparisonAttribute(string propertyName, string propertyDisplayName, PlanogramItemType itemType)
        {
            var item = new EntityComparisonAttribute();
            item.Create(propertyName, propertyDisplayName, itemType);
            return item;
        }

       

        /// <summary>
        ///     Create a new instance of <see cref="EntityComparisonAttribute" />.
        /// </summary>
        public static EntityComparisonAttribute NewEntityComparisonAttribute()
        {
            var item = new EntityComparisonAttribute();
            item.Create();
            return item;
        }

        /// <summary>
        ///     Initialize all default property values for this instance.
        /// </summary>
        protected override void Create()
        {
            LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty<String>(PropertyNameProperty, string.Empty);
            LoadProperty<String>(PropertyDisplayNameProperty, string.Empty);
            LoadProperty<PlanogramItemType>(ItemTypeProperty, PlanogramItemType.Unknown);
            MarkAsChild();
            base.Create();
        }

        private void Create(EntityComparisonAttribute attribute)
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty(PropertyNameProperty, attribute.PropertyName);
            LoadProperty(PropertyDisplayNameProperty, attribute.PropertyDisplayName);
            LoadProperty(ItemTypeProperty, attribute.ItemType);
            MarkAsChild();
            base.Create();
        }

        private void Create(string propertyName, string propertyDisplayName, PlanogramItemType itemType)
        {
            LoadProperty(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty(PropertyNameProperty, propertyName);
            LoadProperty(PropertyDisplayNameProperty, propertyDisplayName);
            LoadProperty(ItemTypeProperty, itemType);
            MarkAsChild();
            base.Create();
        }

        #region Business Rules

        /// <summary>
        ///     Add business rules to this instance.
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            //BusinessRules.AddRule(new Required(PropertyNameProperty));
            //BusinessRules.AddRule(new MaxLength(PropertyNameProperty, 1000));
            //BusinessRules.AddRule(new MaxLength(PropertyDisplayNameProperty, 100));
        }

        #endregion

        #region Authorization Rules

        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository

        #endregion
    }
}
