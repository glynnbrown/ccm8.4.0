#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26367 : A.Silva ~ Added WorkPackage layout type.
// V8-27160 : L.Ineson
//  Removed workpackage layout type.
// V8-27938 : N.Haywood
//  Added Data Sheets
#endregion

#region Version History: (CCM 8.0.2)
// V8-29026 : D.Pleasance
//  Added Location
// V8-28986 : M.Shelley
//	Added PlanAssignment
#endregion

#region Version History: (CCM 8.1.1)
// V8-30357 : M.Brumby
//  Added LocationAndLocationSpace
#endregion

#region Version History: (CCM 8.2.0)
// V8-30991 : L.Ineson
//  Added PlanogramFixture and PlanogramAssembly.
#endregion

#region Version History: CCM830

// V8-31881 : A.Silva
//  Added Planogram Comparison.
// V8-32089 : N.Haywood
//  Added BlockingPlan
// V8-32718 : A.Probyn
//  Added AssortmentLocation & AssortmentProduct
// CCM-14014 : M.Pettit
//  Added PlanogramAssortmentProduct
#endregion

#endregion

namespace Galleria.Ccm.Model
{
    public enum CustomColumnLayoutType
    {
        Undefined,
        Planogram,
        Product,
        PlanogramComponent,
        DataSheet,
        Location,
        PlanAssignment,
        LocationAndLocationSpace,
        Assortment,
        PlanogramFixture,
        PlanogramAssembly,
        PlanogramComparison,
        BlockingPlan,
        PlanogramPosition,
        AssortmentProduct,
        AssortmentLocation,
        AssortmentProductMasterData,
        PlanogramAssortmentProduct
    }
}