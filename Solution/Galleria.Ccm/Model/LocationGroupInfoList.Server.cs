﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//	Created (Auto-generated)
// CCM-25444 : N.Haywood
//      Added FetchByLocationGroupIds
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    public partial class LocationGroupInfoList
    {
        #region Constructors
        private LocationGroupInfoList() { } // force the use of factory methods
        #endregion

        #region FactoryMethods

        /// <summary>
        /// Returns a list all deleted LocationGroupinfos for the given hierarchy id
        /// </summary>
        /// <param name="locationIds"></param>
        /// <returns></returns>
        public static LocationGroupInfoList FetchDeletedByLocationHierarchyId(Int32 hierarchyId)
        {
            return DataPortal.Fetch<LocationGroupInfoList>(new FetchDeletedByLocationHierarchyIdCriteria(hierarchyId));
        }

        /// <summary>
        /// Returns a list of all product group infos for the given ids
        /// </summary>
        /// <param name="locationIds"></param>
        /// <returns></returns>
        public static LocationGroupInfoList FetchByLocationGroupIds(IEnumerable<Int32> productGroupIds)
        {
            return DataPortal.Fetch<LocationGroupInfoList>(new FetchByLocationGroupIdsCriteria(productGroupIds));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when fetching by location ids
        /// </summary>
        /// <param name="criteria"></param>
        private void DataPortal_Fetch(FetchDeletedByLocationHierarchyIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationGroupInfoDal dal = dalContext.GetDal<ILocationGroupInfoDal>())
                {
                    foreach (LocationGroupInfoDto dto in 
                        dal.FetchDeletedByLocationHierarchyId(criteria.LocationHierarchyId))
                    {
                        this.Add(LocationGroupInfo.Fetch(dalContext, dto));
                    }
                }
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when fetching by location ids
        /// </summary>
        /// <param name="criteria"></param>
        private void DataPortal_Fetch(FetchByLocationGroupIdsCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationGroupInfoDal dal = dalContext.GetDal<ILocationGroupInfoDal>())
                {
                    IEnumerable<LocationGroupInfoDto> dtoList = dal.FetchByLocationGroupIds(criteria.LocationGroupIds);

                    foreach (LocationGroupInfoDto dto in dtoList)
                    {
                        this.Add(LocationGroupInfo.Fetch(dalContext, dto));
                    }
                }
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}
