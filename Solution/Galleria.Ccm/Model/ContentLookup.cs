﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26520 :J.Pickup
//	Created
#endregion
#region Version History: (CCM 801)
// CCM-28519 :J.Pickup
//	Removed Date Deleted
#endregion
#region Version History: (CCM 803)
// V8-29746 : A.Probyn
//  Updated to use linked data Id's as well as name
#endregion
#region Version History: (CCM 830)
// V8-31831 : A.Probyn
//  Updated to include PlanogramNameTemplate
// V8-32810 : M.Pettit
//  Added PrintTemplate_Id
#endregion

#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
     /// <summary>
    /// Model representing a ContentLookup object
    /// (Root Object)
    /// 
    /// A class representing an ContentLookup Item within CCM. 
    /// </summary>
    [Serializable]
    [DefaultNewMethod("NewContentLookup", "EntityId")]
    public sealed partial class ContentLookup : ModelObject<ContentLookup>
    {
        #region Static Constructor

        static ContentLookup()
        {
            FriendlyName = Message.ContentLookup;
        }

        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The Assortment Id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32> UserIdProperty =
            RegisterModelProperty<Int32>(c => c.UserId);
        /// <summary>
        /// The Assortment Id
        /// </summary>
        public Int32 UserId
        {
            get { return GetProperty<Int32>(UserIdProperty); }
            set { SetProperty<Int32>(UserIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        /// <summary>
        /// The entity id
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
        }

        public static readonly ModelPropertyInfo<String> PlanogramNameProperty =
            RegisterModelProperty<String>(c => c.PlanogramName);
        /// <summary>
        /// Assortments Name
        /// </summary>
        public String PlanogramName
        {
            get { return GetProperty<String>(PlanogramNameProperty); }
            set { SetProperty<String>(PlanogramNameProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> ProductUniverseNameProperty =
            RegisterModelProperty<String>(c => c.ProductUniverseName);
        /// <summary>
        /// Product Universe Name
        /// </summary>
        public String ProductUniverseName
        {
            get { return GetProperty<String>(ProductUniverseNameProperty); }
            set { SetProperty<String>(ProductUniverseNameProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> AssortmentNameProperty =
            RegisterModelProperty<String>(c => c.AssortmentName);
        /// <summary>
        /// Assortments Name
        /// </summary>
        public String AssortmentName
        {
            get { return GetProperty<String>(AssortmentNameProperty); }
            set { SetProperty<String>(AssortmentNameProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> BlockingNameProperty =
            RegisterModelProperty<String>(c => c.BlockingName);
        /// <summary>
        /// Blocking Name
        /// </summary>
        public String BlockingName
        {
            get { return GetProperty<String>(BlockingNameProperty); }
            set { SetProperty<String>(BlockingNameProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> SequenceNameProperty =
            RegisterModelProperty<String>(c => c.SequenceName);
        /// <summary>
        /// Sequence Name
        /// </summary>
        public String SequenceName
        {
            get { return GetProperty<String>(SequenceNameProperty); }
            set { SetProperty<String>(SequenceNameProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> MetricProfileNameProperty =
            RegisterModelProperty<String>(c => c.MetricProfileName);
        /// <summary>
        /// Metric Profile
        /// </summary>
        public String MetricProfileName
        {
            get { return GetProperty<String>(MetricProfileNameProperty); }
            set { SetProperty<String>(MetricProfileNameProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> InventoryProfileNameProperty =
            RegisterModelProperty<String>(c => c.InventoryProfileName);
        /// <summary>
        /// Sequence Name
        /// </summary>
        public String InventoryProfileName
        {
            get { return GetProperty<String>(InventoryProfileNameProperty); }
            set { SetProperty<String>(InventoryProfileNameProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> ConsumerDecisionTreeNameProperty =
            RegisterModelProperty<String>(c => c.ConsumerDecisionTreeName);
        /// <summary>
        /// Consumer Decision Tree Name
        /// </summary>
        public String ConsumerDecisionTreeName
        {
            get { return GetProperty<String>(ConsumerDecisionTreeNameProperty); }
            set { SetProperty<String>(ConsumerDecisionTreeNameProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> AssortmentMinorRevisionNameProperty =
            RegisterModelProperty<String>(c => c.AssortmentMinorRevisionName);
        /// <summary>
        /// Minor Assortment Name
        /// </summary>
        public String AssortmentMinorRevisionName
        {
            get { return GetProperty<String>(AssortmentMinorRevisionNameProperty); }
            set { SetProperty<String>(AssortmentMinorRevisionNameProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> PerformanceSelectionNameProperty =
            RegisterModelProperty<String>(c => c.PerformanceSelectionName);
        /// <summary>
        /// Performance Selection Name
        /// </summary>
        public String PerformanceSelectionName
        {
            get { return GetProperty<String>(PerformanceSelectionNameProperty); }
            set { SetProperty<String>(PerformanceSelectionNameProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> ClusterNameProperty =
          RegisterModelProperty<String>(c => c.ClusterName);
        /// <summary>
        /// Cluster Name
        /// </summary>
        public String ClusterName
        {
            get { return GetProperty<String>(ClusterNameProperty); }
            set { SetProperty<String>(ClusterNameProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> ClusterSchemeNameProperty =
           RegisterModelProperty<String>(c => c.ClusterSchemeName);
        /// <summary>
        /// Cluster Scheme Name
        /// </summary>
        public String ClusterSchemeName
        {
            get { return GetProperty<String>(ClusterSchemeNameProperty); }
            set { SetProperty<String>(ClusterSchemeNameProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> RenumberingStrategyNameProperty =
            RegisterModelProperty<String>(c => c.RenumberingStrategyName);
        /// <summary>
        /// Renumbering Strategy Name
        /// </summary>
        public String RenumberingStrategyName
        {
            get { return GetProperty<String>(RenumberingStrategyNameProperty); }
            set { SetProperty<String>(RenumberingStrategyNameProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> ValidationTemplateNameProperty =
           RegisterModelProperty<String>(c => c.ValidationTemplateName);
        /// <summary>
        /// Validation Template Name
        /// </summary>
        public String ValidationTemplateName
        {
            get { return GetProperty<String>(ValidationTemplateNameProperty); }
            set { SetProperty<String>(ValidationTemplateNameProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32> PlanogramIdProperty =
            RegisterModelProperty<Int32>(c => c.PlanogramId);
        /// <summary>
        /// Assortments Id
        /// </summary>
        public Int32 PlanogramId
        {
            get { return GetProperty<Int32>(PlanogramIdProperty); }
            set { SetProperty<Int32>(PlanogramIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32?> ProductUniverseIdProperty =
            RegisterModelProperty<Int32?>(c => c.ProductUniverseId);
        /// <summary>
        /// Product Universe Id
        /// </summary>
        public Int32? ProductUniverseId
        {
            get { return GetProperty<Int32?>(ProductUniverseIdProperty); }
            set { SetProperty<Int32?>(ProductUniverseIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32?> AssortmentIdProperty =
            RegisterModelProperty<Int32?>(c => c.AssortmentId);
        /// <summary>
        /// Assortments Id
        /// </summary>
        public Int32? AssortmentId
        {
            get { return GetProperty<Int32?>(AssortmentIdProperty); }
            set { SetProperty<Int32?>(AssortmentIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32?> BlockingIdProperty =
            RegisterModelProperty<Int32?>(c => c.BlockingId);
        /// <summary>
        /// Blocking Id
        /// </summary>
        public Int32? BlockingId
        {
            get { return GetProperty<Int32?>(BlockingIdProperty); }
            set { SetProperty<Int32?>(BlockingIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32?> SequenceIdProperty =
            RegisterModelProperty<Int32?>(c => c.SequenceId);
        /// <summary>
        /// Sequence Id
        /// </summary>
        public Int32? SequenceId
        {
            get { return GetProperty<Int32?>(SequenceIdProperty); }
            set { SetProperty<Int32?>(SequenceIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32?> MetricProfileIdProperty =
            RegisterModelProperty<Int32?>(c => c.MetricProfileId);
        /// <summary>
        /// Metric Profile
        /// </summary>
        public Int32? MetricProfileId
        {
            get { return GetProperty<Int32?>(MetricProfileIdProperty); }
            set { SetProperty<Int32?>(MetricProfileIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32?> InventoryProfileIdProperty =
            RegisterModelProperty<Int32?>(c => c.InventoryProfileId);
        /// <summary>
        /// Sequence Id
        /// </summary>
        public Int32? InventoryProfileId
        {
            get { return GetProperty<Int32?>(InventoryProfileIdProperty); }
            set { SetProperty<Int32?>(InventoryProfileIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32?> ConsumerDecisionTreeIdProperty =
            RegisterModelProperty<Int32?>(c => c.ConsumerDecisionTreeId);
        /// <summary>
        /// Consumer Decision Tree Id
        /// </summary>
        public Int32? ConsumerDecisionTreeId
        {
            get { return GetProperty<Int32?>(ConsumerDecisionTreeIdProperty); }
            set { SetProperty<Int32?>(ConsumerDecisionTreeIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32?> AssortmentMinorRevisionIdProperty =
            RegisterModelProperty<Int32?>(c => c.AssortmentMinorRevisionId);
        /// <summary>
        /// Minor Assortment Id
        /// </summary>
        public Int32? AssortmentMinorRevisionId
        {
            get { return GetProperty<Int32?>(AssortmentMinorRevisionIdProperty); }
            set { SetProperty<Int32?>(AssortmentMinorRevisionIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32?> PerformanceSelectionIdProperty =
            RegisterModelProperty<Int32?>(c => c.PerformanceSelectionId);
        /// <summary>
        /// Performance Selection Id
        /// </summary>
        public Int32? PerformanceSelectionId
        {
            get { return GetProperty<Int32?>(PerformanceSelectionIdProperty); }
            set { SetProperty<Int32?>(PerformanceSelectionIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32?> ClusterIdProperty =
          RegisterModelProperty<Int32?>(c => c.ClusterId);
        /// <summary>
        /// Cluster Id
        /// </summary>
        public Int32? ClusterId
        {
            get { return GetProperty<Int32?>(ClusterIdProperty); }
            set { SetProperty<Int32?>(ClusterIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32?> ClusterSchemeIdProperty =
           RegisterModelProperty<Int32?>(c => c.ClusterSchemeId);
        /// <summary>
        /// Cluster Scheme Id
        /// </summary>
        public Int32? ClusterSchemeId
        {
            get { return GetProperty<Int32?>(ClusterSchemeIdProperty); }
            set { SetProperty<Int32?>(ClusterSchemeIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32?> RenumberingStrategyIdProperty =
            RegisterModelProperty<Int32?>(c => c.RenumberingStrategyId);
        /// <summary>
        /// Renumbering Strategy Id
        /// </summary>
        public Int32? RenumberingStrategyId
        {
            get { return GetProperty<Int32?>(RenumberingStrategyIdProperty); }
            set { SetProperty<Int32?>(RenumberingStrategyIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32?> ValidationTemplateIdProperty =
           RegisterModelProperty<Int32?>(c => c.ValidationTemplateId);
        /// <summary>
        /// Validation Template Id
        /// </summary>
        public Int32? ValidationTemplateId
        {
            get { return GetProperty<Int32?>(ValidationTemplateIdProperty); }
            set { SetProperty<Int32?>(ValidationTemplateIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> PlanogramNameTemplateNameProperty =
           RegisterModelProperty<String>(c => c.PlanogramNameTemplateName);
        /// <summary>
        /// Planogram Name Template Name
        /// </summary>
        public String PlanogramNameTemplateName
        {
            get { return GetProperty<String>(PlanogramNameTemplateNameProperty); }
            set { SetProperty<String>(PlanogramNameTemplateNameProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32?> PlanogramNameTemplateIdProperty =
           RegisterModelProperty<Int32?>(c => c.PlanogramNameTemplateId);
        /// <summary>
        /// Planogram Name Template Id
        /// </summary>
        public Int32? PlanogramNameTemplateId
        {
            get { return GetProperty<Int32?>(PlanogramNameTemplateIdProperty); }
            set { SetProperty<Int32?>(PlanogramNameTemplateIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> PrintTemplateNameProperty =
           RegisterModelProperty<String>(c => c.PrintTemplateName);
        /// <summary>
        /// Print Template Name
        /// </summary>
        public String PrintTemplateName
        {
            get { return GetProperty<String>(PrintTemplateNameProperty); }
            set { SetProperty<String>(PrintTemplateNameProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32?> PrintTemplateIdProperty =
           RegisterModelProperty<Int32?>(c => c.PrintTemplateId);
        /// <summary>
        /// Print Template Id
        /// </summary>
        public Int32? PrintTemplateId
        {
            get { return GetProperty<Int32?>(PrintTemplateIdProperty); }
            set { SetProperty<Int32?>(PrintTemplateIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
           RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The row version timestamp
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }

        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);
        /// <summary>
        /// The Content Lookups created date.
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
        }


        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        /// <summary>
        /// The Content Lookups date last modified
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(PlanogramNameProperty));
            BusinessRules.AddRule(new Required(PlanogramIdProperty));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ContentLookup), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ContentLookupCreate.ToString()));
            BusinessRules.AddRule(typeof(ContentLookup), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(ContentLookup), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ContentLookupEdit.ToString()));
            BusinessRules.AddRule(typeof(ContentLookup), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ContentLookupDelete.ToString()));
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for DeleteById
        /// </summary>
        [Serializable]
        public class DeleteByContentLookupIdCriteria : Csla.CriteriaBase<DeleteByContentLookupIdCriteria>
        {
            public static readonly PropertyInfo<Int32> ContentLookupIdProperty =
            RegisterProperty<Int32>(c => c.ContentLookupId);
            public Int32 ContentLookupId
            {
                get { return ReadProperty<Int32>(ContentLookupIdProperty); }
            }

            public DeleteByContentLookupIdCriteria(Int32 contentLookupId)
            {
                LoadProperty<Int32>(ContentLookupIdProperty, contentLookupId);
            }
        }

        /// <summary>
        /// Criteria for FetchByPlanogramId
        /// </summary>
        [Serializable]
        public class FetchByPlanogramIdCriteria : Csla.CriteriaBase<FetchByPlanogramIdCriteria>
        {
            public static readonly PropertyInfo<Int32> PlanogramIdProperty =
            RegisterProperty<Int32>(c => c.PlanogramId);
            public Int32 PlanogramId
            {
                get { return ReadProperty<Int32>(PlanogramIdProperty); }
            }

            public FetchByPlanogramIdCriteria(Int32 planogramId)
            {
                LoadProperty<Int32>(PlanogramIdProperty, planogramId);
            }
        }
        
        #endregion

        #region FactoryMethods

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="entityId">The id of the entity to create for</param>
        /// <returns>The new assortment</returns>
        public static ContentLookup NewContentLookup(Int32 entityId)
        {
            ContentLookup item = new ContentLookup();
            item.Create(entityId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when this instance is being created
        /// </summary>
        private void Create(Int32 entityId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.LoadProperty<Int32>(UserIdProperty, User.GetCurrentUser().Id);
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            base.Create();
        }

        #endregion

        #endregion

        #region Override

        public override string ToString()
        {
            return this.PlanogramName;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<ClusterScheme>(oldId, newId);
        }

        #endregion
    }
}
