﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History:(CCM800)
//CCM-26158 : I.George
// Initial Version
#endregion

#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class MetricList : ModelList<MetricList, Metric>
    {
        #region Authorization
        /// <summary>
        /// Defines the authorization rule for this type
        /// </summary>
        public static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(MetricList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.MetricGet.ToString()));
            BusinessRules.AddRule(typeof(MetricList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.MetricCreate.ToString()));
            BusinessRules.AddRule(typeof(MetricList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.MetricEdit.ToString()));
            BusinessRules.AddRule(typeof(MetricList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.MetricDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// creates a new instance of this type
        /// </summary>
        /// <returns></returns>
        public static MetricList NewMetricList()
        {
            MetricList item = new MetricList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// caleed when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            base.Create();
        }
        #endregion

        #endregion

        #region Criteria
        /// <summary>
        /// Criteria for the Metric List FetchByEntityId
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : CriteriaBase<FetchByEntityIdCriteria>
        {
            #region Properties

            public static PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            #endregion

            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
            }
        }

        #endregion
    }
}
