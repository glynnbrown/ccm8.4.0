﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-26560 : A.Probyn
//		Created (Auto-generated)
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Sequence Types
    /// </summary>
    public enum SequenceType
    {
        Static = 0,
        Dynamic = 1
    }

    /// <summary>
    /// Helper class to provide internationaalized strings for each SequenceType
    /// </summary>
    public static class SequenceTypeHelper
    {
        public static readonly Dictionary<SequenceType, String> FriendlyNames =
            new Dictionary<SequenceType, String>()
            {
                {SequenceType.Static, Message.Enum_SequenceType_Static},
                {SequenceType.Dynamic, Message.Enum_SequenceType_Dynamic}
            };
    }
}

