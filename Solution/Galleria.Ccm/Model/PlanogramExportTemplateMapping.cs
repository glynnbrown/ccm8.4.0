﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a PlanogramExportTemplateMapping object.
    /// </summary>
    [Serializable]
    [DefaultNewMethod("NewPlanogramExportTemplateMapping", "Field", "FieldType")]
    public partial class PlanogramExportTemplateMapping : ModelObject<PlanogramExportTemplateMapping>
    {
        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PlanogramExportTemplate Parent
        {
            get
            {
                PlanogramExportTemplateMappingList parentList = base.Parent as PlanogramExportTemplateMappingList;
                if (parentList != null)
                {
                    return parentList.Parent as PlanogramExportTemplate;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique identifier for this item.
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region FieldType
        /// <summary>
        /// FieldType property definition.
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramFieldMappingType> FieldTypeProperty =
            RegisterModelProperty<PlanogramFieldMappingType>(c => c.FieldType);

        /// <summary>
        /// Gets/Sets the FieldType of field this mapping is for
        /// </summary>
        public PlanogramFieldMappingType FieldType
        {
            get { return this.GetProperty<PlanogramFieldMappingType>(FieldTypeProperty); }
            set { this.SetProperty<PlanogramFieldMappingType>(FieldTypeProperty, value); }
        }
        #endregion

        #region Field
        /// <summary>
        /// Field property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> FieldProperty =
            RegisterModelProperty<String>(c => c.Field);
        /// <summary>
        /// Gets/Sets the ccm field
        /// </summary>
        public String Field
        {
            get { return this.GetProperty<String>(FieldProperty); }
            set { this.SetProperty<String>(FieldProperty, value); }
        }
        #endregion

        #region ExternalField
        /// <summary>
        /// External Field property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ExternalFieldProperty =
            RegisterModelProperty<String>(c => c.ExternalField);
        /// <summary>
        /// Gets/Sets the external file field
        /// </summary>
        public String ExternalField
        {
            get { return this.GetProperty<String>(ExternalFieldProperty); }
            set { this.SetProperty<String>(ExternalFieldProperty, value); }
        }
        #endregion

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new MaxLength(FieldProperty, 1000));
            BusinessRules.AddRule(new MaxLength(ExternalFieldProperty, 1000));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramExportTemplateMapping NewPlanogramExportTemplateMapping(
            String field, PlanogramFieldMappingType fieldType)
        {
            PlanogramExportTemplateMapping item = new PlanogramExportTemplateMapping();
            item.Create(field, fieldType);
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramExportTemplateMapping NewPlanogramExportTemplateMapping(
            String field, PlanogramFieldMappingType fieldType, String externalField)
        {
            PlanogramExportTemplateMapping item = new PlanogramExportTemplateMapping();
            item.Create(field, fieldType, externalField);
            return item;
        }
        #endregion

        #region Data Access

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(String field, PlanogramFieldMappingType fieldType)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(FieldProperty, field);
            this.LoadProperty<PlanogramFieldMappingType>(FieldTypeProperty, fieldType);

            MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(String field, PlanogramFieldMappingType fieldType, String externalField)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(FieldProperty, field);
            this.LoadProperty<PlanogramFieldMappingType>(FieldTypeProperty, fieldType);
            this.LoadProperty<String>(ExternalFieldProperty, externalField);

            MarkAsChild();
            base.Create();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<PlanogramExportTemplateMapping>(oldId, newId);
        }

        #endregion
    }
}
