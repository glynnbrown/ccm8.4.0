﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26150 : L.Ineson
//	Created
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines the different CustomAttributeData parent types
    /// </summary>
    public enum CustomAttributeDataParentType : byte
    {
        //Planogram values
        Planogram = CustomAttributeDataPlanogramParentType.Planogram,
        PlanogramProduct = CustomAttributeDataPlanogramParentType.PlanogramProduct,
        PlanogramComponent = CustomAttributeDataPlanogramParentType.PlanogramComponent,
        
        //Others -
        Product = 100
    }
}
