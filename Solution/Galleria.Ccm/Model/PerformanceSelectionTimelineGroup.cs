﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26159 : L.Ineson
//	Created (Auto-generated)
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// PerformanceSelectionTimelineGroup Model object
    /// </summary>
    [Serializable]
    public sealed partial class PerformanceSelectionTimelineGroup : ModelObject<PerformanceSelectionTimelineGroup>
    {
        #region Static Constructor
        static PerformanceSelectionTimelineGroup()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PerformanceSelection Parent
        {
            get
            {
                PerformanceSelectionTimelineGroupList parentList = base.Parent as PerformanceSelectionTimelineGroupList;
                if (parentList != null)
                {
                    return parentList.Parent as PerformanceSelection;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region Code

        /// <summary>
        /// Code property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int64> CodeProperty =
            RegisterModelProperty<Int64>(c => c.Code);

        /// <summary>
        /// Gets/Sets the Code value
        /// </summary>
        public Int64 Code
        {
            get { return GetProperty<Int64>(CodeProperty); }
            set { SetProperty<Int64>(CodeProperty, value); }
        }

        #endregion

        #region DateCreated

        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        /// Gets/Sets the DateCreated value
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
            set { SetProperty<DateTime>(DateCreatedProperty, value); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        /// Gets/Sets the DateLastModified value
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
            set { SetProperty<DateTime>(DateLastModifiedProperty, value); }
        }

        #endregion

        #region DateDeleted

        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);

        /// <summary>
        /// Gets/Sets the DateDeleted value
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
            set { SetProperty<DateTime?>(DateDeletedProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PerformanceSelectionTimelineGroup), new IsInRole(AuthorizationActions.GetObject, DomainPermission.PerformanceSelectionGet.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelectionTimelineGroup), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.PerformanceSelectionCreate.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelectionTimelineGroup), new IsInRole(AuthorizationActions.EditObject, DomainPermission.PerformanceSelectionEdit.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelectionTimelineGroup), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.PerformanceSelectionDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static PerformanceSelectionTimelineGroup NewPerformanceSelectionTimelineGroup()
        {
            PerformanceSelectionTimelineGroup item = new PerformanceSelectionTimelineGroup();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static PerformanceSelectionTimelineGroup NewPerformanceSelectionTimelineGroup(Int64 code)
        {
            PerformanceSelectionTimelineGroup item = new PerformanceSelectionTimelineGroup();
            item.Create(code);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create(Int64 code)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int64>(CodeProperty, code);
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<PerformanceSelectionTimelineGroup>(oldId, newId);
        }
        #endregion
    }
}