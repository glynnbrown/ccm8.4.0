﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24779 : L.Luong
//  Created.

#endregion
#region Version History: (CCM 8.03)
// V8-29220 : L.Ineson
//Removed date deleted
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramImportTemplateInfo
    {
        #region Constructors

        private PlanogramImportTemplateInfo() { }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Request a <see cref="ValidationTemplateInfo"/> instance from its <see cref="dto"/>.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">The dto to load from.</param>
        /// <returns>A new instance of <see cref="ValidationTemplateInfo"/>.</returns>
        internal static PlanogramImportTemplateInfo FetchPlanogramImportTemplateInfo(IDalContext dalContext,
            PlanogramImportTemplateInfoDto dto)
        {
            return DataPortal.FetchChild<PlanogramImportTemplateInfo>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object

        private void LoadDataTransferObject(IDalContext dalContext, PlanogramImportTemplateInfoDto dto)
        {
            LoadProperty<Object>(IdProperty, dto.Id);
            LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            LoadProperty<String>(NameProperty, dto.Name);
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     The data portal invokes this method via reflection when loading this object from a dto.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">The dto to load from.</param>
        private void Child_Fetch(IDalContext dalContext, PlanogramImportTemplateInfoDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #endregion
    }
}
