﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
// CCM-24979 : L.Hodson
//      Added FixtureLibraryLocation property
//  V8-25395 : A.Probyn
//      Extended for new ProductLibraryLocation & LastUsedProductLibraryLocation
// V8-27155 : L.Luong
//      Added ProductPlacement and Default Dimensions & Colours Properties
// V8-27411 : M.Pettit
//  Added LastSuccessfulSyncDate and LastFailedSyncDate
#endregion
#region Version History: CCM803
// V8-28662 : L.Ineson
//  Added planogram image settings.
#endregion
#region Version History: CCM820
// V8-30936 : M.Brumby
//  Slotwall defaults
#endregion
#region Version History: CCM830
// V8-32626 : A.Probyn
//  Added Annotations
// V8-32591 : L.Ineson
//  Added textbox defaults.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Globalization;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Ccm.Model
{
    public partial class SystemSettings
    {
        #region Constants

        private const String _foundationServicesEndpoint = "Foundation Services Endpoint";
        private const String _compressionName = "Compression Name";
        private const String _lengthUnitOfMeasure = "Length Unit Of Measure";
        private const String _areaUnitOfMeasure = "Area Unit Of Measure";
        private const String _angleUnitOfMeasure = "Angle Unit Of Measure";
        private const String _volumeUnitOfMeasure = "Volume Unit Of Measure";
        private const String _weightUnitOfMeasure = "Weight Unit Of Measure";
        private const String _currencyUnitOfMeasure = "Currency Unit Of Measure";
        private const String _productPlacementX = "ProductPlacementX";
        private const String _productPlacementY = "ProductPlacementY";
        private const String _productPlacementZ = "ProductPlacementZ";
        private const String _productHeight = "ProductHeight";
        private const String _productWidth = "ProductWidth";
        private const String _productDepth = "ProductDepth";
        private const String _fixtureHeight = "FixtureHeight";
        private const String _fixtureWidth = "FixtureWidth";
        private const String _fixtureDepth = "FixtureDepth";
        private const String _fixtureHasBackboard = "FixtureHasBackboard";
        private const String _backboardDepth = "BackboardDepth";
        private const String _fixtureHasBase = "FixtureHasBase";
        private const String _baseHeight = "BaseHeight";
        private const String _shelfHeight = "ShelfHeight";
        private const String _shelfWidth = "ShelfWidth";
        private const String _shelfDepth = "ShelfDepth";
        private const String _shelfFillColour = "ShelfFillColour";
        private const String _pegboardHeight = "PegboardHeight";
        private const String _pegboardWidth = "PegboardWidth";
        private const String _pegboardDepth = "PegboardDepth";
        private const String _pegboardFillColour = "PegboardFillColour";
        private const String _chestHeight = "ChestHeight";
        private const String _chestWidth = "ChestWidth";
        private const String _chestDepth = "ChestDepth";
        private const String _chestFillColour = "ChestFillColour";
        private const String _barHeight = "BarHeight";
        private const String _barWidth = "BarWidth";
        private const String _barDepth = "BarDepth";
        private const String _barFillColour = "BarFillColour";
        private const String _rodHeight = "RodHeight";
        private const String _rodWidth = "RodWidth";
        private const String _rodDepth = "RodDepth";
        private const String _rodFillColour = "RodFillColour";
        private const String _clipStripHeight = "ClipStripHeight";
        private const String _clipStripWidth = "ClipstripWidth";
        private const String _clipStripDepth = "ClipStripDepth";
        private const String _clipStripFillColour = "ClipStripFillColour";
        private const String _palletHeight = "PalletHeight";
        private const String _palletWidth = "PalletWidth";
        private const String _palletDepth = "PalletDepth";
        private const String _palletFillColour = "PalletFillColour";
        private const String _slotwallHeight = "SlotwallHeight";
        private const String _slotwallWidth = "SlotwallWidth";
        private const String _slotwallDepth = "SlotwallDepth";
        private const String _slotwallFillColour = "SlotwallFillColour";
        private const String _lastSuccessfulSyncDate = "LastSuccessfulSyncDate";
        private const String _lastFailedSyncDate = "LastFailedSyncDate";
        private const String _productImageSource = "ProductImageSource";
        private const String _productImageLocation = "ProductImageLocation";
        private const String _productImageAttribute = "ProductImageAttribute";
        private const String _productImageIgnoreLeadingZeros = "ProductImageIgnoreLeadingZeros";
        private const String _productImageMinFieldLength = "ProductImageMinFieldLength";
        private const String _productImageFacingPostfixFront = "ProductImageFacingPostfixFront";
        private const String _productImageFacingPostfixLeft = "ProductImageFacingPostfixLeft";
        private const String _productImageFacingPostfixTop = "ProductImageFacingPostfixTop";
        private const String _productImageFacingPostfixBack = "ProductImageFacingPostfixBack";
        private const String _productImageFacingPostfixRight = "ProductImageFacingPostfixRight";
        private const String _productImageFacingPostfixBottom = "ProductImageFacingPostfixBottom";
        private const String _productImageCompression = "ProductImageCompression";
        private const String _productImageColourDepth = "ProductImageColourDepth";
        private const String _productImageTransparency = "ProductImageTransparency";
        private const String _fixtureLabel = "FixtureLabel";
        private const String _productLabel = "ProductLabel";
        private const String _highlight = "Highlight";
        private const String _positions = "Positions";
        private const String _positionUnits = "PositionUnits";
        private const String _productImages = "ProductImages";
        private const String _productShapes = "ProductShapes";
        private const String _productFillPatterns = "ProductFillPatterns";
        private const String _fixtureImages = "FixtureImages";
        private const String _fixtureFillPatterns = "FixtureFillPatterns";
        private const String _chestWalls = "ChestWalls";
        private const String _rotateTopDownComponents = "RotateTopDownComponents";
        private const String _shelfRisers = "ShelfRisers";
        private const String _notches = "Notches";
        private const String _pegHoles = "PegHoles";
        private const String _pegs = "Pegs";
        private const String _dividerLines = "DividerLines";
        private const String _dividers = "Dividers";
        private const String _annotations = "Annotations";
        private const String _textBoxFont = "TextBoxFont";
        private const String _textBoxFontSize = "TextBoxFontSize";
        private const String _textBoxCanReduceToFit = "TextBoxCanReduceToFit";
        private const String _textBoxFontColour = "TextBoxFontColour";
        private const String _textBoxBorderColour = "TextBoxBorderColour";
        private const String _textBoxBackgroundColour = "TextBoxBackgroundColour";
        private const String _textBoxBorderThickness = "TextBoxBorderThickness";

        #endregion

        #region Constructors
        private SystemSettings() { } //force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the SystemSettings items for the current user
        /// </summary>
        /// <param name="entityId">Entity Id</param>
        /// <returns>SystemSettings Object</returns>
        internal static SystemSettings FetchSystemSettings(IDalContext dalContext, Int32 entityId)
        {
            return DataPortal.FetchChild<SystemSettings>(dalContext, entityId);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Method to update or insert config values for the current entity id
        /// as required. This is used by both the child_insert & child_update
        /// </summary>
        /// <param name="dal"></param>
        private void UpdateConfig(ISystemSettingsDal dal)
        {
            IEnumerable<SystemSettingsDto> oldDtoList = dal.FetchByEntityId(this.ParentEntity.Id);

            UpdateConfigItem(dal, oldDtoList, _foundationServicesEndpoint, ReadProperty<String>(FoundationServicesEndpointProperty));
            UpdateConfigItem(dal, oldDtoList, _compressionName, ReadProperty<String>(CompressionNameProperty));

            UpdateConfigItem(dal, oldDtoList, _lengthUnitOfMeasure, (Byte)ReadProperty<UnitOfMeasureLengthType>(LengthUnitOfMeasureProperty));
            UpdateConfigItem(dal, oldDtoList, _areaUnitOfMeasure, (Byte)ReadProperty<UnitOfMeasureAreaType>(AreaUnitsOfMeasureProperty));
            UpdateConfigItem(dal, oldDtoList, _angleUnitOfMeasure, (Byte)ReadProperty<UnitOfMeasureAngleType>(AngleUnitsOfMeasureProperty));
            UpdateConfigItem(dal, oldDtoList, _volumeUnitOfMeasure, (Byte)ReadProperty<UnitOfMeasureVolumeType>(VolumeUnitsOfMeasureProperty));
            UpdateConfigItem(dal, oldDtoList, _weightUnitOfMeasure, (Byte)ReadProperty<UnitOfMeasureWeightType>(WeightUnitsOfMeasureProperty));
            UpdateConfigItem(dal, oldDtoList, _currencyUnitOfMeasure, (Byte)ReadProperty<UnitOfMeasureCurrencyType>(CurrencyUnitsOfMesasureProperty));
            UpdateConfigItem(dal, oldDtoList, _productPlacementX, (Byte)ReadProperty<ProductPlacementXType>(ProductPlacementXProperty));
            UpdateConfigItem(dal, oldDtoList, _productPlacementY, (Byte)ReadProperty<ProductPlacementYType>(ProductPlacementYProperty));
            UpdateConfigItem(dal, oldDtoList, _productPlacementZ, (Byte)ReadProperty<ProductPlacementZType>(ProductPlacementZProperty));
            UpdateConfigItem(dal, oldDtoList, _productHeight, ReadProperty<Single>(ProductHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _productWidth, ReadProperty<Single>(ProductWidthProperty));
            UpdateConfigItem(dal, oldDtoList, _productDepth, ReadProperty<Single>(ProductDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _fixtureHeight, ReadProperty<Single>(FixtureHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _fixtureWidth, ReadProperty<Single>(FixtureWidthProperty));
            UpdateConfigItem(dal, oldDtoList, _fixtureDepth, ReadProperty<Single>(FixtureDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _fixtureHasBackboard, ReadProperty<Boolean>(FixtureHasBackboardProperty));
            UpdateConfigItem(dal, oldDtoList, _backboardDepth, ReadProperty<Single>(BackboardDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _fixtureHasBase, ReadProperty<Boolean>(FixtureHasBaseProperty));
            UpdateConfigItem(dal, oldDtoList, _baseHeight, ReadProperty<Single>(BaseHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _shelfHeight, ReadProperty<Single>(ShelfHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _shelfWidth, ReadProperty<Single>(ShelfWidthProperty));
            UpdateConfigItem(dal, oldDtoList, _shelfDepth, ReadProperty<Single>(ShelfDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _shelfFillColour, ReadProperty<Int32>(ShelfFillColourProperty));
            UpdateConfigItem(dal, oldDtoList, _pegboardHeight, ReadProperty<Single>(PegboardHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _pegboardWidth, ReadProperty<Single>(PegboardWidthProperty));
            UpdateConfigItem(dal, oldDtoList, _pegboardDepth, ReadProperty<Single>(PegboardDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _pegboardFillColour, ReadProperty<Int32>(PegboardFillColourProperty));
            UpdateConfigItem(dal, oldDtoList, _chestHeight, ReadProperty<Single>(ChestHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _chestWidth, ReadProperty<Single>(ChestWidthProperty));
            UpdateConfigItem(dal, oldDtoList, _chestDepth, ReadProperty<Single>(ChestDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _chestFillColour, ReadProperty<Int32>(ChestFillColourProperty));
            UpdateConfigItem(dal, oldDtoList, _barHeight, ReadProperty<Single>(BarHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _barWidth, ReadProperty<Single>(BarWidthProperty));
            UpdateConfigItem(dal, oldDtoList, _barDepth, ReadProperty<Single>(BarDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _barFillColour, ReadProperty<Int32>(BarFillColourProperty));
            UpdateConfigItem(dal, oldDtoList, _rodHeight, ReadProperty<Single>(RodHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _rodWidth, ReadProperty<Single>(RodWidthProperty));
            UpdateConfigItem(dal, oldDtoList, _rodDepth, ReadProperty<Single>(RodDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _rodFillColour, ReadProperty<Int32>(RodFillColourProperty));
            UpdateConfigItem(dal, oldDtoList, _clipStripHeight, ReadProperty<Single>(ClipStripHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _clipStripWidth, ReadProperty<Single>(ClipStripWidthProperty));
            UpdateConfigItem(dal, oldDtoList, _clipStripDepth, ReadProperty<Single>(ClipStripDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _clipStripFillColour, ReadProperty<Int32>(ClipStripFillColourProperty));
            UpdateConfigItem(dal, oldDtoList, _palletHeight, ReadProperty<Single>(PalletHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _palletWidth, ReadProperty<Single>(PalletWidthProperty));
            UpdateConfigItem(dal, oldDtoList, _palletDepth, ReadProperty<Single>(PalletDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _palletFillColour, ReadProperty<Int32>(PalletFillColourProperty));
            UpdateConfigItem(dal, oldDtoList, _slotwallHeight, ReadProperty<Single>(SlotwallHeightProperty));
            UpdateConfigItem(dal, oldDtoList, _slotwallWidth, ReadProperty<Single>(SlotwallWidthProperty));
            UpdateConfigItem(dal, oldDtoList, _slotwallDepth, ReadProperty<Single>(SlotwallDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _slotwallFillColour, ReadProperty<Int32>(SlotwallFillColourProperty));
            UpdateConfigItem(dal, oldDtoList, _lastSuccessfulSyncDate, ReadDatePropertyAsInvariantDateTime(LastSuccessfulSyncDateProperty));
            UpdateConfigItem(dal, oldDtoList, _lastFailedSyncDate, ReadDatePropertyAsInvariantDateTime(LastFailedSyncDateProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageSource, ReadProperty<RealImageProviderType>(ProductImageSourceProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageLocation, ReadProperty<String>(ProductImageLocationProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageAttribute, ReadProperty<String>(ProductImageAttributeProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageIgnoreLeadingZeros, ReadProperty<Boolean>(ProductImageIgnoreLeadingZerosProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageMinFieldLength, ReadProperty<Byte>(ProductImageMinFieldLengthProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageFacingPostfixFront, ReadProperty<String>(ProductImageFacingPostfixFrontProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageFacingPostfixLeft, ReadProperty<String>(ProductImageFacingPostfixLeftProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageFacingPostfixTop, ReadProperty<String>(ProductImageFacingPostfixTopProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageFacingPostfixBack, ReadProperty<String>(ProductImageFacingPostfixBackProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageFacingPostfixRight, ReadProperty<String>(ProductImageFacingPostfixRightProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageFacingPostfixBottom, ReadProperty<String>(ProductImageFacingPostfixBottomProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageCompression, ReadProperty<RealImageCompressionType>(ProductImageCompressionProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageColourDepth, ReadProperty<RealImageColourDepthType>(ProductImageColourDepthProperty));
            UpdateConfigItem(dal, oldDtoList, _productImageTransparency, ReadProperty<Boolean>(ProductImageTransparencyProperty));
            UpdateConfigItem(dal, oldDtoList, _fixtureLabel, ReadProperty<String>(FixtureLabelProperty));
            UpdateConfigItem(dal, oldDtoList, _productLabel, ReadProperty<String>(ProductLabelProperty));
            UpdateConfigItem(dal, oldDtoList, _highlight, ReadProperty<String>(HighlightProperty));
            UpdateConfigItem(dal, oldDtoList, _positionUnits, ReadProperty<Boolean>(PositionUnitsProperty));
            UpdateConfigItem(dal, oldDtoList, _productImages, ReadProperty<Boolean>(ProductImagesProperty));
            UpdateConfigItem(dal, oldDtoList, _productShapes, ReadProperty<Boolean>(ProductShapesProperty));
            UpdateConfigItem(dal, oldDtoList, _productFillPatterns, ReadProperty<Boolean>(ProductFillPatternsProperty));
            UpdateConfigItem(dal, oldDtoList, _fixtureImages, ReadProperty<Boolean>(FixtureImagesProperty));
            UpdateConfigItem(dal, oldDtoList, _fixtureFillPatterns, ReadProperty<Boolean>(FixtureFillPatternsProperty));
            UpdateConfigItem(dal, oldDtoList, _chestWalls, ReadProperty<Boolean>(ChestWallsProperty));
            UpdateConfigItem(dal, oldDtoList, _rotateTopDownComponents, ReadProperty<Boolean>(RotateTopDownComponentsProperty));
            UpdateConfigItem(dal, oldDtoList, _shelfRisers, ReadProperty<Boolean>(ShelfRisersProperty));
            UpdateConfigItem(dal, oldDtoList, _notches, ReadProperty<Boolean>(NotchesProperty));
            UpdateConfigItem(dal, oldDtoList, _pegHoles, ReadProperty<Boolean>(PegHolesProperty));
            UpdateConfigItem(dal, oldDtoList, _pegs, ReadProperty<Boolean>(PegsProperty));
            UpdateConfigItem(dal, oldDtoList, _dividerLines, ReadProperty<Boolean>(DividerLinesProperty));
            UpdateConfigItem(dal, oldDtoList, _dividers, ReadProperty<Boolean>(DividersProperty));
            UpdateConfigItem(dal, oldDtoList, _annotations, ReadProperty<Boolean>(AnnotationsProperty));
            UpdateConfigItem(dal, oldDtoList, _textBoxFont, ReadProperty<String>(TextBoxFontProperty));
            UpdateConfigItem(dal, oldDtoList, _textBoxFontSize, ReadProperty<Single>(TextBoxFontSizeProperty));
            UpdateConfigItem(dal, oldDtoList, _textBoxFontColour, ReadProperty<Int32>(TextBoxFontColourProperty));
            UpdateConfigItem(dal, oldDtoList, _textBoxCanReduceToFit, ReadProperty<Boolean>(TextBoxCanReduceToFitProperty));
            UpdateConfigItem(dal, oldDtoList, _textBoxBorderColour, ReadProperty<Int32>(TextBoxBorderColourProperty));
            UpdateConfigItem(dal, oldDtoList, _textBoxBackgroundColour, ReadProperty<Int32>(TextBoxBackgroundColourProperty));
            UpdateConfigItem(dal, oldDtoList, _textBoxBorderThickness, ReadProperty<Single>(TextBoxBorderThicknessProperty));

        }

        /// <summary>
        /// Gets a datetime property and converts to an invariant time string so that 
        /// the date is always correct regardless of locale
        /// </summary>
        /// <param name="propertyInfo"></param>
        /// <returns></returns>
        private String ReadDatePropertyAsInvariantDateTime(PropertyInfo<DateTime?> propertyInfo)
        {
            DateTime? value = ReadProperty<DateTime?>(propertyInfo);
            return value == null ? String.Empty : ((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss");
        }

        /// <summary>
        /// Updates or inserts based on the given key.
        /// </summary>
        /// <param name="dal"></param>
        /// <param name="oldDtoList"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        private void UpdateConfigItem(ISystemSettingsDal dal, IEnumerable<SystemSettingsDto> oldDtoList, String key, Object value)
        {
            SystemSettingsDto newDto = GetDataTransferObject(key, value);
            SystemSettingsDto oldDto = oldDtoList.FirstOrDefault(c => c.Key == key);

            if (oldDto == null)
            {
                dal.Insert(newDto);
            }
            else
            {
                oldDto.Value = newDto.Value;
                dal.Update(oldDto);
            }
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto with the provided details
        /// </summary>
        /// <param name="name">The config value name</param>
        /// <param name="value">The config value</param>
        /// <returns>A new dto</returns>
        private SystemSettingsDto GetDataTransferObject(String key, Object value)
        {
            return new SystemSettingsDto()
            {
                Key = key,
                Value = Convert.ToString(value, CultureInfo.InvariantCulture),
                UserId = null,
                EntityId = this.ParentEntity.Id
            };
        }

        /// <summary>
        /// Returns a dto with the provided details for a datetime datatype value
        /// </summary>
        /// <param name="name">The config value name</param>
        /// <param name="value">The config value</param>
        /// <returns>A new dto</returns>
        private SystemSettingsDto GetDateTypeDataTransferObject(String key, Object value)
        {
            return new SystemSettingsDto()
            {
                Key = key,
                Value = value == null ? String.Empty : ((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss"),
                UserId = null,
                EntityId = this.ParentEntity.Id
            };
        }

        /// <summary>
        /// Returns a list of dtos for this item.
        /// </summary>
        /// <returns></returns>
        private List<SystemSettingsDto> GetDataTransferObjects()
        {
            List<SystemSettingsDto> dtoList = new List<SystemSettingsDto>();

            dtoList.Add(GetDataTransferObject(_foundationServicesEndpoint, ReadProperty<String>(FoundationServicesEndpointProperty)));
            dtoList.Add(GetDataTransferObject(_compressionName, ReadProperty<String>(CompressionNameProperty)));
            dtoList.Add(GetDataTransferObject(_lengthUnitOfMeasure, (Byte)ReadProperty<UnitOfMeasureLengthType>(LengthUnitOfMeasureProperty)));
            dtoList.Add(GetDataTransferObject(_areaUnitOfMeasure, (Byte)ReadProperty<UnitOfMeasureAreaType>(AreaUnitsOfMeasureProperty)));
            dtoList.Add(GetDataTransferObject(_angleUnitOfMeasure, (Byte)ReadProperty<UnitOfMeasureAngleType>(AngleUnitsOfMeasureProperty)));
            dtoList.Add(GetDataTransferObject(_volumeUnitOfMeasure, (Byte)ReadProperty<UnitOfMeasureVolumeType>(VolumeUnitsOfMeasureProperty)));
            dtoList.Add(GetDataTransferObject(_weightUnitOfMeasure, (Byte)ReadProperty<UnitOfMeasureWeightType>(WeightUnitsOfMeasureProperty)));
            dtoList.Add(GetDataTransferObject(_currencyUnitOfMeasure, (Byte)ReadProperty<UnitOfMeasureCurrencyType>(CurrencyUnitsOfMesasureProperty)));
            dtoList.Add(GetDataTransferObject(_productPlacementX, (Byte)ReadProperty<ProductPlacementXType>(ProductPlacementXProperty)));
            dtoList.Add(GetDataTransferObject(_productPlacementY, (Byte)ReadProperty<ProductPlacementYType>(ProductPlacementYProperty)));
            dtoList.Add(GetDataTransferObject(_productPlacementZ, (Byte)ReadProperty<ProductPlacementZType>(ProductPlacementZProperty)));
            dtoList.Add(GetDataTransferObject(_productHeight, ReadProperty<Single>(ProductHeightProperty)));
            dtoList.Add(GetDataTransferObject(_productWidth, ReadProperty<Single>(ProductWidthProperty)));
            dtoList.Add(GetDataTransferObject(_productDepth, ReadProperty<Single>(ProductDepthProperty)));
            dtoList.Add(GetDataTransferObject(_fixtureHeight, ReadProperty<Single>(FixtureHeightProperty)));
            dtoList.Add(GetDataTransferObject(_fixtureWidth, ReadProperty<Single>(FixtureWidthProperty)));
            dtoList.Add(GetDataTransferObject(_fixtureDepth, ReadProperty<Single>(FixtureDepthProperty)));
            dtoList.Add(GetDataTransferObject(_fixtureHasBackboard, ReadProperty<Boolean>(FixtureHasBackboardProperty)));
            dtoList.Add(GetDataTransferObject(_backboardDepth, ReadProperty<Single>(BackboardDepthProperty)));
            dtoList.Add(GetDataTransferObject(_fixtureHasBase, ReadProperty<Boolean>(FixtureHasBaseProperty)));
            dtoList.Add(GetDataTransferObject(_baseHeight, ReadProperty<Single>(BaseHeightProperty)));
            dtoList.Add(GetDataTransferObject(_shelfHeight, ReadProperty<Single>(ShelfHeightProperty)));
            dtoList.Add(GetDataTransferObject(_shelfWidth, ReadProperty<Single>(ShelfWidthProperty)));
            dtoList.Add(GetDataTransferObject(_shelfDepth, ReadProperty<Single>(ShelfDepthProperty)));
            dtoList.Add(GetDataTransferObject(_shelfFillColour, ReadProperty<Int32>(ShelfFillColourProperty)));
            dtoList.Add(GetDataTransferObject(_pegboardHeight, ReadProperty<Single>(PegboardHeightProperty)));
            dtoList.Add(GetDataTransferObject(_pegboardWidth, ReadProperty<Single>(PegboardWidthProperty)));
            dtoList.Add(GetDataTransferObject(_pegboardDepth, ReadProperty<Single>(PegboardDepthProperty)));
            dtoList.Add(GetDataTransferObject(_pegboardFillColour, ReadProperty<Int32>(PegboardFillColourProperty)));
            dtoList.Add(GetDataTransferObject(_chestHeight, ReadProperty<Single>(ChestHeightProperty)));
            dtoList.Add(GetDataTransferObject(_chestWidth, ReadProperty<Single>(ChestWidthProperty)));
            dtoList.Add(GetDataTransferObject(_chestDepth, ReadProperty<Single>(ChestDepthProperty)));
            dtoList.Add(GetDataTransferObject(_chestFillColour, ReadProperty<Int32>(ChestFillColourProperty)));
            dtoList.Add(GetDataTransferObject(_barHeight, ReadProperty<Single>(BarHeightProperty)));
            dtoList.Add(GetDataTransferObject(_barWidth, ReadProperty<Single>(BarWidthProperty)));
            dtoList.Add(GetDataTransferObject(_barDepth, ReadProperty<Single>(BarDepthProperty)));
            dtoList.Add(GetDataTransferObject(_barFillColour, ReadProperty<Int32>(BarFillColourProperty)));
            dtoList.Add(GetDataTransferObject(_rodHeight, ReadProperty<Single>(RodHeightProperty)));
            dtoList.Add(GetDataTransferObject(_rodWidth, ReadProperty<Single>(RodWidthProperty)));
            dtoList.Add(GetDataTransferObject(_rodDepth, ReadProperty<Single>(RodDepthProperty)));
            dtoList.Add(GetDataTransferObject(_rodFillColour, ReadProperty<Int32>(RodFillColourProperty)));
            dtoList.Add(GetDataTransferObject(_clipStripHeight, ReadProperty<Single>(ClipStripHeightProperty)));
            dtoList.Add(GetDataTransferObject(_clipStripWidth, ReadProperty<Single>(ClipStripWidthProperty)));
            dtoList.Add(GetDataTransferObject(_clipStripDepth, ReadProperty<Single>(ClipStripDepthProperty)));
            dtoList.Add(GetDataTransferObject(_clipStripFillColour, ReadProperty<Int32>(ClipStripFillColourProperty)));
            dtoList.Add(GetDataTransferObject(_palletHeight, ReadProperty<Single>(PalletHeightProperty)));
            dtoList.Add(GetDataTransferObject(_palletWidth, ReadProperty<Single>(PalletWidthProperty)));
            dtoList.Add(GetDataTransferObject(_palletDepth, ReadProperty<Single>(PalletDepthProperty)));
            dtoList.Add(GetDataTransferObject(_palletFillColour, ReadProperty<Int32>(PalletFillColourProperty)));
            dtoList.Add(GetDataTransferObject(_palletFillColour, ReadProperty<Int32>(PalletFillColourProperty)));
            dtoList.Add(GetDataTransferObject(_slotwallHeight, ReadProperty<Single>(SlotwallHeightProperty)));
            dtoList.Add(GetDataTransferObject(_slotwallWidth, ReadProperty<Single>(SlotwallWidthProperty)));
            dtoList.Add(GetDataTransferObject(_slotwallDepth, ReadProperty<Single>(SlotwallDepthProperty)));
            dtoList.Add(GetDataTransferObject(_slotwallFillColour, ReadProperty<Int32>(SlotwallFillColourProperty)));
            dtoList.Add(GetDateTypeDataTransferObject(_lastSuccessfulSyncDate, ReadProperty<DateTime?>(LastSuccessfulSyncDateProperty)));
            dtoList.Add(GetDateTypeDataTransferObject(_lastFailedSyncDate, ReadProperty<DateTime?>(LastFailedSyncDateProperty)));
            dtoList.Add(GetDataTransferObject(_productImageSource, ReadProperty<RealImageProviderType>(ProductImageSourceProperty)));
            dtoList.Add(GetDataTransferObject(_productImageLocation, ReadProperty<String>(ProductImageLocationProperty)));
            dtoList.Add(GetDataTransferObject(_productImageAttribute, ReadProperty<String>(ProductImageAttributeProperty)));
            dtoList.Add(GetDataTransferObject(_productImageIgnoreLeadingZeros, ReadProperty<Boolean>(ProductImageIgnoreLeadingZerosProperty)));
            dtoList.Add(GetDataTransferObject(_productImageMinFieldLength, ReadProperty<Byte>(ProductImageMinFieldLengthProperty)));
            dtoList.Add(GetDataTransferObject(_productImageFacingPostfixFront, ReadProperty<String>(ProductImageFacingPostfixFrontProperty)));
            dtoList.Add(GetDataTransferObject(_productImageFacingPostfixLeft, ReadProperty<String>(ProductImageFacingPostfixLeftProperty)));
            dtoList.Add(GetDataTransferObject(_productImageFacingPostfixTop, ReadProperty<String>(ProductImageFacingPostfixTopProperty)));
            dtoList.Add(GetDataTransferObject(_productImageFacingPostfixBack, ReadProperty<String>(ProductImageFacingPostfixBackProperty)));
            dtoList.Add(GetDataTransferObject(_productImageFacingPostfixRight, ReadProperty<String>(ProductImageFacingPostfixRightProperty)));
            dtoList.Add(GetDataTransferObject(_productImageFacingPostfixBottom, ReadProperty<String>(ProductImageFacingPostfixBottomProperty)));
            dtoList.Add(GetDataTransferObject(_productImageCompression, ReadProperty<RealImageCompressionType>(ProductImageCompressionProperty)));
            dtoList.Add(GetDataTransferObject(_productImageColourDepth, ReadProperty<RealImageColourDepthType>(ProductImageColourDepthProperty)));
            dtoList.Add(GetDataTransferObject(_productImageTransparency, ReadProperty<Boolean>(ProductImageTransparencyProperty)));
            dtoList.Add(GetDataTransferObject(_fixtureLabel, ReadProperty<String>(FixtureLabelProperty)));
            dtoList.Add(GetDataTransferObject(_productLabel, ReadProperty<String>(ProductLabelProperty)));
            dtoList.Add(GetDataTransferObject(_highlight, ReadProperty<String>(HighlightProperty)));
            dtoList.Add(GetDataTransferObject(_positions, ReadProperty<Boolean>(PositionsProperty)));
            dtoList.Add(GetDataTransferObject(_positionUnits, ReadProperty<Boolean>(PositionUnitsProperty)));
            dtoList.Add(GetDataTransferObject(_productImages, ReadProperty<Boolean>(ProductImagesProperty)));
            dtoList.Add(GetDataTransferObject(_productShapes, ReadProperty<Boolean>(ProductShapesProperty)));
            dtoList.Add(GetDataTransferObject(_productFillPatterns, ReadProperty<Boolean>(ProductFillPatternsProperty)));
            dtoList.Add(GetDataTransferObject(_fixtureImages, ReadProperty<Boolean>(FixtureImagesProperty)));
            dtoList.Add(GetDataTransferObject(_fixtureFillPatterns, ReadProperty<Boolean>(FixtureFillPatternsProperty)));
            dtoList.Add(GetDataTransferObject(_chestWalls, ReadProperty<Boolean>(ChestWallsProperty)));
            dtoList.Add(GetDataTransferObject(_rotateTopDownComponents, ReadProperty<Boolean>(RotateTopDownComponentsProperty)));
            dtoList.Add(GetDataTransferObject(_shelfRisers, ReadProperty<Boolean>(ShelfRisersProperty)));
            dtoList.Add(GetDataTransferObject(_notches, ReadProperty<Boolean>(NotchesProperty)));
            dtoList.Add(GetDataTransferObject(_pegHoles, ReadProperty<Boolean>(PegHolesProperty)));
            dtoList.Add(GetDataTransferObject(_pegs, ReadProperty<Boolean>(PegsProperty)));
            dtoList.Add(GetDataTransferObject(_dividerLines, ReadProperty<Boolean>(DividerLinesProperty)));
            dtoList.Add(GetDataTransferObject(_dividers, ReadProperty<Boolean>(DividersProperty)));
            dtoList.Add(GetDataTransferObject(_annotations, ReadProperty<Boolean>(AnnotationsProperty)));
            dtoList.Add(GetDataTransferObject(_textBoxFont, ReadProperty<String>(TextBoxFontProperty)));
            dtoList.Add(GetDataTransferObject(_textBoxFontSize, ReadProperty<Single>(TextBoxFontSizeProperty)));
            dtoList.Add(GetDataTransferObject(_textBoxFontColour, ReadProperty<Int32>(TextBoxFontColourProperty)));
            dtoList.Add(GetDataTransferObject(_textBoxCanReduceToFit, ReadProperty<Boolean>(TextBoxCanReduceToFitProperty)));
            dtoList.Add(GetDataTransferObject(_textBoxBorderColour, ReadProperty<Int32>(TextBoxBorderColourProperty)));
            dtoList.Add(GetDataTransferObject(_textBoxBackgroundColour, ReadProperty<Int32>(TextBoxBackgroundColourProperty)));
            dtoList.Add(GetDataTransferObject(_textBoxBorderThickness, ReadProperty<Single>(TextBoxBorderThicknessProperty)));

            return dtoList;
        }

        /// <summary>
        /// Loads a list of dtos into this object
        /// </summary>
        /// <param name="dtoList">The dto list to load</param>
        private void LoadDataTransferObjects(IEnumerable<SystemSettingsDto> dtoList)
        {
            foreach (SystemSettingsDto dto in dtoList)
            {
                try
                {
                    switch (dto.Key)
                    {
                        case _foundationServicesEndpoint: LoadProperty<String>(FoundationServicesEndpointProperty, dto.Value); break;
                        case _compressionName: LoadProperty<String>(CompressionNameProperty, dto.Value); break;
                        case _lengthUnitOfMeasure: LoadProperty<UnitOfMeasureLengthType>(LengthUnitOfMeasureProperty, (UnitOfMeasureLengthType)Byte.Parse(dto.Value, CultureInfo.InvariantCulture)); break;
                        case _areaUnitOfMeasure: LoadProperty<UnitOfMeasureAreaType>(AreaUnitsOfMeasureProperty, (UnitOfMeasureAreaType)Byte.Parse(dto.Value, CultureInfo.InvariantCulture)); break;
                        case _angleUnitOfMeasure: LoadProperty<UnitOfMeasureAngleType>(AngleUnitsOfMeasureProperty, (UnitOfMeasureAngleType)Byte.Parse(dto.Value, CultureInfo.InvariantCulture)); break;
                        case _volumeUnitOfMeasure: LoadProperty<UnitOfMeasureVolumeType>(VolumeUnitsOfMeasureProperty, (UnitOfMeasureVolumeType)Byte.Parse(dto.Value, CultureInfo.InvariantCulture)); break;
                        case _weightUnitOfMeasure: LoadProperty<UnitOfMeasureWeightType>(WeightUnitsOfMeasureProperty, (UnitOfMeasureWeightType)Byte.Parse(dto.Value, CultureInfo.InvariantCulture)); break;
                        case _currencyUnitOfMeasure: LoadProperty<UnitOfMeasureCurrencyType>(CurrencyUnitsOfMesasureProperty, (UnitOfMeasureCurrencyType)Byte.Parse(dto.Value, CultureInfo.InvariantCulture)); break;
                        case _productPlacementX: LoadProperty<ProductPlacementXType>(ProductPlacementXProperty, (ProductPlacementXType)Byte.Parse(dto.Value, CultureInfo.InvariantCulture)); break;
                        case _productPlacementY: LoadProperty<ProductPlacementYType>(ProductPlacementYProperty, (ProductPlacementYType)Byte.Parse(dto.Value, CultureInfo.InvariantCulture)); break;
                        case _productPlacementZ: LoadProperty<ProductPlacementZType>(ProductPlacementZProperty, (ProductPlacementZType)Byte.Parse(dto.Value, CultureInfo.InvariantCulture)); break;
                        case _productHeight: LoadProperty<Single>(ProductHeightProperty, Single.Parse(dto.Value)); break;
                        case _productWidth: LoadProperty<Single>(ProductWidthProperty, Single.Parse(dto.Value)); break;
                        case _productDepth: LoadProperty<Single>(ProductDepthProperty, Single.Parse(dto.Value)); break;
                        case _fixtureHeight: LoadProperty<Single>(FixtureHeightProperty, Single.Parse(dto.Value)); break;
                        case _fixtureWidth: LoadProperty<Single>(FixtureWidthProperty, Single.Parse(dto.Value)); break;
                        case _fixtureDepth: LoadProperty<Single>(FixtureDepthProperty, Single.Parse(dto.Value)); break;
                        case _fixtureHasBackboard: LoadProperty<Boolean>(FixtureHasBackboardProperty, Boolean.Parse(dto.Value)); break;
                        case _backboardDepth: LoadProperty<Single>(BackboardDepthProperty, Single.Parse(dto.Value)); break;
                        case _fixtureHasBase: LoadProperty<Boolean>(FixtureHasBaseProperty, Boolean.Parse(dto.Value)); break;
                        case _baseHeight: LoadProperty<Single>(BaseHeightProperty, Single.Parse(dto.Value)); break;
                        case _shelfHeight: LoadProperty<Single>(ShelfHeightProperty, Single.Parse(dto.Value)); break;
                        case _shelfWidth: LoadProperty<Single>(ShelfWidthProperty, Single.Parse(dto.Value)); break;
                        case _shelfDepth: LoadProperty<Single>(ShelfDepthProperty, Single.Parse(dto.Value)); break;
                        case _shelfFillColour: LoadProperty<Int32>(ShelfFillColourProperty, Int32.Parse(dto.Value)); break;
                        case _pegboardHeight: LoadProperty<Single>(PegboardHeightProperty, Single.Parse(dto.Value)); break;
                        case _pegboardWidth: LoadProperty<Single>(PegboardWidthProperty, Single.Parse(dto.Value)); break;
                        case _pegboardDepth: LoadProperty<Single>(PegboardDepthProperty, Single.Parse(dto.Value)); break;
                        case _pegboardFillColour: LoadProperty<Int32>(PegboardFillColourProperty, Int32.Parse(dto.Value)); break;
                        case _chestHeight: LoadProperty<Single>(ChestHeightProperty, Single.Parse(dto.Value)); break;
                        case _chestWidth: LoadProperty<Single>(ChestWidthProperty, Single.Parse(dto.Value)); break;
                        case _chestDepth: LoadProperty<Single>(ChestDepthProperty, Single.Parse(dto.Value)); break;
                        case _chestFillColour: LoadProperty<Int32>(ChestFillColourProperty, Int32.Parse(dto.Value)); break;
                        case _barHeight: LoadProperty<Single>(BarHeightProperty, Single.Parse(dto.Value)); break;
                        case _barWidth: LoadProperty<Single>(BarWidthProperty, Single.Parse(dto.Value)); break;
                        case _barDepth: LoadProperty<Single>(BarDepthProperty, Single.Parse(dto.Value)); break;
                        case _barFillColour: LoadProperty<Int32>(BarFillColourProperty, Int32.Parse(dto.Value)); break;
                        case _rodHeight: LoadProperty<Single>(RodHeightProperty, Single.Parse(dto.Value)); break;
                        case _rodWidth: LoadProperty<Single>(RodWidthProperty, Single.Parse(dto.Value)); break;
                        case _rodDepth: LoadProperty<Single>(RodDepthProperty, Single.Parse(dto.Value)); break;
                        case _rodFillColour: LoadProperty<Int32>(RodFillColourProperty, Int32.Parse(dto.Value)); break;
                        case _clipStripHeight: LoadProperty<Single>(ClipStripHeightProperty, Single.Parse(dto.Value)); break;
                        case _clipStripWidth: LoadProperty<Single>(ClipStripWidthProperty, Single.Parse(dto.Value)); break;
                        case _clipStripDepth: LoadProperty<Single>(ClipStripDepthProperty, Single.Parse(dto.Value)); break;
                        case _clipStripFillColour: LoadProperty<Int32>(ClipStripFillColourProperty, Int32.Parse(dto.Value)); break;
                        case _palletHeight: LoadProperty<Single>(PalletHeightProperty, Single.Parse(dto.Value)); break;
                        case _palletWidth: LoadProperty<Single>(PalletWidthProperty, Single.Parse(dto.Value)); break;
                        case _palletDepth: LoadProperty<Single>(PalletDepthProperty, Single.Parse(dto.Value)); break;
                        case _palletFillColour: LoadProperty<Int32>(PalletFillColourProperty, Int32.Parse(dto.Value)); break;
                        case _slotwallHeight: LoadProperty<Single>(SlotwallHeightProperty, Single.Parse(dto.Value)); break;
                        case _slotwallWidth: LoadProperty<Single>(SlotwallWidthProperty, Single.Parse(dto.Value)); break;
                        case _slotwallDepth: LoadProperty<Single>(SlotwallDepthProperty, Single.Parse(dto.Value)); break;
                        case _slotwallFillColour: LoadProperty<Int32>(SlotwallFillColourProperty, Int32.Parse(dto.Value)); break;
                        case _lastSuccessfulSyncDate:
                            DateTime lastSuccessfulSyncDate = DateTime.UtcNow;
                            if (DateTime.TryParse(dto.Value, out lastSuccessfulSyncDate))
                            {
                                LoadProperty<DateTime?>(LastSuccessfulSyncDateProperty, lastSuccessfulSyncDate);
                            }
                            else
                            {
                                LoadProperty<DateTime?>(LastSuccessfulSyncDateProperty, null);
                            }
                            break;

                        case _lastFailedSyncDate:
                            DateTime lastFailedSyncDate = DateTime.UtcNow;
                            if (DateTime.TryParse(dto.Value, out lastFailedSyncDate))
                            {
                                LoadProperty<DateTime?>(LastFailedSyncDateProperty, lastFailedSyncDate);
                            }
                            else
                            {
                                LoadProperty<DateTime?>(LastFailedSyncDateProperty, null);
                            }
                            break;
                        case _productImageSource: LoadProperty<RealImageProviderType>(ProductImageSourceProperty, EnumHelper.Parse(dto.Value, RealImageProviderType.Repository)); break;
                        case _productImageLocation: LoadProperty<String>(ProductImageLocationProperty, dto.Value); break;
                        case _productImageAttribute: LoadProperty<String>(ProductImageAttributeProperty, dto.Value); break;
                        case _productImageIgnoreLeadingZeros: LoadProperty<Boolean>(ProductImageIgnoreLeadingZerosProperty, Boolean.Parse(dto.Value)); break;
                        case _productImageMinFieldLength: LoadProperty<Byte>(ProductImageMinFieldLengthProperty, Byte.Parse(dto.Value)); break;
                        case _productImageFacingPostfixFront: LoadProperty<String>(ProductImageFacingPostfixFrontProperty, dto.Value); break;
                        case _productImageFacingPostfixLeft: LoadProperty<String>(ProductImageFacingPostfixLeftProperty, dto.Value); break;
                        case _productImageFacingPostfixTop: LoadProperty<String>(ProductImageFacingPostfixTopProperty, dto.Value); break;
                        case _productImageFacingPostfixBack: LoadProperty<String>(ProductImageFacingPostfixBackProperty, dto.Value); break;
                        case _productImageFacingPostfixRight: LoadProperty<String>(ProductImageFacingPostfixRightProperty, dto.Value); break;
                        case _productImageFacingPostfixBottom: LoadProperty<String>(ProductImageFacingPostfixBottomProperty, dto.Value); break;
                        case _productImageCompression: LoadProperty<RealImageCompressionType>(ProductImageCompressionProperty, EnumHelper.Parse(dto.Value, RealImageCompressionType.None)); break;
                        case _productImageColourDepth: LoadProperty<RealImageColourDepthType>(ProductImageColourDepthProperty, EnumHelper.Parse(dto.Value, RealImageColourDepthType.Bit8)); break;
                        case _productImageTransparency: LoadProperty<Boolean>(ProductImageTransparencyProperty, Boolean.Parse(dto.Value)); break;
                        case _fixtureLabel: LoadProperty<String>(FixtureLabelProperty, dto.Value); break;
                        case _productLabel: LoadProperty<String>(ProductLabelProperty, dto.Value); break;
                        case _highlight: LoadProperty<String>(HighlightProperty, dto.Value); break;
                        case _positions: LoadProperty<Boolean>(PositionsProperty, Boolean.Parse(dto.Value)); break;
                        case _positionUnits: LoadProperty<Boolean>(PositionUnitsProperty, Boolean.Parse(dto.Value)); break;
                        case _productImages: LoadProperty<Boolean>(ProductImagesProperty, Boolean.Parse(dto.Value)); break;
                        case _productShapes: LoadProperty<Boolean>(ProductShapesProperty, Boolean.Parse(dto.Value)); break;
                        case _productFillPatterns: LoadProperty<Boolean>(ProductFillPatternsProperty, Boolean.Parse(dto.Value)); break;
                        case _fixtureImages: LoadProperty<Boolean>(FixtureImagesProperty, Boolean.Parse(dto.Value)); break;
                        case _fixtureFillPatterns: LoadProperty<Boolean>(FixtureFillPatternsProperty, Boolean.Parse(dto.Value)); break;
                        case _chestWalls: LoadProperty<Boolean>(ChestWallsProperty, Boolean.Parse(dto.Value)); break;
                        case _rotateTopDownComponents: LoadProperty<Boolean>(RotateTopDownComponentsProperty, Boolean.Parse(dto.Value)); break;
                        case _shelfRisers: LoadProperty<Boolean>(ShelfRisersProperty, Boolean.Parse(dto.Value)); break;
                        case _notches: LoadProperty<Boolean>(NotchesProperty, Boolean.Parse(dto.Value)); break;
                        case _pegHoles: LoadProperty<Boolean>(PegHolesProperty, Boolean.Parse(dto.Value)); break;
                        case _pegs: LoadProperty<Boolean>(PegsProperty, Boolean.Parse(dto.Value)); break;
                        case _dividerLines: LoadProperty<Boolean>(DividerLinesProperty, Boolean.Parse(dto.Value)); break;
                        case _dividers: LoadProperty<Boolean>(DividersProperty, Boolean.Parse(dto.Value)); break;
                        case _annotations: LoadProperty<Boolean>(AnnotationsProperty, Boolean.Parse(dto.Value)); break;
                        case _textBoxFont: LoadProperty<String>(TextBoxFontProperty, dto.Value); break;
                        case _textBoxFontSize: LoadProperty<Single>(TextBoxFontSizeProperty, Single.Parse(dto.Value)); break;
                        case _textBoxFontColour: LoadProperty<Int32>(TextBoxFontColourProperty, Int32.Parse(dto.Value)); break;
                        case _textBoxCanReduceToFit: LoadProperty<Boolean>(TextBoxCanReduceToFitProperty, Boolean.Parse(dto.Value)); break;
                        case _textBoxBorderColour: LoadProperty<Int32>(TextBoxBorderColourProperty, Int32.Parse(dto.Value)); break;
                        case _textBoxBackgroundColour: LoadProperty<Int32>(TextBoxBackgroundColourProperty, Int32.Parse(dto.Value)); break;
                        case _textBoxBorderThickness: LoadProperty<Single>(TextBoxBorderThicknessProperty, Single.Parse(dto.Value)); break;
                    }
                }
                catch (Exception) { }
            }
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning an existing Object
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 entityId)
        {
            // set default values incase we are missing information
            // within the solution database
            LoadDefaults();

            // now retrieve the solution database config values
            using (ISystemSettingsDal dal = dalContext.GetDal<ISystemSettingsDal>())
            {
                // populate our Object with the default values
                LoadDataTransferObjects(dal.FetchDefault());

                //Required to eliminate unit test exception when user 
                //is generic and cannot be converted to domainIdentity
                if (ApplicationContext.User.Identity.IsAuthenticated)
                {
                    // now overlay the users default values
                    LoadDataTransferObjects(dal.FetchByEntityId(entityId));
                }
            }
        }

        #endregion

        #region Insert

        /// <summary>
        /// Called when updating this instance in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext)
        {
            using (ISystemSettingsDal dal = dalContext.GetDal<ISystemSettingsDal>())
            {
                UpdateConfig(dal);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Update

        /// <summary>
        /// Called when updating this instance in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            using (ISystemSettingsDal dal = dalContext.GetDal<ISystemSettingsDal>())
            {
                UpdateConfig(dal);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #endregion
    }
}
