﻿#region Header Information

// Copyright © Galleria RTS Ltd 2012

#region Version History: (SA 1.0)

//  SA-17581 : A.Probyn
//      Created
//  SA-17551 : J.Freeborough
//      Added MaximumRetryCountReached and MaximumFailureCountReached event

#endregion

#region Version History: (CCM 8.0)

//  V8-27783 : M.Brumby
//      Added Publishing events
// V8-28060 : A.Kuszyk
//  Added initial cohort of Engine Task options.
// V8-28572 : A.Kuszyk
//  Added PlanogramCouldNotPublishProductsDontMatch.

#endregion

#region Version History : CCM 802

// V8-29027 : A.Kuszyk
//  Added PlanogramBlockingInsufficientComponentSpace.
// V8-28991 : A.Kuszyk
//  Added PlanogramBlockingMissingProducts.
// V8-29072 : A.Kuszyk
//  Added PlanogramSequenceReplaced.
// V8-29078 : A.Kuszyk
//  Added PlanogramCategoryUpdated and PlanogramLocationUpdated.
// V8-29015 : A.Silva
//      Added PlanogramBlockingMissingProducts, PlanogramSequenceMissingProduct, PlanogramBlockingNotAssigned, PlanogramProductsToInsert.
//      Added PlanogramPositionsInserted, PlanogramPositionsNotInserted.

#endregion

#region Version History : CCM 803

// V8-29491 : D.Pleasance
//  Added PlanogramPerformanceDataInvalidLocationReference, PlanogramPerformanceDataInvalidClusterReference
// V8-29633 : D.Pleasance
//  Added PlanogramLocationReferenceInvalid \ PlanogramClusterUpdated
// V8-29643 : D.Pleasance
//  Added PlanogramAttributesUpdated
// V8-29723 : D.Pleasance
//  Added ReplacementProductAlreadyOnPlanogram \ ReplacementProductDoesNotExist \ ReplaceProductDoesNotExist 
//        ReplacementProductCouldNotBeAdded \ PlanogramPositionsReplaced \ PlanogramPositionsReplacedCarPark

#endregion

#region Version History : CCM 810

// V8-29827 : D.Pleasance
//  Added PlanogramRecalculateAssortmentInventoryUpdated
// V8-29865 : D.Pleasance
//  Added PlanogramNoPerformanceSelectionData
// V8-29811 : A.Probyn
//  Added PlanogramOverwritten
// V8-29523 : A.Silva
//  Added PlanogramProductsNotPlaced.
// V8-29868 : A.Silva
//  Added PlanogramProductsExistInPlanogram.
// V8-29987 : M.Brumby
//  Added PublishErrorUnableToLink, PublishPlanogramAssignmentResults and PublishAllAssignmentsFailed
// V8-29992 : D.Pleasance
//  Added PlanogramInvalidFileAccess
// V8-29919 : D.Pleasance
//  Added PlanogramPositionsRemoved, PlanogramProductsAndPositionsRemoved, PlanogramPositionsRemovedCarPark
// V8-29533 : D.Pleasance
//  Added PlanogramDuplicateProductGtin
// V8-30052 : A.Silva
//  Added PlanogramProductBetterPerformanceCouldNotBeAddedDueToSize.
// V8-29562 : D.Pleasance
//  Added PlanogramBlockingAddedPerformance
// V8-30113 : M.Brumby
//  Added PlanogramLocationAssignmentsInvalid
// V8-30086 : D.Pleasance
//  Added PlanogramPerformanceDataNoPerformanceSelection
#endregion

#region Version History: CCM 820
// V8-30710 : M.Brumby
//  PlanogramNoAssortmentPresent changed from score 3 to score 1
// V8-30728 : A.Kuszyk
//  Added various events.
// V8-30727 : D.Pleasance
//  Added PlanogramPerformanceDataNoPlanogramAssignmentReference.
// V8-30729 : D.Pleasance
//  Added CreateSequenceTemplateInvalidSequencePlacement, CreateSequenceTemplateInvalidSequenceInformation, CreateSequenceTemplateSuccessful, CreateSequenceTemplateMultisitedProduct.
// V8-30736 : L.Luong
//  Added PlanogramNoValidLocation
// V8-30705 : A.Probyn
//  Added PlanogramProductDelistedDueToProductRule, PlanogramProductDelistedDueToFamilyRule,PlanogramProductDelistedDueToLocalProductRule
//        PlanogramProductDelistedDueToAssortmentOptionalProductRule, PlanogramProductDelistedDueToAssortmentNormalProductRule, PlanogramProductDelistedDueToAssortmentInheritanceProductRule, PlanogramProductDelistedDueToAssortmentDistributionProductRule = 1054,
// V8-30761 : A.Kuszyk
//  Added PlanogramProductInventoryIncreased.
// V8-30427 : J.Pickup
//  Added various events for the RemoveProductFromPlanogramProductList task.
// V8-30905 : A.Kuszyk
//  Added Product(s) not in universe events.
// V8-31005 : A.Probyn
//  Added CreateAssortmentSuccessful & CreateAssortmentUpdatedProducts
// V8-31098 : D.Pleasance
//  Added PlanogramProductDoesNotExist
// V8-31067 : A.Probyn
//  Added PlanogramCouldNotBeLoadedFromSourceFile, SourceFileLocked
// V8-31172 : A.Kuszyk.
//  Added MultisitedProductNotSequenced.
// V8-31284 : D.Pleasance
//  Added PlanogramSequenceMultiSitedProduct.
// V8-31378 : A.Silva
//  Added PlanogramInsufficientPerformanceDataToRecalculateBlocking.
// V8-31345 : A.Probyn
//  Updated UnexpectedException, AuthenticationFailure, AuthorizationFailure score to be 5 from 0.
#endregion

#region Version History: CCM 830
// V8-31560 : D.Pleasance
//  Added PlanogramProductInventoryDecreased, PlanogramProductInventoryDecreasedNoProductPositions, PlanogramPositionUnableToFurtherDecreaseInventory, PlanogramProductInventoryDecreasedBy,
//  PlanogramPositionUnableToDecreaseInventory.
// V8-31562 : D.Pleasance
//  Added PlanogramFixtureBayMismatchBetweenLocationSpaceData, PlanogramFixtureBayElementMismatchBetweenLocationSpaceData, LocationSpaceBayElementDataMissing,
//  PlanogramFixtureBayCreatedFromLocationSpace, PlanogramFixtureBayElementCreatedFromLocationSpace, PlanogramFixtureBayRemovedFromLocationSpace, PlanogramFixtureBayElementRemovedFromLocationSpace,
//  PlanogramFixtureBayElementUpdatedFromLocationSpace, PlanogramFixtureBayUpdatedFromLocationSpace
// V8-31831 : A.Probyn
//  Added PlanogramNameTemplateApplied
//  Added PlanogramNameTemplateFormulaCouldNotBeResolved
// V8-32205 : A.Probyn
//  Added PlanogramRecalculateAssortmentInventoryUpdated
//  Added PlanogramPerformanceDataProductBuddyApplied
//  Added PlanogramPerformanceDataLocationBuddyApplied
// V8-32522 : A.Silva
//  Added PlanogramComponentsRemoved
// V8-13996 : J.Pickup
//  Added PlanogramPerformanceMetricMismatchBetweenMetricProfile
// CCM-14043 : D.Pleasance
//  Added PlanogramPerformanceDataProductMerged
// CCM-18453 : A.Silva
//  Added warning PlanogramComponentFailedToGetRepresentationForBlock
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Planograms.Model;
using System.Diagnostics;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Event Log Event Enum
    /// 
    /// Note: There's an important difference between this enum and others in the code.  This enum is basically just a
    /// repository of constants for use as values for the EventLog.EventId property.  However, you'll notice that this
    /// property is NOT declared as type EventLogEvent but rather Int32.  This is to avoid the need to add entries to
    /// this enum and recompile the GFS dll just because another application wants to write a new event to the event
    /// log via the web service.  Other applications will have their own enums, potentially with different entries,
    /// but if entries across multiple applications have the same numerical value, these must have the same meaning.
    /// The universal list of event log event values is managed in a spreadsheet here: 
    /// 
    /// http://portal/Products/GFS/Technicalcase %20Documents/Eventcase %20Logcase %20Descriptions.xlsx
    /// </summary>
    public enum EventLogEvent
    {
        // Unit testing
        UnitTest = 0,

        // Generic messages
        UnexpectedException = 1,
        AuthenticationFailure = 2,
        AuthorizationFailure = 3,

        // Windows Services (generic code): 100-range
        WindowsServiceStart = 100,
        WindowsServiceStop = 101,
        WindowsServiceStopping = 102,

        // General Processing 
        FileInUse = 201,

        // CCM Engine
        EngineStarting = 300,
        EngineRunning = 301,
        EnginePaused = 302,
        EngineStopping = 303,
        EngineStopped = 304,
        EngineIdle = 305,
        EngineMessageReaped = 306,
        EngineMessageRequeued = 307,
        EngineShutdownException = 308,
        EngineWorkpackageStarted = 309,
        EngineWorkpackageFinished = 310,

        //Sync Service
        SyncProcessStart = 500,
        SyncProcessEnd = 501,
        ErrorWhileSyncingData = 502,
        SyncInformation = 503,
        MaximumRetryCountReached = 504,
        MaximumFailureCountReached = 505,

        //Publishing
        PublishStart = 700,
        PublishEnd = 701,
        PublishInformation = 702,
        PublishErrorPlan = 703,
        PublishErrorLinkingToProject = 704,
        PublishErrorUnableToLink = 705,
        PublishPlanogramAssignmentResults = 706,
        PublishAllAssignmentsFailed = 707,
        PublishProductNotPresentInGfs = 708,
        PlanogramLocationAssignmentsInvalid = 709,

        #region Workflow Task events (800-1200)

        #region Errors (800-900)
        InvalidTaskParameterValue = 801,
        InsufficientPlanogramContent = 802,
        PlanogramBlockingApplyToFixturesFailed = 803,
        PlanogramPerformanceDataCouldNotBeFetched = 804,
        PlanogramCouldNotPublishProductsDontMatch = 805,
        PlanogramBlockingInsufficientComponentSpace = 806,
        PlanogramInvalidFileAccess = 807,
        PlanogramDuplicateProductGtin = 808,
        CreateSequenceTemplateInvalidSequencePlacement = 809,
        CreateSequenceTemplateInvalidSequenceInformation = 810,
        SourceFileLocked = 811,
        PlanogramFixtureBayMismatchBetweenLocationSpaceData = 812,
        PlanogramFixtureBayElementMismatchBetweenLocationSpaceData = 813,
        LocationSpaceBayElementDataMissing = 814,
        PlanogramPerformanceMetricMismatchBetweenMetricProfile = 815,
        #endregion

        #region Warnings (900-1000)
        PlanogramBlockingGroupMerged = 901,
        PlanogramAssortmentProductCouldNotBeRanked = 902,
        PlanogramCouldNotPublishUnassigned = 903,
        PlanogramBlockingMissingProducts = 904,
        PlanogramSequenceMissingProduct = 905,
        PlanogramBlockingNotAssigned = 906,
        PlanogramPerformanceDataInvalidLocationReference = 907,
        PlanogramPerformanceDataInvalidClusterReference = 908,
        PlanogramLocationReferenceInvalid = 909,
        ReplacementProductAlreadyOnPlanogram = 910,
        ReplacementProductDoesNotExist = 911,
        ReplaceProductDoesNotExist = 912,
        ReplacementProductCouldNotBeAdded = 913,
        PlanogramPerformanceDataMissingOrEmpty = 914,
        PlanogramPositionsNotInserted = 915,
        PlanogramNoAssortmentPresent = 916,
        PlanogramBlockDeservesNoSpace = 917,
        InsertProductNotAdded = 918,
        AddProductAlreadyExistsInPlanogram = 919,
        PlanogramProductBetterPerformanceCouldNotBeAddedDueToSize = 920,
        InvalidProductFilter = 921,
        PlanogramPerformanceDataNoPlanogramAssignmentReference = 922,
        PlanogramNoValidLocation = 923,
        PlanogramNoProductsPresent = 924,
        ProductsInPlanNotInUniverse = 925,
        PlanogramProductDoesNotExist = 926,
        PlanogramCouldNotBeLoadedFromSourceFile = 927,
        MultisitedProductNotSequenced = 928,
        PlanogramSequenceMultiSitedProduct = 929,
        PlanogramInsufficientPerformanceDataToRecalculateBlocking = 930,
        PlanogramProductInventoryDecreasedNoProductPositions = 931,
        PlanogramNameTemplateFormulaCouldNotBeResolved = 932,
        PlanogramComponentFailedToGetRepresentationForBlock = 933,
        #endregion

        #region Information (1000 - 1100)
        PlanogramProductsAdded = 1001,
        PlanogramProductsRemoved = 1002,
        NothingForTaskToDo = 1003,
        PlanogramPositionsAdded = 1004,
        PlanogramProductPositionsRemoved = 1005,
        PlanogramProductsToAdd = 1006,
        PlanogramAssortmentReplaced = 1007,
        PlanogramBlockingAdded = 1008,
        PlanogramConsumerDecisionTreeAdded = 1009,
        PlacedPlanogramProductsNotInCdt = 1010,
        UnPlacedPlanogramProductsNotInCdt = 1011,
        PlanogramReNumberingStrategyAdded = 1012,
        PlanogramValidationTemplateAdded = 1013,
        PlanogramAssortmentProductsUnitsDecreased = 1014,
        PlanogramProductPositionsMovedToCarParkShelf = 1015,
        PlanogramMetadataUpdated = 1016,
        PlanogramReNumberingStrategyUpdated = 1017,
        PlanogramValidationUpdated = 1018,
        PlanogramPerformanceDataUpdated = 1019,
        PlanogramAssortmentProductsUnitsIncreased = 1020,
        PlanogramPublishedToGfs = 1021,
        PlanogramMoved = 1022,
        PlanogramProductsUpdated = 1023,
        PlanogramSequenceReplaced = 1024,
        PlanogramLocationUpdated = 1025,
        PlanogramCategoryUpdated = 1026,
        PlanogramProductsToInsert = 1027,
        PlanogramPositionsInserted = 1028,
        PlanogramClusterUpdated = 1029,
        PlanogramAttributesUpdated = 1030,
        PlanogramPositionsReplaced = 1031,
        PlanogramPositionsReplacedCarPark = 1032,
        PlanogramRecalculateAssortmentInventoryUpdated = 1033,
        PlanogramOverwritten = 1034,
        PlanogramProductsNotPlaced = 1035,
        PlanogramProductsExistInPlanogram = 1036,
        PlanogramPositionsRemoved = 1037,
        PlanogramProductsAndPositionsRemoved = 1038,
        PlanogramPositionsRemovedCarPark = 1039,
        PlanogramNoPerformanceSelectionData = 1040,
        RemoveProductDoesNotExist = 1041,
        ProductAddedCarPark = 1042,
        PlanogramBlockingAddedPerformance = 1043,
        PlanogramPerformanceDataNoPerformanceSelection = 1044,
        ProductColoursUpdatedFromHighlight = 1045,
        CreateSequenceTemplateSuccessful = 1046,
        CreateSequenceTemplateMultisitedProduct = 1047,
        PlanogramProductDelistedDueToProductRule = 1048,
        PlanogramProductDelistedDueToFamilyRule = 1049,
        PlanogramProductDelistedDueToLocalProductRule = 1050,
        PlanogramProductDelistedDueToAssortmentOptionalProductRule = 1051,
        PlanogramProductDelistedDueToAssortmentNormalProductRule = 1052,
        PlanogramProductDelistedDueToAssortmentInheritanceProductRule = 1053,
        PlanogramProductDelistedDueToAssortmentDistributionProductRule = 1054,
        PlanogramProductInventoryIncreased = 1055,
        PlanogramProductsContainedNoUnplacedProducts = 1056,
        PlanogramUnplacedProductsRemoved = 1057,
        CreateAssortmentSuccessful = 1058,
        CreateAssortmentUpdatedProducts = 1059,
        PlanogramProductInventoryDecreased = 1060,
        PlanogramFixtureBayCreatedFromLocationSpace = 1061,
        PlanogramFixtureBayElementCreatedFromLocationSpace = 1062,
        PlanogramPerformanceDataNoMetricProfile = 1063,
        PlanogramPositionUnableToFurtherDecreaseInventory = 1064,
        PlanogramNameTemplateApplied = 1065,
        PlanogramComparisonTemplateApplied = 1066,
        PlanogramProductInventoryDecreasedBy = 1067,
        PlanogramPositionUnableToDecreaseInventory = 1068,
        PlanogramRecalculateAssortmentInventoryRuleApplied = 1069,
        PlanogramPerformanceDataProductBuddyApplied = 1070,
        PlanogramPerformanceDataLocationBuddyApplied = 1071,
        PlanogramFixtureBayRemovedFromLocationSpace = 1072,
        PlanogramFixtureBayElementRemovedFromLocationSpace = 1073,
        PlanogramFixtureBayElementUpdatedFromLocationSpace = 1074,
        PlanogramFixtureBayUpdatedFromLocationSpace = 1075,
        PlanogramComponentsRemoved = 1076,
        PlanogramPerformanceDataProductMerged = 1077,
        #endregion

        #region Debug (1100 - 1200)
        PlanogramProductAdded = 1101,
        PlanogramProductRemoved = 1102,
        PlanogramBlockingGroupChanged = 1103,
        PlacedPlanogramProductNotInCdt = 1104,
        UnPlacedPlanogramProductNotInCdt = 1105,
        PlanogramAssortmentProductChanged = 1106,
        PlanogramAssortmentProductPositionsChanged = 1107,
        PlanogramAssortmentProductInventoryChange = 1108,
        ProductInPlanNotInUniverse = 1109,
        #endregion

        #endregion
    }

    /// <summary>
    /// Event Log Event Helper Class
    /// 
    /// Note: If an EventLogEvent is only going to be used in a ProcessException, 
    /// it doesn't need a FriendlyContent, because the LoggingHelper will log the exception's message instead, 
    /// but can you add in FriendlyDescription entries for any new events you create?
    /// </summary>
    public static class EventLogEventHelper
    {
        #region FriendlyDescriptions
        public static readonly Dictionary<EventLogEvent, String> FriendlyDescriptions =
            new Dictionary<EventLogEvent, String>()
            {
                {EventLogEvent.UnexpectedException, Message.Enum_EventLogEvent_UnexpectedException_Description},
                {EventLogEvent.WindowsServiceStart, Message.Enum_EventLogEvent_WindowsServiceStart_Description},
                {EventLogEvent.WindowsServiceStop, Message.Enum_EventLogEvent_WindowsServiceStop_Description},          
                {EventLogEvent.SyncProcessStart, Message.Enum_EventLogEvent_SyncProcessStart_Description},
                {EventLogEvent.SyncProcessEnd, Message.Enum_EventLogEvent_SyncProcessEnd_Description},
                {EventLogEvent.ErrorWhileSyncingData, Message.Enum_EventLogEvent_ErrorWhileSyncingData_Description},
                {EventLogEvent.SyncInformation, Message.Enum_EventLogEvent_SyncInformation_Description},
                {EventLogEvent.MaximumRetryCountReached, Message.Enum_EventLogEvent_MaximumRetryCountReached_Description},
                {EventLogEvent.MaximumFailureCountReached, Message.Enum_EventLogEvent_MaximumFailureCountReached_Description},
                {EventLogEvent.WindowsServiceStopping, Message.Enum_EventLogEvent_WindowsServiceStopping_Description},
                {EventLogEvent.EngineIdle, Message.Enum_EventLogEvent_EngineIdle_Description},
                {EventLogEvent.EngineMessageReaped, Message.Enum_EventLogEvent_EngineMessageReaped_Description},
                {EventLogEvent.EnginePaused, Message.Enum_EventLogEvent_EnginePaused_Description},
                {EventLogEvent.EngineRunning, Message.Enum_EventLogEvent_EngineRunning_Description},
                {EventLogEvent.EngineStarting, Message.Enum_EventLogEvent_EngineStarting_Description},
                {EventLogEvent.EngineStopped, Message.Enum_EventLogEvent_EngineStopped_Description},
                {EventLogEvent.EngineStopping, Message.Enum_EventLogEvent_EngineStopping_Description},
                {EventLogEvent.EngineWorkpackageStarted, Message.Enum_EventLogEvent_EngineWorkpackageStarted_Description},
                {EventLogEvent.EngineWorkpackageFinished, Message.Enum_EventLogEvent_EngineWorkpackageFinished_Description},
                {EventLogEvent.EngineMessageRequeued, Message.Enum_EventLogEvent_EngineMessageRequeued_Description},
                {EventLogEvent.EngineShutdownException, Message.Enum_EventLogEvent_EngineShutdownException_Description},
                {EventLogEvent.PublishStart, Message.Enum_EventLogEvent_PublishStart_Description},
                {EventLogEvent.PublishEnd, Message.Enum_EventLogEvent_PublishEnd_Description},
                {EventLogEvent.PublishInformation, Message.Enum_EventLogEvent_PublishInformation_Description},
                {EventLogEvent.PublishErrorPlan, Message.Enum_EventLogEvent_PublishErrorPlan_Description},
                {EventLogEvent.PublishErrorLinkingToProject, Message.Enum_EventLogEvent_PublishErrorLinkingToProject_Description},
                {EventLogEvent.PublishErrorUnableToLink, Message.Enum_EventLogEvent_PublishErrorUnableToLink_Description},
                {EventLogEvent.PublishPlanogramAssignmentResults, Message.Enum_EventLogEvent_PublishPlanogramAssignmentResults_Description},
                {EventLogEvent.PublishAllAssignmentsFailed, Message.Enum_EventLogEvent_PublishAllAssignmentsFailed_Description},
                {EventLogEvent.InvalidTaskParameterValue, Message.Enum_EventLogEvent_InvalidTaskParameterValue_Description},
                {EventLogEvent.InsufficientPlanogramContent, Message.Enum_EventLogEvent_InsufficientPlanogramContent_Description},
                {EventLogEvent.NothingForTaskToDo, Message.Enum_EventLogEvent_NothingForTaskToDo_Description},
                {EventLogEvent.PlanogramProductAdded, Message.Enum_EventLogEvent_PlanogramProductAdded_Description},
                {EventLogEvent.PlanogramProductRemoved, Message.Enum_EventLogEvent_PlanogramProductRemoved_Description},
                {EventLogEvent.PlanogramProductsAdded, Message.Enum_EventLogEvent_PlanogramProductsAdded_Description},
                {EventLogEvent.PlanogramPositionsAdded, Message.Enum_EventLogEvent_PlanogramPositionsAdded_Description},
                {EventLogEvent.PlanogramProductsToAdd, Message.Enum_EventLogEvent_PlanogramProductsToAdd_Description},
                {EventLogEvent.PlanogramAssortmentReplaced, Message.Enum_EventLogEvent_PlanogramAssortmentReplaced_Description},
                {EventLogEvent.PlanogramBlockingAdded, Message.Enum_EventLogEvent_PlanogramBlockingAdded_Description},
                {EventLogEvent.PlanogramBlockingGroupChanged, Message.Enum_EventLogEvent_PlanogramBlockingGroupChanged_Description},
                {EventLogEvent.PlanogramBlockingApplyToFixturesFailed, Message.Enum_EventLogEvent_PlanogramBlockingApplyToFixturesFailed_Description},
                {EventLogEvent.PlanogramBlockingGroupMerged, Message.Enum_EventLogEvent_PlanogramBlockingGroupMerged_Description},
                {EventLogEvent.PlanogramConsumerDecisionTreeAdded, Message.Enum_EventLogEvent_PlanogramConsumerDecisionTreeAdded_Description},
                {EventLogEvent.PlacedPlanogramProductsNotInCdt, Message.Enum_EventLogEvent_PlacedPlanogramProductsNotInCdt_Description},
                {EventLogEvent.UnPlacedPlanogramProductsNotInCdt, Message.Enum_EventLogEvent_UnPlacedPLanogramProductsNotInCdt_Description},
                {EventLogEvent.PlacedPlanogramProductNotInCdt, Message.Enum_EventLogEvent_PlacedPlanogramProductNotInCdt_Description},
                {EventLogEvent.UnPlacedPlanogramProductNotInCdt, Message.Enum_EventLogEvent_UnPlacedPlanogramProductNotInCdt_Description},
                {EventLogEvent.PlanogramReNumberingStrategyAdded, Message.Enum_EventLogEvent_PlanogramReNumberingStrategyAdded_Description},
                {EventLogEvent.PlanogramValidationTemplateAdded, Message.Enum_EventLogEvent_PlanogramValidationTemplateAdded_Description},
                {EventLogEvent.PlanogramPerformanceDataMissingOrEmpty, Message.Enum_EventLogEvent_PlanogramPerformanceDataMissingOrEmpty_Description},
                {EventLogEvent.PlanogramAssortmentProductChanged, Message.Enum_EventLogEvent_PlanogramAssortmentProductChanged_Description},
                {EventLogEvent.PlanogramAssortmentProductCouldNotBeRanked, Message.Enum_EventLogEvent_PlanogramAssortmentProductCouldNotBeRanked_Description},
                {EventLogEvent.PlanogramAssortmentProductsUnitsDecreased, Message.Enum_EventLogEvent_PlanogramAssortmentProductUnitsDecreased_Description},
                {EventLogEvent.PlanogramAssortmentProductPositionsChanged, Message.Enum_EventLogEvent_PlanogramAssortmentProductPositionsChanged_Description},
                {EventLogEvent.PlanogramProductPositionsRemoved, Message.Enum_EventLogEvent_PlanogramProductPositionsRemoved_Description},
                {EventLogEvent.PlanogramProductPositionsMovedToCarParkShelf, Message.Enum_EventLogEvent_PlanogramProductPositionsMovedToCarParkShelf_Description},
                {EventLogEvent.PlanogramMetadataUpdated, Message.Enum_EventLogEvent_PlanogramMetadataUpdated_Description},
                {EventLogEvent.PlanogramReNumberingStrategyUpdated, Message.Enum_EventLogEvent_PlanogramReNumberingStrategyUpdated_Description},
                {EventLogEvent.PlanogramValidationUpdated, Message.Enum_EventLogEvent_PlanogramValidationUpdated_Description},
                {EventLogEvent.PlanogramPerformanceDataUpdated, Message.Enum_EventLogEvent_PlanogramPerformanceDataUpdated_Description},
                {EventLogEvent.PlanogramPerformanceDataCouldNotBeFetched, Message.Enum_EventLogEvent_PlanogramPerformanceDataCouldNotBeFetched_Description},
                {EventLogEvent.PlanogramAssortmentProductInventoryChange, Message.Enum_EventLogEvent_PlanogramAssortmentProductInventoryChange_Description},
                {EventLogEvent.PlanogramAssortmentProductsUnitsIncreased, Message.Enum_EventLogEvent_PlanogramAssortmentProductUnitsIncreased_Description},
                {EventLogEvent.PlanogramPublishedToGfs, Message.Enum_EventLogEvent_PlanogramPublishedToGfs_Description},
                {EventLogEvent.PlanogramMoved, Message.Enum_EventLogEvent_PlanogramMoved_Description},
                {EventLogEvent.PlanogramCouldNotPublishUnassigned, Message.Enum_EventLogEvent_PlanogramCouldNotPublishUnassigned_Description},
                {EventLogEvent.PlanogramCouldNotPublishProductsDontMatch, Message.Enum_EventLogEvent_PLanogramCouldNotPublishProductsDontMatch_Description},
                {EventLogEvent.PlanogramProductsUpdated, Message.Enum_EventLogEvent_PlanogramProductsUpdated_Description}, 
                {EventLogEvent.PlanogramBlockingInsufficientComponentSpace, Message.Enum_EventLogEvent_PLanogramBlockingInsufficientComponentSpace_Description},
                {EventLogEvent.PlanogramBlockingMissingProducts, Message.Enum_EventLogEvent_PlanogramBlockingMissingProducts_Description}, 
                {EventLogEvent.PlanogramSequenceReplaced, Message.Enum_EventLogEvent_PlanogramSequenceReplaced_Description},  
                {EventLogEvent.PlanogramCategoryUpdated, Message.Enum_EventLogEvent_PlanogramCategoryUpdated_Description}, 
                {EventLogEvent.PlanogramLocationUpdated, Message.Enum_EventLogEvent_PlanogramLocationUpdated_Description}, 
                {EventLogEvent.PlanogramProductsToInsert, Message.Enum_EventLogEvent_PlanogramProductsToInsert_Description},
                {EventLogEvent.PlanogramSequenceMissingProduct, Message.Enum_EventLogEvent_PlanogramSequenceMissingProduct_Description},
                {EventLogEvent.PlanogramBlockingNotAssigned, Message.Enum_EventLogEvent_PlanogramBlockingNotAssigned_Description},
                {EventLogEvent.PlanogramPositionsInserted, Message.Enum_EventLogEvent_PlanogramPositionInserted_Description},
                {EventLogEvent.PlanogramPositionsNotInserted, Message.Enum_EventLogEvent_PlanogramPositionNotInserted_Description},
                {EventLogEvent.PlanogramPerformanceDataInvalidLocationReference, Message.Enum_EventLogEvent_PlanogramPerformanceDataInvalidLocationReference_Description},
                {EventLogEvent.PlanogramPerformanceDataInvalidClusterReference, Message.Enum_EventLogEvent_PlanogramPerformanceDataInvalidClusterReference_Description},
                {EventLogEvent.PlanogramLocationReferenceInvalid, Message.Enum_EventLogEvent_PlanogramLocationReferenceInvalid_Description},
                {EventLogEvent.PlanogramClusterUpdated, Message.Enum_EventLogEvent_PlanogramClusterUpdated_Description},
                {EventLogEvent.PlanogramAttributesUpdated, Message.Enum_EventLogEvent_PlanogramAttributesUpdated_Description},
                { EventLogEvent.ReplacementProductAlreadyOnPlanogram, Message.Enum_EventLogEvent_ReplacementProductAlreadyOnPlanogram_Description },
                { EventLogEvent.ReplacementProductDoesNotExist, Message.Enum_EventLogEvent_ReplacementProductDoesNotExist_Description },
                { EventLogEvent.ReplaceProductDoesNotExist, Message.Enum_EventLogEvent_ReplaceProductDoesNotExist_Description },
                { EventLogEvent.ReplacementProductCouldNotBeAdded, Message.Enum_EventLogEvent_ReplacementProductCouldNotBeAdded_Description },
                { EventLogEvent.PlanogramPositionsReplaced, Message.Enum_EventLogEvent_PlanogramPositionsReplaced_Description },
                { EventLogEvent.PlanogramPositionsReplacedCarPark, Message.Enum_EventLogEvent_PlanogramPositionsReplacedCarPark_Description },
                { EventLogEvent.PlanogramRecalculateAssortmentInventoryUpdated, Message.Enum_EventLogEvent_PlanogramRecalculateAssortmentInventoryUpdated_Description },
                { EventLogEvent.PlanogramNoPerformanceSelectionData, Message.Enum_EventLogEvent_PlanogramNoPerformanceSelectionData_Description },
                { EventLogEvent.PlanogramOverwritten, Message.Enum_EventLogEvent_PlanogramOverwritten_Description },
                {EventLogEvent.PlanogramProductsNotPlaced, Message.Enum_EventLogEvent_PlanogramProductsNotPlaced_Description},
                {EventLogEvent.PlanogramProductsExistInPlanogram, Message.Enum_EventLogEvent_PlanogramProductsExistInPlanogram_Description},
                {EventLogEvent.PlanogramInvalidFileAccess, Message.Enum_EventLogEvent_PlanogramInvalidFileAccess_Description},
                {EventLogEvent.PlanogramPositionsRemoved, Message.Enum_EventLogEvent_PlanogramPositionsRemoved_Description},
                {EventLogEvent.PlanogramProductsAndPositionsRemoved, Message.Enum_EventLogEvent_PlanogramProductsAndPositionsRemoved_Description},
                {EventLogEvent.PlanogramPositionsRemovedCarPark, Message.Enum_EventLogEvent_PlanogramPositionsRemovedCarPark_Description},
                {EventLogEvent.PlanogramDuplicateProductGtin, Message.Enum_EventLogEvent_PlanogramDuplicateProductGtin_Description},
                {EventLogEvent.PlanogramNoAssortmentPresent, Message.Enum_EventLogEvent_PlanogramNoAssortmentPresent_Description},
                {EventLogEvent.PublishProductNotPresentInGfs, Message.Enum_EventLogEvent_PublishProductNotPresentInGfs_Description},
                {EventLogEvent.PlanogramBlockDeservesNoSpace, Message.Enum_EventLogEvent_PlanogramBlockDeservesNoSpace_Description},
                {EventLogEvent.RemoveProductDoesNotExist, Message.Enum_EventLogEvent_RemoveProductDoesNotExist_Description},
                {EventLogEvent.InsertProductNotAdded, Message.Enum_EventLogEvent_InsertProductNotAdded_Description},
                {EventLogEvent.ProductAddedCarPark, Message.Enum_EventLogEvent_ProductAddedCarPark_Description},
                {EventLogEvent.AddProductAlreadyExistsInPlanogram, Message.Enum_EventLogEvent_AddProductAlreadyExistsInPlanogram_Description},
                {EventLogEvent.PlanogramProductBetterPerformanceCouldNotBeAddedDueToSize, Message.Enum_EventLogEvent_PlanogramProductBetterPerfomanceCouldNotBeAddedDueToSize_Description},
                {EventLogEvent.PlanogramBlockingAddedPerformance, Message.Enum_EventLogEvent_PlanogramBlockingAddedPerformance_Description},
                {EventLogEvent.PlanogramLocationAssignmentsInvalid, Message.Enum_EventLogEvent_PlanogramLocationAssignmentsInvalid_Description},
                {EventLogEvent.PlanogramPerformanceDataNoPerformanceSelection, Message.Enum_EventLogEvent_PlanogramPerformanceDataNoPerformanceSelection_Description},
                {EventLogEvent.PlanogramPerformanceDataNoMetricProfile, Message.Enum_EventLogEvent_PlanogramPerformanceDataNoMetricProfile_Description},
                {EventLogEvent.InvalidProductFilter, Message.Enum_EventLogEvent_InvalidProductFilter_Description},
                {EventLogEvent.ProductColoursUpdatedFromHighlight, Message.Enum_EventLogEvent_ProductColoursUpdatedFromHighlight_Description}, 
                {EventLogEvent.PlanogramPerformanceDataNoPlanogramAssignmentReference, Message.Enum_EventLogEvent_PlanogramPerformanceDataNoPlanogramAssignmentReference_Description}, 
                {EventLogEvent.CreateSequenceTemplateInvalidSequencePlacement, Message.Enum_EventLogEvent_CreateSequenceTemplateInvalidSequencePlacement_Description}, 
                {EventLogEvent.CreateSequenceTemplateInvalidSequenceInformation, Message.Enum_EventLogEvent_CreateSequenceTemplateInvalidSequenceInformation_Description}, 
                {EventLogEvent.CreateSequenceTemplateSuccessful, Message.Enum_EventLogEvent_CreateSequenceTemplateSuccessful_Description}, 
                {EventLogEvent.PlanogramNoValidLocation, Message.Enum_EventLogEvent_PlanogramNoValidLocation_Description}, 
                {EventLogEvent.CreateSequenceTemplateMultisitedProduct, Message.Enum_EventLogEvent_CreateSequenceTemplateMultisitedProduct_Description}, 
                {EventLogEvent.PlanogramProductDelistedDueToProductRule, Message.Enum_EventLogEvent_PlanogramProductDelistedDueToProductRule}, 
                {EventLogEvent.PlanogramProductDelistedDueToFamilyRule, Message.Enum_EventLogEvent_PlanogramProductDelistedDueToFamilyRule}, 
                {EventLogEvent.PlanogramProductDelistedDueToLocalProductRule, Message.Enum_EventLogEvent_PlanogramProductDelistedDueToLocalProductRule}, 
                {EventLogEvent.PlanogramProductDelistedDueToAssortmentOptionalProductRule, Message.Enum_EventLogEvent_PlanogramProductDelistedDueToAssortmentOptionalProductRule},
                {EventLogEvent.PlanogramProductDelistedDueToAssortmentNormalProductRule, Message.Enum_EventLogEvent_PlanogramProductDelistedDueToAssortmentNormalProductRule },
                {EventLogEvent.PlanogramProductDelistedDueToAssortmentInheritanceProductRule, Message.Enum_EventLogEvent_PlanogramProductDelistedDueToAssortmentInheritanceProductRule },
                {EventLogEvent.PlanogramProductDelistedDueToAssortmentDistributionProductRule, Message.Enum_EventLogEvent_PlanogramProductDelistedDueToAssortmentDistributionProductRule },
                {EventLogEvent.PlanogramProductInventoryIncreased, Message.Enum_EventLogEvent_PlanogramProductInventoryIncreased_Description},
                {EventLogEvent.PlanogramNoProductsPresent, Message.Enum_EventLogEvent_PlanogramNoProductsPresent_Description},
                {EventLogEvent.PlanogramProductsContainedNoUnplacedProducts, Message.Enum_EventLogEvent_PlanogramProductsContainedNoUnplacedProducts_Description},
                {EventLogEvent.PlanogramUnplacedProductsRemoved, Message.Enum_EventLogEvent_PlanogramUnplacedProductsRemoved_Description},
                {EventLogEvent.ProductsInPlanNotInUniverse, Message.Enum_EventLogEvent_ProductsInPlanNotInUniverse_Description},
                {EventLogEvent.ProductInPlanNotInUniverse, Message.Enum_EventLogEvent_ProductInPlanNotInUniverse_Description},
                {EventLogEvent.CreateAssortmentSuccessful, Message.Enum_EventLogEvent_CreateAssortmentSuccessful_Description},
                {EventLogEvent.CreateAssortmentUpdatedProducts, Message.Enum_EventLogEvent_CreateAssortmentUpdatedProducts_Description},
                {EventLogEvent.PlanogramProductDoesNotExist, Message.Enum_EventLogEvent_PlanogramProductDoesNotExist_Description},
                {EventLogEvent.PlanogramCouldNotBeLoadedFromSourceFile, Message.Enum_EventLogEvent_PlanogramCouldNotBeLoadedFromSourceFile_Description},
                {EventLogEvent.SourceFileLocked, Message.Enum_EventLogEVent_SourceFileLocked_Description},
                {EventLogEvent.MultisitedProductNotSequenced, Message.Enum_EventLogEvent_MultisitedProductNotSequenced_Description}, 
                {EventLogEvent.PlanogramSequenceMultiSitedProduct, Message.Enum_EventLogEvent_PlanogramSequenceMultiSitedProduct_Description},
                {EventLogEvent.PlanogramInsufficientPerformanceDataToRecalculateBlocking, Message.Enum_EventLogEvent_PlanogramInsufficientPerformanceDataToRecalculateBlocking_Description},
                {EventLogEvent.PlanogramProductInventoryDecreased, Message.Enum_EventLogEvent_PlanogramProductInventoryDecreased_Description},
                {EventLogEvent.PlanogramProductInventoryDecreasedNoProductPositions, Message.Enum_EventLogEvent_PlanogramProductInventoryDecreasedNoProductPositions_Description},
                {EventLogEvent.PlanogramFixtureBayMismatchBetweenLocationSpaceData, Message.Enum_EventLogEvent_PlanogramFixtureBayMismatchBetweenLocationSpaceData_Description},
                {EventLogEvent.PlanogramFixtureBayElementMismatchBetweenLocationSpaceData, Message.Enum_EventLogEvent_PlanogramFixtureBayElementMismatchBetweenLocationSpaceData_Description},
                {EventLogEvent.LocationSpaceBayElementDataMissing, Message.Enum_EventLogEvent_LocationSpaceBayElementDataMissing_Description},
                {EventLogEvent.PlanogramPerformanceMetricMismatchBetweenMetricProfile, Message.Enum_EventLogEvent_PlanogramPerformanceMetricMismatchBetweenMetricProfile_Description},
                {EventLogEvent.PlanogramFixtureBayCreatedFromLocationSpace, Message.Enum_EventLogEvent_PlanogramFixtureBayCreatedFromLocationSpace_Description},
                {EventLogEvent.PlanogramFixtureBayElementCreatedFromLocationSpace, Message.Enum_EventLogEvent_PlanogramFixtureBayElementCreatedFromLocationSpace_Description},
                {EventLogEvent.PlanogramPositionUnableToFurtherDecreaseInventory, Message.Enum_EventLogEvent_PlanogramPositionUnableToFurtherDecreaseInventory_Description},
                {EventLogEvent.PlanogramNameTemplateApplied, Message.Enum_EventLogEvent_PlanogramNameTemplateApplied_Description},
                {EventLogEvent.PlanogramNameTemplateFormulaCouldNotBeResolved, Message.Enum_EventLogEvent_PlanogramNameTemplateFormulaCouldNotBeResolved_Description},
                {EventLogEvent.PlanogramComparisonTemplateApplied, Message.Enum_EventLogEvent_PlanogramComparisonTemplateApplied_Description},
                {EventLogEvent.PlanogramProductInventoryDecreasedBy, Message.Enum_EventLogEvent_PlanogramProductInventoryDecreasedBy_Description},
                {EventLogEvent.PlanogramPositionUnableToDecreaseInventory, Message.Enum_EventLogEvent_PlanogramPositionUnableToDecreaseInventory_Description},
                {EventLogEvent.PlanogramRecalculateAssortmentInventoryRuleApplied, Message.Enum_EventLogEvent_PlanogramRecalculateAssortmentInventoryRuleApplied_Description},
                {EventLogEvent.PlanogramPerformanceDataProductBuddyApplied, Message.Enum_EventLogEvent_PlanogramPerformanceDataProductBuddyApplied_Description},
                {EventLogEvent.PlanogramPerformanceDataLocationBuddyApplied, Message.Enum_EventLogEvent_PlanogramPerformanceDataLocationBuddyApplied_Description},
                {EventLogEvent.PlanogramFixtureBayRemovedFromLocationSpace, Message.Enum_EventLogEvent_PlanogramFixtureBayRemovedFromLocationSpace_Description},
                {EventLogEvent.PlanogramFixtureBayElementRemovedFromLocationSpace, Message.Enum_EventLogEvent_PlanogramFixtureBayElementRemovedFromLocationSpace_Description},
                {EventLogEvent.PlanogramFixtureBayElementUpdatedFromLocationSpace, Message.Enum_EventLogEvent_PlanogramFixtureBayElementUpdatedFromLocationSpace_Description},
                {EventLogEvent.PlanogramFixtureBayUpdatedFromLocationSpace, Message.Enum_EventLogEvent_PlanogramFixtureBayUpdatedFromLocationSpace_Description},
                {EventLogEvent.PlanogramComponentsRemoved, Message.Enum_EventLogEven_PlanogramComponentsRemoved_Description},
                {EventLogEvent.PlanogramPerformanceDataProductMerged, Message.Enum_EventLogEvent_PlanogramPerformanceDataProductMerged_Description},
                {EventLogEvent.PlanogramComponentFailedToGetRepresentationForBlock, Message.Enum_EventLogEvent_PlanogramComponentFailedToGetRepresentationForBlock}
            };
        #endregion
        
        #region FriendlyContents
        public static readonly Dictionary<EventLogEvent, String> FriendlyContents =
            new Dictionary<EventLogEvent, String>()
            {
                {EventLogEvent.UnexpectedException, Message.Enum_EventLogEvent_UnexpectedException_Content},
                {EventLogEvent.WindowsServiceStart, Message.Enum_EventLogEvent_WindowsServiceStart_Content},
                {EventLogEvent.WindowsServiceStop, Message.Enum_EventLogEvent_WindowsServiceStop_Content},
                {EventLogEvent.SyncProcessStart, Message.Enum_EventLogEvent_SyncProcessStart_Content},
                {EventLogEvent.SyncProcessEnd, Message.Enum_EventLogEvent_SyncProcessEnd_Content},
                {EventLogEvent.ErrorWhileSyncingData, Message.Enum_EventLogEvent_ErrorWhileSyncingData_Content},
                {EventLogEvent.SyncInformation, Message.Enum_EventLogEvent_SyncInformation_Content},
                {EventLogEvent.MaximumRetryCountReached, Message.Enum_EventLogEvent_MaximumRetryCountReached_Content},
                {EventLogEvent.MaximumFailureCountReached, Message.Enum_EventLogEvent_MaximumFailureCountReached_Content},
                {EventLogEvent.WindowsServiceStopping, Message.Enum_EventLogEvent_WindowsServiceStopping_Content},
                {EventLogEvent.EngineIdle, Message.Enum_EventLogEvent_EngineIdle_Content},
                {EventLogEvent.EngineMessageReaped, Message.Enum_EventLogEvent_EngineMessageReaped_Content},
                {EventLogEvent.EnginePaused, Message.Enum_EventLogEvent_EnginePaused_Content},
                {EventLogEvent.EngineRunning, Message.Enum_EventLogEvent_EngineRunning_Content},
                {EventLogEvent.EngineStarting, Message.Enum_EventLogEvent_EngineStarting_Content},
                {EventLogEvent.EngineStopped, Message.Enum_EventLogEvent_EngineStopped_Content},
                {EventLogEvent.EngineStopping, Message.Enum_EventLogEvent_EngineStopping_Content},
                {EventLogEvent.EngineWorkpackageStarted, Message.Enum_EventLogEvent_EngineWorkpackageStarted_Content},
                {EventLogEvent.EngineWorkpackageFinished, Message.Enum_EventLogEvent_EngineWorkpackageFinished_Content},
                {EventLogEvent.EngineMessageRequeued, Message.Enum_EventLogEvent_EngineMessageRequeued_Content},
                {EventLogEvent.EngineShutdownException, Message.Enum_EventLogEvent_EngineShutdownException_Content},
                {EventLogEvent.PublishStart, Message.Enum_EventLogEvent_PublishStart_Content},
                {EventLogEvent.PublishEnd, Message.Enum_EventLogEvent_PublishEnd_Content},
                {EventLogEvent.PublishInformation, Message.Enum_EventLogEvent_PublishInformation_Content},
                {EventLogEvent.PublishErrorPlan, Message.Enum_EventLogEvent_PublishErrorPlan_Content},
                {EventLogEvent.PublishErrorLinkingToProject, Message.Enum_EventLogEvent_PublishErrorLinkingToProject_Content},
                {EventLogEvent.PublishErrorUnableToLink, Message.Enum_EventLogEvent_PublishErrorUnableToLink_Content},   
                {EventLogEvent.PublishPlanogramAssignmentResults, Message.Enum_EventLogEvent_PublishPlanogramAssignmentResults_Content},
                {EventLogEvent.PublishAllAssignmentsFailed, Message.Enum_EventLogEvent_PublishAllAssignmentsFailed_Content},
                {EventLogEvent.InvalidTaskParameterValue, Message.Enum_EventLogEvent_InvalidTaskParameterValue_Content },
                {EventLogEvent.InsufficientPlanogramContent, Message.Enum_EventLogEvent_InsufficientPlanogramContent_Content},
                {EventLogEvent.NothingForTaskToDo, Message.Enum_EventLogEvent_NothingForTaskToDo_Content},
                {EventLogEvent.PlanogramProductAdded, Message.Enum_EventLogEvent_PlanogramProductAdded_Content},
                {EventLogEvent.PlanogramProductRemoved, Message.Enum_EventLogEvent_PlanogramProductRemoved_Content},
                {EventLogEvent.PlanogramProductsAdded, Message.Enum_EventLogEvent_PlanogramProductsAdded_Content},
                {EventLogEvent.PlanogramPositionsAdded, Message.Enum_EventLogEvent_PlanogramPositionsAdded_Content},
                {EventLogEvent.PlanogramProductsToAdd, Message.Enum_EventLogEvent_PlanogramProductsToAdd_Content},
                {EventLogEvent.PlanogramAssortmentReplaced, Message.Enum_EventLogEvent_PlanogramAssortmentReplaced_Content},
                {EventLogEvent.PlanogramBlockingAdded, Message.Enum_EventLogEvent_PlanogramBlockingAdded_Content},
                {EventLogEvent.PlanogramBlockingGroupChanged, Message.Enum_EventLogEvent_PlanogramBlockingGroupChanged_Content},
                {EventLogEvent.PlanogramBlockingApplyToFixturesFailed, Message.Enum_EventLogEvent_PlanogramBlockingApplyToFixturesFailed_Content},
                {EventLogEvent.PlanogramBlockingGroupMerged, Message.Enum_EventLogEvent_PlanogramBlockingGroupMerged_Content},
                {EventLogEvent.PlanogramConsumerDecisionTreeAdded, Message.Enum_EventLogEvent_PlanogramConsumerDecisionTreeAdded_Content},
                {EventLogEvent.PlacedPlanogramProductsNotInCdt, Message.Enum_EventLogEvent_PlacedPlanogramProductsNotInCdt_Content},
                {EventLogEvent.UnPlacedPlanogramProductsNotInCdt, Message.Enum_EventLogEvent_UnPlacedPlanogramProductsNotInCdt_Content},
                {EventLogEvent.PlacedPlanogramProductNotInCdt, Message.Enum_EventLogEvent_PlacedPLanogramProductNotInCdt_Content},
                {EventLogEvent.UnPlacedPlanogramProductNotInCdt, Message.Enum_EventLogEvent_UnPlacedPlanogramProductNotInCdt_Content},
                {EventLogEvent.PlanogramReNumberingStrategyAdded, Message.Enum_EventLogEvent_PlanogramReNumberingStrategyAdded_Content},
                {EventLogEvent.PlanogramValidationTemplateAdded, Message.Enum_EventLogEvent_PlanogramValidationTemplateAdded_Content},
                {EventLogEvent.PlanogramPerformanceDataMissingOrEmpty, Message.Enum_EventLogEvent_PlanogramPerformanceDataMissingOrEmpty_Content},
                {EventLogEvent.PlanogramAssortmentProductChanged, Message.Enum_EventLogEvent_PlanogramAssortmentProductChanged_Content},
                {EventLogEvent.PlanogramAssortmentProductCouldNotBeRanked, Message.Enum_EventLogEvent_PlanogramAssortmentProductCouldNotBeRanked_Content},
                {EventLogEvent.PlanogramAssortmentProductsUnitsDecreased, Message.Enum_EventLogEvent_PlanogramAssortmentProductsUnitsDecreased_Content},
                {EventLogEvent.PlanogramAssortmentProductPositionsChanged, Message.Enum_EventLogEvent_PlanogramAssortmentProductPositionsChanged_Content},
                {EventLogEvent.PlanogramProductPositionsRemoved, Message.Enum_EventLogEvent_PlanogramProductPositionsRemoved_Content},
                {EventLogEvent.PlanogramProductPositionsMovedToCarParkShelf, Message.Enum_EventLogEvent_PlanogramProductPositionsMovedToCarParkShelf_Content},
                {EventLogEvent.PlanogramMetadataUpdated, Message.Enum_EventLogEvent_PlanogramMetadataUpdated_Content},
                {EventLogEvent.PlanogramReNumberingStrategyUpdated, Message.Enum_EventLogEvent_PlanogramReNumberingStrategyUpdated_Content},
                {EventLogEvent.PlanogramValidationUpdated, Message.Enum_EventLogEvent_PlanogramValidationUpdated_Content},
                {EventLogEvent.PlanogramPerformanceDataUpdated, Message.Enum_EventLogEvent_PlanogramPerformanceDataUpdated_Content},
                {EventLogEvent.PlanogramPerformanceDataCouldNotBeFetched, Message.Enum_EventLogEvent_PlanogramPerformanceDataCouldNotBeFetched_Content},
                {EventLogEvent.PlanogramAssortmentProductInventoryChange, Message.Enum_EventLogEvent_PlanogramAssortmentProductInventoryChange_Content},
                {EventLogEvent.PlanogramAssortmentProductsUnitsIncreased, Message.Enum_EventLogEvent_PlanogramAssortmentProductsUnitsIncreased_Content},
                {EventLogEvent.PlanogramPublishedToGfs, Message.Enum_EventLogEvent_PlanogramPublishedToGfs_Content},
                {EventLogEvent.PlanogramMoved, Message.Enum_EventLogEvent_PlanogramMoved_Content},
                {EventLogEvent.PlanogramCouldNotPublishUnassigned, Message.Enum_EventLogEvent_PlanogramCouldNotPublishUnassigned_Content},
                {EventLogEvent.PlanogramCouldNotPublishProductsDontMatch, Message.Enum_EventLogEvent_PlanogramCouldNotPublishProductsDontMatch_Content},
                {EventLogEvent.PlanogramProductsUpdated, Message.Enum_EventLogEvent_PlanogramProductsUpdated_Content}, 
                {EventLogEvent.PlanogramBlockingInsufficientComponentSpace, Message.Enum_EventLogEvent_PLanogramBlockingInsufficientComponentSpace_Content},
                {EventLogEvent.PlanogramBlockingMissingProducts, Message.Enum_EventLogEvent_PlanogramBlockingMissingProducts_Content}, 
                {EventLogEvent.PlanogramSequenceReplaced, Message.Enum_EventLogEvent_PlanogramSequenceReplaced_Content}, 
                {EventLogEvent.PlanogramCategoryUpdated, Message.Enum_EventLogEvent_PlanogramCategoryUpdated_Content}, 
                {EventLogEvent.PlanogramLocationUpdated, Message.Enum_EventLogEvent_PlanogramLocationUpdated_Content},
                {EventLogEvent.PlanogramProductsToInsert, Message.Enum_EvenLogEvent_PlanogramProductsToInsert_Content},
                {EventLogEvent.PlanogramSequenceMissingProduct, Message.Enum_EventLogEvent_PlanogramSequenceMissingProduct_Content},
                {EventLogEvent.PlanogramBlockingNotAssigned, Message.Enum_EventLogEvent_PlanogramBlockingNotAssigned_Content},
                {EventLogEvent.PlanogramPositionsInserted, Message.Enum_EventLogEvent_PlanogramPositionsInserted_Content},
                {EventLogEvent.PlanogramPositionsNotInserted, Message.Enum_EventLogEvent_PlanogramPositionsNotInserted_Content},
                {EventLogEvent.PlanogramPerformanceDataInvalidLocationReference, Message.Enum_EventLogEvent_PlanogramPerformanceDataInvalidLocationReference_Content},
                {EventLogEvent.PlanogramPerformanceDataInvalidClusterReference, Message.Enum_EventLogEvent_PlanogramPerformanceDataInvalidClusterReference_Content},
                {EventLogEvent.PlanogramLocationReferenceInvalid, Message.Enum_EventLogEvent_PlanogramLocationReferenceInvalid_Content},
                {EventLogEvent.PlanogramClusterUpdated, Message.Enum_EventLogEvent_PlanogramClusterUpdated_Content},
                {EventLogEvent.PlanogramAttributesUpdated, Message.Enum_EventLogEvent_PlanogramAttributesUpdated_Content},
                { EventLogEvent.ReplacementProductAlreadyOnPlanogram, Message.Enum_EventLogEvent_ReplacementProductAlreadyOnPlanogram_Content },
                { EventLogEvent.ReplacementProductDoesNotExist, Message.Enum_EventLogEvent_ReplacementProductDoesNotExist_Content },
                { EventLogEvent.ReplaceProductDoesNotExist, Message.Enum_EventLogEvent_ReplaceProductDoesNotExist_Content },
                { EventLogEvent.ReplacementProductCouldNotBeAdded, Message.Enum_EventLogEvent_ReplacementProductCouldNotBeAdded_Content },
                { EventLogEvent.PlanogramPositionsReplaced, Message.Enum_EventLogEvent_PlanogramPositionsReplaced_Content },
                { EventLogEvent.PlanogramPositionsReplacedCarPark, Message.Enum_EventLogEvent_PlanogramPositionsReplacedCarPark_Content },
                { EventLogEvent.PlanogramRecalculateAssortmentInventoryUpdated, Message.Enum_EventLogEvent_PlanogramRecalculateAssortmentInventoryUpdated_Content },
                { EventLogEvent.PlanogramNoPerformanceSelectionData, Message.Enum_EventLogEvent_PlanogramNoPerformanceSelectionData_Content },
                { EventLogEvent.PlanogramOverwritten, Message.Enum_EventLogEvent_PlanogramOverwritten_Content },
                {EventLogEvent.PlanogramProductsNotPlaced, Message.Enum_EventLogEvent_PlanogramProductsNotPlaced_Content},
                {EventLogEvent.PlanogramProductsExistInPlanogram, Message.Enum_EventLogEvent_PlanogramProductsExistInPlanogram_Content},
                {EventLogEvent.PlanogramInvalidFileAccess, Message.Enum_EventLogEvent_PlanogramInvalidFileAccess_Content},
                {EventLogEvent.PlanogramPositionsRemoved, Message.Enum_EventLogEvent_PlanogramPositionsRemoved_Content},
                {EventLogEvent.PlanogramProductsAndPositionsRemoved, Message.Enum_EventLogEvent_PlanogramProductsAndPositionsRemoved_Content},
                {EventLogEvent.PlanogramPositionsRemovedCarPark, Message.Enum_EventLogEvent_PlanogramPositionsRemovedCarPark_Content},
                {EventLogEvent.PlanogramDuplicateProductGtin, Message.Enum_EventLogEvent_PlanogramDuplicateProductGtin_Content},
                {EventLogEvent.PlanogramNoAssortmentPresent, Message.Enum_EventLogEvent_PlanogramNoAssortmentPresent_Content},
                {EventLogEvent.PublishProductNotPresentInGfs, Message.Enum_EventLogEvent_PublishProductNotPresentInGfs_Content},
                {EventLogEvent.PlanogramBlockDeservesNoSpace, Message.Enum_EventLogEvent_PlanogramBlockDeservesNoSpace_Content},
                {EventLogEvent.RemoveProductDoesNotExist, Message.Enum_EventLogEvent_RemoveProductDoesNotExist_Content},
                {EventLogEvent.InsertProductNotAdded, Message.Enum_EventLogEvent_InsertProductNotAdded_Content},
                {EventLogEvent.ProductAddedCarPark, Message.Enum_EventLogEvent_ProductAddedCarPark_Content},
                {EventLogEvent.AddProductAlreadyExistsInPlanogram, Message.Enum_EventLogEvent_AddProductAlreadyExistsInPlanogram_Content},
                {EventLogEvent.PlanogramProductBetterPerformanceCouldNotBeAddedDueToSize, Message.Enum_EventLogEvent_PlanogramProductBetterPerfomanceCouldNotBeAddedDueToSize_Content},
                {EventLogEvent.PlanogramBlockingAddedPerformance, Message.Enum_EventLogEvent_PlanogramBlockingAddedPerformance_Content},
                {EventLogEvent.PlanogramLocationAssignmentsInvalid, Message.Enum_EventLogEvent_PlanogramLocationAssignmentsInvalid_Content},
                {EventLogEvent.PlanogramPerformanceDataNoPerformanceSelection, Message.Enum_EventLogEvent_PlanogramPerformanceDataNoPerformanceSelection_Content},
                {EventLogEvent.PlanogramPerformanceDataNoMetricProfile, Message.Enum_EventLogEvent_PlanogramPerformanceDataNoMetricProfile_Content},
                {EventLogEvent.InvalidProductFilter, Message.Enum_EventLogEvent_InvalidProductFilter_Content}, 
                {EventLogEvent.ProductColoursUpdatedFromHighlight, Message.Enum_EventLogEvent_ProductColoursUpdatedFromHighlight_Content}, 
                {EventLogEvent.PlanogramPerformanceDataNoPlanogramAssignmentReference, Message.Enum_EventLogEvent_PlanogramPerformanceDataNoPlanogramAssignmentReference_Content},
                {EventLogEvent.CreateSequenceTemplateInvalidSequencePlacement, Message.Enum_EventLogEvent_CreateSequenceTemplateInvalidSequencePlacement_Content}, 
                {EventLogEvent.CreateSequenceTemplateInvalidSequenceInformation, Message.Enum_EventLogEvent_CreateSequenceTemplateInvalidSequenceInformation_Content}, 
                {EventLogEvent.CreateSequenceTemplateSuccessful, Message.Enum_EventLogEvent_CreateSequenceTemplateSuccessful_Content},  
                {EventLogEvent.PlanogramNoValidLocation, Message.Enum_EventLogEvent_PlanogramNoValidLocation_Content},
                {EventLogEvent.CreateSequenceTemplateMultisitedProduct, Message.Enum_EventLogEvent_CreateSequenceTemplateMultisitedProduct_Content},
                {EventLogEvent.PlanogramProductDelistedDueToProductRule, Message.Enum_EventLogEvent_PlanogramProductDelistedDueToProductRule_Content}, 
                {EventLogEvent.PlanogramProductDelistedDueToFamilyRule, Message.Enum_EventLogEvent_PlanogramProductDelistedDueToFamilyRule_Content}, 
                {EventLogEvent.PlanogramProductDelistedDueToLocalProductRule, Message.Enum_EventLogEvent_PlanogramProductDelistedDueToLocalProductRule_Content},
                {EventLogEvent.PlanogramProductDelistedDueToAssortmentOptionalProductRule, Message.Enum_EventLogEvent_PlanogramProductDelistedDueToAssortmentOptionalProductRule_Content},
                {EventLogEvent.PlanogramProductDelistedDueToAssortmentNormalProductRule, Message.Enum_EventLogEvent_PlanogramProductDelistedDueToAssortmentNormalProductRule_Content },
                {EventLogEvent.PlanogramProductDelistedDueToAssortmentInheritanceProductRule, Message.Enum_EventLogEvent_PlanogramProductDelistedDueToAssortmentInheritanceProductRule_Content },
                {EventLogEvent.PlanogramProductDelistedDueToAssortmentDistributionProductRule, Message.Enum_EventLogEvent_PlanogramProductDelistedDueToAssortmentDistributionProductRule_Content },
                {EventLogEvent.PlanogramProductInventoryIncreased, Message.Enum_EventLogEvent_PlanogramProductInventoryIncreased_Content},   
                {EventLogEvent.PlanogramNoProductsPresent, Message.Enum_EventLogEvent_PlanogramNoProductsPresent_Content},
                {EventLogEvent.PlanogramProductsContainedNoUnplacedProducts, Message.Enum_EventLogEvent_PlanogramProductsContainedNoUnplacedProducts_Content},
                {EventLogEvent.PlanogramUnplacedProductsRemoved, Message.Enum_EventLogEvent_PlanogramUnplacedProductsRemoved_Content},
                {EventLogEvent.ProductsInPlanNotInUniverse, Message.Enum_EventLogEvent_ProductsInPlanNotInUniverse_Content},
                {EventLogEvent.ProductInPlanNotInUniverse, Message.Enum_EventLogEvent_ProductInPlanNotInUniverse_Content},
                {EventLogEvent.CreateAssortmentSuccessful, Message.Enum_EventLogEvent_CreateAssortmentSuccessful_Content},
                {EventLogEvent.CreateAssortmentUpdatedProducts, Message.Enum_EventLogEvent_CreateAssortmentUpdatedProducts_Content},
                {EventLogEvent.PlanogramProductDoesNotExist, Message.Enum_EventLogEvent_PlanogramProductDoesNotExist_Content},
                {EventLogEvent.PlanogramCouldNotBeLoadedFromSourceFile, Message.Enum_EventLogEvent_PlanogramCouldNotBeLoadedFromSourceFile_Content},
                {EventLogEvent.SourceFileLocked, Message.Enum_EventLogEVent_SourceFileLocked_Content},
                {EventLogEvent.MultisitedProductNotSequenced, Message.Enum_EventLogEvent_MultisitedProductNotSequenced_Content}, 
                {EventLogEvent.PlanogramSequenceMultiSitedProduct, Message.Enum_EventLogEvent_PlanogramSequenceMultiSitedProduct_Content},
                {EventLogEvent.PlanogramInsufficientPerformanceDataToRecalculateBlocking, Message.Enum_EventLogEvent_PlanogramInsufficientPerformanceDataToRecalculateBlocking_Content},
                {EventLogEvent.PlanogramProductInventoryDecreased, Message.Enum_EventLogEvent_PlanogramProductInventoryDecreased_Content},
                {EventLogEvent.PlanogramProductInventoryDecreasedNoProductPositions, Message.Enum_EventLogEvent_PlanogramProductInventoryDecreasedNoProductPositions_Content},
                {EventLogEvent.PlanogramFixtureBayMismatchBetweenLocationSpaceData, Message.Enum_EventLogEvent_PlanogramFixtureBayMismatchBetweenLocationSpaceData_Content},
                {EventLogEvent.PlanogramFixtureBayElementMismatchBetweenLocationSpaceData, Message.Enum_EventLogEvent_PlanogramFixtureBayElementMismatchBetweenLocationSpaceData_Content},
                {EventLogEvent.PlanogramPerformanceMetricMismatchBetweenMetricProfile, Message.Enum_EventLogEvent_PlanogramPerformanceMetricMismatchBetweenMetricProfile_Content},
                { EventLogEvent.LocationSpaceBayElementDataMissing, Message.Enum_EventLogEvent_LocationSpaceBayElementDataMissing_Content},
                {EventLogEvent.PlanogramFixtureBayCreatedFromLocationSpace, Message.Enum_EventLogEvent_PlanogramFixtureBayCreatedFromLocationSpace_Content},
                {EventLogEvent.PlanogramFixtureBayElementCreatedFromLocationSpace, Message.Enum_EventLogEvent_PlanogramFixtureBayElementCreatedFromLocationSpace_Content},
                {EventLogEvent.PlanogramPositionUnableToFurtherDecreaseInventory, Message.Enum_EventLogEvent_PlanogramPositionUnableToFurtherDecreaseInventory_Content},
                {EventLogEvent.PlanogramNameTemplateApplied, Message.Enum_EventLogEvent_PlanogramNameTemplateApplied_Content},
                {EventLogEvent.PlanogramNameTemplateFormulaCouldNotBeResolved, Message.Enum_EventLogEvent_PlanogramNameTemplateFormulaCouldNotBeResolved_Content},
                {EventLogEvent.PlanogramComparisonTemplateApplied, Message.Enum_EventLogEvent_PlanogramComparisonTemplateApplied_Content},
                {EventLogEvent.PlanogramProductInventoryDecreasedBy, Message.Enum_EventLogEvent_PlanogramProductInventoryDecreasedBy_Content},
                {EventLogEvent.PlanogramPositionUnableToDecreaseInventory, Message.Enum_EventLogEvent_PlanogramPositionUnableToDecreaseInventory_Content},
                {EventLogEvent.PlanogramRecalculateAssortmentInventoryRuleApplied, Message.Enum_EventLogEvent_PlanogramRecalculateAssortmentInventoryRuleApplied_Content},
                {EventLogEvent.PlanogramPerformanceDataProductBuddyApplied, Message.Enum_EventLogEvent_PlanogramPerformanceDataProductBuddyApplied_Content},
                {EventLogEvent.PlanogramPerformanceDataLocationBuddyApplied, Message.Enum_EventLogEvent_PlanogramPerformanceDataLocationBuddyApplied_Content},
                {EventLogEvent.PlanogramFixtureBayRemovedFromLocationSpace, Message.Enum_EventLogEvent_PlanogramFixtureBayRemovedFromLocationSpace_Content},
                {EventLogEvent.PlanogramFixtureBayElementRemovedFromLocationSpace, Message.Enum_EventLogEvent_PlanogramFixtureBayElementRemovedFromLocationSpace_Content},
                {EventLogEvent.PlanogramFixtureBayElementUpdatedFromLocationSpace, Message.Enum_EventLogEvent_PlanogramFixtureBayElementUpdatedFromLocationSpace_Content},
                {EventLogEvent.PlanogramFixtureBayUpdatedFromLocationSpace, Message.Enum_EventLogEvent_PlanogramFixtureBayUpdatedFromLocationSpace_Content},
                {EventLogEvent.PlanogramComponentsRemoved, Message.Enum_EventLogEven_PlanogramComponentsRemoved_Content},
                {EventLogEvent.PlanogramPerformanceDataProductMerged, Message.Enum_EventLogEvent_PlanogramPerformanceDataProductMerged_Content},
                {EventLogEvent.PlanogramComponentFailedToGetRepresentationForBlock, Message.Enum_EventLogEvent_PlanogramComponentFailedToGetRepresentationForBlock_Content}
            };
        #endregion

        /// <summary>
        /// Gets the Planogram Event Type from the given EventLogEvent. This is a manual mapping.
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public static PlanogramEventLogEventType GetPlanogramEventType(EventLogEvent eventId)
        {
            switch (eventId)
            {
                // Additions
                case EventLogEvent.PlanogramPositionsAdded:
                case EventLogEvent.PlanogramProductsAdded:
                case EventLogEvent.PlanogramProductAdded:
                case EventLogEvent.PlanogramBlockingAdded:
                case EventLogEvent.PlanogramBlockingAddedPerformance:
                case EventLogEvent.PlanogramConsumerDecisionTreeAdded:
                case EventLogEvent.PlanogramReNumberingStrategyAdded:
                case EventLogEvent.PlanogramValidationTemplateAdded:
                    return PlanogramEventLogEventType.Addition;

                // Modifications
                case EventLogEvent.PlanogramBlockingGroupChanged:
                case EventLogEvent.PlanogramAssortmentReplaced:
                case EventLogEvent.PlanogramAssortmentProductChanged:
                case EventLogEvent.PlanogramAssortmentProductInventoryChange:
                case EventLogEvent.PlanogramBlockingGroupMerged:
                case EventLogEvent.PlanogramAssortmentProductsUnitsDecreased:
                case EventLogEvent.PlanogramProductPositionsMovedToCarParkShelf:
                case EventLogEvent.PlanogramMetadataUpdated:
                case EventLogEvent.PlanogramValidationUpdated:
                case EventLogEvent.PlanogramPerformanceDataUpdated:
                case EventLogEvent.PlanogramAssortmentProductPositionsChanged:
                case EventLogEvent.PlanogramProductsUpdated:
                case EventLogEvent.PlanogramSequenceReplaced:
                case EventLogEvent.PlanogramCategoryUpdated:
                case EventLogEvent.PlanogramLocationUpdated:
                case EventLogEvent.ProductColoursUpdatedFromHighlight:
                case EventLogEvent.PlanogramProductInventoryIncreased:
                case EventLogEvent.PlanogramRecalculateAssortmentInventoryRuleApplied:
                case EventLogEvent.PlanogramPerformanceDataProductBuddyApplied:
                case EventLogEvent.PlanogramPerformanceDataLocationBuddyApplied:
                    return PlanogramEventLogEventType.Modification;

                // Removals
                case EventLogEvent.PlanogramProductsRemoved:
                case EventLogEvent.PlanogramProductRemoved:
                case EventLogEvent.PlanogramProductPositionsRemoved:
                case EventLogEvent.PlanogramComponentsRemoved:
                    return PlanogramEventLogEventType.Removal;

                default:
                    return PlanogramEventLogEventType.General;
            }
        }

        /// <summary>
        /// Returns the score for the given event id.
        /// </summary>
        public static Byte GetScore(EventLogEvent eventId)
        {
            switch (eventId)
            {
                // Generic messages
                case EventLogEvent.UnexpectedException: return 5;
                case EventLogEvent.AuthenticationFailure: return 5;
                case EventLogEvent.AuthorizationFailure: return 5;

                // Windows Services (generic code): 100-range
                case EventLogEvent.WindowsServiceStart: return 0;
                case EventLogEvent.WindowsServiceStop: return 0;
                case EventLogEvent.WindowsServiceStopping: return 0;

                // General Processing 
                case EventLogEvent.FileInUse: return 1;

                // CCM Engine
                case EventLogEvent.EngineStarting: return 0;
                case EventLogEvent.EngineRunning: return 0;
                case EventLogEvent.EnginePaused: return 0;
                case EventLogEvent.EngineStopping: return 0;
                case EventLogEvent.EngineStopped: return 0;
                case EventLogEvent.EngineIdle: return 0;
                case EventLogEvent.EngineMessageReaped: return 0;
                case EventLogEvent.EngineMessageRequeued: return 0;
                case EventLogEvent.EngineShutdownException: return 5;
                case EventLogEvent.EngineWorkpackageStarted: return 0;
                case EventLogEvent.EngineWorkpackageFinished: return 0;

                //Sync Service
                case EventLogEvent.SyncProcessStart: return 0;
                case EventLogEvent.SyncProcessEnd: return 0;
                case EventLogEvent.ErrorWhileSyncingData: return 4;
                case EventLogEvent.SyncInformation: return 0;
                case EventLogEvent.MaximumRetryCountReached: return 3;
                case EventLogEvent.MaximumFailureCountReached: return 3;

                //Publishing
                case EventLogEvent.PublishStart: return 0;
                case EventLogEvent.PublishEnd: return 0;
                case EventLogEvent.PublishInformation: return 0;
                case EventLogEvent.PublishErrorPlan: return 5;
                case EventLogEvent.PublishErrorLinkingToProject: return 5;
                case EventLogEvent.PublishErrorUnableToLink: return 3;
                case EventLogEvent.PublishPlanogramAssignmentResults: return 5;
                case EventLogEvent.PublishAllAssignmentsFailed: return 5;
                case EventLogEvent.PlanogramLocationAssignmentsInvalid: return 5;

                // Errors (800-900)
                case EventLogEvent.InvalidTaskParameterValue: return 5;
                case EventLogEvent.InsufficientPlanogramContent: return 5;
                case EventLogEvent.PlanogramBlockingApplyToFixturesFailed: return 5;
                case EventLogEvent.PlanogramPerformanceDataCouldNotBeFetched: return 5;
                case EventLogEvent.PlanogramCouldNotPublishProductsDontMatch: return 5;
                case EventLogEvent.PlanogramBlockingInsufficientComponentSpace: return 5;
                case EventLogEvent.ReplacementProductAlreadyOnPlanogram: return 1;
                case EventLogEvent.ReplacementProductDoesNotExist: return 1;
                case EventLogEvent.ReplaceProductDoesNotExist: return 1;
                case EventLogEvent.ReplacementProductCouldNotBeAdded: return 2;
                case EventLogEvent.PlanogramInvalidFileAccess: return 5;
                case EventLogEvent.PlanogramDuplicateProductGtin: return 5;
                case EventLogEvent.CreateSequenceTemplateInvalidSequencePlacement: return 5;
                case EventLogEvent.CreateSequenceTemplateInvalidSequenceInformation: return 5;
                case EventLogEvent.SourceFileLocked: return 5;
                case EventLogEvent.PlanogramFixtureBayMismatchBetweenLocationSpaceData: return 5;
                case EventLogEvent.PlanogramFixtureBayElementMismatchBetweenLocationSpaceData: return 5;
                case EventLogEvent.PlanogramPerformanceMetricMismatchBetweenMetricProfile: return 5;
                case EventLogEvent.LocationSpaceBayElementDataMissing: return 5;

                // Warnings (900-1000)
                case EventLogEvent.PlanogramBlockingGroupMerged: return 1;
                case EventLogEvent.PlanogramAssortmentProductCouldNotBeRanked: return 2;
                case EventLogEvent.PlanogramCouldNotPublishUnassigned: return 2;
                case EventLogEvent.PlanogramBlockingMissingProducts: return 3;
                case EventLogEvent.PlanogramSequenceMissingProduct: return 3;
                case EventLogEvent.PlanogramBlockingNotAssigned: return 4;
                case EventLogEvent.PlanogramPerformanceDataInvalidLocationReference: return 2;
                case EventLogEvent.PlanogramPerformanceDataInvalidClusterReference: return 2;
                case EventLogEvent.PlanogramLocationReferenceInvalid: return 4;
                case EventLogEvent.PlanogramNoPerformanceSelectionData: return 0;
                case EventLogEvent.PlanogramNoAssortmentPresent: return 1;
                case EventLogEvent.PlanogramBlockDeservesNoSpace: return 4;
                case EventLogEvent.PlanogramPositionsNotInserted: return 1;
                case EventLogEvent.PlanogramPerformanceDataMissingOrEmpty: return 2;
                case EventLogEvent.InsertProductNotAdded: return 3;
                case EventLogEvent.AddProductAlreadyExistsInPlanogram: return 2;
                case EventLogEvent.PlanogramProductBetterPerformanceCouldNotBeAddedDueToSize: return 3;
                case EventLogEvent.PlanogramPerformanceDataNoPlanogramAssignmentReference: return 2;
                case EventLogEvent.InvalidProductFilter: return 1;
                case EventLogEvent.PlanogramNoValidLocation: return 1;
                case EventLogEvent.PlanogramNoProductsPresent: return 1;
                case EventLogEvent.ProductsInPlanNotInUniverse: return 1;
                case EventLogEvent.PlanogramProductDoesNotExist: return 1;
                case EventLogEvent.PlanogramCouldNotBeLoadedFromSourceFile: return 2;
                case EventLogEvent.MultisitedProductNotSequenced: return 1;
                case EventLogEvent.PlanogramInsufficientPerformanceDataToRecalculateBlocking: return 3;
                case EventLogEvent.PlanogramProductInventoryDecreasedNoProductPositions: return 1;
                case EventLogEvent.PlanogramNameTemplateFormulaCouldNotBeResolved: return 1;
                case EventLogEvent.PlanogramComponentFailedToGetRepresentationForBlock: return 3;

                // Information (1000 - 1100)
                case EventLogEvent.PlanogramProductsAdded: return 0;
                case EventLogEvent.PlanogramProductsRemoved: return 0;
                case EventLogEvent.NothingForTaskToDo: return 0;
                case EventLogEvent.PlanogramPositionsAdded: return 0;
                case EventLogEvent.PlanogramProductPositionsRemoved: return 0;
                case EventLogEvent.PlanogramProductsToAdd: return 0;
                case EventLogEvent.PlanogramAssortmentReplaced: return 0;
                case EventLogEvent.PlanogramBlockingAdded: return 0;
                case EventLogEvent.PlanogramBlockingAddedPerformance: return 0;
                case EventLogEvent.PlanogramConsumerDecisionTreeAdded: return 0;
                case EventLogEvent.PlacedPlanogramProductsNotInCdt: return 0;
                case EventLogEvent.UnPlacedPlanogramProductsNotInCdt: return 0;
                case EventLogEvent.PlanogramReNumberingStrategyAdded: return 0;
                case EventLogEvent.PlanogramValidationTemplateAdded: return 0;
                case EventLogEvent.PlanogramAssortmentProductsUnitsDecreased: return 0;
                case EventLogEvent.PlanogramProductPositionsMovedToCarParkShelf: return 0;
                case EventLogEvent.PlanogramMetadataUpdated: return 0;
                case EventLogEvent.PlanogramReNumberingStrategyUpdated: return 0;
                case EventLogEvent.PlanogramValidationUpdated: return 0;
                case EventLogEvent.PlanogramPerformanceDataUpdated: return 0;
                case EventLogEvent.PlanogramAssortmentProductsUnitsIncreased: return 0;
                case EventLogEvent.PlanogramPublishedToGfs: return 0;
                case EventLogEvent.PlanogramMoved: return 0;
                case EventLogEvent.PlanogramProductsUpdated: return 0;
                case EventLogEvent.PlanogramSequenceReplaced: return 0;
                case EventLogEvent.PlanogramLocationUpdated: return 0;
                case EventLogEvent.PlanogramCategoryUpdated: return 0;
                case EventLogEvent.PlanogramProductsToInsert: return 0;
                case EventLogEvent.PlanogramPositionsInserted: return 0;
                case EventLogEvent.PlanogramClusterUpdated: return 0;
                case EventLogEvent.PlanogramAttributesUpdated: return 0;
                case EventLogEvent.PlanogramPositionsReplaced: return 0;
                case EventLogEvent.PlanogramPositionsReplacedCarPark: return 0;
                case EventLogEvent.PlanogramRecalculateAssortmentInventoryUpdated: return 0;
                case EventLogEvent.PlanogramOverwritten: return 0;
                case EventLogEvent.PlanogramProductsNotPlaced: return 0;
                case EventLogEvent.PlanogramProductsExistInPlanogram: return 0;
                case EventLogEvent.PlanogramPositionsRemoved: return 0;
                case EventLogEvent.PlanogramProductsAndPositionsRemoved: return 0;
                case EventLogEvent.PlanogramPositionsRemovedCarPark: return 0;
                case EventLogEvent.RemoveProductDoesNotExist: return 0;
                case EventLogEvent.ProductAddedCarPark: return 0;
                case EventLogEvent.PlanogramPerformanceDataNoPerformanceSelection: return 0;
                case EventLogEvent.PlanogramPerformanceDataNoMetricProfile: return 0;
                case EventLogEvent.ProductColoursUpdatedFromHighlight: return 0;
                case EventLogEvent.CreateSequenceTemplateSuccessful: return 0;
                case EventLogEvent.CreateSequenceTemplateMultisitedProduct: return 0;
                case EventLogEvent.PlanogramProductDelistedDueToFamilyRule: return 0;
                case EventLogEvent.PlanogramProductDelistedDueToLocalProductRule: return 0;
                case EventLogEvent.PlanogramProductDelistedDueToProductRule: return 0;
                case EventLogEvent.PlanogramProductDelistedDueToAssortmentOptionalProductRule: return 0;
                case EventLogEvent.PlanogramProductDelistedDueToAssortmentNormalProductRule: return 0;
                case EventLogEvent.PlanogramProductDelistedDueToAssortmentInheritanceProductRule: return 0;
                case EventLogEvent.PlanogramProductDelistedDueToAssortmentDistributionProductRule: return 0;
                case EventLogEvent.PlanogramProductInventoryIncreased: return 0;
                case EventLogEvent.PlanogramProductsContainedNoUnplacedProducts: return 0;
                case EventLogEvent.PlanogramUnplacedProductsRemoved: return 0;
                case EventLogEvent.CreateAssortmentSuccessful: return 0;
                case EventLogEvent.CreateAssortmentUpdatedProducts: return 0;
                case EventLogEvent.PlanogramSequenceMultiSitedProduct: return 0;
                case EventLogEvent.PlanogramProductInventoryDecreased: return 0;
                case EventLogEvent.PlanogramFixtureBayCreatedFromLocationSpace: return 0;
                case EventLogEvent.PlanogramFixtureBayElementCreatedFromLocationSpace: return 0;
                case EventLogEvent.PlanogramPositionUnableToFurtherDecreaseInventory: return 0;
                case EventLogEvent.PlanogramNameTemplateApplied: return 0;
                case EventLogEvent.PlanogramComparisonTemplateApplied: return 0;
                case EventLogEvent.PlanogramProductInventoryDecreasedBy: return 0;
                case EventLogEvent.PlanogramPositionUnableToDecreaseInventory: return 0;
                case EventLogEvent.PlanogramRecalculateAssortmentInventoryRuleApplied: return 0;
                case EventLogEvent.PlanogramPerformanceDataProductBuddyApplied: return 0;
                case EventLogEvent.PlanogramPerformanceDataLocationBuddyApplied: return 0;
                case EventLogEvent.PlanogramFixtureBayRemovedFromLocationSpace: return 0;
                case EventLogEvent.PlanogramFixtureBayElementRemovedFromLocationSpace: return 0;
                case EventLogEvent.PlanogramFixtureBayElementUpdatedFromLocationSpace: return 0;
                case EventLogEvent.PlanogramFixtureBayUpdatedFromLocationSpace: return 0;
                case EventLogEvent.PlanogramComponentsRemoved: return 0;
                case EventLogEvent.PlanogramPerformanceDataProductMerged: return 0;

                // Debug (1100 - 1200)
                case EventLogEvent.PlanogramProductAdded: return 0;
                case EventLogEvent.PlanogramProductRemoved: return 0;
                case EventLogEvent.PlanogramBlockingGroupChanged: return 0;
                case EventLogEvent.PlacedPlanogramProductNotInCdt: return 0;
                case EventLogEvent.UnPlacedPlanogramProductNotInCdt: return 0;
                case EventLogEvent.PlanogramAssortmentProductChanged: return 0;
                case EventLogEvent.PlanogramAssortmentProductPositionsChanged: return 0;
                case EventLogEvent.PlanogramAssortmentProductInventoryChange: return 0;
                case EventLogEvent.ProductInPlanNotInUniverse: return 0;

                default:
                    Debug.Fail("No score has been allocated for this event");
                    return 0;
            }
        }

        public static PlanogramEventLogEntryType GetEntryType(EventLogEvent eventId)
        {
            Byte score = GetScore(eventId);

            //if score is 5 is an error, otherwise its a warning.
            if (score == 5) return PlanogramEventLogEntryType.Error;
            if (score > 0) return PlanogramEventLogEntryType.Warning;

            //if 0 then we need to work out from the id if is information or debug.
            // Information (1000 - 1100)
            Int32 id = (Int32)eventId;

            if (id > 1000 && id <= 1100) return PlanogramEventLogEntryType.Information;

            // Debug (1100 - 1200)
            if (id > 1100 && id <= 1200) return PlanogramEventLogEntryType.Debug;



            return PlanogramEventLogEntryType.Information;
        }
    }
}