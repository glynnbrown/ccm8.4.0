﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26944 : A.Silva ~ Created.
// V8-27004 : A.Silva ~ Properly implemented.

#endregion

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Server-side implementation for <see cref="PlanogramValidationInfoList" />.
    /// </summary>
    public partial class PlanogramValidationInfoList
    {
        #region Constructors

        /// <summary>
        ///     Private constructor. Use factory methods to retrieve new instances of <see cref="PlanogramValidationInfoList" />.
        /// </summary>
        private PlanogramValidationInfoList() { }

        #endregion

        #region Factory Methods

        #region FetchByPlanogramIds

        /// <summary>
        ///     Fetches the <see cref="PlanogramValidationInfoList"/> matching the given <paramref name="planogramIdList"/>.
        /// </summary>
        /// <param name="planogramIdList">Collection of <see cref="Object"/> values with the unique Ids of the planograms to retrieve Planogram Validation data from.</param>
        /// <returns>A new instance of <see cref="PlanogramValidationInfoList"/> containing items obtained from the given <paramref name="planogramIdList"/>.</returns>
        /// <remarks>CSLA will invoke <see cref="DataPortal_Fetch(FetchByPlanogramIdsCriteria)"/> on the server via reflection.</remarks>
        public static PlanogramValidationInfoList FetchByPlanogramIds(IEnumerable<Int32> planogramIdList)
        {
            return DataPortal.Fetch<PlanogramValidationInfoList>(new FetchByPlanogramIdsCriteria(planogramIdList));
        }

        #endregion

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        ///     Invoked by CSLA via reflection when calling <see cref="FetchByPlanogramIds(IEnumerable{Int32})" />.
        /// </summary>
        /// <param name="criteria">Object containing the criteria to use when fetching results.</param>
        /// <remarks>This method fetches Group and Metric information too in one go and loads them all to avoid multiple hits to the database.</remarks>
        private void DataPortal_Fetch(FetchByPlanogramIdsCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            var dalFactory = DalContainer.GetDalFactory();
            using (var dalContext = dalFactory.CreateContext())
            {
                // Fetch all PlanogramValidation infos for the planograms.
                IEnumerable<PlanogramValidationInfoDto> planogramValidationInfoDtos;
                using (var dal = dalContext.GetDal<IPlanogramValidationInfoDal>())
                {
                    planogramValidationInfoDtos = dal.FetchPlanogramValidationsByPlanogramIds(criteria.PlanogramIds);
                }

                // Fetch all PlanogramValidationGroup infos for the planograms.
                IEnumerable<PlanogramValidationGroupInfoDto> validationGroupInfoDtos;
                using (var dal = dalContext.GetDal<IPlanogramValidationGroupInfoDal>())
                {
                    validationGroupInfoDtos = dal.FetchPlanogramValidationGroupsByPlanogramIds(criteria.PlanogramIds);
                }

                // Fetch all PlanogramValidationMetric infos for the planograms.
                IEnumerable<PlanogramValidationMetricInfoDto> validationMetricInfoDtos;
                using (var dal = dalContext.GetDal<IPlanogramValidationMetricInfoDal>())
                {
                    validationMetricInfoDtos = dal.FetchPlanogramValidationMetricsByPlanogramIds(criteria.PlanogramIds);
                }


                var context = dalContext;
                var planogramValidationInfos = planogramValidationInfoDtos
                    .Select(validationInfoDto => PlanogramValidationInfo
                        .GetPlanogramValidationInfo(
                            context,
                            validationInfoDto,
                            validationGroupInfoDtos,
                            validationMetricInfoDtos));
                foreach (var validationInfo in planogramValidationInfos)
                {
                    this.Add(validationInfo);
                }
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}