﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a list of repository sync target parameters
    /// </summary>
    public partial class RepositorySyncTargetParameterList
    {
        #region Constructor
        private RepositorySyncTargetParameterList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all repository sync operations within the solution
        /// </summary>
        /// <returns></returns>
        internal static RepositorySyncTargetParameterList FetchByRepositorySyncTargetId(IDalContext dalContext, Int32 repositorySyncTargetId)
        {
            return DataPortal.FetchChild<RepositorySyncTargetParameterList>(dalContext, repositorySyncTargetId);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning all library types
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 repositorySyncTargetId)
        {
            this.RaiseListChangedEvents = false;
            using (IRepositorySyncTargetParameterDal dal = dalContext.GetDal<IRepositorySyncTargetParameterDal>())
            {
                IEnumerable<RepositorySyncTargetParameterDto> dtoList = dal.FetchByRepositorySyncTargetId(repositorySyncTargetId);
                foreach (RepositorySyncTargetParameterDto dto in dtoList)
                {
                    this.Add(RepositorySyncTargetParameter.FetchRepositorySyncTarget(dalContext, dto));
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
