﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (SA 1.0)
// V8-25629 : A.Kuszyk
//		Created (copied from SA).
// CCM-27078 : I.George
//  Added ProductGroupIdProperty
// V8-27927 : I.George
//  Added OnCopy method
#endregion
#region Version History: (CCM 8.0.3)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class ClusterScheme : ModelObject<ClusterScheme>
    {
        #region Properties

        /// <summary>
        /// The unqiue cluster scheme id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// The RowVersion Timestamp
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }

        /// <summary>
        /// Unique content reference property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Guid> UniqueContentReferenceProperty =
            RegisterModelProperty<Guid>(c => c.UniqueContentReference);
        /// <summary>
        /// Unique content reference
        /// </summary>
        public Guid UniqueContentReference
        {
            get { return GetProperty<Guid>(UniqueContentReferenceProperty); }
        }

        /// <summary>
        /// The cluster scheme name
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        /// <summary>
        /// Is Default Cluster Scheme
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsDefaultProperty =
            RegisterModelProperty<Boolean>(c => c.IsDefault);
        public Boolean IsDefault
        {
            get { return GetProperty<Boolean>(IsDefaultProperty); }
            set { SetProperty<Boolean>(IsDefaultProperty, value); }
        }

        /// <summary>
        /// The cluster schemes cluster collection
        /// </summary>
        public static readonly ModelPropertyInfo<ClusterList> ClustersProperty =
            RegisterModelProperty<ClusterList>(c => c.Clusters);
        public ClusterList Clusters
        {
            get { return GetProperty<ClusterList>(ClustersProperty); }
        }

        /// <summary>
        /// The entity Id.
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
            set { SetProperty<Int32>(EntityIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32?> ProductGroupIdProperty =
            RegisterModelProperty<Int32?>(c => c.ProductGroupId);
        public Int32? ProductGroupId
        {
            get { return GetProperty<Int32?>(ProductGroupIdProperty); }
            set { SetProperty<Int32?>(ProductGroupIdProperty, value); }
        }

        /// <summary>
        /// Parent unique content reference property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Guid?> ParentUniqueContentReferenceProperty =
            RegisterModelProperty<Guid?>(c => c.ParentUniqueContentReference);
        /// <summary>
        /// Parent unique content reference
        /// </summary>
        public Guid? ParentUniqueContentReference
        {
            get { return GetProperty<Guid?>(ParentUniqueContentReferenceProperty); }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Adds authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ClusterScheme), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ClusterSchemeCreate.ToString()));
            BusinessRules.AddRule(typeof(ClusterScheme), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ClusterSchemeGet.ToString()));
            BusinessRules.AddRule(typeof(ClusterScheme), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ClusterSchemeEdit.ToString()));
            BusinessRules.AddRule(typeof(ClusterScheme), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ClusterSchemeDelete.ToString()));
        }
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 255));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new ClusterScheme
        /// </summary>
        public static ClusterScheme NewClusterScheme(Int32 entityId)
        {
            ClusterScheme item = new ClusterScheme();
            item.Create(entityId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 entityId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.LoadProperty<Guid>(UniqueContentReferenceProperty, Guid.NewGuid());
            this.LoadProperty<Boolean>(IsDefaultProperty, false);
            this.LoadProperty<Int32?>(ProductGroupIdProperty, ProductGroupId);
            this.LoadProperty<ClusterList>(ClustersProperty, ClusterList.NewClusterList());
            base.Create();
        }
        #endregion

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<ClusterScheme>(oldId, newId);
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Returns a list of all locations allocated in the cluster scheme
        /// </summary>
        /// <returns></returns>
        public List<ClusterLocation> FetchAllocatedLocations()
        {
            List<ClusterLocation> takenLocations = new List<ClusterLocation>();

            foreach (Cluster cluster in this.Clusters)
            {
                foreach (ClusterLocation location in cluster.Locations)
                {
                    takenLocations.Add(location);
                }
            }

            return takenLocations;
        }

        #endregion
    }
}
