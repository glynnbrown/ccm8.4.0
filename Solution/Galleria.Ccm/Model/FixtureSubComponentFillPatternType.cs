﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Initial Version
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Sub Component Fill Pattern Type Enum
    /// </summary>
    public enum FixtureSubComponentFillPatternType
    {
        Solid = 0,
        Border = 1,
        DiagonalUp = 2,
        DiagonalDown = 3,
        Crosshatch = 4,
        Horizontal = 5,
        Vertical = 6,
        Dotted = 7
    }

    /// <summary>
    /// PlanogramSubComponentFillPatternType Helper Class
    /// </summary>
    public static class FixtureSubComponentFillPatternTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<FixtureSubComponentFillPatternType, String> FriendlyNames =
            new Dictionary<FixtureSubComponentFillPatternType, String>()
            {
                {FixtureSubComponentFillPatternType.Solid, Message.Enum_FixtureSubComponentFillPatternType_Solid},
                {FixtureSubComponentFillPatternType.Border, Message.Enum_FixtureSubComponentFillPatternType_Border},
                {FixtureSubComponentFillPatternType.DiagonalUp, Message.Enum_FixtureSubComponentFillPatternType_DiagonalUp},
                {FixtureSubComponentFillPatternType.DiagonalDown, Message.Enum_FixtureSubComponentFillPatternType_DiagonalDown},
                {FixtureSubComponentFillPatternType.Crosshatch, Message.Enum_FixtureSubComponentFillPatternType_Crosshatch},
                {FixtureSubComponentFillPatternType.Horizontal, Message.Enum_FixtureSubComponentFillPatternType_Horizontal},
                {FixtureSubComponentFillPatternType.Vertical, Message.Enum_FixtureSubComponentFillPatternType_Vertical},
                {FixtureSubComponentFillPatternType.Dotted, Message.Enum_FixtureSubComponentFillPatternType_Dotted}
            };


        public static FixtureSubComponentFillPatternType Parse(String enumName)
        {
            return EnumHelper.Parse<FixtureSubComponentFillPatternType>(enumName, FixtureSubComponentFillPatternType.Solid);
        }

    }
}
