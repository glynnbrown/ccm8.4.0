#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.

#endregion

#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class CustomColumnGroupList : ModelList<CustomColumnGroupList, CustomColumnGroup>
    {
        #region New implementation of Parent

        /// <summary>
        ///     Gets a reference to the parent <see cref="CustomColumnLayout"/>.
        /// </summary>
        /// <remarks>Hides the base's <see cref="Parent"/> so we return specifically a <see cref="CustomColumnLayout"/>.</remarks>
        public new CustomColumnLayout Parent
        {
            get { return (CustomColumnLayout)base.Parent; }
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Creates and returns a new <see cref="CustomColumnGroupList"/>.
        /// </summary>
        /// <returns>A new instance of <see cref="CustomColumnGroupList"/>.</returns>
        public static CustomColumnGroupList NewCustomColumnGroupList()
        {
            var groupList = new CustomColumnGroupList();
            groupList.Create();
            return groupList;
        }

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Data Access

        #region Overrides

        /// <summary>
        ///     Called when a new instance of <see cref="CustomColumnGroupList"/> is being created.
        /// </summary>
        protected override void Create()
        {
            AllowNew = true;
            MarkAsChild();
        }

        #endregion

        #endregion


        #region Methods

        /// <summary>
        /// Adds a new group for the given path string.
        /// </summary>
        public CustomColumnGroup Add(String grouping)
        {
            CustomColumnGroup newItem = CustomColumnGroup.NewCustomColumnGroup(grouping);
            Add(newItem);
            return newItem;
        }

        #endregion
    }
}