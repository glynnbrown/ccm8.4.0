﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26944 : A.Silva ~ Created.
// V8-27004 : A.Silva ~ Properly implemented.

#endregion

#endregion

using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Server-side implementation of <see cref="PlanogramValidationGroupInfoList"/>.
    /// </summary>
    public partial class PlanogramValidationGroupInfoList
    {
        #region Constructor

        /// <summary>
        ///     Private constructor. Use factory methods to retrieve new instances.
        /// </summary>
        private PlanogramValidationGroupInfoList() { }
        
        #endregion

        #region Factory Methods

        /// <summary>
        ///     Fetches the <see cref="PlanogramValidationGroupInfoList"/> resulting from the collection of given <paramref name="dtos"/>.
        /// </summary>
        /// <param name="dalContext">Not used.</param>
        /// <param name="dtos">Collection of <see cref="PlanogramValidationGroupInfoDto"/> to use when creating the new instance.</param>
        /// <param name="validationMetricInfoDtos">Collection of <see cref="PlanogramValidationMetricInfoDto"/> that belong to this instance's groups.</param>
        /// <returns>A new instance of <see cref="PlanogramValidationGroupInfoList"/> containing items created from the given <paramref name="dtos"/>.</returns>
        /// <remarks>CSLA will invoke <see cref="Child_Fetch(IDalContext,IEnumerable{PlanogramValidationGroupInfoDto}, IEnumerable{PlanogramValidationMetricInfoDto})"/> on the server via reflection.</remarks>
        internal static PlanogramValidationGroupInfoList Fetch(IDalContext dalContext,
            IEnumerable<PlanogramValidationGroupInfoDto> dtos,
            IEnumerable<PlanogramValidationMetricInfoDto> validationMetricInfoDtos)
        {
            return DataPortal.FetchChild<PlanogramValidationGroupInfoList>(dalContext, dtos,
                validationMetricInfoDtos);
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        ///     Invoked by CSLA via reflection when calling <see cref="Fetch(IDalContext, IEnumerable{PlanogramValidationGroupInfoDto}, IEnumerable{PlanogramValidationMetricInfoDto})" />.
        /// </summary>
        /// <param name="dalContext">Not used.</param>
        /// <param name="dtos">Collection of <see cref="PlanogramValidationGroupInfoDto"/> to use when creating the new instance.</param>
        /// <param name="validationMetricInfoDtos">Collection of <see cref="PlanogramValidationMetricInfoDto"/> that belong to this instance's groups.</param>
        private void Child_Fetch(IDalContext dalContext, IEnumerable<PlanogramValidationGroupInfoDto> dtos,
            IEnumerable<PlanogramValidationMetricInfoDto> validationMetricInfoDtos)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            var planogramValidationMetricInfoDtos = validationMetricInfoDtos.ToList();
            foreach (var dto in dtos)
            {
                this.Add(PlanogramValidationGroupInfo.GetPlanogramValidationGroupInfo(dalContext, dto, planogramValidationMetricInfoDtos));
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }
        #endregion
        #endregion
    }
}
