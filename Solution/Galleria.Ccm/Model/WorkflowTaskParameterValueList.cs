﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25886 : L.Ineson
//  Created
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class WorkflowTaskParameterValueList : ModelList<WorkflowTaskParameterValueList, WorkflowTaskParameterValue>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(WorkflowTaskParameterValueList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskParameterValueList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.WorkflowCreate.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskParameterValueList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.WorkflowEdit.ToString()));
            BusinessRules.AddRule(typeof(WorkflowTaskParameterValueList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.WorkflowDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        internal static WorkflowTaskParameterValueList NewWorkflowTaskParameterValueList(Object defaultValue)
        {
            WorkflowTaskParameterValueList item = new WorkflowTaskParameterValueList();
            item.Create(defaultValue);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected void Create(Object defaultValue)
        {
            if (defaultValue != null)
            {
                this.RaiseListChangedEvents = false;

                WorkflowTaskParameterValue paramValue = WorkflowTaskParameterValue.NewWorkflowTaskParameterValue();
                paramValue.Value1 = defaultValue;
                this.Add(paramValue);

                this.RaiseListChangedEvents = true;
            }

            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}
