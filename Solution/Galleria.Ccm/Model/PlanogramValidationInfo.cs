﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26944 : A.Silva ~ Created.
// V8-27004 : A.Silva ~ Properly implemented.

#endregion

#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     This class holds the read only info for the results in <see cref="PlanogramValidationTemplate"/>.
    /// </summary>
    /// <remarks>This class ia a child of <see cref="PlanogramValidationInfoList"/>.</remarks>
    [Serializable]
    public sealed partial class PlanogramValidationInfo : ModelReadOnlyObject<PlanogramValidationInfo>
    {
        #region Properties

        #region Id

        /// <summary>
        ///		Metadata for the <see cref="Id"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(o => o.Id);

        /// <summary>
        ///		Gets or sets the value for the <see cref="Id"/> property.
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }

        #endregion

        #region PlanogramId

        /// <summary>
        ///		Metadata for the <see cref="PlanogramId"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> PlanogramIdProperty =
            RegisterModelProperty<Int32>(o => o.PlanogramId);

        /// <summary>
        ///		Gets or sets the value for the <see cref="PlanogramId"/> property.
        /// </summary>
        public Int32 PlanogramId
        {
            get { return this.GetProperty<Int32>(PlanogramIdProperty); }
        }

        #endregion

        #region Name

        /// <summary>
        ///		Metadata for the <see cref="Name"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(o => o.Name);

        /// <summary>
        ///		Gets or sets the value for the <see cref="Name"/> property.
        /// </summary>
        public String Name
        {
            get { return this.GetProperty<String>(NameProperty); }
        }

        #endregion

        #region Groups

        /// <summary>
        ///		Metadata for the <see cref="Groups"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramValidationGroupInfoList> GroupsProperty =
            RegisterModelProperty<PlanogramValidationGroupInfoList>(o => o.Groups);

        /// <summary>
        ///		Gets or sets the value for the <see cref="Groups"/> property.
        /// </summary>
        public PlanogramValidationGroupInfoList Groups
        {
            get { return GetProperty(GroupsProperty); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        ///     Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PlanogramValidationInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanogramValidationInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(PlanogramValidationInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanogramValidationInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Methods

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            return String.Format("{0}", this.Name);
        }

        #endregion
    }
}
