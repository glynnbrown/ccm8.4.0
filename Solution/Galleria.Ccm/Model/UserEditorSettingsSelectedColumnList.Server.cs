﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31934 : A.Heathcote
//      Created.
#endregion
#endregion
using System;
using System.Collections.Generic;
using Csla;
using Csla.Data;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Diagnostics.CodeAnalysis;


namespace Galleria.Ccm.Model
{
   public sealed partial class UserEditorSettingsSelectedColumnList
   {
       #region Constructor
       private UserEditorSettingsSelectedColumnList() { }
       #endregion

       #region Factory Methods 
       internal static UserEditorSettingsSelectedColumnList GetUserEditorSettingsSelectedColumn(IDalContext dalContext)
       {
           return DataPortal.FetchChild<UserEditorSettingsSelectedColumnList>(dalContext);
       }
       #endregion

       #region Data Access
 
       #region Fetch

       [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
       private void Child_Fetch(IDalContext dalContext)
       {
           RaiseListChangedEvents = false;
           using (IUserEditorSettingsSelectedColumnDal dal = dalContext.GetDal<IUserEditorSettingsSelectedColumnDal>())
           {
               foreach (UserEditorSettingsSelectedColumnDto dto in dal.FetchAll())
               {
                   this.Add(UserEditorSettingsSelectedColumn.GetUserEditorSettingsSelectedColumn(dalContext, dto));
               }
           }
           RaiseListChangedEvents = true;
       }

       #endregion 
       #endregion
   }
}
