﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// CCM-29010 : D.Pleasance
//	Created 
#endregion
#region Version History: (CCM 8.3.0)
//V8-31831 : A.Probyn
//  Added PlanogramAttribute
//  Added FetchByEntityIdName
//  Removed SeparatorType
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramNameTemplate
    {
        #region Constructors
        private PlanogramNameTemplate() { } //force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing PlanogramNameTemplate with the given id
        /// </summary>
        public static PlanogramNameTemplate FetchById(Int32 id)
        {
            return DataPortal.Fetch<PlanogramNameTemplate>(new FetchByIdCriteria(id));
        }

        /// <summary>
        /// Returns an existing PlanogramNameTemplate with the given entity id and name
        /// </summary>
        public static PlanogramNameTemplate FetchByEntityIdName(Int32 entityId, String name)
        {
            return DataPortal.Fetch<PlanogramNameTemplate>(new FetchByEntityIdNameCriteria(entityId, name));
        }
        
        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramNameTemplateDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<PlanogramNameTemplatePlanogramAttributeType>(PlanogramAttributeProperty, (PlanogramNameTemplatePlanogramAttributeType)dto.PlanogramAttribute);
            this.LoadProperty<String>(BuilderFormulaProperty, dto.BuilderFormula);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);            
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private PlanogramNameTemplateDto GetDataTransferObject()
        {
            return new PlanogramNameTemplateDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                Name = ReadProperty<String>(NameProperty),
                PlanogramAttribute = (Byte)ReadProperty<PlanogramNameTemplatePlanogramAttributeType>(PlanogramAttributeProperty),
                BuilderFormula = ReadProperty<String>(BuilderFormulaProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning FetchById
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramNameTemplateDal dal = dalContext.GetDal<IPlanogramNameTemplateDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.Id));
                }
            }
        }
        
        /// <summary>
        /// Called when fetching a planogram name template by entity id and name
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdNameCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramNameTemplateDal dal = dalContext.GetDal<IPlanogramNameTemplateDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchByEntityIdName(criteria.EntityId, criteria.Name));
                }
            }
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                PlanogramNameTemplateDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (IPlanogramNameTemplateDal dal = dalContext.GetDal<IPlanogramNameTemplateDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<PlanogramNameTemplate>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this instance
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                PlanogramNameTemplateDto dto = GetDataTransferObject();
                using (IPlanogramNameTemplateDal dal = dalContext.GetDal<IPlanogramNameTemplateDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when this instance is being deleted
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IPlanogramNameTemplateDal dal = dalContext.GetDal<IPlanogramNameTemplateDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #endregion

    }
}