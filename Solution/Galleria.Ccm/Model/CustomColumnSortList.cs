﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.

#endregion

#endregion

using System;
using System.ComponentModel;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class CustomColumnSortList : ModelList<CustomColumnSortList, CustomColumnSort>
    {
        #region New implementation of Parent

        /// <summary>
        ///     Gets a reference to the parent <see cref="CustomColumnLayout"/>.
        /// </summary>
        /// <remarks>Hides the base's <see cref="Parent"/> so we return specifically a <see cref="CustomColumnLayout"/>.</remarks>
        public new CustomColumnLayout Parent
        {
            get { return (CustomColumnLayout)base.Parent; }
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Creates and returns a new <see cref="CustomColumnSortList"/>.
        /// </summary>
        /// <returns>A new instance of <see cref="CustomColumnSortList"/>.</returns>
        public static CustomColumnSortList NewCustomColumnSortList()
        {
            var sortList = new CustomColumnSortList();
            sortList.Create();
            return sortList;
        }

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Data Access

        #region Overrides

        /// <summary>
        ///     Called when a new instance of <see cref="CustomColumnSortList"/> is being created.
        /// </summary>
        protected override void Create()
        {
            AllowNew = true;
            MarkAsChild();
        }

        #endregion

        #endregion

        #region Methods

        public CustomColumnSort Add(String path, ListSortDirection direction)
        {
            CustomColumnSort item = CustomColumnSort.NewCustomColumnSort(path, direction);
            Add(item);
            return item;
        }

        #endregion
    }
}