﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
#endregion
#region Version History: (CCM 8.03)
//V8-29267 : L.Ineson
//  Removed date deleted.
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a locationProductAttributeInfo 
    /// (child of LocationProductAttributeInfoList)
    /// </summary>
    [Serializable]
    public partial class LocationProductAttributeInfo : ModelReadOnlyObject<LocationProductAttributeInfo>
    {
        #region Static Constructor
        /// <summary>
        /// Static Constructor
        /// </summary>
        static LocationProductAttributeInfo()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> ProductIdProperty =
       RegisterModelProperty<Int32>(c => c.ProductId);
        public Int32 ProductId
        {
            get { return GetProperty<Int32>(ProductIdProperty); }
        }

        public static readonly ModelPropertyInfo<String> ProductNameProperty =
                RegisterModelProperty<String>(c => c.ProductName);
        public String ProductName
        {
            get { return GetProperty<String>(ProductNameProperty); }
        }


        public static readonly ModelPropertyInfo<Int16> LocationIdProperty =
       RegisterModelProperty<Int16>(c => c.LocationId);
        public Int16 LocationId
        {
            get { return GetProperty<Int16>(LocationIdProperty); }
        }

        public static readonly ModelPropertyInfo<String> LocationNameProperty =
               RegisterModelProperty<String>(c => c.LocationName);
        public String LocationName
        {
            get { return GetProperty<String>(LocationNameProperty); }
        }


        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
       RegisterModelProperty<Int32>(c => c.EntityId);
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
        }


        public static readonly ModelPropertyInfo<String> GTINProperty =
       RegisterModelProperty<String>(c => c.GTIN);
        public String GTIN
        {
            get { return GetProperty<String>(GTINProperty); }
        }

        public static readonly ModelPropertyInfo<Int16?> CasePackUnitsProperty =
       RegisterModelProperty<Int16?>(c => c.CasePackUnits);
        public Int16? CasePackUnits
        {
            get { return GetProperty<Int16?>(CasePackUnitsProperty); }
        }

        public static readonly ModelPropertyInfo<Byte?> DaysOfSupplyProperty =
       RegisterModelProperty<Byte?>(c => c.DaysOfSupply);
        public Byte? DaysOfSupply
        {
            get { return GetProperty<Byte?>(DaysOfSupplyProperty); }
        }

        public static readonly ModelPropertyInfo<Single?> DeliveryFrequencyDaysProperty =
       RegisterModelProperty<Single?>(c => c.DeliveryFrequencyDays);
        public Single? DeliveryFrequencyDays
        {
            get { return GetProperty<Single?>(DeliveryFrequencyDaysProperty); }
        }

        public static readonly ModelPropertyInfo<Int16?> ShelfLifeProperty =
       RegisterModelProperty<Int16?>(c => c.ShelfLife);
        public Int16? ShelfLife
        {
            get { return GetProperty<Int16?>(ShelfLifeProperty); }
        }
        /// <summary>
        /// The date when the item was created 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
        }

        /// <summary>
        /// The date when the item was modified 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
        }


        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(CasePackUnitsProperty));
            BusinessRules.AddRule(new MinValue<Int16>(CasePackUnitsProperty, 1));
            BusinessRules.AddRule(new MaxValue<Int16>(CasePackUnitsProperty, 999));

            BusinessRules.AddRule(new Required(DaysOfSupplyProperty));

            BusinessRules.AddRule(new MaxLength(GTINProperty, 14));
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationProductAttributeInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(LocationProductAttributeInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(LocationProductAttributeInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(LocationProductAttributeInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        //#region Mapping
        ///// <summary>
        ///// Creates all mappings for this type
        ///// </summary>        
        //internal static void CreateMappings()
        //{
        //    Mapper.CreateMap<LocationProductAttributeInfo, LocationProductAttributeInfoDc>()
        //        .ForMember(dest => dest.LocationCode, map => map.ResolveUsing<LocationIdToCodeResolver>().FromMember(src => src.LocationId))
        //        .ForMember(dest => dest.CasePackUnits, map => map.NullSubstitute(default(Int16)))
        //        .ForMember(dest => dest.DaysOfSupply, map => map.NullSubstitute(default(Byte)));
        //}
        //#endregion

        #region Overrides

        public override String ToString()
        {
            return String.Format("{0} {1}", this.LocationName, this.ProductName);
        }

        #endregion
    }
}
