﻿#region Header Information

#region Version History:(CCM800)
//CCM-26124 : I.George
// Initial Version
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using System.Diagnostics.CodeAnalysis;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public sealed partial class InventoryProfileInfoList
    {
        #region Constructor
        private InventoryProfileInfoList() { }//force use of factory methods
        #endregion

        #region Factory Methods

        public static InventoryProfileInfoList FetchByEntityId(int entityId)
        {
            return DataPortal.Fetch<InventoryProfileInfoList>(new FetchByEntityIdCriteria(entityId));
        }
        #endregion

        #region Data Access

        #region Fetch
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IInventoryProfileInfoDal dal = dalContext.GetDal<IInventoryProfileInfoDal>())
                {
                    IEnumerable<InventoryProfileInfoDto> dtoList = dal.FetchByEntityId(criteria.EntityId);
                    //load the inventory profile

                    foreach (InventoryProfileInfoDto dto in dtoList)
                    {
                        this.Add(InventoryProfileInfo.FetchInventoryProfileInfo(dalContext, dto));
                    }

                }
            }
            this.RaiseListChangedEvents = true;
            this.IsReadOnly = true;
        }
        #endregion
        #endregion
    }
}
