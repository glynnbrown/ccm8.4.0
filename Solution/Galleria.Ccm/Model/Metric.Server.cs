﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26179  : I.George
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.DataStructures;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Enums;

namespace Galleria.Ccm.Model
{
    public partial class Metric
    {
        #region Constructors
        private Metric(){} // force use of factory method
        #endregion

        #region Factory Methods
        //public static Metric FetchById(Int32 id)
        //{
        //    return DataPortal.Fetch<Metric>(new FetchByIdCriteria(id));
        //}

        /// <summary>
        /// returns a metric dto
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal static Metric FetchMetric(IDalContext dalContext, MetricDto dto)
        {
            return DataPortal.FetchChild<Metric>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data transfer object

        private void LoadDataTransferObject(IDalContext dalContext, MetricDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(DescriptionProperty, dto.Description);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);

            this.LoadProperty<MetricElasticityDataModelType>(DataModelTypeProperty, (MetricElasticityDataModelType)dto.DataModelType);
            this.LoadProperty<MetricDirectionType>(DirectionProperty, (MetricDirectionType)dto.Direction);
            this.LoadProperty<MetricSpecialType>(SpecialTypeProperty, (MetricSpecialType)dto.SpecialType);
            this.LoadProperty<MetricType>(TypeProperty, (MetricType)dto.Type);
        }

        private MetricDto GetDataTransferObject()
        {
            return new MetricDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                Name = ReadProperty<String>(NameProperty),
                Description = ReadProperty<String>(DescriptionProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                SpecialType = (Byte)this.ReadProperty<MetricSpecialType>(SpecialTypeProperty),
                Type=(Byte)this.ReadProperty<MetricType>(TypeProperty),
                DataModelType = (Byte)this.ReadProperty<MetricElasticityDataModelType>(DataModelTypeProperty),
                Direction = (Byte)this.ReadProperty<MetricDirectionType>(DirectionProperty)
            };
        }
        #endregion

        #region Fetch
        ///// <summary>
        ///// Returns an existing Metric with the given id
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        //private void DataPortal_Fetch(FetchByIdCriteria criteria)
        //{
        //    IDalFactory dalFactory = DalContainer.GetDalFactory();
        //    using (IDalContext dalContext = dalFactory.CreateContext())
        //    {
        //        using (IMetricDal dal = dalContext.GetDal<IMetricDal>())
        //        {
        //            this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.Id));
        //        }
        //    }

        //}

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, MetricDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext)
        {
            MetricDto dto = GetDataTransferObject();
            Int32 oldId = dto.Id;
            using (IMetricDal dal = dalContext.GetDal<IMetricDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            dalContext.RegisterId<Metric>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when objecting an object of this type
        /// </summary>
        /// <param name="dalContext">the current dal Context</param>
        /// <param name="parent">The parent model</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            MetricDto dto = GetDataTransferObject();
            using (IMetricDal dal = dalContext.GetDal<IMetricDal>())
            {
                dal.Update(dto);
            }
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
       


        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IMetricDal dal = dalContext.GetDal<IMetricDal>())
            {
                dal.DeleteById(this.Id);
            }
        }

        #endregion

        #endregion
    }
}
