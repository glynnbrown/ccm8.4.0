﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27648 : A.Probyn
//  Created
// V8-28356 : A.Kuszyk
//  Added null value check to CopyTo method.
#endregion
#region Version History: CCM802
// V8-29030 : J.Pickup
//  ProductLibraryProductPerformanceData now implements IPlanogramPerformanceData. (Needed for reflection); 
#endregion
#region Version History: CCM830
// V8-31531 : A.Heathcote
//  Removed IsTrayProduct
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
#endregion
#endregion

using System;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Rules;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.DataStructures;
using System.Collections.Generic;
using Galleria.Framework.ViewModel;
using AutoMapper;
using Galleria.Ccm.Helpers;
using System.Reflection;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Product model Object
    /// </summary>
    [Serializable]
    public sealed partial class ProductLibraryProduct :
        ModelObject<ProductLibraryProduct>, IPlanogramProduct
    {
        #region Properties

        /// <summary>
        /// Backing source product property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Product> ProductProperty =
            RegisterModelProperty<Product>(c => c.Product);
        /// <summary>
        /// Product
        /// </summary>
        public Product Product
        {
            get { return GetProperty<Product>(ProductProperty); }
        }

        public static readonly ModelPropertyInfo<ProductLibraryProductPerformanceData> PerformanceDataProperty =
            RegisterModelProperty<ProductLibraryProductPerformanceData>(c => c.PerformanceData);
        public ProductLibraryProductPerformanceData PerformanceData
        {
            get { return GetProperty<ProductLibraryProductPerformanceData>(PerformanceDataProperty); }
        }

        #endregion

        #region Constructor

        public ProductLibraryProduct() { }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new product from the given source
        /// </summary>
        public static ProductLibraryProduct NewProductLibraryProduct(Product product)
        {
            ProductLibraryProduct item = new ProductLibraryProduct();
            item.Create(product);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new Object of this type
        /// </summary>
        private new void Create(Product product)
        {
            //Set backing product field
            LoadProperty<Product>(ProductProperty, product);
            LoadProperty<ProductLibraryProductPerformanceData>(PerformanceDataProperty, ProductLibraryProductPerformanceData.NewProductLibraryProductPerformanceData());

            base.Create();
        }

        #endregion

        #endregion

        #region Helper methods

        /// <summary>
        /// Helper method to return the binding path mask for product grids
        /// </summary>
        /// <returns></returns>
        public static String GetProductBindingPathMask()
        {
            return String.Format("{0}.{1}", "Product", "{0}");
        }

        /// <summary>
        /// Helper method to set the performance value by metric id
        /// </summary>
        /// <param name="product"></param>
        /// <param name="metricId"></param>
        /// <param name="value"></param>
        public static void SetPerformanceValueByMetricId(ProductLibraryProduct product, Int32 metricId, Single? value)
        {
            switch (metricId)
            {
                case (1):
                    product.PerformanceData.P1 = value;
                    break;
                case (2):
                    product.PerformanceData.P2 = value;
                    break;
                case (3):
                    product.PerformanceData.P3 = value;
                    break;
                case (4):
                    product.PerformanceData.P4 = value;
                    break;
                case (5):
                    product.PerformanceData.P5 = value;
                    break;
                case (6):
                    product.PerformanceData.P6 = value;
                    break;
                case (7):
                    product.PerformanceData.P7 = value;
                    break;
                case (8):
                    product.PerformanceData.P8 = value;
                    break;
                case (9):
                    product.PerformanceData.P9 = value;
                    break;
                case (10):
                    product.PerformanceData.P10 = value;
                    break;
                case (11):
                    product.PerformanceData.P11 = value;
                    break;
                case (12):
                    product.PerformanceData.P12 = value;
                    break;
                case (13):
                    product.PerformanceData.P13 = value;
                    break;
                case (14):
                    product.PerformanceData.P14 = value;
                    break;
                case (15):
                    product.PerformanceData.P15 = value;
                    break;
                case (16):
                    product.PerformanceData.P16 = value;
                    break;
                case (17):
                    product.PerformanceData.P17 = value;
                    break;
                case (18):
                    product.PerformanceData.P18 = value;
                    break;
                case (19):
                    product.PerformanceData.P19 = value;
                    break;
                case (20):
                    product.PerformanceData.P20 = value;
                    break;
            }
        }

        #endregion

        #region IPlanogramProduct Implementation

        public object Id
        {
            get { return this.Product.Id; }
        }

        public string Gtin
        {
            get
            {
                return this.Product.Gtin;
            }
            set
            {
                this.Product.Gtin = value;
            }
        }

        public string Name
        {
            get
            {
                return this.Product.Name;
            }
            set
            {
                this.Product.Name = value;
            }
        }

        public string Brand
        {
            get
            {
                return this.Product.Brand;
            }
            set
            {
                this.Product.Brand = value;
            }
        }

        public float Height
        {
            get
            {
                return this.Product.Height;
            }
            set
            {
                this.Product.Height = value;
            }
        }

        public float Width
        {
            get
            {
                return this.Product.Width;
            }
            set
            {
                this.Product.Width = value;
            }
        }

        public float Depth
        {
            get
            {
                return this.Product.Depth;
            }
            set
            {
                this.Product.Depth = value;
            }
        }

        public float DisplayHeight
        {
            get
            {
                return this.Product.DisplayHeight;
            }
            set
            {
                this.Product.DisplayHeight = value;
            }
        }

        public float DisplayWidth
        {
            get
            {
                return this.Product.DisplayWidth;
            }
            set
            {
                this.Product.DisplayWidth = value;
            }
        }

        public float DisplayDepth
        {
            get
            {
                return this.Product.DisplayDepth;
            }
            set
            {
                this.Product.DisplayDepth = value;
            }
        }

        public float AlternateHeight
        {
            get
            {
                return this.Product.AlternateHeight;
            }
            set
            {
                this.Product.AlternateHeight = value;
            }
        }

        public float AlternateWidth
        {
            get
            {
                return this.Product.AlternateWidth;
            }
            set
            {
                this.Product.AlternateWidth = value;
            }
        }

        public float AlternateDepth
        {
            get
            {
                return this.Product.AlternateDepth;
            }
            set
            {
                this.Product.AlternateDepth = value;
            }
        }

        public float PointOfPurchaseHeight
        {
            get
            {
                return this.Product.PointOfPurchaseHeight;
            }
            set
            {
                this.Product.PointOfPurchaseHeight = value;
            }
        }

        public float PointOfPurchaseWidth
        {
            get
            {
                return this.Product.PointOfPurchaseWidth;
            }
            set
            {
                this.Product.PointOfPurchaseWidth = value;
            }
        }

        public float PointOfPurchaseDepth
        {
            get
            {
                return this.Product.PointOfPurchaseDepth;
            }
            set
            {
                this.Product.PointOfPurchaseDepth = value;
            }
        }

        public byte NumberOfPegHoles
        {
            get
            {
                return this.Product.NumberOfPegHoles;
            }
            set
            {
                this.Product.NumberOfPegHoles = value;
            }
        }

        public float PegX
        {
            get
            {
                return this.Product.PegX;
            }
            set
            {
                this.Product.PegX = value;
            }
        }

        public float PegX2
        {
            get
            {
                return this.Product.PegX2;
            }
            set
            {
                this.Product.PegX2 = value;
            }
        }

        public float PegX3
        {
            get
            {
                return this.Product.PegX3;
            }
            set
            {
                this.Product.PegX3 = value;
            }
        }

        public float PegY
        {
            get
            {
                return this.Product.PegY;
            }
            set
            {
                this.Product.PegY = value;
            }
        }

        public float PegY2
        {
            get
            {
                return this.Product.PegY2;
            }
            set
            {
                this.Product.PegY2 = value;
            }
        }

        public float PegY3
        {
            get
            {
                return this.Product.PegY3;
            }
            set
            {
                this.Product.PegY3 = value;
            }
        }

        public float PegProngOffsetX
        {
            get
            {
                return this.Product.PegProngOffsetX;
            }
            set
            {
                this.Product.PegProngOffsetX = value;
            }
        }

        public float PegProngOffsetY
        {
            get
            {
                return this.Product.PegProngOffsetY;
            }
            set
            {
                this.Product.PegProngOffsetY = value;
            }
        }

        public float PegDepth
        {
            get
            {
                return this.Product.PegDepth;
            }
            set
            {
                this.Product.PegDepth = value;
            }
        }

        public float SqueezeHeight
        {
            get
            {
                return this.Product.SqueezeHeight;
            }
            set
            {
                this.Product.SqueezeHeight = value;
            }
        }

        public float SqueezeWidth
        {
            get
            {
                return this.Product.SqueezeWidth;
            }
            set
            {
                this.Product.SqueezeWidth = value;
            }
        }

        public float SqueezeDepth
        {
            get
            {
                return this.Product.SqueezeDepth;
            }
            set
            {
                this.Product.SqueezeDepth = value;
            }
        }

        public float NestingHeight
        {
            get
            {
                return this.Product.NestingHeight;
            }
            set
            {
                this.Product.NestingHeight = value;
            }
        }

        public float NestingWidth
        {
            get
            {
                return this.Product.NestingWidth;
            }
            set
            {
                this.Product.NestingWidth = value;
            }
        }

        public float NestingDepth
        {
            get
            {
                return this.Product.NestingDepth;
            }
            set
            {
                this.Product.NestingDepth = value;
            }
        }

        public short CasePackUnits
        {
            get
            {
                return this.Product.CasePackUnits;
            }
            set
            {
                this.Product.CasePackUnits = value;
            }
        }

        public byte CaseHigh
        {
            get
            {
                return this.Product.CaseHigh;
            }
            set
            {
                this.Product.CaseHigh = value;
            }
        }

        public byte CaseWide
        {
            get
            {
                return this.Product.CaseWide;
            }
            set
            {
                this.Product.CaseWide = value;
            }
        }

        public byte CaseDeep
        {
            get
            {
                return this.Product.CaseDeep;
            }
            set
            {
                this.Product.CaseDeep = value;
            }
        }

        public float CaseHeight
        {
            get
            {
                return this.Product.CaseHeight;
            }
            set
            {
                this.Product.CaseHeight = value;
            }
        }

        public float CaseWidth
        {
            get
            {
                return this.Product.CaseWidth;
            }
            set
            {
                this.Product.CaseWidth = value;
            }
        }

        public float CaseDepth
        {
            get
            {
                return this.Product.CaseDepth;
            }
            set
            {
                this.Product.CaseDepth = value;
            }
        }

        public byte MaxStack
        {
            get
            {
                return this.Product.MaxStack;
            }
            set
            {
                this.Product.MaxStack = value;
            }
        }

        public byte MaxTopCap
        {
            get
            {
                return this.Product.MaxTopCap;
            }
            set
            {
                this.Product.MaxTopCap = value;
            }
        }

        public byte MaxRightCap
        {
            get
            {
                return this.Product.MaxRightCap;
            }
            set
            {
                this.Product.MaxRightCap = value;
            }
        }

        public byte MinDeep
        {
            get
            {
                return this.Product.MinDeep;
            }
            set
            {
                this.Product.MinDeep = value;
            }
        }

        public byte MaxDeep
        {
            get
            {
                return this.Product.MaxDeep;
            }
            set
            {
                this.Product.MaxDeep = value;
            }
        }

        public short TrayPackUnits
        {
            get
            {
                return this.Product.TrayPackUnits;
            }
            set
            {
                this.Product.TrayPackUnits = value;
            }
        }

        public byte TrayHigh
        {
            get
            {
                return this.Product.TrayHigh;
            }
            set
            {
                this.Product.TrayHigh = value;
            }
        }

        public byte TrayWide
        {
            get
            {
                return this.Product.TrayWide;
            }
            set
            {
                this.Product.TrayWide = value;
            }
        }

        public byte TrayDeep
        {
            get
            {
                return this.Product.TrayDeep;
            }
            set
            {
                this.Product.TrayDeep = value;
            }
        }

        public float TrayHeight
        {
            get
            {
                return this.Product.TrayHeight;
            }
            set
            {
                this.Product.TrayHeight = value;
            }
        }

        public float TrayWidth
        {
            get
            {
                return this.Product.TrayWidth;
            }
            set
            {
                this.Product.TrayWidth = value;
            }
        }

        public float TrayDepth
        {
            get
            {
                return this.Product.TrayDepth;
            }
            set
            {
                this.Product.TrayDepth = value;
            }
        }

        public float TrayThickHeight
        {
            get
            {
                return this.Product.TrayThickHeight;
            }
            set
            {
                this.Product.TrayThickHeight = value;
            }
        }

        public float TrayThickWidth
        {
            get
            {
                return this.Product.TrayThickWidth;
            }
            set
            {
                this.Product.TrayThickWidth = value;
            }
        }

        public float TrayThickDepth
        {
            get
            {
                return this.Product.TrayThickDepth;
            }
            set
            {
                this.Product.TrayThickDepth = value;
            }
        }

        public float FrontOverhang
        {
            get
            {
                return this.Product.FrontOverhang;
            }
            set
            {
                this.Product.FrontOverhang = value;
            }
        }

        public float FingerSpaceAbove
        {
            get
            {
                return this.Product.FingerSpaceAbove;
            }
            set
            {
                this.Product.FingerSpaceAbove = value;
            }
        }

        public float FingerSpaceToTheSide
        {
            get
            {
                return this.Product.FingerSpaceToTheSide;
            }
            set
            {
                this.Product.FingerSpaceToTheSide = value;
            }
        }

        public PlanogramProductStatusType StatusType
        {
            get
            {
                return this.Product.StatusType;
            }
            set
            {
                this.Product.StatusType = value;
            }
        }

        public PlanogramProductOrientationType OrientationType
        {
            get
            {
                return this.Product.OrientationType;
            }
            set
            {
                this.Product.OrientationType = value;
            }
        }

        public PlanogramProductMerchandisingStyle MerchandisingStyle
        {
            get
            {
                return this.Product.MerchandisingStyle;
            }
            set
            {
                this.Product.MerchandisingStyle = value;
            }
        }

        public bool IsFrontOnly
        {
            get
            {
                return this.Product.IsFrontOnly;
            }
            set
            {
                this.Product.IsFrontOnly = value;
            }
        }

   

        public bool IsPlaceHolderProduct
        {
            get
            {
                return this.Product.IsPlaceHolderProduct;
            }
            set
            {
                this.Product.IsPlaceHolderProduct = value;
            }
        }

        public bool IsActive
        {
            get
            {
                return this.Product.IsActive;
            }
            set
            {
                this.Product.IsActive = value;
            }
        }

        public bool CanBreakTrayUp
        {
            get
            {
                return this.Product.CanBreakTrayUp;
            }
            set
            {
                this.Product.CanBreakTrayUp = value;
            }
        }

        public bool CanBreakTrayDown
        {
            get
            {
                return this.Product.CanBreakTrayDown;
            }
            set
            {
                this.Product.CanBreakTrayDown = value;
            }
        }

        public bool CanBreakTrayBack
        {
            get
            {
                return this.Product.CanBreakTrayBack;
            }
            set
            {
                this.Product.CanBreakTrayBack = value;
            }
        }

        public bool CanBreakTrayTop
        {
            get
            {
                return this.Product.CanBreakTrayTop;
            }
            set
            {
                this.Product.CanBreakTrayTop = value;
            }
        }

        public bool ForceMiddleCap
        {
            get
            {
                return this.Product.ForceMiddleCap;
            }
            set
            {
                this.Product.ForceMiddleCap = value;
            }
        }

        public bool ForceBottomCap
        {
            get
            {
                return this.Product.ForceBottomCap;
            }
            set
            {
                this.Product.ForceBottomCap = value;
            }
        }

        public PlanogramProductShapeType ShapeType
        {
            get
            {
                return this.Product.ShapeType;
            }
            set
            {
                this.Product.ShapeType = value;
            }
        }

        public PlanogramProductFillPatternType FillPatternType
        {
            get
            {
                return this.Product.FillPatternType;
            }
            set
            {
                this.Product.FillPatternType = value;
            }
        }

        public int FillColour
        {
            get
            {
                return this.Product.FillColour;
            }
            set
            {
                this.Product.FillColour = value;
            }
        }

        public string Shape
        {
            get
            {
                return this.Product.Shape;
            }
            set
            {
                this.Product.Shape = value;
            }
        }

        public string PointOfPurchaseDescription
        {
            get
            {
                return this.Product.PointOfPurchaseDescription;
            }
            set
            {
                this.Product.PointOfPurchaseDescription = value;
            }
        }

        public string ShortDescription
        {
            get
            {
                return this.Product.ShortDescription;
            }
            set
            {
                this.Product.ShortDescription = value;
            }
        }

        public string Subcategory
        {
            get
            {
                return this.Product.Subcategory;
            }
            set
            {
                this.Product.Subcategory = value;
            }
        }

        public string CustomerStatus
        {
            get
            {
                return this.Product.CustomerStatus;
            }
            set
            {
                this.Product.CustomerStatus = value;
            }
        }

        public string Colour
        {
            get
            {
                return this.Product.Colour;
            }
            set
            {
                this.Product.Colour = value;
            }
        }

        public string Flavour
        {
            get
            {
                return this.Product.Flavour;
            }
            set
            {
                this.Product.Flavour = value;
            }
        }

        public string PackagingShape
        {
            get
            {
                return this.Product.PackagingShape;
            }
            set
            {
                this.Product.PackagingShape = value;
            }
        }

        public string PackagingType
        {
            get
            {
                return this.Product.PackagingType;
            }
            set
            {
                this.Product.PackagingType = value;
            }
        }

        public string CountryOfOrigin
        {
            get
            {
                return this.Product.CountryOfOrigin;
            }
            set
            {
                this.Product.CountryOfOrigin = value;
            }
        }

        public string CountryOfProcessing
        {
            get
            {
                return this.Product.CountryOfProcessing;
            }
            set
            {
                this.Product.CountryOfProcessing = value;
            }
        }

        public short ShelfLife
        {
            get
            {
                return this.Product.ShelfLife;
            }
            set
            {
                this.Product.ShelfLife = value;
            }
        }

        public float? DeliveryFrequencyDays
        {
            get
            {
                return this.Product.DeliveryFrequencyDays;
            }
            set
            {
                this.Product.DeliveryFrequencyDays = value;
            }
        }

        public string DeliveryMethod
        {
            get
            {
                return this.Product.DeliveryMethod;
            }
            set
            {
                this.Product.DeliveryMethod = value;
            }
        }

        public string VendorCode
        {
            get
            {
                return this.Product.VendorCode;
            }
            set
            {
                this.Product.VendorCode = value;
            }
        }

        public string Vendor
        {
            get
            {
                return this.Product.Vendor;
            }
            set
            {
                this.Product.Vendor = value;
            }
        }

        public string ManufacturerCode
        {
            get
            {
                return this.Product.ManufacturerCode;
            }
            set
            {
                this.Product.ManufacturerCode = value;
            }
        }

        public string Manufacturer
        {
            get
            {
                return this.Product.Manufacturer;
            }
            set
            {
                this.Product.Manufacturer = value;
            }
        }

        public string Size
        {
            get
            {
                return this.Product.Size;
            }
            set
            {
                this.Product.Size = value;
            }
        }

        public string UnitOfMeasure
        {
            get
            {
                return this.Product.UnitOfMeasure;
            }
            set
            {
                this.Product.UnitOfMeasure = value;
            }
        }

        public DateTime? DateIntroduced
        {
            get
            {
                return this.Product.DateIntroduced;
            }
            set
            {
                this.Product.DateIntroduced = value;
            }
        }

        public DateTime? DateDiscontinued
        {
            get
            {
                return this.Product.DateDiscontinued;
            }
            set
            {
                this.Product.DateDiscontinued = value;
            }
        }

        public DateTime? DateEffective
        {
            get
            {
                return this.Product.DateEffective;
            }
            set
            {
                this.Product.DateEffective = value;
            }
        }

        public string Health
        {
            get
            {
                return this.Product.Health;
            }
            set
            {
                this.Product.Health = value;
            }
        }

        public string CorporateCode
        {
            get
            {
                return this.Product.CorporateCode;
            }
            set
            {
                this.Product.CorporateCode = value;
            }
        }

        public string Barcode
        {
            get
            {
                return this.Product.Barcode;
            }
            set
            {
                this.Product.Barcode = value;
            }
        }

        public float? SellPrice
        {
            get
            {
                return this.Product.SellPrice;
            }
            set
            {
                this.Product.SellPrice = value;
            }
        }

        public short? SellPackCount
        {
            get
            {
                return this.Product.SellPackCount;
            }
            set
            {
                this.Product.SellPackCount = value;
            }
        }

        public string SellPackDescription
        {
            get
            {
                return this.Product.SellPackDescription;
            }
            set
            {
                this.Product.SellPackDescription = value;
            }
        }

        public float? RecommendedRetailPrice
        {
            get
            {
                return this.Product.RecommendedRetailPrice;
            }
            set
            {
                this.Product.RecommendedRetailPrice = value;
            }
        }

        public float? ManufacturerRecommendedRetailPrice
        {
            get
            {
                return this.Product.ManufacturerRecommendedRetailPrice;
            }
            set
            {
                this.Product.ManufacturerRecommendedRetailPrice = value;
            }
        }

        public float? CostPrice
        {
            get
            {
                return this.Product.CostPrice;
            }
            set
            {
                this.Product.CostPrice = value;
            }
        }

        public float? CaseCost
        {
            get
            {
                return this.Product.CaseCost;
            }
            set
            {
                this.Product.CaseCost = value;
            }
        }

        public float? TaxRate
        {
            get
            {
                return this.Product.TaxRate;
            }
            set
            {
                this.Product.TaxRate = value;
            }
        }

        public string ConsumerInformation
        {
            get
            {
                return this.Product.ConsumerInformation;
            }
            set
            {
                this.Product.ConsumerInformation = value;
            }
        }

        public string Texture
        {
            get
            {
                return this.Product.Texture;
            }
            set
            {
                this.Product.Texture = value;
            }
        }

        public short? StyleNumber
        {
            get
            {
                return this.Product.StyleNumber;
            }
            set
            {
                this.Product.StyleNumber = value;
            }
        }

        public string Pattern
        {
            get
            {
                return this.Product.Pattern;
            }
            set
            {
                this.Product.Pattern = value;
            }
        }

        public string Model
        {
            get
            {
                return this.Product.Model;
            }
            set
            {
                this.Product.Model = value;
            }
        }

        public string GarmentType
        {
            get
            {
                return this.Product.GarmentType;
            }
            set
            {
                this.Product.GarmentType = value;
            }
        }

        public bool IsPrivateLabel
        {
            get
            {
                return this.Product.IsPrivateLabel;
            }
            set
            {
                this.Product.IsPrivateLabel = value;
            }
        }

        public bool IsNewProduct
        {
            get
            {
                return this.Product.IsNewProduct;
            }
            set
            {
                this.Product.IsNewProduct = value;
            }
        }

        public string FinancialGroupCode
        {
            get
            {
                return this.Product.FinancialGroupCode;
            }
            set
            {
                this.Product.FinancialGroupCode = value;
            }
        }

        public string FinancialGroupName
        {
            get
            {
                return this.Product.FinancialGroupName;
            }
            set
            {
                this.Product.FinancialGroupName = value;
            }
        }

        public ICustomAttributeData CustomAttributes
        {
            get { return this.Product.CustomAttributes; }
        }

        #endregion
    }

    /// <summary>
    /// Class to hold the performance data for the product library
    /// </summary>
    /// <remarks>This structure must match PlanogramPerformanceData</remarks>
    public sealed partial class ProductLibraryProductPerformanceData :
        ModelObject<ProductLibraryProductPerformanceData>, IPlanogramPerformanceData
    {
        #region Properties

        #region P1
        /// <summary>
        /// P1 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P1Property =
            RegisterModelProperty<Single?>(c => c.P1);
        /// <summary>
        /// P1
        /// </summary>
        public Single? P1
        {
            get { return GetProperty<Single?>(P1Property); }
            set
            {
                if (ReadProperty<Single?>(P1Property) != value)
                {
                    SetProperty<Single?>(P1Property, value);
                }
            }
        }
        #endregion

        #region P2
        /// <summary>
        /// P2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P2Property =
            RegisterModelProperty<Single?>(c => c.P2);
        /// <summary>
        /// P2
        /// </summary>
        public Single? P2
        {
            get { return GetProperty<Single?>(P2Property); }
            set
            {
                if (ReadProperty<Single?>(P2Property) != value)
                {
                    SetProperty<Single?>(P2Property, value);
                }
            }
        }
        #endregion

        #region P3
        /// <summary>
        /// P3 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P3Property =
            RegisterModelProperty<Single?>(c => c.P3);
        /// <summary>
        /// P3
        /// </summary>
        public Single? P3
        {
            get { return GetProperty<Single?>(P3Property); }
            set
            {
                if (ReadProperty<Single?>(P3Property) != value)
                {
                    SetProperty<Single?>(P3Property, value);
                }
            }
        }
        #endregion

        #region P4
        /// <summary>
        /// P4 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P4Property =
            RegisterModelProperty<Single?>(c => c.P4);
        /// <summary>
        /// P4
        /// </summary>
        public Single? P4
        {
            get { return GetProperty<Single?>(P4Property); }
            set
            {
                if (ReadProperty<Single?>(P4Property) != value)
                {
                    SetProperty<Single?>(P4Property, value);
                }
            }
        }
        #endregion

        #region P5
        /// <summary>
        /// P5 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P5Property =
            RegisterModelProperty<Single?>(c => c.P5);
        /// <summary>
        /// P5
        /// </summary>
        public Single? P5
        {
            get { return GetProperty<Single?>(P5Property); }
            set
            {
                if (ReadProperty<Single?>(P5Property) != value)
                {
                    SetProperty<Single?>(P5Property, value);
                }
            }
        }
        #endregion

        #region P6
        /// <summary>
        /// P6 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P6Property =
            RegisterModelProperty<Single?>(c => c.P6);
        /// <summary>
        /// P6
        /// </summary>
        public Single? P6
        {
            get { return GetProperty<Single?>(P6Property); }
            set
            {
                if (ReadProperty<Single?>(P6Property) != value)
                {
                    SetProperty<Single?>(P6Property, value);
                }
            }
        }
        #endregion

        #region P7
        /// <summary>
        /// P7 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P7Property =
            RegisterModelProperty<Single?>(c => c.P7);
        /// <summary>
        /// P7
        /// </summary>
        public Single? P7
        {
            get { return GetProperty<Single?>(P7Property); }
            set
            {
                if (ReadProperty<Single?>(P7Property) != value)
                {
                    SetProperty<Single?>(P7Property, value);
                }
            }
        }
        #endregion

        #region P8
        /// <summary>
        /// P8 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P8Property =
            RegisterModelProperty<Single?>(c => c.P8);
        /// <summary>
        /// P8
        /// </summary>
        public Single? P8
        {
            get { return GetProperty<Single?>(P8Property); }
            set
            {
                if (ReadProperty<Single?>(P8Property) != value)
                {
                    SetProperty<Single?>(P8Property, value);
                }
            }
        }
        #endregion

        #region P9
        /// <summary>
        /// P9 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P9Property =
            RegisterModelProperty<Single?>(c => c.P9);
        /// <summary>
        /// P9
        /// </summary>
        public Single? P9
        {
            get { return GetProperty<Single?>(P9Property); }
            set
            {
                if (ReadProperty<Single?>(P9Property) != value)
                {
                    SetProperty<Single?>(P9Property, value);
                }
            }
        }
        #endregion

        #region P10
        /// <summary>
        /// P10 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P10Property =
            RegisterModelProperty<Single?>(c => c.P10);
        /// <summary>
        /// P10
        /// </summary>
        public Single? P10
        {
            get { return GetProperty<Single?>(P10Property); }
            set
            {
                if (ReadProperty<Single?>(P10Property) != value)
                {
                    SetProperty<Single?>(P10Property, value);
                }
            }
        }
        #endregion

        #region P11
        /// <summary>
        /// P11 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P11Property =
            RegisterModelProperty<Single?>(c => c.P11);
        /// <summary>
        /// P11
        /// </summary>
        public Single? P11
        {
            get { return GetProperty<Single?>(P11Property); }
            set
            {
                if (ReadProperty<Single?>(P11Property) != value)
                {
                    SetProperty<Single?>(P11Property, value);
                }
            }
        }
        #endregion

        #region P12
        /// <summary>
        /// P12 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P12Property =
            RegisterModelProperty<Single?>(c => c.P12);
        /// <summary>
        /// P12
        /// </summary>
        public Single? P12
        {
            get { return GetProperty<Single?>(P12Property); }
            set
            {
                if (ReadProperty<Single?>(P12Property) != value)
                {
                    SetProperty<Single?>(P12Property, value);
                }
            }
        }
        #endregion

        #region P13
        /// <summary>
        /// P13 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P13Property =
            RegisterModelProperty<Single?>(c => c.P13);
        /// <summary>
        /// P13
        /// </summary>
        public Single? P13
        {
            get { return GetProperty<Single?>(P13Property); }
            set
            {
                if (ReadProperty<Single?>(P13Property) != value)
                {
                    SetProperty<Single?>(P13Property, value);
                }
            }
        }
        #endregion

        #region P14
        /// <summary>
        /// P14 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P14Property =
            RegisterModelProperty<Single?>(c => c.P14);
        /// <summary>
        /// P14
        /// </summary>
        public Single? P14
        {
            get { return GetProperty<Single?>(P14Property); }
            set
            {
                if (ReadProperty<Single?>(P14Property) != value)
                {
                    SetProperty<Single?>(P14Property, value);
                }
            }
        }
        #endregion

        #region P15
        /// <summary>
        /// P15 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P15Property =
            RegisterModelProperty<Single?>(c => c.P15);
        /// <summary>
        /// P15
        /// </summary>
        public Single? P15
        {
            get { return GetProperty<Single?>(P15Property); }
            set
            {
                if (ReadProperty<Single?>(P15Property) != value)
                {
                    SetProperty<Single?>(P15Property, value);
                }
            }
        }
        #endregion

        #region P16
        /// <summary>
        /// P16 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P16Property =
            RegisterModelProperty<Single?>(c => c.P16);
        /// <summary>
        /// P16
        /// </summary>
        public Single? P16
        {
            get { return GetProperty<Single?>(P16Property); }
            set
            {
                if (ReadProperty<Single?>(P16Property) != value)
                {
                    SetProperty<Single?>(P16Property, value);
                }
            }
        }
        #endregion

        #region P17
        /// <summary>
        /// P17 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P17Property =
            RegisterModelProperty<Single?>(c => c.P17);
        /// <summary>
        /// P17
        /// </summary>
        public Single? P17
        {
            get { return GetProperty<Single?>(P17Property); }
            set
            {
                if (ReadProperty<Single?>(P17Property) != value)
                {
                    SetProperty<Single?>(P17Property, value);
                }
            }
        }
        #endregion

        #region P18
        /// <summary>
        /// P18 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P18Property =
            RegisterModelProperty<Single?>(c => c.P18);
        /// <summary>
        /// P18
        /// </summary>
        public Single? P18
        {
            get { return GetProperty<Single?>(P18Property); }
            set
            {
                if (ReadProperty<Single?>(P18Property) != value)
                {
                    SetProperty<Single?>(P18Property, value);
                }
            }
        }
        #endregion

        #region P19
        /// <summary>
        /// P19 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P19Property =
            RegisterModelProperty<Single?>(c => c.P19);
        /// <summary>
        /// P19
        /// </summary>
        public Single? P19
        {
            get { return GetProperty<Single?>(P19Property); }
            set
            {
                if (ReadProperty<Single?>(P19Property) != value)
                {
                    SetProperty<Single?>(P19Property, value);
                }
            }
        }
        #endregion

        #region P20
        /// <summary>
        /// P20 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> P20Property =
            RegisterModelProperty<Single?>(c => c.P20);
        /// <summary>
        /// P20
        /// </summary>
        public Single? P20
        {
            get { return GetProperty<Single?>(P20Property); }
            set
            {
                if (ReadProperty<Single?>(P20Property) != value)
                {
                    SetProperty<Single?>(P20Property, value);
                }
            }
        }
        #endregion

        #region UnitsSoldPerDay
        /// <summary>
        /// UnitsSoldPerDay property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> UnitsSoldPerDayProperty =
            RegisterModelProperty<Single?>(c => c.UnitsSoldPerDay);
        /// <summary>
        /// UnitsSoldPerDay
        /// </summary>
        public Single? UnitsSoldPerDay
        {
            get { return GetProperty<Single?>(UnitsSoldPerDayProperty); }
            set { SetProperty<Single?>(UnitsSoldPerDayProperty, value); }
        }
        #endregion

        #region AchievedCasePacks
        /// <summary>
        /// AchievedCasePacks property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> AchievedCasePacksProperty =
            RegisterModelProperty<Single?>(c => c.AchievedCasePacks);
        /// <summary>
        /// AchievedCasePacks
        /// </summary>
        public Single? AchievedCasePacks
        {
            get { return GetProperty<Single?>(AchievedCasePacksProperty); }
            set { SetProperty<Single?>(AchievedCasePacksProperty, value); }
        }
        #endregion

        #region AchievedDos
        /// <summary>
        /// AchievedDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> AchievedDosProperty =
            RegisterModelProperty<Single?>(c => c.AchievedDos);
        /// <summary>
        /// AchievedDos
        /// </summary>
        public Single? AchievedDos
        {
            get { return GetProperty<Single?>(AchievedDosProperty); }
            set { SetProperty<Single?>(AchievedDosProperty, value); }
        }
        #endregion

        #region AchievedShelfLife
        /// <summary>
        /// AchievedShelfLife property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> AchievedShelfLifeProperty =
            RegisterModelProperty<Single?>(c => c.AchievedShelfLife);
        /// <summary>
        /// AchievedShelfLife
        /// </summary>
        public Single? AchievedShelfLife
        {
            get { return GetProperty<Single?>(AchievedShelfLifeProperty); }
            set { SetProperty<Single?>(AchievedShelfLifeProperty, value); }
        }
        #endregion

        #region AchievedDeliveries
        /// <summary>
        /// AchievedDeliveries property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> AchievedDeliveriesProperty =
            RegisterModelProperty<Single?>(c => c.AchievedDeliveries);
        /// <summary>
        /// AchievedDeliveries
        /// </summary>
        public Single? AchievedDeliveries
        {
            get { return GetProperty<Single?>(AchievedDeliveriesProperty); }
            set { SetProperty<Single?>(AchievedDeliveriesProperty, value); }
        }
        #endregion

        #endregion

        #region Constructor

        public ProductLibraryProductPerformanceData()
        { }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new product library product performance data from the given source
        /// </summary>
        public static ProductLibraryProductPerformanceData NewProductLibraryProductPerformanceData()
        {
            ProductLibraryProductPerformanceData item = new ProductLibraryProductPerformanceData();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new Object of this type
        /// </summary>
        private void Create()
        {
            base.Create();
        }

        #endregion

        #endregion

        #region Helper Methods

        /// <summary>
        /// Method to copy performance values to planogram performance data
        /// </summary>
        /// <param name="planogramPerformanceData"></param>
        public void CopyTo(PlanogramPerformanceData planogramPerformanceData)
        {
            if (planogramPerformanceData != null)
            {
                if (planogramPerformanceData.AchievedCasePacks != this.AchievedCasePacks && this.AchievedCasePacks.HasValue) planogramPerformanceData.AchievedCasePacks = this.AchievedCasePacks;
                if (planogramPerformanceData.AchievedDeliveries != this.AchievedDeliveries && this.AchievedDeliveries.HasValue) planogramPerformanceData.AchievedDeliveries = this.AchievedDeliveries;
                if (planogramPerformanceData.AchievedDos != this.AchievedDos && this.AchievedDos.HasValue) planogramPerformanceData.AchievedDos = this.AchievedDos;
                if (planogramPerformanceData.AchievedShelfLife != this.AchievedShelfLife && this.AchievedShelfLife.HasValue) planogramPerformanceData.AchievedShelfLife = this.AchievedShelfLife;
                if (planogramPerformanceData.UnitsSoldPerDay != this.UnitsSoldPerDay && this.UnitsSoldPerDay.HasValue) planogramPerformanceData.UnitsSoldPerDay = this.UnitsSoldPerDay;
                if (planogramPerformanceData.P1 != this.P1 && this.P1.HasValue) planogramPerformanceData.P1 = this.P1;
                if (planogramPerformanceData.P2 != this.P2 && this.P2.HasValue) planogramPerformanceData.P2 = this.P2;
                if (planogramPerformanceData.P3 != this.P3 && this.P3.HasValue) planogramPerformanceData.P3 = this.P3;
                if (planogramPerformanceData.P4 != this.P4 && this.P4.HasValue) planogramPerformanceData.P4 = this.P4;
                if (planogramPerformanceData.P5 != this.P5 && this.P5.HasValue) planogramPerformanceData.P5 = this.P5;
                if (planogramPerformanceData.P6 != this.P6 && this.P6.HasValue) planogramPerformanceData.P6 = this.P6;
                if (planogramPerformanceData.P7 != this.P7 && this.P7.HasValue) planogramPerformanceData.P7 = this.P7;
                if (planogramPerformanceData.P8 != this.P8 && this.P8.HasValue) planogramPerformanceData.P8 = this.P8;
                if (planogramPerformanceData.P9 != this.P9 && this.P9.HasValue) planogramPerformanceData.P9 = this.P9;
                if (planogramPerformanceData.P10 != this.P10 && this.P10.HasValue) planogramPerformanceData.P10 = this.P10;
                if (planogramPerformanceData.P11 != this.P11 && this.P11.HasValue) planogramPerformanceData.P11 = this.P11;
                if (planogramPerformanceData.P12 != this.P12 && this.P12.HasValue) planogramPerformanceData.P12 = this.P12;
                if (planogramPerformanceData.P13 != this.P13 && this.P13.HasValue) planogramPerformanceData.P13 = this.P13;
                if (planogramPerformanceData.P14 != this.P14 && this.P14.HasValue) planogramPerformanceData.P14 = this.P14;
                if (planogramPerformanceData.P15 != this.P15 && this.P15.HasValue) planogramPerformanceData.P15 = this.P15;
                if (planogramPerformanceData.P16 != this.P16 && this.P16.HasValue) planogramPerformanceData.P16 = this.P16;
                if (planogramPerformanceData.P17 != this.P17 && this.P17.HasValue) planogramPerformanceData.P17 = this.P17;
                if (planogramPerformanceData.P18 != this.P18 && this.P18.HasValue) planogramPerformanceData.P18 = this.P18;
                if (planogramPerformanceData.P19 != this.P19 && this.P19.HasValue) planogramPerformanceData.P19 = this.P19;
                if (planogramPerformanceData.P20 != this.P20 && this.P20.HasValue) planogramPerformanceData.P20 = this.P20;
            }
        }

        #endregion
    }
}
