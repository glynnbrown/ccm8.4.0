﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;


namespace Galleria.Ccm.Model
{
    /// <summary>
    /// ViewLayout model object implementation.
    /// Contains the information required to load a specific layout
    /// </summary>
    [Serializable]
    public sealed partial class ViewLayout : ModelObject<ViewLayout>
    {
        #region Properties

        private static readonly ModelPropertyInfo<Int32> IdProperty =
           RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The unique object id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The name of the view layout
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }


        public static readonly ModelPropertyInfo<Boolean> IsVerticalProperty =
            RegisterModelProperty<Boolean>(c => c.IsVertical);
        /// <summary>
        /// If true, the root panel orientation is vertical.
        /// </summary>
        public Boolean IsVertical
        {
            get { return GetProperty<Boolean>(IsVerticalProperty); }
            set { SetProperty<Boolean>(IsVerticalProperty, value); }
        }


        public static readonly ModelPropertyInfo<ViewLayoutItemList> ItemsProperty =
            RegisterModelProperty<ViewLayoutItemList>(c => c.Items);
        /// <summary>
        /// The list of root panel items.
        /// </summary>
        public ViewLayoutItemList Items
        {
            get { return GetProperty<ViewLayoutItemList>(ItemsProperty); }
        }

        //TODO: other controller settings.. is properties panel visible etc.

        #endregion

        #region Business Rules

        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ViewLayout), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(ViewLayout), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(ViewLayout), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(ViewLayout), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Temporary.ToString()));
        }
        #endregion

        #region Factory Methods

        public static ViewLayout NewViewLayout()
        {
            ViewLayout item = new ViewLayout();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            LoadProperty<ViewLayoutItemList>(ItemsProperty, ViewLayoutItemList.NewViewLayoutItemList());
           
            base.Create();
        }

        #endregion

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        #endregion

        #region Helper Methods

        public List<ViewLayoutItem> GetAllChildItems()
        {
            List<ViewLayoutItem> returnList = new List<ViewLayoutItem>();

            foreach (ViewLayoutItem childItem in this.Items)
            {
                returnList.AddRange(childItem.GetAllChildItems());
            }

            return returnList;
        }

        #endregion
    }
}
