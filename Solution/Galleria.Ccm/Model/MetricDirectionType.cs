﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: CCM800
//// V8-26179  : I.George
////  Created
//#endregion
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Galleria.Ccm.Resources.Language;

//namespace Galleria.Ccm.Model
//{
//    /// <summary>
//    /// denotes the available Metric direction types
//    /// </summary>
//    public enum MetricDirectionType 
//    {
//        MaximiseIncrease = 0,
//        maximisedecrease = 1
//    }

//    public static class MetricDirectionTypeHelper
//    {
//        public static readonly Dictionary<MetricDirectionType, String> FriendlyNames =
//            new Dictionary<MetricDirectionType, String>()
//            {
//                {MetricDirectionType.MaximiseIncrease, Message.Enum_MetrciDirectionType_MaximizeIncrease},
//                {MetricDirectionType.maximisedecrease, Message.Enum_MetricDirectionType_MaximizeDescrease},
//            };
//    }
//}
