﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramExportTemplateMappingList
    {
        #region Constructor
        private PlanogramExportTemplateMappingList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        internal static PlanogramExportTemplateMappingList FetchByParentId(IDalContext dalContext, Object parentId)
        {
            return DataPortal.FetchChild<PlanogramExportTemplateMappingList>(dalContext, new FetchByParentIdCriteria(null, parentId));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FetchByParentIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            using (IPlanogramExportTemplateMappingDal dal = dalContext.GetDal<IPlanogramExportTemplateMappingDal>())
            {
                IEnumerable<PlanogramExportTemplateMappingDto> dtoList = dal.FetchByPlanogramExportTemplateId(criteria.ParentId);
                foreach (PlanogramExportTemplateMappingDto dto in dtoList)
                {
                    this.Add(PlanogramExportTemplateMapping.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}
