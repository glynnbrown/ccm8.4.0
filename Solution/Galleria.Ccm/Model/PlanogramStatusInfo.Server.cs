﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramStatusInfo
    {
        #region Constructor
        private PlanogramStatusInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an instance from a dto
        /// </summary>
        internal static PlanogramStatusInfo GetPlanogramStatusInfo(IDalContext dalContext, PlanogramStatusInfoDto dto)
        {
            return DataPortal.FetchChild<PlanogramStatusInfo>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramStatusInfoDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramStatusInfoDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #endregion
    }
}
