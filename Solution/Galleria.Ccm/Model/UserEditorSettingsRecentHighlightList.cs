﻿#region Header Information 
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31699 : A.Heathcote
//  Created.

#endregion
#endregion

using System;
using System.Linq;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// List of user setting RecentHighlight 
    /// </summary>
    [Serializable]
    public sealed partial class UserEditorSettingsRecentHighlightList : ModelList<UserEditorSettingsRecentHighlightList, UserEditorSettingsRecentHighlight>
    {

        #region Authorization Rules

        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called to create a new list
        /// </summary>
        /// <returns>A New List</returns>
        public static UserEditorSettingsRecentHighlightList NewList()
        {
            UserEditorSettingsRecentHighlightList list = new UserEditorSettingsRecentHighlightList();
            list.Create();
            return list;
        }
        #endregion

        #region Methods
        
        /// <summary>
        /// Adds a new item to this list
        /// </summary>
        /// <param name="highlightId">the id of the highlight to add</param>
        /// <param name="maxRecentItems"></param>
        public void AddHighlightId(Object highlightId, Int32 maxRecentItems = 10)
        {
            if (highlightId == null) return;

            //If the item is already in the list, dont bother.
            //TODO: Need to add a modified list and move the item up really..
            if (this.Any(h => Object.Equals(h.HighlightId, highlightId))) return;

            //if we have hit the maximum then remove
            if (this.Count == maxRecentItems)
            {
                this.Remove(this.First());
            }

            //Add the item.
            Add(UserEditorSettingsRecentHighlight.NewUserEditorSettingsRecentHighlight(highlightId));
        }

        #endregion
    }
}



