﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-25787 : N.Foster
//	Created
// V8-26369 : A.Silva ~ Added ProcessingStatusTypeHelper and its FriendlyNames field.
// V8-28315 : D.Pleasance ~ Added ProcessingStatusFilterType amd helper to be used when filtering on mian workpackage screen
#endregion
#region Version History: CCM810
// V8-30079 : L.Ineson
//  Added CompleteWithWarnings
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Enum denoting the available planogram
    ///     processing status
    /// </summary>
    public enum ProcessingStatus : byte
    {
        Pending = 0,
        Queued = 1,
        Processing = 2,
        Complete = 3,
        Failed = 4,
        CompleteWithWarnings = 5,
    }

    /// <summary>
    ///     Processing Status Type Helper Class.
    /// </summary>
    public static class ProcessingStatusTypeHelper
    {
        /// <summary>
        ///     Holds the dictionary mapping <see cref="ProcessingStatus" /> type values to friendly names.
        /// </summary>
        public static readonly Dictionary<ProcessingStatus, String> FriendlyNames = new Dictionary
            <ProcessingStatus, String>
        {
            {ProcessingStatus.Complete, Message.Enum_ProcessingStatus_Complete},
            {ProcessingStatus.Failed, Message.Enum_ProcessingStatus_Failed}, 
            {ProcessingStatus.Pending, Message.Enum_ProcessingStatus_Pending},
            {ProcessingStatus.Processing, Message.Enum_ProcessingStatus_Processing},
            {ProcessingStatus.Queued, Message.Enum_ProcessingStatus_Queued},
            {ProcessingStatus.CompleteWithWarnings, Message.Enum_ProcessingStatus_CompleteWithWarnings},
        };
    }

    /// <summary>
    ///     Enum denoting the available planogram
    ///     processing status
    /// </summary>
    public enum ProcessingStatusFilterType : byte
    {
        All = 0,
        Pending = 1,
        Queued = 2,
        Processing = 3,
        Complete = 4,
        Failed = 5
    }

    /// <summary>
    ///     Processing Status Type Helper Class.
    /// </summary>
    public static class ProcessingStatusFilterTypeHelper
    {
        /// <summary>
        ///     Holds the dictionary mapping <see cref="ProcessingStatus" /> type values to friendly names.
        /// </summary>
        public static readonly Dictionary<ProcessingStatusFilterType, String> FriendlyNames = new Dictionary
            <ProcessingStatusFilterType, String>
        {
            {ProcessingStatusFilterType.All, Message.Enum_ProcessingStatus_All},
            {ProcessingStatusFilterType.Pending, Message.Enum_ProcessingStatus_Pending}, 
            {ProcessingStatusFilterType.Queued, Message.Enum_ProcessingStatus_Queued}, 
            {ProcessingStatusFilterType.Processing, Message.Enum_ProcessingStatus_Processing}, 
            {ProcessingStatusFilterType.Complete, Message.Enum_ProcessingStatus_Complete}, 
            {ProcessingStatusFilterType.Failed, Message.Enum_ProcessingStatus_Failed}, 
        };
    }
}