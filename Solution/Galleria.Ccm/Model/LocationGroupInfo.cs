﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//	Created 
// CCM-25444 : N.Haywood
//      Added FetchByLocationGroupIds
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// LocationGroup Model object
    /// </summary>
    [Serializable]
    public sealed partial class LocationGroupInfo : ModelReadOnlyObject<LocationGroupInfo>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        #endregion

        #region Code

        /// <summary>
        /// Code property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CodeProperty =
            RegisterModelProperty<String>(c => c.Code);

        /// <summary>
        /// Gets/Sets the Code value
        /// </summary>
        public String Code
        {
            get { return GetProperty<String>(CodeProperty); }
        }

        #endregion

        #region DateDeleted

        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);

        /// <summary>
        /// Gets/Sets the DateDeleted value
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationGroupInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(LocationGroupInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(LocationGroupInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(LocationGroupInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new info of the given product group.
        /// </summary>
        /// <param name="productGroup"></param>
        /// <returns></returns>
        public static LocationGroupInfo NewLocationGroupInfo(LocationGroup group)
        {
            LocationGroupInfo item = new LocationGroupInfo();
            item.Create(group);
            return item;
        }

        #endregion

        #region Data Access

        private void Create(LocationGroup group)
        {
            LoadProperty<Int32>(IdProperty, group.Id);
            LoadProperty<String>(CodeProperty, group.Code);
            LoadProperty<String>(NameProperty, group.Name);
            LoadProperty<DateTime?>(DateDeletedProperty, null);
        }

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Code + ' ' + this.Name;
        }

        #endregion
    }
}
