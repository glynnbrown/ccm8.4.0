﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Core;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramExportTemplateInfoList
    {
        
        #region Constructors
        private PlanogramExportTemplateInfoList() { } // force the use of factory methods
        #endregion

        #region FactoryMethods

        public static PlanogramExportTemplateInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<PlanogramExportTemplateInfoList>(new FetchByEntityIdCriteria(entityId));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when retrieving a list of all PlanogramExportTemplates for an entity
        /// Note: This procedure should NOT return deleted items
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramExportTemplateInfoDal dal = dalContext.GetDal<IPlanogramExportTemplateInfoDal>())
                {
                    IEnumerable<PlanogramExportTemplateInfoDto> dtoList = dal.FetchByEntityId(criteria.EntityId);

                    foreach (PlanogramExportTemplateInfoDto dto in dtoList)
                    {
                        this.Add(PlanogramExportTemplateInfo.FetchPlanogramExportTemplateInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}
