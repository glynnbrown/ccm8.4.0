﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Ineson
//	Created (Auto-generated)
// CCM-26091 : L.Ineson
//  Added more facethick properties
#endregion
#region Version History: CCM802
// V8-29023 : M.Pettit
//  Added Merchandisable Depth
// V8-29203 : L.Ineson
//  Added IsProductSqueezeAllowed
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class FixtureSubComponent
    {
        #region Constructor
        private FixtureSubComponent() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static FixtureSubComponent Fetch(IDalContext dalContext, FixtureSubComponentDto dto)
        {
            return DataPortal.FetchChild<FixtureSubComponent>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, FixtureSubComponentDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32?>(Mesh3DIdProperty, dto.Mesh3DId);
            this.LoadProperty<Int32?>(ImageIdFrontProperty, dto.ImageIdFront);
            this.LoadProperty<Int32?>(ImageIdBackProperty, dto.ImageIdBack);
            this.LoadProperty<Int32?>(ImageIdTopProperty, dto.ImageIdTop);
            this.LoadProperty<Int32?>(ImageIdBottomProperty, dto.ImageIdBottom);
            this.LoadProperty<Int32?>(ImageIdLeftProperty, dto.ImageIdLeft);
            this.LoadProperty<Int32?>(ImageIdRightProperty, dto.ImageIdRight);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Single>(HeightProperty, dto.Height);
            this.LoadProperty<Single>(WidthProperty, dto.Width);
            this.LoadProperty<Single>(DepthProperty, dto.Depth);
            this.LoadProperty<Single>(XProperty, dto.X);
            this.LoadProperty<Single>(YProperty, dto.Y);
            this.LoadProperty<Single>(ZProperty, dto.Z);
            this.LoadProperty<Single>(SlopeProperty, dto.Slope);
            this.LoadProperty<Single>(AngleProperty, dto.Angle);
            this.LoadProperty<Single>(RollProperty, dto.Roll);
            this.LoadProperty<FixtureSubComponentShapeType>(ShapeTypeProperty, (FixtureSubComponentShapeType)dto.ShapeType);
            this.LoadProperty<Single>(MerchandisableHeightProperty, dto.MerchandisableHeight);
            this.LoadProperty<Single>(MerchandisableDepthProperty, dto.MerchandisableDepth); 
            this.LoadProperty<Boolean>(IsVisibleProperty, dto.IsVisible);
            this.LoadProperty<Boolean>(HasCollisionDetectionProperty, dto.HasCollisionDetection);
            this.LoadProperty<FixtureSubComponentFillPatternType>(FillPatternTypeFrontProperty, (FixtureSubComponentFillPatternType)dto.FillPatternTypeFront);
            this.LoadProperty<FixtureSubComponentFillPatternType>(FillPatternTypeBackProperty, (FixtureSubComponentFillPatternType)dto.FillPatternTypeBack);
            this.LoadProperty<FixtureSubComponentFillPatternType>(FillPatternTypeTopProperty, (FixtureSubComponentFillPatternType)dto.FillPatternTypeTop);
            this.LoadProperty<FixtureSubComponentFillPatternType>(FillPatternTypeBottomProperty, (FixtureSubComponentFillPatternType)dto.FillPatternTypeBottom);
            this.LoadProperty<FixtureSubComponentFillPatternType>(FillPatternTypeLeftProperty, (FixtureSubComponentFillPatternType)dto.FillPatternTypeLeft);
            this.LoadProperty<FixtureSubComponentFillPatternType>(FillPatternTypeRightProperty, (FixtureSubComponentFillPatternType)dto.FillPatternTypeRight);
            this.LoadProperty<Int32>(FillColourFrontProperty, dto.FillColourFront);
            this.LoadProperty<Int32>(FillColourBackProperty, dto.FillColourBack);
            this.LoadProperty<Int32>(FillColourTopProperty, dto.FillColourTop);
            this.LoadProperty<Int32>(FillColourBottomProperty, dto.FillColourBottom);
            this.LoadProperty<Int32>(FillColourLeftProperty, dto.FillColourLeft);
            this.LoadProperty<Int32>(FillColourRightProperty, dto.FillColourRight);
            this.LoadProperty<Int32>(LineColourProperty, dto.LineColour);
            this.LoadProperty<Int32>(TransparencyPercentFrontProperty, dto.TransparencyPercentFront);
            this.LoadProperty<Int32>(TransparencyPercentBackProperty, dto.TransparencyPercentBack);
            this.LoadProperty<Int32>(TransparencyPercentTopProperty, dto.TransparencyPercentTop);
            this.LoadProperty<Int32>(TransparencyPercentBottomProperty, dto.TransparencyPercentBottom);
            this.LoadProperty<Int32>(TransparencyPercentLeftProperty, dto.TransparencyPercentLeft);
            this.LoadProperty<Int32>(TransparencyPercentRightProperty, dto.TransparencyPercentRight);
            this.LoadProperty<Single>(FaceThicknessFrontProperty, dto.FaceThicknessFront);
            this.LoadProperty<Single>(FaceThicknessBackProperty, dto.FaceThicknessBack);
            this.LoadProperty<Single>(FaceThicknessTopProperty, dto.FaceThicknessTop);
            this.LoadProperty<Single>(FaceThicknessBottomProperty, dto.FaceThicknessBottom);
            this.LoadProperty<Single>(FaceThicknessLeftProperty, dto.FaceThicknessLeft);
            this.LoadProperty<Single>(FaceThicknessRightProperty, dto.FaceThicknessRight);
            this.LoadProperty<Single>(RiserHeightProperty, dto.RiserHeight);
            this.LoadProperty<Single>(RiserThicknessProperty, dto.RiserThickness);
            this.LoadProperty<Boolean>(IsRiserPlacedOnFrontProperty, dto.IsRiserPlacedOnFront);
            this.LoadProperty<Boolean>(IsRiserPlacedOnBackProperty, dto.IsRiserPlacedOnBack);
            this.LoadProperty<Boolean>(IsRiserPlacedOnLeftProperty, dto.IsRiserPlacedOnLeft);
            this.LoadProperty<Boolean>(IsRiserPlacedOnRightProperty, dto.IsRiserPlacedOnRight);
            this.LoadProperty<FixtureSubComponentFillPatternType>(RiserFillPatternTypeProperty, (FixtureSubComponentFillPatternType)dto.RiserFillPatternType);
            this.LoadProperty<Int32>(RiserColourProperty, dto.RiserColour);
            this.LoadProperty<Int32>(RiserTransparencyPercentProperty, dto.RiserTransparencyPercent);
            this.LoadProperty<Single>(NotchStartXProperty, dto.NotchStartX);
            this.LoadProperty<Single>(NotchSpacingXProperty, dto.NotchSpacingX);
            this.LoadProperty<Single>(NotchStartYProperty, dto.NotchStartY);
            this.LoadProperty<Single>(NotchSpacingYProperty, dto.NotchSpacingY);
            this.LoadProperty<Single>(NotchHeightProperty, dto.NotchHeight);
            this.LoadProperty<Single>(NotchWidthProperty, dto.NotchWidth);
            this.LoadProperty<Boolean>(IsNotchPlacedOnFrontProperty, dto.IsNotchPlacedOnFront);
            this.LoadProperty<Boolean>(IsNotchPlacedOnBackProperty, dto.IsNotchPlacedOnBack);
            this.LoadProperty<Boolean>(IsNotchPlacedOnLeftProperty, dto.IsNotchPlacedOnLeft);
            this.LoadProperty<Boolean>(IsNotchPlacedOnRightProperty, dto.IsNotchPlacedOnRight);
            this.LoadProperty<FixtureSubComponentNotchStyleType>(NotchStyleTypeProperty, (FixtureSubComponentNotchStyleType)dto.NotchStyleType);
            this.LoadProperty<Single>(DividerObstructionHeightProperty, dto.DividerObstructionHeight);
            this.LoadProperty<Single>(DividerObstructionWidthProperty, dto.DividerObstructionWidth);
            this.LoadProperty<Single>(DividerObstructionDepthProperty, dto.DividerObstructionDepth);
            this.LoadProperty<Single>(DividerObstructionStartXProperty, dto.DividerObstructionStartX);
            this.LoadProperty<Single>(DividerObstructionSpacingXProperty, dto.DividerObstructionSpacingX);
            this.LoadProperty<Single>(DividerObstructionStartYProperty, dto.DividerObstructionStartY);
            this.LoadProperty<Single>(DividerObstructionSpacingYProperty, dto.DividerObstructionSpacingY);
            this.LoadProperty<Single>(DividerObstructionStartZProperty, dto.DividerObstructionStartZ);
            this.LoadProperty<Single>(DividerObstructionSpacingZProperty, dto.DividerObstructionSpacingZ);
            this.LoadProperty<Boolean>(IsDividerObstructionAtStartProperty, dto.IsDividerObstructionAtStart);
            this.LoadProperty<Boolean>(IsDividerObstructionAtEndProperty, dto.IsDividerObstructionAtEnd);
            this.LoadProperty<Boolean>(IsDividerObstructionByFacingProperty, dto.IsDividerObstructionByFacing);
            this.LoadProperty<Int32>(DividerObstructionFillColourProperty, dto.DividerObstructionFillColour);
            this.LoadProperty<FixtureSubComponentFillPatternType>(DividerObstructionFillPatternProperty, (FixtureSubComponentFillPatternType)dto.DividerObstructionFillPattern);
            this.LoadProperty<Single>(MerchConstraintRow1StartXProperty, dto.MerchConstraintRow1StartX);
            this.LoadProperty<Single>(MerchConstraintRow1SpacingXProperty, dto.MerchConstraintRow1SpacingX);
            this.LoadProperty<Single>(MerchConstraintRow1StartYProperty, dto.MerchConstraintRow1StartY);
            this.LoadProperty<Single>(MerchConstraintRow1SpacingYProperty, dto.MerchConstraintRow1SpacingY);
            this.LoadProperty<Single>(MerchConstraintRow1HeightProperty, dto.MerchConstraintRow1Height);
            this.LoadProperty<Single>(MerchConstraintRow1WidthProperty, dto.MerchConstraintRow1Width);
            this.LoadProperty<Single>(MerchConstraintRow2StartXProperty, dto.MerchConstraintRow2StartX);
            this.LoadProperty<Single>(MerchConstraintRow2SpacingXProperty, dto.MerchConstraintRow2SpacingX);
            this.LoadProperty<Single>(MerchConstraintRow2StartYProperty, dto.MerchConstraintRow2StartY);
            this.LoadProperty<Single>(MerchConstraintRow2SpacingYProperty, dto.MerchConstraintRow2SpacingY);
            this.LoadProperty<Single>(MerchConstraintRow2HeightProperty, dto.MerchConstraintRow2Height);
            this.LoadProperty<Single>(MerchConstraintRow2WidthProperty, dto.MerchConstraintRow2Width);
            this.LoadProperty<Single>(LineThicknessProperty, dto.LineThickness);
            this.LoadProperty<FixtureSubComponentMerchandisingType>(MerchandisingTypeProperty, (FixtureSubComponentMerchandisingType)dto.MerchandisingType);
            this.LoadProperty<FixtureSubComponentCombineType>(CombineTypeProperty, (FixtureSubComponentCombineType)dto.CombineType);
            this.LoadProperty<Boolean>(IsProductOverlapAllowedProperty, dto.IsProductOverlapAllowed);
            this.LoadProperty<Boolean>(IsProductSqueezeAllowedProperty, dto.IsProductSqueezeAllowed);
            this.LoadProperty<FixtureSubComponentXMerchStrategyType>(MerchandisingStrategyXProperty, (FixtureSubComponentXMerchStrategyType)dto.MerchandisingStrategyX);
            this.LoadProperty<FixtureSubComponentYMerchStrategyType>(MerchandisingStrategyYProperty, (FixtureSubComponentYMerchStrategyType)dto.MerchandisingStrategyY);
            this.LoadProperty<FixtureSubComponentZMerchStrategyType>(MerchandisingStrategyZProperty, (FixtureSubComponentZMerchStrategyType)dto.MerchandisingStrategyZ);
            this.LoadProperty<Single>(LeftOverhangProperty, dto.LeftOverhang);
            this.LoadProperty<Single>(RightOverhangProperty, dto.RightOverhang);
            this.LoadProperty<Single>(FrontOverhangProperty, dto.FrontOverhang);
            this.LoadProperty<Single>(BackOverhangProperty, dto.BackOverhang);
            this.LoadProperty<Single>(TopOverhangProperty, dto.TopOverhang);
            this.LoadProperty<Single>(BottomOverhangProperty, dto.BottomOverhang);

        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private FixtureSubComponentDto GetDataTransferObject(FixtureComponent parent)
        {
            return new FixtureSubComponentDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                FixtureComponentId = parent.Id,
                Mesh3DId = ReadProperty<Int32?>(Mesh3DIdProperty),
                ImageIdFront = ReadProperty<Int32?>(ImageIdFrontProperty),
                ImageIdBack = ReadProperty<Int32?>(ImageIdBackProperty),
                ImageIdTop = ReadProperty<Int32?>(ImageIdTopProperty),
                ImageIdBottom = ReadProperty<Int32?>(ImageIdBottomProperty),
                ImageIdLeft = ReadProperty<Int32?>(ImageIdLeftProperty),
                ImageIdRight = ReadProperty<Int32?>(ImageIdRightProperty),
                Name = ReadProperty<String>(NameProperty),
                Height = ReadProperty<Single>(HeightProperty),
                Width = ReadProperty<Single>(WidthProperty),
                Depth = ReadProperty<Single>(DepthProperty),
                X = ReadProperty<Single>(XProperty),
                Y = ReadProperty<Single>(YProperty),
                Z = ReadProperty<Single>(ZProperty),
                Slope = ReadProperty<Single>(SlopeProperty),
                Angle = ReadProperty<Single>(AngleProperty),
                Roll = ReadProperty<Single>(RollProperty),
                ShapeType = (Byte)ReadProperty<FixtureSubComponentShapeType>(ShapeTypeProperty),
                MerchandisableHeight = ReadProperty<Single>(MerchandisableHeightProperty),
                MerchandisableDepth = ReadProperty<Single>(MerchandisableDepthProperty),
                IsVisible = ReadProperty<Boolean>(IsVisibleProperty),
                HasCollisionDetection = ReadProperty<Boolean>(HasCollisionDetectionProperty),
                FillPatternTypeFront = (Byte)ReadProperty<FixtureSubComponentFillPatternType>(FillPatternTypeFrontProperty),
                FillPatternTypeBack = (Byte)ReadProperty<FixtureSubComponentFillPatternType>(FillPatternTypeBackProperty),
                FillPatternTypeTop = (Byte)ReadProperty<FixtureSubComponentFillPatternType>(FillPatternTypeTopProperty),
                FillPatternTypeBottom = (Byte)ReadProperty<FixtureSubComponentFillPatternType>(FillPatternTypeBottomProperty),
                FillPatternTypeLeft = (Byte)ReadProperty<FixtureSubComponentFillPatternType>(FillPatternTypeLeftProperty),
                FillPatternTypeRight = (Byte)ReadProperty<FixtureSubComponentFillPatternType>(FillPatternTypeRightProperty),
                FillColourFront = ReadProperty<Int32>(FillColourFrontProperty),
                FillColourBack = ReadProperty<Int32>(FillColourBackProperty),
                FillColourTop = ReadProperty<Int32>(FillColourTopProperty),
                FillColourBottom = ReadProperty<Int32>(FillColourBottomProperty),
                FillColourLeft = ReadProperty<Int32>(FillColourLeftProperty),
                FillColourRight = ReadProperty<Int32>(FillColourRightProperty),
                LineColour = ReadProperty<Int32>(LineColourProperty),
                TransparencyPercentFront = ReadProperty<Int32>(TransparencyPercentFrontProperty),
                TransparencyPercentBack = ReadProperty<Int32>(TransparencyPercentBackProperty),
                TransparencyPercentTop = ReadProperty<Int32>(TransparencyPercentTopProperty),
                TransparencyPercentBottom = ReadProperty<Int32>(TransparencyPercentBottomProperty),
                TransparencyPercentLeft = ReadProperty<Int32>(TransparencyPercentLeftProperty),
                TransparencyPercentRight = ReadProperty<Int32>(TransparencyPercentRightProperty),
                FaceThicknessFront = ReadProperty<Single>(FaceThicknessFrontProperty),
                FaceThicknessBack = ReadProperty<Single>(FaceThicknessBackProperty),
                FaceThicknessTop = ReadProperty<Single>(FaceThicknessTopProperty),
                FaceThicknessBottom = ReadProperty<Single>(FaceThicknessBottomProperty),
                FaceThicknessLeft = ReadProperty<Single>(FaceThicknessLeftProperty),
                FaceThicknessRight = ReadProperty<Single>(FaceThicknessRightProperty),
                RiserHeight = ReadProperty<Single>(RiserHeightProperty),
                RiserThickness = ReadProperty<Single>(RiserThicknessProperty),
                IsRiserPlacedOnFront = ReadProperty<Boolean>(IsRiserPlacedOnFrontProperty),
                IsRiserPlacedOnBack = ReadProperty<Boolean>(IsRiserPlacedOnBackProperty),
                IsRiserPlacedOnLeft = ReadProperty<Boolean>(IsRiserPlacedOnLeftProperty),
                IsRiserPlacedOnRight = ReadProperty<Boolean>(IsRiserPlacedOnRightProperty),
                RiserFillPatternType = (Byte)ReadProperty<FixtureSubComponentFillPatternType>(RiserFillPatternTypeProperty),
                RiserColour = ReadProperty<Int32>(RiserColourProperty),
                RiserTransparencyPercent = ReadProperty<Int32>(RiserTransparencyPercentProperty),
                NotchStartX = ReadProperty<Single>(NotchStartXProperty),
                NotchSpacingX = ReadProperty<Single>(NotchSpacingXProperty),
                NotchStartY = ReadProperty<Single>(NotchStartYProperty),
                NotchSpacingY = ReadProperty<Single>(NotchSpacingYProperty),
                NotchHeight = ReadProperty<Single>(NotchHeightProperty),
                NotchWidth = ReadProperty<Single>(NotchWidthProperty),
                IsNotchPlacedOnFront = ReadProperty<Boolean>(IsNotchPlacedOnFrontProperty),
                IsNotchPlacedOnBack = ReadProperty<Boolean>(IsNotchPlacedOnBackProperty),
                IsNotchPlacedOnLeft = ReadProperty<Boolean>(IsNotchPlacedOnLeftProperty),
                IsNotchPlacedOnRight = ReadProperty<Boolean>(IsNotchPlacedOnRightProperty),
                NotchStyleType = (Byte)ReadProperty<FixtureSubComponentNotchStyleType>(NotchStyleTypeProperty),
                DividerObstructionHeight = ReadProperty<Single>(DividerObstructionHeightProperty),
                DividerObstructionWidth = ReadProperty<Single>(DividerObstructionWidthProperty),
                DividerObstructionDepth = ReadProperty<Single>(DividerObstructionDepthProperty),
                DividerObstructionStartX = ReadProperty<Single>(DividerObstructionStartXProperty),
                DividerObstructionSpacingX = ReadProperty<Single>(DividerObstructionSpacingXProperty),
                DividerObstructionStartY = ReadProperty<Single>(DividerObstructionStartYProperty),
                DividerObstructionSpacingY = ReadProperty<Single>(DividerObstructionSpacingYProperty),
                DividerObstructionStartZ = ReadProperty<Single>(DividerObstructionStartZProperty),
                DividerObstructionSpacingZ = ReadProperty<Single>(DividerObstructionSpacingZProperty),
                IsDividerObstructionAtStart = ReadProperty<Boolean>(IsDividerObstructionAtStartProperty),
                IsDividerObstructionAtEnd = ReadProperty<Boolean>(IsDividerObstructionAtEndProperty),
                IsDividerObstructionByFacing = ReadProperty<Boolean>(IsDividerObstructionByFacingProperty),
                DividerObstructionFillColour = ReadProperty<Int32>(DividerObstructionFillColourProperty),
                DividerObstructionFillPattern= (Byte)ReadProperty<FixtureSubComponentFillPatternType>(DividerObstructionFillPatternProperty),
                MerchConstraintRow1StartX = ReadProperty<Single>(MerchConstraintRow1StartXProperty),
                MerchConstraintRow1SpacingX = ReadProperty<Single>(MerchConstraintRow1SpacingXProperty),
                MerchConstraintRow1StartY = ReadProperty<Single>(MerchConstraintRow1StartYProperty),
                MerchConstraintRow1SpacingY = ReadProperty<Single>(MerchConstraintRow1SpacingYProperty),
                MerchConstraintRow1Height = ReadProperty<Single>(MerchConstraintRow1HeightProperty),
                MerchConstraintRow1Width = ReadProperty<Single>(MerchConstraintRow1WidthProperty),
                MerchConstraintRow2StartX = ReadProperty<Single>(MerchConstraintRow2StartXProperty),
                MerchConstraintRow2SpacingX = ReadProperty<Single>(MerchConstraintRow2SpacingXProperty),
                MerchConstraintRow2StartY = ReadProperty<Single>(MerchConstraintRow2StartYProperty),
                MerchConstraintRow2SpacingY = ReadProperty<Single>(MerchConstraintRow2SpacingYProperty),
                MerchConstraintRow2Height = ReadProperty<Single>(MerchConstraintRow2HeightProperty),
                MerchConstraintRow2Width = ReadProperty<Single>(MerchConstraintRow2WidthProperty),
                LineThickness = ReadProperty<Single>(LineThicknessProperty),
                MerchandisingType = (Byte)ReadProperty<FixtureSubComponentMerchandisingType>(MerchandisingTypeProperty),
                CombineType = (Byte)ReadProperty<FixtureSubComponentCombineType>(CombineTypeProperty),
                IsProductOverlapAllowed = ReadProperty<Boolean>(IsProductOverlapAllowedProperty),
                IsProductSqueezeAllowed = ReadProperty<Boolean>(IsProductSqueezeAllowedProperty),
                MerchandisingStrategyX = (Byte)ReadProperty<FixtureSubComponentXMerchStrategyType>(MerchandisingStrategyXProperty),
                MerchandisingStrategyY = (Byte)ReadProperty<FixtureSubComponentYMerchStrategyType>(MerchandisingStrategyYProperty),
                MerchandisingStrategyZ = (Byte)ReadProperty<FixtureSubComponentZMerchStrategyType>(MerchandisingStrategyZProperty),
                LeftOverhang = ReadProperty<Single>(LeftOverhangProperty),
                RightOverhang = ReadProperty<Single>(RightOverhangProperty),
                FrontOverhang = ReadProperty<Single>(FrontOverhangProperty),
                BackOverhang = ReadProperty<Single>(BackOverhangProperty),
                TopOverhang = ReadProperty<Single>(TopOverhangProperty),
                BottomOverhang = ReadProperty<Single>(BottomOverhangProperty),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FixtureSubComponentDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, FixtureComponent parent)
        {
            this.ResolveIds(dalContext);
            FixtureSubComponentDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IFixtureSubComponentDal dal = dalContext.GetDal<IFixtureSubComponentDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<FixtureSubComponent>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, FixtureComponent parent)
        {
            this.ResolveIds(dalContext);
            if (this.IsSelfDirty)
            {
                using (IFixtureSubComponentDal dal = dalContext.GetDal<IFixtureSubComponentDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, FixtureComponent parent)
        {
            using (IFixtureSubComponentDal dal = dalContext.GetDal<IFixtureSubComponentDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion

    }
}