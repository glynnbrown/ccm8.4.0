﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created
#endregion

#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentInventoryRule
    {
        #region Constructor
        private AssortmentInventoryRule() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating an instance from a dto
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new instance of this type</returns>
        internal static AssortmentInventoryRule GetAssortmentInventoryRule(IDalContext dalContext, AssortmentInventoryRuleDto dto)
        {
            return DataPortal.FetchChild<AssortmentInventoryRule>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, AssortmentInventoryRuleDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Int32>(ProductIdProperty, dto.ProductId);
            LoadProperty<String>(ProductGtinProperty, dto.ProductGtin);
            LoadProperty<Single>(CasePackProperty, dto.CasePack);
            LoadProperty<Single>(DaysOfSupplyProperty, dto.DaysOfSupply);
            LoadProperty<Single>(ShelfLifeProperty, dto.ShelfLife);
            LoadProperty<Single>(ReplenishmentDaysProperty, dto.ReplenishmentDays);
            LoadProperty<Single>(WasteHurdleUnitsProperty, dto.WasteHurdleUnits);
            LoadProperty<Single>(WasteHurdleCasePackProperty, dto.WasteHurdleCasePack);
            LoadProperty<Int32>(MinUnitsProperty, dto.MinUnits);
            LoadProperty<Int32>(MinFacingsProperty, dto.MinFacings); 
        }

        /// <summary>
        /// Returns a dto created from this instance
        /// </summary>
        /// <param name="parent">The Assortment</param>
        /// <returns>A data transfer object</returns>
        private AssortmentInventoryRuleDto GetDataTransferObject(Assortment parent)
        {
            return new AssortmentInventoryRuleDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                AssortmentId = parent.Id,
                ProductId = ReadProperty<Int32>(ProductIdProperty),
                ProductGtin = ReadProperty<String>(ProductGtinProperty),
                CasePack = ReadProperty<Single>(CasePackProperty),
                DaysOfSupply = ReadProperty<Single>(DaysOfSupplyProperty),
                ShelfLife = ReadProperty<Single>(ShelfLifeProperty),
                ReplenishmentDays = ReadProperty<Single>(ReplenishmentDaysProperty),
                WasteHurdleUnits = ReadProperty<Single>(WasteHurdleUnitsProperty),
                WasteHurdleCasePack = ReadProperty<Single>(WasteHurdleCasePackProperty),
                MinUnits = ReadProperty<Int32>(MinUnitsProperty),
                MinFacings = ReadProperty<Int32>(MinFacingsProperty)
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when this instance is being loaded
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, AssortmentInventoryRuleDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when this instance is being inserted into the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent assortment</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, Assortment parent)
        {
            AssortmentInventoryRuleDto dto = GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IAssortmentInventoryRuleDal dal = dalContext.GetDal<IAssortmentInventoryRuleDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<AssortmentInventoryRule>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when this instance is being updated in the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent assortment</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, Assortment parent)
        {
            if (this.IsSelfDirty)
            {
                using (IAssortmentInventoryRuleDal dal = dalContext.GetDal<IAssortmentInventoryRuleDal>())
                {
                    dal.Update(GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this instance is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        private void Child_DeleteSelf(IDalContext dalContext, Assortment parent)
        {
            using (IAssortmentInventoryRuleDal dal = dalContext.GetDal<IAssortmentInventoryRuleDal>())
            {
                dal.DeleteById(this.Id);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #endregion
    }
}
