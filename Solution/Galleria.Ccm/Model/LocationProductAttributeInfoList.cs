﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
// V8-27630 : I.George
// Removed Unused Factory methods
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a list containing LocationProductAttributeInfo objects
    /// </summary>
    [Serializable]
    public partial class LocationProductAttributeInfoList : ModelReadOnlyList<LocationProductAttributeInfoList, LocationProductAttributeInfo>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationProductAttributeInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(LocationProductAttributeInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(LocationProductAttributeInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(LocationProductAttributeInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Criteria
        /// <summary>
        /// Criteria for FetchByEntityId
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : Csla.CriteriaBase<FetchByEntityIdCriteria>
        {
            #region Properties

            #region EntityId
            /// <summary>
            /// Entity id property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            /// <summary>
            /// Entity Id
            /// </summary>
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="entityId">The entity id</param>
            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
            }
            #endregion
        }

         /// <summary>
        /// Criteria for use with FetchByEntityIdSearchCriteria
        /// </summary>
        [Serializable]
        public class FetchByEntityIdSearchCriteriaCriteria : CriteriaBase<FetchByEntityIdSearchCriteriaCriteria>
        {
            public static PropertyInfo<Int32> EntityIdProperty = RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static PropertyInfo<String> SearchCriteriaProperty = RegisterProperty<String>(c => c.SearchCriteria);
            public String SearchCriteria
            {
                get { return ReadProperty<String>(SearchCriteriaProperty); }
            }

            public FetchByEntityIdSearchCriteriaCriteria(Int32 entityId, String searchCriteria)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<String>(SearchCriteriaProperty, searchCriteria);
            }
        }

        #endregion
    }
}
