﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created (Auto-generated)
// V8-26721 : A.Silva ~ Added IValidationTemplateGroup
// V8-26815 : A.Silva ~ Added new factory method to create a group from a pre existing IValidationTemplateGroup.
// V8-26812 : A.Silva ~ Addded ValidationType property.
// V8-27152 : A.Silva ~ Corrected the creation of a new Validation Template Group from another IValidationTemplateGroup object.
// V8-28553 : A.Kuszyk
//  Added business rule for unique list of metrics in group.
//  Added metric list instantiation to Create method.
//  Added OnChildChanged event handler to update unique list rule when metrics are changed or added/removed.
#endregion

#region Version History: (CCM v8.01)
// V8-28023 : L.Ineson
//  Added NewValidationTemplateGroup(String name)
#endregion

#region Version History : CCM 811
// V8-28563 : A.Kuszyk
//  Changed UniqueList to framework version.
#endregion

#region Version History: (CCM v8.20)
// V8-31533 : A.Probyn
//  Updated lazy loaded properties to not be lazy loaded as they were causing serialization exceptions
//  when used in the workflow client.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Rules;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// ValidationTemplateGroup Model object
    /// </summary>
    [Serializable]
    public sealed partial class ValidationTemplateGroup : ModelObject<ValidationTemplateGroup>, IValidationTemplateGroup
    {
        #region Static Constructor

        static ValidationTemplateGroup()
        {
            MappingHelper.InitializeMappings();
        }

        #endregion

        #region Parent

        /// <summary>
        ///     Returns a reference to the parent object
        /// </summary>
        public new ValidationTemplate Parent
        {
            get
            {
                var parentList = base.Parent as ValidationTemplateGroupList;
                if (parentList != null)
                {
                    return parentList.Parent as ValidationTemplate;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id

        /// <summary>
        ///     Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);

        /// <summary>
        ///     Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty(IdProperty); }
        }

        #endregion

        #region Name

        /// <summary>
        ///     Name property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        ///     Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty(NameProperty); }
            set { SetProperty(NameProperty, value); }
        }

        #endregion

        #region Threshold1

        /// <summary>
        ///     Threshold1 property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Single> Threshold1Property =
            RegisterModelProperty<Single>(c => c.Threshold1);

        /// <summary>
        ///     Gets/Sets the Threshold1 value
        /// </summary>
        public Single Threshold1
        {
            get { return GetProperty(Threshold1Property); }
            set { SetProperty(Threshold1Property, value); }
        }

        #endregion

        #region Threshold2

        /// <summary>
        ///     Threshold2 property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Single> Threshold2Property =
            RegisterModelProperty<Single>(c => c.Threshold2);

        /// <summary>
        ///     Gets/Sets the Threshold2 value
        /// </summary>
        public Single Threshold2
        {
            get { return GetProperty(Threshold2Property); }
            set { SetProperty(Threshold2Property, value); }
        }

        #endregion

        #region Groups

        /// <summary>
        ///     Metrics property definition
        /// </summary>
        private static readonly ModelPropertyInfo<ValidationTemplateGroupMetricList> MetricListProperty =
            RegisterModelProperty<ValidationTemplateGroupMetricList>(o => o.Metrics);

        /// <summary>
        ///     Gets the Metrics.
        /// </summary>
        public ValidationTemplateGroupMetricList Metrics
        {
            get
            {
                return GetProperty(MetricListProperty);
            }
        }

        #endregion

        #region ValidationType

        /// <summary>
        ///		Metadata for the <see cref="ValidationType"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramValidationType> ValidationTypeProperty =
            RegisterModelProperty<PlanogramValidationType>(o => o.ValidationType);

        /// <summary>
        ///		Gets or sets the value for the <see cref="ValidationType"/> property.
        /// </summary>
        public PlanogramValidationType ValidationType
        {
            get { return this.GetProperty<PlanogramValidationType>(ValidationTypeProperty); }
            set { this.SetProperty<PlanogramValidationType>(ValidationTypeProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        ///     Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));
            BusinessRules.AddRule(new UniqueList<ValidationTemplateGroupMetric>(
                MetricListProperty, ValidationTemplateGroupMetric.FieldProperty, "Metrics", "a Validation Group"));
        }

        #endregion

        #region Authorization Rules

        /// <summary>
        ///     Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ValidationTemplateGroup), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ValidationTemplateGet.ToString()));
            BusinessRules.AddRule(typeof(ValidationTemplateGroup), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ValidationTemplateCreate.ToString()));
            BusinessRules.AddRule(typeof(ValidationTemplateGroup), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ValidationTemplateEdit.ToString()));
            BusinessRules.AddRule(typeof(ValidationTemplateGroup), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ValidationTemplateDelete.ToString()));
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static ValidationTemplateGroup NewValidationTemplateGroup()
        {
            var item = new ValidationTemplateGroup();
            item.Create();
            return item;
        }

        /// <summary>
        ///     Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static ValidationTemplateGroup NewValidationTemplateGroup(String name)
        {
            var item = new ValidationTemplateGroup();
            item.Create(name);
            return item;
        }

        /// <summary>
        ///     Creates and initializes a new instance of <see cref="ValidationTemplateGroup"/>.
        /// </summary>
        /// <param name="source">An instance of a type implementing <see cref="IValidationTemplateGroup"/> to use for the initial data of the new instance.</param>
        /// <returns>A new instance of <see cref="ValidationTemplateGroup"/>.</returns>
        public static ValidationTemplateGroup NewValidationTemplateGroup(IValidationTemplateGroup source)
        {
            var item = new ValidationTemplateGroup();
            item.Create(source);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty(Threshold1Property, 5F);
            this.LoadProperty(Threshold2Property, 10F);
            this.LoadProperty(ValidationTypeProperty, PlanogramValidationType.LessThan);
            this.LoadProperty<ValidationTemplateGroupMetricList>(MetricListProperty, ValidationTemplateGroupMetricList.NewValidationTemplateGroupMetricList());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create(String name)
        {
            this.LoadProperty<String>(NameProperty, name);
            this.LoadProperty(Threshold1Property, 5F);
            this.LoadProperty(Threshold2Property, 10F);
            this.LoadProperty(ValidationTypeProperty, PlanogramValidationType.LessThan);
            this.Create();
        }


        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(IValidationTemplateGroup source)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty(NameProperty, source.Name);
            this.LoadProperty(Threshold1Property, source.Threshold1);
            this.LoadProperty(Threshold2Property, source.Threshold2);
            this.LoadProperty(MetricListProperty, ValidationTemplateGroupMetricList.NewValidationTemplateGroupMetricList(source.Metrics));
            this.LoadProperty(ValidationTypeProperty, source.ValidationType);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        ///     Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty(IdProperty, newId);
            context.RegisterId<ValidationTemplateGroup>(oldId, newId);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when a child object of this group is changed.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnChildChanged(Csla.Core.ChildChangedEventArgs e)
        {
            base.OnChildChanged(e);

            if (e.CollectionChangedArgs != null)
            {
                if (e.ChildObject is ValidationTemplateGroupMetricList)
                {
                    this.BusinessRules.CheckRules(MetricListProperty);
                }
            }
            else if (e.ChildObject is ValidationTemplateGroupMetric)
            {
                this.BusinessRules.CheckRules(MetricListProperty);
            }
        }

        #endregion

        #region IValidationTemplateGroup Members

        IList<IValidationTemplateGroupMetric> IValidationTemplateGroup.Metrics
        {
            get { return Metrics.Select(metric => metric as IValidationTemplateGroupMetric).ToList(); }
        }

        IValidationTemplateGroupMetric IValidationTemplateGroup.AddNewMetric()
        {
            var newItem = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();
            Metrics.Add(newItem);
            return newItem;
        }

        void IValidationTemplateGroup.RemoveMetric(IValidationTemplateGroupMetric groupMetric)
        {
            var removedItem = groupMetric as ValidationTemplateGroupMetric;
            if (removedItem == null || !Metrics.Contains(removedItem)) return;

            Metrics.Remove(removedItem);
        }

        #endregion
    }
}