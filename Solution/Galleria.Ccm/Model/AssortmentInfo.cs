﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#region Version History: (CCM CCM803)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion

#endregion


using System;
using AutoMapper;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A class representing info on a Assortment within GFS
    /// (child of AssortmentInfoList)
    /// </summary>
    [Serializable]
    public partial class AssortmentInfo : ModelReadOnlyObject<AssortmentInfo>
    {
        #region Static Constructor
        static AssortmentInfo()
        {
        }
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32?> ConsumerDecisionTreeIdProperty =
            RegisterModelProperty<Int32?>(c => c.ConsumerDecisionTreeId);
        /// <summary>
        /// The Consumer Decision Tree id
        /// </summary>
        public Int32? ConsumerDecisionTreeId
        {
            get { return GetProperty<Int32?>(ConsumerDecisionTreeIdProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The Assortment Id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        /// <summary>
        /// The entity id
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
        }

        /// <summary>
        /// Product group id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProductGroupIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductGroupId);
        /// <summary>
        /// The id of the product group this item is assigned to
        /// </summary>
        public Int32 ProductGroupId
        {
            get { return GetProperty<Int32>(ProductGroupIdProperty); }
        }

        public static readonly ModelPropertyInfo<String> ProductGroupNameProperty =
            RegisterModelProperty<String>(c => c.ProductGroupName);
        /// <summary>
        /// Product groups Name
        /// </summary>
        public String ProductGroupName
        {
            get { return GetProperty<String>(ProductGroupNameProperty); }
        }

        public static readonly ModelPropertyInfo<String> ProductGroupCodeProperty =
            RegisterModelProperty<String>(c => c.ProductGroupCode);
        /// <summary>
        /// Product groups Name
        /// </summary>
        public String ProductGroupCode
        {
            get { return GetProperty<String>(ProductGroupCodeProperty); }
        }

        public static readonly ModelPropertyInfo<String> NameProperty =
           RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Assortments Name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);
        /// <summary>
        /// The Assortments created date.
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
        }


        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        /// <summary>
        /// The Assortments date last modified
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
        }

        /// <summary>
        /// Parent unique content reference property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Guid?> ParentUniqueContentReferenceProperty =
            RegisterModelProperty<Guid?>(c => c.ParentUniqueContentReference);
        /// <summary>
        /// Parent unique content reference
        /// </summary>
        public Guid? ParentUniqueContentReference
        {
            get { return GetProperty<Guid?>(ParentUniqueContentReferenceProperty); }
        }

        #endregion

        #region Helper Properties

        public static readonly ModelPropertyInfo<String> FriendlyProductGroupNameProperty =
            RegisterModelProperty<String>(c => c.FriendlyProductGroupName);
        /// <summary>
        /// Friendly product group : (Assigned category)
        /// </summary>
        public String FriendlyProductGroupName
        {
            get { return String.Concat(ProductGroupCode, " ", ProductGroupName); }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(AssortmentInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(AssortmentInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(AssortmentInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByLatestVersionByName
        /// </summary>
        [Serializable]
        public class FetchLatestVersionByEntityIdNameCriteria : CriteriaBase<FetchLatestVersionByEntityIdNameCriteria>
        {
            #region Properties
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static readonly PropertyInfo<String> NameProperty =
                RegisterProperty<String>(c => c.Name);
            public String Name
            {
                get { return ReadProperty<String>(NameProperty); }
            }
            #endregion

            public FetchLatestVersionByEntityIdNameCriteria(Int32 entityId, String name)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<String>(NameProperty, name);
            }
        }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.Name;
        }

        #endregion
    }
}