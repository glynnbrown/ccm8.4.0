﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class Compression: ModelObject<Compression>
    {
        #region Constants
        public const Int32 OriginalImageId = -1;
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
                RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }

        public static readonly ModelPropertyInfo<String> NameProperty =
                RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32> WidthProperty =
                RegisterModelProperty<Int32>(c => c.Width, null, null, 100);
        public Int32 Width
        {
            get { return GetProperty<Int32>(WidthProperty); }
            set { SetProperty<Int32>(WidthProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32> HeightProperty =
                RegisterModelProperty<Int32>(c => c.Height, null, null, 100);
        public Int32 Height
        {
            get { return GetProperty<Int32>(HeightProperty); }
            set { SetProperty<Int32>(HeightProperty, value); }
        }

        public static readonly ModelPropertyInfo<Boolean> MaintainAspectRatioProperty =
                RegisterModelProperty<Boolean>(c => c.MaintainAspectRatio, null, null, true);
        public Boolean MaintainAspectRatio
        {
            get { return GetProperty<Boolean>(MaintainAspectRatioProperty); }
            set { SetProperty<Boolean>(MaintainAspectRatioProperty, value); }
        }

        public static readonly ModelPropertyInfo<ImagePixelFormat> ColourDepthProperty =
                RegisterModelProperty<ImagePixelFormat>(c => c.ColourDepth, null, null, ImagePixelFormat.pf16bit);
        public ImagePixelFormat ColourDepth
        {
            get { return GetProperty<ImagePixelFormat>(ColourDepthProperty); }
            set { SetProperty<ImagePixelFormat>(ColourDepthProperty, value); }
        }

        public static readonly ModelPropertyInfo<Boolean> EnabledProperty =
                RegisterModelProperty<Boolean>(c => c.Enabled, null, null, true);
        public Boolean Enabled
        {
            get { return GetProperty<Boolean>(EnabledProperty); }
            set { SetProperty<Boolean>(EnabledProperty, value); }
        }
        #endregion

        #region Helper Properties

        /// <summary>
        /// Get the greatest common divider
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        private Int32 getGCD(Int32 a, Int32 b)
        {
            return b == 0 ? a : getGCD(b, a % b);
        }

        /// <summary>
        /// Creates a string to show the ratio between two integers
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        private String CreateRatio(Int32 a, Int32 b)
        {
            Int32 gcd = getGCD(a, b);
            gcd = gcd == 0 ? 1 : gcd;
            return String.Format("{0}:{1}", Width / gcd, Height / gcd);
        }

        public String Description
        {
            get
            {
                return String.Format(Message.Compression_DescriptionFormatString,
                    Name, CreateRatio(Width, Height), Width, Height,
                    Math.Round((Width * Height) / 1000000.0f, 2),
                    PixelFormatFriendlyNames[ColourDepth]);
            }
        }

        public static Dictionary<ImagePixelFormat, Int16> PixelFormatFriendlyNames = new Dictionary<ImagePixelFormat, Int16>()
        {
            {ImagePixelFormat.pf8bit, 8}, 
            {ImagePixelFormat.pf16bit, 16},            
            {ImagePixelFormat.pf24bit, 24}, 
            {ImagePixelFormat.pf32bit, 32}
        };

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(Compression), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(Compression), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(Compression), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(Compression), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Authenticated.ToString()));
        }
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(NameProperty));
        }
        #endregion

        #region Factory Methods

        public static Compression NewCompression()
        {
            Compression item = new Compression();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Overrides
        public override string ToString()
        {
            return Description;
        }
        #endregion
    }
}
