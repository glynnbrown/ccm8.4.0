﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25559 : L.Ineson
//	Copied from SA
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
#endregion
#endregion

using System;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model implementation of the Connection (Child of ConnectionList)
    /// </summary>
    [Serializable]
    public sealed partial class Connection : ModelObject<Connection>
    {
        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region Type
        ///// <summary>
        ///// The connection type
        ///// </summary>
        //public static readonly ModelPropertyInfo<ConnectionType> TypeProperty =
        //    RegisterModelProperty<ConnectionType>(c => c.Type);
        //public ConnectionType Type
        //{
        //    get { return GetProperty<ConnectionType>(TypeProperty); }
        //    set { SetProperty<ConnectionType>(TypeProperty, value); }
        //}
        #endregion

        #region Server Name
        /// <summary>
        /// ServerName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ServerNameProperty =
            RegisterModelProperty<String>(c => c.ServerName);
        /// <summary>
        /// The connection server name
        /// </summary>
        public String ServerName
        {
            get { return GetProperty<String>(ServerNameProperty); }
            set { SetProperty<String>(ServerNameProperty, value); }
        }
        #endregion

        #region DatabaseName
        /// <summary>
        /// DatabaseName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DatabaseNameProperty =
            RegisterModelProperty<String>(c => c.DatabaseName);
        /// <summary>
        /// The connection database name
        /// </summary>
        public String DatabaseName
        {
            get { return GetProperty<String>(DatabaseNameProperty); }
            set { SetProperty<String>(DatabaseNameProperty, value); }
        }
        #endregion

        #region DatabaseFileName
        /// <summary>
        /// DatabaseFileName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DatabaseFileNameProperty =
            RegisterModelProperty<String>(c => c.DatabaseFileName);
        /// <summary>
        /// The connection database file name
        /// </summary>
        public String DatabaseFileName
        {
            get { return GetProperty<String>(DatabaseFileNameProperty); }
            set { SetProperty<String>(DatabaseFileNameProperty, value); }
        }
        #endregion

        #region HostName
        /// <summary>
        /// HostName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> HostNameProperty =
            RegisterModelProperty<String>(c => c.HostName);
        /// <summary>
        /// The connection host name
        /// </summary>
        public String HostName
        {
            get { return GetProperty<String>(HostNameProperty); }
            set { SetProperty<String>(HostNameProperty, value); }
        }
        #endregion

        #region PortNumber
        /// <summary>
        /// PortNumber property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> PortNumberProperty =
            RegisterModelProperty<Byte>(c => c.PortNumber);
        /// <summary>
        /// The connection port number
        /// </summary>
        public Byte PortNumber
        {
            get { return GetProperty<Byte>(PortNumberProperty); }
            set { SetProperty<Byte>(PortNumberProperty, value); }
        }
        #endregion

        #region BusinessEntityId
        /// <summary>
        /// BusinessEntityId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> BusinessEntityIdProperty =
            RegisterModelProperty<Int32>(c => c.BusinessEntityId);
        /// <summary>
        /// The connection business entity id
        /// </summary>
        public Int32 BusinessEntityId
        {
            get { return GetProperty<Int32>(BusinessEntityIdProperty); }
            set { SetProperty<Int32>(BusinessEntityIdProperty, value); }
        }
        #endregion

        #region IsAutoConnect
        /// <summary>
        /// IsAutoConnect property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsAutoConnectProperty =
            RegisterModelProperty<Boolean>(c => c.IsAutoConnect);
        /// <summary>
        /// The connection is auto connect
        /// </summary>
        public Boolean IsAutoConnect
        {
            get { return GetProperty<Boolean>(IsAutoConnectProperty); }
            set { SetProperty<Boolean>(IsAutoConnectProperty, value); }
        }
        #endregion

        #region IsCurrentConnection
        /// <summary>
        /// IsCurrentConnection property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsCurrentConnectionProperty =
            RegisterModelProperty<Boolean>(c => c.IsCurrentConnection);
        /// <summary>
        /// The connection is current connection or not
        /// </summary>
        public Boolean IsCurrentConnection
        {
            get { return GetProperty<Boolean>(IsCurrentConnectionProperty); }
            set { SetProperty<Boolean>(IsCurrentConnectionProperty, value); }
        }
        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new metric
        /// </summary>
        /// <returns>Returns the new metric</returns>
        public static Connection NewConnection()
        {
            Connection connection = new Connection();
            connection.Create();
            return connection;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new Metric object
        /// </summary>
        private void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            //LoadProperty<ConnectionType>(TypeProperty, ConnectionType.Database);
            this.LoadProperty<String>(ServerNameProperty, String.Empty);
            this.LoadProperty<String>(DatabaseNameProperty, String.Empty);
            this.LoadProperty<String>(DatabaseFileNameProperty, String.Empty);
            this.LoadProperty<String>(HostNameProperty, String.Empty);
            this.LoadProperty<Byte>(PortNumberProperty, 0);
            this.LoadProperty<Int32>(BusinessEntityIdProperty, 0);
            this.LoadProperty<Boolean>(IsAutoConnectProperty, false);
            this.LoadProperty<Boolean>(IsCurrentConnectionProperty, true);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

    }
}
