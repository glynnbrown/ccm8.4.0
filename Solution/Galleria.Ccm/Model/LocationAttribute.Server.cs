﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Csla;

namespace Galleria.Ccm.Model
{
    public partial class LocationAttribute
    {
        #region Constructor
        private LocationAttribute() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the store attributes from the solution
        /// </summary>
        /// <returns></returns>
        public static LocationAttribute FetchLocationAttributes(Int32 entityId)
        {
            //Call fetch using entity id stored in application global context
            return DataPortal.Fetch<LocationAttribute>(entityId);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads a dtos into this object
        /// </summary>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(LocationAttributeDto dto)
        {
            LoadProperty<Boolean>(HasRegionProperty, dto.HasRegion);
            LoadProperty<Boolean>(HasCountyProperty, dto.HasCounty);
            LoadProperty<Boolean>(HasTVRegionProperty, dto.HasTVRegion);
            LoadProperty<Boolean>(HasAddress1Property, dto.HasAddress1);
            LoadProperty<Boolean>(HasAddress2Property, dto.HasAddress2);
            LoadProperty<Boolean>(Has24HoursProperty, dto.Has24Hours);
            LoadProperty<Boolean>(HasPetrolForecourtProperty, dto.HasPetrolForecourt);
            LoadProperty<Boolean>(HasPetrolForecourtTypeProperty, dto.HasPetrolForecourtType);
            LoadProperty<Boolean>(HasRestaurantProperty, dto.HasRestaurant);
            LoadProperty<Boolean>(HasMezzFittedProperty, dto.HasMezzFitted);
            LoadProperty<Boolean>(HasSizeGrossAreaProperty, dto.HasSizeGrossArea);
            LoadProperty<Boolean>(HasSizeNetSalesAreaProperty, dto.HasSizeNetSalesArea);
            LoadProperty<Boolean>(HasSizeMezzSalesAreaProperty, dto.HasSizeMezzSalesArea);
            LoadProperty<Boolean>(HasCityProperty, dto.HasCity);
            LoadProperty<Boolean>(HasCountryProperty, dto.HasCountry);
            LoadProperty<Boolean>(HasManagerNameProperty, dto.HasManagerName);
            LoadProperty<Boolean>(HasRegionalManagerNameProperty, dto.HasRegionalManagerName);
            LoadProperty<Boolean>(HasDivisionalManagerNameProperty, dto.HasDivisionalManagerName);
            LoadProperty<Boolean>(HasAdvertisingZoneProperty, dto.HasAdvertisingZone);
            LoadProperty<Boolean>(HasDistributionCentreProperty, dto.HasDistributionCentre);
            LoadProperty<Boolean>(HasAtmMachinesProperty, dto.HasAtmMachines);
            LoadProperty<Boolean>(HasNoOfCheckoutsProperty, dto.HasNoOfCheckouts);
            LoadProperty<Boolean>(HasCustomerWCProperty, dto.HasCustomerWC);
            LoadProperty<Boolean>(HasBabyChangingProperty, dto.HasBabyChanging);
            LoadProperty<Boolean>(HasInLocationBakeryProperty, dto.HasInLocationBakery);
            LoadProperty<Boolean>(HasHotFoodToGoProperty, dto.HasHotFoodToGo);
            LoadProperty<Boolean>(HasRotisserieProperty, dto.HasRotisserie);
            LoadProperty<Boolean>(HasFishmongerProperty, dto.HasFishmonger);
            LoadProperty<Boolean>(HasButcherProperty, dto.HasButcher);
            LoadProperty<Boolean>(HasPizzaProperty, dto.HasPizza);
            LoadProperty<Boolean>(HasDeliProperty, dto.HasDeli);
            LoadProperty<Boolean>(HasSaladBarProperty, dto.HasSaladBar);
            LoadProperty<Boolean>(HasOrganicProperty, dto.HasOrganic);
            LoadProperty<Boolean>(HasGroceryProperty, dto.HasGrocery);
            LoadProperty<Boolean>(HasMobilePhonesProperty, dto.HasMobilePhones);
            LoadProperty<Boolean>(HasDryCleaningProperty, dto.HasDryCleaning);
            LoadProperty<Boolean>(HasHomeShoppingAvailableProperty, dto.HasHomeShoppingAvailable);
            LoadProperty<Boolean>(HasOpticianProperty, dto.HasOptician);
            LoadProperty<Boolean>(HasPharmacyProperty, dto.HasPharmacy);
            LoadProperty<Boolean>(HasTravelProperty, dto.HasTravel);
            LoadProperty<Boolean>(HasPhotoDepartmentProperty, dto.HasPhotoDepartment);
            LoadProperty<Boolean>(HasCarServiceAreaProperty, dto.HasCarServiceArea);
            LoadProperty<Boolean>(HasGardenCentreProperty, dto.HasGardenCentre);
            LoadProperty<Boolean>(HasClinicProperty, dto.HasClinic);
            LoadProperty<Boolean>(HasAlcoholProperty, dto.HasAlcohol);
            LoadProperty<Boolean>(HasFashionProperty, dto.HasFashion);
            LoadProperty<Boolean>(HasCafeProperty, dto.HasCafe);
            LoadProperty<Boolean>(HasRecyclingProperty, dto.HasRecycling);
            LoadProperty<Boolean>(HasPhotocopierProperty, dto.HasPhotocopier);
            LoadProperty<Boolean>(HasLotteryProperty, dto.HasLottery);
            LoadProperty<Boolean>(HasPostOfficeProperty, dto.HasPostOffice);
            LoadProperty<Boolean>(HasMovieRentalProperty, dto.HasMovieRental);
            LoadProperty<Boolean>(HasNewsCubeProperty, dto.HasNewsCube);
            LoadProperty<Boolean>(HasOpenMondayProperty, dto.HasOpenMonday);
            LoadProperty<Boolean>(HasOpenTuesdayProperty, dto.HasOpenTuesday);
            LoadProperty<Boolean>(HasOpenWednesdayProperty, dto.HasOpenWednesday);
            LoadProperty<Boolean>(HasOpenThursdayProperty, dto.HasOpenThursday);
            LoadProperty<Boolean>(HasOpenFridayProperty, dto.HasOpenFriday);
            LoadProperty<Boolean>(HasOpenSaturdayProperty, dto.HasOpenSaturday);
            LoadProperty<Boolean>(HasOpenSundayProperty, dto.HasOpenSunday);
            LoadProperty<Boolean>(HasJewelleryProperty, dto.HasJewellery);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(Int32 entityId)
        {
            try
            {
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (ILocationAttributeDal dal = dalContext.GetDal<ILocationAttributeDal>())
                    {
                        LocationAttributeDto dto = dal.FetchByEntityId(entityId);
                        LoadDataTransferObject(dto);
                    }
                }
            }
            catch (DtoDoesNotExistException)
            {
                //do nothing
            }
        }
        #endregion

        #endregion

    }
}
