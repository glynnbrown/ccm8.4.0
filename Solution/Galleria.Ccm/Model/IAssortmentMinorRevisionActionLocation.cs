﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Interface defining common properties / methods between minor revision action locations
    /// </summary>
    public interface IAssortmentMinorRevisionActionLocation
    {
        /// <summary>
        /// Returns the Location id
        /// </summary>
        Int16? LocationId { get; }

        /// <summary>
        /// Returns the Location code
        /// </summary>
        String LocationCode { get; }

        /// <summary>
        /// Returns the Location name
        /// </summary>
        String LocationName { get; }
    }
}