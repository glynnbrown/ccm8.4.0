﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//		Created (Auto-generated)
// V8-25556 : D.Pleasance
//      Added FetchByEntityIdContext
#endregion
#endregion

using System;
using System.Linq;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    public partial class ProductHierarchy
    {
        #region Constructors
        private ProductHierarchy() { } //force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing ProductHierarchy with the given id
        /// </summary>
        public static ProductHierarchy FetchById(Int32 id)
        {
            return DataPortal.Fetch<ProductHierarchy>(new FetchByIdCriteria(id));
        }

        /// <summary>
        /// Returns the hierarchy for the given entity id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static ProductHierarchy FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<ProductHierarchy>(new FetchByEntityIdCriteria(entityId));
        }

        /// <summary>
        /// Returns the hierarchy structure for the given entity id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static ProductHierarchy FetchByEntityIdContext(Int32 entityId, IDalContext context)
        {
            return DataPortal.Fetch<ProductHierarchy>(new FetchProductHierarchyByEntityIdContextCriteria(entityId, context));
        }


        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, 
            ProductHierarchyDto dto,
            IEnumerable<ProductLevelDto> childProductLevelList,
            IEnumerable<ProductGroupDto> childProductGroupList)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);

            //get the root level dto and load it
            ProductLevelDto rootLevelDto = childProductLevelList.FirstOrDefault(l => Object.Equals(l.ParentLevelId, null));
            if (rootLevelDto != null)
            {
                LoadProperty<ProductLevel>(RootLevelProperty, ProductLevel.Fetch(dalContext, rootLevelDto, childProductLevelList));
            }

            //get the root group dto and load
            ProductGroupDto rootGroupDto = childProductGroupList.FirstOrDefault(l => Object.Equals(l.ParentGroupId, null));
            if (rootGroupDto != null)
            {
                LoadProperty<ProductGroup>(RootGroupProperty,
                    ProductGroup.Fetch(dalContext, rootGroupDto, childProductGroupList));
            }
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private ProductHierarchyDto GetDataTransferObject()
        {
            return new ProductHierarchyDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                Name = ReadProperty<String>(NameProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning FetchById
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            Int32 hierarchyId = criteria.Id;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                // fetch the merchandising hierarch structure dto
                ProductHierarchyDto dto;
                using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                {
                    dto = dal.FetchById(hierarchyId);
                }

                //now use the hierarchy id to get all the levels in one go
                IEnumerable<ProductLevelDto> productLevelDtoList;
                using (IProductLevelDal dal = dalContext.GetDal<IProductLevelDal>())
                {
                    productLevelDtoList = dal.FetchByProductHierarchyId(hierarchyId);
                }

                //and get all the groups in one go
                IEnumerable<ProductGroupDto> productGroupDtoList;
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    productGroupDtoList = dal.FetchByProductHierarchyId(hierarchyId);
                }

                //load the hierarchy
                LoadDataTransferObject(dalContext, dto, productLevelDtoList, productGroupDtoList);
            }
        }

        /// <summary>
        /// Called when return FetchByEntityId
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            Int32 entityId = criteria.EntityId;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                // fetch the merchandising hierarch structure dto
                ProductHierarchyDto dto;
                using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                {
                    dto = dal.FetchByEntityId(entityId);
                }

                //now use the hierarchy id to get all the levels in one go
                IEnumerable<ProductLevelDto> productLevelDtoList;
                using (IProductLevelDal dal = dalContext.GetDal<IProductLevelDal>())
                {
                    productLevelDtoList = dal.FetchByProductHierarchyId(dto.Id);
                }

                //and get all the groups in one go
                IEnumerable<ProductGroupDto> productGroupDtoList;
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    productGroupDtoList = dal.FetchByProductHierarchyId(dto.Id);
                }

                //load the hierarchy
                LoadDataTransferObject(dalContext, dto, productLevelDtoList, productGroupDtoList);
            }
        }

        /// <summary>
        /// Called when return FetchProductHierarchyByEntityIdContextCriteria
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(ProductHierarchy.FetchProductHierarchyByEntityIdContextCriteria criteria)
        {
            Int32 entityId = criteria.EntityId;

            // fetch the merchandising hierarch structure dto
            ProductHierarchyDto dto;
            using (IProductHierarchyDal dal = criteria.Context.GetDal<IProductHierarchyDal>())
            {
                dto = dal.FetchByEntityId(entityId);
            }

            //now use the hierarchy id to get all the levels in one go
            IEnumerable<ProductLevelDto> productLevelDtoList;
            using (IProductLevelDal dal = criteria.Context.GetDal<IProductLevelDal>())
            {
                productLevelDtoList = dal.FetchByProductHierarchyId(dto.Id);
            }

            //and get all the groups in one go
            IEnumerable<ProductGroupDto> productGroupDtoList;
            using (IProductGroupDal dal = criteria.Context.GetDal<IProductGroupDal>())
            {
                productGroupDtoList = dal.FetchByProductHierarchyId(dto.Id);
            }

            //load the hierarchy
            LoadDataTransferObject(criteria.Context, dto, productLevelDtoList, productGroupDtoList);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                ProductHierarchyDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<ProductHierarchy>(oldId, dto.Id);
                DataPortal.UpdateChild(this.RootLevel, dalContext, this);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this instance
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                ProductHierarchyDto dto = GetDataTransferObject();
                using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                DataPortal.UpdateChild(this.RootLevel, dalContext, this);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when this instance is being deleted
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #endregion

    }
}