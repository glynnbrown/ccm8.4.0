﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
//CCM-25460 : L.Ineson
//  Added FetchBySearchCriteria
// CCM-25460 : L.Ineson
//  AddedFetchByIds
// CCM-26520 :J.Pickup
//  Added FetchByCategoryCode & FetchByNullCatgeoryCode
// CCM-27599 : J.Pickup
//  Added Entity to FetchBySearchCriteria

#endregion
#region Version History: (CCM 801)
// V8-28507 : D.Pleasance
//  Added FetchByWorkpackageIdPagingCriteria \ FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria
#endregion
#region Version History: CCM810
// V8-29986 : A.Probyn
//  Added FetchNonDebugByCategoryCode
// V8-30213 : L.Luong
//  Added FetchNonDebugByLocationCode
#endregion
#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramInfoList
    {
        #region Constructors
        public PlanogramInfoList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        #region FetchByPlanogramGroupId
        /// <summary>
        /// Returns all planograms for the specified planogram group
        /// </summary>
        public static PlanogramInfoList FetchByPlanogramGroupId(Int32 planogramGroupId)
        {
            return DataPortal.Fetch<PlanogramInfoList>(new FetchByPlanogramGroupIdCriteria(planogramGroupId));
        }
        #endregion

        #region FetchByWorkpackageId
        /// <summary>
        /// Returns all planograms for the specified workpackage
        /// </summary>
        public static PlanogramInfoList FetchByWorkpackageId(Int32 workpackageId)
        {
            return DataPortal.Fetch<PlanogramInfoList>(new FetchByWorkpackageIdCriteria(workpackageId));
        }
        #endregion

        #region FetchByWorkpackageIdPagingCriteria
        /// <summary>
        /// Returns all planograms for the specified workpackage, page number, page size
        /// </summary>
        public static PlanogramInfoList FetchByWorkpackageIdPagingDetail(Int32 workpackageId, Byte pageNumber, Int16 pageSize)
        {
            return DataPortal.Fetch<PlanogramInfoList>(new FetchByWorkpackageIdPagingCriteria(workpackageId, pageNumber, pageSize));
        }
        #endregion

        #region FetchByWorkpackageIdAutomationProcessingStatus
        /// <summary>
        /// Returns all planograms for the specified workpackage and automation status
        /// </summary>
        public static PlanogramInfoList FetchByWorkpackageIdAutomationProcessingStatus(Int32 workpackageId, ProcessingStatus automationStatus)
        {
            return DataPortal.Fetch<PlanogramInfoList>(new FetchByWorkpackageIdAutomationProcessingStatusCriteria(workpackageId, automationStatus));
        }
        #endregion

        #region FetchByWorkpackageIdAutomationProcessingStatusPagingDetail
        /// <summary>
        /// Returns all planograms for the specified workpackage and automation status, page number, page size
        /// </summary>
        public static PlanogramInfoList FetchByWorkpackageIdAutomationProcessingStatusPagingDetail(Int32 workpackageId, List<ProcessingStatus> automationStatusList, Byte pageNumber, Int16 pageSize)
        {
            return DataPortal.Fetch<PlanogramInfoList>(new FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria(workpackageId, automationStatusList, pageNumber, pageSize));
        }
        #endregion

        #region FetchBySearchCriteria
        /// <summary>
        /// Returns all planograms which match the specified search criteria.
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="name"></param>
        /// <param name="planogramGroupId"></param>
        /// <returns></returns>
        public static PlanogramInfoList FetchBySearchCriteria(String name, Int32? planogramGroupId, Int32 entityId)
        {
            return DataPortal.Fetch<PlanogramInfoList>(new FetchBySearchCriteriaCriteria(name, planogramGroupId, entityId));
        }
        #endregion

        #region FetchByIds
        /// <summary>
        /// Returns all planograms which match the given ids.
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="name"></param>
        /// <param name="planogramGroupId"></param>
        /// <returns></returns>
        public static PlanogramInfoList FetchByIds(IEnumerable<Int32> planIdList)
        {
            return DataPortal.Fetch<PlanogramInfoList>(new FetchByIdsCriteria(planIdList));
        }
        #endregion

        #region FetchByCategoryCode

        /// <summary>
        /// Returns all planograms for the specified planogram group
        /// </summary>
        public static PlanogramInfoList FetchByCategoryCode(String categoryCode)
        {
            return DataPortal.Fetch<PlanogramInfoList>(new FetchByCategoryCodeCriteria(categoryCode));
        }

        #endregion

        #region FetchByNullCategoryCode

        /// <summary>
        /// Returns all planograms for the specified planogram group
        /// </summary>
        public static PlanogramInfoList FetchByNullCategoryCode()
        {
            return DataPortal.Fetch<PlanogramInfoList>(new FetchByNullCategoryCodeCriteria());
        }

        #endregion

        #region FetchNonDebugByCategoryCode

        /// <summary>
        /// Returns all non debug planograms for the specified planogram group
        /// </summary>
        public static PlanogramInfoList FetchNonDebugByCategoryCode(String categoryCode)
        {
            return DataPortal.Fetch<PlanogramInfoList>(new FetchNonDebugByCategoryCodeCriteria(categoryCode));
        }

        #endregion

        #region FetchNonDebugByLocationCode

        /// <summary>
        /// Returns all non debug planograms for the specified planogram group
        /// </summary>
        public static PlanogramInfoList FetchNonDebugByLocationCode(String locationCode)
        {
            return DataPortal.Fetch<PlanogramInfoList>(new FetchNonDebugByLocationCodeCriteria(locationCode));
        }

        #endregion

        #endregion

        #region Data Access

        /// <summary>
        /// Called when returning all planograms for the specified planogram group
        /// </summary>
        private void DataPortal_Fetch(FetchByPlanogramGroupIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramInfoDal dal = dalContext.GetDal<IPlanogramInfoDal>())
                {
                    IEnumerable<PlanogramInfoDto> dtoList = dal.FetchByPlanogramGroupId(criteria.PlanogramGroupId);
                    foreach (PlanogramInfoDto dto in dtoList)
                    {
                        this.Add(PlanogramInfo.GetPlanogramInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returning all planograms for the specified workpackage
        /// </summary>
        private void DataPortal_Fetch(FetchByWorkpackageIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramInfoDal dal = dalContext.GetDal<IPlanogramInfoDal>())
                {
                    IEnumerable<PlanogramInfoDto> dtoList = dal.FetchByWorkpackageId(criteria.WorkpackageId);
                    foreach (PlanogramInfoDto dto in dtoList)
                    {
                        this.Add(PlanogramInfo.GetPlanogramInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returning all planograms for the specified workpackage, page number, page size
        /// </summary>
        private void DataPortal_Fetch(FetchByWorkpackageIdPagingCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramInfoDal dal = dalContext.GetDal<IPlanogramInfoDal>())
                {
                    IEnumerable<PlanogramInfoDto> dtoList = dal.FetchByWorkpackageIdPagingCriteria(criteria.WorkpackageId, criteria.PageNumber, criteria.PageSize);
                    foreach (PlanogramInfoDto dto in dtoList)
                    {
                        this.Add(PlanogramInfo.GetPlanogramInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returning all planograms for the specified workpackage and automation status
        /// </summary>
        private void DataPortal_Fetch(FetchByWorkpackageIdAutomationProcessingStatusCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramInfoDal dal = dalContext.GetDal<IPlanogramInfoDal>())
                {
                    IEnumerable<PlanogramInfoDto> dtoList = dal.FetchByWorkpackageIdAutomationProcessingStatus(criteria.WorkpackageId, (Byte)criteria.AutomationStatus);
                    foreach (PlanogramInfoDto dto in dtoList)
                    {
                        this.Add(PlanogramInfo.GetPlanogramInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returning all planograms for the specified workpackage and automation status, page number, page size
        /// </summary>
        private void DataPortal_Fetch(FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramInfoDal dal = dalContext.GetDal<IPlanogramInfoDal>())
                {
                    IEnumerable<PlanogramInfoDto> dtoList = dal.FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria(
                        criteria.WorkpackageId, criteria.AutomationStatusList.Select(s=> (Byte)s).ToList(), 
                        criteria.PageNumber, criteria.PageSize);

                    foreach (PlanogramInfoDto dto in dtoList)
                    {
                        this.Add(PlanogramInfo.GetPlanogramInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returning all planograms for the specified planogram group
        /// </summary>
        private void DataPortal_Fetch(FetchBySearchCriteriaCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramInfoDal dal = dalContext.GetDal<IPlanogramInfoDal>())
                {
                    IEnumerable<PlanogramInfoDto> dtoList = 
                        dal.FetchBySearchCriteria(criteria.Name, criteria.PlanogramGroupId, criteria.EntityId);
                    
                    foreach (PlanogramInfoDto dto in dtoList)
                    {
                        this.Add(PlanogramInfo.GetPlanogramInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returning all planograms for the given ids
        /// </summary>
        private void DataPortal_Fetch(FetchByIdsCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramInfoDal dal = dalContext.GetDal<IPlanogramInfoDal>())
                {
                    IEnumerable<PlanogramInfoDto> dtoList = dal.FetchByIds(criteria.IdList);

                    foreach (PlanogramInfoDto dto in dtoList)
                    {
                        this.Add(PlanogramInfo.GetPlanogramInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }



        /// <summary>
        /// Called when returning all planograms for the specified category code
        /// </summary>
        private void DataPortal_Fetch(FetchByCategoryCodeCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramInfoDal dal = dalContext.GetDal<IPlanogramInfoDal>())
                {
                    IEnumerable<PlanogramInfoDto> dtoList = dal.FetchByCategoryCode(criteria.CatgeoryCode);
                    foreach (PlanogramInfoDto dto in dtoList)
                    {
                        this.Add(PlanogramInfo.GetPlanogramInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returning all planograms for the specified category code
        /// </summary>
        private void DataPortal_Fetch(FetchNonDebugByCategoryCodeCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramInfoDal dal = dalContext.GetDal<IPlanogramInfoDal>())
                {
                    IEnumerable<PlanogramInfoDto> dtoList = dal.FetchNonDebugByCategoryCode(criteria.CatgeoryCode);
                    foreach (PlanogramInfoDto dto in dtoList)
                    {
                        this.Add(PlanogramInfo.GetPlanogramInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returning all planograms for the specified location code
        /// </summary>
        private void DataPortal_Fetch(FetchNonDebugByLocationCodeCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramInfoDal dal = dalContext.GetDal<IPlanogramInfoDal>())
                {
                    IEnumerable<PlanogramInfoDto> dtoList = dal.FetchNonDebugByLocationCode(criteria.LocationCode);
                    foreach (PlanogramInfoDto dto in dtoList)
                    {
                        this.Add(PlanogramInfo.GetPlanogramInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returning all planograms for the specified category code
        /// </summary>
        private void DataPortal_Fetch(FetchByNullCategoryCodeCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramInfoDal dal = dalContext.GetDal<IPlanogramInfoDal>())
                {
                    IEnumerable<PlanogramInfoDto> dtoList = dal.FetchByNullCategoryCode();
                    foreach (PlanogramInfoDto dto in dtoList)
                    {
                        this.Add(PlanogramInfo.GetPlanogramInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion
    }
}
