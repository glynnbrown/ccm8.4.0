﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
// V8-25886 : L.Ineson
//  Removed value, added values
//  Added IsHidden and IsReadOnly
#endregion
#region Version History: CCM801
// V8-28835 : A.Kuszyk
//  Removed call to FieldManager.UpdateChildren() in delete method.
#endregion
#region Version History: CCM820
// V8-30596 : N.Foster
//  Resolved issue where duplicate tasks in same workflow resulted in exception
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class WorkflowTaskParameter
    {
        #region Constructors
        private WorkflowTaskParameter() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a new instance loaded from a dto
        /// </summary>
        internal static WorkflowTaskParameter GetWorkflowTaskParameter(IDalContext dalContext, WorkflowTaskParameterDto dto, EngineTaskInfo task, EngineTaskParameterInfo parameter)
        {
            return DataPortal.FetchChild<WorkflowTaskParameter>(dalContext, dto, task, parameter);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, WorkflowTaskParameterDto dto, EngineTaskInfo task, EngineTaskParameterInfo parameter)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<EngineTaskInfo>(TaskDetailsProperty, task.Copy());
            this.LoadProperty<EngineTaskParameterInfo>(ParameterDetailsProperty, parameter.Copy());
            this.LoadProperty<Boolean>(IsHiddenProperty, dto.IsHidden);
            this.LoadProperty<Boolean>(IsReadOnlyProperty, dto.IsReadOnly);
            this.LoadProperty<WorkflowTaskParameterValueList>(ValuesProperty,
                WorkflowTaskParameterValueList.FetchByWorkflowTaskParameterId(dalContext, dto.Id));
        }

        /// <summary>
        /// Gets a data transfer object from this instance
        /// </summary>
        private WorkflowTaskParameterDto GetDataTransferObject(WorkflowTask parent)
        {
            return new WorkflowTaskParameterDto()
            {
                Id = this.ReadProperty<Int32>(IdProperty),
                ParameterId = this.ParameterDetails.Id,
                IsHidden = this.ReadProperty<Boolean>(IsHiddenProperty),
                IsReadOnly = this.ReadProperty<Boolean>(IsReadOnlyProperty),
                WorkflowTaskId = parent.Id,
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when loading this instance from a dto
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, WorkflowTaskParameterDto dto, EngineTaskInfo task, EngineTaskParameterInfo parameter)
        {
            this.LoadDataTransferObject(dalContext, dto, task, parameter);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        private void Child_Insert(IDalContext dalContext, WorkflowTask parent)
        {
            WorkflowTaskParameterDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IWorkflowTaskParameterDal dal = dalContext.GetDal<IWorkflowTaskParameterDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<WorkflowTaskParameter>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        private void Child_Update(IDalContext dalContext, WorkflowTask parent)
        {
            if (this.IsSelfDirty)
            {
                using (IWorkflowTaskParameterDal dal = dalContext.GetDal<IWorkflowTaskParameterDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this child is being deleted
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, WorkflowTask parent)
        {
            using (IWorkflowTaskParameterDal dal = dalContext.GetDal<IWorkflowTaskParameterDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion
    }
}
