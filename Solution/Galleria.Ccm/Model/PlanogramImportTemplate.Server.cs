﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27804 : L.Ineson
//  Created
// V8-24779 : L.Luong
//  Added EntityId, RowVersion and Date properties
#endregion
#region Version History: (CCM 8.0.1)
// V8-28622 : D.Pleasance
//  Added PerformanceMetrics
#endregion
#region Version History: (CCM 8.03)
// V8-29220 : L.Ineson
//Removed date deleted
#endregion
#region Version History: (CCM 8.30)
// V8-32360 : M.Brumby
//  [PCR01564] Auto split bays by a fixed value
// V8-32480 : M.Brumby
//  [PCR01564] Make split by fixed with default if there is no split option set
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramImportTemplate
    {
        #region Constructor
        private PlanogramImportTemplate() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing PlanogramImportTemplate with the given string
        /// </summary>
        public static PlanogramImportTemplate FetchByFilename(String fileName)
        {
            return FetchByFilename( fileName, /*asReadOnly*/false);
        }

        /// <summary>
        /// Returns an existing PlanogramImportTemplate with the given string
        /// </summary>
        public static PlanogramImportTemplate FetchByFilename(String fileName, Boolean asReadOnly)
        {
            return DataPortal.Fetch<PlanogramImportTemplate>(new FetchByIdCriteria(PlanogramImportTemplateDalType.FileSystem, fileName, asReadOnly));
        }

        /// <summary>
        /// Returns an existing PlanogramImportTemplate with the given entity id and name.
        /// </summary>
        public static PlanogramImportTemplate FetchByEntityIdName(Int32 entityId, String name)
        {
            return DataPortal.Fetch<PlanogramImportTemplate>(new FetchByEntityIdNameCriteria(entityId, name));
        }

        /// <summary>
        /// Returns an existing PlanogramImportTemplate with the given id
        /// For unit testing purposes.
        /// </summary>
        public static PlanogramImportTemplate FetchById(Object id)
        {
            return DataPortal.Fetch<PlanogramImportTemplate>(new FetchByIdCriteria(PlanogramImportTemplateDalType.Unknown, id));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramImportTemplateDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<PlanogramImportFileType>(FileTypeProperty, (PlanogramImportFileType)dto.FileType);
            this.LoadProperty<String>(FileVersionProperty, dto.FileVersion);
            this.LoadProperty<PlanogramImportTemplateMappingList>(MappingsProperty, PlanogramImportTemplateMappingList.FetchByParentId(dalContext, dto.Id));
            this.LoadProperty<PlanogramImportTemplatePerformanceMetricList>(PerformanceMetricsProperty, PlanogramImportTemplatePerformanceMetricList.FetchByParentId(dalContext, dto.Id));
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<Boolean>(AllowAdvancedBaySplittingProperty, dto.AllowAdvancedBaySplitting);
            this.LoadProperty<Boolean>(CanSplitComponentsProperty, dto.CanSplitComponents);
            this.LoadProperty<Boolean>(SplitBaysByComponentStartProperty, dto.SplitBaysByComponentStart);
            this.LoadProperty<Boolean>(SplitBaysByComponentEndProperty, dto.SplitBaysByComponentEnd);
            this.LoadProperty<Boolean>(SplitByFixedSizeProperty, dto.SplitByFixedSize);
            this.LoadProperty<Boolean>(SplitByRecurringPatternProperty, dto.SplitByRecurringPattern);
            this.LoadProperty<Boolean>(SplitByBayCountProperty, dto.SplitByBayCount);
            this.LoadProperty<Double>(SplitFixedSizeProperty, dto.SplitFixedSize);
            this.LoadProperty<String>(SplitRecurringPatternProperty, dto.SplitRecurringPattern);
            this.LoadProperty<Int32>(SplitBayCountProperty, dto.SplitBayCount);

            //ensure a split selection is always set
            if (!(dto.SplitByFixedSize || dto.SplitByRecurringPattern || dto.SplitByBayCount))
            {
                this.LoadProperty<Boolean>(SplitByFixedSizeProperty, true);
            }
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private PlanogramImportTemplateDto GetDataTransferObject()
        {
            return new PlanogramImportTemplateDto()
            {
                Id = ReadProperty<Object>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                Name = ReadProperty<String>(NameProperty),
                FileType = (Byte)ReadProperty<PlanogramImportFileType>(FileTypeProperty),
                FileVersion = (String)ReadProperty<String>(FileVersionProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                AllowAdvancedBaySplitting = ReadProperty<Boolean>(AllowAdvancedBaySplittingProperty),
                CanSplitComponents = ReadProperty<Boolean>(CanSplitComponentsProperty),
                SplitBaysByComponentStart = ReadProperty<Boolean>(SplitBaysByComponentStartProperty),
                SplitBaysByComponentEnd = ReadProperty<Boolean>(SplitBaysByComponentEndProperty),
                SplitByFixedSize = ReadProperty<Boolean>(SplitByFixedSizeProperty),
                SplitByRecurringPattern = ReadProperty<Boolean>(SplitByRecurringPatternProperty),
                SplitByBayCount = ReadProperty<Boolean>(SplitByBayCountProperty),
                SplitFixedSize = ReadProperty<Double>(SplitFixedSizeProperty),
                SplitRecurringPattern = ReadProperty<String>(SplitRecurringPatternProperty),
                SplitBayCount = ReadProperty<Int32>(SplitBayCountProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching a package by its Id
        /// </summary>
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            //lock the file first
            if (criteria.DalType == PlanogramImportTemplateDalType.FileSystem
                && !criteria.AsReadOnly)
            {
                LockPlanogramImportTemplateByFileName((String)criteria.Id);
            }


            IDalFactory dalFactory = this.GetDalFactory(criteria.DalType);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramImportTemplateDal dal = dalContext.GetDal<IPlanogramImportTemplateDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.Id));
                }
            }
        }

        /// <summary>
        /// Called when fetching a package by entityId and Name
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdNameCriteria criteria)
        {
            IDalFactory dalFactory = GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramImportTemplateDal dal = dalContext.GetDal<IPlanogramImportTemplateDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchByEntityIdName(criteria.EntityId, criteria.Name));
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                PlanogramImportTemplateDto dto = this.GetDataTransferObject();
                Object oldId = dto.Id;
                using (IPlanogramImportTemplateDal dal = dalContext.GetDal<IPlanogramImportTemplateDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Object>(IdProperty, dto.Id);
                this.LoadProperty(RowVersionProperty, dto.RowVersion);
                this.LoadProperty(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<PlanogramImportTemplate>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                PlanogramImportTemplateDto dto = this.GetDataTransferObject();
                if (this.IsSelfDirty)
                {
                    using (IPlanogramImportTemplateDal dal = dalContext.GetDal<IPlanogramImportTemplateDal>())
                    {
                        dal.Update(dto);
                    }
                }
                this.LoadProperty(RowVersionProperty, dto.RowVersion);
                this.LoadProperty(DateLastModifiedProperty, dto.DateLastModified);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IPlanogramImportTemplateDal dal = dalContext.GetDal<IPlanogramImportTemplateDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #endregion

        #region Commands

        #region LockPlanogramImportTemplateCommand
        /// <summary>
        /// Performs the locking of a highlight
        /// </summary>
        private partial class LockPlanogramImportTemplateCommand : CommandBase<LockPlanogramImportTemplateCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.DalFactoryType, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IPlanogramImportTemplateDal dal = dalContext.GetDal<IPlanogramImportTemplateDal>())
                    {
                        dal.LockById(this.Id);
                    }
                }
            }
            #endregion

            #endregion
        }
        #endregion

        #region UnlockPlanogramImportTemplateCommand
        /// <summary>
        /// Performs the unlocking of a PlanogramImportTemplate
        /// </summary>
        private partial class UnlockPlanogramImportTemplateCommand : CommandBase<UnlockPlanogramImportTemplateCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.DalFactoryType, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IPlanogramImportTemplateDal dal = dalContext.GetDal<IPlanogramImportTemplateDal>())
                    {
                        dal.UnlockById(this.Id);
                    }
                }
            }
            #endregion

            #endregion
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the correct dal factory based on the daltype and args.
        /// </summary>
        /// <param name="dalType"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private IDalFactory GetDalFactory(PlanogramImportTemplateDalType dalType)
        {
            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(dalType, out dalFactoryName);
            this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            this.LoadProperty<PlanogramImportTemplateDalType>(DalFactoryTypeProperty, dalType);

            return dalFactory;
        }

        /// <summary>
        /// Returns the correct dal factory for this instance
        /// </summary>
        protected override IDalFactory GetDalFactory()
        {
            if (String.IsNullOrEmpty(this.DalFactoryName))
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.DalFactoryType, out dalFactoryName);
                this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            }

            return DalContainer.GetDalFactory(this.DalFactoryName);
        }

        /// <summary>
        /// Returns the correct dal factory based on the given type and args.
        /// </summary>
        private static IDalFactory GetDalFactory(PlanogramImportTemplateDalType dalType, out String dalFactoryName)
        {
            switch (dalType)
            {
                case PlanogramImportTemplateDalType.FileSystem:
                    dalFactoryName = Constants.UserDal;
                    return DalContainer.GetDalFactory(dalFactoryName);

                default:
                    dalFactoryName = DalContainer.DalName;
                    return DalContainer.GetDalFactory();
            }

        }
        #endregion

    }
}
