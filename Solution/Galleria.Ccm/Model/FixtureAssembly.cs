﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
// V8-25526 : K.Pickup
//  Use RelationshipTypes.LazyLoad when registering lazy-loaded model properties.
// V8-26182 : L.Ineson
//  Added NewPlanogramAssembly
#endregion
#region Version History: (CCM CCM830)
// V8-32524 : L.Ineson
//  Added FixtureAnnotations and simplified updated back to plan
#endregion
#endregion

using System;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// FixtureAssembly Model object
    /// </summary>
    [Serializable]
    public sealed partial class FixtureAssembly : ModelObject<FixtureAssembly>
    {
        #region Static Constructor
        static FixtureAssembly()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new FixturePackage Parent
        {
            get
            {
                FixtureAssemblyList parentList = base.Parent as FixtureAssemblyList;
                if (parentList != null)
                {
                    return parentList.Parent;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region Height

        /// <summary>
        /// Height property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HeightProperty =
            RegisterModelProperty<Single>(c => c.Height);

        /// <summary>
        /// Gets/Sets the Height value
        /// </summary>
        public Single Height
        {
            get { return GetProperty<Single>(HeightProperty); }
            set { SetProperty<Single>(HeightProperty, value); }
        }

        #endregion

        #region Width

        /// <summary>
        /// Width property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> WidthProperty =
            RegisterModelProperty<Single>(c => c.Width);

        /// <summary>
        /// Gets/Sets the Width value
        /// </summary>
        public Single Width
        {
            get { return GetProperty<Single>(WidthProperty); }
            set { SetProperty<Single>(WidthProperty, value); }
        }

        #endregion

        #region Depth

        /// <summary>
        /// Depth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DepthProperty =
            RegisterModelProperty<Single>(c => c.Depth);

        /// <summary>
        /// Gets/Sets the Depth value
        /// </summary>
        public Single Depth
        {
            get { return GetProperty<Single>(DepthProperty); }
            set { SetProperty<Single>(DepthProperty, value); }
        }

        #endregion

        #region NumberOfComponents

        /// <summary>
        /// NumberOfComponents property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> NumberOfComponentsProperty =
            RegisterModelProperty<Int16>(c => c.NumberOfComponents);

        /// <summary>
        /// Gets/Sets the NumberOfComponents value
        /// </summary>
        public Int16 NumberOfComponents
        {
            get { return GetProperty<Int16>(NumberOfComponentsProperty); }
            set { SetProperty<Int16>(NumberOfComponentsProperty, value); }
        }

        #endregion

        #region TotalComponentCost

        /// <summary>
        /// TotalComponentCost property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> TotalComponentCostProperty =
            RegisterModelProperty<Single>(c => c.TotalComponentCost);

        /// <summary>
        /// Gets/Sets the TotalComponentCost value
        /// </summary>
        public Single TotalComponentCost
        {
            get { return GetProperty<Single>(TotalComponentCostProperty); }
            set { SetProperty<Single>(TotalComponentCostProperty, value); }
        }

        #endregion

        #region Components
        /// <summary>
        /// Components property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureAssemblyComponentList> ComponentsProperty =
            RegisterModelProperty<FixtureAssemblyComponentList>(c => c.Components, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Returns the Components within this package
        /// </summary>
        public FixtureAssemblyComponentList Components
        {
            get
            {
                return this.GetPropertyLazy<FixtureAssemblyComponentList>(
                    ComponentsProperty,
                    new FixtureAssemblyComponentList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// Asynchronously returns the Components within this package
        /// </summary>
        public FixtureAssemblyComponentList ComponentsAsync
        {
            get
            {
                return this.GetPropertyLazy<FixtureAssemblyComponentList>(
                    ComponentsProperty,
                    new FixtureAssemblyComponentList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));

        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static FixtureAssembly NewFixtureAssembly()
        {
            FixtureAssembly item = new FixtureAssembly();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        internal static FixtureAssembly NewFixtureAssembly(PlanogramAssembly planAssembly)
        {
            FixtureAssembly item = new FixtureAssembly();
            item.Create(planAssembly);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<FixtureAssemblyComponentList>(ComponentsProperty, FixtureAssemblyComponentList.NewFixtureAssemblyComponentList());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(PlanogramAssembly planAssembly)
        {
            this.LoadProperty<Int32>(IdProperty, (Int32)planAssembly.Id);
            this.LoadProperty<String>(NameProperty, planAssembly.Name);
            this.LoadProperty<Single>(HeightProperty, planAssembly.Height);
            this.LoadProperty<Single>(WidthProperty, planAssembly.Width);
            this.LoadProperty<Single>(DepthProperty, planAssembly.Depth);
            this.LoadProperty<Single>(TotalComponentCostProperty, planAssembly.TotalComponentCost);
            this.LoadProperty<Int16>(NumberOfComponentsProperty, (Int16)planAssembly.Components.Count);

            this.LoadProperty<FixtureAssemblyComponentList>(ComponentsProperty,
                FixtureAssemblyComponentList.NewFixtureAssemblyComponentList(planAssembly.Components));

            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<FixtureAssembly>(oldId, newId);
        }

        /// <summary>
        /// Updates this assembly back to the planogram.
        /// </summary>
        public PlanogramAssembly AddOrUpdatePlanogramAssembly(Planogram plan)
        {
            PlanogramAssembly planAssembly = plan.Assemblies.FindById(this.Id);
            if (planAssembly == null)
            {
                planAssembly = PlanogramAssembly.NewPlanogramAssembly();
                planAssembly.Id = this.Id;
                plan.Assemblies.Add(planAssembly);
            }

            planAssembly.Name = this.Name;
            planAssembly.Height = this.Height;
            planAssembly.Width = this.Width;
            planAssembly.Depth = this.Depth;
            planAssembly.TotalComponentCost = this.TotalComponentCost;


            //update components
            List<PlanogramAssemblyComponent> updatedComponents = new List<PlanogramAssemblyComponent>(this.Components.Count);
            foreach (FixtureAssemblyComponent acChild in this.Components)
            {
                updatedComponents.Add(acChild.AddOrUpdatePlanogramAssemblyComponent(planAssembly));
            }
            planAssembly.Components.RemoveList(planAssembly.Components.Except(updatedComponents).ToArray());


            return planAssembly;
        }

        #endregion

    }
}