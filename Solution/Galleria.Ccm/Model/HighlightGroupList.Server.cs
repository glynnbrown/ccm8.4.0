﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24801 L.Hodson
//  Created
#endregion
#endregion

using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;
using System;
using Csla;

namespace Galleria.Ccm.Model
{
    public sealed partial class HighlightGroupList
    {
        #region Constructor
        private HighlightGroupList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        internal static HighlightGroupList FetchByParentId(IDalContext dalContext, Object parentId)
        {
            return DataPortal.FetchChild<HighlightGroupList>(dalContext, new FetchByParentIdCriteria(null, parentId));
        }

        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FetchByParentIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            using (IHighlightGroupDal dal = dalContext.GetDal<IHighlightGroupDal>())
            {
                IEnumerable<HighlightGroupDto> dtoList = dal.FetchByHighlightId(criteria.ParentId);
                foreach (HighlightGroupDto dto in dtoList)
                {
                    this.Add(HighlightGroup.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}
