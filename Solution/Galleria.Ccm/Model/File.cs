﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
// GFS-27056 : J.Pickup
//  Amended GetFileType for Unit Testing. 
#endregion

#endregion

using System;
using System.IO;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a File
    /// 
    /// This is a root MasterData object and so it has the following properties/actions:
    /// - This object has a DateDeleted property and associated field in its supporting database table
    /// - If this object is deleted, it is marked as deleted (this allows for 'undeletes') and any child objects are left unchanged
    /// </summary>
    [Serializable]
    public partial class File : ModelObject<File>
    {
        #region Locks
        private Object _blobLock = new object();
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The unique object id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The row version timestamp
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }

        /// <summary>
        /// The entity Id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
        }

        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The file name (w/o path)
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> SourceFilePathProperty =
            RegisterModelProperty<String>(c => c.SourceFilePath);
        /// <summary>
        /// The file source path where the file was loaded from
        /// </summary>
        public String SourceFilePath
        {
            get { return GetProperty<String>(SourceFilePathProperty); }
            set { SetProperty<String>(SourceFilePathProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int64> SizeInBytesProperty =
            RegisterModelProperty<Int64>(c => c.SizeInBytes);
        /// <summary>
        /// The file SizeInBytes
        /// </summary>
        public Int64 SizeInBytes
        {
            get { return GetProperty<Int64>(SizeInBytesProperty); }
            set { SetProperty<Int64>(SizeInBytesProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32> BlobIdProperty =
            RegisterModelProperty<Int32>(c => c.BlobID);
        public Int32 BlobID
        {
            get { return GetProperty<Int32>(BlobIdProperty); }
        }

        public static readonly ModelPropertyInfo<Blob> FileBlobProperty =
                RegisterModelProperty<Blob>(c => c.FileBlob);
        public Blob FileBlob
        {
            get
            {
#if !SILVERLIGHT
                if (GetProperty<Blob>(FileBlobProperty) == null)
                {
                    lock (_blobLock)
                    {
                        if (GetProperty<Blob>(FileBlobProperty) == null)
                        {
                            LoadProperty<Blob>(FileBlobProperty,
                                Blob.GetById(ReadProperty(BlobIdProperty)));
                            OnPropertyChanged(FileBlobProperty);
                        }

                    }

                }
                return GetProperty<Blob>(FileBlobProperty);
#else
                if (ReadProperty<Blob>(FileBlobProperty) == null)
                {
                    this.MarkBusy();
                    DataPortal.BeginFetch<Blob>((o, e) =>
                    {
                        if(e.Error != null)
                        {
                            throw e.Error;
                        }
                        LoadProperty<Blob>(FileBlobProperty, e.Object);
                        OnPropertyChanged(FileBlobProperty);
                        MarkIdle();
                    });
                }
                return GetProperty<Blob>(FileBlobProperty);
#endif
            }
        }

        public static readonly ModelPropertyInfo<Byte[]> DataProperty =
            RegisterModelProperty<Byte[]>(c => c.Data);
        /// <summary>
        /// The file Data
        /// </summary>
        public Byte[] Data
        {
            get
            {
                return FileBlob.Data;
            }
        }

        public static readonly ModelPropertyInfo<Int32?> UserIdProperty =
            RegisterModelProperty<Int32?>(c => c.UserId);
        /// <summary>
        /// The user who created the file
        /// </summary>
        public Int32? UserId
        {
            get { return GetProperty<Int32?>(UserIdProperty); }
            set { SetProperty<Int32?>(UserIdProperty, value); }
        }

        /// <summary>
        /// The date when the item was created 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
        }

        /// <summary>
        /// The date when the item was modified 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
        }

        /// <summary>
        /// The date when the item was deleted 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
        }

        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(File), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(File), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(File), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(File), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Authenticated.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static File NewFile(Int32 entityId)
        {
            File item = new File();
            item.Create(entityId);
            return item;
        }

        /// <summary>
        /// Creates a new object by passing details of file in
        /// </summary>
        /// <returns>A new object</returns>
        public static File NewFile(Int32 entityId, String fileNameAndPath, Int32? userId)
        {
            File item = new File();
            item.Create(entityId, fileNameAndPath, userId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 entityId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            base.Create();
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 entityId, String fileNameAndPath, Int32? userId)
        {
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(fileNameAndPath);
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.LoadProperty<String>(NameProperty, Path.GetFileName(fileNameAndPath));
            this.LoadProperty<String>(SourceFilePathProperty, Path.GetDirectoryName(fileNameAndPath));
            this.LoadProperty<Int64>(SizeInBytesProperty, (Int64)fileInfo.Length);
            this.LoadProperty<Blob>(FileBlobProperty, Blob.NewBlob(fileNameAndPath));
            this.LoadProperty<Int32?>(UserIdProperty, userId);
            this.LoadProperty<DateTime>(DateCreatedProperty, fileInfo.LastWriteTimeUtc);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, fileInfo.LastWriteTimeUtc);
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Returns a friendly message indicating the file's document type
        /// </summary>
        /// <param name="fileNameAndPath"></param>
        /// <returns></returns>
        public static String GetFileType(String fileNameAndPath)
        {
            if (fileNameAndPath == null)
            {
                fileNameAndPath = "Not Avalailble";
            }

            String extn = Path.GetExtension(fileNameAndPath).ToUpper();
            String fileType = "N/A";
            switch (extn)
            {
                case ".PDF":
                    fileType = Message.File_FileType_Pdf;
                    break;

                case ".DOC":
                case ".DOCX":
                    fileType = Message.File_FileType_Doc_Docx;
                    break;

                case ".XLS":
                case ".XLSX":
                    fileType = Message.File_FileType_Xls_Xlsx;
                    break;

                case ".PPT":
                case ".PPTX":
                    fileType = Message.File_FileType_Ppt_Pptx;
                    break;

                case ".JPG":
                case ".JPEG":
                    fileType = Message.File_FileType_Img_Jpg;
                    break;

                case ".BMP":
                    fileType = Message.File_FileType_Img_Bmp;
                    break;

                case ".PNG":
                    fileType = Message.File_FileType_Img_Png;
                    break;

                case ".GIF":
                    fileType = Message.File_FileType_Img_Gif;
                    break;
            }

            return fileType;
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Returns a string representation of this object
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }
        #endregion

    }
}