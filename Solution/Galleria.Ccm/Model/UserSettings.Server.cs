﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25559 : L.Ineson
//		Copied from SA
#endregion

#region Version History: (CCM 8.0.3)
// V8-29174 : M.Shelley
//  Add setting to save user's last Plan Assignment "View By" selection
#endregion

#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class UserSystemSetting
    {
        #region Constructor
        private UserSystemSetting() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the UserSystemSettings items for the current user
        /// </summary>
        /// <returns>UserSystemSettings object</returns>
        public static UserSystemSetting FetchUserSettings(Boolean unitTesting = false)
        {
            return DataPortal.Fetch<UserSystemSetting>(unitTesting);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Returns a dto with the provided details
        /// </summary>
        /// <returns>A new dto</returns>
        private UserSystemSettingDto GetDataTransferObject()
        {
            return new UserSystemSettingDto()
            {
               DisplayLanguage = ReadProperty<String>(DisplayLanguageProperty),
               DisplayCEIPWindow = ReadProperty<Boolean>(DisplayCEIPWindowProperty),
               IsDatabaseSelectionRemembered = ReadProperty<Boolean>(IsDatabaseSelectionRememberedProperty),
               SendCEIPInformation = ReadProperty<Boolean>(SendCEIPInformationProperty),
               IsEntitySelectionRemembered = ReadProperty<Boolean>(IsEntitySelectionRememberedProperty),
               PlanAssignmentViewBy = ReadProperty<PlanAssignmentViewType>(PlanAssignmentViewByProperty),
            };
        }

        /// <summary>
        /// Loads the values from the given dto.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto"></param>
        private void LoadDataTransferObject(IDalContext dalContext, UserSystemSettingDto dto)
        {
            this.LoadProperty<String>(DisplayLanguageProperty, dto.DisplayLanguage);
            this.LoadProperty<Boolean>(DisplayCEIPWindowProperty, dto.DisplayCEIPWindow);
            this.LoadProperty<Boolean>(SendCEIPInformationProperty, dto.SendCEIPInformation);
            this.LoadProperty<Boolean>(IsDatabaseSelectionRememberedProperty, dto.IsDatabaseSelectionRemembered);
            this.LoadProperty<Boolean>(IsEntitySelectionRememberedProperty, dto.IsEntitySelectionRemembered);
            this.LoadProperty<PlanAssignmentViewType>(PlanAssignmentViewByProperty, dto.PlanAssignmentViewBy);

            LoadProperty<ConnectionList>(ConnectionsProperty, ConnectionList.Fetch(dalContext));
            LoadProperty(ColumnLayoutsProperty, ColumnLayoutSettingList.Fetch(dalContext));
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning an existing object
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(Boolean unitTesting = false)
        {
            // set default values incase we are missing information
            // within the solution database
            LoadDefaults();

            if (!unitTesting)
            {
                IDalFactory dalFactory = GetDalFactory(Constants.UserDal);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IUserSystemSettingDal dal = dalContext.GetDal<IUserSystemSettingDal>())
                    {
                        LoadDataTransferObject(dalContext, dal.Fetch());
                    }
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = GetDalFactory(Constants.UserDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                UserSystemSettingDto dto = GetDataTransferObject();
                using (IUserSystemSettingDal dal = dalContext.GetDal<IUserSystemSettingDal>())
                {
                    dal.Insert(dto);
                }
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update

        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = GetDalFactory(Constants.UserDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                if (this.IsSelfDirty)
                {
                    using (IUserSystemSettingDal dal = dalContext.GetDal<IUserSystemSettingDal>())
                    {
                        dal.Update(this.GetDataTransferObject());
                    }
                }
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the correct dal factory for this instance
        /// </summary>
        protected override IDalFactory GetDalFactory()
        {
            return DalContainer.GetDalFactory(Constants.UserDal);
        }

        #endregion

    }
}
