﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created (Auto-generated)
// V8-27241 : A.Kuszyk
//  Changed FetchByName to FetchByEntityIdName.
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class Blocking
    {
        #region Constructors
        private Blocking() { } //force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing Blocking with the given id
        /// </summary>
        public static Blocking FetchById(Int32 id)
        {
            return DataPortal.Fetch<Blocking>(new FetchByIdCriteria(id));
        }

        /// <summary>
        /// Returns an existing Blocking with the given name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Blocking FetchByEntityIdName(Int32 entityId, String name)
        {
            return DataPortal.Fetch<Blocking>(new FetchByEntityIdNameCriteria(entityId, name));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, BlockingDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<Int32>(ProductGroupIdProperty, dto.ProductGroupId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(DescriptionProperty, dto.Description);
            this.LoadProperty<Single>(HeightProperty, dto.Height);
            this.LoadProperty<Single>(WidthProperty, dto.Width);
            this.LoadProperty<Int32?>(PlanogramIdProperty, dto.PlanogramId);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);

            this.LoadProperty<BlockingDividerList>(DividersProperty, BlockingDividerList.FetchByBlockingId(dalContext, dto.Id));
            this.LoadProperty<BlockingGroupList>(GroupsProperty, BlockingGroupList.FetchByBlockingId(dalContext, dto.Id));
            this.LoadProperty<BlockingLocationList>(LocationsProperty, BlockingLocationList.FetchByBlockingId(dalContext, dto.Id));

        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private BlockingDto GetDataTransferObject()
        {
            return new BlockingDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                ProductGroupId = ReadProperty<Int32>(ProductGroupIdProperty),
                Name = ReadProperty<String>(NameProperty),
                Description = ReadProperty<String>(DescriptionProperty),
                Height = ReadProperty<Single>(HeightProperty),
                Width = ReadProperty<Single>(WidthProperty),
                PlanogramId = ReadProperty<Int32?>(PlanogramIdProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty),

            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning FetchById
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IBlockingDal dal = dalContext.GetDal<IBlockingDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.Id));
                }
            }
        }

        /// <summary>
        /// Called when returning FetchById
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdNameCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IBlockingDal dal = dalContext.GetDal<IBlockingDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchByEntityIdName(criteria.EntityId, criteria.Name));
                }
            }
        }


        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                BlockingDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (IBlockingDal dal = dalContext.GetDal<IBlockingDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<Blocking>(oldId, dto.Id);
                DataPortal.UpdateChild(Groups, dalContext, this);
                DataPortal.UpdateChild(Dividers, dalContext, this);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this instance
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                BlockingDto dto = GetDataTransferObject();
                using (IBlockingDal dal = dalContext.GetDal<IBlockingDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                DataPortal.UpdateChild(Groups, dalContext, this);
                DataPortal.UpdateChild(Dividers, dalContext, this);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when this instance is being deleted
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IBlockingDal dal = dalContext.GetDal<IBlockingDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #endregion

    }
}