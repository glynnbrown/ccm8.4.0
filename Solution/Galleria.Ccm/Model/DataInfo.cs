﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014.

#region Version History: (CCM 8.0)
// V8-28234 : A.Probyn
//      Adapted from SA.
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Model;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class DataInfo : ModelReadOnlyObject<DataInfo>
    {
        #region Authorisation Rules
        /// <summary>
        /// Defines the authorisation rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(DataInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(DataInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(DataInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(DataInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Properties

        /// <summary>
        /// Returns number of categories which represents whether a merchandising hiearchy
        /// has been setup.
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasProductHierarchyProperty =
            RegisterModelProperty<Boolean>(c => c.HasProductHierarchy);
        public Boolean HasProductHierarchy
        {
            get { return GetProperty<Boolean>(HasProductHierarchyProperty); }
        }

        /// <summary>
        /// Returns number of store space records in the databasde
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasLocationHierarchyProperty =
            RegisterModelProperty<Boolean>(c => c.HasLocationHierarchy);
        public Boolean HasLocationHierarchy
        {
            get { return GetProperty<Boolean>(HasLocationHierarchyProperty); }
        }

        /// <summary>
        /// Returns number of stores in the database
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasLocationProperty =
            RegisterModelProperty<Boolean>(c => c.HasLocation);
        public Boolean HasLocation
        {
            get { return GetProperty<Boolean>(HasLocationProperty); }
        }

        /// <summary>
        /// Returns number of products in the databasde
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasProductProperty =
            RegisterModelProperty<Boolean>(c => c.HasProduct);
        public Boolean HasProduct
        {
            get { return GetProperty<Boolean>(HasProductProperty); }
        }

        /// <summary>
        /// Returns whether assortments exist in the database
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasAssortmentProperty =
            RegisterModelProperty<Boolean>(c => c.HasAssortment);
        public Boolean HasAssortment
        {
            get { return GetProperty<Boolean>(HasAssortmentProperty); }
        }

        /// <summary>
        /// Returns number of clusters in the database
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasClusterSchemeProperty =
            RegisterModelProperty<Boolean>(c => c.HasClusterScheme);
        public Boolean HasClusterScheme
        {
            get { return GetProperty<Boolean>(HasClusterSchemeProperty); }
        }

        /// <summary>
        /// Returns number of location product attributes in the database
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasLocationProductAttributeProperty =
            RegisterModelProperty<Boolean>(c => c.HasLocationProductAttribute);
        public Boolean HasLocationProductAttribute
        {
            get { return GetProperty<Boolean>(HasLocationProductAttributeProperty); }
        }

        /// <summary>
        /// Returns number of location product illegal in the database
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasLocationProductIllegalProperty =
            RegisterModelProperty<Boolean>(c => c.HasLocationProductIllegal);
        public Boolean HasLocationProductIllegal
        {
            get { return GetProperty<Boolean>(HasLocationProductIllegalProperty); }
        }

        /// <summary>
        /// Returns number of location product Legal in the database
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasLocationProductLegalProperty =
            RegisterModelProperty<Boolean>(c => c.HasLocationProductLegal);
        public Boolean HasLocationProductLegal
        {
            get { return GetProperty<Boolean>(HasLocationProductLegalProperty); }
        }

        /// <summary>
        /// Returns number of location space in the database
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasLocationSpaceProperty =
            RegisterModelProperty<Boolean>(c => c.HasLocationSpace);
        public Boolean HasLocationSpace
        {
            get { return GetProperty<Boolean>(HasLocationSpaceProperty); }
        }

        /// <summary>
        /// Returns number of location space bays in the database
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasLocationSpaceBayProperty =
            RegisterModelProperty<Boolean>(c => c.HasLocationSpaceBay);
        public Boolean HasLocationSpaceBay
        {
            get { return GetProperty<Boolean>(HasLocationSpaceBayProperty); }
        }

        /// <summary>
        /// Returns number of location space Elements in the database
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasLocationSpaceElementProperty =
            RegisterModelProperty<Boolean>(c => c.HasLocationSpaceElement);
        public Boolean HasLocationSpaceElement
        {
            get { return GetProperty<Boolean>(HasLocationSpaceElementProperty); }
        }
        
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);
        /// <summary>
        /// Returns the utc date and time at which this object fetched.
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns true if all required data is available
        /// </summary>
        /// <returns></returns>
        public Boolean IsMandatoryDataAvailable()
        {
            return
                (this.HasProductHierarchy) &&
                (this.HasLocationHierarchy) &&
                (this.HasProduct) &&
                (this.HasLocation);
        }

        #endregion

        #region Criteria


        /// <summary>
        /// Criteria for FetchByEntityId
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : CriteriaBase<FetchByEntityIdCriteria>
        {
            #region Properties

            public static PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            #endregion

            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
            }
        }

        #endregion
    }
}
