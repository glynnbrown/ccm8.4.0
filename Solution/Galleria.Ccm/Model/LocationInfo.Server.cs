﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25628: A.Kuszyk
//		Created (Copied from SA)
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class LocationInfo
    {
        #region Constructors
        private LocationInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static LocationInfo FetchLocationInfo(IDalContext dalContext, LocationInfoDto dto)
        {
            return DataPortal.FetchChild<LocationInfo>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, LocationInfoDto dto)
        {
            LoadProperty<Int16>(IdProperty, dto.Id);
            LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            LoadProperty<Int32>(LocationGroupIdProperty, dto.LocationGroupId);
            LoadProperty<String>(CodeProperty, dto.Code);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<String>(RegionProperty, dto.Region);
            LoadProperty<String>(CountyProperty, dto.County);
            LoadProperty<String>(TVRegionProperty, dto.TVRegion);
            LoadProperty<String>(Address1Property, dto.Address1);
            LoadProperty<String>(Address2Property, dto.Address2);
            LoadProperty<String>(PostalCodeProperty, dto.PostalCode);
            LoadProperty<String>(CityProperty, dto.City);
            LoadProperty<String>(CountryProperty, dto.Country);
            LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when loading this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        private void Child_Fetch(IDalContext dalContext, LocationInfoDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #endregion
    }
}
