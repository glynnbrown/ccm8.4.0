﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-V8-24261 : L.Hodson
//	Created (Auto-generated)
#endregion

#region Version History: CCM 802
// V8-27433 : A.Kuszyk
//  Added Path property.
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class RecentPlanogram
    {
        #region Constructor
        private RecentPlanogram() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static RecentPlanogram Fetch(IDalContext dalContext, RecentPlanogramDto dto)
        {
            return DataPortal.FetchChild<RecentPlanogram>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, RecentPlanogramDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<String>(PathProperty, dto.Path);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<RecentPlanogramType>(TypeProperty, (RecentPlanogramType)dto.Type);
            this.LoadProperty<DateTime>(DateLastAccessedProperty, dto.DateLastAccessed);
            this.LoadProperty<Boolean>(IsPinnedProperty, dto.IsPinned);
            this.LoadProperty<String>(LocationProperty, dto.Location);

        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private RecentPlanogramDto GetDataTransferObject()
        {
            return new RecentPlanogramDto()
            {
                Id = ReadProperty<Object>(IdProperty),
                Path = ReadProperty<String>(PathProperty),
                Name = ReadProperty<String>(NameProperty),
                Type = (Byte)ReadProperty<RecentPlanogramType>(TypeProperty),
                DateLastAccessed = ReadProperty<DateTime>(DateLastAccessedProperty),
                IsPinned = ReadProperty<Boolean>(IsPinnedProperty),
                Location = ReadProperty<String>(LocationProperty),

            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, RecentPlanogramDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext)
        {
            RecentPlanogramDto dto = this.GetDataTransferObject();
            Object oldId = dto.Id;
            using (IRecentPlanogramDal dal = dalContext.GetDal<IRecentPlanogramDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Object>(IdProperty, dto.Id);
            dalContext.RegisterId<RecentPlanogram>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            if (this.IsSelfDirty)
            {
                using (IRecentPlanogramDal dal = dalContext.GetDal<IRecentPlanogramDal>())
                {
                    dal.Update(this.GetDataTransferObject());
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IRecentPlanogramDal dal = dalContext.GetDal<IRecentPlanogramDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion

    }
}