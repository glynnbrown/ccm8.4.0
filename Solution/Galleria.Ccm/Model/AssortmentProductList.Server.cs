﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentProductList
    {
        #region Constructor
        private AssortmentProductList() { } // Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a list of existing items based on their parent assortment
        /// </summary>
        /// <returns>A list of existing it</returns>
        internal static AssortmentProductList GetAssortmentProductList(IDalContext dalContext, Int32 assortmentId)
        {
            return DataPortal.FetchChild<AssortmentProductList>(dalContext, assortmentId);
        }

        /// <summary>
        /// Returns all items for the given entity id
        /// </summary>
        /// <returns>A list of entity infos</returns>
        public static AssortmentProductList FetchByAssortmentId(Int32 assortmentId)
        {
            return DataPortal.Fetch<AssortmentProductList>(assortmentId);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="assortmentId">The parent assortment id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 assortmentId)
        {
            RaiseListChangedEvents = false;
            using (IAssortmentProductDal dal = dalContext.GetDal<IAssortmentProductDal>())
            {
                IEnumerable<AssortmentProductDto> dtoList = dal.FetchByAssortmentId(assortmentId);
                foreach (AssortmentProductDto dto in dtoList)
                {
                    this.Add(AssortmentProduct.GetAssortmentProduct(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when retrieving a list of all entity infos
        /// </summary>
        private void DataPortal_Fetch(Int32 assortmentId)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IAssortmentProductDal dal = dalContext.GetDal<IAssortmentProductDal>())
                {
                    IEnumerable<AssortmentProductDto> dtoList = dal.FetchByAssortmentId(assortmentId);
                    foreach (AssortmentProductDto dto in dtoList)
                    {
                        this.Add(AssortmentProduct.GetAssortmentProduct(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}