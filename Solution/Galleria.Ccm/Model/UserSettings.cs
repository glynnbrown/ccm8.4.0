﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25559 : L.Ineson
//	Copied from SA
// V8-24700 : A.Silva ~ Added ColumnLayoutPlanogram
#endregion

#region Version History: (CCM 8.0.3)
// V8-29174 : M.Shelley
//  Add setting to save user's last Plan Assignment "View By" selection
#endregion

#endregion

using System;
using System.Linq;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Holds the user system settings for the current user
    /// </summary>
    [Serializable]
    public sealed partial class UserSystemSetting : ModelObject<UserSystemSetting>
    {
        #region Properties

        #region DisplayLanguage
        /// <summary>
        /// DisplayLanguage property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DisplayLanguageProperty =
            RegisterModelProperty<String>(c => c.DisplayLanguage);
        /// <summary>
        /// Returns the display language
        /// </summary>
        public String DisplayLanguage
        {
            get { return GetProperty<String>(DisplayLanguageProperty); }
            set { SetProperty<String>(DisplayLanguageProperty, value); }
        }
        #endregion

        #region DisplayCEIPWindow
        /// <summary>
        /// DisplayCEIPWindow property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> DisplayCEIPWindowProperty =
            RegisterModelProperty<Boolean>(c => c.DisplayCEIPWindow);
        /// <summary>
        /// Returns whether to display the CEIP window
        /// </summary>
        public Boolean DisplayCEIPWindow
        {
            get { return GetProperty<Boolean>(DisplayCEIPWindowProperty); }
            set { SetProperty<Boolean>(DisplayCEIPWindowProperty, value); }
        }
        #endregion

        #region SendCEIPInformation
        /// <summary>
        /// SendCEIPInformation property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> SendCEIPInformationProperty =
             RegisterModelProperty<Boolean>(c => c.SendCEIPInformation);
        /// <summary>
        /// Returns whether to send the CEIP information
        /// </summary>
        public Boolean SendCEIPInformation
        {
            get { return GetProperty<Boolean>(SendCEIPInformationProperty); }
            set { SetProperty<Boolean>(SendCEIPInformationProperty, value); }
        }
        #endregion

        #region IsDatabaseSelectionRemembered
        /// <summary>
        /// IsDatabaseSelectionRemembered property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsDatabaseSelectionRememberedProperty =
            RegisterModelProperty<Boolean>(c => c.IsDatabaseSelectionRemembered);
        /// <summary>
        /// Returns whether database selection is remembered
        /// </summary>
        public Boolean IsDatabaseSelectionRemembered
        {
            get { return GetProperty<Boolean>(IsDatabaseSelectionRememberedProperty); }
            set { SetProperty<Boolean>(IsDatabaseSelectionRememberedProperty, value); }
        }
        #endregion

        #region IsEntitySelectionRemembered
        /// <summary>
        /// IsEntitySelectionRemembered property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsEntitySelectionRememberedProperty =
                RegisterModelProperty<Boolean>(c => c.IsEntitySelectionRemembered);
        /// <summary>
        /// Returns whether entity selection is remembered
        /// </summary>
        public Boolean IsEntitySelectionRemembered
        {
            get { return GetProperty<Boolean>(IsEntitySelectionRememberedProperty); }
            set { SetProperty<Boolean>(IsEntitySelectionRememberedProperty, value); }
        }
        #endregion

        #region Connections
        /// <summary>
        /// Connections property definition
        /// </summary>
        public static readonly ModelPropertyInfo<ConnectionList> ConnectionsProperty =
                RegisterModelProperty<ConnectionList>(c => c.Connections);
        /// <summary>
        /// Returns the connection list
        /// </summary>
        public ConnectionList Connections
        {
            get { return GetProperty<ConnectionList>(ConnectionsProperty); }
        }
        #endregion

        #region ColumnLayouts

        /// <summary>
        ///     <see cref="ModelPropertyInfo{T}"/> for the <see cref="ColumnLayouts"/> property.
        /// </summary>
        public static readonly ModelPropertyInfo<ColumnLayoutSettingList> ColumnLayoutsProperty =
            RegisterModelProperty<ColumnLayoutSettingList>(o => o.ColumnLayouts);

        /// <summary>
        ///     Gets the list of columns layouts filenames for each <see cref="CustomColumnLayoutType"/>.
        /// </summary>
        public ColumnLayoutSettingList ColumnLayouts { get { return GetProperty(ColumnLayoutsProperty); } }

        #endregion

        // Plan Assignment "View By" setting

        #region PlanAssignmentViewBy

        /// <summary>
        /// PlanAssignmentViewBy property definition
        /// </summary>
        private static readonly ModelPropertyInfo<PlanAssignmentViewType> PlanAssignmentViewByProperty =
            RegisterModelProperty<PlanAssignmentViewType>(o => o.PlanAssignmentViewBy);

        /// <summary>
        ///Gets/Sets whether text on a planogram should be resized dynamically according to the camera zoom.
        /// </summary>
        public PlanAssignmentViewType PlanAssignmentViewBy
        {
            get { return this.GetProperty<PlanAssignmentViewType>(PlanAssignmentViewByProperty); }
            set { this.SetProperty<PlanAssignmentViewType>(PlanAssignmentViewByProperty, value); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        //this object is accessed without authentication and so cannot have rules
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.CheckRules();
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new UserSettings object
        /// </summary>
        /// <returns>A new UserSettings object</returns>
        public static UserSystemSetting NewUserSystemSetting()
        {
            UserSystemSetting item = new UserSystemSetting();
            item.Create();
            return item;
        }

        /// <summary>
        /// Resets all connections apart from the specified connection
        /// </summary>
        /// <param name="currentConnection">Specified connection</param>
        public void ResetCurrentConnection(Connection currentConnection)
        {
            foreach (Connection conn in this.Connections)
            {
                if (!conn.Compare(currentConnection))
                {
                    conn.IsCurrentConnection = false;
                }
            }
        }

        /// <summary>
        /// Resets the current selected entity
        /// </summary>
        public void ResetEntityDefault()
        {
            foreach (Connection conn in this.Connections)
            {
                conn.IsAutoConnect = false;
            }

            this.IsEntitySelectionRemembered = false;

            this.Save();
        }

        /// <summary>
        /// Return the current connection
        /// </summary>
        /// <returns></returns>
        public Connection GetCurrentConnection()
        {
            Connection currentConnection = this.Connections.Where(c => c.IsCurrentConnection).FirstOrDefault();
            //If local connection, then override servername and set to be local
            //if (currentConnection != null)
            //{
            //    if (currentConnection.Type == ConnectionType.Local)
            //    {
            //        currentConnection.ServerName = Message.Enum_ConnectionType_Local;
            //    }
            //}
            return currentConnection;
        }

        /// <summary>
        /// Resets the current selected database
        /// </summary>
        public void ResetDatabaseDefault()
        {
            foreach (Connection conn in this.Connections)
            {
                conn.IsAutoConnect = false;
            }

            this.IsDatabaseSelectionRemembered = false;
            //If resetting database selection entity should be reset too
            this.IsEntitySelectionRemembered = false;

            this.Save();
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create()
        {
            //load in the default values
            LoadDefaults();

            base.Create();
        }
        #endregion

        #region Load Defaults
        
        /// <summary>
        ///     Loads the defaults for this <see cref="UserSystemSetting"/> instance's properties.
        /// </summary>
        private void LoadDefaults()
        {
            LoadProperty(DisplayLanguageProperty, "en-gb");
            LoadProperty(SendCEIPInformationProperty, true);
            LoadProperty(IsDatabaseSelectionRememberedProperty, false);
            LoadProperty(IsEntitySelectionRememberedProperty, false);
            LoadProperty(ConnectionsProperty, ConnectionList.NewConnectionList());
            LoadProperty(DisplayCEIPWindowProperty, true);
            LoadProperty(ColumnLayoutsProperty,ColumnLayoutSettingList.NewColumnLayoutSettingList());
            LoadProperty(PlanAssignmentViewByProperty, PlanAssignmentViewType.ViewCategory);
        }

        #endregion

        #endregion
    }
}
