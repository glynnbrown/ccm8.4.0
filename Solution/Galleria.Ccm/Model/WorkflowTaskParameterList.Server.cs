﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion
#region Version History: CCM811
// V8-30417 : M.Brumby
//  Missing parameters will now be loaded in as a new parameter.
#endregion
#region Version History: CCM820
// V8-30596 : N.Foster
//  Resolved issue where duplicate tasks in same workflow resulted in exception
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class WorkflowTaskParameterList
    {
        #region Constructor
        private WorkflowTaskParameterList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all tasks within the specified workflow
        /// </summary>
        internal static WorkflowTaskParameterList FetchByWorkflowTaskId(IDalContext dalContext, Int32 workflowTaskId, EngineTaskInfo task)
        {
            return DataPortal.FetchChild<WorkflowTaskParameterList>(dalContext, workflowTaskId, task);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returing all items for a given parent id
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, Int32 workflowTaskId, EngineTaskInfo task)
        {
            this.RaiseListChangedEvents = false;
            using (IWorkflowTaskParameterDal dal = dalContext.GetDal<IWorkflowTaskParameterDal>())
            {
                IEnumerable<WorkflowTaskParameterDto> dtoList = dal.FetchByWorkflowTaskId(workflowTaskId);
                foreach (EngineTaskParameterInfo parameter in task.Parameters)
                {
                    WorkflowTaskParameterDto dto = dtoList.FirstOrDefault(item => parameter.Id == item.ParameterId);
                    if (dto != null)
                    {
                        this.Add(WorkflowTaskParameter.GetWorkflowTaskParameter(dalContext, dto, task, parameter));
                    }
                    else
                    {
                        this.Add(WorkflowTaskParameter.NewWorkflowTaskParameter(task, parameter));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
