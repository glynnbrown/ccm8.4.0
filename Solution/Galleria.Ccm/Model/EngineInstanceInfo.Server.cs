﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-27411 : M.Pettit
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Engine;
using Galleria.Framework.Engine.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class EngineInstanceInfo
    {
        #region Constructors
        private EngineInstanceInfo() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an instance from a dto
        /// </summary>
        internal static EngineInstanceInfo GetEngineInstanceInfo(IDalContext dalContext, EngineInstanceDto dto)
        {
            return DataPortal.FetchChild<EngineInstanceInfo>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, EngineInstanceDto dto)
        {
            this.LoadProperty<Guid>(IdProperty, dto.Id);
            this.LoadProperty<String>(ComputerNameProperty, dto.ComputerName);
            this.LoadProperty<Int32>(LifespanProperty, dto.Lifespan);
            this.LoadProperty<Int32>(WorkerCountProperty, dto.WorkerCount);
            this.LoadProperty<DateTime>(DateLastActiveProperty, dto.DateLastActive);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, EngineInstanceDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #endregion
    }
}
