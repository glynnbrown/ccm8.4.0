﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26891 : L.Ineson
//	Created (Auto-generated)
// V8-27241 : A.Kuszyk
//  Changed FetchByName to FetchByEntityIdName.
// V8-27957 : N.Foster
//  Implement CCM security
// V8-28277 : L.Ineson
//  Added min rule for ProductGroupId as it is mandatory.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Blocking model object
    /// </summary>
    [Serializable]
    [DefaultNewMethod("NewBlocking", "EntityId")]
    public sealed partial class Blocking : ModelObject<Blocking>, IPlanogramBlocking
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The row version timestamp
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region EntityId

        /// <summary>
        /// EntityId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);

        /// <summary>
        /// Gets/Sets the EntityId value
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
            set { SetProperty<Int32>(EntityIdProperty, value); }
        }

        #endregion

        #region ProductGroupId

        /// <summary>
        /// ProductGroupId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProductGroupIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductGroupId);

        /// <summary>
        /// Gets/Sets the ProductGroupId value
        /// </summary>
        public Int32 ProductGroupId
        {
            get { return GetProperty<Int32>(ProductGroupIdProperty); }
            set { SetProperty<Int32>(ProductGroupIdProperty, value); }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region Description

        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);

        /// <summary>
        /// Gets/Sets the Description value
        /// </summary>
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
            set { SetProperty<String>(DescriptionProperty, value); }
        }

        #endregion

        #region Height

        /// <summary>
        /// Height property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HeightProperty =
            RegisterModelProperty<Single>(c => c.Height);

        /// <summary>
        /// Gets/Sets the Height value
        /// </summary>
        public Single Height
        {
            get { return GetProperty<Single>(HeightProperty); }
            set { SetProperty<Single>(HeightProperty, value); }
        }

        #endregion

        #region Width

        /// <summary>
        /// Width property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> WidthProperty =
            RegisterModelProperty<Single>(c => c.Width);

        /// <summary>
        /// Gets/Sets the Width value
        /// </summary>
        public Single Width
        {
            get { return GetProperty<Single>(WidthProperty); }
            set { SetProperty<Single>(WidthProperty, value); }
        }

        #endregion

        #region PlanogramId

        /// <summary>
        /// PlanogramId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> PlanogramIdProperty =
            RegisterModelProperty<Int32?>(c => c.PlanogramId);

        /// <summary>
        /// Gets/Sets the PlanogramId value
        /// </summary>
        public Int32? PlanogramId
        {
            get { return GetProperty<Int32?>(PlanogramIdProperty); }
            set { SetProperty<Int32?>(PlanogramIdProperty, value); }
        }

        #endregion

        #region DateCreated

        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        /// Gets/Sets the DateCreated value
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
            set { SetProperty<DateTime>(DateCreatedProperty, value); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        /// Gets/Sets the DateLastModified value
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
            set { SetProperty<DateTime>(DateLastModifiedProperty, value); }
        }

        #endregion

        #region DateDeleted

        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);

        /// <summary>
        /// Gets/Sets the DateDeleted value
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
            set { SetProperty<DateTime?>(DateDeletedProperty, value); }
        }

        #endregion


        #region Dividers

        /// <summary>
        /// Dividers property definition
        /// </summary>
        public static readonly ModelPropertyInfo<BlockingDividerList> DividersProperty =
            RegisterModelProperty<BlockingDividerList>(c => c.Dividers);

        /// <summary>
        /// Returns the list of dividers
        /// </summary>
        public BlockingDividerList Dividers
        {
            get { return GetProperty<BlockingDividerList>(DividersProperty); }
        }

        /// <summary>
        /// Explicit IPlanogramBlocking implementation of Dividers
        /// </summary>
        IEnumerable<IPlanogramBlockingDivider> IPlanogramBlocking.Dividers
        {
            get { return this.Dividers; }
        }

        #endregion

        #region Groups

        /// <summary>
        /// Groups property definition
        /// </summary>
        public static readonly ModelPropertyInfo<BlockingGroupList> GroupsProperty =
            RegisterModelProperty<BlockingGroupList>(c => c.Groups);


        /// <summary>
        /// Returns the list of groups
        /// </summary>
        public BlockingGroupList Groups
        {
            get { return GetProperty<BlockingGroupList>(GroupsProperty); }
        }

        /// <summary>
        /// Explicit IPlanogramBlocking implementation of Groups
        /// </summary>
        IEnumerable<IPlanogramBlockingGroup> IPlanogramBlocking.Groups
        {
            get { return this.Groups; }
        }

        #endregion

        #region Locations

        /// <summary>
        /// Locations property definition
        /// </summary>
        public static readonly ModelPropertyInfo<BlockingLocationList> LocationsProperty =
            RegisterModelProperty<BlockingLocationList>(c => c.Locations);


        /// <summary>
        /// Returns the list of locations
        /// </summary>
        public BlockingLocationList Locations
        {
            get { return GetProperty<BlockingLocationList>(LocationsProperty); }
        }

        /// <summary>
        /// Explicit IPlanogramBlocking implementation of Groups
        /// </summary>
        IEnumerable<IPlanogramBlockingLocation> IPlanogramBlocking.Locations
        {
            get { return this.Locations; }
        }


        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new MinValue<Int32>(EntityIdProperty, 1));
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
            BusinessRules.AddRule(new MaxLength(DescriptionProperty, 255));
            BusinessRules.AddRule(new MinValue<Int32>(ProductGroupIdProperty, 1));
            BusinessRules.AddRule(new MinValue<Single>(HeightProperty, 0));
            BusinessRules.AddRule(new MinValue<Single>(WidthProperty, 0));
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(Blocking), new IsInRole(AuthorizationActions.GetObject, DomainPermission.BlockingGet.ToString()));
            BusinessRules.AddRule(typeof(Blocking), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.BlockingCreate.ToString()));
            BusinessRules.AddRule(typeof(Blocking), new IsInRole(AuthorizationActions.EditObject, DomainPermission.BlockingEdit.ToString()));
            BusinessRules.AddRule(typeof(Blocking), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.BlockingDelete.ToString()));
        }
        #endregion

        #region Criteria

        #region FetchByIdCriteria

        // <summary>
        /// Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> IdProperty =
                RegisterProperty<Int32>(c => c.Id);
            /// <summary>
            /// Returns the id
            /// </summary>
            public Int32 Id
            {
                get { return this.ReadProperty<Int32>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(Int32 id)
            {
                this.LoadProperty<Int32>(IdProperty, id);
            }
            #endregion
        }

        #endregion

        #region FetchByNameCriteria

        // <summary>
        /// Criteria for the FetchByName factory method
        /// </summary>
        [Serializable]
        public class FetchByEntityIdNameCriteria : CriteriaBase<FetchByEntityIdNameCriteria>
        {
            #region Properties

            #region Name
            /// <summary>
            /// Name property definition
            /// </summary>
            public static readonly PropertyInfo<String> NameProperty =
                RegisterProperty<String>(c => c.Name);
            /// <summary>
            /// Returns the id
            /// </summary>
            public String Name
            {
                get { return this.ReadProperty<String>(NameProperty); }
            }
            #endregion

            #region EntityId
            /// <summary>
            /// EntityId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            /// <summary>
            /// Returns the id
            /// </summary>
            public Int32 EntityId
            {
                get { return this.ReadProperty<Int32>(EntityIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByEntityIdNameCriteria(Int32 entityId, String name)
            {
                this.LoadProperty<String>(NameProperty, name);
                this.LoadProperty<Int32>(EntityIdProperty, entityId);
            }
            #endregion
        }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new Blocking
        /// </summary>
        public static Blocking NewBlocking(Int32 entityId)
        {
            Blocking item = new Blocking();
            item.Create(entityId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 entityId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            this.LoadProperty<BlockingDividerList>(DividersProperty, BlockingDividerList.NewBlockingDividerList());
            this.LoadProperty<BlockingGroupList>(GroupsProperty, BlockingGroupList.NewBlockingGroupList());
            this.LoadProperty<BlockingLocationList>(LocationsProperty, BlockingLocationList.NewBlockingLocationList());
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<Blocking>(oldId, newId);
        }

        /// <summary>
        /// Override of the object to string.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            if(!String.IsNullOrEmpty(this.Name))
                return this.Name;
            else return Message.Generic_NewItem;
        }

        /// <summary>
        /// Returns the next available group colour
        /// </summary>
        /// <returns></returns>
        public Int32 GetNextBlockingGroupColour()
        {
            return BlockingHelper.GetNextBlockingGroupColour(this);
        }

        ///// <summary>
        ///// Move a specified divider by the given delta ensuring that all related 
        ///// dividers and locations are correctly updated.
        ///// </summary>
        ///// <param name="divider"></param>
        ///// <param name="delta"></param>
        //public void MoveDividerTo(BlockingDivider divider, Single newValue)
        //{
        //    if (divider.Type == PlanogramBlockingDividerType.Vertical)
        //    {
        //        MoveVerticalDividerTo(divider, newValue);
        //    }
        //    else
        //    {
        //        if (newValue == divider.Y) return;
        //        MoveHorizontalDividerTo(divider, newValue);
        //    }
        //}

        //private void MoveVerticalDividerTo(BlockingDivider divider, Single newDivX)
        //{
        //    Single oldX = divider.X;
        //    if (newDivX == oldX) return;

        //    //get a list of adjacent dividers
        //    List<BlockingDivider> adjacentDividers = divider.GetAdjacentDividers();

        //    //get the min and max based on dividers either side
        //    BlockingDivider minParallel, maxParallel;
        //    divider.GetParallelDividers(out minParallel, out maxParallel);

        //    Single minX = (minParallel != null) ? minParallel.X + 0.01F : 0.01F;
        //    Single maxX = (maxParallel != null) ? maxParallel.X - 0.01F : 0.99F;

        //    //Clamp the delta and apply
        //    newDivX = Math.Max(minX, newDivX);
        //    newDivX = Math.Min(maxX, newDivX);
        //    divider.X = newDivX;

        //    //update all affected dividers
        //    foreach (BlockingDivider div in adjacentDividers)
        //    {
        //        if (div.X == oldX)
        //        {
        //            div.X = divider.X;
        //        }
        //        div.UpdateLength();
        //    }

        //}

        //private void MoveHorizontalDividerTo(BlockingDivider divider, Single newDivY)
        //{
        //    Single oldY = divider.Y;
        //    if (newDivY == oldY) return;

        //    //get a list of adjacent dividers
        //    List<BlockingDivider> adjacentDividers = divider.GetAdjacentDividers();

        //    //get the min and max based on dividers either side
        //    BlockingDivider minParallel, maxParallel;
        //    divider.GetParallelDividers(out minParallel, out maxParallel);

        //    Single minY = (minParallel != null) ? minParallel.Y + 0.01F : 0.01F;
        //    Single maxY = (maxParallel != null) ? maxParallel.Y - 0.01F : 0.99F;


        //    //Clamp the delta and apply
        //    newDivY = Math.Max(minY, newDivY);
        //    newDivY = Math.Min(maxY, newDivY);
        //    divider.Y = newDivY;

        //    //update all affected dividers
        //    foreach (BlockingDivider div in adjacentDividers)
        //    {
        //        if (div.Y == oldY)
        //        {
        //            div.Y = divider.Y;
        //        }
        //        div.UpdateLength();
        //    }
            
        //}


        #region IPlanogramBlocking Members

        IPlanogramBlockingDivider IPlanogramBlocking.AddNewDivider(Single x, Single y, PlanogramBlockingDividerType type)
        {
            return this.Dividers.Add(x, y, type);
        }

        void IPlanogramBlocking.RemoveDivider(IPlanogramBlockingDivider divider)
        {
            this.Dividers.Remove(divider as BlockingDivider);
        }

        #endregion

        #endregion
    }
}