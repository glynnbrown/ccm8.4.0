﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.

#endregion

#endregion

using System.Linq;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Server implementation of <see cref="CustomColumnList" />.
    /// </summary>
    public sealed partial class CustomColumnList
    {
        #region Constructor

        /// <summary>
        ///     Instantiate using one of the public factory methods.
        /// </summary>
        private CustomColumnList()
        {
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        ///     Fetches all custom columns fitting the provided <paramref name="criteria"/>.
        /// </summary>
        /// <param name="criteria">Criteria to fetch custom columns with.</param>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using (var dalContext = GetDalFactory(criteria.DalFactoryName).CreateContext())
            using (var dal = dalContext.GetDal<ICustomColumnDal>())
            {
                AddRange(dal.FetchByColumnLayoutId(criteria.ParentId).Select(dto => CustomColumn.FetchCustomColumn(dalContext,dto)).ToList());
            }

            MarkAsChild();

            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}
