﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25559 : L.Hodson
//		Copied from SA
#endregion
#endregion


using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System;
using Galleria.Ccm.Security;


namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Entity info list
    /// </summary>
    public partial class EntityInfoList
    {
        #region Constructors
        private EntityInfoList() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns all entity infos
        /// </summary>
        /// <returns>A list of entity infos</returns>
        public static EntityInfoList FetchAllEntityInfos()
        {
            return DataPortal.Fetch<EntityInfoList>(new FetchAllCriteria());
        }

        /// <summary>
        /// Returns all entity infos inside GFS allowing
        /// entity level security to be overridden
        /// </summary>
        /// <returns></returns>
        public static EntityInfoList FetchAllEntityInfos(Boolean overrideSecurity)
        {
            return DataPortal.Fetch<EntityInfoList>(new FetchAllCriteria(overrideSecurity));
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Called when retrieving a list of all entity infos
        /// </summary>
        private void DataPortal_Fetch(FetchAllCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            try
            {
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IEntityInfoDal dal = dalContext.GetDal<IEntityInfoDal>())
                    {
                        IEnumerable<EntityInfoDto> dtoList = dal.FetchAll();
                        foreach (EntityInfoDto dto in dtoList)
                        {
                            // if security is being overridden, or the user is
                            // an administrator, or the user has access to the entity
                            // then add the entity to the list of entity infos
                            if (criteria.OverrideSecurity ||
                                (ApplicationContext.User.IsInRole(DomainPermission.Administrator.ToString())) ||
                                (ApplicationContext.User.IsInRole(String.Format(Constants.EntityAccessPermission, dto.Id))))
                            {
                                this.Add(EntityInfo.FetchEntityInfo(dalContext, dto));
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                //  The connection was not set, failed or something else, just leave the list empty.
                Console.WriteLine(e);
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

      
        #endregion
    }
}
