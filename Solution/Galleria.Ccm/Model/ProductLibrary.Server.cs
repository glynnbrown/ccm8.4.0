﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25395 : A.Probyn
//		Created (Auto-generated)
#endregion

#region Version History: CCM 830
//V8-32361 : L.Ineson
//  Added entityid and name.
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal.Configuration;
using System.IO;

namespace Galleria.Ccm.Model
{
    public partial class ProductLibrary
    {
        #region Constants
        private const String _dalAssemblyName = "Galleria.Ccm.Dal.{0}.dll";
        private const String _fileSystemDal = "User";
        #endregion

        #region Constructors
        private ProductLibrary() { } //force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing ProductLibrary with the given id
        /// </summary>
        public static ProductLibrary FetchById(Object id)
        {
            return DataPortal.Fetch<ProductLibrary>(
                new FetchByIdCriteria(ProductLibraryDalFactoryType.Unknown, id, /*asReadOnly*/false));
        }

        /// <summary>
        /// Returns an existing ProductLibrary from the given filename.
        /// </summary>
        public static ProductLibrary FetchByFilename(String fileName)
        {
            return FetchByFilename(fileName, /*asReadOnly*/false);
        }

        /// <summary>
        /// Returns an existing PrintTemplate with the given file path
        /// </summary>
        public static ProductLibrary FetchByFilename(String fileName, Boolean asReadOnly)
        {
            return DataPortal.Fetch<ProductLibrary>(new FetchByIdCriteria(ProductLibraryDalFactoryType.FileSystem, fileName, asReadOnly));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, ProductLibraryDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<ImportDefinitionFileType>(FileTypeProperty, (ImportDefinitionFileType)dto.FileType);
            this.LoadProperty<String>(ExcelWorkbookSheetProperty, dto.ExcelWorkbookSheet);
            this.LoadProperty<Int32>(StartAtRowProperty, dto.StartAtRow);
            this.LoadProperty<Boolean>(IsFirstRowHeadersProperty, dto.IsFirstRowHeaders);
            this.LoadProperty<ImportDefinitionTextDataFormatType?>(TextDataFormatProperty, (ImportDefinitionTextDataFormatType?)dto.TextDataFormat);
            this.LoadProperty<ImportDefinitionTextDelimiterType?>(TextDelimiterTypeProperty, (ImportDefinitionTextDelimiterType?)dto.TextDelimiterType);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);

            this.LoadProperty<ProductLibraryColumnMappingList>(ColumnMappingsProperty, ProductLibraryColumnMappingList.FetchByParentId(dalContext, dto.Id));
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private ProductLibraryDto GetDataTransferObject()
        {
            return new ProductLibraryDto
            {
                Id = ReadProperty<Object>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                Name = ReadProperty<String>(NameProperty),
                FileType = (Byte)ReadProperty<ImportDefinitionFileType>(FileTypeProperty),
                ExcelWorkbookSheet = ReadProperty<String>(ExcelWorkbookSheetProperty),
                StartAtRow = ReadProperty<Int32>(StartAtRowProperty),
                IsFirstRowHeaders = ReadProperty<Boolean>(IsFirstRowHeadersProperty),
                TextDataFormat = (Byte?)ReadProperty<ImportDefinitionTextDataFormatType?>(TextDataFormatProperty),
                TextDelimiterType = (Byte?)ReadProperty<ImportDefinitionTextDelimiterType?>(TextDelimiterTypeProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning FetchById
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            //lock the file first if required.
            if (criteria.DalType == ProductLibraryDalFactoryType.FileSystem
                && !criteria.AsReadOnly)
            {
                LockProductLibraryByFileName((String)criteria.Id);
            }


            IDalFactory dalFactory = this.GetDalFactory(criteria.DalType);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductLibraryDal dal = dalContext.GetDal<IProductLibraryDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.Id));

                    //set flags
                    this.LoadProperty<Boolean>(IsReadOnlyProperty, criteria.AsReadOnly);

                    if (criteria.DalType == ProductLibraryDalFactoryType.FileSystem)
                    {
                        LoadProperty<Boolean>(IsFileProperty, true);
                    }
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                ProductLibraryDto dto = GetDataTransferObject();
                Object oldId = dto.Id;

                using (IProductLibraryDal dal = dalContext.GetDal<IProductLibraryDal>())
                {
                    dal.Insert(dto);
                }

                this.LoadProperty<Object>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<ProductLibrary>(oldId, dto.Id);

                FieldManager.UpdateChildren(dalContext, this);

                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this instance
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                ProductLibraryDto dto = GetDataTransferObject();
                if (this.IsSelfDirty)
                {
                    using (IProductLibraryDal dal = dalContext.GetDal<IProductLibraryDal>())
                    {
                        dal.Update(dto);
                    }

                    this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                    this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                }

                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when this instance is being deleted
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IProductLibraryDal dal = dalContext.GetDal<IProductLibraryDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #region Commands

        /// <summary>
        /// Performs the locking of a product library
        /// </summary>
        private partial class LockProductLibraryCommand : CommandBase<LockProductLibraryCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.DalType, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IProductLibraryDal dal = dalContext.GetDal<IProductLibraryDal>())
                    {
                        dal.LockById(this.Id);
                    }
                }
            }
            #endregion

            #endregion
        }


        /// <summary>
        /// Performs the unlocking of a product library
        /// </summary>
        private partial class UnlockByIdCommand : CommandBase<UnlockByIdCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.Type, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IProductLibraryDal dal = dalContext.GetDal<IProductLibraryDal>())
                    {
                        dal.UnlockById(this.Id);
                    }
                }
            }
            #endregion

            #endregion
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the correct dal factory based on the daltype and args.
        /// </summary>
        private IDalFactory GetDalFactory(ProductLibraryDalFactoryType dalType)
        {
            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(dalType, out dalFactoryName);
            this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            this.LoadProperty<ProductLibraryDalFactoryType>(DalFactoryTypeProperty, dalType);

            return dalFactory;
        }

        /// <summary>
        /// Returns the correct dal factory for this instance
        /// </summary>
        protected override IDalFactory GetDalFactory()
        {
            if (String.IsNullOrEmpty(this.DalFactoryName))
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.DalFactoryType, out dalFactoryName);
                this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            }

            return DalContainer.GetDalFactory(this.DalFactoryName);
        }

        /// <summary>
        /// Returns the correct dal factory based on the given type and args.
        /// </summary>
        private static IDalFactory GetDalFactory(ProductLibraryDalFactoryType dalType, out String dalFactoryName)
        {
            switch (dalType)
            {
                case ProductLibraryDalFactoryType.FileSystem:
                    dalFactoryName = Constants.UserDal;
                    return DalContainer.GetDalFactory(dalFactoryName);

                default:
                    dalFactoryName = DalContainer.DalName;
                    return DalContainer.GetDalFactory();
            }

        }
        #endregion

    }
}