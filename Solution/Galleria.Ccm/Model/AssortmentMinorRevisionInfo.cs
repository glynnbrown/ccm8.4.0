﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
// V8-26140 :I.George
//  //Added RowVersion Property
#endregion
#region Version History: (CCM 8.0.3)
// V8-29214 : D.Pleasance
//  Removed DateDeleted as items are now deleted outright
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Helpers;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Csla;
using Galleria.Framework.Model;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class AssortmentMinorRevisionInfo : ModelReadOnlyObject<AssortmentMinorRevisionInfo>
    {
        #region Static Constructor
        static AssortmentMinorRevisionInfo()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(AssortmentMinorRevisionInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
           RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The Consumer Decision Tree Id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<Int32?> ConsumerDecisionTreeIdProperty =
            RegisterModelProperty<Int32?>(c => c.ConsumerDecisionTreeId);
        /// <summary>
        /// The Consumer Decision Tree Id
        /// </summary>
        public Int32? ConsumerDecisionTreeId
        {
            get { return GetProperty<Int32?>(ConsumerDecisionTreeIdProperty); }
        }

        public static readonly ModelPropertyInfo<Guid> UniqueContentReferenceProperty =
           RegisterModelProperty<Guid>(c => c.UniqueContentReference);
        /// <summary>
        /// The Consumer Decision Tree Id
        /// </summary>
        public Guid UniqueContentReference
        {
            get { return GetProperty<Guid>(UniqueContentReferenceProperty); }
        }


        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The Consumer Decision Tree Id
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }



        /// <summary>
        /// Entity id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        /// <summary>
        /// The entity id
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
        }


        /// <summary>
        /// Product group id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProductGroupIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductGroupId);
        /// <summary>
        /// The id of the product group this item is assigned to
        /// </summary>
        public Int32 ProductGroupId
        {
            get { return GetProperty<Int32>(ProductGroupIdProperty); }
        }

        public static readonly ModelPropertyInfo<String> ProductGroupNameProperty =
            RegisterModelProperty<String>(c => c.ProductGroupName);
        /// <summary>
        /// Product groups Name
        /// </summary>
        public String ProductGroupName
        {
            get { return GetProperty<String>(ProductGroupNameProperty); }
        }


        /// <summary>
        /// Date Created property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);
        /// <summary>
        /// Returns the date this item was created
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
        }

  
        /// <summary>
        /// Date last modified proeprty definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        /// <summary>
        /// Returns the date this item was last modified
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
        }

        /// <summary>
        /// Parent unique content reference property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Guid?> ParentUniqueContentReferenceProperty =
            RegisterModelProperty<Guid?>(c => c.ParentUniqueContentReference);
        /// <summary>
        /// Parent unique content reference
        /// </summary>
        public Guid? ParentUniqueContentReference
        {
            get { return GetProperty<Guid?>(ParentUniqueContentReferenceProperty); }
        }
        
        #region Helper properties

        /// <summary>
        /// Freindly Product group
        /// </summary>
        public static readonly ModelPropertyInfo<String> FriendlyAssignedCategoryProperty =
            RegisterModelProperty<String>(c => c.FriendlyAssignedCategory);
        /// <summary>
        /// Returns the date this item was deleted or null if item has not been deleted
        /// </summary>
        public String FriendlyAssignedCategory
        {
            get { return String.Concat(ProductGroupId.ToString(), " ", ProductGroupName) ; }
        }

        #endregion

        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByLatestVersionByName
        /// </summary>
        [Serializable]
        public class FetchLatestVersionByEntityIdNameCriteria : CriteriaBase<FetchLatestVersionByEntityIdNameCriteria>
        {
            #region Properties
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static readonly PropertyInfo<String> NameProperty =
                RegisterProperty<String>(c => c.Name);
            public String Name
            {
                get { return ReadProperty<String>(NameProperty); }
            }
            #endregion

            public FetchLatestVersionByEntityIdNameCriteria(Int32 entityId, String name)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<String>(NameProperty, name);
            }
        }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.Name;
        }

        #endregion
    }
}