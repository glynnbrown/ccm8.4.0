﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
// V8-24779 : D.Pleasance
//  Added helper property PlanogramSourceFileName and implementation to create from plan file
// V8-27411 : M.Pettit
//  Added UserName property
#endregion
#region Version History: CCM802
// V8-29026 : D.Pleasance
//  Added new NewWorkpackagePlanogram implementation to include planogramName
#endregion
#region Version History: CCM810
// V8-29811 : A.Probyn
//  Added new ApplyWorkpackageSourceFolderPostfix
// V8-29858 : M.Pettit
//  Parameters list property registration must be marked as lazy loaded.
//  Added OnCopy() method implementation
#endregion
#endregion

using System;
using System.Security.Principal;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Csla;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class WorkpackagePlanogram : ModelObject<WorkpackagePlanogram>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent workpackage
        /// </summary>
        public new Workpackage Parent
        {
            get { return ((WorkpackagePlanogramList)base.Parent).Parent; }

        }
        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the workpackage planogram id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region WorkflowId
        /// <summary>
        /// WorkflowId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> WorkflowIdProperty =
            RegisterModelProperty<Int32>(c => c.WorkflowId);
        /// <summary>
        /// Returns the workflow id
        /// </summary>
        public Int32 WorkflowId
        {
            get { return this.GetProperty<Int32>(WorkflowIdProperty); }
        }
        #endregion

        #region SourcePlanogramId
        /// <summary>
        /// SourcePlanogramId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> SourcePlanogramIdProperty =
            RegisterModelProperty<Int32?>(c => c.SourcePlanogramId);
        /// <summary>
        /// Returns the source planogram id, if there is one
        /// </summary>
        public Int32? SourcePlanogramId
        {
            get { return this.GetProperty<Int32?>(SourcePlanogramIdProperty); }
        }
        #endregion

        #region DestinationPlanogramId
        /// <summary>
        /// DestinationPlanogramId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> DestinationPlanogramIdProperty =
            RegisterModelProperty<Int32>(c => c.DestinationPlanogramId);
        /// <summary>
        /// Returns the destination planogram id
        /// </summary>
        public Int32 DestinationPlanogramId
        {
            get { return this.GetProperty<Int32>(DestinationPlanogramIdProperty); }
            set { this.SetProperty<Int32>(DestinationPlanogramIdProperty, value); }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Returns the planogram name
        /// </summary>
        public String Name
        {
            get { return this.GetProperty<String>(NameProperty); }
        }
        #endregion

        #region UserName
        /// <summary>
        /// UserName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> UserNameProperty =
            RegisterModelProperty<String>(c => c.UserName);
        /// <summary>
        /// Returns the planogram UserName (Owner)
        /// </summary>
        public String UserName
        {
            get { return this.GetProperty<String>(UserNameProperty); }
        }
        #endregion

        #region ProcessingStatus
        /// <summary>
        /// ProcessingStatus property definition
        /// </summary>
        public static readonly ModelPropertyInfo<ProcessingStatus> ProcessingStatusProperty =
            RegisterModelProperty<ProcessingStatus>(c => c.ProcessingStatus);
        /// <summary>
        /// Returns the processing status
        /// </summary>
        public ProcessingStatus ProcessingStatus
        {
            get { return this.GetProperty<ProcessingStatus>(ProcessingStatusProperty); }
        }

        #endregion

        #region Parameters
        /// <summary>
        /// Parameters property definition
        /// </summary>
        public static readonly ModelPropertyInfo<WorkpackagePlanogramParameterList> ParametersProperty =
            RegisterModelProperty<WorkpackagePlanogramParameterList>(c => c.Parameters, RelationshipTypes.LazyLoad);
        /// <summary>
        /// Synchronously returns all parameters for the planogram
        /// </summary>
        public WorkpackagePlanogramParameterList Parameters
        {
            get
            {
                return this.GetPropertyLazy<WorkpackagePlanogramParameterList>(
                    ParametersProperty,
                    new WorkpackagePlanogramParameterList.FetchByWorkpackgePlanogramIdCriteria(this.Id, this.WorkflowId),
                    true);
            }
        }

        /// <summary>
        /// Asynchronously returns all parameters for the planogram
        /// </summary>
        public WorkpackagePlanogramParameterList ParametersAsync
        {
            get
            {
                return this.GetPropertyLazy<WorkpackagePlanogramParameterList>(
                    ParametersProperty,
                    new WorkpackagePlanogramParameterList.FetchByWorkpackgePlanogramIdCriteria(this.Id, this.WorkflowId),
                    false);
            }
        }
        #endregion

        #region Helper properties

        /// <summary>
        /// PlanogramSourceFileName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> PlanogramSourceFileNameProperty =
            RegisterModelProperty<String>(c => c.PlanogramSourceFileName);
        /// <summary>
        /// The Planogram Source File Name
        /// </summary>
        public String PlanogramSourceFileName
        {
            get { return this.GetProperty<String>(PlanogramSourceFileNameProperty); }
            set { this.SetProperty<String>(PlanogramSourceFileNameProperty, value); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(WorkpackagePlanogram), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogram), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.WorkpackageCreate.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogram), new IsInRole(AuthorizationActions.EditObject, DomainPermission.WorkpackageEdit.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogram), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.WorkpackageDelete.ToString()));
        }
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            this.BusinessRules.AddRule(new Required(NameProperty));
            this.BusinessRules.AddRule(new MaxLength(NameProperty, 100));
            base.AddBusinessRules();
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new workpackage planogram
        /// </summary>
        internal static WorkpackagePlanogram NewWorkpackagePlanogram()
        {
            WorkpackagePlanogram item = new WorkpackagePlanogram();
            item.Create(null, null, null);
            return item;
        }

        /// <summary>
        /// Creates a new workpackage planogram
        /// </summary>
        internal static WorkpackagePlanogram NewWorkpackagePlanogram(PlanogramInfo sourcePlanogram)
        {
            WorkpackagePlanogram item = new WorkpackagePlanogram();
            item.Create(sourcePlanogram, null, null);
            return item;
        }

        /// <summary>
        /// Creates a new workpackage planogram
        /// </summary>
        internal static WorkpackagePlanogram NewWorkpackagePlanogram(PlanogramInfo sourcePlanogram, String planogramName)
        {
            WorkpackagePlanogram item = new WorkpackagePlanogram();
            item.Create(sourcePlanogram, null, planogramName);
            return item;
        }

        /// <summary>
        /// Creates a new workpackage planogram
        /// </summary>
        internal static WorkpackagePlanogram NewWorkpackagePlanogram(String planFile)
        {
            WorkpackagePlanogram item = new WorkpackagePlanogram();
            item.Create(null, planFile, null);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected void Create(PlanogramInfo sourcePlanogram, String planFile, String planogramName)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<ProcessingStatus>(ProcessingStatusProperty, ProcessingStatus.Pending);
            this.LoadProperty<WorkpackagePlanogramParameterList>(ParametersProperty, WorkpackagePlanogramParameterList.NewWorkpackagePlanogramParameterList());
            this.LoadProperty<String>(UserNameProperty, WindowsIdentity.GetCurrent().Name);
            if (sourcePlanogram != null)
            {
                this.LoadProperty<Int32?>(SourcePlanogramIdProperty, sourcePlanogram.Id);
                this.LoadProperty<String>(NameProperty, (planogramName == null) ? sourcePlanogram.Name : planogramName);
            }
            if (!String.IsNullOrEmpty(planFile))
            {
                this.LoadProperty<String>(NameProperty, planFile);
            }
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Helper method to update the work package planograms name to include the workpackage.
        /// </summary>
        /// <param name="workpackagePlanogram"></param>
        /// <param name="workpackageName"></param>
        public static void ApplyWorkpackageSourceFolderPostfix(WorkpackagePlanogram workpackagePlanogram, String workpackageName)
        { 
            //Include workpackage name in the workpackage planogram
            workpackagePlanogram.LoadProperty<String>(NameProperty, String.Format("{0} {1}", workpackagePlanogram.Name, workpackageName));
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<WorkpackagePlanogram>(oldId, newId);
        }

        #endregion
    }
}
