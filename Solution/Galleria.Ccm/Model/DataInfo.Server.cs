﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014.

#region Version History: (CCM 8.0)
// V8-28234 : A.Probyn
//      Adapted from SA.
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;

using Csla;

using Galleria.Framework.Dal;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class DataInfo
    {
        #region Constructor
        private DataInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the data info from the solution
        /// </summary>
        /// <returns>DataInfo object</returns>
        /// <param name="entityId">Id of the entity in question</param>
        public static DataInfo FetchDataInfo(Int32 entityId)
        {
            //Call fetch using entity id stored in application global context
            return DataPortal.Fetch<DataInfo>(new FetchByEntityIdCriteria(entityId));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads a dtos into this object
        /// </summary>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(DataInfoDto dto)
        {
            LoadProperty<Boolean>(HasProductHierarchyProperty, dto.HasProductHierarchy);
            LoadProperty<Boolean>(HasLocationProperty, dto.HasLocation);
            LoadProperty<Boolean>(HasProductProperty, dto.HasProduct);
            LoadProperty<Boolean>(HasLocationHierarchyProperty, dto.HasLocationHierarchy);
            LoadProperty<Boolean>(HasAssortmentProperty, dto.HasAssortment);
            LoadProperty<Boolean>(HasClusterSchemeProperty, dto.HasClusterScheme);
            LoadProperty<Boolean>(HasLocationSpaceProperty, dto.HasLocationSpace);
            LoadProperty<Boolean>(HasLocationSpaceBayProperty, dto.HasLocationSpaceBay);
            LoadProperty<Boolean>(HasLocationSpaceElementProperty, dto.HasLocationSpaceElement);
            LoadProperty<Boolean>(HasLocationProductAttributeProperty, dto.HasLocationProductAttribute);
            LoadProperty<Boolean>(HasLocationProductIllegalProperty, dto.HasLocationProductIllegal);
            LoadProperty<Boolean>(HasLocationProductLegalProperty, dto.HasLocationProductLegal);

            //take a note of the time this status was created.
            LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            try
            {
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IDataInfoDal dal = dalContext.GetDal<IDataInfoDal>())
                    {
                        DataInfoDto dto = dal.FetchByEntityId(criteria.EntityId);
                        LoadDataTransferObject(dto);
                    }
                }
            }
            catch (DtoDoesNotExistException)
            {
                //do nothing
            }
        }
        #endregion

        #endregion
    }
}
