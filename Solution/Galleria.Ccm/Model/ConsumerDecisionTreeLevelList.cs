﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a list containing ConsumerDecisionTreeLevel objects
    /// </summary>
    [Serializable]
    public sealed partial class ConsumerDecisionTreeLevelList : ModelList<ConsumerDecisionTreeLevelList, ConsumerDecisionTreeLevel>
    {
        #region Parent

        /// <summary>
        /// Returns the parent product level
        /// </summary>
        public ConsumerDecisionTreeLevel ParentLevel
        {
            get { return base.Parent as ConsumerDecisionTreeLevel; }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeLevelList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ConsumerDecisionTreeCreate.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeLevelList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ConsumerDecisionTreeGet.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeLevelList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ConsumerDecisionTreeEdit.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeLevelList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ConsumerDecisionTreeDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        internal static ConsumerDecisionTreeLevelList NewConsumerDecisionTreeLevelList()
        {
            ConsumerDecisionTreeLevelList item = new ConsumerDecisionTreeLevelList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private new void Create()
        {
            MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}
