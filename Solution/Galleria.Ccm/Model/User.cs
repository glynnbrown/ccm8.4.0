﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25664: A.Kuszyk
//		Created (adapted from SA).
// V8-26222 : L.Luong
//      Added date deleted
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a user within the solution
    /// (root object)
    /// </summary>
    [Serializable]
    public sealed partial class User : ModelObject<User>
    {
        #region Properties

        #region Id
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The users id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region RowVersion
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The users row version
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region UserName
        public static readonly ModelPropertyInfo<String> UserNameProperty =
            RegisterModelProperty<String>(c => c.UserName, Message.User_UserName);
        /// <summary>
        /// The users username
        /// </summary>
        public String UserName
        {
            get { return GetProperty<String>(UserNameProperty); }
            set { SetProperty<String>(UserNameProperty, value); }
        }
        #endregion

        #region FirstName
        public static readonly ModelPropertyInfo<String> FirstNameProperty =
            RegisterModelProperty<String>(c => c.FirstName, Message.User_FirstName);
        /// <summary>
        /// The users first name
        /// </summary>
        public String FirstName
        {
            get { return GetProperty<String>(FirstNameProperty); }
            set { SetProperty<String>(FirstNameProperty, value); }
        }
        #endregion

        #region LastName
        public static readonly ModelPropertyInfo<String> LastNameProperty =
            RegisterModelProperty<String>(c => c.LastName, Message.User_LastName);
        /// <summary>
        /// The users last name
        /// </summary>
        public String LastName
        {
            get { return GetProperty<String>(LastNameProperty); }
            set { SetProperty<String>(LastNameProperty, value); }
        }
        #endregion

        #region Description
        /// <summary>
        /// Description property defintion
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description, Message.User_Description);
        /// <summary>
        /// The users description
        /// </summary>
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
            set { SetProperty<String>(DescriptionProperty, value); }
        }
        #endregion

        #region Office
        /// <summary>
        /// Office property defintion
        /// </summary>
        public static readonly ModelPropertyInfo<String> OfficeProperty =
            RegisterModelProperty<String>(c => c.Office, Message.User_Office);
        /// <summary>
        /// The users office
        /// </summary>
        public String Office
        {
            get { return GetProperty<String>(OfficeProperty); }
            set { SetProperty<String>(OfficeProperty, value); }
        }
        #endregion

        #region Telephone Number
        /// <summary>
        /// TelephoneNumber property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> TelephoneNumberProperty =
            RegisterModelProperty<String>(c => c.TelephoneNumber, Message.User_TelephoneNumber);
        /// <summary>
        /// The users telephone number
        /// </summary>
        public String TelephoneNumber
        {
            get { return GetProperty<String>(TelephoneNumberProperty); }
            set { SetProperty<String>(TelephoneNumberProperty, value); }
        }
        #endregion

        #region EMailAddress
        /// <summary>
        /// EmailAddress property defintion
        /// </summary>
        public static readonly ModelPropertyInfo<String> EMailAddressProperty =
            RegisterModelProperty<String>(c => c.EMailAddress, Message.User_EMailAddress);
        /// <summary>
        /// The users email address
        /// </summary>
        public String EMailAddress
        {
            get { return GetProperty<String>(EMailAddressProperty); }
            set { SetProperty<String>(EMailAddressProperty, value); }
        }
        #endregion

        #region Title
        /// <summary>
        /// Title property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> TitleProperty =
            RegisterModelProperty<String>(c => c.Title, Message.User_Title);
        /// <summary>
        /// The users title
        /// </summary>
        public String Title
        {
            get { return GetProperty<String>(TitleProperty); }
            set { SetProperty<String>(TitleProperty, value); }
        }
        #endregion

        #region Department
        /// <summary>
        /// Department property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DepartmentProperty =
            RegisterModelProperty<String>(c => c.Department, Message.User_Department);
        /// <summary>
        /// The users department
        /// </summary>
        public String Department
        {
            get { return GetProperty<String>(DepartmentProperty); }
            set { SetProperty<String>(DepartmentProperty, value); }
        }
        #endregion

        #region Company
        /// <summary>
        /// Company property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CompanyProperty =
            RegisterModelProperty<String>(c => c.Company, Message.User_Company);
        /// <summary>
        /// The users company
        /// </summary>
        public String Company
        {
            get { return GetProperty<String>(CompanyProperty); }
            set { SetProperty<String>(CompanyProperty, value); }
        }
        #endregion

        #region Manager
        /// <summary>
        /// Manager property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ManagerProperty =
            RegisterModelProperty<String>(c => c.Manager, Message.User_Manager);
        /// <summary>
        /// The users company
        /// </summary>
        public String Manager
        {
            get { return GetProperty<String>(ManagerProperty); }
            set { SetProperty<String>(ManagerProperty, value); }
        }

        #endregion

        #region DateDeleted
        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);

        /// <summary>
        /// Gets/Sets the DateDeleted value
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
            set { SetProperty<DateTime?>(DateDeletedProperty, value); }
        }
        #endregion

        #region RoleMembers
        /// <summary>
        /// RoleMembers property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RoleMemberList> RoleMembersProperty =
            RegisterModelProperty<RoleMemberList>(c => c.RoleMembers);
        /// <summary>
        /// Returns the list of role member records for this user.
        /// </summary>
        public RoleMemberList RoleMembers
        {
            get { return GetProperty<RoleMemberList>(RoleMembersProperty); }
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(User), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.UserCreate.ToString()));
            BusinessRules.AddRule(typeof(User), new IsInRole(AuthorizationActions.GetObject, DomainPermission.UserGet.ToString()));
            BusinessRules.AddRule(typeof(User), new IsInRole(AuthorizationActions.EditObject, DomainPermission.UserEdit.ToString()));
            BusinessRules.AddRule(typeof(User), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.UserDelete.ToString()));
        }
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(UserNameProperty));
            BusinessRules.AddRule(new MaxLength(UserNameProperty, 50));

            BusinessRules.AddRule(new MaxLength(FirstNameProperty, 100));
            BusinessRules.AddRule(new MaxLength(LastNameProperty, 100));
            BusinessRules.AddRule(new MaxLength(DescriptionProperty, 255));
            BusinessRules.AddRule(new MaxLength(OfficeProperty, 100));
            BusinessRules.AddRule(new MaxLength(TelephoneNumberProperty, 50));
            BusinessRules.AddRule(new MaxLength(EMailAddressProperty, 50));
            BusinessRules.AddRule(new MaxLength(TitleProperty, 50));
            BusinessRules.AddRule(new MaxLength(DepartmentProperty, 50));
            BusinessRules.AddRule(new MaxLength(CompanyProperty, 50));
            BusinessRules.AddRule(new MaxLength(ManagerProperty, 50));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new user
        /// </summary>
        /// <returns>A new user object</returns>
        public static User NewUser()
        {
            User item = new User();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        public void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<RoleMemberList>(RoleMembersProperty, RoleMemberList.NewList());
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the full name string
        /// for this user.
        /// </summary>
        public String GetFullName()
        {
            return this.FirstName + " " + this.LastName;
        }

        #endregion

        #region Override

        public override string ToString()
        {
            return this.UserName;
        }

        #endregion
    }
}
