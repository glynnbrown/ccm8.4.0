﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
// GFS-25455 : J.Pickup
//  Added FetchByEntityIdAssortmentIdsCriteria
// CCM-26520 :J.Pickup
//  Added FetchByProductGroupIdCriteria
#endregion

#region Version History: (CCM 8.0.3)
// GFS-29134 : D.Pleasance
//  removed FetchByEntityIdIncludingDeletedCriteria as not required since we now delete outright
#endregion

#region Version History: (CCM 8.1.1)
// V8-30675 : D.Pleasance
//  Amended FetchByEntityIdAssortmentIdsCriteria so that within constructor the property AssortmentIdsProperty 
//  is set to the constructor parameter rather than itself!
#endregion 

#region Version History: (CCM 8.2.0)
// V8-30461 : M.Shelley
//  Added Entity_Id to FetchByProductGroupId
#endregion

#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Csla.Serialization;
using Galleria.Framework.Model;
using Galleria.Ccm.Security;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// AssortmentInfoList
    /// </summary>
    [Serializable]
    public partial class AssortmentInfoList : ModelReadOnlyList<AssortmentInfoList, AssortmentInfo>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(AssortmentInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(AssortmentInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(AssortmentInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByEntityId
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : Csla.CriteriaBase<FetchByEntityIdCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
            RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
            }
        }

        /// <summary>
        /// Criteria for FetchByProductGroupId
        /// </summary>
        [Serializable]
        public class FetchByProductGroupIdCriteria : Csla.CriteriaBase<FetchByProductGroupIdCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static readonly PropertyInfo<Int32> ProductGroupIdProperty =
                RegisterProperty<Int32>(c => c.ProductGroupId);
            public Int32 ProductGroupId
            {
                get { return ReadProperty<Int32>(ProductGroupIdProperty); }
            }

            public FetchByProductGroupIdCriteria(Int32 entityId, Int32 productGroupId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<Int32>(ProductGroupIdProperty, productGroupId);
            }
        }
        
        /// <summary>
        /// Criteria for FetchByEntityIdChangeDate
        /// </summary>
        [Serializable]
        public class FetchByEntityIdChangeDateCriteria : CriteriaBase<FetchByEntityIdChangeDateCriteria>
        {
            #region Properties

            public static PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static PropertyInfo<DateTime> ChangeDateProperty =
                RegisterProperty<DateTime>(c => c.ChangeDate);
            public DateTime ChangeDate
            {
                get { return ReadProperty<DateTime>(ChangeDateProperty); }
            }

            #endregion

            public FetchByEntityIdChangeDateCriteria(Int32 entityId, DateTime changeDate)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<DateTime>(ChangeDateProperty, changeDate);
            }
        }

        /// <summary>
        /// Criteria for FetchByDeletedEntityId
        /// </summary>
        [Serializable]
        public class FetchDeletedByEntityIdCriteria : CriteriaBase<FetchDeletedByEntityIdCriteria>
        {
            #region Properties

            public static PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static PropertyInfo<DateTime?> DeletedDateProperty =
                RegisterProperty<DateTime?>(c => c.DeletedDate);
            public DateTime? DeletedDate
            {
                get { return ReadProperty<DateTime?>(DeletedDateProperty); }
            }

            #endregion

            public FetchDeletedByEntityIdCriteria(Int32 entityId, DateTime? deletedDate)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<DateTime?>(DeletedDateProperty, deletedDate);
            }
        }


        /// <summary>
        /// Criteria for FetchByLatestVersionByName
        /// </summary>
        [Serializable]
        public class FetchAssortmentByEntityIdAssortmentSearchCriteria : CriteriaBase<FetchAssortmentByEntityIdAssortmentSearchCriteria>
        {
            #region Properties
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static readonly PropertyInfo<String> AssortmentSearchCriteriaProperty =
                RegisterProperty<String>(c => c.AssortmentSearchCriteria);
            public String AssortmentSearchCriteria
            {
                get { return ReadProperty<String>(AssortmentSearchCriteriaProperty); }
            }
            #endregion

            public FetchAssortmentByEntityIdAssortmentSearchCriteria(Int32 entityId, String AssortmentSearchCriteria)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<String>(AssortmentSearchCriteriaProperty, AssortmentSearchCriteria);
            }
        }

        #region FetchByEntityIdProductGtinsCriteria
        /// <summary>
        /// Criteria for FetchByEntityIdAssortmentIdsCriteria
        /// </summary>
        [Serializable]
        internal class FetchByEntityIdAssortmentIdsCriteria : CriteriaBase<FetchByEntityIdAssortmentIdsCriteria>
        {
            #region Properties

            #region EntityId
            /// <summary>
            /// EntityId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            /// <summary>
            /// Returns the entity id
            /// </summary>
            public Int32 EntityId
            {
                get { return this.ReadProperty<Int32>(EntityIdProperty); }
            }
            #endregion

            #region Assortment Ids
            /// <summary>
            /// AssortmentIds property definition
            /// </summary>
            public static PropertyInfo<IEnumerable<Int32>> AssortmentIdsProperty =
                RegisterProperty<IEnumerable<Int32>>(c => c.AssortmentIds);
            /// <summary>
            /// Returns the assortment ids
            /// </summary>
            public IEnumerable<Int32> AssortmentIds
            {
                get { return ReadProperty<IEnumerable<Int32>>(AssortmentIdsProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByEntityIdAssortmentIdsCriteria(Int32 entityId, IEnumerable<Int32> assortmentIds)
            {
                this.LoadProperty<Int32>(EntityIdProperty, entityId);
                this.LoadProperty<IEnumerable<Int32>>(AssortmentIdsProperty, assortmentIds);
            }
            #endregion
        }
        #endregion

        #endregion
    }
}