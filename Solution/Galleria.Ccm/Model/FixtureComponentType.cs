﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
#endregion
#region Version History: (CCM 8.2)
// V8-30936 : M.Brumby
//  Added slotwall
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    public enum FixtureComponentType
    {
        Custom = 0,
        Shelf = 1,
        Bar = 2,
        Peg = 3,
        Chest = 4,
        Backboard = 5,
        Panel = 6,
        Base = 7,
        Rod = 8,
        ClipStrip = 9,
        Pallet = 10,
        SlotWall = 11
    }

    /// <summary>
    /// Component Type Helper Class
    /// </summary>
    public static class FixtureComponentTypeHelper
    {
        public static readonly Dictionary<FixtureComponentType, String> FriendlyNames =
            new Dictionary<FixtureComponentType, String>()
            {
                {FixtureComponentType.Custom, Message.Enum_ComponentType_Custom },
                {FixtureComponentType.Shelf, Message.Enum_ComponentType_Shelf },
                {FixtureComponentType.Bar, Message.Enum_ComponentType_Bar},
                {FixtureComponentType.Peg, Message.Enum_ComponentType_Peg},
                {FixtureComponentType.Chest, Message.Enum_ComponentType_Chest},
                {FixtureComponentType.Backboard, Message.Enum_ComponentType_Backboard},
                {FixtureComponentType.Panel, Message.Enum_ComponentType_Panel},
                {FixtureComponentType.Base, Message.Enum_ComponentType_Base},
                {FixtureComponentType.Rod, Message.Enum_ComponentType_Rod},
                {FixtureComponentType.ClipStrip, Message.Enum_ComponentType_Clipstrip},
                {FixtureComponentType.Pallet, Message.Enum_ComponentType_Pallet},
                {FixtureComponentType.SlotWall, Message.Enum_ComponentType_SlotWall},
            };

        public static FixtureComponentType Parse(String value)
        {
            return EnumHelper.Parse<FixtureComponentType>(value, FixtureComponentType.Custom);
        }
    }
}
