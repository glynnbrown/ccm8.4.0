﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class WorkpackagePlanogramParameterList
    {
        #region Constructor
        private WorkpackagePlanogramParameterList() { } // force use of factory methods
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning all child parameters for a specified workpackage planogram
        /// </summary>
        private void DataPortal_Fetch(FetchByWorkpackgePlanogramIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                // fetch the workflow task details
                IEnumerable<WorkflowTaskDto> workflowTaskDtoList;
                using (IWorkflowTaskDal dal = dalContext.GetDal<IWorkflowTaskDal>())
                {
                    workflowTaskDtoList = dal.FetchByWorkflowId(criteria.WorkflowId);
                }

                // fetch the workflow task parameter list
                IEnumerable<WorkflowTaskParameterDto> workflowTaskParameterDtoList;
                using (IWorkflowTaskParameterDal dal = dalContext.GetDal<IWorkflowTaskParameterDal>())
                {
                    workflowTaskParameterDtoList = dal.FetchByWorkflowId(criteria.WorkflowId);
                }

                // now fetch the workpackage planogram parameters
                using (IWorkpackagePlanogramParameterDal dal = dalContext.GetDal<IWorkpackagePlanogramParameterDal>())
                {
                    IEnumerable<WorkpackagePlanogramParameterDto> dtoList = dal.FetchByWorkpackagePlanogramId(criteria.WorkpackagePlanogramId);
                    foreach (WorkpackagePlanogramParameterDto dto in dtoList)
                    {
                        // find the associated workflow task parameter
                        WorkflowTaskParameterDto workflowTaskParameterDto = workflowTaskParameterDtoList.FirstOrDefault(item => item.Id == dto.WorkflowTaskParameterId);

                        // find the associated workflow task
                        WorkflowTaskDto workflowTaskDto = null;
                        if (workflowTaskParameterDto != null)
                        {
                            workflowTaskDto = workflowTaskDtoList.FirstOrDefault(item => item.Id == workflowTaskParameterDto.WorkflowTaskId);
                        }

                        // add the workpackage planogram parameter
                        this.Add(WorkpackagePlanogramParameter.GetWorkpackagePlanogramParameter(dalContext, dto, workflowTaskDto, workflowTaskParameterDto));
                    }
                }
            }
            this.MarkAsChild();
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
