﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630: A.Kuszyk
//  Created (copied from SA).
// V8-26560 : A.Probyn
//  Added FetchById
#endregion
#region Version History: (CCM 8.0.3)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion

using System;
using System.Linq;
using System.Diagnostics.CodeAnalysis;

using Csla;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Resources.Language;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    public partial class ProductUniverseInfo
    {
        #region Constructor
        private ProductUniverseInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an ProductUniverseInfo from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        /// <returns>An ProductUniverseInfo object</returns>
        internal static ProductUniverseInfo FetchProductUniverseInfo(IDalContext dalContext, ProductUniverseInfoDto dto)
        {
            return DataPortal.FetchChild<ProductUniverseInfo>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, ProductUniverseInfoDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Guid>(UniqueContentReferenceProperty, dto.UniqueContentReference);
            LoadProperty<Int32?>(ProductGroupIdProperty, dto.ProductGroupId);
            LoadProperty<String>(ProductGroupCodeProperty, dto.ProductGroupCode);
            LoadProperty<String>(ProductGroupNameProperty, dto.ProductGroupName);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<Boolean>(IsMasterProperty, dto.IsMaster);
            LoadProperty<Int32>(ProductCountProperty, dto.ProductCount);
            LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            LoadProperty<Guid?>(ParentUniqueContentReferenceProperty, dto.ParentUniqueContentReference);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, ProductUniverseInfoDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #endregion

        #region Helpers

        /// <summary>
        /// Returns a single ProductUniverse info by id.
        /// Uses the ProductUniverseInfoList FetchByProductUniverseIds.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ProductUniverseInfo FetchById(Int32 id)
        {
            return ProductUniverseInfoList.FetchByProductUniverseIds(new List<Int32> { id }).FirstOrDefault();
        }

        #endregion
    }
}
