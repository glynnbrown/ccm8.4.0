﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25436 L.Luong
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class HighlightInfo : ModelReadOnlyObject<HighlightInfo>
    {
        #region Properties

        #region Id

        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Object> IdProperty =
         RegisterModelProperty<Object>(c => c.Id);
        /// <summary>
        /// The unique object id
        /// </summary>
        public Object Id
        {
            get { return GetProperty<Object>(IdProperty); }
        }

        #endregion

        #region FileName

        /// <summary>
        /// FileName property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> FileNameProperty =
            RegisterModelProperty<String>(c => c.FileName);
        /// <summary>
        /// Returns the name of the file this info represents
        /// if of a file system type.
        /// </summary>
        public String FileName
        {
            get { return this.GetProperty<String>(FileNameProperty); }
        }

        #endregion

        #region Name

        public static readonly ModelPropertyInfo<String> NameProperty =
           RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The product name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Methods

        public override String ToString()
        {
            return this.Name;
        }

        #endregion
    }
}
