﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM803
// V8-29491 : D.Pleasance
//  Created
#endregion
#region Version History : CCM820
// V8-30727 : D.Pleasance
//  Added PlanogramAssignment.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Enum that defines a workpackage performance data type
    /// </summary>
    public enum WorkpackagePerformanceDataType : byte
    {
        Enterprise = 0,
        Cluster = 1,
        Store = 2,
        PlanogramAssignment = 3
    }
    /// <summary>
    ///     UnitOfMeasureWeightType Helper Class
    /// </summary>
    public static class WorkpackagePerformanceDataTypeHelper
    {
        /// <summary>
        ///     Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<WorkpackagePerformanceDataType, String> FriendlyNames =
            new Dictionary<WorkpackagePerformanceDataType, String>
            {
                {WorkpackagePerformanceDataType.Enterprise, Message.Enum_WorkpackagePerformanceDataType_Enterprise},
                {WorkpackagePerformanceDataType.Cluster, Message.Enum_WorkpackagePerformanceDataType_Cluster},
                {WorkpackagePerformanceDataType.Store, Message.Enum_WorkpackagePerformanceDataType_Location},
                {WorkpackagePerformanceDataType.PlanogramAssignment, Message.Enum_WorkpackagePerformanceDataType_PlanogramAssignment}
            };
    }
}