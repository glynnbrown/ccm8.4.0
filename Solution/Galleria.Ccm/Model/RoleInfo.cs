﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26234 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Lightweight role object definition
    /// </summary>
    [Serializable]
    public sealed partial class RoleInfo : ModelReadOnlyObject<RoleInfo>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The unique role id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The role name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }
        #endregion

        #region Description
        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);
        /// <summary>
        /// The role description
        /// </summary>
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
        }
        #endregion

        #region Is Administrator
        /// <summary>
        /// IsAdministrator property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsAdministratorRoleProperty =
            RegisterModelProperty<Boolean>(c => c.IsAdministratorRole);
        /// <summary>
        /// Indicates if this is the in-built administrator role
        /// </summary>
        public Boolean IsAdministratorRole
        {
            get { return GetProperty<Boolean>(IsAdministratorRoleProperty); }
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(RoleInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(RoleInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.RoleGet.ToString()));
            BusinessRules.AddRule(typeof(RoleInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(RoleInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Overrides

        #region ToString

        public override String ToString()
        {
            return this.Name;
        }

        #endregion

        #endregion
    }
}
