﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
// V8-25868 : L.Ineson
//  Added Description property
#endregion
#region Version History: CCM803
// V8-29680 : N.Foster
//  Added method to create a workflow that contains tasks
#endregion
#region Version History: CCM810
// V8-30068 : M.Brumby
//  Created Default task lists
#endregion
#region Version History : CCM820
// V8-30803/30804/30805 : A.Kuszyk
//  Added additional default workflows.
// V8-30989 : A.Kuszyk
//  Added additional tasks to Merchandise workflows.
// V8-31076 : A.Probyn
//  Removed Merchandised Planogram from the default workflow tasks.
// V8-31174 : A.Kuszyk
//  Added Update Blocking to Range & Inventory refresh workflow.
// V8-31246 : A.Kuszyk
//  Added Update Product Attributes task to Create Sequence Template workflow.
// V8-31284 : A.Kuszyk
//  Replaced insert product task with merchandise product task in merchandise workflows.
#endregion
#region Version History : CCM830
// V8-31584 : A.Kuzyk
//  Added cache for data required by parameter values.
// V8-31835 : N.Haywood
//  Added Illegal and Legal MetaData and Validation
// V8-31830 : A.Probyn
//  Expanded cache for timeline selection, performance sources and timeline groups
// V8-31831 : A.Probyn
//  Expanded cache for Planogram Name Template Infos
//  Added UpdatePlanogramAttributeUsingPlanogramNameTemplate to PublishPlanogramWorkflowTasks
// V8-31808 : D.Pleasance
//  Amended Merchandise CLASSI and Location Specific workflows to include new merchandise inventory tasks.
// V8-32453 : A.Silva
//  Amended Merchandise CLASSI and Location Specific workflows to include Update Planogram Using Location Space and NOT include Remove Non Legal Products for the Specific Location.
// V8-32662 : A.Silva
//  Pulled out TryGetPerformanceSelectionInfo from PerformanceSelectionNameTaskParameterHelper.
// V8-32654 : D.Pleasance
//  Amended Merchandise CLASSI and Location Specific workflows to make use of new space constraint type 'within block presentation'. 
//  MerchandiseIncreaseProductInventory task now called twice, first time within block presentation and then beyond block presentation.
// V8-32589 : L.Ineson
//  Stopped DataCache methods from crashing when gfsentity returns null due to connection failure.
#endregion

#endregion

using System;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class Workflow : ModelObject<Workflow>
    {
        #region Default Task Workflow lists
        // Range Refresh
        public static String[] RangeRefreshWorkFlowTasks = new String[]
         {
            "Galleria.Ccm.Engine.Tasks.GetAssortmentByName.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdateProductPerformance.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdateProductAttributes.v1.Task",
            "Galleria.Ccm.Engine.Tasks.RemoveProductsFromAssortment.v1.Task",
            "Galleria.Ccm.Engine.Tasks.AddProductFromAssortment.v1.Task"
         };
        public static String[] InventoryRefreshWorkFlowTasks = new String[]
         {
            "Galleria.Ccm.Engine.Tasks.GetAssortmentByName.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdateProductPerformance.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdateProductAttributes.v1.Task",
            "Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.Task",
            "Galleria.Ccm.Engine.Tasks.RemoveProductsFromAssortment.v1.Task",
            "Galleria.Ccm.Engine.Tasks.ReduceAssortmentInventory.v1.Task",
            "Galleria.Ccm.Engine.Tasks.AddProductFromAssortment.v1.Task",
            "Galleria.Ccm.Engine.Tasks.IncreaseAssortmentInventory.v1.Task"
         };
        public static String[] RangeAndInventoryRefreshWorkFlowTasks = new String[]
         {
            "Galleria.Ccm.Engine.Tasks.GetAssortmentByName.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdateProductPerformance.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdateProductAttributes.v1.Task",
            "Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.Task",
            "Galleria.Ccm.Engine.Tasks.GetSequenceFromPlanogram.v1.Task",
            "Galleria.Ccm.Engine.Tasks.RemoveProductsFromAssortment.v1.Task",
            "Galleria.Ccm.Engine.Tasks.ReduceAssortmentInventory.v1.Task",
            "Galleria.Ccm.Engine.Tasks.InsertProductFromAssortment.v1.Task",
            "Galleria.Ccm.Engine.Tasks.IncreaseAssortmentInventory.v1.Task",
            "Galleria.Ccm.Engine.Tasks.GetValidationTemplateByName.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdatePlanogramValidation.v1.Task"
         };
        public static String[] BasicChangeWorkFlowTasks = new String[]
         {
            "Galleria.Ccm.Engine.Tasks.RemoveProductManual.v1.Task",
            "Galleria.Ccm.Engine.Tasks.ReplaceProductManual.v1.Task",
            "Galleria.Ccm.Engine.Tasks.AddProductManual.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdateProductPerformance.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdateProductAttributes.v1.Task",
            "Galleria.Ccm.Engine.Tasks.GetValidationTemplateByName.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdatePlanogramValidation.v1.Task"
         };

        public static String[] ValidationWorkFlowTasks = new String[]
         {
            "Galleria.Ccm.Engine.Tasks.GetValidationTemplateByName.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdatePlanogramValidation.v1.Task"
         };
        public static String[] PublishPlanogramWorkFlowTasks = new String[]
         {
             "Galleria.Ccm.Engine.Tasks.UpdatePlanogramAttributeUsingPlanogramNameTemplate.v1.Task",
             "Galleria.Ccm.Engine.Tasks.GetRenumberingStrategyByName.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdatePlanogramRenumbering.v1.Task",
            "Galleria.Ccm.Engine.Tasks.PublishToGfs.v1.Task"
         };

        public static String[] GoLocationSpecificWorkFlowTasks = new String[]
         {
             "Galleria.Ccm.Engine.Tasks.UpdatePlanogramAttributes.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdatePlanogramLocationAttribute.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdatePlanogramClusterAttribute.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdateProductPerformance.v1.Task",
            "Galleria.Ccm.Engine.Tasks.GetValidationTemplateByName.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdatePlanogramValidation.v1.Task"
         };

        public static readonly String[] MerchandiseClassiPlanogramWorkflowTasks = 
        {
            "Galleria.Ccm.Engine.Tasks.UpdatePlanogramLocationSpace.v1.Task",
            "Galleria.Ccm.Engine.Tasks.GetAssortmentByName.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdateProductPerformance.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdateProductAttributes.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdateLocationProductAttributes.v1.Task",
            "Galleria.Ccm.Engine.Tasks.RemoveIllegalProductsLocationSpecific.v1.Task",
            "Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.Task",
            "Galleria.Ccm.Engine.Tasks.GetSequenceFromPlanogram.v1.Task",
            "Galleria.Ccm.Engine.Tasks.GetBlockingFromPlanogram.v1.Task",
            "Galleria.Ccm.Engine.Tasks.MerchandisePlanogram.v1.Task",
            "Galleria.Ccm.Engine.Tasks.MerchandiseProductsUsingAssortment.v1.Task",
            "Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.Task",
            "Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1.Task",
            "Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1.Task",
            "Galleria.Ccm.Engine.Tasks.GetValidationTemplateByName.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdatePlanogramValidation.v1.Task",
        };

        public static readonly String[] MerchandiseLocationSpecificPlanogramWorkflowTasks = 
        {
            "Galleria.Ccm.Engine.Tasks.UpdatePlanogramLocationSpace.v1.Task",
            "Galleria.Ccm.Engine.Tasks.GetAssortmentByName.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdateProductPerformance.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdateProductAttributes.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdateLocationProductAttributes.v1.Task",
            "Galleria.Ccm.Engine.Tasks.RemoveIllegalProductsLocationSpecific.v1.Task",
            "Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1.Task",
            "Galleria.Ccm.Engine.Tasks.GetSequenceFromPlanogram.v1.Task",
            "Galleria.Ccm.Engine.Tasks.GetBlockingFromPlanogram.v1.Task",
            "Galleria.Ccm.Engine.Tasks.MerchandisePlanogram.v1.Task",
            "Galleria.Ccm.Engine.Tasks.MerchandiseProductsUsingAssortment.v1.Task",
            "Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseAssortmentInventory.v1.Task",
            "Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1.Task",
            "Galleria.Ccm.Engine.Tasks.MerchandiseIncreaseProductInventory.v1.Task",
            "Galleria.Ccm.Engine.Tasks.GetValidationTemplateByName.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdatePlanogramValidation.v1.Task",
        };

        public static String[] CreateSequenceTemplateWorkflowTasks = new String[]
        {
            // TODO - if this workflow is modified, be sure to update the default parameters
            // wherever it is referenced.
            "Galleria.Ccm.Engine.Tasks.UpdateProductAttributes.v1.Task",
            "Galleria.Ccm.Engine.Tasks.AddProductsUsingProductUniverse.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdateProductColourFromHighlight.v1.Task",
            "Galleria.Ccm.Engine.Tasks.CreateSequenceTemplate.v1.Task",
            "Galleria.Ccm.Engine.Tasks.GetValidationTemplateByName.v1.Task",
            "Galleria.Ccm.Engine.Tasks.UpdatePlanogramValidation.v1.Task",
        };
        #endregion

        #region Fields

        /// <summary>
        /// Holds a cache of data used by task parameter value helpers when selecting and validating data.
        /// </summary>
        private DataCache _taskParameterDataCache = null;

        #endregion

        #region Nested Types
        /// <summary>
        /// Caches common data required by an ITaskParameterHelper when selecting and validating data. This cache
        /// is held for the lifetime of this <see cref="Workflow"/>, but since this data is only ever accessed through the UI,
        /// the lifetime of each instance is limited.
        /// </summary>
        [Serializable]
        public class DataCache
        {
            #region Fields

            private Int32 _entityId;
            private Object _assortmentInfosLock = new Object();
            private AssortmentInfoList _assortmentInfos = null;
            private Object _blockingInfosLock = new Object();
            private BlockingInfoList _blockingInfos = null;
            private Object _clusterSchemeInfosLock = new Object();
            private ClusterSchemeInfoList _clusterSchemeInfos = null;
            private Object _consumerDecisionTreeInfosLock = new Object();
            private ConsumerDecisionTreeInfoList _consumerDecisionTreeInfos = null;
            private Object _entityLock = new Object();
            private Entity _entity = null;
            private Object _gfsModelEntityLock = new Object();
            private GFSModel.Entity _gfsModelEntity = null;
            private Object _planogramInfosByIdLock = new Object();
            private Dictionary<Int32, PlanogramInfo> _planogramInfosById = null;
            private Object _locationPlanAssignmentsByPlanogramIdLock = new Object();
            private Dictionary<Int32, LocationPlanAssignmentList> _locationPlanAssignmentsByPlanogramId = null;
            private Object _productGroupInfosByIdLock = new Object();
            private Dictionary<Int32, ProductGroupInfo> _productGroupInfosById = null;
            private Object _projectInfosByProductGroupCodeLock = new Object();
            private Dictionary<String,GFSModel.ProjectList> _projectInfosByProductGroupCode = null;
            private Object _projectInfosLock = new Object();
            private GFSModel.ProjectList _projectInfos = null;
            private Object _projectInfosWithNoProductGroupAssignedLock = new Object();
            private GFSModel.ProjectList _projectInfosWithNoProductGroupAssigned = null;
            private Object _highlightInfosLock = new Object();
            private HighlightInfoList _highlightInfos = null;
            private Object _inventoryProfilesinfosLock = new Object();
            private InventoryProfileInfoList _inventoryProfileInfos = null;
            private Object _locationInfosLock = new Object();
            private LocationInfoList _locationInfos = null;
            private Object _locationHierarchyLock = new Object();
            private LocationHierarchy _locationHierarchy = null;
            private Object _metricProfileInfosLock = new Object();
            private MetricProfileInfoList _metricProfileInfos = null;
            private Object _performanceSelectionInfosLock = new Object();
            private PerformanceSelectionInfoList _performanceSelectionInfos = null;
            private Object _planogramImportTemplateInfosLock = new Object();
            private PlanogramImportTemplateInfoList _planogramImportTemplateInfos = null;
            private Object _planogramImportTemplatesByIdLock = new Object();
            private Dictionary<Object, PlanogramImportTemplate> _planogramImportTemplatesById = null;
            private Object _productUniverseInfosLock = new Object();
            private ProductUniverseInfoList _productUniverseInfos = null;
            private Object _planogramNameTemplateInfosLock = new Object();
            private PlanogramNameTemplateInfoList _planogramNameTemplateInfos = null;
            private Object _planogramNameTemplatesByIdLock = new Object();
            private Dictionary<Int32, PlanogramNameTemplate> _planogramNameTemplatesById = null;
            private Object _renumberingStrategyInfosLock = new Object();
            private RenumberingStrategyInfoList _renumberingStrategyInfos = null;
            private Object _validationTemplateInfosLock = new Object();
            private ValidationTemplateInfoList _validationTemplateInfos = null;
            private Dictionary<Int32, GFSModel.PerformanceSource> _performanceSourceByPerformanceSourceId = null;
            private Object _performanceSourceByPerformanceSourceIdLock = new Object();
            private Dictionary<Int32, GFSModel.TimelineGroupList> _timelineGroupsByGFSPerformanceSourceId = null;
            private Object _timelineGroupsByGFSPerformanceSourceIdLock = new Object();
            private Dictionary<Int32, PerformanceSelection> _performanceSelectionByPerformanceSelectionId = null;
            private Object _performanceSelectionByPerformanceSelectionIdLock = new Object();
            private readonly Object _planogramComparisonTemplateInfosLock = new Object();
            private PlanogramComparisonTemplateInfoList _planogramComparisonTemplateInfos;
            #endregion

            #region Constructors
            public DataCache(Int32 entityId)
            {
                _entityId = entityId;
            } 
            #endregion

            #region Methods

            public ValidationTemplateInfoList GetValidationTemplateInfos()
            {
                if (_validationTemplateInfos == null)
                {
                    lock (_validationTemplateInfosLock)
                    {
                        _validationTemplateInfos = ValidationTemplateInfoList.FetchByEntityId(_entityId);
                    }
                }
                return _validationTemplateInfos;
            }

            public RenumberingStrategyInfoList GetRenumberingStrategyInfos()
            {
                if (_renumberingStrategyInfos == null)
                {
                    lock (_renumberingStrategyInfosLock)
                    {
                        _renumberingStrategyInfos = RenumberingStrategyInfoList.FetchByEntityId(_entityId);
                    }
                }
                return _renumberingStrategyInfos;
            }

            public ProductUniverseInfoList GetProductUniverseInfos()
            {
                if (_productUniverseInfos == null)
                {
                    lock (_productUniverseInfosLock)
                    {
                        _productUniverseInfos = ProductUniverseInfoList.FetchByEntityId(_entityId);
                    }
                }
                return _productUniverseInfos;
            }

            public PlanogramImportTemplate GetPlanogramImportTemplate(Object importTemplateId)
            {
                if (_planogramImportTemplatesById == null)
                {
                    lock (_planogramImportTemplatesByIdLock)
                    {
                        _planogramImportTemplatesById = new Dictionary<Object, PlanogramImportTemplate>();
                    }
                }

                PlanogramImportTemplate importTemplate;
                if (_planogramImportTemplatesById.TryGetValue(importTemplateId, out importTemplate))
                {
                    return importTemplate;
                }

                importTemplate = PlanogramImportTemplate.FetchById(importTemplateId);
                lock (_planogramImportTemplatesByIdLock)
                {
                    _planogramImportTemplatesById.Add(importTemplateId, importTemplate);
                }
                return importTemplate;
            }

            public PlanogramImportTemplateInfoList GetPlanogramImportTemplateInfos()
            {
                if (_planogramImportTemplateInfos == null)
                {
                    lock (_planogramImportTemplateInfosLock)
                    {
                        _planogramImportTemplateInfos = PlanogramImportTemplateInfoList.FetchByEntityId(_entityId);
                    }
                }
                return _planogramImportTemplateInfos;
            }
            
            public PlanogramNameTemplateInfoList GetPlanogramNameTemplateInfos()
            {
                if (_planogramNameTemplateInfos == null)
                {
                    lock (_planogramNameTemplateInfosLock)
                    {
                        _planogramNameTemplateInfos = PlanogramNameTemplateInfoList.FetchByEntityId(_entityId);
                    }
                }
                return _planogramNameTemplateInfos;
            }

            public PlanogramNameTemplate GetPlanogramNameTemplate(Int32 planogramNameTemplateId)
            {
                if (_planogramNameTemplatesById == null)
                {
                    lock (_planogramNameTemplatesByIdLock)
                    {
                        _planogramNameTemplatesById = new Dictionary<Int32, PlanogramNameTemplate>();
                    }
                }

                PlanogramNameTemplate nameTemplate;
                if (_planogramNameTemplatesById.TryGetValue(planogramNameTemplateId, out nameTemplate))
                {
                    return nameTemplate;
                }

                nameTemplate = PlanogramNameTemplate.FetchById(planogramNameTemplateId);
                lock (_planogramNameTemplatesByIdLock)
                {
                    _planogramNameTemplatesById.Add(planogramNameTemplateId, nameTemplate);
                }
                return nameTemplate;
            }

            public PerformanceSelectionInfoList GetPerformanceSelectionInfos()
            {
                if (_performanceSelectionInfos == null)
                {
                    lock (_performanceSelectionInfosLock)
                    {
                        _performanceSelectionInfos = PerformanceSelectionInfoList.FetchByEntityId(_entityId);
                    }
                }
                return _performanceSelectionInfos;
            }

            public MetricProfileInfoList GetMetricProfileInfos()
            {
                if (_metricProfileInfos == null)
                {
                    lock (_metricProfileInfosLock)
                    {
                        _metricProfileInfos = MetricProfileInfoList.FetchByEntityId(_entityId);
                    }
                }
                return _metricProfileInfos;
            }

            public LocationHierarchy GetLocationHierarchy()
            {
                if (_locationHierarchy == null)
                {
                    lock (_locationHierarchyLock)
                    {
                        _locationHierarchy = LocationHierarchy.FetchByEntityId(_entityId);
                    }
                }
                return _locationHierarchy;
            }

            public LocationInfoList GetLocationInfos()
            {
                if (_locationInfos == null)
                {
                    lock (_locationInfosLock)
                    {
                        _locationInfos = LocationInfoList.FetchByEntityId(_entityId);
                    }
                }
                return _locationInfos;
            }

            public PlanogramInfo GetPlanogramInfo(Int32 planogramId, IEnumerable<Int32> allPlanogramIds)
            {
                if (_planogramInfosById == null)
                {
                    lock (_planogramInfosByIdLock)
                    {
                        _planogramInfosById = new Dictionary<Int32, PlanogramInfo>();
                    }
                }

                if (allPlanogramIds != null && allPlanogramIds.Any())
                {
                    if (allPlanogramIds.Count() != _planogramInfosById.Count)
                    {
                        foreach (PlanogramInfo fetchedPlanogramInfo in PlanogramInfoList.FetchByIds(allPlanogramIds))
                        {
                            if (_planogramInfosById.ContainsKey(fetchedPlanogramInfo.Id)) continue;
                            _planogramInfosById.Add(fetchedPlanogramInfo.Id, fetchedPlanogramInfo);
                        }
                    }
                }

                PlanogramInfo planogramInfo;
                if (_planogramInfosById.TryGetValue(planogramId, out planogramInfo))
                {
                    return planogramInfo;
                }

                planogramInfo = PlanogramInfoList.FetchByIds(new[] { planogramId }).FirstOrDefault();
                lock (_planogramInfosByIdLock)
	            {
                    _planogramInfosById.Add(planogramId, planogramInfo);
	            }
                return planogramInfo;
            }

            public LocationPlanAssignmentList GetLocationPlanAssignments(Int32 planogramId)
            {
                if (_locationPlanAssignmentsByPlanogramId == null)
                {
                    lock (_locationPlanAssignmentsByPlanogramIdLock)
                    {
                        _locationPlanAssignmentsByPlanogramId = new Dictionary<Int32, LocationPlanAssignmentList>();
                    }
                }

                LocationPlanAssignmentList locationPlanAssignments;
                if (_locationPlanAssignmentsByPlanogramId.TryGetValue(planogramId, out locationPlanAssignments))
                {
                    return locationPlanAssignments;
                }

                locationPlanAssignments = LocationPlanAssignmentList.FetchByPlanogramId(planogramId);
                lock (_locationPlanAssignmentsByPlanogramIdLock)
                {
                    _locationPlanAssignmentsByPlanogramId.Add(planogramId, locationPlanAssignments);
                }
                return locationPlanAssignments;
            }

            public ProductGroupInfo GetProductGroupInfo(Int32 productGroupId)
            {
                if (_productGroupInfosById == null)
                {
                    lock (_productGroupInfosByIdLock)
                    {
                        _productGroupInfosById = new Dictionary<Int32, ProductGroupInfo>();
                    }
                }
                
                ProductGroupInfo productGroupInfo;
                if (_productGroupInfosById.TryGetValue(productGroupId, out productGroupInfo))
                {
                    return productGroupInfo;
                }

                productGroupInfo = ProductGroupInfo.FetchById(productGroupId);
                lock (_productGroupInfosByIdLock)
                {
                    _productGroupInfosById.Add(productGroupId, productGroupInfo);
                }
                return productGroupInfo;
            }

            public GFSModel.ProjectList GetProjectInfoByProductGroupCode(String productGroupCode)
            {
                if (_projectInfosByProductGroupCode == null)
                {
                    lock (_projectInfosByProductGroupCodeLock)
                    {
                        _projectInfosByProductGroupCode = new Dictionary<String, GFSModel.ProjectList>();
                    }
                }

                GFSModel.ProjectList projectList;
                if (_projectInfosByProductGroupCode.TryGetValue(productGroupCode, out projectList))
                {
                    return projectList;
                }

                GFSModel.Entity gfsEntity = GetGfsModelEntity();
                if (gfsEntity == null) return projectList;

                projectList = GFSModel.ProjectList.GetProjectInfoByEntityNameProductGroupCode(gfsEntity.Name, productGroupCode, gfsEntity);
                lock (_projectInfosByProductGroupCode)
                {
                    _projectInfosByProductGroupCode.Add(productGroupCode, projectList);
                }
                return projectList;
            }

            public GFSModel.ProjectList GetProjectInfos()
            {
                if (_projectInfos == null)
                {
                    lock (_projectInfosLock)
                    {
                        GFSModel.Entity gfsEntity = GetGfsModelEntity();
                        if (gfsEntity != null)
                        {
                            _projectInfos = GFSModel.ProjectList.FetchAll(gfsEntity);
                        }
                    }
                }
                return _projectInfos;
            }

            public GFSModel.ProjectList GetProjectInfosWithNoProductGroupAssigned()
            {
                if (_projectInfosWithNoProductGroupAssigned == null)
                {
                    lock (_projectInfosWithNoProductGroupAssignedLock)
                    {
                        GFSModel.Entity gfsEntity = GetGfsModelEntity();
                        if (gfsEntity != null)
                        {
                            _projectInfosWithNoProductGroupAssigned =
                                GFSModel.ProjectList.GetProjectInfosWithNoProductGroupAssignedByEntityName(gfsEntity);
                        }
                    }
                }
                return _projectInfosWithNoProductGroupAssigned;
            }

            public AssortmentInfoList GetAssortmentInfos()
            {
                if (_assortmentInfos == null)
                {
                    lock (_assortmentInfosLock)
                    {
                        _assortmentInfos = AssortmentInfoList.FetchByEntityId(_entityId);
                    }
                }
                return _assortmentInfos;
            }

            public BlockingInfoList GetBlockingInfos()
            {
                if (_blockingInfos == null)
                {
                    lock (_blockingInfosLock)
                    {
                        _blockingInfos = BlockingInfoList.FetchByEntityId(_entityId);
                    }
                }
                return _blockingInfos;
            }

            public ClusterSchemeInfoList GetClusterSchemeInfos()
            {
                if (_clusterSchemeInfos == null)
                {
                    lock (_clusterSchemeInfosLock)
                    {
                        _clusterSchemeInfos = ClusterSchemeInfoList.FetchByEntityId(_entityId);
                    }
                }
                return _clusterSchemeInfos;
            }

            public ConsumerDecisionTreeInfoList GetConsumerDecisionTreeInfos()
            {
                if (_consumerDecisionTreeInfos == null)
                {
                    lock (_consumerDecisionTreeInfosLock)
                    {
                        _consumerDecisionTreeInfos = ConsumerDecisionTreeInfoList.FetchByEntityId(_entityId);
                    }
                }
                return _consumerDecisionTreeInfos;
            }

            public Entity GetEntity()
            {
                if (_entity == null)
                {
                    lock (_entityLock)
                    {
                        _entity = Entity.FetchById(_entityId);
                    }
                }
                return _entity;
            }

            public GFSModel.Entity GetGfsModelEntity()
            {
                if (_gfsModelEntity == null)
                {
                    lock (_gfsModelEntityLock)
                    {
                        try
                        {
                            _gfsModelEntity = GFSModel.Entity.FetchByCcmEntity(GetEntity());
                        }
                        catch (Exception) { }
                    }
                }
                return _gfsModelEntity;
            } 

            public HighlightInfoList GetHighlightInfos()
            {
                if (_highlightInfos == null)
                {
                    lock (_highlightInfosLock)
                    {
                        _highlightInfos = HighlightInfoList.FetchByEntityId(_entityId);
                    }
                }
                return _highlightInfos;
            }

            public InventoryProfileInfoList GetInventoryProfileInfos()
            {
                if (_inventoryProfileInfos == null)
                {
                    lock (_inventoryProfilesinfosLock)
                    {
                        _inventoryProfileInfos = InventoryProfileInfoList.FetchByEntityId(_entityId);
                    }
                }
                return _inventoryProfileInfos;
            }
            
            public GFSModel.PerformanceSource GetPerformanceSourceById(Int32 gfsPerformanceSourceId)
            {
                if (_performanceSourceByPerformanceSourceId == null)
                {
                    lock (_performanceSourceByPerformanceSourceIdLock)
                    {
                        GFSModel.Entity gfsEntity = GetGfsModelEntity();
                        if (gfsEntity != null)
                        {
                           _performanceSourceByPerformanceSourceId = GFSModel.PerformanceSourceList.FetchAll(gfsEntity).ToDictionary(p => p.Id);
                        }
                    }
                }
                GFSModel.PerformanceSource performanceSource = null;
                if (_performanceSourceByPerformanceSourceId != null)
                {
                    _performanceSourceByPerformanceSourceId.TryGetValue(gfsPerformanceSourceId, out performanceSource);
                }
                

                return performanceSource;
            }

            public GFSModel.TimelineGroupList GetPerformanceSourceTimelineGroupsByPerformanceSelection(PerformanceSelectionInfo performanceSelectionInfo)
            {
                if (_timelineGroupsByGFSPerformanceSourceId == null)
                {
                    lock (_timelineGroupsByGFSPerformanceSourceIdLock)
                    {
                        _timelineGroupsByGFSPerformanceSourceId = new Dictionary<Int32, GFSModel.TimelineGroupList>();
                    }
                }

                GFSModel.TimelineGroupList timelineGroupList;
                if (_timelineGroupsByGFSPerformanceSourceId.TryGetValue(performanceSelectionInfo.GFSPerformanceSourceId, out timelineGroupList))
                {
                    return timelineGroupList;
                }

                //fetch gfs performance source
                GFSModel.PerformanceSource performanceSource = GetPerformanceSourceById(performanceSelectionInfo.GFSPerformanceSourceId);

                //If found
                if (performanceSource != null)
                {
                    //Fetch the timeline group
                    GFSModel.Entity gfsEntity = GetGfsModelEntity();
                    if (gfsEntity != null)
                    {
                        DateTime startDate = new DateTime();
                        DateTime endDate = DateTime.UtcNow;
                        timelineGroupList = GFSModel.TimelineGroupList.FetchByDateRange(gfsEntity, performanceSource, startDate, endDate);
                        lock (_timelineGroupsByGFSPerformanceSourceIdLock)
                        {
                            _timelineGroupsByGFSPerformanceSourceId.Add(performanceSelectionInfo.GFSPerformanceSourceId, timelineGroupList);
                        }
                    }
                }
                return timelineGroupList;
            }
            
            public PerformanceSelection GetPerformanceSelectionById(Int32 performanceSelectionId)
            {
                if (_performanceSelectionByPerformanceSelectionId == null)
                {
                    lock (_performanceSelectionByPerformanceSelectionIdLock)
                    {
                        _performanceSelectionByPerformanceSelectionId = new Dictionary<Int32, PerformanceSelection>();
                    }
                }

                PerformanceSelection performanceSource = null;
                _performanceSelectionByPerformanceSelectionId.TryGetValue(performanceSelectionId, out performanceSource);

                if (performanceSource == null)
                {
                    performanceSource = PerformanceSelection.FetchById(performanceSelectionId);
                    _performanceSelectionByPerformanceSelectionId.Add(performanceSelectionId, performanceSource);
                }

                return performanceSource;
            }

            public PlanogramComparisonTemplateInfoList GetPlanogramComparisonTemplateInfos()
            {
                if (_planogramComparisonTemplateInfos == null)
                {
                    lock (_planogramComparisonTemplateInfosLock)
                    {
                        _planogramComparisonTemplateInfos = PlanogramComparisonTemplateInfoList.FetchByEntityId(_entityId);
                    }
                }
                return _planogramComparisonTemplateInfos;
            }

            /// <summary>
            ///     Try getting the <see cref="PerformanceSelectionInfo"/> that matches the <paramref name="name"/>.
            /// </summary>
            /// <param name="name"></param>
            /// <param name="performanceSelectionInfo"></param>
            /// <returns><c>True</c> if a match was found, <c>false</c> otherwise.</returns>
            /// <remarks>If no matching <see cref="PerformanceSelectionInfo"/> is found for the <paramref name="name"/>, <c>null</c> will be returned.</remarks>
            public Boolean TryGetPerformanceSelectionInfo(String name, out PerformanceSelectionInfo performanceSelectionInfo)
            {
                PerformanceSelectionInfoList workflowSelectedPerformanceSelections = GetPerformanceSelectionInfos();
                performanceSelectionInfo =
                    workflowSelectedPerformanceSelections.FirstOrDefault(p => p.Name.Equals(name));
                return performanceSelectionInfo != null;
            }

            #endregion
        }
        #endregion
    
        #region Properties

        #region TaskParameterDataCache
        /// <summary>
        /// Holds a cache of data used by task parameter value helpers when selecting and validating data.
        /// </summary>
        public DataCache TaskParameterDataCache
        {
            get
            {
                if (_taskParameterDataCache == null)
                {
                    _taskParameterDataCache = new DataCache(this.EntityId);
                }
                return _taskParameterDataCache;
            }
        }
        #endregion

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique worflow id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        internal static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// Returns the row version
        /// </summary>
        internal RowVersion RowVersion
        {
            get { return this.GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region EntityId
        /// <summary>
        /// EntityId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        /// <summary>
        /// Returns the entity id
        /// </summary>
        public Int32 EntityId
        {
            get { return this.GetProperty<Int32>(EntityIdProperty); }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Gets or sets the workflow name
        /// </summary>
        public String Name
        {
            get { return this.GetProperty<String>(NameProperty); }
            set { this.SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region Description
        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);
        /// <summary>
        /// Gets or sets the workflow name
        /// </summary>
        public String Description
        {
            get { return this.GetProperty<String>(DescriptionProperty); }
            set { this.SetProperty<String>(DescriptionProperty, value); }
        }
        #endregion

        #region Tasks
        /// <summary>
        /// Tasks property definition
        /// </summary>
        public static readonly ModelPropertyInfo<WorkflowTaskList> TasksProperty =
            RegisterModelProperty<WorkflowTaskList>(c => c.Tasks);
        /// <summary>
        /// Returns all tasks within the workflow
        /// </summary>
        public WorkflowTaskList Tasks
        {
            get { return this.GetProperty<WorkflowTaskList>(TasksProperty); }
        }
        #endregion

        #region DateCreated
        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);
        /// <summary>
        /// Returns the date this workflow was created
        /// </summary>
        public DateTime DateCreated
        {
            get { return this.GetProperty<DateTime>(DateCreatedProperty); }
        }
        #endregion

        #region DateLastModified
        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        /// <summary>
        /// Returns the date this workflow was last modified
        /// </summary>
        public DateTime DateLastModified
        {
            get { return this.GetProperty<DateTime>(DateLastModifiedProperty);}
        }
        #endregion

        #region DateDeleted
        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);
        /// <summary>
        /// Returns the date this workflow was deleted
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return this.GetProperty<DateTime?>(DateDeletedProperty); }
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(Workflow), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(Workflow), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.WorkflowCreate.ToString()));
            BusinessRules.AddRule(typeof(Workflow), new IsInRole(AuthorizationActions.EditObject, DomainPermission.WorkflowEdit.ToString()));
            BusinessRules.AddRule(typeof(Workflow), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.WorkflowDelete.ToString()));
        }
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));

            BusinessRules.AddRule(new MaxLength(DescriptionProperty, 255));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static Workflow NewWorkflow(EntityInfo entity)
        {
            return NewWorkflow(entity.Id);
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static Workflow NewWorkflow(Entity entity)
        {
            return NewWorkflow(entity.Id);
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static Workflow NewWorkflow(Int32 entityId)
        {
            return NewWorkflow(entityId, Message.Workflow_DefaultName, null);
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static Workflow NewWorkflow(Int32 entityId, String name, String description, params String[] taskTypes)
        {
            Workflow item = new Workflow();
            item.Create(entityId, name, description, taskTypes);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected void Create(Int32 entityId, String name, String description, String[] taskTypes)
        {
            // set the workflow properties
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.LoadProperty<String>(NameProperty, name);
            this.LoadProperty<String>(DescriptionProperty, description);
            this.LoadProperty<WorkflowTaskList>(TasksProperty, WorkflowTaskList.NewWorkflowTaskList());

            // add tasks to this workflow if specified
            if (taskTypes != null)
            {
                EngineTaskInfoList registeredTasks = EngineTaskInfoList.FetchAll();
                foreach (String taskType in taskTypes)
                {
                    EngineTaskInfo task = registeredTasks.FirstOrDefault(t => t.TaskType == taskType);
                    if (task != null)
                    {
                        this.Tasks.Add(task);
                    }
                }
            }

            // call base create
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// ToString override
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }

        #endregion
    }
}
