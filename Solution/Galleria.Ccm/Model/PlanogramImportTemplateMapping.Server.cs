﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-27804 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramImportTemplateMapping
    {
        #region Constructor
        private PlanogramImportTemplateMapping() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static PlanogramImportTemplateMapping Fetch(IDalContext dalContext, PlanogramImportTemplateMappingDto dto)
        {
            return DataPortal.FetchChild<PlanogramImportTemplateMapping>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramImportTemplateMappingDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<PlanogramFieldMappingType>(FieldTypeProperty, (PlanogramFieldMappingType)dto.FieldType);
            this.LoadProperty<String>(FieldProperty, dto.Field);
            this.LoadProperty<String>(ExternalFieldProperty, dto.ExternalField);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private PlanogramImportTemplateMappingDto GetDataTransferObject(PlanogramImportTemplate parent)
        {
            return new PlanogramImportTemplateMappingDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                PlanogramImportTemplateId = parent.Id,
                FieldType = (Byte)ReadProperty<PlanogramFieldMappingType>(FieldTypeProperty),
                Field = ReadProperty<String>(FieldProperty),
                ExternalField = ReadProperty<String>(ExternalFieldProperty)
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, PlanogramImportTemplateMappingDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, PlanogramImportTemplate parent)
        {
            PlanogramImportTemplateMappingDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IPlanogramImportTemplateMappingDal dal = dalContext.GetDal<IPlanogramImportTemplateMappingDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<PlanogramImportTemplateMapping>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, PlanogramImportTemplate parent)
        {
            if (this.IsSelfDirty)
            {
                using (IPlanogramImportTemplateMappingDal dal = dalContext.GetDal<IPlanogramImportTemplateMappingDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, PlanogramImportTemplate parent)
        {
            using (IPlanogramImportTemplateMappingDal dal = dalContext.GetDal<IPlanogramImportTemplateMappingDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion
    }
}
