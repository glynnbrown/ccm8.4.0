﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentLocationList
    {
        #region Constructor
        private AssortmentLocationList() { } // Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a list of existing items based on their parent assortment
        /// </summary>
        /// <returns>A list of existing it</returns>
        internal static AssortmentLocationList GetAssortmentLocationList(IDalContext dalContext, Int32 assortmentId)
        {
            return DataPortal.FetchChild<AssortmentLocationList>(dalContext, assortmentId);
        }

        /// <summary>
        /// Returns all location items for the given assortment
        /// </summary>
        /// <returns>A list of assortment locations</returns>
        public static AssortmentLocationList FetchByAssortmentId(Int32 assortmentId)
        {
            return DataPortal.Fetch<AssortmentLocationList>(assortmentId);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="assortmentId">The parent assortment id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 assortmentId)
        {
            RaiseListChangedEvents = false;
            using (IAssortmentLocationDal dal = dalContext.GetDal<IAssortmentLocationDal>())
            {
                IEnumerable<AssortmentLocationDto> dtoList = dal.FetchByAssortmentId(assortmentId);
                foreach (AssortmentLocationDto dto in dtoList)
                {
                    this.Add(AssortmentLocation.GetAssortmentLocation(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when retrieving a list of all locations for an assortment
        /// </summary>
        private void DataPortal_Fetch(Int32 assortmentId)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IAssortmentLocationDal dal = dalContext.GetDal<IAssortmentLocationDal>())
                {
                    IEnumerable<AssortmentLocationDto> dtoList = dal.FetchByAssortmentId(assortmentId);
                    foreach (AssortmentLocationDto dto in dtoList)
                    {
                        this.Add(AssortmentLocation.GetAssortmentLocation(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
