﻿#region HeaderInformation 
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31699 : A.heathcote
//  Created.
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;


namespace Galleria.Ccm.Model
{
    public partial class UserEditorSettingsRecentLabel
    {
        #region Constructor
        private UserEditorSettingsRecentLabel() { }   //force use of factory methods
        #endregion

        #region Factory Methods

        internal static UserEditorSettingsRecentLabel GetUserEditorSettingsRecentLabel(IDalContext dalContext, UserEditorSettingsRecentLabelDto dto)
        {
            return DataPortal.FetchChild<UserEditorSettingsRecentLabel>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object

        private UserEditorSettingsRecentLabelDto GetDataTransferObjects()
        {
            return new UserEditorSettingsRecentLabelDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                Name = ReadProperty<String>(NameProperty),
                LabelType = (Byte)ReadProperty<LabelType>(LabelTypeProperty),
                SaveType = (Byte)ReadProperty<LabelDalFactoryType>(SaveTypeProperty),
                LabelId = ReadProperty<Object>(LabelIdProperty)
            };
        }

        private void LoadDataTransferObject(IDalContext dalcontext, UserEditorSettingsRecentLabelDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<LabelType>(LabelTypeProperty, (LabelType)dto.LabelType);
            LoadProperty<LabelDalFactoryType>(SaveTypeProperty, (LabelDalFactoryType)dto.SaveType);
            LoadProperty<Object>(LabelIdProperty, dto.LabelId);

        }
        #endregion 

        #region Fetch 
        
        private void Child_Fetch(IDalContext dalcontext, UserEditorSettingsRecentLabelDto dto)
        {
            LoadDataTransferObject(dalcontext, dto);
        }

        #endregion

        #region Insert 

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalcontext)
        {
            UserEditorSettingsRecentLabelDto dto = GetDataTransferObjects();
            Int32 oldId = dto.Id;
            using (IUserEditorSettingsRecentLabelDal dal = dalcontext.GetDal<IUserEditorSettingsRecentLabelDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalcontext.RegisterId<UserEditorSettingsRecentLabel>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalcontext, this);
        }

           #region Update
        /// <summary>
        /// Called when updating this object in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            if (this.IsSelfDirty)
            {
                using (IUserEditorSettingsRecentLabelDal dal = dalContext.GetDal<IUserEditorSettingsRecentLabelDal>())
                {
                    dal.Update(this.GetDataTransferObjects());
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IUserEditorSettingsRecentLabelDal dal = dalContext.GetDal<IUserEditorSettingsRecentLabelDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion
    }
        #endregion    
}
