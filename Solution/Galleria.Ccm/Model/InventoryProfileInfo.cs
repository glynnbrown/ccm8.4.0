﻿#region Header Information

#region Version History:(CCM800)
//CCM-26124 : I.George
// Initial Version
#endregion
#region Version History:(CCM820)
//CCM-30836 : J.Pickup
// Introduced IPtype.
#endregion

#endregion
using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// An info for an Inventory Profile
    /// </summary>
    [Serializable]
    public sealed partial class InventoryProfileInfo : ModelReadOnlyObject<InventoryProfileInfo>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        #endregion

        #region InventoryProfileType

        /// <summary>
        /// Inventory Profile Type property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramInventoryMetricType> InventoryProfileTypeProperty =
            RegisterModelProperty<PlanogramInventoryMetricType>(c => c.InventoryProfileType);

        /// <summary>
        /// Gets/Sets the Inventory Profile Type  value
        /// </summary>
        public PlanogramInventoryMetricType InventoryProfileType
        {
            get { return GetProperty<PlanogramInventoryMetricType>(InventoryProfileTypeProperty); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(InventoryProfileInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(InventoryProfileInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(InventoryProfileInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(InventoryProfileInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Overides
        /// <summary>
        /// ToString override
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }
        #endregion
    }
}
