﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-25628 : A.Kuszyk
//	Created (Auto-generated)
// CCM-25445 : L.Ineson
//  Added FetchByLocationGroupId
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of Location objects.
    /// </summary>
    [Serializable]
    public sealed partial class LocationList : ModelList<LocationList, Location>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.LocationGet.ToString()));
            BusinessRules.AddRule(typeof(LocationList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationCreate.ToString()));
            BusinessRules.AddRule(typeof(LocationList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static LocationList NewLocationList()
        {
            LocationList item = new LocationList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for LocationList.FetchByEntityId
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : CriteriaBase<FetchByEntityIdCriteria>
        {
            #region Properties

            public static PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            #endregion

            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
            }
        }

        /// <summary>
        /// Criteria for LocationList.FetchByLocationGroupId
        /// </summary>
        [Serializable]
        public class FetchByLocationGroupIdCriteria : CriteriaBase<FetchByLocationGroupIdCriteria>
        {
            #region Properties

            public static PropertyInfo<Int32> LocationGroupIdProperty =
                RegisterProperty<Int32>(c => c.LocationGroupId);
            public Int32 LocationGroupId
            {
                get { return ReadProperty<Int32>(LocationGroupIdProperty); }
            }

            #endregion

            public FetchByLocationGroupIdCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(LocationGroupIdProperty, entityId);
            }
        }


        #endregion
    }
}