﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
// V8-26523 : L.Ineson
//  If the product id is 0 then this no longer attempts to fetch
// as the product is obviously new.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class ProductImageList
    {
        #region Constructor
        private ProductImageList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        internal static ProductImageList GetListByProductId(Int32 productId)
        {
            return DataPortal.Fetch<ProductImageList>(new FetchByProductIdCriteria(productId));
        }

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        internal static ProductImageList GetListByProductIdImageType(Int32 productId, ProductImageType imageType)
        {
            return DataPortal.Fetch<ProductImageList>(new FetchByProductIdImageTypeCriteria(productId, imageType));
        }

        /// <summary>
        /// Returns a list of existing items based on their parent id
        /// </summary>
        public static ProductImageList GetListByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<ProductImageList>(new SingleCriteria<Int32>(entityId));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        private void DataPortal_Fetch(FetchByProductIdImageTypeCriteria criteria)
        {
            //[V8-26523] don't fetch for new products.
            if (criteria.ProductId == 0) return;

            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductImageDal dal = dalContext.GetDal<IProductImageDal>())
                {
                    IEnumerable<ProductImageDto> dtoList = dal.FetchByProductIdImageType(criteria.ProductId, (Byte)criteria.ImageType);
                    foreach (ProductImageDto dto in dtoList)
                    {
                        this.Add(ProductImage.GetProductImage(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        private void DataPortal_Fetch(FetchByProductIdCriteria criteria)
        {
            //[V8-26523] don't fetch for new products.
            if (criteria.ProductId == 0) return;

            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductImageDal dal = dalContext.GetDal<IProductImageDal>())
                {
                    IEnumerable<ProductImageDto> dtoList = dal.FetchByProductId(criteria.ProductId);
                    foreach (ProductImageDto dto in dtoList)
                    {
                        this.Add(ProductImage.GetProductImage(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;

        }

        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductImageDal dal = dalContext.GetDal<IProductImageDal>())
                {
                    IEnumerable<ProductImageDto> dtoList = dal.FetchByEntityId(criteria.Value);
                    foreach (ProductImageDto dto in dtoList)
                    {
                        this.Add(ProductImage.GetProductImage(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #region Update

        /// <summary>
        /// Called when returning updating a list
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        private new void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                Child_Update(dalContext);
                dalContext.Commit();
            }
        }

        #endregion

        #endregion
    }
}
