﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GFS 1.0)
// CCM-25445 : L.Ineson
//		Copied from GFS 
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public partial class LocationGroupLocationList
    {
        #region Constructor
        private LocationGroupLocationList() { } // force use of factory methods
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returngin all products for a planogram
        /// </summary>
        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = this.GetDalFactory(criteria.DalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationGroupLocationDal dal = dalContext.GetDal<ILocationGroupLocationDal>())
                {
                    IEnumerable<LocationGroupLocationDto> dtoList = dal.FetchByLocationGroupId((Int32)criteria.ParentId);
                    foreach (LocationGroupLocationDto dto in dtoList)
                    {
                        this.Add(LocationGroupLocation.Fetch(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
            this.MarkAsChild();
        }
        #endregion

        #endregion
    }
}
