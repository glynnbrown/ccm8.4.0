﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
// V8-27630 : I.George
// Removed Unused Factory methods
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class LocationProductAttributeInfoList
    {
        #region Constructor
        private LocationProductAttributeInfoList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        public static LocationProductAttributeInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<LocationProductAttributeInfoList>(new FetchByEntityIdCriteria(entityId));
        }

        public static LocationProductAttributeInfoList FetchByEntityIdSearchCriteria(Int32 entityId, String searchCriteria)
        {
            return DataPortal.Fetch<LocationProductAttributeInfoList>(new FetchByEntityIdSearchCriteriaCriteria(entityId, searchCriteria));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returning all library types
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationProductAttributeInfoDal dal = dalContext.GetDal<ILocationProductAttributeInfoDal>())
                {
                    IEnumerable<LocationProductAttributeInfoDto> dtoList = dal.FetchByEntityId(criteria.EntityId);
                    foreach (LocationProductAttributeInfoDto dto in dtoList)
                    {
                        this.Add(LocationProductAttributeInfo.GetLocationProductAttributeInfo(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        private void DataPortal_Fetch(FetchByEntityIdSearchCriteriaCriteria criteria)
        {
            if (criteria.SearchCriteria != null)
            {
                //only perform the search if we have over 2 valid characters.           
                if (criteria.SearchCriteria.Where(c => Char.IsLetterOrDigit(c)).Count() > 2)
                {
                    String searchString = criteria.SearchCriteria.Replace("&", "and");
                    searchString = searchString.Replace("|", "or");

                    this.RaiseListChangedEvents = false;
                    this.IsReadOnly = false;

                    IDalFactory dalFactory = DalContainer.GetDalFactory();
                    using (IDalContext dalContext = dalFactory.CreateContext())
                    {
                        using (ILocationProductAttributeInfoDal dal = dalContext.GetDal<ILocationProductAttributeInfoDal>())
                        {
                            IEnumerable<LocationProductAttributeInfoDto> dtoList = dal.FetchByEntityIdSearchCriteria(criteria.EntityId, searchString);

                            foreach (LocationProductAttributeInfoDto dto in dtoList)
                            {
                                this.Add(LocationProductAttributeInfo.GetLocationProductAttributeInfo(dalContext, dto));
                            }
                        }
                    }

                    this.IsReadOnly = true;
                    this.RaiseListChangedEvents = true;
                }
            }
        }

        #endregion

        #endregion

    }
}
