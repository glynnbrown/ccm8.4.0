﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
#endregion

#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentLocationBuddy
    {
        #region Constructor
        private AssortmentLocationBuddy() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating an instance from a dto
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new instance of this type</returns>
        internal static AssortmentLocationBuddy GetAssortmentLocationBuddy(IDalContext dalContext, AssortmentLocationBuddyDto dto)
        {
            return DataPortal.FetchChild<AssortmentLocationBuddy>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, AssortmentLocationBuddyDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Int16>(LocationIdProperty, dto.LocationId);
            LoadProperty<String>(LocationCodeProperty, dto.LocationCode);
            LoadProperty<PlanogramAssortmentLocationBuddyTreatmentType>(TreatmentTypeProperty, (PlanogramAssortmentLocationBuddyTreatmentType)dto.TreatmentType);
            LoadProperty<Int16?>(S1LocationIdProperty, dto.S1LocationId);
            LoadProperty<String>(S1LocationCodeProperty, dto.S1LocationCode);
            LoadProperty<Single?>(S1PercentageProperty, dto.S1Percentage);
            LoadProperty<Int16?>(S2LocationIdProperty, dto.S2LocationId);
            LoadProperty<String>(S2LocationCodeProperty, dto.S2LocationCode);
            LoadProperty<Single?>(S2PercentageProperty, dto.S2Percentage);
            LoadProperty<Int16?>(S3LocationIdProperty, dto.S3LocationId);
            LoadProperty<String>(S3LocationCodeProperty, dto.S3LocationCode);
            LoadProperty<Single?>(S3PercentageProperty, dto.S3Percentage);
            LoadProperty<Int16?>(S4LocationIdProperty, dto.S4LocationId);
            LoadProperty<String>(S4LocationCodeProperty, dto.S4LocationCode);
            LoadProperty<Single?>(S4PercentageProperty, dto.S4Percentage);
            LoadProperty<Int16?>(S5LocationIdProperty, dto.S5LocationId);
            LoadProperty<String>(S5LocationCodeProperty, dto.S5LocationCode);
            LoadProperty<Single?>(S5PercentageProperty, dto.S5Percentage);
        }

        /// <summary>
        /// Returns a dto created from this instance
        /// </summary>
        /// <param name="parent">The Assortment</param>
        /// <returns>A data transfer object</returns>
        private AssortmentLocationBuddyDto GetDataTransferObject(Assortment parent)
        {
            return new AssortmentLocationBuddyDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                AssortmentId = parent.Id,
                TreatmentType = (Byte)ReadProperty<PlanogramAssortmentLocationBuddyTreatmentType>(TreatmentTypeProperty),
                LocationId = ReadProperty<Int16>(LocationIdProperty),
                LocationCode = ReadProperty<String>(LocationCodeProperty),
                S1LocationId = ReadProperty<Int16?>(S1LocationIdProperty),
                S1LocationCode = ReadProperty<String>(S1LocationCodeProperty),
                S1Percentage = ReadProperty<Single?>(S1PercentageProperty),
                S2LocationId = ReadProperty<Int16?>(S2LocationIdProperty),
                S2LocationCode = ReadProperty<String>(S2LocationCodeProperty),
                S2Percentage = ReadProperty<Single?>(S2PercentageProperty),
                S3LocationId = ReadProperty<Int16?>(S3LocationIdProperty),
                S3LocationCode = ReadProperty<String>(S3LocationCodeProperty),
                S3Percentage = ReadProperty<Single?>(S3PercentageProperty),
                S4LocationId = ReadProperty<Int16?>(S4LocationIdProperty),
                S4LocationCode = ReadProperty<String>(S4LocationCodeProperty),
                S4Percentage = ReadProperty<Single?>(S4PercentageProperty),
                S5LocationId = ReadProperty<Int16?>(S5LocationIdProperty),
                S5LocationCode = ReadProperty<String>(S5LocationCodeProperty),
                S5Percentage = ReadProperty<Single?>(S5PercentageProperty),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when this instance is being loaded
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, AssortmentLocationBuddyDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when this instance is being inserted into the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent assortment</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, Assortment parent)
        {
            AssortmentLocationBuddyDto dto = GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IAssortmentLocationBuddyDal dal = dalContext.GetDal<IAssortmentLocationBuddyDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<AssortmentLocationBuddy>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when this instance is being updated in the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent assortment</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, Assortment parent)
        {
            if (this.IsSelfDirty)
            {
                using (IAssortmentLocationBuddyDal dal = dalContext.GetDal<IAssortmentLocationBuddyDal>())
                {
                    dal.Update(GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this instance is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        private void Child_DeleteSelf(IDalContext dalContext, Assortment parent)
        {
            using (IAssortmentLocationBuddyDal dal = dalContext.GetDal<IAssortmentLocationBuddyDal>())
            {
                dal.DeleteById(this.Id);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #endregion
    }
}
