﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26159 : L.Ineson
//	Created (Auto-generated)
// V8-27763 : N.Foster
//  Added FetchByName, GetTimelines
// V8-26953 : I.George
//  Added PerformanceSelectionMetricList
// V8-27035 : L.Ineson
//  Added GetAggregationTypes
#endregion
#region Version History: CCM830
// V8-32133 : A.Probyn
//  Updated so that timeline related code can be reused.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Enums;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// PerformanceSelection model object
    /// </summary>
    [Serializable]
    public sealed partial class PerformanceSelection : ModelObject<PerformanceSelection>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The row version timestamp
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region EntityId

        /// <summary>
        /// EntityId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);

        /// <summary>
        /// Gets/Sets the EntityId value
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
            set { SetProperty<Int32>(EntityIdProperty, value); }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region GFSPerformanceSourceId

        /// <summary>
        /// GFSPerformanceSourceId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> GFSPerformanceSourceIdProperty =
            RegisterModelProperty<Int32>(c => c.GFSPerformanceSourceId);

        /// <summary>
        /// Gets/Sets the GFSPerformanceSourceId value
        /// </summary>
        public Int32 GFSPerformanceSourceId
        {
            get { return GetProperty<Int32>(GFSPerformanceSourceIdProperty); }
            set { SetProperty<Int32>(GFSPerformanceSourceIdProperty, value); }
        }

        #endregion

        #region TimeType

        /// <summary>
        /// TimeType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PerformanceSelectionTimeType> TimeTypeProperty =
            RegisterModelProperty<PerformanceSelectionTimeType>(c => c.TimeType);

        /// <summary>
        /// Gets/Sets the TimeType value
        /// </summary>
        public PerformanceSelectionTimeType TimeType
        {
            get { return GetProperty<PerformanceSelectionTimeType>(TimeTypeProperty); }
            set { SetProperty<PerformanceSelectionTimeType>(TimeTypeProperty, value); }
        }

        #endregion

        #region SelectionType

        /// <summary>
        /// SelectionType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PerformanceSelectionType> SelectionTypeProperty =
            RegisterModelProperty<PerformanceSelectionType>(c => c.SelectionType);

        /// <summary>
        /// Gets/Sets the SelectionType value
        /// </summary>
        public PerformanceSelectionType SelectionType
        {
            get { return GetProperty<PerformanceSelectionType>(SelectionTypeProperty); }
            set { SetProperty<PerformanceSelectionType>(SelectionTypeProperty, value); }
        }

        #endregion

        #region DynamicValue

        /// <summary>
        /// DynamicValue property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> DynamicValueProperty =
            RegisterModelProperty<Int32>(c => c.DynamicValue);

        /// <summary>
        /// Gets/Sets the DynamicValue value
        /// </summary>
        public Int32 DynamicValue
        {
            get { return GetProperty<Int32>(DynamicValueProperty); }
            set { SetProperty<Int32>(DynamicValueProperty, value); }
        }

        #endregion

        #region DateCreated

        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        /// Gets/Sets the DateCreated value
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
            set { SetProperty<DateTime>(DateCreatedProperty, value); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        /// Gets/Sets the DateLastModified value
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
            set { SetProperty<DateTime>(DateLastModifiedProperty, value); }
        }

        #endregion

        #region DateDeleted

        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);

        /// <summary>
        /// Gets/Sets the DateDeleted value
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
            set { SetProperty<DateTime?>(DateDeletedProperty, value); }
        }

        #endregion

        #region TimelineGroups

        /// <summary>
        /// TimelineGroups property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PerformanceSelectionTimelineGroupList> TimelineGroupsProperty =
            RegisterModelProperty<PerformanceSelectionTimelineGroupList>(c => c.TimelineGroups);

        /// <summary>
        /// Returns the list of timeline groups.
        /// If fixed selection type, these are the actual selected groups.
        /// If dynamic, these are the exclusion groups.
        /// </summary>
        public PerformanceSelectionTimelineGroupList TimelineGroups
        {
            get { return GetProperty<PerformanceSelectionTimelineGroupList>(TimelineGroupsProperty); }
        }

        #endregion

        #region Metrics
        /// <summary>
        /// Metrics Property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PerformanceSelectionMetricList> MetricsProperty =
            RegisterModelProperty<PerformanceSelectionMetricList>(c => c.Metrics);

        /// <summary>
        /// Returns the list of Metrics
        /// </summary>
        public PerformanceSelectionMetricList Metrics
        {
            get { return GetProperty<PerformanceSelectionMetricList>(MetricsProperty); }
        }
        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new MinValue<Int32>(EntityIdProperty, 1));

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));

        }

        #endregion

        #region Authorization Rules

        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PerformanceSelection), new IsInRole(AuthorizationActions.GetObject, DomainPermission.PerformanceSelectionGet.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelection), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.PerformanceSelectionCreate.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelection), new IsInRole(AuthorizationActions.EditObject, DomainPermission.PerformanceSelectionEdit.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelection), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.PerformanceSelectionDelete.ToString()));
        }

        #endregion

        #region Criteria

        #region FetchByIdCriteria

        // <summary>
        /// Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> IdProperty =
                RegisterProperty<Int32>(c => c.Id);
            /// <summary>
            /// Returns the id
            /// </summary>
            public Int32 Id
            {
                get { return this.ReadProperty<Int32>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(Int32 id)
            {
                this.LoadProperty<Int32>(IdProperty, id);
            }
            #endregion
        }

        #endregion

        #region FetchByEntityIdNameCriteria

        /// <summary>
        /// Criteria for the FetchByEntityIdName factory method
        /// </summary>
        [Serializable]
        public class FetchByEntityIdNameCriteria : CriteriaBase<FetchByEntityIdNameCriteria>
        {
            #region Properties

            #region EntityId
            /// <summary>
            /// EntityId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            /// <summary>
            /// Returns the entityId
            /// </summary>
            public Int32 EntityId
            {
                get { return this.ReadProperty<Int32>(EntityIdProperty); }
            }
            #endregion

            #region Name
            /// <summary>
            /// Name property definition
            /// </summary>
            public static readonly PropertyInfo<String> NameProperty =
                RegisterProperty<String>(c => c.Name);
            /// <summary>
            /// Returns the name
            /// </summary>
            public String Name
            {
                get { return this.ReadProperty<String>(NameProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByEntityIdNameCriteria(Int32 entityId, String name)
            {
                this.LoadProperty<Int32>(EntityIdProperty, entityId);
                this.LoadProperty<String>(NameProperty, name);
            }
            #endregion
        }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new PerformanceSelection
        /// </summary>
        public static PerformanceSelection NewPerformanceSelection(Int32 entityId)
        {
            PerformanceSelection item = new PerformanceSelection();
            item.Create(entityId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 entityId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            this.LoadProperty<PerformanceSelectionTimelineGroupList>(TimelineGroupsProperty, PerformanceSelectionTimelineGroupList.NewPerformanceSelectionTimelineGroupList());
            this.LoadProperty<PerformanceSelectionMetricList>(MetricsProperty, PerformanceSelectionMetricList.NewPerformanceSelectionMetricList());
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        #region Overrides
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<PerformanceSelection>(oldId, newId);
        }

        public override String ToString()
        {
            if (!String.IsNullOrEmpty(this.Name))
            {
                return this.Name;
            }
            return "<New Item>";
        }
        #endregion

        #region Get Performance Source
        /// <summary>
        /// Returns the gfs performance source
        /// associated to this performance selection
        /// </summary>
        public GFSModel.PerformanceSource GetPerformanceSource(Entity entity)
        {
            // get the entity that this performance selection relates to
            GFSModel.Entity gfsEntity = GFSModel.Entity.FetchByCcmEntity(entity);

            // return the performance source
            return this.GetPerformanceSource(gfsEntity);
        }

        /// <summary>
        /// Returns the gfs performance source
        /// associciated to this performance selection
        /// </summary>
        public GFSModel.PerformanceSource GetPerformanceSource(GFSModel.Entity entity)
        {
            // fetch all performance sources from GFS
            GFSModel.PerformanceSourceList performanceSources = GFSModel.PerformanceSourceList.FetchAll(entity);

            // locate the performance source that
            // we are actually interested in
            return performanceSources.FirstOrDefault(item => item.Id == this.GFSPerformanceSourceId);
        }

        #endregion

        #region Get Timeline Codes
        /// <summary>
        /// Returns a list of timelines covered by
        /// this performance selection
        /// </summary>
        public IEnumerable<Int64> GetTimelineCodes(GFSModel.Entity entity, GFSModel.PerformanceSource performanceSource)
        {
            switch (this.SelectionType)
            {
                case PerformanceSelectionType.Dynamic:
                    return GetTimelineCodesDynamic(entity, performanceSource);
                case PerformanceSelectionType.Fixed:
                    return GetTimelineCodesFixed();
                default:
                    throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Returns a list of timelines based on
        /// the fixed selection in the performance selection
        /// </summary>
        private IEnumerable<Int64> GetTimelineCodesFixed()
        {
            // this should be pretty straightforward,
            // just build a list of timeline codes
            // that already exist within this performance selection
            return this.TimelineGroups.Select(item => item.Code);
        }

        /// <summary>
        /// Returns a list of timelines based
        /// on the dynamic selectio nin the performance selection
        /// </summary>
        /// <param name="sourcePerformanceSource">Optional</param>
        public IEnumerable<Int64> GetTimelineCodesDynamic(GFSModel.Entity entity, 
            GFSModel.PerformanceSource performanceSource)
        {
            // the end date of the timelines is based
            // in the current date and time
            DateTime endDate = DateTime.UtcNow;

            // get the default calendar
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            Calendar calendar = cultureInfo.Calendar;

            // we now subtract a number of units
            // based on the timeline type and
            // the dynamic value entered by the user
            DateTime startDate;
            switch (this.TimeType)
            {
                case PerformanceSelectionTimeType.Day:
                    startDate = calendar.AddDays(endDate, - this.DynamicValue);
                    break;
                case PerformanceSelectionTimeType.Week:
                    startDate = calendar.AddWeeks(endDate, -this.DynamicValue);
                    break;
                case PerformanceSelectionTimeType.Month:
                    startDate = calendar.AddWeeks(endDate, -this.DynamicValue);
                    break;
                case PerformanceSelectionTimeType.Quarter:
                    startDate = calendar.AddWeeks(endDate, -this.DynamicValue);
                    break;
                case PerformanceSelectionTimeType.Year:
                    startDate = calendar.AddWeeks(endDate, -this.DynamicValue);
                    break;
                default:
                    throw new InvalidOperationException();
            }
                        
            // if the performance source could not be located
            // then throw an exception
            if (performanceSource == null) throw new InvalidOperationException();

            // now we have the performance source,
            // we can retrieve the timelines that
            // fall within the start and end dates
            GFSModel.TimelineGroupList timelineGroups = GFSModel.TimelineGroupList.FetchByDateRange(
                entity,
                performanceSource,
                startDate,
                endDate);

            // convert to a list of timeline codes
            List<Int64> timelineCodes = timelineGroups.Select(item => item.Code).ToList();

            // now remove any exclusions from this list
            foreach (PerformanceSelectionTimelineGroup timelineGroup in this.TimelineGroups)
            {
                timelineCodes.Remove(timelineGroup.Code);
            }

            // and return the timeline codes
            return timelineCodes;
        }
        #endregion

        #region Get Aggregation Types

        /// <summary>
        /// Returns a list of aggregation types based on the given gfs performance source.
        /// </summary>
        /// <param name="performanceSource"></param>
        /// <returns></returns>
        public IEnumerable<Services.Performance.PerformanceSourceMetricAggregationType> GetAggregationTypes(GFSModel.PerformanceSource performanceSource)
        {
            List<Services.Performance.PerformanceSourceMetricAggregationType> aggregationTypes = new List<Services.Performance.PerformanceSourceMetricAggregationType>();

            //only pass the standard (non-calculated) metrics to GFS through to the Performance Service requests. Calculated Metrics are evaluated later
            //once the raw data has been received from GFS
            //foreach (GFSModel.PerformanceSourceMetric sourceMetric in performanceSource.PerformanceSourceMetrics)
            foreach (GFSModel.PerformanceSourceMetric sourceMetric in performanceSource.PerformanceSourceMetrics.Where(p => p.IsCalculatedMetric == false))
            {
                String aggregationType = AggregationType.Sum.ToString();

                PerformanceSelectionMetric selectionMetric = 
                    this.Metrics.FirstOrDefault(m => m.GFSPerformanceSourceMetricId == sourceMetric.Id);
                if (selectionMetric != null)
                {
                    aggregationType = selectionMetric.AggregationType.ToString();
                }
                else
                {
                    aggregationType = sourceMetric.AggregationType.ToString();
                }

                aggregationTypes.Add(new Services.Performance.PerformanceSourceMetricAggregationType()
                {
                    PerformanceSourceMetricName = sourceMetric.MetricName,
                    AggregationType = aggregationType
                });
            }

            return aggregationTypes;
        }

        ///// <summary>
        ///// Returns the gfs aggregation type name for the given selection aggregation type.
        ///// </summary>
        //private String GetAggregationTypeName(PerformanceSelectionMetricAggregationType selectionAggregationType)
        //{
        //    switch (selectionAggregationType)
        //    {
        //        case PerformanceSelectionMetricAggregationType.Sum:
        //            return GFSModel.PerformanceSourceMetricAggregationType.Sum.ToString();

        //        case PerformanceSelectionMetricAggregationType.Avg:
        //            return GFSModel.PerformanceSourceMetricAggregationType.Average.ToString();

        //        case PerformanceSelectionMetricAggregationType.Count:
        //            return GFSModel.PerformanceSourceMetricAggregationType.Count.ToString();

        //        case PerformanceSelectionMetricAggregationType.Max:
        //            return GFSModel.PerformanceSourceMetricAggregationType.Max.ToString();

        //        case PerformanceSelectionMetricAggregationType.Min:
        //            return GFSModel.PerformanceSourceMetricAggregationType.Min.ToString();
                       
        //        default:
        //            Debug.Fail("Type not handled");
        //            return GFSModel.PerformanceSourceMetricAggregationType.Sum.ToString();
        //    }
        //}

        #endregion

        #endregion
    }
}