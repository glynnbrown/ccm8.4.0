﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a list containing ProductImage objects
    /// (Child of Product)
    /// </summary>
    [Serializable]
    public partial class ProductImageList : ModelList<ProductImageList,ProductImage>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ProductImageList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ProductCreate.ToString()));
            BusinessRules.AddRule(typeof(ProductImageList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ProductGet.ToString()));
            BusinessRules.AddRule(typeof(ProductImageList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ProductEdit.ToString()));
            BusinessRules.AddRule(typeof(ProductImageList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ProductDelete.ToString()));
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns the parent product 
        /// </summary>
        public Product ParentProduct
        {
            get { return base.Parent as Product; }
        }

        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        public static ProductImageList NewList()
        {
            ProductImageList item = new ProductImageList();
            item.MarkAsChild();
            item.Create();
            return item;
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByProductCriteria
        /// </summary>
        [Serializable]
        public class FetchByProductIdCriteria : Csla.CriteriaBase<FetchByProductIdCriteria>
        {
            public static readonly PropertyInfo<Int32> ProductIdProperty =
                RegisterProperty<Int32>(c => c.ProductId);
            public Int32 ProductId
            {
                get { return ReadProperty<Int32>(ProductIdProperty); }
            }

            public FetchByProductIdCriteria(Int32 productId)
            {
                LoadProperty<Int32>(ProductIdProperty, productId);
            }
        }

        /// <summary>
        /// Criteria for FetchByProductIdImageTypeCriteria
        /// </summary>
        [Serializable]
        public class FetchByProductIdImageTypeCriteria : Csla.CriteriaBase<FetchByProductIdImageTypeCriteria>
        {
            public static readonly PropertyInfo<Int32> ProductIdProperty =
                RegisterProperty<Int32>(c => c.ProductId);
            public Int32 ProductId
            {
                get { return ReadProperty<Int32>(ProductIdProperty); }
            }

            public static readonly PropertyInfo<ProductImageType> ImageTypeProperty =
                RegisterProperty<ProductImageType>(c => c.ImageType);
            public ProductImageType ImageType
            {
                get { return ReadProperty<ProductImageType>(ImageTypeProperty); }
            }

            public FetchByProductIdImageTypeCriteria(Int32 productId, ProductImageType imageType)
            {
                LoadProperty<Int32>(ProductIdProperty, productId);
                LoadProperty<ProductImageType>(ImageTypeProperty, imageType);
            }
        }

        #endregion
    }
}
