﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
#endregion
#region Version History: (CCM CCM830)
// V8-32524 : L.Ineson
//  Added FixtureAnnotations and simplified updated back to plan
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of FixtureComponentItem objects.
    /// </summary>
    [Serializable]
    public sealed partial class FixtureComponentItemList : ModelList<FixtureComponentItemList, FixtureComponentItem>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Fixture Parent
        {
            get { return (Fixture)base.Parent; }
        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static FixtureComponentItemList NewFixtureComponentItemList()
        {
            FixtureComponentItemList item = new FixtureComponentItemList();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        internal static FixtureComponentItemList NewFixtureComponentItemList(IEnumerable<PlanogramFixtureComponent> planFixtureComponents)
        {
            FixtureComponentItemList item = new FixtureComponentItemList();
            item.Create(planFixtureComponents);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(IEnumerable<PlanogramFixtureComponent> planFixtureComponents)
        {
            this.RaiseListChangedEvents = false;
            foreach (PlanogramFixtureComponent planFixtureComponent in planFixtureComponents)
            {
                Add(FixtureComponentItem.NewFixtureComponentItem(planFixtureComponent));
            }
            this.RaiseListChangedEvents = true;

            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}