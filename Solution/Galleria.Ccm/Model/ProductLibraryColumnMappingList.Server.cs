﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25395 : A.Probyn
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public sealed partial class ProductLibraryColumnMappingList
    {
        #region Constructor
        private ProductLibraryColumnMappingList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Fetches the list by its parent id
        /// </summary>
        internal static ProductLibraryColumnMappingList FetchByParentId(IDalContext dalContext, Object parentId)
        {
            return DataPortal.FetchChild<ProductLibraryColumnMappingList>(dalContext, parentId);
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returing all items for a given parent id
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Object parentId)
        {
            RaiseListChangedEvents = false;
            using (IProductLibraryColumnMappingDal dal = dalContext.GetDal<IProductLibraryColumnMappingDal>())
            {
                IEnumerable<ProductLibraryColumnMappingDto> dtoList = dal.FetchByProductLibraryId(parentId);
                foreach (ProductLibraryColumnMappingDto dto in dtoList)
                {
                    this.Add(ProductLibraryColumnMapping.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}