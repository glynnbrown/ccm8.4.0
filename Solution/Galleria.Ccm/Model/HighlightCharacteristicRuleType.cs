﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.01)
// V8-28181 : L.Luong
//  Added Contains
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Model
{
    public enum HighlightCharacteristicRuleType
    {
        Equals,
        NotEquals,
        LessThan,
        LessThanOrEqualTo,
        MoreThan,
        MoreThanOrEqualTo,
        Contains,
    }

    public static class HighlightCharacteristicRuleTypeHelper
    {
        public static readonly Dictionary<HighlightCharacteristicRuleType, String> FriendlyNames =
            new Dictionary<HighlightCharacteristicRuleType, String>()
            {
                {HighlightCharacteristicRuleType.Equals, "=" },
                {HighlightCharacteristicRuleType.NotEquals, "<>" },
                {HighlightCharacteristicRuleType.LessThan, "<" },
                {HighlightCharacteristicRuleType.LessThanOrEqualTo, "<=" },
                {HighlightCharacteristicRuleType.MoreThan, ">" },
                {HighlightCharacteristicRuleType.MoreThanOrEqualTo, ">=" },
                {HighlightCharacteristicRuleType.Contains, Message.HighlightType_Contains },
            };

        public static PlanogramHighlightCharacteristicRuleType ToPlanogramHighlightCharacteristicRuleType(this HighlightCharacteristicRuleType value)
        {
            switch (value)
            {
                default:
                case HighlightCharacteristicRuleType.Equals: return PlanogramHighlightCharacteristicRuleType.Equals;
                case HighlightCharacteristicRuleType.LessThan: return PlanogramHighlightCharacteristicRuleType.LessThan;
                case HighlightCharacteristicRuleType.LessThanOrEqualTo: return PlanogramHighlightCharacteristicRuleType.LessThanOrEqualTo;
                case HighlightCharacteristicRuleType.MoreThan: return PlanogramHighlightCharacteristicRuleType.MoreThan;
                case HighlightCharacteristicRuleType.MoreThanOrEqualTo: return PlanogramHighlightCharacteristicRuleType.MoreThanOrEqualTo;
                case HighlightCharacteristicRuleType.NotEquals: return PlanogramHighlightCharacteristicRuleType.NotEquals;
            }
        }
    }
}
