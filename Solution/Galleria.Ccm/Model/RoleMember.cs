﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26234 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a role member within GFS
    /// </summary>
    [Serializable]
    public partial class RoleMember : ModelObject<RoleMember>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The unique id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The row version
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region Role Id
        /// <summary>
        /// RoleId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> RoleIdProperty =
            RegisterModelProperty<Int32>(c => c.RoleId);
        /// <summary>
        /// The role id
        /// </summary>
        public Int32 RoleId
        {
            get { return GetProperty<Int32>(RoleIdProperty); }
        }
        #endregion

        #region User Id
        /// <summary>
        /// UserId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> UserIdProperty =
            RegisterModelProperty<Int32>(c => c.UserId);
        /// <summary>
        /// The user id
        /// </summary>
        public Int32 UserId
        {
            get { return GetProperty<Int32>(UserIdProperty); }
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(RoleMember), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.RoleCreate.ToString()));
            BusinessRules.AddRule(typeof(RoleMember), new IsInRole(AuthorizationActions.GetObject, DomainPermission.RoleGet.ToString()));
            BusinessRules.AddRule(typeof(RoleMember), new IsInRole(AuthorizationActions.EditObject, DomainPermission.RoleEdit.ToString()));
            BusinessRules.AddRule(typeof(RoleMember), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.RoleDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="userId">The user id</param>
        /// <param name="roleId">The role id</param>
        /// <returns>The new role memeber</returns>
        public static RoleMember NewRoleMember(Int32 userId, Int32 roleId)
        {
            RoleMember item = new RoleMember();
            item.Create(userId, roleId);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating an instance of this type
        /// </summary>
        /// <param name="userId">The user id</param>
        /// <param name="roleId">The role id</param>
        private void Create(Int32 userId, Int32 roleId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(UserIdProperty, userId);
            this.LoadProperty<Int32>(RoleIdProperty, roleId);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}
