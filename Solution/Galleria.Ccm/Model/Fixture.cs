﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
// V8-25526 : K.Pickup
//  Use RelationshipTypes.LazyLoad when registering lazy-loaded model properties.
// V8-27058 : A.Probyn
//  Updated references to updated meta data properties on PlanogramFixture
// V8-26182 : L.Ineson
//  Added NewPlanogramFixture
#endregion
#region Version History: (CCM CCM830)
// V8-32524 : L.Ineson
//  Added FixtureAnnotations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Fixture Model object
    /// </summary>
    [Serializable]
    public sealed partial class Fixture : ModelObject<Fixture>
    {
        #region Static Constructor
        static Fixture()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new FixturePackage Parent
        {
            get
            {
                FixtureList parentList = base.Parent as FixtureList;
                if (parentList != null)
                {
                    return parentList.Parent;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name, Message.Generic_Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region Height

        /// <summary>
        /// Height property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HeightProperty =
            RegisterModelProperty<Single>(c => c.Height, Message.Fixture_Height);

        /// <summary>
        /// Gets/Sets the Height value
        /// </summary>
        public Single Height
        {
            get { return GetProperty<Single>(HeightProperty); }
            set { SetProperty<Single>(HeightProperty, value); }
        }

        #endregion

        #region Width

        /// <summary>
        /// Width property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> WidthProperty =
            RegisterModelProperty<Single>(c => c.Width, Message.Fixture_Width);

        /// <summary>
        /// Gets/Sets the Width value
        /// </summary>
        public Single Width
        {
            get { return GetProperty<Single>(WidthProperty); }
            set { SetProperty<Single>(WidthProperty, value); }
        }

        #endregion

        #region Depth

        /// <summary>
        /// Depth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DepthProperty =
            RegisterModelProperty<Single>(c => c.Depth, Message.Fixture_Depth);

        /// <summary>
        /// Gets/Sets the Depth value
        /// </summary>
        public Single Depth
        {
            get { return GetProperty<Single>(DepthProperty); }
            set { SetProperty<Single>(DepthProperty, value); }
        }

        #endregion

        #region NumberOfAssemblies

        /// <summary>
        /// NumberOfAssemblies property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> NumberOfAssembliesProperty =
            RegisterModelProperty<Int16>(c => c.NumberOfAssemblies, Message.Fixture_NumberOfAssemblies);

        /// <summary>
        /// Gets/Sets the NumberOfAssemblies value
        /// </summary>
        public Int16 NumberOfAssemblies
        {
            get { return GetProperty<Int16>(NumberOfAssembliesProperty); }
            set { SetProperty<Int16>(NumberOfAssembliesProperty, value); }
        }

        #endregion

        #region NumberOfMerchandisedSubComponents

        /// <summary>
        /// NumberOfMerchandisedSubComponents property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> NumberOfMerchandisedSubComponentsProperty =
            RegisterModelProperty<Int16>(c => c.NumberOfMerchandisedSubComponents, Message.Fixture_NumberOfMerchandisedSubComponents);

        /// <summary>
        /// Gets/Sets the NumberOfMerchandisedSubComponents value
        /// </summary>
        public Int16 NumberOfMerchandisedSubComponents
        {
            get { return GetProperty<Int16>(NumberOfMerchandisedSubComponentsProperty); }
            set { SetProperty<Int16>(NumberOfMerchandisedSubComponentsProperty, value); }
        }

        #endregion

        #region TotalFixtureCost

        /// <summary>
        /// TotalFixtureCost property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> TotalFixtureCostProperty =
            RegisterModelProperty<Single>(c => c.TotalFixtureCost, Message.Fixture_TotalFixtureCost);

        /// <summary>
        /// Gets/Sets the TotalFixtureCost value
        /// </summary>
        public Single TotalFixtureCost
        {
            get { return GetProperty<Single>(TotalFixtureCostProperty); }
            set { SetProperty<Single>(TotalFixtureCostProperty, value); }
        }

        #endregion

        #region Assemblies
        /// <summary>
        /// Assemblies property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureAssemblyItemList> AssembliesProperty =
            RegisterModelProperty<FixtureAssemblyItemList>(c => c.Assemblies, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Returns the Assemblies within this package
        /// </summary>
        public FixtureAssemblyItemList Assemblies
        {
            get
            {
                return this.GetPropertyLazy<FixtureAssemblyItemList>(
                    AssembliesProperty,
                    new FixtureAssemblyItemList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// Asynchronously returns the Assemblys within this package
        /// </summary>
        public FixtureAssemblyItemList AssemblysAsync
        {
            get
            {
                return this.GetPropertyLazy<FixtureAssemblyItemList>(
                    AssembliesProperty,
                    new FixtureAssemblyItemList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #region Components
        /// <summary>
        /// Components property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureComponentItemList> ComponentsProperty =
            RegisterModelProperty<FixtureComponentItemList>(c => c.Components, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Returns the Components within this package
        /// </summary>
        public FixtureComponentItemList Components
        {
            get
            {
                return this.GetPropertyLazy<FixtureComponentItemList>(
                    ComponentsProperty,
                    new FixtureComponentItemList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// Asynchronously returns the Components within this package
        /// </summary>
        public FixtureComponentItemList ComponentsAsync
        {
            get
            {
                return this.GetPropertyLazy<FixtureComponentItemList>(
                    ComponentsProperty,
                    new FixtureComponentItemList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));

        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static Fixture NewFixture()
        {
            Fixture item = new Fixture();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        internal static Fixture NewFixture(PlanogramFixture planFixture)
        {
            Fixture item = new Fixture();
            item.Create(planFixture);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<FixtureAssemblyItemList>(AssembliesProperty, FixtureAssemblyItemList.NewFixtureAssemblyItemList());
            this.LoadProperty<FixtureComponentItemList>(ComponentsProperty, FixtureComponentItemList.NewFixtureComponentItemList());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(PlanogramFixture planFixture)
        {
            this.LoadProperty<Int32>(IdProperty, (Int32)planFixture.Id);
            this.LoadProperty<String>(NameProperty, planFixture.Name);
            this.LoadProperty<Single>(HeightProperty, planFixture.Height);
            this.LoadProperty<Single>(WidthProperty, planFixture.Width);
            this.LoadProperty<Single>(DepthProperty, planFixture.Depth);
            this.LoadProperty<Single>(TotalFixtureCostProperty, planFixture.MetaTotalFixtureCost.HasValue ? planFixture.MetaTotalFixtureCost.Value : 0);

            this.LoadProperty<FixtureAssemblyItemList>(AssembliesProperty,
                FixtureAssemblyItemList.NewFixtureAssemblyItemList(planFixture.Assemblies));

            this.LoadProperty<FixtureComponentItemList>(ComponentsProperty, 
                FixtureComponentItemList.NewFixtureComponentItemList(planFixture.Components));
            
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<Fixture>(oldId, newId);
        }

        /// <summary>
        /// Updates this fixture in the planogram.
        /// </summary>
        public PlanogramFixture AddOrUpdatePlanogramFixture(Planogram plan)
        {
            PlanogramFixture fixture = plan.Fixtures.FindById(this.Id);
            if (fixture == null)
            {
                fixture = PlanogramFixture.NewPlanogramFixture();
                fixture.Id = this.Id;
                plan.Fixtures.Add(fixture);
            }

            fixture.Name = this.Name;
            fixture.Height = this.Height;
            fixture.Width = this.Width;
            fixture.Depth = this.Depth;

            //copy child assemblies.
            List<PlanogramFixtureAssembly> updatedAssemblies = new List<PlanogramFixtureAssembly>(this.Assemblies.Count);
            foreach (FixtureAssemblyItem faChild in this.Assemblies)
            {
                updatedAssemblies.Add(faChild.AddOrUpdatePlanogramFixtureAssembly(fixture));
            }
            fixture.Assemblies.RemoveList(fixture.Assemblies.Except(updatedAssemblies).ToArray());

            //copy child components
            List<PlanogramFixtureComponent> updatedComponents = new List<PlanogramFixtureComponent>(this.Components.Count);
            foreach (FixtureComponentItem fcChild in this.Components)
            {
                updatedComponents.Add(fcChild.AddOrUpdatePlanogramFixtureComponent(fixture));
            }
            fixture.Components.RemoveList(fixture.Components.Except(updatedComponents).ToArray());


            return fixture;
        }

        #endregion
    }
}