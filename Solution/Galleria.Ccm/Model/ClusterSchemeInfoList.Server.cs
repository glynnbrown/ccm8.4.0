﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629 : A.Kuszyk
//		Created (copied from SA).
#endregion
#region Version History: (CCM 8.0.3)
// V8-29216 : D.Pleasance
//		Removed FetchByEntityIdIncludingDeletedCriteria, data now deleted outright
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class ClusterSchemeInfoList
    {
        #region Constructors
        private ClusterSchemeInfoList() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a list of all cluster scheme infos objects in the solution by entity id
        /// </summary>
        /// <returns></returns>
        public static ClusterSchemeInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<ClusterSchemeInfoList>(new FetchByEntityIdCriteria(entityId));
        }

        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when retrieving a list of cluster scheme infos
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IClusterSchemeInfoDal dal = dalContext.GetDal<IClusterSchemeInfoDal>())
                {
                    IEnumerable<ClusterSchemeInfoDto> dtoList = dal.FetchByEntityId(criteria.EntityId);
                    foreach (ClusterSchemeInfoDto dto in dtoList)
                    {
                        this.Add(ClusterSchemeInfo.FetchClusterSchemeInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}
