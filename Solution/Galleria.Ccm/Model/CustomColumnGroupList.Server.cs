#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public sealed partial class CustomColumnGroupList
    {
        #region Constructor

        /// <summary>
        ///     Use the provided factory methods to create a new instance of <see cref="CustomColumnGroupList"/>.
        /// </summary>
        private CustomColumnGroupList()
        {
        }

        #endregion

        #region Data Access

        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using (var dalContext = GetDalFactory(criteria.DalFactoryName).CreateContext())
            using (var dal = dalContext.GetDal<ICustomColumnGroupDal>())
            {
                dal.FetchByCustomColumnLayoutId(criteria.ParentId)
                    .ToList()
                    .ForEach(dto => Add(CustomColumnGroup.FetchCustomColumnGroup(dalContext, dto)));
            }

            MarkAsChild();

            RaiseListChangedEvents = true;
        }

        #endregion
    }
}