﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created (Auto-generated)
// V8-27152 : A.Silva ~ Added factory method to create a new group list using the existing list in a given IValidationTemplate.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Model representing a list of ValidationTemplateGroupMetric objects.
    /// </summary>
    [Serializable]
    public sealed partial class ValidationTemplateGroupMetricList :
        ModelList<ValidationTemplateGroupMetricList, ValidationTemplateGroupMetric>
    {
        #region Authorization Rules
        /// <summary>
        ///     Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ValidationTemplateGroupMetricList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ValidationTemplateGet.ToString()));
            BusinessRules.AddRule(typeof(ValidationTemplateGroupMetricList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ValidationTemplateCreate.ToString()));
            BusinessRules.AddRule(typeof(ValidationTemplateGroupMetricList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ValidationTemplateEdit.ToString()));
            BusinessRules.AddRule(typeof(ValidationTemplateGroupMetricList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ValidationTemplateDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        ///     Creates a new instance of this type
        /// </summary>
        public static ValidationTemplateGroupMetricList NewValidationTemplateGroupMetricList()
        {
            var item = new ValidationTemplateGroupMetricList();
            item.Create();
            return item;
        }

        /// <summary>
        ///     Creates a new instance of <see cref="ValidationTemplateGroupMetricList"/> adding the items already present in the given <paramref name="metrics"/> list.
        /// </summary>
        /// <param name="metrics">List of instances of <see cref="IValidationTemplateGroupMetric"/> to be added to the newly created <see cref="ValidationTemplateGroupMetricList"/>.</param>
        /// <returns>A new instance of <see cref="ValidationTemplateGroupMetricList"/> intialized with the values from the given <paramref name="metrics"/>.</returns>
        public static ValidationTemplateGroupMetricList NewValidationTemplateGroupMetricList(IEnumerable<IValidationTemplateGroupMetric> metrics)
        {
            var item = new ValidationTemplateGroupMetricList();
            item.Create(metrics);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        ///     Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            MarkAsChild();
            base.Create();
        }

        /// <summary>
        ///     Called when creating a new instance of <see cref="ValidationTemplateGroupMetricList"/> initialized with the given <paramref name="metrics"/>.
        /// </summary>
        /// <param name="metrics">the list of <see cref="IValidationTemplateGroupMetric"/> items to initialize the new list with.</param>
        private void Create(IEnumerable<IValidationTemplateGroupMetric> metrics)
        {
            foreach (var templateMetric in metrics)
            {
                this.Add(ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric(templateMetric));
            }

            this.Create();
        }

        #endregion

        #endregion

        new ValidationTemplateGroupMetric this[int index]
        {
            get { return Count > index ? base[index] : null; }
        }
    }
}