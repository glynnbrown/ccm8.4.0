﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created
#endregion

#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using AutoMapper;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A class representing an Assortment Inventory Rule within CCM
    /// (Child of Assortment)
    /// This is a child Content object and so it has the following properties/actions:
    /// - This object does NOT have a DateDeleted property or DateDeleted field in its supporting database table
    /// - if its parent is deleted, it is not removed or marked as deleted (this allows for parent 'undeletes')
    /// - if it is deleted directly, it's associated record is removed from the database. 
    /// </summary>
    [Serializable]
    [DefaultNewMethod("NewAssortmentInventoryRule")]
    public partial class AssortmentInventoryRule : ModelObject<AssortmentInventoryRule>, IPlanogramAssortmentInventoryRule
    {
        #region Static Constructor
        static AssortmentInventoryRule()
        {
        }
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The AssortmentRegion id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> ProductIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductId);
        /// <summary>
        /// The Product Id
        /// </summary>
        public Int32 ProductId
        {
            get { return GetProperty<Int32>(ProductIdProperty); }
            set { SetProperty<Int32>(ProductIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> ProductGtinProperty =
           RegisterModelProperty<String>(c => c.ProductGtin);
        /// <summary>
        /// The product Gtin
        /// </summary>
        public String ProductGtin
        {
            get { return GetProperty<String>(ProductGtinProperty); }
            set { SetProperty<String>(ProductGtinProperty, value); }
        }

        public static readonly ModelPropertyInfo<Single> CasePackProperty =
            RegisterModelProperty<Single>(c => c.CasePack);
        public Single CasePack
        {
            get { return GetProperty<Single>(CasePackProperty); }
            set { SetProperty<Single>(CasePackProperty, value); }
        }

        public static readonly ModelPropertyInfo<Single> DaysOfSupplyProperty =
            RegisterModelProperty<Single>(c => c.DaysOfSupply);
        public Single DaysOfSupply
        {
            get { return GetProperty<Single>(DaysOfSupplyProperty); }
            set { SetProperty<Single>(DaysOfSupplyProperty, value); }
        }

        public static readonly ModelPropertyInfo<Single> ShelfLifeProperty =
            RegisterModelProperty<Single>(c => c.ShelfLife);
        public Single ShelfLife
        {
            get { return GetProperty<Single>(ShelfLifeProperty); }
            set { SetProperty<Single>(ShelfLifeProperty, value); }
        }

        public static readonly ModelPropertyInfo<Single> ReplenishmentDaysProperty =
            RegisterModelProperty<Single>(c => c.ReplenishmentDays);
        public Single ReplenishmentDays
        {
            get { return GetProperty<Single>(ReplenishmentDaysProperty); }
            set { SetProperty<Single>(ReplenishmentDaysProperty, value); }
        }

        public static readonly ModelPropertyInfo<Single> WasteHurdleUnitsProperty =
            RegisterModelProperty<Single>(c => c.WasteHurdleUnits);
        public Single WasteHurdleUnits
        {
            get { return GetProperty<Single>(WasteHurdleUnitsProperty); }
            set { SetProperty<Single>(WasteHurdleUnitsProperty, value); }
        }

        public static readonly ModelPropertyInfo<Single> WasteHurdleCasePackProperty =
            RegisterModelProperty<Single>(c => c.WasteHurdleCasePack);
        public Single WasteHurdleCasePack
        {
            get { return GetProperty<Single>(WasteHurdleCasePackProperty); }
            set { SetProperty<Single>(WasteHurdleCasePackProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32> MinUnitsProperty =
            RegisterModelProperty<Int32>(c => c.MinUnits);
        public Int32 MinUnits
        {
            get { return GetProperty<Int32>(MinUnitsProperty); }
            set { SetProperty<Int32>(MinUnitsProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32> MinFacingsProperty =
            RegisterModelProperty<Int32>(c => c.MinFacings);
        public Int32 MinFacings
        {
            get { return GetProperty<Int32>(MinFacingsProperty); }
            set { SetProperty<Int32>(MinFacingsProperty, value); }
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new MaxValue<Single>(ShelfLifeProperty, 1));
            BusinessRules.AddRule(new MinValue<Single>(ShelfLifeProperty, 0));
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentInventoryRule), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.AssortmentCreate.ToString()));
            BusinessRules.AddRule(typeof(AssortmentInventoryRule), new IsInRole(AuthorizationActions.GetObject, DomainPermission.AssortmentGet.ToString()));
            BusinessRules.AddRule(typeof(AssortmentInventoryRule), new IsInRole(AuthorizationActions.EditObject, DomainPermission.AssortmentEdit.ToString()));
            BusinessRules.AddRule(typeof(AssortmentInventoryRule), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.AssortmentDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentInventoryRule NewAssortmentInventoryRule()
        {
            AssortmentInventoryRule item = new AssortmentInventoryRule();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentInventoryRule NewAssortmentInventoryRule(AssortmentProduct product)
        {
            AssortmentInventoryRule item = new AssortmentInventoryRule();
            item.Create(product.ProductId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create(Int32 productId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(ProductIdProperty, productId);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods
        
        public void CopyValues(AssortmentInventoryRule rule)
        {
            if (rule != null)
            {
                CasePack = rule.CasePack;
                DaysOfSupply = rule.DaysOfSupply;
                ShelfLife = rule.ShelfLife;
                ReplenishmentDays = rule.ReplenishmentDays;
                WasteHurdleUnits = rule.WasteHurdleUnits;
                WasteHurdleCasePack = rule.WasteHurdleCasePack;
                MinUnits = rule.MinUnits;
                MinFacings = rule.MinFacings;
            }
        }


        #endregion
    }
}
