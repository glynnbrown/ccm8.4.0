﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public sealed partial class BlockingInfoList
    {
        #region Constructor
        private BlockingInfoList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        public static BlockingInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<BlockingInfoList>(new FetchByEntityIdCriteria(entityId));
        }
        #endregion

        #region Data Access

        /// <summary>
        /// Called when retrieving a list of all entity infos
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IBlockingInfoDal dal = dalContext.GetDal<IBlockingInfoDal>())
                {
                    IEnumerable<BlockingInfoDto> dtoList = dal.FetchByEntityId(criteria.EntityId);
                    foreach (BlockingInfoDto dto in dtoList)
                    {
                        this.Add(BlockingInfo.GetBlockingInfo(dalContext, dto));
                    }
                }
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }
        #endregion

    }
}