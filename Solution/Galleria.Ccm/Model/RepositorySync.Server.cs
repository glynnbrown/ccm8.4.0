﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model object representing a repository sync operation
    /// </summary>
    public partial class RepositorySync
    {
        #region Constants
        private const String _directionParameter = "Direction";
        #endregion

        #region Constructor
        private RepositorySync() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the specified repository sync operation
        /// </summary>
        internal static RepositorySync FetchRepositorySync(IDalContext dalContext, RepositorySyncDto dto)
        {
            return DataPortal.FetchChild<RepositorySync>(dalContext, dto);
        }

        /// <summary>
        /// Returns the specified repository sync operation
        /// </summary>
        /// <param name="id">The id of the operation to fetch</param>
        /// <returns>The specified repository sync operation</returns>
        public static RepositorySync FetchById(Int32 id)
        {
            return DataPortal.Fetch<RepositorySync>(new SingleCriteria<Int32>(id));
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private RepositorySyncDto GetDataTransferObject()
        {
            return new RepositorySyncDto
            {
                Id = this.ReadProperty<Int32>(IdProperty),
            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        /// <Param name="dalContext"> The current dal context </param>
        private void LoadDataTransferObject(IDalContext dalContext, RepositorySyncDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RepositorySyncTarget>(SourceProperty, RepositorySyncTarget.FetchRepositorySyncTarget(dalContext, dto.SourceTargetId));
            this.LoadProperty<RepositorySyncTarget>(DestinationProperty, RepositorySyncTarget.FetchRepositorySyncTarget(dalContext, dto.DestinationTargetId));
            using (IRepositorySyncParameterDal dal = dalContext.GetDal<IRepositorySyncParameterDal>())
            {
                foreach (RepositorySyncParameterDto parameterDto in dal.FetchByRepositorySyncId(dto.Id))
                {
                    switch (parameterDto.Name)
                    {
                        case _directionParameter:
                            this.LoadProperty<RepositorySyncDirection>(DirectionProperty, EnumHelper.Parse(parameterDto.Value, RepositorySyncDirection.OneWay));
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when loading a parameter from a dto
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, RepositorySyncDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        /// <summary>
        /// Called when fetching a repository sync by its id
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory(Constants.RepositorySyncDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IRepositorySyncDal dal = dalContext.GetDal<IRepositorySyncDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.Value));
                }
            }
        }
        #endregion

        #endregion
    }
}
