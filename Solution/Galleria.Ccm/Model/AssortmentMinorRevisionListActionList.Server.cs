﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentMinorRevisionListActionList
    {
        #region Constructors
        private AssortmentMinorRevisionListActionList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an existing Assortment minor revision action list
        /// </summary>
        /// <param name="childData"></param>
        /// <returns>A new Assortment minor revision action list</returns>
        internal static AssortmentMinorRevisionListActionList FetchByAssortmentMinorRevisionContentId(IDalContext dalContext, Int32 AssortmentMinorRevisionContentId)
        {
            return DataPortal.FetchChild<AssortmentMinorRevisionListActionList>(dalContext, AssortmentMinorRevisionContentId);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="reviewId">The parent Assortment id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 AssortmentMinorRevisionContentId)
        {
            RaiseListChangedEvents = false;
            using (IAssortmentMinorRevisionListActionDal dal = dalContext.GetDal<IAssortmentMinorRevisionListActionDal>())
            {
                IEnumerable<AssortmentMinorRevisionListActionDto> dtoList = dal.FetchByAssortmentMinorRevisionId(AssortmentMinorRevisionContentId);
                foreach (AssortmentMinorRevisionListActionDto dto in dtoList)
                {
                    this.Add(AssortmentMinorRevisionListAction.FetchAssortmentMinorRevisionListAction(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
