﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25879 : M.Shelley
//  Created.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    public enum PlanAssignmentType
    {
        None = 0,
        ManualPlan = 1,
        ClusterSpacePlan = 2,
        StoreSpacePlan = 3,
    }

    public static class PlanAssignmentTypeHelper
    {
        public static readonly Dictionary<PlanAssignmentType, String> FriendlyNames =
            new Dictionary<PlanAssignmentType, String>()
            {
                {PlanAssignmentType.None, Message.Enum_AssignmentType_None},
                {PlanAssignmentType.ManualPlan, Message.Enum_AssignmentType_ManualPlan},
                //{PlanAssignmentType.ClusterSpacePlan, Message.Enum_AssignmentType_ClusterSpacePlan},
                {PlanAssignmentType.StoreSpacePlan, Message.Enum_AssignmentType_StoreSpacePlan},
            };

        /// <summary>
        /// Returns the matching enum value from the specified description
        /// Returns null if not match found
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanAssignmentType? PublishingTypeGetEnum(String description)
        {
            foreach (KeyValuePair<PlanAssignmentType, String> keyPair in PlanAssignmentTypeHelper.FriendlyNames)
            {
                if (keyPair.Value.Equals(description))
                {
                    return keyPair.Key;
                }
            }
            return null;
        }
    }
}
