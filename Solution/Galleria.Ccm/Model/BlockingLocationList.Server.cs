﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public sealed partial class BlockingLocationList
    {
        #region Constructor
        private BlockingLocationList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Fetches the list by its parent id
        /// </summary>
        internal static BlockingLocationList FetchByBlockingId(IDalContext dalContext, Int32 blockingId)
        {
            return DataPortal.FetchChild<BlockingLocationList>(dalContext, blockingId);
        }

        #endregion

        #region Data Access

        #region Fetch


        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 parentId)
        {
            RaiseListChangedEvents = false;
            using (IBlockingLocationDal dal = dalContext.GetDal<IBlockingLocationDal>())
            {
                IEnumerable<BlockingLocationDto> dtoList = dal.FetchByBlockingId(parentId);
                foreach (BlockingLocationDto dto in dtoList)
                {
                    this.Add(BlockingLocation.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}