﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630: A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

using Csla;

using Galleria.Framework.Dal;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class ProductUniverseProductList
    {
        #region Constructor
        private ProductUniverseProductList() { } // Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a list of existing items based on their parent product universe
        /// </summary>
        /// <returns>A list of existing it</returns>
        internal static ProductUniverseProductList FetchByProductUniverseId(IDalContext dalContext, Int32 productUniverseId)
        {
            return DataPortal.FetchChild<ProductUniverseProductList>(dalContext, productUniverseId);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="productUniverseId">The parent product universe id id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 productUniverseId)
        {
            RaiseListChangedEvents = false;
            using (IProductUniverseProductDal dal = dalContext.GetDal<IProductUniverseProductDal>())
            {
                IEnumerable<ProductUniverseProductDto> dtoList = dal.FetchByProductUniverseId(productUniverseId);
                foreach (ProductUniverseProductDto dto in dtoList)
                {
                    this.Add(ProductUniverseProduct.FetchProductUniverseProduct(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
