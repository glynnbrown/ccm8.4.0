﻿#region Header Information 
// Copyright © Galleria RTS Ltd 2015

#region Version History 830
// V8-31699 : A.Heathcote
//  Created.
// V8-31699 : A.Heathcote
//  Added the in line conditional to the create method

#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    ///  <summary>
    ///  A Recent Label
    /// </summary>
    [Serializable]
    public partial class UserEditorSettingsRecentLabel : ModelObject<UserEditorSettingsRecentLabel>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Defines Property 
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Unique Id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }
        #endregion 

        #region Name
        /// <summary>
        /// Defines the Name Property
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Unique Id 
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }
        #endregion

        #region LabelType

        public static readonly ModelPropertyInfo<LabelType> LabelTypeProperty =
            RegisterModelProperty<LabelType>(c => c.LabelType);

        public LabelType LabelType
        {
            get { return GetProperty<LabelType>(LabelTypeProperty); }
        }
        #endregion

        #region SaveType 

        public static readonly ModelPropertyInfo<LabelDalFactoryType> SaveTypeProperty =
            RegisterModelProperty<LabelDalFactoryType>(c => c.SaveType);

        public LabelDalFactoryType SaveType
        {
            get { return GetProperty<LabelDalFactoryType>(SaveTypeProperty); }
        }
        
        #endregion

        #region LabelID

        public static readonly ModelPropertyInfo<Object> LabelIdProperty =
            RegisterModelProperty<Object>(c => c.LabelId);
        public Object LabelId
        {
            get
            { return GetProperty<Object>(LabelIdProperty); }
               
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// defines the autherization rules
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
        }
        #endregion

        #region Factory Methods 

        internal static UserEditorSettingsRecentLabel NewUserEditorSettingsRecentLabel(
            Object labelId, String labelName, LabelType labelType)
        {
            UserEditorSettingsRecentLabel item = new UserEditorSettingsRecentLabel();
            item.Create(labelId, labelName, labelType);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        private void Create(Object labelId, String labelName, LabelType labelType)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, labelName);
            this.LoadProperty<LabelType>(LabelTypeProperty, labelType);
            this.LoadProperty<LabelDalFactoryType>(SaveTypeProperty, labelId is String ? LabelDalFactoryType.FileSystem : LabelDalFactoryType.Unknown);
            this.LoadProperty<Object>(LabelIdProperty, labelId);  
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}
