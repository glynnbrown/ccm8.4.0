﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-25608 : N.Foster
//  Created
// V8- : L.Ineson
//  Added additional metadata properties.
// V8-25787 : N.Foster
//  Added processing status
// V8-25787 : N.Foster
//  Added Row Version
// CCM-25880 : N.Haywood
//  Added CategoryCode and CategoryName
// V8-25881 : A.Probyn
//  Added MetaData properties
// V8-26492 : L.Ineson
//  Added unit of measure properties
// V8-26822 : L.Ineson
//  Populated model property display types.
//  Added planogram group id and name
// V8-27058 : A.Probyn
//  Updated meta data properties
// V8-27154 : L.Luong
//  Added ProductPlacement X,Y,Z
// V8-27411 : M.Pettit
//  Added LockType, isLocked, LockUserId, LockUserName properties
//  Changed names of ProcessingStatus and Progress properties to be Automation-specific
//  Added MetaData and Validation status properties
//  Changed dto's Status property to a byte as it is now an enum value
//  Added DateValidationDataCalculated Property
//  Added ValidationIsOutOfDate property
//  Added UserName property
// V8-27966 : A.Silva
//      Added NewPlanogramInfo to retrieve a placeholder info from a planogram.
// V8-28253 : L.Ineson
// Commented out fields in EnumerateDisplayablePropertyInfos that are not currently populated.
// V8-28495 : N.Haywood
//  Changed MetaTotalMerchandisableVolumetricSpace's display type to none
// V8-28759 : J.Pickup
//  Uncommented MetaDOS fields. (Now get populated). 
#endregion

#region Version History: CCM803
// V8-29326 : M.Shelley
//	Added ClusterSchemeName and ClusterName Properties for displaying in plan assignment
//	Added LocationCode and LocationName Properties for displaying in plan assignment
// V8-29590 : A.Probyn
//	Extended for new Added MetaNoOfErrors, MetaNoOfWarnings, 
//	MetaTotalErrorScore, MetaHighestErrorScore
// V8-28242 : M.Shelley
//  Added Status, DateWip, DateApproved, DateArchived 
// V8-29860 : M.Shelley
//  Added the PlanogramType property
#endregion

#region Version History: CCM810
// V8-29590 : A.Probyn
//  Added MetaNoOfInformationEventsProperty & MetaNoOfDebugEventsProperty
#endregion

#region Version History: CCM811

// V8-30164 : A.Silva
//  Amended MetaAverageFacings, MetaAverageUnits, MetaSpaceToUnitsIndex to be <Single?>.
// V8-30225 : M.Shelley
//  Added the Planogram UCR
// V8-30359 : A.Probyn
//  Added new MostRecentLinkedWorkpackageName
// V8-30380 : L.ineson
//  Commented out MetaTotal highest error score from displayable fields.
// V8-30449 : L.Ineson
//  Updated DateModified and created to show time too.
// V8-30463 : L.Ineson
//  Added GetIdValue override
#endregion

#region Version History: CCM820
// V8-31131 : A.Probyn
//  Removed MetaSpaceToUnitsIndex
// V8-31568 : L.Ineson
//  Metadata friendly names now point at planogram model ones.
#endregion

#region Version History: CCM830
// CCM-32015 : N.Haywood
//  Added mission fields
// CCM-31831 : A.Probyn
//  Adding more missing fields.
// V8-32521 : J.Pickup
//	Added MetaHasComponentsOutsideOfFixtureArea
// V8-33003 : J.Pickup
//  PlanogramInfo_AssignedLocationCount Added.
#endregion

#region Version History: CCM840
// V8-19069 : J.Mendes
//  Five new Note field attributes for the Custom Attribute Data 
#endregion

#endregion

using System;
using System.Collections.Generic;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using PlanogramsMessage =Galleria.Framework.Planograms.Resources.Language.Message;
using Galleria.Framework.ViewModel;
using System.Globalization;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class PlanogramInfo : ModelReadOnlyObject<PlanogramInfo>
    {
        #region Properties

        #region Id

        /// <summary>
        ///     Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);

        /// <summary>
        ///     Gets the planogram id.
        /// </summary>
        public Int32 Id
        {
            get { return ReadProperty(IdProperty); }
        }

        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// Returns the planogram row version
        /// </summary>
        public RowVersion RowVersion
        {
            get { return this.ReadProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region Name

        /// <summary>
        ///     Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name, Message.PlanogramInfo_Name);

        /// <summary>
        ///     Gets the planogram name.
        /// </summary>
        public String Name
        {
            get { return ReadProperty(NameProperty); }
        }

        #endregion

        #region Status

        /// <summary>
        ///     Status property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramStatusType> StatusProperty =
            RegisterModelProperty<PlanogramStatusType>(c => c.Status, Message.PlanogramInfo_Status);

        /// <summary>
        ///     Gets the planogram status.
        /// </summary>
        public PlanogramStatusType Status
        {
            get { return ReadProperty(StatusProperty); }
        }

        #endregion

        #region Height

        /// <summary>
        /// Height property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HeightProperty =
            RegisterModelProperty<Single>(c => c.Height, Message.PlanogramInfo_Height, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets the specified height of the planogram
        /// </summary>
        public Single Height
        {
            get { return ReadProperty<Single>(HeightProperty); }
        }

        #endregion

        #region Width

        /// <summary>
        /// Width property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> WidthProperty =
            RegisterModelProperty<Single>(c => c.Width, Message.PlanogramInfo_Width, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets the specified width of the planogram
        /// </summary>
        public Single Width
        {
            get { return ReadProperty<Single>(WidthProperty); }
        }

        #endregion

        #region Depth

        /// <summary>
        /// Depth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DepthProperty =
            RegisterModelProperty<Single>(c => c.Depth, Message.PlanogramInfo_Depth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets the specified depth of the planogram.
        /// </summary>
        public Single Depth
        {
            get { return ReadProperty<Single>(DepthProperty); }
        }

        #endregion

        #region UserName
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> UserNameProperty =
            RegisterModelProperty<String>(c => c.UserName, "Owner");
        /// <summary>
        /// Gets or sets the owner of the planogram
        /// </summary>
        public String UserName
        {
            get { return this.GetProperty<String>(UserNameProperty); }
        }
        #endregion

        #region DateCreated

        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated, Message.PlanogramInfo_DateCreated,
            ModelPropertyDisplayType.DateTime);
        /// <summary>
        /// Gets the utc datetime value for when the planogram was created.
        /// </summary>
        public DateTime DateCreated
        {
            get { return ReadProperty<DateTime>(DateCreatedProperty); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified, Message.PlanogramInfo_DateLastModified,
            ModelPropertyDisplayType.DateTime);
        /// <summary>
        /// Gets the utc datevalue for when the planogram was last modified.
        /// </summary>
        public DateTime DateLastModified
        {
            get { return ReadProperty<DateTime>(DateLastModifiedProperty); }
        }

        #endregion

        #region DateDeleted
        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);
        /// <summary>
        /// Returns the date the planogram was deleted
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return this.ReadProperty<DateTime?>(DateDeletedProperty); }
        }
        #endregion

        #region CategoryCode

        /// <summary>
        ///     CategoryCode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CategoryCodeProperty =
            RegisterModelProperty<String>(c => c.CategoryCode, Message.PlanogramInfo_CategoryCode);

        /// <summary>
        ///     Gets the planogram name.
        /// </summary>
        public String CategoryCode
        {
            get { return ReadProperty(CategoryCodeProperty); }
        }

        #endregion

        #region CategoryName

        /// <summary>
        ///     Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CategoryNameProperty =
            RegisterModelProperty<String>(c => c.CategoryName, Message.PlanogramInfo_CategoryName);

        /// <summary>
        ///     Gets the planogram name.
        /// </summary>
        public String CategoryName
        {
            get { return ReadProperty(CategoryNameProperty); }
        }

        #endregion

        #region LengthUnitsOfMeasure
        /// <summary>
        /// LengthUnitsOfMeasure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramLengthUnitOfMeasureType> LengthUnitsOfMeasureProperty =
            RegisterModelProperty<PlanogramLengthUnitOfMeasureType>(c => c.LengthUnitsOfMeasure);
        /// <summary>
        /// Indicates the units of measure for length values
        /// 0 = Unknown, 1 = Centimeters, 2 = Inches
        /// </summary>
        public PlanogramLengthUnitOfMeasureType LengthUnitsOfMeasure
        {
            get { return this.GetProperty<PlanogramLengthUnitOfMeasureType>(LengthUnitsOfMeasureProperty); }
        }
        #endregion

        #region AreaUnitsOfMeasure
        /// <summary>
        /// AreaUnitsOfMeasure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramAreaUnitOfMeasureType> AreaUnitsOfMeasureProperty =
            RegisterModelProperty<PlanogramAreaUnitOfMeasureType>(c => c.AreaUnitsOfMeasure);
        /// <summary>
        /// Indicates the units of measure for area values
        /// 0 = Unknown, 1 = Square Centimeters, 2 = Square Inches
        /// </summary>
        public PlanogramAreaUnitOfMeasureType AreaUnitsOfMeasure
        {
            get { return this.GetProperty<PlanogramAreaUnitOfMeasureType>(AreaUnitsOfMeasureProperty); }
        }
        #endregion

        #region VolumeUnitsOfMeasure
        /// <summary>
        /// VolumeUnitsOfMeasure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramVolumeUnitOfMeasureType> VolumeUnitsOfMeasureProperty =
            RegisterModelProperty<PlanogramVolumeUnitOfMeasureType>(c => c.VolumeUnitsOfMeasure);
        /// <summary>
        /// Indicates the units of measure for volume values
        /// 0 = Unknown, 1 = Litres, 2 = Cubic Feet
        /// </summary>
        public PlanogramVolumeUnitOfMeasureType VolumeUnitsOfMeasure
        {
            get { return this.GetProperty<PlanogramVolumeUnitOfMeasureType>(VolumeUnitsOfMeasureProperty); }
        }
        #endregion

        #region WeightUnitsOfMeasure
        /// <summary>
        /// WeightUnitsOfMeasure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramWeightUnitOfMeasureType> WeightUnitsOfMeasureProperty =
            RegisterModelProperty<PlanogramWeightUnitOfMeasureType>(c => c.WeightUnitsOfMeasure);
        /// <summary>
        /// Indicates the units of measure for weight values
        /// 0 = Unknown, 1 = Kilograms, 2 = Pounds
        /// </summary>
        public PlanogramWeightUnitOfMeasureType WeightUnitsOfMeasure
        {
            get { return this.GetProperty<PlanogramWeightUnitOfMeasureType>(WeightUnitsOfMeasureProperty); }
        }
        #endregion

        #region AngleUnitsOfMeasure
        /// <summary>
        /// AngleUnitsOfMeasure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> AngleUnitsOfMeasureProperty =
            RegisterModelProperty<Byte>(c => c.AngleUnitsOfMeasure, Message.PlanogramInfo_AngleUnitsOfMeasure);
        /// <summary>
        /// Indicates the units of measure for angle values
        /// 0 = Unknown, 1 = Radians, 2 = Degrees
        /// </summary>
        public Byte AngleUnitsOfMeasure
        {
            get { return this.GetProperty<Byte>(AngleUnitsOfMeasureProperty); }
        }
        #endregion

        #region CurrencyUnitsOfMeasure
        /// <summary>
        /// CurrencyUnitsOfMeasure property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramCurrencyUnitOfMeasureType> CurrencyUnitsOfMeasureProperty =
            RegisterModelProperty<PlanogramCurrencyUnitOfMeasureType>(c => c.CurrencyUnitsOfMeasure);
        /// <summary>
        /// Indicates the units of measure for currency values
        /// 0 = Unknown, 1 = GBP, 2 = USD
        /// </summary>
        public PlanogramCurrencyUnitOfMeasureType CurrencyUnitsOfMeasure
        {
            get { return this.GetProperty<PlanogramCurrencyUnitOfMeasureType>(CurrencyUnitsOfMeasureProperty); }
        }
        #endregion

        #region ProductPlacementX
        /// <summary>
        /// ProductPlacementX property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramProductPlacementXType> ProductPlacementXProperty =
            RegisterModelProperty<PlanogramProductPlacementXType>(c => c.ProductPlacementX);
        /// <summary>
        /// Indicates the units of measure for currency values
        /// 0 = Single, 1 = FillWide
        /// </summary>
        public PlanogramProductPlacementXType ProductPlacementX
        {
            get { return this.GetProperty<PlanogramProductPlacementXType>(ProductPlacementXProperty); }
        }
        #endregion

        #region ProductPlacementY
        /// <summary>
        /// ProductPlacementY property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramProductPlacementYType> ProductPlacementYProperty =
            RegisterModelProperty<PlanogramProductPlacementYType>(c => c.ProductPlacementY);
        /// <summary>
        /// Indicates the units of measure for currency values
        /// 0 = Single, 1 = FillHigh
        /// </summary>
        public PlanogramProductPlacementYType ProductPlacementY
        {
            get { return this.GetProperty<PlanogramProductPlacementYType>(ProductPlacementYProperty); }
        }
        #endregion

        #region ProductPlacementZ
        /// <summary>
        /// ProductPlacementZ property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramProductPlacementZType> ProductPlacementZProperty =
            RegisterModelProperty<PlanogramProductPlacementZType>(c => c.ProductPlacementZ, PlanogramsMessage.Planogram_MetaBayCount);
        /// <summary>
        /// Indicates the units of measure for currency values
        /// 0 = Single, 1 = FillDeep
        /// </summary>
        public PlanogramProductPlacementZType ProductPlacementZ
        {
            get { return this.GetProperty<PlanogramProductPlacementZType>(ProductPlacementZProperty); }
        }
        #endregion

        #region Meta Data Properties

        #region MetaBayCount
        /// <summary>
        /// MetaBayCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaBayCountProperty =
            RegisterModelProperty<Int32?>(c => c.MetaBayCount, PlanogramsMessage.Planogram_MetaBayCount);
        /// <summary>
        /// Gets/Sets the meta data bay count;
        /// </summary>
        public Int32? MetaBayCount
        {
            get { return this.GetProperty<Int32?>(MetaBayCountProperty); }
        }
        #endregion

        #region MetaUniqueProductCount
        /// <summary>
        /// MetaUniqueProductCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaUniqueProductCountProperty =
            RegisterModelProperty<Int32?>(c => c.MetaUniqueProductCount, PlanogramsMessage.Planogram_MetaUniqueProductCount);
        /// <summary>
        /// Gets/Sets the meta data unique product count;
        /// </summary>
        public Int32? MetaUniqueProductCount
        {
            get { return this.GetProperty<Int32?>(MetaUniqueProductCountProperty); }
        }
        #endregion

        #region MetaComponentCount
        /// <summary>
        /// MetaComponentCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaComponentCountProperty =
            RegisterModelProperty<Int32?>(c => c.MetaComponentCount, PlanogramsMessage.Planogram_MetaComponentCount);
        /// <summary>
        /// Gets/Sets the meta data component count;
        /// </summary>
        public Int32? MetaComponentCount
        {
            get { return this.GetProperty<Int32?>(MetaComponentCountProperty); }
        }
        #endregion

        #region MetaTotalMerchandisableLinearSpace
        /// <summary>
        /// MetaTotalMerchandisableLinearSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalMerchandisableLinearSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalMerchandisableLinearSpace, PlanogramsMessage.Planogram_MetaTotalMerchandisableLinearSpace, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the meta data TotalMerchandisableLinearSpace
        /// </summary>
        public Single? MetaTotalMerchandisableLinearSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalMerchandisableLinearSpaceProperty); }
        }
        #endregion

        #region MetaTotalMerchandisableAreaSpace
        /// <summary>
        /// MetaTotalMerchandisableAreaSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalMerchandisableAreaSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalMerchandisableAreaSpace, PlanogramsMessage.Planogram_MetaTotalMerchandisableAreaSpace, ModelPropertyDisplayType.Area);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalMerchandisableAreaSpace
        /// </summary>
        public Single? MetaTotalMerchandisableAreaSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalMerchandisableAreaSpaceProperty); }
        }
        #endregion

        #region MetaTotalMerchandisableVolumetricSpace
        /// <summary>
        /// MetaTotalMerchandisableVolumetricSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalMerchandisableVolumetricSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalMerchandisableVolumetricSpace,
            PlanogramsMessage.Planogram_MetaTotalMerchandisableVolumetricSpace, ModelPropertyDisplayType.LengthCubed);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalMerchandisableVolumetricSpace
        /// </summary>
        public Single? MetaTotalMerchandisableVolumetricSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalMerchandisableVolumetricSpaceProperty); }
        }
        #endregion

        #region MetaTotalLinearWhiteSpace
        /// <summary>
        /// MetaTotalLinearWhiteSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalLinearWhiteSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalLinearWhiteSpace, PlanogramsMessage.Planogram_MetaTotalLinearWhiteSpace, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalMerchandisableVolumetricSpace
        /// </summary>
        public Single? MetaTotalLinearWhiteSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalLinearWhiteSpaceProperty); }
        }
        #endregion

        #region MetaTotalAreaWhiteSpace
        /// <summary>
        /// MetaTotalAreaWhiteSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalAreaWhiteSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalAreaWhiteSpace, PlanogramsMessage.Planogram_MetaTotalAreaWhiteSpace, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalAreaWhiteSpace
        /// </summary>
        public Single? MetaTotalAreaWhiteSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalAreaWhiteSpaceProperty); }
        }
        #endregion

        #region MetaTotalVolumetricWhiteSpace
        /// <summary>
        /// MetaTotalVolumetricWhiteSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaTotalVolumetricWhiteSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaTotalVolumetricWhiteSpace, PlanogramsMessage.Planogram_MetaTotalVolumetricWhiteSpace, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalAreaWhiteSpace
        /// </summary>
        public Single? MetaTotalVolumetricWhiteSpace
        {
            get { return this.GetProperty<Single?>(MetaTotalVolumetricWhiteSpaceProperty); }
        }
        #endregion

        #region MetaProductsPlaced
        /// <summary>
        /// MetaProductsPlaced property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaProductsPlacedProperty =
            RegisterModelProperty<Int32?>(c => c.MetaProductsPlaced, PlanogramsMessage.Planogram_MetaProductsPlaced);
        /// <summary>
        /// Gets/Sets the meta data MetaProductsPlaced
        /// </summary>
        public Int32? MetaProductsPlaced
        {
            get { return this.GetProperty<Int32?>(MetaProductsPlacedProperty); }
        }
        #endregion

        #region MetaProductsUnplaced
        /// <summary>
        /// MetaProductsUnplaced property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaProductsUnplacedProperty =
            RegisterModelProperty<Int32?>(c => c.MetaProductsUnplaced, PlanogramsMessage.Planogram_MetaProductsUnplaced);
        /// <summary>
        /// Gets/Sets the meta data MetaProductsUnplaced
        /// </summary>
        public Int32? MetaProductsUnplaced
        {
            get { return this.GetProperty<Int32?>(MetaProductsUnplacedProperty); }
        }
        #endregion

        #region MetaNewProducts
        /// <summary>
        /// MetaNewProducts property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaNewProductsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaNewProducts, PlanogramsMessage.Planogram_MetaNewProducts);
        /// <summary>
        /// Gets/Sets the meta data MetaNewProducts
        /// </summary>
        public Int32? MetaNewProducts
        {
            get { return this.GetProperty<Int32?>(MetaNewProductsProperty); }
        }
        #endregion

        #region MetaChangesFromPreviousCount
        /// <summary>
        /// MetaChangesFromPreviousCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaChangesFromPreviousCountProperty =
            RegisterModelProperty<Int32?>(c => c.MetaChangesFromPreviousCount, PlanogramsMessage.Planogram_MetaChangesFromPreviousCount);
        /// <summary>
        /// Gets/Sets the meta data MetaChangesFromPreviousCount
        /// </summary>
        public Int32? MetaChangesFromPreviousCount
        {
            get { return this.GetProperty<Int32?>(MetaChangesFromPreviousCountProperty); }
        }
        #endregion

        #region MetaChangeFromPreviousStarRating
        /// <summary>
        /// MetaChangeFromPreviousStarRating property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaChangeFromPreviousStarRatingProperty =
            RegisterModelProperty<Int32?>(c => c.MetaChangeFromPreviousStarRating, PlanogramsMessage.Planogram_MetaChangesFromPreviousCount);
        /// <summary>
        /// Gets/Sets the meta data MetaChangeFromPreviousStarRating
        /// </summary>
        public Int32? MetaChangeFromPreviousStarRating
        {
            get { return this.GetProperty<Int32?>(MetaChangeFromPreviousStarRatingProperty); }
        }
        #endregion

        #region MetaBlocksDropped
        /// <summary>
        /// MetaBlocksDropped property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaBlocksDroppedProperty =
            RegisterModelProperty<Int32?>(c => c.MetaBlocksDropped, PlanogramsMessage.Planogram_MetaBlocksDropped);
        /// <summary>
        /// Gets/Sets the meta data MetaBlocksDropped
        /// </summary>
        public Int32? MetaBlocksDropped
        {
            get { return this.GetProperty<Int32?>(MetaBlocksDroppedProperty); }
        }
        #endregion

        #region MetaNotAchievedInventory
        /// <summary>
        /// MetaNotAchievedInventory property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaNotAchievedInventoryProperty =
            RegisterModelProperty<Int32?>(c => c.MetaNotAchievedInventory, PlanogramsMessage.Planogram_MetaNotAchievedInventory);
        /// <summary>
        /// Gets/Sets the meta data MetaNotAchievedInventory
        /// </summary>
        public Int32? MetaNotAchievedInventory
        {
            get { return this.GetProperty<Int32?>(MetaNotAchievedInventoryProperty); }
        }
        #endregion

        #region MetaTotalFacings
        /// <summary>
        /// MetaTotalFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalFacingsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalFacings, PlanogramsMessage.Planogram_MetaTotalFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalFacings
        /// </summary>
        public Int32? MetaTotalFacings
        {
            get { return this.GetProperty<Int32?>(MetaTotalFacingsProperty); }
        }
        #endregion

        #region MetaAverageFacings

        /// <summary>
        ///     Property definition for <see cref="MetaAverageFacings"/>.
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageFacingsProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageFacings, PlanogramsMessage.Planogram_MetaAverageFacings);

        /// <summary>
        ///     Gets or sets the average number of <c>Facings</c> per <c>Product</c> on the <c>Planogram</c>.
        /// </summary>
        /// <remarks>
        ///     The value is calculated as:
        /// <para></para>   Average of <c>PlanogramPosition_FacingsWide</c> for all the records linked to the <c>Planogram</c> in the <c>planogram position</c> table.
        /// </remarks>
        public Single? MetaAverageFacings
        {
            get { return this.GetProperty<Single?>(MetaAverageFacingsProperty); }
        }

        #endregion

        #region MetaTotalUnits
        /// <summary>
        /// MetaTotalUnits property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalUnitsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalUnits, PlanogramsMessage.Planogram_MetaTotalUnits);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalUnits
        /// </summary>
        public Int32? MetaTotalUnits
        {
            get { return this.GetProperty<Int32?>(MetaTotalUnitsProperty); }
        }
        #endregion

        #region MetaAverageUnits

        /// <summary>
        ///     Property definition for <see cref="MetaAverageUnits"/>.
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageUnitsProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageUnits, PlanogramsMessage.Planogram_MetaAverageUnits);

        /// <summary>
        ///     Gets or sets the average number of <c>Units</c> per <c>Product</c> on the <c>Planogram</c>.
        /// </summary>
        /// <remarks>
        ///     The value is calculated as:
        /// <para></para>   Average of <c>PlanogramPosition_TotalUnits</c> for all the records linked to the <c>Planogram</c> in the <c>planogram position</c> table.
        /// </remarks>
        public Single? MetaAverageUnits
        {
            get { return this.GetProperty<Single?>(MetaAverageUnitsProperty); }
        }

        #endregion

        #region MetaMinDos
        /// <summary>
        /// MetaMinDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMinDosProperty =
            RegisterModelProperty<Single?>(c => c.MetaMinDos, PlanogramsMessage.Planogram_MetaMinDos);
        /// <summary>
        /// Gets/Sets the meta data MetaMinDos
        /// </summary>
        public Single? MetaMinDos
        {
            get { return this.GetProperty<Single?>(MetaMinDosProperty); }
        }
        #endregion

        #region MetaMaxDos
        /// <summary>
        /// MetaMaxDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMaxDosProperty =
            RegisterModelProperty<Single?>(c => c.MetaMaxDos, PlanogramsMessage.Planogram_MetaMaxDos);
        /// <summary>
        /// Gets/Sets the meta data MetaMinDos
        /// </summary>
        public Single? MetaMaxDos
        {
            get { return this.GetProperty<Single?>(MetaMaxDosProperty); }
        }
        #endregion

        #region MetaAverageDos
        /// <summary>
        /// MetaAverageDos property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageDosProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageDos, PlanogramsMessage.Planogram_MetaAverageDos);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageDos
        /// </summary>
        public Single? MetaAverageDos
        {
            get { return this.GetProperty<Single?>(MetaAverageDosProperty); }
        }
        #endregion

        #region MetaMinCases
        /// <summary>
        /// MetaMinCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMinCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaMinCases, PlanogramsMessage.Planogram_MetaMinCases);
        /// <summary>
        /// Gets/Sets the meta data MetaMinCases
        /// </summary>
        public Single? MetaMinCases
        {
            get { return this.GetProperty<Single?>(MetaMinCasesProperty); }
        }
        #endregion

        #region MetaAverageCases
        /// <summary>
        /// MetaAverageCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageCases, PlanogramsMessage.Planogram_MetaAverageCases);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageCases
        /// </summary>
        public Single? MetaAverageCases
        {
            get { return this.GetProperty<Single?>(MetaAverageCasesProperty); }
        }
        #endregion

        #region MetaMaxCases
        /// <summary>
        /// MetaMaxCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaMaxCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaMaxCases, PlanogramsMessage.Planogram_MetaMaxCases);
        /// <summary>
        /// Gets/Sets the meta data MetaMaxCases
        /// </summary>
        public Single? MetaMaxCases
        {
            get { return this.GetProperty<Single?>(MetaMaxCasesProperty); }
        }
        #endregion
        
        #region MetaTotalComponentCollisions
        /// <summary>
        /// MetaTotalComponentCollisions property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalComponentCollisionsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalComponentCollisions, PlanogramsMessage.Planogram_MetaTotalComponentCollisions);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalComponentCollisions
        /// </summary>
        public Int32? MetaTotalComponentCollisions
        {
            get { return this.GetProperty<Int32?>(MetaTotalComponentCollisionsProperty); }
        }
        #endregion

        #region MetaTotalComponentsOverMerchandisedDepth
        /// <summary>
        /// MetaTotalComponentsOverMerchandisedDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalComponentsOverMerchandisedDepthProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalComponentsOverMerchandisedDepth,
            PlanogramsMessage.Planogram_MetaTotalComponentsOverMerchandisedDepth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalComponentsOverMerchandisedDepth
        /// </summary>
        public Int32? MetaTotalComponentsOverMerchandisedDepth
        {
            get { return this.GetProperty<Int32?>(MetaTotalComponentsOverMerchandisedDepthProperty); }
        }
        #endregion

        #region MetaTotalComponentsOverMerchandisedHeight
        /// <summary>
        /// MetaTotalComponentsOverMerchandisedHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalComponentsOverMerchandisedHeightProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalComponentsOverMerchandisedHeight,
            PlanogramsMessage.Planogram_MetaTotalComponentsOverMerchandisedHeight, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalComponentsOverMerchandisedHeight
        /// </summary>
        public Int32? MetaTotalComponentsOverMerchandisedHeight
        {
            get { return this.GetProperty<Int32?>(MetaTotalComponentsOverMerchandisedHeightProperty); }
        }
        #endregion

        #region MetaTotalComponentsOverMerchandisedWidth
        /// <summary>
        /// MetaTotalComponentsOverMerchandisedWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalComponentsOverMerchandisedWidthProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalComponentsOverMerchandisedWidth,
            PlanogramsMessage.Planogram_MetaTotalComponentsOverMerchandisedWidth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalComponentsOverMerchandisedWidth
        /// </summary>
        public Int32? MetaTotalComponentsOverMerchandisedWidth
        {
            get { return this.GetProperty<Int32?>(MetaTotalComponentsOverMerchandisedWidthProperty); }
        }
        #endregion

        #region MetaTotalPositionCollisions
        /// <summary>
        /// MetaTotalPositionCollisions property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalPositionCollisionsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalPositionCollisions, PlanogramsMessage.Planogram_MetaTotalPositionCollisions);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalPositionCollisions
        /// </summary>
        public Int32? MetaTotalPositionCollisions
        {
            get { return this.GetProperty<Int32?>(MetaTotalPositionCollisionsProperty); }
        }
        #endregion

        #region MetaTotalFrontFacings
        /// <summary>
        /// MetaTotalFrontFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaTotalFrontFacingsProperty =
            RegisterModelProperty<Int16?>(c => c.MetaTotalFrontFacings, PlanogramsMessage.Planogram_MetaTotalFrontFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalFrontFacings
        /// </summary>
        public Int16? MetaTotalFrontFacings
        {
            get { return this.GetProperty<Int16?>(MetaTotalFrontFacingsProperty); }
        }
        #endregion

        #region MetaAverageFrontFacings
        /// <summary>
        /// MetaAverageFrontFacings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaAverageFrontFacingsProperty =
            RegisterModelProperty<Single?>(c => c.MetaAverageFrontFacings, PlanogramsMessage.Planogram_MetaAverageFrontFacings);
        /// <summary>
        /// Gets/Sets the meta data MetaAverageFrontFacings
        /// </summary>
        public Single? MetaAverageFrontFacings
        {
            get { return this.GetProperty<Single?>(MetaAverageFrontFacingsProperty); }
        }
        #endregion
        
        #region MetaNoOfErrors
        /// <summary>
        /// MetaNoOfErrors property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaNoOfErrorsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaNoOfErrors, PlanogramsMessage.Planogram_MetaNoOfErrors);
        /// <summary>
        /// Gets/Sets the meta data MetaNoOfErrors
        /// </summary>
        public Int32? MetaNoOfErrors
        {
            get { return this.GetProperty<Int32?>(MetaNoOfErrorsProperty); }
        }
        #endregion

        #region MetaNoOfWarnings
        /// <summary>
        /// MetaNoOfWarnings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaNoOfWarningsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaNoOfWarnings, PlanogramsMessage.Planogram_MetaNoOfWarnings);
        /// <summary>
        /// Gets/Sets the meta data MetaNoOfWarnings
        /// </summary>
        public Int32? MetaNoOfWarnings
        {
            get { return this.GetProperty<Int32?>(MetaNoOfWarningsProperty); }
        }
        #endregion

        #region MetaTotalErrorScore
        /// <summary>
        /// MetaTotalErrorScore property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaTotalErrorScoreProperty =
            RegisterModelProperty<Int32?>(c => c.MetaTotalErrorScore, PlanogramsMessage.Planogram_MetaTotalErrorScore);
        /// <summary>
        /// Gets/Sets the meta data MetaTotalErrorScore
        /// </summary>
        public Int32? MetaTotalErrorScore
        {
            get { return this.GetProperty<Int32?>(MetaTotalErrorScoreProperty); }
        }
        #endregion

        #region MetaNoOfInformationEvents
        /// <summary>
        /// MetaNoOfInformationEvents property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaNoOfInformationEventsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaNoOfInformationEvents, PlanogramsMessage.Planogram_MetaNoOfInformationEvents);
        /// <summary>
        /// Gets/Sets the meta data MetaNoOfInformationEvents
        /// </summary>
        public Int32? MetaNoOfInformationEvents
        {
            get { return this.GetProperty<Int32?>(MetaNoOfInformationEventsProperty); }
        }
        #endregion

        #region MetaNoOfDebugEvents
        /// <summary>
        /// MetaNoOfDebugEvents property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaNoOfDebugEventsProperty =
            RegisterModelProperty<Int32?>(c => c.MetaNoOfDebugEvents, PlanogramsMessage.Planogram_MetaNoOfDebugEvents);
        /// <summary>
        /// Gets/Sets the meta data MetaNoOfDebugEvents
        /// </summary>
        public Int32? MetaNoOfDebugEvents
        {
            get { return this.GetProperty<Int32?>(MetaNoOfDebugEventsProperty); }
        }
        #endregion

        #region MetaHighestErrorScore
        /// <summary>
        /// MetaHighestErrorScore property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte?> MetaHighestErrorScoreProperty =
            RegisterModelProperty<Byte?>(c => c.MetaHighestErrorScore, PlanogramsMessage.Planogram_MetaHighestErrorScore);
        /// <summary>
        /// Gets/Sets the meta data MetaHighestErrorScore
        /// </summary>
        public Byte? MetaHighestErrorScore
        {
            get { return this.GetProperty<Byte?>(MetaHighestErrorScoreProperty); }
        }
        #endregion

        #region DateMetadataCalculated

        /// <summary>
        /// DateMetadataCalculated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateMetadataCalculatedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateMetadataCalculated, Message.PlanogramInfo_DateMetadataCalculated);
        /// <summary>
        /// Gets/Sets the meta data DateMetadataCalculated
        /// </summary>
        public DateTime? DateMetadataCalculated
        {
            get { return this.GetProperty<DateTime?>(DateMetadataCalculatedProperty); }
        }

        #endregion

        #endregion

        #region Validation Properties

        /// <summary>
        /// DateValidationDataCalculated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateValidationDataCalculatedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateValidationDataCalculated, Message.PlanogramInfo_DateValidationCalculated);
        /// <summary>
        /// Gets/Sets the meta data DateValidationDataCalculated
        /// </summary>
        public DateTime? DateValidationDataCalculated
        {
            get { return this.GetProperty<DateTime?>(DateValidationDataCalculatedProperty); }
        }

        #endregion

        #region PlanogramGroupId

        /// <summary>
        /// PlanogramGroupId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> PlanogramGroupIdProperty =
            RegisterModelProperty<Int32?>(c => c.PlanogramGroupId);
        /// <summary>
        /// Returns the id of the planogram group this plan is assigned to.
        /// </summary>
        public Int32? PlanogramGroupId
        {
            get { return this.GetProperty<Int32?>(PlanogramGroupIdProperty); }
        }

        #endregion

        #region PlanogramGroupName

        /// <summary>
        /// PlanogramGroupName
        /// </summary>
        public static readonly ModelPropertyInfo<String> PlanogramGroupNameProperty =
            RegisterModelProperty<String>(c => c.PlanogramGroupName, Message.PlanogramInfo_PlanogramGroupName);
        /// <summary>
        /// Returns the name of the planogram group this plan is assigned to.
        /// </summary>
        public String PlanogramGroupName
        {
            get { return this.GetProperty<String>(PlanogramGroupNameProperty); }
        }

        #endregion

        #region IsLocked

        /// <summary>
        /// IsLocked
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsLockedProperty =
            RegisterModelProperty<Boolean>(c => c.IsLocked, Message.PlanogramInfo_IsLocked);
        /// <summary>
        /// Returns whether the current planogram is locked
        /// </summary>
        public Boolean IsLocked
        {
            get { return this.GetProperty<Boolean>(IsLockedProperty); }
        }

        #endregion

        #region LockType

        /// <summary>
        /// LockType
        /// </summary>
        public static readonly ModelPropertyInfo<PackageLockType?> LockTypeProperty =
            RegisterModelProperty<PackageLockType?>(c => c.LockType, Message.PlanogramInfo_LockType);
        /// <summary>
        /// Returns whether the current planogram is locked
        /// </summary>
        public PackageLockType? LockType
        {
            get { return this.GetProperty<PackageLockType?>(LockTypeProperty); }
        }

        #endregion

        #region LockDate

        /// <summary>
        /// LockDate
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> LockDateProperty =
            RegisterModelProperty<DateTime?>(c => c.LockDate, Message.PlanogramInfo_LockDate);
        /// <summary>
        /// Returns the date the current lock was placed on the planogram
        /// </summary>
        public DateTime? LockDate
        {
            get { return this.GetProperty<DateTime?>(LockDateProperty); }
        }

        #endregion

        #region LockUserId

        /// <summary>
        /// LockUserId
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> LockUserIdProperty =
            RegisterModelProperty<Int32?>(c => c.LockUserId, Message.PlanogramInfo_LockUserId);
        /// <summary>
        /// Returns the id of who has the current lock on the planogram
        /// </summary>
        public Int32? LockUserId
        {
            get { return this.GetProperty<Int32?>(LockUserIdProperty); }
        }

        #endregion

        #region LockUserName

        /// <summary>
        /// LockUserName
        /// </summary>
        public static readonly ModelPropertyInfo<String> LockUserNameProperty =
            RegisterModelProperty<String>(c => c.LockUserName, Message.PlanogramInfo_LockUserName);
        /// <summary>
        /// Returns the UserName of who has the current lock on the planogram
        /// </summary>
        public String LockUserName
        {
            get { return this.GetProperty<String>(LockUserNameProperty); }
        }

        #endregion

        #region LockUserDisplayName

        /// <summary>
        /// LockUserDisplayName
        /// </summary>
        public static readonly ModelPropertyInfo<String> LockUserDisplayNameProperty =
            RegisterModelProperty<String>(c => c.LockUserDisplayName, Message.PlanogramInfo_LockUserName);
        /// <summary>
        /// Returns the display name of who has the current lock on the planogram
        /// </summary>
        public String LockUserDisplayName
        {
            get { return this.GetProperty<String>(LockUserDisplayNameProperty); }
        }

        #endregion

        #region Automation Status Properties

        #region AutomationProcessingStatus
        /// <summary>
        /// AutomationProcessingStatus property definition
        /// </summary>
        public static readonly ModelPropertyInfo<ProcessingStatus> AutomationProcessingStatusProperty =
            RegisterModelProperty<ProcessingStatus>(c => c.AutomationProcessingStatus);
        /// <summary>
        /// Returns the Automation processing status
        /// </summary>
        public ProcessingStatus AutomationProcessingStatus
        {
            get { return this.ReadProperty<ProcessingStatus>(AutomationProcessingStatusProperty); }
        }
        #endregion

        #region AutomationProcessingStatusDescription
        /// <summary>
        /// AutomationProcessingStatusDescription property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> AutomationProcessingStatusDescriptionProperty =
            RegisterModelProperty<String>(c => c.AutomationProcessingStatusDescription);
        /// <summary>
        /// Returns the Automation processing status description
        /// </summary>
        public String AutomationProcessingStatusDescription
        {
            get { return this.ReadProperty<String>(AutomationProcessingStatusDescriptionProperty); }
        }
        #endregion

        #region AutomationProgressMax
        /// <summary>
        /// AutomationProgressMax property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> AutomationProgressMaxProperty =
            RegisterModelProperty<Int32>(c => c.AutomationProgressMax);
        /// <summary>
        /// Returns the maximum Automation progress value for this workpackage
        /// </summary>
        public Int32 AutomationProgressMax
        {
            get { return this.ReadProperty<Int32>(AutomationProgressMaxProperty); }
        }
        #endregion

        #region AutomationProgressCurrent
        /// <summary>
        /// AutomationProgressCurrent property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> AutomationProgressCurrentProperty =
            RegisterModelProperty<Int32>(c => c.AutomationProgressCurrent);
        /// <summary>
        /// Returns the current Automation progress value for this workpackage
        /// </summary>
        public Int32 AutomationProgressCurrent
        {
            get { return this.ReadProperty<Int32>(AutomationProgressCurrentProperty); }
        }
        #endregion

        #region AutomationProgress
        /// <summary>
        /// AutomationProgress property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AutomationProgressProperty =
            RegisterModelProperty<Single>(c => c.AutomationProgress);
        /// <summary>
        /// Returns the Automation percentage progress of this workflow
        /// </summary>
        public Single AutomationProgress
        {
            get { return this.ReadProperty<Single>(AutomationProgressProperty); }
        }
        #endregion

        #region AutomationDateStarted
        /// <summary>
        /// StartDate property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> AutomationDateStartedProperty =
            RegisterModelProperty<DateTime?>(c => c.AutomationDateStarted);
        /// <summary>
        /// Gets or sets the date the current Automation process was started
        /// </summary>
        public DateTime? AutomationDateStarted
        {
            get { return this.ReadProperty<DateTime?>(AutomationDateStartedProperty); }
        }
        #endregion

        #region AutomationDateLastUpdated
        /// <summary>
        /// Date Last Updated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> AutomationDateLastUpdatedProperty =
            RegisterModelProperty<DateTime?>(c => c.AutomationDateLastUpdated);
        /// <summary>
        /// Gets or sets the date the current Automation process was last updated
        /// </summary>
        public DateTime? AutomationDateLastUpdated
        {
            get { return this.ReadProperty<DateTime?>(AutomationDateLastUpdatedProperty); }
        }
        #endregion

        #endregion

        #region MetaData Status Properties

        #region MetaDataProcessingStatus
        /// <summary>
        /// MetaDataProcessingStatus property definition
        /// </summary>
        public static readonly ModelPropertyInfo<ProcessingStatus> MetaDataProcessingStatusProperty =
            RegisterModelProperty<ProcessingStatus>(c => c.MetaDataProcessingStatus);
        /// <summary>
        /// Returns the MetaData processing status
        /// </summary>
        public ProcessingStatus MetaDataProcessingStatus
        {
            get { return this.ReadProperty<ProcessingStatus>(MetaDataProcessingStatusProperty); }
        }
        #endregion

        #region MetaDataProcessingStatusDescription
        /// <summary>
        /// MetaDataProcessingStatusDescription property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> MetaDataProcessingStatusDescriptionProperty =
            RegisterModelProperty<String>(c => c.MetaDataProcessingStatusDescription);
        /// <summary>
        /// Returns the MetaData processing status description
        /// </summary>
        public String MetaDataProcessingStatusDescription
        {
            get { return this.ReadProperty<String>(MetaDataProcessingStatusDescriptionProperty); }
        }
        #endregion

        #region MetaDataProgressMax
        /// <summary>
        /// MetaDataProgressMax property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> MetaDataProgressMaxProperty =
            RegisterModelProperty<Int32>(c => c.MetaDataProgressMax);
        /// <summary>
        /// Returns the maximum MetaData progress value for this workpackage
        /// </summary>
        public Int32 MetaDataProgressMax
        {
            get { return this.ReadProperty<Int32>(MetaDataProgressMaxProperty); }
        }
        #endregion

        #region MetaDataProgressCurrent
        /// <summary>
        /// MetaDataProgressCurrent property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> MetaDataProgressCurrentProperty =
            RegisterModelProperty<Int32>(c => c.MetaDataProgressCurrent);
        /// <summary>
        /// Returns the current MetaData progress value for this workpackage
        /// </summary>
        public Int32 MetaDataProgressCurrent
        {
            get { return this.ReadProperty<Int32>(MetaDataProgressCurrentProperty); }
        }
        #endregion

        #region MetaDataProgress
        /// <summary>
        /// MetaDataProgress property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MetaDataProgressProperty =
            RegisterModelProperty<Single>(c => c.MetaDataProgress);
        /// <summary>
        /// Returns the MetaData percentage progress of this workflow
        /// </summary>
        public Single MetaDataProgress
        {
            get { return this.ReadProperty<Single>(MetaDataProgressProperty); }
        }
        #endregion

        #region MetaDataDateStarted
        /// <summary>
        /// StartDate property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> MetaDataDateStartedProperty =
            RegisterModelProperty<DateTime?>(c => c.MetaDataDateStarted);
        /// <summary>
        /// Gets or sets the date the current MetaData process was started
        /// </summary>
        public DateTime? MetaDataDateStarted
        {
            get { return this.ReadProperty<DateTime?>(MetaDataDateStartedProperty); }
        }
        #endregion

        #region MetaDataDateLastUpdated
        /// <summary>
        /// Date Last Updated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> MetaDataDateLastUpdatedProperty =
            RegisterModelProperty<DateTime?>(c => c.MetaDataDateLastUpdated);
        /// <summary>
        /// Gets or sets the date the current MetaData process was last updated
        /// </summary>
        public DateTime? MetaDataDateLastUpdated
        {
            get { return this.ReadProperty<DateTime?>(MetaDataDateLastUpdatedProperty); }
        }
        #endregion

        #endregion

        #region Validation Status Properties

        #region ValidationProcessingStatus
        /// <summary>
        /// ValidationProcessingStatus property definition
        /// </summary>
        public static readonly ModelPropertyInfo<ProcessingStatus> ValidationProcessingStatusProperty =
            RegisterModelProperty<ProcessingStatus>(c => c.ValidationProcessingStatus);
        /// <summary>
        /// Returns the Validation processing status
        /// </summary>
        public ProcessingStatus ValidationProcessingStatus
        {
            get { return this.ReadProperty<ProcessingStatus>(ValidationProcessingStatusProperty); }
        }
        #endregion

        #region ValidationProcessingStatusDescription
        /// <summary>
        /// ValidationProcessingStatusDescription property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ValidationProcessingStatusDescriptionProperty =
            RegisterModelProperty<String>(c => c.ValidationProcessingStatusDescription);
        /// <summary>
        /// Returns the Validation processing status description
        /// </summary>
        public String ValidationProcessingStatusDescription
        {
            get { return this.ReadProperty<String>(ValidationProcessingStatusDescriptionProperty); }
        }
        #endregion

        #region ValidationProgressMax
        /// <summary>
        /// ValidationProgressMax property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ValidationProgressMaxProperty =
            RegisterModelProperty<Int32>(c => c.ValidationProgressMax);
        /// <summary>
        /// Returns the maximum Validation progress value for this workpackage
        /// </summary>
        public Int32 ValidationProgressMax
        {
            get { return this.ReadProperty<Int32>(ValidationProgressMaxProperty); }
        }
        #endregion

        #region ValidationProgressCurrent
        /// <summary>
        /// ValidationProgressCurrent property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ValidationProgressCurrentProperty =
            RegisterModelProperty<Int32>(c => c.ValidationProgressCurrent);
        /// <summary>
        /// Returns the current Validation progress value for this workpackage
        /// </summary>
        public Int32 ValidationProgressCurrent
        {
            get { return this.ReadProperty<Int32>(ValidationProgressCurrentProperty); }
        }
        #endregion

        #region ValidationProgress
        /// <summary>
        /// ValidationProgress property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ValidationProgressProperty =
            RegisterModelProperty<Single>(c => c.ValidationProgress);
        /// <summary>
        /// Returns the Validation percentage progress of this workflow
        /// </summary>
        public Single ValidationProgress
        {
            get { return this.ReadProperty<Single>(ValidationProgressProperty); }
        }
        #endregion

        #region ValidationDateStarted
        /// <summary>
        /// StartDate property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> ValidationDateStartedProperty =
            RegisterModelProperty<DateTime?>(c => c.ValidationDateStarted);
        /// <summary>
        /// Gets or sets the date the current Validation process was started
        /// </summary>
        public DateTime? ValidationDateStarted
        {
            get { return this.ReadProperty<DateTime?>(ValidationDateStartedProperty); }
        }
        #endregion

        #region ValidationDateLastUpdated
        /// <summary>
        /// Date Last Updated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> ValidationDateLastUpdatedProperty =
            RegisterModelProperty<DateTime?>(c => c.ValidationDateLastUpdated);
        /// <summary>
        /// Gets or sets the date the current Validation process was last updated
        /// </summary>
        public DateTime? ValidationDateLastUpdated
        {
            get { return this.ReadProperty<DateTime?>(ValidationDateLastUpdatedProperty); }
        }
        #endregion

        #endregion

        #region ValidationIsOutOfDate
        /// <summary>
        /// ValidationIsOutOfDate property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> ValidationIsOutOfDateProperty =
            RegisterModelProperty<Boolean>(c => c.ValidationIsOutOfDate);
        /// <summary>
        /// Returns whether the Validation Is Out Of Date, either because the template 
        /// has been modified or because there are no validation results
        /// </summary>
        public Boolean ValidationIsOutOfDate
        {
            get { return this.ReadProperty<Boolean>(ValidationIsOutOfDateProperty); }
        }
        #endregion

        #region ClusterSchemeName
        /// <summary>
        /// ClusterSchemeName Property Definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ClusterSchemeNameProperty =
            RegisterModelProperty<String>(c => c.ClusterSchemeName, Message.PlanogramInfo_ClusterSchemeName);

        /// <summary>
        /// Gets/Sets Planogram ClusterScheme 
        /// </summary>
        public String ClusterSchemeName
        {
            get { return this.GetProperty<String>(ClusterSchemeNameProperty); }
        }
        #endregion

        #region ClusterName
        /// <summary>
        /// ClusterName Property Definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ClusterNameProperty =
            RegisterModelProperty<String>(c => c.ClusterName, Message.PlanogramInfo_ClusterName); 

        /// <summary>
        /// Gets/Sets Planogram Cluster Name
        /// </summary>
        public String ClusterName
        {
            get { return this.GetProperty<String>(ClusterNameProperty); }
        }
        #endregion

        #region LocationCode property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<String> LocationCodeProperty =
            RegisterModelProperty<String>(c => c.LocationCode, Message.PlanogramInfo_LocationCode);
        /// <summary>
        /// 
        /// </summary>
        public String LocationCode
        {
            get { return this.GetProperty<String>(LocationCodeProperty); }
        }

        #endregion

        #region LocationName property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<String> LocationNameProperty =
            RegisterModelProperty<String>(c => c.LocationName, Message.PlanogramInfo_LocationName);
        /// <summary>
        /// 
        /// </summary>
        public String LocationName
        {
            get { return this.GetProperty<String>(LocationNameProperty); }
        }

        #endregion

        #region DateWIP property

        /// <summary>
        /// DateWIP property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateWipProperty =
            RegisterModelProperty<DateTime?>(c => c.DateWip, Message.PlanogramInfo_DateWip,
            ModelPropertyDisplayType.Date);
        /// <summary>
        /// Gets the date on which the planogram was set to work in progress.
        /// </summary>
        public DateTime? DateWip
        {
            get { return this.GetProperty<DateTime?>(DateWipProperty); }
        }

        #endregion

        #region DateApproved property

        /// <summary>
        /// DateApproved property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateApprovedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateApproved, Message.PlanogramInfo_DateApproved, 
            ModelPropertyDisplayType.Date);
        /// <summary>
        /// Gets the date on which the planogram was set to approved.
        /// </summary>
        public DateTime? DateApproved
        {
            get { return this.GetProperty<DateTime?>(DateApprovedProperty); }
        }

        #endregion

        #region DateArchived property

        /// <summary>
        /// DateArchived property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateArchivedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateArchived, Message.PlanogramInfo_DateArchived,
            ModelPropertyDisplayType.Date);
        /// <summary>
        /// Gets the date on which the planogram was set to archived
        /// </summary>
        public DateTime? DateArchived
        {
            get { return this.GetProperty<DateTime?>(DateArchivedProperty); }
        }

        #endregion

        #region PlanogramType property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramType> PlanogramTypeProperty =
            RegisterModelProperty<PlanogramType>(c => c.PlanogramType, Message.PlanogramInfo_PlanogramType);
        /// <summary>
        /// 
        /// </summary>
        public PlanogramType PlanogramType
        {
            get { return this.GetProperty<PlanogramType>(PlanogramTypeProperty); }
        }

        #endregion

        #region UniqueContentReference property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Guid> UniqueContentReferenceProperty =
            RegisterModelProperty<Guid>(c => c.UniqueContentReference, Message.PlanogramInfo_UniqueContentReference);
        /// <summary>
        /// 
        /// </summary>
        public Guid UniqueContentReference
        {
            get { return this.GetProperty<Guid>(UniqueContentReferenceProperty); }
        }

        #endregion

        #region MostRecentLinkedWorkpackageName property

        /// <summary>
        /// MostRecentLinkedWorkpackageName
        /// </summary>
        public static readonly ModelPropertyInfo<String> MostRecentLinkedWorkpackageNameProperty =
            RegisterModelProperty<String>(c => c.MostRecentLinkedWorkpackageName, Message.PlanogramInfo_MostRecentLinkedWorkpackageName);
        /// <summary>
        /// Returns the name of the most recent workpackage the planogram is linked to
        /// </summary>
        public String MostRecentLinkedWorkpackageName
        {
            get { return this.GetProperty<String>(MostRecentLinkedWorkpackageNameProperty); }
        }

        #endregion

        #region Highlight Sequence Strategy
        /// <summary>
        /// The name of the highlight that has most recently been applied to the plan.
        /// </summary>
        public static readonly ModelPropertyInfo<String> HighlightSequenceStrategyProperty =
            RegisterModelProperty<String>(c => c.HighlightSequenceStrategy, Message.PlanogramInfo_HighlightSequenceStrategy);
        /// <summary>
        /// The name of the highlight that has most recently been applied to the plan.
        /// </summary>
        public String HighlightSequenceStrategy
        {
            get { return this.GetProperty<String>(HighlightSequenceStrategyProperty); }
        }
        #endregion

        #region Assigned Location Count Property
        /// <summary>
        /// The number of distinct stores that are assigned to this plan via location plan assignment.
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> AssignedLocationCountProperty =
            RegisterModelProperty<Int32>(c => c.AssignedLocationCount, Message.PlanogramInfo_AssignedLocationCount);
        /// <summary>
        /// The number of distinct stores that are assigned to this plan via location plan assignment
        /// </summary>
        public Int32 AssignedLocationCount
        {
            get { return this.GetProperty<Int32>(AssignedLocationCountProperty); }
        }
        #endregion

        #region MetaCountOfPlacedProductsNotRecommendedInAssortment
        /// <summary>
        /// MetaCountOfPlacedProductsNotRecommendedInAssortment property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfPlacedProductsNotRecommendedInAssortmentProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCountOfPlacedProductsNotRecommendedInAssortment, Message.PlanogramInfo_MetaCountOfPlacedProductsNotRecommendedInAssortment);

        /// <summary>
        /// Gets/Sets MetaCountOfPlacedProductsNotRecommendedInAssortment value
        /// </summary>
        public Int32? MetaCountOfPlacedProductsNotRecommendedInAssortment
        {
            get { return this.GetProperty<Int32?>(MetaCountOfPlacedProductsNotRecommendedInAssortmentProperty); } 
        }
        #endregion

        #region MetaCountOfPlacedProductsRecommendedInAssortment
        /// <summary>
        /// MetaCountOfPlacedProductsRecommendedInAssortment property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfPlacedProductsRecommendedInAssortmentProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCountOfPlacedProductsRecommendedInAssortment, Message.PlanogramInfo_MetaCountOfPlacedProductsRecommendedInAssortment);

        /// <summary>
        /// Gets/Sets MetaCountOfPlacedProductsRecommendedInAssortment value
        /// </summary>
        public Int32? MetaCountOfPlacedProductsRecommendedInAssortment
        {
            get { return this.GetProperty<Int32?>(MetaCountOfPlacedProductsRecommendedInAssortmentProperty); } 
        }
        #endregion

        #region MetaCountOfPositionsOutsideOfBlockSpace
        /// <summary>
        /// MetaCountOfPositionsOutsideOfBlockSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfPositionsOutsideOfBlockSpaceProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCountOfPositionsOutsideOfBlockSpace, Message.PlanogramInfo_MetaCountOfPositionsOutsideOfBlockSpace);

        /// <summary>
        /// Gets/Sets MetaCountOfPositionsOutsideOfBlockSpace value
        /// </summary>
        public Int32? MetaCountOfPositionsOutsideOfBlockSpace
        {
            get { return this.GetProperty<Int32?>(MetaCountOfPositionsOutsideOfBlockSpaceProperty); }
        }
        #endregion

        #region MetaCountOfProductNotAchievedCases
        /// <summary>
        /// MetaCountOfProductNotAchievedCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfProductNotAchievedCasesProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCountOfProductNotAchievedCases, Message.PlanogramInfo_MetaCountOfProductNotAchievedCases);

        /// <summary>
        /// Gets/Sets MetaCountOfProductNotAchievedCases value
        /// </summary>
        public Int32? MetaCountOfProductNotAchievedCases
        {
            get { return this.GetProperty<Int32?>(MetaCountOfProductNotAchievedCasesProperty); }
        }
        #endregion

        #region MetaCountOfProductNotAchievedDeliveries
        /// <summary>
        /// MetaCountOfProductNotAchievedDeliveries property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfProductNotAchievedDeliveriesProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCountOfProductNotAchievedDeliveries, Message.PlanogramInfo_MetaCountOfProductNotAchievedDeliveries);

        /// <summary>
        /// Gets/Sets MetaCountOfProductNotAchievedDeliveries value
        /// </summary>
        public Int32? MetaCountOfProductNotAchievedDeliveries
        {
            get { return this.GetProperty<Int32?>(MetaCountOfProductNotAchievedDeliveriesProperty); }
        }
        #endregion

        #region MetaCountOfProductNotAchievedDOS
        /// <summary>
        /// MetaCountOfProductNotAchievedDOS property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfProductNotAchievedDOSProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCountOfProductNotAchievedDOS, Message.PlanogramInfo_MetaCountOfProductNotAchievedDOS);

        /// <summary>
        /// Gets/Sets MetaCountOfProductNotAchievedDOS value
        /// </summary>
        public Int32? MetaCountOfProductNotAchievedDOS
        {
            get { return this.GetProperty<Int32?>(MetaCountOfProductNotAchievedDOSProperty); }
        }
        #endregion

        #region MetaCountOfProductOverShelfLifePercent
        /// <summary>
        /// MetaCountOfProductOverShelfLifePercent property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfProductOverShelfLifePercentProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCountOfProductOverShelfLifePercent, Message.PlanogramInfo_MetaCountOfProductOverShelfLifePercent);

        /// <summary>
        /// Gets/Sets MetaCountOfProductOverShelfLifePercent value
        /// </summary>
        public Int32? MetaCountOfProductOverShelfLifePercent
        {
            get { return this.GetProperty<Int32?>(MetaCountOfProductOverShelfLifePercentProperty); }
        }
        #endregion

        #region MetaCountOfRecommendedProductsInAssortment
        /// <summary>
        /// MetaCountOfRecommendedProductsInAssortment property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> MetaCountOfRecommendedProductsInAssortmentProperty =
            RegisterModelProperty<Int32?>(c => c.MetaCountOfRecommendedProductsInAssortment, Message.PlanogramInfo_MetaCountOfRecommendedProductsInAssortment);

        /// <summary>
        /// Gets/Sets MetaCountOfRecommendedProductsInAssortment value
        /// </summary>
        public Int32? MetaCountOfRecommendedProductsInAssortment
        {
            get { return this.GetProperty<Int32?>(MetaCountOfRecommendedProductsInAssortmentProperty); }
        }
        #endregion

        #region MetaNumberOfAssortmentRulesBroken
        /// <summary>
        /// MetaNumberOfAssortmentRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfAssortmentRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfAssortmentRulesBroken, Message.PlanogramInfo_MetaNumberOfAssortmentRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfAssortmentRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfAssortmentRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfAssortmentRulesBrokenProperty); }
        }
        #endregion

        #region MetaNumberOfCoreRulesBroken
        /// <summary>
        /// MetaNumberOfCoreRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfCoreRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfCoreRulesBroken, Message.PlanogramInfo_MetaNumberOfCoreRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfCoreRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfCoreRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfCoreRulesBrokenProperty); }
        }
        #endregion

        #region MetaNumberOfDelistProductRulesBroken
        /// <summary>
        /// MetaNumberOfDelistProductRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfDelistProductRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfDelistProductRulesBroken, Message.PlanogramInfo_MetaNumberOfDelistProductRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfDelistProductRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfDelistProductRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfDelistProductRulesBrokenProperty); }
        }
        #endregion

        #region MetaNumberOfDependencyFamilyRulesBroken
        /// <summary>
        /// MetaNumberOfDependencyFamilyRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfDependencyFamilyRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfDependencyFamilyRulesBroken, Message.PlanogramInfo_MetaNumberOfDependencyFamilyRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfDependencyFamilyRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfDependencyFamilyRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfDependencyFamilyRulesBrokenProperty); }
        }
        #endregion

        #region MetaNumberOfDelistFamilyRulesBroken
        /// <summary>
        /// MetaNumberOfDelistFamilyRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfDelistFamilyRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfDelistFamilyRulesBroken, Message.PlanogramInfo_MetaNumberOfDelistFamilyRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfDelistFamilyRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfDelistFamilyRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfDelistFamilyRulesBrokenProperty); }
        }
        #endregion

        #region MetaNumberOfDistributionRulesBroken
        /// <summary>
        /// MetaNumberOfDistributionRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfDistributionRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfDistributionRulesBroken, Message.PlanogramInfo_MetaNumberOfDistributionRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfDistributionRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfDistributionRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfDistributionRulesBrokenProperty); }
        }
        #endregion

        #region MetaNumberOfFamilyRulesBroken
        /// <summary>
        /// MetaNumberOfFamilyRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfFamilyRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfFamilyRulesBroken, Message.PlanogramInfo_MetaNumberOfFamilyRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfFamilyRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfFamilyRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfFamilyRulesBrokenProperty); }
        }
        #endregion

        #region MetaNumberOfForceProductRulesBroken
        /// <summary>
        /// MetaNumberOfForceProductRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfForceProductRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfForceProductRulesBroken, Message.PlanogramInfo_MetaNumberOfForceProductRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfForceProductRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfForceProductRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfForceProductRulesBrokenProperty); }
        }
        #endregion

        #region MetaNumberOfInheritanceRulesBroken
        /// <summary>
        /// MetaNumberOfInheritanceRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfInheritanceRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfInheritanceRulesBroken, Message.PlanogramInfo_MetaNumberOfInheritanceRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfInheritanceRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfInheritanceRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfInheritanceRulesBrokenProperty); }
        }
        #endregion

        #region MetaNumberOfLocalProductRulesBroken
        /// <summary>
        /// MetaNumberOfLocalProductRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfLocalProductRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfLocalProductRulesBroken, Message.PlanogramInfo_MetaNumberOfLocalProductRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfLocalProductRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfLocalProductRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfLocalProductRulesBrokenProperty); }
        }
        #endregion

        #region MetaNumberOfMaximumProductFamilyRulesBroken
        /// <summary>
        /// MetaNumberOfMaximumProductFamilyRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfMaximumProductFamilyRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfMaximumProductFamilyRulesBroken, Message.PlanogramInfo_MetaNumberOfMaximumProductFamilyRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfMaximumProductFamilyRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfMaximumProductFamilyRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfMaximumProductFamilyRulesBrokenProperty); }
        }
        #endregion

        #region MetaNumberOfMinimumHurdleProductRulesBroken
        /// <summary>
        /// MetaNumberOfMinimumHurdleProductRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfMinimumHurdleProductRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfMinimumHurdleProductRulesBroken, Message.PlanogramInfo_MetaNumberOfMinimumHurdleProductRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfMinimumHurdleProductRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfMinimumHurdleProductRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfMinimumHurdleProductRulesBrokenProperty); }
        }
        #endregion

        #region MetaNumberOfMinimumProductFamilyRulesBroken
        /// <summary>
        /// MetaNumberOfMinimumProductFamilyRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfMinimumProductFamilyRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfMinimumProductFamilyRulesBroken, Message.PlanogramInfo_MetaNumberOfMinimumProductFamilyRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfMinimumProductFamilyRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfMinimumProductFamilyRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfMinimumProductFamilyRulesBrokenProperty); }
        }
        #endregion

        #region MetaNumberOfPreserveProductRulesBroken
        /// <summary>
        /// MetaNumberOfPreserveProductRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfPreserveProductRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfPreserveProductRulesBroken, Message.PlanogramInfo_MetaNumberOfPreserveProductRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfPreserveProductRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfPreserveProductRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfPreserveProductRulesBrokenProperty); }
        }
        #endregion

        #region MetaNumberOfProductRulesBroken
        /// <summary>
        /// MetaNumberOfProductRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaNumberOfProductRulesBrokenProperty =
            RegisterModelProperty<Int16?>(c => c.MetaNumberOfProductRulesBroken, Message.PlanogramInfo_MetaNumberOfProductRulesBroken,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaNumberOfProductRulesBroken value
        /// </summary>
        public Int16? MetaNumberOfProductRulesBroken
        {
            get { return this.GetProperty<Int16?>(MetaNumberOfProductRulesBrokenProperty); }
        }
        #endregion

        #region MetaPercentageOfAssortmentRulesBroken
        /// <summary>
        /// MetaPercentageOfAssortmentRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentageOfAssortmentRulesBrokenProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentageOfAssortmentRulesBroken, Message.PlanogramInfo_MetaPercentageOfAssortmentRulesBroken,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentageOfAssortmentRulesBroken value
        /// </summary>
        public Single? MetaPercentageOfAssortmentRulesBroken
        {
            get { return this.GetProperty<Single?>(MetaPercentageOfAssortmentRulesBrokenProperty); }
        }
        #endregion

        #region MetaPercentageOfCoreRulesBroken
        /// <summary>
        /// MetaPercentageOfCoreRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentageOfCoreRulesBrokenProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentageOfCoreRulesBroken, Message.PlanogramInfo_MetaPercentageOfCoreRulesBroken,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentageOfCoreRulesBroken value
        /// </summary>
        public Single? MetaPercentageOfCoreRulesBroken
        {
            get { return this.GetProperty<Single?>(MetaPercentageOfCoreRulesBrokenProperty); }
        }
        #endregion

        #region MetaPercentageOfDistributionRulesBroken
        /// <summary>
        /// MetaPercentageOfDistributionRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentageOfDistributionRulesBrokenProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentageOfDistributionRulesBroken, Message.PlanogramInfo_MetaPercentageOfDistributionRulesBroken,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentageOfDistributionRulesBroken value
        /// </summary>
        public Single? MetaPercentageOfDistributionRulesBroken
        {
            get { return this.GetProperty<Single?>(MetaPercentageOfDistributionRulesBrokenProperty); }
        }
        #endregion

        #region MetaPercentageOfFamilyRulesBroken
        /// <summary>
        /// MetaPercentageOfFamilyRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentageOfFamilyRulesBrokenProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentageOfFamilyRulesBroken, Message.PlanogramInfo_MetaPercentageOfFamilyRulesBroken,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentageOfFamilyRulesBroken value
        /// </summary>
        public Single? MetaPercentageOfFamilyRulesBroken
        {
            get { return this.GetProperty<Single?>(MetaPercentageOfFamilyRulesBrokenProperty); }
        }
        #endregion

        #region MetaPercentageOfInheritanceRulesBroken
        /// <summary>
        /// MetaPercentageOfInheritanceRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentageOfInheritanceRulesBrokenProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentageOfInheritanceRulesBroken, Message.PlanogramInfo_MetaPercentageOfInheritanceRulesBroken,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentageOfInheritanceRulesBroken value
        /// </summary>
        public Single? MetaPercentageOfInheritanceRulesBroken
        {
            get { return this.GetProperty<Single?>(MetaPercentageOfInheritanceRulesBrokenProperty); }
        }
        #endregion

        #region MetaPercentageOfLocalProductRulesBroken
        /// <summary>
        /// MetaPercentageOfLocalProductRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentageOfLocalProductRulesBrokenProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentageOfLocalProductRulesBroken, Message.PlanogramInfo_MetaPercentageOfLocalProductRulesBroken,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentageOfLocalProductRulesBroken value
        /// </summary>
        public Single? MetaPercentageOfLocalProductRulesBroken
        {
            get { return this.GetProperty<Single?>(MetaPercentageOfLocalProductRulesBrokenProperty); }
        }
        #endregion

        #region MetaPercentageOfProductRulesBroken
        /// <summary>
        /// MetaPercentageOfProductRulesBroken property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentageOfProductRulesBrokenProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentageOfProductRulesBroken, Message.PlanogramInfo_MetaPercentageOfProductRulesBroken,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentageOfProductRulesBroken value
        /// </summary>
        public Single? MetaPercentageOfProductRulesBroken
        {
            get { return this.GetProperty<Single?>(MetaPercentageOfProductRulesBrokenProperty); }
        }
        #endregion

        #region MetaPercentOfPlacedProductsRecommendedInAssortment
        /// <summary>
        /// MetaPercentOfPlacedProductsRecommendedInAssortment property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentOfPlacedProductsRecommendedInAssortmentProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentOfPlacedProductsRecommendedInAssortment,
            Message.PlanogramInfo_MetaPercentOfPlacedProductsRecommendedInAssortment, ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentOfPlacedProductsRecommendedInAssortment value
        /// </summary>
        public Single? MetaPercentOfPlacedProductsRecommendedInAssortment
        {
            get { return this.GetProperty<Single?>(MetaPercentOfPlacedProductsRecommendedInAssortmentProperty); }
        }
        #endregion

        #region MetaPercentOfPositionsOutsideOfBlockSpace
        /// <summary>
        /// MetaPercentOfPositionsOutsideOfBlockSpace property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentOfPositionsOutsideOfBlockSpaceProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentOfPositionsOutsideOfBlockSpace,
            Message.PlanogramInfo_MetaPercentOfPositionsOutsideOfBlockSpace, ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentOfPositionsOutsideOfBlockSpace value
        /// </summary>
        public Single? MetaPercentOfPositionsOutsideOfBlockSpace
        {
            get { return this.GetProperty<Single?>(MetaPercentOfPositionsOutsideOfBlockSpaceProperty); }
        }
        #endregion

        #region MetaPercentOfProductNotAchievedCases
        /// <summary>
        /// MetaPercentOfProductNotAchievedCases property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentOfProductNotAchievedCasesProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentOfProductNotAchievedCases, Message.PlanogramInfo_MetaPercentOfProductNotAchievedCases,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentOfProductNotAchievedCases value
        /// </summary>
        public Single? MetaPercentOfProductNotAchievedCases
        {
            get { return this.GetProperty<Single?>(MetaPercentOfProductNotAchievedCasesProperty); }
        }
        #endregion

        #region MetaPercentOfProductNotAchievedDeliveries
        /// <summary>
        /// MetaPercentOfProductNotAchievedDeliveries property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentOfProductNotAchievedDeliveriesProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentOfProductNotAchievedDeliveries, Message.PlanogramInfo_MetaPercentOfProductNotAchievedDeliveries,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentOfProductNotAchievedDeliveries value
        /// </summary>
        public Single? MetaPercentOfProductNotAchievedDeliveries
        {
            get { return this.GetProperty<Single?>(MetaPercentOfProductNotAchievedDeliveriesProperty); }
        }
        #endregion

        #region MetaPercentOfProductNotAchievedDOS
        /// <summary>
        /// MetaPercentOfProductNotAchievedDOS property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentOfProductNotAchievedDOSProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentOfProductNotAchievedDOS, Message.PlanogramInfo_MetaPercentOfProductNotAchievedDOS,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentOfProductNotAchievedDOS value
        /// </summary>
        public Single? MetaPercentOfProductNotAchievedDOS
        {
            get { return this.GetProperty<Single?>(MetaPercentOfProductNotAchievedDOSProperty); }
        }
        #endregion

        #region MetaPercentOfProductNotAchievedInventory
        /// <summary>
        /// MetaPercentOfProductNotAchievedInventory property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentOfProductNotAchievedInventoryProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentOfProductNotAchievedInventory, Message.PlanogramInfo_MetaPercentOfProductNotAchievedInventory,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentOfProductNotAchievedInventory value
        /// </summary>
        public Single? MetaPercentOfProductNotAchievedInventory
        {
            get { return this.GetProperty<Single?>(MetaPercentOfProductNotAchievedInventoryProperty); }
        }
        #endregion

        #region MetaPercentOfProductOverShelfLifePercent
        /// <summary>
        /// MetaPercentOfProductOverShelfLifePercent property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> MetaPercentOfProductOverShelfLifePercentProperty =
            RegisterModelProperty<Single?>(c => c.MetaPercentOfProductOverShelfLifePercent, Message.PlanogramInfo_MetaPercentOfProductOverShelfLifePercent,
            ModelPropertyDisplayType.Percentage);

        /// <summary>
        /// Gets/Sets MetaPercentOfProductOverShelfLifePercent value
        /// </summary>
        public Single? MetaPercentOfProductOverShelfLifePercent
        {
            get { return this.GetProperty<Single?>(MetaPercentOfProductOverShelfLifePercentProperty); }
        }
        #endregion

        #region MetaHasComponentsOutsideOfFixtureArea
        /// <summary>
        /// MetaHasComponentsOutsideOfFixtureArea property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> MetaHasComponentsOutsideOfFixtureAreaProperty =
            RegisterModelProperty<Boolean?>(c => c.MetaHasComponentsOutsideOfFixtureArea, Message.PlanogramInfo_MetaHasComponentsOutsideOfFixtureArea);

        /// <summary>
        /// Gets/Sets MetaHasComponentsOutsideOfFixtureArea value
        /// </summary>
        public Boolean? MetaHasComponentsOutsideOfFixtureArea
        {
            get { return this.GetProperty<Boolean?>(MetaHasComponentsOutsideOfFixtureAreaProperty); }
        }
        #endregion

        #region MetaCountOfProductsBuddied
        /// <summary>
        /// MetaCountOfProductsBuddied property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> MetaCountOfProductsBuddiedProperty =
            RegisterModelProperty<Int16?>(c => c.MetaCountOfProductsBuddied, Message.PlanogramInfo_MetaCountOfProductsBuddied,
            ModelPropertyDisplayType.None);

        /// <summary>
        /// Gets/Sets MetaCountOfProductsBuddied value
        /// </summary>
        public Int16? MetaCountOfProductsBuddied
        {
            get { return this.GetProperty<Int16?>(MetaCountOfProductsBuddiedProperty); }
        }
        #endregion

        #region Custom Values

        #region Text Properties

        #region Text1

        /// <summary>
        /// Text1 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text1Property =
            RegisterModelProperty<String>(c => c.Text1,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, "01"));

        /// <summary>
        /// Gets/Sets the Text1 value
        /// </summary>
        public String Text1
        {
            get { return GetProperty<String>(Text1Property); }

        }

        #endregion

        #region Text2

        /// <summary>
        /// Text2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text2Property =
            RegisterModelProperty<String>(c => c.Text2,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, "02"));

        /// <summary>
        /// Gets/Sets the Text2 value
        /// </summary>
        public String Text2
        {
            get { return GetProperty<String>(Text2Property); }

        }

        #endregion

        #region Text3

        /// <summary>
        /// Text3 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text3Property =
            RegisterModelProperty<String>(c => c.Text3,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, "03"));

        /// <summary>
        /// Gets/Sets the Text3 value
        /// </summary>
        public String Text3
        {
            get { return GetProperty<String>(Text3Property); }

        }

        #endregion

        #region Text4

        /// <summary>
        /// Text4 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text4Property =
            RegisterModelProperty<String>(c => c.Text4,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, "04"));

        /// <summary>
        /// Gets/Sets the Text4 value
        /// </summary>
        public String Text4
        {
            get { return GetProperty<String>(Text4Property); }

        }

        #endregion

        #region Text5

        /// <summary>
        /// Text5 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text5Property =
            RegisterModelProperty<String>(c => c.Text5,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, "05"));

        /// <summary>
        /// Gets/Sets the Text5 value
        /// </summary>
        public String Text5
        {
            get { return GetProperty<String>(Text5Property); }

        }

        #endregion

        #region Text6

        /// <summary>
        /// Text6 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text6Property =
            RegisterModelProperty<String>(c => c.Text6,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, "06"));

        /// <summary>
        /// Gets/Sets the Text6 value
        /// </summary>
        public String Text6
        {
            get { return GetProperty<String>(Text6Property); }

        }

        #endregion

        #region Text7

        /// <summary>
        /// Text7 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text7Property =
            RegisterModelProperty<String>(c => c.Text7,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, "07"));

        /// <summary>
        /// Gets/Sets the Text7 value
        /// </summary>
        public String Text7
        {
            get { return GetProperty<String>(Text7Property); }

        }

        #endregion

        #region Text8

        /// <summary>
        /// Text8 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text8Property =
            RegisterModelProperty<String>(c => c.Text8,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, "08"));

        /// <summary>
        /// Gets/Sets the Text8 value
        /// </summary>
        public String Text8
        {
            get { return GetProperty<String>(Text8Property); }

        }

        #endregion

        #region Text9

        /// <summary>
        /// Text9 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text9Property =
            RegisterModelProperty<String>(c => c.Text9,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, "09"));

        /// <summary>
        /// Gets/Sets the Text9 value
        /// </summary>
        public String Text9
        {
            get { return GetProperty<String>(Text9Property); }

        }

        #endregion

        #region Text10

        /// <summary>
        /// Text10 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text10Property =
            RegisterModelProperty<String>(c => c.Text10,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 10));

        /// <summary>
        /// Gets/Sets the Text10 value
        /// </summary>
        public String Text10
        {
            get { return GetProperty<String>(Text10Property); }

        }

        #endregion

        #region Text11

        /// <summary>
        /// Text11 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text11Property =
            RegisterModelProperty<String>(c => c.Text11,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 11));

        /// <summary>
        /// Gets/Sets the Text11 value
        /// </summary>
        public String Text11
        {
            get { return GetProperty<String>(Text11Property); }

        }

        #endregion

        #region Text12

        /// <summary>
        /// Text12 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text12Property =
            RegisterModelProperty<String>(c => c.Text12,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 12));

        /// <summary>
        /// Gets/Sets the Text12 value
        /// </summary>
        public String Text12
        {
            get { return GetProperty<String>(Text12Property); }

        }

        #endregion

        #region Text13

        /// <summary>
        /// Text13 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text13Property =
            RegisterModelProperty<String>(c => c.Text13,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 13));

        /// <summary>
        /// Gets/Sets the Text13 value
        /// </summary>
        public String Text13
        {
            get { return GetProperty<String>(Text13Property); }

        }

        #endregion

        #region Text14

        /// <summary>
        /// Text14 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text14Property =
            RegisterModelProperty<String>(c => c.Text14,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 14));

        /// <summary>
        /// Gets/Sets the Text14 value
        /// </summary>
        public String Text14
        {
            get { return GetProperty<String>(Text14Property); }

        }

        #endregion

        #region Text15

        /// <summary>
        /// Text15 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text15Property =
            RegisterModelProperty<String>(c => c.Text15,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 15));

        /// <summary>
        /// Gets/Sets the Text15 value
        /// </summary>
        public String Text15
        {
            get { return GetProperty<String>(Text15Property); }

        }

        #endregion

        #region Text16

        /// <summary>
        /// Text16 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text16Property =
            RegisterModelProperty<String>(c => c.Text16,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 16));

        /// <summary>
        /// Gets/Sets the Text16 value
        /// </summary>
        public String Text16
        {
            get { return GetProperty<String>(Text16Property); }

        }

        #endregion

        #region Text17

        /// <summary>
        /// Text17 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text17Property =
            RegisterModelProperty<String>(c => c.Text17,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 17));

        /// <summary>
        /// Gets/Sets the Text17 value
        /// </summary>
        public String Text17
        {
            get { return GetProperty<String>(Text17Property); }

        }

        #endregion

        #region Text18

        /// <summary>
        /// Text18 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text18Property =
            RegisterModelProperty<String>(c => c.Text18,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 18));

        /// <summary>
        /// Gets/Sets the Text18 value
        /// </summary>
        public String Text18
        {
            get { return GetProperty<String>(Text18Property); }

        }

        #endregion

        #region Text19

        /// <summary>
        /// Text19 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text19Property =
            RegisterModelProperty<String>(c => c.Text19,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 19));

        /// <summary>
        /// Gets/Sets the Text19 value
        /// </summary>
        public String Text19
        {
            get { return GetProperty<String>(Text19Property); }

        }

        #endregion

        #region Text20

        /// <summary>
        /// Text20 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text20Property =
            RegisterModelProperty<String>(c => c.Text20,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 20));

        /// <summary>
        /// Gets/Sets the Text20 value
        /// </summary>
        public String Text20
        {
            get { return GetProperty<String>(Text20Property); }

        }

        #endregion

        #region Text21

        /// <summary>
        /// Text21 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text21Property =
            RegisterModelProperty<String>(c => c.Text21,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 21));

        /// <summary>
        /// Gets/Sets the Text21 value
        /// </summary>
        public String Text21
        {
            get { return GetProperty<String>(Text21Property); }

        }

        #endregion

        #region Text22

        /// <summary>
        /// Text22 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text22Property =
            RegisterModelProperty<String>(c => c.Text22,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 22));

        /// <summary>
        /// Gets/Sets the Text22 value
        /// </summary>
        public String Text22
        {
            get { return GetProperty<String>(Text22Property); }

        }

        #endregion

        #region Text23

        /// <summary>
        /// Text23 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text23Property =
            RegisterModelProperty<String>(c => c.Text23,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 23));

        /// <summary>
        /// Gets/Sets the Text23 value
        /// </summary>
        public String Text23
        {
            get { return GetProperty<String>(Text23Property); }

        }

        #endregion

        #region Text24

        /// <summary>
        /// Text24 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text24Property =
            RegisterModelProperty<String>(c => c.Text24,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 24));

        /// <summary>
        /// Gets/Sets the Text24 value
        /// </summary>
        public String Text24
        {
            get { return GetProperty<String>(Text24Property); }

        }

        #endregion

        #region Text25

        /// <summary>
        /// Text25 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text25Property =
            RegisterModelProperty<String>(c => c.Text25,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 25));

        /// <summary>
        /// Gets/Sets the Text25 value
        /// </summary>
        public String Text25
        {
            get { return GetProperty<String>(Text25Property); }

        }

        #endregion

        #region Text26

        /// <summary>
        /// Text26 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text26Property =
            RegisterModelProperty<String>(c => c.Text26,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 26));

        /// <summary>
        /// Gets/Sets the Text26 value
        /// </summary>
        public String Text26
        {
            get { return GetProperty<String>(Text26Property); }

        }

        #endregion

        #region Text27

        /// <summary>
        /// Text27 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text27Property =
            RegisterModelProperty<String>(c => c.Text27,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 27));

        /// <summary>
        /// Gets/Sets the Text27 value
        /// </summary>
        public String Text27
        {
            get { return GetProperty<String>(Text27Property); }

        }

        #endregion

        #region Text28

        /// <summary>
        /// Text28 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text28Property =
            RegisterModelProperty<String>(c => c.Text28,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 28));

        /// <summary>
        /// Gets/Sets the Text28 value
        /// </summary>
        public String Text28
        {
            get { return GetProperty<String>(Text28Property); }

        }

        #endregion

        #region Text29

        /// <summary>
        /// Text29 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text29Property =
            RegisterModelProperty<String>(c => c.Text29,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 29));

        /// <summary>
        /// Gets/Sets the Text29 value
        /// </summary>
        public String Text29
        {
            get { return GetProperty<String>(Text29Property); }

        }

        #endregion

        #region Text30

        /// <summary>
        /// Text30 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text30Property =
            RegisterModelProperty<String>(c => c.Text30,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 30));

        /// <summary>
        /// Gets/Sets the Text30 value
        /// </summary>
        public String Text30
        {
            get { return GetProperty<String>(Text30Property); }

        }

        #endregion

        #region Text31

        /// <summary>
        /// Text31 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text31Property =
            RegisterModelProperty<String>(c => c.Text31,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 31));

        /// <summary>
        /// Gets/Sets the Text31 value
        /// </summary>
        public String Text31
        {
            get { return GetProperty<String>(Text31Property); }

        }

        #endregion

        #region Text32

        /// <summary>
        /// Text32 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text32Property =
            RegisterModelProperty<String>(c => c.Text32,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 32));

        /// <summary>
        /// Gets/Sets the Text32 value
        /// </summary>
        public String Text32
        {
            get { return GetProperty<String>(Text32Property); }

        }

        #endregion

        #region Text33

        /// <summary>
        /// Text33 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text33Property =
            RegisterModelProperty<String>(c => c.Text33,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 33));

        /// <summary>
        /// Gets/Sets the Text33 value
        /// </summary>
        public String Text33
        {
            get { return GetProperty<String>(Text33Property); }

        }

        #endregion

        #region Text34

        /// <summary>
        /// Text34 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text34Property =
            RegisterModelProperty<String>(c => c.Text34,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 34));

        /// <summary>
        /// Gets/Sets the Text34 value
        /// </summary>
        public String Text34
        {
            get { return GetProperty<String>(Text34Property); }

        }

        #endregion

        #region Text35

        /// <summary>
        /// Text35 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text35Property =
            RegisterModelProperty<String>(c => c.Text35,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 35));

        /// <summary>
        /// Gets/Sets the Text35 value
        /// </summary>
        public String Text35
        {
            get { return GetProperty<String>(Text35Property); }

        }

        #endregion

        #region Text36

        /// <summary>
        /// Text36 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text36Property =
            RegisterModelProperty<String>(c => c.Text36,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 36));

        /// <summary>
        /// Gets/Sets the Text36 value
        /// </summary>
        public String Text36
        {
            get { return GetProperty<String>(Text36Property); }

        }

        #endregion

        #region Text37

        /// <summary>
        /// Text37 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text37Property =
            RegisterModelProperty<String>(c => c.Text37,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 37));

        /// <summary>
        /// Gets/Sets the Text37 value
        /// </summary>
        public String Text37
        {
            get { return GetProperty<String>(Text37Property); }

        }

        #endregion

        #region Text38

        /// <summary>
        /// Text38 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text38Property =
            RegisterModelProperty<String>(c => c.Text38,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 38));

        /// <summary>
        /// Gets/Sets the Text38 value
        /// </summary>
        public String Text38
        {
            get { return GetProperty<String>(Text38Property); }

        }

        #endregion

        #region Text39

        /// <summary>
        /// Text39 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text39Property =
            RegisterModelProperty<String>(c => c.Text39,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 39));

        /// <summary>
        /// Gets/Sets the Text39 value
        /// </summary>
        public String Text39
        {
            get { return GetProperty<String>(Text39Property); }

        }

        #endregion

        #region Text40

        /// <summary>
        /// Text40 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text40Property =
            RegisterModelProperty<String>(c => c.Text40,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 40));

        /// <summary>
        /// Gets/Sets the Text40 value
        /// </summary>
        public String Text40
        {
            get { return GetProperty<String>(Text40Property); }

        }

        #endregion

        #region Text41

        /// <summary>
        /// Text41 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text41Property =
            RegisterModelProperty<String>(c => c.Text41,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 41));

        /// <summary>
        /// Gets/Sets the Text41 value
        /// </summary>
        public String Text41
        {
            get { return GetProperty<String>(Text41Property); }

        }

        #endregion

        #region Text42

        /// <summary>
        /// Text42 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text42Property =
            RegisterModelProperty<String>(c => c.Text42,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 42));

        /// <summary>
        /// Gets/Sets the Text42 value
        /// </summary>
        public String Text42
        {
            get { return GetProperty<String>(Text42Property); }

        }

        #endregion

        #region Text43

        /// <summary>
        /// Text43 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text43Property =
            RegisterModelProperty<String>(c => c.Text43,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 43));

        /// <summary>
        /// Gets/Sets the Text43 value
        /// </summary>
        public String Text43
        {
            get { return GetProperty<String>(Text43Property); }

        }

        #endregion

        #region Text44

        /// <summary>
        /// Text44 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text44Property =
            RegisterModelProperty<String>(c => c.Text44,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 44));

        /// <summary>
        /// Gets/Sets the Text44 value
        /// </summary>
        public String Text44
        {
            get { return GetProperty<String>(Text44Property); }

        }

        #endregion

        #region Text45

        /// <summary>
        /// Text45 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text45Property =
            RegisterModelProperty<String>(c => c.Text45,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 45));

        /// <summary>
        /// Gets/Sets the Text45 value
        /// </summary>
        public String Text45
        {
            get { return GetProperty<String>(Text45Property); }

        }

        #endregion

        #region Text46

        /// <summary>
        /// Text46 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text46Property =
            RegisterModelProperty<String>(c => c.Text46,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 46));

        /// <summary>
        /// Gets/Sets the Text46 value
        /// </summary>
        public String Text46
        {
            get { return GetProperty<String>(Text46Property); }

        }

        #endregion

        #region Text47

        /// <summary>
        /// Text47 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text47Property =
            RegisterModelProperty<String>(c => c.Text47,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 47));

        /// <summary>
        /// Gets/Sets the Text47 value
        /// </summary>
        public String Text47
        {
            get { return GetProperty<String>(Text47Property); }

        }

        #endregion

        #region Text48

        /// <summary>
        /// Text48 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text48Property =
            RegisterModelProperty<String>(c => c.Text48,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 48));

        /// <summary>
        /// Gets/Sets the Text48 value
        /// </summary>
        public String Text48
        {
            get { return GetProperty<String>(Text48Property); }

        }

        #endregion

        #region Text49

        /// <summary>
        /// Text49 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text49Property =
            RegisterModelProperty<String>(c => c.Text49,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 49));

        /// <summary>
        /// Gets/Sets the Text49 value
        /// </summary>
        public String Text49
        {
            get { return GetProperty<String>(Text49Property); }

        }

        #endregion

        #region Text50

        /// <summary>
        /// Text50 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Text50Property =
            RegisterModelProperty<String>(c => c.Text50,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Text, 50));

        /// <summary>
        /// Gets/Sets the Text50 value
        /// </summary>
        public String Text50
        {
            get { return GetProperty<String>(Text50Property); }

        }

        #endregion

        #endregion

        #region Value Properties

        #region Value1

        /// <summary>
        /// Value1 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value1Property =
            RegisterModelProperty<Single?>(c => c.Value1,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, "01"));

        /// <summary>
        /// Gets/Sets the Value1 value
        /// </summary>
        public Single? Value1
        {
            get { return GetProperty<Single?>(Value1Property); }

        }

        #endregion

        #region Value2

        /// <summary>
        /// Value2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value2Property =
            RegisterModelProperty<Single?>(c => c.Value2,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, "02"));

        /// <summary>
        /// Gets/Sets the Value2 value
        /// </summary>
        public Single? Value2
        {
            get { return GetProperty<Single?>(Value2Property); }

        }

        #endregion

        #region Value3

        /// <summary>
        /// Value3 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value3Property =
            RegisterModelProperty<Single?>(c => c.Value3,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, "03"));

        /// <summary>
        /// Gets/Sets the Value3 value
        /// </summary>
        public Single? Value3
        {
            get { return GetProperty<Single?>(Value3Property); }

        }

        #endregion

        #region Value4

        /// <summary>
        /// Value4 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value4Property =
            RegisterModelProperty<Single?>(c => c.Value4,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, "04"));

        /// <summary>
        /// Gets/Sets the Value4 value
        /// </summary>
        public Single? Value4
        {
            get { return GetProperty<Single?>(Value4Property); }

        }

        #endregion

        #region Value5

        /// <summary>
        /// Value5 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value5Property =
            RegisterModelProperty<Single?>(c => c.Value5,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, "05"));

        /// <summary>
        /// Gets/Sets the Value5 value
        /// </summary>
        public Single? Value5
        {
            get { return GetProperty<Single?>(Value5Property); }

        }

        #endregion

        #region Value6

        /// <summary>
        /// Value6 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value6Property =
            RegisterModelProperty<Single?>(c => c.Value6,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, "06"));

        /// <summary>
        /// Gets/Sets the Value6 value
        /// </summary>
        public Single? Value6
        {
            get { return GetProperty<Single?>(Value6Property); }

        }

        #endregion

        #region Value7

        /// <summary>
        /// Value7 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value7Property =
            RegisterModelProperty<Single?>(c => c.Value7,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, "07"));

        /// <summary>
        /// Gets/Sets the Value7 value
        /// </summary>
        public Single? Value7
        {
            get { return GetProperty<Single?>(Value7Property); }

        }

        #endregion

        #region Value8

        /// <summary>
        /// Value8 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value8Property =
            RegisterModelProperty<Single?>(c => c.Value8,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, "08"));

        /// <summary>
        /// Gets/Sets the Value8 value
        /// </summary>
        public Single? Value8
        {
            get { return GetProperty<Single?>(Value8Property); }

        }

        #endregion

        #region Value9

        /// <summary>
        /// Value9 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value9Property =
            RegisterModelProperty<Single?>(c => c.Value9,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, "09"));

        /// <summary>
        /// Gets/Sets the Value9 value
        /// </summary>
        public Single? Value9
        {
            get { return GetProperty<Single?>(Value9Property); }

        }

        #endregion

        #region Value10

        /// <summary>
        /// Value10 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value10Property =
            RegisterModelProperty<Single?>(c => c.Value10,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 10));

        /// <summary>
        /// Gets/Sets the Value10 value
        /// </summary>
        public Single? Value10
        {
            get { return GetProperty<Single?>(Value10Property); }

        }

        #endregion

        #region Value11

        /// <summary>
        /// Value11 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value11Property =
            RegisterModelProperty<Single?>(c => c.Value11,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 11));

        /// <summary>
        /// Gets/Sets the Value11 value
        /// </summary>
        public Single? Value11
        {
            get { return GetProperty<Single?>(Value11Property); }

        }

        #endregion

        #region Value12

        /// <summary>
        /// Value12 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value12Property =
            RegisterModelProperty<Single?>(c => c.Value12,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 12));

        /// <summary>
        /// Gets/Sets the Value12 value
        /// </summary>
        public Single? Value12
        {
            get { return GetProperty<Single?>(Value12Property); }

        }

        #endregion

        #region Value13

        /// <summary>
        /// Value13 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value13Property =
            RegisterModelProperty<Single?>(c => c.Value13,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 13));

        /// <summary>
        /// Gets/Sets the Value13 value
        /// </summary>
        public Single? Value13
        {
            get { return GetProperty<Single?>(Value13Property); }

        }

        #endregion

        #region Value14

        /// <summary>
        /// Value14 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value14Property =
            RegisterModelProperty<Single?>(c => c.Value14,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 14));

        /// <summary>
        /// Gets/Sets the Value14 value
        /// </summary>
        public Single? Value14
        {
            get { return GetProperty<Single?>(Value14Property); }

        }

        #endregion

        #region Value15

        /// <summary>
        /// Value15 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value15Property =
            RegisterModelProperty<Single?>(c => c.Value15,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 15));

        /// <summary>
        /// Gets/Sets the Value15 value
        /// </summary>
        public Single? Value15
        {
            get { return GetProperty<Single?>(Value15Property); }

        }

        #endregion

        #region Value16

        /// <summary>
        /// Value16 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value16Property =
            RegisterModelProperty<Single?>(c => c.Value16,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 16));

        /// <summary>
        /// Gets/Sets the Value16 value
        /// </summary>
        public Single? Value16
        {
            get { return GetProperty<Single?>(Value16Property); }

        }

        #endregion

        #region Value17

        /// <summary>
        /// Value17 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value17Property =
            RegisterModelProperty<Single?>(c => c.Value17,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 17));

        /// <summary>
        /// Gets/Sets the Value17 value
        /// </summary>
        public Single? Value17
        {
            get { return GetProperty<Single?>(Value17Property); }

        }

        #endregion

        #region Value18

        /// <summary>
        /// Value18 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value18Property =
            RegisterModelProperty<Single?>(c => c.Value18,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 18));

        /// <summary>
        /// Gets/Sets the Value18 value
        /// </summary>
        public Single? Value18
        {
            get { return GetProperty<Single?>(Value18Property); }

        }

        #endregion

        #region Value19

        /// <summary>
        /// Value19 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value19Property =
            RegisterModelProperty<Single?>(c => c.Value19,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 19));

        /// <summary>
        /// Gets/Sets the Value19 value
        /// </summary>
        public Single? Value19
        {
            get { return GetProperty<Single?>(Value19Property); }

        }

        #endregion

        #region Value20

        /// <summary>
        /// Value20 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value20Property =
            RegisterModelProperty<Single?>(c => c.Value20,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 20));

        /// <summary>
        /// Gets/Sets the Value20 value
        /// </summary>
        public Single? Value20
        {
            get { return GetProperty<Single?>(Value20Property); }

        }

        #endregion

        #region Value21

        /// <summary>
        /// Value21 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value21Property =
            RegisterModelProperty<Single?>(c => c.Value21,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 21));

        /// <summary>
        /// Gets/Sets the Value21 value
        /// </summary>
        public Single? Value21
        {
            get { return GetProperty<Single?>(Value21Property); }

        }

        #endregion

        #region Value22

        /// <summary>
        /// Value22 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value22Property =
            RegisterModelProperty<Single?>(c => c.Value22,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 22));

        /// <summary>
        /// Gets/Sets the Value22 value
        /// </summary>
        public Single? Value22
        {
            get { return GetProperty<Single?>(Value22Property); }

        }

        #endregion

        #region Value23

        /// <summary>
        /// Value23 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value23Property =
            RegisterModelProperty<Single?>(c => c.Value23,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 23));

        /// <summary>
        /// Gets/Sets the Value23 value
        /// </summary>
        public Single? Value23
        {
            get { return GetProperty<Single?>(Value23Property); }

        }

        #endregion

        #region Value24

        /// <summary>
        /// Value24 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value24Property =
            RegisterModelProperty<Single?>(c => c.Value24,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 24));

        /// <summary>
        /// Gets/Sets the Value24 value
        /// </summary>
        public Single? Value24
        {
            get { return GetProperty<Single?>(Value24Property); }

        }

        #endregion

        #region Value25

        /// <summary>
        /// Value25 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value25Property =
            RegisterModelProperty<Single?>(c => c.Value25,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 25));

        /// <summary>
        /// Gets/Sets the Value25 value
        /// </summary>
        public Single? Value25
        {
            get { return GetProperty<Single?>(Value25Property); }

        }

        #endregion

        #region Value26

        /// <summary>
        /// Value26 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value26Property =
            RegisterModelProperty<Single?>(c => c.Value26,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 26));

        /// <summary>
        /// Gets/Sets the Value26 value
        /// </summary>
        public Single? Value26
        {
            get { return GetProperty<Single?>(Value26Property); }

        }

        #endregion

        #region Value27

        /// <summary>
        /// Value27 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value27Property =
            RegisterModelProperty<Single?>(c => c.Value27,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 27));

        /// <summary>
        /// Gets/Sets the Value27 value
        /// </summary>
        public Single? Value27
        {
            get { return GetProperty<Single?>(Value27Property); }

        }

        #endregion

        #region Value28

        /// <summary>
        /// Value28 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value28Property =
            RegisterModelProperty<Single?>(c => c.Value28,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 28));

        /// <summary>
        /// Gets/Sets the Value28 value
        /// </summary>
        public Single? Value28
        {
            get { return GetProperty<Single?>(Value28Property); }

        }

        #endregion

        #region Value29

        /// <summary>
        /// Value29 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value29Property =
            RegisterModelProperty<Single?>(c => c.Value29,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 29));

        /// <summary>
        /// Gets/Sets the Value29 value
        /// </summary>
        public Single? Value29
        {
            get { return GetProperty<Single?>(Value29Property); }

        }

        #endregion

        #region Value30

        /// <summary>
        /// Value30 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value30Property =
            RegisterModelProperty<Single?>(c => c.Value30,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 30));

        /// <summary>
        /// Gets/Sets the Value30 value
        /// </summary>
        public Single? Value30
        {
            get { return GetProperty<Single?>(Value30Property); }

        }

        #endregion

        #region Value31

        /// <summary>
        /// Value31 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value31Property =
            RegisterModelProperty<Single?>(c => c.Value31,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 31));

        /// <summary>
        /// Gets/Sets the Value31 value
        /// </summary>
        public Single? Value31
        {
            get { return GetProperty<Single?>(Value31Property); }

        }

        #endregion

        #region Value32

        /// <summary>
        /// Value32 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value32Property =
            RegisterModelProperty<Single?>(c => c.Value32,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 32));

        /// <summary>
        /// Gets/Sets the Value32 value
        /// </summary>
        public Single? Value32
        {
            get { return GetProperty<Single?>(Value32Property); }

        }

        #endregion

        #region Value33

        /// <summary>
        /// Value33 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value33Property =
            RegisterModelProperty<Single?>(c => c.Value33,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 33));

        /// <summary>
        /// Gets/Sets the Value33 value
        /// </summary>
        public Single? Value33
        {
            get { return GetProperty<Single?>(Value33Property); }

        }

        #endregion

        #region Value34

        /// <summary>
        /// Value34 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value34Property =
            RegisterModelProperty<Single?>(c => c.Value34,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 34));

        /// <summary>
        /// Gets/Sets the Value34 value
        /// </summary>
        public Single? Value34
        {
            get { return GetProperty<Single?>(Value34Property); }

        }

        #endregion

        #region Value35

        /// <summary>
        /// Value35 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value35Property =
            RegisterModelProperty<Single?>(c => c.Value35,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 35));

        /// <summary>
        /// Gets/Sets the Value35 value
        /// </summary>
        public Single? Value35
        {
            get { return GetProperty<Single?>(Value35Property); }

        }

        #endregion

        #region Value36

        /// <summary>
        /// Value36 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value36Property =
            RegisterModelProperty<Single?>(c => c.Value36,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 36));

        /// <summary>
        /// Gets/Sets the Value36 value
        /// </summary>
        public Single? Value36
        {
            get { return GetProperty<Single?>(Value36Property); }

        }

        #endregion

        #region Value37

        /// <summary>
        /// Value37 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value37Property =
            RegisterModelProperty<Single?>(c => c.Value37,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 37));

        /// <summary>
        /// Gets/Sets the Value37 value
        /// </summary>
        public Single? Value37
        {
            get { return GetProperty<Single?>(Value37Property); }

        }

        #endregion

        #region Value38

        /// <summary>
        /// Value38 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value38Property =
            RegisterModelProperty<Single?>(c => c.Value38,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 38));

        /// <summary>
        /// Gets/Sets the Value38 value
        /// </summary>
        public Single? Value38
        {
            get { return GetProperty<Single?>(Value38Property); }

        }

        #endregion

        #region Value39

        /// <summary>
        /// Value39 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value39Property =
            RegisterModelProperty<Single?>(c => c.Value39,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 39));

        /// <summary>
        /// Gets/Sets the Value39 value
        /// </summary>
        public Single? Value39
        {
            get { return GetProperty<Single?>(Value39Property); }

        }

        #endregion

        #region Value40

        /// <summary>
        /// Value40 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value40Property =
            RegisterModelProperty<Single?>(c => c.Value40,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 40));

        /// <summary>
        /// Gets/Sets the Value40 value
        /// </summary>
        public Single? Value40
        {
            get { return GetProperty<Single?>(Value40Property); }

        }

        #endregion

        #region Value41

        /// <summary>
        /// Value41 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value41Property =
            RegisterModelProperty<Single?>(c => c.Value41,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 41));

        /// <summary>
        /// Gets/Sets the Value41 value
        /// </summary>
        public Single? Value41
        {
            get { return GetProperty<Single?>(Value41Property); }

        }

        #endregion

        #region Value42

        /// <summary>
        /// Value42 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value42Property =
            RegisterModelProperty<Single?>(c => c.Value42,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 42));

        /// <summary>
        /// Gets/Sets the Value42 value
        /// </summary>
        public Single? Value42
        {
            get { return GetProperty<Single?>(Value42Property); }

        }

        #endregion

        #region Value43

        /// <summary>
        /// Value43 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value43Property =
            RegisterModelProperty<Single?>(c => c.Value43,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 43));

        /// <summary>
        /// Gets/Sets the Value43 value
        /// </summary>
        public Single? Value43
        {
            get { return GetProperty<Single?>(Value43Property); }

        }

        #endregion

        #region Value44

        /// <summary>
        /// Value44 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value44Property =
            RegisterModelProperty<Single?>(c => c.Value44,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 44));

        /// <summary>
        /// Gets/Sets the Value44 value
        /// </summary>
        public Single? Value44
        {
            get { return GetProperty<Single?>(Value44Property); }

        }

        #endregion

        #region Value45

        /// <summary>
        /// Value45 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value45Property =
            RegisterModelProperty<Single?>(c => c.Value45,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 45));

        /// <summary>
        /// Gets/Sets the Value45 value
        /// </summary>
        public Single? Value45
        {
            get { return GetProperty<Single?>(Value45Property); }

        }

        #endregion

        #region Value46

        /// <summary>
        /// Value46 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value46Property =
            RegisterModelProperty<Single?>(c => c.Value46,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 46));

        /// <summary>
        /// Gets/Sets the Value46 value
        /// </summary>
        public Single? Value46
        {
            get { return GetProperty<Single?>(Value46Property); }

        }

        #endregion

        #region Value47

        /// <summary>
        /// Value47 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value47Property =
            RegisterModelProperty<Single?>(c => c.Value47,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 47));

        /// <summary>
        /// Gets/Sets the Value47 value
        /// </summary>
        public Single? Value47
        {
            get { return GetProperty<Single?>(Value47Property); }

        }

        #endregion

        #region Value48

        /// <summary>
        /// Value48 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value48Property =
            RegisterModelProperty<Single?>(c => c.Value48,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 48));

        /// <summary>
        /// Gets/Sets the Value48 value
        /// </summary>
        public Single? Value48
        {
            get { return GetProperty<Single?>(Value48Property); }

        }

        #endregion

        #region Value49

        /// <summary>
        /// Value49 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value49Property =
            RegisterModelProperty<Single?>(c => c.Value49,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 49));

        /// <summary>
        /// Gets/Sets the Value49 value
        /// </summary>
        public Single? Value49
        {
            get { return GetProperty<Single?>(Value49Property); }

        }

        #endregion

        #region Value50

        /// <summary>
        /// Value50 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> Value50Property =
            RegisterModelProperty<Single?>(c => c.Value50,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Value, 50));

        /// <summary>
        /// Gets/Sets the Value50 value
        /// </summary>
        public Single? Value50
        {
            get { return GetProperty<Single?>(Value50Property); }

        }

        #endregion

        #endregion

        #region Flag Properties

        #region Flag1

        /// <summary>
        /// Flag1 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> Flag1Property =
            RegisterModelProperty<Boolean?>(c => c.Flag1,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Flag, "01"));

        /// <summary>
        /// Gets/Sets the Flag1 value
        /// </summary>
        public Boolean? Flag1
        {
            get { return GetProperty<Boolean?>(Flag1Property); }

        }

        #endregion

        #region Flag2

        /// <summary>
        /// Flag2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> Flag2Property =
            RegisterModelProperty<Boolean?>(c => c.Flag2,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Flag, "02"));

        /// <summary>
        /// Gets/Sets the Flag2 value
        /// </summary>
        public Boolean? Flag2
        {
            get { return GetProperty<Boolean?>(Flag2Property); }

        }

        #endregion

        #region Flag3

        /// <summary>
        /// Flag3 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> Flag3Property =
            RegisterModelProperty<Boolean?>(c => c.Flag3,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Flag, "03"));

        /// <summary>
        /// Gets/Sets the Flag3 value
        /// </summary>
        public Boolean? Flag3
        {
            get { return GetProperty<Boolean?>(Flag3Property); }

        }

        #endregion

        #region Flag4

        /// <summary>
        /// Flag4 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> Flag4Property =
            RegisterModelProperty<Boolean?>(c => c.Flag4,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Flag, "04"));

        /// <summary>
        /// Gets/Sets the Flag4 value
        /// </summary>
        public Boolean? Flag4
        {
            get { return GetProperty<Boolean?>(Flag4Property); }

        }

        #endregion

        #region Flag5

        /// <summary>
        /// Flag5 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> Flag5Property =
            RegisterModelProperty<Boolean?>(c => c.Flag5,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Flag, "05"));

        /// <summary>
        /// Gets/Sets the Flag5 value
        /// </summary>
        public Boolean? Flag5
        {
            get { return GetProperty<Boolean?>(Flag5Property); }

        }

        #endregion

        #region Flag6

        /// <summary>
        /// Flag6 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> Flag6Property =
            RegisterModelProperty<Boolean?>(c => c.Flag6,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Flag, "06"));

        /// <summary>
        /// Gets/Sets the Flag6 value
        /// </summary>
        public Boolean? Flag6
        {
            get { return GetProperty<Boolean?>(Flag6Property); }

        }

        #endregion

        #region Flag7

        /// <summary>
        /// Flag7 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> Flag7Property =
            RegisterModelProperty<Boolean?>(c => c.Flag7,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Flag, "07"));

        /// <summary>
        /// Gets/Sets the Flag7 value
        /// </summary>
        public Boolean? Flag7
        {
            get { return GetProperty<Boolean?>(Flag7Property); }

        }

        #endregion

        #region Flag8

        /// <summary>
        /// Flag8 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> Flag8Property =
            RegisterModelProperty<Boolean?>(c => c.Flag8,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Flag, "08"));

        /// <summary>
        /// Gets/Sets the Flag8 value
        /// </summary>
        public Boolean? Flag8
        {
            get { return GetProperty<Boolean?>(Flag8Property); }

        }

        #endregion

        #region Flag9

        /// <summary>
        /// Flag9 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> Flag9Property =
            RegisterModelProperty<Boolean?>(c => c.Flag9,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Flag, "09"));

        /// <summary>
        /// Gets/Sets the Flag9 value
        /// </summary>
        public Boolean? Flag9
        {
            get { return GetProperty<Boolean?>(Flag9Property); }

        }

        #endregion

        #region Flag10

        /// <summary>
        /// Flag10 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> Flag10Property =
            RegisterModelProperty<Boolean?>(c => c.Flag10,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Flag, 10));

        /// <summary>
        /// Gets/Sets the Flag10 value
        /// </summary>
        public Boolean? Flag10
        {
            get { return GetProperty<Boolean?>(Flag10Property); }

        }

        #endregion

        #endregion

        #region Date Properties

        #region Date1

        /// <summary>
        /// Date1 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> Date1Property =
            RegisterModelProperty<DateTime?>(c => c.Date1,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Date, "01"));

        /// <summary>
        /// Gets/Sets the Date1 value
        /// </summary>
        public DateTime? Date1
        {
            get { return GetProperty<DateTime?>(Date1Property); }

        }

        #endregion

        #region Date2

        /// <summary>
        /// Date2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> Date2Property =
            RegisterModelProperty<DateTime?>(c => c.Date2,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Date, "02"));

        /// <summary>
        /// Gets/Sets the Date2 value
        /// </summary>
        public DateTime? Date2
        {
            get { return GetProperty<DateTime?>(Date2Property); }

        }

        #endregion

        #region Date3

        /// <summary>
        /// Date3 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> Date3Property =
            RegisterModelProperty<DateTime?>(c => c.Date3,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Date, "03"));

        /// <summary>
        /// Gets/Sets the Date3 value
        /// </summary>
        public DateTime? Date3
        {
            get { return GetProperty<DateTime?>(Date3Property); }

        }

        #endregion

        #region Date4

        /// <summary>
        /// Date4 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> Date4Property =
            RegisterModelProperty<DateTime?>(c => c.Date4,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Date, "04"));

        /// <summary>
        /// Gets/Sets the Date4 value
        /// </summary>
        public DateTime? Date4
        {
            get { return GetProperty<DateTime?>(Date4Property); }

        }

        #endregion

        #region Date5

        /// <summary>
        /// Date5 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> Date5Property =
            RegisterModelProperty<DateTime?>(c => c.Date5,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Date, "05"));

        /// <summary>
        /// Gets/Sets the Date5 value
        /// </summary>
        public DateTime? Date5
        {
            get { return GetProperty<DateTime?>(Date5Property); }

        }

        #endregion

        #region Date6

        /// <summary>
        /// Date6 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> Date6Property =
            RegisterModelProperty<DateTime?>(c => c.Date6,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Date, "06"));

        /// <summary>
        /// Gets/Sets the Date6 value
        /// </summary>
        public DateTime? Date6
        {
            get { return GetProperty<DateTime?>(Date6Property); }

        }

        #endregion

        #region Date7

        /// <summary>
        /// Date7 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> Date7Property =
            RegisterModelProperty<DateTime?>(c => c.Date7,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Date, "07"));

        /// <summary>
        /// Gets/Sets the Date7 value
        /// </summary>
        public DateTime? Date7
        {
            get { return GetProperty<DateTime?>(Date7Property); }

        }

        #endregion

        #region Date8

        /// <summary>
        /// Date8 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> Date8Property =
            RegisterModelProperty<DateTime?>(c => c.Date8,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Date, "08"));

        /// <summary>
        /// Gets/Sets the Date8 value
        /// </summary>
        public DateTime? Date8
        {
            get { return GetProperty<DateTime?>(Date8Property); }

        }

        #endregion

        #region Date9

        /// <summary>
        /// Date9 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> Date9Property =
            RegisterModelProperty<DateTime?>(c => c.Date9,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Date, "09"));

        /// <summary>
        /// Gets/Sets the Date9 value
        /// </summary>
        public DateTime? Date9
        {
            get { return GetProperty<DateTime?>(Date9Property); }

        }

        #endregion

        #region Date10

        /// <summary>
        /// Date10 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> Date10Property =
            RegisterModelProperty<DateTime?>(c => c.Date10,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Date, 10));

        /// <summary>
        /// Gets/Sets the Date10 value
        /// </summary>
        public DateTime? Date10
        {
            get { return GetProperty<DateTime?>(Date10Property); }

        }

        #endregion

        #endregion

        #region Note Properties

        #region Note1

        /// <summary>
        /// Note1 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Note1Property =
            RegisterModelProperty<String>(c => c.Note1,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Note, "01"));

        /// <summary>
        /// Gets/Sets the Note1 value
        /// </summary>
        public String Note1
        {
            get { return GetProperty<String>(Note1Property); }

        }

        #endregion

        #region Note2

        /// <summary>
        /// Note2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Note2Property =
            RegisterModelProperty<String>(c => c.Note2,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Note, "02"));

        /// <summary>
        /// Gets/Sets the Note2 value
        /// </summary>
        public String Note2
        {
            get { return GetProperty<String>(Note2Property); }

        }

        #endregion

        #region Note3

        /// <summary>
        /// Note3 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Note3Property =
            RegisterModelProperty<String>(c => c.Note3,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Note, "03"));

        /// <summary>
        /// Gets/Sets the Note3 value
        /// </summary>
        public String Note3
        {
            get { return GetProperty<String>(Note3Property); }

        }

        #endregion

        #region Note4

        /// <summary>
        /// Note4 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Note4Property =
            RegisterModelProperty<String>(c => c.Note4,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Note, "04"));

        /// <summary>
        /// Gets/Sets the Note4 value
        /// </summary>
        public String Note4
        {
            get { return GetProperty<String>(Note4Property); }

        }

        #endregion

        #region Note5

        /// <summary>
        /// Note5 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Note5Property =
            RegisterModelProperty<String>(c => c.Note5,
            String.Format(CultureInfo.CurrentCulture, Message.Planogram_CustomAttributeData_Note, "05"));

        /// <summary>
        /// Gets/Sets the Note5 value
        /// </summary>
        public String Note5
        {
            get { return GetProperty<String>(Note5Property); }

        }

        #endregion

        #endregion

        #endregion

        #endregion

        #region Authorization Rules

        /// <summary>
        ///     Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PlanogramInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanogramInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(PlanogramInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanogramInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }

        #endregion

        #region Factory Methods

        #region V8-27966 copying planograms, disabled for now.

        ///// <summary>
        /////     Gets a basic <see cref="PlanogramInfo"/> from a given <paramref name="planogram"/> and <paramref name="planogramGroup"/>.
        ///// </summary>
        ///// <param name="planogram">The planogram this planogram info will be representing.</param>
        ///// <param name="planogramGroup">The planogram group the planogram belongs to.</param>
        ///// <returns>A new instance of <see cref="PlanogramInfo"/>.</returns>
        //public static PlanogramInfo NewPlanogramInfo(Planogram planogram, PlanogramGroup planogramGroup)
        //{
        //    PlanogramInfoDto dto = new PlanogramInfoDto
        //    {
        //        Id = (Int32) planogram.Id,
        //        Name = planogram.Name,
        //        PlanogramGroupId = planogramGroup.Id,
        //        PlanogramGroupName = planogramGroup.Name,
        //        DateCreated = DateTime.UtcNow,
        //        Status = (Byte) PlanogramStatusType.None
        //    };
        //    return GetPlanogramInfo(null, dto);
        //}

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<IModelPropertyInfo> EnumerateDisplayablePropertyInfos()
        {
            foreach(IModelPropertyInfo info in EnumerateDisplayablePlanogramInfos())
            { 
                yield return info; 
            }

            foreach (IModelPropertyInfo info in EnumerateDisplayablePlanogramCustomInfos())
            {
                yield return info;
            }
        }

        private static IEnumerable<IModelPropertyInfo> EnumerateDisplayablePlanogramInfos()
        {
            yield return NameProperty;
            yield return StatusProperty;
            yield return UniqueContentReferenceProperty;
            yield return HeightProperty;
            yield return WidthProperty;
            yield return DepthProperty;
            yield return UserNameProperty;
            yield return DateCreatedProperty;
            yield return DateLastModifiedProperty;
            yield return CategoryCodeProperty;
            yield return CategoryNameProperty;
            yield return PlanogramGroupNameProperty;
            //yield return DateMetadataCalculatedProperty;
            //yield return DateValidationDataCalculatedProperty;
            yield return MetaBayCountProperty;
            yield return MetaUniqueProductCountProperty;
            yield return MetaComponentCountProperty;
            yield return MetaTotalMerchandisableLinearSpaceProperty;
            yield return MetaTotalMerchandisableAreaSpaceProperty;
            yield return MetaTotalMerchandisableVolumetricSpaceProperty;
            yield return MetaTotalLinearWhiteSpaceProperty;
            yield return MetaTotalAreaWhiteSpaceProperty;
            yield return MetaTotalVolumetricWhiteSpaceProperty;
            yield return MetaProductsPlacedProperty;
            yield return MetaProductsUnplacedProperty;
            //yield return MetaNewProductsProperty; // not currently populated
            //yield return MetaChangesFromPreviousCountProperty; // not currently populated
            //yield return MetaChangeFromPreviousStarRatingProperty; // not currently populated
            //yield return MetaBlocksDroppedProperty; // not currently populated
            yield return MetaNotAchievedInventoryProperty;
            yield return MetaTotalFacingsProperty;
            yield return MetaAverageFacingsProperty;
            yield return MetaAverageFrontFacingsProperty;
            yield return MetaTotalUnitsProperty;
            yield return MetaAverageUnitsProperty;
            yield return MetaMinDosProperty;
            yield return MetaMaxDosProperty;
            yield return MetaAverageDosProperty;
            yield return MetaMinCasesProperty;
            yield return MetaMaxCasesProperty;
            yield return MetaAverageCasesProperty;
            //yield return MetaSpaceToUnitsIndexProperty;  // not currently populated
            yield return MetaTotalComponentsOverMerchandisedHeightProperty;
            yield return MetaTotalComponentsOverMerchandisedWidthProperty;
            yield return MetaTotalComponentsOverMerchandisedDepthProperty;
            yield return MetaTotalComponentCollisionsProperty;
            yield return MetaTotalPositionCollisionsProperty;
            yield return MetaTotalFrontFacingsProperty;
            yield return MetaNoOfErrorsProperty;
            yield return MetaNoOfWarningsProperty;
            yield return MetaNoOfInformationEventsProperty;
            yield return MetaNoOfDebugEventsProperty;
            yield return MetaHasComponentsOutsideOfFixtureAreaProperty;

            //yield return MetaTotalErrorScoreProperty; //not in use
            //yield return MetaHighestErrorScoreProperty;//not in use

            yield return ClusterSchemeNameProperty;
            yield return ClusterNameProperty;
            yield return LocationCodeProperty;
            yield return LocationNameProperty;
            yield return DateWipProperty;
            yield return DateApprovedProperty;
            yield return DateArchivedProperty;
            yield return PlanogramTypeProperty;
            yield return MostRecentLinkedWorkpackageNameProperty;

            yield return AngleUnitsOfMeasureProperty;
            yield return HighlightSequenceStrategyProperty;
            yield return AssignedLocationCountProperty;
            yield return MetaCountOfPlacedProductsNotRecommendedInAssortmentProperty;
            yield return MetaCountOfPlacedProductsRecommendedInAssortmentProperty;
            yield return MetaCountOfPositionsOutsideOfBlockSpaceProperty;
            yield return MetaCountOfProductNotAchievedCasesProperty;
            yield return MetaCountOfProductNotAchievedDeliveriesProperty;
            yield return MetaCountOfProductNotAchievedDOSProperty;
            yield return MetaCountOfProductOverShelfLifePercentProperty;
            yield return MetaCountOfRecommendedProductsInAssortmentProperty;
            yield return MetaNumberOfAssortmentRulesBrokenProperty;
            yield return MetaNumberOfCoreRulesBrokenProperty;
            yield return MetaNumberOfDelistProductRulesBrokenProperty;
            yield return MetaNumberOfDependencyFamilyRulesBrokenProperty;
            yield return MetaNumberOfDelistFamilyRulesBrokenProperty;
            yield return MetaNumberOfDistributionRulesBrokenProperty;
            yield return MetaNumberOfFamilyRulesBrokenProperty;
            yield return MetaNumberOfForceProductRulesBrokenProperty;
            yield return MetaNumberOfInheritanceRulesBrokenProperty;
            yield return MetaNumberOfLocalProductRulesBrokenProperty;
            yield return MetaNumberOfMaximumProductFamilyRulesBrokenProperty;
            yield return MetaNumberOfMinimumHurdleProductRulesBrokenProperty;
            yield return MetaNumberOfMinimumProductFamilyRulesBrokenProperty;
            yield return MetaNumberOfPreserveProductRulesBrokenProperty;
            yield return MetaNumberOfProductRulesBrokenProperty;
            yield return MetaPercentageOfAssortmentRulesBrokenProperty;
            yield return MetaPercentageOfCoreRulesBrokenProperty;
            yield return MetaPercentageOfDistributionRulesBrokenProperty;
            yield return MetaPercentageOfFamilyRulesBrokenProperty;
            yield return MetaPercentageOfInheritanceRulesBrokenProperty;
            yield return MetaPercentageOfLocalProductRulesBrokenProperty;
            yield return MetaPercentageOfProductRulesBrokenProperty;
            yield return MetaPercentOfPlacedProductsRecommendedInAssortmentProperty;
            yield return MetaPercentOfPositionsOutsideOfBlockSpaceProperty;
            yield return MetaPercentOfProductNotAchievedCasesProperty;
            yield return MetaPercentOfProductNotAchievedDeliveriesProperty;
            yield return MetaPercentOfProductNotAchievedDOSProperty;
            yield return MetaPercentOfProductNotAchievedInventoryProperty;
            yield return MetaPercentOfProductOverShelfLifePercentProperty;
            yield return MetaCountOfProductsBuddiedProperty;
        }

        public static IEnumerable<IModelPropertyInfo> EnumerateDisplayablePlanogramCustomInfos()
        {
            yield return Text1Property;
            yield return Text2Property;
            yield return Text3Property;
            yield return Text4Property;
            yield return Text5Property;
            yield return Text6Property;
            yield return Text7Property;
            yield return Text8Property;
            yield return Text9Property;
            yield return Text10Property;
            yield return Text11Property;
            yield return Text12Property;
            yield return Text13Property;
            yield return Text14Property;
            yield return Text15Property;
            yield return Text16Property;
            yield return Text17Property;
            yield return Text18Property;
            yield return Text19Property;
            yield return Text20Property;
            yield return Text21Property;
            yield return Text22Property;
            yield return Text23Property;
            yield return Text24Property;
            yield return Text25Property;
            yield return Text26Property;
            yield return Text27Property;
            yield return Text28Property;
            yield return Text29Property;
            yield return Text30Property;
            yield return Text31Property;
            yield return Text32Property;
            yield return Text33Property;
            yield return Text34Property;
            yield return Text35Property;
            yield return Text36Property;
            yield return Text37Property;
            yield return Text38Property;
            yield return Text39Property;
            yield return Text40Property;
            yield return Text41Property;
            yield return Text42Property;
            yield return Text43Property;
            yield return Text44Property;
            yield return Text45Property;
            yield return Text46Property;
            yield return Text47Property;
            yield return Text48Property;
            yield return Text49Property;
            yield return Text50Property;

            yield return Value1Property;
            yield return Value2Property;
            yield return Value3Property;
            yield return Value4Property;
            yield return Value5Property;
            yield return Value6Property;
            yield return Value7Property;
            yield return Value8Property;
            yield return Value9Property;
            yield return Value10Property;
            yield return Value11Property;
            yield return Value12Property;
            yield return Value13Property;
            yield return Value14Property;
            yield return Value15Property;
            yield return Value16Property;
            yield return Value17Property;
            yield return Value18Property;
            yield return Value19Property;
            yield return Value20Property;
            yield return Value21Property;
            yield return Value22Property;
            yield return Value23Property;
            yield return Value24Property;
            yield return Value25Property;
            yield return Value26Property;
            yield return Value27Property;
            yield return Value28Property;
            yield return Value29Property;
            yield return Value30Property;
            yield return Value31Property;
            yield return Value32Property;
            yield return Value33Property;
            yield return Value34Property;
            yield return Value35Property;
            yield return Value36Property;
            yield return Value37Property;
            yield return Value38Property;
            yield return Value39Property;
            yield return Value40Property;
            yield return Value41Property;
            yield return Value42Property;
            yield return Value43Property;
            yield return Value44Property;
            yield return Value45Property;
            yield return Value46Property;
            yield return Value47Property;
            yield return Value48Property;
            yield return Value49Property;
            yield return Value50Property;

            yield return Flag1Property;
            yield return Flag2Property;
            yield return Flag3Property;
            yield return Flag4Property;
            yield return Flag5Property;
            yield return Flag6Property;
            yield return Flag7Property;
            yield return Flag8Property;
            yield return Flag9Property;
            yield return Flag10Property;

            yield return Date1Property;
            yield return Date2Property;
            yield return Date3Property;
            yield return Date4Property;
            yield return Date5Property;
            yield return Date6Property;
            yield return Date7Property;
            yield return Date8Property;
            yield return Date9Property;
            yield return Date10Property;

            yield return Note1Property;
            yield return Note2Property;
            yield return Note3Property;
            yield return Note4Property;
            yield return Note5Property;
        }

        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos()
        {
            String ownerFriendlyName = Planogram.FriendlyName;
            Type ownerType = typeof(PlanogramInfo);
            
            String detailsGroup = Planogram.FriendlyName;
            String customGroup = Message.PlanogramInfo_PropertyGroup_Custom;

            foreach (IModelPropertyInfo propertyInfo in PlanogramInfo.EnumerateDisplayablePlanogramInfos())
            {
                var field = ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, propertyInfo);
                field.GroupName = detailsGroup;

                yield return field;
            }
            foreach (IModelPropertyInfo propertyInfo in PlanogramInfo.EnumerateDisplayablePlanogramCustomInfos())
            {
                var field = ObjectFieldInfo.NewObjectFieldInfo(ownerType, ownerFriendlyName, propertyInfo);
                field.GroupName = customGroup;

                yield return field;
            }
        }

        #endregion

        #region Overrides

        /// <summary>
        ///     Overrides <see cref="ToString" /> to return the planogram name
        /// </summary>
        public override String ToString()
        {
            return Name;
        }

        protected override object GetIdValue()
        {
            return this.Id;
        }

        #endregion
    }
}