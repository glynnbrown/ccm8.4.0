﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
// V8-26520 : J.Pickup
//		Added fetch by product group id criteria
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// ConsumerDecisionTree info list
    /// </summary>
    [Serializable]
    public sealed partial class ConsumerDecisionTreeInfoList : ModelReadOnlyList<ConsumerDecisionTreeInfoList, ConsumerDecisionTreeInfo>
    {
        #region Criteria

        /// <summary>
        /// Our criteria object used for location all of the current reviews
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : CriteriaBase<FetchByEntityIdCriteria>
        {
            /// <summary>
            /// Entity id
            /// </summary>
            private static readonly PropertyInfo<Int32> _entityIdProperty = RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(_entityIdProperty); }
                set { LoadProperty<Int32>(_entityIdProperty, value); }
            }

            #region Constructors
            /// <summary>
            /// Creates a new instance of this criteria type
            /// </summary>
            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(_entityIdProperty, entityId);
            }
        }


        /// <summary>
        /// criteria for retrieving by product group id
        /// </summary>
        [Serializable]
        public class FetchByProductGroupIdCriteria : CriteriaBase<FetchByProductGroupIdCriteria>
        {
            /// <summary>
            /// ProductGroupId
            /// </summary>
            private static readonly PropertyInfo<Int32> _productGroupIdProperty = RegisterProperty<Int32>(c => c.ProductGroupId);
            public Int32 ProductGroupId
            {
                get { return ReadProperty<Int32>(_productGroupIdProperty); }
                set { LoadProperty<Int32>(_productGroupIdProperty, value); }
            }

            /// <summary>
            /// Creates a new instance of this criteria type
            /// </summary>
            public FetchByProductGroupIdCriteria(Int32 productGroupId)
            {
                LoadProperty<Int32>(_productGroupIdProperty, productGroupId);
            }
        }

        /// <summary>
        /// Criteria for FetchByIds
        /// </summary>
        [Serializable]
        public class FetchByIdsCriteria : Csla.CriteriaBase<FetchByIdsCriteria>
        {
            public static readonly PropertyInfo<List<Int32>> IdsProperty =
            RegisterProperty<List<Int32>>(c => c.Ids);
            public List<Int32> Ids
            {
                get { return ReadProperty<List<Int32>>(IdsProperty); }
            }

            public FetchByIdsCriteria(List<Int32> ids)
            {
                LoadProperty<List<Int32>>(IdsProperty, ids);
            }
        }

            #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
