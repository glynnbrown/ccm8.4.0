﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class LocationSpaceElementList : ModelList<LocationSpaceElementList, LocationSpaceElement>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new LocationSpaceBay Parent
        {
            get { return (LocationSpaceBay)base.Parent; }
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationSpaceElementList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationSpaceCreate.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceElementList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.LocationSpaceGet.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceElementList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationSpaceEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceElementList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationSpaceDelete.ToString()));
        }

        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new list
        /// </summary>
        public static LocationSpaceElementList NewLocationSpaceElementList(Boolean createDefaultElement = true)
        {
            LocationSpaceElementList list = new LocationSpaceElementList();
            list.Create(createDefaultElement);
            return list;
        }
        #endregion

        #region Data Access
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        #region Create
        private void Create(Boolean createDefaultElement)
        {
            MarkAsChild();
            if (createDefaultElement)
            {
                AddNew();
            }
        }
        #endregion
        #endregion

        #region Criteria

        [Serializable]
        public class FetchByLocationSpaceBayIdCriteria : CriteriaBase<FetchByLocationSpaceBayIdCriteria>
        {
            #region Properties

            public static PropertyInfo<Int32> LocationSpaceBayIdProperty =
                RegisterProperty<Int32>(c => c.LocationSpaceBayId);
            public Int32 LocationSpaceBayId
            {
                get { return ReadProperty<Int32>(LocationSpaceBayIdProperty); }
            }

            #endregion

            public FetchByLocationSpaceBayIdCriteria(Int32 locationSpaceBayId)
            {
                LoadProperty<Int32>(LocationSpaceBayIdProperty, locationSpaceBayId);
            }
        }

        #endregion
    }
}
