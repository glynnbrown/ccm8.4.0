﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27155 : L.Luong
//  Created
#endregion
#region Version History: CCM802
// V8-28766 : J.Pickup
//  Switched single to 'Manual' for user clarity.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;
namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Denotes the available product placement Y types
    /// </summary>
    public enum ProductPlacementYType
    {
        Manual = 0,
        FillHigh = 1
    }

    public static class ProductPlacementYTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<ProductPlacementYType, String> FriendlyNames =
            new Dictionary<ProductPlacementYType, String>()
            {
                {ProductPlacementYType.Manual, Message.Enum_ProductPlacementYType_Manual},
                {ProductPlacementYType.FillHigh, Message.Enum_ProductPlacementYType_FillHigh},
            };
    }
}
