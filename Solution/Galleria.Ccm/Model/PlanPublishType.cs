﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.2.0)
// V8-30732 : M.Shelley
//  Created.
// V8-31171 : M.Pettit
//	Removed explicit datatype for Reporting compatibility
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    public enum PlanPublishType
    {
        None = 0,
        PublishGFS = 1,
        //ExportPDF = 2,
        //ExportSpaceman = 3,
    }

    public static class PlanPublishTypeHelper
    {
        public static readonly Dictionary<PlanPublishType, String> FriendlyNames =
            new Dictionary<PlanPublishType, String>
            {
                {PlanPublishType.None, Message.Enum_PlanPublishType_None},
                {PlanPublishType.PublishGFS, Message.Enum_PlanPublishType_PublishGFS},
                //{PlanPublishType.ExportPDF, Message.Enum_PublishingType_ExportPDF},
                //{PlanPublishType.ExportSpaceman, Message.Enum_PublishingType_ExportSpaceman},
            };

        /// <summary>
        /// Returns the matching enum value from the specified description
        /// Returns null if not match found
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static PlanPublishType? PublishingTypeGetEnum(String description)
        {
            foreach (KeyValuePair<PlanPublishType, String> keyPair in PlanPublishTypeHelper.FriendlyNames)
            {
                if (keyPair.Value.Equals(description))
                {
                    return keyPair.Key;
                }
            }
            return null;
        }
    }
}
