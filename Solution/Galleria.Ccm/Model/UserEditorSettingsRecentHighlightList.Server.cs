﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31699 : A.Heathcote
//  Created.

#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Data;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public partial class UserEditorSettingsRecentHighlightList
    {
        #region Constructor
        private UserEditorSettingsRecentHighlightList() { }  //Forces use of Factory methods
        #endregion

        #region Factory Methods

        internal static UserEditorSettingsRecentHighlightList GetUserEditorSettingsRecentHighlight(IDalContext dalContext)
        {
            return DataPortal.FetchChild<UserEditorSettingsRecentHighlightList>(dalContext);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dalContext">This is the current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext)
        {
            RaiseListChangedEvents = false;
            using (IUserEditorSettingsRecentHighlightDal dal = dalContext.GetDal<IUserEditorSettingsRecentHighlightDal>())
            {
                foreach (UserEditorSettingsRecentHighlightDto dto in dal.FetchAll())
                {
                    this.Add(UserEditorSettingsRecentHighlight.GetUserEditorSettingsRecentHighlight(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}