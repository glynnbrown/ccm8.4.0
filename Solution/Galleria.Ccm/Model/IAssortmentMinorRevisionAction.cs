﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Interface defining common properties / methods between minor revision actions
    /// </summary>
    public interface IAssortmentMinorRevisionAction
    {
        /// <summary>
        /// Returns the action type
        /// </summary>
        AssortmentMinorRevisionActionType Type { get; }

        /// <summary>
        /// Returns the product id
        /// </summary>
        Int32? ProductId { get; }

        /// <summary>
        /// Returns the product gtin
        /// </summary>
        String ProductGtin { get; }

        /// <summary>
        /// Returns the product name
        /// </summary>
        String ProductName { get; }

        /// <summary>
        /// Gets/Sets the action priority
        /// </summary>
        Int32 Priority { get; set; }

        /// <summary>
        /// Gets/Sets the action comments
        /// </summary>
        String Comments { get; set; }

        /// <summary>
        /// Returns the action locations
        /// </summary>
        /// <remarks>This is done as a method rather than a property as mapping 
        /// to an interface to be used in a data contract does not work. Instead the 
        /// location list should be exposed via this interface method directly.</remarks>
        ICollection<IAssortmentMinorRevisionActionLocation> GetIAssortmentMinorRevisionActionLocations();
    }

}
