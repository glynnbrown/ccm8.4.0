﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25447 : N.Haywood
//  Copied over from GFS
#endregion
#region Version History: (CCM 8.03)
// V8-29268 : L.Ineson
//  Removed date deleted
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a LocationProductIllegal
    /// (Child of LocationProductIllegalList)
    /// 
    /// This is a root MasterData object and so it has the following properties/actions:
    /// - This object has a DateDeleted property and associated field in its supporting database table
    /// - If this object is deleted, it is marked as deleted (this allows for 'undeletes') and any child objects are left unchanged
    /// 
    /// NOTE: 
    /// THIS IS AN EXCEPTION OF THE DATA DELETE RULES - When a LocationProductIllegal is deleted 
    /// and another created, the object's datadeleted flag is cleared and the object is 'un-deleted'.
    /// This is because it uses a composite key based on the objects it represents (Location/product) 
    /// and cannot be mistakenly re-inserted to represent something different. This is required to allow
    /// re-insert of a deleted LocationProductIllegal item.
    /// </summary>
    [Serializable]
    public sealed partial class LocationProductIllegal : ModelObject<LocationProductIllegal>
    {
        #region Properties

        public static readonly ModelPropertyInfo<Int16> LocationIdProperty =
            RegisterModelProperty<Int16>(c => c.LocationId);
        public Int16 LocationId
        {
            get { return GetProperty<Int16>(LocationIdProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> ProductIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductId);
        public Int32 ProductId
        {
            get { return GetProperty<Int32>(ProductIdProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
        }

        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }

        /// <summary>
        /// The date when the item was created 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
        }

        /// <summary>
        /// The date when the item was modified 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
        }

        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }

        #endregion

        #region Authorisation Rules
        /// <summary>
        /// defines authorisation rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationProductIllegal), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationProductIllegalCreate.ToString()));
            BusinessRules.AddRule(typeof(LocationProductIllegal), new IsInRole(AuthorizationActions.GetObject, DomainPermission.LocationProductIllegalGet.ToString()));
            BusinessRules.AddRule(typeof(LocationProductIllegal), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationProductIllegalEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationProductIllegal), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationProductIllegalDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>New object instance</returns>
        public static LocationProductIllegal NewLocationProductIllegal(Int16 locationId, Int32 productId, Int32 entityId)
        {
            LocationProductIllegal item = new LocationProductIllegal();
            item.Create(locationId, productId, entityId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int16 locationId, Int32 productId, Int32 entityId)
        {
            LoadProperty<Int32>(EntityIdProperty, entityId);
            LoadProperty<Int16>(LocationIdProperty, locationId);
            LoadProperty<Int32>(ProductIdProperty, productId);
            LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Overrides

        public override string ToString()
        {
            return String.Format("{0},{1}", this.LocationId, this.ProductId);
        }

        #endregion
    }
}
