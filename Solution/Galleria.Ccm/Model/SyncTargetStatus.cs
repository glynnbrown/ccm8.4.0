﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public enum SyncTargetStatus
    {
        Idle = 0,
        Running = 1,
        Cancelling = 2,
        Cancelled = 3,
        Failed = 4,
        Errored = 5,
        Queued = 6
    }

    public static class SyncTargetStatusHelper
    {
        public static readonly Dictionary<SyncTargetStatus, string> FriendlyNames =
            new Dictionary<SyncTargetStatus, string>()
            {
                {SyncTargetStatus.Idle, Message.Enum_SyncTargetStatus_Idle},
                {SyncTargetStatus.Running, Message.Enum_SyncTargetStatus_Running},
                {SyncTargetStatus.Cancelling, Message.Enum_SyncTargetStatus_Cancelling},
                {SyncTargetStatus.Cancelled, Message.Enum_SyncTargetStatus_Cancelled},
                {SyncTargetStatus.Failed, Message.Enum_SyncTargetStatus_Failed},
                {SyncTargetStatus.Errored, Message.Enum_SyncTargetStatus_Errored},
                {SyncTargetStatus.Queued, Message.Enum_SyncTargetStatus_Queued}
            };

        public static readonly Dictionary<SyncTargetStatus, string> FriendlyDescriptions =
            new Dictionary<SyncTargetStatus, string>()
            {
                {SyncTargetStatus.Idle, Message.Enum_SyncTargetStatus_Idle_Description},
                {SyncTargetStatus.Running, Message.Enum_SyncTargetStatus_Running_Description},
                {SyncTargetStatus.Cancelling, Message.Enum_SyncTargetStatus_Cancelling_Description},
                {SyncTargetStatus.Cancelled, Message.Enum_SyncTargetStatus_Cancelled_Description},
                {SyncTargetStatus.Failed, Message.Enum_SyncTargetStatus_Failed_Description},
                {SyncTargetStatus.Errored, Message.Enum_SyncTargetStatus_Errored_Description},
                {SyncTargetStatus.Queued, Message.Enum_SyncTargetStatus_Queued_Description}
            };

        /// <summary>
        /// Returns the matching enum value from the specifed description
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static SyncTargetStatus? SyncTargetStatusGetEnum(string description)
        {
            foreach (KeyValuePair<SyncTargetStatus, string> keyPair in SyncTargetStatusHelper.FriendlyNames)
            {
                if (keyPair.Value.Equals(description))
                {
                    return keyPair.Key;
                }
            }
            return null;
        }
    }
}