﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25628 : A.Kuszyk
//		Created (Copied from SA)
// V8-25454 : J.Pickup
//		Added FetchByEntityIdIncludingDeletedCriteria
// V8-27918 : J.Pickup
//      Added FetchByWorkpackageIdIncludingDeleted() & FetchByWorkpackagePlanogramIdIncludingDeleted()

#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a readonly list holding LocationInfo objects
    /// (root object)
    /// </summary>
    [Serializable]
    public sealed partial class LocationInfoList : ModelReadOnlyList<LocationInfoList, LocationInfo>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(LocationInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(LocationInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(LocationInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByEntityId
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : Csla.CriteriaBase<FetchByEntityIdCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
            RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
            }
        }

        /// <summary>
        /// Criteria for FetchByEntityIdIncludingDeleted
        /// </summary>
        [Serializable]
        public class FetchByEntityIdIncludingDeletedCriteria : Csla.CriteriaBase<FetchByEntityIdIncludingDeletedCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
            RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public FetchByEntityIdIncludingDeletedCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
            }
        }
        
        /// <summary>
        /// Criteria for LocationInfoList.FetchByEntityIdLocationCodes
        /// </summary>
        [Serializable]
        public class FetchByEntityIdLocationCodesCriteria : CriteriaBase<FetchByEntityIdLocationCodesCriteria>
        {
            #region Properties

            public static PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static PropertyInfo<IEnumerable<String>> LocationCodesProperty =
                RegisterProperty<IEnumerable<String>>(c => c.LocationCodes);
            public IEnumerable<String> LocationCodes
            {
                get { return ReadProperty<IEnumerable<String>>(LocationCodesProperty); }
            }

            #endregion

            public FetchByEntityIdLocationCodesCriteria(Int32 entityId, IEnumerable<String> locationCodes)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<IEnumerable<String>>(LocationCodesProperty, locationCodes);
            }
        }


        /// <summary>
        /// Criteria for FetchByWorkpackageIdIncludingDeleted
        /// </summary>
        [Serializable]
        public class FetchByWorkpackageIdIncludingDeletedCriteria : Csla.CriteriaBase<FetchByWorkpackageIdIncludingDeletedCriteria>
        {
            public static readonly PropertyInfo<Int32> WorkpackageIdProperty =
            RegisterProperty<Int32>(c => c.WorkpackageId);
            public Int32 WorkpackageId
            {
                get { return ReadProperty<Int32>(WorkpackageIdProperty); }
            }

            public FetchByWorkpackageIdIncludingDeletedCriteria(Int32 workpackageId)
            {
                LoadProperty<Int32>(WorkpackageIdProperty, workpackageId);
            }
        }


        /// <summary>
        /// Criteria for FetchByWorkpackagePlanogramIdIncludingDeleted
        /// </summary>
        [Serializable]
        public class FetchByWorkpackagePlanogramIdIncludingDeletedCriteria : Csla.CriteriaBase<FetchByWorkpackagePlanogramIdIncludingDeletedCriteria>
        {
            public static readonly PropertyInfo<Int32> PlanogramIdProperty =
            RegisterProperty<Int32>(c => c.PlanogramId);
            public Int32 PlanogramId
            {
                get { return ReadProperty<Int32>(PlanogramIdProperty); }
            }

            public FetchByWorkpackagePlanogramIdIncludingDeletedCriteria(Int32 planogramId)
            {
                LoadProperty<Int32>(PlanogramIdProperty, planogramId);
            }
        }

        #endregion
    }
}
