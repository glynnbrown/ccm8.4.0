﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)

// CCM-25559 : L.Hodson
//		Created (Auto-generated)
//CCM-26179: I.George
// added more fields
// CCM-26306 : L.Ineson
//  Added System Settings & moved uom values there
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
// V8-27964 : A.Silva
//      Added ProductLevelId and DateLastMerchSynced. Both nullable.
//      Added SyncPlanHierarchy method.
// V8-29066 : D.Pleasance
//  Amended SyncProductLevel to lookup existing planogram group by code, name and now also by product group id, 
//  this is to capture when just the product group name has been changed.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Entity model object
    /// </summary>
    [Serializable]
    public sealed partial class Entity : ModelObject<Entity>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The row version timestamp
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region GFSId

        /// <summary>
        /// GFSId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> GFSIdProperty =
            RegisterModelProperty<Int32?>(c => c.GFSId);

        /// <summary>
        /// Gets/Sets the GFSId value
        /// </summary>
        public Int32? GFSId
        {
            get { return GetProperty<Int32?>(GFSIdProperty); }
            set { SetProperty<Int32?>(GFSIdProperty, value); }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region Description

        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);

        /// <summary>
        /// Gets/Sets the Description value
        /// </summary>
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
            set { SetProperty<String>(DescriptionProperty, value); }
        }

        #endregion

        #region DateCreated

        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        /// Gets/Sets the DateCreated value
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
            set { SetProperty<DateTime>(DateCreatedProperty, value); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        /// Gets/Sets the DateLastModified value
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
            set { SetProperty<DateTime>(DateLastModifiedProperty, value); }
        }

        #endregion

        #region DateDeleted

        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);

        /// <summary>
        /// Gets/Sets the DateDeleted value
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
            set { SetProperty<DateTime?>(DateDeletedProperty, value); }
        }

        #endregion

        #region System Settings

        /// <summary>
        /// SystemSettings property definition
        /// </summary>
        public static readonly ModelPropertyInfo<SystemSettings> SystemSettingsProperty =
           RegisterModelProperty<SystemSettings>(c => c.SystemSettings, "SystemSettings");

        /// <summary>
        /// Gets the entity system settings
        /// </summary>
        public SystemSettings SystemSettings
        {
            get { return GetProperty<SystemSettings>(SystemSettingsProperty); }
            set { SetProperty<SystemSettings>(SystemSettingsProperty, value); }
        }

        #endregion

        #region ProductLevelId

        /// <summary>
        ///		Metadata for the <see cref="ProductLevelId"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Int32?> ProductLevelIdProperty =
            RegisterModelProperty<Int32?>(o => o.ProductLevelId);

        /// <summary>
        ///		Gets or sets the Product Level Id from which to sync the Merch Hierarchy to the Plan Hierachy.
        /// </summary>
        public Int32? ProductLevelId
        {
            get { return this.GetProperty<Int32?>(ProductLevelIdProperty); }
            set { this.SetProperty<Int32?>(ProductLevelIdProperty, value); }
        }

        #endregion

        #region DateLastMerchSynced

        /// <summary>
        ///		Metadata for the <see cref="DateLastMerchSynced"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<DateTime?> DateLastMerchSyncedProperty =
            RegisterModelProperty<DateTime?>(o => o.DateLastMerchSynced);

        /// <summary>
        ///		Gets or sets the date when the Merch Hierachy was last synced to the Plan Hierarchy.
        /// </summary>
        public DateTime? DateLastMerchSynced
        {
            get { return this.GetProperty<DateTime?>(DateLastMerchSyncedProperty); }
            set { this.SetProperty<DateTime?>(DateLastMerchSyncedProperty, value); }
        }

        #endregion

        #region ComparisonAttributes

        public static readonly ModelPropertyInfo<EntityComparisonAttributeList> ComparisonAttributesProperty = RegisterModelProperty<EntityComparisonAttributeList>(o => o.ComparisonAttributes);

        public EntityComparisonAttributeList ComparisonAttributes => GetProperty(ComparisonAttributesProperty);

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
            BusinessRules.AddRule(new MaxLength(DescriptionProperty, 255));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(Entity), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(Entity), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.EntityCreate.ToString()));
            BusinessRules.AddRule(typeof(Entity), new IsInRole(AuthorizationActions.EditObject, DomainPermission.EntityEdit.ToString()));
            BusinessRules.AddRule(typeof(Entity), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.EntityDelete.ToString()));
        }
        #endregion

        #region Criteria

        #region FetchByIdCriteria

        // <summary>
        /// Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> IdProperty =
                RegisterProperty<Int32>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Int32 Id
            {
                get { return this.ReadProperty<Int32>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(Int32 id)
            {
                this.LoadProperty<Int32>(IdProperty, id);
            }
            #endregion
        }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new Entity
        /// </summary>
        public static Entity NewEntity()
        {
            Entity item = new Entity();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, String.Empty);
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            this.LoadProperty<SystemSettings>(SystemSettingsProperty, SystemSettings.NewSystemSettings());
            this.LoadProperty<EntityComparisonAttributeList>(ComparisonAttributesProperty, EntityComparisonAttributeList.NewEntityComparisonAttributeList());
            
            base.Create();
        }
        #endregion

        #endregion

        #region Overrides
        /// <summary>
        /// ToString override
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<Entity>(oldId, newId);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the GFS entity that corresponds
        /// to this entity that is within CCM
        /// </summary>
        public GFSModel.Entity GetGfsEntity()
        {
            return GFSModel.Entity.FetchByCcmEntity(this);
        }

        /// <summary>
        ///     Synchronizes this entity Planogram Hierarchy to the Merchandising Hierarchy.
        /// </summary>
        public void SyncPlanHierarchy()
        {
            //  Fetch this Entity's Merchandising Hierarchy.
            ProductHierarchy entityProductHierarchy = ProductHierarchy.FetchByEntityId(Id);

            //  No need to sync if the Merchandising Hierarchy has not changed.
            if (entityProductHierarchy.DateLastModified <= DateLastMerchSynced) return;

            //  Fetch the initial product level to be synced.
            ProductLevel startingProductLevel = entityProductHierarchy.EnumerateAllLevels().FirstOrDefault(o => o.Id == ProductLevelId);
            if (startingProductLevel == null) return;

            //  Fetch all available groups in the Merchandising Hierarchy.
            ReadOnlyCollection<ProductGroup> productGroups = entityProductHierarchy.FetchAllGroups();

            // Fetch this Entity's Planogram Hierarchy.
            PlanogramHierarchy entityPlanogramHierarchy = PlanogramHierarchy.FetchByEntityId(Id);

            //  Sync the Product Groups in the product level.
            //ProductLevel productLevel = startingProductLevel.ChildLevel;

            if (startingProductLevel != null)
            {
                Int32 sourceProductLevelId = startingProductLevel.Id;
                SyncProductLevel(sourceProductLevelId, entityPlanogramHierarchy.RootGroup, null, productGroups);

                //  Save the synced planogram hierarchy, update the last sync date and save the entity.
                entityPlanogramHierarchy.Save();
                DateLastMerchSynced = DateTime.UtcNow;
                Save();
            }
        }

        /// <summary>
        ///     Recursive method to sync each product level in turn for each parent product group.
        /// </summary>
        /// <param name="sourceProductLevelId"></param>
        /// <param name="targetPlanogramGroup"></param>
        /// <param name="parentGroup"></param>
        /// <param name="productGroups"></param>
        private static void SyncProductLevel(Int32 sourceProductLevelId, PlanogramGroup targetPlanogramGroup, ProductGroup parentGroup, IList<ProductGroup> productGroups)
        {
            IEnumerable<ProductGroup> productGroupsByLevelAndParent = productGroups.Where(o => o.ProductLevelId == sourceProductLevelId && (parentGroup == null || o.ParentGroup.Id == parentGroup.Id));
            foreach (ProductGroup productGroup in productGroupsByLevelAndParent)
            {
                PlanogramGroup matchingPlanogramGroup = targetPlanogramGroup.ChildList.ToList()
                    .FirstOrDefault(o =>
                        String.Equals(o.Name, String.Format("{0} {1}", productGroup.Code, productGroup.Name), StringComparison.InvariantCultureIgnoreCase));

                // The matching planogram group may not be found by name, so attempt to see if the planogram group can be found by product group id 
                // (To prevent new groups from being created with just a rename of the merch product group)
                if (matchingPlanogramGroup == null)
                {
                    matchingPlanogramGroup = targetPlanogramGroup.ChildList.ToList()
                        .FirstOrDefault(o => o.ProductGroupId == productGroup.Id);

                    if (matchingPlanogramGroup != null)
                    {
                        // Update existing name
                        matchingPlanogramGroup.Name = String.Format("{0} {1}", productGroup.Code, productGroup.Name);
                    }
                }

                //  Check whether the matching Planogram Group is not linked to the current Merchandising Group.
                if (matchingPlanogramGroup == null)
                {
                    matchingPlanogramGroup = PlanogramGroup.NewPlanogramGroup();
                    matchingPlanogramGroup.Name = String.Format("{0} {1}", productGroup.Code, productGroup.Name);
                    matchingPlanogramGroup.ProductGroupId = productGroup.Id;
                    targetPlanogramGroup.ChildList.Add(matchingPlanogramGroup);
                }


                //truncate the group name if over the limit
                if (matchingPlanogramGroup.Name.Length > 100) matchingPlanogramGroup.Name = matchingPlanogramGroup.Name.Substring(0, 100);


                //  Make sure if it existed without a product group that it gets the right one.
                if (matchingPlanogramGroup.ProductGroupId == null)
                    matchingPlanogramGroup.ProductGroupId = productGroup.Id;

                //  Check that it is not currently linked to another product group.
                if (matchingPlanogramGroup.ProductGroupId != productGroup.Id) break;

                //  Synchronize any lower levels, if any.
                ProductLevel productLevel = productGroup.GetAssociatedLevel();
                ProductLevel childLevel = productLevel.ChildLevel;
                if (childLevel != null)
                {
                    SyncProductLevel(childLevel.Id, matchingPlanogramGroup, productGroup, productGroups);
                }
            }
        }

        #endregion
    }
}