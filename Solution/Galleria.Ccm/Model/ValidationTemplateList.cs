﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)

// CCM-26474 : A.Silva ~ Created

#endregion

#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     A list containing <see cref="ValidationTemplate"/> objects.
    /// </summary>
    [Serializable]
    public sealed partial class ValidationTemplateList : ModelList<ValidationTemplateList, ValidationTemplate>
    {
        #region Authorization Rules

        /// <summary>
        ///     Defines the authorization rules for <see cref="ValidationTemplateList" />.
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ProductList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ValidationTemplateCreate.ToString()));
            BusinessRules.AddRule(typeof(ProductList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ValidationTemplateGet.ToString()));
            BusinessRules.AddRule(typeof(ProductList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ValidationTemplateEdit.ToString()));
            BusinessRules.AddRule(typeof(ProductList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ValidationTemplateDelete.ToString()));
        }

        #endregion

        #region Criteria

        #region FetchByEntityIdCriteria

        /// <summary>
        ///     The criteria to retrieve<see cref="ValidationTemplate"/> object belonging to the specified <see cref="Entity"/>.
        /// </summary>
        [Serializable]
        internal class FetchByEntityIdCriteria : CriteriaBase<FetchByEntityIdCriteria>
        {
            #region Properties

            /// <summary>
            ///     Metadata for the <see cref="EntityId"/> property.
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty = RegisterProperty<Int32>(o => o.EntityId);
            
            /// <summary>
            ///     Gets the unique Id value for the <see cref="Entity"/> to filter the results with.
            /// </summary>
            public Int32 EntityId { get { return ReadProperty(EntityIdProperty); }}

            #endregion

            #region Constructor

            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty(EntityIdProperty, entityId);
            }

            #endregion
        }
        #endregion
        
        #endregion
    }
}