﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-28258 : N.Foster
//	Created
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines an engine task library within the solution
    /// </summary>
    public static partial class TaskLibrary
    {
        #region Enums
        /// <summary>
        /// Enum that indicates if the register of
        /// a task library was successful or not
        /// </summary>
        public enum TaskLibraryRegisterResult : byte
        {
            InvalidAssembly = 0,
            AssemblyContainsNoTasks = 1,
            AssemblyOutOfDate = 2,
            Successful = 3
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Registers a task library within the solution
        /// </summary>
        public static TaskLibraryRegisterResult Register(String filePath)
        {
            return DataPortal.Execute<RegisterCommand>(new RegisterCommand(filePath)).Result;
        }
        #endregion

        #region Commands

        #region Register
        /// <summary>
        /// Registers a library within the solution
        /// </summary>
        [Serializable]
        private partial class RegisterCommand : CommandBase<RegisterCommand>
        {
            #region Authorization Rules
            /// <summary>
            /// Defines the authorization rules for this type
            /// </summary>
            private static void AddObjectAuthorizationRules()
            {
                BusinessRules.AddRule(typeof(RegisterCommand), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(RegisterCommand), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(RegisterCommand), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Administrator.ToString()));
                BusinessRules.AddRule(typeof(RegisterCommand), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
            }
            #endregion

            #region Properties

            #region FilePath
            /// <summary>
            /// FilePath property definition
            /// </summary>
            public static readonly PropertyInfo<String> FilePathProperty =
                RegisterProperty<String>(c => c.FilePath);
            /// <summary>
            /// Returns the full assembly file name and path
            /// </summary>
            public String FilePath
            {
                get { return this.ReadProperty<String>(FilePathProperty); }
            }
            #endregion

            #region Data
            /// <summary>
            /// Data property definition
            /// </summary>
            public static readonly PropertyInfo<Byte[]> DataProperty =
                RegisterProperty<Byte[]>(c => c.Data);
            /// <summary>
            /// Returns the assembly data
            /// </summary>
            public Byte[] Data
            {
                get { return this.ReadProperty<Byte[]>(DataProperty); }
            }
            #endregion

            #region Result
            /// <summary>
            /// Result property definition
            /// </summary>
            public static readonly PropertyInfo<TaskLibraryRegisterResult> ResultProperty =
                RegisterProperty<TaskLibraryRegisterResult>(c => c.Result);
            /// <summary>
            /// Returns the register result
            /// </summary>
            public TaskLibraryRegisterResult Result
            {
                get { return this.ReadProperty<TaskLibraryRegisterResult>(ResultProperty); }
                private set { this.LoadProperty<TaskLibraryRegisterResult>(ResultProperty, value); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public RegisterCommand(String filePath)
            {
                this.LoadProperty<String>(FilePathProperty, filePath);
                this.LoadProperty<Byte[]>(DataProperty, System.IO.File.ReadAllBytes(filePath));
            }
            #endregion
        }
        #endregion

        #region DeleteById
        /// <summary>
        /// Deletes the library with the specified id
        /// </summary>
        [Serializable]
        private partial class DeleteByIdCommand : CommandBase<DeleteByIdCommand>
        {
            #region Authorization Rules
            /// <summary>
            /// Defines the authorization rules for this type
            /// </summary>
            private static void AddObjectAuthorizationRules()
            {
                BusinessRules.AddRule(typeof(DeleteByIdCommand), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(DeleteByIdCommand), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(DeleteByIdCommand), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Administrator.ToString()));
                BusinessRules.AddRule(typeof(DeleteByIdCommand), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
            }
            #endregion

            #region Properties

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> IdProperty =
                RegisterProperty<Int32>(c => c.Id);
            /// <summary>
            /// Returns the task library id
            /// </summary>
            public Int32 Id
            {
                get { return this.ReadProperty<Int32>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public DeleteByIdCommand(Int32 id)
            {
                this.LoadProperty<Int32>(IdProperty, id);
            }
            #endregion
        }
        #endregion

        #endregion
    }
}
