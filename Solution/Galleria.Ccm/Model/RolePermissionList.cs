﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26234 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// List of permissions for a role
    /// </summary>
    [Serializable]
    public sealed partial class RolePermissionList : ModelList<RolePermissionList, RolePermission>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(RolePermissionList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.RoleCreate.ToString()));
            BusinessRules.AddRule(typeof(RolePermissionList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.RoleGet.ToString()));
            BusinessRules.AddRule(typeof(RolePermissionList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.RoleEdit.ToString()));
            BusinessRules.AddRule(typeof(RolePermissionList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.RoleDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>a new list</returns>
        internal static RolePermissionList NewList()
        {
            RolePermissionList list = new RolePermissionList();
            list.Create();
            return list;
        }
        #endregion
    }
}
