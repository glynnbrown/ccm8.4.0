﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Denotes the available repository sync directions
    /// </summary>
    public enum RepositorySyncDirection
    {
        OneWay = 0,
        TwoWay = 1
    }

    public static class RepositorySyncDirectionHelper
    {
        public static readonly Dictionary<RepositorySyncDirection, String> FriendlyName =
            new Dictionary<RepositorySyncDirection, String>()
            {
                { RepositorySyncDirection.OneWay, Message.Enum_RepositorySyncDirection_OneWay},
                { RepositorySyncDirection.TwoWay, Message.Enum_RepositorySyncDirection_TwoWay}
            };

        public static RepositorySyncDirection? RepositorySyncTargetTypeGetEnum(String description)
        {
            foreach (KeyValuePair<RepositorySyncDirection, String> keyPair in RepositorySyncDirectionHelper.FriendlyName)
            {
                if (keyPair.Value.Equals(description))
                {
                    return keyPair.Key;
                }
            }
            return null;
        }
    }
}
