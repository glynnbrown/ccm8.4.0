﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
#endregion
#endregion

using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public partial class ViewLayoutInfoList
    {
        #region Constructor

        private ViewLayoutInfoList() { }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Fetches a list of all view layout infos
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <returns></returns>
        public static ViewLayoutInfoList FetchAll()
        {
            return DataPortal.Fetch<ViewLayoutInfoList>(new FetchAllCriteria());
        }

        #endregion

        #region Data Access

        #region Fetch

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchAllCriteria criteria)
        {
            //TODO:
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            foreach (ViewLayoutInfoDto dto in MockDataHelper.FetchViewLayoutInfos())
            {
                this.Add(ViewLayoutInfo.GetViewLayoutInfo(null, dto));
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion

        #endregion

    }
}
