#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.
// V8-26351 : A.Silva ~ Some refactoring to tidy up Custom Column Layout classes.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Readonly list holding <see cref="CustomColumnLayoutInfo" /> objects.
    /// </summary>
    [Serializable]
    public sealed partial class CustomColumnLayoutInfoList :
        ModelReadOnlyList<CustomColumnLayoutInfoList, CustomColumnLayoutInfo>
    {
        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Criteria

        #region Nested type: FetchByPlanogramIdsCriteria

        [Serializable]
        public class FetchByIdsCriteria : CriteriaBase<FetchByIdsCriteria>
        {
            #region Properties

            #region DalType

            /// <summary>
            ///     The <see cref="ModelPropertyInfo{T}" /> for the <see cref="DalType" /> property.
            /// </summary>
            public static readonly PropertyInfo<CustomColumnLayoutDalFactoryType> DalTypeProperty =
                RegisterProperty<CustomColumnLayoutDalFactoryType>(o => o.DalType);

            /// <summary>
            ///     Gets the DAL type.
            /// </summary>
            public CustomColumnLayoutDalFactoryType DalType
            {
                get { return ReadProperty(DalTypeProperty); }
            }

            #endregion

            #region Ids property

            /// <summary>
            ///     The <see cref="ModelPropertyInfo{T}" /> for the <see cref="Ids" /> property.
            /// </summary>
            public static readonly PropertyInfo<List<object>> IdsProperty =
                RegisterProperty<List<Object>>(c => c.Ids);

            /// <summary>
            ///     Gets the list of Ids used as criteria.
            /// </summary>
            public List<Object> Ids
            {
                get { return ReadProperty(IdsProperty); }
            }

            #endregion

            #endregion

            #region Constructor

            public FetchByIdsCriteria(CustomColumnLayoutDalFactoryType sourceType, List<Object> ids)
            {
                LoadProperty(DalTypeProperty, sourceType);
                LoadProperty(IdsProperty, ids);
            }

            #endregion
        }

        #endregion

        #region Nested type: FetchByTypeCriteria

        [Serializable]
        public class FetchByTypeCriteria : CriteriaBase<FetchByTypeCriteria>
        {
            #region Properties

            #region DalType

            /// <summary>
            ///     The <see cref="ModelPropertyInfo{T}" /> for the <see cref="DalType" /> property.
            /// </summary>
            public static readonly PropertyInfo<CustomColumnLayoutDalFactoryType> DalTypeProperty =
                RegisterProperty<CustomColumnLayoutDalFactoryType>(o => o.DalType);

            /// <summary>
            ///     Gets the DAL type.
            /// </summary>
            public CustomColumnLayoutDalFactoryType DalType
            {
                get { return ReadProperty(DalTypeProperty); }
            }

            #endregion

            #region Ids property

            /// <summary>
            ///     The <see cref="ModelPropertyInfo{T}" /> for the <see cref="Ids" /> property.
            /// </summary>
            public static readonly PropertyInfo<List<object>> IdsProperty =
                RegisterProperty<List<Object>>(c => c.Ids);

            /// <summary>
            ///     Gets the list of Ids used as criteria.
            /// </summary>
            public List<Object> Ids
            {
                get { return ReadProperty(IdsProperty); }
            }

            #endregion

            #region Type property

            /// <summary>
            ///     The <see cref="ModelPropertyInfo{T}" /> for the <see cref="Type" /> property.
            /// </summary>
            public static readonly PropertyInfo<CustomColumnLayoutType> TypeProperty =
                RegisterProperty<CustomColumnLayoutType>(c => c.Type);

            /// <summary>
            ///     Gets the type used as criteria.
            /// </summary>
            public CustomColumnLayoutType Type
            {
                get { return ReadProperty(TypeProperty); }
            }

            #endregion

            #endregion

            #region Constructor

            public FetchByTypeCriteria(CustomColumnLayoutDalFactoryType sourceType, List<Object> ids,
                CustomColumnLayoutType type)
            {
                LoadProperty(DalTypeProperty, sourceType);
                LoadProperty(IdsProperty, ids);
                LoadProperty(TypeProperty, type);
            }

            #endregion
        }

        #endregion

        #endregion
    }
}