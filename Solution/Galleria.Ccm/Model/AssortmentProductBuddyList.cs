﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
#endregion

#endregion


using System;
using System.Linq;
using Csla;
using Galleria.Framework.Model;
using System.Collections.Generic;
using System.Collections.Specialized;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A list of inventory rules contained within an assortment
    /// </summary>
    [Serializable]
    public partial class AssortmentProductBuddyList : ModelList<AssortmentProductBuddyList, AssortmentProductBuddy>
    {
        #region Parent

        /// <summary>
        /// Returns the parent assortment
        /// </summary>
        public Assortment Parent
        {
            get { return base.Parent as Assortment; }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentProductBuddyList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.AssortmentCreate.ToString()));
            BusinessRules.AddRule(typeof(AssortmentProductBuddyList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.AssortmentGet.ToString()));
            BusinessRules.AddRule(typeof(AssortmentProductBuddyList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.AssortmentEdit.ToString()));
            BusinessRules.AddRule(typeof(AssortmentProductBuddyList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.AssortmentDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A new list</returns>
        public static AssortmentProductBuddyList NewAssortmentProductBuddyList()
        {
            AssortmentProductBuddyList item = new AssortmentProductBuddyList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}