﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25919 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class WorkflowTaskInfoList
    {
        #region Constructors
        public WorkflowTaskInfoList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        #region FetchByWorkflowId

        /// <summary>
        /// Returns all workflow task infos for the given workflow id
        /// </summary>
        public static WorkflowTaskInfoList FetchByWorkflowId(Int32 workflowId)
        {
            return DataPortal.Fetch<WorkflowTaskInfoList>(new FetchByWorkflowIdCriteria(workflowId));
        }

        #endregion

        #region FetchByWorkpackageId

        /// <summary>
        /// Returns all workflow task infos for the given workflow id
        /// </summary>
        public static WorkflowTaskInfoList FetchByWorkpackageId(Int32 workflowId)
        {
            return DataPortal.Fetch<WorkflowTaskInfoList>(new FetchByWorkpackageIdCriteria(workflowId));
        }

        #endregion

        #endregion

        #region Data Access

        /// <summary>
        /// Called when returning all workflows for the specified entity
        /// </summary>
        private void DataPortal_Fetch(FetchByWorkflowIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IWorkflowTaskInfoDal dal = dalContext.GetDal<IWorkflowTaskInfoDal>())
                {
                    IEnumerable<WorkflowTaskInfoDto> dtoList = dal.FetchByWorkflowId(criteria.WorkflowId);
                    foreach (WorkflowTaskInfoDto dto in dtoList)
                    {
                        this.Add(WorkflowTaskInfo.GetWorkflowTaskInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returning all workflows for the specified entity
        /// </summary>
        private void DataPortal_Fetch(FetchByWorkpackageIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IWorkflowTaskInfoDal dal = dalContext.GetDal<IWorkflowTaskInfoDal>())
                {
                    IEnumerable<WorkflowTaskInfoDto> dtoList = dal.FetchByWorkpackageId(criteria.WorkpackageId);
                    foreach (WorkflowTaskInfoDto dto in dtoList)
                    {
                        this.Add(WorkflowTaskInfo.GetWorkflowTaskInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion
    }
}
