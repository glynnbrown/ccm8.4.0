﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class FixturePackageInfoList
    {
        #region Constants
        private const String _dalAssemblyName = "Galleria.Ccm.Dal.{0}.dll";
        #endregion

        #region Constructors
        private FixturePackageInfoList() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Fetches a list of infos for the givne fixture packages
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <returns></returns>
        public static FixturePackageInfoList FetchByFileNames(IEnumerable<String> fileNames)
        {
            return DataPortal.Fetch<FixturePackageInfoList>(new FetchByIdsCriteria(FixturePackageType.FileSystem, fileNames.Cast<Object>().ToList()));
        }

        /// <summary>
        /// Returns the package with the specified ID from the default DAL.  Created for the purpose of unit testing
        /// with the Mock DAL--not sure if this is something you'd need in real life or not.
        /// </summary>
        public static FixturePackageInfoList FetchById(IEnumerable<Object> ids)
        {
            return DataPortal.Fetch<FixturePackageInfoList>(new FetchByIdsCriteria(FixturePackageType.Unknown, ids.ToList()));
        }

        #endregion

        #region Data Access

        #region Fetch

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdsCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(criteria.PackageType, out dalFactoryName);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IFixturePackageInfoDal dal = dalContext.GetDal<IFixturePackageInfoDal>())
                {
                    foreach (FixturePackageInfoDto dto in dal.FetchByIds(criteria.Ids))
                    {
                        this.Add(FixturePackageInfo.GetFixturePackageInfo(dalContext, dto));
                    }
                }
            }


            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the correct dal factory based on the given type and args.
        /// </summary>
        private static IDalFactory GetDalFactory(FixturePackageType dalType, out String dalFactoryName)
        {
            switch (dalType)
            {
                case FixturePackageType.FileSystem:
                    dalFactoryName = Constants.UserDal;
                    return DalContainer.GetDalFactory(dalFactoryName);

                default:
                    dalFactoryName = DalContainer.DalName;
                    return DalContainer.GetDalFactory();
            }

        }

        #endregion
    }
}
