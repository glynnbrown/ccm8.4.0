﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created (Auto-generated)
// V8-26514 : A.Silva ~ Added Score Properties.
// V8-26721 : A.Silva ~ Added IValidationTemplateGroupMetric.
// V8-26815 : A.Silva ~ Added new factory method to create a group metric from a pre existing IValidationTemplateGroupMetric.
// V8-26812 : A.Silva ~ Added ValidationType property.

#endregion

#region Version History: CCM803

// V8-29596 : L.Luong
//  Added Criteria

#endregion

#region Version History : CCM820
// V8-30844 : A.Kuszyk
//  Added validation rules to match PlanogramValidationTemplateGroupMetric
#endregion

#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     ValidationTemplateGroupMetric Model object
    /// </summary>
    [Serializable]
    public sealed partial class ValidationTemplateGroupMetric : ModelObject<ValidationTemplateGroupMetric>, IValidationTemplateGroupMetric
    {
        #region Static Constructor

        static ValidationTemplateGroupMetric()
        {
            MappingHelper.InitializeMappings();
        }

        #endregion

        #region Parent

        /// <summary>
        ///     Returns a reference to the parent object
        /// </summary>
        public new ValidationTemplateGroup Parent
        {
            get
            {
                var parentList = base.Parent as ValidationTemplateGroupMetricList;
                if (parentList != null)
                {
                    return parentList.Parent as ValidationTemplateGroup;
                }
                return null;
            }
        }

        IValidationTemplateGroup IValidationTemplateGroupMetric.Parent
        {
            get { return Parent; }
        }
        #endregion

        #region Properties

        #region Id

        /// <summary>
        ///     Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);

        /// <summary>
        ///     Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty(IdProperty); }
        }

        #endregion

        #region Field

        /// <summary>
        ///     Field property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> FieldProperty =
            RegisterModelProperty<String>(c => c.Field);

        /// <summary>
        ///     Gets/Sets the Field value
        /// </summary>
        public String Field
        {
            get { return GetProperty(FieldProperty); }
            set { SetProperty(FieldProperty, value); }
        }

        #endregion

        #region Threshold1

        /// <summary>
        ///     Threshold1 property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Single> Threshold1Property =
            RegisterModelProperty<Single>(c => c.Threshold1);

        /// <summary>
        ///     Gets/Sets the Threshold1 value
        /// </summary>
        public Single Threshold1
        {
            get { return GetProperty(Threshold1Property); }
            set { SetProperty(Threshold1Property, value); }
        }

        #endregion

        #region Threshold2

        /// <summary>
        ///     Threshold2 property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Single> Threshold2Property =
            RegisterModelProperty<Single>(c => c.Threshold2);

        /// <summary>
        ///     Gets/Sets the Threshold2 value
        /// </summary>
        public Single Threshold2
        {
            get { return GetProperty(Threshold2Property); }
            set { SetProperty(Threshold2Property, value); }
        }

        #endregion

        #region Score1

        /// <summary>
        ///     Score1 property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Single> Score1Property =
            RegisterModelProperty<Single>(c => c.Score1);

        /// <summary>
        ///     Gets/Sets the Score1 value
        /// </summary>
        public Single Score1
        {
            get { return GetProperty(Score1Property); }
            set { SetProperty(Score1Property, value); }
        }

        #endregion

        #region Score2

        /// <summary>
        ///     Score2 property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Single> Score2Property =
            RegisterModelProperty<Single>(c => c.Score2);

        /// <summary>
        ///     Gets/Sets the Score2 value
        /// </summary>
        public Single Score2
        {
            get { return GetProperty(Score2Property); }
            set { SetProperty(Score2Property, value); }
        }

        #endregion

        #region Score3

        /// <summary>
        ///     Score3 property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Single> Score3Property =
            RegisterModelProperty<Single>(c => c.Score3);

        /// <summary>
        ///     Gets/Sets the Score3 value
        /// </summary>
        public Single Score3
        {
            get { return GetProperty(Score3Property); }
            set { SetProperty(Score3Property, value); }
        }

        #endregion

        #region AggregationType

        /// <summary>
        ///     AggregationType property definition
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramValidationAggregationType> AggregationTypeProperty =
            RegisterModelProperty<PlanogramValidationAggregationType>(c => c.AggregationType);

        /// <summary>
        ///     Gets/Sets the AggregationType value
        /// </summary>
        public PlanogramValidationAggregationType AggregationType
        {
            get { return GetProperty(AggregationTypeProperty); }
            set { SetProperty(AggregationTypeProperty, value); }
        }

        #endregion

        #region ValidationType

        /// <summary>
        ///		Metadata for the <see cref="ValidationType"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramValidationType> ValidationTypeProperty =
            RegisterModelProperty<PlanogramValidationType>(o => o.ValidationType);

        /// <summary>
        ///		Gets or sets the value for the <see cref="ValidationType"/> property.
        /// </summary>
        public PlanogramValidationType ValidationType
        {
            get { return this.GetProperty<PlanogramValidationType>(ValidationTypeProperty); }
            set { this.SetProperty<PlanogramValidationType>(ValidationTypeProperty, value); }
        }

        #endregion

        #region Criteria

        /// <summary>
        ///		Criteria property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> CriteriaProperty =
            RegisterModelProperty<String>(o => o.Criteria);

        /// <summary>
        ///		Gets or sets the value for the <see cref="Criteria"/> property.
        /// </summary>
        public String Criteria
        {
            get { return this.GetProperty<String>(CriteriaProperty); }
            set { this.SetProperty<String>(CriteriaProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        ///     Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(FieldProperty));
            BusinessRules.AddRule(new MaxLength(FieldProperty, 255));
            BusinessRules.AddRule(new Galleria.Framework.Planograms.Helpers.PlanogramHelper.ValidationTypeConstraint(ValidationTypeProperty,
                Threshold1Property, Threshold2Property));
            BusinessRules.AddRule(new Dependency(Threshold1Property, ValidationTypeProperty));
            BusinessRules.AddRule(new Dependency(Threshold2Property, ValidationTypeProperty));
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        ///     Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ValidationTemplateGroupMetric), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ValidationTemplateGet.ToString()));
            BusinessRules.AddRule(typeof(ValidationTemplateGroupMetric), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ValidationTemplateCreate.ToString()));
            BusinessRules.AddRule(typeof(ValidationTemplateGroupMetric), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ValidationTemplateEdit.ToString()));
            BusinessRules.AddRule(typeof(ValidationTemplateGroupMetric), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ValidationTemplateDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        ///     Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static ValidationTemplateGroupMetric NewValidationTemplateGroupMetric()
        {
            var item = new ValidationTemplateGroupMetric();
            item.Create();
            return item;
        }

        /// <summary>
        ///     Creates and initializes a new instace of <see cref="ValidationTemplateGroupMetric"/>.
        /// </summary>
        /// <param name="source">An instance of a type implementing <see cref="IValidationTemplateGroupMetric"/> to use for the initial data of the new instance.</param>
        /// <returns>A new instance of <see cref="ValidationTemplateGroupMetric"/>.</returns>
        public static ValidationTemplateGroupMetric NewValidationTemplateGroupMetric(IValidationTemplateGroupMetric source)
        {
            var item = new ValidationTemplateGroupMetric();
            item.Create(source);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        ///     Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty(Score2Property, 5F);
            this.LoadProperty(Score3Property, 20F);
            this.MarkAsChild();
            base.Create();
        }

        private void Create(IValidationTemplateGroupMetric source)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty(FieldProperty, source.Field);
            this.LoadProperty(AggregationTypeProperty, source.AggregationType);
            this.LoadProperty(Threshold1Property, source.Threshold1);
            this.LoadProperty(Threshold2Property, source.Threshold2);
            this.LoadProperty(Score1Property, source.Score1);
            this.LoadProperty(Score2Property, source.Score2);
            this.LoadProperty(Score3Property, source.Score3);
            this.LoadProperty(ValidationTypeProperty, source.ValidationType);
            this.LoadProperty(CriteriaProperty, source.Criteria);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods
        /// <summary>
        ///     Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty(IdProperty, newId);
            context.RegisterId<ValidationTemplateGroupMetric>(oldId, newId);
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:ValidationTemplateGroupMetric"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:ValidationTemplateGroupMetric"/>.
        /// </returns>
        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", 
                PlanogramValidationAggregationTypeHelper.FriendlyNames[AggregationType], 
                Field, 
                PlanogramValidationTypeHelper.FriendlyGreenResultCaptions[ValidationType], 
                Threshold1);
        }

        #endregion
    }
}