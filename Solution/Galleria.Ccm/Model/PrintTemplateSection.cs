﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using System.Diagnostics;
using System.Windows.Media;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// PrintTemplateSection Model object
    /// </summary>
    [Serializable]
    public sealed partial class PrintTemplateSection : ModelObject<PrintTemplateSection>
    {
        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PrintTemplateSectionGroup Parent
        {
            get
            {
                PrintTemplateSectionList parentList = base.Parent as PrintTemplateSectionList;
                if (parentList != null)
                {
                    return parentList.Parent as PrintTemplateSectionGroup;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region Number

        /// <summary>
        /// Number property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> NumberProperty =
            RegisterModelProperty<Byte>(c => c.Number);

        /// <summary>
        /// Gets/Sets the Number value
        /// </summary>
        public Byte Number
        {
            get { return GetProperty<Byte>(NumberProperty); }
            set { SetProperty<Byte>(NumberProperty, value); }
        }

        #endregion

        #region Orientation

        /// <summary>
        /// Orientation property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PrintTemplateOrientationType> OrientationProperty =
            RegisterModelProperty<PrintTemplateOrientationType>(c => c.Orientation);

        /// <summary>
        /// Gets/Sets the Orientation value
        /// </summary>
        public PrintTemplateOrientationType Orientation
        {
            get { return GetProperty<PrintTemplateOrientationType>(OrientationProperty); }
            set 
            { 
                SetProperty<PrintTemplateOrientationType>(OrientationProperty, value);

                //update the page size.
                UpdatePageSize();
            }
        }

        #endregion

        #region Components

        /// <summary>
        /// Components property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PrintTemplateComponentList> ComponentsProperty =
           RegisterModelProperty<PrintTemplateComponentList>(c => c.Components);

        /// <summary>
        /// The components of the print template section
        /// </summary>
        public PrintTemplateComponentList Components
        {
            get { return GetProperty<PrintTemplateComponentList>(ComponentsProperty); }
        }

        #endregion



        #region Helper Properties

        #region PreviewImage
        /// <summary>
        /// The Print Options Section Preview Image
        /// </summary>
        [NonSerialized]
        Brush _previewImage = null;
        public Brush PreviewImage
        {
            get
            {
                if (_previewImage == null)
                {
                    _previewImage = Brushes.White;
                }
                return _previewImage;
            }
            set
            {
                _previewImage = value;
                OnPropertyChanged("PreviewImage");
            }
        }
        #endregion


        #region ScaledPageHeight

        /// <summary>
        /// ScaledPageHeight property defintion
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ScaledPageHeightProperty =
            RegisterModelProperty<Int32>(c => c.ScaledPageHeight);

        /// <summary>
        /// Gets/Sets the ScaledPageHeight helper property
        /// </summary>
        public Int32 ScaledPageHeight
        {
            get { return GetProperty<Int32>(ScaledPageHeightProperty); }
            set 
            {
                if (ReadProperty<Int32>(ScaledPageHeightProperty) != value)
                {
                    SetProperty<Int32>(ScaledPageHeightProperty, value);
                    OnPropertyChanged("PreviewPageHeight");
                    OnPropertyChanged("ActualPageHeight");
                }
            }
        }

        #endregion

        #region ScaledPageWidth

        /// <summary>
        /// ScaledPageWidth property defintion
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ScaledPageWidthProperty =
            RegisterModelProperty<Int32>(c => c.ScaledPageWidth);

        /// <summary>
        /// Gets/Sets the ScaledPageWidth helper property
        /// </summary>
        public Int32 ScaledPageWidth
        {
            get { return GetProperty<Int32>(ScaledPageWidthProperty); }
            set
            {
                if (ReadProperty<Int32>(ScaledPageWidthProperty) != value)
                {
                    SetProperty<Int32>(ScaledPageWidthProperty, value);
                    OnPropertyChanged("PreviewPageWidth");
                    OnPropertyChanged("ActualPageWidth");
                }
            }
        }

        #endregion


        const Double _scale = 2.835;
        const Int32 _previewScale = 4;

        /// <summary>
        /// The Print Options Section Preview Page Height
        /// </summary>
        public Int32 PreviewPageHeight
        {
            get { return this.ScaledPageHeight / _previewScale; }
        }

        /// <summary>
        /// The Print Options Section Preview Page Width
        /// </summary>
        public Int32 PreviewPageWidth
        {
            get { return this.ScaledPageWidth / _previewScale; }
        }

        /// <summary>
        /// The Print Options Section Actual Page Height
        /// </summary>
        public Int32 ActualPageHeight
        {
            get { return (Int32)(this.ScaledPageHeight / _scale); }
        }

        /// <summary>
        /// The Print Options Section Actual Page Width
        /// </summary>
        public Int32 ActualPageWidth
        {
            get { return (Int32)(this.ScaledPageWidth / _scale); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods

        //TEMP
        public static PrintTemplateSection NewEmpty()
        {
            PrintTemplateSection item = new PrintTemplateSection();
            item.Create(PrintTemplatePaperSizeType.A4, 1);
            return item;
        }

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static PrintTemplateSection NewPrintTemplateSection(PrintTemplatePaperSizeType paperSizeType, Byte sectionNumber)
        {
            PrintTemplateSection item = new PrintTemplateSection();
            item.Create(paperSizeType, sectionNumber);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create(PrintTemplatePaperSizeType paperSizeType, Byte sectionNumber)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());

            this.LoadProperty<Byte>(NumberProperty, sectionNumber);
            this.LoadProperty<PrintTemplateOrientationType>(OrientationProperty, PrintTemplateOrientationType.Landscape);
            this.LoadProperty<PrintTemplateComponentList>(ComponentsProperty, PrintTemplateComponentList.NewPrintTemplateComponentList());

            UpdatePageSize(paperSizeType);

            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<PrintTemplateSection>(oldId, newId);
        }

        /// <summary>
        /// Moves the print option section to a new section group
        /// </summary>
        internal static PrintTemplateSection MovePrintTemplateSection(PrintTemplateSection section, PrintTemplateSectionGroup newGroup)
        {
            if (section.Parent == null) return section;
            if (section.Parent == newGroup) return section;

            return section.Move<PrintTemplateSectionList>(section.Parent.Sections, newGroup.Sections);
        }

        /// <summary>
        /// Updates the size of this section according to the parent
        /// size type.
        /// </summary>
        internal void UpdatePageSize()
        {
            PrintTemplateSectionGroup parentGroup = this.Parent;
            if(parentGroup == null) return;

            PrintTemplate parentTemplate = parentGroup.Parent;
            if(parentTemplate == null) return;

            UpdatePageSize(parentTemplate.PaperSize);
        }

        /// <summary>
        /// Updates the size of this section according to the given 
        /// size type.
        /// </summary>
        internal void UpdatePageSize(PrintTemplatePaperSizeType paperSizeType)
        {
            switch (this.Orientation)
            {
                case PrintTemplateOrientationType.Landscape:
                    {
                        switch (paperSizeType)
                        {
                            case PrintTemplatePaperSizeType.A3:
                                this.ScaledPageHeight = 842;
                                this.ScaledPageWidth = 1190;
                                break;

                            case PrintTemplatePaperSizeType.A4:
                                this.ScaledPageHeight = 595;
                                this.ScaledPageWidth = 842;
                                break;

                            case PrintTemplatePaperSizeType.A5:
                                this.ScaledPageHeight = 420;
                                this.ScaledPageWidth = 595;
                                break;

                            case PrintTemplatePaperSizeType.B4:
                                this.ScaledPageHeight = 709;
                                this.ScaledPageWidth = 1001;
                                break;

                            case PrintTemplatePaperSizeType.B5:
                                this.ScaledPageHeight = 499;
                                this.ScaledPageWidth = 709;
                                break;

                            case PrintTemplatePaperSizeType.Executive:
                                this.ScaledPageHeight = 522;
                                this.ScaledPageWidth = 757;
                                break;

                            case PrintTemplatePaperSizeType.Legal:
                                this.ScaledPageHeight = 610;
                                this.ScaledPageWidth = 1006;
                                break;

                            case PrintTemplatePaperSizeType.Letter:
                                this.ScaledPageHeight = 610;
                                this.ScaledPageWidth = 791;
                                break;

                            default:
                                Debug.Fail("PaperSize not handled");
                                break;
                        }
                    }
                    break;

                case PrintTemplateOrientationType.Portrait:
                    {
                        switch (paperSizeType)
                        {
                            case PrintTemplatePaperSizeType.A3:
                                this.ScaledPageHeight = 1190;
                                this.ScaledPageWidth = 842;
                                break;

                            case PrintTemplatePaperSizeType.A4:
                                this.ScaledPageHeight = 842;
                                this.ScaledPageWidth = 595;
                                break;

                            case PrintTemplatePaperSizeType.A5:
                                this.ScaledPageHeight = 595;
                                this.ScaledPageWidth = 420;
                                break;

                            case PrintTemplatePaperSizeType.B4:
                                this.ScaledPageHeight = 1001;
                                this.ScaledPageWidth = 709;
                                break;

                            case PrintTemplatePaperSizeType.B5:
                                this.ScaledPageHeight = 709;
                                this.ScaledPageWidth = 499;
                                break;

                            case PrintTemplatePaperSizeType.Executive:
                                this.ScaledPageHeight = 757;
                                this.ScaledPageWidth = 522;
                                break;

                            case PrintTemplatePaperSizeType.Legal:
                                this.ScaledPageHeight = 1006;
                                this.ScaledPageWidth = 610;
                                break;

                            case PrintTemplatePaperSizeType.Letter:
                                this.ScaledPageHeight = 791;
                                this.ScaledPageWidth = 610;
                                break;

                            default:
                                Debug.Fail("PaperSize not handled");
                                break;
                        }
                    }
                    break;
            }
        }

        #endregion
    }
}