﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
// CCM-25452 : N.Haywood
//  Added FetchByEntityIdCriteria and removed fetch by search criteria
// V8-25453 : A.Kuszyk
//  Added FetchByEntityIdProductIdsCriteria.
// CCM-25447 : N.Haywood
//  Added LocationProductIllegal
// CCM-26099 :I.George
//  Added FetchByMerchandisingGroupIdCriteria
#endregion
#region Version History: (CCm 8.3.0)
// V8-31835 : N.Haywood
//  Added Illegal and Legal MetaData and Validation
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{

    /// <summary>
    /// Readonly list holding ProductInfo objects.
    /// </summary>
    [Serializable]
    public sealed partial class ProductInfoList : ModelReadOnlyList<ProductInfoList, ProductInfo>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ProductInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ProductInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(ProductInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ProductInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for ProductInfoList.FetchByEntityIdProductGtins
        /// </summary>
        [Serializable]
        public class FetchByEntityIdProductGtinsCriteria : CriteriaBase<FetchByEntityIdProductGtinsCriteria>
        {
            #region Properties

            public static PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static PropertyInfo<IEnumerable<String>> ProductGtinsProperty =
                RegisterProperty<IEnumerable<String>>(c => c.ProductGtins);

            public IEnumerable<String> ProductGtins
            {
                get { return ReadProperty<IEnumerable<String>>(ProductGtinsProperty); }
            }

            #endregion

            public FetchByEntityIdProductGtinsCriteria(Int32 entityId, IEnumerable<String> productGtins)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<IEnumerable<String>>(ProductGtinsProperty, productGtins);
            }
        }

        /// <summary>
        /// Criteria for use with ProductInfoList.FetchByEntityIdSearchCriteria
        /// </summary>
        [Serializable]
        public class FetchByEntityIdSearchCriteriaCriteria : CriteriaBase<FetchByEntityIdSearchCriteriaCriteria>
        {
            public static PropertyInfo<Int32> EntityIdProperty = RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static PropertyInfo<String> SearchCriteriaProperty = RegisterProperty<String>(c => c.SearchCriteria);
            public String SearchCriteria
            {
                get { return ReadProperty<String>(SearchCriteriaProperty); }
            }

            public FetchByEntityIdSearchCriteriaCriteria(Int32 entityId, String searchCriteria)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<String>(SearchCriteriaProperty, searchCriteria);
            }
        }

        /// <summary>
        /// Criteria for use with ProductInfoList.FetchByProductUniverseId
        /// </summary>
        [Serializable]
        public class FetchByProductUniverseIdCriteria : CriteriaBase<FetchByProductUniverseIdCriteria>
        {
            public static PropertyInfo<Int32> ProductUniverseIdProperty = RegisterProperty<Int32>(c => c.ProductUniverseId);
            public Int32 ProductUniverseId
            {
                get { return ReadProperty<Int32>(ProductUniverseIdProperty); }
            }

            public FetchByProductUniverseIdCriteria(Int32 productUniverseId)
            {
                LoadProperty<Int32>(ProductUniverseIdProperty, productUniverseId);
            }
        }

        /// <summary>
        /// Criteria for use with ProductInfoList.FetchByMerchandisingGroupId
        /// </summary>
        [Serializable]
        public class FetchByMerchandisingGroupIdCriteria : CriteriaBase<FetchByMerchandisingGroupIdCriteria>
        {
            public static PropertyInfo<Int32> CategoryIdProperty = RegisterProperty<Int32>(c => c.CategoryId);
            public Int32 CategoryId
            {
                get { return ReadProperty<Int32>(CategoryIdProperty); }
            }

            public FetchByMerchandisingGroupIdCriteria(Int32 categoryId)
            {
                LoadProperty<Int32>(CategoryIdProperty, categoryId);
            }
        }
        /// <summary>
        /// Criteria for ProductInfoList.FetchByProductIds
        /// </summary>
        [Serializable]
        public class FetchByProductIdsCriteria : CriteriaBase<FetchByProductIdsCriteria>
        {
            #region Properties

            public static PropertyInfo<IEnumerable<Int32>> ProductIdsProperty =
                RegisterProperty<IEnumerable<Int32>>(c => c.ProductIds);

            public IEnumerable<Int32> ProductIds
            {
                get { return ReadProperty<IEnumerable<Int32>>(ProductIdsProperty); }
            }

            #endregion

            public FetchByProductIdsCriteria(IEnumerable<Int32> productIds)
            {
                LoadProperty<IEnumerable<Int32>>(ProductIdsProperty, productIds);
            }
        }

        /// <summary>
        /// Criteria for ProductInfoList.FetchByEntityId
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : CriteriaBase<FetchByEntityIdCriteria>
        {
            #region Properties

            public static PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            #endregion

            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
            }
        }

        /// <summary>
        /// Criteria for ProductInfoList.FetchByEntityIdProductIds
        /// </summary>
        [Serializable]
        public class FetchByEntityIdProductIdsCriteria : CriteriaBase<FetchByEntityIdProductIdsCriteria>
        {
            #region Properties

            public static PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static PropertyInfo<IEnumerable<Int32>> ProductIdsProperty =
                RegisterProperty<IEnumerable<Int32>>(c => c.ProductIds);

            public IEnumerable<Int32> ProductIds
            {
                get { return ReadProperty<IEnumerable<Int32>>(ProductIdsProperty); }
            }

            #endregion

            public FetchByEntityIdProductIdsCriteria(Int32 entityId, IEnumerable<Int32> productIds)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<IEnumerable<Int32>>(ProductIdsProperty, productIds);
            }
        }

        /// <summary>
        /// Criteria for ProductInfoList.FetchByEntityId
        /// </summary>
        [Serializable]
        public class FetchByEntityIdIncludingDeletedCriteria : CriteriaBase<FetchByEntityIdIncludingDeletedCriteria>
        {
            #region Properties

            public static PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            #endregion

            public FetchByEntityIdIncludingDeletedCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
            }
        }

        /// <summary>
        /// Criteria for ProductInfoList.FetchProductInfoIllegalsByLocationCodeProductGroupCode
        /// </summary>
        [Serializable]
        public class FetchIllegalsByLocationCodeProductGroupCodeCriteria : CriteriaBase<FetchIllegalsByLocationCodeProductGroupCodeCriteria>
        {
            #region Properties

            public static PropertyInfo<String> LocationCodeProperty =
                RegisterProperty<String>(c => c.LocationCode);
            public String LocationCode
            {
                get { return ReadProperty<String>(LocationCodeProperty); }
            }

            public static PropertyInfo<String> ProductGroupCodeProperty =
                RegisterProperty<String>(c => c.ProductGroupCode);
            public String ProductGroupCode
            {
                get { return ReadProperty<String>(ProductGroupCodeProperty); }
            }

            #endregion

            public FetchIllegalsByLocationCodeProductGroupCodeCriteria(String locationCode, String productGroupCode)
            {
                LoadProperty<String>(LocationCodeProperty, locationCode);
                LoadProperty<String>(ProductGroupCodeProperty, productGroupCode);
            }
        }

        /// <summary>
        /// Criteria for ProductInfoList.FetchProductInfoLegalsByLocationCodeProductGroupCode
        /// </summary>
        [Serializable]
        public class FetchLegalsByLocationCodeProductGroupCodeCriteria : CriteriaBase<FetchLegalsByLocationCodeProductGroupCodeCriteria>
        {
            #region Properties

            public static PropertyInfo<String> LocationCodeProperty =
                RegisterProperty<String>(c => c.LocationCode);
            public String LocationCode
            {
                get { return ReadProperty<String>(LocationCodeProperty); }
            }

            public static PropertyInfo<String> ProductGroupCodeProperty =
                RegisterProperty<String>(c => c.ProductGroupCode);
            public String ProductGroupCode
            {
                get { return ReadProperty<String>(ProductGroupCodeProperty); }
            }

            #endregion

            public FetchLegalsByLocationCodeProductGroupCodeCriteria(String locationCode, String productGroupCode)
            {
                LoadProperty<String>(LocationCodeProperty, locationCode);
                LoadProperty<String>(ProductGroupCodeProperty, productGroupCode);
            }
        }

        #endregion
    }
}
