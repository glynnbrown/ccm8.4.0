﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class ViewLayoutItemList
    {
        #region Constructor
        private ViewLayoutItemList() { } // Force use of factory methods
        #endregion

        #region Factory Methods

        internal static ViewLayoutItemList FetchByViewLayoutId(IDalContext dalContext, Int32 parentId)
        {
            return DataPortal.FetchChild<ViewLayoutItemList>(dalContext, parentId);
        }

        internal static ViewLayoutItemList GetByViewLayoutItemId(IDalContext dalContext, Int32? parentId, IEnumerable<ViewLayoutItemDto> layoutItemDtos)
        {
            return DataPortal.FetchChild<ViewLayoutItemList>(dalContext, parentId, layoutItemDtos);
        }

        #endregion

        #region Data Access

        #region Fetch

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 parentId)
        {
            RaiseListChangedEvents = false;

            //TODO:
            List<ViewLayoutItemDto> childItems =  MockDataHelper.FetchViewLayoutItemsByParentId(parentId);
            foreach (ViewLayoutItemDto dto in childItems.ToList())
            {
                if (dto.ParentViewLayoutItemId == null)
                {
                    this.Add(ViewLayoutItem.GetViewLayoutItem(dalContext, dto, childItems));
                }
            }

            RaiseListChangedEvents = true;
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32? parentId, IEnumerable<ViewLayoutItemDto> layoutItemDtos)
        {
            RaiseListChangedEvents = false;

            //TODO:
            foreach (ViewLayoutItemDto dto in layoutItemDtos)
            {
                if (dto.ParentViewLayoutItemId == parentId)
                {
                    this.Add(ViewLayoutItem.GetViewLayoutItem(dalContext, dto, layoutItemDtos));
                }
            } 

            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}
