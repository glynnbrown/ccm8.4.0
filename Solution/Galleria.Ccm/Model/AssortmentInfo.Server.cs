﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#region Version History: (CCM CCM803)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion

#region Version History: (CCM 8.2.0)
// V8-30461 : M.Shelley
//  Added ProductGroupId to LoadDataTransferObject and GetDataTransferObject
#endregion

#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentInfo
    {
        #region Constructor
        private AssortmentInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an AssortmentInfo from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        /// <returns>An AssortmentInfo object</returns>
        internal static AssortmentInfo GetAssortmentInfo(IDalContext dalContext, AssortmentInfoDto dto)
        {
            return DataPortal.FetchChild<AssortmentInfo>(dalContext, dto);
        }

        /// <summary>
        /// Returns the latest Assortment with the specified name
        /// </summary>
        /// <param name="name">The Assortment name</param>
        /// <returns>The latest version of the Assortment</returns>
        public static AssortmentInfo FetchLatestVersionByEntityIdName(Int32 entityId, String name)
        {
            return DataPortal.Fetch<AssortmentInfo>(new FetchLatestVersionByEntityIdNameCriteria(entityId, name));
        }
        #endregion

        #region Data Access

        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, AssortmentInfoDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Int32>(EntityIdProperty, dto.Id);
            LoadProperty<Int32?>(ConsumerDecisionTreeIdProperty, dto.ConsumerDecisionTreeId);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<String>(ProductGroupNameProperty, dto.ProductGroupName);
            LoadProperty<Int32>(ProductGroupIdProperty, dto.ProductGroupId);
            LoadProperty<String>(ProductGroupCodeProperty, dto.ProductGroupCode);
            LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            LoadProperty<Guid?>(ParentUniqueContentReferenceProperty, dto.ParentUniqueContentReference);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        /// <returns>A new data transfer object</returns>
        private AssortmentInfoDto GetDataTransferObject()
        {
            AssortmentInfoDto dto = new AssortmentInfoDto();
            dto.Id = ReadProperty<Int32>(IdProperty);
            dto.EntityId = ReadProperty<Int32>(EntityIdProperty);
            dto.ConsumerDecisionTreeId = ReadProperty<Int32?>(ConsumerDecisionTreeIdProperty);
            dto.Name = ReadProperty<String>(NameProperty);
            dto.ProductGroupName = ReadProperty<String>(ProductGroupNameProperty);
            dto.ProductGroupCode = ReadProperty<String>(ProductGroupCodeProperty);
            dto.ProductGroupId = ReadProperty<Int32>(ProductGroupIdProperty);
            dto.DateCreated = ReadProperty<DateTime>(DateCreatedProperty);
            dto.DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty);
            dto.ParentUniqueContentReference = ReadProperty<Guid?>(ParentUniqueContentReferenceProperty);
            return dto;
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, AssortmentInfoDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #endregion
    }
}