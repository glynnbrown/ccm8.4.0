﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
#endregion
#region Version History: (CCM CCM830)
// V8-32524 : L.Ineson
//  Added FixtureAnnotations and simplified updated back to plan
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Model;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of FixtureAssemblyComponent objects.
    /// </summary>
    [Serializable]
    public sealed partial class FixtureAssemblyComponentList : ModelList<FixtureAssemblyComponentList, FixtureAssemblyComponent>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new FixtureAssembly Parent
        {
            get { return (FixtureAssembly)base.Parent; }
        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static FixtureAssemblyComponentList NewFixtureAssemblyComponentList()
        {
            FixtureAssemblyComponentList item = new FixtureAssemblyComponentList();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        internal static FixtureAssemblyComponentList NewFixtureAssemblyComponentList(PlanogramAssemblyComponentList planAssemblyComponents)
        {
            FixtureAssemblyComponentList item = new FixtureAssemblyComponentList();
            item.Create(planAssemblyComponents);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(PlanogramAssemblyComponentList planAssemblyComponents)
        {
            this.RaiseListChangedEvents = false;
            foreach (PlanogramAssemblyComponent planAssemblyComponent in planAssemblyComponents)
            {
                Add(FixtureAssemblyComponent.NewFixtureAssemblyComponent(planAssemblyComponent));
            }
            this.RaiseListChangedEvents = true;

            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion
    }

}