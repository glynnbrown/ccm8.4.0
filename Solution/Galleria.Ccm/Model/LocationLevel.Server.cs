﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using System.Collections.Generic;
using Csla.Core;

namespace Galleria.Ccm.Model
{
    public partial class LocationLevel
    {
        #region Constructor
        private LocationLevel() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static LocationLevel Fetch(IDalContext dalContext, LocationLevelDto dto, IEnumerable<LocationLevelDto> hierarchyLevelList)
        {
            return DataPortal.FetchChild<LocationLevel>(dalContext, dto, hierarchyLevelList);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, LocationLevelDto dto, IEnumerable<LocationLevelDto> hierarchyLevelList)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Byte>(ShapeNoProperty, dto.ShapeNo);
            this.LoadProperty<Int32>(ColourProperty, dto.Colour);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);

            LoadProperty<LocationLevelList>(ChildLevelListProperty,
                LocationLevelList.FetchByParentLocationLevelId(dalContext, dto.Id, hierarchyLevelList));

        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private LocationLevelDto GetDataTransferObject()
        {
            return new LocationLevelDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                LocationHierarchyId = this.ParentHierarchy.Id,
                Name = ReadProperty<String>(NameProperty),
                ShapeNo = ReadProperty<Byte>(ShapeNoProperty),
                Colour = ReadProperty<Int32>(ColourProperty),
                ParentLevelId = (this.ParentLevel != null) ? (Int32?)this.ParentLevel.Id : null,
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty),

            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, LocationLevelDto dto, IEnumerable<LocationLevelDto> hierarchyLevelList)
        {
            this.LoadDataTransferObject(dalContext, dto, hierarchyLevelList);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext)
        {
            LocationLevelDto dto = this.GetDataTransferObject();
            Int32 oldId = dto.Id;
            using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            dalContext.RegisterId<LocationLevel>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            if (this.IsSelfDirty)
            {
                LocationLevelDto dto = this.GetDataTransferObject();
                using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
            {
                dal.DeleteById(this.Id);
            }

            //GEM:14222
            //mark any child levels of this as deleted
            foreach (LocationLevel child in this.ChildLevelList)
            {
                //forcibly mark the child as deleted
                ((IEditableBusinessObject)child).DeleteChild();
            }

            //make sure this updates its children so they get deleted if required.
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #endregion

    }
}