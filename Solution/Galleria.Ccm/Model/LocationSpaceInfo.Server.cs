﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#region Version History: (CCM 8.0.3)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class LocationSpaceInfo
    {
        #region Constructor
        private LocationSpaceInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an LocationSpaceInfo from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        /// <returns>An LocationSpaceInfo object</returns>
        internal static LocationSpaceInfo GetLocationSpaceInfo(IDalContext dalContext, LocationSpaceInfoDto dto)
        {
            return DataPortal.FetchChild<LocationSpaceInfo>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, LocationSpaceInfoDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Guid>(UniqueContentReferenceProperty, dto.UniqueContentReference);
            LoadProperty<Int16>(LocationIdProperty, dto.LocationId);
            LoadProperty<String>(LocationCodeProperty, dto.LocationCode);
            LoadProperty<String>(LocationNameProperty, dto.LocationName);
            LoadProperty<Guid?>(ParentUniqueContentReferenceProperty, dto.ParentUniqueContentReference);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, LocationSpaceInfoDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #endregion
    }
}
