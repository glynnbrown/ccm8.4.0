﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630: A.Kuszyk
//  Created (copied from SA).
// V8-26560 : A.Probyn
//  Added FetchByProductUniverseIdsCriteria
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// ProductUniverseInfoList
    /// </summary>
    [Serializable]
    public sealed partial class ProductUniverseInfoList : ModelReadOnlyList<ProductUniverseInfoList, ProductUniverseInfo>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ProductUniverseInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ProductUniverseInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(ProductUniverseInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ProductUniverseInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByEntityId
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : Csla.CriteriaBase<FetchByEntityIdCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
            RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
            }
        }

        /// <summary>
        /// Criteria for ProductUniverseInfoList.FetchByProductUniverseIds
        /// </summary>
        [Serializable]
        public class FetchByProductUniverseIdsCriteria : CriteriaBase<FetchByProductUniverseIdsCriteria>
        {
            #region Properties

            public static PropertyInfo<IEnumerable<Int32>> ProductUniverseIdsProperty =
                RegisterProperty<IEnumerable<Int32>>(c => c.ProductUniverseIds);
            public IEnumerable<Int32> ProductUniverseIds
            {
                get { return ReadProperty<IEnumerable<Int32>>(ProductUniverseIdsProperty); }
            }

            #endregion

            public FetchByProductUniverseIdsCriteria(IEnumerable<Int32> locationIds)
            {
                LoadProperty<IEnumerable<Int32>>(ProductUniverseIdsProperty, locationIds);
            }
        }

        /// <summary>
        /// Criteria for FetchByProductGroupId
        /// </summary>
        [Serializable]
        public class FetchByProductGroupIdCriteria : Csla.CriteriaBase<FetchByProductGroupIdCriteria>
        {
            public static readonly PropertyInfo<Int32> ProductGroupIdProperty =
            RegisterProperty<Int32>(c => c.ProductGroupId);
            public Int32 ProductGroupId
            {
                get { return ReadProperty<Int32>(ProductGroupIdProperty); }
            }

            public FetchByProductGroupIdCriteria(Int32 productGroupId)
            {
                LoadProperty<Int32>(ProductGroupIdProperty, productGroupId);
            }
        }

        #endregion
    }
}
