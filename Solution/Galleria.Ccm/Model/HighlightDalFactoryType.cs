﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-25436 : L.Luong
//  Created
// V8-27941 : J.Pickup
//  Introduced CcmRepository
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// An enumeration that defines the highlight dal factory type
    /// which indicates the source of a highlight
    /// </summary>
    public enum HighlightDalFactoryType
    {
        Unknown = 0,
        FileSystem = 1,
    }
}
