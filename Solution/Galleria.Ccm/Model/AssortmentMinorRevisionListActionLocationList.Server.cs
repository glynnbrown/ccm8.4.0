﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentMinorRevisionListActionLocationList
    {
        #region Constructors
        private AssortmentMinorRevisionListActionLocationList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an existing Assortment location list
        /// </summary>
        /// <param name="childData"></param>
        /// <returns>A new Assortment location list</returns>
        internal static AssortmentMinorRevisionListActionLocationList FetchByAssortmentMinorRevisionListActionId(IDalContext dalContext, Int32 AssortmentMinorRevisionListActionId)
        {
            return DataPortal.FetchChild<AssortmentMinorRevisionListActionLocationList>(dalContext, AssortmentMinorRevisionListActionId);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning an existing list
        /// </summary>
        /// <param name="reviewId">The parent Assortment id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 AssortmentMinorRevisionActionId)
        {
            RaiseListChangedEvents = false;
            using (IAssortmentMinorRevisionListActionLocationDal dal = dalContext.GetDal<IAssortmentMinorRevisionListActionLocationDal>())
            {
                IEnumerable<AssortmentMinorRevisionListActionLocationDto> dtoList = dal.FetchByAssortmentMinorRevisionListActionId(AssortmentMinorRevisionActionId);
                foreach (AssortmentMinorRevisionListActionLocationDto dto in dtoList)
                {
                    this.Add(AssortmentMinorRevisionListActionLocation.FetchAssortmentMinorRevisionListActionLocation(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}

