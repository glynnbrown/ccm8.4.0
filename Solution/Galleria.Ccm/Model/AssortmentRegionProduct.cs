﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
// V8-26704 : A.Kuszyk
//  Implemented IPlanogramAssortmentRegionProduct
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
#endregion

#region Version History: (CCM 8.3.0)
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion
#endregion

using System;
using AutoMapper;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A class representing an Assortment Regional Product within CCM
    /// (Child of AssortmentRegion)
    /// This is a child Content object and so it has the following properties/actions:
    /// - This object does NOT have a DateDeleted property or DateDeleted field in its supporting database table
    /// - if its parent is deleted, it is not removed or marked as deleted (this allows for parent 'undeletes')
    /// - if it is deleted directly, it's associated record is removed from the database. 
    [Serializable]
    [DefaultNewMethod("NewAssortmentRegionProduct")]
    public partial class AssortmentRegionProduct : ModelObject<AssortmentRegionProduct>,IPlanogramAssortmentRegionProduct
    {
        #region Parent

        /// <summary>
        /// Returns a reference to the parent ScenarioRegion
        /// </summary>
        public AssortmentRegion ParentAssortmentRegion
        {
            get
            {
                AssortmentRegionProductList parentList = base.Parent as AssortmentRegionProductList;
                if (parentList != null)
                {
                    return parentList.Parent as AssortmentRegion;
                }
                return null;
            }
        }

        #endregion

        #region Static Constructor
        static AssortmentRegionProduct()
        {
        }
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The AssortmentRegion id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> PrimaryProductIdProperty =
            RegisterModelProperty<Int32>(c => c.PrimaryProductId, Message.AssortmentRegionProduct_PrimaryProduct);
        /// <summary>
        /// The Primary Product Id
        /// </summary>
        public Int32 PrimaryProductId
        {
            get { return GetProperty<Int32>(PrimaryProductIdProperty); }
            set { SetProperty<Int32>(PrimaryProductIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> PrimaryProductGtinProperty =
            RegisterModelProperty<String>(c => c.PrimaryProductGtin);
        /// <summary>
        /// The Primary product GTIN
        /// </summary>
        public String PrimaryProductGtin
        {
            get { return GetProperty<String>(PrimaryProductGtinProperty); }
            set { SetProperty<String>(PrimaryProductGtinProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> PrimaryProductDescriptionProperty =
            RegisterModelProperty<String>(c => c.PrimaryProductDescription);
        /// <summary>
        /// The Primary product Description
        /// </summary>
        public String PrimaryProductDescription
        {
            get { return GetProperty<String>(PrimaryProductDescriptionProperty); }
        }

        public static readonly ModelPropertyInfo<Int32?> RegionalProductIdProperty =
            RegisterModelProperty<Int32?>(c => c.RegionalProductId);
        /// <summary>
        /// The Regional Product Id
        /// </summary>
        public Int32? RegionalProductId
        {
            get { return GetProperty<Int32?>(RegionalProductIdProperty); }
            set { SetProperty<Int32?>(RegionalProductIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> RegionalProductGTINProperty =
            RegisterModelProperty<String>(c => c.RegionalProductGtin);
        /// <summary>
        /// The Regional product GTIN
        /// </summary>
        public String RegionalProductGtin
        {
            get { return GetProperty<String>(RegionalProductGTINProperty); }
            set { SetProperty<String>(RegionalProductGTINProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> RegionalProductDescriptionProperty =
            RegisterModelProperty<String>(c => c.RegionalProductDescription);
        /// <summary>
        /// The Regional product Description
        /// </summary>
        public String RegionalProductDescription
        {
            get { return GetProperty<String>(RegionalProductDescriptionProperty); }
            set { SetProperty<String>(RegionalProductDescriptionProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> RegionNameProperty =
            RegisterModelProperty<String>(c => c.RegionName);
        /// <summary>
        /// The Region Name
        /// </summary>
        public String RegionName
        {
            get { return GetProperty<String>(RegionNameProperty); }
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new Required(PrimaryProductIdProperty));

            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentRegionProduct), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.AssortmentCreate.ToString()));
            BusinessRules.AddRule(typeof(AssortmentRegionProduct), new IsInRole(AuthorizationActions.GetObject, DomainPermission.AssortmentGet.ToString()));
            BusinessRules.AddRule(typeof(AssortmentRegionProduct), new IsInRole(AuthorizationActions.EditObject, DomainPermission.AssortmentEdit.ToString()));
            BusinessRules.AddRule(typeof(AssortmentRegionProduct), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.AssortmentDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentRegionProduct NewAssortmentRegionProduct()
        {
            AssortmentRegionProduct item = new AssortmentRegionProduct();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentRegionProduct NewAssortmentRegionalProduct(AssortmentRegion assortmentRegion, AssortmentProduct primaryProduct)
        {
            AssortmentRegionProduct item = new AssortmentRegionProduct();
            item.Create(assortmentRegion, primaryProduct);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }


        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create(AssortmentRegion assortmentRegion, AssortmentProduct primaryProduct)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(PrimaryProductIdProperty, primaryProduct.ProductId);
            this.LoadProperty<String>(PrimaryProductGtinProperty, primaryProduct.Gtin);
            this.LoadProperty<String>(PrimaryProductDescriptionProperty, primaryProduct.Name);
            this.LoadProperty<String>(RegionNameProperty, assortmentRegion.Name);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion
    }
}