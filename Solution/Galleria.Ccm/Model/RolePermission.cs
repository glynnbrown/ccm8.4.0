﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26234 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A list of all permissions for a role
    /// </summary>
    [Serializable]
    public partial class RolePermission : ModelObject<RolePermission>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The unique id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region Permission Type
        /// <summary>
        /// PermissionType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> PermissionTypeProperty =
            RegisterModelProperty<Int16>(c => c.PermissionType);
        /// <summary>
        /// The permission type
        /// </summary>
        public Int16 PermissionType
        {
            get { return GetProperty<Int16>(PermissionTypeProperty); }
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(RolePermission), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.RoleCreate.ToString()));
            BusinessRules.AddRule(typeof(RolePermission), new IsInRole(AuthorizationActions.GetObject, DomainPermission.RoleGet.ToString()));
            BusinessRules.AddRule(typeof(RolePermission), new IsInRole(AuthorizationActions.EditObject, DomainPermission.RoleEdit.ToString()));
            BusinessRules.AddRule(typeof(RolePermission), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.RoleDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new role permission
        /// </summary>
        /// <param name="permissionType">The permission type</param>
        /// <returns>A new role permission</returns>
        public static RolePermission NewRolePermission(Int16 permissionType)
        {
            RolePermission item = new RolePermission();
            item.Create(permissionType);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="permissionType"></param>
        private void Create(Int16 permissionType)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int16>(PermissionTypeProperty, permissionType);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}
