﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramStatusInfoList
    {
        #region Constructors
        public PlanogramStatusInfoList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        #region FetchByEntityId
        /// <summary>
        /// Returns all status' for the specified entity
        /// </summary>
        public static PlanogramStatusInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<PlanogramStatusInfoList>(new FetchByEntityIdCriteria(entityId));
        }

        /// <summary>
        /// Returns all status' for the specified entity
        /// </summary>
        public static PlanogramStatusInfoList FetchByEntityId(EntityInfo entity)
        {
            return DataPortal.Fetch<PlanogramStatusInfoList>(new FetchByEntityIdCriteria(entity.Id));
        }

        /// <summary>
        /// Returns all status' for the specified entity
        /// </summary>
        public static PlanogramStatusInfoList FetchByEntityId(Entity entity)
        {
            return DataPortal.Fetch<PlanogramStatusInfoList>(new FetchByEntityIdCriteria(entity.Id));
        }
        #endregion

        #region FetchByEntityIdIncludingDeleted
        /// <summary>
        /// Returns all status' for the specified entity including deleted
        /// </summary>
        public static PlanogramStatusInfoList FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            return DataPortal.Fetch<PlanogramStatusInfoList>(new FetchByEntityIdIncludingDeletedCriteria(entityId));
        }

        /// <summary>
        /// Returns all status' for the specified entity including deleted
        /// </summary>
        public static PlanogramStatusInfoList FetchByEntityIdIncludingDeleted(EntityInfo entity)
        {
            return DataPortal.Fetch<PlanogramStatusInfoList>(new FetchByEntityIdIncludingDeletedCriteria(entity.Id));
        }

        /// <summary>
        /// Returns all status' for the specified entity including deleted
        /// </summary>
        public static PlanogramStatusInfoList FetchByEntityIdIncludingDeleted(Entity entity)
        {
            return DataPortal.Fetch<PlanogramStatusInfoList>(new FetchByEntityIdIncludingDeletedCriteria(entity.Id));
        }
        #endregion

        #endregion

        #region Data Access
        /// <summary>
        /// Called when returning all status' for the specified entity
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramStatusInfoDal dal = dalContext.GetDal<IPlanogramStatusInfoDal>())
                {
                    IEnumerable<PlanogramStatusInfoDto> dtoList = dal.FetchByEntityId(criteria.EntityId);
                    foreach (PlanogramStatusInfoDto dto in dtoList)
                    {
                        this.Add(PlanogramStatusInfo.GetPlanogramStatusInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returning all status' for the specified entity including deleted
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdIncludingDeletedCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanogramStatusInfoDal dal = dalContext.GetDal<IPlanogramStatusInfoDal>())
                {
                    IEnumerable<PlanogramStatusInfoDto> dtoList = dal.FetchByEntityIdIncludingDeleted(criteria.EntityId);
                    foreach (PlanogramStatusInfoDto dto in dtoList)
                    {
                        this.Add(PlanogramStatusInfo.GetPlanogramStatusInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }
        #endregion
    }
}
