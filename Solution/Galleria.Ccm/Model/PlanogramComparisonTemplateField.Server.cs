﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public sealed partial class PlanogramComparisonTemplateField
    {
        #region Constructor

        /// <summary>
        ///     Private constructor to enforce use of factory methods.
        /// </summary>
        public PlanogramComparisonTemplateField() {}

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Called by reflection when fetching an item from a <paramref name="dto" />.
        /// </summary>
        internal static PlanogramComparisonTemplateField Fetch(IDalContext dalContext, PlanogramComparisonTemplateFieldDto dto)
        {
            return DataPortal.FetchChild<PlanogramComparisonTemplateField>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Load the data from the given <paramref name="dto" /> into this instance.
        /// </summary>
        /// <param name="dalContext">[<c>not used</c>]</param>
        /// <param name="dto">The <see cref="PlanogramComparisonTemplateFieldDto" /> containing the values to load into this instance.</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramComparisonTemplateFieldDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(ItemTypeProperty, dto.ItemType);
            LoadProperty(FieldPlaceholderProperty, dto.FieldPlaceholder);
            LoadProperty(DisplayNameProperty, dto.DisplayName);
            LoadProperty(NumberProperty, dto.Number);
            LoadProperty(DisplayProperty, dto.Display);
        }

        /// <summary>
        ///     Create a data transfer object from the data in this instance.
        /// </summary>
        /// <param name="parent">Planogram Comparison that contains this instance.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonTemplateFieldDto" /> with the data in this instance.</returns>
        private PlanogramComparisonTemplateFieldDto GetDataTransferObject(PlanogramComparisonTemplate parent)
        {
            return new PlanogramComparisonTemplateFieldDto
                   {
                       Id = ReadProperty(IdProperty),
                       PlanogramComparisonTemplateId = parent.Id,
                       ItemType = (Byte) ReadProperty(ItemTypeProperty),
                       FieldPlaceholder = ReadProperty(FieldPlaceholderProperty),
                       DisplayName = ReadProperty(DisplayNameProperty),
                       Number = ReadProperty(NumberProperty),
                       Display = ReadProperty(DisplayProperty)
                   };
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Called by reflection when fetching an instance of <see cref="PlanogramComparisonTemplateField" />.
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, PlanogramComparisonTemplateFieldDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert

        /// <summary>
        ///     Called by reflection when inserting an instance of <see cref="PlanogramComparisonTemplateField" />.
        /// </summary>
        private void Child_Insert(IDalContext dalContext, PlanogramComparisonTemplate parent)
        {
            PlanogramComparisonTemplateFieldDto dto = GetDataTransferObject(parent);
            var oldId = (Int32) dto.Id;
            using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateFieldDal>())
            {
                dal.Insert(dto);
            }
            LoadProperty(IdProperty, (Int32) dto.Id);
            dalContext.RegisterId<PlanogramComparisonTemplateField>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Update

        /// <summary>
        ///     Called by reflection when updating an instance of <see cref="PlanogramComparisonTemplateField" />.
        /// </summary>
        private void Child_Update(IDalContext dalContext, PlanogramComparisonTemplate parent)
        {
            if (IsSelfDirty)
            {
                using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateFieldDal>())
                {
                    dal.Update(GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Called by reflection when deleting an instance of <see cref="PlanogramComparisonTemplate" />.
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, PlanogramComparisonTemplate parent)
        {
            using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateFieldDal>())
            {
                dal.DeleteById(Id);
            }
        }

        #endregion

        #endregion
    }
}