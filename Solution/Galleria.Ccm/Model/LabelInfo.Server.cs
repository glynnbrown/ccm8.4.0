﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24265 : N.Haywood
//  Created
// V8-27940 : L.Luong
//  Added EntityId and DateDeleted
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public partial class LabelInfo
    {
        #region Constructors
        private LabelInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static LabelInfo GetLabelInfo(IDalContext dalContext, LabelInfoDto dto)
        {
            return DataPortal.FetchChild<LabelInfo>(dalContext, dto);
        }

        /// <summary>
        /// Returns an info of an existing object
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        internal static LabelInfo GetLabelInfo(IDalContext dalContext, LabelInfoDto dto, String fileName)
        {
            return DataPortal.FetchChild<LabelInfo>(dalContext, dto, fileName);
        }


        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, LabelInfoDto dto)
        {
            LoadProperty<Object>(IdProperty, dto.Id);
            LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<LabelType>(TypeProperty, (LabelType)dto.Type);
            LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, LabelInfoDto dto, String fileName)
        {
            LoadProperty<Object>(IdProperty, dto.Id);
            LoadProperty<String>(FileNameProperty, fileName);
            LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<LabelType>(TypeProperty, (LabelType)dto.Type);
            LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when loading this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, LabelInfoDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        /// <summary>
        /// Called when loading this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, LabelInfoDto dto, String fileName)
        {
            LoadDataTransferObject(dalContext, dto, fileName);
        }

        #endregion

        #endregion
    }
}
