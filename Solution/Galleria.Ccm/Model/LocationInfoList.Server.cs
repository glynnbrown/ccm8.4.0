﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25628 : A.Kuszyk
//		Created (Copied from SA)
// V8-25454 : J.Pickup
//		Added FetchByEntityIdIncludingDeleted factory method
// V8-27918 : J.Pickup
//      Added FetchByWorkpackageIdIncludingDeleted()
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Model
{
    public partial class LocationInfoList
    {
        #region Constructors
        private LocationInfoList() { } // force the use of factory methods
        #endregion

        #region FactoryMethods

        public static LocationInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<LocationInfoList>(new FetchByEntityIdCriteria(entityId));
        }

        public static LocationInfoList FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            return DataPortal.Fetch<LocationInfoList>(new FetchByEntityIdIncludingDeletedCriteria(entityId));
        }

        public static LocationInfoList FetchByEntityIdLocationCodes(Int32 entityId, IEnumerable<String> locationCodes)
        {
            return DataPortal.Fetch<LocationInfoList>(new FetchByEntityIdLocationCodesCriteria(entityId, locationCodes));
        }

        public static LocationInfoList FetchByWorkpackageIdIncludingDeleted(Int32 workpackageId)
        {
            return DataPortal.Fetch<LocationInfoList>(new FetchByWorkpackageIdIncludingDeletedCriteria(workpackageId));
        }

        public static LocationInfoList FetchByWorkpackagePlanogramIdIncludingDeleted(Int32 workpackageId)
        {
            return DataPortal.Fetch<LocationInfoList>(new FetchByWorkpackagePlanogramIdIncludingDeletedCriteria(workpackageId));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when retrieving a list of all locations for an entity
        /// Note: This procedure should NOT return deleted items
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationInfoDal dal = dalContext.GetDal<ILocationInfoDal>())
                {
                    IEnumerable<LocationInfoDto> dtoList = dal.FetchByEntityId(criteria.EntityId);

                    foreach (LocationInfoDto dto in dtoList)
                    {
                        this.Add(LocationInfo.FetchLocationInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when retrieving a list of all locations for an entity
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdIncludingDeletedCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationInfoDal dal = dalContext.GetDal<ILocationInfoDal>())
                {
                    IEnumerable<LocationInfoDto> dtoList = dal.FetchByEntityIdIncludingDeleted(criteria.EntityId);

                    foreach (LocationInfoDto dto in dtoList)
                    {
                        this.Add(LocationInfo.FetchLocationInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when fetching a list by entity id and location code
        /// Note: This procedure should return deleted items since it is explicitely calling for them by code
        /// </summary>
        /// <param name="criteria"></param>
        private void DataPortal_Fetch(FetchByEntityIdLocationCodesCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationInfoDal dal = dalContext.GetDal<ILocationInfoDal>())
                {
                    IEnumerable<LocationInfoDto> dtoList = dal.FetchByEntityIdLocationCodes(criteria.EntityId, criteria.LocationCodes);

                    foreach (LocationInfoDto dto in dtoList)
                    {
                        this.Add(LocationInfo.FetchLocationInfo(dalContext, dto));
                    }
                }
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }



        /// <summary>
        /// Called when retrieving a list of all locations for a Workpackage Id
        /// </summary>
        private void DataPortal_Fetch(FetchByWorkpackageIdIncludingDeletedCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationInfoDal dal = dalContext.GetDal<ILocationInfoDal>())
                {
                    IEnumerable<LocationInfoDto> dtoList = dal.FetchByWorkpackageIdIncludingDeleted(criteria.WorkpackageId);

                    foreach (LocationInfoDto dto in dtoList)
                    {
                        this.Add(LocationInfo.FetchLocationInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }


        /// <summary>
        /// Called when retrieving a list of all locations for a Workpackage Id
        /// </summary>
        private void DataPortal_Fetch(FetchByWorkpackagePlanogramIdIncludingDeletedCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationInfoDal dal = dalContext.GetDal<ILocationInfoDal>())
                {
                    IEnumerable<LocationInfoDto> dtoList = dal.FetchByWorkpackagePlanogramIdIncludingDeleted(criteria.PlanogramId);

                    foreach (LocationInfoDto dto in dtoList)
                    {
                        this.Add(LocationInfo.FetchLocationInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}
