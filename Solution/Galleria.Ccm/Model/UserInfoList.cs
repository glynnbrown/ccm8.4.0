﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-26222 : L.Luong
//		Created (Auto-generated)

#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{

    /// <summary>
    /// Represents a readonly list holding UserInfo objects
    /// (root object)
    /// </summary>
    [Serializable]
    public sealed partial class UserInfoList : ModelReadOnlyList<UserInfoList, UserInfo>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(UserInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(UserInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(UserInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(UserInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Criteria

        #region FetchAllCriteria

        /// <summary>
        /// Criteria for the FetchAll factory method
        /// </summary>
        [Serializable]
        public class FetchAllCriteria : CriteriaBase<FetchAllCriteria>
        {
            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchAllCriteria()
            {
            }
            #endregion
        }

        #endregion



        #endregion
    }
}
