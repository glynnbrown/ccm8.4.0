﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//      Initial version.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    public enum FixtureSubComponentZMerchStrategyType
    {
        Manual = 0,
        Front = 1,
        FrontStacked = 2,
        Back = 3,
        BackStacked = 4,
        Even = 5,
    }

    /// <summary>
    /// PlanogramSubComponentZMerchStrategyType Helper Class
    /// </summary>
    public static class FixtureSubComponentZMerchStrategyTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<FixtureSubComponentZMerchStrategyType, String> FriendlyNames =
            new Dictionary<FixtureSubComponentZMerchStrategyType, String>()
            {
                {FixtureSubComponentZMerchStrategyType.Manual, Message.Enum_FixtureSubComponentZMerchStrategyType_Manual},
                {FixtureSubComponentZMerchStrategyType.Front, Message.Enum_FixtureSubComponentZMerchStrategyType_Front},
                {FixtureSubComponentZMerchStrategyType.FrontStacked, Message.Enum_FixtureSubComponentZMerchStrategyType_FrontStacked},
                {FixtureSubComponentZMerchStrategyType.Back, Message.Enum_FixtureSubComponentZMerchStrategyType_Back},
                {FixtureSubComponentZMerchStrategyType.BackStacked, Message.Enum_FixtureSubComponentZMerchStrategyType_BackStacked},
                {FixtureSubComponentZMerchStrategyType.Even, Message.Enum_FixtureSubComponentZMerchStrategyType_Even},
            };

        public static FixtureSubComponentZMerchStrategyType Parse(String enumName)
        {
            return EnumHelper.Parse<FixtureSubComponentZMerchStrategyType>(enumName, FixtureSubComponentZMerchStrategyType.Front);
        }
    }
}
