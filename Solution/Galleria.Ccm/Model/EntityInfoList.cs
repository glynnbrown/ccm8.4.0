﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25559 : L.Hodson
//		Copied from SA
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Entity info list
    /// </summary>
    [Serializable]
    public sealed partial class EntityInfoList : ModelReadOnlyList<EntityInfoList, EntityInfo>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(EntityInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(EntityInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(EntityInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(EntityInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Criteria

        #region FetchAllCriteria

        // <summary>
        /// Criteria for the FetchAll factory method
        /// </summary>
        [Serializable]
        public class FetchAllCriteria : CriteriaBase<FetchAllCriteria>
        {
            #region Properties

            #region OverrideSecurity
            /// <summary>
            /// OverrideSecurity property definition
            /// </summary>
            public static readonly PropertyInfo<Boolean> OverrideSecurityProperty =
                RegisterProperty<Boolean>(c => c.OverrideSecurity);
            /// <summary>
            /// OverrideSecurity
            /// </summary>
            public Boolean OverrideSecurity
            {
                get { return this.ReadProperty<Boolean>(OverrideSecurityProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchAllCriteria()
            {
            }

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchAllCriteria(Boolean overrideSecurity)
            {
                LoadProperty<Boolean>(OverrideSecurityProperty, overrideSecurity);
            }
            #endregion
        }

        #endregion

        #endregion
    }
}
