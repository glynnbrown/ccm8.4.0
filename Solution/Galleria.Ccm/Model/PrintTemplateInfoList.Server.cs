﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public sealed partial class PrintTemplateInfoList
    {
        #region Constructor
        private PrintTemplateInfoList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of items for the given entity id.
        /// </summary>
        public static PrintTemplateInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<PrintTemplateInfoList>(new FetchByEntityIdCriteria(entityId));
        }

        /// <summary>
        /// Returns a list of items for the given ids.
        /// </summary>
        public static PrintTemplateInfoList FetchByIds(IEnumerable<Object> idList)
        {
            return DataPortal.Fetch<PrintTemplateInfoList>(
                new FetchByIdsCriteria(PrintTemplate.PrintTemplateDalFactoryType.Unknown, 
                    idList.ToList()));
        }

        /// <summary>
        /// Returns a list of items for the given ids.
        /// </summary>
        public static PrintTemplateInfoList FetchByFileNames(IEnumerable<String> fileNameList)
        {
            return DataPortal.Fetch<PrintTemplateInfoList>(
                new FetchByIdsCriteria(PrintTemplate.PrintTemplateDalFactoryType.FileSystem, 
                    fileNameList.Cast<Object>().ToList()));
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Called when retrieving using FetchByEntityIdCriteria
        /// </summary>
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(criteria.DalType, out dalFactoryName);

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPrintTemplateInfoDal dal = dalContext.GetDal<IPrintTemplateInfoDal>())
                {
                    IEnumerable<PrintTemplateInfoDto> dtoList = dal.FetchByEntityId(criteria.EntityId);
                    foreach (PrintTemplateInfoDto dto in dtoList)
                    {
                        this.Add(PrintTemplateInfo.GetPrintTemplateInfo(dalContext, dto));
                    }
                }
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }


        /// <summary>
        /// Called when retrieving using FetchByIdsCriteria
        /// </summary>
        private void DataPortal_Fetch(FetchByIdsCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(criteria.DalType, out dalFactoryName);

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPrintTemplateInfoDal dal = dalContext.GetDal<IPrintTemplateInfoDal>())
                {
                    IEnumerable<PrintTemplateInfoDto> dtoList = dal.FetchByIds(criteria.Ids);
                    foreach (PrintTemplateInfoDto dto in dtoList)
                    {
                        this.Add(PrintTemplateInfo.GetPrintTemplateInfo(dalContext, dto));
                    }
                }
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the correct dal factory based on the given type and args.
        /// </summary>
        private static IDalFactory GetDalFactory(PrintTemplate.PrintTemplateDalFactoryType dalType, out String dalFactoryName)
        {
            switch (dalType)
            {
                case PrintTemplate.PrintTemplateDalFactoryType.FileSystem:
                    dalFactoryName = Constants.UserDal;
                    return DalContainer.GetDalFactory(dalFactoryName);

                default:
                    dalFactoryName = DalContainer.DalName;
                    return DalContainer.GetDalFactory();
            }

        }

        #endregion

    }
}