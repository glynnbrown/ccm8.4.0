﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26895 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  Aded description, dateStarted, dateLastUpdated properties to Increment Command
#endregion

#region Version History: CCM810
// V8-30079 : L.Ineson
//  Ammended status property enum type.
#endregion

#region Version History: CCM8.2.0
// V8-30646 : M.Shelley
//  Added SetPendingCommand 
#endregion

#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class WorkpackageProcessingStatus : ModelObject<WorkpackageProcessingStatus>
    {
        #region Properties

        #region WorkpackageId
        /// <summary>
        /// WorkpackageId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> WorkpackageIdProperty =
            RegisterModelProperty<Int32>(c => c.WorkpackageId);
        /// <summary>
        /// Returns the workpackage id
        /// </summary>
        public Int32 WorkpackageId
        {
            get { return this.GetProperty<Int32>(WorkpackageIdProperty); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// Returns the row version
        /// </summary>
        public RowVersion RowVersion
        {
            get { return this.GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region Status
        /// <summary>
        /// Status property definition
        /// </summary>
        public static readonly ModelPropertyInfo<WorkpackageProcessingStatusType> StatusProperty =
            RegisterModelProperty<WorkpackageProcessingStatusType>(c => c.Status);
        /// <summary>
        /// Gets or sets the processing status of the workpackage
        /// </summary>
        public WorkpackageProcessingStatusType Status
        {
            get { return this.GetProperty<WorkpackageProcessingStatusType>(StatusProperty); }
            set { this.SetProperty<WorkpackageProcessingStatusType>(StatusProperty, value); }
        }

        #endregion

        #region StatusDescription
        /// <summary>
        /// StatusDescription property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> StatusDescriptionProperty =
            RegisterModelProperty<String>(c => c.StatusDescription);
        /// <summary>
        /// Gets or sets the processing StatusDescription of the workpackage
        /// </summary>
        public String StatusDescription
        {
            get { return this.GetProperty<String>(StatusDescriptionProperty); }
            set { this.SetProperty<String>(StatusDescriptionProperty, value); }
        }

        #endregion

        #region ProgressMax
        /// <summary>
        /// ProgressMax property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProgressMaxProperty =
            RegisterModelProperty<Int32>(c => c.ProgressMax);
        /// <summary>
        /// Gets or sets the max progress value
        /// </summary>
        public Int32 ProgressMax
        {
            get { return this.ReadProperty<Int32>(ProgressMaxProperty); }
            set { this.SetProperty<Int32>(ProgressMaxProperty, value); }
        }
        #endregion

        #region ProgressMin
        /// <summary>
        /// ProgressCurrent property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProgressCurrentProperty =
            RegisterModelProperty<Int32>(c => c.ProgressCurrent);
        /// <summary>
        /// Gets or sets the current progress
        /// </summary>
        public Int32 ProgressCurrent
        {
            get { return this.ReadProperty<Int32>(ProgressCurrentProperty); }
            set { this.SetProperty<Int32>(ProgressCurrentProperty, value); }
        }
        #endregion

        #region DateStarted
        /// <summary>
        /// DateStarted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateStartedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateStarted);
        /// <summary>
        /// Gets or sets the processing Status date started of the workpackage
        /// </summary>
        public DateTime? DateStarted
        {
            get { return this.GetProperty<DateTime?>(DateStartedProperty); }
            set { this.SetProperty<DateTime?>(DateStartedProperty, value); }
        }

        #endregion

        #region DateLastUpdated
        /// <summary>
        /// DateLastUpdated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateLastUpdatedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateLastUpdated);
        /// <summary>
        /// Gets or sets the processing Status date started of the workpackage
        /// </summary>
        public DateTime? DateLastUpdated
        {
            get { return this.GetProperty<DateTime?>(DateLastUpdatedProperty); }
            set { this.SetProperty<DateTime?>(DateLastUpdatedProperty, value); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(WorkpackageProcessingStatus), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkpackageProcessingStatus), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.WorkpackageCreate.ToString()));
            BusinessRules.AddRule(typeof(WorkpackageProcessingStatus), new IsInRole(AuthorizationActions.EditObject, DomainPermission.WorkpackageEdit.ToString()));
            BusinessRules.AddRule(typeof(WorkpackageProcessingStatus), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.WorkpackageDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Increments the progress for the specified workpackage
        /// </summary>
        public static void IncrementProgress(Int32 workpackageId, String statusDescription, DateTime dateLastUpdated)
        {
            DataPortal.Execute<IncrementCommand>(new IncrementCommand(workpackageId, statusDescription, dateLastUpdated));
        }

        public static void SetPending(Int32 workpackageId)
        {
            DataPortal.Execute<SetPendingCommand>(new SetPendingCommand(workpackageId));
        }
        #endregion

        #region Commands

        #region Increment
        /// <summary>
        /// Increments the progess for a workpackage
        /// </summary>
        [Serializable]
        private partial class IncrementCommand : CommandBase<IncrementCommand>
        {
            #region Authorization Rules
            /// <summary>
            /// Defines the authorization rules for this type
            /// </summary>
            private static void AddObjectAuthorizationRules()
            {
                BusinessRules.AddRule(typeof(IncrementCommand), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(IncrementCommand), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(IncrementCommand), new IsInRole(AuthorizationActions.EditObject, DomainPermission.WorkpackageEdit.ToString()));
                BusinessRules.AddRule(typeof(IncrementCommand), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
            }
            #endregion

            #region Properties

            #region WorkpackageId
            /// <summary>
            /// WorkpackageId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> WorkpackageIdProperty =
                RegisterProperty<Int32>(c => c.WorkpackageId);
            /// <summary>
            /// Returns the workpackage id
            /// </summary>
            public Int32 WorkpackageId
            {
                get { return this.ReadProperty<Int32>(WorkpackageIdProperty); }
            }
            #endregion

            #region StatusDescription
            /// <summary>
            /// StatusDescription property definition
            /// </summary>
            public static readonly PropertyInfo<String> StatusDescriptionProperty =
                RegisterProperty<String>(c => c.StatusDescription);
            /// <summary>
            /// Returns the Status Description
            /// </summary>
            public String StatusDescription
            {
                get { return this.ReadProperty<String>(StatusDescriptionProperty); }
            }
            #endregion

            #region Date Last Updated
            /// <summary>
            /// Date Last Updated property definition
            /// </summary>
            public static readonly PropertyInfo<DateTime> DateLastUpdatedProperty =
                RegisterProperty<DateTime>(c => c.DateLastUpdated);
            /// <summary>
            /// Returns the Date Last Updated
            /// </summary>
            public DateTime DateLastUpdated
            {
                get { return this.ReadProperty<DateTime>(DateLastUpdatedProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public IncrementCommand(Int32 workpackageId, String statusDescription, DateTime dateLastUpdated)
            {
                this.LoadProperty<Int32>(WorkpackageIdProperty, workpackageId);
                this.LoadProperty<String>(StatusDescriptionProperty, statusDescription);
                this.LoadProperty<DateTime>(DateLastUpdatedProperty, dateLastUpdated);
            }
            #endregion
        }
        #endregion

        /// <summary>
        /// Sets the processing status of a work package to "Pending"
        /// </summary>
        [Serializable]
        private partial class SetPendingCommand : CommandBase<SetPendingCommand>
        {
            #region Authorization Rules

            /// <summary>
            /// Defines the authorization rules for this type
            /// </summary>
            private static void AddObjectAuthorizationRules()
            {
                BusinessRules.AddRule(typeof(SetPendingCommand), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(SetPendingCommand), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(SetPendingCommand), new IsInRole(AuthorizationActions.EditObject, DomainPermission.WorkpackageEdit.ToString()));
                BusinessRules.AddRule(typeof(SetPendingCommand), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
            }

            #endregion

            #region Properties

            #region WorkpackageId

            /// <summary>
            /// WorkpackageId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> WorkpackageIdProperty =
                RegisterProperty<Int32>(c => c.WorkpackageId);
            /// <summary>
            /// Returns the workpackage id
            /// </summary>
            public Int32 WorkpackageId
            {
                get { return this.ReadProperty<Int32>(WorkpackageIdProperty); }
            }

            #endregion

            #region ProcessingStatus property

            /// <summary>
            /// Workpackage processing status
            /// </summary>
            public static readonly PropertyInfo<WorkpackageProcessingStatusType> ProcessingStatusProperty =
                RegisterProperty<WorkpackageProcessingStatusType>(c => c.ProcessingStatus);
            /// <summary>
            /// Returns the processing status
            /// </summary>
            public WorkpackageProcessingStatusType ProcessingStatus
            {
                get { return this.ReadProperty<WorkpackageProcessingStatusType>(ProcessingStatusProperty); }
            }

            #endregion

            #region StatusDescription
            /// <summary>
            /// StatusDescription property definition
            /// </summary>
            public static readonly PropertyInfo<String> StatusDescriptionProperty =
                RegisterProperty<String>(c => c.StatusDescription);
            /// <summary>
            /// Returns the Status Description
            /// </summary>
            public String StatusDescription
            {
                get { return this.ReadProperty<String>(StatusDescriptionProperty); }
            }
            #endregion

            #endregion

            #region Constructor

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public SetPendingCommand(Int32 workpackageId)
            {
                this.LoadProperty<Int32>(WorkpackageIdProperty, workpackageId);
                this.LoadProperty<WorkpackageProcessingStatusType>(ProcessingStatusProperty, WorkpackageProcessingStatusType.Pending);
                this.LoadProperty<String>(StatusDescriptionProperty, WorkpackageProcessingStatusTypeHelper.FriendlyNames[WorkpackageProcessingStatusType.Pending]);
            }

            #endregion
        }

        #endregion
    }
}
