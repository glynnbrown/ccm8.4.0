﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created (Auto-generated)
// V8-26401 : A.Silva ~ Modified the Create Method to allow instantiating with a default number of groups already in the collection.
// V8-26721 : A.Silva ~ Added support for IValidationTemplate.
// V8-26782 : A.Silva ~ Corrected the naming of new groups so that they are created with a name.
// V8-26809 : A.Silva ~ Added Override to ToString() so that the name is returned wehn using the object with framework helpers.
// V8-26815 : A.Silva ~ Implemented persisting to file as well as to the repository.
// V8-27152 : A.Silva ~ Corrected the creation of a new Validation Template from another IValidationTemplate object.
// V8-27269: A.Kuszyk
//  Added FetchByEntityIdName.
// V8-28476 : N.Haywood
//  Removed authorisation rules as it's handled by the ui and was causing when trying to save a file
#endregion

#region Version History: (CCM v8.03)
// V8-29219 : L.Ineson
//  Removed date deleted
#endregion
#region Version History: (CCM v8.10)
// V8-29730 : L.Ineson
//  Added CreateDefaultValidationTemplate
// V8-30259 : M.Brumby
//  Defatult validation values changed:
//      group3.Threshold2 = 9 instead of 10
#endregion
#region Version History: (CCM v8.20)
// V8-31533 : A.Probyn
//  Updated lazy loaded properties to not be lazy loaded as they were causing serialization exceptions
//  when used in the workflow client.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Represents a validation template.
    ///     A validation template contains up to five validation groups and their associated data.
    ///     A validation template could be persisted to a file or a database.
    /// </summary>
    [Serializable]
    public sealed partial class ValidationTemplate : ModelObject<ValidationTemplate>, IValidationTemplate, IDisposable
    {
        #region Properties

        /// <summary>
        /// Gets the file extension to use for labels.
        /// </summary>
        public static String FileExtension
        {
            get { return Constants.ValidationTemplateFileExtension; }
        }

        #region DalFactoryName

        /// <summary>
        ///		Metadata for the <see cref="DalFactoryName"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<string> DalFactoryNameProperty =
            RegisterModelProperty<string>(o => o.DalFactoryName);

        /// <summary>
        ///		Gets the value for the <see cref="DalFactoryName"/> property.
        /// </summary>
        protected override String DalFactoryName
        {
            get { return GetProperty(DalFactoryNameProperty); }
        }

        #endregion

        #region DalFactoryType

        /// <summary>
        ///		Metadata for the <see cref="DalFactoryType"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<ValidationTemplateType> DalFactoryTypeProperty =
            RegisterModelProperty<ValidationTemplateType>(o => o.DalFactoryType);

        /// <summary>
        ///		Gets or sets the value for the <see cref="DalFactoryType"/> property.
        /// </summary>
        private ValidationTemplateType DalFactoryType
        {
            get { return GetProperty(DalFactoryTypeProperty); }
        }

        #endregion

        #region Id

        /// <summary>
        ///     Metadata for <see cref="Id" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Object> IdProperty =
            RegisterModelProperty<Object>(c => c.Id);

        /// <summary>
        ///     Gets or sets the unique Id value for this instance.
        /// </summary>
        public Object Id
        {
            get { return GetProperty(IdProperty); }
        }

        #endregion

        #region RowVersion

        /// <summary>
        ///     Metadata for the <see cref="RowVersion" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);

        /// <summary>
        ///     Gets the row version timestamp.
        /// </summary>
        private RowVersion RowVersion
        {
            get { return GetProperty(RowVersionProperty); }
        }

        #endregion

        #region EntityId

        /// <summary>
        ///     Metadata for the <see cref="EntityId" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<int> EntityIdProperty =
            RegisterModelProperty<int>(c => c.EntityId);

        /// <summary>
        ///     Gets or sets the unique Id value for the <see cref="Entity" /> model this instance belongs to.
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty(EntityIdProperty); }
            set { SetProperty(EntityIdProperty, value); }
        }

        #endregion

        #region Name

        /// <summary>
        ///     Metadata for the <see cref="Name" /> property.
        /// </summary>
        public static readonly ModelPropertyInfo<string> NameProperty =
            RegisterModelProperty<string>(c => c.Name);

        /// <summary>
        ///     Gets or sets the name used for this instance.
        /// </summary>
        public String Name
        {
            get { return GetProperty(NameProperty); }
            set { SetProperty(NameProperty, value); }
        }

        #endregion

        #region DateCreated

        /// <summary>
        ///     Metadata for the <see cref="DateCreated" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        ///     Gets or sets the date this instance was created originally.
        /// </summary>
        private DateTime DateCreated
        {
            get { return GetProperty(DateCreatedProperty); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        ///     Metadata for the <see cref="DateLastModified" /> property.
        /// </summary>
        private static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        ///     Gets or sets the last date this instance was modified (and persisted).
        /// </summary>
        private DateTime DateLastModified
        {
            get { return GetProperty(DateLastModifiedProperty); }
        }

        #endregion

        #region Groups

        /// <summary>
        ///     Metadata for the <see cref="Groups" /> property.
        /// </summary>
        /// <remarks>This is a child object and lazy loaded.</remarks>
        private static readonly ModelPropertyInfo<ValidationTemplateGroupList> GroupListProperty =
            RegisterModelProperty<ValidationTemplateGroupList>(o => o.Groups);

        /// <summary>
        ///     Gets the <see cref="ValidationTemplateGroupList" /> containing the <see cref="ValidationTemplateGroup" /> items
        ///     belonging to this instance.
        /// </summary>
        /// <remarks>This list is lazy loaded.</remarks>
        public ValidationTemplateGroupList Groups
        {
            get
            {
                return GetProperty(GroupListProperty);
            }
        }

        IEnumerable<IValidationTemplateGroup> IValidationTemplate.Groups
        {
            get { return Groups; }
        }

        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        ///     Adds business rules to this instance.
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
        }

        #endregion

        #region Authorization Rules

        // no authentication rules required as object is accessed through both the editor and workflow

        /// <summary>
        /// Returns a dictionary of the current role permissions allocated to the user for this object type.
        /// </summary>
        /// <returns></returns>
        public static IDictionary<AuthorizationActions, Boolean> GetUserPermissions()
        {
            return new Dictionary<AuthorizationActions, Boolean>()
            {
                {AuthorizationActions.CreateObject, ApplicationContext.User.IsInRole(DomainPermission.ValidationTemplateCreate.ToString())},
                {AuthorizationActions.GetObject, ApplicationContext.User.IsInRole(DomainPermission.ValidationTemplateGet.ToString())},
                {AuthorizationActions.EditObject, ApplicationContext.User.IsInRole(DomainPermission.ValidationTemplateEdit.ToString())},
                {AuthorizationActions.DeleteObject, ApplicationContext.User.IsInRole(DomainPermission.ValidationTemplateDelete.ToString())},
            };
        }

        #endregion

        #region Criteria

        #region FetchByIdCriteria

        /// <summary>
        ///     Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region validationTemplateType

            /// <summary>
            ///		Metadata for the <see cref="DalFactoryType"/> property.
            /// </summary>
            private static readonly PropertyInfo<ValidationTemplateType> DalFactoryTypeProperty =
                RegisterProperty<ValidationTemplateType>(o => o.DalFactoryType);

            /// <summary>
            ///		Gets or sets the value for the <see cref="DalFactoryType"/> property.
            /// </summary>
            public ValidationTemplateType DalFactoryType
            {
                get { return ReadProperty(DalFactoryTypeProperty); }
            }

            #endregion
            #region Id

            /// <summary>
            ///     Metadata for the <see cref="Id" /> property.
            /// </summary>
            private static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);

            /// <summary>
            ///     Gets the unique value used to identifiy the instance to be fetched.
            /// </summary>
            public Object Id
            {
                get { return ReadProperty(IdProperty); }
            }

            #endregion

            #endregion

            #region Constructor

            /// <summary>
            ///     Creates a new instance of <see cref="FetchByIdCriteria" />.
            /// </summary>
            /// <param name="sourceType"></param>
            /// <param name="id">Unique id value identifying the instance to be fetched.</param>
            public FetchByIdCriteria(ValidationTemplateType sourceType, Object id)
            {
                LoadProperty(DalFactoryTypeProperty, sourceType);
                LoadProperty(IdProperty, id);
            }

            #endregion
        }

        #endregion

        #region FetchByEntityIdName Criteria

        [Serializable]
        public class FetchByEntityIdNameCriteria : CriteriaBase<FetchByEntityIdNameCriteria>
        {
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static readonly PropertyInfo<String> NameProperty =
                RegisterProperty<String>(c => c.Name);
            public String Name
            {
                get { return ReadProperty<String>(NameProperty); }
            }

            public FetchByEntityIdNameCriteria(Int32 entityId, String name)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<String>(NameProperty, name);
            }
        }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Creates, initializes and returns a new <see cref="ValidationTemplate" /> instance for the <see cref="Entity" />
        ///     with the provided <paramref name="entityId" />.
        /// </summary>
        /// <param name="entityId">Unique identifier for the <see cref="Entity" /> model that will contain this instance.</param>
        public static ValidationTemplate NewValidationTemplate(Int32 entityId)
        {
            var item = new ValidationTemplate();
            item.Create(entityId);

            return item;
        }

        /// <summary>
        ///     Creates, initializes and returns a new <see cref="ValidationTemplate" /> instance using the given <paramref name="source" /> to set all the initial values.
        /// </summary>
        /// <param name="entityId">Unique identifier for the <see cref="Entity" /> model that will contain this instance.</param>
        /// <param name="source">An instance of a type implementing <see cref="IValidationTemplate"/> to use for the initial data of the new instance.</param>
        public static ValidationTemplate NewValidationTemplate(Int32 entityId, IValidationTemplate source)
        {
            var item = new ValidationTemplate();
            item.Create(entityId, source);

            return item;
        }

        /// <summary>
        /// Unlock the given file.
        /// </summary>
        /// <param name="fileName"></param>
        public static void TryUnlockByFileName(String fileName)
        {
            try
            {
                DataPortal.Execute(new UnlockValidationTemplateCommand(ValidationTemplateType.FileSystem, fileName));
            }
            catch (DataPortalException)
            {
                //just carry on.
            }
        }

        /// <summary>
        ///Locks the validation template with the given id
        /// </summary>
        private static Boolean LockValidationTemplateById(ValidationTemplateType validationTemplateType, Object id)
        {
            return DataPortal.Execute(new LockValidationTemplateCommand(validationTemplateType, id)).Success;
        }

        /// <summary>
        /// Unlocks the validation template with the given id
        /// </summary>
        private static void UnlockValidationTemplateById(ValidationTemplateType validationTemplateType, Object id)
        {
            DataPortal.Execute(new UnlockValidationTemplateCommand(validationTemplateType, id));
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        ///     Called when creating a new object of <see cref="ValidationTemplate" />.
        /// </summary>
        private void Create(Int32 entityId)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty(EntityIdProperty, entityId);
            this.LoadProperty(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty(DateLastModifiedProperty, DateTime.UtcNow);
            this.LoadProperty(GroupListProperty, ValidationTemplateGroupList.NewValidationTemplateGroupList());
            base.Create();
        }

        /// <summary>
        ///     Creates a new <see cref="ValidationTemplate"/> instance initializing it to the values in <paramref name="source"/>.
        /// </summary>
        /// <param name="entityId">Unique Id of the entity this validation template should be in.</param>
        /// <param name="source">An instance of an object implemeting <see cref="IValidationTemplate"/> from which to initialize the new instance.</param>
        private void Create(Int32 entityId, IValidationTemplate source)
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty(EntityIdProperty, entityId);
            this.LoadProperty(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty(DateLastModifiedProperty, DateTime.UtcNow);
            this.LoadProperty(GroupListProperty, ValidationTemplateGroupList.NewValidationTemplateGroupList(source.Groups));
            this.LoadProperty(NameProperty, source.Name);
            base.Create();
        }

        #endregion

        #endregion

        #region Commands

        #region LockValidationTemplateCommand

        /// <summary>
        ///     Encapsulates the locking mechanism for <see cref="ValidationTemplate"/> instances.
        /// </summary>
        [Serializable]
        private partial class LockValidationTemplateCommand : CommandBase<LockValidationTemplateCommand>
        {
            #region Properties

            #region Type

            /// <summary>
            ///		Metadata for the <see cref="Type"/> property.
            /// </summary>
            private static readonly PropertyInfo<ValidationTemplateType> TypeProperty =
                RegisterProperty<ValidationTemplateType>(o => o.Type);

            /// <summary>
            ///		Gets or sets the value for the <see cref="Type"/> property.
            /// </summary>
            private ValidationTemplateType Type
            {
                get { return ReadProperty(TypeProperty); }
            }

            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #region Success

            /// <summary>
            ///		Metadata for the <see cref="Success"/> property.
            /// </summary>
            private static readonly PropertyInfo<Boolean> SuccessProperty =
                RegisterProperty<Boolean>(o => o.Success);

            /// <summary>
            ///		Gets or sets the value for the <see cref="Success"/> property.
            /// </summary>
            internal Boolean Success
            {
                get { return ReadProperty(SuccessProperty); }
                set { LoadProperty(SuccessProperty, value); }
            }

            #endregion

            #endregion

            #region Constructor

            public LockValidationTemplateCommand(ValidationTemplateType validationTemplateType, Object id)
            {
                LoadProperty(TypeProperty, validationTemplateType);
                LoadProperty(IdProperty, id);
            }

            #endregion
        }

        #endregion

        #region UnlockValidationTemplateCommand

        /// <summary>
        ///     Encapsulates the unlocking mechanism for <see cref="ValidationTemplate"/> instances.
        /// </summary>
        [Serializable]
        private partial class UnlockValidationTemplateCommand : CommandBase<UnlockValidationTemplateCommand>
        {
            #region Properties

            #region DalFactoryType
            /// <summary>
            /// SourceType property definition
            /// </summary>
            public static readonly PropertyInfo<ValidationTemplateType> DalFactoryTypeProperty =
                RegisterProperty<ValidationTemplateType>(c => c.DalFactoryType);
            /// <summary>
            /// Returns the source type
            /// </summary>
            public ValidationTemplateType DalFactoryType
            {
                get { return this.ReadProperty<ValidationTemplateType>(DalFactoryTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public UnlockValidationTemplateCommand(ValidationTemplateType sourceType, Object id)
            {
                this.LoadProperty<ValidationTemplateType>(DalFactoryTypeProperty, sourceType);
                this.LoadProperty<Object>(IdProperty, id);
            }
            #endregion
        }

        #endregion

        #endregion

        #region Methods

        public override String ToString()
        {
            return Name;
        }

        /// <summary>
        ///     Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<ValidationTemplate>(oldId, newId);
        }

        #region SaveAs

        public ValidationTemplate SaveAs(String path)
        {
            ValidationTemplate returnValue = null;
            String fileName = path;

            //make sure the file extension is correct.
            fileName = Path.ChangeExtension(path, FileExtension);

            //mark this as a new item if it is not.
            if (!IsNew)
            {
                //if this model is not new then we need to unlock the old one.
                // we do this even if the id is the same so that the original file gets cleared from the cache.
                UnlockValidationTemplateById(this.DalFactoryType, this.Id);

                MarkGraphAsNew();
            }

            LoadProperty<ValidationTemplateType>(DalFactoryTypeProperty, ValidationTemplateType.FileSystem);
            LoadProperty<Object>(IdProperty, fileName);
            this.Name = Path.GetFileNameWithoutExtension(fileName);
            returnValue = Save();

            //V8-25873 : Make sure id is the file name for file system, dal is returning 1
            LoadProperty<Object>(IdProperty, fileName);
            return returnValue;
        }

        public ValidationTemplate SaveAs()
        {
            return SaveAs(ValidationTemplateType.Unknown, DalContainer.DalName, Id);
        }

        private ValidationTemplate SaveAs(ValidationTemplateType newValidationTemplateType, String newDalFactoryName,
            Object newId)
        {
            // Record any old values.
            var oldType = DalFactoryType;
            var oldId = Id;

            // Load the new properties.
            LoadProperty(DalFactoryTypeProperty, newValidationTemplateType);
            LoadProperty(DalFactoryNameProperty, newDalFactoryName);
            LoadProperty(IdProperty, newId);

            // SaveAs always results in a new item.
            if (!IsNew) MarkGraphAsNew();

            // Perform the save.
            var returnValue = Save();

            // Unlock any existing previous item.
            UnlockValidationTemplateById(oldType, oldId);

            // Return the new item.
            return returnValue;
        }

        #endregion
        
        /// <summary>
        /// Creates the default validation template
        /// </summary>
        internal static ValidationTemplate CreateDefaultValidationTemplate(Int32 entityId)
        {
            ValidationTemplate validationTemplate = ValidationTemplate.NewValidationTemplate(entityId);
            validationTemplate.Name = Message.PlanogramValidationTemplate_DefaultTemplate_Name;

            #region Group 1

            ValidationTemplateGroup group1 = ValidationTemplateGroup.NewValidationTemplateGroup(Message.PlanogramValidationTemplate_DefaultTemplate_Group1);
            validationTemplate.Groups.Add(group1);
            group1.Threshold1 = 1;
            group1.Threshold2 = 12;
            group1.ValidationType = PlanogramValidationType.LessThan;
            

            ValidationTemplateGroupMetric g1metric1 = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();
            g1metric1.AggregationType = PlanogramValidationAggregationType.Count;
            g1metric1.Field = "[Planogram.MetaTotalPositionCollisions]";
            g1metric1.Score1 = 0;
            g1metric1.Score2 = 2;
            g1metric1.Score3 = 20;
            g1metric1.Threshold1 = 1;
            g1metric1.Threshold2 = 1;
            g1metric1.ValidationType = PlanogramValidationType.LessThan;
            group1.Metrics.Add(g1metric1);


            ValidationTemplateGroupMetric g1metric2 = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();
            g1metric2.AggregationType = PlanogramValidationAggregationType.Count;
            g1metric2.Field = "[Planogram.MetaTotalComponentCollisions]";
            g1metric2.Score1 = 0;
            g1metric2.Score2 = 2;
            g1metric2.Score3 = 20;
            g1metric2.Threshold1 = 1;
            g1metric2.Threshold2 = 1;
            g1metric2.ValidationType = PlanogramValidationType.LessThan;
            group1.Metrics.Add(g1metric2);

            ValidationTemplateGroupMetric g1metric3 = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();
            g1metric3.AggregationType = PlanogramValidationAggregationType.Avg;
            g1metric3.Field = "[Planogram.MetaTotalComponentsOverMerchandisedWidth]";
            g1metric3.Score1 = 0;
            g1metric3.Score2 = 2;
            g1metric3.Score3 = 20;
            g1metric3.Threshold1 = 1;
            g1metric3.Threshold2 = 1;
            g1metric3.ValidationType = PlanogramValidationType.LessThan;
            group1.Metrics.Add(g1metric3);

            ValidationTemplateGroupMetric g1metric4 = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();
            g1metric4.AggregationType = PlanogramValidationAggregationType.Avg;
            g1metric4.Field = "[Planogram.MetaTotalComponentsOverMerchandisedHeight]";
            g1metric4.Score1 = 0;
            g1metric4.Score2 = 2;
            g1metric4.Score3 = 20;
            g1metric4.Threshold1 = 1;
            g1metric4.Threshold2 = 1;
            g1metric4.ValidationType = PlanogramValidationType.LessThan;
            group1.Metrics.Add(g1metric4);


            ValidationTemplateGroupMetric g1metric5 = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();
            g1metric5.AggregationType = PlanogramValidationAggregationType.Avg;
            g1metric5.Field = "[Planogram.MetaTotalComponentsOverMerchandisedDepth]";
            g1metric5.Score1 = 0;
            g1metric5.Score2 = 2;
            g1metric5.Score3 = 20;
            g1metric5.Threshold1 = 1;
            g1metric5.Threshold2 = 1;
            g1metric5.ValidationType = PlanogramValidationType.LessThan;
            group1.Metrics.Add(g1metric5);

            
            #endregion

            #region Group 2

            ValidationTemplateGroup group2 = ValidationTemplateGroup.NewValidationTemplateGroup(Message.PlanogramValidationTemplate_DefaultTemplate_Group2);
            validationTemplate.Groups.Add(group2);
            group2.Threshold1 = 2;
            group2.Threshold2 = 9;
            group2.ValidationType = PlanogramValidationType.LessThan;

            ValidationTemplateGroupMetric g2metric1 = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();
            g2metric1.AggregationType = PlanogramValidationAggregationType.Sum;
            g2metric1.Field = "[Planogram.MetaPercentOfProductNotAchievedCases]";
            g2metric1.Score1 = 0;
            g2metric1.Score2 = 2;
            g2metric1.Score3 = 10;
            g2metric1.Threshold1 = 2;
            g2metric1.Threshold2 = 5;
            g2metric1.ValidationType = PlanogramValidationType.LessThan;
            group2.Metrics.Add(g2metric1);

            ValidationTemplateGroupMetric g2metric2 = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();
            g2metric2.AggregationType = PlanogramValidationAggregationType.Avg;
            g2metric2.Field = "[Planogram.MetaPercentOfProductNotAchievedDOS]";
            g2metric2.Score1 = 0;
            g2metric2.Score2 = 2;
            g2metric2.Score3 = 10;
            g2metric2.Threshold1 = 2;
            g2metric2.Threshold2 = 5;
            g2metric2.ValidationType = PlanogramValidationType.LessThan;
            group2.Metrics.Add(g2metric2);

            ValidationTemplateGroupMetric g2metric3 = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();
            g2metric3.AggregationType = PlanogramValidationAggregationType.Avg;
            g2metric3.Field = "[Planogram.MetaPercentOfProductNotAchievedInventory]";
            g2metric3.Score1 = 0;
            g2metric3.Score2 = 2;
            g2metric3.Score3 = 10;
            g2metric3.Threshold1 = 2;
            g2metric3.Threshold2 = 5;
            g2metric3.ValidationType = PlanogramValidationType.LessThan;
            group2.Metrics.Add(g2metric3);

            #endregion

            #region Group 3

            ValidationTemplateGroup group3 = ValidationTemplateGroup.NewValidationTemplateGroup(Message.PlanogramValidationTemplate_DefaultTemplate_Group3);
            validationTemplate.Groups.Add(group3);
            group3.Threshold1 = 5;
            group3.Threshold2 = 9;
            group3.ValidationType = PlanogramValidationType.LessThan;

            ValidationTemplateGroupMetric g3metric1 = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();
            g3metric1.AggregationType = PlanogramValidationAggregationType.Avg;
            g3metric1.Field = "[Planogram.MetaTotalLinearWhiteSpace]";
            g3metric1.Score1 = 0;
            g3metric1.Score2 = 5;
            g3metric1.Score3 = 10;
            g3metric1.Threshold1 = 6;
            g3metric1.Threshold2 = 10;
            g3metric1.ValidationType = PlanogramValidationType.LessThan;
            group3.Metrics.Add(g3metric1);

            #endregion

            #region Group 4

            ValidationTemplateGroup group4 = ValidationTemplateGroup.NewValidationTemplateGroup(Message.PlanogramValidationTemplate_DefaultTemplate_Group4);
            validationTemplate.Groups.Add(group4);
            group4.Threshold1 = 5;
            group4.Threshold2 = 9;
            group4.ValidationType = PlanogramValidationType.LessThan;

            ValidationTemplateGroupMetric g4metric1 = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();
            g4metric1.AggregationType = PlanogramValidationAggregationType.Count;
            g4metric1.Field = "[Planogram.MetaPercentOfPlacedProductsRecommendedInAssortment]";
            g4metric1.Score1 = 0;
            g4metric1.Score2 = 0;
            g4metric1.Score3 = 10;
            g4metric1.Threshold1 = 99;
            g4metric1.Threshold2 = 90;
            g4metric1.ValidationType = PlanogramValidationType.GreaterThan;
            group4.Metrics.Add(g4metric1);

            #endregion

            #region Group 5

            ValidationTemplateGroup group5 = ValidationTemplateGroup.NewValidationTemplateGroup(Message.PlanogramValidationTemplate_DefaultTemplate_Group5);
            validationTemplate.Groups.Add(group5);
            group5.Threshold1 = 5;
            group5.Threshold2 = 9;
            group5.ValidationType = PlanogramValidationType.LessThan;


            ValidationTemplateGroupMetric g5metric1 = ValidationTemplateGroupMetric.NewValidationTemplateGroupMetric();
            g5metric1.AggregationType = PlanogramValidationAggregationType.Avg;
            g5metric1.Field = "[Planogram.MetaPercentOfPositionsOutsideOfBlockSpace]";
            g5metric1.Score1 = 0;
            g5metric1.Score2 = 5;
            g5metric1.Score3 = 10;
            g5metric1.Threshold1 = 10;
            g5metric1.Threshold2 = 25;
            g5metric1.ValidationType = PlanogramValidationType.LessThan;
            group5.Metrics.Add(g5metric1);

            #endregion

            return validationTemplate;
        }

        #endregion

        #region IValidationTemplate Members

        IValidationTemplateGroup IValidationTemplate.AddNewGroup(String name)
        {
            return this.Groups.Add(name);
        }

        void IValidationTemplate.RemoveGroup(IValidationTemplateGroup templateGroup)
        {
            var removedItem = templateGroup as ValidationTemplateGroup;
            if (removedItem == null || !Groups.Contains(removedItem)) return;

            Groups.Remove(removedItem);
        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(Boolean disposing)
        {
            if (_isDisposed) return;

            if (disposing)
            {
                if (!String.IsNullOrEmpty(DalFactoryName))
                {
                    UnlockValidationTemplateById(this.DalFactoryType, this.Id);
                }
            }
            _isDisposed = true;
        }

        #endregion
    }
}