﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Ineson
//	Created (Auto-generated)
// V8-26495 : L.Ineson
//  Added property display types
// V8-26182 : L.Ineson
//  Added NewPlanogramFixtureSubComponent
#endregion
#region Version History: CCM802
// V8-29023 : M.Pettit
//  Added Merchandisable Depth
// V8-29203 : L.Ineson
//  Added IsProductSqueezeAllowed
#endregion
#region Version History: (CCM 8.2)
// V8-31055 : L.Ineson
//  Marked colour types
#endregion
#region Version History: (CCM CCM830)
// V8-32524 : L.Ineson
//  Added FixtureAnnotations and simplified updated back to plan
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Helpers;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using PlanogramMessage = Galleria.Framework.Planograms.Resources.Language.Message; 

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// FixtureSubComponent Model object
    /// </summary>
    [Serializable]
    public sealed partial class FixtureSubComponent : ModelObject<FixtureSubComponent>
    {
        #region Static Constructor
        static FixtureSubComponent()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new FixtureComponent Parent
        {
            get
            {
                FixtureSubComponentList parentList = base.Parent as FixtureSubComponentList;
                if (parentList != null)
                {
                    return parentList.Parent;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region Mesh3DId

        /// <summary>
        /// Mesh3DId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> Mesh3DIdProperty =
            RegisterModelProperty<Int32?>(c => c.Mesh3DId);

        /// <summary>
        /// Gets/Sets the Mesh3DId value
        /// </summary>
        public Int32? Mesh3DId
        {
            get { return GetProperty<Int32?>(Mesh3DIdProperty); }
            set { SetProperty<Int32?>(Mesh3DIdProperty, value); }
        }

        #endregion

        #region ImageIdFront

        /// <summary>
        /// ImageIdFront property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> ImageIdFrontProperty =
            RegisterModelProperty<Int32?>(c => c.ImageIdFront);

        /// <summary>
        /// Gets/Sets the ImageIdFront value
        /// </summary>
        public Int32? ImageIdFront
        {
            get { return GetProperty<Int32?>(ImageIdFrontProperty); }
            set { SetProperty<Int32?>(ImageIdFrontProperty, value); }
        }

        #endregion

        #region ImageIdBack

        /// <summary>
        /// ImageIdBack property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> ImageIdBackProperty =
            RegisterModelProperty<Int32?>(c => c.ImageIdBack);

        /// <summary>
        /// Gets/Sets the ImageIdBack value
        /// </summary>
        public Int32? ImageIdBack
        {
            get { return GetProperty<Int32?>(ImageIdBackProperty); }
            set { SetProperty<Int32?>(ImageIdBackProperty, value); }
        }

        #endregion

        #region ImageIdTop

        /// <summary>
        /// ImageIdTop property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> ImageIdTopProperty =
            RegisterModelProperty<Int32?>(c => c.ImageIdTop);

        /// <summary>
        /// Gets/Sets the ImageIdTop value
        /// </summary>
        public Int32? ImageIdTop
        {
            get { return GetProperty<Int32?>(ImageIdTopProperty); }
            set { SetProperty<Int32?>(ImageIdTopProperty, value); }
        }

        #endregion

        #region ImageIdBottom

        /// <summary>
        /// ImageIdBottom property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> ImageIdBottomProperty =
            RegisterModelProperty<Int32?>(c => c.ImageIdBottom);

        /// <summary>
        /// Gets/Sets the ImageIdBottom value
        /// </summary>
        public Int32? ImageIdBottom
        {
            get { return GetProperty<Int32?>(ImageIdBottomProperty); }
            set { SetProperty<Int32?>(ImageIdBottomProperty, value); }
        }

        #endregion

        #region ImageIdLeft

        /// <summary>
        /// ImageIdLeft property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> ImageIdLeftProperty =
            RegisterModelProperty<Int32?>(c => c.ImageIdLeft);

        /// <summary>
        /// Gets/Sets the ImageIdLeft value
        /// </summary>
        public Int32? ImageIdLeft
        {
            get { return GetProperty<Int32?>(ImageIdLeftProperty); }
            set { SetProperty<Int32?>(ImageIdLeftProperty, value); }
        }

        #endregion

        #region ImageIdRight

        /// <summary>
        /// ImageIdRight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> ImageIdRightProperty =
            RegisterModelProperty<Int32?>(c => c.ImageIdRight);

        /// <summary>
        /// Gets/Sets the ImageIdRight value
        /// </summary>
        public Int32? ImageIdRight
        {
            get { return GetProperty<Int32?>(ImageIdRightProperty); }
            set { SetProperty<Int32?>(ImageIdRightProperty, value); }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name, Message.Generic_Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region Height

        /// <summary>
        /// Height property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HeightProperty =
            RegisterModelProperty<Single>(c => c.Height, Message.FixtureSubComponent_Height, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the Height value
        /// </summary>
        public Single Height
        {
            get { return GetProperty<Single>(HeightProperty); }
            set { SetProperty<Single>(HeightProperty, value); }
        }

        #endregion

        #region Width

        /// <summary>
        /// Width property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> WidthProperty =
            RegisterModelProperty<Single>(c => c.Width, Message.FixtureSubComponent_Width, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the Width value
        /// </summary>
        public Single Width
        {
            get { return GetProperty<Single>(WidthProperty); }
            set { SetProperty<Single>(WidthProperty, value); }
        }

        #endregion

        #region Depth

        /// <summary>
        /// Depth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DepthProperty =
            RegisterModelProperty<Single>(c => c.Depth, Message.FixtureSubComponent_Depth, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the Depth value
        /// </summary>
        public Single Depth
        {
            get { return GetProperty<Single>(DepthProperty); }
            set { SetProperty<Single>(DepthProperty, value); }
        }

        #endregion

        #region X

        /// <summary>
        /// X property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> XProperty =
            RegisterModelProperty<Single>(c => c.X, Message.FixtureSubComponent_X, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the X value
        /// </summary>
        public Single X
        {
            get { return GetProperty<Single>(XProperty); }
            set { SetProperty<Single>(XProperty, value); }
        }

        #endregion

        #region Y

        /// <summary>
        /// Y property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> YProperty =
            RegisterModelProperty<Single>(c => c.Y, Message.FixtureSubComponent_Y, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the Y value
        /// </summary>
        public Single Y
        {
            get { return GetProperty<Single>(YProperty); }
            set { SetProperty<Single>(YProperty, value); }
        }

        #endregion

        #region Z

        /// <summary>
        /// Z property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ZProperty =
            RegisterModelProperty<Single>(c => c.Z, Message.FixtureSubComponent_Z, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the Z value
        /// </summary>
        public Single Z
        {
            get { return GetProperty<Single>(ZProperty); }
            set { SetProperty<Single>(ZProperty, value); }
        }

        #endregion

        #region Slope

        /// <summary>
        /// Slope property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> SlopeProperty =
            RegisterModelProperty<Single>(c => c.Slope, Message.FixtureSubComponent_Slope, ModelPropertyDisplayType.Angle);

        /// <summary>
        /// Gets/Sets the Slope value
        /// </summary>
        public Single Slope
        {
            get { return GetProperty<Single>(SlopeProperty); }
            set { SetProperty<Single>(SlopeProperty, value); }
        }

        #endregion

        #region Angle

        /// <summary>
        /// Angle property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> AngleProperty =
            RegisterModelProperty<Single>(c => c.Angle, Message.FixtureSubComponent_Angle, ModelPropertyDisplayType.Angle);

        /// <summary>
        /// Gets/Sets the Angle value
        /// </summary>
        public Single Angle
        {
            get { return GetProperty<Single>(AngleProperty); }
            set { SetProperty<Single>(AngleProperty, value); }
        }

        #endregion

        #region Roll

        /// <summary>
        /// Roll property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> RollProperty =
            RegisterModelProperty<Single>(c => c.Roll, Message.FixtureSubComponent_Roll, ModelPropertyDisplayType.Angle);

        /// <summary>
        /// Gets/Sets the Roll value
        /// </summary>
        public Single Roll
        {
            get { return GetProperty<Single>(RollProperty); }
            set { SetProperty<Single>(RollProperty, value); }
        }

        #endregion

        #region ShapeType

        /// <summary>
        /// ShapeType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureSubComponentShapeType> ShapeTypeProperty =
            RegisterModelProperty<FixtureSubComponentShapeType>(c => c.ShapeType, Message.FixtureSubComponent_ShapeType);

        /// <summary>
        /// Gets/Sets the ShapeType value
        /// </summary>
        public FixtureSubComponentShapeType ShapeType
        {
            get { return GetProperty<FixtureSubComponentShapeType>(ShapeTypeProperty); }
            set { SetProperty<FixtureSubComponentShapeType>(ShapeTypeProperty, value); }
        }

        #endregion

        #region MerchandisableHeight

        /// <summary>
        /// MerchandisableHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MerchandisableHeightProperty =
            RegisterModelProperty<Single>(c => c.MerchandisableHeight, Message.FixtureSubComponent_MerchandisableHeight, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the MerchandisableHeight value
        /// </summary>
        public Single MerchandisableHeight
        {
            get { return GetProperty<Single>(MerchandisableHeightProperty); }
            set { SetProperty<Single>(MerchandisableHeightProperty, value); }
        }

        #endregion

        #region MerchandisableDepth

        /// <summary>
        /// MerchandisableDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MerchandisableDepthProperty =
            RegisterModelProperty<Single>(c => c.MerchandisableDepth, Message.FixtureSubComponent_MerchandisableDepth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the MerchandisableDepth value
        /// </summary>
        public Single MerchandisableDepth
        {
            get { return GetProperty<Single>(MerchandisableDepthProperty); }
            set { SetProperty<Single>(MerchandisableDepthProperty, value); }
        }
        #endregion

        #region IsVisible

        /// <summary>
        /// IsVisible property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsVisibleProperty =
            RegisterModelProperty<Boolean>(c => c.IsVisible, Message.FixtureSubComponent_IsVisible);

        /// <summary>
        /// Gets/Sets the IsVisible value
        /// </summary>
        public Boolean IsVisible
        {
            get { return GetProperty<Boolean>(IsVisibleProperty); }
            set { SetProperty<Boolean>(IsVisibleProperty, value); }
        }

        #endregion

        #region HasCollisionDetection

        /// <summary>
        /// HasCollisionDetection property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> HasCollisionDetectionProperty =
            RegisterModelProperty<Boolean>(c => c.HasCollisionDetection, Message.FixtureSubComponent_HasCollisionDetection);

        /// <summary>
        /// Gets/Sets the HasCollisionDetection value
        /// </summary>
        public Boolean HasCollisionDetection
        {
            get { return GetProperty<Boolean>(HasCollisionDetectionProperty); }
            set { SetProperty<Boolean>(HasCollisionDetectionProperty, value); }
        }

        #endregion

        #region FillPatternTypeFront

        /// <summary>
        /// FillPatternTypeFront property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureSubComponentFillPatternType> FillPatternTypeFrontProperty =
            RegisterModelProperty<FixtureSubComponentFillPatternType>(c => c.FillPatternTypeFront, Message.FixtureSubComponent_FillPatternTypeFront);

        /// <summary>
        /// Gets/Sets the FillPatternTypeFront value
        /// </summary>
        public FixtureSubComponentFillPatternType FillPatternTypeFront
        {
            get { return GetProperty<FixtureSubComponentFillPatternType>(FillPatternTypeFrontProperty); }
            set { SetProperty<FixtureSubComponentFillPatternType>(FillPatternTypeFrontProperty, value); }
        }

        #endregion

        #region FillPatternTypeBack

        /// <summary>
        /// FillPatternTypeBack property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureSubComponentFillPatternType> FillPatternTypeBackProperty =
            RegisterModelProperty<FixtureSubComponentFillPatternType>(c => c.FillPatternTypeBack, Message.FixtureSubComponent_FillPatternTypeBack);

        /// <summary>
        /// Gets/Sets the FillPatternTypeBack value
        /// </summary>
        public FixtureSubComponentFillPatternType FillPatternTypeBack
        {
            get { return GetProperty<FixtureSubComponentFillPatternType>(FillPatternTypeBackProperty); }
            set { SetProperty<FixtureSubComponentFillPatternType>(FillPatternTypeBackProperty, value); }
        }

        #endregion

        #region FillPatternTypeTop

        /// <summary>
        /// FillPatternTypeTop property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureSubComponentFillPatternType> FillPatternTypeTopProperty =
            RegisterModelProperty<FixtureSubComponentFillPatternType>(c => c.FillPatternTypeTop, Message.FixtureSubComponent_FillPatternTypeTop);

        /// <summary>
        /// Gets/Sets the FillPatternTypeTop value
        /// </summary>
        public FixtureSubComponentFillPatternType FillPatternTypeTop
        {
            get { return GetProperty<FixtureSubComponentFillPatternType>(FillPatternTypeTopProperty); }
            set { SetProperty<FixtureSubComponentFillPatternType>(FillPatternTypeTopProperty, value); }
        }

        #endregion

        #region FillPatternTypeBottom

        /// <summary>
        /// FillPatternTypeBottom property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureSubComponentFillPatternType> FillPatternTypeBottomProperty =
            RegisterModelProperty<FixtureSubComponentFillPatternType>(c => c.FillPatternTypeBottom, Message.FixtureSubComponent_FillPatternTypeBottom);

        /// <summary>
        /// Gets/Sets the FillPatternTypeBottom value
        /// </summary>
        public FixtureSubComponentFillPatternType FillPatternTypeBottom
        {
            get { return GetProperty<FixtureSubComponentFillPatternType>(FillPatternTypeBottomProperty); }
            set { SetProperty<FixtureSubComponentFillPatternType>(FillPatternTypeBottomProperty, value); }
        }

        #endregion

        #region FillPatternTypeLeft

        /// <summary>
        /// FillPatternTypeLeft property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureSubComponentFillPatternType> FillPatternTypeLeftProperty =
            RegisterModelProperty<FixtureSubComponentFillPatternType>(c => c.FillPatternTypeLeft, Message.FixtureSubComponent_FillPatternTypeLeft);

        /// <summary>
        /// Gets/Sets the FillPatternTypeLeft value
        /// </summary>
        public FixtureSubComponentFillPatternType FillPatternTypeLeft
        {
            get { return GetProperty<FixtureSubComponentFillPatternType>(FillPatternTypeLeftProperty); }
            set { SetProperty<FixtureSubComponentFillPatternType>(FillPatternTypeLeftProperty, value); }
        }

        #endregion

        #region FillPatternTypeRight

        /// <summary>
        /// FillPatternTypeRight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureSubComponentFillPatternType> FillPatternTypeRightProperty =
            RegisterModelProperty<FixtureSubComponentFillPatternType>(c => c.FillPatternTypeRight, Message.FixtureSubComponent_FillPatternTypeRight);

        /// <summary>
        /// Gets/Sets the FillPatternTypeRight value
        /// </summary>
        public FixtureSubComponentFillPatternType FillPatternTypeRight
        {
            get { return GetProperty<FixtureSubComponentFillPatternType>(FillPatternTypeRightProperty); }
            set { SetProperty<FixtureSubComponentFillPatternType>(FillPatternTypeRightProperty, value); }
        }

        #endregion

        #region FillColourFront

        /// <summary>
        /// FillColourFront property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FillColourFrontProperty =
            RegisterModelProperty<Int32>(c => c.FillColourFront, Message.FixtureSubComponent_FillColorFront,
            ModelPropertyDisplayType.Colour);

        /// <summary>
        /// Gets/Sets the FillColourFront value
        /// </summary>
        public Int32 FillColourFront
        {
            get { return GetProperty<Int32>(FillColourFrontProperty); }
            set { SetProperty<Int32>(FillColourFrontProperty, value); }
        }

        #endregion

        #region FillColourBack

        /// <summary>
        /// FillColourBack property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FillColourBackProperty =
            RegisterModelProperty<Int32>(c => c.FillColourBack, Message.FixtureSubComponent_FillColorBack,
            ModelPropertyDisplayType.Colour);

        /// <summary>
        /// Gets/Sets the FillColourBack value
        /// </summary>
        public Int32 FillColourBack
        {
            get { return GetProperty<Int32>(FillColourBackProperty); }
            set { SetProperty<Int32>(FillColourBackProperty, value); }
        }

        #endregion

        #region FillColourTop

        /// <summary>
        /// FillColourTop property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FillColourTopProperty =
            RegisterModelProperty<Int32>(c => c.FillColourTop, Message.FixtureSubComponent_FillColorTop,
            ModelPropertyDisplayType.Colour);

        /// <summary>
        /// Gets/Sets the FillColourTop value
        /// </summary>
        public Int32 FillColourTop
        {
            get { return GetProperty<Int32>(FillColourTopProperty); }
            set { SetProperty<Int32>(FillColourTopProperty, value); }
        }

        #endregion

        #region FillColourBottom

        /// <summary>
        /// FillColourBottom property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FillColourBottomProperty =
            RegisterModelProperty<Int32>(c => c.FillColourBottom, Message.FixtureSubComponent_FillColorBottom,
            ModelPropertyDisplayType.Colour);

        /// <summary>
        /// Gets/Sets the FillColourBottom value
        /// </summary>
        public Int32 FillColourBottom
        {
            get { return GetProperty<Int32>(FillColourBottomProperty); }
            set { SetProperty<Int32>(FillColourBottomProperty, value); }
        }

        #endregion

        #region FillColourLeft

        /// <summary>
        /// FillColourLeft property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FillColourLeftProperty =
            RegisterModelProperty<Int32>(c => c.FillColourLeft, Message.FixtureSubComponent_FillColorLeft,
            ModelPropertyDisplayType.Colour);

        /// <summary>
        /// Gets/Sets the FillColourLeft value
        /// </summary>
        public Int32 FillColourLeft
        {
            get { return GetProperty<Int32>(FillColourLeftProperty); }
            set { SetProperty<Int32>(FillColourLeftProperty, value); }
        }

        #endregion

        #region FillColourRight

        /// <summary>
        /// FillColourRight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FillColourRightProperty =
            RegisterModelProperty<Int32>(c => c.FillColourRight, Message.FixtureSubComponent_FillColorRight,
            ModelPropertyDisplayType.Colour);

        /// <summary>
        /// Gets/Sets the FillColourRight value
        /// </summary>
        public Int32 FillColourRight
        {
            get { return GetProperty<Int32>(FillColourRightProperty); }
            set { SetProperty<Int32>(FillColourRightProperty, value); }
        }

        #endregion

        #region LineColour

        /// <summary>
        /// LineColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> LineColourProperty =
            RegisterModelProperty<Int32>(c => c.LineColour, Message.FixtureSubComponent_LineColor,
            ModelPropertyDisplayType.Colour);

        /// <summary>
        /// Gets/Sets the LineColour value
        /// </summary>
        public Int32 LineColour
        {
            get { return GetProperty<Int32>(LineColourProperty); }
            set { SetProperty<Int32>(LineColourProperty, value); }
        }

        #endregion

        #region TransparencyPercentFront

        /// <summary>
        /// TransparencyPercentFront property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> TransparencyPercentFrontProperty =
            RegisterModelProperty<Int32>(c => c.TransparencyPercentFront, Message.FixtureSubComponent_TransparencyPercentFront);

        /// <summary>
        /// Gets/Sets the TransparencyPercentFront value
        /// </summary>
        public Int32 TransparencyPercentFront
        {
            get { return GetProperty<Int32>(TransparencyPercentFrontProperty); }
            set { SetProperty<Int32>(TransparencyPercentFrontProperty, value); }
        }

        #endregion

        #region TransparencyPercentBack

        /// <summary>
        /// TransparencyPercentBack property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> TransparencyPercentBackProperty =
            RegisterModelProperty<Int32>(c => c.TransparencyPercentBack, Message.FixtureSubComponent_TransparencyPercentBack);

        /// <summary>
        /// Gets/Sets the TransparencyPercentBack value
        /// </summary>
        public Int32 TransparencyPercentBack
        {
            get { return GetProperty<Int32>(TransparencyPercentBackProperty); }
            set { SetProperty<Int32>(TransparencyPercentBackProperty, value); }
        }

        #endregion

        #region TransparencyPercentTop

        /// <summary>
        /// TransparencyPercentTop property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> TransparencyPercentTopProperty =
            RegisterModelProperty<Int32>(c => c.TransparencyPercentTop, Message.FixtureSubComponent_TransparencyPercentTop);

        /// <summary>
        /// Gets/Sets the TransparencyPercentTop value
        /// </summary>
        public Int32 TransparencyPercentTop
        {
            get { return GetProperty<Int32>(TransparencyPercentTopProperty); }
            set { SetProperty<Int32>(TransparencyPercentTopProperty, value); }
        }

        #endregion

        #region TransparencyPercentBottom

        /// <summary>
        /// TransparencyPercentBottom property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> TransparencyPercentBottomProperty =
            RegisterModelProperty<Int32>(c => c.TransparencyPercentBottom, Message.FixtureSubComponent_TransparencyPercentBottom);

        /// <summary>
        /// Gets/Sets the TransparencyPercentBottom value
        /// </summary>
        public Int32 TransparencyPercentBottom
        {
            get { return GetProperty<Int32>(TransparencyPercentBottomProperty); }
            set { SetProperty<Int32>(TransparencyPercentBottomProperty, value); }
        }

        #endregion

        #region TransparencyPercentLeft

        /// <summary>
        /// TransparencyPercentLeft property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> TransparencyPercentLeftProperty =
            RegisterModelProperty<Int32>(c => c.TransparencyPercentLeft, Message.FixtureSubComponent_TransparencyPercentLeft);

        /// <summary>
        /// Gets/Sets the TransparencyPercentLeft value
        /// </summary>
        public Int32 TransparencyPercentLeft
        {
            get { return GetProperty<Int32>(TransparencyPercentLeftProperty); }
            set { SetProperty<Int32>(TransparencyPercentLeftProperty, value); }
        }

        #endregion

        #region TransparencyPercentRight

        /// <summary>
        /// TransparencyPercentRight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> TransparencyPercentRightProperty =
            RegisterModelProperty<Int32>(c => c.TransparencyPercentRight, Message.FixtureSubComponent_TransparencyPercentRight);

        /// <summary>
        /// Gets/Sets the TransparencyPercentRight value
        /// </summary>
        public Int32 TransparencyPercentRight
        {
            get { return GetProperty<Int32>(TransparencyPercentRightProperty); }
            set { SetProperty<Int32>(TransparencyPercentRightProperty, value); }
        }

        #endregion

        #region FaceThicknessFront

        /// <summary>
        /// FaceThicknessFront property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FaceThicknessFrontProperty =
            RegisterModelProperty<Single>(c => c.FaceThicknessFront, Message.FixtureSubComponent_FaceThicknessFront, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the FaceThicknessFront value
        /// </summary>
        public Single FaceThicknessFront
        {
            get { return GetProperty<Single>(FaceThicknessFrontProperty); }
            set { SetProperty<Single>(FaceThicknessFrontProperty, value); }
        }

        #endregion

        #region FaceThicknessBack

        /// <summary>
        /// FaceThicknessBack property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FaceThicknessBackProperty =
            RegisterModelProperty<Single>(c => c.FaceThicknessBack, Message.FixtureSubComponent_FaceThicknessBack, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the FaceThicknessBack value
        /// </summary>
        public Single FaceThicknessBack
        {
            get { return GetProperty<Single>(FaceThicknessBackProperty); }
            set { SetProperty<Single>(FaceThicknessBackProperty, value); }
        }

        #endregion

        #region FaceThicknessTop

        /// <summary>
        /// FaceThicknessTop property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FaceThicknessTopProperty =
            RegisterModelProperty<Single>(c => c.FaceThicknessTop, Message.FixtureSubComponent_FaceThicknessTop, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the FaceThicknessTop value
        /// </summary>
        public Single FaceThicknessTop
        {
            get { return GetProperty<Single>(FaceThicknessTopProperty); }
            set { SetProperty<Single>(FaceThicknessTopProperty, value); }
        }

        #endregion

        #region FaceThicknessBottom

        /// <summary>
        /// FaceThicknessBottom property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FaceThicknessBottomProperty =
            RegisterModelProperty<Single>(c => c.FaceThicknessBottom, Message.FixtureSubComponent_FaceThicknessBottom, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the FaceThicknessBottom value
        /// </summary>
        public Single FaceThicknessBottom
        {
            get { return GetProperty<Single>(FaceThicknessBottomProperty); }
            set { SetProperty<Single>(FaceThicknessBottomProperty, value); }
        }

        #endregion

        #region FaceThicknessLeft

        /// <summary>
        /// FaceThicknessLeft property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FaceThicknessLeftProperty =
            RegisterModelProperty<Single>(c => c.FaceThicknessLeft, Message.FixtureSubComponent_FaceThicknessLeft, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the FaceThicknessLeft value
        /// </summary>
        public Single FaceThicknessLeft
        {
            get { return GetProperty<Single>(FaceThicknessLeftProperty); }
            set { SetProperty<Single>(FaceThicknessLeftProperty, value); }
        }

        #endregion

        #region FaceThicknessRight

        /// <summary>
        /// FaceThicknessRight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FaceThicknessRightProperty =
            RegisterModelProperty<Single>(c => c.FaceThicknessRight, Message.FixtureSubComponent_FaceThicknessRight, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the FaceThicknessRight value
        /// </summary>
        public Single FaceThicknessRight
        {
            get { return GetProperty<Single>(FaceThicknessRightProperty); }
            set { SetProperty<Single>(FaceThicknessRightProperty, value); }
        }

        #endregion

        #region RiserHeight

        /// <summary>
        /// RiserHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> RiserHeightProperty =
            RegisterModelProperty<Single>(c => c.RiserHeight, Message.FixtureSubComponent_RiserHeight, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the RiserHeight value
        /// </summary>
        public Single RiserHeight
        {
            get { return GetProperty<Single>(RiserHeightProperty); }
            set { SetProperty<Single>(RiserHeightProperty, value); }
        }

        #endregion

        #region RiserThickness

        /// <summary>
        /// RiserThickness property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> RiserThicknessProperty =
            RegisterModelProperty<Single>(c => c.RiserThickness, Message.FixtureSubComponent_RiserThickness, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the RiserThickness value
        /// </summary>
        public Single RiserThickness
        {
            get { return GetProperty<Single>(RiserThicknessProperty); }
            set { SetProperty<Single>(RiserThicknessProperty, value); }
        }

        #endregion

        #region IsRiserPlacedOnFront

        /// <summary>
        /// IsRiserPlacedOnFront property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsRiserPlacedOnFrontProperty =
            RegisterModelProperty<Boolean>(c => c.IsRiserPlacedOnFront, Message.FixtureSubComponent_IsRiserPlacedOnFront);

        /// <summary>
        /// Gets/Sets the IsRiserPlacedOnFront value
        /// </summary>
        public Boolean IsRiserPlacedOnFront
        {
            get { return GetProperty<Boolean>(IsRiserPlacedOnFrontProperty); }
            set { SetProperty<Boolean>(IsRiserPlacedOnFrontProperty, value); }
        }

        #endregion

        #region IsRiserPlacedOnBack

        /// <summary>
        /// IsRiserPlacedOnBack property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsRiserPlacedOnBackProperty =
            RegisterModelProperty<Boolean>(c => c.IsRiserPlacedOnBack, Message.FixtureSubComponent_IsRiserPlacedOnBack);

        /// <summary>
        /// Gets/Sets the IsRiserPlacedOnBack value
        /// </summary>
        public Boolean IsRiserPlacedOnBack
        {
            get { return GetProperty<Boolean>(IsRiserPlacedOnBackProperty); }
            set { SetProperty<Boolean>(IsRiserPlacedOnBackProperty, value); }
        }

        #endregion

        #region IsRiserPlacedOnLeft

        /// <summary>
        /// IsRiserPlacedOnLeft property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsRiserPlacedOnLeftProperty =
            RegisterModelProperty<Boolean>(c => c.IsRiserPlacedOnLeft, Message.FixtureSubComponent_IsRiserPlacedOnLeft);

        /// <summary>
        /// Gets/Sets the IsRiserPlacedOnLeft value
        /// </summary>
        public Boolean IsRiserPlacedOnLeft
        {
            get { return GetProperty<Boolean>(IsRiserPlacedOnLeftProperty); }
            set { SetProperty<Boolean>(IsRiserPlacedOnLeftProperty, value); }
        }

        #endregion

        #region IsRiserPlacedOnRight

        /// <summary>
        /// IsRiserPlacedOnRight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsRiserPlacedOnRightProperty =
            RegisterModelProperty<Boolean>(c => c.IsRiserPlacedOnRight, Message.FixtureSubComponent_IsRiserPlacedOnRight);

        /// <summary>
        /// Gets/Sets the IsRiserPlacedOnRight value
        /// </summary>
        public Boolean IsRiserPlacedOnRight
        {
            get { return GetProperty<Boolean>(IsRiserPlacedOnRightProperty); }
            set { SetProperty<Boolean>(IsRiserPlacedOnRightProperty, value); }
        }

        #endregion

        #region RiserFillPatternType

        /// <summary>
        /// RiserFillPatternType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureSubComponentFillPatternType> RiserFillPatternTypeProperty =
            RegisterModelProperty<FixtureSubComponentFillPatternType>(c => c.RiserFillPatternType, Message.FixtureSubComponent_RiserFillPatternType);

        /// <summary>
        /// Gets/Sets the RiserFillPatternType value
        /// </summary>
        public FixtureSubComponentFillPatternType RiserFillPatternType
        {
            get { return GetProperty<FixtureSubComponentFillPatternType>(RiserFillPatternTypeProperty); }
            set { SetProperty<FixtureSubComponentFillPatternType>(RiserFillPatternTypeProperty, value); }
        }

        #endregion

        #region RiserColour

        /// <summary>
        /// RiserColour property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> RiserColourProperty =
            RegisterModelProperty<Int32>(c => c.RiserColour, Message.FixtureSubComponent_RiserColor,
            ModelPropertyDisplayType.Colour);

        /// <summary>
        /// Gets/Sets the RiserColour value
        /// </summary>
        public Int32 RiserColour
        {
            get { return GetProperty<Int32>(RiserColourProperty); }
            set { SetProperty<Int32>(RiserColourProperty, value); }
        }

        #endregion

        #region RiserTransparencyPercent

        /// <summary>
        /// RiserTransparencyPercent property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> RiserTransparencyPercentProperty =
            RegisterModelProperty<Int32>(c => c.RiserTransparencyPercent, Message.FixtureSubComponent_RiserTransparencyPercent);

        /// <summary>
        /// Gets/Sets the RiserTransparencyPercent value
        /// </summary>
        public Int32 RiserTransparencyPercent
        {
            get { return GetProperty<Int32>(RiserTransparencyPercentProperty); }
            set { SetProperty<Int32>(RiserTransparencyPercentProperty, value); }
        }

        #endregion

        #region NotchStartX

        /// <summary>
        /// NotchStartX property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> NotchStartXProperty =
            RegisterModelProperty<Single>(c => c.NotchStartX, Message.FixtureSubComponent_NotchStartX, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the NotchStartX value
        /// </summary>
        public Single NotchStartX
        {
            get { return GetProperty<Single>(NotchStartXProperty); }
            set { SetProperty<Single>(NotchStartXProperty, value); }
        }

        #endregion

        #region NotchSpacingX

        /// <summary>
        /// NotchSpacingX property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> NotchSpacingXProperty =
            RegisterModelProperty<Single>(c => c.NotchSpacingX, Message.FixtureSubComponent_NotchSpacingX, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the NotchSpacingX value
        /// </summary>
        public Single NotchSpacingX
        {
            get { return GetProperty<Single>(NotchSpacingXProperty); }
            set { SetProperty<Single>(NotchSpacingXProperty, value); }
        }

        #endregion

        #region NotchStartY

        /// <summary>
        /// NotchStartY property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> NotchStartYProperty =
            RegisterModelProperty<Single>(c => c.NotchStartY, Message.FixtureSubComponent_NotchStartY, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the NotchStartY value
        /// </summary>
        public Single NotchStartY
        {
            get { return GetProperty<Single>(NotchStartYProperty); }
            set { SetProperty<Single>(NotchStartYProperty, value); }
        }

        #endregion

        #region NotchSpacingY

        /// <summary>
        /// NotchSpacingY property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> NotchSpacingYProperty =
            RegisterModelProperty<Single>(c => c.NotchSpacingY, Message.FixtureSubComponent_NotchSpacingY, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the NotchSpacingY value
        /// </summary>
        public Single NotchSpacingY
        {
            get { return GetProperty<Single>(NotchSpacingYProperty); }
            set { SetProperty<Single>(NotchSpacingYProperty, value); }
        }

        #endregion

        #region NotchHeight

        /// <summary>
        /// NotchHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> NotchHeightProperty =
            RegisterModelProperty<Single>(c => c.NotchHeight, Message.FixtureSubComponent_NotchHeight, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the NotchHeight value
        /// </summary>
        public Single NotchHeight
        {
            get { return GetProperty<Single>(NotchHeightProperty); }
            set { SetProperty<Single>(NotchHeightProperty, value); }
        }

        #endregion

        #region NotchWidth

        /// <summary>
        /// NotchWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> NotchWidthProperty =
            RegisterModelProperty<Single>(c => c.NotchWidth, Message.FixtureSubComponent_NotchWidth, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the NotchWidth value
        /// </summary>
        public Single NotchWidth
        {
            get { return GetProperty<Single>(NotchWidthProperty); }
            set { SetProperty<Single>(NotchWidthProperty, value); }
        }

        #endregion

        #region IsNotchPlacedOnFront

        /// <summary>
        /// IsNotchPlacedOnFront property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsNotchPlacedOnFrontProperty =
            RegisterModelProperty<Boolean>(c => c.IsNotchPlacedOnFront, Message.FixtureSubComponent_IsNotchPlacedOnFront);

        /// <summary>
        /// Gets/Sets the IsNotchPlacedOnFront value
        /// </summary>
        public Boolean IsNotchPlacedOnFront
        {
            get { return GetProperty<Boolean>(IsNotchPlacedOnFrontProperty); }
            set { SetProperty<Boolean>(IsNotchPlacedOnFrontProperty, value); }
        }

        #endregion

        #region IsNotchPlacedOnBack

        /// <summary>
        /// IsNotchPlacedOnBack property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsNotchPlacedOnBackProperty =
            RegisterModelProperty<Boolean>(c => c.IsNotchPlacedOnBack, Message.FixtureSubComponent_IsNotchPlacedOnBack);

        /// <summary>
        /// Gets/Sets the IsNotchPlacedOnBack value
        /// </summary>
        public Boolean IsNotchPlacedOnBack
        {
            get { return GetProperty<Boolean>(IsNotchPlacedOnBackProperty); }
            set { SetProperty<Boolean>(IsNotchPlacedOnBackProperty, value); }
        }

        #endregion

        #region IsNotchPlacedOnLeft

        /// <summary>
        /// IsNotchPlacedOnLeft property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsNotchPlacedOnLeftProperty =
            RegisterModelProperty<Boolean>(c => c.IsNotchPlacedOnLeft, Message.FixtureSubComponent_IsNotchPlacedOnLeft);

        /// <summary>
        /// Gets/Sets the IsNotchPlacedOnLeft value
        /// </summary>
        public Boolean IsNotchPlacedOnLeft
        {
            get { return GetProperty<Boolean>(IsNotchPlacedOnLeftProperty); }
            set { SetProperty<Boolean>(IsNotchPlacedOnLeftProperty, value); }
        }

        #endregion

        #region IsNotchPlacedOnRight

        /// <summary>
        /// IsNotchPlacedOnRight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsNotchPlacedOnRightProperty =
            RegisterModelProperty<Boolean>(c => c.IsNotchPlacedOnRight, Message.FixtureSubComponent_IsNotchPlacedOnRight);

        /// <summary>
        /// Gets/Sets the IsNotchPlacedOnRight value
        /// </summary>
        public Boolean IsNotchPlacedOnRight
        {
            get { return GetProperty<Boolean>(IsNotchPlacedOnRightProperty); }
            set { SetProperty<Boolean>(IsNotchPlacedOnRightProperty, value); }
        }

        #endregion

        #region NotchStyleType

        /// <summary>
        /// NotchStyleType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureSubComponentNotchStyleType> NotchStyleTypeProperty =
            RegisterModelProperty<FixtureSubComponentNotchStyleType>(c => c.NotchStyleType, Message.FixtureSubComponent_NotchStyleType);

        /// <summary>
        /// Gets/Sets the NotchStyleType value
        /// </summary>
        public FixtureSubComponentNotchStyleType NotchStyleType
        {
            get { return GetProperty<FixtureSubComponentNotchStyleType>(NotchStyleTypeProperty); }
            set { SetProperty<FixtureSubComponentNotchStyleType>(NotchStyleTypeProperty, value); }
        }

        #endregion

        #region DividerObstructionHeight

        /// <summary>
        /// DividerObstructionHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DividerObstructionHeightProperty =
            RegisterModelProperty<Single>(c => c.DividerObstructionHeight, Message.FixtureSubComponent_DividerObstructionHeight, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the DividerObstructionHeight value
        /// </summary>
        public Single DividerObstructionHeight
        {
            get { return GetProperty<Single>(DividerObstructionHeightProperty); }
            set { SetProperty<Single>(DividerObstructionHeightProperty, value); }
        }

        #endregion

        #region DividerObstructionWidth

        /// <summary>
        /// DividerObstructionWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DividerObstructionWidthProperty =
            RegisterModelProperty<Single>(c => c.DividerObstructionWidth, Message.FixtureSubComponent_DividerObstructionWidth, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the DividerObstructionWidth value
        /// </summary>
        public Single DividerObstructionWidth
        {
            get { return GetProperty<Single>(DividerObstructionWidthProperty); }
            set { SetProperty<Single>(DividerObstructionWidthProperty, value); }
        }

        #endregion

        #region DividerObstructionDepth

        /// <summary>
        /// DividerObstructionDepth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DividerObstructionDepthProperty =
            RegisterModelProperty<Single>(c => c.DividerObstructionDepth, Message.FixtureSubComponent_DividerObstructionDepth, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the DividerObstructionDepth value
        /// </summary>
        public Single DividerObstructionDepth
        {
            get { return GetProperty<Single>(DividerObstructionDepthProperty); }
            set { SetProperty<Single>(DividerObstructionDepthProperty, value); }
        }

        #endregion

        #region DividerObstructionStartX

        /// <summary>
        /// DividerObstructionStartX property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DividerObstructionStartXProperty =
            RegisterModelProperty<Single>(c => c.DividerObstructionStartX, Message.FixtureSubComponent_DividerObstructionStartX, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the DividerObstructionStartX value
        /// </summary>
        public Single DividerObstructionStartX
        {
            get { return GetProperty<Single>(DividerObstructionStartXProperty); }
            set { SetProperty<Single>(DividerObstructionStartXProperty, value); }
        }

        #endregion

        #region DividerObstructionSpacingX

        /// <summary>
        /// DividerObstructionSpacingX property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DividerObstructionSpacingXProperty =
            RegisterModelProperty<Single>(c => c.DividerObstructionSpacingX, Message.FixtureSubComponent_DividerObstructionSpacingX, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the DividerObstructionSpacingX value
        /// </summary>
        public Single DividerObstructionSpacingX
        {
            get { return GetProperty<Single>(DividerObstructionSpacingXProperty); }
            set { SetProperty<Single>(DividerObstructionSpacingXProperty, value); }
        }

        #endregion

        #region DividerObstructionStartY

        /// <summary>
        /// DividerObstructionStartY property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DividerObstructionStartYProperty =
            RegisterModelProperty<Single>(c => c.DividerObstructionStartY, Message.FixtureSubComponent_DividerObstructionStartY, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the DividerObstructionStartY value
        /// </summary>
        public Single DividerObstructionStartY
        {
            get { return GetProperty<Single>(DividerObstructionStartYProperty); }
            set { SetProperty<Single>(DividerObstructionStartYProperty, value); }
        }

        #endregion

        #region DividerObstructionSpacingY

        /// <summary>
        /// DividerObstructionSpacingY property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DividerObstructionSpacingYProperty =
            RegisterModelProperty<Single>(c => c.DividerObstructionSpacingY, Message.FixtureSubComponent_DividerObstructionSpacingY, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the DividerObstructionSpacingY value
        /// </summary>
        public Single DividerObstructionSpacingY
        {
            get { return GetProperty<Single>(DividerObstructionSpacingYProperty); }
            set { SetProperty<Single>(DividerObstructionSpacingYProperty, value); }
        }

        #endregion

        #region DividerObstructionStartZ

        /// <summary>
        /// DividerObstructionStartZ property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DividerObstructionStartZProperty =
            RegisterModelProperty<Single>(c => c.DividerObstructionStartZ, Message.FixtureSubComponent_DividerObstructionStartZ, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the DividerObstructionStartZ value
        /// </summary>
        public Single DividerObstructionStartZ
        {
            get { return GetProperty<Single>(DividerObstructionStartZProperty); }
            set { SetProperty<Single>(DividerObstructionStartZProperty, value); }
        }

        #endregion

        #region DividerObstructionSpacingZ

        /// <summary>
        /// DividerObstructionSpacingZ property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DividerObstructionSpacingZProperty =
            RegisterModelProperty<Single>(c => c.DividerObstructionSpacingZ, Message.FixtureSubComponent_DividerObstructionSpacingZ, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the DividerObstructionSpacingZ value
        /// </summary>
        public Single DividerObstructionSpacingZ
        {
            get { return GetProperty<Single>(DividerObstructionSpacingZProperty); }
            set { SetProperty<Single>(DividerObstructionSpacingZProperty, value); }
        }

        #endregion

        #region IsDividerObstructionAtStart

        /// <summary>
        /// IsDividerObstructionAtStart property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsDividerObstructionAtStartProperty =
            RegisterModelProperty<Boolean>(c => c.IsDividerObstructionAtStart, Message.FixtureSubComponent_IsDividerObstructionAtStart);

        /// <summary>
        /// Gets/Sets the IsDividerObstructionAtStart value
        /// </summary>
        public Boolean IsDividerObstructionAtStart
        {
            get { return GetProperty<Boolean>(IsDividerObstructionAtStartProperty); }
            set { SetProperty<Boolean>(IsDividerObstructionAtStartProperty, value); }
        }

        #endregion

        #region IsDividerObstructionAtEnd

        /// <summary>
        /// IsDividerObstructionAtEnd property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsDividerObstructionAtEndProperty =
            RegisterModelProperty<Boolean>(c => c.IsDividerObstructionAtEnd, Message.FixtureSubComponent_IsDividerObstructionAtEnd);

        /// <summary>
        /// Gets/Sets the IsDividerObstructionAtEnd value
        /// </summary>
        public Boolean IsDividerObstructionAtEnd
        {
            get { return GetProperty<Boolean>(IsDividerObstructionAtEndProperty); }
            set { SetProperty<Boolean>(IsDividerObstructionAtEndProperty, value); }
        }

        #endregion

        #region IsDividerObstructionByFacing

        /// <summary>
        /// IsDividerObstructionByFacing property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsDividerObstructionByFacingProperty =
            RegisterModelProperty<Boolean>(c => c.IsDividerObstructionByFacing, Message.FixtureSubComponent_IsDividerObstructionByFacing);

        /// <summary>
        /// Gets/Sets the IsDividerObstructionByFacing value
        /// </summary>
        public Boolean IsDividerObstructionByFacing
        {
            get { return GetProperty<Boolean>(IsDividerObstructionByFacingProperty); }
            set { SetProperty<Boolean>(IsDividerObstructionByFacingProperty, value); }
        }

        #endregion

        #region DividerObstructionFillColour
        public static readonly ModelPropertyInfo<Int32> DividerObstructionFillColourProperty =
            RegisterModelProperty<Int32>(c => c.DividerObstructionFillColour, PlanogramMessage.PlanogramSubComponent_DividerObstructionFillColour,
                ModelPropertyDisplayType.Colour);
        /// <summary>
        /// The fill colour that should be used to render dividers.
        /// </summary>
        public Int32 DividerObstructionFillColour
        {
            get { return GetProperty<Int32>(DividerObstructionFillColourProperty); }
            set { SetProperty<Int32>(DividerObstructionFillColourProperty, value); }
        }
        #endregion

        #region DividerObstructionFillPattern
        public static readonly ModelPropertyInfo<FixtureSubComponentFillPatternType> DividerObstructionFillPatternProperty =
            RegisterModelProperty<FixtureSubComponentFillPatternType>(c => c.DividerObstructionFillPattern, PlanogramMessage.PlanogramSubComponent_DividerObstructionFillPattern);
        /// <summary>
        /// The fill pattern that should be used to render dividers.
        /// </summary>
        public FixtureSubComponentFillPatternType DividerObstructionFillPattern
        {
            get { return GetProperty<FixtureSubComponentFillPatternType>(DividerObstructionFillPatternProperty); }
            set { SetProperty<FixtureSubComponentFillPatternType>(DividerObstructionFillPatternProperty, value); }
        }
        #endregion

        #region MerchConstraintRow1StartX

        /// <summary>
        /// MerchConstraintRow1StartX property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow1StartXProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow1StartX, Message.FixtureSubComponent_MerchConstraintRow1StartX, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the MerchConstraintRow1StartX value
        /// </summary>
        public Single MerchConstraintRow1StartX
        {
            get { return GetProperty<Single>(MerchConstraintRow1StartXProperty); }
            set { SetProperty<Single>(MerchConstraintRow1StartXProperty, value); }
        }

        #endregion

        #region MerchConstraintRow1SpacingX

        /// <summary>
        /// MerchConstraintRow1SpacingX property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow1SpacingXProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow1SpacingX, Message.FixtureSubComponent_MerchConstraintRow1SpacingX, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the MerchConstraintRow1SpacingX value
        /// </summary>
        public Single MerchConstraintRow1SpacingX
        {
            get { return GetProperty<Single>(MerchConstraintRow1SpacingXProperty); }
            set { SetProperty<Single>(MerchConstraintRow1SpacingXProperty, value); }
        }

        #endregion

        #region MerchConstraintRow1StartY

        /// <summary>
        /// MerchConstraintRow1StartY property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow1StartYProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow1StartY, Message.FixtureSubComponent_MerchConstraintRow1StartY, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the MerchConstraintRow1StartY value
        /// </summary>
        public Single MerchConstraintRow1StartY
        {
            get { return GetProperty<Single>(MerchConstraintRow1StartYProperty); }
            set { SetProperty<Single>(MerchConstraintRow1StartYProperty, value); }
        }

        #endregion

        #region MerchConstraintRow1SpacingY

        /// <summary>
        /// MerchConstraintRow1SpacingY property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow1SpacingYProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow1SpacingY, Message.FixtureSubComponent_MerchConstraintRow1SpacingY, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the MerchConstraintRow1SpacingY value
        /// </summary>
        public Single MerchConstraintRow1SpacingY
        {
            get { return GetProperty<Single>(MerchConstraintRow1SpacingYProperty); }
            set { SetProperty<Single>(MerchConstraintRow1SpacingYProperty, value); }
        }

        #endregion

        #region MerchConstraintRow1Height

        /// <summary>
        /// MerchConstraintRow1Height property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow1HeightProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow1Height, Message.FixtureSubComponent_MerchConstraintRow1Height, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the MerchConstraintRow1Height value
        /// </summary>
        public Single MerchConstraintRow1Height
        {
            get { return GetProperty<Single>(MerchConstraintRow1HeightProperty); }
            set { SetProperty<Single>(MerchConstraintRow1HeightProperty, value); }
        }

        #endregion

        #region MerchConstraintRow1Width

        /// <summary>
        /// MerchConstraintRow1Width property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow1WidthProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow1Width, Message.FixtureSubComponent_MerchConstraintRow1Width, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the MerchConstraintRow1Width value
        /// </summary>
        public Single MerchConstraintRow1Width
        {
            get { return GetProperty<Single>(MerchConstraintRow1WidthProperty); }
            set { SetProperty<Single>(MerchConstraintRow1WidthProperty, value); }
        }

        #endregion

        #region MerchConstraintRow2StartX

        /// <summary>
        /// MerchConstraintRow2StartX property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow2StartXProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow2StartX, Message.FixtureSubComponent_MerchConstraintRow2StartX, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the MerchConstraintRow2StartX value
        /// </summary>
        public Single MerchConstraintRow2StartX
        {
            get { return GetProperty<Single>(MerchConstraintRow2StartXProperty); }
            set { SetProperty<Single>(MerchConstraintRow2StartXProperty, value); }
        }

        #endregion

        #region MerchConstraintRow2SpacingX

        /// <summary>
        /// MerchConstraintRow2SpacingX property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow2SpacingXProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow2SpacingX, Message.FixtureSubComponent_MerchConstraintRow2SpacingX, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the MerchConstraintRow2SpacingX value
        /// </summary>
        public Single MerchConstraintRow2SpacingX
        {
            get { return GetProperty<Single>(MerchConstraintRow2SpacingXProperty); }
            set { SetProperty<Single>(MerchConstraintRow2SpacingXProperty, value); }
        }

        #endregion

        #region MerchConstraintRow2StartY

        /// <summary>
        /// MerchConstraintRow2StartY property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow2StartYProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow2StartY, Message.FixtureSubComponent_MerchConstraintRow2StartY, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the MerchConstraintRow2StartY value
        /// </summary>
        public Single MerchConstraintRow2StartY
        {
            get { return GetProperty<Single>(MerchConstraintRow2StartYProperty); }
            set { SetProperty<Single>(MerchConstraintRow2StartYProperty, value); }
        }

        #endregion

        #region MerchConstraintRow2SpacingY

        /// <summary>
        /// MerchConstraintRow2SpacingY property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow2SpacingYProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow2SpacingY, Message.FixtureSubComponent_MerchConstraintRow2SpacingY, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the MerchConstraintRow2SpacingY value
        /// </summary>
        public Single MerchConstraintRow2SpacingY
        {
            get { return GetProperty<Single>(MerchConstraintRow2SpacingYProperty); }
            set { SetProperty<Single>(MerchConstraintRow2SpacingYProperty, value); }
        }

        #endregion

        #region MerchConstraintRow2Height

        /// <summary>
        /// MerchConstraintRow2Height property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow2HeightProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow2Height, Message.FixtureSubComponent_MerchConstraintRow2Height, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the MerchConstraintRow2Height value
        /// </summary>
        public Single MerchConstraintRow2Height
        {
            get { return GetProperty<Single>(MerchConstraintRow2HeightProperty); }
            set { SetProperty<Single>(MerchConstraintRow2HeightProperty, value); }
        }

        #endregion

        #region MerchConstraintRow2Width

        /// <summary>
        /// MerchConstraintRow2Width property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MerchConstraintRow2WidthProperty =
            RegisterModelProperty<Single>(c => c.MerchConstraintRow2Width, Message.FixtureSubComponent_MerchConstraintRow2Width, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the MerchConstraintRow2Width value
        /// </summary>
        public Single MerchConstraintRow2Width
        {
            get { return GetProperty<Single>(MerchConstraintRow2WidthProperty); }
            set { SetProperty<Single>(MerchConstraintRow2WidthProperty, value); }
        }

        #endregion

        #region LineThickness

        /// <summary>
        /// LineThickness property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> LineThicknessProperty =
            RegisterModelProperty<Single>(c => c.LineThickness, Message.FixtureSubComponent_LineThickness);

        /// <summary>
        /// Gets/Sets the LineThickness value
        /// </summary>
        public Single LineThickness
        {
            get { return GetProperty<Single>(LineThicknessProperty); }
            set { SetProperty<Single>(LineThicknessProperty, value); }
        }

        #endregion

        #region MerchandisingType

        /// <summary>
        /// MerchandisingType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureSubComponentMerchandisingType> MerchandisingTypeProperty =
            RegisterModelProperty<FixtureSubComponentMerchandisingType>(c => c.MerchandisingType, Message.FixtureSubComponent_MerchandisingType);

        /// <summary>
        /// Gets/Sets the MerchandisingType value
        /// </summary>
        public FixtureSubComponentMerchandisingType MerchandisingType
        {
            get { return GetProperty<FixtureSubComponentMerchandisingType>(MerchandisingTypeProperty); }
            set { SetProperty<FixtureSubComponentMerchandisingType>(MerchandisingTypeProperty, value); }
        }

        #endregion

        #region CombineType

        /// <summary>
        /// CombineType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureSubComponentCombineType> CombineTypeProperty =
            RegisterModelProperty<FixtureSubComponentCombineType>(c => c.CombineType, Message.FixtureSubComponent_CombineType);

        /// <summary>
        /// Gets/Sets the CombineType value
        /// </summary>
        public FixtureSubComponentCombineType CombineType
        {
            get { return GetProperty<FixtureSubComponentCombineType>(CombineTypeProperty); }
            set { SetProperty<FixtureSubComponentCombineType>(CombineTypeProperty, value); }
        }

        #endregion

        #region IsProductOverlapAllowed

        /// <summary>
        /// IsProductOverlapAllowed property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsProductOverlapAllowedProperty =
            RegisterModelProperty<Boolean>(c => c.IsProductOverlapAllowed, Message.FixtureSubComponent_IsProductOverlapAllowed);

        /// <summary>
        /// Gets/Sets the IsProductOverlapAllowed value
        /// </summary>
        public Boolean IsProductOverlapAllowed
        {
            get { return GetProperty<Boolean>(IsProductOverlapAllowedProperty); }
            set { SetProperty<Boolean>(IsProductOverlapAllowedProperty, value); }
        }

        #endregion

        #region IsProductSqueezeAllowed

        /// <summary>
        /// IsProductSqueezeAllowed property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsProductSqueezeAllowedProperty =
            RegisterModelProperty<Boolean>(c => c.IsProductSqueezeAllowed, Message.FixtureSubComponent_IsProductSqueezeAllowed);

        /// <summary>
        /// Gets/Sets the IsProductSqueezeAllowed value
        /// </summary>
        public Boolean IsProductSqueezeAllowed
        {
            get { return GetProperty<Boolean>(IsProductSqueezeAllowedProperty); }
            set { SetProperty<Boolean>(IsProductSqueezeAllowedProperty, value); }
        }

        #endregion

        #region MerchandisingStrategyX

        /// <summary>
        /// MerchandisingStrategyX property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureSubComponentXMerchStrategyType> MerchandisingStrategyXProperty =
            RegisterModelProperty<FixtureSubComponentXMerchStrategyType>(c => c.MerchandisingStrategyX, Message.FixtureSubComponent_MerchandisingStrategyX);

        /// <summary>
        /// Gets/Sets the MerchandisingStrategyX value
        /// </summary>
        public FixtureSubComponentXMerchStrategyType MerchandisingStrategyX
        {
            get { return GetProperty<FixtureSubComponentXMerchStrategyType>(MerchandisingStrategyXProperty); }
            set { SetProperty<FixtureSubComponentXMerchStrategyType>(MerchandisingStrategyXProperty, value); }
        }

        #endregion

        #region MerchandisingStrategyY

        /// <summary>
        /// MerchandisingStrategyY property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureSubComponentYMerchStrategyType> MerchandisingStrategyYProperty =
            RegisterModelProperty<FixtureSubComponentYMerchStrategyType>(c => c.MerchandisingStrategyY, Message.FixtureSubComponent_MerchandisingStrategyY);

        /// <summary>
        /// Gets/Sets the MerchandisingStrategyY value
        /// </summary>
        public FixtureSubComponentYMerchStrategyType MerchandisingStrategyY
        {
            get { return GetProperty<FixtureSubComponentYMerchStrategyType>(MerchandisingStrategyYProperty); }
            set { SetProperty<FixtureSubComponentYMerchStrategyType>(MerchandisingStrategyYProperty, value); }
        }

        #endregion

        #region MerchandisingStrategyZ

        /// <summary>
        /// MerchandisingStrategyZ property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FixtureSubComponentZMerchStrategyType> MerchandisingStrategyZProperty =
            RegisterModelProperty<FixtureSubComponentZMerchStrategyType>(c => c.MerchandisingStrategyZ, Message.FixtureSubComponent_MerchandisingStrategyZ);

        /// <summary>
        /// Gets/Sets the MerchandisingStrategyZ value
        /// </summary>
        public FixtureSubComponentZMerchStrategyType MerchandisingStrategyZ
        {
            get { return GetProperty<FixtureSubComponentZMerchStrategyType>(MerchandisingStrategyZProperty); }
            set { SetProperty<FixtureSubComponentZMerchStrategyType>(MerchandisingStrategyZProperty, value); }
        }

        #endregion

        #region LeftOverhang

        /// <summary>
        /// LeftOverhang property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> LeftOverhangProperty =
            RegisterModelProperty<Single>(c => c.LeftOverhang, Message.FixtureSubComponent_LeftOverhang, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the LeftOverhang value
        /// </summary>
        public Single LeftOverhang
        {
            get { return GetProperty<Single>(LeftOverhangProperty); }
            set { SetProperty<Single>(LeftOverhangProperty, value); }
        }

        #endregion

        #region RightOverhang

        /// <summary>
        /// RightOverhang property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> RightOverhangProperty =
            RegisterModelProperty<Single>(c => c.RightOverhang, Message.FixtureSubComponent_RightOverhang, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the RightOverhang value
        /// </summary>
        public Single RightOverhang
        {
            get { return GetProperty<Single>(RightOverhangProperty); }
            set { SetProperty<Single>(RightOverhangProperty, value); }
        }

        #endregion

        #region FrontOverhang

        /// <summary>
        /// FrontOverhang property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FrontOverhangProperty =
            RegisterModelProperty<Single>(c => c.FrontOverhang, Message.FixtureSubComponent_FrontOverhang, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the FrontOverhang value
        /// </summary>
        public Single FrontOverhang
        {
            get { return GetProperty<Single>(FrontOverhangProperty); }
            set { SetProperty<Single>(FrontOverhangProperty, value); }
        }

        #endregion

        #region BackOverhang

        /// <summary>
        /// BackOverhang property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BackOverhangProperty =
            RegisterModelProperty<Single>(c => c.BackOverhang, Message.FixtureSubComponent_BackOverhang, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the BackOverhang value
        /// </summary>
        public Single BackOverhang
        {
            get { return GetProperty<Single>(BackOverhangProperty); }
            set { SetProperty<Single>(BackOverhangProperty, value); }
        }

        #endregion

        #region TopOverhang

        /// <summary>
        /// TopOverhang property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> TopOverhangProperty =
            RegisterModelProperty<Single>(c => c.TopOverhang, Message.FixtureSubComponent_TopOverhang, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the TopOverhang value
        /// </summary>
        public Single TopOverhang
        {
            get { return GetProperty<Single>(TopOverhangProperty); }
            set { SetProperty<Single>(TopOverhangProperty, value); }
        }

        #endregion

        #region BottomOverhang

        /// <summary>
        /// BottomOverhang property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single> BottomOverhangProperty =
            RegisterModelProperty<Single>(c => c.BottomOverhang, Message.FixtureSubComponent_BottomOverhang, ModelPropertyDisplayType.Length);

        /// <summary>
        /// Gets/Sets the BottomOverhang value
        /// </summary>
        public Single BottomOverhang
        {
            get { return GetProperty<Single>(BottomOverhangProperty); }
            set { SetProperty<Single>(BottomOverhangProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));

        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static FixtureSubComponent NewFixtureSubComponent()
        {
            FixtureSubComponent item = new FixtureSubComponent();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static FixtureSubComponent NewFixtureSubComponent(
            String name, Single height, Single width, Single depth, Int32 fillColour)
        {
            FixtureSubComponent item = new FixtureSubComponent();
            item.Create(name, height, width, depth, fillColour);
            return item;
        }

        /// <summary>
        /// Creates a new fixture subcomponent from an existing planogram subcomponent.
        /// </summary>
        /// <param name="planSub"></param>
        /// <returns></returns>
        public static FixtureSubComponent NewFixtureSubComponent(PlanogramSubComponent planSub, FixturePackage parentPackage)
        {
            FixtureSubComponent item = new FixtureSubComponent();
            item.Create(planSub, parentPackage);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(String name, Single height, Single width, Single depth, Int32 fillColour)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(NameProperty, name);
            this.LoadProperty<Single>(HeightProperty, height);
            this.LoadProperty<Single>(WidthProperty, width);
            this.LoadProperty<Single>(DepthProperty, depth);
            this.LoadProperty<Int32>(FillColourFrontProperty, fillColour);
            this.LoadProperty<Int32>(FillColourBackProperty, fillColour);
            this.LoadProperty<Int32>(FillColourTopProperty, fillColour);
            this.LoadProperty<Int32>(FillColourBottomProperty, fillColour);
            this.LoadProperty<Int32>(FillColourLeftProperty, fillColour);
            this.LoadProperty<Int32>(FillColourRightProperty, fillColour);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// from a planogram subcomponent
        /// </summary>
        private void Create(PlanogramSubComponent planogramSub, FixturePackage parentPackage)
        {
            LoadProperty<Int32>(IdProperty, (Int32)planogramSub.Id);
            LoadProperty<String>(NameProperty, planogramSub.Name);

            LoadProperty<Single>(XProperty, planogramSub.X);
            LoadProperty<Single>(YProperty, planogramSub.Y);
            LoadProperty<Single>(ZProperty, planogramSub.Z);

            LoadProperty<Single>(HeightProperty, planogramSub.Height);
            LoadProperty<Single>(WidthProperty, planogramSub.Width);
            LoadProperty<Single>(DepthProperty, planogramSub.Depth);

            LoadProperty<Single>(AngleProperty, planogramSub.Angle);
            LoadProperty<Single>(SlopeProperty, planogramSub.Slope);
            LoadProperty<Single>(RollProperty, planogramSub.Roll);

            LoadProperty<Single>(DividerObstructionHeightProperty, planogramSub.DividerObstructionHeight);
            LoadProperty<Single>(DividerObstructionDepthProperty, planogramSub.DividerObstructionDepth);
            LoadProperty<Single>(DividerObstructionSpacingXProperty, planogramSub.DividerObstructionSpacingX);
            LoadProperty<Single>(DividerObstructionSpacingYProperty, planogramSub.DividerObstructionSpacingY);
            LoadProperty<Single>(DividerObstructionSpacingZProperty, planogramSub.DividerObstructionSpacingZ);
            LoadProperty<Single>(DividerObstructionStartXProperty, planogramSub.DividerObstructionStartX);
            LoadProperty<Single>(DividerObstructionStartYProperty, planogramSub.DividerObstructionStartY);
            LoadProperty<Single>(DividerObstructionStartZProperty, planogramSub.DividerObstructionStartZ);
            LoadProperty<Single>(DividerObstructionWidthProperty, planogramSub.DividerObstructionWidth);
            LoadProperty<Boolean>(IsDividerObstructionAtStartProperty, planogramSub.IsDividerObstructionAtStart);
            LoadProperty<Boolean>(IsDividerObstructionAtEndProperty, planogramSub.IsDividerObstructionAtEnd);
            LoadProperty<Boolean>(IsDividerObstructionByFacingProperty, planogramSub.IsDividerObstructionByFacing);
            LoadProperty<Int32>(DividerObstructionFillColourProperty, planogramSub.DividerObstructionFillColour);
            LoadProperty<FixtureSubComponentFillPatternType>(DividerObstructionFillPatternProperty, FixtureSubComponentFillPatternTypeHelper.Parse(planogramSub.DividerObstructionFillPattern.ToString()));

            LoadProperty<Single>(FaceThicknessFrontProperty, planogramSub.FaceThicknessFront);
            LoadProperty<Single>(FaceThicknessBackProperty, planogramSub.FaceThicknessBack);
            LoadProperty<Single>(FaceThicknessTopProperty, planogramSub.FaceThicknessTop);
            LoadProperty<Single>(FaceThicknessBottomProperty, planogramSub.FaceThicknessBottom);
            LoadProperty<Single>(FaceThicknessLeftProperty, planogramSub.FaceThicknessLeft);
            LoadProperty<Single>(FaceThicknessRightProperty, planogramSub.FaceThicknessRight);

            LoadProperty<Int32>(FillColourBackProperty, planogramSub.FillColourBack);
            LoadProperty<Int32>(FillColourBottomProperty, planogramSub.FillColourBottom);
            LoadProperty<Int32>(FillColourFrontProperty, planogramSub.FillColourFront);
            LoadProperty<Int32>(FillColourLeftProperty, planogramSub.FillColourLeft);
            LoadProperty<Int32>(FillColourRightProperty, planogramSub.FillColourRight);
            LoadProperty<Int32>(FillColourTopProperty, planogramSub.FillColourTop);
            LoadProperty<Int32>(LineColourProperty, planogramSub.LineColour);
            LoadProperty<Single>(LineThicknessProperty, planogramSub.LineThickness);
            LoadProperty<FixtureSubComponentFillPatternType>(FillPatternTypeBackProperty, FixtureSubComponentFillPatternTypeHelper.Parse(planogramSub.FillPatternTypeBack.ToString()));
            LoadProperty<FixtureSubComponentFillPatternType>(FillPatternTypeBottomProperty, FixtureSubComponentFillPatternTypeHelper.Parse(planogramSub.FillPatternTypeBottom.ToString()));
            LoadProperty<FixtureSubComponentFillPatternType>(FillPatternTypeFrontProperty, FixtureSubComponentFillPatternTypeHelper.Parse(planogramSub.FillPatternTypeFront.ToString()));
            LoadProperty<FixtureSubComponentFillPatternType>(FillPatternTypeLeftProperty, FixtureSubComponentFillPatternTypeHelper.Parse(planogramSub.FillPatternTypeLeft.ToString()));
            LoadProperty<FixtureSubComponentFillPatternType>(FillPatternTypeRightProperty, FixtureSubComponentFillPatternTypeHelper.Parse(planogramSub.FillPatternTypeRight.ToString()));
            LoadProperty<FixtureSubComponentFillPatternType>(FillPatternTypeTopProperty, FixtureSubComponentFillPatternTypeHelper.Parse(planogramSub.FillPatternTypeTop.ToString()));
            LoadProperty<Int32>(TransparencyPercentBackProperty, planogramSub.TransparencyPercentBack);
            LoadProperty<Int32>(TransparencyPercentBottomProperty, planogramSub.TransparencyPercentBottom);
            LoadProperty<Int32>(TransparencyPercentBottomProperty, planogramSub.TransparencyPercentBottom);
            LoadProperty<Int32>(TransparencyPercentFrontProperty, planogramSub.TransparencyPercentFront);
            LoadProperty<Int32>(TransparencyPercentLeftProperty, planogramSub.TransparencyPercentLeft);
            LoadProperty<Int32>(TransparencyPercentRightProperty, planogramSub.TransparencyPercentRight);
            LoadProperty<Int32>(TransparencyPercentTopProperty, planogramSub.TransparencyPercentTop);

            LoadProperty<Boolean>(HasCollisionDetectionProperty, planogramSub.HasCollisionDetection);
            LoadProperty<Boolean>(IsVisibleProperty, planogramSub.IsVisible);
            LoadProperty<FixtureSubComponentShapeType>(ShapeTypeProperty, FixtureSubComponentShapeTypeHelper.Parse(planogramSub.ShapeType.ToString()));

            LoadProperty<Boolean>(IsNotchPlacedOnBackProperty, planogramSub.IsNotchPlacedOnBack);
            LoadProperty<Boolean>(IsNotchPlacedOnFrontProperty, planogramSub.IsNotchPlacedOnFront);
            LoadProperty<Boolean>(IsNotchPlacedOnLeftProperty, planogramSub.IsNotchPlacedOnLeft);
            LoadProperty<Boolean>(IsNotchPlacedOnRightProperty, planogramSub.IsNotchPlacedOnRight);
            LoadProperty<Single>(NotchHeightProperty, planogramSub.NotchHeight);
            LoadProperty<Single>(NotchSpacingXProperty, planogramSub.NotchSpacingX);
            LoadProperty<Single>(NotchSpacingYProperty, planogramSub.NotchSpacingY);
            LoadProperty<Single>(NotchStartXProperty, planogramSub.NotchStartX);
            LoadProperty<Single>(NotchStartYProperty, planogramSub.NotchStartY);
            LoadProperty<FixtureSubComponentNotchStyleType>(NotchStyleTypeProperty, FixtureSubComponentNotchStyleTypeHelper.Parse(planogramSub.NotchStyleType.ToString()));
            LoadProperty<Single>(NotchWidthProperty, planogramSub.NotchWidth);

            LoadProperty<Boolean>(IsRiserPlacedOnBackProperty, planogramSub.IsRiserPlacedOnBack);
            LoadProperty<Boolean>(IsRiserPlacedOnFrontProperty, planogramSub.IsRiserPlacedOnFront);
            LoadProperty<Boolean>(IsRiserPlacedOnLeftProperty, planogramSub.IsRiserPlacedOnLeft);
            LoadProperty<Boolean>(IsRiserPlacedOnRightProperty, planogramSub.IsRiserPlacedOnRight);
            LoadProperty<Int32>(RiserColourProperty, planogramSub.RiserColour);
            LoadProperty<FixtureSubComponentFillPatternType>(RiserFillPatternTypeProperty,FixtureSubComponentFillPatternTypeHelper.Parse(planogramSub.RiserFillPatternType.ToString()));
            LoadProperty<Single>(RiserHeightProperty, planogramSub.RiserHeight);
            LoadProperty<Single>(RiserThicknessProperty, planogramSub.RiserThickness);
            LoadProperty<Int32>(RiserTransparencyPercentProperty, planogramSub.RiserTransparencyPercent);

            LoadProperty<Single>(MerchandisableHeightProperty, planogramSub.MerchandisableHeight);
            LoadProperty<Single>(MerchandisableDepthProperty, planogramSub.MerchandisableDepth);
            LoadProperty<FixtureSubComponentMerchandisingType>(MerchandisingTypeProperty, FixtureSubComponentMerchandisingTypeHelper.Parse(planogramSub.MerchandisingType.ToString()));
            LoadProperty<FixtureSubComponentCombineType>(CombineTypeProperty, FixtureSubComponentCombineTypeHelper.Parse(planogramSub.CombineType.ToString()));
            LoadProperty<FixtureSubComponentXMerchStrategyType>(MerchandisingStrategyXProperty, FixtureSubComponentXMerchStrategyTypeHelper.Parse(planogramSub.MerchandisingStrategyX.ToString()));
            LoadProperty<FixtureSubComponentYMerchStrategyType>(MerchandisingStrategyYProperty, FixtureSubComponentYMerchStrategyTypeHelper.Parse(planogramSub.MerchandisingStrategyY.ToString()));
            LoadProperty<FixtureSubComponentZMerchStrategyType>(MerchandisingStrategyZProperty, FixtureSubComponentZMerchStrategyTypeHelper.Parse(planogramSub.MerchandisingStrategyZ.ToString()));
            LoadProperty<Boolean>(IsProductOverlapAllowedProperty, planogramSub.IsProductOverlapAllowed);
            LoadProperty<Boolean>(IsProductSqueezeAllowedProperty, planogramSub.IsProductSqueezeAllowed);

            LoadProperty<Single>(LeftOverhangProperty, planogramSub.LeftOverhang);
            LoadProperty<Single>(RightOverhangProperty, planogramSub.RightOverhang);
            LoadProperty<Single>(FrontOverhangProperty, planogramSub.FrontOverhang);
            LoadProperty<Single>(BackOverhangProperty, planogramSub.BackOverhang);
            LoadProperty<Single>(TopOverhangProperty, planogramSub.TopOverhang);
            LoadProperty<Single>(BottomOverhangProperty, planogramSub.BottomOverhang);

            LoadProperty<Single>(MerchConstraintRow1SpacingXProperty, planogramSub.MerchConstraintRow1SpacingX);
            LoadProperty<Single>(MerchConstraintRow1SpacingYProperty, planogramSub.MerchConstraintRow1SpacingY);
            LoadProperty<Single>(MerchConstraintRow1StartXProperty, planogramSub.MerchConstraintRow1StartX);
            LoadProperty<Single>(MerchConstraintRow1StartYProperty, planogramSub.MerchConstraintRow1StartY);
            LoadProperty<Single>(MerchConstraintRow1HeightProperty, planogramSub.MerchConstraintRow1Height);
            LoadProperty<Single>(MerchConstraintRow1WidthProperty, planogramSub.MerchConstraintRow1Width);
            LoadProperty<Single>(MerchConstraintRow2SpacingXProperty, planogramSub.MerchConstraintRow2SpacingX);
            LoadProperty<Single>(MerchConstraintRow2SpacingYProperty, planogramSub.MerchConstraintRow2SpacingY);
            LoadProperty<Single>(MerchConstraintRow2StartXProperty, planogramSub.MerchConstraintRow2StartX);
            LoadProperty<Single>(MerchConstraintRow2StartYProperty, planogramSub.MerchConstraintRow2StartY);
            LoadProperty<Single>(MerchConstraintRow2HeightProperty, planogramSub.MerchConstraintRow2Height);
            LoadProperty<Single>(MerchConstraintRow2WidthProperty, planogramSub.MerchConstraintRow2Width);

            //Images
            if (parentPackage != null)
            {
                parentPackage.AddImages(
                    new Int32?[]
                    {
                        (Int32?)planogramSub.ImageIdFront, 
                        (Int32?)planogramSub.ImageIdBack,
                        (Int32?)planogramSub.ImageIdTop, 
                        (Int32?)planogramSub.ImageIdBottom, 
                        (Int32?)planogramSub.ImageIdLeft,
                        (Int32?) planogramSub.ImageIdRight

                    }, planogramSub.Parent.Parent);
            }
            LoadProperty<Int32?>(ImageIdFrontProperty, planogramSub.ImageIdFront as Int32?);
            LoadProperty<Int32?>(ImageIdBackProperty, planogramSub.ImageIdBack as Int32?);
            LoadProperty<Int32?>(ImageIdTopProperty, planogramSub.ImageIdTop as Int32?);
            LoadProperty<Int32?>(ImageIdBottomProperty, planogramSub.ImageIdBottom as Int32?);
            LoadProperty<Int32?>(ImageIdLeftProperty, planogramSub.ImageIdLeft as Int32?);
            LoadProperty<Int32?>(ImageIdRightProperty, planogramSub.ImageIdRight as Int32?);


            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<FixtureSubComponent>(oldId, newId);
        }

        /// <summary>
        /// Called when a copy operation completes on this instance
        /// </summary>
        protected override void OnCopyComplete(CopyContext context)
        {
            this.ResolveIds(context);
        }

        /// <summary>
        /// Called when resolving ids on this context
        /// </summary>
        private void ResolveIds(IResolveContext context)
        {
            // ImageIdFront
            Object imageIdFront = context.ResolveId<FixtureImage>(this.ReadProperty<Int32?>(ImageIdFrontProperty));
            if (imageIdFront != null) this.LoadProperty<Int32?>(ImageIdFrontProperty, (Int32?)imageIdFront);

            // ImageIdBack
            Object imageIdBack = context.ResolveId<FixtureImage>(this.ReadProperty<Int32?>(ImageIdBackProperty));
            if (imageIdBack != null) this.LoadProperty<Int32?>(ImageIdBackProperty, (Int32?)imageIdBack);

            // ImageIdTop
            Object imageIdTop = context.ResolveId<FixtureImage>(this.ReadProperty<Int32?>(ImageIdTopProperty));
            if (imageIdTop != null) this.LoadProperty<Int32?>(ImageIdTopProperty, (Int32?)imageIdTop);

            // ImageIdBottom
            Object imageIdBottom = context.ResolveId<FixtureImage>(this.ReadProperty<Int32?>(ImageIdBottomProperty));
            if (imageIdBottom != null) this.LoadProperty<Int32?>(ImageIdBottomProperty, (Int32?)imageIdBottom);

            // ImageIdLeft
            Object imageIdLeft = context.ResolveId<FixtureImage>(this.ReadProperty<Int32?>(ImageIdLeftProperty));
            if (imageIdLeft != null) this.LoadProperty<Int32?>(ImageIdLeftProperty, (Int32?)imageIdLeft);

            // ImageIdRight
            Object imageIdRight = context.ResolveId<FixtureImage>(this.ReadProperty<Int32?>(ImageIdRightProperty));
            if (imageIdRight != null) this.LoadProperty<Int32?>(ImageIdRightProperty, (Int32?)imageIdRight);
        }

        /// <summary>
        /// Copies the values of this fixture component into the given planogram component.
        /// </summary>
        /// <param name="parentComponent"></param>
        /// <returns></returns>
        public PlanogramSubComponent AddOrUpdatePlanogramSubComponent(PlanogramComponent parentComponent)
        {
            PlanogramSubComponent item = parentComponent.SubComponents.FindById(this.Id);
            if (item == null)
            {
                item = PlanogramSubComponent.NewPlanogramSubComponent();
                item.Id = this.Id;
                parentComponent.SubComponents.Add(item);
            }

            item.Angle = this.Angle;
            item.Depth = this.Depth;
            item.DividerObstructionHeight = this.DividerObstructionHeight;
            item.DividerObstructionDepth = this.DividerObstructionDepth;
            item.DividerObstructionSpacingX = this.DividerObstructionSpacingX;
            item.DividerObstructionSpacingY = this.DividerObstructionSpacingY;
            item.DividerObstructionSpacingZ = this.DividerObstructionSpacingZ;
            item.DividerObstructionStartX = this.DividerObstructionStartX;
            item.DividerObstructionStartY = this.DividerObstructionStartY;
            item.DividerObstructionStartZ = this.DividerObstructionStartZ;
            item.DividerObstructionWidth = this.DividerObstructionWidth;
            item.IsDividerObstructionAtEnd = this.IsDividerObstructionAtEnd;
            item.IsDividerObstructionAtStart = this.IsDividerObstructionAtStart;
            item.IsDividerObstructionByFacing = this.IsDividerObstructionByFacing;
            item.DividerObstructionFillColour = this.DividerObstructionFillColour;
            item.DividerObstructionFillPattern = (PlanogramSubComponentFillPatternType)this.DividerObstructionFillPattern;
            item.FaceThicknessFront = this.FaceThicknessFront;
            item.FaceThicknessBack = this.FaceThicknessBack;
            item.FaceThicknessTop = this.FaceThicknessTop;
            item.FaceThicknessBottom = this.FaceThicknessBottom;
            item.FaceThicknessLeft = this.FaceThicknessLeft;
            item.FaceThicknessRight = this.FaceThicknessRight;
            item.FillColourBack = this.FillColourBack;
            item.FillColourBottom = this.FillColourBottom;
            item.FillColourFront = this.FillColourFront;
            item.FillColourLeft = this.FillColourLeft;
            item.FillColourRight = this.FillColourRight;
            item.FillColourTop = this.FillColourTop;
            item.FillPatternTypeBack = (PlanogramSubComponentFillPatternType)this.FillPatternTypeBack;
            item.FillPatternTypeBottom = (PlanogramSubComponentFillPatternType)this.FillPatternTypeBottom;
            item.FillPatternTypeFront = (PlanogramSubComponentFillPatternType)this.FillPatternTypeFront;
            item.FillPatternTypeLeft = (PlanogramSubComponentFillPatternType)this.FillPatternTypeLeft;
            item.FillPatternTypeRight = (PlanogramSubComponentFillPatternType)this.FillPatternTypeRight;
            item.FillPatternTypeTop = (PlanogramSubComponentFillPatternType)this.FillPatternTypeTop;
            item.HasCollisionDetection = this.HasCollisionDetection;
            item.Height = this.Height;
            item.IsNotchPlacedOnBack = this.IsNotchPlacedOnBack;
            item.IsNotchPlacedOnFront = this.IsNotchPlacedOnFront;
            item.IsNotchPlacedOnLeft = this.IsNotchPlacedOnLeft;
            item.IsNotchPlacedOnRight = this.IsNotchPlacedOnRight;
            item.IsRiserPlacedOnBack = this.IsRiserPlacedOnBack;
            item.IsRiserPlacedOnFront = this.IsRiserPlacedOnFront;
            item.IsRiserPlacedOnLeft = this.IsRiserPlacedOnLeft;
            item.IsRiserPlacedOnRight = this.IsRiserPlacedOnRight;
            item.IsVisible = this.IsVisible;
            item.LineColour = this.LineColour;
            item.LineThickness = this.LineThickness;
            item.MerchandisableHeight = this.MerchandisableHeight;
            item.MerchandisableDepth = this.MerchandisableDepth;
            item.MerchConstraintRow1SpacingX = this.MerchConstraintRow1SpacingX;
            item.MerchConstraintRow1SpacingY = this.MerchConstraintRow1SpacingY;
            item.MerchConstraintRow1StartX = this.MerchConstraintRow1StartX;
            item.MerchConstraintRow1StartY = this.MerchConstraintRow1StartY;
            item.MerchConstraintRow1Height = this.MerchConstraintRow1Height;
            item.MerchConstraintRow1Width = this.MerchConstraintRow1Width;
            item.MerchConstraintRow2SpacingX = this.MerchConstraintRow2SpacingX;
            item.MerchConstraintRow2SpacingY = this.MerchConstraintRow2SpacingY;
            item.MerchConstraintRow2StartX = this.MerchConstraintRow2StartX;
            item.MerchConstraintRow2StartY = this.MerchConstraintRow2StartY;
            item.MerchConstraintRow2Height = this.MerchConstraintRow2Height;
            item.MerchConstraintRow2Width = this.MerchConstraintRow2Width;
            item.Name = this.Name;
            item.NotchHeight = this.NotchHeight;
            item.NotchSpacingX = this.NotchSpacingX;
            item.NotchSpacingY = this.NotchSpacingY;
            item.NotchStartX = this.NotchStartX;
            item.NotchStartY = this.NotchStartY;
            item.NotchStyleType = (PlanogramSubComponentNotchStyleType)this.NotchStyleType;
            item.NotchWidth = this.NotchWidth;
            item.RiserColour = this.RiserColour;
            item.RiserFillPatternType = (PlanogramSubComponentFillPatternType)this.RiserFillPatternType;
            item.RiserHeight = this.RiserHeight;
            item.RiserThickness = this.RiserThickness;
            item.RiserTransparencyPercent = this.RiserTransparencyPercent;
            item.Roll = this.Roll;
            item.ShapeType = (PlanogramSubComponentShapeType)this.ShapeType;
            item.Slope = this.Slope;
            item.TransparencyPercentBack = this.TransparencyPercentBack;
            item.TransparencyPercentBottom = this.TransparencyPercentBottom;
            item.TransparencyPercentFront = this.TransparencyPercentFront;
            item.TransparencyPercentLeft = this.TransparencyPercentLeft;
            item.TransparencyPercentRight = this.TransparencyPercentRight;
            item.TransparencyPercentTop = this.TransparencyPercentTop;
            item.Width = this.Width;
            item.X = this.X;
            item.Y = this.Y;
            item.Z = this.Z;
            item.LineThickness = this.LineThickness;
            item.MerchandisingType = (PlanogramSubComponentMerchandisingType)this.MerchandisingType;
            item.CombineType = (PlanogramSubComponentCombineType)this.CombineType;
            item.MerchandisingStrategyX = (PlanogramSubComponentXMerchStrategyType)this.MerchandisingStrategyX;
            item.MerchandisingStrategyY = (PlanogramSubComponentYMerchStrategyType)this.MerchandisingStrategyY;
            item.MerchandisingStrategyZ = (PlanogramSubComponentZMerchStrategyType)this.MerchandisingStrategyZ;
            item.IsProductOverlapAllowed = this.IsProductOverlapAllowed;
            item.IsProductSqueezeAllowed = this.IsProductSqueezeAllowed;
            item.LeftOverhang = this.LeftOverhang;
            item.RightOverhang = this.RightOverhang;
            item.FrontOverhang = this.FrontOverhang;
            item.BackOverhang = this.BackOverhang;
            item.TopOverhang = this.TopOverhang;
            item.BottomOverhang = this.BottomOverhang;


            //images
            item.ImageIdFront = this.ImageIdFront;
            item.ImageIdBack = this.ImageIdBack;
            item.ImageIdTop = this.ImageIdTop;
            item.ImageIdBottom = this.ImageIdBottom;
            item.ImageIdLeft = this.ImageIdLeft;
            item.ImageIdRight = this.ImageIdRight;
            foreach (Object imgId in EnumerateImageIds())
            {
                FixtureImage img = this.Parent.Parent.Images.FindById(imgId);
                if (img == null) continue;
                img.AddOrUpdatePlanogramImage(parentComponent.Parent);
            }

            return item;
        }

        /// <summary>
        /// Enumerates through all associated image ids.
        /// </summary>
        public IEnumerable<Object> EnumerateImageIds()
        {
            if (ImageIdFront != null) yield return ImageIdFront;
            if (ImageIdLeft != null) yield return ImageIdLeft;
            if (ImageIdTop != null) yield return ImageIdTop;
            if (ImageIdBack != null) yield return ImageIdBack;
            if (ImageIdRight != null) yield return ImageIdRight;
            if (ImageIdBottom != null) yield return ImageIdBottom;
        }

        #endregion
    }
}