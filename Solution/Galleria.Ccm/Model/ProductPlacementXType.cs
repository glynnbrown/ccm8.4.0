﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27155 : L.Luong
//  Created
#endregion
#region Version History: CCM802
// V8-28766 : J.Pickup
//  Switched single to 'Manual' for user clarity.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Denotes the available product placement X types
    /// </summary>
    public enum ProductPlacementXType
    {
        Manual = 0,
        FillWide = 1
    }

    public static class ProductPlacementXTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<ProductPlacementXType, String> FriendlyNames =
            new Dictionary<ProductPlacementXType, String>()
            {
                {ProductPlacementXType.Manual, Message.Enum_ProductPlacementXType_Manual},
                {ProductPlacementXType.FillWide, Message.Enum_ProductPlacementXType_FillWide},
            };
    }
}
