﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a list of repository sync operations
    /// </summary>
    public partial class RepositorySyncList
    {
        #region Constructor
        private RepositorySyncList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all repository sync operations within the solution
        /// </summary>
        /// <returns></returns>
        public static RepositorySyncList FetchAll()
        {
            return DataPortal.Fetch<RepositorySyncList>();
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning all library types
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch()
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory("RepositorySync");
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IRepositorySyncDal dal = dalContext.GetDal<IRepositorySyncDal>())
                {
                    IEnumerable<RepositorySyncDto> dtoList = dal.FetchAll();
                    foreach (RepositorySyncDto dto in dtoList)
                    {
                        this.Add(RepositorySync.FetchRepositorySync(dalContext, dto));
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when this list is being updated
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory("RepositorySync");
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                this.Child_Update(dalContext);
                dalContext.Commit();
            }
        }
        #endregion

        #endregion
    }
}
