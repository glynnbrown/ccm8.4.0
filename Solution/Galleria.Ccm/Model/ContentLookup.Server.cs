﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26520 :J.Pickup
//	Created
#endregion

#region Version History: (CCM 801)
// CCM-28519 :J.Pickup
//	Removed Date Deleted
#endregion

#region Version History: (CCM 803)
// V8-29746 : A.Probyn
//  Updated to use linked data Id's as well as name
#endregion

#region Version History: (CCM 830)
// V8-31831 : A.Probyn
//  Updated to include PlanogramNameTemplate
// V8-32810 : M.Pettit
//  Added PrintTemplate_Id
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.DataStructures;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class ContentLookup
    {
        #region Constructor

        public ContentLookup() { } // force the use of factory methods

        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the specified ContentLookup
        /// </summary>
        /// <param name="id">The id of the ContentLookup</param>
        /// <returns></returns>
        public static ContentLookup FetchById(Int32 id)
        {
            return DataPortal.Fetch<ContentLookup>(new SingleCriteria<Int32>(id));
        }

        /// <summary>
        /// Returns the latest ContentLookup with the specified planogram id
        /// </summary>
        /// <param name="name">The Planogram name the Content Lookup links to</param>
        /// <returns>The latest version of the Assortment</returns>
        public static ContentLookup FetchByPlanogramId(Int32 planogramId)
        {
            return DataPortal.Fetch<ContentLookup>(new FetchByPlanogramIdCriteria(planogramId));
        }

        /// <summary>
        /// Deletes Content Lookup with the given id 
        /// </summary>
        /// <param name="parentContentId"></param>
        public static void DeleteById(Int32 Id)
        {
            DataPortal.Delete<ContentLookup>(new DeleteByContentLookupIdCriteria(Id));
        }

        /// <summary>
        /// Returns an ContentLookup from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        /// <returns>An ContentLookup object</returns>
        internal static ContentLookup GetContentLookup(IDalContext dalContext, ContentLookupDto dto)
        {
            return DataPortal.FetchChild<ContentLookup>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this object from a data transfer object
        /// </summary>
        /// <param name="context">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext context, ContentLookupDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Int32>(UserIdProperty, dto.UserId);
            LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            LoadProperty<String>(PlanogramNameProperty, dto.PlanogramName);
            LoadProperty<String>(AssortmentNameProperty, dto.AssortmentName);
            LoadProperty<String>(ProductUniverseNameProperty, dto.ProductUniverseName);
            LoadProperty<String>(BlockingNameProperty, dto.BlockingName);
            LoadProperty<String>(SequenceNameProperty, dto.SequenceName);
            LoadProperty<String>(MetricProfileNameProperty, dto.MetricProfileName);
            LoadProperty<String>(InventoryProfileNameProperty, dto.InventoryProfileName);
            LoadProperty<String>(ConsumerDecisionTreeNameProperty, dto.ConsumerDecisionTreeName);
            LoadProperty<String>(AssortmentMinorRevisionNameProperty, dto.AssortmentMinorRevisionName);
            LoadProperty<String>(PerformanceSelectionNameProperty, dto.PerformanceSelectionName);
            LoadProperty<String>(ClusterSchemeNameProperty, dto.ClusterSchemeName);
            LoadProperty<String>(ClusterNameProperty, dto.ClusterName);
            LoadProperty<String>(RenumberingStrategyNameProperty, dto.RenumberingStrategyName);
            LoadProperty<String>(ValidationTemplateNameProperty, dto.ValidationTemplateName);
            LoadProperty<String>(PlanogramNameTemplateNameProperty, dto.PlanogramNameTemplateName);
            LoadProperty<String>(PrintTemplateNameProperty, dto.PrintTemplateName);
            LoadProperty<Int32>(PlanogramIdProperty, dto.PlanogramId);
            LoadProperty<Int32?>(AssortmentIdProperty, dto.AssortmentId);
            LoadProperty<Int32?>(ProductUniverseIdProperty, dto.ProductUniverseId);
            LoadProperty<Int32?>(BlockingIdProperty, dto.BlockingId);
            LoadProperty<Int32?>(SequenceIdProperty, dto.SequenceId);
            LoadProperty<Int32?>(MetricProfileIdProperty, dto.MetricProfileId);
            LoadProperty<Int32?>(InventoryProfileIdProperty, dto.InventoryProfileId);
            LoadProperty<Int32?>(ConsumerDecisionTreeIdProperty, dto.ConsumerDecisionTreeId);
            LoadProperty<Int32?>(AssortmentMinorRevisionIdProperty, dto.AssortmentMinorRevisionId);
            LoadProperty<Int32?>(PerformanceSelectionIdProperty, dto.PerformanceSelectionId);
            LoadProperty<Int32?>(ClusterSchemeIdProperty, dto.ClusterSchemeId);
            LoadProperty<Int32?>(ClusterIdProperty, dto.ClusterId);
            LoadProperty<Int32?>(RenumberingStrategyIdProperty, dto.RenumberingStrategyId);
            LoadProperty<Int32?>(ValidationTemplateIdProperty, dto.ValidationTemplateId);
            LoadProperty<Int32?>(PlanogramNameTemplateIdProperty, dto.PlanogramNameTemplateId);
            LoadProperty<Int32?>(PrintTemplateIdProperty, dto.PrintTemplateId);
            LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        /// <returns>A new data transfer object</returns>
        private ContentLookupDto GetDataTransferObject()
        {
            ContentLookupDto dto = new ContentLookupDto();
            dto.Id = ReadProperty<Int32>(IdProperty);
            dto.UserId = ReadProperty<Int32>(UserIdProperty);
            dto.EntityId = ReadProperty<Int32>(EntityIdProperty);
            dto.AssortmentName = ReadProperty<String>(AssortmentNameProperty);
            dto.PlanogramName = ReadProperty<String>(PlanogramNameProperty);
            dto.ProductUniverseName = ReadProperty<String>(ProductUniverseNameProperty);
            dto.BlockingName = ReadProperty<String>(BlockingNameProperty);
            dto.SequenceName = ReadProperty<String>(SequenceNameProperty);
            dto.MetricProfileName = ReadProperty<String>(MetricProfileNameProperty);
            dto.InventoryProfileName = ReadProperty<String>(InventoryProfileNameProperty);
            dto.ConsumerDecisionTreeName = ReadProperty<String>(ConsumerDecisionTreeNameProperty);
            dto.AssortmentMinorRevisionName = ReadProperty<String>(AssortmentMinorRevisionNameProperty);
            dto.PerformanceSelectionName = ReadProperty<String>(PerformanceSelectionNameProperty);
            dto.ClusterSchemeName = ReadProperty<String>(ClusterSchemeNameProperty);
            dto.ClusterName = ReadProperty<String>(ClusterNameProperty);
            dto.RenumberingStrategyName = ReadProperty<String>(RenumberingStrategyNameProperty);
            dto.ValidationTemplateName = ReadProperty<String>(ValidationTemplateNameProperty);
            dto.PlanogramNameTemplateName = ReadProperty<String>(PlanogramNameTemplateNameProperty);
            dto.PrintTemplateName = ReadProperty<String>(PrintTemplateNameProperty);
            dto.AssortmentId = ReadProperty<Int32?>(AssortmentIdProperty);
            dto.PlanogramId = ReadProperty<Int32>(PlanogramIdProperty);
            dto.ProductUniverseId = ReadProperty<Int32?>(ProductUniverseIdProperty);
            dto.BlockingId = ReadProperty<Int32?>(BlockingIdProperty);
            dto.SequenceId = ReadProperty<Int32?>(SequenceIdProperty);
            dto.MetricProfileId = ReadProperty<Int32?>(MetricProfileIdProperty);
            dto.InventoryProfileId = ReadProperty<Int32?>(InventoryProfileIdProperty);
            dto.ConsumerDecisionTreeId = ReadProperty<Int32?>(ConsumerDecisionTreeIdProperty);
            dto.AssortmentMinorRevisionId = ReadProperty<Int32?>(AssortmentMinorRevisionIdProperty);
            dto.PerformanceSelectionId = ReadProperty<Int32?>(PerformanceSelectionIdProperty);
            dto.ClusterSchemeId = ReadProperty<Int32?>(ClusterSchemeIdProperty);
            dto.ClusterId = ReadProperty<Int32?>(ClusterIdProperty);
            dto.RenumberingStrategyId = ReadProperty<Int32?>(RenumberingStrategyIdProperty);
            dto.ValidationTemplateId = ReadProperty<Int32?>(ValidationTemplateIdProperty);
            dto.PlanogramNameTemplateId = ReadProperty<Int32?>(PlanogramNameTemplateIdProperty);
            dto.PrintTemplateId = ReadProperty<Int32?>(PrintTemplateIdProperty);
            dto.DateCreated = ReadProperty<DateTime>(DateCreatedProperty);
            dto.DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty);
            dto.RowVersion = ReadProperty<RowVersion>(RowVersionProperty);

            return dto;
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching a Content Lookup by id
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IContentLookupDal dal = dalContext.GetDal<IContentLookupDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchById(criteria.Value));
                }
            }
        }

        /// <summary>
        /// Called when fetching a Content Lookup by id
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByPlanogramIdCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IContentLookupDal dal = dalContext.GetDal<IContentLookupDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchByPlanogramId(criteria.PlanogramId));
                }
            }
        }


        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, ContentLookupDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }


        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this instance into the database
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                ContentLookupDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (IContentLookupDal dal = dalContext.GetDal<IContentLookupDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<ContentLookup>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when this instance is being updated
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                ContentLookupDto dto = GetDataTransferObject();
                using (IContentLookupDal dal = dalContext.GetDal<IContentLookupDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }

        /// <summary>
        /// Called when this instance is being updated from a parent object
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected void Child_Update()
        {
            this.DataPortal_Update();
        }
        #endregion

        #region Delete

        /// <summary>
        ///Called when deleting by ContentLookupId
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Delete(DeleteByContentLookupIdCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IContentLookupDal dal = dalContext.GetDal<IContentLookupDal>())
                {
                    dal.DeleteById(criteria.ContentLookupId);
                }
            }
        }

        #endregion

        #endregion
    }
}
