﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
// V8-27132 : A.Kuszyk
//  Implemented common interface.
// V8-28026 : A.Kuszyk
//  Fixed Create method to get Id.
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a cdt node assigned product
    /// (Child of ConsumerDecisionTreeNodeProductList)
    /// </summary>
    [Serializable]
    public sealed partial class ConsumerDecisionTreeNodeProduct : ModelObject<ConsumerDecisionTreeNodeProduct>, IPlanogramConsumerDecisionTreeNodeProduct
    {
        #region Parent

        /// <summary>
        /// Returns the parent node of this product
        /// </summary>
        public ConsumerDecisionTreeNode ParentConsumerDecisionTreeNode
        {
            get
            {
                ConsumerDecisionTreeNodeProductList parentList = base.Parent as ConsumerDecisionTreeNodeProductList;
                if (parentList != null)
                {
                    return parentList.Parent as ConsumerDecisionTreeNode;
                }
                return null;
            }
        }

        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> ProductIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductId);
        public Int32 ProductId
        {
            get { return GetProperty<Int32>(ProductIdProperty); }
        }

        public static readonly ModelPropertyInfo<String> ProductGtinProperty =
            RegisterModelProperty<String>(c => c.ProductGtin);
        public String ProductGtin
        {
            get { return GetProperty<String>(ProductGtinProperty); }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeNodeProduct), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ConsumerDecisionTreeCreate.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeNodeProduct), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ConsumerDecisionTreeGet.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeNodeProduct), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ConsumerDecisionTreeEdit.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeNodeProduct), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ConsumerDecisionTreeDelete.ToString()));
        }
        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new MinValue<Int32>(ProductIdProperty, 1));
        }

        #endregion

        #region Factory Methods

        public static ConsumerDecisionTreeNodeProduct NewConsumerDecisionTreeNodeProduct(Int32 productId)
        {
            ConsumerDecisionTreeNodeProduct item = new ConsumerDecisionTreeNodeProduct();
            item.Create(productId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 productId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(ProductIdProperty, productId);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion
    }
} 
