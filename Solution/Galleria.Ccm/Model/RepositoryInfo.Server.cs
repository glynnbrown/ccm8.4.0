﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model object representing info about a repository
    /// </summary>
    public partial class RepositoryInfo
    {
        #region Constructor
        private RepositoryInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the repository information
        /// </summary>
        /// <param name="name">The Assortment name</param>
        /// <returns>The latest version of the Assortment</returns>
        public static RepositoryInfo Fetch(Dictionary<String, String> parameters)
        {
            return DataPortal.Fetch<RepositoryInfo>(parameters);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to laod from</param>
        private void LoadDataTransferObject(IDalContext dalContext, RepositoryInfoDto dto)
        {
            this.LoadProperty<Guid>(UniqueIdProperty, dto.UniqueId);
            this.LoadProperty<string>(NameProperty, dto.Name);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching a repository info
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(Dictionary<String, String> parameters)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory(Constants.RepositoryDal);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IRepositoryInfoDal dal = dalContext.GetDal<IRepositoryInfoDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.Fetch(parameters));
                }
            }
        }
        #endregion

        #endregion
    }
}
