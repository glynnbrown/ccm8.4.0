﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//      Initial version.
// V8-25304 : L.Ineson
//  Added HangFromBottom for rods and bars where the product hangs from the bottom facing.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Resources.Language;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    public enum FixtureSubComponentMerchandisingType
    {
        None = 0,
        Stack = 1,
        Hang = 2,
        HangFromBottom = 3,
    }

    /// <summary>
    /// PlanogramSubComponentMerchandisingType Helper Class
    /// </summary>
    public static class FixtureSubComponentMerchandisingTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<FixtureSubComponentMerchandisingType, String> FriendlyNames =
            new Dictionary<FixtureSubComponentMerchandisingType, String>()
            {
                {FixtureSubComponentMerchandisingType.None, Message.Enum_PlanogramSubComponentMerchandisingType_None},
                {FixtureSubComponentMerchandisingType.Stack, Message.Enum_PlanogramSubComponentMerchandisingType_Stack},
                {FixtureSubComponentMerchandisingType.Hang, Message.Enum_PlanogramSubComponentMerchandisingType_Hang},
                {FixtureSubComponentMerchandisingType.HangFromBottom, Message.Enum_PlanogramSubComponentMerchandisingType_HangFromBottom},
            };

        public static FixtureSubComponentMerchandisingType Parse(String enumName)
        {
            return EnumHelper.Parse<FixtureSubComponentMerchandisingType>(enumName, FixtureSubComponentMerchandisingType.Stack);
        }

    }
}
