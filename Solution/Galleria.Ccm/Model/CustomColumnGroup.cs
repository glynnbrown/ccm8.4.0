#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26076 : A.Silva ~ Removed Id property as identifier, use Grouping Property instead.

#endregion

#region Version History: (CCM 8.3)
// V8-31542 : L.Ineson
//  Added Id and updated implementation.
#endregion

#endregion

using System;
using Galleria.Framework.Model;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class CustomColumnGroup : ModelObject<CustomColumnGroup>
    {
        #region Parent

        /// <summary>
        /// Returns the parent CustomColumnLayout
        /// </summary>
        public new CustomColumnLayout Parent
        {
            get
            {
                CustomColumnGroupList parentList = base.Parent as CustomColumnGroupList;
                if (parentList == null) return null;
                else return parentList.Parent;
            }
        }

        #endregion

        #region Properties

        #region Id

        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);

        /// <summary>
        /// Returns the item unqiue identifier.
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        #endregion

        #region Grouping Property

        /// <summary>
        /// Grouping property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> GroupingProperty = 
            RegisterModelProperty<String>(o => o.Grouping, Message.CustomColumnGroup_Grouping);

        /// <summary>
        ///     The text of the <see cref="CustomColumnGroup"/>.
        /// </summary>
        public String Grouping
        {
            get { return GetProperty(GroupingProperty); }
            set { SetProperty(GroupingProperty, value); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Business Rules

        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Creates and returns a new <see cref="CustomColumnGroup"/>.
        /// </summary>
        /// <returns>A new instance of <see cref="CustomColumnGroup"/>.</returns>
        public static CustomColumnGroup NewCustomColumnGroup(String grouping)
        {
            CustomColumnGroup item = new CustomColumnGroup();
            item.Create(grouping);
            return item;
        }

        #endregion

        #region Data Access

        /// <summary>
        ///     Called when a new instance of <see cref="CustomColumnGroup"/> is being created.
        /// </summary>
        private void Create(String grouping)
        {
            LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty<String>(GroupingProperty, grouping);

            MarkAsChild();
            Create();
        }

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<CustomColumnGroup>(oldId, newId);
        }

        #endregion

    }
}