﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of FixtureSubComponent objects.
    /// </summary>
    [Serializable]
    public sealed partial class FixtureSubComponentList : ModelList<FixtureSubComponentList, FixtureSubComponent>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new FixtureComponent Parent
        {
            get { return (FixtureComponent)base.Parent; }
        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static FixtureSubComponentList NewFixtureSubComponentList()
        {
            FixtureSubComponentList item = new FixtureSubComponentList();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static FixtureSubComponentList NewFixtureSubComponentList(PlanogramSubComponentList planSubComponents, FixturePackage parentPackage)
        {
            FixtureSubComponentList item = new FixtureSubComponentList();
            item.Create(planSubComponents, parentPackage);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(PlanogramSubComponentList planSubComponents, FixturePackage parentPackage)
        {
            this.RaiseListChangedEvents = false;
            Add(planSubComponents, parentPackage);
            this.RaiseListChangedEvents = true;

            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Adds the given subcomponents to this list.
        /// </summary>
        private void Add(IEnumerable<PlanogramSubComponent> sourceList, FixturePackage parentPackage)
        {
            foreach (PlanogramSubComponent planSub in sourceList)
            {
                Add(FixtureSubComponent.NewFixtureSubComponent(planSub, parentPackage));
            }
        }

        #endregion
    }
}