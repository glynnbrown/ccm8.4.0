#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26076 : A.Silva ~ Removed Id Property as identifier, use Path Property instead.

#endregion

#region Version History: (CCM 8.3)
// V8-31542 : L.Ineson
//  Added Id and updated implementation.
#endregion

#endregion

using System;
using System.ComponentModel;
using Galleria.Framework.Model;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Helpers;
using Csla.Rules.CommonRules;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class CustomColumnSort : ModelObject<CustomColumnSort>
    {
        #region Parent

        /// <summary>
        /// Returns the parent CustomColumnLayout
        /// </summary>
        public new CustomColumnLayout Parent
        {
            get 
            {
                CustomColumnSortList parentList = base.Parent as CustomColumnSortList;
                if (parentList == null) return null;
                else return parentList.Parent;
            }
        }

        #endregion

        #region Properties

        #region Id

        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);

        /// <summary>
        /// Returns the item unqiue identifier.
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        #endregion

        #region Path Property

        /// <summary>
        /// Path property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> PathProperty =
            RegisterModelProperty<String>(c => c.Path, Message.CustomColumnSort_Path);

        /// <summary>
        ///  The binding path to the column being sorted.
        /// </summary>
        public String Path
        {
            get { return GetProperty(PathProperty); }
            set { SetProperty(PathProperty, value); }
        }

        #endregion

        #region Direction Property

        /// <summary>
        /// Direction property definition
        /// </summary>
        public static readonly ModelPropertyInfo<ListSortDirection> DirectionProperty = 
            RegisterModelProperty<ListSortDirection>(c => c.Direction, Message.CustomColumnSort_Direction);

        /// <summary>
        ///     The sorting direction of the column.
        /// </summary>
        public ListSortDirection Direction
        {
            get { return GetProperty(DirectionProperty); }
            set { SetProperty(DirectionProperty, value); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Business Rules

        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(PathProperty));
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public static CustomColumnSort NewCustomColumnSort(String path, ListSortDirection direction)
        {
            var filter = new CustomColumnSort();
            filter.Create(path, direction);
            return filter;
        }

        #endregion

        #region Data Access

        /// <summary>
        ///     Called when a new instance of <see cref="CustomColumnSort"/> is being created.
        /// </summary>
        private void Create(String path, ListSortDirection direction)
        {
            LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            LoadProperty<String>(PathProperty, path);
            LoadProperty<ListSortDirection>(DirectionProperty, direction);
            
            MarkAsChild();
            Create();
        }

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<CustomColumnSort>(oldId, newId);
        }

        #endregion

    }
}