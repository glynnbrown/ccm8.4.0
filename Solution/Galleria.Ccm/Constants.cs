﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM800
//  Created : N.Foster
//  V8-25556 : D.Pleasance
//      Added OdsDalType, SyncDalType, FoundationServicesEndpoint
// CCM-25903 : N.Haywood
//      Added CcmDatabaseType
// CCM-26234 : L.Ineson
//     Added EntityAccessPermission
// CCM-27266 : L.Ineson
//      Added file extension constants.
// CCM -27804 : L.Ineson
//      Added PlanogramImportTemplateFileExtension
// CCM -27941 : J.Pickup
//      Added MssqlDal
// V8-27938 : N.Haywood
//  Added Data Sheets
#endregion
#region Version History: CCM820
// V8-30738 : L.Ineson
//  Added PrintTemplateFileExtension
// V8-31225 : A.Silva
//  Added FileTemplatesFolderName.
#endregion
#region Version History: CCM830
// V8-31546 : M.Pettit
//  Added PlanogramExportTemplateFileExtension
//  Added ExportFileTemplatesFolderName
// V8-31945 : A.Silva
//  Added PlanogramComparisonTemplatesFolderName
// V8-32823  : J.Pickup
//  Added app setting keys (Don't exist in app settings but theese are the specified keys to use (Do not change!).
#endregion

#endregion

using System;

namespace Galleria.Ccm
{
    /// <summary>
    /// Static class containing global constants
    /// </summary>
    public static class Constants
    {
        internal const String UserDal = "User";
        internal const String SyncDal = "Sync"; // the sync dal type string
        internal const String MssqlDal = "Mssql";
        internal const String EntityAccessPermission = "Entity:{0}"; // the entity data access permission name

        public const String CcmDatabaseType = "Customer Centric Merchandising"; //The schema table type description for CCM databases

        //File extensions
        public const String PlanogramFileExtension = ".pog";
        public const String PlanogramAutosaveFileExtension = ".pog.auto";
        public const String FixtureLibraryFileExtension = ".pogfx";
        public const String LabelFileExtension = ".poglb";
        public const String HighlightFileExtension = ".poghl";
        public const String CustomColumnLayoutFileExtension = ".pogcl";
        public const String ValidationTemplateFileExtension = ".pogvt";
        public const String ProductLibraryFileExtension = ".pogpl";
        public const String PlanogramImportTemplateFileExtension = ".pogft";
        public const String PlanogramExportTemplateFileExtension = ".pogeft";
        public const String DataSheetFileExtension = ".pogds";
        public const String PrintTemplateFileExtension = ".pogprt";
        public const String PlanogramComparisonTemplateFileExtension = "pogct";
        
        //Fixed Directory names
        public const String AppDataFolderName = @"Customer Centric Merchandising v8";
        public const String EditorAppDataFolderName = @"Customer Centric Merchandising v8\Space Planning";
        public const String MyDocsAppFolderName = @"Customer Centric Merchandising";
        public const String FileTemplatesFolderName = @"Import File Templates";
        public const String ExportFileTemplatesFolderName = @"Export File Templates";
        public const String PlanogramComparisonTemplatesFolderName = @"Comparison Templates";

        //Fixed File Paths
        public const String EditorRecentPlanogramsFileName = "Mru.xml";
        public const String EditorUserSettingsFileName = "UserSettings.xml";
        public const String AppConnectionSettingsFileName = "CCMUsers.xml";

        //App Setting Keys SpacePlanning & Workflow Common.
        public const String exportSpacemanAvailable = "ExportSpacemanAvailable";
        public const String exportJDAAvailable = "ExportJDAAvailable";
        public const String exportApolloAvailable = "ExportApolloAvailable";

    }
}
