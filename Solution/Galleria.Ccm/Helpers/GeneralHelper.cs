﻿using System;
using System.Reflection;
using System.Collections;
using System.Globalization;

namespace Galleria.Ccm.Helpers
{
    public static class GeneralHelper
    {
        /// <summary>
        /// Returns the value of the property at the given path for the given item.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="propertyPath"></param>
        /// <returns></returns>
        public static Object GetItemPropertyValue(Object item, String propertyPath)
        {
            Object objPropertyValue = item;

            if (propertyPath != null)
            {
                String[] pathParts = propertyPath.Split('.');

                //follow the path down to find the property value
                Type sourceType = item.GetType();

                //cycle through down the path parts.
                foreach (String propertyName in pathParts)
                {
                    #region Standard Path
                    //check if we are pointing directly to the object
                    if (String.IsNullOrEmpty(propertyName))
                    {
                        objPropertyValue = item.ToString();
                    }
                    #endregion

                    #region Collection Path
                    //check if the propertyName given includes a dictionary index
                    else if (propertyName.EndsWith("]"))
                    {

                        Int32 startIndexParam = propertyName.IndexOf('[');
                        String propertyWithoutParam = propertyName.Substring(0, startIndexParam);

                        String paramIndexKey =
                            propertyName.Substring(startIndexParam + 1, propertyName.Length - 2 - propertyWithoutParam.Length);


                        PropertyInfo property = sourceType.GetProperty(propertyWithoutParam);
                        if (property != null)
                        {
                            objPropertyValue = property.GetValue(objPropertyValue, null);

                            if (objPropertyValue != null)
                            {
                                sourceType = objPropertyValue.GetType();

                                //resolve the index further for IDictionary
                                if (objPropertyValue is IDictionary)
                                {
                                    IDictionary dictionaryValue = objPropertyValue as IDictionary;
                                    if (dictionaryValue != null)
                                    {
                                        objPropertyValue = null;

                                        if (dictionaryValue.Keys.Count > 0)
                                        {
                                            var typing = dictionaryValue.GetType().GetGenericArguments();
                                            Type keyType = typing[0];

                                            //convert the key if required
                                            Object convertedKey = null;

                                            if (keyType == typeof(String))
                                            {
                                                convertedKey = paramIndexKey;
                                            }
                                            else if (keyType == typeof(Int32))
                                            {
                                                Int32 intKey;
                                                if (Int32.TryParse(paramIndexKey, out intKey))
                                                {
                                                    convertedKey = intKey;
                                                }
                                            }
                                            else if (keyType == typeof(Guid))
                                            {
                                                Guid gKey;
                                                if (Guid.TryParse(paramIndexKey, out gKey))
                                                {
                                                    convertedKey = gKey;
                                                }
                                            }
                                            else
                                            {
                                                Int32 paramIndex;
                                                if (Int32.TryParse(paramIndexKey, out paramIndex))
                                                {
                                                    convertedKey = paramIndex;
                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                        convertedKey = Convert.ChangeType(paramIndexKey, keyType, CultureInfo.CurrentCulture);
                                                    }
                                                    catch (Exception) { }
                                                }
                                            }

                                            //use the key
                                            if (convertedKey != null)
                                            {
                                                try
                                                {
                                                    objPropertyValue = dictionaryValue[convertedKey];
                                                }
                                                catch (Exception) { }
                                            }
                                        }

                                        if (objPropertyValue != null)
                                        {
                                            sourceType = objPropertyValue.GetType();
                                        }
                                    }
                                }

                                //[GAF-19302] resolve the index further for IList if IDictionary is not supported
                                else if (objPropertyValue is IList)
                                {
                                    IList listValue = objPropertyValue as IList;
                                    if (listValue != null)
                                    {
                                        Int32 paramIndex = 0;
                                        if (Int32.TryParse(paramIndexKey, out paramIndex))
                                        {
                                            if (paramIndex < listValue.Count)
                                            {
                                                objPropertyValue = listValue[paramIndex];

                                                if (objPropertyValue != null)
                                                {
                                                    sourceType = objPropertyValue.GetType();
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            else { break; }
                        }
                        else
                        {
                            objPropertyValue = item.ToString();
                        }
                    }
                    #endregion

                    #region Normal Path
                    else
                    {
                        //process the normal property
                        PropertyInfo property = sourceType.GetProperty(propertyName);

                        if (property != null)
                        {
                            objPropertyValue = property.GetValue(objPropertyValue, null);

                            if (objPropertyValue != null)
                            {
                                sourceType = objPropertyValue.GetType();
                            }
                            else { break; }
                        }
                        else
                        {
                            objPropertyValue = item.ToString();
                        }
                    }
                    #endregion
                }
            }

            return objPropertyValue;
        }
    }
}
