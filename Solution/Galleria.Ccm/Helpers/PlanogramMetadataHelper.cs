﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015
#region Version History: CCM803
// V8-28491 : L.Luong
//  Initial Version
// V8-29711 : L.Ineson
//  Changed to get dictionary of product infos all at once and up front
// to speed thngs up.
#endregion
#region Version History: CCM820
// V8-30738 : L.Ineson
//  Added IPlanogramImageSettings additional properties
#endregion
#region Version History: (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Added Illegal and Legal MetaData and Validation
// V8-32437 : M.Pettit
//  Products are now only marked as invalid legal products if a category code has been set
// V8-32636 : A.Probyn
//  Implemented Annotations
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Helpers
{
    /// <summary>
    /// Helper class used when calculating planogram metadata values
    /// </summary>
    [Serializable]
    public sealed class PlanogramMetadataHelper : IPlanogramMetadataHelper
    {
        #region Nested Classes

        /// <summary>
        /// Meta data plan thumbnail implementation of IPlanogramImageSettings
        /// </summary>
        public sealed class PlanogramThumbnailSettings : IPlanogramImageSettings
        {
            #region Properties
            public PlanogramImageViewType ViewType { get; private set; }
            public Boolean Risers { get; private set; }
            public Boolean ChestWalls { get; private set; }
            public Boolean FixtureImages { get; private set; }
            public Boolean FixtureFillPatterns { get; private set; }
            public Boolean Notches { get; private set; }
            public Boolean PegHoles { get; private set; }
            public Boolean DividerLines { get; private set; }
            public Boolean Positions { get; private set; }
            public Boolean PositionUnits { get; private set; }
            public Boolean RotateTopDownComponents { get; private set; }
            public Boolean ProductImages { get; private set; }
            public Boolean ProductShapes { get; private set; }
            public Boolean ProductFillPatterns { get; private set; }
            public Boolean ProductFillColours { get { return true; } }
            public Boolean Pegs { get; private set; }
            public Boolean Dividers { get; private set; }
            public Boolean TextBoxes { get; private set; }
            public Boolean Annotations { get; private set; }
            public IPlanogramLabel ProductLabel { get; private set; }
            public IPlanogramLabel FixtureLabel { get; private set; }
            public PlanogramPositionHighlightColours PositionHighlights { get; private set; }
            public PlanogramPositionLabelTexts PositionLabelText { get; private set; }
            public PlanogramFixtureLabelTexts FixtureLabelText { get; private set; }
            public Framework.DataStructures.VectorValue? CameraLookDirection { get { return null; } }
            public Framework.DataStructures.PointValue? CameraPosition { get { return null; } }
            #endregion

            #region Constructor

            public PlanogramThumbnailSettings(
                SystemSettings sysSettings, 
                IPlanogramLabel productLabel, 
                IPlanogramLabel fixtureLabel,
                PlanogramPositionLabelTexts positionLabelText,
                PlanogramFixtureLabelTexts fixtureLabelText,
                PlanogramPositionHighlightColours positionHighlights)
            {
                this.Risers = sysSettings.ShelfRisers;
                this.ChestWalls = sysSettings.ChestWalls;
                this.FixtureImages = sysSettings.FixtureImages;
                this.FixtureFillPatterns = sysSettings.FixtureFillPatterns;
                this.Notches = sysSettings.Notches;
                this.PegHoles = sysSettings.PegHoles;
                this.DividerLines = sysSettings.DividerLines;
                this.Positions = sysSettings.Positions;
                this.PositionUnits = sysSettings.PositionUnits;
                this.RotateTopDownComponents = sysSettings.RotateTopDownComponents;
                this.ProductImages = sysSettings.ProductImages;
                this.ProductShapes = sysSettings.ProductShapes;
                this.ProductFillPatterns = sysSettings.ProductFillPatterns;
                this.Pegs = sysSettings.Pegs;
                this.Dividers = sysSettings.Dividers;
                this.Annotations = sysSettings.Annotations;
                this.ProductLabel = productLabel;
                this.FixtureLabel = fixtureLabel;
                this.PositionLabelText = positionLabelText;
                this.FixtureLabelText = fixtureLabelText;
                this.PositionHighlights = positionHighlights;
                this.TextBoxes = true;
                this.ViewType = PlanogramImageViewType.Design;
            }

            #endregion

        }

        #endregion

        #region Fields

        private Package _package;

        private Int32 _entityId;
        private SystemSettings _entitySettings;
        private Dictionary<String, ProductInfo> _productInfosByGtin;
        private Dictionary<Planogram, List<String>> _locationProductIllegalList;//planinfo : productgtin
        private Dictionary<Planogram, List<String>> _locationProductLegalList;//planinfo : productgtin
        #endregion

        #region Constructor
        private PlanogramMetadataHelper() { } //force use of factory methods.
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="entityId">the entity id to fetch from</param>
        /// <param name="planogram">the planogram to load informatin for.</param>
        public static PlanogramMetadataHelper NewPlanogramMetadataHelper(Int32 entityId, Package package)
        {
            PlanogramMetadataHelper item = new PlanogramMetadataHelper();
            item.Create(entityId, package);
            return item;
        }

        #endregion

        #region Data Access

        private void Create(Int32 entityId, Package package)
        {
            _package = package;
            _entityId = entityId;

            if (_entityId > 0)
            {
                _productInfosByGtin =
                    ProductInfoList.FetchByEntityIdProductGtins(
                        _entityId,
                        package.Planograms.SelectMany(pl => pl.Products.Select(p => p.Gtin)).Distinct().ToList())
                    .ToDictionary(p => p.Gtin);

                _locationProductIllegalList = new Dictionary<Planogram, List<String>>();
                _locationProductLegalList = new Dictionary<Planogram, List<String>>();
               
                foreach (Planogram plan in package.Planograms)
                {
                    if (plan.LocationCode != null && plan.CategoryCode != null)
                    {
                        _locationProductIllegalList[plan] = 
                            ProductInfoList.FetchIllegalsByLocationCodeProductGroupCode(plan.LocationCode, plan.CategoryCode)
                            .Select(p => p.Gtin).Distinct().ToList();
                    }
                }

                foreach (Planogram plan in package.Planograms)
                {
                    if (plan.LocationCode != null && plan.CategoryCode != null)
                    {
                        _locationProductLegalList[plan] =
                            ProductInfoList.FetchLegalsByLocationCodeProductGroupCode(plan.LocationCode, plan.CategoryCode)
                            .Select(p => p.Gtin).Distinct().ToList();
                    }
                }
            }
            else
            {
                _productInfosByGtin = new Dictionary<String, ProductInfo>();
                _locationProductIllegalList = new Dictionary<Planogram, List<String>>();
                _locationProductLegalList = new Dictionary<Planogram, List<String>>();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns true if a product with a matching gtin exists in
        /// master data.
        /// </summary>
        public Boolean IsProductGtinInMasterData(String gtin)
        {
            return _productInfosByGtin.ContainsKey(gtin);
        }

        /// <summary>
        /// Returns the settings for the current entity.
        /// </summary>
        private SystemSettings GetEntitySettings()
        {
            if (_entitySettings == null && _entityId > 0)
            {
                Entity ent = Entity.FetchById(_entityId);
                if (ent != null) _entitySettings = ent.SystemSettings;
            }
            return _entitySettings;
        }

        /// <summary>
        /// Returns the settings to use when rendering the planogram thumbnail image.
        /// </summary>
        public IPlanogramImageSettings GetPlanogramImageSettings(Planogram plan)
        {
            SystemSettings settings = GetEntitySettings();

            if (settings == null)
            {
                settings = SystemSettings.NewSystemSettings();
            }

            IPlanogramLabel productLabel = null;
            PlanogramPositionLabelTexts positionLabelText = null;
            PlanogramFixtureLabelTexts fixtureLabelText = null;
            IPlanogramLabel fixtureLabel = null;
            PlanogramPositionHighlightColours highlightColours = null;

            if (_entitySettings != null)
            {
                //Product Label
                if (!String.IsNullOrEmpty(settings.ProductLabel))
                {
                    try
                    {
                        productLabel = Label.FetchByEntityIdName(_entityId, settings.ProductLabel);
                        if(productLabel != null)
                        {
                            //Process the label against the plan.
                            positionLabelText = new PlanogramPositionLabelTexts(productLabel.Text, plan);
                        }
                    }
                    catch (DataPortalException) { }
                }

                //Fixture Label
                if (!String.IsNullOrEmpty(settings.FixtureLabel))
                {
                    try
                    {
                        fixtureLabel = Label.FetchByEntityIdName(_entityId, settings.FixtureLabel);
                        if (fixtureLabel != null)
                        {
                            fixtureLabelText = new PlanogramFixtureLabelTexts(fixtureLabel.Text, plan);
                        }
                    }
                    catch (DataPortalException) { }
                }

                //Highlight
                if (!String.IsNullOrEmpty(settings.Highlight))
                {
                    try
                    {
                        Highlight highlight = Highlight.FetchByEntityIdName(_entityId, settings.Highlight);
                        if (highlight != null)
                        {
                            //Process the highlight against the plan
                            highlightColours = new PlanogramPositionHighlightColours(
                                PlanogramHighlightHelper.ProcessPositionHighlight(plan, highlight));
                        }
                    }
                    catch (DataPortalException) { }
                }


            }


            PlanogramThumbnailSettings imgSettings =
                new PlanogramThumbnailSettings(settings, productLabel, fixtureLabel,
                    positionLabelText, fixtureLabelText, highlightColours);

            return imgSettings;
        }

        public Boolean IsProductIllegal(Planogram plan, String gtin)
        {
            if (_locationProductIllegalList.ContainsKey(plan))
            {
                return _locationProductIllegalList[plan].Contains(gtin);
            }
            return false;
        }

        public Boolean IsProductNotLegal(Planogram plan, String gtin)
        {
            if (_locationProductLegalList.ContainsKey(plan))
            {
                //the planogram must have a category set, otherwise the _locationProductLegalList for the planogram is empty.
                //The list is built from a FetchLegalsByLocationCodeProductGroupCode)
                if (!String.IsNullOrEmpty(plan.CategoryCode))
                {
                    return !_locationProductLegalList[plan].Contains(gtin);
                }
            }
            return false;
        }

        #endregion
    }
}
