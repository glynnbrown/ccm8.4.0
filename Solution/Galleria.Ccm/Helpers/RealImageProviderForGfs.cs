#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History: CCM801

// V8-28964 : A.Silva
//      Moved from RealImageProviderForRepository.cs.

#endregion

#region Version History: CCM820
// V8-31365 : L.Ineson
//  Added the ability to bulk load images when the async load is running.
//   Fetch of GFS entity and compression types is also now done up front.
#endregion
#region Version History: CCM830
// V8-32777 : L.Ineson
//  GFS entity fetch will recheck after 5 mins if not found.
// CCM-18454 : L.Ineson
//  Made changes to how images are cached, prioritised and flagged as expired to speed things up.
#endregion
#endregion

using Galleria.Ccm.Model;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Services;
using Galleria.Ccm.Services.Image;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using Image = Galleria.Ccm.Services.Image.Image;
using ProductImage = Galleria.Ccm.Services.Image.ProductImage;

namespace Galleria.Ccm.Helpers
{
    /// <summary>
    /// Service for providing a way to retrieve real images from
    /// a connected gfs webservice.
    /// </summary>
    public sealed class RealImageProviderForGfs : RealImageProviderBase
    {
        #region Fields

        private String _endpoint;
        private String _compressionName;
        private GFSModel.Entity _gfsEntity = null;
        private DateTime? _lastEntityCheck;

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public RealImageProviderForGfs(Int32 entityId, IRealImageProviderSettings settings)
            : base(settings, entityId)
        {
            //Tell the base that when fetching images asynchronously this should be done in bulk.

            this.FetchBatchSize = Convert.ToInt32(ConfigurationManager.AppSettings["GFSImagePollBatchSize"]);

            //get the ccm entity
            UpdateGFSEntityDetails();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the details of the connected GFS entity.
        /// </summary>
        private void UpdateGFSEntityDetails()
        {
            try
            {
                _lastEntityCheck = DateTime.Now;

                Entity ccmEntity = Entity.FetchById(this.EntityId);
                if (!ccmEntity.GFSId.HasValue) return;

                _endpoint = ccmEntity.SystemSettings.FoundationServicesEndpoint;
                _compressionName = ccmEntity.SystemSettings.CompressionName;

                _gfsEntity = GFSModel.Entity.FetchByCcmEntity(ccmEntity);

                //  Validate the compression name so that it exists in GFS too.
                IEnumerable<GFSModel.Compression> gfsCompressions = GFSModel.CompressionList.FetchAll(_gfsEntity);
                if (!gfsCompressions.Any(o => String.Equals(o.Name, _compressionName, StringComparison.InvariantCultureIgnoreCase)))
                {
                    Debug.Fail("The Compression used to fetch images from GFS should be valid in GFS.");
                    _compressionName = gfsCompressions.First().Name;
                }
            }
            catch (Exception) { }

        }

        /// <summary>
        ///     Updates the real image in the <see cref="PlanogramImage"/> instance.
        /// </summary>
        /// <param name="planogramImage">The instance that should have its image data updated.</param>
        /// <returns>A <see cref="Byte"/> array with the image that was updated, if any.</returns>
        /// <remarks>This override will use the GFS service to retrieve images from.</remarks>
        protected override Byte[] UpdateRealImage(CachedPlanogramImage planogramImage)
        {
            //  Validate parameters.
            if (planogramImage == null) return null;


            //  Check whether the image was retrieved not too long ago.
            String cacheItemId = planogramImage.Image.FileName;
            if (planogramImage.IsImageConfirmedMissing()) return null;

            //  Fetch the image from GFS.
            Image image = null;
            if (_gfsEntity != null)
            {
                //  Reconstruct the original criteria used for the cache item.
                CachedPlanogramImageCriteria reconstructedCriteria = new CachedPlanogramImageCriteria(cacheItemId);

                //  Do not try to fetch an image if there is no Attribute (GTIN) value as we will not be able to query GFS.
                if (reconstructedCriteria.Attribute != null)
                {
                    try
                    {
                        //  Get the request parameters
                        String facingType = reconstructedCriteria.FacingType.ToString();
                        String imageType = GetValidImageTypeNameForGfs(reconstructedCriteria.ImageType);
                        if (String.IsNullOrEmpty(imageType)) return null;

                        //  Try getting the actual image from GFS.
                        using (FoundationServiceClient proxy = new FoundationServiceClient(_gfsEntity.EndpointRoot))
                        {
                            ProductImage productImage =
                                proxy.ImageServiceClient
                                     .GetProductImageByEntityNameProductGTINImageTypeFacingTypeCompressionName(
                                         new GetProductImageByEntityNameProductGTINImageTypeFacingTypeCompressionNameRequest(
                                             _gfsEntity.Name, reconstructedCriteria.Attribute, imageType, facingType,
                                             _compressionName)).ProductImage;

                            if (productImage != null) image = productImage.ProductImageData;
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.Fail(e.Message);
                        Console.WriteLine(e.Message);
                    }
                }
            }


            if (image == null)
            {
                return null;
            }
            else
            {
                //  Update the image data.
                Byte[] imageData = image.ImageBlob.Data;
                planogramImage.UpdateImage(imageData);
                return imageData;
            }
        }

        /// <summary>
        /// Updates the given list of real images.
        /// Called when performing a bulk fetch operation.
        /// </summary>
        /// <param name="imageList"></param>
        protected override void UpdateRealImages(List<CachedPlanogramImage> imageList)
        {
            //create a list of images we should try to fetch from GFS
            Dictionary<String, CachedPlanogramImage> imagesByCacheId = new Dictionary<String, CachedPlanogramImage>();

            foreach (CachedPlanogramImage planogramImage in imageList)
            {
                //  Check whether the image was retrieved not too long ago.
                String cacheItemId = planogramImage.Image.FileName;
                if (planogramImage.IsImageConfirmedMissing()) continue;

                imagesByCacheId.Add(cacheItemId, planogramImage);
            }

            //perform the fetch
            try
            {
                GFSModel.Entity gfsEntity = _gfsEntity;
                if (gfsEntity == null && (!_lastEntityCheck.HasValue || _lastEntityCheck.Value.AddMinutes(5) < DateTime.Now))
                {
                    UpdateGFSEntityDetails();
                    gfsEntity = _gfsEntity;
                }
                String compressionName = _compressionName;
                
                //create the request parameters
                Dictionary<String, ProductImageRequestItem> requestItems = new Dictionary<String, ProductImageRequestItem>();
                foreach (String cacheItemId in imagesByCacheId.Keys)
                {
                    CachedPlanogramImageCriteria criteria = new CachedPlanogramImageCriteria(cacheItemId);
                    if (criteria.Attribute == null) continue;

                    String facingType = criteria.FacingType.ToString();
                    String imageType = GetValidImageTypeNameForGfs(criteria.ImageType);
                    if (String.IsNullOrEmpty(imageType)) continue;

                    requestItems.Add(cacheItemId, new ProductImageRequestItem()
                    {
                        CompressionName = compressionName,
                        GTIN = criteria.Attribute,
                        ImageType = imageType,
                        FacingType = facingType
                    });

                }

                if (requestItems.Any())
                {
                    //perform the request
                    using (FoundationServiceClient proxy = new FoundationServiceClient(gfsEntity.EndpointRoot))
                    {
                        List<ProductImage> gfsImages =
                            proxy.ImageServiceClient.GetProductImages(
                            new GetProductImagesRequest(gfsEntity.Name, requestItems.Values.ToList()))
                            .ProductImages;

                        //update the image
                        foreach (ProductImage image in gfsImages)
                        {
                            foreach (var v in requestItems)
                            {
                                if (v.Value.GTIN == image.GTIN
                                    && v.Value.ImageType == image.ImageType
                                    && v.Value.FacingType == image.FacingType)
                                {
                                    CachedPlanogramImage planogramImage;
                                    if (imagesByCacheId.TryGetValue(v.Key, out planogramImage))
                                    {
                                        //  Update the image data.
                                        planogramImage.UpdateImage(image.ProductImageData.ImageBlob.Data);
                                        imagesByCacheId.Remove(v.Key);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }

        }

        #region Helpers

        protected override String GetNameCriteria(Object imageTarget)
        {
            PlanogramProduct planogramProduct = imageTarget as PlanogramProduct;
            if (planogramProduct == null) return String.Empty;

            return planogramProduct.Gtin;
        }

        /// <summary>
        ///   Create a new description for the <see cref="PlanogramImage" /> based on the given <paramref name="criteria" />.
        /// </summary>
        /// <param name="criteria">
        ///     A <see cref="CachedPlanogramImageCriteria" /> instance containing the criteria to use when
        ///     creating the description.
        /// </param>
        /// <returns>A description created from the values in the <paramref name="criteria" />.</returns>
        protected override String GetPlanogramImageDescription(String imageSource, CachedPlanogramImageCriteria criteria)
        {
            return String.Format(Message.RealImageProvider_RepositoryImagenDescription, criteria.Attribute, criteria.ImageType,
                criteria.FacingType);
        }

        /// <summary>
        ///     Create a new file name for the <see cref="PlanogramImage" /> based on the given <paramref name="criteria" />.
        /// </summary>
        /// <param name="criteria">
        ///     A <see cref="CachedPlanogramImageCriteria" /> instance containing the criteria to use when
        ///     creating the file name.
        /// </param>
        /// <returns>A file name created from the values in the <paramref name="criteria" />.</returns>
        protected override String GetPlanogramImageFileName(CachedPlanogramImageCriteria criteria)
        {
            return criteria.GetCacheItemId();
        }

        /// <summary>
        ///     Gets the name for the image type, if it exists in GFS too.
        /// </summary>
        /// <param name="imageType>The criteria used to fetch the image.</param>
        /// <returns>The name for the image type if it exists in GFS, or null otherwise.</returns>
        private static String GetValidImageTypeNameForGfs(PlanogramImageType imageType)
        {
            //  NB: PlanogramImageType.None is ProductImageType.Unit.
            String imageTypeName = (imageType == PlanogramImageType.None) ? "Unit" : imageType.ToString();

            //  Check the Image Type will be valid for GFS.
            ProductImageType productImageType;
            Boolean isValidForGfs = Enum.TryParse(imageTypeName, true, out productImageType);
            return isValidForGfs ? imageTypeName : null;
        }

        #endregion


        #endregion

    }
}