#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History: CCM801

// V8-28676 : A.Silva
//      Created.

#endregion

#region Version History: CCM802

// V8-28952 : A.Silva
//      Changed DefaultTimeStampDuration from 15 to 1 (minutes before a missing image can be tried to be fetched again).
// V8-29039 : D.Pleasance
//      Reworked background thread so that priority can be set. Also amended so that WeakDelegate references are created and maintained better. 
//      Also added priority image logic.
// V8-29018 : D.Pleasance
//      Added ImageSourceDescription, this is because the image provider may be settings from the repository but ultimately uses folder implementation.

#endregion

#region Version History: CCM820
// V8-31365 : L.Ineson
//  Added the ability to bulk load images when the async load is running.
// V8-31380 : L.Ineson
//  Async fetch now turns itself on and off as required.
#endregion

#region Version History: CCM830 
// V8-32953 : L.Ineson
// Added RealImageWeakDelegate and the ability to pass a userstate.
// V8-32675 : L.Ineson
//  Amended FetchRealImagesBulkAsync to remove returned images from the priority queue so
//  it does not keep refetching them.
// CCM-18454 : L.Ineson
//  Made changes to how images are cached, prioritised and flagged as expired to speed things up.
//  Added PreloadPlanogram.
#endregion

#endregion

using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Reflection;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;

namespace Galleria.Ccm.Helpers
{
    /// <summary>
    /// Abstract base implementation of IRealImageProvider
    /// </summary>
    public abstract class RealImageProviderBase : IRealImageProvider
    {
        #region Nested Classes

        #region RealImageWeakDelegate
        /// <summary>
        /// Simple class to bring together a weak delegate and a userstate parameter.
        /// </summary>
        /// <remarks>This was added because anonymous expressions were unreliable with the weak delegate.</remarks>
        public sealed class RealImageWeakDelegate
        {
            #region Fields
            private WeakDelegate _weakDelegate;
            private Object _userState;
            #endregion

            public WeakDelegate WeakDelegate
            {
                get { return _weakDelegate; }
            }

            public Object UserState
            {
                get { return _userState; }
            }

            public Boolean IsAlive
            {
                get { return _weakDelegate.IsAlive; }
            }

            /// <summary>
            /// Constructor
            /// </summary>
            public RealImageWeakDelegate(Action<Object> callback, Object userState)
            {
                _weakDelegate = new Framework.Reflection.WeakDelegate(callback);
                _userState = userState;
            }

            public Object Invoke()
            {
                return _weakDelegate.Invoke(_userState);
            }
        }
        #endregion

        #endregion

        #region Fields

        public static readonly TimeSpan IsImageValidDuration = TimeSpan.FromMinutes(60);
        private Int32 _fetchBatchSize = 1;

        private readonly ConcurrentDictionary<String, CachedPlanogramImage> _imageCache = new ConcurrentDictionary<String, CachedPlanogramImage>();

        private readonly List<String> _priorityImages = new List<String>();
        private Object _priorityImagesLock = new Object(); // object used for locking the priority images

        private readonly Dictionary<String, List<RealImageWeakDelegate>> _weakDelegateByImageCacheId = new Dictionary<String, List<RealImageWeakDelegate>>();

        private Object _weakDelegateByImageCacheIdLock = new Object(); // object used for locking the weakDelegateByImageCacheId
        private Object _settingsLock = new Object(); // object used for locking the settings

        private ManualResetEvent _shutdownEvent;
        private Thread _realImageProviderThread;
        private Boolean _isPausedRealImageProviderThread = true;

        private IRealImageProviderSettings _settings;
        private String _imageSourceDescription;

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets how many images can be fetched at once.
        /// </summary>
        public Int32 FetchBatchSize
        {
            get { return _fetchBatchSize; }
            protected set { _fetchBatchSize = value; }
        }

        /// <summary>
        ///     The cache of already read files to avoid reading the same file over and over.
        /// </summary>
        protected ConcurrentDictionary<String, CachedPlanogramImage> ImageCache
        {
            get { return _imageCache; }
        }

        /// <summary>
        /// Get the dictionary of images and weak delegate references.
        /// </summary>
        protected Dictionary<String, List<RealImageWeakDelegate>> WeakDelegateByImageCacheId
        {
            get
            {
                lock (_weakDelegateByImageCacheIdLock)
                {
                    return _weakDelegateByImageCacheId;
                }
            }
        }

        protected Int32 EntityId { get; private set; }

        /// <summary>
        ///     Gets or sets the settings used to provide real images.
        /// </summary>
        protected IRealImageProviderSettings Settings
        {
            get
            {
                lock (_settingsLock)
                {
                    return _settings;
                }
            }
            private set { _settings = new RealImageProviderSettings(value); }
        }

        public String ImageSourceDescription
        {
            get { return _imageSourceDescription; }
        }


        /// <summary>
        /// Returns true if the image fetch is running.
        /// </summary>
        Boolean IRealImageProvider.IsRunning
        {
            get { return !_isPausedRealImageProviderThread; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="entityId"></param>
        protected RealImageProviderBase(IRealImageProviderSettings settings, Int32 entityId)
        {
            this.Settings = settings;
            _imageSourceDescription = RealImageProviderTypeHelper.FriendlyNames[settings.ProductImageSource];
            this.EntityId = entityId;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when the real image provider settings are changing.
        /// </summary>
        /// <param name="settings"></param>
        protected virtual void OnSettingsChanging(IRealImageProviderSettings settings)
        {
            //override as required.
        }

        #endregion

        #region Methods

        #region Start/Stop/Pause/Continue

        /// <summary>
        /// Kicks off the async image fetch
        /// </summary>
        private void StartFetchAsync()
        {
            //carry out pre fetch actions.


            //  Start the background task fetching images.
            if (_realImageProviderThread == null || !_realImageProviderThread.IsAlive)
            {
                _shutdownEvent = new ManualResetEvent(false);


                if (this.FetchBatchSize > 1)
                {
                    _realImageProviderThread = new Thread(new ThreadStart(FetchRealImagesBulkAsync));
                }
                else
                {
                    _realImageProviderThread = new Thread(new ThreadStart(FetchRealImageAsync));
                }
                _realImageProviderThread.Priority = ThreadPriority.Lowest;
                _realImageProviderThread.Name = "RealImageProviderThread";
                _realImageProviderThread.IsBackground = true;
                _realImageProviderThread.Start();
            }

            _isPausedRealImageProviderThread = false;
        }

        /// <summary>
        /// Pauses the async image fetch.
        /// </summary>
        public void PauseFetchAsync()
        {
            if (_isPausedRealImageProviderThread) return;

            _isPausedRealImageProviderThread = true;
        }

        /// <summary>
        /// Continues the async image fetch
        /// </summary>
        public void ContinueFetchAsync()
        {
            if (!_isPausedRealImageProviderThread) return;
            _isPausedRealImageProviderThread = false;
        }

        /// <summary>
        /// Stops the real image fetch asyncronously.
        /// </summary>
        public void StopFetchAsync()
        {
            if (_shutdownEvent != null && _realImageProviderThread != null)
            {
                if (_realImageProviderThread.IsAlive)
                {
                    ImageCache.Clear();
                    _shutdownEvent.Set();
                    _realImageProviderThread.Join(1000);
                }
                else
                {
                    _realImageProviderThread = null;
                }
            }
        }

        /// <summary>
        /// Updates the image settings.
        /// </summary>
        /// <param name="settings"></param>
        public void Reload(IRealImageProviderSettings settings)
        {
            Boolean requireRestart = !_isPausedRealImageProviderThread;
            PauseFetchAsync();

            //  Do any specific operations needed for the change (sub classes need to define those operations by examining the new settings.
            OnSettingsChanging(settings);

            //  Load the new settings.
            Settings = settings;
            _imageSourceDescription = RealImageProviderTypeHelper.FriendlyNames[settings.ProductImageSource];

            if (requireRestart)
            {
                StartFetchAsync();
            }
        }

        #endregion

        #region Image Fetch

        /// <summary>
        /// Gets an image using the configuration.
        /// </summary>
        /// <param name="imageTarget"></param>
        /// <param name="facingType"></param>
        /// <param name="imageType"></param>
        /// <param name="imageUpdateCallBack">Action which will accept the userstate parameter</param>
        /// <param name="userState">parameter value to pass when invoking the imageUpdateCallBack</param>
        public PlanogramImage GetImageAsync(Object imageTarget, PlanogramImageFacingType facingType,
            PlanogramImageType imageType, Action<Object> imageUpdateCallBack, Object userState)
        {
            //  Try to process the image target as a Planogram Product.
            PlanogramProduct planogramProduct = imageTarget as PlanogramProduct;
            if (planogramProduct == null) return null;

            //  Return the image.
            CachedPlanogramImageCriteria criteria = new CachedPlanogramImageCriteria(
                GetNameCriteria(imageTarget),
                imageType,
                facingType);

            CachedPlanogramImage match = GetOrAddToPending(criteria, imageUpdateCallBack, userState);

            return (match != null)? match.Image : null;

        }

        /// <summary>
        /// Gets the requested image synchronously.
        /// </summary>
        /// <param name="imageTarget"></param>
        /// <param name="facingType"></param>
        /// <param name="imageType"></param>
        /// <returns></returns>
        public PlanogramImage GetImage(Object imageTarget, PlanogramImageFacingType facingType, PlanogramImageType imageType)
        {
            //  Try to process the image target as a Planogram Product.
            PlanogramProduct planogramProduct = imageTarget as PlanogramProduct;
            if (planogramProduct == null) return null;

            //  Return the image.
            CachedPlanogramImageCriteria criteria = new CachedPlanogramImageCriteria(
                GetNameCriteria(imageTarget), imageType, facingType);


            String cacheItemId = criteria.GetCacheItemId();
            if (String.IsNullOrEmpty(cacheItemId) || String.IsNullOrEmpty(criteria.Attribute)) return PlanogramImage.NewPlanogramImage();

            //  Try to find a match.
            CachedPlanogramImage match;
            ImageCache.TryGetValue(cacheItemId, out match);
            if (match == null)
            {
                match = new CachedPlanogramImage(cacheItemId, criteria.ImageType, criteria.FacingType);
                match.Image.ImageData = null;
                match.Image.FileName = GetPlanogramImageFileName(criteria);
                match.Image.Description = GetPlanogramImageDescription(_imageSourceDescription, criteria);

                ImageCache[cacheItemId] = match;
            }

            if (match.IsExpired() || match.IsPending())
            {
                UpdateRealImage(match);
            }


            return match.Image;
        }

        /// <summary>
        ///     Attempts to retrieve the corresponding <see cref="PlanogramImage" /> from the cache, based on the given
        ///     <paramref name="criteria" />.
        /// </summary>
        /// <param name="criteria">
        ///     Contains the information needed to determine the corresponding <see cref="PlanogramImage" /> to
        ///     be retrieved.
        /// </param>
        /// <param name="imageUpdateCallBack"></param>
        /// <returns><c>True</c> if there was a match found in the cache, <c>false</c> otherwise.</returns>
        private CachedPlanogramImage GetOrAddToPending(CachedPlanogramImageCriteria criteria, Action<Object> action, Object userState)
        {
            //  Get the expected Id for the the given criteria.
            String cacheItemId = criteria.GetCacheItemId();
            if (String.IsNullOrEmpty(cacheItemId) ||
                String.IsNullOrEmpty(criteria.Attribute)) return null;

            //  Try to find a match.
            CachedPlanogramImage match;
            ImageCache.TryGetValue(cacheItemId, out match);
            if (match == null)
            {
                //  Determine the file name for the given context.
                String name = GetPlanogramImageFileName(criteria);

                match = new CachedPlanogramImage(cacheItemId, criteria.ImageType, criteria.FacingType);
                match.Image.ImageData = null;
                match.Image.FileName = name;
                match.Image.Description = GetPlanogramImageDescription(_imageSourceDescription, criteria);


                if (action == null)
                {
                    match.RefreshAndPrioritizeAsync();
                }

                if (ImageCache.ContainsKey(cacheItemId)) return match;


                ImageCache[cacheItemId] = match;

            }
            else if(match.IsExpired())
            {
                match.RefreshAsync();
            }


            if(action != null)
            {
                List<RealImageWeakDelegate> weakDelegates;
                this.WeakDelegateByImageCacheId.TryGetValue(cacheItemId, out weakDelegates);

                if (weakDelegates == null)
                {
                    weakDelegates = new List<RealImageWeakDelegate>();
                    this.WeakDelegateByImageCacheId.Add(cacheItemId, weakDelegates);
                }

                if (weakDelegates.All(p => !p.WeakDelegate.IsTarget(action.Target)))
                {
                    weakDelegates.Add(new RealImageWeakDelegate(action, userState));
                }
            }


            //Make sure the async fetch is running.
            if (match != null)
            {
                StartFetchAsync();
            }


            return match;
        }

        /// <summary>
        /// Fetches real images a single image at a time.
        /// </summary>
        private void FetchRealImageAsync()
        {
            while (true)
            {
                if (!_isPausedRealImageProviderThread)
                {
                    try
                    {
                        CachedPlanogramImage planogramImage = ImageCache.Values.FirstOrDefault(i => i.IsPriority);
                        if (planogramImage == null)
                        {
                            planogramImage =
                                ImageCache.Values
                                .Where(o => !o.IsPriority && o.IsPending())
                                .OrderBy(CachedPlanogramImage.GetImageFetchPriority)
                                .FirstOrDefault();
                        }

                        Int32 wait = 0;

                        if (planogramImage != null)
                        {
                            if (UpdateRealImage(planogramImage) != null)
                            {
                                List<RealImageWeakDelegate> weakDelegates;
                                this.WeakDelegateByImageCacheId.TryGetValue(planogramImage.Identifier, out weakDelegates);

                                if (weakDelegates != null)
                                {
                                    if (weakDelegates.Where(p => p.IsAlive).Any())
                                    {
                                        foreach (RealImageWeakDelegate weakDelegate in weakDelegates)
                                        {
                                            weakDelegate.Invoke();
                                        }
                                    }
                                    else
                                    {
                                        this.WeakDelegateByImageCacheId.Remove(planogramImage.Identifier);
                                    }
                                }
                            }

                        }
                        else
                        {
                            //start tidyig up old expired images.
                            ClearExpiredCacheItems();

                            //check if we still have nothing pending.
                            if (!ImageCache.Values.Any(i => i.IsPending()))
                            {
                                //stop processing as we have nothing left in the queue
                                break;
                            }
                            //wait = 1000;
                        }

                        if (_shutdownEvent.WaitOne(wait))
                        {
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
                else
                {
                    Thread.Sleep(1000);

                    try
                    {
                        List<String> cacheItemIds = this.WeakDelegateByImageCacheId.Keys.ToList();

                        foreach (String cacheItemId in cacheItemIds)
                        {
                            if (!_isPausedRealImageProviderThread) break;

                            List<RealImageWeakDelegate> weakDelegates;
                            this.WeakDelegateByImageCacheId.TryGetValue(cacheItemId, out weakDelegates);

                            if (weakDelegates != null && !weakDelegates.Any(p => p.IsAlive))
                            {
                                this.WeakDelegateByImageCacheId.Remove(cacheItemId);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
        }

        /// <summary>
        ///     Updates the real image in the <see cref="PlanogramImage"/> instance.
        /// </summary>
        /// <param name="planogramImage">The instance that should have its image data updated.</param>
        /// <returns>A <see cref="Byte"/> array with the image that was updated, if any.</returns>
        /// <remarks>Override in inherited classes for the correct implementation..</remarks>
        protected abstract Byte[] UpdateRealImage(CachedPlanogramImage planogramImage);

        /// <summary>
        /// Fetches real images as a bulk operation.
        /// </summary>
        private void FetchRealImagesBulkAsync()
        {
            while (true)
            {
                if (!_isPausedRealImageProviderThread)
                {
                    try
                    {
                        //get the next lot of ids to fetch.
                        Dictionary<String, CachedPlanogramImage> planImages =
                         //get from the priority list first.
                         ImageCache.Where(i => i.Value.IsPriority).Take(this.FetchBatchSize).ToDictionary(i => i.Key, i => i.Value);

                        if (planImages.Count < this.FetchBatchSize)
                        {
                            foreach (var entry in
                                ImageCache.Where(o => !o.Value.IsPriority && o.Value.IsPending())
                                .OrderBy(CachedPlanogramImage.GetImageFetchPriority)
                                .ToList())
                            {
                                planImages[entry.Key] = entry.Value;
                                if (planImages.Count >= this.FetchBatchSize) break;
                            }
                        }



                        Int32 wait = 0;

                        if (planImages.Any())
                        {

                            //fetch the images
                            UpdateRealImages(planImages.Values.ToList());


                            foreach (var entry in planImages)
                            {
                                if (entry.Value.Image.ImageData == null) continue;

                                List<RealImageWeakDelegate> weakDelegates;
                                this.WeakDelegateByImageCacheId.TryGetValue(entry.Key, out weakDelegates);

                                if (weakDelegates != null)
                                {
                                    if (weakDelegates.Any(p => p.IsAlive))
                                    {
                                        foreach (RealImageWeakDelegate weakDelegate in weakDelegates)
                                        {
                                            weakDelegate.Invoke();
                                        }
                                    }
                                    else
                                    {
                                        this.WeakDelegateByImageCacheId.Remove(entry.Key);
                                    }
                                }

                            }


                        }
                        else
                        {
                            //start tidyig up old expired images.
                            ClearExpiredCacheItems();

                            //check if we still have nothing pending.
                            if (!ImageCache.Values.Any(i => i.IsPending()))
                            {
                                //stop processing as we have nothing left in the queue
                                break;
                                //wait = 1000;
                            }
                        }


                        if (_shutdownEvent.WaitOne(wait))
                        {
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
                else
                {
                    Thread.Sleep(1000);

                    try
                    {
                        List<String> cacheItemIds = this.WeakDelegateByImageCacheId.Keys.ToList();

                        foreach (String cacheItemId in cacheItemIds)
                        {
                            if (!_isPausedRealImageProviderThread) break;

                            List<RealImageWeakDelegate> weakDelegates;
                            this.WeakDelegateByImageCacheId.TryGetValue(cacheItemId, out weakDelegates);

                            if (weakDelegates != null && !weakDelegates.Any(p => p.IsAlive))
                            {
                                this.WeakDelegateByImageCacheId.Remove(cacheItemId);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }
        }

        /// <summary>
        /// Updates the given list of real images as a bulk operation.
        /// </summary>
        /// <param name="imageList"></param>
        protected virtual void UpdateRealImages(List<CachedPlanogramImage> imageList)
        {
            //overide as required.
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Clear any old items from the image cache.
        /// </summary>
        protected void ClearExpiredCacheItems()
        {
            foreach (var entry in this.ImageCache.Where(i => i.Value.IsExpired()))
            {
                CachedPlanogramImage old;
                this.ImageCache.TryRemove(entry.Key, out old);
            }

        }

        /// <summary>
        /// Returns true if the image is in the cache and is valid.
        /// </summary>
        /// <param name="cachedId"></param>
        /// <returns></returns>
        protected Boolean IsInCacheAndIsValid(String cachedId)
        {
            CachedPlanogramImage cached = null;
            this.ImageCache.TryGetValue(cachedId, out cached);

            if (cached == null) return false;
            else if (cached.IsExpired()) return false;
            else return true;
        }

        protected abstract String GetNameCriteria(Object imageTarget);

        /// <summary>
        ///     Create a new description for the <see cref="PlanogramImage" /> based on the given <paramref name="criteria" />.
        /// </summary>
        /// <param name="criteria">
        ///     A <see cref="CachedPlanogramImageCriteria" /> instance containing the criteria to use when
        ///     creating the description.
        /// </param>
        /// <returns>A description created from the values in the <paramref name="criteria" />.</returns>
        protected abstract String GetPlanogramImageDescription(String imageSource, CachedPlanogramImageCriteria criteria);

        /// <summary>
        ///     Create a new file name for the <see cref="PlanogramImage" /> based on the given <paramref name="criteria" />.
        /// </summary>
        /// <param name="criteria">
        ///     A <see cref="CachedPlanogramImageCriteria" /> instance containing the criteria to use when
        ///     creating the file name.
        /// </param>
        /// <returns>A file name created from the values in the <paramref name="criteria" />.</returns>
        protected abstract String GetPlanogramImageFileName(CachedPlanogramImageCriteria criteria);

        #region Preload Planogram


        /// <summary>
        /// Preloads any images required by the given planogram into the cache.
        /// This is used by the print process for speed.
        /// </summary>
        /// <param name="plan"></param>
        public void PreloadPlanogram(Planogram plan, Boolean commonFacesOnly)
        {
            List<CachedPlanogramImage> newItems = new List<CachedPlanogramImage>();

            foreach (PlanogramPosition position in plan.Positions)
            {
                PlanogramProduct product = position.GetPlanogramProduct();

                List<PlanogramProductMerchandisingStyle> merchStyles = new List<PlanogramProductMerchandisingStyle>();
                merchStyles.Add(position.GetAppliedMerchandisingStyle(PlanogramPositionBlockType.Main));
                if (position.IsBlockActive(PlanogramPositionBlockType.X)) merchStyles.Add(position.GetAppliedMerchandisingStyle(PlanogramPositionBlockType.X));
                if (position.IsBlockActive(PlanogramPositionBlockType.Y)) merchStyles.Add(position.GetAppliedMerchandisingStyle(PlanogramPositionBlockType.Y));
                if (position.IsBlockActive(PlanogramPositionBlockType.Z)) merchStyles.Add(position.GetAppliedMerchandisingStyle(PlanogramPositionBlockType.Z));
                merchStyles.Add(PlanogramProductMerchandisingStyle.Unit); //always add unit as a fallback.

                PlanogramProductFaceType[] faces =
                    (commonFacesOnly) ? new PlanogramProductFaceType[] { PlanogramProductFaceType.Front, PlanogramProductFaceType.Top, PlanogramProductFaceType.Right }
                    : new PlanogramProductFaceType[] {
                        PlanogramProductFaceType.Front, PlanogramProductFaceType.Top, PlanogramProductFaceType.Right,
                        PlanogramProductFaceType.Left, PlanogramProductFaceType.Bottom, PlanogramProductFaceType.Back };

                foreach (PlanogramProductMerchandisingStyle merchStyle in merchStyles.Distinct())
                {
                    foreach (PlanogramProductFaceType faceType in faces)
                    {
                        CachedPlanogramImage item = GetMerchStyleImageMissingCacheItem(product, merchStyle, faceType);
                        if (item == null || newItems.Contains(item)) continue;
                        newItems.Add(item);
                    }
                }
            }

            //now that all missing items for the plan have been added as pending to the cache,
            //bulk update them.
            UpdateRealImages(newItems);
        }

        private CachedPlanogramImage GetMerchStyleImageMissingCacheItem(
            PlanogramProduct product, PlanogramProductMerchandisingStyle merchStyle, PlanogramProductFaceType faceType)
        {
            CachedPlanogramImage img = null;
            PlanogramImageType imageType = PlanogramImageType.None;

            Planogram plan = product.Parent;
            if (plan == null) return null;

            switch (merchStyle)
            {
                case PlanogramProductMerchandisingStyle.Unit:
                    imageType = PlanogramImageType.None;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: img = GetMissingImageCacheItem(product, product.PlanogramImageIdFront, PlanogramImageFacingType.Front, imageType); break;
                        case PlanogramProductFaceType.Back: img = GetMissingImageCacheItem(product, product.PlanogramImageIdBack, PlanogramImageFacingType.Back, imageType); break;
                        case PlanogramProductFaceType.Top: img = GetMissingImageCacheItem(product, product.PlanogramImageIdTop, PlanogramImageFacingType.Top, imageType); break;
                        case PlanogramProductFaceType.Bottom: img = GetMissingImageCacheItem(product, product.PlanogramImageIdBottom, PlanogramImageFacingType.Bottom, imageType); break;
                        case PlanogramProductFaceType.Left: img = GetMissingImageCacheItem(product, product.PlanogramImageIdLeft, PlanogramImageFacingType.Left, imageType); break;
                        case PlanogramProductFaceType.Right: img = GetMissingImageCacheItem(product, product.PlanogramImageIdRight, PlanogramImageFacingType.Right, imageType); break;
                    }
                    break;

                case PlanogramProductMerchandisingStyle.Display:
                    imageType = PlanogramImageType.Display;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: img = GetMissingImageCacheItem(product, product.PlanogramImageIdDisplayFront, PlanogramImageFacingType.Front, imageType); break;
                        case PlanogramProductFaceType.Back: img = GetMissingImageCacheItem(product, product.PlanogramImageIdDisplayBack, PlanogramImageFacingType.Back, imageType); break;
                        case PlanogramProductFaceType.Top: img = GetMissingImageCacheItem(product, product.PlanogramImageIdDisplayTop, PlanogramImageFacingType.Top, imageType); break;
                        case PlanogramProductFaceType.Bottom: img = GetMissingImageCacheItem(product, product.PlanogramImageIdDisplayBottom, PlanogramImageFacingType.Bottom, imageType); break;
                        case PlanogramProductFaceType.Left: img = GetMissingImageCacheItem(product, product.PlanogramImageIdDisplayLeft, PlanogramImageFacingType.Left, imageType); break;
                        case PlanogramProductFaceType.Right: img = GetMissingImageCacheItem(product, product.PlanogramImageIdDisplayRight, PlanogramImageFacingType.Right, imageType); break;
                    }
                    break;

                case PlanogramProductMerchandisingStyle.Tray:
                    imageType = PlanogramImageType.Tray;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: img = GetMissingImageCacheItem(product, product.PlanogramImageIdTrayFront, PlanogramImageFacingType.Front, imageType); break;
                        case PlanogramProductFaceType.Back: img = GetMissingImageCacheItem(product, product.PlanogramImageIdTrayBack, PlanogramImageFacingType.Back, imageType); break;
                        case PlanogramProductFaceType.Top: img = GetMissingImageCacheItem(product, product.PlanogramImageIdTrayTop, PlanogramImageFacingType.Top, imageType); break;
                        case PlanogramProductFaceType.Bottom: img = GetMissingImageCacheItem(product, product.PlanogramImageIdTrayBottom, PlanogramImageFacingType.Bottom, imageType); break;
                        case PlanogramProductFaceType.Left: img = GetMissingImageCacheItem(product, product.PlanogramImageIdTrayLeft, PlanogramImageFacingType.Left, imageType); break;
                        case PlanogramProductFaceType.Right: img = GetMissingImageCacheItem(product, product.PlanogramImageIdTrayRight, PlanogramImageFacingType.Right, imageType); break;
                    }
                    break;

                case PlanogramProductMerchandisingStyle.PointOfPurchase:
                    imageType = PlanogramImageType.PointOfPurchase;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: img = GetMissingImageCacheItem(product, product.PlanogramImageIdPointOfPurchaseFront, PlanogramImageFacingType.Front, imageType); break;
                        case PlanogramProductFaceType.Back: img = GetMissingImageCacheItem(product, product.PlanogramImageIdPointOfPurchaseBack, PlanogramImageFacingType.Back, imageType); break;
                        case PlanogramProductFaceType.Top: img = GetMissingImageCacheItem(product, product.PlanogramImageIdPointOfPurchaseTop, PlanogramImageFacingType.Top, imageType); break;
                        case PlanogramProductFaceType.Bottom: img = GetMissingImageCacheItem(product, product.PlanogramImageIdPointOfPurchaseBottom, PlanogramImageFacingType.Bottom, imageType); break;
                        case PlanogramProductFaceType.Left: img = GetMissingImageCacheItem(product, product.PlanogramImageIdPointOfPurchaseLeft, PlanogramImageFacingType.Left, imageType); break;
                        case PlanogramProductFaceType.Right: img = GetMissingImageCacheItem(product, product.PlanogramImageIdPointOfPurchaseRight, PlanogramImageFacingType.Right, imageType); break;
                    }
                    break;

                case PlanogramProductMerchandisingStyle.Alternate:
                    imageType = PlanogramImageType.Alternate;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: img = GetMissingImageCacheItem(product, product.PlanogramImageIdAlternateFront, PlanogramImageFacingType.Front, imageType); break;
                        case PlanogramProductFaceType.Back: img = GetMissingImageCacheItem(product, product.PlanogramImageIdAlternateBack, PlanogramImageFacingType.Back, imageType); break;
                        case PlanogramProductFaceType.Top: img = GetMissingImageCacheItem(product, product.PlanogramImageIdAlternateTop, PlanogramImageFacingType.Top, imageType); break;
                        case PlanogramProductFaceType.Bottom: img = GetMissingImageCacheItem(product, product.PlanogramImageIdAlternateBottom, PlanogramImageFacingType.Bottom, imageType); break;
                        case PlanogramProductFaceType.Left: img = GetMissingImageCacheItem(product, product.PlanogramImageIdAlternateLeft, PlanogramImageFacingType.Left, imageType); break;
                        case PlanogramProductFaceType.Right: img = GetMissingImageCacheItem(product, product.PlanogramImageIdAlternateRight, PlanogramImageFacingType.Right, imageType); break;
                    }
                    break;

                case PlanogramProductMerchandisingStyle.Case:
                    imageType = PlanogramImageType.Case;
                    switch (faceType)
                    {
                        case PlanogramProductFaceType.Front: img = GetMissingImageCacheItem(product, product.PlanogramImageIdCaseFront, PlanogramImageFacingType.Front, imageType); break;
                        case PlanogramProductFaceType.Back: img = GetMissingImageCacheItem(product, product.PlanogramImageIdCaseBack, PlanogramImageFacingType.Back, imageType); break;
                        case PlanogramProductFaceType.Top: img = GetMissingImageCacheItem(product, product.PlanogramImageIdCaseTop, PlanogramImageFacingType.Top, imageType); break;
                        case PlanogramProductFaceType.Bottom: img = GetMissingImageCacheItem(product, product.PlanogramImageIdCaseBottom, PlanogramImageFacingType.Bottom, imageType); break;
                        case PlanogramProductFaceType.Left: img = GetMissingImageCacheItem(product, product.PlanogramImageIdCaseLeft, PlanogramImageFacingType.Left, imageType); break;
                        case PlanogramProductFaceType.Right: img = GetMissingImageCacheItem(product, product.PlanogramImageIdCaseRight, PlanogramImageFacingType.Right, imageType); break;
                    }
                    break;
            }

            // return the image
            return img;
        }

        private CachedPlanogramImage GetMissingImageCacheItem(PlanogramProduct imageTarget, Object imageId, PlanogramImageFacingType facingType, PlanogramImageType imageType)
        {
            //if we have an image id then the item does not need to go into the cache
            if (imageId != null) return null;

            CachedPlanogramImageCriteria criteria = new CachedPlanogramImageCriteria(GetNameCriteria(imageTarget), imageType, facingType);


            String cacheItemId = criteria.GetCacheItemId();
            if (String.IsNullOrEmpty(cacheItemId) || String.IsNullOrEmpty(criteria.Attribute)) return null;

            //  Try to find a match.
            CachedPlanogramImage match;
            ImageCache.TryGetValue(cacheItemId, out match);
            if (match == null)
            {
                match = new CachedPlanogramImage(cacheItemId, criteria.ImageType, criteria.FacingType);
                match.Image.ImageData = null;
                match.Image.FileName = GetPlanogramImageFileName(criteria);
                match.Image.Description = GetPlanogramImageDescription(_imageSourceDescription, criteria);

                ImageCache[cacheItemId] = match;
                return match;
            }
            else if (match.IsExpired() || match.IsPending())
            {
                match.RefreshAsync();
                return match;
            }
            return null;
        }

        #endregion

        #endregion

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        protected Boolean IsDisposed
        {
            get { return _isDisposed; }
            private set { _isDisposed = value; }
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public virtual void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean isDisposing)
        {
            if (IsDisposed) return;

            if (isDisposing)
            {
                StopFetchAsync();
                ImageCache.Clear();
            }
            IsDisposed = true;
        }

        #endregion


        
    }

    #region CachedPlanogramImageCriteria

    public sealed class CachedPlanogramImageCriteria
    {
        #region Constants

        //private const String AttributevalueFacingtypeReplacePattern = @"${attributeValue}.${facingType}";

        private const String CacheItemIdPattern = "{0}_{1}.{2}";
        private const String AttributeValueCaptureGroup = "attributeValue";
        private const String FacingTypeCaptureGroup = "facingType";
        private const String ImageTypeCaptureGroup = "imageType";

        #endregion

        #region Properties

        public String Attribute { get; private set; }
        public PlanogramImageType ImageType { get; private set; }
        public PlanogramImageFacingType FacingType { get; private set; }

        #endregion

        #region Constructor

        public CachedPlanogramImageCriteria(String attribute, PlanogramImageType imageType,
            PlanogramImageFacingType facingType)
        {
            Attribute = attribute;
            ImageType = imageType;
            FacingType = facingType;
        }

        public CachedPlanogramImageCriteria(String cacheItemId)
        {
            Attribute = AttributeSubstring(cacheItemId);
            ImageType = EnumHelper.Parse(ImageTypeSubstring(cacheItemId),
                PlanogramImageType.None);
            FacingType = EnumHelper.Parse(FacingTypeSubstring(cacheItemId),
                PlanogramImageFacingType.Front);
        }

        #endregion

        #region Methods

        public String GetCacheItemId()
        {
            return String.Format(CacheItemIdPattern, Attribute, (Int32)ImageType, (Int32)FacingType);
        }

        #endregion

        #region Static Helper Methods

        /// <summary>
        ///     Gets the regular expression for the pattern followed when constructing a cache item id.
        /// </summary>
        /// <returns>A <see cref="String"/> with the pattern's regular expression.</returns>
        public static String GetCacheItemIdPattern()
        {
            String attributeValueRegExGroup = String.Format(@"^(?<{0}>.+)", AttributeValueCaptureGroup);
            String imageTypeRegExGroup = String.Format(@"(?<{0}>.+)\", ImageTypeCaptureGroup);
            String facingTypeRegExGroup = String.Format(@"(?<{0}>.+)$", FacingTypeCaptureGroup);
            return String.Format(CacheItemIdPattern, attributeValueRegExGroup, imageTypeRegExGroup, facingTypeRegExGroup);
        }

        public static String ExtractFacingImageTypes(String cacheItemId)
        {
            return String.Format("{0}{1}", FacingTypeSubstring(cacheItemId), ImageTypeSubstring(cacheItemId));
        }

        public static String ExtractFacingImageTypes(KeyValuePair<String, CachedPlanogramImage> entry)
        {
            return ExtractFacingImageTypes(entry.Key);
        }
        public static String ExtractFacingImageTypes(CachedPlanogramImage entry)
        {
            return ExtractFacingImageTypes(entry.Identifier);
        }

        private static String AttributeSubstring(String cacheItemId)
        {
            return Regex.Match(cacheItemId, GetCacheItemIdPattern()).Groups[AttributeValueCaptureGroup].Value;
        }

        private static String FacingTypeSubstring(String cacheItemId)
        {
            return Regex.Match(cacheItemId, GetCacheItemIdPattern()).Groups[FacingTypeCaptureGroup].Value;
        }

        private static String ImageTypeSubstring(String cacheItemId)
        {
            return Regex.Match(cacheItemId, GetCacheItemIdPattern()).Groups[ImageTypeCaptureGroup].Value;
        }

        #endregion
    }

    #endregion

    #region CachedPlanogramImage

    /// <summary>
    /// Represents a single image in the image cache.
    /// </summary>
    public sealed class CachedPlanogramImage
    {
        #region Fields
        private DateTime? _timeStamp = null; //the timestamp for when this was last updated.
        #endregion

        #region Properties

        /// <summary>
        /// Gets the image identifier.
        /// </summary>
        public String Identifier { get; private set; }


        /// <summary>
        /// Gets the image model.
        /// </summary>
        public PlanogramImage Image { get; private set; }


        /// <summary>
        /// Flags this as a priority
        /// </summary>
        public Boolean IsPriority { get; private set; }

        /// <summary>
        /// The type of image this represents
        /// </summary>
        public PlanogramImageType ImageType { get; set; }

        /// <summary>
        /// The type of facing this represents.
        /// </summary>
        public PlanogramImageFacingType FacingType { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public CachedPlanogramImage(String identifier, PlanogramImageType imageType, PlanogramImageFacingType facingType)
        {
            IsPriority = false;
            Identifier = identifier;
            Image = PlanogramImage.NewPlanogramImage();
            _timeStamp = null; //set as pending.

            this.ImageType = imageType;
            this.FacingType = facingType;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="image"></param>
        public CachedPlanogramImage(String identifier, PlanogramImage image, PlanogramImageType imageType, PlanogramImageFacingType facingType)
        {
            IsPriority = false;
            Identifier = identifier;
            Image = image;
            _timeStamp = DateTime.Now;

            this.ImageType = imageType;
            this.FacingType = facingType;
        }

        #endregion

        #region Methods                   

        /// <summary>
        /// Updates the image with the data given.
        /// </summary>
        /// <param name="imageData"></param>
        public void UpdateImage(Byte[] imageData)
        {
            Image.ImageData = imageData;
            _timeStamp = DateTime.Now;
            IsPriority = false;
        }

        /// <summary>
        /// Updates the image with the data given.
        /// </summary>
        /// <param name="imageData"></param>
        public void UpdateImage(Byte[] imageData, String description)
        {
            Image.ImageData = imageData;
            Image.Description = description;
            _timeStamp = DateTime.Now;
            IsPriority = false;
        }

        /// <summary>
        /// Returns true if this cached image is no longer valid.
        /// </summary>
        /// <returns></returns>
        public Boolean IsExpired()
        {
            if (IsPending()) return false;

            TimeSpan elapsedTime = (DateTime.Now - _timeStamp.Value).Duration();
            return elapsedTime > RealImageProviderBase.IsImageValidDuration;
        }

        /// <summary>
        /// Returns true if the image is pending.
        /// </summary>
        /// <returns></returns>
        public Boolean IsPending()
        {
            return !_timeStamp.HasValue;
        }

        /// <summary>
        /// Sets this back to pending.
        /// </summary>
        public void RefreshAsync()
        {
            _timeStamp = null;
        }

        /// <summary>
        /// Sets this back to pending.
        /// </summary>
        public void RefreshAndPrioritizeAsync()
        {
            IsPriority = true;
            _timeStamp = null;
        }

        /// <summary>
        /// Returns true if the image was checked recently but was not found
        /// </summary>
        public Boolean IsImageConfirmedMissing()
        {
            if (this.IsExpired() || this.IsPending()) return false;
            else return true;
        }

        public static Int32 GetImageFetchPriority(KeyValuePair<String, CachedPlanogramImage> entry)
        {
            return GetImageFetchPriority(entry.Value);
        }

        public static Int32 GetImageFetchPriority(CachedPlanogramImage img)
        {
            Int32 imgTypeOrder = 50;
            switch (img.ImageType)
            {
                case PlanogramImageType.Tray:
                    imgTypeOrder = 10;
                    break;

                case PlanogramImageType.None:
                    imgTypeOrder = 20;
                    break;
            }

            Int32 imgFacingOrder = 7;
            switch (img.FacingType)
            {
                case PlanogramImageFacingType.Front:
                    imgFacingOrder = 0;
                    break;

                case PlanogramImageFacingType.Top:
                    imgFacingOrder = 1;
                    break;

                case PlanogramImageFacingType.Right:
                    imgFacingOrder = 2;
                    break;
            }

            return imgTypeOrder + imgFacingOrder;
        }

        #endregion

    }

    #endregion
}