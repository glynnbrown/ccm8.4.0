﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using System.Reflection;

namespace Galleria.Ccm.Helpers
{
    /// <summary>
    /// Mapping helper class
    /// </summary>
    public static class MappingHelper
    {
        #region Fields
        private static bool _initialized = false; // indicates if the helper has initialized all mappings
        private static object _initializeLock = new object(); // object used to lock while initializing
        #endregion

        #region Methods
        /// <summary>
        /// Initializes all mappings within the framework
        /// </summary>
        public static void InitializeMappings()
        {
            if (!_initialized)
            {
                lock (_initializeLock)
                {
                    if (!_initialized)
                    {
                        // set the iniialized flag now to prevent re-entry
                        _initialized = true;

                        // get the current assembly
                        Assembly assembly = Assembly.GetExecutingAssembly();
                        foreach (Type type in assembly.GetTypes())
                        {
                            if (type.Namespace == "Galleria.Ccm.Model")
                            {
                                MethodInfo method = type.GetMethod("CreateMappings", BindingFlags.Static | BindingFlags.NonPublic);
                                if (method != null)
                                {
                                    method.Invoke(null, null);
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion
    }
}
