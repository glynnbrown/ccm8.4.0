﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// V8-27783 : M.Brumby
//  Created.
// V8-28298 : M.Brumby
//  Made use of blocking to get blocking colour for positions.
// V8-28427 : M.Brumby
//  Publish sequence numbers.
// V8-28665 : N.Foster
//  Resolved issues where wrong call to GetPositionDetails() was being made
#endregion

#region Version History: (CCM 8.10)
//  V8-29775 : M.Brumby
//      Removed meta data calc
//  V8-29806 : M.Brumby
//      Set location code now it is available
//  V8-28672 : M.Shelley
//      Update the GFS publish process to use the blocking colours defined in 
//      the PlanogramProduct list data (in the ConvertToPlanogramDc method)
//  V8-30006 : D.Pleasance
//      Sub component fill pattern now defaulted to solid.
//  V8-30129 : M.Brumby
//      Updated valid name check to include trimming the name to account for
//      single space names.
//  V8-30165 : M.Brumby
//      Corrected total white space % and added total product linear space.
#endregion

#region Version History: (CCM 8.11)
// V8-30345 : L.Ineson
//  Merch groups are no longer created just to get planogram product blocking colour.
// V8-30576 : N.Haywood
//  changed annotation positions to reflect depth for current gfs structure
#endregion

#region Version History: (CCM v8.2.0)
// V8-30743 : N.Haywood
//  Added cluster scheme name and cluster name to planogram publish
// V8-30967 : N.Haywood
//  Published up bay/element sequence numbers
// V8-31064 : N.Haywood
//  published up notch numbers
// V8-31123 : L.Ineson
//  Position sequence number is now populated by the correct property.
// V8-31164 : D.Pleasance
//  Amended so that blocking colour now obtained from Position.SequenceColour
// V8-31233 : A.Probyn
//  Updated to publish more properties from the PlanogramProduct and PlanogramPosition structure to GFS.
// V8-31599 : N.Haywood
//  set IsMerchendisable for missing merchandisable element types 
#endregion

#region Version History: CCM830
//  V8-31531 : A.Heathcote 
//      Removed IsTrayProduct
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-31802 : N.Haywood
//  Changed gfs position total units to use positiondetail.totalunitcount
// V8-31802 : M.Brumby
//  Changed gfs position total units to use a total unit calculation basted off the positiondetail.totalunitcount calculation
//  rather than positiondetail.totalunitcount as the positions can get chopped up all over the show. 
// V8-31904 : N.Haywood
//  Added squeeze dimensions based off the meta calculated position dimensions
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
// V8-32301 : N.Haywood
//  Fixed fix for squeeze dimensions
// V8-32523 : L.ineson
//  Updated annotation dc create to support component level annotations.
// V8-32569 : N.Haywood
//  Updated annotations to set y position as annotation linking isn't handled by GFS
// V8-32729 : L.Ineson
//  Added PlanogramProduct.PegProngOffsetY
// V8-32764 : M.Brumby
//  CCM Product orientations are now correctly converted to the more limited set of GFS product orientations when publishing.
// V8-32835 : N.Haywood
//  Fixed publishing of tray unit counts and combining caps
//  Populated tray size dimensions to use units if they're not set
// V8-32887 : N.Haywood
//  Fixed publishing of cases where cases have no size dimensions
// CCM-13760 : M.Brumby
//  Handled an issue were positions on a new element will think they are on an assembly instead of a component.
#endregion

#region Version History: CCM840
// GS230-192 : J.Mendes 
// Added GFS Custom Attribute Data Table to receive data from the CCM Custom Attribute Data table
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using GFS = Galleria.Ccm.Services.Planogram;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Helpers
{
    /// <summary>    
    /// Converts Ccm plan data to the Gfs planogram structure
    /// </summary>
    public sealed class CcmToGfsPlanogramConverter
    {

        #region Constructor
        private CcmToGfsPlanogramConverter() { }
        #endregion

        /// <summary>
        /// Converts the ccm planogram refereneced ina  location plan assignment into the GFS structure
        /// </summary>
        /// <param name="ccmPlanogram"></param>
        /// <returns></returns>
        public static GFS.Planogram ConvertToPlanogramDc(Int32 planogramId)
        {
            Planogram ccmPlanogram = Galleria.Framework.Planograms.Model.Package.FetchById(planogramId).Planograms.First();
            return ConvertToPlanogramDc(ccmPlanogram);
        }

        /// <summary>
        /// Converts a ccm planogram into the GFS structure
        /// </summary>
        /// <param name="ccmPlanogram"></param>
        /// <returns></returns>
        public static GFS.Planogram ConvertToPlanogramDc(Planogram ccmPlanogram)
        {
            GFS.Planogram output = new GFS.Planogram();
            RectValue? planogramBounds = null;
            output.UserName = ccmPlanogram.UserName;
            output.UniqueContentReference = ccmPlanogram.UniqueContentReference;
            output.Name = ValidName(ccmPlanogram.Name); //TODO Make into Unique plan name??!?!
            output.LocationCode = String.IsNullOrEmpty(ccmPlanogram.LocationCode) ? null : ccmPlanogram.LocationCode; //set to null if there is no code else we get a no location match exception
            output.FixtureItems = new List<GFS.PlanogramFixtureItem>();
            output.Fixtures = new List<GFS.PlanogramFixture>();
            output.Components = new List<GFS.PlanogramComponent>();
            output.Assemblies = new List<GFS.PlanogramAssembly>();
            output.Positions = new List<GFS.PlanogramPosition>();
            output.Annotations = new List<GFS.PlanogramAnnotation>();
            output.Products = new List<GFS.PlanogramProduct>();
            output.ClusterSchemeName = ccmPlanogram.ClusterSchemeName;
            output.ClusterName = ccmPlanogram.ClusterName;
            
            #region FixtureItems
            //ccmPlanogram.GetPlanogramFixtureItem
            
            foreach (PlanogramFixtureItem ccmFixtureItem in ccmPlanogram.FixtureItems)
            {
                GFS.PlanogramFixtureItem gfsFixtureItem = new GFS.PlanogramFixtureItem();
                gfsFixtureItem.Id = System.Convert.ToInt32(ccmFixtureItem.Id);
                gfsFixtureItem.IsBay = true; //they are all bays      
                gfsFixtureItem.X = ccmFixtureItem.X;
                gfsFixtureItem.Y = ccmFixtureItem.Y;
                gfsFixtureItem.Z = ccmFixtureItem.Z;
                gfsFixtureItem.Slope = ccmFixtureItem.Slope;
                gfsFixtureItem.Angle = ccmFixtureItem.Angle;
                gfsFixtureItem.Roll = ccmFixtureItem.Roll;
                gfsFixtureItem.PlanogramFixtureId = System.Convert.ToInt32(ccmFixtureItem.PlanogramFixtureId);
                gfsFixtureItem.BaySequenceNumber = ccmFixtureItem.BaySequenceNumber;

                RectValue currentBound = ccmFixtureItem.GetPlanogramRelativeBoundingBox(ccmFixtureItem.GetPlanogramFixture());
                if (planogramBounds.HasValue)
                {
                    planogramBounds = planogramBounds.Value.Union(currentBound);
                }
                else
                {
                    planogramBounds = currentBound;
                }
                output.FixtureItems.Add(gfsFixtureItem);
            }
            #endregion FixtureItems

            #region Fixtures
            //note dummy Ids move in the negative direction so they don't clash with any real Ids used. These
            //Ids will only be used in GFS as temporary Ids and will be overwritten when saved.
            Int32 dummyFixtureAssemblyId = -1;
            Dictionary<Object, Int32> fixtureComponentToDummyFixtureAssemblyIds = new Dictionary<Object, Int32>();
            Int32 dummyAssemblyId = -1;
            PlanogramFixtureItem sizingFixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem(); //dummy item used for sizing purposes.
            foreach (PlanogramFixture ccmFixture in ccmPlanogram.Fixtures)
            {
                GFS.PlanogramFixture gfsFixture = new GFS.PlanogramFixture();
                gfsFixture.Assemblies = new List<GFS.PlanogramFixtureAssembly>();

                gfsFixture.Id = System.Convert.ToInt32(ccmFixture.Id);
                gfsFixture.Name = ValidName(ccmFixture.Name);
                gfsFixture.Height = ccmFixture.Height;
                gfsFixture.Width = ccmFixture.Width;
                gfsFixture.Depth = ccmFixture.Depth;

                gfsFixture.UniqueProductCount = 0; //TODO (not used in GFS yet?)
                if (ccmFixture.MetaTotalFixtureCost.HasValue) gfsFixture.TotalFixtureCost = ccmFixture.MetaTotalFixtureCost.Value;
                if(ccmFixture.MetaNumberOfMerchandisedSubComponents.HasValue) gfsFixture.NumberOfMerchandisedSubComponents = ccmFixture.MetaNumberOfMerchandisedSubComponents.Value;
                gfsFixture.NumberOfAssemblies = Convert.ToInt16(ccmFixture.Assemblies.Count);
                
                #region DummyFixtureAssembly

                if (ccmFixture.Components.Count > 0)
                {
                    GFS.PlanogramFixtureAssembly gfsDummyFixtureAssembly = new GFS.PlanogramFixtureAssembly();

                    #region Assign properties
                    gfsDummyFixtureAssembly.Id = dummyFixtureAssemblyId;
                    gfsDummyFixtureAssembly.PlanogramAssemblyId = dummyAssemblyId;
                    gfsDummyFixtureAssembly.Angle = 0;
                    gfsDummyFixtureAssembly.Roll = 0;
                    gfsDummyFixtureAssembly.Slope = 0;
                    gfsDummyFixtureAssembly.X = 0;
                    gfsDummyFixtureAssembly.Y = 0;
                    gfsDummyFixtureAssembly.Z = 0;
                    gfsDummyFixtureAssembly.NotchNumber = null; //Gets calculated when fetching the DC

                    GFS.PlanogramAssembly gfsAssembly = new GFS.PlanogramAssembly();

                    gfsAssembly.Id = dummyAssemblyId;
                    gfsAssembly.Name = ValidName("Dummy");
                    gfsAssembly.TotalComponentCost = 0;
                    gfsAssembly.Components = new List<GFS.PlanogramAssemblyComponent>();

                    RectValue? assemeblyBounds = null;

                    foreach (PlanogramFixtureComponent ccmFixtureComponent in ccmFixture.Components)
                    {
                        GFS.PlanogramAssemblyComponent gfsAssemblyComponent = new GFS.PlanogramAssemblyComponent();
                        gfsAssemblyComponent.Id = System.Convert.ToInt32(ccmFixtureComponent.Id);
                        gfsAssemblyComponent.X = ccmFixtureComponent.X;
                        gfsAssemblyComponent.Y = ccmFixtureComponent.Y;
                        gfsAssemblyComponent.Z = ccmFixtureComponent.Z;
                        gfsAssemblyComponent.Slope = -ccmFixtureComponent.Slope;
                        gfsAssemblyComponent.Angle = -ccmFixtureComponent.Angle;
                        gfsAssemblyComponent.Roll = -ccmFixtureComponent.Roll;
                        gfsAssemblyComponent.ElementSequenceNumber = ccmFixtureComponent.ComponentSequenceNumber;
                        gfsAssemblyComponent.NotchNumber = ccmFixtureComponent.NotchNumber;
                        PlanogramComponent currentComponent = ccmFixtureComponent.GetPlanogramComponent();

                        //Keep track of our bounding area so we can set the assembly size later.
                        RectValue currentBound = ccmFixtureComponent.GetPlanogramRelativeBoundingBox(currentComponent, sizingFixtureItem);
                        if (assemeblyBounds.HasValue)
                        {
                            assemeblyBounds = assemeblyBounds.Value.Union(currentBound);
                        }
                        else
                        {
                            assemeblyBounds = currentBound;
                        }

                        //Add the cost on if we have the data
                        if (currentComponent.SupplierCostPrice.HasValue)
                        {
                            gfsAssembly.TotalComponentCost += currentComponent.SupplierCostPrice.Value;
                        }

                        gfsAssemblyComponent.PlanogramComponentId = System.Convert.ToInt32(ccmFixtureComponent.PlanogramComponentId);
                        gfsAssembly.Components.Add(gfsAssemblyComponent);

                        fixtureComponentToDummyFixtureAssemblyIds.Add(ccmFixtureComponent.Id, dummyFixtureAssemblyId);
                    }
                    gfsAssembly.Height = assemeblyBounds.Value.Height;
                    gfsAssembly.Width = assemeblyBounds.Value.Width;
                    gfsAssembly.Depth = assemeblyBounds.Value.Depth;

                    gfsFixture.Assemblies.Add(gfsDummyFixtureAssembly);
                    gfsFixture.NumberOfAssemblies++;
                    dummyFixtureAssemblyId--;
                    output.Assemblies.Add(gfsAssembly);                    
                    dummyAssemblyId--;
                    #endregion
                }
                #endregion

                foreach(PlanogramFixtureAssembly ccmAssembly in ccmFixture.Assemblies)
                {
                    GFS.PlanogramFixtureAssembly gfsFixtureAssembly = new GFS.PlanogramFixtureAssembly();

                    #region Assign properties
                    gfsFixtureAssembly.Id = System.Convert.ToInt32(ccmAssembly.Id);
                    //gfsFixtureAssembly.IsBay = true; //they are all bays                                                     
                    gfsFixtureAssembly.PlanogramAssemblyId = System.Convert.ToInt32(ccmAssembly.PlanogramAssemblyId);
                    gfsFixtureAssembly.Angle = ccmAssembly.Angle;
                    gfsFixtureAssembly.Roll = ccmAssembly.Roll;
                    gfsFixtureAssembly.Slope = ccmAssembly.Slope;
                    gfsFixtureAssembly.X = ccmAssembly.X;
                    gfsFixtureAssembly.Y = ccmAssembly.Y;
                    gfsFixtureAssembly.Z = ccmAssembly.Z;
                    gfsFixtureAssembly.NotchNumber = null; //Gets calculated when fetching the DC
                    #endregion
                    gfsFixture.Assemblies.Add(gfsFixtureAssembly);
                }

                output.Fixtures.Add(gfsFixture);
            }
            #endregion Fixtures

            #region Components and SubComponents
            //note dummy Ids move in the negative direction so they don't clash with any real Ids used. These
            //Ids will only be used in GFS as temporary Ids and will be overwritten when saved.
            Int32 dummySubComponentId = -1;
            foreach (PlanogramComponent ccmComponent in ccmPlanogram.Components)
            {
                GFS.PlanogramComponent gfsComponent = new GFS.PlanogramComponent();

                gfsComponent.Id = System.Convert.ToInt32(ccmComponent.Id);
                gfsComponent.Name = ValidName(ccmComponent.Name);
                gfsComponent.Height = ccmComponent.Height;
                gfsComponent.Width = ccmComponent.Width;
                gfsComponent.Depth = ccmComponent.Depth;
                gfsComponent.IsMoveable = ccmComponent.IsMoveable;
                gfsComponent.IsDisplayOnly = ccmComponent.IsDisplayOnly;
                gfsComponent.CanAttachShelfEdgeLabel = ccmComponent.CanAttachShelfEdgeLabel;
                gfsComponent.RetailerReference = ccmComponent.RetailerReferenceCode;
                gfsComponent.BarCode = ccmComponent.BarCode;
                gfsComponent.Manufacturer = ccmComponent.Manufacturer;
                gfsComponent.ManufacturerPartName = ccmComponent.ManufacturerPartName;
                gfsComponent.ManufacturerPartNumber = ccmComponent.ManufacturerPartNumber;
                gfsComponent.SupplierName = ccmComponent.SupplierName;
                gfsComponent.SupplierPartNumber = ccmComponent.SupplierPartNumber;
                gfsComponent.SupplierCostPrice = ccmComponent.SupplierCostPrice.HasValue ? ccmComponent.SupplierCostPrice.Value : 0;
                gfsComponent.SupplierDiscount = ccmComponent.SupplierDiscount.HasValue ? ccmComponent.SupplierDiscount.Value : 0;
                gfsComponent.SupplierLeadTime = ccmComponent.SupplierLeadTime.HasValue ? ccmComponent.SupplierLeadTime.Value : 0;
                gfsComponent.MinPurchaseQty = System.Convert.ToByte(ccmComponent.MinPurchaseQty);
                gfsComponent.WeightLimit = ccmComponent.WeightLimit.HasValue ? ccmComponent.WeightLimit.Value : 0;
                gfsComponent.Weight = ccmComponent.Weight.HasValue ? ccmComponent.Weight.Value : 0;
                gfsComponent.Volume = ccmComponent.Volume.HasValue ? ccmComponent.Volume.Value : 0;
                gfsComponent.Diameter = ccmComponent.Diameter.HasValue ? ccmComponent.Diameter.Value : 0;
                gfsComponent.Capacity = ccmComponent.Capacity.HasValue ? ccmComponent.Capacity.Value : (Int16)0;
                gfsComponent.NumberOfSubComponents = System.Convert.ToInt16(ccmComponent.MetaNumberOfSubComponents);
                gfsComponent.NumberOfMerchandisedSubComponents = System.Convert.ToInt16(ccmComponent.MetaNumberOfMerchandisedSubComponents);
                gfsComponent.SubComponents = new List<GFS.PlanogramSubComponent>();
                gfsComponent.MerchandiseType = "Display"; //TODO
                
                foreach (PlanogramSubComponent ccmSubComponent in ccmComponent.SubComponents)
                {
                    //In GFS the Component.Colour field is used as the fill colour.
                    gfsComponent.Colour = ccmSubComponent.FillColourFront;

                    //If the sub component has face thickness values then we have to treat it like a chest. Even if it is say, a basket.
                    if (ccmSubComponent.HasFaceThickness())
                    {
                        //Base
                        GFS.PlanogramSubComponent chestBase = ConvertToSubComponent(ccmSubComponent, ccmComponent.ComponentType);
                        chestBase.SubComponentType = PlanogramComponentType.Chest.ToString(); //force to chest so show/hide walls works in GFS
                        chestBase.IsMerchandisable = true;
                        chestBase.Height = ccmSubComponent.FaceThicknessBottom;
                        gfsComponent.SubComponents.Add(chestBase);

                        if (ccmSubComponent.FaceThicknessLeft > 0)
                        {
                            GFS.PlanogramSubComponent chestWallLeft = ConvertToSubComponent(ccmSubComponent, ccmComponent.ComponentType);
                            chestWallLeft.SubComponentType = PlanogramComponentType.Chest.ToString(); //force to chest so show/hide walls works in GFS
                            chestWallLeft.IsMerchandisable = false;
                            chestWallLeft.MerchandisableHeight = 0;
                            chestWallLeft.Height = ccmSubComponent.Height - ccmSubComponent.FaceThicknessBottom;
                            chestWallLeft.Y += ccmSubComponent.FaceThicknessBottom;
                            chestWallLeft.Width = ccmSubComponent.FaceThicknessLeft;
                            chestWallLeft.Id = dummySubComponentId;                            
                            dummySubComponentId--;
                            gfsComponent.SubComponents.Add(chestWallLeft);
                        }
                        if (ccmSubComponent.FaceThicknessRight > 0)
                        {
                            GFS.PlanogramSubComponent chestWallRight = ConvertToSubComponent(ccmSubComponent, ccmComponent.ComponentType);
                            chestWallRight.SubComponentType = PlanogramComponentType.Chest.ToString(); //force to chest so show/hide walls works in GFS
                            chestWallRight.IsMerchandisable = false;
                            chestWallRight.MerchandisableHeight = 0;
                            chestWallRight.Height = ccmSubComponent.Height - ccmSubComponent.FaceThicknessBottom;
                            chestWallRight.Y += ccmSubComponent.FaceThicknessBottom;
                            chestWallRight.X += ccmSubComponent.Width - ccmSubComponent.FaceThicknessRight;
                            chestWallRight.Width = ccmSubComponent.FaceThicknessRight;
                            chestWallRight.Id = dummySubComponentId;
                            dummySubComponentId--;
                            gfsComponent.SubComponents.Add(chestWallRight);
                        }

                        if (ccmSubComponent.FaceThicknessBack > 0)
                        {
                            GFS.PlanogramSubComponent chestWallBack = ConvertToSubComponent(ccmSubComponent, ccmComponent.ComponentType);
                            chestWallBack.SubComponentType = PlanogramComponentType.Chest.ToString(); //force to chest so show/hide walls works in GFS
                            chestWallBack.IsMerchandisable = false;
                            chestWallBack.MerchandisableHeight = 0;
                            chestWallBack.Height = ccmSubComponent.Height - ccmSubComponent.FaceThicknessBottom;
                            chestWallBack.Y += ccmSubComponent.FaceThicknessBottom;
                            chestWallBack.Z -= ccmSubComponent.Depth - ccmSubComponent.FaceThicknessBack;
                            chestWallBack.Depth = ccmSubComponent.FaceThicknessBack;
                            chestWallBack.Id = dummySubComponentId;
                            dummySubComponentId--;
                            gfsComponent.SubComponents.Add(chestWallBack);
                        }

                        if (ccmSubComponent.FaceThicknessFront > 0)
                        {
                            GFS.PlanogramSubComponent chestWallfront = ConvertToSubComponent(ccmSubComponent, ccmComponent.ComponentType);
                            chestWallfront.SubComponentType = PlanogramComponentType.Chest.ToString(); //force to chest so show/hide walls works in GFS
                            chestWallfront.IsMerchandisable = false;
                            chestWallfront.MerchandisableHeight = 0;
                            chestWallfront.Height = ccmSubComponent.Height - ccmSubComponent.FaceThicknessBottom;
                            chestWallfront.Y += ccmSubComponent.FaceThicknessBottom;
                            chestWallfront.Depth = ccmSubComponent.FaceThicknessFront;
                            chestWallfront.Id = dummySubComponentId;
                            dummySubComponentId--;
                            gfsComponent.SubComponents.Add(chestWallfront);
                        }
                        

                    }
                    else
                    {
                        gfsComponent.SubComponents.Add(ConvertToSubComponent(ccmSubComponent, ccmComponent.ComponentType));
                    }
                }

                #region CustomAttributes

                if (ccmComponent.CustomAttributes != null)
                {
                    GFS.CustomAttributeData customAttributeDataItem = new GFS.CustomAttributeData();

                    customAttributeDataItem.Id = IdentityHelper.GetNextInt32();
                    customAttributeDataItem.ParentId = gfsComponent.Id;
                    customAttributeDataItem.ParentType = (byte)CustomAttributeDataPlanogramParentType.PlanogramComponent;
                    customAttributeDataItem.Text1 = ccmComponent.CustomAttributes.Text1;
                    customAttributeDataItem.Text2 = ccmComponent.CustomAttributes.Text2;
                    customAttributeDataItem.Text3 = ccmComponent.CustomAttributes.Text3;
                    customAttributeDataItem.Text4 = ccmComponent.CustomAttributes.Text4;
                    customAttributeDataItem.Text5 = ccmComponent.CustomAttributes.Text5;
                    customAttributeDataItem.Text6 = ccmComponent.CustomAttributes.Text6;
                    customAttributeDataItem.Text7 = ccmComponent.CustomAttributes.Text7;
                    customAttributeDataItem.Text8 = ccmComponent.CustomAttributes.Text8;
                    customAttributeDataItem.Text9 = ccmComponent.CustomAttributes.Text9;
                    customAttributeDataItem.Text10 = ccmComponent.CustomAttributes.Text10;
                    customAttributeDataItem.Text11 = ccmComponent.CustomAttributes.Text11;
                    customAttributeDataItem.Text12 = ccmComponent.CustomAttributes.Text12;
                    customAttributeDataItem.Text13 = ccmComponent.CustomAttributes.Text13;
                    customAttributeDataItem.Text14 = ccmComponent.CustomAttributes.Text14;
                    customAttributeDataItem.Text15 = ccmComponent.CustomAttributes.Text15;
                    customAttributeDataItem.Text16 = ccmComponent.CustomAttributes.Text16;
                    customAttributeDataItem.Text17 = ccmComponent.CustomAttributes.Text17;
                    customAttributeDataItem.Text18 = ccmComponent.CustomAttributes.Text18;
                    customAttributeDataItem.Text19 = ccmComponent.CustomAttributes.Text19;
                    customAttributeDataItem.Text20 = ccmComponent.CustomAttributes.Text20;
                    customAttributeDataItem.Text21 = ccmComponent.CustomAttributes.Text21;
                    customAttributeDataItem.Text22 = ccmComponent.CustomAttributes.Text22;
                    customAttributeDataItem.Text23 = ccmComponent.CustomAttributes.Text23;
                    customAttributeDataItem.Text24 = ccmComponent.CustomAttributes.Text24;
                    customAttributeDataItem.Text25 = ccmComponent.CustomAttributes.Text25;
                    customAttributeDataItem.Text26 = ccmComponent.CustomAttributes.Text26;
                    customAttributeDataItem.Text27 = ccmComponent.CustomAttributes.Text27;
                    customAttributeDataItem.Text28 = ccmComponent.CustomAttributes.Text28;
                    customAttributeDataItem.Text29 = ccmComponent.CustomAttributes.Text29;
                    customAttributeDataItem.Text30 = ccmComponent.CustomAttributes.Text30;
                    customAttributeDataItem.Text31 = ccmComponent.CustomAttributes.Text31;
                    customAttributeDataItem.Text32 = ccmComponent.CustomAttributes.Text32;
                    customAttributeDataItem.Text33 = ccmComponent.CustomAttributes.Text33;
                    customAttributeDataItem.Text34 = ccmComponent.CustomAttributes.Text34;
                    customAttributeDataItem.Text35 = ccmComponent.CustomAttributes.Text35;
                    customAttributeDataItem.Text36 = ccmComponent.CustomAttributes.Text36;
                    customAttributeDataItem.Text37 = ccmComponent.CustomAttributes.Text37;
                    customAttributeDataItem.Text38 = ccmComponent.CustomAttributes.Text38;
                    customAttributeDataItem.Text39 = ccmComponent.CustomAttributes.Text39;
                    customAttributeDataItem.Text40 = ccmComponent.CustomAttributes.Text40;
                    customAttributeDataItem.Text41 = ccmComponent.CustomAttributes.Text41;
                    customAttributeDataItem.Text42 = ccmComponent.CustomAttributes.Text42;
                    customAttributeDataItem.Text43 = ccmComponent.CustomAttributes.Text43;
                    customAttributeDataItem.Text44 = ccmComponent.CustomAttributes.Text44;
                    customAttributeDataItem.Text45 = ccmComponent.CustomAttributes.Text45;
                    customAttributeDataItem.Text46 = ccmComponent.CustomAttributes.Text46;
                    customAttributeDataItem.Text47 = ccmComponent.CustomAttributes.Text47;
                    customAttributeDataItem.Text48 = ccmComponent.CustomAttributes.Text48;
                    customAttributeDataItem.Text49 = ccmComponent.CustomAttributes.Text49;
                    customAttributeDataItem.Text50 = ccmComponent.CustomAttributes.Text50;

                    customAttributeDataItem.Value1 = ccmComponent.CustomAttributes.Value1;
                    customAttributeDataItem.Value2 = ccmComponent.CustomAttributes.Value2;
                    customAttributeDataItem.Value3 = ccmComponent.CustomAttributes.Value3;
                    customAttributeDataItem.Value4 = ccmComponent.CustomAttributes.Value4;
                    customAttributeDataItem.Value5 = ccmComponent.CustomAttributes.Value5;
                    customAttributeDataItem.Value6 = ccmComponent.CustomAttributes.Value6;
                    customAttributeDataItem.Value7 = ccmComponent.CustomAttributes.Value7;
                    customAttributeDataItem.Value8 = ccmComponent.CustomAttributes.Value8;
                    customAttributeDataItem.Value9 = ccmComponent.CustomAttributes.Value9;
                    customAttributeDataItem.Value10 = ccmComponent.CustomAttributes.Value10;
                    customAttributeDataItem.Value11 = ccmComponent.CustomAttributes.Value11;
                    customAttributeDataItem.Value12 = ccmComponent.CustomAttributes.Value12;
                    customAttributeDataItem.Value13 = ccmComponent.CustomAttributes.Value13;
                    customAttributeDataItem.Value14 = ccmComponent.CustomAttributes.Value14;
                    customAttributeDataItem.Value15 = ccmComponent.CustomAttributes.Value15;
                    customAttributeDataItem.Value16 = ccmComponent.CustomAttributes.Value16;
                    customAttributeDataItem.Value17 = ccmComponent.CustomAttributes.Value17;
                    customAttributeDataItem.Value18 = ccmComponent.CustomAttributes.Value18;
                    customAttributeDataItem.Value19 = ccmComponent.CustomAttributes.Value19;
                    customAttributeDataItem.Value20 = ccmComponent.CustomAttributes.Value20;
                    customAttributeDataItem.Value21 = ccmComponent.CustomAttributes.Value21;
                    customAttributeDataItem.Value22 = ccmComponent.CustomAttributes.Value22;
                    customAttributeDataItem.Value23 = ccmComponent.CustomAttributes.Value23;
                    customAttributeDataItem.Value24 = ccmComponent.CustomAttributes.Value24;
                    customAttributeDataItem.Value25 = ccmComponent.CustomAttributes.Value25;
                    customAttributeDataItem.Value26 = ccmComponent.CustomAttributes.Value26;
                    customAttributeDataItem.Value27 = ccmComponent.CustomAttributes.Value27;
                    customAttributeDataItem.Value28 = ccmComponent.CustomAttributes.Value28;
                    customAttributeDataItem.Value29 = ccmComponent.CustomAttributes.Value29;
                    customAttributeDataItem.Value30 = ccmComponent.CustomAttributes.Value30;
                    customAttributeDataItem.Value31 = ccmComponent.CustomAttributes.Value31;
                    customAttributeDataItem.Value32 = ccmComponent.CustomAttributes.Value32;
                    customAttributeDataItem.Value33 = ccmComponent.CustomAttributes.Value33;
                    customAttributeDataItem.Value34 = ccmComponent.CustomAttributes.Value34;
                    customAttributeDataItem.Value35 = ccmComponent.CustomAttributes.Value35;
                    customAttributeDataItem.Value36 = ccmComponent.CustomAttributes.Value36;
                    customAttributeDataItem.Value37 = ccmComponent.CustomAttributes.Value37;
                    customAttributeDataItem.Value38 = ccmComponent.CustomAttributes.Value38;
                    customAttributeDataItem.Value39 = ccmComponent.CustomAttributes.Value39;
                    customAttributeDataItem.Value40 = ccmComponent.CustomAttributes.Value40;
                    customAttributeDataItem.Value41 = ccmComponent.CustomAttributes.Value41;
                    customAttributeDataItem.Value42 = ccmComponent.CustomAttributes.Value42;
                    customAttributeDataItem.Value43 = ccmComponent.CustomAttributes.Value43;
                    customAttributeDataItem.Value44 = ccmComponent.CustomAttributes.Value44;
                    customAttributeDataItem.Value45 = ccmComponent.CustomAttributes.Value45;
                    customAttributeDataItem.Value46 = ccmComponent.CustomAttributes.Value46;
                    customAttributeDataItem.Value47 = ccmComponent.CustomAttributes.Value47;
                    customAttributeDataItem.Value48 = ccmComponent.CustomAttributes.Value48;
                    customAttributeDataItem.Value49 = ccmComponent.CustomAttributes.Value49;
                    customAttributeDataItem.Value50 = ccmComponent.CustomAttributes.Value50;

                    customAttributeDataItem.Flag1 = ccmComponent.CustomAttributes.Flag1;
                    customAttributeDataItem.Flag2 = ccmComponent.CustomAttributes.Flag2;
                    customAttributeDataItem.Flag3 = ccmComponent.CustomAttributes.Flag3;
                    customAttributeDataItem.Flag4 = ccmComponent.CustomAttributes.Flag4;
                    customAttributeDataItem.Flag5 = ccmComponent.CustomAttributes.Flag5;
                    customAttributeDataItem.Flag6 = ccmComponent.CustomAttributes.Flag6;
                    customAttributeDataItem.Flag7 = ccmComponent.CustomAttributes.Flag7;
                    customAttributeDataItem.Flag8 = ccmComponent.CustomAttributes.Flag8;
                    customAttributeDataItem.Flag9 = ccmComponent.CustomAttributes.Flag9;
                    customAttributeDataItem.Flag10 = ccmComponent.CustomAttributes.Flag10;

                    customAttributeDataItem.Date1 = ccmComponent.CustomAttributes.Date1;
                    customAttributeDataItem.Date2 = ccmComponent.CustomAttributes.Date2;
                    customAttributeDataItem.Date3 = ccmComponent.CustomAttributes.Date3;
                    customAttributeDataItem.Date4 = ccmComponent.CustomAttributes.Date4;
                    customAttributeDataItem.Date5 = ccmComponent.CustomAttributes.Date5;
                    customAttributeDataItem.Date6 = ccmComponent.CustomAttributes.Date6;
                    customAttributeDataItem.Date7 = ccmComponent.CustomAttributes.Date7;
                    customAttributeDataItem.Date8 = ccmComponent.CustomAttributes.Date8;
                    customAttributeDataItem.Date9 = ccmComponent.CustomAttributes.Date9;
                    customAttributeDataItem.Date10 = ccmComponent.CustomAttributes.Date10;

                    customAttributeDataItem.Note1 = ccmComponent.CustomAttributes.Note1;
                    customAttributeDataItem.Note2 = ccmComponent.CustomAttributes.Note2;
                    customAttributeDataItem.Note3 = ccmComponent.CustomAttributes.Note3;
                    customAttributeDataItem.Note4 = ccmComponent.CustomAttributes.Note4;
                    customAttributeDataItem.Note5 = ccmComponent.CustomAttributes.Note5;

                    gfsComponent.CustomAttributes = customAttributeDataItem;
                }

                #endregion CustomAttributes

                output.Components.Add(gfsComponent);
            }

            #endregion Components and SubComponents

            #region Assemblies / AssemblyComponents

            foreach (PlanogramAssembly ccmAssembly in ccmPlanogram.Assemblies)
            {
                GFS.PlanogramAssembly gfsAssembly = new GFS.PlanogramAssembly();
                gfsAssembly.Id = System.Convert.ToInt32(ccmAssembly.Id);
                gfsAssembly.Name = ValidName(ccmAssembly.Name);
                gfsAssembly.Height = ccmAssembly.Height;
                gfsAssembly.Width = ccmAssembly.Width;
                gfsAssembly.Depth = ccmAssembly.Depth;
                gfsAssembly.TotalComponentCost = ccmAssembly.TotalComponentCost;
                gfsAssembly.Components = new List<GFS.PlanogramAssemblyComponent>();
                foreach (PlanogramAssemblyComponent ccmPlanogramAssemblyComponent in ccmAssembly.Components)
                {
                    GFS.PlanogramAssemblyComponent gfsAssemblyComponent = new GFS.PlanogramAssemblyComponent();
                    gfsAssemblyComponent.Id = System.Convert.ToInt32(ccmPlanogramAssemblyComponent.Id);
                    gfsAssemblyComponent.X = ccmPlanogramAssemblyComponent.X;
                    gfsAssemblyComponent.Y = ccmPlanogramAssemblyComponent.Y;
                    gfsAssemblyComponent.Z = ccmPlanogramAssemblyComponent.Z;
                    gfsAssemblyComponent.Slope = ccmPlanogramAssemblyComponent.Slope;
                    gfsAssemblyComponent.Angle = ccmPlanogramAssemblyComponent.Angle;
                    gfsAssemblyComponent.Roll = ccmPlanogramAssemblyComponent.Roll;
                    gfsAssemblyComponent.PlanogramComponentId = System.Convert.ToInt32(ccmPlanogramAssemblyComponent.PlanogramComponentId);
                    gfsAssemblyComponent.ElementSequenceNumber = ccmPlanogramAssemblyComponent.ComponentSequenceNumber;
                    gfsAssembly.Components.Add(gfsAssemblyComponent);                    
                }

                output.Assemblies.Add(gfsAssembly);
            }

            #endregion Assemblies / AssemblyComponents

            Dictionary<Int32, PlanogramPosition> planPositionsByPlanProductId = new Dictionary<Int32,PlanogramPosition>();
            foreach (PlanogramPosition pos in ccmPlanogram.Positions)
            {
                planPositionsByPlanProductId[Convert.ToInt32(pos.PlanogramProductId)] = pos;
            }


            #region Products
            foreach (PlanogramProduct ccmProduct in ccmPlanogram.Products)
            {
                GFS.PlanogramProduct gfsProduct = new GFS.PlanogramProduct();
                gfsProduct.Id = System.Convert.ToInt32(ccmProduct.Id);
                gfsProduct.GTIN = ccmProduct.Gtin;
                gfsProduct.Name = ValidName(ccmProduct.Name);
                gfsProduct.Brand = ccmProduct.Brand;
                gfsProduct.Height = ccmProduct.Height;
                gfsProduct.Width = ccmProduct.Width;
                gfsProduct.Depth = ccmProduct.Depth;
                gfsProduct.DisplayHeight = ccmProduct.DisplayHeight;
                gfsProduct.DisplayWidth = ccmProduct.DisplayWidth;
                gfsProduct.DisplayDepth = ccmProduct.DisplayDepth;
                gfsProduct.AlternateHeight = ccmProduct.AlternateHeight;
                gfsProduct.AlternateWidth = ccmProduct.AlternateWidth;
                gfsProduct.AlternateDepth = ccmProduct.AlternateDepth;
                gfsProduct.PointOfPurchaseHeight = ccmProduct.PointOfPurchaseHeight;
                gfsProduct.PointOfPurchaseWidth = ccmProduct.PointOfPurchaseWidth;
                gfsProduct.PointOfPurchaseDepth = ccmProduct.PointOfPurchaseDepth;
                gfsProduct.NumberOfPegHoles = ccmProduct.NumberOfPegHoles;
                gfsProduct.PegX = ccmProduct.PegX;
                gfsProduct.PegX2 = ccmProduct.PegX2;
                gfsProduct.PegX3 = ccmProduct.PegX3;
                gfsProduct.PegY = ccmProduct.PegY;
                gfsProduct.PegY2 = ccmProduct.PegY2;
                gfsProduct.PegY3 = ccmProduct.PegY3;
                gfsProduct.PegProngOffset = ccmProduct.PegProngOffsetX;
                gfsProduct.PegProngOffsetY = ccmProduct.PegProngOffsetY;
                gfsProduct.PegDepth = ccmProduct.PegDepth;

                if(planPositionsByPlanProductId.ContainsKey(Convert.ToInt32(ccmProduct.Id)))
                {
                    PlanogramPosition ccmPos = planPositionsByPlanProductId[Convert.ToInt32(ccmProduct.Id)];
                    PlanogramPositionDetails details = ccmPos.GetPositionDetails();
                    gfsProduct.SqueezeHeight = Math.Max(0.01F, details != null ? details.MainUnitSize.Height : ccmProduct.Height);
                    gfsProduct.SqueezeWidth = Math.Max(0.01F, details != null ? details.MainUnitSize.Width : ccmProduct.Width);
                    gfsProduct.SqueezeDepth = Math.Max(0.01F, details != null ? details.MainUnitSize.Depth : ccmProduct.Depth);
                }
                gfsProduct.NestingHeight = ccmProduct.NestingHeight;
                gfsProduct.NestingWidth = ccmProduct.NestingWidth;
                gfsProduct.NestingDepth = ccmProduct.NestingDepth;
                gfsProduct.CasePackUnits = ccmProduct.CasePackUnits;
                gfsProduct.CaseHigh = ccmProduct.CaseHigh;
                gfsProduct.CaseWide = ccmProduct.CaseWide;
                gfsProduct.CaseDeep = ccmProduct.CaseDeep;
                gfsProduct.CaseHeight = ccmProduct.CaseHeight;
                gfsProduct.CaseWidth = ccmProduct.CaseWidth;
                gfsProduct.CaseDepth = ccmProduct.CaseDepth;
                gfsProduct.MaxStack = ccmProduct.MaxStack;
                gfsProduct.MaxTopCap = ccmProduct.MaxTopCap;
                gfsProduct.MaxRightCap = ccmProduct.MaxRightCap;
                gfsProduct.MinDeep = ccmProduct.MinDeep;
                gfsProduct.MaxDeep = ccmProduct.MaxDeep;
                gfsProduct.TrayPackUnits = ccmProduct.TrayPackUnits;
                gfsProduct.TrayHigh = ccmProduct.TrayHigh;
                gfsProduct.TrayWide = ccmProduct.TrayWide;
                gfsProduct.TrayDeep = ccmProduct.TrayDeep;
                gfsProduct.TrayHeight = ccmProduct.TrayHeight;
                gfsProduct.TrayWidth = ccmProduct.TrayWidth;
                gfsProduct.TrayDepth = ccmProduct.TrayDepth;
                gfsProduct.TrayThickHeight = ccmProduct.TrayThickHeight;
                gfsProduct.TrayThickWidth = ccmProduct.TrayThickWidth;
                gfsProduct.TrayThickDepth = ccmProduct.TrayThickDepth;
                gfsProduct.FrontOverhang = ccmProduct.FrontOverhang;
                gfsProduct.FingerSpaceAbove = ccmProduct.FingerSpaceAbove;
                gfsProduct.FingerSpaceToTheSide = ccmProduct.FingerSpaceToTheSide;
                gfsProduct.StatusType = ccmProduct.StatusType.ToString();
                gfsProduct.OrientationType = GFSOrientationType(PlanogramPositionOrientationType.Default, ccmProduct.OrientationType).ToString();
                gfsProduct.MerchandisingStyle = ccmProduct.MerchandisingStyle.ToString();
                gfsProduct.IsFrontOnly = ccmProduct.IsFrontOnly;
                gfsProduct.IsTrayProduct = ccmProduct.MerchandisingStyle == PlanogramProductMerchandisingStyle.Tray;
                gfsProduct.IsPlaceHolderProduct = ccmProduct.IsPlaceHolderProduct;
                gfsProduct.IsActive = ccmProduct.IsActive;
                gfsProduct.CanBreakTrayUp = ccmProduct.CanBreakTrayUp;
                gfsProduct.CanBreakTrayDown = ccmProduct.CanBreakTrayDown;
                gfsProduct.CanBreakTrayBack = ccmProduct.CanBreakTrayBack;
                gfsProduct.CanBreakTrayTop = ccmProduct.CanBreakTrayTop;
                gfsProduct.ForceMiddleCap = ccmProduct.ForceMiddleCap;
                gfsProduct.ForceBottomCap = ccmProduct.ForceBottomCap;

                //V8-31233 : Try to populate performance
                PlanogramPerformanceData ccmPerformanceData = ccmProduct.GetPlanogramPerformanceData();
                if (ccmPerformanceData != null)
                {
                    gfsProduct.PerformanceDataP1 = ccmPerformanceData.P1;
                    gfsProduct.PerformanceDataP2 = ccmPerformanceData.P2;
                    gfsProduct.PerformanceDataP3 = ccmPerformanceData.P3;
                    gfsProduct.PerformanceDataP4 = ccmPerformanceData.P4;
                    gfsProduct.PerformanceDataP5 = ccmPerformanceData.P5;
                    gfsProduct.PerformanceDataP6 = ccmPerformanceData.P6;
                    gfsProduct.PerformanceDataP7 = ccmPerformanceData.P7;
                    gfsProduct.PerformanceDataP8 = ccmPerformanceData.P8;
                    gfsProduct.PerformanceDataP9 = ccmPerformanceData.P9;
                    gfsProduct.PerformanceDataP10 = ccmPerformanceData.P10;
                    gfsProduct.PerformanceDataP11 = ccmPerformanceData.P11;
                    gfsProduct.PerformanceDataP12 = ccmPerformanceData.P12;
                    gfsProduct.PerformanceDataP13 = ccmPerformanceData.P13;
                    gfsProduct.PerformanceDataP14 = ccmPerformanceData.P14;
                    gfsProduct.PerformanceDataP15 = ccmPerformanceData.P15;
                    gfsProduct.PerformanceDataP16 = ccmPerformanceData.P16;
                    gfsProduct.PerformanceDataP17 = ccmPerformanceData.P17;
                    gfsProduct.PerformanceDataP18 = ccmPerformanceData.P18;
                    gfsProduct.PerformanceDataP19 = ccmPerformanceData.P19;
                    gfsProduct.PerformanceDataP20 = ccmPerformanceData.P20;
                }

                #region CustomAttributes

                if (ccmProduct.CustomAttributes != null)
                {
                    GFS.CustomAttributeData customAttributeDataItem = new GFS.CustomAttributeData();

                    customAttributeDataItem.Id = IdentityHelper.GetNextInt32();
                    customAttributeDataItem.ParentId = gfsProduct.Id;
                    customAttributeDataItem.ParentType = (byte)CustomAttributeDataPlanogramParentType.PlanogramProduct;
                    customAttributeDataItem.Text1 = ccmProduct.CustomAttributes.Text1;
                    customAttributeDataItem.Text2 = ccmProduct.CustomAttributes.Text2;
                    customAttributeDataItem.Text3 = ccmProduct.CustomAttributes.Text3;
                    customAttributeDataItem.Text4 = ccmProduct.CustomAttributes.Text4;
                    customAttributeDataItem.Text5 = ccmProduct.CustomAttributes.Text5;
                    customAttributeDataItem.Text6 = ccmProduct.CustomAttributes.Text6;
                    customAttributeDataItem.Text7 = ccmProduct.CustomAttributes.Text7;
                    customAttributeDataItem.Text8 = ccmProduct.CustomAttributes.Text8;
                    customAttributeDataItem.Text9 = ccmProduct.CustomAttributes.Text9;
                    customAttributeDataItem.Text10 = ccmProduct.CustomAttributes.Text10;
                    customAttributeDataItem.Text11 = ccmProduct.CustomAttributes.Text11;
                    customAttributeDataItem.Text12 = ccmProduct.CustomAttributes.Text12;
                    customAttributeDataItem.Text13 = ccmProduct.CustomAttributes.Text13;
                    customAttributeDataItem.Text14 = ccmProduct.CustomAttributes.Text14;
                    customAttributeDataItem.Text15 = ccmProduct.CustomAttributes.Text15;
                    customAttributeDataItem.Text16 = ccmProduct.CustomAttributes.Text16;
                    customAttributeDataItem.Text17 = ccmProduct.CustomAttributes.Text17;
                    customAttributeDataItem.Text18 = ccmProduct.CustomAttributes.Text18;
                    customAttributeDataItem.Text19 = ccmProduct.CustomAttributes.Text19;
                    customAttributeDataItem.Text20 = ccmProduct.CustomAttributes.Text20;
                    customAttributeDataItem.Text21 = ccmProduct.CustomAttributes.Text21;
                    customAttributeDataItem.Text22 = ccmProduct.CustomAttributes.Text22;
                    customAttributeDataItem.Text23 = ccmProduct.CustomAttributes.Text23;
                    customAttributeDataItem.Text24 = ccmProduct.CustomAttributes.Text24;
                    customAttributeDataItem.Text25 = ccmProduct.CustomAttributes.Text25;
                    customAttributeDataItem.Text26 = ccmProduct.CustomAttributes.Text26;
                    customAttributeDataItem.Text27 = ccmProduct.CustomAttributes.Text27;
                    customAttributeDataItem.Text28 = ccmProduct.CustomAttributes.Text28;
                    customAttributeDataItem.Text29 = ccmProduct.CustomAttributes.Text29;
                    customAttributeDataItem.Text30 = ccmProduct.CustomAttributes.Text30;
                    customAttributeDataItem.Text31 = ccmProduct.CustomAttributes.Text31;
                    customAttributeDataItem.Text32 = ccmProduct.CustomAttributes.Text32;
                    customAttributeDataItem.Text33 = ccmProduct.CustomAttributes.Text33;
                    customAttributeDataItem.Text34 = ccmProduct.CustomAttributes.Text34;
                    customAttributeDataItem.Text35 = ccmProduct.CustomAttributes.Text35;
                    customAttributeDataItem.Text36 = ccmProduct.CustomAttributes.Text36;
                    customAttributeDataItem.Text37 = ccmProduct.CustomAttributes.Text37;
                    customAttributeDataItem.Text38 = ccmProduct.CustomAttributes.Text38;
                    customAttributeDataItem.Text39 = ccmProduct.CustomAttributes.Text39;
                    customAttributeDataItem.Text40 = ccmProduct.CustomAttributes.Text40;
                    customAttributeDataItem.Text41 = ccmProduct.CustomAttributes.Text41;
                    customAttributeDataItem.Text42 = ccmProduct.CustomAttributes.Text42;
                    customAttributeDataItem.Text43 = ccmProduct.CustomAttributes.Text43;
                    customAttributeDataItem.Text44 = ccmProduct.CustomAttributes.Text44;
                    customAttributeDataItem.Text45 = ccmProduct.CustomAttributes.Text45;
                    customAttributeDataItem.Text46 = ccmProduct.CustomAttributes.Text46;
                    customAttributeDataItem.Text47 = ccmProduct.CustomAttributes.Text47;
                    customAttributeDataItem.Text48 = ccmProduct.CustomAttributes.Text48;
                    customAttributeDataItem.Text49 = ccmProduct.CustomAttributes.Text49;
                    customAttributeDataItem.Text50 = ccmProduct.CustomAttributes.Text50;

                    customAttributeDataItem.Value1 = ccmProduct.CustomAttributes.Value1;
                    customAttributeDataItem.Value2 = ccmProduct.CustomAttributes.Value2;
                    customAttributeDataItem.Value3 = ccmProduct.CustomAttributes.Value3;
                    customAttributeDataItem.Value4 = ccmProduct.CustomAttributes.Value4;
                    customAttributeDataItem.Value5 = ccmProduct.CustomAttributes.Value5;
                    customAttributeDataItem.Value6 = ccmProduct.CustomAttributes.Value6;
                    customAttributeDataItem.Value7 = ccmProduct.CustomAttributes.Value7;
                    customAttributeDataItem.Value8 = ccmProduct.CustomAttributes.Value8;
                    customAttributeDataItem.Value9 = ccmProduct.CustomAttributes.Value9;
                    customAttributeDataItem.Value10 = ccmProduct.CustomAttributes.Value10;
                    customAttributeDataItem.Value11 = ccmProduct.CustomAttributes.Value11;
                    customAttributeDataItem.Value12 = ccmProduct.CustomAttributes.Value12;
                    customAttributeDataItem.Value13 = ccmProduct.CustomAttributes.Value13;
                    customAttributeDataItem.Value14 = ccmProduct.CustomAttributes.Value14;
                    customAttributeDataItem.Value15 = ccmProduct.CustomAttributes.Value15;
                    customAttributeDataItem.Value16 = ccmProduct.CustomAttributes.Value16;
                    customAttributeDataItem.Value17 = ccmProduct.CustomAttributes.Value17;
                    customAttributeDataItem.Value18 = ccmProduct.CustomAttributes.Value18;
                    customAttributeDataItem.Value19 = ccmProduct.CustomAttributes.Value19;
                    customAttributeDataItem.Value20 = ccmProduct.CustomAttributes.Value20;
                    customAttributeDataItem.Value21 = ccmProduct.CustomAttributes.Value21;
                    customAttributeDataItem.Value22 = ccmProduct.CustomAttributes.Value22;
                    customAttributeDataItem.Value23 = ccmProduct.CustomAttributes.Value23;
                    customAttributeDataItem.Value24 = ccmProduct.CustomAttributes.Value24;
                    customAttributeDataItem.Value25 = ccmProduct.CustomAttributes.Value25;
                    customAttributeDataItem.Value26 = ccmProduct.CustomAttributes.Value26;
                    customAttributeDataItem.Value27 = ccmProduct.CustomAttributes.Value27;
                    customAttributeDataItem.Value28 = ccmProduct.CustomAttributes.Value28;
                    customAttributeDataItem.Value29 = ccmProduct.CustomAttributes.Value29;
                    customAttributeDataItem.Value30 = ccmProduct.CustomAttributes.Value30;
                    customAttributeDataItem.Value31 = ccmProduct.CustomAttributes.Value31;
                    customAttributeDataItem.Value32 = ccmProduct.CustomAttributes.Value32;
                    customAttributeDataItem.Value33 = ccmProduct.CustomAttributes.Value33;
                    customAttributeDataItem.Value34 = ccmProduct.CustomAttributes.Value34;
                    customAttributeDataItem.Value35 = ccmProduct.CustomAttributes.Value35;
                    customAttributeDataItem.Value36 = ccmProduct.CustomAttributes.Value36;
                    customAttributeDataItem.Value37 = ccmProduct.CustomAttributes.Value37;
                    customAttributeDataItem.Value38 = ccmProduct.CustomAttributes.Value38;
                    customAttributeDataItem.Value39 = ccmProduct.CustomAttributes.Value39;
                    customAttributeDataItem.Value40 = ccmProduct.CustomAttributes.Value40;
                    customAttributeDataItem.Value41 = ccmProduct.CustomAttributes.Value41;
                    customAttributeDataItem.Value42 = ccmProduct.CustomAttributes.Value42;
                    customAttributeDataItem.Value43 = ccmProduct.CustomAttributes.Value43;
                    customAttributeDataItem.Value44 = ccmProduct.CustomAttributes.Value44;
                    customAttributeDataItem.Value45 = ccmProduct.CustomAttributes.Value45;
                    customAttributeDataItem.Value46 = ccmProduct.CustomAttributes.Value46;
                    customAttributeDataItem.Value47 = ccmProduct.CustomAttributes.Value47;
                    customAttributeDataItem.Value48 = ccmProduct.CustomAttributes.Value48;
                    customAttributeDataItem.Value49 = ccmProduct.CustomAttributes.Value49;
                    customAttributeDataItem.Value50 = ccmProduct.CustomAttributes.Value50;

                    customAttributeDataItem.Flag1 = ccmProduct.CustomAttributes.Flag1;
                    customAttributeDataItem.Flag2 = ccmProduct.CustomAttributes.Flag2;
                    customAttributeDataItem.Flag3 = ccmProduct.CustomAttributes.Flag3;
                    customAttributeDataItem.Flag4 = ccmProduct.CustomAttributes.Flag4;
                    customAttributeDataItem.Flag5 = ccmProduct.CustomAttributes.Flag5;
                    customAttributeDataItem.Flag6 = ccmProduct.CustomAttributes.Flag6;
                    customAttributeDataItem.Flag7 = ccmProduct.CustomAttributes.Flag7;
                    customAttributeDataItem.Flag8 = ccmProduct.CustomAttributes.Flag8;
                    customAttributeDataItem.Flag9 = ccmProduct.CustomAttributes.Flag9;
                    customAttributeDataItem.Flag10 = ccmProduct.CustomAttributes.Flag10;

                    customAttributeDataItem.Date1 = ccmProduct.CustomAttributes.Date1;
                    customAttributeDataItem.Date2 = ccmProduct.CustomAttributes.Date2;
                    customAttributeDataItem.Date3 = ccmProduct.CustomAttributes.Date3;
                    customAttributeDataItem.Date4 = ccmProduct.CustomAttributes.Date4;
                    customAttributeDataItem.Date5 = ccmProduct.CustomAttributes.Date5;
                    customAttributeDataItem.Date6 = ccmProduct.CustomAttributes.Date6;
                    customAttributeDataItem.Date7 = ccmProduct.CustomAttributes.Date7;
                    customAttributeDataItem.Date8 = ccmProduct.CustomAttributes.Date8;
                    customAttributeDataItem.Date9 = ccmProduct.CustomAttributes.Date9;
                    customAttributeDataItem.Date10 = ccmProduct.CustomAttributes.Date10;

                    customAttributeDataItem.Note1 = ccmProduct.CustomAttributes.Note1;
                    customAttributeDataItem.Note2 = ccmProduct.CustomAttributes.Note2;
                    customAttributeDataItem.Note3 = ccmProduct.CustomAttributes.Note3;
                    customAttributeDataItem.Note4 = ccmProduct.CustomAttributes.Note4;
                    customAttributeDataItem.Note5 = ccmProduct.CustomAttributes.Note5;

                    gfsProduct.CustomAttributes = customAttributeDataItem;
                }

                #endregion CustomAttributes

                output.Products.Add(gfsProduct);
            }
            #endregion Products

            #region Positions

            Single designHight, designWidth, designTopDownOffset;
            ccmPlanogram.GetBlockingAreaSize(out designHight, out designWidth, out designTopDownOffset);
            PlanogramBlocking blocking = ccmPlanogram.Blocking.OrderByDescending(b => b.Type).FirstOrDefault();

            //get our blocking colours.
            Dictionary<Object, Int32?> sourcePosIdToBlocking = new Dictionary<Object, Int32?>();
            if (blocking != null)
            {
                foreach (PlanogramPosition position in ccmPlanogram.Positions)
                {
                    sourcePosIdToBlocking[position.Id] = (position.SequenceColour != null) ? position.SequenceColour : /*white*/-1;
                }
                //using (PlanogramMerchandisingGroupList merchGroups = ccmPlanogram.GetMerchandisingGroups())
                //{
                //    foreach (PlanogramMerchandisingGroup merchandisingGroup in merchGroups)
                //    {
                //        foreach (PlanogramPositionPlacement positionPlacement in merchandisingGroup.PositionPlacements)
                //        {
                //            DesignViewPosition designViewPosition = new DesignViewPosition(positionPlacement, designTopDownOffset);

                //            //Int32 blockingColour = DesignViewHelper.GetPositionBlockingGroupColour(blocking, designViewPosition, designWidth, designHight);

                //            // Get the blocking colour from the PlanogramProduct object that has already been calculated
                //            // and add to ref. dictionary
                //            sourcePosIdToBlocking[positionPlacement.Position.Id] = positionPlacement.Product.BlockingColour;
                //        }
                //    }
                //}
            }

            //create positions to output
            foreach (PlanogramSubComponentPlacement subPlacement in ccmPlanogram.GetPlanogramSubComponentPlacements())
            {
                foreach (PlanogramPosition pos in subPlacement.GetPlanogramPositions())
                {
                    List<GFS.PlanogramPosition> splitPositions = new List<GFS.PlanogramPosition>(CreatePositions(ccmPlanogram, pos, fixtureComponentToDummyFixtureAssemblyIds));

                    //set the blocking colour
                    if (blocking != null)
                    {
                        Int32? blockingColour;
                        if (sourcePosIdToBlocking.TryGetValue(pos.Id, out blockingColour))
                        {
                            if (blockingColour != null)
                            {
                                splitPositions.ForEach(p => p.BlockingColour = (Int32) blockingColour);
                            }
                        }
                    }

                    output.Positions.AddRange(splitPositions);

                    #region tray dimensions
                    if (splitPositions.Any(p => p.MerchandisingStyle == PlanogramProductMerchandisingStyle.Tray.ToString()))
                    {
                        GFS.PlanogramProduct prod = output.Products.FirstOrDefault(p => p.Id == splitPositions.First().PlanogramProductId);
                        if (prod != null)
                        {
                            prod.IsTrayProduct = true;
                            //ensure tray dimensions are set
                            if (prod.TrayHeight == 0)
                            {
                                prod.TrayHeight = prod.Height;
                            }
                            if (prod.TrayDepth == 0)
                            {
                                prod.TrayDepth = prod.Depth;
                            }
                            if (prod.TrayWidth == 0)
                            {
                                prod.TrayWidth = prod.Width;
                            }
                        }
                    }
                    #endregion
                    #region case dimensions
                    if (splitPositions.Any(p => p.MerchandisingStyle == PlanogramProductMerchandisingStyle.Case.ToString()))
                    {
                        GFS.PlanogramProduct prod = output.Products.FirstOrDefault(p => p.Id == splitPositions.First().PlanogramProductId);
                        if (prod != null)
                        {
                            //ensure case dimensions are set
                            if (prod.CaseHeight == 0)
                            {
                                prod.CaseHeight = prod.Height;
                            }
                            if (prod.CaseDepth == 0)
                            {
                                prod.CaseDepth = prod.Depth;
                            }
                            if (prod.CaseWidth == 0)
                            {
                                prod.CaseWidth = prod.Width;
                            }
                        }
                    }
                    #endregion
                }
            }

            #endregion Positions
            
            #region Annotations
            foreach (PlanogramAnnotation ccmAnnotation in ccmPlanogram.Annotations)
            {
                GFS.PlanogramAnnotation gfsAnnotation = new GFS.PlanogramAnnotation();
                gfsAnnotation.Id = Convert.ToInt32(ccmAnnotation.Id);

                gfsAnnotation.PlanogramFixtureItemId = Convert.ToInt32(ccmAnnotation.PlanogramFixtureItemId);

                //all must be populated (preserved space) or none (fixture annotation)
                if (ccmAnnotation.PlanogramFixtureAssemblyId != null &&
                    ccmAnnotation.PlanogramAssemblyComponentId != null &&
                    ccmAnnotation.PlanogramSubComponentId != null &&
                    ccmAnnotation.PlanogramPositionId != null)
                {
                    gfsAnnotation.PlanogramFixtureAssemblyId = Convert.ToInt32(ccmAnnotation.PlanogramFixtureAssemblyId);
                    gfsAnnotation.PlanogramAssemblyComponentId = Convert.ToInt32(ccmAnnotation.PlanogramAssemblyComponentId);
                    gfsAnnotation.PlanogramSubComponentId = Convert.ToInt32(ccmAnnotation.PlanogramSubComponentId);
                    gfsAnnotation.PlanogramPositionId = Convert.ToInt32(ccmAnnotation.PlanogramPositionId);
                }

                gfsAnnotation.X = ccmAnnotation.X;
                gfsAnnotation.Y = ccmAnnotation.Y;
                gfsAnnotation.Z = ccmAnnotation.Z + ccmAnnotation.Depth;
                if (ccmAnnotation.PlanogramFixtureComponentId != null)
                {
                    //get linked component
                    PlanogramFixtureComponent component = ccmPlanogram.GetPlanogramFixtureComponent(ccmAnnotation.PlanogramFixtureComponentId);
                    gfsAnnotation.Y = ccmAnnotation.Y + component.Y;
                }
                
                //null off id if 0
                if(gfsAnnotation.PlanogramFixtureItemId == 0) gfsAnnotation.PlanogramFixtureItemId = null;
                if (gfsAnnotation.PlanogramFixtureAssemblyId == 0) gfsAnnotation.PlanogramFixtureAssemblyId = null;
                if (gfsAnnotation.PlanogramAssemblyComponentId == 0) gfsAnnotation.PlanogramAssemblyComponentId = null;
                if (gfsAnnotation.PlanogramSubComponentId == 0) gfsAnnotation.PlanogramSubComponentId = null;
                if (gfsAnnotation.PlanogramPositionId == 0) gfsAnnotation.PlanogramPositionId = null;


                gfsAnnotation.AnnotationType = ccmAnnotation.AnnotationType.ToString();
                gfsAnnotation.Text = ccmAnnotation.Text;

                gfsAnnotation.Height = ccmAnnotation.Height;
                gfsAnnotation.Width = ccmAnnotation.Width;
                gfsAnnotation.Depth = ccmAnnotation.Depth;

                output.Annotations.Add(gfsAnnotation);
            }
            #endregion Annotations

            #region Set Meta Data
            output.BayCount = output.FixtureItems.Count;
            
            if(ccmPlanogram.MetaComponentCount.HasValue) output.ComponentCount = ccmPlanogram.MetaComponentCount.Value;
            
            //Space
            if (ccmPlanogram.MetaTotalMerchandisableLinearSpace.HasValue) output.MerchandisableLinearSpace = ccmPlanogram.MetaTotalMerchandisableLinearSpace.Value;
            if (ccmPlanogram.MetaTotalMerchandisableAreaSpace.HasValue) output.MerchandisableAreaSpace = ccmPlanogram.MetaTotalMerchandisableAreaSpace.Value;
            if (ccmPlanogram.MetaTotalMerchandisableVolumetricSpace.HasValue) output.MerchandisableVolumetricSpace = ccmPlanogram.MetaTotalMerchandisableVolumetricSpace.Value;
            //NOTE Meta Data white space is already a %
            if (ccmPlanogram.MetaTotalVolumetricWhiteSpace.HasValue) output.WhiteSpacePercentage = ccmPlanogram.MetaTotalVolumetricWhiteSpace.Value;
            
            //positions
            if (ccmPlanogram.MetaTotalFacings.HasValue) output.TotalFacingCount = ccmPlanogram.MetaTotalFacings.Value;
            output.TotalPositionCount = ccmPlanogram.Positions.Count;

            //NOTE Meta Data white space is already a %
            //Calculate product linear space.
            if (ccmPlanogram.MetaTotalMerchandisableLinearSpace.HasValue && ccmPlanogram.MetaTotalLinearWhiteSpace.HasValue) output.TotalProductLinearSpace = ccmPlanogram.MetaTotalMerchandisableLinearSpace.Value - (ccmPlanogram.MetaTotalMerchandisableLinearSpace.Value * ccmPlanogram.MetaTotalLinearWhiteSpace.Value);

            #endregion Set Meta Data

            #region CustomAttributes

            if (ccmPlanogram.CustomAttributes != null)
            {
                GFS.CustomAttributeData customAttributeDataItem = new GFS.CustomAttributeData();

                customAttributeDataItem.Id = IdentityHelper.GetNextInt32();
                customAttributeDataItem.ParentId = output.Id;
                customAttributeDataItem.ParentType = (byte)CustomAttributeDataPlanogramParentType.Planogram;
                customAttributeDataItem.Text1 = ccmPlanogram.CustomAttributes.Text1;
                customAttributeDataItem.Text2 = ccmPlanogram.CustomAttributes.Text2;
                customAttributeDataItem.Text3 = ccmPlanogram.CustomAttributes.Text3;
                customAttributeDataItem.Text4 = ccmPlanogram.CustomAttributes.Text4;
                customAttributeDataItem.Text5 = ccmPlanogram.CustomAttributes.Text5;
                customAttributeDataItem.Text6 = ccmPlanogram.CustomAttributes.Text6;
                customAttributeDataItem.Text7 = ccmPlanogram.CustomAttributes.Text7;
                customAttributeDataItem.Text8 = ccmPlanogram.CustomAttributes.Text8;
                customAttributeDataItem.Text9 = ccmPlanogram.CustomAttributes.Text9;
                customAttributeDataItem.Text10 = ccmPlanogram.CustomAttributes.Text10;
                customAttributeDataItem.Text11 = ccmPlanogram.CustomAttributes.Text11;
                customAttributeDataItem.Text12 = ccmPlanogram.CustomAttributes.Text12;
                customAttributeDataItem.Text13 = ccmPlanogram.CustomAttributes.Text13;
                customAttributeDataItem.Text14 = ccmPlanogram.CustomAttributes.Text14;
                customAttributeDataItem.Text15 = ccmPlanogram.CustomAttributes.Text15;
                customAttributeDataItem.Text16 = ccmPlanogram.CustomAttributes.Text16;
                customAttributeDataItem.Text17 = ccmPlanogram.CustomAttributes.Text17;
                customAttributeDataItem.Text18 = ccmPlanogram.CustomAttributes.Text18;
                customAttributeDataItem.Text19 = ccmPlanogram.CustomAttributes.Text19;
                customAttributeDataItem.Text20 = ccmPlanogram.CustomAttributes.Text20;
                customAttributeDataItem.Text21 = ccmPlanogram.CustomAttributes.Text21;
                customAttributeDataItem.Text22 = ccmPlanogram.CustomAttributes.Text22;
                customAttributeDataItem.Text23 = ccmPlanogram.CustomAttributes.Text23;
                customAttributeDataItem.Text24 = ccmPlanogram.CustomAttributes.Text24;
                customAttributeDataItem.Text25 = ccmPlanogram.CustomAttributes.Text25;
                customAttributeDataItem.Text26 = ccmPlanogram.CustomAttributes.Text26;
                customAttributeDataItem.Text27 = ccmPlanogram.CustomAttributes.Text27;
                customAttributeDataItem.Text28 = ccmPlanogram.CustomAttributes.Text28;
                customAttributeDataItem.Text29 = ccmPlanogram.CustomAttributes.Text29;
                customAttributeDataItem.Text30 = ccmPlanogram.CustomAttributes.Text30;
                customAttributeDataItem.Text31 = ccmPlanogram.CustomAttributes.Text31;
                customAttributeDataItem.Text32 = ccmPlanogram.CustomAttributes.Text32;
                customAttributeDataItem.Text33 = ccmPlanogram.CustomAttributes.Text33;
                customAttributeDataItem.Text34 = ccmPlanogram.CustomAttributes.Text34;
                customAttributeDataItem.Text35 = ccmPlanogram.CustomAttributes.Text35;
                customAttributeDataItem.Text36 = ccmPlanogram.CustomAttributes.Text36;
                customAttributeDataItem.Text37 = ccmPlanogram.CustomAttributes.Text37;
                customAttributeDataItem.Text38 = ccmPlanogram.CustomAttributes.Text38;
                customAttributeDataItem.Text39 = ccmPlanogram.CustomAttributes.Text39;
                customAttributeDataItem.Text40 = ccmPlanogram.CustomAttributes.Text40;
                customAttributeDataItem.Text41 = ccmPlanogram.CustomAttributes.Text41;
                customAttributeDataItem.Text42 = ccmPlanogram.CustomAttributes.Text42;
                customAttributeDataItem.Text43 = ccmPlanogram.CustomAttributes.Text43;
                customAttributeDataItem.Text44 = ccmPlanogram.CustomAttributes.Text44;
                customAttributeDataItem.Text45 = ccmPlanogram.CustomAttributes.Text45;
                customAttributeDataItem.Text46 = ccmPlanogram.CustomAttributes.Text46;
                customAttributeDataItem.Text47 = ccmPlanogram.CustomAttributes.Text47;
                customAttributeDataItem.Text48 = ccmPlanogram.CustomAttributes.Text48;
                customAttributeDataItem.Text49 = ccmPlanogram.CustomAttributes.Text49;
                customAttributeDataItem.Text50 = ccmPlanogram.CustomAttributes.Text50;

                customAttributeDataItem.Value1 = ccmPlanogram.CustomAttributes.Value1;
                customAttributeDataItem.Value2 = ccmPlanogram.CustomAttributes.Value2;
                customAttributeDataItem.Value3 = ccmPlanogram.CustomAttributes.Value3;
                customAttributeDataItem.Value4 = ccmPlanogram.CustomAttributes.Value4;
                customAttributeDataItem.Value5 = ccmPlanogram.CustomAttributes.Value5;
                customAttributeDataItem.Value6 = ccmPlanogram.CustomAttributes.Value6;
                customAttributeDataItem.Value7 = ccmPlanogram.CustomAttributes.Value7;
                customAttributeDataItem.Value8 = ccmPlanogram.CustomAttributes.Value8;
                customAttributeDataItem.Value9 = ccmPlanogram.CustomAttributes.Value9;
                customAttributeDataItem.Value10 = ccmPlanogram.CustomAttributes.Value10;
                customAttributeDataItem.Value11 = ccmPlanogram.CustomAttributes.Value11;
                customAttributeDataItem.Value12 = ccmPlanogram.CustomAttributes.Value12;
                customAttributeDataItem.Value13 = ccmPlanogram.CustomAttributes.Value13;
                customAttributeDataItem.Value14 = ccmPlanogram.CustomAttributes.Value14;
                customAttributeDataItem.Value15 = ccmPlanogram.CustomAttributes.Value15;
                customAttributeDataItem.Value16 = ccmPlanogram.CustomAttributes.Value16;
                customAttributeDataItem.Value17 = ccmPlanogram.CustomAttributes.Value17;
                customAttributeDataItem.Value18 = ccmPlanogram.CustomAttributes.Value18;
                customAttributeDataItem.Value19 = ccmPlanogram.CustomAttributes.Value19;
                customAttributeDataItem.Value20 = ccmPlanogram.CustomAttributes.Value20;
                customAttributeDataItem.Value21 = ccmPlanogram.CustomAttributes.Value21;
                customAttributeDataItem.Value22 = ccmPlanogram.CustomAttributes.Value22;
                customAttributeDataItem.Value23 = ccmPlanogram.CustomAttributes.Value23;
                customAttributeDataItem.Value24 = ccmPlanogram.CustomAttributes.Value24;
                customAttributeDataItem.Value25 = ccmPlanogram.CustomAttributes.Value25;
                customAttributeDataItem.Value26 = ccmPlanogram.CustomAttributes.Value26;
                customAttributeDataItem.Value27 = ccmPlanogram.CustomAttributes.Value27;
                customAttributeDataItem.Value28 = ccmPlanogram.CustomAttributes.Value28;
                customAttributeDataItem.Value29 = ccmPlanogram.CustomAttributes.Value29;
                customAttributeDataItem.Value30 = ccmPlanogram.CustomAttributes.Value30;
                customAttributeDataItem.Value31 = ccmPlanogram.CustomAttributes.Value31;
                customAttributeDataItem.Value32 = ccmPlanogram.CustomAttributes.Value32;
                customAttributeDataItem.Value33 = ccmPlanogram.CustomAttributes.Value33;
                customAttributeDataItem.Value34 = ccmPlanogram.CustomAttributes.Value34;
                customAttributeDataItem.Value35 = ccmPlanogram.CustomAttributes.Value35;
                customAttributeDataItem.Value36 = ccmPlanogram.CustomAttributes.Value36;
                customAttributeDataItem.Value37 = ccmPlanogram.CustomAttributes.Value37;
                customAttributeDataItem.Value38 = ccmPlanogram.CustomAttributes.Value38;
                customAttributeDataItem.Value39 = ccmPlanogram.CustomAttributes.Value39;
                customAttributeDataItem.Value40 = ccmPlanogram.CustomAttributes.Value40;
                customAttributeDataItem.Value41 = ccmPlanogram.CustomAttributes.Value41;
                customAttributeDataItem.Value42 = ccmPlanogram.CustomAttributes.Value42;
                customAttributeDataItem.Value43 = ccmPlanogram.CustomAttributes.Value43;
                customAttributeDataItem.Value44 = ccmPlanogram.CustomAttributes.Value44;
                customAttributeDataItem.Value45 = ccmPlanogram.CustomAttributes.Value45;
                customAttributeDataItem.Value46 = ccmPlanogram.CustomAttributes.Value46;
                customAttributeDataItem.Value47 = ccmPlanogram.CustomAttributes.Value47;
                customAttributeDataItem.Value48 = ccmPlanogram.CustomAttributes.Value48;
                customAttributeDataItem.Value49 = ccmPlanogram.CustomAttributes.Value49;
                customAttributeDataItem.Value50 = ccmPlanogram.CustomAttributes.Value50;

                customAttributeDataItem.Flag1 = ccmPlanogram.CustomAttributes.Flag1;
                customAttributeDataItem.Flag2 = ccmPlanogram.CustomAttributes.Flag2;
                customAttributeDataItem.Flag3 = ccmPlanogram.CustomAttributes.Flag3;
                customAttributeDataItem.Flag4 = ccmPlanogram.CustomAttributes.Flag4;
                customAttributeDataItem.Flag5 = ccmPlanogram.CustomAttributes.Flag5;
                customAttributeDataItem.Flag6 = ccmPlanogram.CustomAttributes.Flag6;
                customAttributeDataItem.Flag7 = ccmPlanogram.CustomAttributes.Flag7;
                customAttributeDataItem.Flag8 = ccmPlanogram.CustomAttributes.Flag8;
                customAttributeDataItem.Flag9 = ccmPlanogram.CustomAttributes.Flag9;
                customAttributeDataItem.Flag10 = ccmPlanogram.CustomAttributes.Flag10;

                customAttributeDataItem.Date1 = ccmPlanogram.CustomAttributes.Date1;
                customAttributeDataItem.Date2 = ccmPlanogram.CustomAttributes.Date2;
                customAttributeDataItem.Date3 = ccmPlanogram.CustomAttributes.Date3;
                customAttributeDataItem.Date4 = ccmPlanogram.CustomAttributes.Date4;
                customAttributeDataItem.Date5 = ccmPlanogram.CustomAttributes.Date5;
                customAttributeDataItem.Date6 = ccmPlanogram.CustomAttributes.Date6;
                customAttributeDataItem.Date7 = ccmPlanogram.CustomAttributes.Date7;
                customAttributeDataItem.Date8 = ccmPlanogram.CustomAttributes.Date8;
                customAttributeDataItem.Date9 = ccmPlanogram.CustomAttributes.Date9;
                customAttributeDataItem.Date10 = ccmPlanogram.CustomAttributes.Date10;

                customAttributeDataItem.Note1 = ccmPlanogram.CustomAttributes.Note1;
                customAttributeDataItem.Note2 = ccmPlanogram.CustomAttributes.Note2;
                customAttributeDataItem.Note3 = ccmPlanogram.CustomAttributes.Note3;
                customAttributeDataItem.Note4 = ccmPlanogram.CustomAttributes.Note4;
                customAttributeDataItem.Note5 = ccmPlanogram.CustomAttributes.Note5;

                output.CustomAttributes = customAttributeDataItem;
            }

            #endregion CustomAttributes


            //If we have recalculated the planogram bounds then use that value.
            if (planogramBounds.HasValue)
            {
                output.Height = planogramBounds.Value.Height;
                output.Width = planogramBounds.Value.Width;
                output.Depth = planogramBounds.Value.Depth;
            }
            else
            {
                output.Height = ccmPlanogram.Height;
                output.Width = ccmPlanogram.Width;
                output.Depth = ccmPlanogram.Depth;
            }
            return output;
        }

        /// <summary>
        /// Converts a ccm subcomponent into a gfs subcomponent
        /// </summary>
        /// <param name="ccmSubComponent"></param>
        /// <param name="componentType"></param>
        /// <returns></returns>
        private static GFS.PlanogramSubComponent ConvertToSubComponent(PlanogramSubComponent ccmSubComponent, PlanogramComponentType componentType)
        {            
            GFS.PlanogramSubComponent gfsSubComponent = new GFS.PlanogramSubComponent();
            gfsSubComponent.Id = System.Convert.ToInt32(ccmSubComponent.Id);
            gfsSubComponent.Name = ValidName(ccmSubComponent.Name);
            gfsSubComponent.Height = ccmSubComponent.Height;
            gfsSubComponent.Width = ccmSubComponent.Width;
            gfsSubComponent.Depth = ccmSubComponent.Depth;
            gfsSubComponent.X = ccmSubComponent.X;
            gfsSubComponent.Y = ccmSubComponent.Y;
            gfsSubComponent.Z = ccmSubComponent.Z + ccmSubComponent.Depth; //TODO THIS WONT WORK IF ROTATED
            gfsSubComponent.Slope = ccmSubComponent.Slope;
            gfsSubComponent.Angle = ccmSubComponent.Angle;
            gfsSubComponent.Roll = ccmSubComponent.Roll;
            gfsSubComponent.ShapeType = ccmSubComponent.ShapeType.ToString();
            gfsSubComponent.MerchandisableHeight = ccmSubComponent.MerchandisableHeight;
            gfsSubComponent.IsVisible = ccmSubComponent.IsVisible;
            gfsSubComponent.HasCollisionDetection = ccmSubComponent.HasCollisionDetection;
            gfsSubComponent.FillPatternTypeFront = PlanogramSubComponentFillPatternType.Solid.ToString();
            gfsSubComponent.FillPatternTypeBack = PlanogramSubComponentFillPatternType.Solid.ToString();
            gfsSubComponent.FillPatternTypeTop = PlanogramSubComponentFillPatternType.Solid.ToString();
            gfsSubComponent.FillPatternTypeBottom = PlanogramSubComponentFillPatternType.Solid.ToString();
            gfsSubComponent.FillPatternTypeLeft = PlanogramSubComponentFillPatternType.Solid.ToString();
            gfsSubComponent.FillPatternTypeRight = PlanogramSubComponentFillPatternType.Solid.ToString();
            gfsSubComponent.LineColour = ccmSubComponent.LineColour;
            gfsSubComponent.TransparencyPercentFront = ccmSubComponent.TransparencyPercentFront;
            gfsSubComponent.TransparencyPercentBack = ccmSubComponent.TransparencyPercentBack;
            gfsSubComponent.TransparencyPercentTop = ccmSubComponent.TransparencyPercentTop;
            gfsSubComponent.TransparencyPercentBottom = ccmSubComponent.TransparencyPercentBottom;
            gfsSubComponent.TransparencyPercentLeft = ccmSubComponent.TransparencyPercentLeft;
            gfsSubComponent.TransparencyPercentRight = ccmSubComponent.TransparencyPercentRight;
            gfsSubComponent.FaceThicknessTop = ccmSubComponent.FaceThicknessTop;
            gfsSubComponent.FaceThickness = ccmSubComponent.FaceThicknessFront;

            #region FaceThickness F/B/B/L/R are not covered in GFS
            //gfsSubComponent.FaceThicknessFront = ccmSubComponent.FaceThicknessFront;
            //gfsSubComponent.FaceThicknessBack = ccmSubComponent.FaceThicknessBack;
            //gfsSubComponent.FaceThicknessBottom = ccmSubComponent.FaceThicknessBottom;
            //gfsSubComponent.FaceThicknessLeft = ccmSubComponent.FaceThicknessLeft;
            //gfsSubComponent.FaceThicknessRight = ccmSubComponent.FaceThicknessRight;
            #endregion

            gfsSubComponent.RiserHeight = ccmSubComponent.RiserHeight;
            gfsSubComponent.IsRiserPlacedOnRight = ccmSubComponent.IsRiserPlacedOnRight;
            gfsSubComponent.IsRiserPlacedOnLeft = ccmSubComponent.IsRiserPlacedOnLeft;
            gfsSubComponent.IsRiserPlacedOnBack = ccmSubComponent.IsRiserPlacedOnBack;
            gfsSubComponent.IsRiserPlacedOnFront = ccmSubComponent.IsRiserPlacedOnFront;
            gfsSubComponent.RiserFillPatternType = PlanogramSubComponentFillPatternType.Solid.ToString();
            gfsSubComponent.NotchStartX = ccmSubComponent.NotchStartX;
            gfsSubComponent.NotchSpacingX = ccmSubComponent.NotchSpacingX;
            gfsSubComponent.NotchHeight = ccmSubComponent.NotchHeight;
            gfsSubComponent.IsNotchPlacedOnRight = ccmSubComponent.IsNotchPlacedOnRight;
            gfsSubComponent.IsNotchPlacedOnLeft = ccmSubComponent.IsNotchPlacedOnLeft;
            gfsSubComponent.IsNotchPlacedOnBack = ccmSubComponent.IsNotchPlacedOnBack;
            gfsSubComponent.IsNotchPlacedOnFront = ccmSubComponent.IsNotchPlacedOnFront;
            gfsSubComponent.NotchStyleType = ccmSubComponent.NotchStyleType.ToString();
            gfsSubComponent.DividerObstructionHeight = ccmSubComponent.DividerObstructionHeight;
            gfsSubComponent.DividerObstructionWidth = ccmSubComponent.DividerObstructionWidth;
            gfsSubComponent.DividerObstructionStartX = ccmSubComponent.DividerObstructionStartX;
            gfsSubComponent.DividerObstructionSpacingX = ccmSubComponent.DividerObstructionSpacingX;
            gfsSubComponent.DividerObstructionStartY = ccmSubComponent.DividerObstructionStartY;
            gfsSubComponent.DividerObstructionSpacingY = ccmSubComponent.DividerObstructionSpacingY;
            gfsSubComponent.MerchConstraintRow1StartX = ccmSubComponent.MerchConstraintRow1StartX;
            gfsSubComponent.MerchConstraintRow1SpacingX = ccmSubComponent.MerchConstraintRow1SpacingX;
            gfsSubComponent.MerchConstraintRow1StartY = ccmSubComponent.MerchConstraintRow1StartY;
            gfsSubComponent.MerchConstraintRow1SpacingY = ccmSubComponent.MerchConstraintRow1SpacingY;
            gfsSubComponent.MerchConstraintRow2StartX = ccmSubComponent.MerchConstraintRow2StartX;
            gfsSubComponent.MerchConstraintRow2SpacingX = ccmSubComponent.MerchConstraintRow2SpacingX;
            gfsSubComponent.MerchConstraintRow2StartY = ccmSubComponent.MerchConstraintRow2StartY;
            gfsSubComponent.MerchConstraintRow2SpacingY = ccmSubComponent.MerchConstraintRow2SpacingY;

            #region Unused in GFS
            //gfsSubComponent.CombineType = ccmSubComponent.CombineType;
            //gfsSubComponent.RiserColour = ccmSubComponent.RiserColour;
            //gfsSubComponent.RiserTransparencyPercent = ccmSubComponent.RiserTransparencyPercent;
            //gfsSubComponent.NotchStartY = ccmSubComponent.NotchStartY;
            //gfsSubComponent.NotchSpacingY = ccmSubComponent.NotchSpacingY;
            //gfsSubComponent.NotchWidth = ccmSubComponent.NotchWidth;
            //gfsSubComponent.MerchConstraintRow1Height = ccmSubComponent.MerchConstraintRow1Height;
            //gfsSubComponent.MerchConstraintRow1Width = ccmSubComponent.MerchConstraintRow1Width;
            //gfsSubComponent.MerchConstraintRow2Height = ccmSubComponent.MerchConstraintRow2Height;
            //gfsSubComponent.MerchConstraintRow2Width = ccmSubComponent.MerchConstraintRow2Width;
            //gfsSubComponent.MerchandisingStrategyX =ccmSubComponent.MerchandisingStrategyX;
            //gfsSubComponent.MerchandisingStrategyY = ccmSubComponent.MerchandisingStrategyY;
            //gfsSubComponent.MerchandisingStrategyZ = ccmSubComponent.MerchandisingStrategyZ;
            //gfsSubComponent.IsProductOverlapAllowed = ccmSubComponent.IsProductOverlapAllowed;
            #endregion

            
            gfsSubComponent.FillColourFront = ccmSubComponent.FillColourFront;
            gfsSubComponent.FillColourBack = ccmSubComponent.FillColourBack;
            gfsSubComponent.FillColourTop = ccmSubComponent.FillColourTop;
            gfsSubComponent.FillColourBottom = ccmSubComponent.FillColourBottom;
            gfsSubComponent.FillColourLeft = ccmSubComponent.FillColourLeft;
            gfsSubComponent.FillColourRight = ccmSubComponent.FillColourRight;

            switch (componentType)
            {
                case PlanogramComponentType.Backboard: //backboard
                    gfsSubComponent.IsMerchandisable = false;
                    //gfsSubComponent.SubComponentType = PlanogramComponentType.Backboard.ToString();
                    break;

                case PlanogramComponentType.Shelf: //shelf
                    gfsSubComponent.IsMerchandisable = true;
                    gfsSubComponent.SubComponentType = PlanogramComponentType.Shelf.ToString();
                    break;

                case PlanogramComponentType.Chest: //chest
                    gfsSubComponent.IsMerchandisable = true;
                    gfsSubComponent.SubComponentType = PlanogramComponentType.Chest.ToString();
                    break;

                case PlanogramComponentType.Bar: //bar
                    gfsSubComponent.IsMerchandisable = true;
                    gfsSubComponent.SubComponentType = PlanogramComponentType.Bar.ToString();
                    break;

                case PlanogramComponentType.Peg: //peg
                    gfsSubComponent.IsMerchandisable = true;
                    gfsSubComponent.SubComponentType = PlanogramComponentType.Peg.ToString();
                    break;

                case PlanogramComponentType.Rod: //rod
                    gfsSubComponent.IsMerchandisable = true;
                    // gfsSubComponent.SubComponentType = PlanogramComponentType.Rod.ToString();
                    break;

                case PlanogramComponentType.SlotWall: // send as peg
                    gfsSubComponent.IsMerchandisable = true;
                    gfsSubComponent.SubComponentType = PlanogramComponentType.Peg.ToString();
                    break;

                case PlanogramComponentType.ClipStrip:
                    gfsSubComponent.IsMerchandisable = true;
                    gfsSubComponent.SubComponentType = PlanogramComponentType.Peg.ToString();
                    break;
                case PlanogramComponentType.Pallet:
                    gfsSubComponent.IsMerchandisable = true;
                    gfsSubComponent.SubComponentType = PlanogramComponentType.Shelf.ToString();
                    break;

                default:
                    gfsSubComponent.IsMerchandisable = ccmSubComponent.MerchandisingType != PlanogramSubComponentMerchandisingType.None;
                    //gfsSubComponent.SubComponentType = PlanogramComponentType.Custom.ToString();
                    break;
            }

            return gfsSubComponent;
        }

        private static List<GFS.PlanogramPosition> CreatePositions(Planogram ccmPlanogram, PlanogramPosition ccmPosition, Dictionary<Object, Int32> fixtureComponentToDummyFixtureAssemblyIds)
        {
            List<GFS.PlanogramPosition> output = new List<GFS.PlanogramPosition>();
           // PlanogramPosition ccmPosition = ccmPosition.Position;
            PlanogramProduct posProduct = ccmPosition.GetPlanogramProduct();
            PlanogramSubComponentPlacement ccmSubPlacement = ccmPosition.GetPlanogramSubComponentPlacement();
            if (ccmSubPlacement == null) return output;
            PlanogramSubComponent posSubComponent = ccmSubPlacement.SubComponent;
            PlanogramPositionDetails ccmPositionDetails = ccmPosition.GetPositionDetails();

            //Create the main position
            CcmPositionDetailPart mainPart = new CcmPositionDetailPart(ccmPosition, ccmPositionDetails, CcmPositionDetailPartType.Main);
            List<GFS.PlanogramPosition> gfsMainPositions = new List<GFS.PlanogramPosition>();            
            foreach (CcmPositionDetailPart part in mainPart.SplitFacingsIntoBytes())
            {
                gfsMainPositions.Add(CreatePosition(ccmPlanogram, ccmPosition, ccmPositionDetails, posProduct, part, posSubComponent, fixtureComponentToDummyFixtureAssemblyIds));
            }
            
            //Create the position capped in the X plane
            CcmPositionDetailPart xPart = new CcmPositionDetailPart(ccmPosition, ccmPositionDetails, CcmPositionDetailPartType.XCap);
            List<GFS.PlanogramPosition> gfsXPositions = new List<GFS.PlanogramPosition>();
            foreach (CcmPositionDetailPart part in xPart.SplitFacingsIntoBytes())
            {
                gfsXPositions.Add(CreatePosition(ccmPlanogram, ccmPosition, ccmPositionDetails, posProduct, part, posSubComponent, fixtureComponentToDummyFixtureAssemblyIds));
            }

            //Create the position capped in the Y plane
            CcmPositionDetailPart yPart = new CcmPositionDetailPart(ccmPosition, ccmPositionDetails, CcmPositionDetailPartType.YCap);
            List<GFS.PlanogramPosition> gfsYPositions = new List<GFS.PlanogramPosition>();
            foreach (CcmPositionDetailPart part in yPart.SplitFacingsIntoBytes())
            {
                gfsYPositions.Add(CreatePosition(ccmPlanogram, ccmPosition, ccmPositionDetails, posProduct, part, posSubComponent, fixtureComponentToDummyFixtureAssemblyIds));
            }

            //Create the position capped in the Z plane
            CcmPositionDetailPart zPart = new CcmPositionDetailPart(ccmPosition, ccmPositionDetails, CcmPositionDetailPartType.ZCap);
            List<GFS.PlanogramPosition> gfsZPositions = new List<GFS.PlanogramPosition>();
            foreach (CcmPositionDetailPart part in zPart.SplitFacingsIntoBytes())
            {
                gfsZPositions.Add(CreatePosition(ccmPlanogram, ccmPosition, ccmPositionDetails, posProduct, part, posSubComponent, fixtureComponentToDummyFixtureAssemblyIds));
            }

            MergeRightCaps(ccmPosition, posProduct, gfsMainPositions, gfsXPositions);
            MergeTopCaps(ccmPosition, posProduct, gfsMainPositions, gfsYPositions);
            MergeBreakBack(ccmPosition, posProduct, gfsMainPositions, gfsZPositions);
   
            output.AddRange(gfsMainPositions);
            output.AddRange(gfsXPositions);
            output.AddRange(gfsYPositions);
            output.AddRange(gfsZPositions);

            return output;
        }

        /// <summary>
        /// Calculates the total units for a position
        /// </summary>
        /// <remarks>Loosly copied from PlangormPostionDetails.CalculateTotalUnits</remarks>
        private static Int16 CalculateTotalUnits(PlanogramProduct product, PlanogramPositionMerchandisingStyle merchStyle, Int16 high, Int16 wide, Int16 deep)
        {

            Int16 mainUnitsHigh = high;
            Int16 mainUnitsWide = wide;
            Int16 mainUnitsDeep = deep;
            Int32 mainTotalUnits = Convert.ToInt32(mainUnitsHigh * mainUnitsWide * mainUnitsDeep);

            if (merchStyle == PlanogramPositionMerchandisingStyle.Tray ||
                (merchStyle == PlanogramPositionMerchandisingStyle.Default && product.MerchandisingStyle == PlanogramProductMerchandisingStyle.Tray))
            {
                mainUnitsHigh *= ((product.TrayHigh > 0) ? product.TrayHigh : (Int16)1);
                mainUnitsWide *= ((product.TrayWide > 0) ? product.TrayWide : (Int16)1);
                mainUnitsDeep *= ((product.TrayDeep > 0) ? product.TrayDeep : (Int16)1);

                mainTotalUnits =
                    (product.TrayPackUnits > 1) ? mainTotalUnits * product.TrayPackUnits
                    : Convert.ToInt32(mainUnitsHigh * mainUnitsWide * mainUnitsDeep);
            }
            else if (merchStyle == PlanogramPositionMerchandisingStyle.Case ||
                (merchStyle == PlanogramPositionMerchandisingStyle.Default && product.MerchandisingStyle == PlanogramProductMerchandisingStyle.Case))
            {
                mainUnitsHigh *= ((product.CaseHigh > 0) ? product.CaseHigh : (Int16)1);
                mainUnitsWide *= ((product.CaseWide > 0) ? product.CaseWide : (Int16)1);
                mainUnitsDeep *= ((product.CaseDeep > 0) ? product.CaseDeep : (Int16)1);

                mainTotalUnits =
                    (product.CasePackUnits > 1) ? mainTotalUnits * product.CasePackUnits
                    : Convert.ToInt32(mainUnitsHigh * mainUnitsWide * mainUnitsDeep);
            }

            return Convert.ToInt16(mainTotalUnits);

        }


        /// <summary>
        /// Create a GFS position from a ccm position. Where the GFS positon is only a part of the CCM position, either the main position or one of the
        /// XYZ capping positions.
        /// </summary>
        /// <param name="ccmPosition">The original ccm position</param>
        /// <param name="posProduct">The ccm product</param>
        /// <param name="detailPart">The ccm position part either main or XYZ capping</param>
        /// <param name="posSubComponent">sub component the position sits on</param>
        /// <param name="fixtureComponentToDummyFixtureAssemblyIds">collection of dummy fixture assembly ids to use instead of fixture component Ids</param>
        /// <returns></returns>
        private static GFS.PlanogramPosition CreatePosition(Planogram ccmPlanogram, PlanogramPosition ccmPosition, PlanogramPositionDetails ccmPositionDetails, PlanogramProduct posProduct, CcmPositionDetailPart detailPart, PlanogramSubComponent posSubComponent, Dictionary<Object, Int32> fixtureComponentToDummyFixtureAssemblyIds)
        {
        
            Int16 componentSquenceNumber = 0;
            Int16 baySquenceNumber = 0;
            GFS.PlanogramPosition gfsPosition = new GFS.PlanogramPosition();
            PlanogramFixtureItem ccmFixtureItem = ccmPlanogram.GetPlanogramFixtureItem(ccmPosition.PlanogramFixtureItemId);
            if (ccmFixtureItem != null) baySquenceNumber = ccmFixtureItem.BaySequenceNumber;

            gfsPosition.Id = System.Convert.ToInt32(ccmPosition.Id);
            gfsPosition.MerchandisingStyle = detailPart.MerchStyle.ToString();

            //link to parents
            gfsPosition.PlanogramFixtureItemId = Convert.ToInt32(ccmPosition.PlanogramFixtureItemId);
            gfsPosition.PlanogramSubComponentId = Convert.ToInt32(ccmPosition.PlanogramSubComponentId);
            gfsPosition.PlanogramProductId = Convert.ToInt32(ccmPosition.PlanogramProductId);

            //If we have a fixture component Id then we need to make use of a dummy fixture assembly
            //as GFS currently does not have fixture components.
            if (ccmPosition.PlanogramFixtureComponentId != null)
            {
                gfsPosition.PlanogramFixtureAssemblyId = fixtureComponentToDummyFixtureAssemblyIds[ccmPosition.PlanogramFixtureComponentId];
                gfsPosition.PlanogramAssemblyComponentId = Convert.ToInt32(ccmPosition.PlanogramFixtureComponentId);

                PlanogramFixtureComponent fixtureComponent = ccmPlanogram.GetPlanogramFixtureComponent(ccmPosition.PlanogramFixtureComponentId);

                if (fixtureComponent != null && fixtureComponent.ComponentSequenceNumber.HasValue)
                {
                    componentSquenceNumber = fixtureComponent.ComponentSequenceNumber.Value;
                }

            }
            else
            {
                gfsPosition.PlanogramFixtureAssemblyId = Convert.ToInt32(ccmPosition.PlanogramFixtureAssemblyId);
                gfsPosition.PlanogramAssemblyComponentId = Convert.ToInt32(ccmPosition.PlanogramAssemblyComponentId);

                PlanogramAssemblyComponent assemblyComponent = ccmPlanogram.GetPlanogramAssemblyComponent(ccmPosition.PlanogramAssemblyComponentId);
                if (assemblyComponent != null && assemblyComponent.ComponentSequenceNumber.HasValue)
                {
                    componentSquenceNumber = assemblyComponent.ComponentSequenceNumber.Value;
                }
            }

            //Set the position coordinates.
            //offset this position part by the overall position coordinates
            gfsPosition.X = ccmPosition.X + detailPart.Coordinates.X;
            gfsPosition.Y = ccmPosition.Y + detailPart.Coordinates.Y;
            //The Z position of GFS positions is set at the front of the position instead of the back
            //so we need to take this into account.
            Single newZ = -((posSubComponent.Depth - ccmPosition.Z) - ccmPositionDetails.MainTotalSize.Depth);
            gfsPosition.Z = newZ + detailPart.Coordinates.Z;

            //The total units high wide deep for the position
            //as it is only a part of the position it will be
            //the same as the front high/wide/deep
            gfsPosition.UnitsHigh = detailPart.High;
            gfsPosition.UnitsWide = detailPart.Wide;
            gfsPosition.UnitsDeep = detailPart.Deep;

            gfsPosition.FrontHigh = Convert.ToByte(detailPart.High);
            gfsPosition.FrontWide = Convert.ToByte(detailPart.Wide);
            gfsPosition.FrontDeep = Convert.ToByte(detailPart.Deep);

            gfsPosition.TotalUnits = CalculateTotalUnits(posProduct, detailPart.MerchStyle, detailPart.High, detailPart.Wide, detailPart.Deep);

            SetOrientationValues(gfsPosition, detailPart.OrientationType);
            
            //Set tray values if we are dealing with a tray.
            if (detailPart.MerchStyle == PlanogramPositionMerchandisingStyle.Tray ||
                (detailPart.MerchStyle == PlanogramPositionMerchandisingStyle.Default && posProduct.MerchandisingStyle == PlanogramProductMerchandisingStyle.Tray))
            {
                gfsPosition.TraysHigh = Convert.ToByte(detailPart.High);
                gfsPosition.TraysWide = Convert.ToByte(detailPart.Wide);
                gfsPosition.TraysDeep = Convert.ToByte(detailPart.Deep);

                WidthHeightDepthValue highWideDeep = ProductOrientationHelper.GetOrientatedSize(detailPart.OrientationType, posProduct.TrayWide, posProduct.TrayHigh, posProduct.TrayDeep);
                gfsPosition.UnitsHigh *= Math.Max((Int16)1, (Int16)highWideDeep.Height);
                gfsPosition.UnitsWide *= Math.Max((Int16)1, (Int16)highWideDeep.Width);
                gfsPosition.UnitsDeep *= Math.Max((Int16)1, (Int16)highWideDeep.Depth);
                gfsPosition.FrontHigh *= Math.Max((Byte)1, (Byte)highWideDeep.Height);
                gfsPosition.FrontWide *= Math.Max((Byte)1, (Byte)highWideDeep.Width);
                gfsPosition.FrontDeep *= Math.Max((Byte)1, (Byte)highWideDeep.Depth);

                gfsPosition.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Tray.ToString();
            }

            //gfs no longer requires break tray down
            
            gfsPosition.BaySequenceNumber = baySquenceNumber;
            gfsPosition.ElementSequenceNumber = componentSquenceNumber;
            gfsPosition.PositionSequenceNumber = (ccmPosition.PositionSequenceNumber.HasValue)? ccmPosition.PositionSequenceNumber.Value : (Int16)0;

            //V8-31233 : New position properties to be published
            gfsPosition.PlanogramX = ccmPosition.MetaWorldX;
            gfsPosition.PlanogramY = ccmPosition.MetaWorldY;
            gfsPosition.PlanogramZ = ccmPosition.MetaWorldZ;
            gfsPosition.MetaTotalLinearSpace = ccmPosition.MetaTotalLinearSpace;
            gfsPosition.MetaTotalAreaSpace = ccmPosition.MetaTotalAreaSpace;
            gfsPosition.MetaTotalVolumetricSpace = ccmPosition.MetaTotalVolumetricSpace;
             

            return gfsPosition;
        }

        private static void MergeRightCaps(PlanogramPosition ccmPosition, PlanogramProduct posProduct, List<GFS.PlanogramPosition> gfsMainPositions, List<GFS.PlanogramPosition> gfsXPositions)
        {
            //Xplacment must be to the right of the main placement
            if (!ccmPosition.IsXPlacedLeft && gfsMainPositions.Count > 0 && gfsXPositions.Count > 0)
            {
                List<GFS.PlanogramPosition> mainRightPositions = new List<GFS.PlanogramPosition>();
                List<GFS.PlanogramPosition> xLeftPositions = new List<GFS.PlanogramPosition>();

                #region createPositon Lists
                Single? lastX = null;
                foreach (GFS.PlanogramPosition pos in gfsMainPositions.OrderByDescending(p => p.X))
                {
                    if (lastX.HasValue)
                    {
                        if (lastX == pos.X)
                        {
                            mainRightPositions.Add(pos);
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        mainRightPositions.Add(pos);
                    }

                    lastX = pos.X;
                }

                lastX = null;
                foreach (GFS.PlanogramPosition pos in gfsXPositions.OrderBy(p => p.X))
                {
                    if (lastX.HasValue)
                    {
                        if (lastX == pos.X)
                        {
                            xLeftPositions.Add(pos);
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        xLeftPositions.Add(pos);
                    }

                    lastX = pos.X;
                }
                #endregion

                GFS.PlanogramPosition main = mainRightPositions.OrderByDescending(p => p.Z).First();
                GFS.PlanogramPosition cap = xLeftPositions.OrderByDescending(p => p.Z).First();

                if (RightCapCheck(ccmPosition, posProduct))
                {
                    if (cap.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Tray.ToString())
                    {
                        main.TraysRightHigh = cap.TraysHigh;
                        main.TraysRightWide = cap.TraysWide;
                        main.TraysRightDeep = cap.TraysDeep;
                    }
                    main.FrontRightHigh = cap.FrontHigh;
                    main.FrontRightWide = cap.FrontWide;
                    main.FrontRightDeep = cap.FrontDeep;
                    main.TotalUnits += cap.TotalUnits;
                    main.UnitsHigh = Math.Max(main.UnitsHigh, cap.UnitsHigh);
                    main.UnitsWide += cap.FrontWide;
                    main.UnitsDeep = Math.Max(main.UnitsDeep, cap.UnitsDeep);
                    
                    gfsXPositions.Remove(cap);
                }
                else if (BreakUpCheck(ccmPosition, posProduct))
                {
                    main.TrayBreakUpHigh = cap.FrontHigh;
                    main.TrayBreakUpWide = cap.FrontWide;
                    main.TrayBreakUpDeep = cap.FrontDeep;
                    main.TotalUnits += cap.TotalUnits;
                    main.UnitsHigh = Math.Max(main.UnitsHigh, cap.UnitsHigh);
                    main.UnitsWide += cap.FrontWide;
                    main.UnitsDeep = Math.Max(main.UnitsDeep, cap.UnitsDeep);

                    gfsXPositions.Remove(cap);
                }
            }
        }

        private static void MergeTopCaps(PlanogramPosition ccmPosition, PlanogramProduct posProduct, List<GFS.PlanogramPosition> gfsMainPositions, List<GFS.PlanogramPosition> gfsYPositions)
        {
            //Xplacment must be to the right of the main placement
            if (!ccmPosition.IsYPlacedBottom && gfsMainPositions.Count > 0 && gfsYPositions.Count > 0)
            {
                List<GFS.PlanogramPosition> mainTopPositions = new List<GFS.PlanogramPosition>();
                List<GFS.PlanogramPosition> yBottomPositions = new List<GFS.PlanogramPosition>();

                #region createPositon Lists
                Single? lastY = null;
                foreach (GFS.PlanogramPosition pos in gfsMainPositions.OrderByDescending(p => p.Y))
                {
                    if (lastY.HasValue)
                    {
                        if (lastY == pos.Y)
                        {
                            mainTopPositions.Add(pos);
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        mainTopPositions.Add(pos);
                    }

                    lastY = pos.Y;
                }

                lastY = null;
                foreach (GFS.PlanogramPosition pos in gfsYPositions.OrderBy(p => p.Y))
                {
                    if (lastY.HasValue)
                    {
                        if (lastY == pos.Y)
                        {
                            yBottomPositions.Add(pos);
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        yBottomPositions.Add(pos);
                    }

                    lastY = pos.Y;
                }
                #endregion

                GFS.PlanogramPosition main = mainTopPositions.OrderBy(p => p.X).ThenByDescending(p => p.Z).First();
                GFS.PlanogramPosition cap = yBottomPositions.OrderBy(p => p.X).ThenByDescending(p => p.Z).First();

                if (TopCapCheck(ccmPosition, posProduct))
                {
                    main.FrontTopCapHigh = cap.FrontHigh;
                    main.FrontTopCapWide = cap.FrontWide;
                    main.FrontTopCapDeep = cap.FrontDeep;
                    main.TotalUnits += cap.TotalUnits;
                    main.UnitsHigh += cap.FrontHigh;
                    main.UnitsWide = Math.Max(main.UnitsWide, main.UnitsWide);
                    main.UnitsDeep = Math.Max(main.UnitsDeep, main.UnitsDeep);

                    gfsYPositions.Remove(cap);
                }
                else if (BreakTopCheck(ccmPosition, posProduct))
                {
                    main.TrayBreakTopHigh = cap.FrontHigh;
                    main.TrayBreakTopWide = cap.FrontWide;
                    main.TrayBreakTopDeep = cap.FrontDeep;
                    main.TotalUnits += cap.TotalUnits;
                    main.UnitsHigh += cap.FrontHigh;
                    main.UnitsWide = Math.Max(main.UnitsWide, main.UnitsWide);
                    main.UnitsDeep = Math.Max(main.UnitsDeep, main.UnitsDeep);

                    gfsYPositions.Remove(cap);
                }
            }
        }

        private static void MergeBreakBack(PlanogramPosition ccmPosition, PlanogramProduct posProduct, List<GFS.PlanogramPosition> gfsMainPositions, List<GFS.PlanogramPosition> gfsZPositions)
        {
            //Xplacment must be to the right of the main placement
            if (!ccmPosition.IsZPlacedFront && gfsMainPositions.Count > 0 && gfsZPositions.Count > 0)
            {
                List<GFS.PlanogramPosition> mainBackPositions = new List<GFS.PlanogramPosition>();
                List<GFS.PlanogramPosition> zfrontPositions = new List<GFS.PlanogramPosition>();

                #region createPositon Lists
                Single? lastZ = null;
                foreach (GFS.PlanogramPosition pos in gfsMainPositions.OrderBy(p => p.Z))
                {
                    if (lastZ.HasValue)
                    {
                        if (lastZ == pos.Z)
                        {
                            mainBackPositions.Add(pos);
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        mainBackPositions.Add(pos);
                    }

                    lastZ = pos.Z;
                }

                lastZ = null;
                foreach (GFS.PlanogramPosition pos in gfsZPositions.OrderByDescending(p => p.Z))
                {
                    if (lastZ.HasValue)
                    {
                        if (lastZ == pos.Z)
                        {
                            zfrontPositions.Add(pos);
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        zfrontPositions.Add(pos);
                    }

                    lastZ = pos.Z;
                }
                #endregion

                GFS.PlanogramPosition main = mainBackPositions.OrderBy(p => p.X).ThenBy(p => p.Y).First();
                GFS.PlanogramPosition cap = zfrontPositions.OrderBy(p => p.X).ThenBy(p => p.Y).First();

                if (BreakBackCheck(ccmPosition, posProduct))
                {
                    main.TrayBreakBackHigh = cap.FrontHigh;
                    main.TrayBreakBackWide = cap.FrontWide;
                    main.TrayBreakBackDeep = cap.FrontDeep;
                    main.TotalUnits += cap.TotalUnits;
                    main.UnitsHigh = Math.Max(main.UnitsHigh, main.FrontHigh);
                    main.UnitsWide = Math.Max(main.UnitsWide, main.UnitsWide);
                    main.UnitsDeep += cap.FrontDeep;

                    gfsZPositions.Remove(cap);
                }
            }
        }


        /// <summary>
        /// Takes a position orientation type and uses it to set the rotation values
        /// of a gfs position so that it will be displayed with that orientation.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="orientation"></param>
        private static void SetOrientationValues(GFS.PlanogramPosition position, PlanogramPositionOrientationType orientation)
        {
            switch (orientation)
            {
                case PlanogramPositionOrientationType.Back90:
                case PlanogramPositionOrientationType.Back270:
                case PlanogramPositionOrientationType.Front270:
                case PlanogramPositionOrientationType.Front90:
                    position.Roll = (Single)(Math.PI / 2);
                    position.Slope = 0;
                    position.Angle = 0;
                    break;
                case PlanogramPositionOrientationType.Bottom0:
                case PlanogramPositionOrientationType.Bottom180:
                case PlanogramPositionOrientationType.Top0:
                case PlanogramPositionOrientationType.Top180:
                    position.Roll = 0;
                    position.Slope = (Single)(Math.PI / 2);
                    position.Angle = 0;
                    break;
                case PlanogramPositionOrientationType.Bottom90:
                case PlanogramPositionOrientationType.Bottom270:
                case PlanogramPositionOrientationType.Top90:
                case PlanogramPositionOrientationType.Top270:
                    position.Roll = (Single)(Math.PI / 2);
                    position.Slope = 0;
                    position.Angle = (Single)(Math.PI / 2);
                    break;
                case PlanogramPositionOrientationType.Left0:
                case PlanogramPositionOrientationType.Left180:
                case PlanogramPositionOrientationType.Right0:
                case PlanogramPositionOrientationType.Right180:
                    position.Roll = 0;
                    position.Slope = 0;
                    position.Angle = (Single)(Math.PI / 2);
                    break;
                case PlanogramPositionOrientationType.Left90:
                case PlanogramPositionOrientationType.Left270:
                case PlanogramPositionOrientationType.Right90:
                case PlanogramPositionOrientationType.Right270:
                    position.Roll = 0;
                    position.Slope = (Single)(Math.PI / 2);
                    position.Angle = (Single)(Math.PI / 2);
                    break;
                default:
                    position.Roll = 0;
                    position.Slope = 0;
                    position.Angle = 0;
                    break;
            }
        }

        /// <summary>
        /// Validation for item names as GFS requires things to have names
        /// before it will save.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private static String ValidName(String name)
        {
            //Trim name as it will be trimmed in GFS causing spaces to become blanks.
            if (String.IsNullOrEmpty(name.Trim()))
            {
                return Galleria.Ccm.Resources.Language.Message.Publish_DefaultComponentName;
            }

            return name;
        }

        /// <summary>
        /// Filters an orientation type down to the currently available choice
        /// off GFS orientations.
        /// </summary>
        /// <param name="orientationType"></param>
        /// <returns></returns>
        private static PlanogramPositionOrientationType GFSOrientationType(PlanogramPositionOrientationType orientationType, PlanogramProductOrientationType defaultOrientation)
        {
            switch (orientationType)
            {
                case PlanogramPositionOrientationType.Default:
                    switch (defaultOrientation)
                    {
                        case PlanogramProductOrientationType.Back90:
                        case PlanogramProductOrientationType.Back270:
                        case PlanogramProductOrientationType.Front270:
                        case PlanogramProductOrientationType.Front90:
                            return PlanogramPositionOrientationType.Front90;
                        case PlanogramProductOrientationType.Bottom0:
                        case PlanogramProductOrientationType.Bottom180:
                        case PlanogramProductOrientationType.Top0:
                        case PlanogramProductOrientationType.Top180:
                            return PlanogramPositionOrientationType.Top0;
                        case PlanogramProductOrientationType.Bottom90:
                        case PlanogramProductOrientationType.Bottom270:
                        case PlanogramProductOrientationType.Top90:
                        case PlanogramProductOrientationType.Top270:
                            return PlanogramPositionOrientationType.Top90;
                        case PlanogramProductOrientationType.Left0:
                        case PlanogramProductOrientationType.Left180:
                        case PlanogramProductOrientationType.Right0:
                        case PlanogramProductOrientationType.Right180:
                            return PlanogramPositionOrientationType.Right0;
                        case PlanogramProductOrientationType.Left90:
                        case PlanogramProductOrientationType.Left270:
                        case PlanogramProductOrientationType.Right90:
                        case PlanogramProductOrientationType.Right270:
                            return PlanogramPositionOrientationType.Right90;
                        default:
                            return PlanogramPositionOrientationType.Front0;
                  }
                      break;
                case PlanogramPositionOrientationType.Back90:
                case PlanogramPositionOrientationType.Back270:
                case PlanogramPositionOrientationType.Front270:
                case PlanogramPositionOrientationType.Front90:
                    return PlanogramPositionOrientationType.Front90;
                case PlanogramPositionOrientationType.Bottom0:
                case PlanogramPositionOrientationType.Bottom180:
                case PlanogramPositionOrientationType.Top0:
                case PlanogramPositionOrientationType.Top180:
                    return PlanogramPositionOrientationType.Top0;
                case PlanogramPositionOrientationType.Bottom90:
                case PlanogramPositionOrientationType.Bottom270:
                case PlanogramPositionOrientationType.Top90:
                case PlanogramPositionOrientationType.Top270:
                    return PlanogramPositionOrientationType.Top90;
                case PlanogramPositionOrientationType.Left0:
                case PlanogramPositionOrientationType.Left180:
                case PlanogramPositionOrientationType.Right0:
                case PlanogramPositionOrientationType.Right180:
                    return PlanogramPositionOrientationType.Right0;
                case PlanogramPositionOrientationType.Left90:
                case PlanogramPositionOrientationType.Left270:
                case PlanogramPositionOrientationType.Right90:
                case PlanogramPositionOrientationType.Right270:
                    return PlanogramPositionOrientationType.Right90;
                default:
                    return PlanogramPositionOrientationType.Front0;
            }
        }

        /// <summary>
        /// Checks if the X orientation is correct for a right cap.
        /// </summary>
        /// <param name="ccmPosition"></param>
        /// <returns></returns>
        private static Boolean RightCapCheck(PlanogramPosition ccmPosition, PlanogramProduct product)
        {
            PlanogramPositionMerchandisingStyle mainStyle = ccmPosition.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default ? ProductStyleToPositionStyle(product.MerchandisingStyle) : ccmPosition.MerchandisingStyle;
            PlanogramPositionMerchandisingStyle capStyle = ccmPosition.MerchandisingStyleX == PlanogramPositionMerchandisingStyle.Default ? ProductStyleToPositionStyle(product.MerchandisingStyle) : ccmPosition.MerchandisingStyleX;

            //styles must be the same
            if (mainStyle != capStyle) return false;

            switch (GFSOrientationType(ccmPosition.OrientationType, product.OrientationType))
            {
                case PlanogramPositionOrientationType.Front90:
                    return GFSOrientationType(ccmPosition.OrientationTypeX, product.OrientationType) == PlanogramPositionOrientationType.Top90;
                case PlanogramPositionOrientationType.Top0:
                    return GFSOrientationType(ccmPosition.OrientationTypeX, product.OrientationType) == PlanogramPositionOrientationType.Right90;
                case PlanogramPositionOrientationType.Top90:
                    return GFSOrientationType(ccmPosition.OrientationTypeX, product.OrientationType) == PlanogramPositionOrientationType.Front90;
                case PlanogramPositionOrientationType.Right0:
                    return GFSOrientationType(ccmPosition.OrientationTypeX, product.OrientationType) == PlanogramPositionOrientationType.Front0;
                case PlanogramPositionOrientationType.Right90:
                    return GFSOrientationType(ccmPosition.OrientationTypeX, product.OrientationType) == PlanogramPositionOrientationType.Top0;
                default:
                    return GFSOrientationType(ccmPosition.OrientationTypeX, product.OrientationType) == PlanogramPositionOrientationType.Right0;
            }
        }

        /// <summary>
        /// Checks if the X orientation is correct for a break up.
        /// </summary>
        /// <param name="ccmPosition"></param>
        /// <returns></returns>
        private static Boolean BreakUpCheck(PlanogramPosition ccmPosition, PlanogramProduct product)
        {
            PlanogramPositionMerchandisingStyle mainStyle = ccmPosition.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default ? ProductStyleToPositionStyle(product.MerchandisingStyle) : ccmPosition.MerchandisingStyle;
            PlanogramPositionMerchandisingStyle capStyle = ccmPosition.MerchandisingStyleX == PlanogramPositionMerchandisingStyle.Default ? ProductStyleToPositionStyle(product.MerchandisingStyle) : ccmPosition.MerchandisingStyleX;

            //only trays break up
            if (!(mainStyle == PlanogramPositionMerchandisingStyle.Tray &&
                capStyle == PlanogramPositionMerchandisingStyle.Unit)) return false;

            return GFSOrientationType(ccmPosition.OrientationType, product.OrientationType) ==
                    GFSOrientationType(ccmPosition.OrientationTypeX, product.OrientationType);
           
        }

        /// <summary>
        /// Checks if the Y orientation is correct for a top cap.
        /// </summary>
        /// <param name="ccmPosition"></param>
        /// <returns></returns>
        private static Boolean TopCapCheck(PlanogramPosition ccmPosition, PlanogramProduct product)
        {
            PlanogramPositionMerchandisingStyle mainStyle = ccmPosition.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default ? ProductStyleToPositionStyle(product.MerchandisingStyle) : ccmPosition.MerchandisingStyle;
            PlanogramPositionMerchandisingStyle capStyle = ccmPosition.MerchandisingStyleY == PlanogramPositionMerchandisingStyle.Default ? ProductStyleToPositionStyle(product.MerchandisingStyle) : ccmPosition.MerchandisingStyleY;

            //styles must be the same
            if (mainStyle != capStyle) return false;

            switch (GFSOrientationType(ccmPosition.OrientationType, product.OrientationType))
            {
                case PlanogramPositionOrientationType.Front90:
                    return GFSOrientationType(ccmPosition.OrientationTypeY, product.OrientationType) == PlanogramPositionOrientationType.Right90;
                case PlanogramPositionOrientationType.Top0:
                    return GFSOrientationType(ccmPosition.OrientationTypeY, product.OrientationType) == PlanogramPositionOrientationType.Front0;
                case PlanogramPositionOrientationType.Top90:
                    return GFSOrientationType(ccmPosition.OrientationTypeY, product.OrientationType) == PlanogramPositionOrientationType.Right0;
                case PlanogramPositionOrientationType.Right0:
                    return GFSOrientationType(ccmPosition.OrientationTypeY, product.OrientationType) == PlanogramPositionOrientationType.Top90;
                case PlanogramPositionOrientationType.Right90:
                    return GFSOrientationType(ccmPosition.OrientationTypeY, product.OrientationType) == PlanogramPositionOrientationType.Front90;
                default:
                    return GFSOrientationType(ccmPosition.OrientationTypeY, product.OrientationType) == PlanogramPositionOrientationType.Top0;
            }
        }

        private static Boolean BreakTopCheck(PlanogramPosition ccmPosition, PlanogramProduct product)
        {
            PlanogramPositionMerchandisingStyle mainStyle = ccmPosition.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default ? ProductStyleToPositionStyle(product.MerchandisingStyle) : ccmPosition.MerchandisingStyle;
            PlanogramPositionMerchandisingStyle capStyle = ccmPosition.MerchandisingStyleY == PlanogramPositionMerchandisingStyle.Default ? ProductStyleToPositionStyle(product.MerchandisingStyle) : ccmPosition.MerchandisingStyleY;

            //only trays break up
            if (!(mainStyle == PlanogramPositionMerchandisingStyle.Tray &&
                capStyle == PlanogramPositionMerchandisingStyle.Unit)) return false;
            
            return GFSOrientationType(ccmPosition.OrientationType, product.OrientationType) ==
                    GFSOrientationType(ccmPosition.OrientationTypeY, product.OrientationType);

        }

        private static Boolean BreakBackCheck(PlanogramPosition ccmPosition, PlanogramProduct product)
        {
            PlanogramPositionMerchandisingStyle mainStyle = ccmPosition.MerchandisingStyle == PlanogramPositionMerchandisingStyle.Default ? ProductStyleToPositionStyle(product.MerchandisingStyle) : ccmPosition.MerchandisingStyle;
            PlanogramPositionMerchandisingStyle capStyle = ccmPosition.MerchandisingStyleZ == PlanogramPositionMerchandisingStyle.Default ? ProductStyleToPositionStyle(product.MerchandisingStyle) : ccmPosition.MerchandisingStyleZ;

            //only trays break up
            if (!(mainStyle == PlanogramPositionMerchandisingStyle.Tray &&
                capStyle == PlanogramPositionMerchandisingStyle.Unit)) return false;

            return GFSOrientationType(ccmPosition.OrientationType, product.OrientationType) ==
                    GFSOrientationType(ccmPosition.OrientationTypeZ, product.OrientationType);

        }

        private static PlanogramPositionMerchandisingStyle ProductStyleToPositionStyle(PlanogramProductMerchandisingStyle product)
        {
            PlanogramPositionMerchandisingStyle? output = PlanogramPositionMerchandisingStyleHelper.PlanogramPositionMerchandisingStyleGetEnum(product.ToString().ToLowerInvariant());

            if (output.HasValue) return output.Value;

            return PlanogramPositionMerchandisingStyle.Default;
        }
    }


    /// <summary>
    /// CCM postion part type for just one part of a
    /// ccm position. This can be either the main position or
    /// any of the XYZ capping parts.
    /// </summary>
    /// <remarks>This can be removed when GFS is updated to
    /// match the V8 structure</remarks>
    public enum CcmPositionDetailPartType
    {
        Main,
        XCap,
        YCap,
        ZCap
    }

    /// <summary>
    /// Class which holds position data for just one part of a
    /// ccm position. This can be either the main position or
    /// any of the XYZ capping parts.
    /// </summary>
    /// <remarks>This can be removed when GFS is updated to
    /// match the V8 structure</remarks>
    public class CcmPositionDetailPart
    {
        #region Fields
        private PointValue _coordinates;
        private PlanogramPositionMerchandisingStyle _merchStyle;
        private PlanogramPositionOrientationType _orientationType;
        private WidthHeightDepthValue _rotatedUnitSize;
        private RotationValue _rotation;
        private WidthHeightDepthValue _totalSize;
        private WidthHeightDepthValue _unitSize;
        private Int16 _high;
        private Int16 _wide;
        private Int16 _deep;
        private PlanogramPositionDetails _positionDetail;
        #endregion

        #region Properties

        public PointValue Coordinates
        {
            get
            {
                return _coordinates;
            }
        }
        public PlanogramPositionMerchandisingStyle MerchStyle
        {
            get
            {
                return _merchStyle;
            }
        }
        public PlanogramPositionOrientationType OrientationType
        {
            get
            {
                return _orientationType;
            }
        }
        public WidthHeightDepthValue RotatedUnitSize
        {
            get
            {
                return _rotatedUnitSize;
            }
        }
        public RotationValue Rotation
        {
            get
            {
                return _rotation;
            }
        }
        public WidthHeightDepthValue TotalSize
        {
            get
            {
                return _totalSize;
            }
        }
        public WidthHeightDepthValue UnitSize
        {
            get
            {
                return _unitSize;
            }
        }
        public Int16 High
        {
            get
            {
                return _high;
            }
            private set
            {
                _high = value;
            }
        }
        public Int16 Wide
        {
            get
            {
                return _wide;
            }
            private set
            {
                _wide = value;
            }
        }
        public Int16 Deep
        {
            get
            {
                return _deep;
            }
            private set
            {
                _deep = value;
            }
        }
        public Int16 TotalUnits
        {
            get
            {
                return Convert.ToInt16(High * Wide * Deep);
            }
        }
        #endregion

        #region Constructor
        private CcmPositionDetailPart()
        {
        }
        public CcmPositionDetailPart(PlanogramPosition ccmPosition, PlanogramPositionDetails positionDetails, CcmPositionDetailPartType detailType)
        {
            _positionDetail = positionDetails;
            switch (detailType)
            {
                case CcmPositionDetailPartType.Main:
                    _coordinates = positionDetails.MainCoordinates;
                    _merchStyle = ccmPosition.MerchandisingStyle;
                    _orientationType = ccmPosition.OrientationType;
                    _rotatedUnitSize = positionDetails.MainRotatedUnitSize;
                    _rotation = positionDetails.MainRotation;
                    _totalSize = positionDetails.MainTotalSize;
                    _unitSize = positionDetails.MainUnitSize;
                    _high = ccmPosition.FacingsHigh;
                    _wide = ccmPosition.FacingsWide;
                    _deep = ccmPosition.FacingsDeep;
                    
                    break;
                case CcmPositionDetailPartType.XCap:
                    _coordinates = positionDetails.XCoordinates;
                    _merchStyle = ccmPosition.MerchandisingStyleX;
                    _orientationType = ccmPosition.OrientationTypeX;
                    _rotatedUnitSize = positionDetails.XRotatedUnitSize;
                    _rotation = positionDetails.XRotation;
                    _totalSize = positionDetails.XTotalSize;
                    _unitSize = positionDetails.XUnitSize;
                    _high = ccmPosition.FacingsXHigh;
                    _wide = ccmPosition.FacingsXWide;
                    _deep = ccmPosition.FacingsXDeep;
                    break;
                case CcmPositionDetailPartType.YCap:
                    _coordinates = positionDetails.YCoordinates;
                    _merchStyle = ccmPosition.MerchandisingStyleY;
                    _orientationType = ccmPosition.OrientationTypeY;
                    _rotatedUnitSize = positionDetails.YRotatedUnitSize;
                    _rotation = positionDetails.YRotation;
                    _totalSize = positionDetails.YTotalSize;
                    _unitSize = positionDetails.YUnitSize;
                    _high = ccmPosition.FacingsYHigh;
                    _wide = ccmPosition.FacingsYWide;
                    _deep = ccmPosition.FacingsYDeep;
                    break;
                case CcmPositionDetailPartType.ZCap:
                    _coordinates = positionDetails.ZCoordinates;
                    _merchStyle = ccmPosition.MerchandisingStyleZ;
                    _orientationType = ccmPosition.OrientationTypeZ;
                    _rotatedUnitSize = positionDetails.ZRotatedUnitSize;
                    _rotation = positionDetails.ZRotation;
                    _totalSize = positionDetails.ZTotalSize;
                    _unitSize = positionDetails.ZUnitSize;
                    _high = ccmPosition.FacingsZHigh;
                    _wide = ccmPosition.FacingsZWide;
                    _deep = ccmPosition.FacingsZDeep;
                    break;
            }

            if(detailType != CcmPositionDetailPartType.Main)
            {
                //Z coord is relative to the main part. This needs to be modified
                //to take into account the Z coord in GFS is from the front instead
                //of from the back.
                _coordinates.Z -= positionDetails.MainTotalSize.Depth - _totalSize.Depth;
            }

        }

        public List<CcmPositionDetailPart> SplitFacingsIntoBytes()
        {
            List<CcmPositionDetailPart> output = new List<CcmPositionDetailPart>();
            Int16 high = High;
            Int16 wide = Wide;
            Int16 deep = Deep;
            Int16 highAdd = 0;
            Int16 wideAdd = 0;
            //Int16 deepAdd = 0;
            Int16 highUsed = 0;
            Int16 wideUsed = 0;
            Int16 deepUsed = 0;

            for (Int16 h = high; h > 0; h -= Byte.MaxValue)
            {
                wideUsed = 0;
                if (h > Byte.MaxValue)
                {
                    highAdd = Byte.MaxValue;
                }
                else
                {
                    highAdd = h;
                }
                for (Int16 w = wide; w > 0; w -= Byte.MaxValue)
                {
                    deepUsed = 0;
                    if (w > Byte.MaxValue)
                    {
                        wideAdd = Byte.MaxValue;
                    }
                    else
                    {
                        wideAdd = w;
                    }
                    for (Int16 d = deep; d > 0; d -= Byte.MaxValue)
                    {
                        CcmPositionDetailPart pos = this.Copy();

                        pos.High = highAdd;
                        pos.Wide = wideAdd;
                        if (d > Byte.MaxValue)
                        {
                            pos.Deep = Byte.MaxValue;
                        }
                        else
                        {
                            pos.Deep = d;
                        }

                        pos._coordinates.Y += _positionDetail.FacingSize.Height * highUsed;
                        pos._coordinates.X += _positionDetail.FacingSize.Width * wideUsed;
                        pos._coordinates.Z += _positionDetail.FacingSize.Depth * deepUsed;

                       
                        deepUsed += pos.Deep;

                        output.Add(pos);
                    }
                    wideUsed += wideAdd;
                }
                highUsed += highAdd;
            }

            return output;
        }
        #endregion

        #region Private Methods

        private CcmPositionDetailPart Copy()
        {
            CcmPositionDetailPart copy = new CcmPositionDetailPart()
            {
                _positionDetail = this._positionDetail,
                _coordinates = this._coordinates,
                _deep = this._deep,
                _high = this._high,
                _merchStyle = this._merchStyle,
                _orientationType = this._orientationType,
                _rotatedUnitSize = this._rotatedUnitSize,
                _rotation = this._rotation,
                _totalSize = this._totalSize,
                _unitSize = this._unitSize,
                _wide = this._wide
            };
            return copy;
        }

        #endregion
    }
   
}
