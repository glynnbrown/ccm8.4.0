﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26911 : Luong
//  Created (Copied From GFS)
#endregion
#region Version History: CCM801
// V8-28258 : N.Foster
//  Increased the amount of information logged when an unexpected exception occurs
#endregion
#region Version History: CCM803
// V8-29590 : A.Probyn
//	Extended for new WorkpackageName, WorkpackageId property, temporary set to null until ui implemented
// V8-29731 : N.Foster
//  Added methods to pass workpackage name and id to the system event log
#endregion
#endregion

using System;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Security.Principal;
using System.Text;
using System.Threading;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using Gibraltar.Agent;
using CcmEventLog = Galleria.Ccm.Model.EventLog;
using CcmEventLogEvent = Galleria.Ccm.Model.EventLogEvent;
using CcmEventLogEventHelper = Galleria.Ccm.Model.EventLogEventHelper;

namespace Galleria.Ccm.Helpers
{
    /// <summary>
    /// The <b>LoggingHelper</b> class is used for logging application log messages and errors.
    /// </summary>
    public sealed class LoggingHelper : IDisposable
    {
        #region Constants
        public const String ImportsEventLogName = "Imports";
        public const String ImageMaintenanceEventLogName = "Image Processing";
        public const String SystemPublisherEventLogName = "Publishing";
        public const String SecurityEventLogName = "Security";
        public const String WebServicesEventLogName = "Web Services";
        public const String ErpIntegrationEventLogName = "ERP Integration";

        public const String SessionIdName = "Session ID";
        #endregion

        #region Fields

        private const String _gfsLogName = "CCM";
        private const String _appLogName = "Application";
        private const String _appSettingsSection = "appSettings";
        private const String _gfsWindowsSourceName = "CCM";
        private const String _gfsDbSourceName = "CCM";

        private EventLog _eventLog;
        private CultureInfo _locale;

        private static LoggingHelper _logger;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets and sets the <b>LoggingHelper</b> to for the current <see cref="AppDomain"/>.
        /// </summary>
        public static LoggingHelper Logger
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                if (_logger == null)
                {
                    // If someone tries to use the app domain logger without explicitely having created one, just 
                    // assume that they don't want to write to the windows event log.
                    _logger = new LoggingHelper(true);
                }

                return _logger;
            }
            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _logger = value;
            }
        }

        /// <summary>
        /// Returns the windows application log name
        /// </summary>
        public static String ApplicationLogName
        {
            get { return _appLogName; }
        }
        #endregion Properties

        #region Construction + Destruction

        public LoggingHelper() : this(null) { }

        /// <summary>
        /// Initializes a new instance of the <b>LoggingHelper</b> class.
        /// </summary>
        /// <param name="source">The source of the log messages. i.e. SystemNode.</param>
        /// <param name="config">The configuration object holding the setup parameters i.e. LoggingMode</param>
        public LoggingHelper(String source)
        {
            Initialise(source);
        }

        public LoggingHelper(Boolean useOnAppDomain) : this(null, useOnAppDomain) { }

        /// <summary>
        /// Initializes a new instance of the <b>LoggingHelper</b> class.
        /// </summary>
        /// <param name="source">The source of the log messages. i.e. SystemNode.</param>
        /// <param name="useOnAppDomain">When <b>true</b>, this new instance becomes the default for the <see cref="AppDomain"/>.</param>
        /// <param name="config">The configuration object holding the setup parameters i.e. LoggingMode</param>
        public LoggingHelper(String source, Boolean useOnAppDomain)
            : this(source)
        {
            if (useOnAppDomain)
            {
                _logger = this;
            }
        }


        #endregion Construction + Destruction

        #region Public Methods

        /// <summary>
        /// Writes a message to the system event log
        /// </summary>
        public void WriteAuthenticationFailure(String eventLogName)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            StringBuilder database = new StringBuilder();
            foreach (DalFactoryParameterConfigElement parameter in dalFactory.DalFactoryConfig.DalParameters)
            {
                database.Append(parameter.Name);
                database.Append("=");
                database.Append(parameter.Value);
                database.Append(",");
            }
            database.Remove(database.Length - 1, 1);
            Write(
                EventLogEntryType.FailureAudit,
                eventLogName,
                null,
                CcmEventLogEvent.AuthenticationFailure,
                null,
                null,
                WindowsIdentity.GetCurrent().Name,
                database.ToString());
        }

        /// <summary>
        /// Writes a message to the system event log
        /// </summary>
        public void Write(String eventLogName, Galleria.Ccm.Processes.ProcessException ex)
        {
            if (ex == null) throw new ArgumentException("ex");
            Write(EventLogEntryType.Error, eventLogName, ex.EntityId, CcmEventLogEvent.UnexpectedException, null, null, ex.Message);

        }

        /// <summary>
        /// Writes a message to the system event log
        /// </summary>
        public void Write(String eventLogName, Int32? entityId, Exception ex)
        {
            if (ex == null) throw new ArgumentNullException("ex");
            Write(EventLogEntryType.Error, eventLogName, entityId, CcmEventLogEvent.UnexpectedException, null, null, ex.Message);
        }

        /// <summary>
        /// Writes a message to the system event log
        /// </summary>
        public void Write(String eventLogName, Int32? entityId, String workpackageName, Int32? workpackageId, Exception ex)
        {
            if (ex == null) throw new ArgumentNullException("ex");
            Write(EventLogEntryType.Error, eventLogName, entityId, CcmEventLogEvent.UnexpectedException, workpackageName, workpackageId, ex.Message);
        }

        /// <summary>
        /// Writes a message to the system event log
        /// </summary>
        public void Write(String eventLogName, Int32? entityId, CcmEventLogEvent eventId, String workpackageName, Int32? workpackageId, params Object[] args)
        {
            Write(EventLogEntryType.Information, eventLogName, entityId, eventId, workpackageName, workpackageId, args);
        }

        /// <summary>
        /// Writes a message to the system event log
        /// </summary>
        public void Write(
            EventLogEntryType type,
            String eventLogName,
            Int32? entityId,
            CcmEventLogEvent eventId,
            String workpackageName,
            Int32? workpackageId,
            params Object[] args)
        {
            String content = String.Format(_locale, CcmEventLogEventHelper.FriendlyContents[eventId], args);
            Write(type, eventLogName, entityId, eventId, content, workpackageName, workpackageId);
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Sets up the appropriate xml keys in the web.config file for the Logging Helper
        /// </summary>
        /// <param name="source">The source of the log messages. i.e. SystemNode.</param>
        /// <param name="config">The configuration object holding the setup parameters i.e. LoggingMode</param>
        private void Initialise(String source)
        {
            _locale = Thread.CurrentThread.CurrentCulture;

            if (source != null)
            {
                //_eventLog = new EventLog(_gfsLogName, Environment.MachineName, source); <- GFS-16727: Use this
                _eventLog = new EventLog(_appLogName, Environment.MachineName, source);
            }
        }

        /// <summary>
        /// Writes a message to the system event log
        /// </summary>
        private void Write(
            EventLogEntryType type,
            String eventLogName,
            Int32? entityId,
            CcmEventLogEvent eventId,
            String content,
            String workpackageName,
            Int32? workpackageId)
        {
            String description = CcmEventLogEventHelper.FriendlyDescriptions[eventId];
            String descriptionAndContent = String.Format(
                "{0}: {1}{2}{3}Gibraltar Session ID: {4}",
                description,
                content,
                Environment.NewLine,
                Environment.NewLine,
                Log.SessionSummary.Properties[SessionIdName]);

            try
            {
                // Log to Gibraltar.
                switch (type)
                {
                    case EventLogEntryType.Error:
                    case EventLogEntryType.FailureAudit:
                        Trace.TraceError(descriptionAndContent);
                        break;
                    case EventLogEntryType.Warning:
                        Trace.TraceWarning(descriptionAndContent);
                        break;
                    case EventLogEntryType.Information:
                    case EventLogEntryType.SuccessAudit:
                        Trace.TraceInformation(descriptionAndContent);
                        break;
                }

                // Log to database, unless we're not authenticated, in which case we won't have permission to, but
                // the authentication code will have logged an error already.
                if (DomainPrincipal.CurrentUserId != null)
                {
                    try
                    {
                        CcmEventLog.CreateEventLog(
                            entityId,
                            _gfsDbSourceName,
                            eventLogName,
                            (Int32)eventId,
                            type,
                            DomainPrincipal.CurrentUserId,
                            description,
                            content,
                            Log.SessionSummary.Properties[SessionIdName],
                            workpackageName,
                            workpackageId);
                    }
                    catch (Exception)
                    {
                        // Don't cry over spilt milk.
                    }
                }

                // Log to Windows event log.
                if (_eventLog != null)
                {
                    _eventLog.WriteEntry(descriptionAndContent, type, (Int32)eventId);
                }
            }
            // GFS-16727: Use the below
            //catch (Win32Exception ex)
            //{
            //    // If something goes wrong writing to the windows event log, such as it being full, we'll land here.
            //    Trace.TraceError(ex.ToString());

            //    using (EventLog appLog = new EventLog(_appLogName, _eventLog.MachineName, _gfsWindowsSourceName))
            //    {
            //        String logMessage = _eventLog.Source + ": " + ex.ToString();
            //        appLog.WriteEntry(logMessage, EventLogEntryType.Error);
            //    }
            //}
            catch (Exception ex)
            {
                // If something more fundamental goes wrong, such as a developer running in console mode without 
                // having registered the required windows event log and sources, we'll land here.  Nothing more we can
                // do...
                Trace.TraceError(ex.ToString());
            }
        }

        #endregion Private Methods

        #region IDisposable Members

        /// <summary>
        /// Releases all resources.
        /// </summary>
        public void Dispose()
        {
            // TODO: Implement properly
            using (_eventLog)
            {
                _eventLog = null;
            }
        }

        #endregion IDisposable Members
    }
}
