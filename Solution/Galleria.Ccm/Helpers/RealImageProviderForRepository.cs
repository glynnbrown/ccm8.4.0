//#region Header Information

//// Copyright � Galleria RTS Ltd 2015

//#region Version History: CCM801

//// V8-28676 : A.Silva
////      Created.
//// V8-28897 : A.Silva
////      Amended UpdateRealImage so that missing images are not constantly tried to be retrieved
////      and repeteadly being stored as miss.

//#endregion

//#region Version History: CCM802

//// V8-28964 : A.Silva
////      Moved RealImageProviderForRepository to its own file.

//#endregion

//#endregion

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using Galleria.Ccm.Model;
//using Galleria.Ccm.Resources.Language;
//using Galleria.Framework.Planograms.Interfaces;
//using Galleria.Framework.Planograms.Model;

//namespace Galleria.Ccm.Helpers
//{
//    public class RealImageProviderForRepository : RealImageProviderBase
//    {
//        #region Fields

//        private readonly Dictionary<Object, Product> _productCache = new Dictionary<Object, Product>();

//        //private readonly Dictionary<String, DateTime> _missingImages = new Dictionary<String, DateTime>();

//        #endregion

//        #region Properties

//        //private Dictionary<String, DateTime> MissingImages
//        //{
//        //    get
//        //    {
//        //        //  Remove old missing files so these are retried once more.
//        //        DateTime now = DateTime.UtcNow;
//        //        foreach (String key in _missingImages.Where(o => IsTimeStampExpired(o.Value, now)).Select(o => o.Key).ToList())
//        //        {
//        //            _missingImages.Remove(key);
//        //        }
//        //        return _missingImages;
//        //    }
//        //}

//        #endregion

//        #region Constructor

//        public RealImageProviderForRepository(Int32 entityId, IRealImageProviderSettings settings) 
//            : base(settings, entityId) {}

//        #endregion

//        #region Methods

//        //protected override void OnSettingsChanging(IRealImageProviderSettings settings)
//        //{
//        //    //Type type = typeof(IRealImageProviderSettings);
//        //    //if (type.GetProperties().All(propertyInfo => Equals(propertyInfo.GetValue(Settings, null), propertyInfo.GetValue(settings, null)))) return;

//        //    //_productCache.Clear();
//        //    //_missingImages.Clear();
//        //    //ImageCache.Clear();
//        //}

//        protected override String GetNameCriteria(Object imageTarget)
//        {
//            PlanogramProduct planogramProduct = (PlanogramProduct) imageTarget;
//            return planogramProduct.Gtin;
//        }

//        protected override String GetPlanogramImageDescription(String imageSource, CachedPlanogramImageCriteria criteria)
//        {
//            return String.Format(Message.RealImageProvider_RepositoryImagenDescription, criteria.Attribute, criteria.ImageType,
//                    criteria.FacingType);
//        }

//        protected override String GetPlanogramImageFileName(CachedPlanogramImageCriteria criteria)
//        {
//            return criteria.GetCacheItemId();
//        }

//        protected override Byte[] UpdateRealImage(CachedPlanogramImage planogramImage)
//        {
//            // A planogram image is needed to be updated.
//            if (planogramImage == null) return null;

//            // If it is a missing image do not even try to load it.
//            if (planogramImage.IsImageConfirmedMissing()) return null;
//            //if (this.MissingImages.ContainsKey(planogramImage.Image.FileName)) return null;

//            // Get the Gtin for the image so we can locate the product.
//            CachedPlanogramImageCriteria reconstructedCriteria = new CachedPlanogramImageCriteria(planogramImage.Image.FileName);
//            String gtin = reconstructedCriteria.Attribute;
//            Byte[] imageData = null;

//            if (gtin != null)
//            {
//                // Try to get the product from the cache, if not from the repository.
//                Product product;
//                if (!_productCache.TryGetValue(gtin, out product))
//                {
//                    product = this.FetchProductByGtin(gtin);
//                }

//                // Try to get the image and update it if we have a product.
//                if (product != null)
//                {
//                    imageData = product.GetImageData(reconstructedCriteria.ImageType, reconstructedCriteria.FacingType);
//                    //planogramImage.UpdateImage(imageData);
//                }
//            }

//            planogramImage.UpdateImage(imageData);

//            // If there is no image data mark the image as missing so we do not try to retrieve it for a while.
//            //if (imageData == null)
//            //{
//            //_missingImages[planogramImage.Image.FileName] = DateTime.UtcNow;
//            //}


//            // Return the image data.
//            return imageData;
//        }

//        ///// <summary>
//        /////     Determines whether the <see cref="PlanogramImage" /> is missing its image data and contains a valid filename.
//        ///// </summary>
//        ///// <param name="planogramImage"></param>
//        ///// <returns></returns>
//        //protected override Boolean IsImageDataPending(PlanogramImage planogramImage)
//        //{
//        //    return planogramImage.ImageData == null && !String.IsNullOrEmpty(planogramImage.FileName) &&
//        //           !this.MissingImages.ContainsKey(planogramImage.FileName);
//        //}

//        private Product FetchProductByGtin(String gtin)
//        {
//            Product product = ProductList.FetchByEntityIdProductGtins(this.EntityId, new[] {gtin}).FirstOrDefault();
//            if (!_productCache.ContainsKey(gtin)) _productCache.Add(gtin, product);
//            return product;
//        }

//        private Boolean IsTimeStampExpired(DateTime timeStamp, DateTime now)
//        {
//            TimeSpan elapsedTime = (now - timeStamp).Duration();
//            return elapsedTime > IsImageValidDuration;
//        }

//        #endregion

//        #region IDisposable Members

//        /// <summary>
//        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
//        /// </summary>
//        public override void Dispose()
//        {
//            if (IsDisposed) return;

//            _productCache.Clear();
//            base.Dispose();
//        }

//        #endregion
//    }
//}