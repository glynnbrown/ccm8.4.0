﻿#region Header Information
#region Version History CCM830
// V8-31531 : A.Heathcote
//      Removed IsTrayProduct (there was no header info so I added this. 
//      If this document doesnt require one, either delete or let me know and I will delete.
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Model;



namespace Galleria.Ccm.Helpers
{
    /// <summary>
    /// TEMPORY
    /// Converts GFS plan data to the new CCM planogram model
    /// </summary>
    public sealed class GfsToCcmPlanogramConverter
    {
        #region Nested Classes

        public class GFSPlanogramSubComponentDto
        {
            #region Properties

            public Int32 Id { get; set; }
            public Int32 ComponentId { get; set; }
            //public Int32 Mesh3DId { get; set; }
            public Int32 ImageIdFront { get; set; }
            public Int32 ImageIdBack { get; set; }
            public Int32 ImageIdTop { get; set; }
            public Int32 ImageIdBottom { get; set; }
            public Int32 ImageIdLeft { get; set; }
            public Int32 ImageIdRight { get; set; }
            public String Name { get; set; }
            public Single Height { get; set; }
            public Single Width { get; set; }
            public Single Depth { get; set; }
            public Single X { get; set; }
            public Single Y { get; set; }
            public Single Z { get; set; }
            public Single Slope { get; set; }
            public Single Angle { get; set; }
            public Single Roll { get; set; }
            public Byte ShapeType { get; set; }
            public Single MerchandisableHeight { get; set; }
            public Boolean IsMerchandisable { get; set; }
            public Boolean IsVisible { get; set; }
            public Boolean HasCollisionDetection { get; set; }
            public Byte FillPatternTypeFront { get; set; }
            public Byte FillPatternTypeBack { get; set; }
            public Byte FillPatternTypeTop { get; set; }
            public Byte FillPatternTypeBottom { get; set; }
            public Byte FillPatternTypeLeft { get; set; }
            public Byte FillPatternTypeRight { get; set; }
            public Int32 FillColourFront { get; set; }
            public Int32 FillColourBack { get; set; }
            public Int32 FillColourTop { get; set; }
            public Int32 FillColourBottom { get; set; }
            public Int32 FillColourLeft { get; set; }
            public Int32 FillColourRight { get; set; }
            public Int32 LineColour { get; set; }
            public Int32 TransparencyPercentFront { get; set; }
            public Int32 TransparencyPercentBack { get; set; }
            public Int32 TransparencyPercentTop { get; set; }
            public Int32 TransparencyPercentBottom { get; set; }
            public Int32 TransparencyPercentLeft { get; set; }
            public Int32 TransparencyPercentRight { get; set; }
            public Single FaceThickness { get; set; }
            public Single FaceThicknessTop { get; set; }
            public Single RiserHeight { get; set; }
            public Boolean IsRiserPlacedOnFront { get; set; }
            public Boolean IsRiserPlacedOnBack { get; set; }
            public Boolean IsRiserPlacedOnLeft { get; set; }
            public Boolean IsRiserPlacedOnRight { get; set; }
            public Byte RiserFillPatternType { get; set; }
            public Single NotchStartX { get; set; }
            public Single NotchSpacingX { get; set; }
            public Single NotchHeight { get; set; }
            public Boolean IsNotchPlacedOnFront { get; set; }
            public Boolean IsNotchPlacedOnBack { get; set; }
            public Boolean IsNotchPlacedOnLeft { get; set; }
            public Boolean IsNotchPlacedOnRight { get; set; }
            public Byte NotchStyleType { get; set; }
            public Single DividerObstructionHeight { get; set; }
            public Single DividerObstructionWidth { get; set; }
            public Single DividerObstructionStartX { get; set; }
            public Single DividerObstructionSpacingX { get; set; }
            public Single DividerObstructionStartY { get; set; }
            public Single DividerObstructionSpacingY { get; set; }
            public Single MerchConstraintRow1StartX { get; set; }
            public Single MerchConstraintRow1SpacingX { get; set; }
            public Single MerchConstraintRow1StartY { get; set; }
            public Single MerchConstraintRow1SpacingY { get; set; }
            public Single MerchConstraintRow2StartX { get; set; }
            public Single MerchConstraintRow2SpacingX { get; set; }
            public Single MerchConstraintRow2StartY { get; set; }
            public Single MerchConstraintRow2SpacingY { get; set; }
            public Byte SubComponentType { get; set; }

            #endregion

            #region Methods

            public override int GetHashCode()
            {
                return this.Id.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                GFSPlanogramSubComponentDto other = obj as GFSPlanogramSubComponentDto;
                if (other != null)
                {
                    if (other.Id != this.Id)
                    {
                        return false;
                    }
                    if (other.ComponentId != this.ComponentId)
                    {
                        return false;
                    }
                    //if (other.Mesh3DId != this.Mesh3DId)
                    //{
                    //    return false;
                    //}
                    if (other.ImageIdFront != this.ImageIdFront)
                    {
                        return false;
                    }
                    if (other.ImageIdBack != this.ImageIdBack)
                    {
                        return false;
                    }
                    if (other.ImageIdTop != this.ImageIdTop)
                    {
                        return false;
                    }
                    if (other.ImageIdBottom != this.ImageIdBottom)
                    {
                        return false;
                    }
                    if (other.ImageIdLeft != this.ImageIdLeft)
                    {
                        return false;
                    }
                    if (other.ImageIdRight != this.ImageIdRight)
                    {
                        return false;
                    }
                    if (other.Name != this.Name)
                    {
                        return false;
                    }
                    if (other.Height != this.Height)
                    {
                        return false;
                    }
                    if (other.Width != this.Width)
                    {
                        return false;
                    }
                    if (other.Depth != this.Depth)
                    {
                        return false;
                    }
                    if (other.X != this.X)
                    {
                        return false;
                    }
                    if (other.Y != this.Y)
                    {
                        return false;
                    }
                    if (other.Z != this.Z)
                    {
                        return false;
                    }
                    if (other.Slope != this.Slope)
                    {
                        return false;
                    }
                    if (other.Angle != this.Angle)
                    {
                        return false;
                    }
                    if (other.Roll != this.Roll)
                    {
                        return false;
                    }
                    if (other.ShapeType != this.ShapeType)
                    {
                        return false;
                    }
                    if (other.MerchandisableHeight != this.MerchandisableHeight)
                    {
                        return false;
                    }
                    if (other.IsMerchandisable != this.IsMerchandisable)
                    {
                        return false;
                    }
                    if (other.IsVisible != this.IsVisible)
                    {
                        return false;
                    }
                    if (other.HasCollisionDetection != this.HasCollisionDetection)
                    {
                        return false;
                    }
                    if (other.FillPatternTypeFront != this.FillPatternTypeFront)
                    {
                        return false;
                    }
                    if (other.FillPatternTypeBack != this.FillPatternTypeBack)
                    {
                        return false;
                    }
                    if (other.FillPatternTypeTop != this.FillPatternTypeTop)
                    {
                        return false;
                    }
                    if (other.FillPatternTypeBottom != this.FillPatternTypeBottom)
                    {
                        return false;
                    }
                    if (other.FillPatternTypeLeft != this.FillPatternTypeLeft)
                    {
                        return false;
                    }
                    if (other.FillPatternTypeRight != this.FillPatternTypeRight)
                    {
                        return false;
                    }
                    if (other.FillColourFront != this.FillColourFront)
                    {
                        return false;
                    }
                    if (other.FillColourBack != this.FillColourBack)
                    {
                        return false;
                    }
                    if (other.FillColourTop != this.FillColourTop)
                    {
                        return false;
                    }
                    if (other.FillColourBottom != this.FillColourBottom)
                    {
                        return false;
                    }
                    if (other.FillColourLeft != this.FillColourLeft)
                    {
                        return false;
                    }
                    if (other.FillColourRight != this.FillColourRight)
                    {
                        return false;
                    }
                    if (other.LineColour != this.LineColour)
                    {
                        return false;
                    }
                    if (other.TransparencyPercentFront != this.TransparencyPercentFront)
                    {
                        return false;
                    }
                    if (other.TransparencyPercentBack != this.TransparencyPercentBack)
                    {
                        return false;
                    }
                    if (other.TransparencyPercentTop != this.TransparencyPercentTop)
                    {
                        return false;
                    }
                    if (other.TransparencyPercentBottom != this.TransparencyPercentBottom)
                    {
                        return false;
                    }
                    if (other.TransparencyPercentLeft != this.TransparencyPercentLeft)
                    {
                        return false;
                    }
                    if (other.TransparencyPercentRight != this.TransparencyPercentRight)
                    {
                        return false;
                    }
                    if (other.FaceThickness != this.FaceThickness)
                    {
                        return false;
                    }
                    if (other.FaceThicknessTop != this.FaceThicknessTop)
                    {
                        return false;
                    }
                    if (other.RiserHeight != this.RiserHeight)
                    {
                        return false;
                    }
                    if (other.IsRiserPlacedOnFront != this.IsRiserPlacedOnFront)
                    {
                        return false;
                    }
                    if (other.IsRiserPlacedOnBack != this.IsRiserPlacedOnBack)
                    {
                        return false;
                    }
                    if (other.IsRiserPlacedOnLeft != this.IsRiserPlacedOnLeft)
                    {
                        return false;
                    }
                    if (other.IsRiserPlacedOnRight != this.IsRiserPlacedOnRight)
                    {
                        return false;
                    }
                    if (other.RiserFillPatternType != this.RiserFillPatternType)
                    {
                        return false;
                    }
                    if (other.NotchStartX != this.NotchStartX)
                    {
                        return false;
                    }
                    if (other.NotchSpacingX != this.NotchSpacingX)
                    {
                        return false;
                    }
                    if (other.NotchHeight != this.NotchHeight)
                    {
                        return false;
                    }
                    if (other.IsNotchPlacedOnFront != this.IsNotchPlacedOnFront)
                    {
                        return false;
                    }
                    if (other.IsNotchPlacedOnBack != this.IsNotchPlacedOnBack)
                    {
                        return false;
                    }
                    if (other.IsNotchPlacedOnLeft != this.IsNotchPlacedOnLeft)
                    {
                        return false;
                    }
                    if (other.IsNotchPlacedOnRight != this.IsNotchPlacedOnRight)
                    {
                        return false;
                    }
                    if (other.NotchStyleType != this.NotchStyleType)
                    {
                        return false;
                    }
                    if (other.DividerObstructionHeight != this.DividerObstructionHeight)
                    {
                        return false;
                    }
                    if (other.DividerObstructionWidth != this.DividerObstructionWidth)
                    {
                        return false;
                    }
                    if (other.DividerObstructionStartX != this.DividerObstructionStartX)
                    {
                        return false;
                    }
                    if (other.DividerObstructionSpacingX != this.DividerObstructionSpacingX)
                    {
                        return false;
                    }
                    if (other.DividerObstructionStartY != this.DividerObstructionStartY)
                    {
                        return false;
                    }
                    if (other.DividerObstructionSpacingY != this.DividerObstructionSpacingY)
                    {
                        return false;
                    }
                    if (other.MerchConstraintRow1StartX != this.MerchConstraintRow1StartX)
                    {
                        return false;
                    }
                    if (other.MerchConstraintRow1SpacingX != this.MerchConstraintRow1SpacingX)
                    {
                        return false;
                    }
                    if (other.MerchConstraintRow1StartY != this.MerchConstraintRow1StartY)
                    {
                        return false;
                    }
                    if (other.MerchConstraintRow1SpacingY != this.MerchConstraintRow1SpacingY)
                    {
                        return false;
                    }
                    if (other.MerchConstraintRow2StartX != this.MerchConstraintRow2StartX)
                    {
                        return false;
                    }
                    if (other.MerchConstraintRow2SpacingX != this.MerchConstraintRow2SpacingX)
                    {
                        return false;
                    }
                    if (other.MerchConstraintRow2StartY != this.MerchConstraintRow2StartY)
                    {
                        return false;
                    }
                    if (other.MerchConstraintRow2SpacingY != this.MerchConstraintRow2SpacingY)
                    {
                        return false;
                    }
                    if (other.SubComponentType != this.SubComponentType)
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                return true;
            }
            #endregion
        }

        public class GFSPlanogramProductDto
        {
            #region Properties

            public Int32 Id { get; set; }
            public Int32 PlanogramContentId { get; set; }
            public Int32 ProductId { get; set; }
            public String GTIN { get; set; }
            public String Name { get; set; }
            public String Brand { get; set; }
            public Single Height { get; set; }
            public Single Width { get; set; }
            public Single Depth { get; set; }
            public Single DisplayHeight { get; set; }
            public Single DisplayWidth { get; set; }
            public Single DisplayDepth { get; set; }
            public Single AlternateHeight { get; set; }
            public Single AlternateWidth { get; set; }
            public Single AlternateDepth { get; set; }
            public Single PointOfPurchaseHeight { get; set; }
            public Single PointOfPurchaseWidth { get; set; }
            public Single PointOfPurchaseDepth { get; set; }
            public Byte NumberOfPegHoles { get; set; }
            public Single PegX { get; set; }
            public Single PegX2 { get; set; }
            public Single PegX3 { get; set; }
            public Single PegY { get; set; }
            public Single PegY2 { get; set; }
            public Single PegY3 { get; set; }
            public Single PegProngOffset { get; set; }
            public Single PegDepth { get; set; }
            public Single SqueezeHeight { get; set; }
            public Single SqueezeWidth { get; set; }
            public Single SqueezeDepth { get; set; }
            public Single NestingHeight { get; set; }
            public Single NestingWidth { get; set; }
            public Single NestingDepth { get; set; }
            public Int16 CasePackUnits { get; set; }
            public Byte CaseHigh { get; set; }
            public Byte CaseWide { get; set; }
            public Byte CaseDeep { get; set; }
            public Single CaseHeight { get; set; }
            public Single CaseWidth { get; set; }
            public Single CaseDepth { get; set; }
            public Byte MaxStack { get; set; }
            public Byte MaxTopCap { get; set; }
            public Byte MaxRightCap { get; set; }
            public Byte MinDeep { get; set; }
            public Byte MaxDeep { get; set; }
            public Int16 TrayPackUnits { get; set; }
            public Byte TrayHigh { get; set; }
            public Byte TrayWide { get; set; }
            public Byte TrayDeep { get; set; }
            public Single TrayHeight { get; set; }
            public Single TrayWidth { get; set; }
            public Single TrayDepth { get; set; }
            public Single TrayThickHeight { get; set; }
            public Single TrayThickWidth { get; set; }
            public Single TrayThickDepth { get; set; }
            public Single FrontOverhang { get; set; }
            public Single FingerSpaceAbove { get; set; }
            public Single FingerSpaceToTheSide { get; set; }
            public Byte StatusType { get; set; }
            public Byte OrientationType { get; set; }
            public Byte MerchandisingStyle { get; set; }
            public Boolean IsFrontOnly { get; set; }
            public Boolean IsTrayProduct { get; set; }
            public Boolean IsPlaceHolderProduct { get; set; }
            public Boolean IsActive { get; set; }
            public Boolean CanBreakTrayUp { get; set; }
            public Boolean CanBreakTrayDown { get; set; }
            public Boolean CanBreakTrayBack { get; set; }
            public Boolean CanBreakTrayTop { get; set; }
            public Boolean ForceMiddleCap { get; set; }
            public Boolean ForceBottomCap { get; set; }

            public Boolean CanMerchandiseOnShelf { get; set; }
            public Boolean CanMerchandiseOnBar { get; set; }
            public Boolean CanMerchandiseOnPeg { get; set; }
            public Boolean CanMerchandiseInChest { get; set; }

            #endregion

            #region Methods
            /// <summary>
            /// Returns the objects hash code
            /// </summary>
            /// <returns>The objects hash code</returns>
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            /// <summary>
            /// Compares two instance of this type
            /// </summary>
            /// <param name="obj">The object to compare to</param>
            /// <returns>True if equal, else false</returns>
            public override bool Equals(object obj)
            {
                GFSPlanogramProductDto other = obj as GFSPlanogramProductDto;
                if (other != null)
                {
                    if (other.Id != this.Id)
                    {
                        return false;
                    }
                    if (other.PlanogramContentId != this.PlanogramContentId)
                    {
                        return false;
                    }
                    if (other.ProductId != this.ProductId)
                    {
                        return false;
                    }
                    if (other.GTIN != this.GTIN)
                    {
                        return false;
                    }
                    if (other.Name != this.Name)
                    {
                        return false;
                    }
                    if (other.Brand != this.Brand)
                    {
                        return false;
                    }
                    if (other.Height != this.Height)
                    {
                        return false;
                    }
                    if (other.Width != this.Width)
                    {
                        return false;
                    }
                    if (other.Depth != this.Depth)
                    {
                        return false;
                    }
                    if (other.DisplayHeight != this.DisplayHeight)
                    {
                        return false;
                    }
                    if (other.DisplayWidth != this.DisplayWidth)
                    {
                        return false;
                    }
                    if (other.DisplayDepth != this.DisplayDepth)
                    {
                        return false;
                    }
                    if (other.AlternateHeight != this.AlternateHeight)
                    {
                        return false;
                    }
                    if (other.AlternateWidth != this.AlternateWidth)
                    {
                        return false;
                    }
                    if (other.AlternateDepth != this.AlternateDepth)
                    {
                        return false;
                    }
                    if (other.PointOfPurchaseHeight != this.PointOfPurchaseHeight)
                    {
                        return false;
                    }
                    if (other.PointOfPurchaseWidth != this.PointOfPurchaseWidth)
                    {
                        return false;
                    }
                    if (other.PointOfPurchaseDepth != this.PointOfPurchaseDepth)
                    {
                        return false;
                    }
                    if (other.NumberOfPegHoles != this.NumberOfPegHoles)
                    {
                        return false;
                    }
                    if (other.PegX != this.PegX)
                    {
                        return false;
                    }
                    if (other.PegX2 != this.PegX2)
                    {
                        return false;
                    }
                    if (other.PegX3 != this.PegX3)
                    {
                        return false;
                    }
                    if (other.PegY != this.PegY)
                    {
                        return false;
                    }
                    if (other.PegY2 != this.PegY2)
                    {
                        return false;
                    }
                    if (other.PegY3 != this.PegY3)
                    {
                        return false;
                    }
                    if (other.PegProngOffset != this.PegProngOffset)
                    {
                        return false;
                    }
                    if (other.PegDepth != this.PegDepth)
                    {
                        return false;
                    }
                    if (other.SqueezeHeight != this.SqueezeHeight)
                    {
                        return false;
                    }
                    if (other.SqueezeWidth != this.SqueezeWidth)
                    {
                        return false;
                    }
                    if (other.SqueezeDepth != this.SqueezeDepth)
                    {
                        return false;
                    }
                    if (other.NestingHeight != this.NestingHeight)
                    {
                        return false;
                    }
                    if (other.NestingWidth != this.NestingWidth)
                    {
                        return false;
                    }
                    if (other.NestingDepth != this.NestingDepth)
                    {
                        return false;
                    }
                    if (other.CasePackUnits != this.CasePackUnits)
                    {
                        return false;
                    }
                    if (other.CaseHigh != this.CaseHigh)
                    {
                        return false;
                    }
                    if (other.CaseWide != this.CaseWide)
                    {
                        return false;
                    }
                    if (other.CaseDeep != this.CaseDeep)
                    {
                        return false;
                    }
                    if (other.CaseHeight != this.CaseHeight)
                    {
                        return false;
                    }
                    if (other.CaseWidth != this.CaseWidth)
                    {
                        return false;
                    }
                    if (other.CaseDepth != this.CaseDepth)
                    {
                        return false;
                    }
                    if (other.MaxStack != this.MaxStack)
                    {
                        return false;
                    }
                    if (other.MaxTopCap != this.MaxTopCap)
                    {
                        return false;
                    }
                    if (other.MaxRightCap != this.MaxRightCap)
                    {
                        return false;
                    }
                    if (other.MinDeep != this.MinDeep)
                    {
                        return false;
                    }
                    if (other.MaxDeep != this.MaxDeep)
                    {
                        return false;
                    }
                    if (other.TrayPackUnits != this.TrayPackUnits)
                    {
                        return false;
                    }
                    if (other.TrayHigh != this.TrayHigh)
                    {
                        return false;
                    }
                    if (other.TrayWide != this.TrayWide)
                    {
                        return false;
                    }
                    if (other.TrayDeep != this.TrayDeep)
                    {
                        return false;
                    }
                    if (other.TrayHeight != this.TrayHeight)
                    {
                        return false;
                    }
                    if (other.TrayWidth != this.TrayWidth)
                    {
                        return false;
                    }
                    if (other.TrayDepth != this.TrayDepth)
                    {
                        return false;
                    }
                    if (other.TrayThickHeight != this.TrayThickHeight)
                    {
                        return false;
                    }
                    if (other.TrayThickWidth != this.TrayThickWidth)
                    {
                        return false;
                    }
                    if (other.TrayThickDepth != this.TrayThickDepth)
                    {
                        return false;
                    }
                    if (other.FrontOverhang != this.FrontOverhang)
                    {
                        return false;
                    }
                    if (other.FingerSpaceAbove != this.FingerSpaceAbove)
                    {
                        return false;
                    }
                    if (other.FingerSpaceToTheSide != this.FingerSpaceToTheSide)
                    {
                        return false;
                    }
                    if (other.StatusType != this.StatusType)
                    {
                        return false;
                    }
                    if (other.OrientationType != this.OrientationType)
                    {
                        return false;
                    }
                    if (other.MerchandisingStyle != this.MerchandisingStyle)
                    {
                        return false;
                    }
                    if (other.IsFrontOnly != this.IsFrontOnly)
                    {
                        return false;
                    }
                    if (other.IsTrayProduct != this.IsTrayProduct)
                    {
                        return false;
                    }
                    if (other.IsPlaceHolderProduct != this.IsPlaceHolderProduct)
                    {
                        return false;
                    }
                    if (other.IsActive != this.IsActive)
                    {
                        return false;
                    }
                    if (other.CanBreakTrayUp != this.CanBreakTrayUp)
                    {
                        return false;
                    }
                    if (other.CanBreakTrayDown != this.CanBreakTrayDown)
                    {
                        return false;
                    }
                    if (other.CanBreakTrayBack != this.CanBreakTrayBack)
                    {
                        return false;
                    }
                    if (other.CanBreakTrayTop != this.CanBreakTrayTop)
                    {
                        return false;
                    }
                    if (other.ForceMiddleCap != this.ForceMiddleCap)
                    {
                        return false;
                    }
                    if (other.ForceBottomCap != this.ForceBottomCap)
                    {
                        return false;
                    }

                    if (other.CanMerchandiseOnShelf != this.CanMerchandiseOnShelf) { return false; }
                    if (other.CanMerchandiseOnBar != this.CanMerchandiseOnBar) { return false; }
                    if (other.CanMerchandiseOnPeg != this.CanMerchandiseOnPeg) { return false; }
                    if (other.CanMerchandiseInChest != this.CanMerchandiseInChest) { return false; }
                }
                else
                {
                    return false;
                }
                return true;
            }
            #endregion
        }

        public class GFSPlanogramPositionDto
        {
            #region Properties

            public Int32 Id { get; set; }
            public Int32 PlanogramContentId { get; set; }
            public Int32 PlanogramFixtureItemId { get; set; }
            public Int32 PlanogramFixtureAssemblyId { get; set; }
            public Int32 PlanogramAssemblyComponentId { get; set; }
            public Int32 PlanogramSubComponentId { get; set; }
            public Int32 PlanogramProductId { get; set; }
            public Single X { get; set; }
            public Single Y { get; set; }
            public Single Z { get; set; }
            public Single Slope { get; set; }
            public Single Angle { get; set; }
            public Single Roll { get; set; }
            public Int32 BlockingColour { get; set; }
            public Byte FrontHigh { get; set; }
            public Byte FrontWide { get; set; }
            public Byte FrontDeep { get; set; }
            public Byte FrontTopCapHigh { get; set; }
            public Byte FrontTopCapWide { get; set; }
            public Byte FrontTopCapDeep { get; set; }
            public Byte FrontRightHigh { get; set; }
            public Byte FrontRightWide { get; set; }
            public Byte FrontRightDeep { get; set; }
            public Byte FrontTopCapRightHigh { get; set; }
            public Byte FrontTopCapRightWide { get; set; }
            public Byte FrontTopCapRightDeep { get; set; }
            public Byte TraysHigh { get; set; }
            public Byte TraysWide { get; set; }
            public Byte TraysDeep { get; set; }
            public Byte TraysRightHigh { get; set; }
            public Byte TraysRightWide { get; set; }
            public Byte TraysRightDeep { get; set; }
            public Byte TrayBreakTopHigh { get; set; }
            public Byte TrayBreakTopWide { get; set; }
            public Byte TrayBreakTopDeep { get; set; }
            public Byte TrayBreakTopRightHigh { get; set; }
            public Byte TrayBreakTopRightWide { get; set; }
            public Byte TrayBreakTopRightDeep { get; set; }
            public Byte TrayBreakBackHigh { get; set; }
            public Byte TrayBreakBackWide { get; set; }
            public Byte TrayBreakBackDeep { get; set; }
            public Byte TrayBreakUpHigh { get; set; }
            public Byte TrayBreakUpWide { get; set; }
            public Byte TrayBreakUpDeep { get; set; }
            public Byte TrayBreakDownHigh { get; set; }
            public Byte TrayBreakDownWide { get; set; }
            public Byte TrayBreakDownDeep { get; set; }
            public Int16 UnitsHigh { get; set; }
            public Int16 UnitsWide { get; set; }
            public Int16 UnitsDeep { get; set; }
            public Int16 TotalUnits { get; set; }
            public Int16 BaySequenceNumber { get; set; }
            public Int16 ElementSequenceNumber { get; set; }
            public Int16 PositionSequenceNumber { get; set; }

            #endregion

            #region Methods
            /// <summary>
            /// Returns the objects hash code
            /// </summary>
            /// <returns>The objects hash code</returns>
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            /// <summary>
            /// Compares two instance of this type
            /// </summary>
            /// <param name="obj">The object to compare to</param>
            /// <returns>True if equal, else false</returns>
            public override bool Equals(object obj)
            {
                GFSPlanogramPositionDto other = obj as GFSPlanogramPositionDto;
                if (other != null)
                {
                    if (other.Id != this.Id)
                    {
                        return false;
                    }
                    if (other.PlanogramContentId != this.PlanogramContentId)
                    {
                        return false;
                    }
                    if (other.PlanogramFixtureItemId != this.PlanogramFixtureItemId)
                    {
                        return false;
                    }
                    if (other.PlanogramFixtureAssemblyId != this.PlanogramFixtureAssemblyId)
                    {
                        return false;
                    }
                    if (other.PlanogramAssemblyComponentId != this.PlanogramAssemblyComponentId)
                    {
                        return false;
                    }
                    if (other.PlanogramSubComponentId != this.PlanogramSubComponentId)
                    {
                        return false;
                    }
                    if (other.PlanogramProductId != this.PlanogramProductId)
                    {
                        return false;
                    }
                    if (other.X != this.X)
                    {
                        return false;
                    }
                    if (other.Y != this.Y)
                    {
                        return false;
                    }
                    if (other.Z != this.Z)
                    {
                        return false;
                    }
                    if (other.Slope != this.Slope)
                    {
                        return false;
                    }
                    if (other.Angle != this.Angle)
                    {
                        return false;
                    }
                    if (other.Roll != this.Roll)
                    {
                        return false;
                    }
                    if (other.UnitsHigh != this.UnitsHigh)
                    {
                        return false;
                    }
                    if (other.UnitsWide != this.UnitsWide)
                    {
                        return false;
                    }
                    if (other.UnitsDeep != this.UnitsDeep)
                    {
                        return false;
                    }
                    if (other.BlockingColour != this.BlockingColour) { return false; }
                    if (other.FrontHigh != this.FrontHigh) { return false; }
                    if (other.FrontWide != this.FrontWide) { return false; }
                    if (other.FrontDeep != this.FrontDeep) { return false; }
                    if (other.FrontTopCapHigh != this.FrontTopCapHigh) { return false; }
                    if (other.FrontTopCapWide != this.FrontTopCapWide) { return false; }
                    if (other.FrontTopCapDeep != this.FrontTopCapDeep) { return false; }
                    if (other.FrontTopCapRightHigh != this.FrontTopCapRightHigh) { return false; }
                    if (other.FrontTopCapRightWide != this.FrontTopCapRightWide) { return false; }
                    if (other.FrontTopCapRightDeep != this.FrontTopCapRightDeep) { return false; }
                    if (other.FrontRightHigh != this.FrontRightHigh) { return false; }
                    if (other.FrontRightWide != this.FrontRightWide) { return false; }
                    if (other.FrontRightDeep != this.FrontRightDeep) { return false; }
                    if (other.TraysHigh != this.TraysHigh) { return false; }
                    if (other.TraysWide != this.TraysWide) { return false; }
                    if (other.TraysDeep != this.TraysDeep) { return false; }
                    if (other.TraysRightHigh != this.TraysRightHigh) { return false; }
                    if (other.TraysRightWide != this.TraysRightWide) { return false; }
                    if (other.TraysRightDeep != this.TraysRightDeep) { return false; }
                    if (other.TrayBreakTopHigh != this.TrayBreakTopHigh) { return false; }
                    if (other.TrayBreakTopWide != this.TrayBreakTopWide) { return false; }
                    if (other.TrayBreakTopDeep != this.TrayBreakTopDeep) { return false; }
                    if (other.TrayBreakTopRightHigh != this.TrayBreakTopRightHigh) { return false; }
                    if (other.TrayBreakTopRightWide != this.TrayBreakTopRightWide) { return false; }
                    if (other.TrayBreakTopRightDeep != this.TrayBreakTopRightDeep) { return false; }
                    if (other.TrayBreakBackHigh != this.TrayBreakBackHigh) { return false; }
                    if (other.TrayBreakBackWide != this.TrayBreakBackWide) { return false; }
                    if (other.TrayBreakBackDeep != this.TrayBreakBackDeep) { return false; }
                    if (other.TrayBreakUpHigh != this.TrayBreakUpHigh) { return false; }
                    if (other.TrayBreakUpWide != this.TrayBreakUpWide) { return false; }
                    if (other.TrayBreakUpDeep != this.TrayBreakUpDeep) { return false; }
                    if (other.TrayBreakDownHigh != this.TrayBreakDownHigh) { return false; }
                    if (other.TrayBreakDownWide != this.TrayBreakDownWide) { return false; }
                    if (other.TrayBreakDownDeep != this.TrayBreakDownDeep) { return false; }
                    if (other.TotalUnits != this.TotalUnits) { return false; }
                    if (other.BaySequenceNumber != this.BaySequenceNumber) { return false; }
                    if (other.ElementSequenceNumber != this.ElementSequenceNumber) { return false; }
                    if (other.PositionSequenceNumber != this.PositionSequenceNumber) { return false; }
                }
                else
                {
                    return false;
                }
                return true;
            }
            #endregion
        }

        public class GFSPlanogramFixtureItemDto
        {
            #region Properties
            public Int32 Id { get; set; }
            public Int32 PlanogramFixtureId { get; set; }
            public Single X { get; set; }
            public Single Y { get; set; }
            public Single Z { get; set; }
            public Single Slope { get; set; }
            public Single Angle { get; set; }
            public Single Roll { get; set; }
            public Boolean IsBay { get; set; }
            public Int32 PlanogramContentId { get; set; }
            #endregion

            #region Methods

            public override int GetHashCode()
            {
                return this.Id.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                GFSPlanogramFixtureItemDto other = obj as GFSPlanogramFixtureItemDto;
                if (other != null)
                {
                    if (other.Id != this.Id)
                    {
                        return false;
                    }
                    if (other.PlanogramFixtureId != this.PlanogramFixtureId)
                    {
                        return false;
                    }
                    if (other.X != this.X)
                    {
                        return false;
                    }
                    if (other.Y != this.Y)
                    {
                        return false;
                    }
                    if (other.Z != this.Z)
                    {
                        return false;
                    }
                    if (other.Slope != this.Slope)
                    {
                        return false;
                    }
                    if (other.Angle != this.Angle)
                    {
                        return false;
                    }
                    if (other.Roll != this.Roll)
                    {
                        return false;
                    }
                    if (other.IsBay != this.IsBay)
                    {
                        return false;
                    }
                    if (other.PlanogramContentId != this.PlanogramContentId)
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                return true;
            }
            #endregion
        }

        public class GFSPlanogramFixtureDto
        {
            #region Properties
            public Int32 Id { get; set; }
            public Int32 PlanogramContentId { get; set; }
            public String Name { get; set; }
            public Single Height { get; set; }
            public Single Width { get; set; }
            public Single Depth { get; set; }
            //public Int16 NumberOfAssemblies { get; set; }
            //public Int16 NumberOfMerchandisedSubComponents { get; set; }
            //public Single TotalFixtureCost { get; set; }
            //public Int16 UniqueProductCount { get; set; }
            #endregion

            #region Methods
            /// <summary>
            /// Returns the objects hash code
            /// </summary>
            /// <returns>The objects hash code</returns>
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            /// <summary>
            /// Compares two instance of this type
            /// </summary>
            /// <param name="obj">The object to compare to</param>
            /// <returns>True if equal, else false</returns>
            public override bool Equals(object obj)
            {
                GFSPlanogramFixtureDto other = obj as GFSPlanogramFixtureDto;
                if (other != null)
                {
                    if (other.Id != this.Id)
                    {
                        return false;
                    }
                    if (other.PlanogramContentId != this.PlanogramContentId)
                    {
                        return false;
                    }
                    if (other.Name != this.Name)
                    {
                        return false;
                    }
                    if (other.Height != this.Height)
                    {
                        return false;
                    }
                    if (other.Width != this.Width)
                    {
                        return false;
                    }
                    if (other.Depth != this.Depth)
                    {
                        return false;
                    }
                    //if (other.NumberOfAssemblies != this.NumberOfAssemblies)
                    //{
                    //    return false;
                    //}
                    //if (other.NumberOfMerchandisedSubComponents != this.NumberOfMerchandisedSubComponents)
                    //{
                    //    return false;
                    //}
                    //if (other.TotalFixtureCost != this.TotalFixtureCost)
                    //{
                    //    return false;
                    //}
                    //if (other.UniqueProductCount != this.UniqueProductCount)
                    //{
                    //    return false;
                    //}
                }
                else
                {
                    return false;
                }
                return true;
            }
            #endregion
        }

        public class GFSPlanogramFixtureAssemblyDto
        {
            #region Properties
            public Int32 Id { get; set; }
            public Int32 PlanogramFixtureId { get; set; }
            public Int32 PlanogramAssemblyId { get; set; }
            public Single X { get; set; }
            public Single Y { get; set; }
            public Single Z { get; set; }
            public Single Slope { get; set; }
            public Single Angle { get; set; }
            public Single Roll { get; set; }
            #endregion

            #region Methods

            public override int GetHashCode()
            {
                return this.Id.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                GFSPlanogramFixtureAssemblyDto other = obj as GFSPlanogramFixtureAssemblyDto;
                if (other != null)
                {
                    if (other.Id != this.Id)
                    {
                        return false;
                    }
                    if (other.PlanogramFixtureId != this.PlanogramFixtureId)
                    {
                        return false;
                    }
                    if (other.PlanogramAssemblyId != this.PlanogramAssemblyId)
                    {
                        return false;
                    }
                    if (other.X != this.X)
                    {
                        return false;
                    }
                    if (other.Y != this.Y)
                    {
                        return false;
                    }
                    if (other.Z != this.Z)
                    {
                        return false;
                    }
                    if (other.Slope != this.Slope)
                    {
                        return false;
                    }
                    if (other.Angle != this.Angle)
                    {
                        return false;
                    }
                    if (other.Roll != this.Roll)
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                return true;
            }
            #endregion
        }

        public class GFSPlanogramAnnotationDto
        {
            #region Properties

            public Int32 Id { get; set; }
            public Int32? PlanogramContentId { get; set; }
            public Int32? PlanogramFixtureItemId { get; set; }
            public Int32? PlanogramFixtureAssemblyId { get; set; }
            public Int32? PlanogramAssemblyComponentId { get; set; }
            public Int32? PlanogramSubComponentId { get; set; }
            public Int32? PlanogramPositionId { get; set; }
            public String Text { get; set; }
            public Single? X { get; set; }
            public Single? Y { get; set; }
            public Single? Z { get; set; }
            public Single Height { get; set; }
            public Single Width { get; set; }
            public Single Depth { get; set; }
            public Byte AnnotationType { get; set; }

            #endregion

            #region Methods
            /// <summary>
            /// Returns the objects hash code
            /// </summary>
            /// <returns>The objects hash code</returns>
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            /// <summary>
            /// Compares two instance of this type
            /// </summary>
            /// <param name="obj">The object to compare to</param>
            /// <returns>True if equal, else false</returns>
            public override bool Equals(object obj)
            {
                GFSPlanogramAnnotationDto other = obj as GFSPlanogramAnnotationDto;
                if (other != null)
                {
                    if (other.Id != this.Id)
                    {
                        return false;
                    }
                    if (other.PlanogramContentId != this.PlanogramContentId)
                    {
                        return false;
                    }
                    if (other.PlanogramFixtureItemId != this.PlanogramFixtureItemId)
                    {
                        return false;
                    }
                    if (other.PlanogramFixtureAssemblyId != this.PlanogramFixtureAssemblyId)
                    {
                        return false;
                    }
                    if (other.PlanogramAssemblyComponentId != this.PlanogramAssemblyComponentId)
                    {
                        return false;
                    }
                    if (other.PlanogramSubComponentId != this.PlanogramSubComponentId)
                    {
                        return false;
                    }
                    if (other.PlanogramPositionId != this.PlanogramPositionId)
                    {
                        return false;
                    }
                    if (other.Text != this.Text)
                    {
                        return false;
                    }
                    if (other.X != this.X)
                    {
                        return false;
                    }
                    if (other.Y != this.Y)
                    {
                        return false;
                    }
                    if (other.Z != this.Z)
                    {
                        return false;
                    }
                    if (other.Height != this.Height)
                    {
                        return false;
                    }
                    if (other.Width != this.Width)
                    {
                        return false;
                    }
                    if (other.Depth != this.Depth)
                    {
                        return false;
                    }
                    if (other.AnnotationType != this.AnnotationType)
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                return true;
            }
            #endregion
        }

        public class GFSPlanogramAssemblyComponentDto
        {
            #region Properties
            public Int32 Id { get; set; }
            public Int32 PlanogramAssemblyId { get; set; }
            public Int32 PlanogramComponentId { get; set; }
            public Single X { get; set; }
            public Single Y { get; set; }
            public Single Z { get; set; }
            public Single Slope { get; set; }
            public Single Angle { get; set; }
            public Single Roll { get; set; }
            #endregion

            #region Methods

            public override int GetHashCode()
            {
                return this.Id.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                GFSPlanogramAssemblyComponentDto other = obj as GFSPlanogramAssemblyComponentDto;
                if (other != null)
                {
                    if (other.Id != this.Id)
                    {
                        return false;
                    }
                    if (other.PlanogramAssemblyId != this.PlanogramAssemblyId)
                    {
                        return false;
                    }
                    if (other.PlanogramComponentId != this.PlanogramComponentId)
                    {
                        return false;
                    }
                    if (other.X != this.X)
                    {
                        return false;
                    }
                    if (other.Y != this.Y)
                    {
                        return false;
                    }
                    if (other.Z != this.Z)
                    {
                        return false;
                    }
                    if (other.Slope != this.Slope)
                    {
                        return false;
                    }
                    if (other.Angle != this.Angle)
                    {
                        return false;
                    }
                    if (other.Roll != this.Roll)
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                return true;
            }
            #endregion
        }

        public class GFSPlanogramAssemblyDto
        {
            #region Properties
            public Int32 Id { get; set; }
            public String Name { get; set; }
            public Single Height { get; set; }
            public Single Width { get; set; }
            public Single Depth { get; set; }
            //public Int16 NumberOfComponents { get; set; }
            public Single TotalComponentCost { get; set; }
            public Int32 PlanogramContentId { get; set; }
            #endregion

            #region Methods

            public override int GetHashCode()
            {
                return this.Id.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                GFSPlanogramAssemblyDto other = obj as GFSPlanogramAssemblyDto;
                if (other != null)
                {
                    if (other.Id != this.Id)
                    {
                        return false;
                    }
                    if (other.Name != this.Name)
                    {
                        return false;
                    }
                    if (other.Height != this.Height)
                    {
                        return false;
                    }
                    if (other.Width != this.Width)
                    {
                        return false;
                    }
                    if (other.Depth != this.Depth)
                    {
                        return false;
                    }
                    //if (other.NumberOfComponents != this.NumberOfComponents)
                    //{
                    //    return false;
                    //}
                    if (other.TotalComponentCost != this.TotalComponentCost)
                    {
                        return false;
                    }
                    if (other.PlanogramContentId != this.PlanogramContentId)
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                return true;
            }
            #endregion
        }

        public class GFSPlanogramComponentDto
        {
            #region Properties
            public Int32 Id { get; set; }
            //public Int32 Mesh3DId { get; set; }
            //public Int32 ImageIdFront { get; set; }
            //public Int32 ImageIdBack { get; set; }
            //public Int32 ImageIdTop { get; set; }
            //public Int32 ImageIdBottom { get; set; }
            //public Int32 ImageIdLeft { get; set; }
            //public Int32 ImageIdRight { get; set; }
            public String Name { get; set; }
            public Single Height { get; set; }
            public Single Width { get; set; }
            public Single Depth { get; set; }
            public Byte MerchandiseType { get; set; }
            //public Int16 NumberOfSubComponents { get; set; }
            //public Int16 NumberOfMerchandisedSubComponents { get; set; }
            public Int16 NumberOfShelfEdgeLabels { get; set; }
            public Boolean IsMoveable { get; set; }
            public Boolean IsDisplayOnly { get; set; }
            public Boolean CanAttachShelfEdgeLabel { get; set; }
            public String RetailerReference { get; set; }
            public String BarCode { get; set; }
            public String Manufacturer { get; set; }
            public String ManufacturerPartName { get; set; }
            public String ManufacturerPartNumber { get; set; }
            public String SupplierName { get; set; }
            public String SupplierPartNumber { get; set; }
            public Single SupplierCostPrice { get; set; }
            public Single SupplierDiscount { get; set; }
            public Single SupplierLeadTime { get; set; }
            public Byte MinPurchaseQty { get; set; }
            public Single WeightLimit { get; set; }
            public Single Weight { get; set; }
            public Single Volume { get; set; }
            public Single Diameter { get; set; }
            public Int32 Colour { get; set; }
            public Int16 Capacity { get; set; }
            public Int32 PlanogramContentId { get; set; }

            #endregion

            #region Methods

            public override int GetHashCode()
            {
                return this.Id.GetHashCode();
            }

            public override bool Equals(object obj)
            {
                GFSPlanogramComponentDto other = obj as GFSPlanogramComponentDto;
                if (other != null)
                {
                    if (other.Id != this.Id)
                    {
                        return false;
                    }
                    //if (other.Mesh3DId != this.Mesh3DId)
                    //{
                    //    return false;
                    //}
                    //if (other.ImageIdFront != this.ImageIdFront)
                    //{
                    //    return false;
                    //}
                    //if (other.ImageIdBack != this.ImageIdBack)
                    //{
                    //    return false;
                    //}
                    //if (other.ImageIdTop != this.ImageIdTop)
                    //{
                    //    return false;
                    //}
                    //if (other.ImageIdBottom != this.ImageIdBottom)
                    //{
                    //    return false;
                    //}
                    //if (other.ImageIdLeft != this.ImageIdLeft)
                    //{
                    //    return false;
                    //}
                    //if (other.ImageIdRight != this.ImageIdRight)
                    //{
                    //    return false;
                    //}
                    if (other.Name != this.Name)
                    {
                        return false;
                    }
                    if (other.Height != this.Height)
                    {
                        return false;
                    }
                    if (other.Width != this.Width)
                    {
                        return false;
                    }
                    if (other.Depth != this.Depth)
                    {
                        return false;
                    }
                    if (other.MerchandiseType != this.MerchandiseType)
                    {
                        return false;
                    }
                    //if (other.NumberOfSubComponents != this.NumberOfSubComponents)
                    //{
                    //    return false;
                    //}
                    //if (other.NumberOfMerchandisedSubComponents != this.NumberOfMerchandisedSubComponents)
                    //{
                    //    return false;
                    //}
                    if (other.NumberOfShelfEdgeLabels != this.NumberOfShelfEdgeLabels)
                    {
                        return false;
                    }
                    if (other.IsMoveable != this.IsMoveable)
                    {
                        return false;
                    }
                    if (other.IsDisplayOnly != this.IsDisplayOnly)
                    {
                        return false;
                    }
                    if (other.CanAttachShelfEdgeLabel != this.CanAttachShelfEdgeLabel)
                    {
                        return false;
                    }
                    if (other.RetailerReference != this.RetailerReference)
                    {
                        return false;
                    }
                    if (other.BarCode != this.BarCode)
                    {
                        return false;
                    }
                    if (other.Manufacturer != this.Manufacturer)
                    {
                        return false;
                    }
                    if (other.ManufacturerPartName != this.ManufacturerPartName)
                    {
                        return false;
                    }
                    if (other.ManufacturerPartNumber != this.ManufacturerPartNumber)
                    {
                        return false;
                    }
                    if (other.SupplierName != this.SupplierName)
                    {
                        return false;
                    }
                    if (other.SupplierPartNumber != this.SupplierPartNumber)
                    {
                        return false;
                    }
                    if (other.SupplierCostPrice != this.SupplierCostPrice)
                    {
                        return false;
                    }
                    if (other.SupplierDiscount != this.SupplierDiscount)
                    {
                        return false;
                    }
                    if (other.SupplierLeadTime != this.SupplierLeadTime)
                    {
                        return false;
                    }
                    if (other.MinPurchaseQty != this.MinPurchaseQty)
                    {
                        return false;
                    }
                    if (other.WeightLimit != this.WeightLimit)
                    {
                        return false;
                    }
                    if (other.Weight != this.Weight)
                    {
                        return false;
                    }
                    if (other.Volume != this.Volume)
                    {
                        return false;
                    }
                    if (other.Diameter != this.Diameter)
                    {
                        return false;
                    }
                    if (other.Colour != this.Colour)
                    {
                        return false;
                    }
                    if (other.Capacity != this.Capacity)
                    {
                        return false;
                    }
                    if (other.PlanogramContentId != this.PlanogramContentId)
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                return true;
            }
            #endregion
        }

        public class GFSPlanogramDto
        {
            #region Properties
            public Int32 Id { get; set; }
            //public RowVersion RowVersion { get; set; }
            //public Int32? LocationGroupId { get; set; }
            //public Int16? LocationId { get; set; }
            public Int32? ProductGroupId { get; set; }
            public Guid UniqueContentReference { get; set; }
            public String Name { get; set; }
            public String Description { get; set; }
            //public Byte ContentType { get; set; }
            //public Int32 ContentVersion { get; set; }
            //public String Tags { get; set; }
            //public String ProductName { get; set; }
            //public String ComputerName { get; set; }
            //public Int32 UserId { get; set; }
            //public DateTime DateCreated { get; set; }
            //public DateTime DateLastModified { get; set; }
            //public Int32? ParentContentId { get; set; }
            public Int32 EntityId { get; set; }
            public Boolean IsLatestVersion { get; set; }
            //public DateTime? DateDeleted { get; set; }
            public Single Height { get; set; }
            public Single Width { get; set; }
            public Single Depth { get; set; }
            //public Int16 UniqueProductCount { get; set; }
            //public Int32 TotalPositionCount { get; set; }
            //public Int32 BayCount { get; set; }
            //public Int32 ComponentCount { get; set; }
            //public Single MerchandisableLinearSpace { get; set; }
            //public Single MerchandisableAreaSpace { get; set; }
            //public Double MerchandisableVolumetricSpace { get; set; }
            //public Int32 TotalFacingCount { get; set; }
            //public Int32 ProductsDroppedCount { get; set; }
            //public Single WhiteSpacePercentage { get; set; }
            //public Single TotalProductLinearSpace { get; set; }
            #endregion

        }

        public class GFSProductGroupDto
        {
            public String Code { get; set; }
            public String Name { get; set; }
        }

        #endregion

        #region Fields

        private Boolean _isMetric = true;
        private GFSPlanogramDto _planDto;
        private List<GFSPlanogramSubComponentDto> _subComponentDtos = new List<GFSPlanogramSubComponentDto>();
        private List<GFSPlanogramProductDto> _productDtos = new List<GFSPlanogramProductDto>();
        private List<GFSPlanogramPositionDto> _positionDtos = new List<GFSPlanogramPositionDto>();
        private List<GFSPlanogramFixtureItemDto> _fixtureItemDtos = new List<GFSPlanogramFixtureItemDto>();
        private List<GFSPlanogramFixtureDto> _fixtureDtos = new List<GFSPlanogramFixtureDto>();
        private List<GFSPlanogramFixtureAssemblyDto> _fixtureAssemblyDtos = new List<GFSPlanogramFixtureAssemblyDto>();
        private List<GFSPlanogramAnnotationDto> _annotationDtos = new List<GFSPlanogramAnnotationDto>();
        private List<GFSPlanogramAssemblyComponentDto> _assemblyComponentDtos = new List<GFSPlanogramAssemblyComponentDto>();
        private List<GFSPlanogramAssemblyDto> _assemblyDtos = new List<GFSPlanogramAssemblyDto>();
        private List<GFSPlanogramComponentDto> _componentDtos = new List<GFSPlanogramComponentDto>();
        private GFSProductGroupDto _productGroupDto = new GFSProductGroupDto();

        private Planogram _plan;
        private Entity _entity;

        #endregion

        #region Properties

        public Planogram Plan
        {
            get { return _plan; }
        }

        public String PlanogramGroupName { get; set; }

        public Boolean IsMetric
        {
            get { return _isMetric; }
            set { _isMetric = value; }
        }

        public GFSPlanogramDto GfsPlanogram
        {
            get { return _planDto; }
            set { _planDto = value; }
        }
        public List<GFSPlanogramSubComponentDto> GfsSubComponents
        {
            get { return _subComponentDtos; }
        }
        public List<GFSPlanogramProductDto> GfsProducts
        {
            get { return _productDtos; }
        }
        public List<GFSPlanogramPositionDto> GfsPositions
        {
            get { return _positionDtos; }
        }
        public List<GFSPlanogramFixtureItemDto> GfsFixtureItems
        {
            get { return _fixtureItemDtos; }
        }
        public List<GFSPlanogramFixtureDto> GfsFixtures
        {
            get { return _fixtureDtos; }
        }
        public List<GFSPlanogramFixtureAssemblyDto> GfsFixtureAssemblies
        {
            get { return _fixtureAssemblyDtos; }
        }
        public List<GFSPlanogramAnnotationDto> GfsAnnotations
        {
            get { return _annotationDtos; }
        }
        public List<GFSPlanogramAssemblyComponentDto> GfsAssemblyComponents
        {
            get { return _assemblyComponentDtos; }
        }
        public List<GFSPlanogramAssemblyDto> GfsAssemblies
        {
            get { return _assemblyDtos; }
        }
        public List<GFSPlanogramComponentDto> GfsComponents
        {
            get { return _componentDtos; }
        }
        public GFSProductGroupDto GfsProductGroup
        {
            get { return _productGroupDto; }
            set { _productGroupDto = value; }
        }

        #endregion

        #region Constructor

        public GfsToCcmPlanogramConverter(Entity ccmEntity)
        {
            _entity = ccmEntity;
        }

        #endregion


        #region Methods

        public Planogram Convert()
        {
            _plan = ConvertToCCM();
            return _plan;
        }
        
        private Planogram ConvertToCCM()
        {
            Planogram plan = Planogram.NewPlanogram();

            Dictionary<Int32, Object> productIds = new Dictionary<Int32, Object>();
            Dictionary<Int32, Object> subComponentIds = new Dictionary<Int32, Object>();
            Dictionary<Int32, Object> componentIds = new Dictionary<Int32, Object>();
            Dictionary<Int32, Object> assemblyIds = new Dictionary<Int32, Object>();
            Dictionary<Int32, Object> assemblyComponentIds = new Dictionary<Int32, Object>();
            Dictionary<Int32, Object> fixtureIds = new Dictionary<Int32, Object>();
            Dictionary<Int32, Object> fixtureAssemblyIds = new Dictionary<Int32, Object>();
            Dictionary<Int32, Object> fixtureItemIds = new Dictionary<Int32, Object>();
            Dictionary<Int32, Object> positionIds = new Dictionary<Int32, Object>();

            #region Planogram

            GFSPlanogramDto planDto = _planDto;

            plan.Name = planDto.Name;
            plan.Height = planDto.Height;
            plan.Width = planDto.Width;
            plan.Depth = planDto.Depth;
            if (_productGroupDto != null)
            {
                plan.CategoryCode = _productGroupDto.Code;
                plan.CategoryName = _productGroupDto.Name;
            }
            plan.CurrencyUnitsOfMeasure = PlanogramCurrencyUnitOfMeasureType.GBP;

            if (_isMetric)
            {
                plan.LengthUnitsOfMeasure = PlanogramLengthUnitOfMeasureType.Centimeters;
                plan.AreaUnitsOfMeasure = PlanogramAreaUnitOfMeasureType.SquareCentimeters;
                plan.VolumeUnitsOfMeasure = PlanogramVolumeUnitOfMeasureType.Litres;
                plan.WeightUnitsOfMeasure = PlanogramWeightUnitOfMeasureType.Kilograms;
            }
            else
            {
                plan.LengthUnitsOfMeasure = PlanogramLengthUnitOfMeasureType.Inches;
                plan.AreaUnitsOfMeasure = PlanogramAreaUnitOfMeasureType.SquareInches;
                plan.VolumeUnitsOfMeasure = PlanogramVolumeUnitOfMeasureType.CubicFeet;
                plan.WeightUnitsOfMeasure = PlanogramWeightUnitOfMeasureType.Pounds;
            }

            #endregion

            #region Products
            foreach (GFSPlanogramProductDto productDto in _productDtos)
            {
                PlanogramProduct product = PlanogramProduct.NewPlanogramProduct();
                product.Gtin = productDto.GTIN;
                product.Name = productDto.Name;
                product.Brand = productDto.Brand;
                product.Height = productDto.Height;
                product.Width = productDto.Width;
                product.Depth = productDto.Depth;
                product.DisplayHeight = productDto.DisplayHeight;
                product.DisplayWidth = productDto.DisplayWidth;
                product.DisplayDepth = productDto.DisplayDepth;
                product.AlternateHeight = productDto.AlternateHeight;
                product.AlternateWidth = productDto.AlternateWidth;
                product.AlternateDepth = productDto.AlternateDepth;
                product.PointOfPurchaseHeight = productDto.PointOfPurchaseHeight;
                product.PointOfPurchaseWidth = productDto.PointOfPurchaseWidth;
                product.PointOfPurchaseDepth = productDto.PointOfPurchaseDepth;
                product.NumberOfPegHoles = productDto.NumberOfPegHoles;
                product.PegX = productDto.PegX;
                product.PegX2 = productDto.PegX2;
                product.PegX3 = productDto.PegX3;
                product.PegY = productDto.PegY;
                product.PegY2 = productDto.PegY2;
                product.PegY3 = productDto.PegY3;
                product.PegProngOffsetX = productDto.PegProngOffset;
                product.PegDepth = productDto.PegDepth;
                product.SqueezeHeight = Math.Min(1, productDto.SqueezeHeight);
                product.SqueezeWidth = Math.Min(1, productDto.SqueezeWidth);
                product.SqueezeDepth = Math.Min(1, productDto.SqueezeDepth);
                product.NestingHeight = productDto.NestingHeight;
                product.NestingWidth = productDto.NestingWidth;
                product.NestingDepth = productDto.NestingDepth;
                product.CasePackUnits = productDto.CasePackUnits;
                product.CaseHigh = productDto.CaseHigh;
                product.CaseWide = productDto.CaseWide;
                product.CaseDeep = productDto.CaseDeep;
                product.CaseHeight = productDto.CaseHeight;
                product.CaseWidth = productDto.CaseWidth;
                product.CaseDepth = productDto.CaseDepth;
                product.MaxStack = productDto.MaxStack;
                product.MaxTopCap = productDto.MaxTopCap;
                product.MaxRightCap = productDto.MaxRightCap;
                product.MinDeep = productDto.MinDeep;
                product.MaxDeep = productDto.MaxDeep;
                product.TrayPackUnits = productDto.TrayPackUnits;
                product.TrayHigh = productDto.TrayHigh;
                product.TrayWide = productDto.TrayWide;
                product.TrayDeep = productDto.TrayDeep;
                product.TrayHeight = productDto.TrayHeight;
                product.TrayWidth = productDto.TrayWidth;
                product.TrayDepth = productDto.TrayDepth;
                product.TrayThickHeight = productDto.TrayThickHeight;
                product.TrayThickWidth = productDto.TrayThickWidth;
                product.TrayThickDepth = productDto.TrayThickDepth;
                product.FrontOverhang = productDto.FrontOverhang;
                product.FingerSpaceAbove = productDto.FingerSpaceAbove;
                product.FingerSpaceToTheSide = productDto.FingerSpaceToTheSide;
                product.StatusType = (PlanogramProductStatusType)productDto.StatusType;
                product.OrientationType = (PlanogramProductOrientationType)productDto.OrientationType;
                product.MerchandisingStyle = (PlanogramProductMerchandisingStyle)productDto.MerchandisingStyle;
                product.IsFrontOnly = productDto.IsFrontOnly;
                product.IsPlaceHolderProduct = productDto.IsPlaceHolderProduct;
                product.IsActive = productDto.IsActive;
                product.CanBreakTrayUp = productDto.CanBreakTrayUp;
                product.CanBreakTrayDown = productDto.CanBreakTrayDown;
                product.CanBreakTrayBack = productDto.CanBreakTrayBack;
                product.CanBreakTrayTop = productDto.CanBreakTrayTop;
                product.ForceMiddleCap = productDto.ForceMiddleCap;
                product.ForceBottomCap = productDto.ForceBottomCap;

                product.FillColour = -1;
                product.ShapeType = PlanogramProductShapeType.Box;
                product.FillPatternType = PlanogramProductFillPatternType.Solid;

                plan.Products.Add(product);
                productIds.Add(productDto.Id, product.Id);
            }
            #endregion

            #region Components/SubComponents
            foreach (GFSPlanogramComponentDto componentDto in _componentDtos)
            {
                #region Component
                PlanogramComponent component = PlanogramComponent.NewPlanogramComponent();
                componentIds.Add(componentDto.Id, component.Id);
                component.Name = componentDto.Name;
                component.Height = componentDto.Height;
                component.Width = componentDto.Width;
                component.Depth = componentDto.Depth;
                component.IsMoveable = componentDto.IsMoveable;
                component.IsDisplayOnly = componentDto.IsDisplayOnly;
                component.CanAttachShelfEdgeLabel = componentDto.CanAttachShelfEdgeLabel;
                component.RetailerReferenceCode = componentDto.RetailerReference;
                component.BarCode = componentDto.BarCode;
                component.Manufacturer = componentDto.Manufacturer;
                component.ManufacturerPartName = componentDto.ManufacturerPartName;
                component.ManufacturerPartNumber = componentDto.ManufacturerPartNumber;
                component.SupplierName = componentDto.SupplierName;
                component.SupplierPartNumber = componentDto.SupplierPartNumber;
                component.SupplierCostPrice = componentDto.SupplierCostPrice;
                component.SupplierDiscount = componentDto.SupplierDiscount;
                component.SupplierLeadTime = componentDto.SupplierLeadTime;
                component.MinPurchaseQty = componentDto.MinPurchaseQty;
                component.WeightLimit = componentDto.WeightLimit;
                component.Weight = componentDto.Weight;
                component.Volume = componentDto.Volume;
                component.Diameter = componentDto.Diameter;
                component.Capacity = componentDto.Capacity;
                #endregion

                #region SubComponents
                foreach (GFSPlanogramSubComponentDto subDto in _subComponentDtos)
                {
                    if (subDto.ComponentId == componentDto.Id)
                    {
                        PlanogramSubComponent subComponent = PlanogramSubComponent.NewPlanogramSubComponent();
                        subComponentIds.Add(subDto.Id, subComponent.Id);
                        subComponent.Name = subDto.Name;
                        subComponent.Height = subDto.Height;
                        subComponent.Width = subDto.Width;
                        subComponent.Depth = subDto.Depth;
                        subComponent.X = subDto.X;
                        subComponent.Y = subDto.Y;
                        subComponent.Z = subDto.Z;
                        subComponent.Slope = subDto.Slope;
                        subComponent.Angle = subDto.Angle;
                        subComponent.Roll = subDto.Roll;
                        subComponent.ShapeType = (PlanogramSubComponentShapeType)subDto.ShapeType;
                        subComponent.MerchandisableHeight = subDto.MerchandisableHeight;
                        subComponent.IsVisible = subDto.IsVisible;
                        subComponent.HasCollisionDetection = subDto.HasCollisionDetection;
                        subComponent.FillPatternTypeFront = (PlanogramSubComponentFillPatternType)subDto.FillPatternTypeFront;
                        subComponent.FillPatternTypeBack = (PlanogramSubComponentFillPatternType)subDto.FillPatternTypeBack;
                        subComponent.FillPatternTypeTop = (PlanogramSubComponentFillPatternType)subDto.FillPatternTypeTop;
                        subComponent.FillPatternTypeBottom = (PlanogramSubComponentFillPatternType)subDto.FillPatternTypeBottom;
                        subComponent.FillPatternTypeLeft = (PlanogramSubComponentFillPatternType)subDto.FillPatternTypeLeft;
                        subComponent.FillPatternTypeRight = (PlanogramSubComponentFillPatternType)subDto.FillPatternTypeRight;
                        subComponent.LineColour = subDto.LineColour;
                        subComponent.TransparencyPercentFront = subDto.TransparencyPercentFront;
                        subComponent.TransparencyPercentBack = subDto.TransparencyPercentBack;
                        subComponent.TransparencyPercentTop = subDto.TransparencyPercentTop;
                        subComponent.TransparencyPercentBottom = subDto.TransparencyPercentBottom;
                        subComponent.TransparencyPercentLeft = subDto.TransparencyPercentLeft;
                        subComponent.TransparencyPercentRight = subDto.TransparencyPercentRight;
                        subComponent.FaceThicknessFront = subDto.FaceThickness;
                        subComponent.FaceThicknessBack = subDto.FaceThickness;
                        subComponent.FaceThicknessTop = subDto.FaceThicknessTop;
                        subComponent.FaceThicknessBottom = subDto.FaceThickness;
                        subComponent.FaceThicknessLeft = subDto.FaceThickness;
                        subComponent.FaceThicknessRight = subDto.FaceThickness;
                        subComponent.RiserHeight = subDto.RiserHeight;
                        subComponent.IsRiserPlacedOnRight = subDto.IsRiserPlacedOnRight;
                        subComponent.IsRiserPlacedOnLeft = subDto.IsRiserPlacedOnLeft;
                        subComponent.IsRiserPlacedOnBack = subDto.IsRiserPlacedOnBack;
                        subComponent.IsRiserPlacedOnFront = subDto.IsRiserPlacedOnFront;
                        subComponent.RiserFillPatternType = (PlanogramSubComponentFillPatternType)subDto.RiserFillPatternType;
                        subComponent.NotchStartX = subDto.NotchStartX;
                        subComponent.NotchSpacingX = subDto.NotchSpacingX;
                        subComponent.NotchHeight = subDto.NotchHeight;
                        subComponent.IsNotchPlacedOnRight = subDto.IsNotchPlacedOnRight;
                        subComponent.IsNotchPlacedOnLeft = subDto.IsNotchPlacedOnLeft;
                        subComponent.IsNotchPlacedOnBack = subDto.IsNotchPlacedOnBack;
                        subComponent.IsNotchPlacedOnFront = subDto.IsNotchPlacedOnFront;
                        subComponent.NotchStyleType = (PlanogramSubComponentNotchStyleType)subDto.NotchStyleType;
                        subComponent.DividerObstructionHeight = subDto.DividerObstructionHeight;
                        subComponent.DividerObstructionWidth = subDto.DividerObstructionWidth;
                        subComponent.DividerObstructionStartX = subDto.DividerObstructionStartX;
                        subComponent.DividerObstructionSpacingX = subDto.DividerObstructionSpacingX;
                        subComponent.DividerObstructionStartY = subDto.DividerObstructionStartY;
                        subComponent.DividerObstructionSpacingY = subDto.DividerObstructionSpacingY;
                        subComponent.MerchConstraintRow1StartX = subDto.MerchConstraintRow1StartX;
                        subComponent.MerchConstraintRow1SpacingX = subDto.MerchConstraintRow1SpacingX;
                        subComponent.MerchConstraintRow1StartY = subDto.MerchConstraintRow1StartY;
                        subComponent.MerchConstraintRow1SpacingY = subDto.MerchConstraintRow1SpacingY;
                        subComponent.MerchConstraintRow2StartX = subDto.MerchConstraintRow2StartX;
                        subComponent.MerchConstraintRow2SpacingX = subDto.MerchConstraintRow2SpacingX;
                        subComponent.MerchConstraintRow2StartY = subDto.MerchConstraintRow2StartY;
                        subComponent.MerchConstraintRow2SpacingY = subDto.MerchConstraintRow2SpacingY;

                        subComponent.CombineType = PlanogramSubComponentCombineType.None;
                        subComponent.RiserColour = -1;
                        subComponent.RiserTransparencyPercent = 0;
                        subComponent.NotchStartY = 20;
                        subComponent.NotchSpacingY = 10;
                        subComponent.NotchWidth = 10;
                        subComponent.MerchConstraintRow1Height = 4;
                        subComponent.MerchConstraintRow1Width = 4;
                        subComponent.MerchConstraintRow2Height = 4;
                        subComponent.MerchConstraintRow2Width = 4;
                        subComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;
                        subComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;
                        subComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;
                        subComponent.IsProductOverlapAllowed = true;

                        //In GFS the Component.Colour field is used as the fill colour.
                        //Copy to subcomponents.
                        subComponent.FillColourFront = componentDto.Colour;
                        subComponent.FillColourBack = componentDto.Colour;
                        subComponent.FillColourTop = componentDto.Colour;
                        subComponent.FillColourBottom = componentDto.Colour;
                        subComponent.FillColourLeft = componentDto.Colour;
                        subComponent.FillColourRight = componentDto.Colour;

                        //Use the dto SubComponentType to determine the merch settings
                        subComponent.IsProductOverlapAllowed = true;

                        switch (subDto.SubComponentType)
                        {
                            case 0: //backboard
                                subComponent.MerchandisingType = PlanogramSubComponentMerchandisingType.None;
                                component.ComponentType = PlanogramComponentType.Backboard;
                                break;

                            case 1: //shelf
                                subComponent.MerchandisingType = PlanogramSubComponentMerchandisingType.Stack;
                                subComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual; //so positions dont move.
                                subComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Bottom;
                                subComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Front;
                                subComponent.CombineType = PlanogramSubComponentCombineType.None;//PlanogramSubComponentCombineType.Both;
                                component.ComponentType = PlanogramComponentType.Shelf;
                                break;

                            case 4: //chest
                                subComponent.MerchandisingType = PlanogramSubComponentMerchandisingType.Stack;
                                subComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;//so positions dont move.
                                subComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;//so positions dont move.
                                subComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;//so positions dont move.
                                component.ComponentType = PlanogramComponentType.Chest;
                                component.IsMerchandisedTopDown = true;
                                break;

                            case 2: //bar
                                subComponent.MerchandisingType = PlanogramSubComponentMerchandisingType.HangFromBottom;
                                subComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;//so positions dont move.
                                subComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Top;
                                subComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;
                                component.ComponentType = PlanogramComponentType.Bar;
                                break;

                            case 3: //peg
                                subComponent.MerchandisingType = PlanogramSubComponentMerchandisingType.Hang;
                                subComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;//so positions dont move.
                                subComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;//so positions dont move.
                                subComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Back;
                                component.ComponentType = PlanogramComponentType.Peg;
                                break;

                            case 5: //rod
                                subComponent.MerchandisingType = PlanogramSubComponentMerchandisingType.HangFromBottom;
                                subComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;//so positions dont move.
                                subComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Top;
                                subComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.BackStacked;
                                component.ComponentType = PlanogramComponentType.Rod;
                                break;

                            default:
                                subComponent.MerchandisingType = PlanogramSubComponentMerchandisingType.None;
                                subComponent.MerchandisingStrategyX = PlanogramSubComponentXMerchStrategyType.Manual;//so positions dont move.
                                subComponent.MerchandisingStrategyY = PlanogramSubComponentYMerchStrategyType.Manual;//so positions dont move.
                                subComponent.MerchandisingStrategyZ = PlanogramSubComponentZMerchStrategyType.Manual;//so positions dont move.
                                component.ComponentType = PlanogramComponentType.Custom;
                                break;
                        }


                        component.SubComponents.Add(subComponent);
                    }
                }
                #endregion

                plan.Components.Add(component);
            }
            #endregion

            #region Assemblies / AssemblyComponents
            foreach (GFSPlanogramAssemblyDto assemblyDto in _assemblyDtos)
            {
                PlanogramAssembly assembly = PlanogramAssembly.NewPlanogramAssembly();
                assemblyIds.Add(assemblyDto.Id, assembly.Id);
                assembly.Name = assemblyDto.Name;
                assembly.Height = assemblyDto.Height;
                assembly.Width = assemblyDto.Width;
                assembly.Depth = assemblyDto.Depth;
                assembly.TotalComponentCost = assemblyDto.TotalComponentCost;

                foreach (GFSPlanogramAssemblyComponentDto acDto in _assemblyComponentDtos)
                {
                    if (acDto.PlanogramAssemblyId == assemblyDto.Id)
                    {
                        PlanogramAssemblyComponent assemblyComponent = PlanogramAssemblyComponent.NewPlanogramAssemblyComponent();
                        assemblyComponentIds.Add(acDto.Id, assemblyComponent.Id);
                        assemblyComponent.X = acDto.X;
                        assemblyComponent.Y = acDto.Y;
                        assemblyComponent.Z = acDto.Z;
                        assemblyComponent.Slope = acDto.Slope;
                        assemblyComponent.Angle = acDto.Angle;
                        assemblyComponent.Roll = acDto.Roll;

                        //link to component
                        assemblyComponent.PlanogramComponentId = componentIds[acDto.PlanogramComponentId];

                        assembly.Components.Add(assemblyComponent);
                    }
                }

                plan.Assemblies.Add(assembly);
            }
            #endregion

            #region Fixtures
            foreach (GFSPlanogramFixtureDto fixtureDto in _fixtureDtos)
            {
                PlanogramFixture fixture = PlanogramFixture.NewPlanogramFixture();
                fixtureIds.Add(fixtureDto.Id, fixture.Id);
                fixture.Name = fixtureDto.Name;
                fixture.Height = fixtureDto.Height;
                fixture.Width = fixtureDto.Width;
                fixture.Depth = fixtureDto.Depth;

                foreach (GFSPlanogramFixtureAssemblyDto faDto in _fixtureAssemblyDtos)
                {
                    if (faDto.PlanogramFixtureId == fixtureDto.Id)
                    {
                        PlanogramFixtureAssembly fixtureAssembly = PlanogramFixtureAssembly.NewPlanogramFixtureAssembly();
                        fixtureAssemblyIds.Add(faDto.Id, fixtureAssembly.Id);
                        fixtureAssembly.X = faDto.X;
                        fixtureAssembly.Y = faDto.Y;
                        fixtureAssembly.Z = faDto.Z;
                        fixtureAssembly.Slope = faDto.Slope;
                        fixtureAssembly.Angle = faDto.Angle;
                        fixtureAssembly.Roll = faDto.Roll;


                        //link to assembly
                        fixtureAssembly.PlanogramAssemblyId = assemblyIds[faDto.PlanogramAssemblyId];

                        fixture.Assemblies.Add(fixtureAssembly);
                    }
                }

                plan.Fixtures.Add(fixture);
            }
            #endregion

            #region Fixture Items
            foreach (GFSPlanogramFixtureItemDto fixtureItemDto in _fixtureItemDtos)
            {
                PlanogramFixtureItem fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem();
                fixtureItemIds.Add(fixtureItemDto.Id, fixtureItem.Id);
                fixtureItem.X = fixtureItemDto.X;
                fixtureItem.Y = fixtureItemDto.Y;
                fixtureItem.Z = fixtureItemDto.Z;
                fixtureItem.Slope = fixtureItemDto.Slope;
                fixtureItem.Angle = fixtureItemDto.Angle;
                fixtureItem.Roll = fixtureItemDto.Roll;

                //fixture id
                fixtureItem.PlanogramFixtureId = fixtureIds[fixtureItemDto.PlanogramFixtureId];

                plan.FixtureItems.Add(fixtureItem);
            }
            #endregion

            #region Position
            foreach (GFSPlanogramPositionDto posDto in _positionDtos)
            {

                PlanogramPosition pos = PlanogramPosition.NewPlanogramPosition(plan.Positions.GetNextSequence());
                positionIds.Add(posDto.Id, pos.Id);
                plan.Positions.Add(pos);

                //link to parents
                pos.PlanogramFixtureItemId = fixtureItemIds[posDto.PlanogramFixtureItemId];
                pos.PlanogramFixtureAssemblyId = fixtureAssemblyIds[posDto.PlanogramFixtureAssemblyId];
                pos.PlanogramAssemblyComponentId = assemblyComponentIds[posDto.PlanogramAssemblyComponentId];
                pos.PlanogramSubComponentId = subComponentIds[posDto.PlanogramSubComponentId];
                pos.PlanogramProductId = productIds[posDto.PlanogramProductId];
                pos.X = posDto.X;
                pos.Y = posDto.Y;
                pos.Z = posDto.Z;
                pos.UnitsHigh = posDto.UnitsHigh;
                pos.UnitsWide = posDto.UnitsWide;
                pos.UnitsDeep = posDto.UnitsDeep;
                pos.TotalUnits = posDto.TotalUnits;

                //nb - the structure here is now completely different from gfs.

                //Orientation type
                pos.OrientationType = GetOrientationType(posDto.Angle, posDto.Slope, posDto.Roll);
                pos.OrientationTypeX = SuggestRightCapOrientation(pos.OrientationType);
                pos.OrientationTypeY = SuggestTopCapOrientation(pos.OrientationType);
                pos.OrientationTypeZ = pos.OrientationType;

                //0 off rotation as we now use orientation type.
                pos.Angle = 0;
                pos.Slope = 0;
                pos.Roll = 0;

                PlanogramProduct prod = plan.Products.FindById(pos.PlanogramProductId);
                Single prodHeight = prod.SqueezeHeightActual;
                Single prodWidth = prod.SqueezeWidthActual;
                Single prodDepth = prod.SqueezeDepthActual;

                pos.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Default;
                pos.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Default;
                pos.MerchandisingStyleY = PlanogramPositionMerchandisingStyle.Unit;
                pos.MerchandisingStyleZ = PlanogramPositionMerchandisingStyle.Unit;


                Boolean hasFront = (posDto.FrontHigh > 0 && posDto.FrontWide > 0 && posDto.FrontDeep > 0);
                Boolean hasTopCap = (posDto.FrontTopCapHigh > 0 && posDto.FrontTopCapWide > 0 && posDto.FrontTopCapDeep > 0);
                Boolean hasRight = (posDto.FrontRightHigh > 0 && posDto.FrontRightWide > 0 && posDto.FrontRightDeep > 0);
                Boolean hasRightTopCap = (posDto.FrontTopCapRightHigh > 0 && posDto.FrontTopCapRightWide > 0 && posDto.FrontTopCapRightDeep > 0);
                Boolean hasFrontTrays = (posDto.TraysHigh > 0 && posDto.TraysWide > 0 && posDto.TraysDeep > 0);
                Boolean hasRightTrays = (posDto.TraysRightHigh > 0 && posDto.TraysRightWide > 0 && posDto.TraysRightDeep > 0);
                Boolean hasFrontBreakTop = (posDto.TrayBreakTopHigh > 0 && posDto.TrayBreakTopWide > 0 && posDto.TrayBreakTopDeep > 0);
                Boolean hasRightBreakTop = (posDto.TrayBreakTopRightHigh > 0 && posDto.TrayBreakTopRightWide > 0 && posDto.TrayBreakTopRightDeep > 0);
                Boolean hasTrayBreakUp = (posDto.TrayBreakUpHigh > 0 && posDto.TrayBreakUpWide > 0 && posDto.TrayBreakUpDeep > 0);
                Boolean hasTrayBreakDown = (posDto.TrayBreakDownHigh > 0 && posDto.TrayBreakDownWide > 0 && posDto.TrayBreakDownDeep > 0);
                Boolean hasTrayBreakBack = (posDto.TrayBreakBackHigh > 0 && posDto.TrayBreakBackWide > 0 && posDto.TrayBreakBackDeep > 0);


                XYZValue finalProdSize = GetOrientatedSize(pos.OrientationType, prodWidth, prodHeight, prodDepth);
                XYZValue finalTraySize = GetOrientatedSize(pos.OrientationType, prod.TrayWidth, prod.TrayHeight, prod.TrayDepth);

                #region Tray Product
                if (prod.MerchandisingStyle == PlanogramProductMerchandisingStyle.Tray)
                {
                    #region Break Down Only
                    if (hasTrayBreakDown)
                    {
                        pos.MerchandisingStyle = PlanogramPositionMerchandisingStyle.Unit;
                        pos.FacingsHigh = posDto.TrayBreakDownHigh;
                        pos.FacingsWide = posDto.TrayBreakDownWide;
                        pos.FacingsDeep = posDto.TrayBreakDownDeep;

                        if (hasTopCap)
                        {
                            pos.OrientationTypeY = SuggestTopCapOrientation(pos.OrientationType);
                            pos.FacingsYHigh = posDto.FrontTopCapHigh;
                            pos.FacingsYWide = posDto.FrontTopCapWide;
                            pos.FacingsYDeep = posDto.FrontTopCapDeep;
                        }
                    }
                    #endregion

                    #region Front Trays only
                    else if (hasFront && !hasRight)
                    {
                        pos.FacingsHigh = posDto.TraysHigh;
                        pos.FacingsWide = posDto.TraysWide;
                        pos.FacingsDeep = posDto.TraysDeep;

                        if (hasFrontBreakTop)
                        {
                            pos.FacingsYHigh = posDto.TrayBreakTopHigh;
                            pos.FacingsYWide = posDto.TrayBreakTopWide;
                            pos.FacingsYDeep = posDto.TrayBreakTopDeep;
                        }

                        if (hasTrayBreakBack)
                        {
                            pos.FacingsZHigh = posDto.TrayBreakBackHigh;
                            pos.FacingsZWide = posDto.TrayBreakBackWide;
                            pos.FacingsZDeep = posDto.TrayBreakBackDeep;
                        }

                        if (hasTrayBreakUp)
                        {
                            pos.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Unit;
                            pos.FacingsXHigh = posDto.TrayBreakUpHigh;
                            pos.FacingsXWide = posDto.TrayBreakUpWide;
                            pos.FacingsXDeep = posDto.TrayBreakUpDeep;
                        }

                        if (hasTopCap)
                        {
                            if (!hasFrontBreakTop && !hasTrayBreakBack && !hasTrayBreakUp)
                            {
                                //just add
                                pos.OrientationTypeY = SuggestTopCapOrientation(pos.OrientationType);
                                pos.FacingsYHigh = posDto.FrontTopCapHigh;
                                pos.FacingsYWide = posDto.FrontTopCapWide;
                                pos.FacingsYDeep = posDto.FrontTopCapDeep;
                            }
                            else
                            {
                                //split
                                PlanogramPosition topCapPos = pos.Copy();
                                topCapPos.Y = pos.Y + (pos.FacingsHigh * prod.TrayHeight) + (pos.FacingsYHigh * finalProdSize.Y);

                                pos.OrientationType = SuggestTopCapOrientation(pos.OrientationType);
                                pos.FacingsHigh = posDto.FrontTopCapHigh;
                                pos.FacingsWide = posDto.FrontTopCapWide;
                                pos.FacingsDeep = posDto.FrontTopCapDeep;

                                if (hasTrayBreakBack)
                                {
                                    //TODO : how many deep?
                                    pos.OrientationTypeZ = pos.OrientationType;
                                    pos.FacingsZHigh = posDto.FrontTopCapHigh;
                                    pos.FacingsZWide = posDto.TrayBreakBackWide;
                                    pos.FacingsZDeep = 1;
                                }

                                if (hasTrayBreakUp)
                                {
                                    //TODO : how many deep?
                                    pos.OrientationTypeX = pos.OrientationType;
                                    pos.FacingsXHigh = posDto.FrontTopCapHigh;
                                    pos.FacingsXWide = posDto.TrayBreakUpWide;
                                    pos.FacingsXDeep = 1;
                                }

                                //TODO: Set Position

                                plan.Positions.Add(topCapPos);

                            }
                        }
                    }
                    #endregion

                    #region Front & Right
                    else if (hasFront && hasRight)
                    {
                        pos.FacingsHigh = posDto.TraysHigh;
                        pos.FacingsWide = posDto.TraysWide;
                        pos.FacingsDeep = posDto.TraysDeep;

                        if (!hasTopCap && !hasFrontBreakTop && !hasRightBreakTop && !hasTrayBreakUp && !hasTrayBreakBack)
                        {
                            //just front and right

                            pos.OrientationTypeX = SuggestRightCapOrientation(pos.OrientationType);
                            pos.FacingsXHigh = posDto.TraysRightHigh;
                            pos.FacingsXWide = posDto.TraysRightWide;
                            pos.FacingsXDeep = posDto.TraysRightDeep;
                        }
                        else
                        {
                            //split into 2 
                            PlanogramPosition rightTrayPos = pos.Copy();
                            rightTrayPos.X = pos.X + (pos.FacingsWide * finalTraySize.X) + (pos.FacingsXWide * finalProdSize.X);
                            plan.Positions.Add(rightTrayPos);
                            rightTrayPos.OrientationType = SuggestRightCapOrientation(pos.OrientationType);
                            rightTrayPos.FacingsHigh = posDto.TraysRightHigh;
                            rightTrayPos.FacingsWide = posDto.TraysRightWide;
                            rightTrayPos.FacingsDeep = posDto.TraysRightDeep;

                            if (hasFrontBreakTop)
                            {
                                pos.FacingsYHigh = posDto.TrayBreakTopHigh;
                                pos.FacingsYWide = posDto.TrayBreakTopWide;
                                pos.FacingsYDeep = posDto.TrayBreakTopDeep;
                            }
                            if (hasRightBreakTop)
                            {
                                rightTrayPos.FacingsYHigh = posDto.TrayBreakTopRightHigh;
                                rightTrayPos.FacingsYWide = posDto.TrayBreakTopRightWide;
                                rightTrayPos.FacingsYDeep = posDto.TrayBreakTopRightDeep;
                            }

                            if (hasTrayBreakBack)
                            {
                                pos.FacingsZHigh = posDto.TrayBreakBackHigh;
                                pos.FacingsZWide = posDto.TrayBreakBackWide;
                                pos.FacingsZDeep = posDto.TrayBreakBackDeep;

                                rightTrayPos.FacingsZHigh = posDto.TrayBreakBackHigh;
                                rightTrayPos.FacingsZWide = posDto.TrayBreakBackWide;
                                rightTrayPos.FacingsZDeep = posDto.TrayBreakBackDeep;
                            }

                            if (hasTrayBreakUp)
                            {
                                //right only.
                                rightTrayPos.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Unit;
                                rightTrayPos.FacingsXHigh = posDto.TrayBreakUpHigh;
                                rightTrayPos.FacingsXWide = posDto.TrayBreakUpWide;
                                rightTrayPos.FacingsXDeep = posDto.TrayBreakUpDeep;
                            }


                            #region TopCap
                            if (hasTopCap)
                            {
                                #region Front
                                if (!hasFrontBreakTop && !hasTrayBreakBack && !hasTrayBreakUp)
                                {
                                    //just add
                                    pos.OrientationTypeY = SuggestTopCapOrientation(pos.OrientationType);
                                    pos.FacingsYHigh = posDto.FrontTopCapHigh;
                                    pos.FacingsYWide = posDto.FrontTopCapWide;
                                    pos.FacingsYDeep = posDto.FrontTopCapDeep;
                                }
                                else
                                {
                                    //split
                                    PlanogramPosition topCapPos = pos.Copy();
                                    topCapPos.OrientationType = SuggestTopCapOrientation(pos.OrientationType);
                                    topCapPos.FacingsHigh = posDto.FrontTopCapHigh;
                                    topCapPos.FacingsWide = posDto.FrontTopCapWide;
                                    topCapPos.FacingsDeep = posDto.FrontTopCapDeep;

                                    if (hasTrayBreakBack)
                                    {
                                        //TODO : how many deep?
                                        topCapPos.OrientationTypeZ = topCapPos.OrientationType;
                                        topCapPos.FacingsZHigh = posDto.FrontTopCapHigh;
                                        topCapPos.FacingsZWide = posDto.TrayBreakBackWide;
                                        topCapPos.FacingsZDeep = 1;
                                    }

                                    if (hasTrayBreakUp)
                                    {
                                        //TODO : how many deep?
                                        topCapPos.OrientationTypeX = topCapPos.OrientationType;
                                        topCapPos.FacingsXHigh = posDto.FrontTopCapHigh;
                                        topCapPos.FacingsXWide = posDto.TrayBreakUpWide;
                                        topCapPos.FacingsXDeep = 1;
                                    }

                                    topCapPos.Y = pos.Y + (pos.FacingsHigh * prod.TrayHeight) + (pos.FacingsYHigh * finalProdSize.Y);

                                    plan.Positions.Add(topCapPos);

                                }
                                #endregion

                                #region Right
                                if (!hasRightBreakTop && !hasTrayBreakBack && !hasTrayBreakUp)
                                {
                                    //just add
                                    pos.OrientationTypeY = SuggestTopCapOrientation(rightTrayPos.OrientationType);
                                    pos.FacingsYHigh = posDto.FrontTopCapHigh;
                                    pos.FacingsYWide = posDto.FrontTopCapWide;
                                    pos.FacingsYDeep = posDto.FrontTopCapDeep;
                                }
                                else
                                {
                                    //split
                                    PlanogramPosition topCapPos = pos.Copy();

                                    topCapPos.OrientationType = SuggestTopCapOrientation(rightTrayPos.OrientationType);
                                    topCapPos.FacingsHigh = posDto.FrontTopCapHigh;
                                    topCapPos.FacingsWide = posDto.FrontTopCapWide;
                                    topCapPos.FacingsDeep = posDto.FrontTopCapDeep;

                                    if (hasTrayBreakBack)
                                    {
                                        //TODO : how many deep?
                                        topCapPos.OrientationTypeZ = topCapPos.OrientationType;
                                        topCapPos.FacingsZHigh = posDto.FrontTopCapHigh;
                                        topCapPos.FacingsZWide = posDto.TrayBreakBackWide;
                                        topCapPos.FacingsZDeep = 1;
                                    }

                                    if (hasTrayBreakUp)
                                    {
                                        //TODO : how many deep?
                                        topCapPos.OrientationTypeX = topCapPos.OrientationType;
                                        topCapPos.FacingsXHigh = posDto.FrontTopCapHigh;
                                        topCapPos.FacingsXWide = posDto.TrayBreakUpWide;
                                        topCapPos.FacingsXDeep = 1;
                                    }

                                    //TODO: Set Position

                                    plan.Positions.Add(topCapPos);

                                }
                                #endregion
                            }
                            #endregion

                        }


                    }
                    #endregion

                    #region Right Trays Only
                    else if (hasRight)
                    {
                        pos.OrientationType = PlanogramPositionOrientationType.Right0;
                        pos.FacingsHigh = posDto.TraysRightHigh;
                        pos.FacingsWide = posDto.TraysRightWide;
                        pos.FacingsDeep = posDto.TraysRightDeep;

                        if (hasRightBreakTop)
                        {
                            pos.FacingsYHigh = posDto.TrayBreakTopRightHigh;
                            pos.FacingsYWide = posDto.TrayBreakTopRightWide;
                            pos.FacingsYDeep = posDto.TrayBreakTopRightDeep;
                        }

                        if (hasTrayBreakBack)
                        {
                            pos.FacingsZHigh = posDto.TrayBreakBackHigh;
                            pos.FacingsZWide = posDto.TrayBreakBackWide;
                            pos.FacingsZDeep = posDto.TrayBreakBackDeep;
                        }

                        if (hasTrayBreakUp)
                        {
                            pos.MerchandisingStyleX = PlanogramPositionMerchandisingStyle.Unit;
                            pos.FacingsXHigh = posDto.TrayBreakUpHigh;
                            pos.FacingsXWide = posDto.TrayBreakUpWide;
                            pos.FacingsXDeep = posDto.TrayBreakUpDeep;
                        }


                        if (hasTopCap)
                        {
                            if (!hasRightBreakTop && !hasTrayBreakBack && !hasTrayBreakUp)
                            {
                                //just add
                                pos.OrientationTypeY = PlanogramPositionOrientationType.Top0;
                                pos.FacingsYHigh = posDto.FrontTopCapHigh;
                                pos.FacingsYWide = posDto.FrontTopCapWide;
                                pos.FacingsYDeep = posDto.FrontTopCapDeep;
                            }
                            else
                            {
                                //split
                                PlanogramPosition topCapPos = pos.Copy();

                                pos.OrientationType = PlanogramPositionOrientationType.Top0;
                                pos.FacingsHigh = posDto.FrontTopCapHigh;
                                pos.FacingsWide = posDto.FrontTopCapWide;
                                pos.FacingsDeep = posDto.FrontTopCapDeep;

                                if (hasTrayBreakBack)
                                {
                                    //TODO : how many deep?
                                    pos.OrientationTypeZ = PlanogramPositionOrientationType.Top0;
                                    pos.FacingsZHigh = posDto.FrontTopCapHigh;
                                    pos.FacingsZWide = posDto.TrayBreakBackWide;
                                    pos.FacingsZDeep = 1;
                                }

                                if (hasTrayBreakUp)
                                {
                                    //TODO : how many deep?
                                    pos.OrientationTypeX = PlanogramPositionOrientationType.Top0;
                                    pos.FacingsXHigh = posDto.FrontTopCapHigh;
                                    pos.FacingsXWide = posDto.TrayBreakUpWide;
                                    pos.FacingsXDeep = 1;
                                }

                                //TODO: Set Position

                                plan.Positions.Add(topCapPos);

                            }
                        }

                    }
                    #endregion

                }
                #endregion

                #region Non-Tray
                else
                {
                    //Front - yes, Right - no, topcap - optional.
                    if (hasFront && !hasRight)
                    {
                        pos.FacingsHigh = posDto.FrontHigh;
                        pos.FacingsWide = posDto.FrontWide;
                        pos.FacingsDeep = posDto.FrontDeep;

                        if (hasTopCap)
                        {
                            pos.OrientationTypeY = SuggestTopCapOrientation(pos.OrientationType);
                            pos.FacingsYHigh = posDto.FrontTopCapHigh;
                            pos.FacingsYWide = posDto.FrontTopCapWide;
                            pos.FacingsYDeep = posDto.FrontTopCapDeep;
                        }
                    }

                    //Front with right cap but no right top cap, topcap optional.
                    else if (hasFront && hasRight && !hasRightTopCap)
                    {
                        pos.FacingsHigh = posDto.FrontHigh;
                        pos.FacingsWide = posDto.FrontWide;
                        pos.FacingsDeep = posDto.FrontDeep;

                        pos.OrientationTypeX = SuggestRightCapOrientation(pos.OrientationType);
                        pos.FacingsXHigh = posDto.FrontRightHigh;
                        pos.FacingsXWide = posDto.FrontRightWide;
                        pos.FacingsXDeep = posDto.FrontRightDeep;

                        if (hasTopCap)
                        {
                            pos.OrientationTypeY = SuggestTopCapOrientation(pos.OrientationType);
                            pos.FacingsYHigh = posDto.FrontTopCapHigh;
                            pos.FacingsYWide = posDto.FrontTopCapWide;
                            pos.FacingsYDeep = posDto.FrontTopCapDeep;
                        }
                    }

                    //Front with right cap and right top cap.
                    else if (hasFront && hasRight && hasRightTopCap)
                    {
                        //This must now be split into 2 positions!

                        //pos 1 - front
                        pos.FacingsHigh = posDto.FrontHigh;
                        pos.FacingsWide = posDto.FrontWide;
                        pos.FacingsDeep = posDto.FrontDeep;
                        if (hasTopCap)
                        {
                            pos.OrientationTypeY = SuggestTopCapOrientation(pos.OrientationType);
                            pos.FacingsYHigh = posDto.FrontTopCapHigh;
                            pos.FacingsYWide = posDto.FrontTopCapWide;
                            pos.FacingsYDeep = posDto.FrontTopCapDeep;
                        }

                        //pos 2 - right
                        PlanogramPosition pos2 = pos.Copy();
                        pos2.OrientationType = SuggestRightCapOrientation(pos2.OrientationType);
                        pos2.FacingsHigh = posDto.FrontRightHigh;
                        pos2.FacingsWide = posDto.FrontRightWide;
                        pos2.FacingsDeep = posDto.FrontRightDeep;
                        pos2.OrientationTypeY = SuggestTopCapOrientation(pos2.OrientationType);
                        pos2.FacingsYHigh = posDto.FrontTopCapRightHigh;
                        pos2.FacingsYWide = posDto.FrontTopCapRightWide;
                        pos2.FacingsYDeep = posDto.FrontTopCapRightDeep;

                        pos2.X = pos.X + (pos.FacingsWide * finalProdSize.X);

                        plan.Positions.Add(pos2);
                    }

                }
                #endregion

            }
            #endregion

            #region  Annotation
            foreach (GFSPlanogramAnnotationDto annoDto in _annotationDtos)
            {
                PlanogramAnnotation annotation = PlanogramAnnotation.NewPlanogramAnnotation();
                if (annoDto.PlanogramFixtureItemId.HasValue) annotation.PlanogramFixtureItemId = fixtureItemIds[annoDto.PlanogramFixtureItemId.Value];
                if (annoDto.PlanogramFixtureAssemblyId.HasValue) annotation.PlanogramFixtureAssemblyId = fixtureAssemblyIds[annoDto.PlanogramFixtureAssemblyId.Value];
                if (annoDto.PlanogramAssemblyComponentId.HasValue) annotation.PlanogramAssemblyComponentId = assemblyComponentIds[annoDto.PlanogramAssemblyComponentId.Value];
                if (annoDto.PlanogramSubComponentId.HasValue) annotation.PlanogramSubComponentId = subComponentIds[annoDto.PlanogramSubComponentId.Value];
                if (annoDto.PlanogramPositionId.HasValue) annotation.PlanogramPositionId = positionIds[annoDto.PlanogramPositionId.Value];

                annotation.AnnotationType = (PlanogramAnnotationType)annoDto.AnnotationType;
                annotation.Text = annoDto.Text;
                annotation.X = annoDto.X;
                annotation.Y = annoDto.Y;
                annotation.Z = annoDto.Z;
                annotation.Height = annoDto.Height;
                annotation.Width = annoDto.Width;
                annotation.Depth = annoDto.Depth;

                annotation.BackgroundColour = -1;
                annotation.BorderColour = -16777216;
                annotation.BorderThickness = 0.5F;
                annotation.FontColour = -16777216;
                annotation.FontName = "Arial";
                annotation.FontSize = 8;
                annotation.CanReduceFontToFit = true;

                plan.Annotations.Add(annotation);
            }



            #endregion


            #region Changing 1 component Assemblies to FixtureComponents.
            //other changes
            //GFS has no concept of fixture components
            // to upgrade any single component assemblies need changing.
            List<PlanogramAssembly> assembliesToRemove = new List<PlanogramAssembly>();
            foreach (PlanogramFixture fixture in plan.Fixtures)
            {
                foreach (PlanogramFixtureAssembly fixtureAssembly in fixture.Assemblies.ToList())
                {
                    PlanogramAssembly assembly = plan.Assemblies.FindById(fixtureAssembly.PlanogramAssemblyId);
                    if (assembly != null)
                    {
                        if (assembly.Components.Count == 1)
                        {
                            if (!assembliesToRemove.Contains(assembly))
                            {
                                assembliesToRemove.Add(assembly);
                            }

                            //create a new FixtureComponent
                            PlanogramFixtureComponent fixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent();
                            fixtureComponent.PlanogramComponentId = assembly.Components[0].PlanogramComponentId;

                            fixtureComponent.X = fixtureAssembly.X;
                            fixtureComponent.Y = fixtureAssembly.Y;

                            //the assembly z in gfs is out by the depth due
                            // to a change in origin point.
                            fixtureComponent.Z = fixtureAssembly.Z - assembly.Depth;

                            //rotation in gfs is anti-clockwise
                            fixtureComponent.Angle = -fixtureAssembly.Angle;
                            fixtureComponent.Roll = -fixtureAssembly.Roll;
                            fixtureComponent.Slope = -fixtureAssembly.Slope;

                            fixture.Components.Add(fixtureComponent);
                            fixture.Assemblies.Remove(fixtureAssembly);

                            //reset ids
                            foreach (PlanogramPosition pos in plan.Positions)
                            {
                                if (Object.Equals(pos.PlanogramFixtureAssemblyId, fixtureAssembly.Id))
                                {
                                    pos.PlanogramFixtureAssemblyId = null;
                                    pos.PlanogramAssemblyComponentId = null;
                                    pos.PlanogramFixtureComponentId = fixtureComponent.Id;
                                }
                            }
                            foreach (PlanogramAnnotation anno in plan.Annotations)
                            {
                                if (Object.Equals(anno.PlanogramFixtureAssemblyId, fixtureAssembly.Id))
                                {
                                    anno.PlanogramFixtureAssemblyId = null;
                                    anno.PlanogramAssemblyComponentId = null;
                                    anno.PlanogramFixtureComponentId = fixtureComponent.Id;
                                }
                            }


                        }

                    }

                }
            }

            plan.Assemblies.RemoveList(assembliesToRemove);
            #endregion

            #region Identifying Backboards

            //In gfs backboards have been given a subcomponent type of shelf
            // need to make sure these are identified correctly for ccm
            foreach (PlanogramFixture fixture in plan.Fixtures)
            {
                foreach (PlanogramFixtureComponent fc in fixture.Components)
                {
                    if (fc.X == 0 && fc.Y == 0)
                    {
                        PlanogramComponent component = plan.Components.FindById(fc.PlanogramComponentId);
                        if (component.ComponentType == PlanogramComponentType.Backboard)
                        {
                            break; //don't search other components in this fixture.
                        }
                        else if (component.ComponentType == PlanogramComponentType.Shelf && component.SubComponents.Count == 1)
                        {
                            PlanogramSubComponent sub = component.SubComponents[0];

                            // if height and width match fixture (depth can be ignored)
                            if (sub.Height == fixture.Height && sub.Width == fixture.Width)
                            {
                                Int32 posCount = plan.Positions.Count(p => Object.Equals(p.PlanogramSubComponentId, sub.Id));
                                if (posCount == 0)
                                {
                                    //mark as a backboard
                                    sub.MerchandisingType = PlanogramSubComponentMerchandisingType.None;
                                    sub.Depth = 1;

                                    component.ComponentType = PlanogramComponentType.Backboard;
                                    component.Depth = 1;
                                    fc.Z = -1;

                                    break; //don't search other components in this fixture.
                                }
                            }
                        }
                    }

                }
            }

            #endregion

            #region Correct sloping shelves
            //Originally in GFS the rotation origin was calculated as the bottom
            // front corner of the box but in CCMv8 this is now the bottom back.

            //This means that we now need to correct the y position of sloping shelves.
            foreach (PlanogramFixture fixture in plan.Fixtures)
            {
                foreach (PlanogramFixtureComponent fixtureComp in fixture.Components)
                {
                    if (fixtureComp.Slope != 0)
                    {
                        PlanogramComponent comp = plan.Components.FindById(fixtureComp.PlanogramComponentId);
                        if (comp != null && comp.ComponentType == PlanogramComponentType.Shelf)
                        {
                            //using the shelf depth and angle calculate how
                            // much we need to shift the shelf up by.
                            Single angle = fixtureComp.Slope;
                            Single cSquared = comp.Depth;

                            Double sideA = cSquared * Math.Sin(-(Double)angle);

                            fixtureComp.Y = System.Convert.ToSingle(Math.Round(fixtureComp.Y + sideA, 5));

                            //reset the z to 0.
                            if (fixtureComp.Z < 0)
                            {
                                fixtureComp.Z = 0;
                            }
                        }
                    }
                }
            }
            #endregion

            #region Correct Chests to a single sub
            //in GFS, chests were created with more than one subcomponent.
            // Need to go through and convert these to a single sub for v8 and
            // make sure that the correct flags are set.
            foreach (PlanogramComponent comp in plan.Components)
            {
                if (comp.ComponentType == PlanogramComponentType.Chest
                    && comp.SubComponents.Count > 1)
                {
                    List<Object> curSubIds = comp.SubComponents.Select(s => s.Id).ToList();

                    PlanogramSubComponent newSub = PlanogramSubComponent.NewPlanogramSubComponent(
                        comp.SubComponents[0].Name, PlanogramComponentType.Chest, comp.Width, comp.Height, comp.Depth, _entity.SystemSettings.ChestFillColour);

                    Single chestThick = comp.SubComponents.First(s => s.Depth != comp.Depth).Width;
                    newSub.FaceThicknessFront = chestThick;
                    newSub.FaceThicknessBack = chestThick;
                    newSub.FaceThicknessTop = 0; //chests have no top
                    newSub.FaceThicknessBottom = chestThick;
                    newSub.FaceThicknessLeft = chestThick;
                    newSub.FaceThicknessRight = chestThick;

                    comp.SubComponents.Add(newSub);

                    //update any positions and annotations associated with the other subs of this component
                    foreach (PlanogramPosition pos in this.Plan.Positions)
                    {
                        if (curSubIds.Contains(pos.PlanogramSubComponentId)) pos.PlanogramSubComponentId = newSub.Id;
                    }
                    foreach (PlanogramAnnotation anno in this.Plan.Annotations)
                    {
                        if (curSubIds.Contains(anno.PlanogramSubComponentId)) anno.PlanogramSubComponentId = newSub.Id;
                    }

                    //remove the old subs
                    foreach (PlanogramSubComponent sub in comp.SubComponents.ToList())
                    {
                        if (sub != newSub) comp.SubComponents.Remove(sub);
                    }
                }
            }
            #endregion

            #region Sequence positions

            //Populate the new sequence field for each subcomponent's positions.
            foreach (PlanogramComponent component in plan.Components)
            {
                foreach (PlanogramSubComponent sub in component.SubComponents)
                {
                    List<PlanogramPosition> positions =
                        plan.Positions.Where(p => Object.Equals(p.PlanogramSubComponentId, sub.Id)).ToList();
                    if (positions.Count > 0)
                    {
                        Int16 seq = 1;
                        foreach (var group in positions.OrderBy(p => p.X).GroupBy(p => p.X))
                        {
                            foreach (PlanogramPosition pos in group)
                            {
                                pos.SequenceX = seq;
                            }
                            seq++;
                        }

                        seq = 1;
                        foreach (var group in positions.OrderBy(p => p.Y).GroupBy(p => p.Y))
                        {
                            foreach (PlanogramPosition pos in group)
                            {
                                pos.SequenceY = seq;
                            }
                            seq++;
                        }

                        seq = 1;
                        foreach (var group in positions.OrderBy(p => p.Z).GroupBy(p => p.Z))
                        {
                            foreach (PlanogramPosition pos in group)
                            {
                                pos.SequenceZ = seq;
                            }
                            seq++;
                        }



                        //set the main seq number
                        Boolean isFarX = sub.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.Left || sub.MerchandisingStrategyX == PlanogramSubComponentXMerchStrategyType.Right;
                        Boolean isFarY = sub.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.Top || sub.MerchandisingStrategyY == PlanogramSubComponentYMerchStrategyType.Bottom;
                        Boolean isFarZ = sub.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.Front || sub.MerchandisingStrategyZ == PlanogramSubComponentZMerchStrategyType.Back;


                        IOrderedEnumerable<PlanogramPosition> orderedPositions;
                        if (isFarX)
                        {
                            orderedPositions = positions.OrderBy(p => positions.IndexOf(p));
                        }
                        else
                        {
                            orderedPositions = positions.OrderBy(p => p.SequenceX);
                        }

                        if (isFarY)
                        {
                            orderedPositions = orderedPositions.ThenBy(p => positions.IndexOf(p));
                        }
                        else
                        {
                            orderedPositions = orderedPositions.ThenBy(p => p.SequenceY);
                        }

                        if (isFarZ)
                        {
                            orderedPositions = orderedPositions.ThenBy(p => positions.IndexOf(p));
                        }
                        else
                        {
                            orderedPositions = orderedPositions.ThenBy(p => p.SequenceZ);
                        }

                        seq = 1;
                        foreach (PlanogramPosition pos in orderedPositions.ToList())
                        {
                            pos.Sequence = seq;
                            seq++;
                        };

                    }

                }
            }
            #endregion

            return plan;
        }

        #endregion

        #region Static Helpers

        /// <summary>
        /// Gets the suggested right cap orientation for the given main 
        /// </summary>
        /// <param name="mainOrientation"></param>
        /// <returns></returns>
        private static PlanogramPositionOrientationType SuggestRightCapOrientation(PlanogramPositionOrientationType mainOrientation)
        {

            switch (mainOrientation)
            {
                case PlanogramPositionOrientationType.Front0: return PlanogramPositionOrientationType.Right0;
                case PlanogramPositionOrientationType.Front90: return PlanogramPositionOrientationType.Top90;
                case PlanogramPositionOrientationType.Front180: return PlanogramPositionOrientationType.Right180;
                case PlanogramPositionOrientationType.Front270: return PlanogramPositionOrientationType.Bottom270;
                case PlanogramPositionOrientationType.Top0: return PlanogramPositionOrientationType.Right270;
                case PlanogramPositionOrientationType.Top90: return PlanogramPositionOrientationType.Back270;
                case PlanogramPositionOrientationType.Top180: return PlanogramPositionOrientationType.Left270;
                case PlanogramPositionOrientationType.Top270: return PlanogramPositionOrientationType.Front270;
                case PlanogramPositionOrientationType.Right0: return PlanogramPositionOrientationType.Back0;
                case PlanogramPositionOrientationType.Right90: return PlanogramPositionOrientationType.Top180;
                case PlanogramPositionOrientationType.Right180: return PlanogramPositionOrientationType.Front180;
                case PlanogramPositionOrientationType.Right270: return PlanogramPositionOrientationType.Back180;
                case PlanogramPositionOrientationType.Back0: return PlanogramPositionOrientationType.Left0;
                case PlanogramPositionOrientationType.Back90: return PlanogramPositionOrientationType.Top270;
                case PlanogramPositionOrientationType.Back180: return PlanogramPositionOrientationType.Left180;
                case PlanogramPositionOrientationType.Back270: return PlanogramPositionOrientationType.Bottom90;
                case PlanogramPositionOrientationType.Bottom0: return PlanogramPositionOrientationType.Right90;
                case PlanogramPositionOrientationType.Bottom90: return PlanogramPositionOrientationType.Front90;
                case PlanogramPositionOrientationType.Bottom180: return PlanogramPositionOrientationType.Right270;
                case PlanogramPositionOrientationType.Bottom270: return PlanogramPositionOrientationType.Back90;
                case PlanogramPositionOrientationType.Left0: return PlanogramPositionOrientationType.Front0;
                case PlanogramPositionOrientationType.Left90: return PlanogramPositionOrientationType.Top0;
                case PlanogramPositionOrientationType.Left180: return PlanogramPositionOrientationType.Front180;
                case PlanogramPositionOrientationType.Left270: return PlanogramPositionOrientationType.Bottom0;

                default:
                    return PlanogramPositionOrientationType.Right0;

            }

        }

        /// <summary>
        /// Gets the suggested top cap orientation for the given main.
        /// </summary>
        /// <param name="mainOrientation"></param>
        /// <returns></returns>
        private static PlanogramPositionOrientationType SuggestTopCapOrientation(PlanogramPositionOrientationType mainOrientation)
        {
            switch (mainOrientation)
            {
                case PlanogramPositionOrientationType.Front0: return PlanogramPositionOrientationType.Top0;
                case PlanogramPositionOrientationType.Front90: return PlanogramPositionOrientationType.Left90;
                case PlanogramPositionOrientationType.Front180: return PlanogramPositionOrientationType.Top180;
                case PlanogramPositionOrientationType.Front270: return PlanogramPositionOrientationType.Right270;
                case PlanogramPositionOrientationType.Top0: return PlanogramPositionOrientationType.Front0;
                case PlanogramPositionOrientationType.Top90: return PlanogramPositionOrientationType.Left180;
                case PlanogramPositionOrientationType.Top180: return PlanogramPositionOrientationType.Front180;
                case PlanogramPositionOrientationType.Top270: return PlanogramPositionOrientationType.Right180;
                case PlanogramPositionOrientationType.Right0: return PlanogramPositionOrientationType.Top90;
                case PlanogramPositionOrientationType.Right90: return PlanogramPositionOrientationType.Front90;
                case PlanogramPositionOrientationType.Right180: return PlanogramPositionOrientationType.Bottom90;
                case PlanogramPositionOrientationType.Right270: return PlanogramPositionOrientationType.Front270;
                case PlanogramPositionOrientationType.Back0: return PlanogramPositionOrientationType.Bottom180;
                case PlanogramPositionOrientationType.Back90: return PlanogramPositionOrientationType.Right90;
                case PlanogramPositionOrientationType.Back180: return PlanogramPositionOrientationType.Bottom0;
                case PlanogramPositionOrientationType.Back270: return PlanogramPositionOrientationType.Right270;
                case PlanogramPositionOrientationType.Bottom0: return PlanogramPositionOrientationType.Front0;
                case PlanogramPositionOrientationType.Bottom90: return PlanogramPositionOrientationType.Left0;
                case PlanogramPositionOrientationType.Bottom180: return PlanogramPositionOrientationType.Back0;
                case PlanogramPositionOrientationType.Bottom270: return PlanogramPositionOrientationType.Right0;
                case PlanogramPositionOrientationType.Left0: return PlanogramPositionOrientationType.Top270;
                case PlanogramPositionOrientationType.Left90: return PlanogramPositionOrientationType.Back90;
                case PlanogramPositionOrientationType.Left180: return PlanogramPositionOrientationType.Bottom270;
                case PlanogramPositionOrientationType.Left270: return PlanogramPositionOrientationType.Front270;


                default:

                    return PlanogramPositionOrientationType.Top0;

            }
        }

        /// <summary>
        /// Returns the item size orientated to the given orientation type.
        /// </summary>
        private static XYZValue GetOrientatedSize(PlanogramPositionOrientationType orientationType,
            Single width, Single height, Single depth)
        {
            Single prodHeight = height;
            Single prodWidth = width;
            Single prodDepth = depth;

            switch (orientationType)
            {
                default:
                case PlanogramPositionOrientationType.Front0:
                case PlanogramPositionOrientationType.Back0:
                case PlanogramPositionOrientationType.Front180:
                case PlanogramPositionOrientationType.Back180:
                    return new XYZValue(prodWidth, prodHeight, prodDepth);

                case PlanogramPositionOrientationType.Front90:
                case PlanogramPositionOrientationType.Back90:
                case PlanogramPositionOrientationType.Front270:
                case PlanogramPositionOrientationType.Back270:
                    return new XYZValue(prodHeight, prodWidth, prodDepth);


                case PlanogramPositionOrientationType.Right0:
                case PlanogramPositionOrientationType.Left0:
                case PlanogramPositionOrientationType.Right180:
                case PlanogramPositionOrientationType.Left180:
                    return new XYZValue(prodDepth, prodHeight, prodWidth);


                case PlanogramPositionOrientationType.Right90:
                case PlanogramPositionOrientationType.Left90:
                case PlanogramPositionOrientationType.Right270:
                case PlanogramPositionOrientationType.Left270:
                    return new XYZValue(prodHeight, prodDepth, prodWidth);

                case PlanogramPositionOrientationType.Top0:
                case PlanogramPositionOrientationType.Bottom0:
                case PlanogramPositionOrientationType.Top180:
                case PlanogramPositionOrientationType.Bottom180:
                    return new XYZValue(prodWidth, prodDepth, prodHeight);


                case PlanogramPositionOrientationType.Top90:
                case PlanogramPositionOrientationType.Bottom90:
                case PlanogramPositionOrientationType.Top270:
                case PlanogramPositionOrientationType.Bottom270:
                    return new XYZValue(prodDepth, prodWidth, prodHeight);

            }
        }

        /// <summary>
        /// Simple structure holding XYZ values
        /// </summary>
        private struct XYZValue
        {
            private Single _x;
            private Single _y;
            private Single _z;

            public Single X
            {
                get { return _x; }
                set { _x = value; }
            }

            public Single Y
            {
                get { return _y; }
                set { _y = value; }
            }

            public Single Z
            {
                get { return _z; }
                set { _z = value; }
            }

            public XYZValue(Single x, Single y, Single z)
            {
                _x = x;
                _y = y;
                _z = z;
            }
        }

        private static PlanogramPositionOrientationType GetOrientationType(Single angle, Single slope, Single roll)
        {
            //OLD GFS VERS!

            Boolean isFront90 = (roll > Math.PI / 4 && roll <= 3 * Math.PI / 4) && angle == 0 && slope == 0;
            if (isFront90)
            {
                return PlanogramPositionOrientationType.Front90;
            }


            Boolean isTop0 = (slope > Math.PI / 4 && slope <= 3 * Math.PI / 4) && angle == 0 && roll == 0;
            if (isTop0)
            {
                return PlanogramPositionOrientationType.Top0;
            }


            Boolean isTop90 = (roll > Math.PI / 4 && roll <= 3 * Math.PI / 4) &&
                                (angle > Math.PI / 4 && angle <= 3 * Math.PI / 4) && slope == 0;
            if (isTop90)
            {
                return PlanogramPositionOrientationType.Top90;
            }

            Boolean isRight0 = (angle > Math.PI / 4 && angle <= 3 * Math.PI / 4) && slope == 0 && roll == 0;
            if (isRight0)
            {
                return PlanogramPositionOrientationType.Right0;
            }

            Boolean isRight90 = (angle > Math.PI / 4 && angle <= 3 * Math.PI / 4) &&
                                (slope > Math.PI / 4 && slope <= 3 * Math.PI / 4) && roll == 0;
            if (isRight90)
            {
                return PlanogramPositionOrientationType.Right90;
            }


            return PlanogramPositionOrientationType.Front0;
        }

        #endregion
    }
}
