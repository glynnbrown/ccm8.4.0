﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32524 : L.Ineson
//  Created
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Helpers
{
    /// <summary>
    /// Provides helper methods for dealing with FixturePackages.
    /// </summary>
    public static class FixturePackageHelper
    {
        public class AddToPlanogramResult
        {
            public List<PlanogramFixtureItem> AddedFixtureItems { get; set; }
            public PlanogramFixtureAssembly AddedFixtureAssembly { get; set; }
            public PlanogramFixtureComponent AddedFixtureComponent { get; set; }
        }

        public static AddToPlanogramResult AddToPlanogram(FixturePackage sourcePackage, Planogram plan, PlanogramFixtureItem nearestFixtureModel)
        {
            //copy the fixture package to force it to add rather than update.
            FixturePackage package = sourcePackage.Copy();

            AddToPlanogramResult result = new AddToPlanogramResult();

            if (package.FixtureItems.Count > 0)
            {
                result.AddedFixtureItems = FixturePackageHelper.AddToPlanogram(package.FixtureItems, plan, nearestFixtureModel);
            }
            else if (package.Fixtures.Count > 0)
            {
                PlanogramFixtureItem addedFixtureItem = FixturePackageHelper.AddToPlanogram(package.Fixtures[0], plan, nearestFixtureModel);
                if (addedFixtureItem != null) result.AddedFixtureItems = new List<PlanogramFixtureItem> { addedFixtureItem };
            }
            else if (package.Assemblies.Count > 0)
            {
                result.AddedFixtureAssembly = FixturePackageHelper.AddToPlanogram(package.Assemblies[0], plan, nearestFixtureModel);
            }
            else if (package.Components.Count > 0)
            {
                result.AddedFixtureComponent= FixturePackageHelper.AddToPlanogram(package.Components[0], plan, nearestFixtureModel);
            }

            return result;
        }


        /// <summary>
        /// Adds the given fixture component to the planogram
        /// </summary>
        /// <param name="fixtureComponent"></param>
        /// <param name="plan"></param>
        /// <param name="nearestFixtureItem"></param>
        /// <returns></returns>
        private static PlanogramFixtureComponent AddToPlanogram(FixtureComponent fixtureComponent, Planogram plan, PlanogramFixtureItem nearestFixtureItem)
        {
            FixturePackage package = fixtureComponent.Parent;

            //get the fixture to add to.
            PlanogramFixtureItem addToFixtureItem = nearestFixtureItem;
            PlanogramFixture addToFixture = (nearestFixtureItem != null) ? plan.Fixtures.FindById(nearestFixtureItem.PlanogramFixtureId) : null;
            if (addToFixture == null)
            {
                if (plan.FixtureItems.Count == 0)
                {
                    addToFixtureItem = plan.FixtureItems.Add();
                    addToFixture = plan.Fixtures.FindById(addToFixtureItem.PlanogramFixtureId);
                }
                else
                {
                    addToFixtureItem = plan.FixtureItems.Last();
                    addToFixture = plan.Fixtures.FindById(addToFixtureItem.PlanogramFixtureId);
                }
            }

            //copy the component
            PlanogramComponent planComponent = fixtureComponent.AddOrUpdatePlanogramComponent(plan);


            //hook the new component to the fixture
            PlanogramFixtureComponent planFixtureComponent = PlanogramFixtureComponent.NewPlanogramFixtureComponent();
            planFixtureComponent.PlanogramComponentId = planComponent.Id;
            addToFixture.Components.Add(planFixtureComponent);

            //place above center fixture 
            planFixtureComponent.X = (addToFixture.Width / 2) - (planComponent.Width / 2);
            planFixtureComponent.Y = addToFixtureItem.Y + addToFixture.Height + 10;

            //add linked annotations
            foreach (FixtureAnnotation annotation in package.Annotations)
            {
                PlanogramAnnotation planAnno = annotation.NewPlanogramAnnotation();
                planAnno.PlanogramFixtureItemId = addToFixtureItem.Id;
                planAnno.PlanogramFixtureComponentId = planFixtureComponent.Id;
                plan.Annotations.Add(planAnno);
            }

            return planFixtureComponent;
        }

        /// <summary>
        /// Adds the given fixture assembly to the planogram
        /// </summary>
        /// <param name="fixtureAssembly"></param>
        /// <param name="plan"></param>
        /// <param name="nearestFixtureItem"></param>
        /// <returns></returns>
        private static PlanogramFixtureAssembly AddToPlanogram(FixtureAssembly fixtureAssembly, Planogram plan, PlanogramFixtureItem nearestFixtureItem)
        {
            FixturePackage package = fixtureAssembly.Parent;

            //get the fixture to add to.
            PlanogramFixtureItem addToFixtureItem = nearestFixtureItem;
            PlanogramFixture addToFixture = (nearestFixtureItem != null) ? plan.Fixtures.FindById(nearestFixtureItem.PlanogramFixtureId) : null;
            if (addToFixture == null)
            {
                if (!plan.FixtureItems.Any())
                {
                    addToFixtureItem = plan.FixtureItems.Add();
                    addToFixture = plan.Fixtures.FindById(addToFixtureItem.PlanogramFixtureId);
                }
                else
                {
                    addToFixtureItem = plan.FixtureItems.Last();
                    addToFixture = plan.Fixtures.FindById(addToFixtureItem.PlanogramFixtureId);
                }
            }

            //copy the assembly
            PlanogramAssembly planAssembly = fixtureAssembly.AddOrUpdatePlanogramAssembly(plan);

            //hook the assembly to the fixture
            PlanogramFixtureAssembly planFixtureAssembly = PlanogramFixtureAssembly.NewPlanogramFixtureAssembly();
            planFixtureAssembly.PlanogramAssemblyId = planAssembly.Id;
            addToFixture.Assemblies.Add(planFixtureAssembly);

            //place above center fixture 
            planFixtureAssembly.X = (addToFixture.Width / 2) - (planAssembly.Width / 2);
            planFixtureAssembly.Y = addToFixtureItem.Y + addToFixture.Height + 10;


            //add linked annotations
            foreach (FixtureAnnotation annotation in package.Annotations)
            {
                PlanogramAnnotation planAnno = annotation.NewPlanogramAnnotation();
                planAnno.PlanogramFixtureItemId = addToFixtureItem.Id;
                planAnno.PlanogramFixtureAssemblyId = planFixtureAssembly.Id;
                plan.Annotations.Add(planAnno);
            }

            return planFixtureAssembly;
        }

        /// <summary>
        /// Adds the given fixture to the planogram
        /// </summary>
        /// <param name="fixture"></param>
        /// <param name="plan"></param>
        /// <param name="nearestFixtureItem"></param>
        /// <returns></returns>
        private static PlanogramFixtureItem AddToPlanogram(Fixture fixture, Planogram plan, PlanogramFixtureItem nearestFixtureItem)
        {
            FixturePackage package = fixture.Parent;

            //get the fixture to add to.
            PlanogramFixtureItem addToFixture = nearestFixtureItem;
            if (addToFixture == null && plan.FixtureItems.Count > 0)
            {
                addToFixture = plan.FixtureItems.Last();
            }

            //copy the fixture
            PlanogramFixture planFixture = fixture.AddOrUpdatePlanogramFixture(plan);

            //add fixture to plan
            PlanogramFixtureItem fixtureItem = PlanogramFixtureItem.NewPlanogramFixtureItem();
            fixtureItem.PlanogramFixtureId = planFixture.Id;
            if (addToFixture != null)
            {
                fixtureItem.X = addToFixture.X + plan.Fixtures.FindById(addToFixture.PlanogramFixtureId).Width;
            }
            plan.FixtureItems.Add(fixtureItem);


            //add linked annotations
            //setting the new fixture item id.
            foreach (FixtureAnnotation annotation in package.Annotations)
            {
                PlanogramAnnotation planAnnotation = annotation.NewPlanogramAnnotation();
                planAnnotation.PlanogramFixtureItemId = fixtureItem.Id;
                plan.Annotations.Add(planAnnotation);
            }


            return fixtureItem;
        }

        /// <summary>
        /// Adds the given fixture items to the planogram.
        /// </summary>
        /// <param name="fixtureItems"></param>
        /// <param name="plan"></param>
        /// <param name="nearestFixtureItem"></param>
        /// <returns></returns>
        private static List<PlanogramFixtureItem> AddToPlanogram(IEnumerable<FixtureItem> fixtureItems, Planogram plan, PlanogramFixtureItem nearestFixtureItem)
        {
            List<PlanogramFixtureItem> addedItems = new List<PlanogramFixtureItem>();
            if (!fixtureItems.Any()) return addedItems;

            FixturePackage package = fixtureItems.First().Parent;

            //get the fixture to add to.
            PlanogramFixtureItem addToFixtureItem = nearestFixtureItem;
            if (addToFixtureItem == null && plan.FixtureItems.Count > 0)
            {
                addToFixtureItem = plan.FixtureItems.Last();
            }
            PlanogramFixture addToFixture = (addToFixtureItem != null) ? plan.Fixtures.FindById(addToFixtureItem.PlanogramFixtureId) : null;

            Single xOffset = (addToFixtureItem != null) ? addToFixtureItem.X + addToFixture.Width : 0;

            foreach (FixtureItem fixtureItem in fixtureItems)
            {
                PlanogramFixtureItem planFixtureItem = fixtureItem.AddOrUpdatePlanogramFixtureItem(plan);
                planFixtureItem.X += xOffset;
                addedItems.Add(planFixtureItem);
            }

            //add linked annotations
            //setting the new fixture item id.
            foreach (FixtureAnnotation annotation in package.Annotations)
            {
                plan.Annotations.Add(annotation.NewPlanogramAnnotation());
            }


            return addedItems;
        }
    }
}
