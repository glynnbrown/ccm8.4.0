﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM801

// V8-28676 : A.Silva
//      Created.
// V8-28892 : A.Silva
//      Added caching mechanism for files in folder so that fetching them is faster in multiple requests.
// V8-28901 : A.Silva
//      Amended FindFileInFolder so that if the image for ImageType other than None is not found to get the None one instead.
// V8-28900 : A.Silva
//      Added GetPlanogramImageDescription overload and used to add the full path to the file once it is known (when the image is finally loaded).
// V8-29018 : D.Pleasance
//      Amended MaxCacheDuration to 10 mins
// V8-29018 : D.Pleasance
//      Amended GetPlanogramImageDescription to include image source (settings may be repository or folder)
#endregion

#region Version History: CCM820
// V8-31442 : A.Probyn
//      Updated to make thread safe.
// V8-31442 : L.Ineson
//  Reverted back so that FindFileInFolder doesn't strip the extension.
#endregion

#region Version History: CCM830
// CCM-14044 : G.Richards
//  Colour depth is automatically set to a pixel format of Pbgra32 when transparency is on
//  allowing the image transparency to be shown correctly. 
// CCM-18454 : L.Ineson
//  Made changes to how images are cached, prioritised and flagged as expired to speed things up.
#endregion
#endregion

using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Galleria.Ccm.Helpers
{
    /// <summary>
    ///     Provides real images from folders.
    /// </summary>
    public class RealImageProviderForFolder : RealImageProviderBase
    {
        #region Fields

        private static readonly Dictionary<RealImageCompressionType, Size> _sizeLookUp = new Dictionary
            <RealImageCompressionType, Size>
        {
            {RealImageCompressionType.None, new Size(512, 512)},
            {RealImageCompressionType.Quarter, new Size(384, 384)},
            {RealImageCompressionType.Half, new Size(256, 256)},
            {RealImageCompressionType.ThreeQuarter, new Size(128, 128)}
        };

        private static readonly Dictionary<RealImageColourDepthType, PixelFormat> _pixelFormatLookUp = new Dictionary
            <RealImageColourDepthType, PixelFormat>
        {
            {RealImageColourDepthType.Bit4, PixelFormats.Indexed4},
            {RealImageColourDepthType.Bit8, PixelFormats.Indexed8},
            {RealImageColourDepthType.Bit24, PixelFormats.Rgb24}
        };

        /// <summary>
        ///     Lookup dictionary for the resolved full path for a given filename.
        /// </summary>
        private readonly Dictionary<String, String> _foundPaths = new Dictionary<String, String>();

        /// <summary>
        ///     Lookup dictionary for the <see cref="CachedDirectories"/> information for a given directory path.
        /// </summary>
        private static readonly Dictionary<String, CachedDirectories> DirectoriesCache = new Dictionary<String, CachedDirectories>();

        /// <summary>
        ///     Lookup dictionary for the <see cref="CachedFiles"/> information for a given directory path.
        /// </summary>
        private static readonly Dictionary<String, CachedFiles> FilesCache = new Dictionary<String, CachedFiles>();

        #endregion

        #region Constructor

        public RealImageProviderForFolder(IRealImageProviderSettings settings)
            : base(settings, 0)
        {
            this.FetchBatchSize = Convert.ToInt32(ConfigurationManager.AppSettings["FolderImagePollBatchSize"]);
        }

        #endregion

        #region Methods

        private String GetPath(String name)
        {
            //  If the name had been found already, ensure it is still valid and return it if so.
            String path;
            if (_foundPaths.ContainsKey(name))
            {
                path = _foundPaths[name];
                if (File.Exists(path)) return path;

                _foundPaths.Remove(name);
            }

            //  Try to find the file in the folder structure.
            path = FindFileInFolder(name, this.Settings.ProductImageLocation, true);

            //  If it is a miss, store it so we do not search for it in a while.
            if (String.IsNullOrEmpty(path))
                {
                //add a blank entry to the cache.
                //lock (_missingPaths)
                //{
                //    if (!_missingPaths.ContainsKey(name)) _missingPaths.Add(name, DateTime.UtcNow);
                //}
                }
            else if (!_foundPaths.ContainsKey(name)) _foundPaths.Add(name, path);

            return path;
        }

        private String GetAttributeValue(Object imageTarget, String fieldPlaceHolder)
        {
            PlanogramProduct product = imageTarget as PlanogramProduct;
            if (product == null) return String.Empty;

            ObjectFieldInfo objectFieldInfo =
                PlanogramProduct.EnumerateDisplayableFieldInfos(/*incMetadata*/false, /*incCustom*/true, /*incPerformance*/false)
                .FirstOrDefault(o => o.FieldPlaceholder == fieldPlaceHolder);

            return objectFieldInfo != null
                       ? Convert.ToString(objectFieldInfo.GetValue(imageTarget))
                       : String.Empty;
        }

        protected override void OnSettingsChanging(IRealImageProviderSettings settings)
        {
            Type type = typeof (IRealImageProviderSettings);
            if (type.GetProperties().All(propertyInfo => Equals(propertyInfo.GetValue(this.Settings, null), propertyInfo.GetValue(settings, null)))) return;

            _foundPaths.Clear();
            ImageCache.Clear();
        }

        protected override String GetNameCriteria(Object imageTarget)
        {
            return this.GetAttributeValue(imageTarget, this.Settings.ProductImageAttribute);
        }

        /// <summary>
        ///     Create a new description for the <see cref="PlanogramImage" /> based on the given <paramref name="criteria" />.
        /// </summary>
        /// <param name="criteria">
        ///     A <see cref="CachedPlanogramImageCriteria" /> instance containing the
        ///     criteria to use when creating the description.
        /// </param>
        /// <returns>A description created from the values in the <paramref name="criteria" />.</returns>
        protected override String GetPlanogramImageDescription(String imageSource, CachedPlanogramImageCriteria criteria)
        {
            return GetPlanogramImageDescription(imageSource, this.GetPlanogramImageFileName(criteria));
        }

        /// <summary>
        ///     Create a new file name for the <see cref="PlanogramImage" /> based on the given <paramref name="criteria" />.
        /// </summary>
        /// <param name="criteria">
        ///     A <see cref="CachedPlanogramImageCriteria" /> instance containing the
        ///     criteria to use when creating the file name.
        /// </param>
        /// <returns>A file name created from the values in the <paramref name="criteria" />.</returns>
        protected override String GetPlanogramImageFileName(CachedPlanogramImageCriteria criteria)
        {
            return String.Format("{0}{1}.{2}", criteria.Attribute, GetSufixByImageType(criteria.ImageType), GetExtensionByFacing(criteria.FacingType));
        }

        

        /// <summary>
        ///     Updates the real image in the <see cref="PlanogramImage"/> instance.
        /// </summary>
        /// <param name="planogramImage">The instance that should have its image data updated.</param>
        /// <returns>A <see cref="Byte"/> array with the image that was updated, if any.</returns>
        /// <remarks>This override will use the file system to retrieve images from.</remarks>
        protected override Byte[] UpdateRealImage(CachedPlanogramImage planogramImage)
        {
            // A planogram image is needed to be updated.
            if (planogramImage == null) return null;

            // Get the path to the file, nothing to update without one.
            String path =
                (!planogramImage.IsImageConfirmedMissing())?
                this.GetPath(planogramImage.Image.FileName) : null;

            if (String.IsNullOrEmpty(path))
            {
                planogramImage.UpdateImage(null);
                return null;
            }

            // Get the image from the file, update the planogram image and return the new image.
            Byte[] imageData = CompressImage(File.ReadAllBytes(path));
            planogramImage.UpdateImage(imageData,
                GetPlanogramImageDescription(this.ImageSourceDescription, path).Replace(Settings.ProductImageLocation, @".."));

            return imageData;
        }
        
        protected override void UpdateRealImages(List<CachedPlanogramImage> imageList)
        {
            Parallel.ForEach(imageList,
                (img) =>
                {
                    UpdateRealImage(img);
                });
        }

        #region Helpers

        private static String GetSufixByImageType(PlanogramImageType imageType)
        {
            return imageType == PlanogramImageType.None
                       ? String.Empty
                       : String.Format("_{0}", (Int32)imageType);
        }

        /// <summary>
        /// Returns the extension for the given facing type.
        /// </summary>
        /// <param name="facingType"></param>
        /// <returns></returns>
        private String GetExtensionByFacing(PlanogramImageFacingType facingType)
        {
            switch (facingType)
            {
                case PlanogramImageFacingType.Front:
                    return Settings.ProductImageFacingPostfixFront;
                case PlanogramImageFacingType.Left:
                    return Settings.ProductImageFacingPostfixLeft;
                case PlanogramImageFacingType.Top:
                    return Settings.ProductImageFacingPostfixTop;
                case PlanogramImageFacingType.Back:
                    return Settings.ProductImageFacingPostfixBack;
                case PlanogramImageFacingType.Right:
                    return Settings.ProductImageFacingPostfixRight;
                case PlanogramImageFacingType.Bottom:
                    return Settings.ProductImageFacingPostfixBottom;
            }
            return Settings.ProductImageFacingPostfixFront;
        }

        /// <summary>
        /// Compresses the given image data according to the current pixel format setting.
        /// </summary>
        private Byte[] CompressImage(Byte[] original)
        {
            using (MemoryStream memoryStream = new MemoryStream(original))
            {
                Size size = _sizeLookUp[Settings.ProductImageCompression];
                PixelFormat pixelFormat = _pixelFormatLookUp[this.Settings.ProductImageColourDepth];

                if (this.Settings.ProductImageTransparency)
                {
                    pixelFormat = PixelFormats.Pbgra32;
                }

                BitmapSource bitmap = BitmapHelper.CreateBitmap(memoryStream, size, pixelFormat, true);
                Byte[] compressImage = BitmapHelper.CreateBlob(bitmap);
                return compressImage;
            }
        }

        private static String FindFileInFolder(String fileName, String path, Boolean recursive)
        {

            try
            {
                // Get all files in the base path for the images.
                String filePath = GetFilesInFolder(path).FirstOrDefault(s =>
                {
                    String name = Path.GetFileName(s);
                    return name != null && String.Equals(name, fileName, StringComparison.CurrentCultureIgnoreCase);
                });

   

                if (!String.IsNullOrEmpty(filePath)) return filePath;

                // NB - A.Silva: This code will retrieve the unit merchandising type image if there is no file for the current request.
                // I am commenting this out as it would seem that the best place to do this is at the source of the request.
                // If there is a sufix try without...
                //if (Regex.IsMatch(fileName, AttributevalueImagetypeFacingtypePattern))
                //{
                //    String unitName = Regex.Replace(fileName, AttributevalueImagetypeFacingtypePattern, AttributevalueFacingtypeReplacePattern);
                //    if (!String.IsNullOrEmpty(unitName))
                //    {
                //        filePath = GetFilesInFolder(path).FirstOrDefault(s =>
                //        {
                //            String name = Path.GetFileName(s);
                //            return name != null &&
                //                   String.Equals(name, unitName, StringComparison.InvariantCultureIgnoreCase);
                //        });

                //        if (!String.IsNullOrEmpty(filePath)) return filePath;
                //    }
                    
                //}

                // Iterate each sub forder recursevely, to find the file.
                if (recursive)
                {foreach (String directory in GetDirectories(path))
                    {
                        filePath = FindFileInFolder(fileName, directory, true);
                        if (!String.IsNullOrEmpty(filePath)) break;
                    }
                    return filePath;
                }
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine(e);
            }
            catch (PathTooLongException e)
            {
                Console.WriteLine(e);
            }
            return String.Empty;
        }

        private static IEnumerable<String> GetDirectories(String path)
        {
            CachedDirectories cachedDirectories;

            //  IF there are cached directories 
            //  AND they have not expired yet
            //  THEN return them.
            if (DirectoriesCache.TryGetValue(path, out cachedDirectories) &&
                !cachedDirectories.IsExpired)
                return cachedDirectories.Directories;

            //  Update or fetch for the first time the files to be cached.
            cachedDirectories = new CachedDirectories(path);
            lock (DirectoriesCache)
            {
                if (DirectoriesCache.ContainsKey(path))
                {
                    DirectoriesCache[path] = cachedDirectories;
                }
                else
                {
                    DirectoriesCache.Add(path, cachedDirectories);
                }
            }

            //  Return the fetched directories.
            return cachedDirectories.Directories;
        }

        /// <summary>
        ///     Gets the files in a given folder, caching the results for a minute, to avoid multiple consecutive hits to the
        ///     hardrive.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static IEnumerable<String> GetFilesInFolder(String path)
        {
            CachedFiles cachedFiles;

            //  IF there are cached files 
            //  AND they have not expired yet
            //  THEN return them.
            if (FilesCache.TryGetValue(path, out cachedFiles) &&
                !cachedFiles.IsExpired)
                return cachedFiles.Files;

            //  Update or fetch for the first time the files to be cached.
            cachedFiles = new CachedFiles(path);
            lock (FilesCache)
            {
                if (FilesCache.ContainsKey(path))
                {
                    FilesCache[path] = cachedFiles;
                }
                else
                {
                    FilesCache.Add(path, cachedFiles);
                }
            }

            //  Return the fetched files.
            return cachedFiles.Files;
        }

        /// <summary>
        ///     Overload version to accept the path directly.
        /// </summary>
        /// <param name="path">The path to be used in the <see cref="PlanogramImage"/> description.</param>
        /// <returns>A description for the <see cref="PlanogramImage"/> containing the specified path.</returns>
        private static String GetPlanogramImageDescription(String imageSource, String path)
        {
            return String.Format(Message.RealImageProvider_FolderImagenDescription, imageSource, path);
        }
        #endregion

        #endregion

        #region IDisposable Members

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public override void Dispose()
        {
            if (IsDisposed) return;

            //_missingPaths.Clear();
            _foundPaths.Clear();
            base.Dispose();
        }

        #endregion

        #region Nested Class: CachedDirectories

        /// <summary>
        ///     Private helper class to keep track of the cached elements and when they expire.
        /// </summary>
        private class CachedDirectories
        {
            #region Constants

            /// <summary>
            ///     Arbitrary time to wait before expecting the cache to be too old.
            /// </summary>
            private const Int32 MaxCacheDuration = 600;

            #endregion

            #region Fields

            /// <summary>
            ///     The date and time when the directory files were cached.
            /// </summary>
            private readonly DateTime _timeStamp;

            #endregion

            #region Properties

            /// <summary>
            ///     The enumeration of files that were found in the path when the cache was created.
            /// </summary>
            public IEnumerable<String> Directories { get; private set; }

            /// <summary>
            ///     Gets whether the instance file cache is to be considered expired or still valid.
            /// </summary>
            public Boolean IsExpired
            {
                get
                {
                    TimeSpan timeStampAge = DateTime.UtcNow.Subtract(_timeStamp).Duration();
                    TimeSpan maxTimeStampAge = TimeSpan.FromSeconds(MaxCacheDuration);
                    return timeStampAge > maxTimeStampAge;
                }
            }

            #endregion

            #region Constructor

            /// <summary>
            ///     Initializes a new instance of <see cref="CachedDirectories"/> for the given <paramref name="path"/>, marking a timestamp for when the files where cached.
            /// </summary>
            /// <param name="path">The path to get the files from.</param>
            public CachedDirectories(String path)
            {
                IEnumerable<String> files = null;
                if (path != null)
                {
                    files = Directory.GetDirectories(path);
                }
                this.Directories = new List<String>(files ?? new List<String>());
                _timeStamp = DateTime.UtcNow;
            }

            #endregion
        }

        #endregion

        #region Nested Class: CachedFiles

        /// <summary>
        ///     Private helper class to keep track of the cached elements and when they expire.
        /// </summary>
        private class CachedFiles
        {
            #region Constants

            /// <summary>
            ///     Arbitrary time to wait before expecting the cache to be too old.
            /// </summary>
            private const Int32 MaxCacheDuration = 600;

            #endregion

            #region Fields

            /// <summary>
            ///     The date and time when the directory files were cached.
            /// </summary>
            private readonly DateTime _timeStamp;

            #endregion

            #region Properties

            /// <summary>
            ///     The enumeration of files that were found in the path when the cache was created.
            /// </summary>
            public IEnumerable<String> Files { get; private set; }

            /// <summary>
            ///     Gets whether the instance file cache is to be considered expired or still valid.
            /// </summary>
            public Boolean IsExpired
            {
                get
                {
                    TimeSpan timeStampAge = DateTime.UtcNow.Subtract(_timeStamp).Duration();
                    TimeSpan maxTimeStampAge = TimeSpan.FromSeconds(MaxCacheDuration);
                    return timeStampAge > maxTimeStampAge;
                }
            }

            #endregion

            #region Constructor

            /// <summary>
            ///     Initializes a new instance of <see cref="CachedFiles"/> for the given <paramref name="path"/>, marking a timestamp for when the files where cached.
            /// </summary>
            /// <param name="path">The path to get the files from.</param>
            public CachedFiles(String path)
            {
                IEnumerable<String> files = null;
                if (path != null)
                {
                    files = Directory.GetFiles(path);
                }
                this.Files = new List<String>(files ?? new List<String>());
                _timeStamp = DateTime.UtcNow;
            }

            #endregion
        }

        #endregion
    }
}