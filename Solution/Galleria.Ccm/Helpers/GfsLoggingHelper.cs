﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
// V8-26365 : L.Ineson
//  Updated so that mock services can be used in testing.
// V8-26982 : L.Luong
//  Renamed file and moved file to Ccm.Helpers
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Security.Principal;
using System.Threading;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;

using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.Processes;

using CcmEventLog = Galleria.Ccm.Services.EventLog.EventLog;
using CcmEventLogEvent = Galleria.Ccm.Model.EventLogEvent;
using CcmEventLogEventHelper = Galleria.Ccm.Model.EventLogEventHelper;

using Gibraltar.Agent;
using Galleria.Ccm.Processes.Synchronization.SynchronizeTarget;
using System.Runtime.CompilerServices;

namespace Galleria.Ccm.Helpers
{
    /// <summary>
    /// The <b>LoggingHelper</b> class is used for logging application log messages and errors.
    /// </summary>
    public sealed class GfsLoggingHelper : IDisposable
    {
        #region Constants
        public const String SyncEventLogName = "Synchronization";
        private const String _sessionIdName = "Session ID";
        #endregion

        #region Fields

        private const String _appLogName = "Application";
        private const String _saWindowsSourceName = "CCM";
        private const String _saDbSourceName = "CCM";

        //private System.Diagnostics.
        EventLog _eventLog;
        private CultureInfo _locale;

        private static GfsLoggingHelper _logger;

        #endregion

        #region Properties

        /// <summary>
        /// Gets and sets the <b>LoggingHelper</b> to for the current <see cref="AppDomain"/>.
        /// </summary>
        public static GfsLoggingHelper Logger
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                if (_logger == null)
                {
                    // If someone tries to use the app domain logger without explicitely having created one, just 
                    // assume that they don't want to write to the windows event log.
                    _logger = new GfsLoggingHelper(true);
                }

                return _logger;
            }
            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                _logger = value;
            }
        }

        /// <summary>
        /// Returns the windows application log name
        /// </summary>
        public static String ApplicationLogName
        {
            get { return _appLogName; }
        }
        #endregion

        #region Construction + Destruction

        public GfsLoggingHelper() : this(null) { }

        /// <summary>
        /// Initializes a new instance of the <b>LoggingHelper</b> class.
        /// </summary>
        /// <param name="source">The source of the log messages. i.e. SystemNode.</param>
        /// <param name="config">The configuration object holding the setup parameters i.e. LoggingMode</param>
        public GfsLoggingHelper(String source)
        {
            Initialise(source);
        }

        public GfsLoggingHelper(Boolean useOnAppDomain) : this(null, useOnAppDomain) { }

        /// <summary>
        /// Initializes a new instance of the <b>LoggingHelper</b> class.
        /// </summary>
        /// <param name="source">The source of the log messages. i.e. SystemNode.</param>
        /// <param name="useOnAppDomain">When <b>true</b>, this new instance becomes the default for the <see cref="AppDomain"/>.</param>
        /// <param name="config">The configuration object holding the setup parameters i.e. LoggingMode</param>
        public GfsLoggingHelper(String source, Boolean useOnAppDomain)
            : this(source)
        {
            if (useOnAppDomain)
            {
                _logger = this;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Method to write authentication failure
        /// </summary>
        /// <param name="eventLogName"></param>
        public void WriteAuthenticationFailure(String eventLogName)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            StringBuilder database = new StringBuilder();
            foreach (DalFactoryParameterConfigElement parameter in dalFactory.DalFactoryConfig.DalParameters)
            {
                database.Append(parameter.Name);
                database.Append("=");
                database.Append(parameter.Value);
                database.Append(",");
            }
            database.Remove(database.Length - 1, 1);
            Write(
                EventLogEntryType.FailureAudit,
                eventLogName,
                null,
                CcmEventLogEvent.AuthenticationFailure,
                WindowsIdentity.GetCurrent().Name,
                database.ToString());
        }

        /// <summary>
        /// Writes the exception's details to the log. Exceptions are always logged regardless of the <see cref="ActiveLoggingMode"/>.
        /// </summary>
        /// <param name="ex">The exception to log.</param>
        public void Write(String eventLogName, String entityName, Exception ex, ProcessContext context)
        {
            if (ex == null)
            {
                throw new ArgumentNullException("ex");
            }


            Write(EventLogEntryType.Error, eventLogName, entityName, CcmEventLogEvent.UnexpectedException, context, ex);
        }

        /// <summary>
        /// Writes the exception's details to the log. Exceptions are always logged regardless of the <see cref="ActiveLoggingMode"/>.
        /// </summary>
        /// <param name="ex">The exception to log.</param>
        public void Write(String eventLogName, Int32? entityId, Exception ex)
        {
            if (ex == null)
            {
                throw new ArgumentNullException("ex");
            }

            Write(EventLogEntryType.Error, eventLogName, entityId, CcmEventLogEvent.UnexpectedException, ex);
        }

        /// <summary>
        /// Writes the fomatted message to the log.
        /// </summary>
        /// <param name="mode">The logging mode for which this message will be logged.</param>
        /// <param name="format">A composite format String.</param>
        /// <param name="args">An <see cref="Object"/> array containing zero or more objects to format.</param>
        public void Write(String eventLogName, Int32? entityId, CcmEventLogEvent eventId, params Object[] args)
        {
            Write(EventLogEntryType.Information, eventLogName, entityId, eventId, args);
        }

        /// <summary>
        /// Writes the fomatted message to the log.
        /// </summary>
        /// <param name="mode">The logging mode for which this message will be logged.</param>
        /// <param name="format">A composite format String.</param>
        /// <param name="args">An <see cref="Object"/> array containing zero or more objects to format.</param>
        public void Write(String eventLogName, String entityName, CcmEventLogEvent eventId, ProcessContext context, params Object[] args)
        {
            Write(EventLogEntryType.Information, eventLogName, entityName, eventId, context, args);
        }

        public void Write(
            EventLogEntryType type,
            String eventLogName,
            String entityName,
            CcmEventLogEvent eventId,
            ProcessContext context,
            params Object[] args
            )
        {
            String content = String.Format(_locale, CcmEventLogEventHelper.FriendlyContents[eventId], args);
            Write(type, eventLogName, entityName, eventId, context, content);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Sets up the appropriate xml keys in the web.config file for the Logging Helper
        /// </summary>
        /// <param name="source">The source of the log messages. i.e. SystemNode.</param>
        /// <param name="config">The configuration object holding the setup parameters i.e. LoggingMode</param>
        private void Initialise(String source)
        {
            _locale = Thread.CurrentThread.CurrentCulture;

            if (source != null)
            {
                _eventLog = new EventLog(_appLogName, Environment.MachineName, source);
            }
        }

        private void Write(
            EventLogEntryType type,
            String eventLogName,
            String entityName,
            CcmEventLogEvent eventId,
            ProcessContext context,
            String content
            )
        {

            String description = CcmEventLogEventHelper.FriendlyDescriptions[eventId];
            String descriptionAndContent = String.Format(
                "{0}: {1}{2}{3}Gibraltar Session ID: {4}",
                description,
                content,
                Environment.NewLine,
                Environment.NewLine,
                Log.SessionSummary.Properties[_sessionIdName]);

            try
            {
                // Log to Gibraltar.
                switch (type)
                {
                    case EventLogEntryType.Error:
                    case EventLogEntryType.FailureAudit:
                        Trace.TraceError(descriptionAndContent);
                        break;
                    case EventLogEntryType.Warning:
                        Trace.TraceWarning(descriptionAndContent);
                        break;
                    case EventLogEntryType.Information:
                    case EventLogEntryType.SuccessAudit:
                        Trace.TraceInformation(descriptionAndContent);
                        break;
                }

                // Log to webservice.
                try
                {
                    CcmEventLog eventLog = new CcmEventLog()
                    {
                        ComputerName = System.Environment.MachineName,
                        Content = content,
                        Description = description,
                        EntityName = entityName,
                        EventId = (Int32)eventId,
                        EventLogName = eventLogName,
                        LevelId = type.ToString(),
                        Process = Path.GetFileName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName),
                        SessionId = Log.SessionSummary.Properties[_sessionIdName],
                        Source = _saDbSourceName,
                    };

                    //write to the webservice
                    context.Services.EventLogServiceClient.AddEvent(new Services.EventLog.AddEventRequest(eventLog));

                }
                catch (Exception)
                {
                    // Don't cry over spilt milk.
                }

                // Log to Windows event log.
                if (_eventLog != null)
                {
                    _eventLog.WriteEntry(descriptionAndContent, type);
                }
            }
            catch (Win32Exception ex)
            {
                // If something goes wrong writing to the windows event log, such as it being full, we'll land here.
                Trace.TraceError(ex.ToString());

                using (EventLog appLog = new EventLog(_appLogName, _eventLog.MachineName, _saWindowsSourceName))
                {
                    String logMessage = _eventLog.Source + ": " + ex.ToString();
                    appLog.WriteEntry(logMessage, EventLogEntryType.Error);
                }
            }
            catch (Exception ex)
            {
                // If something more fundamental goes wrong, such as a developer running in console mode without 
                // having registered the required windows event log and sources, we'll land here.  Nothing more we can
                // do...
                Trace.TraceError(ex.ToString());
            }
        }

        private void Write(
            EventLogEntryType type,
            String eventLogName,
            Int32? entityId,
            CcmEventLogEvent eventId,
            String content)
        {
            String description = CcmEventLogEventHelper.FriendlyDescriptions[eventId];
            String descriptionAndContent = String.Format(
                "{0}: {1}{2}{3}Gibraltar Session ID: {4}",
                description,
                content,
                Environment.NewLine,
                Environment.NewLine,
                Log.SessionSummary.Properties[_sessionIdName]);

            try
            {
                // Log to Gibraltar.
                switch (type)
                {
                    case EventLogEntryType.Error:
                    case EventLogEntryType.FailureAudit:
                        Trace.TraceError(descriptionAndContent);
                        break;
                    case EventLogEntryType.Warning:
                        Trace.TraceWarning(descriptionAndContent);
                        break;
                    case EventLogEntryType.Information:
                    case EventLogEntryType.SuccessAudit:
                        Trace.TraceInformation(descriptionAndContent);
                        break;
                }

                // Log to Windows event log.
                if (_eventLog != null)
                {
                    _eventLog.WriteEntry(descriptionAndContent, type, (Int32)eventId);
                }
            }
            // GFS-16727: Use the below
            //catch (Win32Exception ex)
            //{
            //    // If something goes wrong writing to the windows event log, such as it being full, we'll land here.
            //    Trace.TraceError(ex.ToString());

            //    using (EventLog appLog = new EventLog(_appLogName, _eventLog.MachineName, _gfsWindowsSourceName))
            //    {
            //        String logMessage = _eventLog.Source + ": " + ex.ToString();
            //        appLog.WriteEntry(logMessage, EventLogEntryType.Error);
            //    }
            //}
            catch (Exception ex)
            {
                // If something more fundamental goes wrong, such as a developer running in console mode without 
                // having registered the required windows event log and sources, we'll land here.  Nothing more we can
                // do...
                Trace.TraceError(ex.ToString());
            }
        }

        public void Write(
            EventLogEntryType type,
            String eventLogName,
            Int32? entityId,
            CcmEventLogEvent eventId,
            params Object[] args)
        {
            String content = String.Format(_locale, CcmEventLogEventHelper.FriendlyContents[eventId], args);
            Write(type, eventLogName, entityId, eventId, content);
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Releases all resources.
        /// </summary>
        public void Dispose()
        {
            // TODO: Implement properly
            using (_eventLog)
            {
                _eventLog = null;
            }
        }

        #endregion

        internal void Write(EventLogEntryType eventLogEntryType, string _eventLogName, int p, CcmEventLogEvent eventLogEvent)
        {
            throw new NotImplementedException();
        }
    }
}