﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM810
// V8-29811 : A.Probyn
//  Created
// V8-30231 : A.Probyn
//  Updated OverwritePackage to reset the assigned date when copying plan assignments.
#endregion
#region Version History: CCM830
// CCM-13446 : R.Cooper
//  Use Package.SaveAsCopy when copying package.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Model;
using Csla;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Resources.Language;
using System.Globalization;

namespace Galleria.Ccm.Helpers
{
    public static class PackageHelper
    {
        /// <summary>
        /// Static method to overwrite package
        /// </summary>
        /// <param name="planToOverwrite"></param>
        /// <param name="planId"></param>
        /// <param name="planName"></param>
        /// <param name="userId"></param>
        /// <param name="plansInTarget"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static Int32? OverwritePackage(PlanogramInfo planToOverwrite, Int32 planId, String planName, Int32? userId, PlanogramInfoList plansInTarget, Int32 entityId)
        {
            // Understand our target
            var targetPlan = Package.FetchById(planToOverwrite.Id).Planograms.First();
            var catName = targetPlan.CategoryName;
            var catCode = targetPlan.CategoryCode;

            //if the plans to be overwritten and overwrite with are the same dont bother.
            if (planToOverwrite.Id == planId) return null;

            //fetch any existing content lookup
            ContentLookup existingContentLookup = null;
            try
            {
                existingContentLookup = ContentLookup.FetchByPlanogramId(planToOverwrite.Id);
            }
            catch (DataPortalException)
            {
                //simple - no content lookup exists
            }

            //fetch any existing planogram assignments
            LocationPlanAssignmentList existingPlanAssigments = LocationPlanAssignmentList.FetchByPlanogramId(planToOverwrite.Id);

            //delete the existing package (including content lookup & plan assignment)
            Boolean deleteFailed = false;
            try
            {
                Package.DeletePackageById(planToOverwrite.Id, userId, PackageLockType.User);
            }
            catch (DataPortalException)
            {
                deleteFailed = true;
            }

            Int32 copyId;
            if (!deleteFailed)
            {
                //copy the new one.
                copyId = CopyPackage(planId, userId, planName, entityId, true, catCode, catName, true);
            }
            else
            {
                //copy as a new name instead.
                String copyName = GetPlanCopyName(planName, plansInTarget);
                copyId = CopyPackage(planId, userId, copyName, entityId, true, catCode, catName, true);
            }

            //If content lookup existed
            if (existingContentLookup != null)
            {
                if (!deleteFailed)
                {
                    //delete didn't fail, so original will have been deleted as part of the package delete
                    ContentLookup newContentLookup = existingContentLookup.Copy();
                    newContentLookup.PlanogramId = copyId;
                    newContentLookup.Save();
                }
                else
                {
                    //delete failed so updated existing to match new package
                    existingContentLookup.PlanogramId = copyId;
                    existingContentLookup.Save();
                }
            }

            //If plan assignments existed
            if (existingPlanAssigments.Count > 0)
            {
                if (!deleteFailed)
                {
                    // Get the current local date in UTC
                    DateTime currentLocalDate = DateTime.UtcNow;

                    //delete didn't fail, so original will have been deleted as part of the package delete
                    LocationPlanAssignmentList newPlanAssigments = LocationPlanAssignmentList.NewLocationPlanAssignmentList();
                    foreach (LocationPlanAssignment existingPlanAssignment in existingPlanAssigments)
                    {
                        LocationPlanAssignment newPlanAssignment = existingPlanAssignment.Copy();
                        newPlanAssignment.PlanogramId = copyId;
                        //newPlanAssignment.DateAssigned = currentLocalDate;
                        newPlanAssignment.SetAssignedDate(currentLocalDate);
                        
                        // We have changed the plan so publishing needs to know.
                        if (newPlanAssignment.PublishStatus == PlanAssignmentPublishStatusType.Successful) newPlanAssignment.PublishStatus = PlanAssignmentPublishStatusType.PlanogramAltered;

                        newPlanAssigments.Add(newPlanAssignment);
                    }
                    newPlanAssigments.Save();
                }
                else
                {
                    //delete failed so updated existing to match new package
                    foreach (LocationPlanAssignment existingPlanAssignment in existingPlanAssigments)
                    {
                        existingPlanAssignment.PlanogramId = copyId;
                    }
                    existingPlanAssigments.Save();
                }
            }

            return copyId;
        }

        public static Int32 CopyPackage(Int32 packageToCopyId, Int32? userId, String copyName, Int32 entityId, Boolean isPartOfOverwrite, String catCode, String catName, Boolean shouldOverrideCategoryCodeName)
        {
            // If shouldOverrideCategoryCodeName then we take the code and name from the original plan and inject it to override the one in the new plan

            Int32 copyId;
            using (Package packageToCopy = Package.FetchById(packageToCopyId))
            {
                packageToCopy.Name = copyName;
                packageToCopy.Planograms[0].Name = copyName;
                
                if (shouldOverrideCategoryCodeName)
                {
                    if (String.IsNullOrEmpty(packageToCopy.Planograms[0].CategoryCode)) packageToCopy.Planograms[0].CategoryCode = catCode;
                    if (String.IsNullOrEmpty(packageToCopy.Planograms[0].CategoryName)) packageToCopy.Planograms[0].CategoryName = catName;
                }
               
                Package copy = packageToCopy.SaveAsCopy(userId, entityId);
                copyId = (Int32)copy.Id;
                copy.Dispose();
                copy = null;
            }

            //If this is not part of the overwrite, copy the linked content. The overwrite behaviour is differnt and controlled
            //within the Overwrite Package method.
            if (!isPartOfOverwrite)
            {
                CopyPackageContent(packageToCopyId, copyId);
            }

            return copyId;
        }

        public static void CopyPackageContent(Int32 copyFromPlanId, Int32 copyToPlanId)
        {
            //copy any content lookup to the new copy
            ContentLookup existingContentLookup = null;
            try
            {
                existingContentLookup = ContentLookup.FetchByPlanogramId(copyFromPlanId);
            }
            catch (DataPortalException)
            {
                //simple - no content lookup exists
            }

            //If there is any existing content lookup 
            if (existingContentLookup != null)
            {
                //Copy it
                ContentLookup newContentLookup = existingContentLookup.Copy();
                newContentLookup.PlanogramId = copyToPlanId;
                newContentLookup.Save();
            }
        }

        public static String GetPlanCopyName(String currentName, PlanogramInfoList plansInTarget)
        {
            //first strip out the start "Copy of " from the plan name
            String initialCopyPrefix = Message.Workpackage_CopyOf.TrimEnd("{0}".ToArray());
            currentName = currentName.Replace(initialCopyPrefix, "");


            //get initial copy name
            String copyName = String.Format(CultureInfo.CurrentCulture, Message.Workpackage_CopyOf, currentName);
            if (copyName.Length > 100)
            {
                //too long so trim it.
                copyName = String.Format(CultureInfo.CurrentCulture, Message.Workpackage_CopyOfTrimmed,
                    currentName.Substring(0, currentName.Length - (copyName.Length - 100 - 2)));
            }
            PlanogramInfo existingPlan = plansInTarget.FirstOrDefault(p => String.Compare(p.Name, copyName, true) == 0);

            //if name already taken then start adding numbers
            Int32 copyNo = 1;
            while (existingPlan != null)
            {
                copyName = String.Format(CultureInfo.CurrentCulture, Message.Workpackage_CopyXOf, copyNo, currentName);
                if (copyName.Length > 100)
                {
                    copyName = String.Format(CultureInfo.CurrentCulture, Message.Workpackage_CopyXOf, copyNo,
                        currentName.Substring(0, currentName.Length - (copyName.Length - 100)));
                }
                existingPlan = plansInTarget.FirstOrDefault(p => String.Compare(p.Name, copyName, true) == 0);
                copyNo++;
            }

            return copyName;
        }

    }
}
