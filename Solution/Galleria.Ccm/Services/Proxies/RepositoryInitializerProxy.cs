﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using System.Collections.Generic;
using AutoMapper;
using Galleria.Ccm.Model;
using Galleria.Framework2.Repository.Services;
using Galleria.Framework2.Repository.Services.DataContracts;
using Galleria.Framework2.Repository.Services.MessageContracts;
using Galleria.Framework2.Services;

namespace Galleria.Ccm.Services.Proxies
{
    public class RepositoryInitializerProxy : ServiceProxy<IRepositoryInitializer>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public RepositoryInitializerProxy(String address)
            : base(address)
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called when initializing this service
        /// </summary>
        public void Initialize(RepositorySyncTarget target)
        {
            this.Channel.Initialize(new InitializeRequest()
            {
                Parameters = Mapper.Map<RepositorySyncTargetParameterList, List<RepositoryParameterDc>>(
                    target.Parameters,
                    new List<RepositoryParameterDc>())
            });
        }
        #endregion
    }
}
