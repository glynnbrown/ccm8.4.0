﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using System.Collections.Generic;
using AutoMapper;
using Galleria.Ccm.Model;
using Galleria.Framework2.Repository.Services;
using Galleria.Framework2.Repository.Services.DataContracts;
using Galleria.Framework2.Repository.Services.MessageContracts;
using Galleria.Framework2.Services;

namespace Galleria.Ccm.Services.Proxies
{
    /// <summary>
    /// Repository Service Proxy
    /// </summary>
    public class RepositoryServiceProxy : ServiceProxy<IRepositoryService>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public RepositoryServiceProxy(String address)
            : base(address)
        {
        }
        #endregion

        #region Methods

        #region GetRepositoryInfo
        /// <summary>
        /// Returns the repository information
        /// </summary>
        /// <returns>The repository information</returns>
        public RepositoryInfoDc GetRepositoryInfo(RepositorySyncTarget target)
        {
            return this.Channel.GetRepositoryInfo(new GetRepositoryInfoRequest()
            {
                Parameters = Mapper.Map<RepositorySyncTargetParameterList, List<RepositoryParameterDc>>(
                    target.Parameters, new List<RepositoryParameterDc>())
            }).RepositoryInfo;
        }
        #endregion

        #region RegisterRepository
        /// <summary>
        /// Registers an external repository within this repository
        /// </summary>
        /// <param name="repositoryInfo">The repository info</param>
        public void RegisterRepository(RepositoryInfoDc repositoryInfo)
        {
            this.Channel.RegisterRepository(new RegisterRepositoryRequest()
            {
                RepositoryInfo = repositoryInfo
            });
        }
        #endregion

        #region GetNextRepositoryItem
        /// <summary>
        /// Returns the next item in the repository
        /// for the specified destination repository id
        /// </summary>
        /// <param name="repositoryId">The destination repository id</param>
        /// <returns>The next repository item</returns>
        public RepositoryItemDc GetNextRepositoryItem(Guid repositoryId)
        {
            return this.Channel.GetNextRepositoryItem(new GetNextRepositoryItemRequest()
            {
                RepositoryId = repositoryId
            }).RepositoryItem;
        }
        #endregion

        #region GetRepositoryPlanogram
        /// <summary>
        /// Returns the specified repository planogram
        /// </summary>
        /// <param name="id">The id of the planogram to return</param>
        /// <returns>The specified repository planogram</returns>
        public RepositoryPlanogramDc GetRepositoryPlanogram(Guid id)
        {
            return this.Channel.GetRepositoryPlanogram(new GetRepositoryPlanogramRequest()
            {
                Id = id
            }).RepositoryPlanogram;
        }
        #endregion

        #region PublishRepositoryPlanogram
        /// <summary>
        /// Publishes the specified repository planogram
        /// </summary>
        /// <param name="repositoryPlanogram">The repository planogram to publish</param>
        public void PublishRepositoryPlanogram(RepositoryPlanogramDc repositoryPlanogram)
        {
            this.Channel.PublishRepositoryPlanogram(new PublishRepositoryPlanogramRequest()
            {
                RepositoryPlanogram = repositoryPlanogram
            });
        }
        #endregion

        #endregion
    }
}
