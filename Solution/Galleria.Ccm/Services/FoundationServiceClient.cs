﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
// V8-26365 : L.Ineson
//  Updated so that mock services can be used in testing.
// V8-25556 : J.Pickup
//  Added LocationProductService
// V8-27783 : M.Brumby
//  Added Content and Planogram Services
#endregion
#endregion

using System;
using System.Globalization;
using Csla;
using Galleria.Ccm.Services.Assortment;
using Galleria.Ccm.Services.AssortmentMinorRevision;
using Galleria.Ccm.Services.CategoryGoal;
using Galleria.Ccm.Services.CategoryRole;
using Galleria.Ccm.Services.CategoryTactic;
using Galleria.Ccm.Services.Cluster;
using Galleria.Ccm.Services.ConsumerDecisionTree;
using Galleria.Ccm.Services.Entity;
using Galleria.Ccm.Services.EventLog;
using Galleria.Ccm.Services.File;
using Galleria.Ccm.Services.Image;
using Galleria.Ccm.Services.Location;
using Galleria.Ccm.Services.LocationProduct;
using Galleria.Ccm.Services.LocationSpace;
using Galleria.Ccm.Services.Metric;
using Galleria.Ccm.Services.Performance;
using Galleria.Ccm.Services.Product;
using Galleria.Ccm.Services.Project;
using Galleria.Ccm.Services.Publishing;
using Galleria.Ccm.Services.Range;
using Galleria.Ccm.Services.Settings;
using Galleria.Ccm.Services.Timeline;
using Galleria.Ccm.Services.Content;
using Galleria.Ccm.Services.Planogram;

namespace Galleria.Ccm.Services
{
    /// <summary>
    /// A class used to manage all the service client proxies
    /// that are related to galleria foundation services
    /// </summary>
    public sealed class FoundationServiceClient : ServiceClient
    {
        #region Fields

        private String _endpointAddressRoot;

        private EntityService _entityServiceClient;
        private ProductService _productServiceClient;
        private LocationService _locationServiceClient;
        private LocationProductService _locationProductServiceClient;
        private ClusterService _clusterServiceClient;
        private LocationSpaceService _locationSpaceServiceClient;
        private EventLogService _eventLogServiceClient;
        private PerformanceService _performanceServiceClient;
        private MetricService _metricServiceClient;
        private SettingsService _settingsServiceClient;
        private TimelineService _timelineServiceClient;
        private ConsumerDecisionTreeService _consumerDecisionTreeServiceClient;
        private RangeService _rangeServiceClient;
        private ImageService _imageServiceClient;
        private CategoryGoalService _categoryGoalServiceClient;
        private CategoryRoleService _categoryRoleServiceClient;
        private CategoryTacticService _categoryTacticServiceClient;
        private ProjectService _projectServiceClient;
        private AssortmentService _assortmentServiceClient;
        private FileService _fileServiceClient;
        private AssortmentMinorRevisionService _assortmentMinorRevisionServiceClient;
        private PublishingService _publishingServiceClient;
        private ContentService _contentServiceClient;
        private PlanogramService _planogramServiceClient;

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public FoundationServiceClient(String endpointRoot)
            : base()
        {
            if (!String.IsNullOrEmpty(endpointRoot))
            {
                // ensure backslashes are not forward slashes
                endpointRoot.Replace(@"\", @"/");

                // remove any trailing backslashes
                while (endpointRoot.EndsWith(@"/"))
                {
                    endpointRoot = endpointRoot.Remove(endpointRoot.Length - 1, 1);
                }

                //SA-20778 - adjust the endpoint if contains unnessery slashes  
                String endToAdjust = endpointRoot.Substring(7);
                if (endpointRoot.StartsWith(@"http://") && endToAdjust.Contains(@"//"))
                {
                    endpointRoot = endpointRoot.Substring(0, 7) + endToAdjust.Replace(@"//", @"/");
                }

                // add the services subdirectory to the end if required
                if (!endpointRoot.EndsWith("SERVICES", StringComparison.InvariantCultureIgnoreCase))
                {
                    endpointRoot += @"/SERVICES";
                }

                // set the endpoint root
                _endpointAddressRoot = endpointRoot;
            }
        }
        #endregion

        #region Properties
        /// <summary>
        /// The Assortment service client
        /// </summary>
        public AssortmentService AssortmentServiceClient
        {
            get
            {
                if (_assortmentServiceClient == null)
                {
                    _assortmentServiceClient = OpenService<AssortmentService>(GetAddress(_endpointAddressRoot, "Assortment"));
                }
                return _assortmentServiceClient;
            }
        }

        /// <summary>
        /// The Assortment Minor Revision Service Client
        /// </summary>
        public AssortmentMinorRevisionService AssortmentMinorRevisionServiceClient
        {
            get
            {
                if (_assortmentMinorRevisionServiceClient == null)
                {
                    _assortmentMinorRevisionServiceClient = OpenService<AssortmentMinorRevisionService>(GetAddress(_endpointAddressRoot, "AssortmentMinorRevision"));
                }
                return _assortmentMinorRevisionServiceClient;
            }
        }

        /// <summary>
        /// the Category Goal service client
        /// </summary>
        public CategoryGoalService CategoryGoalServiceClient
        {
            get
            {
                if (_categoryGoalServiceClient == null)
                {
                    _categoryGoalServiceClient = OpenService<CategoryGoalService>(GetAddress(_endpointAddressRoot, "CategoryGoal"));
                }
                return _categoryGoalServiceClient;
            }
        }

        /// <summary>
        /// the Category Role service client
        /// </summary>
        public CategoryRoleService CategoryRoleServiceClient
        {
            get
            {
                if (_categoryRoleServiceClient == null)
                {
                    _categoryRoleServiceClient = OpenService<CategoryRoleService>(GetAddress(_endpointAddressRoot, "CategoryRole"));
                }
                return _categoryRoleServiceClient;
            }
        }

        /// <summary>
        /// the Category Tactic service client
        /// </summary>
        public CategoryTacticService CategoryTacticServiceClient
        {
            get
            {
                if (_categoryTacticServiceClient == null)
                {
                    _categoryTacticServiceClient = OpenService<CategoryTacticService>(GetAddress(_endpointAddressRoot, "CategoryTactic"));
                }
                return _categoryTacticServiceClient;
            }
        }

        /// <summary>
        /// The cluster service client proxy
        /// </summary>
        public ClusterService ClusterServiceClient
        {
            get
            {
                if (_clusterServiceClient == null)
                {
                    _clusterServiceClient = OpenService<ClusterService>(GetAddress(_endpointAddressRoot, "Cluster"));
                }
                return _clusterServiceClient;
            }
        }

        /// <summary>
        /// The consumer decision tree service client
        /// </summary>
        public ConsumerDecisionTreeService ConsumerDecisionTreeServiceClient
        {
            get
            {
                if (_consumerDecisionTreeServiceClient == null)
                {
                    _consumerDecisionTreeServiceClient = OpenService<ConsumerDecisionTreeService>(GetAddress(_endpointAddressRoot, "ConsumerDecisionTree"));
                }
                return _consumerDecisionTreeServiceClient;
            }
        }

        /// <summary>
        /// The entity service client proxy
        /// </summary>
        public EntityService EntityServiceClient
        {
            get
            {
                if (_entityServiceClient == null)
                {
                    _entityServiceClient = OpenService<EntityService>(GetAddress(_endpointAddressRoot, "Entity"));
                }
                return _entityServiceClient;
            }
        }

        /// <summary>
        /// The event log service client
        /// </summary>
        public EventLogService EventLogServiceClient
        {
            get
            {
                if (_eventLogServiceClient == null)
                {
                    _eventLogServiceClient = OpenService<EventLogService>(GetAddress(_endpointAddressRoot, "EventLog"));
                }
                return _eventLogServiceClient;
            }
        }

        /// <summary>
        /// The File service client
        /// </summary>
        public FileService FileServiceClient
        {
            get
            {
                if (_fileServiceClient == null)
                {
                    _fileServiceClient = OpenService<FileService>(GetAddress(_endpointAddressRoot, "File"));
                }
                return _fileServiceClient;
            }
        }

        /// <summary>
        /// The image service client
        /// </summary>
        public ImageService ImageServiceClient
        {
            get
            {
                if (_imageServiceClient == null)
                {
                    _imageServiceClient = OpenService<ImageService>(GetAddress(_endpointAddressRoot, "Image"));
                }
                return _imageServiceClient;
            }
        }


        /// <summary>
        /// The location service client proxy
        /// </summary>
        public LocationService LocationServiceClient
        {
            get 
            {
                if (_locationServiceClient == null)
                {
                    _locationServiceClient = OpenService<LocationService>(GetAddress(_endpointAddressRoot, "Location"));
                }
                return _locationServiceClient; 
            }
        }


        /// <summary>
        /// The locationProduct service client proxy
        /// </summary>
        public LocationProductService LocationProductServiceClient
        {
            get
            {
                if (_locationProductServiceClient == null)
                {
                    _locationProductServiceClient = OpenService<LocationProductService>(GetAddress(_endpointAddressRoot, "LocationProduct"));
                }
                return _locationProductServiceClient;
            }
        }

        /// <summary>
        /// The location Space service client proxy
        /// </summary>
        public LocationSpaceService LocationSpaceServiceClient
        {
            get 
            {
                if (_locationSpaceServiceClient == null)
                {
                    _locationSpaceServiceClient = OpenService<LocationSpaceService>(GetAddress(_endpointAddressRoot, "LocationSpace"));
                }
                return _locationSpaceServiceClient; 
            }
        }

        /// <summary>
        /// The metric service client
        /// </summary>
        public MetricService MetricServiceClient
        {
            get
            {
                if (_metricServiceClient == null)
                {
                    _metricServiceClient = OpenService<MetricService>(GetAddress(_endpointAddressRoot, "Metric"));
                }
                return _metricServiceClient;
            }
        }

        /// <summary>
        /// The performance service client
        /// </summary>
        public PerformanceService PerformanceServiceClient
        {
            get
            {
                if (_performanceServiceClient == null)
                {
                    _performanceServiceClient = OpenService<PerformanceService>(GetAddress(_endpointAddressRoot, "Performance"));
                }
                return _performanceServiceClient;
            }
        }

        /// <summary>
        /// The product service client proxy
        /// </summary>
        public ProductService ProductServiceClient
        {
            get
            {
                if (_productServiceClient == null)
                {
                    _productServiceClient = OpenService<ProductService>(GetAddress(_endpointAddressRoot, "Product"));
                }
                return _productServiceClient;
            }
        }

        /// <summary>
        /// The Project service client
        /// </summary>
        public ProjectService ProjectServiceClient
        {
            get
            {
                if (_projectServiceClient == null)
                {
                    _projectServiceClient = OpenService<ProjectService>(GetAddress(_endpointAddressRoot, "Project"));
                }
                return _projectServiceClient;
            }
        }

        /// <summary>
        /// The Publishing Service Client
        /// </summary>
        public PublishingService PublishingServiceClient
        {
            get
            {
                if (_publishingServiceClient == null)
                {
                    _publishingServiceClient = OpenService<PublishingService>(GetAddress(_endpointAddressRoot, "Publishing"));
                }
                return _publishingServiceClient;
            }
        }

        /// <summary>
        /// The range service client
        /// </summary>
        public RangeService RangeServiceClient
        {
            get
            {
                if (_rangeServiceClient == null)
                {
                    _rangeServiceClient = OpenService<RangeService>(GetAddress(_endpointAddressRoot, "Range"));
                }
                return _rangeServiceClient;
            }
        }

        /// <summary>
        /// The settings service client
        /// </summary>
        public SettingsService SettingsServiceClient
        {
            get
            {
                if (_settingsServiceClient == null)
                {
                    _settingsServiceClient = OpenService<SettingsService>(GetAddress(_endpointAddressRoot, "Settings"));
                }
                return _settingsServiceClient;
            }
        }

        /// <summary>
        /// The timeline service client
        /// </summary>
        public TimelineService TimelineServiceClient
        {
            get 
            {
                if (_timelineServiceClient == null)
                {
                    _timelineServiceClient = OpenService<TimelineService>(GetAddress(_endpointAddressRoot, "Timeline"));
                }
                return _timelineServiceClient; 
            }
        }


        /// <summary>
        /// The content service client
        /// </summary>
        public ContentService ContentServiceClient
        {
            get
            {
                if (_contentServiceClient == null)
                {
                    _contentServiceClient = OpenService<ContentService>(GetAddress(_endpointAddressRoot, "Content"));
                }
                return _contentServiceClient;
            }
        }

        /// <summary>
        /// The planogram service client
        /// </summary>
        public PlanogramService PlanogramServiceClient
        {
            get
            {
                if (_planogramServiceClient == null)
                {
                    _planogramServiceClient = OpenService<PlanogramService>(GetAddress(_endpointAddressRoot, "Planogram"));
                }
                return _planogramServiceClient;
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Creates the service endpoint address string
        /// </summary>
        /// <param name="endpointRoot">The root endpoint url</param>
        /// <param name="endpointName">The service name</param>
        /// <returns>A service endpoint string</returns>
        private String GetAddress(String endpointRoot, String endpointName)
        {
            return String.Format(CultureInfo.InvariantCulture, "{0}/{1}.svc", endpointRoot, endpointName);
        }
        #endregion

        #region Static Helper

        /// <summary>
        /// Registers the GFS service clients with the ServiceContainer.
        /// </summary>
        public static void RegisterGFSServiceClientTypes()
        {
            ServiceContainer.RegisterService<EntityService, EntityServiceClient>();
            ServiceContainer.RegisterService<ProductService, ProductServiceClient>();
            ServiceContainer.RegisterService<LocationService, LocationServiceClient>();
            ServiceContainer.RegisterService<ClusterService, ClusterServiceClient>();
            ServiceContainer.RegisterService<LocationSpaceService, LocationSpaceServiceClient>();
            ServiceContainer.RegisterService<EventLogService, EventLogServiceClient>();
            ServiceContainer.RegisterService<PerformanceService, PerformanceServiceClient>();
            ServiceContainer.RegisterService<MetricService, MetricServiceClient>();
            ServiceContainer.RegisterService<SettingsService, SettingsServiceClient>();
            ServiceContainer.RegisterService<TimelineService, TimelineServiceClient>();
            ServiceContainer.RegisterService<ConsumerDecisionTreeService, ConsumerDecisionTreeServiceClient>();
            ServiceContainer.RegisterService<RangeService, RangeServiceClient>();
            ServiceContainer.RegisterService<ImageService, ImageServiceClient>();
            ServiceContainer.RegisterService<CategoryGoalService, CategoryGoalServiceClient>();
            ServiceContainer.RegisterService<CategoryRoleService, CategoryRoleServiceClient>();
            ServiceContainer.RegisterService<CategoryTacticService, CategoryTacticServiceClient>();
            ServiceContainer.RegisterService<ProjectService, ProjectServiceClient>();
            ServiceContainer.RegisterService<AssortmentService, AssortmentServiceClient>();
            ServiceContainer.RegisterService<FileService, FileServiceClient>();
            ServiceContainer.RegisterService<AssortmentMinorRevisionService, AssortmentMinorRevisionServiceClient>();
            ServiceContainer.RegisterService<PublishingService, PublishingServiceClient>();
            ServiceContainer.RegisterService<LocationProductService, LocationProductServiceClient>();
            ServiceContainer.RegisterService<ContentService, ContentServiceClient>();
            ServiceContainer.RegisterService<PlanogramService, PlanogramServiceClient>();
            
        }

        #endregion
    }
}