﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ServiceModel;
using AutoMapper;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.Services;
using Galleria.Framework2.Repository.Services;
using Galleria.Framework2.Repository.Services.DataContracts;
using Galleria.Framework2.Repository.Services.MessageContracts;

namespace Galleria.Ccm.Services
{
    /// <summary>
    /// A generic implementation of the repository service
    /// created to cater for the majority of syncrhonisation
    /// targets that CCM will interact with
    /// </summary>
    [ServiceBehavior(Namespace = "http://www.galleria-rts.com/ccm/services/2012/01")]
    public class RepositoryService : ServiceBase, IRepositoryService, IRepositoryInitializer
    {
        #region Constants
        private const String _dalFactoryParameter = "DalFactory";
        #endregion

        #region Version 1

        #region Initialize
        /// <summary>
        /// Called when initializing this repository service
        /// this only applies when running as a local service
        /// within the repository host
        /// </summary>
        public InitializeResponse Initialize(InitializeRequest request)
        {
            InitializeResponse response = new InitializeResponse();
            try
            {
                // create the dal factory configuration
                DalFactoryConfigElement dalFactoryConfig = new DalFactoryConfigElement(Constants.RepositoryDal);

                // loop through the request parameters and either add
                // them to the config as parameters, or use them to
                // set a property of the dal factory
                foreach (RepositoryParameterDc parameter in request.Parameters)
                {
                    switch (parameter.Name)
                    {
                        case _dalFactoryParameter:
                            dalFactoryConfig.AssemblyName = parameter.Value;
                            break;
                        default:
                            dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement(
                                parameter.Name,
                                parameter.Value));
                            break;
                    }
                }

                // and register the factory within the dal container
                DalContainer.RegisterFactory(dalFactoryConfig);
            }
            catch (Exception ex)
            {
                this.ThrowException(ex);
            }
            return response;
        }
        #endregion

        #region GetRepositoryInfo
        /// <summary>
        /// Returns information about this repository
        /// </summary>
        /// <param name="request">The message request</param>
        /// <returns>The message response</returns>
        public GetRepositoryInfoResponse GetRepositoryInfo(GetRepositoryInfoRequest request)
        {
            GetRepositoryInfoResponse response = new GetRepositoryInfoResponse();
            try
            {
                // create a dictionary of our request parameters
                Dictionary<String, String> parameters = new Dictionary<String, String>();
                foreach (RepositoryParameterDc parameter in request.Parameters)
                {
                    parameters.Add(parameter.Name, parameter.Value);
                }

                // now retrieve the repository information using the parameters
                response.RepositoryInfo = Mapper.Map<RepositoryInfo, RepositoryInfoDc>(
                    RepositoryInfo.Fetch(parameters),
                    new RepositoryInfoDc());
            }
            catch (Exception ex)
            {
                this.ThrowException(ex);
            }
            return response;
        }
        #endregion

        #region RegisterRepository
        /// <summary>
        /// Registers an external repository with this repository
        /// </summary>
        /// <param name="request">The message request</param>
        /// <returns>The message response</returns>
        public RegisterRepositoryResponse RegisterRepository(RegisterRepositoryRequest request)
        {
            RegisterRepositoryResponse response = new RegisterRepositoryResponse();
            try
            {
                // NF - TODO
            }
            catch (Exception ex)
            {
                this.ThrowException(ex);
            }
            return response;
        }
        #endregion

        #region GetNextRepositoryItem
        /// <summary>
        /// Returns the next repository item that requires synchronisation
        /// </summary>
        /// <param name="request">The message request</param>
        /// <returns>The message response</returns>
        public GetNextRepositoryItemResponse GetNextRepositoryItem(GetNextRepositoryItemRequest request)
        {
            GetNextRepositoryItemResponse response = new GetNextRepositoryItemResponse();
            try
            {
                // NF - TODO
            }
            catch (Exception ex)
            {
                this.ThrowException(ex);
            }
            return response;
        }
        #endregion

        #region GetRepositoryPlanogram
        /// <summary>
        /// Returns the specified repository planogram
        /// </summary>
        /// <param name="request">The message request</param>
        /// <returns>The message response</returns>
        public GetRepositoryPlanogramResponse GetRepositoryPlanogram(GetRepositoryPlanogramRequest request)
        {
            GetRepositoryPlanogramResponse response = new GetRepositoryPlanogramResponse();
            try
            {
                // NF - TODO
            }
            catch (Exception ex)
            {
                this.ThrowException(ex);
            }
            return response;
        }
        #endregion

        #region PublishRepositoryPlanogram
        /// <summary>
        /// Publishes a planogram to this repository
        /// </summary>
        /// <param name="request">The message request</param>
        /// <returns>The message response</returns>
        public PublishRepositoryPlanogramResponse PublishRepositoryPlanogram(PublishRepositoryPlanogramRequest request)
        {
            PublishRepositoryPlanogramResponse response = new PublishRepositoryPlanogramResponse();
            try
            {
                // NF - TODO
            }
            catch (Exception ex)
            {
                this.ThrowException(ex);
            }
            return response;
        }
        #endregion

        #endregion
    }
}
