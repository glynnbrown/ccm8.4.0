﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26336 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Concurrent;

namespace Galleria.Ccm.Services
{
    /// <summary>
    /// Static service container used
    /// to hold
    /// </summary>
    public static class ServiceContainer
    {
        #region Fields
        private static ConcurrentDictionary<Type, Object> _services = new ConcurrentDictionary<Type, Object>();
        #endregion

        #region Methods
        /// <summary>
        /// Returns an instance of a service
        /// that implements the specified interface
        /// </summary>
        public static TInterface GetService<TInterface>()
        {
            Object service;

            if (!_services.TryGetValue(typeof(TInterface), out service))
            {
                throw new InvalidOperationException("A service is not registered for the specified interface");
            }

            if (service is Type)
            {
                //if only a service type was registered
                // return an instance of the service
                return (TInterface)Activator.CreateInstance((Type)service);
            }
            else
            {
                //otherwise return the service object itself.
                return (TInterface)service;
            }
        }

        /// <summary>
        /// Registeres a service within this container
        /// </summary>
        public static void RegisterService<TInterface, TService>()
            where TService : class
        {
            RegisterService(typeof(TInterface), typeof(TService));
        }

        /// <summary>
        /// Registers a service within this container
        /// </summary>
        public static void RegisterService(Type interfaceType, Type serviceType)
        {
            _services.AddOrUpdate(interfaceType, serviceType, (k, v) => serviceType);
        }

        /// <summary>
        /// Registers a service object within this container.
        /// </summary>
        public static void RegisterServiceObject<TInterface>(TInterface serviceObject)
            where TInterface : class
        {
            _services.AddOrUpdate(typeof(TInterface), serviceObject, (k, v) => serviceObject);
        }

        /// <summary>
        /// Resets the servic container
        /// </summary>
        public static void Reset()
        {
            _services.Clear();
        }
        #endregion
    }
}
