﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26336 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Concurrent;
using System.ServiceModel;

namespace Galleria.Ccm.Services
{
    /// <summary>
    /// Abstract class that forms the base for a service client
    /// </summary>
    public abstract class ServiceClient : IDisposable
    {
        #region Fields
        public ConcurrentBag<IDisposable> _services = new ConcurrentBag<IDisposable>();
        #endregion

        #region Methods
        /// <summary>
        /// Creates and opens a new instance of a service
        /// that implements the specified interface
        /// </summary>
        protected TInterface OpenService<TInterface>()
            where TInterface : class
        {
            return this.OpenService<TInterface>(String.Empty);
        }

        /// <summary>
        /// Creates and opens a new instance of a service
        /// that implements the specified interface
        /// </summary>
        protected TInterface OpenService<TInterface>(String endpointAddress)
            where TInterface : class
        {
            // use the service container to create
            // a new instance of this service
            TInterface service = ServiceContainer.GetService<TInterface>();

            // determine if this service is diposable
            // and if so, hold a reference so we can
            // safely dispose of it later
            IDisposable disposable = service as IDisposable;
            if (disposable != null) _services.Add(disposable);

            // attempt to set the endpoint address and open the service
            ClientBase<TInterface> serviceBase = service as ClientBase<TInterface>;
            if (serviceBase != null)
            {
                serviceBase.Endpoint.Address = new EndpointAddress(endpointAddress);
                serviceBase.Open();
            }

            // and return the service
            return service;
        }

        /// <summary>
        /// Called when this instance is diposed
        /// </summary>
        public virtual void Dispose()
        {
            // dispose of all clients created
            // by this client
            foreach (IDisposable service in _services)
            {
                service.Dispose();
            }
        }
        #endregion
    }
}
