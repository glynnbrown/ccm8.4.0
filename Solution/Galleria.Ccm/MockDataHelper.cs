﻿using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;


namespace Galleria.Ccm
{

    /// <summary>
    /// TEMPORARY helper for passing around some 
    /// fake data until the backend gets implemented.
    /// </summary>
    public static class MockDataHelper
    {

        #region ViewLayout

        private static List<ViewLayoutDto> _viewLayoutDtos = new List<ViewLayoutDto>();
        private static List<ViewLayoutItemDto> _viewLayoutItemDtos = new List<ViewLayoutItemDto>();

        public static ViewLayoutDto FetchViewLayoutById(Int32 id)
        {
            return _viewLayoutDtos.FirstOrDefault(f => f.Id == id);
        }

        public static void InsertViewLayout(ViewLayoutDto dto)
        {
            Int32 nextId = 1;
            if (_viewLayoutDtos.Count > 0)
            {
                nextId = _viewLayoutDtos.Max(v => v.Id) + 1;
            }

            dto.Id = nextId;

            _viewLayoutDtos.Add(dto);
        }

        public static void UpdateViewLayout(ViewLayoutDto dto)
        {
            ViewLayoutDto old = FetchViewLayoutById(dto.Id);
            _viewLayoutDtos.Remove(old);

            _viewLayoutDtos.Add(dto);
        }

        public static void DeleteViewLayout(Int32 id)
        {
            ViewLayoutDto old = FetchViewLayoutById(id);
            _viewLayoutDtos.Remove(old);
        }


        public static List<ViewLayoutItemDto> FetchViewLayoutItemsByParentId(Int32 id)
        {
            List<ViewLayoutItemDto> dtoList = new List<ViewLayoutItemDto>();

            foreach (ViewLayoutItemDto dto in _viewLayoutItemDtos)
            {
                if (dto.ViewLayoutId == id)
                {
                    dtoList.Add(dto);
                }
            }

            return dtoList;
        }

        public static void InsertViewLayoutItem(ViewLayoutItemDto dto)
        {
            Int32 nextId = 1;
            if (_viewLayoutItemDtos.Count > 0)
            {
                nextId = _viewLayoutItemDtos.Max(v => v.Id) + 1;
            }

            dto.Id = nextId;

            _viewLayoutItemDtos.Add(dto);
        }

        public static void UpdateViewLayoutItem(ViewLayoutItemDto dto)
        {
            ViewLayoutItemDto old = _viewLayoutItemDtos.FirstOrDefault(d => d.Id == dto.Id);
            _viewLayoutItemDtos.Remove(old);

            _viewLayoutItemDtos.Add(dto);
        }

        public static void DeleteViewLayoutItem(Int32 id)
        {
            ViewLayoutItemDto old = _viewLayoutItemDtos.FirstOrDefault(d => d.Id == id);
            _viewLayoutItemDtos.Remove(old);
        }

        public static List<ViewLayoutInfoDto> FetchViewLayoutInfos()
        {
            List<ViewLayoutInfoDto> dtoList = new List<ViewLayoutInfoDto>();

            if (_viewLayoutDtos.Count == 0)
            {
                //add some mock data
                AddViewLayoutTempData();
            }


            foreach (ViewLayoutDto dto in _viewLayoutDtos)
            {
                dtoList.Add(
                    new ViewLayoutInfoDto()
                    {
                        Id = dto.Id,
                        Name = dto.Name
                    });
            }

            return dtoList;
        }

        private static void AddViewLayoutTempData()
        {
            #region Layout 1
            {
                ViewLayoutDto layout = new ViewLayoutDto();
                layout.Name = "Front and Lists";
                layout.IsVertical = true;
                InsertViewLayout(layout);

                //3d view on top
                ViewLayoutItemDto view = new ViewLayoutItemDto();
                view.Order = 0;
                view.IsNewPane = true;
                view.DocumentType = (Byte)DocumentType.Front;
                view.ViewLayoutId = layout.Id;
                InsertViewLayoutItem(view);

                //horiz group beneath
                ViewLayoutItemDto listsGroup = new ViewLayoutItemDto();
                listsGroup.IsVertical = false;
                listsGroup.Order = 1;
                listsGroup.ViewLayoutId = layout.Id;
                InsertViewLayoutItem(listsGroup);


                //fixture list
                ViewLayoutItemDto fixtureView = new ViewLayoutItemDto();
                fixtureView.Order = 0;
                fixtureView.IsNewPane = true;
                fixtureView.DocumentType = (Byte)DocumentType.FixtureList;
                fixtureView.ViewLayoutId = layout.Id;
                fixtureView.ParentViewLayoutItemId = listsGroup.Id;
                InsertViewLayoutItem(fixtureView);


                //product list
                ViewLayoutItemDto positionsView = new ViewLayoutItemDto();
                positionsView.Order = 1;
                positionsView.IsNewPane = false;
                positionsView.DocumentType = (Byte)DocumentType.ProductList;
                positionsView.ViewLayoutId = layout.Id;
                positionsView.ParentViewLayoutItemId = listsGroup.Id;
                InsertViewLayoutItem(positionsView);
            }
            #endregion

            #region Layout 2
            {
                ViewLayoutDto layout = new ViewLayoutDto();
                layout.Name = "3D and Fixture";
                layout.IsVertical = true;
                InsertViewLayout(layout);

                //3d view on top
                ViewLayoutItemDto view = new ViewLayoutItemDto();
                view.Order = 0;
                view.IsNewPane = true;
                view.DocumentType = (Byte)DocumentType.Perspective;
                view.ViewLayoutId = layout.Id;
                InsertViewLayoutItem(view);

                //fixture list on bottom
                ViewLayoutItemDto fixture = new ViewLayoutItemDto();
                fixture.Order = 1;
                fixture.IsNewPane = true;
                fixture.DocumentType = (Byte)DocumentType.FixtureList;
                fixture.ViewLayoutId = layout.Id;
                InsertViewLayoutItem(fixture);
            }
            #endregion

            #region Layout 3
            {
                ViewLayoutDto layout = new ViewLayoutDto();
                layout.Name = "Front and Products";
                layout.IsVertical = false;
                InsertViewLayout(layout);

                //front view to left
                ViewLayoutItemDto view = new ViewLayoutItemDto();
                view.Order = 0;
                view.IsNewPane = true;
                view.DocumentType = (Byte)DocumentType.Front;
                view.ViewLayoutId = layout.Id;
                InsertViewLayoutItem(view);

                //product list to right
                ViewLayoutItemDto fixture = new ViewLayoutItemDto();
                fixture.Order = 1;
                fixture.IsNewPane = true;
                fixture.DocumentType = (Byte)DocumentType.ProductList;
                fixture.ViewLayoutId = layout.Id;
                InsertViewLayoutItem(fixture);
            }
            #endregion

            #region Layout 4
            {
                ViewLayoutDto layout = new ViewLayoutDto();
                layout.Name = "New Plan Default";
                layout.IsVertical = false;
                InsertViewLayout(layout);

                ViewLayoutItemDto leftGroup = new ViewLayoutItemDto();
                leftGroup.Order = 0;
                leftGroup.IsNewPane = true;
                leftGroup.IsVertical = true;
                leftGroup.ViewLayoutId = layout.Id;
                InsertViewLayoutItem(leftGroup);

                ViewLayoutItemDto orthFrontView = new ViewLayoutItemDto();
                orthFrontView.Order = 0;
                orthFrontView.IsNewPane = true;
                orthFrontView.DocumentType = (Byte)DocumentType.Design;
                orthFrontView.ViewLayoutId = layout.Id;
                orthFrontView.ParentViewLayoutItemId = leftGroup.Id;
                InsertViewLayoutItem(orthFrontView);

                //ViewLayoutItemDto rightGroup = new ViewLayoutItemDto();
                //rightGroup.Order = 1;
                //rightGroup.IsNewPane = true;
                //rightGroup.IsVertical = true;
                //rightGroup.ViewLayoutId = layout.Id;
                //InsertViewLayoutItem(rightGroup);

                //ViewLayoutItemDto productsView = new ViewLayoutItemDto();
                //productsView.Order = 0;
                //productsView.IsNewPane = true;
                //productsView.DocumentType = (Byte)DocumentType.ProductList;
                //productsView.ViewLayoutId = layout.Id;
                //productsView.ParentViewLayoutItemId = rightGroup.Id;
                //InsertViewLayoutItem(productsView);

            }
            #endregion

            #region Layout 5
            {
                ViewLayoutDto layout = new ViewLayoutDto();
                layout.Name = "Open Plan Default";
                layout.IsVertical = false;
                InsertViewLayout(layout);

                //front view
                ViewLayoutItemDto view = new ViewLayoutItemDto();
                view.Order = 0;
                view.IsNewPane = true;
                view.DocumentType = (Byte)DocumentType.Design;
                view.ViewLayoutId = layout.Id;
                InsertViewLayoutItem(view);

                //product list
                ViewLayoutItemDto fixture = new ViewLayoutItemDto();
                fixture.Order = 1;
                fixture.IsNewPane = false;
                fixture.DocumentType = (Byte)DocumentType.ProductList;
                fixture.ViewLayoutId = layout.Id;
                InsertViewLayoutItem(fixture);

            }
            #endregion

        }





        //private static List<ViewLayout> _viewLayoutList;

        //internal static List<ViewLayout> FetchViewLayoutList()
        //{
        //    if (_viewLayoutList == null)
        //    {
        //        _viewLayoutList = CreateViewLayoutTempData();
        //    }
        //    return _viewLayoutList.ToList();
        //}

        //private static List<ViewLayout> CreateViewLayoutTempData()
        //{
        //    List<ViewLayout> returnList = new List<ViewLayout>();

        //    ViewLayout layout = CreateViewLayout1();
        //    layout.Id = 1;
        //    returnList.Add(layout);

        //    layout = CreateViewLayout2();
        //    layout.Id = 2;
        //    returnList.Add(layout);

        //    layout = CreateViewLayout3();
        //    layout.Id = 3;
        //    returnList.Add(layout);

        //    layout = CreateDefaultNewPlanLayout();
        //    layout.Id = 4;
        //    returnList.Add(layout);

        //    layout = CreateDefaultOpenPlanLayout();
        //    layout.Id = 5;
        //    returnList.Add(layout);

        //    return returnList;
        //}
        ///// <summary>
        ///// Create layout with 3d, fixture and product list views
        ///// </summary>
        ///// <returns></returns>
        //private static ViewLayout CreateViewLayout1()
        //{
        //    ViewLayout layout = new ViewLayout();
        //    layout.Name = "Front and Lists";
        //    layout.IsVertical = true;


        //    //3d view on top
        //    ViewLayoutItem view = new ViewLayoutItem();
        //    view.OrderNum = 0;
        //    view.IsDocument = true;
        //    view.IsNewInPane = true;
        //    view.DocumentType = PlanDocumentType.OrthFront;
        //    layout.Items.Add(view);

        //    //horiz group beneath
        //    ViewLayoutItem listsGroup = new ViewLayoutItem();
        //    listsGroup.IsVertical = false;
        //    listsGroup.OrderNum = 1;
        //    layout.Items.Add(listsGroup);


        //    //fixture list
        //    ViewLayoutItem fixtureView = new ViewLayoutItem();
        //    fixtureView.OrderNum = 0;
        //    fixtureView.IsDocument = true;
        //    fixtureView.IsNewInPane = true;
        //    fixtureView.DocumentType = PlanDocumentType.FixtureList;
        //    listsGroup.Items.Add(fixtureView);

        //    //product list
        //    ViewLayoutItem positionsView = new ViewLayoutItem();
        //    positionsView.OrderNum = 1;
        //    positionsView.IsDocument = true;
        //    positionsView.IsNewInPane = false;
        //    positionsView.DocumentType = PlanDocumentType.ProductList;
        //    listsGroup.Items.Add(positionsView);


        //    return layout;
        //}

        //private static ViewLayout CreateViewLayout2()
        //{
        //    ViewLayout layout = new ViewLayout();
        //    layout.Name = "3D and Fixture";
        //    layout.IsVertical = true;

        //    //3d view on top
        //    ViewLayoutItem view = new ViewLayoutItem();
        //    view.OrderNum = 0;
        //    view.IsDocument = true;
        //    view.IsNewInPane = true;
        //    view.DocumentType = PlanDocumentType.ThreeDimensional;
        //    layout.Items.Add(view);

        //    //fixture list on bottom
        //    ViewLayoutItem fixture = new ViewLayoutItem();
        //    fixture.OrderNum = 1;
        //    fixture.IsDocument = true;
        //    fixture.IsNewInPane = true;
        //    fixture.DocumentType = PlanDocumentType.FixtureList;
        //    layout.Items.Add(fixture);

        //    return layout;
        //}

        //private static ViewLayout CreateViewLayout3()
        //{
        //    ViewLayout layout = new ViewLayout();
        //    layout.Name = "Front and Products";
        //    layout.IsVertical = false;

        //    //front view to left
        //    ViewLayoutItem view = new ViewLayoutItem();
        //    view.OrderNum = 0;
        //    view.IsDocument = true;
        //    view.IsNewInPane = true;
        //    view.DocumentType = PlanDocumentType.OrthFront;
        //    layout.Items.Add(view);

        //    //product list to right
        //    ViewLayoutItem fixture = new ViewLayoutItem();
        //    fixture.OrderNum = 1;
        //    fixture.IsDocument = true;
        //    fixture.IsNewInPane = true;
        //    fixture.DocumentType = PlanDocumentType.ProductList;
        //    layout.Items.Add(fixture);

        //    return layout;
        //}

        //private static ViewLayout CreateDefaultNewPlanLayout()
        //{
        //    ViewLayout layout = new ViewLayout();
        //    layout.Name = "New Plan Default";
        //    layout.IsVertical = false;

        //    ViewLayoutItem leftGroup = new ViewLayoutItem();
        //    leftGroup.OrderNum = 0;
        //    leftGroup.IsDocument = false;
        //    leftGroup.IsNewInPane = true;
        //    leftGroup.IsVertical = true;
        //    layout.Items.Add(leftGroup);

        //    ViewLayoutItem orthFrontView = new ViewLayoutItem();
        //    orthFrontView.OrderNum = 0;
        //    orthFrontView.IsNewInPane = true;
        //    orthFrontView.IsDocument = true;
        //    orthFrontView.DocumentType = PlanDocumentType.Design;
        //    leftGroup.Items.Add(orthFrontView);

        //    ViewLayoutItem rightGroup = new ViewLayoutItem();
        //    rightGroup.OrderNum = 1;
        //    rightGroup.IsDocument = false;
        //    rightGroup.IsNewInPane = true;
        //    rightGroup.IsVertical = true;
        //    layout.Items.Add(rightGroup);

        //    ViewLayoutItem productsView = new ViewLayoutItem();
        //    productsView.OrderNum = 0;
        //    productsView.IsNewInPane = true;
        //    productsView.IsDocument = true;
        //    productsView.DocumentType = PlanDocumentType.ProductList;
        //    rightGroup.Items.Add(productsView);



        //    //ViewLayoutItem orthFrontView = new ViewLayoutItem();
        //    //orthFrontView.OrderNum = 0;
        //    //orthFrontView.IsNewInPane = true;
        //    //orthFrontView.IsDocument = true;
        //    //orthFrontView.DocumentType = PlanDocumentType.OrthFront;
        //    //leftGroup.Items.Add(orthFrontView);

        //    //ViewLayoutItem orthTopView = new ViewLayoutItem();
        //    //orthTopView.OrderNum = 1;
        //    //orthTopView.IsNewInPane = false;
        //    //orthTopView.IsDocument = true;
        //    //orthTopView.DocumentType = PlanDocumentType.OrthTop;
        //    //leftGroup.Items.Add(orthTopView);

        //    //ViewLayoutItem orthLeftView = new ViewLayoutItem();
        //    //orthLeftView.OrderNum = 2;
        //    //orthLeftView.IsNewInPane = false;
        //    //orthLeftView.IsDocument = true;
        //    //orthLeftView.DocumentType = PlanDocumentType.OrthLeft;
        //    //leftGroup.Items.Add(orthLeftView);

        //    //ViewLayoutItem orthRightView = new ViewLayoutItem();
        //    //orthRightView.OrderNum =3;
        //    //orthRightView.IsNewInPane = false;
        //    //orthRightView.IsDocument = true;
        //    //orthRightView.DocumentType = PlanDocumentType.OrthRight;
        //    //leftGroup.Items.Add(orthRightView);


        //    return layout;
        //}

        //private static ViewLayout CreateDefaultOpenPlanLayout()
        //{
        //    ViewLayout layout = new ViewLayout();
        //    layout.Name = "Open Plan Default";
        //    layout.IsVertical = false;

        //    //front view
        //    ViewLayoutItem view = new ViewLayoutItem();
        //    view.OrderNum = 0;
        //    view.IsDocument = true;
        //    view.IsNewInPane = true;
        //    view.DocumentType = PlanDocumentType.Design;
        //    layout.Items.Add(view);


        //    ////3e view
        //    //ViewLayoutItem view3d = new ViewLayoutItem();
        //    //view3d.OrderNum = 1;
        //    //view3d.IsDocument = true;
        //    //view3d.IsNewInPane = true;
        //    //view3d.DocumentType = PlanDocumentType.ThreeDimensional;
        //    //layout.Items.Add(view3d);

        //    //product list
        //    ViewLayoutItem fixture = new ViewLayoutItem();
        //    fixture.OrderNum = 1;
        //    fixture.IsDocument = true;
        //    fixture.IsNewInPane = false;
        //    fixture.DocumentType = PlanDocumentType.ProductList;
        //    layout.Items.Add(fixture);



        //    return layout;
        //}

        #endregion

        #region Product

        private static List<ProductDto> _productDtos = new List<ProductDto>();

        public static List<ProductInfoDto> FetchProductInfosBySearchCriteria(String searchCriteria)
        {
            List<ProductInfoDto> dtoList = new List<ProductInfoDto>();

            if (_productDtos.Count == 0)
            {
                AddMockProducts();
            }

            Boolean noCriteria = String.IsNullOrEmpty(searchCriteria);
            String criteria = searchCriteria.ToLowerInvariant();

            foreach (ProductDto dto in _productDtos)
            {
                if (noCriteria
                    || (!String.IsNullOrEmpty(dto.Gtin) && dto.Gtin.ToLowerInvariant().Contains(criteria))
                    || (!String.IsNullOrEmpty(dto.Name) && dto.Name.ToLowerInvariant().Contains(criteria)))
                {

                    dtoList.Add(
                        new ProductInfoDto()
                        {
                            Id = dto.Id,
                            Gtin = dto.Gtin,
                            Name = dto.Name
                        });
                }
            }


            return dtoList;
        }

        public static ProductDto FetchProductById(Int32 id)
        {
            return _productDtos.FirstOrDefault(d => d.Id == id);
        }

        public static void InsertProductDto(ProductDto dto)
        {
            Int32 nextId = 1;
            if (_productDtos.Count > 0)
            {
                nextId = _productDtos.Max(d => d.Id) + 1;
            }

            dto.Id = nextId;

            _productDtos.Add(dto);
        }

        private static void AddMockProducts()
        {
            List<ProductDto> dtoList = new List<ProductDto>();

            String productFile = @"./UserData/ProductData.csv";
            if (System.IO.File.Exists(productFile))
            {
                String[] lines = System.IO.File.ReadAllLines(productFile);
                Dictionary<Int32, String> propertyIdx = new Dictionary<Int32, String>();

                String[] headerLine = lines[0].Split(',');
                for (Int32 i = 0; i < headerLine.Count(); i++)
                {
                    propertyIdx[i] = headerLine[i];
                }

                for (Int32 lineIdx = 1; lineIdx < Math.Min(lines.Count(), 1000); lineIdx++)
                {
                    String[] lineArr = lines[lineIdx].Split(',');
                    
                    if (lineArr.Count() != headerLine.Count())
                    {
                        continue;
                    }

                    try
                    {

                        ProductDto prod = new ProductDto();
                        for (Int32 i = 0; i < lineArr.Count(); i++)
                        {
                            String propertyName = propertyIdx[i];
                            String val = lineArr[i];

                            if (!String.IsNullOrEmpty(val))
                            {
                                switch (propertyName)
                                {
                                    case "Product_Id": prod.Id = Convert.ToInt32(val); break;
                                    case "Product_GTIN": prod.Gtin = Convert.ToString(val); break;
                                    case "Product_Name": prod.Name = Convert.ToString(val); break;
                                    case "Product_Height": prod.Height = Convert.ToSingle(val); break;
                                    case "Product_Width": prod.Width = Convert.ToSingle(val); break;
                                    case "Product_Depth": prod.Depth = Convert.ToSingle(val); break;
                                    case "Product_DisplayHeight": prod.DisplayHeight = Convert.ToSingle(val); break;
                                    case "Product_Brand": prod.Brand = Convert.ToString(val); break;
                                    case "Product_DisplayWidth": prod.DisplayWidth = Convert.ToSingle(val); break;
                                    case "Product_DisplayDepth": prod.DisplayDepth = Convert.ToSingle(val); break;
                                    case "Product_AlternateHeight": prod.AlternateHeight = Convert.ToSingle(val); break;
                                    case "Product_AlternateWidth": prod.AlternateWidth = Convert.ToSingle(val); break;
                                    case "Product_AlternateDepth": prod.AlternateDepth = Convert.ToSingle(val); break;
                                    case "Product_PointOfPurchaseHeight": prod.PointOfPurchaseHeight = Convert.ToSingle(val); break;
                                    case "Product_PointOfPurchaseWidth": prod.PointOfPurchaseWidth = Convert.ToSingle(val); break;
                                    case "Product_PointOfPurchaseDepth": prod.PointOfPurchaseDepth = Convert.ToSingle(val); break;
                                    case "Product_NumberOfPegHoles": prod.NumberOfPegHoles = Convert.ToByte(val); break;
                                    case "Product_PegX": prod.PegX = Convert.ToSingle(val); break;
                                    case "Product_PegX2": prod.PegX2 = Convert.ToSingle(val); break;
                                    case "Product_PegX3": prod.PegX3 = Convert.ToSingle(val); break;
                                    case "Product_PegY": prod.PegY = Convert.ToSingle(val); break;
                                    case "Product_PegY2": prod.PegY2 = Convert.ToSingle(val); break;
                                    case "Product_PegProngOffset": prod.PegProngOffset = Convert.ToSingle(val); break;
                                    case "Product_PegDepth": prod.PegDepth = Convert.ToSingle(val); break;
                                    case "Product_SqueezeHeight": prod.SqueezeHeight = Convert.ToSingle(val); break;
                                    case "Product_SqueezeWidth": prod.SqueezeWidth = Convert.ToSingle(val); break;
                                    case "Product_SqueezeDepth": prod.SqueezeDepth = Convert.ToSingle(val); break;
                                    case "Product_NestingHeight": prod.NestingHeight = Convert.ToSingle(val); break;
                                    case "Product_NestingWidth": prod.NestingWidth = Convert.ToSingle(val); break;
                                    case "Product_NestingDepth": prod.NestingDepth = Convert.ToSingle(val); break;
                                    case "Product_CasePackUnits": prod.CasePackUnits = Convert.ToInt16(val); break;
                                    case "Product_CaseHigh": prod.CaseHigh = Convert.ToByte(val); break;
                                    case "Product_CaseWide": prod.CaseWide = Convert.ToByte(val); break;
                                    case "Product_CaseDeep": prod.CaseDeep = Convert.ToByte(val); break;
                                    case "Product_CaseHeight": prod.CaseHeight = Convert.ToSingle(val); break;
                                    case "Product_CaseWidth": prod.CaseWidth = Convert.ToSingle(val); break;
                                    case "Product_CaseDepth": prod.CaseDepth = Convert.ToSingle(val); break;
                                    case "Product_MaxStack": prod.MaxStack = Convert.ToByte(val); break;
                                    case "Product_MaxTopCap": prod.MaxTopCap = Convert.ToByte(val); break;
                                    case "Product_MaxRightCap": prod.MaxRightCap = Convert.ToByte(val); break;
                                    case "Product_MinDeep": prod.MinDeep = Convert.ToByte(val); break;
                                    case "Product_MaxDeep": prod.MaxDeep = Convert.ToByte(val); break;
                                    case "Product_MaxNestingHigh": prod.MaxNestingHigh = Convert.ToByte(val); break;
                                    case "Product_MaxNestingDeep": prod.MaxNestingDeep = Convert.ToByte(val); break;
                                    case "Product_TrayPackUnits": prod.TrayPackUnits = Convert.ToInt16(val); break;
                                    case "Product_TrayHigh": prod.TrayHigh = Convert.ToByte(val); break;
                                    case "Product_TrayWide": prod.TrayWide = Convert.ToByte(val); break;
                                    case "Product_TrayDeep": prod.TrayDeep = Convert.ToByte(val); break;
                                    case "Product_TrayHeight": prod.TrayHeight = Convert.ToSingle(val); break;
                                    case "Product_TrayWidth": prod.TrayWidth = Convert.ToSingle(val); break;
                                    case "Product_TrayDepth": prod.TrayDepth = Convert.ToSingle(val); break;
                                    case "Product_TrayThickHeight": prod.TrayThickHeight = Convert.ToSingle(val); break;
                                    case "Product_TrayThickWidth": prod.TrayThickWidth = Convert.ToSingle(val); break;
                                    case "Product_TrayThickDepth": prod.TrayThickDepth = Convert.ToSingle(val); break;
                                    case "Product_FrontOverhang": prod.FrontOverhang = Convert.ToSingle(val); break;
                                    case "Product_FingerSpaceAbove": prod.FingerSpaceAbove = Convert.ToSingle(val); break;
                                    case "Product_FingerSpaceToTheSide": prod.FingerSpaceToTheSide = Convert.ToSingle(val); break;
                                    case "Product_StatusType": prod.StatusType = Convert.ToByte(val); break;
                                    case "Product_OrientationType": prod.OrientationType = Convert.ToByte(val); break;
                                    case "Product_MerchandisingStyle": prod.MerchandisingStyle = Convert.ToByte(lineArr[61]); break;
                                    case "Product_IsFrontOnly": prod.IsFrontOnly = Convert.ToBoolean(Convert.ToByte(val)); break;
                                    case "Product_IsTrayProduct": prod.IsTrayProduct = Convert.ToBoolean(Convert.ToByte(val)); break;
                                    case "Product_IsPlaceHolderProduct": prod.IsPlaceHolderProduct = Convert.ToBoolean(Convert.ToByte(val)); break;
                                    case "Product_IsActive": prod.IsActive = Convert.ToBoolean(Convert.ToByte(val)); break;
                                    case "Product_CanBreakTrayUp": prod.CanBreakTrayUp = Convert.ToBoolean(Convert.ToByte(val)); break;
                                    case "Product_CanBreakTrayDown": prod.CanBreakTrayDown = Convert.ToBoolean(Convert.ToByte(val)); break;
                                    case "Product_CanBreakTrayBack": prod.CanBreakTrayBack = Convert.ToBoolean(Convert.ToByte(val)); break;
                                    case "Product_CanBreakTrayTop": prod.CanBreakTrayTop = Convert.ToBoolean(Convert.ToByte(val)); break;
                                    case "Product_ForceMiddleCap": prod.ForceMiddleCap = Convert.ToBoolean(Convert.ToByte(val)); break;
                                    case "Product_ForceBottomCap": prod.ForceBottomCap = Convert.ToBoolean(Convert.ToByte(val)); break;

                                }
                            }
                        }

                        if (prod.SqueezeHeight > 0)
                        {
                            prod.SqueezeHeight = prod.Height * prod.SqueezeHeight;
                        }
                        if (prod.SqueezeWidth > 0)
                        {
                            prod.SqueezeWidth = prod.Width * prod.SqueezeWidth;
                        }
                        if (prod.SqueezeDepth > 0)
                        {
                            prod.SqueezeDepth = prod.Depth * prod.SqueezeDepth;
                        }

                        if (!dtoList.Any(d => String.Compare(d.Gtin, prod.Gtin, true) == 0))
                        {
                            InsertProductDto(prod);
                            dtoList.Add(prod);
                        }
                    }
                    catch (Exception) 
                    { 
                    }
                }

            }


        }

        #endregion

        #region LabelSetting
        //TODO: Remove?
        //const String _labelSettingsPath = @".\UserData\LabelSettings.xml";

        //public static List<LabelSettingDto> FetchLabelSettings()
        //{
        //    String filePath = _labelSettingsPath;

        //    List<LabelSettingDto> list = new List<LabelSettingDto>();

        //    if (File.Exists(filePath))
        //    {
        //        using (XmlReader reader = XmlReader.Create(filePath))
        //        {
        //            Type dtoType = typeof(LabelSettingDto);
        //            XmlSerializer serializer = new XmlSerializer(dtoType);

        //            while (reader.Read())
        //            {
        //                if (reader.Name == "Settings")
        //                {
        //                    while (reader.Read())
        //                    {
        //                        if (reader.Name == dtoType.Name)
        //                        {
        //                            LabelSettingDto dto = serializer.Deserialize(reader) as LabelSettingDto;
        //                            while (dto != null)
        //                            {
        //                                list.Add(dto);

        //                                //deserialise next
        //                                dto = serializer.Deserialize(reader) as LabelSettingDto;
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    else
        //    {
        //        //create default data
        //        list.Add(
        //            new LabelSettingDto()
        //            {
        //                Name = "Standard",
        //                Type = 0,
        //                BackgroundColour = 2063597567,
        //                BorderColour = -16777216,
        //                Text = "<Product.Name>",
        //                TextColour = -16777088,
        //                FontSize = 8,
        //                Font = "Arial",
        //                IsShrinkToFitOn = true,
        //                IsRotateToFitOn = true,
        //            });
        //    }

        //    return list;
        //}

        //public static void InsertLabelSetting(LabelSettingDto dto)
        //{
        //    List<LabelSettingDto> list = FetchLabelSettings();

        //    Int32 newId = 1;
        //    if (list.Count > 0)
        //    {
        //        newId = list.Max(d => d.Id) + 1;
        //    }

        //    dto.Id = 1;
        //    list.Add(dto);

        //    SaveLabelSettings(list);
        //}

        //public static void UpdateLabelSetting(LabelSettingDto dto)
        //{
        //    List<LabelSettingDto> list = FetchLabelSettings();

        //    LabelSettingDto removeDto = list.FirstOrDefault(d => d.Id == dto.Id);
        //    Int32 idx = list.IndexOf(removeDto);
        //    list.Remove(removeDto);

        //    list.Insert(idx, dto);

        //    SaveLabelSettings(list);
        //}

        //public static void DeleteLabelSettingById(Int32 id)
        //{
        //    List<LabelSettingDto> list = FetchLabelSettings();

        //    LabelSettingDto removeDto = list.FirstOrDefault(d => d.Id == id);
        //    list.Remove(removeDto);

        //    SaveLabelSettings(list);
        //}

        //private static void SaveLabelSettings(List<LabelSettingDto> dtoList)
        //{
        //    String filePath = _labelSettingsPath;

        //    if (File.Exists(filePath))
        //    {
        //        File.Delete(filePath);
        //    }


        //    using (XmlWriter writer = XmlWriter.Create(filePath))
        //    {
        //        writer.WriteStartDocument();
        //        writer.WriteStartElement("Settings");

        //        foreach (LabelSettingDto dto in dtoList)
        //        {
        //            XmlSerializer serializer = new XmlSerializer(typeof(LabelSettingDto));
        //            serializer.Serialize(writer, dto);
        //        }


        //        writer.WriteEndElement();
        //        writer.WriteEndDocument();
        //        writer.Close();
        //    }
        //}

        #endregion

    }


}
