﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM 8.0)
//// V8-25556 : D.Pleasance
////  Created
//#endregion
//#endregion

//using System;
//using System.Diagnostics;
//using System.Reflection;

//using Gibraltar.Agent;
//using Gibraltar.Agent.Metrics;

//namespace Galleria.Ccm.Logging
//{
//    [EventMetric("Galleria", "Galleria.StrategicAssortment", "Dal Commands", Caption = "Dal Commands", Description = "Performance information for each Dal command")]
//    public class DalCommandPerformanceMetric : IDisposable
//    {
//        #region Fields
//        private Stopwatch _stopWatch = new Stopwatch(); // a stopwatch used to record the duration of the process
//        private string _dal;
//        private string _commandText; // the command text
//        #endregion

//        #region Constructors
//        /// <summary>
//        /// Creates a new instance of this type
//        /// </summary>
//        public DalCommandPerformanceMetric(string dal, string commandText)
//        {
//            _dal = dal;
//            _commandText = commandText;
//            // ISO-13065: By truncating SQL strings, we make it more likely that SQL commands that basically do
//            // the same thing (e.g. insert data into a table not using a stored procedure), but using different
//            // values, still get grouped together.
//            if (_commandText.Length > 200)
//            {
//                _commandText = _commandText.Substring(0, 200);
//            }
//#if DEBUG
//            _stopWatch.Start();
//#endif
//        }
//        #endregion

//        #region Properties
//        /// <summary>
//        /// The command text (metric name)
//        /// </summary>
//        [EventMetricValue("name", SummaryFunction.Count, null, Caption = "Command", Description = "The DAL and text of the command that has been executed")]
//        public string Command
//        {
//            get { return string.Format("{0}.{1}", _dal, _commandText); }
//        }

//        /// <summary>
//        /// The operation duration
//        /// </summary>
//        [EventMetricValue("duration", SummaryFunction.Average, "ms", Caption = "Duration", Description = "Duration of the executed operation", IsDefaultValue = true)]
//        public TimeSpan Duration
//        {
//            get { return _stopWatch.Elapsed; }
//        }
//        #endregion

//        #region Methods
//        /// <summary>
//        /// Called when this instance is being disposed
//        /// </summary>
//        public void Dispose()
//        {
//#if DEBUG
//            _stopWatch.Stop();
//            EventMetric.Write(this);
//#endif
//        }
//        #endregion
//    }
//}