﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// CCM-29010 : D.Pleasance
//  Created
#endregion
#region Version History: (CCM 8.3.0)
// V8-31831 : A.Probyn
//  Added FetchByEntityIdName
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IPlanogramNameTemplateDal : IDal
    {
        PlanogramNameTemplateDto FetchById(Object id);
        PlanogramNameTemplateDto FetchByEntityIdName(Int32 entityId, String name);
        void Insert(PlanogramNameTemplateDto dto);
        void Update(PlanogramNameTemplateDto dto);
        void DeleteById(Object id);
    }
}