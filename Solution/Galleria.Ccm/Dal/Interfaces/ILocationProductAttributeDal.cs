﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
// V8-27630 : I.George
// Removed interfaces that are not used

#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByLocationIdProductId", "LocationId", "ProductId")]
    [DefaultDeleteMethod("DeleteById", "LocationId", "ProductId")]
    public interface ILocationProductAttributeDal : IDal
    {
        void DeleteById(Int16 locationId, Int32 productId);
        void DeleteByEntityId(Int32 entityId);
        void Insert(LocationProductAttributeDto dto);
        void Update(LocationProductAttributeDto dto);
        void Upsert(IEnumerable<LocationProductAttributeDto> dtoList, LocationProductAttributeIsSetDto isSetDto);
        LocationProductAttributeDto FetchByLocationIdProductId(Int16 locationId, Int32 productId);
        IEnumerable<LocationProductAttributeDto> FetchByEntityId(Int32 entityId);
        List<LocationProductAttributeDto> FetchByLocationIdProductIdCombinations(IEnumerable<Tuple<Int16, Int32>> combinationsList);
        List<LocationProductAttributeDto> FetchByEntityIdLocationIdProductIds(Int32 entityId, Int16 locationId, IEnumerable<Int32> productIds);
    }

    public struct LocationProductAttributeIdSearchCriteria : IEquatable<LocationProductAttributeIdSearchCriteria>
    {
        public String ProductGTIN { get; set; }
        public String LocationCode { get; set; }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + this.ProductGTIN.ToLowerInvariant().GetHashCode();
                hash = hash * 23 + this.LocationCode.ToLowerInvariant().GetHashCode();
                return hash;
            }
        }

        public override Boolean Equals(object obj)
        {
            if (obj is LocationProductAttributeIdSearchCriteria)
            {
                return this.Equals((LocationProductAttributeIdSearchCriteria)obj);
            }
            return false;
        }

        public Boolean Equals(LocationProductAttributeIdSearchCriteria other)
        {
            StringComparer s = StringComparer.OrdinalIgnoreCase;

            if (s.Compare(other.ProductGTIN, this.ProductGTIN) != 0) { return false; }
            if (s.Compare(other.LocationCode, this.LocationCode) != 0) { return false; }

            return true;
        }
    }
}
