﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-24265 : N.Haywood
//		Created (Auto-generated)
// CCM-24265 : J.Pickup
//		Re-implemented and renamed from ILabelSettingDal' to 'ILabelDal'
// CCM-24265 : N.Haywood
//      Added locking and unlocking
// V8-27940 : L.Luong
//      Added FetchByEntityIdName
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface ILabelDal : IDal
    {
        //IEnumerable<LabelDto> FetchByIds(IEnumerable<Object> ids);
        //IEnumerable<LabelDto> FetchByType(LabelType labelType, IEnumerable<Object> ids);
        LabelDto FetchById(Object id);
        LabelDto FetchByEntityIdName(Int32 entityId, String name);
        void Insert(LabelDto dto);
        void Update(LabelDto dto);
        void DeleteById(Object id);

        void LockById(Object id);
        void UnlockById(Object id);
    }
}
