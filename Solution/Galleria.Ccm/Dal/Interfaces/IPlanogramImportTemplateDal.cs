﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-27804 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// Denotes the expected memebers of a PlanogramImportTemplate Dal
    /// </summary>
    public interface IPlanogramImportTemplateDal : IDal
    {
        PlanogramImportTemplateDto FetchById(Object id);
        PlanogramImportTemplateDto FetchByEntityIdName(Int32 entityId, String name);
        void Insert(PlanogramImportTemplateDto dto);
        void Update(PlanogramImportTemplateDto dto);
        void DeleteById(Object id);

        void LockById(Object id);
        void UnlockById(Object id);
    }
}
