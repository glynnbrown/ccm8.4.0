﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26159 : L.Ineson
//		Created (Auto-generated)
// CCM-27172 : I.George
//     Added FetchByEntityIdIncludingDeleted
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// Defines the expected memebers of a PerformanceSelectionInfo dal class
    /// </summary>
    public interface IPerformanceSelectionInfoDal : IDal
    {
        IEnumerable<PerformanceSelectionInfoDto> FetchByEntityId(Int32 entityId);
        IEnumerable<PerformanceSelectionInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId);
    }
}
