﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-26234 : L.Ineson
//		Copied from GFS
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IRoleInfoDal : IDal
    {
        RoleInfoDto FetchById(Int32 id);
        IEnumerable<RoleInfoDto> FetchByEntityId(Int32 entityId);
        IEnumerable<RoleInfoDto> FetchAll();
    }
}
