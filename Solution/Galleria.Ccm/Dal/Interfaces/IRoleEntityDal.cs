﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-26234 : L.Ineson
//		Copied from GFS
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchById","RoleId","EntityId")]
    public interface IRoleEntityDal : IDal
    {
        void DeleteById(Int32 Id);

        void Insert(RoleEntityDto dto);

        RoleEntityDto FetchById(Int32 roleId, Int32 entityId);

        IEnumerable<RoleEntityDto> FetchByRoleId(Int32 roleId);

        IEnumerable<RoleEntityDto> FetchByEntityId(Int32 entityId);

        IEnumerable<RoleEntityDto> FetchAll();
    }
}
