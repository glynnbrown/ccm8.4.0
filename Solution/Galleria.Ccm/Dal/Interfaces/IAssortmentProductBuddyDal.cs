﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IAssortmentProductBuddyDal : IDal
    {
        IEnumerable<AssortmentProductBuddyDto> FetchByAssortmentId(Int32 assortmentId);
        AssortmentProductBuddyDto FetchById(Int32 id);
        void Insert(AssortmentProductBuddyDto dto);
        void Update(AssortmentProductBuddyDto dto);
        void DeleteById(Int32 id);
    }
}
