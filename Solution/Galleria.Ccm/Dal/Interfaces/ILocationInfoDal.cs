﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-25628 : A.Kuszyk
//		Created (Auto-generated)
// CCM-25454 : J.Pickup
//		Added FetchByEntityIdIncludingDeleted Method interface
// V8-27918 : J.Pickup
//      Added FetchByWorkpackageIdIncludingDeleted() & FetchByWorkpackagePlanogramIdIncludingDeleted()

#endregion
#region Version History: (CCM 8.1.0)
// V8-29667 : N.Haywood
//  Removed FetchAllIncludingDeleted
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface ILocationInfoDal : IDal
    { 
        IEnumerable<LocationInfoDto> FetchByEntityId(Int32 entityId);
        IEnumerable<LocationInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId);
        IEnumerable<LocationInfoDto> FetchByWorkpackageIdIncludingDeleted(Int32 workpackageId);
        IEnumerable<LocationInfoDto> FetchByWorkpackagePlanogramIdIncludingDeleted(Int32 planogramId);
        IEnumerable<LocationInfoDto> FetchByEntityIdLocationCodes(Int32 entityId, IEnumerable<String> locationCodes);
        //IEnumerable<LocationInfoDto> FetchAllIncludingDeleted();
    }
}
