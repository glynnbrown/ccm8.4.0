﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByHighlightId", "HighlightId")]
    public interface IHighlightFilterDal : IDal
    {
        IEnumerable<HighlightFilterDto> FetchByHighlightId(Object highlightId);
        void Insert(HighlightFilterDto dto);
        void Update(HighlightFilterDto dto);
        void DeleteById(Int32 id);
    }
}
