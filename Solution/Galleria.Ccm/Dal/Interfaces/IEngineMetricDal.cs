﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IEngineMetricDal : Galleria.Framework.Engine.Dal.Interfaces.IEngineMetricDal
    {
        Boolean DeleteByTimestamp(DateTime timestamp);
    }
}
