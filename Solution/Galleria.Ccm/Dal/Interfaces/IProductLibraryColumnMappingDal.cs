﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25395 : A.Probyn
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByProductLibraryId", "ProductLibraryId")]
    public interface IProductLibraryColumnMappingDal : IDal
    {
        ProductLibraryColumnMappingDto FetchById(Int32 id);
        IEnumerable<ProductLibraryColumnMappingDto> FetchByProductLibraryId(Object productLibraryId);
        void Insert(ProductLibraryColumnMappingDto dto);
        void Update(ProductLibraryColumnMappingDto dto);
        void DeleteById(Int32 id);
    }
}
