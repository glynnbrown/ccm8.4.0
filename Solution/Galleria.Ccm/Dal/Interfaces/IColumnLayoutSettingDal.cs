﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva
//  Created.

#endregion

#endregion

using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchAll")]
    public interface IColumnLayoutSettingDal : IDal
    {
        IEnumerable<ColumnLayoutSettingDto> FetchAll();
        void Insert(ColumnLayoutSettingDto dto);
        void Update(ColumnLayoutSettingDto dto);
    }
}
