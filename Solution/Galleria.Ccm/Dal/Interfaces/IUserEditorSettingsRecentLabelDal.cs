﻿#region Header information
// Copyright © Galleria RTS Ltd 2015
#region Version History CCM830
// V8-31699 : A.Heathcote
//      Created this Interface
#endregion
#endregion
using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchAll")]
    public interface IUserEditorSettingsRecentLabelDal : IDal
    {
        IEnumerable<UserEditorSettingsRecentLabelDto> FetchAll();
        void Insert(UserEditorSettingsRecentLabelDto dto);
        void Update(UserEditorSettingsRecentLabelDto dto);
        void DeleteById(Int32 id);

    }
}
