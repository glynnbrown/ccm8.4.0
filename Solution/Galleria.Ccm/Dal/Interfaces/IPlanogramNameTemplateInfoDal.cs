﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// V8-29010 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// Denotes the expected memebers of a IPlanogramNameTemplateInfo Dal
    /// </summary>
    public interface IPlanogramNameTemplateInfoDal : IDal
    {
        IEnumerable<PlanogramNameTemplateInfoDto> FetchByEntityId(Int32 entityId);
    }
}