﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// V8-25395 : A.PRobyn
//		Created
#endregion
#region Version History: (CCM V8.3)
// V8-32361 : L.Ineson
//  Added FetchByEntityId
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByIds", "Id")]
    public interface IProductLibraryInfoDal : IDal
    {
        IEnumerable<ProductLibraryInfoDto> FetchByIds(IEnumerable<Object> ids);
        IEnumerable<ProductLibraryInfoDto> FetchByEntityId(Int32 entityId);
    }
}
