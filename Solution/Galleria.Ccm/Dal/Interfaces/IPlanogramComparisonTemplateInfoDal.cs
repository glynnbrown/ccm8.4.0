﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    ///     Interface declaring the expected behavior for a <c>Planogram Comparison Template Info</c> DAL implementation.
    /// </summary>
    /// <remarks>The default fetch method is <c>FetchByIds</c> which requires the <c>Id</c> values.</remarks>
    [DefaultFetchMethod("FetchByIds", "Id")]
    public interface IPlanogramComparisonTemplateInfoDal : IDal
    {
        /// <summary>
        ///     Fetch a collection <c>Data Transfer Object</c> from the <c>DAL</c> for <c>Planogram Comparison Template Info</c> model objects
        ///     that is associated to the given <paramref name="ids" />.
        /// </summary>
        /// <param name="ids"><see cref="Object" /> containing the ids of matching <c>Planogram Comparison Template Info</c> to fetch.</param>
        /// <returns>A collection of new instances of <see cref="PlanogramComparisonTemplateDto" /> matching the given <paramref name="ids"/>.</returns>
        IEnumerable<PlanogramComparisonTemplateInfoDto> FetchByIds(IEnumerable<Object> ids);

        /// <summary>
        ///     Fetch a collection <c>Data Transfer Object</c> from the <c>DAL</c> for <c>Planogram Comparison Template Info</c> model objects
        ///     that is associated to the given <paramref name="entityId" />.
        /// </summary>
        /// <param name="entityId"><see cref="Int32" /> containing the id of the <c>Entity</c> associated with all matching <c>Planogram Comparison Template Info</c> to fetch.</param>
        /// <returns>A collection of new instances of <see cref="PlanogramComparisonTemplateDto" /> matching the given <paramref name="entityId"/>.</returns>
        IEnumerable<PlanogramComparisonTemplateInfoDto> FetchByEntityId(Int32 entityId);

        /// <summary>
        ///     Fetch a collection <c>Data Transfer Object</c> from the <c>DAL</c> for <c>Planogram Comparison Template Info</c> model objects
        ///     that is associated to the given <paramref name="entityId" />, including the deleted ones.
        /// </summary>
        /// <param name="entityId"><see cref="Int32" /> containing the id of the <c>Entity</c> associated with all matching <c>Planogram Comparison Template Info</c> to fetch.</param>
        /// <returns>A collection of new instances of <see cref="PlanogramComparisonTemplateDto" /> matching the given <paramref name="entityId"/>.</returns>
        IEnumerable<PlanogramComparisonTemplateInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId);
    }
}