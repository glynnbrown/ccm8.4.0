﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
// V8-25453 : A.Kuszyk
//      Added FetchByEntityIdConsumerDecisionTreeIds.
// V8-27132 : A.Kuszyk
//      Added FetchByEntityIdName.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IConsumerDecisionTreeDal : IDal
    {
        ConsumerDecisionTreeDto FetchById(Int32 id);
        ConsumerDecisionTreeDto FetchByEntityIdName(Int32 entityId, String name);
        IEnumerable<ConsumerDecisionTreeDto> FetchByEntityIdConsumerDecisionTreeIds(Int32 entityId, IEnumerable<Int32> consumerDecisionTreeIds);
        void Insert(ConsumerDecisionTreeDto dto);
        void Update(ConsumerDecisionTreeDto dto);
        void DeleteById(Int32 id);
    }
}
