﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25787 : N.Foster
//  Created
#endregion
#endregion

using System.Collections.Generic;
using Galleria.Framework.Engine.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IEngineInstanceDal : Galleria.Framework.Engine.Dal.Interfaces.IEngineInstanceDal
    {
        void DeleteInactive();
        IEnumerable<EngineInstanceDto> FetchAll();
    }
}
