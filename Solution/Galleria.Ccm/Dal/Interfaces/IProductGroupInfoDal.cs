﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//		Created
// CCM-25452 : N.Haywood
//      Added FetchByProductGroupIds
#endregion

#region Version History : CCM 802
// V8-29078 : A.Kuszyk
//  Added FetchByProductGroupCodes.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IProductGroupInfoDal : IDal
    {
        /// <summary>
        /// Returns all deleted groups for the given hierarchy id.
        /// </summary>
        IEnumerable<ProductGroupInfoDto> FetchDeletedByProductHierarchyId(Int32 hierarchyId);
        IEnumerable<ProductGroupInfoDto> FetchByProductGroupIds(IEnumerable<Int32> productGroupIds);
        IEnumerable<ProductGroupInfoDto> FetchByProductGroupCodes(IEnumerable<String> productGroupCodes);
    }
}
