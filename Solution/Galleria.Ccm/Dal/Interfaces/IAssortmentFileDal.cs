﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25454 : J.Pickup
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IAssortmentFileDal : IDal
    {
        IEnumerable<AssortmentFileDto> FetchByAssortmentId(Int32 assortmentId);
        AssortmentFileDto FetchById(Int32 id);
        void Insert(AssortmentFileDto dto);
        void Update(AssortmentFileDto dto);
        void DeleteById(Int32 id);
    }
}
