﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// Denotes the expected memebers of a PlanogramImportTemplatePerformanceMetric Dal
    /// </summary>
    [DefaultFetchMethod("FetchByPlanogramExportTemplateId", "PlanogramExportTemplateId")]
    public interface IPlanogramExportTemplatePerformanceMetricDal : IDal
    {
        IEnumerable<PlanogramExportTemplatePerformanceMetricDto> FetchByPlanogramExportTemplateId(Object planogramExportTemplateId);
        void Insert(PlanogramExportTemplatePerformanceMetricDto dto);
        void Update(PlanogramExportTemplatePerformanceMetricDto dto);
        void DeleteById(Int32 id);
    }
}
