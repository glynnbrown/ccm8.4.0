﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// Denotes the expected members of a PrintTemplateComponentDetail Dal
    /// </summary>
    public interface IPrintTemplateComponentDetailDal : IDal
    {
        PrintTemplateComponentDetailDto FetchById(Int32 id);
        IEnumerable<PrintTemplateComponentDetailDto> FetchByPrintTemplateComponentId(Int32 printTemplateComponentId);
        void Insert(PrintTemplateComponentDetailDto dto);
        void Update(PrintTemplateComponentDetailDto dto);
        void DeleteById(Int32 id);
        void DeleteByPrintTemplateComponentId(Int32 printTemplateComponentId);
    }
}
