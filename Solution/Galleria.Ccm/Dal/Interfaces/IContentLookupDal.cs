﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26520 : J.Pickup
//  Created Initial Version
#endregion

#region Version History: (CCM 811)
// V8-30155 : L.Ineson
//  Added FetchByPlanogramIds
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// Interface denoting the expected members of a ContentLookupDal.
    /// </summary>
    public interface IContentLookupDal : IDal
    {
        ContentLookupDto FetchById(Int32 contentLookupId);
        ContentLookupDto FetchByPlanogramId(Int32 planogramId);
        IEnumerable<ContentLookupDto> FetchByEntityId(Int32 entityId);
        IEnumerable<ContentLookupDto> FetchByPlanogramIds(IEnumerable<Int32> planogramIds);
        void Insert(ContentLookupDto dto);
        void Update(ContentLookupDto dto);
        void DeleteById(Int32 contentLookupId);
    }
}
