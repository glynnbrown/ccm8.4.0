﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IAssortmentMinorRevisionDeListActionLocationDal : IDal
    {
        AssortmentMinorRevisionDeListActionLocationDto FetchById(Int32 id);
        IEnumerable<AssortmentMinorRevisionDeListActionLocationDto> FetchByAssortmentMinorRevisionDeListActionId(Int32 asortmentMinorRevisionDeListActionId);
        void Insert(AssortmentMinorRevisionDeListActionLocationDto dto);
        void Update(AssortmentMinorRevisionDeListActionLocationDto dto);
        void DeleteById(Int32 id);
    }
}
