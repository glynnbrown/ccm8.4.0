﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History:(CCM800)
//CCM-26158 : I.George
// Initial Version
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByEntityId", "EntityId")]
    public interface IMetricDal : IDal
    {
        void DeleteById(Int32 Id);
        void Insert(MetricDto dto);
        void Update(MetricDto dto);
        IEnumerable<MetricDto> FetchByEntityId(Int32 EntityId);
    }
}