﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25919 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByWorkflowId", "WorkflowId")]
    public interface IWorkflowTaskInfoDal : IDal
    {
        IEnumerable<WorkflowTaskInfoDto> FetchByWorkflowId(Int32 workflowId);
        IEnumerable<WorkflowTaskInfoDto> FetchByWorkpackageId(Int32 workpackageId);
    }
}
