﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630 : A.Kuszyk
//  Created (copied from SA).
// CCM-25452 : N.Haywood
//	Added UpdateProductUniverses
#endregion
#region Version History : CCM820
// V8-30840 : A.Kuszyk
//  Added FetchByEntityIdName.
#endregion
#endregion

using System;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IProductUniverseDal : IDal
    {
        ProductUniverseDto FetchById(Int32 id);
        ProductUniverseDto FetchByEntityIdName(Int32 entityId, String name);
        IEnumerable<ProductUniverseDto> FetchByEntityId(Int32 entityId);
        void Insert(ProductUniverseDto dto);
        void Update(ProductUniverseDto dto);
        void Upsert(IEnumerable<ProductUniverseDto> dtoList, ProductUniverseIsSetDto isSetDto);
        void DeleteById(Int32 id);
        void DeleteByEntityId(Int32 entityId);

        void UpdateProductUniverses(Int32 entityId);
    }

        /// <summary>
    /// Code Criteria for use when searching
    /// </summary>
    public struct ProductUniverseSearchCriteria
    {
        public Int32 ProductUniverseId { get; set; }
        public String ProductUniverseName { get; set; }
        public String ProductGroupCode { get; set; }
        public String ProductGTIN { get; set; }

        public ProductUniverseSearchCriteria(
            String productUniverseName,
            String productGroupCode,
            String productGTIN)
            : this()
        {
            ProductUniverseName = productUniverseName;
            ProductGroupCode = productGroupCode;
            ProductGTIN = productGTIN;
        }
    }
}
