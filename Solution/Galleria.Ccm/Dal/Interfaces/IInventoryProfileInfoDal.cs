﻿#region Header Information

#region Version History:(CCM800)
//CCM-26124 : I.George
// Initial Version
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IInventoryProfileInfoDal : IDal
    {
        /// <summary>
        /// Returns inventory profile info dtos
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        IEnumerable<InventoryProfileInfoDto> FetchByEntityId(Int32 entityId);
    }
}
