﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created (Auto-generated)

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByValidationTemplateGroupId", "ValidationTemplateGroupId")]
    public interface IValidationTemplateGroupMetricDal : IDal
    {
        void DeleteById(Int32 id);
        IEnumerable<ValidationTemplateGroupMetricDto> FetchByValidationTemplateGroupId(Int32 validationTemplateGroupId);
        void Insert(ValidationTemplateGroupMetricDto dto);
        void Update(ValidationTemplateGroupMetricDto dto);
    }
}