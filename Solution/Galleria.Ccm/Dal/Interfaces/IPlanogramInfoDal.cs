﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25546 : N.Foster
//  Created
//CCM-25460 : L.Ineson
//  Added FetchBySearchCriteria
// CCM-25460 : L.Ineson
//  AddedFetchByIds
// CCM-26520 : J.Pickup
//  AddedFetchByCatgeoryCode & FetchByNullCategoryCode
// CCM-27599 : J.Pickup
//  Added Entity to FetchBySearchCriteria
// V8-28493 : L.Ineson
//  Added FetchByWorkpackageIdAutomationProcessingStatus
#endregion
#region Version History: CCM801
// V8-28507 : D.Pleasance
//  Added FetchByWorkpackageIdPagingCriteria \ FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria
#endregion
#region Version History: CCM803
// V8-29606 : N.Foster
//  Removed PlanogramInfo_FetchByMetadataCalculationRequired
//  Removed PlanogramInfo_FetchByValidationCalculationRequired
#endregion
#region Version History: CCM810
// V8-29986 : A.Probyn
//  Added FetchNonDebugByCategoryCode
// V8-30079 : L.Ineson
//  Amended FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria to take a list of statuses
// V8-30213 : L.Luong
//  Added FetchNonDebugByLocationCode
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IPlanogramInfoDal : IDal
    {
        IEnumerable<PlanogramInfoDto> FetchByPlanogramGroupId(Int32 planogramGroupId);
        IEnumerable<PlanogramInfoDto> FetchByCategoryCode(String categoryCode);
        IEnumerable<PlanogramInfoDto> FetchNonDebugByCategoryCode(String categoryCode);
        IEnumerable<PlanogramInfoDto> FetchNonDebugByLocationCode(String locationCode);
        IEnumerable<PlanogramInfoDto> FetchByNullCategoryCode();
        IEnumerable<PlanogramInfoDto> FetchByWorkpackageId(Int32 workpackageId);
        IEnumerable<PlanogramInfoDto> FetchByWorkpackageIdPagingCriteria(Int32 workpackageId, Byte pageNumber, Int16 pageSize);
        IEnumerable<PlanogramInfoDto> FetchByWorkpackageIdAutomationProcessingStatus(Int32 workpackageId, Byte automationProcessingStatus);
        IEnumerable<PlanogramInfoDto> FetchByWorkpackageIdAutomationProcessingStatusPagingCriteria(Int32 workpackageId, List<Byte> statusList, Byte pageNumber, Int16 pageSize);        
        IEnumerable<PlanogramInfoDto> FetchBySearchCriteria(String name, Int32? planogramGroupId, Int32 entityId);
        IEnumerable<PlanogramInfoDto> FetchByIds(IEnumerable<Int32> idList);
    }
}
