﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchById", "Id")]
    public interface IPlanogramStatusDal : IDal
    {
        PlanogramStatusDto FetchById(Int32 id);
        void Insert(PlanogramStatusDto dto);
        void Update(PlanogramStatusDto dto);
        void DeleteById(Int32 id);
    }
}
