﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
// V8-28016 : I.George
//  Renamed the DefaultDeleteMethod name parameter
// V8-28232 : N.Foster
//  Added support for batch operations
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Made changes to improve performance of engine
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByWorkpackageId", "WorkpackageId")]
    [DefaultDeleteMethod("DeleteById", "Id")]
    public interface IWorkpackagePlanogramDal : IBatchDal<WorkpackagePlanogramDto>
    {
        IEnumerable<WorkpackagePlanogramDto> FetchByWorkpackageId(Int32 workpackageId);
        WorkpackagePlanogramDto FetchByWorkpackageIdDestinationPlanogramId(Int32 workpackageId, Int32 destinationPlanogramId);
        void Insert(WorkpackagePlanogramDto dto);
        void Update(WorkpackagePlanogramDto dto);
        void DeleteById(Int32 id);
    }
}
