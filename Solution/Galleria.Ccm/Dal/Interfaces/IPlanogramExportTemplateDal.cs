﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 830)
//V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// Denotes the expected members of a PlanogramExportTemplate Dal
    /// </summary>
    public interface IPlanogramExportTemplateDal : IDal
    {
        PlanogramExportTemplateDto FetchById(Object id);
        PlanogramExportTemplateDto FetchByEntityIdName(Int32 entityId, String name);
        void Insert(PlanogramExportTemplateDto dto);
        void Update(PlanogramExportTemplateDto dto);
        void DeleteById(Object id);

        void LockById(Object id);
        void UnlockById(Object id);
    }
}
