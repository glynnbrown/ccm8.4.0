﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25447 : N.Haywood
//  Copied over from GFS
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchById", "LocationId", "ProductId")]
    [DefaultDeleteMethod("DeleteById", "LocationId", "ProductId")]
    public interface ILocationProductIllegalDal : IDal
    {
        LocationProductIllegalDto FetchById(Int16 locationId, Int32 productId);
        IEnumerable<LocationProductIllegalDto> FetchByEntityId(Int32 entityId);
        void Insert(LocationProductIllegalDto dto);
        void Update(LocationProductIllegalDto dto);
        void Upsert(IEnumerable<LocationProductIllegalDto> dtoList);
        void DeleteById(Int16 locationId, Int32 productId);
        void DeleteByEntityId(Int32 entityId);
        List<LocationProductIllegalDto> FetchByLocationIdProductIdCombinations(IEnumerable<Tuple<Int16, Int32>> locationProductIdCombinations);
        
    }

    /// <summary>
    /// Struct used for searching with location code and product gtin as criteria
    /// </summary>
    public struct LocationProductIllegalCodeSearchCriteria : IEquatable<LocationProductIllegalCodeSearchCriteria>
    {
        public String Code { get; set; }
        public String GTIN { get; set; }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + this.Code.ToLowerInvariant().GetHashCode();
                hash = hash * 23 + this.GTIN.ToLowerInvariant().GetHashCode();
                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is LocationProductIllegalCodeSearchCriteria)
            {
                return this.Equals((LocationProductIllegalCodeSearchCriteria)obj);
            }
            return false;
        }

        public bool Equals(LocationProductIllegalCodeSearchCriteria other)
        {
            StringComparer s = StringComparer.OrdinalIgnoreCase;

            if (s.Compare(other.Code, this.Code) != 0) { return false; }
            if (s.Compare(other.GTIN, this.GTIN) != 0) { return false; }

            return true;
        }
    }

    /// <summary>
    /// Struct used for searching with location id and product id as criteria
    /// </summary>
    public struct LocationProductIllegalIdSearchCriteria : IEquatable<LocationProductIllegalIdSearchCriteria>
    {
        public Int16 LocationId { get; set; }
        public Int32 ProductId { get; set; }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + this.LocationId.GetHashCode();
                hash = hash * 23 + this.ProductId.GetHashCode();
                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is LocationProductIllegalIdSearchCriteria)
            {
                return this.Equals((LocationProductIllegalIdSearchCriteria)obj);
            }
            return false;
        }

        public bool Equals(LocationProductIllegalIdSearchCriteria other)
        {
            return
                other.LocationId == this.LocationId &&
                other.ProductId == this.ProductId;
        }
    }

    /// <summary>
    /// Struct used for searching with location id and product id as criteria
    /// </summary>
    public struct LocationProductIllegalEntitySearchCriteria : IEquatable<LocationProductIllegalEntitySearchCriteria>
    {
        public Int16 LocationId { get; set; }
        public Int32 ProductId { get; set; }
        public Int32 EntityId { get; set; }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + this.LocationId.GetHashCode();
                hash = hash * 23 + this.ProductId.GetHashCode();
                hash = hash * 23 + this.EntityId.GetHashCode();
                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is LocationProductIllegalEntitySearchCriteria)
            {
                return this.Equals((LocationProductIllegalEntitySearchCriteria)obj);
            }
            return false;
        }

        public bool Equals(LocationProductIllegalEntitySearchCriteria other)
        {
            return
                other.LocationId == this.LocationId &&
                other.ProductId == this.ProductId &&
                other.EntityId == this.EntityId;
        }
    }
}
