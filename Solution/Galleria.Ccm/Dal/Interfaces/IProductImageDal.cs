﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IProductImageDal : IDal
    {
        IEnumerable<ProductImageDto> FetchByEntityId(Int32 entityId);
        IEnumerable<ProductImageDto> FetchByProductId(Int32 productId);
        IEnumerable<ProductImageDto> FetchByProductIdImageType(Int32 productId, Byte imageType);
        ProductImageDto FetchByProductIdImageTypeFacingTypeCompressionId
            (Int32 productId, Byte imageType, Byte facingType, Int32 compressionId);
        ProductImageDto FetchById(Int32 id);
        void Insert(ProductImageDto dto);
        void DeleteById(Int32 id);
    }
}
