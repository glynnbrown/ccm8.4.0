﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : L.Luong
//   Created (Copied From GFS)
// V8-27404 : L.Luong
//   Changed LevelId to EntryType
#endregion

#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//	Extended for new WorkpackageName property
//  Added new FetchByWorkpackageId
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;


namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IEventLogDal : IDal
    {

        /// <summary>
        /// Returns All Event Log
        /// </summary>
        /// <returns></returns>
        IEnumerable<EventLogDto> FetchAll();

        /// <summary>
        /// Returns All Event Log
        /// </summary>
        /// <returns></returns>
        IEnumerable<EventLogDto> FetchTopSpecifiedAmountIncludingDeleted(Int32 amountOfRecordsToReturn);

        /// <summary>
        /// Returns All Event Logs after given datetime
        /// </summary>
        /// <returns></returns>
        IEnumerable<EventLogDto> FetchAllOccuredAfterDateTime(DateTime lastUpdatedDateTime);

        /// <summary>
        /// Returns All Event Log
        /// </summary>
        /// <returns></returns>
        IEnumerable<EventLogDto> FetchByEventLogNameIdEntryTypeEntityId(Int16 TopRowCount, Nullable<Int32> EventLogNameID, Nullable<Int16> EntryType, Nullable<Int32> EntityID);

        /// <summary>
        /// Returns All Event Logs for Given set of Mixed criteria
        /// </summary>
        /// <returns></returns>
        IEnumerable<EventLogDto> FetchByMixedCriteria(Int16 topRowCount, Nullable<Int32> eventLogNameId, String source,
            Nullable<Int32> eventId, Nullable<Int16> entryType, Nullable<Int32> userId, Nullable<Int32> entityId, Nullable<DateTime> dateTime,
            String process, String computerName, String urlHelperLink, String description,
            String content, String gibraltarSessionId, DateTime startDate, DateTime endDate, String workpackageName, Int32? workpackageId);

        /// <summary>
        /// Returns All Event Log
        /// </summary>
        /// <returns></returns>
        EventLogDto FetchById(Int32 id);

        /// <summary>
        /// Returns All Event Log
        /// </summary>
        /// <returns></returns>
        IEnumerable<EventLogDto> FetchByEventLogNameId(Int32 id);

        /// <summary>
        /// Returns All Event Log by workpackageId
        /// </summary>
        /// <returns></returns>
        IEnumerable<EventLogDto> FetchByWorkpackageId(Int32 workpackageId);

        /// <summary>
        /// Inserts the given dto
        /// </summary>
        /// <param name="dto"></param>
        void Insert(EventLogDto dto);

        /// <summary>
        /// Inserts the given dto, using the value of EventLogDto.EventLogName property, ignoring EventLogNameId.
        /// If no record EventLogName record with the given name exists, one will be created.
        /// </summary>
        /// <param name="dto"></param>
        void InsertWithName(EventLogDto dto);

        /// <summary>
        /// Marks all event log entries as deleted. 
        /// </summary>
        void DeleteAll();

    }
}
