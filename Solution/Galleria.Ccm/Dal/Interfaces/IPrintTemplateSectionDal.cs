﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// Denotes the expected members of a PrintTemplateSection Dal
    /// </summary>
    public interface IPrintTemplateSectionDal : IDal
    {
        PrintTemplateSectionDto FetchById(Int32 id);
        IEnumerable<PrintTemplateSectionDto> FetchByPrintTemplateSectionGroupId(Int32 printTemplateSectionGroupId);
        void Insert(PrintTemplateSectionDto dto);
        void Update(PrintTemplateSectionDto dto);
        void DeleteById(Int32 id);
    }
}
