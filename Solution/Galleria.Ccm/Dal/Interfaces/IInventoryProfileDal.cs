﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26124 : I.George
//		Created (Auto-generated)
// V8-28013 : A.Kuszyk
//  Added FetchByEntityIdName.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IInventoryProfileDal : IDal
    {
        void DeleteById(Int32 id);
        void Insert(InventoryProfileDto dto);
        void Update(InventoryProfileDto dto);
        InventoryProfileDto FetchById(Int32 id);
        InventoryProfileDto FetchByEntityIdName(Int32 entityId, String name);
    }
}

