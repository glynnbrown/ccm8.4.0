﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchById", "Id")]
    public interface IFixturePackageDal : IDal
    {
        FixturePackageDto FetchById(Object id);
        void Insert(FixturePackageDto dto);
        void Update(FixturePackageDto dto);
        void DeleteById(Object id);

        void LockById(Object id);
        void UnlockById(Object id);
    }
}
