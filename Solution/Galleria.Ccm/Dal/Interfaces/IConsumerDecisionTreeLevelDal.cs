﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IConsumerDecisionTreeLevelDal : IDal
    {
        ConsumerDecisionTreeLevelDto FetchById(Int32 id);
        IEnumerable<ConsumerDecisionTreeLevelDto> FetchByEntityId(Int32 entityId);
        IEnumerable<ConsumerDecisionTreeLevelDto> FetchByConsumerDecisionTreeId(Int32 consumerDecisionTreeId);
        void Insert(ConsumerDecisionTreeLevelDto dto);
        void Update(ConsumerDecisionTreeLevelDto dto);
        void DeleteById(Int32 id);
    }
}
