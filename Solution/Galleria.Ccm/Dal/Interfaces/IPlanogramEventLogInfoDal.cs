﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM810)
// V8-29861 : A.Probyn
//	Created
#endregion

#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IPlanogramEventLogInfoDal : IDal
    {
        IEnumerable<PlanogramEventLogInfoDto> FetchByWorkpackageId(Int32 workpackageId);
    }
}
