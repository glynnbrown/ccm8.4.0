﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IFileInfoDal : IDal
    {
        /// <summary>
        /// Returns all file info dtos in the data store
        /// </summary>
        /// <returns>All file info dtos</returns>
        FileInfoDto FetchById(Int32 fileId);
    }
}
