﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//		Created (Auto-generated)
// CCM-25444 : N.Haywood
//      Added FetchByLocationGroupIds
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface ILocationGroupInfoDal : IDal
    {
        /// <summary>
        /// Returns all deleted groups for the given location hierarchy id.
        /// </summary>
        IEnumerable<LocationGroupInfoDto> FetchDeletedByLocationHierarchyId(Int32 locationHierarchyId);
        IEnumerable<LocationGroupInfoDto> FetchByLocationGroupIds(IEnumerable<Int32> locationGroupIds);
    }
}
