﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByEntityIdIncludingDeleted", "EntityId")]
    public interface IPlanogramStatusInfoDal : IDal
    {
        IEnumerable<PlanogramStatusInfoDto> FetchByEntityId(Int32 entityId);
        IEnumerable<PlanogramStatusInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId);
    }
}
