﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25454 : J.Pickup
//		Created (Auto-generated)
// CCM-25556 : J.Pickup
//      Added FetchAllIncludingDeleted()
// V8-27241 : A.Kuszyk
//      Added FetchByEntityIdName.
// V8-255556 : J.Pickup
//      Added FetchByUniqueContentReference
#endregion
#region Version History: CCM803
// V8-29134 : D.Pleasance
//  Renamed FetchAllIncludingDeleted to FetchAll as we now deleted data outright
#endregion 
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IAssortmentDal : IDal
    {
        IEnumerable<AssortmentDto> FetchAll();
        AssortmentDto FetchById(Int32 id);
        AssortmentDto FetchByUniqueContentReference(Guid ucr);
        AssortmentDto FetchByEntityIdName(Int32 entityId, String name);
        void Insert(AssortmentDto dto);
        void Update(AssortmentDto dto);
        void DeleteById(Int32 id);
        void DeleteByEntityId(Int32 entityId);
    }

    public interface IAssortmentSearchCriteriaDal : IDal
    {
        /// <summary>
        /// Returns a list of latest version assortment records by entity id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        IEnumerable<AssortmentSearchCriteria> FetchLatestVersionsByEntityId(Int32 entityId);

        /// <summary>
        /// Returns a list of maximum version assortment records by entity id (includes deleted)
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        IEnumerable<AssortmentSearchCriteria> FetchMaximumVersionsByEntityId(Int32 entityId);

        /// <summary>
        /// Returns a list of latest version assortment location records by entity id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        IEnumerable<AssortmentLocationSearchCriteria> FetchLatestAssortmentLocationVersionsByEntityId(Int32 entityId);

        /// <summary>
        /// Returns a list of maximum version assortment location records by entity id (includes deleted)
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        IEnumerable<AssortmentLocationSearchCriteria> FetchMaximumAssortmentLocationVersionsByEntityId(Int32 entityId);
    }

    /// <summary>
    /// Assortment criteria for use when searching (imports)
    /// </summary>
    public struct AssortmentSearchCriteria
    {
        public Int32 AssortmentId { get; set; }
        public Int32? AssortmentProductId { get; set; }
        public String AssortmentName { get; set; }
        public String ProductGroupCode { get; set; }
        public String ProductCode { get; set; }

        public AssortmentSearchCriteria(
            String assortmentName,
            String productGroupCode,
            String productCode)
            : this()
        {
            AssortmentName = assortmentName;
            ProductGroupCode = productGroupCode;
            ProductCode = productCode;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 7 + this.AssortmentName.ToLowerInvariant().GetHashCode();
                hash = hash * 7 + this.ProductGroupCode.ToLowerInvariant().GetHashCode();

                if (this.ProductCode != null)
                {
                    hash = hash * 7 + this.ProductCode.ToLowerInvariant().GetHashCode();
                }
                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            AssortmentSearchCriteria other = (AssortmentSearchCriteria)obj;

            //Dont compare id as this is only used in the Assortment import for lookup purposes
            StringComparer s = StringComparer.OrdinalIgnoreCase;

            if (s.Compare(other.AssortmentName, this.AssortmentName) != 0) { return false; }
            if (s.Compare(other.ProductGroupCode, this.ProductGroupCode) != 0) { return false; }
            if (s.Compare(other.ProductCode, this.ProductCode) != 0) { return false; }

            return true;
        }

        public bool Equals(AssortmentSearchCriteria other)
        {
            StringComparer s = StringComparer.OrdinalIgnoreCase;

            if (s.Compare(other.AssortmentName, this.AssortmentName) != 0) { return false; }
            if (s.Compare(other.ProductGroupCode, this.ProductGroupCode) != 0) { return false; }
            if (s.Compare(other.ProductCode, this.ProductCode) != 0) { return false; }

            return true;
        }
    }

    /// <summary>
    /// Assortment Location criteria for use when searching (imports)
    /// </summary>
    public struct AssortmentLocationSearchCriteria
    {
        public Int32 AssortmentId { get; set; }
        public Int32 AssortmentLocationId { get; set; }
        public String AssortmentName { get; set; }
        public String LocationCode { get; set; }

        public AssortmentLocationSearchCriteria(
            String assortmentName,
            String locationCode)
            : this()
        {
            AssortmentName = assortmentName;
            LocationCode = locationCode;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 7 + this.AssortmentName.ToLowerInvariant().GetHashCode();
                hash = hash * 7 + this.LocationCode.ToLowerInvariant().GetHashCode();

                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            AssortmentLocationSearchCriteria other = (AssortmentLocationSearchCriteria)obj;

            //Dont compare id as this is only used in the Assortment Location import for lookup purposes
            StringComparer s = StringComparer.OrdinalIgnoreCase;

            if (s.Compare(other.AssortmentName, this.AssortmentName) != 0) { return false; }
            if (s.Compare(other.LocationCode, this.LocationCode) != 0) { return false; }

            return true;
        }

        public bool Equals(AssortmentLocationSearchCriteria other)
        {
            StringComparer s = StringComparer.OrdinalIgnoreCase;

            if (s.Compare(other.AssortmentName, this.AssortmentName) != 0) { return false; }
            if (s.Compare(other.LocationCode, this.LocationCode) != 0) { return false; }

            return true;
        }
    }
}
