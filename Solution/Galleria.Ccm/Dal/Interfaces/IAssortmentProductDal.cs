﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25454 : J.Pickup
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IAssortmentProductDal : IDal
    {
        IEnumerable<AssortmentProductDto> FetchByAssortmentId(Int32 assortmentId);
        AssortmentProductDto FetchById(Int32 id);
        void Insert(AssortmentProductDto dto);
        void Update(AssortmentProductDto dto);
        void DeleteById(Int32 id);
    }

    /// <summary>
    /// Struct used for searching with assortment id and product id as criteria
    /// </summary>
    public struct AssortmentProductIdSearchCriteria : IEquatable<AssortmentProductIdSearchCriteria>
    {
        public Int32 AssortmentId { get; set; }
        public Int32 ProductId { get; set; }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + this.AssortmentId.GetHashCode();
                hash = hash * 23 + this.ProductId.GetHashCode();
                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is AssortmentProductIdSearchCriteria)
            {
                return this.Equals((AssortmentProductIdSearchCriteria)obj);
            }
            return false;
        }

        public bool Equals(AssortmentProductIdSearchCriteria other)
        {
            return
                other.AssortmentId == this.AssortmentId &&
                other.ProductId == this.ProductId;
        }
    }

    /// <summary>
    /// Struct used for searching with assortment name and product gtin as criteria
    /// </summary>
    public struct AssortmentProductCodeSearchCriteria : IEquatable<AssortmentProductCodeSearchCriteria>
    {
        public String AssortmentName { get; set; }
        public String GTIN { get; set; }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + this.AssortmentName.GetHashCode();
                hash = hash * 23 + this.GTIN.GetHashCode();
                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is AssortmentProductCodeSearchCriteria)
            {
                return this.Equals((AssortmentProductCodeSearchCriteria)obj);
            }
            return false;
        }

        public bool Equals(AssortmentProductCodeSearchCriteria other)
        {
            return
                other.AssortmentName == this.AssortmentName &&
                other.GTIN == this.GTIN;
        }
    }
}
