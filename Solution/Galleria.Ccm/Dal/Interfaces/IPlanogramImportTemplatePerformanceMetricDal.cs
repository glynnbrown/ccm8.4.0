﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.1)
//V8-28622 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// Denotes the expected memebers of a PlanogramImportTemplatePerformanceMetric Dal
    /// </summary>
    [DefaultFetchMethod("FetchByPlanogramImportTemplateId", "PlanogramImportTemplateId")]
    public interface IPlanogramImportTemplatePerformanceMetricDal : IDal
    {
        IEnumerable<PlanogramImportTemplatePerformanceMetricDto> FetchByPlanogramImportTemplateId(Object planogramImportTemplateId);
        void Insert(PlanogramImportTemplatePerformanceMetricDto dto);
        void Update(PlanogramImportTemplatePerformanceMetricDto dto);
        void DeleteById(Int32 id);
    }
}