﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM CCM830)
// CCM-32524 : L.Ineson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByFixturePackageId", "FixturePackageId")]
    public interface IFixtureAnnotationDal : IDal
    {
        IEnumerable<FixtureAnnotationDto> FetchByFixturePackageId(Object fixturePackageId);
        void Insert(FixtureAnnotationDto dto);
        void Update(FixtureAnnotationDto dto);
        void DeleteById(Int32 id);
    }
}
