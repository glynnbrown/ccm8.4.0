﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IAssortmentMinorRevisionAmendDistributionActionLocationDal : IDal
    {
        AssortmentMinorRevisionAmendDistributionActionLocationDto FetchById(Int32 id);
        IEnumerable<AssortmentMinorRevisionAmendDistributionActionLocationDto> FetchByAssortmentMinorRevisionAmendDistributionActionId(Int32 assortmentMinorRevisionAmendDistributionActionId);
        void Insert(AssortmentMinorRevisionAmendDistributionActionLocationDto dto);
        void Update(AssortmentMinorRevisionAmendDistributionActionLocationDto dto);
        void DeleteById(Int32 id);
    }
}