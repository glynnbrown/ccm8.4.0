﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
// V8-26140 :I.George
//  Added FetchByEntityIdIncludingDeleted
#endregion
#region Version History: (CCM 8.0.3)
// V8-29214 : D.Pleasance
//  Removed FetchDeletedByEntityId \ FetchByEntityIdIncludingDeleted
#endregion 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IAssortmentMinorRevisionInfoDal : IDal
    {
        List<AssortmentMinorRevisionInfoDto> FetchByEntityId(Int32 entityId);
        AssortmentMinorRevisionInfoDto FetchByEntityIdName(Int32 entityId, String name);
        List<AssortmentMinorRevisionInfoDto> FetchByProductGroupId(Int32 productGroupId);
        List<AssortmentMinorRevisionInfoDto> FetchByEntityIdChangeDate(Int32 entityId, DateTime changeDate);
        List<AssortmentMinorRevisionInfoDto> FetchByEntityIdAssortmentMinorRevisionSearchCriteria(Int32 entityId, String assortmentSearchCritiera);
    }
}
