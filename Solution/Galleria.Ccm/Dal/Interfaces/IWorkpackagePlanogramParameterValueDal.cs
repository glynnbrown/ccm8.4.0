﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25886 : L.Ineson
//  Created
// V8-28232 : N.Foster
//  Added support for batch operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByWorkpackagePlanogramParameterId", "WorkpackagePlanogramParameterId")]
    public interface IWorkpackagePlanogramParameterValueDal : IBatchDal<WorkpackagePlanogramParameterValueDto>
    {
        IEnumerable<WorkpackagePlanogramParameterValueDto> FetchByWorkpackagePlanogramParameterId(Int32 workpackagePlanogramParameterId);
        void Insert(WorkpackagePlanogramParameterValueDto dto);
        void Update(WorkpackagePlanogramParameterValueDto dto);
        void DeleteById(Int32 id);
    }
}