﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByFixturePackageId", "FixturePackageId")]
    public interface IFixtureComponentDal : IDal
    {
        IEnumerable<FixtureComponentDto> FetchByFixturePackageId(Object fixturePackageId);
        void Insert(FixtureComponentDto dto);
        void Update(FixtureComponentDto dto);
        void DeleteById(Int32 id);
    }
}
