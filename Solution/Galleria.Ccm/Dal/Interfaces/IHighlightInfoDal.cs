﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25436 L.Luong
//  Created
// V8-27941 J.Pcikup
//  FetchAll();
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByIds", "Id")]
    public interface IHighlightInfoDal : IDal
    {
        IEnumerable<HighlightInfoDto> FetchByIds(IEnumerable<Object> ids);
        IEnumerable<HighlightInfoDto> FetchByEntityId(Int32 entityId);
        IEnumerable<HighlightInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId);
    }
}
