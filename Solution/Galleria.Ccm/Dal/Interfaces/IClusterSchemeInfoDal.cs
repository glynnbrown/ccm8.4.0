﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (SA 1.0)
// V8-25629 : A.Kuszyk
//		Created (Copied from SA).
#endregion
#region Version History: (CCM v8.0.3)
// V8-29216 : D.Pleasance
//  Removed FetchByEntityIdIncludingDeleted
#endregion 
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IClusterSchemeInfoDal : IDal
    {
        IEnumerable<ClusterSchemeInfoDto> FetchByEntityId(Int32 entityId);
    }
}
