﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchById", "Id")]
    public interface IProductLevelDal : IDal
    {
        void DeleteById(Int32 id);
        ProductLevelDto FetchById(Int32 id);
        IEnumerable<ProductLevelDto> FetchByProductHierarchyId(Int32 productHierarchyId);
        void Insert(ProductLevelDto dto);
        void Update(ProductLevelDto dto);
    }
}
