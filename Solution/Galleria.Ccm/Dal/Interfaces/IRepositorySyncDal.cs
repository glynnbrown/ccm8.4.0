﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IRepositorySyncDal : IDal
    {
        IEnumerable<RepositorySyncDto> FetchAll();
        RepositorySyncDto FetchById(Int32 id);
    }
}
