﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    ///     Dal interface for Renumbering Strategy.
    /// </summary>
    [DefaultFetchMethod("FetchByEntityId","EntityId")]
    public interface IRenumberingStrategyDal : IDal
    {
        #region Fetch

        /// <summary>
        ///     Returns a <see cref="RenumberingStrategyDto" /> matching the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id of the <see cref="RenumberingStrategyDto" /> that will be fetched.</param>
        /// <returns>A new instance of <see cref="RenumberingStrategyDto" /> with the data for the provided <paramref name="id" />.</returns>
        RenumberingStrategyDto FetchById(Int32 id);

        /// <summary>
        ///     Returns an enumeration of <see cref="RenumberingStrategyDto" /> matching the provided <paramref name="entityId" />.
        /// </summary>
        /// <param name="entityId">Id of the <see cref="Entity" /> that will be matched for fetching.</param>
        /// <returns>An enumeration of instances of <see cref="RenumberingStrategyDto" /> wmatching the provided <paramref name="entityId" />.</returns>
        IEnumerable<RenumberingStrategyDto> FetchByEntityId(Int32 entityId);

        /// <summary>
        ///     Returns a <see cref="RenumberingStrategyDto" /> matching the provided <paramref name="entityId" /> and <paramref name="name"/>.
        /// </summary>
        /// <param name="entityId">Id of the <see cref="Entity" /> that will be matched for fetching.</param>
        /// <param name="name">Name to be matched for fetching.</param>
        /// <returns>An instance of <see cref="RenumberingStrategyDto" /> matching the provided <paramref name="entityId" /> and <paramref name="name"/>.</returns>
        RenumberingStrategyDto FetchByEntityIdName(Int32 entityId, String name);

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the provided <see cref="RenumberingStrategyDto" /> into the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be inserted.</param>
        void Insert(RenumberingStrategyDto dto);

        #endregion

        #region Update

        /// <summary>
        ///     Updates the provided <see cref="RenumberingStrategyDto" /> in the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be updated.</param>
        void Update(RenumberingStrategyDto dto);

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the data in the data store corresponding to the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id value to determine the data to delete.</param>
        void DeleteById(Object id);

        #endregion
    }
}
