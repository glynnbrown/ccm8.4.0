﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31934 : A.Heathcote
//      Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchAll")]
    public interface IUserEditorSettingsSelectedColumnDal : IDal
    {
        IEnumerable<UserEditorSettingsSelectedColumnDto> FetchAll();
        void Insert(UserEditorSettingsSelectedColumnDto dto);
        void Update(UserEditorSettingsSelectedColumnDto dto);
        void DeleteById(Int32 id);
    }
}
