﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24779 : L.Luong
//  Created
#endregion
#region Version History: (CCM 8.03)
// V8-29220 : L.Ineson
//Removed date deleted
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// Denotes the expected memebers of a PlanogramImportTemplateInfo Dal
    /// </summary>
    public interface IPlanogramImportTemplateInfoDal : IDal
    {
        IEnumerable<PlanogramImportTemplateInfoDto> FetchByEntityId(Int32 entityId);
    }
}
