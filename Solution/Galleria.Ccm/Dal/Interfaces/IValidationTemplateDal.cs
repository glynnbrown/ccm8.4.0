﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created (Auto-generated)
// V8-26815 : A.Silva ~ Added LockById and UnlockById. Used in the User Dal when accessing files.
// V8-27269 : A.Kuszyk
//      Added FetchByEntityIdName.
#endregion

#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    ///     Interface for the Validation Template Dal.
    /// </summary>
    [DefaultFetchMethod("FetchById", "Id")]
    public interface IValidationTemplateDal : IDal
    {
        #region Fetch

        /// <summary>
        ///     Returns a <see cref="ValidationTemplateDto" /> matching the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id of the <see cref="ValidationTemplateDto" /> that will be fetched.</param>
        /// <returns>A new instance of <see cref="ValidationTemplateDto" /> with the data for the provided <paramref name="id" />.</returns>
        ValidationTemplateDto FetchById(Object id);

        ValidationTemplateDto FetchByEntityIdName(Int32 entityId, String name);

        #endregion

        #region Insert

        /// <summary>
        ///     Inserts the provided <see cref="ValidationTemplateDto" /> into the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be inserted.</param>
        void Insert(ValidationTemplateDto dto);

        #endregion

        #region Update

        /// <summary>
        ///     Updates the provided <see cref="ValidationTemplateDto" /> in the data store.
        /// </summary>
        /// <param name="dto">The instance containing the data to be updated.</param>
        void Update(ValidationTemplateDto dto);

        #endregion

        #region Delete

        /// <summary>
        ///     Deletes the data in the data store corresponding to the provided <paramref name="id" />.
        /// </summary>
        /// <param name="id">Id value to determine the data to delete.</param>
        void DeleteById(Object id);

        #endregion

        #region Commands

        /// <summary>
        ///     Locks a validation template for editing.
        /// </summary>
        /// <param name="id">Id of the dal used to access the validation template.</param>
        /// <returns><c>True</c> if the validation template was succesfully locked, <c>false</c> otherwise.</returns>
        /// <remarks>Only implemented for user dal, as it is not used in any other.</remarks>
        Boolean LockById(Object id);

        /// <summary>
        ///     Unlocks a validation template after editing.
        /// </summary>
        /// <param name="id">Id of the dal used to access the validation template.</param>
        /// <remarks>Only implemented for user dal, as it is not used in any other.</remarks>
        void UnlockById(Object id);

        #endregion
    }
}