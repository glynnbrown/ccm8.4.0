﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-25629 : A.Kuszyk
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IClusterDal : IDal
    { 
        ClusterDto FetchById(Int32 id);
        IEnumerable<ClusterDto> FetchByClusterSchemeId(Int32 clusterSchemeId);
        void Insert(ClusterDto dto); 
        void Update(ClusterDto dto); 
        void Upsert(IEnumerable<ClusterDto> dtoList, ClusterIsSetDto isSetDto); 
        void DeleteById(Int32 id);
    }
}
