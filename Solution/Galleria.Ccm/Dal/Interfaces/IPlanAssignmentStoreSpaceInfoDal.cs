#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25879 : Martin Shelley
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IPlanAssignmentStoreSpaceInfoDal : IDal
    {
        IEnumerable<PlanAssignmentStoreSpaceInfoDto> FetchByEntityIdProductGroupId(Int32 entityId, Int32 productGroupId, Int32? clusterSchemeId);
        IEnumerable<PlanAssignmentStoreSpaceInfoDto> FetchByEntityIdLocationId(Int32 entityId, Int16 locationId, Int32? clusterSchemeId);
    }
}
