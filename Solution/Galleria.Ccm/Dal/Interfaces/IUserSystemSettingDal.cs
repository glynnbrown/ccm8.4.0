﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25559 : L.Ineson
//		Copied from SA
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;


namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("Fetch")]
    public interface IUserSystemSettingDal : IDal
    {
        UserSystemSettingDto Fetch();
        void Insert(UserSystemSettingDto dto);
        void Update(UserSystemSettingDto dto);
    }
}
