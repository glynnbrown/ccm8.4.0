﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25546 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByWorkflowTaskId", "WorkflowTaskId")]
    public interface IWorkflowTaskParameterDal : IDal
    {
        IEnumerable<WorkflowTaskParameterDto> FetchByWorkflowTaskId(Int32 workflowTaskId);
        IEnumerable<WorkflowTaskParameterDto> FetchByWorkflowId(Int32 workflowId);
        void Insert(WorkflowTaskParameterDto dto);
        void Update(WorkflowTaskParameterDto dto);
        void DeleteById(Int32 id);
    }
}
