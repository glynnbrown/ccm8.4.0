﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26222 : L.Luong
//		Created (Auto-generated)

#endregion
#endregion
 
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IUserInfoDal : IDal
    {
        IEnumerable<UserInfoDto> FetchAll();
    }
}

