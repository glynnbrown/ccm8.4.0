﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IAssortmentMinorRevisionReplaceActionLocationDal : IDal
    {
        AssortmentMinorRevisionReplaceActionLocationDto FetchById(Int32 id);
        IEnumerable<AssortmentMinorRevisionReplaceActionLocationDto> FetchByAssortmentMinorRevisionReplaceActionId(Int32 assortmentMinorRevisionReplaceActionId);
        void Insert(AssortmentMinorRevisionReplaceActionLocationDto dto);
        void Update(AssortmentMinorRevisionReplaceActionLocationDto dto);
        void DeleteById(Int32 id);
    }
}

