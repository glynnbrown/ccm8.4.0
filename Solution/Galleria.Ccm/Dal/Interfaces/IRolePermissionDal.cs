﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-26234 : L.Ineson
//		Copied from GFS
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IRolePermissionDal : IDal
    {
        void DeleteById(Int32 Id);

        void Update(RolePermissionDto dto);

        void Insert(RolePermissionDto dto);

        RolePermissionDto FetchById(Int32 Id);

        IEnumerable<RolePermissionDto> FetchByRoleId(Int32 roleId);

        IEnumerable<RolePermissionDto> FetchAll();
    }
}
