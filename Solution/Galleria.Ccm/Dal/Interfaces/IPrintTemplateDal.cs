﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// Denotes the expected members of a PrintTemplate Dal
    /// </summary>
    public interface IPrintTemplateDal : IDal
    {
        PrintTemplateDto FetchById(Object id);
        void Insert(PrintTemplateDto dto);
        void Update(PrintTemplateDto dto);
        void DeleteById(Object id);

        void LockById(Object id);
        void UnlockById(Object id);
    }
}
