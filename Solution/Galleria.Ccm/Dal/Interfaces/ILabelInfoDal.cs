﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24265 : N.Haywood
//  Created
// V8-27940 : L.Luong
//  Added FetchByEntityId and FetchByEntityIdIncludingDeleted
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByIds", "Id")]
    public interface ILabelInfoDal : IDal
    {
        IEnumerable<LabelInfoDto> FetchByIds(IEnumerable<Object> ids);
        IEnumerable<LabelInfoDto> FetchByEntityId(Int32 entityId);
        IEnumerable<LabelInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId);
    }
}
