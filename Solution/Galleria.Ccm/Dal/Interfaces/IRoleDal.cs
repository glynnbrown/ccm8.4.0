﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-26234 : L.Ineson
//		Copied from GFS
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IRoleDal : IDal
    {
        /// <summary>
        /// Deletes the role with the given id
        /// </summary>
        /// <param name="id"></param>
        void DeleteById(Int32 id);

        /// <summary>
        /// Returns the role dto with the given id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        RoleDto FetchById(Int32 id);

        /// <summary>
        /// Returns a list of roles for the give entity id
        /// </summary>
        IEnumerable<RoleDto> FetchByEntityId(Int32 entityId);

        /// <summary>
        /// Inserts the given dto
        /// </summary>
        /// <param name="dto"></param>
        void Insert(RoleDto dto);

        /// <summary>
        /// Updates the given dto
        /// </summary>
        /// <param name="dto"></param>
        void Update(RoleDto dto);
    }
}
