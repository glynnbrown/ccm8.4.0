﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface ILocationSpaceElementSearchCriteriaDal : IDal
    {
        IEnumerable<LocationSpaceElementSearchCriteriaDto> FetchByEntityId(Int32 entityId);
    }

    public interface ILocationSpaceElementDal : IDal
    {
        /// <summary>
        /// Returns Location Space Element by ID
        /// </summary>
        /// <returns></returns>
        LocationSpaceElementDto FetchById(Int32 id);

        /// <summary>
        /// Returns Location Space Element by Location Space Bay ID
        /// </summary>
        /// <returns></returns>
        IEnumerable<LocationSpaceElementDto> FetchByLocationSpaceBayId(Int32 id);

        /// <summary>
        /// Inserts the given dto
        /// </summary>
        /// <param name="dto"></param>
        void Insert(LocationSpaceElementDto dto);

        /// <summary>
        /// Inserts a list of given dtos
        /// </summary>
        /// <param name="dtoList"></param>
        void Insert(IEnumerable<LocationSpaceElementDto> dtoList);

        /// <summary>
        /// Updates the given dto
        /// </summary>
        /// <param name="dto"></param>
        void Update(LocationSpaceElementDto dto);


        /// <summary>
        /// Delete the given Location Space Element ID
        /// </summary>
        /// <param name="Int32"></param>
        void DeleteById(Int32 id);

        /// <summary>
        /// Delete by entity Id
        /// </summary>
        /// <param name="Int32"></param>
        void DeleteByEntityId(Int32 id);

        /// <summary>
        /// Upsert collection of location space element dto's
        /// </summary>
        /// <param name="dtoList"></param>
        void Upsert(IEnumerable<LocationSpaceElementDto> dtoList);

    }
}
