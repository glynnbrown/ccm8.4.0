﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GFS 1.0)
// CCM-25445 : L.Ineson
//		Copied from GFS 
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface ILocationGroupLocationDal : IDal
    {
        /// <summary>
        /// Returns the dto for the given location id
        /// </summary>
        /// <param name="locationId"></param>
        /// <returns></returns>
        //LocationGroupLocationDto FetchByLocationId(Int16 locationId);

        /// <summary>
        /// Returns dtos for all locations assigned to the given group id
        /// </summary>
        /// <param name="locationGroupId"></param>
        /// <returns></returns>
        IEnumerable<LocationGroupLocationDto> FetchByLocationGroupId(Int32 locationGroupId);

        /// <summary>
        /// Updates the location based on the given dto
        /// </summary>
        /// <param name="dto"></param>
        void Update(LocationGroupLocationDto dto);

    }
}
