﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-25664 : A.Kuszyk
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IUserPlanogramGroupDal : IDal
    {
        UserPlanogramGroupDto FetchById(Int32 userPlanogramGroupId);
        IEnumerable<UserPlanogramGroupDto> FetchByUserId(Int32 userId);
        void Insert(UserPlanogramGroupDto dto);
        void Update(UserPlanogramGroupDto dto);
        void DeleteById(Int32 id);
    }
}
