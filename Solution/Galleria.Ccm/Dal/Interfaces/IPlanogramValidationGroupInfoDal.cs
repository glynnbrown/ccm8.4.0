﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26944 : A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IPlanogramValidationGroupInfoDal : IDal
    {
        /// <summary>
        ///     Fetches a collection of <see cref="PlanogramValidationGroupInfoDto"/> instances matching the given <paramref name="planogramIds"/>.
        /// </summary>
        /// <param name="planogramIds">A collection of <see cref="Int32"/> values that must be matched by the returned dtos.</param>
        /// <returns>A new collection of <see cref="PlanogramValidationGroupInfoDto"/>.</returns>
        IEnumerable<PlanogramValidationGroupInfoDto> FetchPlanogramValidationGroupsByPlanogramIds(IEnumerable<Int32> planogramIds);
    }
}
