﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IConsumerDecisionTreeNodeProductDal : IDal
    {
        ConsumerDecisionTreeNodeProductDto FetchById(Int32 id);
        IEnumerable<ConsumerDecisionTreeNodeProductDto> FetchByConsumerDecisionTreeNodeId(Int32 consumerDecisionTreeNodeId);
        void Insert(ConsumerDecisionTreeNodeProductDto dto);
        void Update(ConsumerDecisionTreeNodeProductDto dto);
        void DeleteById(Int32 id);
    }
}
