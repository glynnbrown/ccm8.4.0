﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 L.Hodson
//  Created
// V8-25436 : L.Luong
//  Added Lock and Unlock 
#endregion
#region Version History: (CCM 8.03)
// V8-28661 : L.Ineson
//  Added FetchByEntityIdName
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchById", "Id")]
    public interface IHighlightDal : IDal
    {
        HighlightDto FetchById(Object id);
        HighlightDto FetchByEntityIdName(Int32 entityId, String name);
        void Insert(HighlightDto dto);
        void Update(HighlightDto dto);
        void DeleteById(Object id);

        void LockById(Object id);
        void UnlockById(Object id);
    }
}
