﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    ///     Interface declaring the expected behavior for a <c>Planogram Comparison Template</c> DAL implementation.
    /// </summary>
    /// <remarks>The default fetch method is <c>FetchByPlanogramId</c> which requires the <c>PlanogramId</c> value.</remarks>
    [DefaultFetchMethod("FetchById", "Id")]
    public interface IPlanogramComparisonTemplateDal : IDal
    {
        /// <summary>
        ///     Fetch a <c>Data Transfer Object</c> from the <c>DAL</c> for a <c>Planogram Comparison Template</c> model object
        ///     that is associated to the given <paramref name="id" />.
        /// </summary>
        /// <param name="id"><see cref="Object" /> containing the id of the <c>Planogram Comparison Template</c> to fetch.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonTemplateDto" />.</returns>
        PlanogramComparisonTemplateDto FetchById(Object id);

        /// <summary>
        ///     Fetch a <c>Data Transfer Object</c> from the <c>DAL</c> for a <c>Planogram Comparison Template</c> model object
        ///     that is associated to the given <paramref name="entityId" />.
        /// </summary>
        /// <param name="entityId"><see cref="Int32" /> containing the id of the entity Id from which to fetch.</param>
        /// <param name="name">The name of the template to be fetched.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonTemplateDto" />.</returns>
        PlanogramComparisonTemplateDto FetchByEntityIdName(Int32 entityId, String name);

        /// <summary>
        ///     Insert the data contained in the given <paramref name="dto" /> into the <c>DAL</c>.
        /// </summary>
        /// <param name="dto"></param>
        void Insert(PlanogramComparisonTemplateDto dto);

        /// <summary>
        ///     Update the data contained in the given <paramref name="dto" /> into the <c>DAL</c>.
        /// </summary>
        /// <param name="dto"></param>
        void Update(PlanogramComparisonTemplateDto dto);

        /// <summary>
        ///     Delete the data linked to the given <paramref name="id" /> into the <c>DAL</c>.
        /// </summary>
        /// <param name="id"></param>
        void DeleteById(Object id);

        /// <summary>
        ///     Lock the underlying file by the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The file name of the file to be locked.</param>
        void LockById(Object id);

        /// <summary>
        ///     Unlock the underlying file by the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The file name of the file to be unlocked.</param>
        void UnlockById(Object id);
    }
}