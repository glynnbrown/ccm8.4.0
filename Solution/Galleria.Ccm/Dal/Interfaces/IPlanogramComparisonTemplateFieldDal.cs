﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    ///     Interface declaring the expected behavior for a <c>Planogram Comparison Field</c> DAL implementation.
    /// </summary>
    /// <remarks>The default fetch method is <c>FetchByPlanogramComparisonId</c> which requires the <c>PlanogramComparisonId</c> value.</remarks>
    [DefaultFetchMethod("FetchByPlanogramComparisonTemplateId", "PlanogramComparisonTemplateId")]
    public interface IPlanogramComparisonTemplateFieldDal : IDal
    {
        /// <summary>
        ///     Fetch a <c>Data Transfer Object</c> from the <c>DAL</c> for a <c>Planogram Comparison Template Field</c> model object that belongs to a <c>Planogram Comparison Template</c> with the given<paramref name="planogramComparisonTemplateId"/>.
        /// </summary>
        /// <param name="planogramComparisonTemplateId"><see cref="Object"/> containing the id of the parent <c>Planogram Comparison Template</c>.</param>
        /// <returns>A new instance of <see cref="PlanogramComparisonTemplateFieldDto"/>.</returns>
        IEnumerable<PlanogramComparisonTemplateFieldDto> FetchByPlanogramComparisonTemplateId(Object planogramComparisonTemplateId);

        /// <summary>
        ///     Insert the data contained in the given <paramref name="dto"/> into the <c>DAL</c>.
        /// </summary>
        /// <param name="dto"></param>
         void Insert(PlanogramComparisonTemplateFieldDto dto);

        /// <summary>
        ///     Update the data contained in the given <paramref name="dto"/> into the <c>DAL</c>.
        /// </summary>
        /// <param name="dto"></param>
         void Update(PlanogramComparisonTemplateFieldDto dto);

        /// <summary>
        ///     Delete the data linked to the given <paramref name="id"/> into the <c>DAL</c>.
        /// </summary>
        /// <param name="id"></param>
        void DeleteById(Object id);
    }
}