﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByFixtureId", "FixtureId")]
    public interface IFixtureComponentItemDal : IDal
    {
        IEnumerable<FixtureComponentItemDto> FetchByFixtureId(Int32 fixtureId);
        void Insert(FixtureComponentItemDto dto);
        void Update(FixtureComponentItemDto dto);
        void DeleteById(Int32 id);
    }
}
