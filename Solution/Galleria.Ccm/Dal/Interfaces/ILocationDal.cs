﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25444 : L.Ineson
//		Created (Auto-generated)
// V8-25619 : A.Kuszyk
//  Removed all Fetch operations apart from FetchById.
// V8-25445 : L.Ineson
//  Added FetchByLocationGroupId
// V8-25748 : K.Pickup
//      Changed data type of LocationId to Int16.
// V8-25556 : D.Pleasance
//      Added FetchByEntityIdIncludingDeleted
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface ILocationDal : IDal
    { 
         void DeleteById(Int16 id); 
         void DeleteByEntityId(Int32 entityId);
         LocationDto FetchById(Int16 id); 
         IEnumerable<LocationDto> FetchByEntityId(Int32 entityId);
         IEnumerable<LocationDto> FetchByEntityIdIncludingDeleted(Int32 entityId);
         IEnumerable<LocationDto> FetchByLocationGroupId (Int32 locationGroupId); 
         //FetchByLocationIds 
         void Insert(LocationDto dto); 
         void Update(LocationDto dto); 
         void Upsert(IEnumerable<LocationDto> dtoList, LocationIsSetDto isSetDto, Boolean updateDeleted);
    }
}