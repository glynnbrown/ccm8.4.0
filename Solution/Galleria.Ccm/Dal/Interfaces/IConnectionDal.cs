﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25559 : L.Ineson
//		Copied from SA
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchAll")]
    public interface IConnectionDal : IDal
    {
        IEnumerable<ConnectionDto> FetchAll();
        void Insert(ConnectionDto dto);
        void Update(ConnectionDto dto);
    }
}
