﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26159 : L.Ineson
//	Created (Auto-generated)
// V8-26773 : N.Foster
//  Added FetchByName
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// Defines the expected memebers of a PerformanceSelection dal class
    /// </summary>
    public interface IPerformanceSelectionDal : IDal
    {
        PerformanceSelectionDto FetchById(Int32 id);
        PerformanceSelectionDto FetchByEntityIdName(Int32 entityId, String name);
        void Insert(PerformanceSelectionDto dto);
        void Update(PerformanceSelectionDto dto);
        void DeleteById(Int32 id);
    }
}
