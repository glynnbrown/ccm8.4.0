﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History : (CCM CCM800)

// CCM800-26953 : I.George
//   Initial Version

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// Defines the expected members of the performanceSelectionMetricDal class
    /// </summary>
    [DefaultFetchMethod("FetchByPerformanceSelectionId", "PerformanceSelectionId")]
    public interface IPerformanceSelectionMetricDal : IDal
    {
        IEnumerable<PerformanceSelectionMetricDto> FetchByPerformanceSelectionId(Int32 performanceSelectionId);
        void Insert(PerformanceSelectionMetricDto dto);
        void Update(PerformanceSelectionMetricDto dto);
        void DeleteById(Int32 id);
    }
}
