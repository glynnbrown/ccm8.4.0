﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// The sync source dal interface
    /// </summary>
    public interface ISyncTargetGfsPerformanceDal : IDal
    {
        SyncTargetGfsPerformanceDto FetchById(Int32 id);
        IEnumerable<SyncTargetGfsPerformanceDto> FetchBySyncTargetId(Int32 syncTargetId);
        void Insert(SyncTargetGfsPerformanceDto dto);
        void Update(SyncTargetGfsPerformanceDto dto);
        void DeleteById(Int32 id);
        void DeleteBySyncTargetId(Int32 syncTargetId);
    }
}