﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
// V8-25455 : J.Pickup
//		Added Fetch by ID
#endregion
#region Version History: (CCM 8.0.3)
// V8-29215 : D.Pleasance
//  Removed FetchAllIncludingDeleted
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IConsumerDecisionTreeInfoDal : IDal
    {
        IEnumerable<ConsumerDecisionTreeInfoDto> FetchByEntityId(Int32 entityId);
        IEnumerable<ConsumerDecisionTreeInfoDto> FetchByProductGroupId(Int32 productGroupId);
        ConsumerDecisionTreeInfoDto FetchById(Int32 consumerDecisionTreeId);
    }
}
