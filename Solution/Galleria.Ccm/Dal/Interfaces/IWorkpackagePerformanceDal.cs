﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26773 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IWorkpackagePerformanceDal : IDal
    {
        void Insert(WorkpackagePerformanceDto dto);
        void DeleteByWorkpackageId(Int32 workpackageId);
    }
}
