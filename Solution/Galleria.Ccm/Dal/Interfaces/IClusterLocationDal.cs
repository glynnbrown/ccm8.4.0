﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629 : A.Kuszyk
//		Created (copied from SA)
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IClusterLocationDal : IDal
    {
        ClusterLocationDto FetchById(Int32 id);
        IEnumerable<ClusterLocationDto> FetchByClusterId(Int32 clusterId);
        void Insert(ClusterLocationDto dto);
        void Update(ClusterLocationDto dto);
        void Upsert(IEnumerable<ClusterLocationDto> dtoList, ClusterLocationIsSetDto isSetDto);
        void DeleteById(Int32 id);
    }
}
