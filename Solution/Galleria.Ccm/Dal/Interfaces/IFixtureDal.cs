﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByFixturePackageId", "FixturePackageId")]
    public interface IFixtureDal : IDal
    {
        IEnumerable<FixtureDto> FetchByFixturePackageId(Object fixturePackageId);
        void Insert(FixtureDto dto);
        void Update(FixtureDto dto);
        void DeleteById(Int32 id);
    }
}
