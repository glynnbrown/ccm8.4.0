﻿using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.Interfaces
{ 
    public interface IWorkpackagePlanogramProcessingStatusDal : IDal
    {
        IEnumerable<WorkpackagePlanogramProcessingStatusDto> FetchByPlanogramId(Int32 planogramId);

        WorkpackagePlanogramProcessingStatusDto FetchByPlanogramIdWorkpackageId(Int32 workpackageId, Int32 planogramId);

        void Update(WorkpackagePlanogramProcessingStatusDto dto);

        void AutomationStatusIncrement(Int32 planogramId, Int32 workpackageId, String statusDescription, DateTime dateLastUpdated);
    }
}
