﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByFixtureAssemblyId", "FixtureAssemblyId")]
    public interface IFixtureAssemblyComponentDal : IDal
    {
        IEnumerable<FixtureAssemblyComponentDto> FetchByFixtureAssemblyId(Int32 fixtureAssemblyId);
        void Insert(FixtureAssemblyComponentDto dto);
        void Update(FixtureAssemblyComponentDto dto);
        void DeleteById(Int32 id);
    }
}
