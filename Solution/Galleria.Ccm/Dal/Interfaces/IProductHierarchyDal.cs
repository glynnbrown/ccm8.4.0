﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchById", "Id")]
    public interface IProductHierarchyDal : IDal
    {
        ProductHierarchyDto FetchById(Int32 id);
        ProductHierarchyDto FetchByEntityId(Int32 entityId);
        void Insert(ProductHierarchyDto dto);
        void Update(ProductHierarchyDto dto);
        void DeleteById(Int32 id);
    }
}
