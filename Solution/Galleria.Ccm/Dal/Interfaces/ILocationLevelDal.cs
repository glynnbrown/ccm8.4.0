﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchById", "Id")]
    public interface ILocationLevelDal : IDal
    {
        void DeleteById(Int32 id);
        LocationLevelDto FetchById(Int32 id);
        IEnumerable<LocationLevelDto> FetchByLocationHierarchyId(Int32 locationHierarchyId);
        void Insert(LocationLevelDto dto);
        void Update(LocationLevelDto dto);
    }
}
