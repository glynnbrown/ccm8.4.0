#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26076 : A.Silva ~ DeleteById(Int32 id) changed to DeleteByGrouping(String columnLayoutId, String grouping).

#endregion
#region Version History: (CCM 8.3)
//V8-31542 : L.Ineson
//  Changed delete back to DeleteById(Int32 id)
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface ICustomColumnGroupDal : IDal
    {
        IEnumerable<CustomColumnGroupDto> FetchByCustomColumnLayoutId(Object columnLayoutId);
        void Insert(CustomColumnGroupDto dto);
        void Update(CustomColumnGroupDto dto);
        void DeleteById(Int32 id);
    }
}