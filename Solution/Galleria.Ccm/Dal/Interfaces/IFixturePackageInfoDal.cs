﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByIds", "Id")]
    public interface IFixturePackageInfoDal : IDal
    {
        IEnumerable<FixturePackageInfoDto> FetchByIds(IEnumerable<Object> ids);
        void SetFolderId(Object fixturePackageId, Object folderId);
    }
}
