﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{

    [DefaultFetchMethod("FetchByHighlightCharacteristicId", "HighlightCharacteristicId")]
    public interface IHighlightCharacteristicRuleDal : IDal
    {
        IEnumerable<HighlightCharacteristicRuleDto> FetchByHighlightCharacteristicId(Int32 highlightCharacteristicId);
        void Insert(HighlightCharacteristicRuleDto dto);
        void Update(HighlightCharacteristicRuleDto dto);
        void DeleteById(Int32 id);
    }
}
