﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27411 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IPlanogramLockDal : IDal
    {
        Boolean DeleteByLockTypeDateLocked(Int32 userId, Byte lockType, DateTime dateLocked);
    }
}
