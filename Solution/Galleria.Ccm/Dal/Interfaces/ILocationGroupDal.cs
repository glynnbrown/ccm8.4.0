﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchById", "Id")]
    public interface ILocationGroupDal : IDal
    {
        LocationGroupDto FetchById(Int32 id);
        IEnumerable<LocationGroupDto> FetchByLocationHierarchyId(Int32 locationHierarchyId);
        IEnumerable<LocationGroupDto> FetchByLocationHierarchyIdIncludingDeleted(Int32 locationHierarchyId);
        void Insert(LocationGroupDto dto);
        void Update(LocationGroupDto dto);
        void Upsert(IEnumerable<LocationGroupDto> dtoList, LocationGroupIsSetDto isSetDto);
        void DeleteById(Int32 id);
    }
}
