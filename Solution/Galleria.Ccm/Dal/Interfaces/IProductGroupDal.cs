﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchById", "Id")]
    public interface IProductGroupDal : IDal
    {
        ProductGroupDto FetchById(Int32 id);
        IEnumerable<ProductGroupDto> FetchByProductHierarchyId(Int32 productHierarchyId);
        IEnumerable<ProductGroupDto> FetchByProductHierarchyIdIncludingDeleted(Int32 productHierarchyId);
        void Insert(ProductGroupDto dto);
        void Update(ProductGroupDto dto);
        void Upsert(IEnumerable<ProductGroupDto> dtoList, ProductGroupIsSetDto isSetDto);
        void DeleteById(Int32 id);
    }
}
