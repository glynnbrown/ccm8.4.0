﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-28258 : N.Foster
//	Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// Defines the operations available for a task library
    /// </summary>
    public interface ITaskLibraryDal : IDal
    {
        TaskLibraryDto FetchByFullName(String fullName);
        void Insert(TaskLibraryDto dto);
        void Update(TaskLibraryDto dto);
        void DeleteById(Int32 id);
    }
}
