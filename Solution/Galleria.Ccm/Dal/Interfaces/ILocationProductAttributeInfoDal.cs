﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
// V8-27630 : I.George
// Removed Interfaces that are not used
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByEntityId", "EntityId")]
    public interface ILocationProductAttributeInfoDal : IDal
    {
        List<LocationProductAttributeInfoDto> FetchByEntityId(Int32 entityId);
        List<LocationProductAttributeInfoDto> FetchByEntityIdSearchCriteria(Int32 entityId, String searchCriteria);
    }
}
