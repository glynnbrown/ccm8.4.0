﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IAssortmentLocationBuddyDal : IDal
    {
        IEnumerable<AssortmentLocationBuddyDto> FetchByAssortmentId(Int32 assortmentId);
        AssortmentLocationBuddyDto FetchById(Int32 id);
        void Insert(AssortmentLocationBuddyDto dto);
        void Update(AssortmentLocationBuddyDto dto);
        void DeleteById(Int32 id);
    }
}
