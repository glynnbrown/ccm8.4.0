﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-V8-24261 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IRecentPlanogramDal : IDal
    {
        IEnumerable<RecentPlanogramDto> FetchAll();
        RecentPlanogramDto FetchById(Object id);
        void Insert(RecentPlanogramDto dto);
        void Update(RecentPlanogramDto dto);
        void DeleteById(Object id);
    }
}
