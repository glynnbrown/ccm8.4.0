﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IAssortmentInventoryRuleDal : IDal
    {
        IEnumerable<AssortmentInventoryRuleDto> FetchByAssortmentId(Int32 assortmentId);
        AssortmentInventoryRuleDto FetchById(Int32 id);
        void Insert(AssortmentInventoryRuleDto dto);
        void Update(AssortmentInventoryRuleDto dto);
        void DeleteById(Int32 id);
    }
}
