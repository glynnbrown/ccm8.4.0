﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25669 : A.Kuszyk
//		Created
// CCM-25452 : N.Haywood
//	    Added FetchByEntityId
// V8-25453 : A.Kuszyk
//      Added FetchByEntityIdProductIds
// CCM-25447 : N.Haywood
//      Added FetchByParentProductGroupId
// CCM-26099 :I.George
//     Added FetchByMerchandisingGroupId
#endregion
#region Version History: (CCM 8.1.0)
// V8-29667 : N.Haywood
//  Removed FetchAllIncludingDeleted
#endregion
#region Version History: (CCm 8.3.0)
// V8-31835 : N.Haywood
//  Added Illegal and Legal MetaData and Validation
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IProductInfoDal : IDal
    {
        IEnumerable<ProductInfoDto> FetchByEntityIdSearchCriteria(Int32 entityId, String searchCriteria);
        IEnumerable<ProductInfoDto> FetchByEntityIdProductGtins(Int32 entityId, IEnumerable<String> productGtins);
        List<ProductInfoDto> FetchByProductUniverseId(Int32 productUniverseId);
        IEnumerable<ProductInfoDto> FetchByProductIds(IEnumerable<Int32> productIds);
        IEnumerable<ProductInfoDto> FetchByEntityId(Int32 entityId);
        IEnumerable<ProductInfoDto> FetchByEntityIdProductIds(Int32 entityId, IEnumerable<Int32> productIds);
        IEnumerable<ProductInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId);
        IEnumerable<ProductInfoDto> FetchByMerchandisingGroupId(Int32 merchGroupId);
        //IEnumerable<ProductInfoDto> FetchAllIncludingDeleted();
        List<ProductInfoDto> FetchIllegalsByLocationCodeProductGroupCode(String locationCode, String productGroupCode);
        List<ProductInfoDto> FetchLegalsByLocationCodeProductGroupCode(String locationCode, String productGroupCode);
    }
}
