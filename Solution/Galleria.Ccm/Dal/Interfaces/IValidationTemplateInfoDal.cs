﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26401 : A.Silva ~ Created.

#endregion

#region Version History: (CCM v8.03)
// V8-29219 : L.Ineson
//  Removed FetchByEntityIdIncludingDeleted
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByEntityId", "EntityId")]
    public interface IValidationTemplateInfoDal : IDal
    {
        IEnumerable<ValidationTemplateInfoDto> FetchByEntityId(Int32 entityId);
    }
}
