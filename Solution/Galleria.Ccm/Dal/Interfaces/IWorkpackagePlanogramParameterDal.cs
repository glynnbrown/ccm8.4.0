﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
// V8-28232 : N.Foster
//  Added support for batch operations
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByWorkpackagePlanogramId", "WorkpackagePlanogramId")]
    public interface IWorkpackagePlanogramParameterDal : IBatchDal<WorkpackagePlanogramParameterDto>
    {
        IEnumerable<WorkpackagePlanogramParameterDto> FetchByWorkpackagePlanogramId(Int32 workpackagePlanogramId);
        void Insert(WorkpackagePlanogramParameterDto dto);
        void Update(WorkpackagePlanogramParameterDto dto);
        void DeleteById(Int32 id);
    }
}
