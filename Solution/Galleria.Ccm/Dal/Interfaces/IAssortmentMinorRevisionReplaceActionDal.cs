﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IAssortmentMinorRevisionReplaceActionDal : IDal
    {
        AssortmentMinorRevisionReplaceActionDto FetchById(Int32 id);
        IEnumerable<AssortmentMinorRevisionReplaceActionDto> FetchByAssortmentMinorRevisionId(Int32 assortmentMinorRevisionId);
        void Insert(AssortmentMinorRevisionReplaceActionDto dto);
        void Update(AssortmentMinorRevisionReplaceActionDto dto);
        void DeleteById(Int32 id);
    }
}
