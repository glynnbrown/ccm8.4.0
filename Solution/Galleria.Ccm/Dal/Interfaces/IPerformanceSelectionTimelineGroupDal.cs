﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26159 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// Defines the expected memebers of a PerformanceSelectionTimelineGroup dal class
    /// </summary>
    [DefaultFetchMethod("FetchByPerformanceSelectionId", "PerformanceSelectionId")]
    public interface IPerformanceSelectionTimelineGroupDal : IDal
    {
        IEnumerable<PerformanceSelectionTimelineGroupDto> FetchByPerformanceSelectionId(Int32 performanceSelectionId);
        void Insert(PerformanceSelectionTimelineGroupDto dto);
        void Update(PerformanceSelectionTimelineGroupDto dto);
        void DeleteById(Int32 id);
    }
}
