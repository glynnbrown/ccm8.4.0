﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface ILocationSpaceSearchCriteriaDal : IDal
    {
        IEnumerable<LocationSpaceSearchCriteriaDto> FetchByEntityId(Int32 entityId);
    }

    public interface ILocationSpaceDal : IDal
    {

        /// <summary>
        /// Returns Location Space by ID
        /// </summary>
        /// <returns></returns>
        LocationSpaceDto FetchById(Int32 id);

        /// <summary>
        /// Inserts the given dto
        /// </summary>
        /// <param name="dto"></param>
        void Insert(LocationSpaceDto dto);


        /// <summary>
        /// Updates the given dto
        /// </summary>
        /// <param name="dto"></param>
        void Update(LocationSpaceDto dto);


        /// <summary>
        /// Delete the given Location Space ID
        /// </summary>
        /// <param name="Int32"></param>
        void DeleteById(Int32 id);

        /// <summary>
        /// Delete all location space by entity ID
        /// </summary>
        /// <param name="entityId"></param>
        void DeleteByEntityId(Int32 entityId);

        /// <summary>
        /// Upsert collection of location space dto's
        /// </summary>
        /// <param name="dtoList"></param>
        void Upsert(IEnumerable<LocationSpaceDto> dtoList);
    }
}
