#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26076 : A.Silva ~ DeleteById(Int32 id) changed to DeleteByPath(String columnLayoutId, String path).

#endregion
#region Version History: (CCM 8.3)
//V8-31542 : L.Ineson
//  Changed delete back to DeleteById(Int32 id)
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    ///     Interface extending <see cref="IDal" /> describing the functionality of any DAL for custom columns.
    /// </summary>
    [DefaultFetchMethod("FetchByColumnLayoutId", "ColumnLayoutId")]
    public interface ICustomColumnDal : IDal
    {
        IEnumerable<CustomColumnDto> FetchByColumnLayoutId(Object columnLayoutId);
        void Insert(CustomColumnDto dto);
        void Update(CustomColumnDto dto);
        void DeleteById(Int32 id);
    }
}