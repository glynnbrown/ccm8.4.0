﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
// V8 25556 : J.Pickup
//	    Introduced FetchAllIncludingDeleted()
#endregion
#region Version History: (CCM 8.0.3)
// V8-29215 : D.Pleasance
//  Renamed FetchAllIncludingDeleted to FetchAll, we dont keep deleted records anymore
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IConsumerDecisionTreeNodeDal : IDal
    {
        ConsumerDecisionTreeNodeDto FetchById(Int32 id);
        IEnumerable<ConsumerDecisionTreeNodeDto> FetchByConsumerDecisionTreeId(Int32 consumerDecisionTreeId);
        IEnumerable<ConsumerDecisionTreeNodeDto> FetchAll();
        void Insert(ConsumerDecisionTreeNodeDto dto);
        void Update(ConsumerDecisionTreeNodeDto dto);
        void DeleteById(Int32 id);
    }
}
