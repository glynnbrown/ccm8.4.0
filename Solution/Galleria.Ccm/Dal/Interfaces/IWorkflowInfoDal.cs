﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25546 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByEntityIdIncludingDeleted", "EntityId")]
    public interface IWorkflowInfoDal : IDal
    {
        IEnumerable<WorkflowInfoDto> FetchByEntityId(Int32 entityId);
        IEnumerable<WorkflowInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId);
        WorkflowInfoDto FetchById(Int32 id);
    }
}
