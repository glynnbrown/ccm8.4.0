﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25879 : N.Haywood
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface ILocationPlanAssignmentDal : IDal
    {
        LocationPlanAssignmentDto FetchById(Int32 id);
        IEnumerable<LocationPlanAssignmentDto> FetchByPlanogramId(Int32 planId);
        IEnumerable<LocationPlanAssignmentDto> FetchByProductGroupId(Int32 productGroupId);
        IEnumerable<LocationPlanAssignmentDto> FetchByLocationId(Int16 locationId);
        void Insert(LocationPlanAssignmentDto dto);
        void DeleteById(Int32 id);
        void Update(LocationPlanAssignmentDto dto);
    }
}
