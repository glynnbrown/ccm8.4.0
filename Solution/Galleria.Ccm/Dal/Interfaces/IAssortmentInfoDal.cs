﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM CCM800
// CCM-25454 : J.Pickup
//		Created (Auto-generated)
// CCM-25455 : J.Pickup
//		Added: FetchByEntityIdAssortmentIds
// CCM-26140 : I.George
//     Added FetchByEntityIdIncludingDeleted
// CCM-26520 : J.Pickup
//     Added FetchByProductGroupId
#endregion

#region Version History: CCM803
// V8-29134 : D.Pleasance
//  Removed FetchByEntityIdIncludingDeleted since we now deleted data outright
#endregion 

#region Version History: CCM8.2.0
// V8-30461 : M.Shelley
//  Added Entity_Id to FetchByProductGroupId
#endregion

#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IAssortmentInfoDal : IDal
    {
        IEnumerable<AssortmentInfoDto> FetchByEntityId(Int32 entityId);
        IEnumerable<AssortmentInfoDto> FetchByProductGroupId(Int32 entityId, Int32 productGroupId);
        IEnumerable<AssortmentInfoDto> FetchByEntityIdAssortmentSearchCriteria(Int32 entityId, String assortmentSearchCritiera);
        IEnumerable<AssortmentInfoDto> FetchByEntityIdAssortmentIds(Int32 entityId, IEnumerable<Int32> assortmentIds);
    }
}
