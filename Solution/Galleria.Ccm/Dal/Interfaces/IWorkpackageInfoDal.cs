﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
// V8-25757 : L.Ineson
//  Removed PlanogramGroup_Id link
//  Added Entity_Id link
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByEntityId", "EntityId")]
    public interface IWorkpackageInfoDal : IDal
    {
        IEnumerable<WorkpackageInfoDto> FetchByEntityId(Int32 entityId);
        WorkpackageInfoDto FetchById(Int32 id);
    }
}
