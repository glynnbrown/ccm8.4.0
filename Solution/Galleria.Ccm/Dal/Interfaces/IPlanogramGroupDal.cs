﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25587 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchById", "Id")]
    public interface IPlanogramGroupDal : IDal
    {
        PlanogramGroupDto FetchById(Int32 id);
        IEnumerable<PlanogramGroupDto> FetchByPlanogramHierarchyId(Int32 planogramHierarchyId);
        void Insert(PlanogramGroupDto dto);
        void Update(PlanogramGroupDto dto);
        void Upsert(IEnumerable<PlanogramGroupDto> dtoList, PlanogramGroupIsSetDto isSetDto);
        void DeleteById(Int32 id);
    }
}
