﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-V8-26306 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchAll")]
    public interface IUserEditorSettingDal : IDal
    {
        IEnumerable<UserEditorSettingDto> FetchAll();
        void Insert(UserEditorSettingDto dto);
        void Update(UserEditorSettingDto dto);
        void DeleteById(Int32 id);
    }
}
