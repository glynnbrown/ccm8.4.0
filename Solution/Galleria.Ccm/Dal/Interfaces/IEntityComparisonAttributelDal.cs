using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByEntityId", "EntityId")]
    public interface IEntityComparisonAttributeDal : IDal
    {
        /// <summary>
        ///     Fetch a <c>Data Transfer Object</c> from the <c>DAL</c> for a <c>Planogram Comparison</c> model object that belongs to a <c>Planogram</c> with the given<paramref name="id"/>.
        /// </summary>
        /// <param name="id"><see cref="Object"/> containing the id of the parent <c>Planogram</c>.</param>
        /// <returns>A new instance of <see cref="EntityComparisonAttributeDto"/>.</returns>
        IEnumerable <EntityComparisonAttributeDto> FetchByEntityId(Object id);

        /// <summary>
        ///     Insert the data contained in the given <paramref name="dto"/> into the <c>DAL</c>.
        /// </summary>
        /// <param name="dto"></param>
        new void Insert(EntityComparisonAttributeDto dto);

        /// <summary>
        ///     Update the data contained in the given <paramref name="dto"/> into the <c>DAL</c>.
        /// </summary>
        /// <param name="dto"></param>
        new void Update(EntityComparisonAttributeDto dto);

        /// <summary>
        ///     Delete the data linked to the given <paramref name="id"/> into the <c>DAL</c>.
        /// </summary>
        /// <param name="id"></param>
        void DeleteById(Object id);
    }
}