﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25587 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchById", "Id")]
    public interface IPlanogramHierarchyDal : IDal
    {
        PlanogramHierarchyDto FetchById(Int32 id);
        PlanogramHierarchyDto FetchByEntityId(Int32 entityId);
        void Insert(PlanogramHierarchyDto dto);
        void Update(PlanogramHierarchyDto dto);
        void DeleteById(Int32 id);
        Dictionary<Guid, Guid> FetchPlanogramPlanogramGroupGroupings(Int32 entityId);
    }
}
