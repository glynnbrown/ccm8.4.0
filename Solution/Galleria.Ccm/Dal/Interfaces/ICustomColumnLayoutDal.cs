#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface ICustomColumnLayoutDal : IDal
    {
        void DeleteById(Object id);
        //IEnumerable<CustomColumnLayoutDto> FetchAll();
        CustomColumnLayoutDto FetchById(Object customColumnLayoutId);
        void Insert(CustomColumnLayoutDto dto);
        void Update(CustomColumnLayoutDto dto);

        void LockById(Object id);
        void UnlockById(Object id);
    }
}