﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26895 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  Renamed Increment to AutomationIncrement
//  Added status description, date last updated
//  Added MetaData, Validation increment methods
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IPlanogramProcessingStatusDal : IDal
    {
        PlanogramProcessingStatusDto FetchByPlanogramId(Int32 planogramId);
        void Update(PlanogramProcessingStatusDto dto);
        void MetaDataStatusIncrement(Int32 planogramId, String statusDescription, DateTime dateLastUpdated);
        void ValidationStatusIncrement(Int32 planogramId, String statusDescription, DateTime dateLastUpdated);
    }
}
