﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IPlanogramGroupPlanogramDal : IDal
    {
        void Upsert(IEnumerable<PlanogramGroupPlanogramDto> dtos);
    }
}
