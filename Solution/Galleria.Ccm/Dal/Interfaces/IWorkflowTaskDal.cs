﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25546 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByWorkflowId", "WorkflowId")]
    public interface IWorkflowTaskDal : IDal
    {
        IEnumerable<WorkflowTaskDto> FetchByWorkflowId(Int32 workflowId);
        void Insert(WorkflowTaskDto dto);
        void Update(WorkflowTaskDto dto);
        void DeleteById(Int32 id);
    }
}
