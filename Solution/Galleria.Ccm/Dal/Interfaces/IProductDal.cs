﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25586 : A.Probyn
//		Created
// CCM-25452 : N.Haywood
//	    Added FetchByEntityId, DeleteByEntityId, FetchByProductGroupId and FetchByEntityIdProductIds
// V8-25453 : A.Kuszyk
//      Added FetchByEntityIdProductIds.
// V8-25556 : D.Pleasance
//      Added FetchByEntityIdProductGtins
// V8-27150 : L.Ineson
//  Added FetchByMerchandisingGroupId & FetchByEntityIdSearchText
#endregion
#region Version History: (CCM 8.3.0)
// V8-32356 : N.Haywood
//  Added FetchByEntityIdMultipleSearchText
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IProductDal : IDal
    {
        ProductDto FetchById(Int32 id);
        IEnumerable<ProductDto> FetchByEntityId(Int32 entityId);
        void Insert(ProductDto dto);
        void Upsert(IEnumerable<ProductDto> dtoList, ProductIsSetDto isSetDto);
        void Update(ProductDto dto);
        void DeleteById(Int32 id);
        void DeleteByEntityId(Int32 entityId);
        IEnumerable<ProductDto> FetchByProductGroupId(Int32 productGroupId);
        IEnumerable<ProductDto> FetchByProductIds(IEnumerable<Int32> productIds);
        IEnumerable<ProductDto> FetchByProductUniverseId(Int32 productUniverseId);
        IEnumerable<ProductDto> FetchByEntityIdProductGtins(Int32 entityId, IEnumerable<String> productGtins);
        IEnumerable<ProductDto> FetchByMerchandisingGroupId(Int32 merchGroupId);
        IEnumerable<ProductDto> FetchByEntityIdSearchText(Int32 entityId, String searchText);
        IEnumerable<ProductDto> FetchByEntityIdMultipleSearchText(Int32 entityId, IEnumerable<String> searchText);
    }
}
