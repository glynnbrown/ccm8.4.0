#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByIds", "Id")]
    public interface ICustomColumnLayoutInfoDal : IDal
    {
        IEnumerable<CustomColumnLayoutInfoDto> FetchByIds(IEnumerable<object> ids);
        IEnumerable<CustomColumnLayoutInfoDto> FetchByType(IEnumerable<Object> ids, Byte type);
    }
}