﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created (Auto-generated)
// V8-27241 : A.Kuszyk
//      Added FetchByEntityIdName.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IBlockingDal : IDal
    {
        BlockingDto FetchById(Int32 id);
        BlockingDto FetchByEntityIdName(Int32 entityId, String name);
        void Insert(BlockingDto dto);
        void Update(BlockingDto dto);
        void DeleteById(Int32 id);
    }
}
