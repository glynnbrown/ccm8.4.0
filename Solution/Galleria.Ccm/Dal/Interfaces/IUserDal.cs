﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-25664 : A.Kuszyk
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IUserDal : IDal
    {
        UserDto FetchById(Int32 id);
        UserDto FetchByUserName(String userName);
        void Insert(UserDto dto);
        void Update(UserDto dto);
        void DeleteById(Int32 id);
    }
}
