﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface ICompressionDal : IDal
    {
        CompressionDto FetchById(Int32 id);
        List<CompressionDto> FetchAll();
        List<CompressionDto> FetchAllEnabled();
        void Insert(CompressionDto dto);
        void Update(CompressionDto dto);
        void DeleteById(Int32 id);
    }
}
