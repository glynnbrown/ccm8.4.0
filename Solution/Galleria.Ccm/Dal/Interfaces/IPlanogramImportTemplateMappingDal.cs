﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-27804 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// Denotes the expected memebers of a PlanogramImportTemplateMapping Dal
    /// </summary>
    [DefaultFetchMethod("FetchByPlanogramImportTemplateId", "PlanogramImportTemplateId")]
    public interface IPlanogramImportTemplateMappingDal : IDal
    {
        IEnumerable<PlanogramImportTemplateMappingDto> FetchByPlanogramImportTemplateId(Object planogramImportTemplateId);
        void Insert(PlanogramImportTemplateMappingDto dto);
        void Update(PlanogramImportTemplateMappingDto dto);
        void DeleteById(Int32 id);
    }
}
