﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-V8-24863 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface ISystemSettingsDal : IDal
    {
        SystemSettingsDto FetchById(Int32 id);
        IEnumerable<SystemSettingsDto> FetchByEntityId(Int32 entityId);
        IEnumerable<SystemSettingsDto> FetchDefault();
        void Insert(SystemSettingsDto dto);
        void Update(SystemSettingsDto dto);
        void DeleteById(Int32 id);
    }
   
}
