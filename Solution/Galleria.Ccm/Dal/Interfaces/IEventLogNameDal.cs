﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : Luong
//   Created (Copied From GFS)
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IEventLogNameDal : IDal
    {
        /// <summary>
        /// Returns All Event Log
        /// </summary>
        /// <returns></returns>
        IEnumerable<EventLogNameDto> FetchAll();

        /// <summary>
        /// Returns All Event Log
        /// </summary>
        /// <returns></returns>
        EventLogNameDto FetchById(Int32 id);

        /// <summary>
        /// Returns All Event Log
        /// </summary>
        /// <returns></returns>
        EventLogNameDto FetchByName(String name);

        /// <summary>
        /// Inserts the given dto
        /// </summary>
        /// <param name="dto"></param>
        void Insert(EventLogNameDto dto);

    }
}

