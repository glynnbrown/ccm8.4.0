﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
// V8-25556 : J.Pickup
//  Added FetchAllIncludingDeleted
#endregion
#region Version History: (CCM 8.0.3)
// V8-29214 : D.Pleasance
//  Renamed FetchAllIncludingDeleted to FetchAll as we now deleted data outright
#endregion 
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IAssortmentMinorRevisionDal : IDal
    {
        AssortmentMinorRevisionDto FetchById(Int32 id);
        AssortmentMinorRevisionDto FetchByEntityIdName(Int32 entityId, String name);
        IEnumerable<AssortmentMinorRevisionDto> FetchAll();
        void Insert(AssortmentMinorRevisionDto dto);
        void Update(AssortmentMinorRevisionDto dto);
        void DeleteById(Int32 id);
    }
}
