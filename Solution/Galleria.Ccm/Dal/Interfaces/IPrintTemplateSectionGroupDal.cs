﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// Denotes the expected members of a PrintTemplateSectionGroup Dal
    /// </summary>
    public interface IPrintTemplateSectionGroupDal : IDal
    {
        PrintTemplateSectionGroupDto FetchById(Int32 id);
        IEnumerable<PrintTemplateSectionGroupDto> FetchByPrintTemplateId(Object printTemplateId);
        void Insert(PrintTemplateSectionGroupDto dto);
        void Update(PrintTemplateSectionGroupDto dto);
        void DeleteById(Int32 id);
    }
}
