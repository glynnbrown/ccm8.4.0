﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630 : A.Kuszyk
//  Created (copied from SA).
// V8-26560 : A.Probyn
//  Added FetchByProductUniverseIds inline with ProductGroupInfo
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IProductUniverseInfoDal : IDal
    {
        IEnumerable<ProductUniverseInfoDto> FetchByEntityId(Int32 entityId);
        IEnumerable<ProductUniverseInfoDto> FetchByProductUniverseIds(IEnumerable<Int32> productUniverseIds);
        IEnumerable<ProductUniverseInfoDto> FetchByProductGroupId(Int32 productGroupId);
    }
}
