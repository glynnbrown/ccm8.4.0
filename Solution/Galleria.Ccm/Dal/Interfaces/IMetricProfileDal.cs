﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26123 : L.Ineson
//		Created (Auto-generated)
// V8-27548 : A.Kuszyk
//  Added FetchByEntityIdName.
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IMetricProfileDal : IDal
    {
        void DeleteById(Int32 id);
        MetricProfileDto FetchById(Int32 id);
        MetricProfileDto FetchByEntityIdName(Int32 entityId, String name);
        void Insert(MetricProfileDto dto);
        void Update(MetricProfileDto dto);
    }
}
