﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25559 : L.Hodson
//		Created (Auto-generated)#
// V8-25556 : D.Pleasance
//      Added FetchAll
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchById", "Id")]
    public interface IEntityDal : IDal
    {
        IEnumerable<EntityDto> FetchAll();
        EntityDto FetchById(Int32 id);
        EntityDto FetchDeletedByName(String name);
        void Insert(EntityDto dto);
        void Update(EntityDto dto);
        void DeleteById(Int32 id);
    }
}