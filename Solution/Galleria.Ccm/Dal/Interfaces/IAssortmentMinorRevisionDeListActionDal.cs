﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IAssortmentMinorRevisionDeListActionDal : IDal
    {
        AssortmentMinorRevisionDeListActionDto FetchById(Int32 id);
        IEnumerable<AssortmentMinorRevisionDeListActionDto> FetchByAssortmentMinorRevisionId(Int32 assortmentMinorRevisionContentId);
        void Insert(AssortmentMinorRevisionDeListActionDto dto);
        void Update(AssortmentMinorRevisionDeListActionDto dto);
        void DeleteById(Int32 id);
    }
}
