﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-25629 : A.Kuszyk
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IClusterSchemeDal : IDal
    {
        ClusterSchemeDto FetchById(Int32 id);
        IEnumerable<ClusterSchemeDto> FetchByEntityId(Int32 entityId);
        void Insert(ClusterSchemeDto dto);
        void Update(ClusterSchemeDto dto);
        void Upsert(IEnumerable<ClusterSchemeDto> dtoList, ClusterSchemeIsSetDto isSetDto);
        void DeleteById(Int32 id);
        void DeleteByEntityId(Int32 entityId);
    }
}
