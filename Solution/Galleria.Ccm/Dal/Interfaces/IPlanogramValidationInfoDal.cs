﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26944 : A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    ///     Interface for the <see cref="PlanogramValidationInfoDto"/> dal.
    /// </summary>
    public interface IPlanogramValidationInfoDal : IDal
    {
        /// <summary>
        ///     Fetches a collection of <see cref="PlanogramValidationInfoDto"/> instances matching the given <paramref name="planogramIds"/>.
        /// </summary>
        /// <param name="planogramIds">A collection of <see cref="Int32"/> values that must be matched by the returned dtos.</param>
        /// <returns>A new collection of <see cref="PlanogramValidationInfoDto"/>.</returns>
        IEnumerable<PlanogramValidationInfoDto> FetchPlanogramValidationsByPlanogramIds(IEnumerable<Int32> planogramIds);
    }
}
