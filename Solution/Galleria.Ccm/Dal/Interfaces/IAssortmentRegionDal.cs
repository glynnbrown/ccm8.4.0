﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25454 : J.Pickup
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IAssortmentRegionDal : IDal
    {
        IEnumerable<AssortmentRegionDto> FetchByAssortmentId(Int32 assortmentId);
        AssortmentRegionDto FetchById(Int32 id);
        void Insert(AssortmentRegionDto dto);
        void Update(AssortmentRegionDto dto);
        void DeleteById(Int32 id);
    }
}
