﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM803
// CCM-29606 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IEngineMaintenanceDal : IDal
    {
        IEnumerable<Int32> FetchPlanogramsByMetadataCalculationRequired();
        IEnumerable<Int32> FetchPlanogramsByValidationCalculationRequired();
        IEnumerable<Int32> FetchPlanogramsByDeletionRequired();
    }
}
