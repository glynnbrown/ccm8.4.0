﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26895 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  Added statusDescription, and dateLastUpdated parameters to increment command
#endregion

#region Version History: CCM8.2.0
// V8-30646 : M.Shelley
//  Added SetPending method 
#endregion

#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IWorkpackageProcessingStatusDal : IDal
    {
        WorkpackageProcessingStatusDto FetchByWorkpackageId(Int32 workpackageId);
        void Initialize(WorkpackageProcessingStatusDto dto);
        void Update(WorkpackageProcessingStatusDto dto);
        void Increment(Int32 workpackageId, String statusDescription, DateTime dateLastUpdated);
        void SetPending(Int32 workpackageId, Byte processingStatus, String statusDescription);
    }
}
