﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface ILocationSpaceProductGroupDal : IDal
    {
        /// <summary>
        /// Returns Location Space Product Group by ID
        /// </summary>
        /// <returns></returns>
        LocationSpaceProductGroupDto FetchById(Int32 id);

        /// <summary>
        /// Returns List of Location Spaces by location space id
        /// </summary>
        /// <returns></returns>
        IEnumerable<LocationSpaceProductGroupDto> FetchByLocationSpaceId(Int32 locationSpaceId);

        /// <summary>
        /// Inserts the given dto
        /// </summary>
        /// <param name="dto"></param>
        void Insert(LocationSpaceProductGroupDto dto);

        /// <summary>
        /// Inserts a list of given dtos
        /// </summary>
        /// <param name="dtoList"></param>
        void Insert(IEnumerable<LocationSpaceProductGroupDto> dtoList);

        /// <summary>
        /// Updates the given dto
        /// </summary>
        /// <param name="dto"></param>
        void Update(LocationSpaceProductGroupDto dto);

        /// <summary>
        /// Delete the given Location Space Product Group ID
        /// </summary>
        /// <param name="Int32"></param>
        void DeleteById(Int32 id);

        /// <summary>
        /// Delete the given Location Space Product Group ID by Location space id
        /// </summary>
        /// <param name="Int32"></param>
        void DeleteByLocationSpaceId(Int32 locationSpaceId);

        /// <summary>
        /// Upsert collection of location space product group dto's
        /// </summary>
        /// <param name="dtoList"></param>
        void Upsert(IEnumerable<LocationSpaceProductGroupDto> dtoList);
    }
}
