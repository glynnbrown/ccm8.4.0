﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchById", "LocationId", "ProductId")]
    [DefaultDeleteMethod("DeleteById", "LocationId", "ProductId")]
    public interface ILocationProductLegalDal : IDal
    {
        LocationProductLegalDto FetchById(Int16 locationId, Int32 productId);
        IEnumerable<LocationProductLegalDto> FetchByEntityId(Int32 entityId);
        void Insert(LocationProductLegalDto dto);
        void Update(LocationProductLegalDto dto);
        void Upsert(IEnumerable<LocationProductLegalDto> dtoList);
        void DeleteById(Int16 locationId, Int32 productId);
        void DeleteByEntityId(Int32 entityId);
        List<LocationProductLegalDto> FetchByLocationIdProductIdCombinations(IEnumerable<Tuple<Int16, Int32>> locationProductIdCombinations);
    }

    /// <summary>
    /// Struct used for searching with location code and product gtin as criteria
    /// </summary>
    public struct LocationProductLegalCodeSearchCriteria : IEquatable<LocationProductLegalCodeSearchCriteria>
    {
        public String Code { get; set; }
        public String GTIN { get; set; }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + this.Code.ToLowerInvariant().GetHashCode();
                hash = hash * 23 + this.GTIN.ToLowerInvariant().GetHashCode();
                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is LocationProductLegalCodeSearchCriteria)
            {
                return this.Equals((LocationProductLegalCodeSearchCriteria)obj);
            }
            return false;
        }

        public bool Equals(LocationProductLegalCodeSearchCriteria other)
        {
            StringComparer s = StringComparer.OrdinalIgnoreCase;

            if (s.Compare(other.Code, this.Code) != 0) { return false; }
            if (s.Compare(other.GTIN, this.GTIN) != 0) { return false; }

            return true;
        }
    }

    /// <summary>
    /// Struct used for searching with location id and product id as criteria
    /// </summary>
    public struct LocationProductLegalIdSearchCriteria : IEquatable<LocationProductLegalIdSearchCriteria>
    {
        public Int16 LocationId { get; set; }
        public Int32 ProductId { get; set; }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + this.LocationId.GetHashCode();
                hash = hash * 23 + this.ProductId.GetHashCode();
                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is LocationProductLegalIdSearchCriteria)
            {
                return this.Equals((LocationProductLegalIdSearchCriteria)obj);
            }
            return false;
        }

        public bool Equals(LocationProductLegalIdSearchCriteria other)
        {
            return
                other.LocationId == this.LocationId &&
                other.ProductId == this.ProductId;
        }
    }

    /// <summary>
    /// Struct used for searching with location id and product id as criteria
    /// </summary>
    public struct LocationProductLegalEntitySearchCriteria : IEquatable<LocationProductLegalEntitySearchCriteria>
    {
        public Int16 LocationId { get; set; }
        public Int32 ProductId { get; set; }
        public Int32 EntityId { get; set; }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + this.LocationId.GetHashCode();
                hash = hash * 23 + this.ProductId.GetHashCode();
                hash = hash * 23 + this.EntityId.GetHashCode();
                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is LocationProductLegalEntitySearchCriteria)
            {
                return this.Equals((LocationProductLegalEntitySearchCriteria)obj);
            }
            return false;
        }

        public bool Equals(LocationProductLegalEntitySearchCriteria other)
        {
            return
                other.LocationId == this.LocationId &&
                other.ProductId == this.ProductId &&
                other.EntityId == this.EntityId;
        }
    }
}
