﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-26317 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IWorkpackagePlanogramDebugDal : IDal
    {
        IEnumerable<WorkpackagePlanogramDebugDto> FetchBySourcePlanogramId(Int32 sourcePlanogramId);
        void Insert(WorkpackagePlanogramDebugDto dto);
        void Update(WorkpackagePlanogramDebugDto dto);
        void DeleteBySourcePlanogramIdWorkflowTaskId(Int32 sourcePlanogramId, Int32 workflowTaskId);
    }
}
