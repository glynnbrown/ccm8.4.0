﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25586 : A.Probyn
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IProductAttributeDal : IDal
    {
        ProductAttributeDto FetchById(Object id);
        void Insert(ProductAttributeDto dto);
        void Update(ProductAttributeDto dto);
        void DeleteById(Object id);
    }
}
