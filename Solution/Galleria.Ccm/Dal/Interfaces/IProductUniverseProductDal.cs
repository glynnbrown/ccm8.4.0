﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630 : A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IProductUniverseProductDal : IDal
    {
        IEnumerable<ProductUniverseProductDto> FetchByProductUniverseId(Int32 productUniverseId);
        ProductUniverseProductDto FetchById(Int32 id);
        void Insert(ProductUniverseProductDto dto);
        void Update(ProductUniverseProductDto dto);
        void Upsert(IEnumerable<ProductUniverseProductDto> dtoList, ProductUniverseProductIsSetDto isSetDto);
        void DeleteById(Int32 id);
    }
}
