﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// Denotes the expected members of a PrintTemplateComponent Dal
    /// </summary>
    public interface IPrintTemplateComponentDal : IDal
    {
        PrintTemplateComponentDto FetchById(Int32 id);
        IEnumerable<PrintTemplateComponentDto> FetchByPrintTemplateSectionId(Int32 printTemplateSectionId);
        void Insert(PrintTemplateComponentDto dto);
        void Update(PrintTemplateComponentDto dto);
        void DeleteById(Int32 id);
    }
}
