﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25395 : A.Probyn
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchById", "Id")]
    public interface IProductLibraryDal : IDal
    {
        ProductLibraryDto FetchById(Object id);
        void Insert(ProductLibraryDto dto);
        void Update(ProductLibraryDto dto);
        void DeleteById(Object id);

        void LockById(Object id);
        void UnlockById(Object id);
    }
}
