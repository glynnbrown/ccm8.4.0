﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using System.Collections.Generic;


namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// Denotes the expected members of a PrintTemplateInfo Dal
    /// </summary>
    [DefaultFetchMethod("FetchByIds", "Id")]
    public interface IPrintTemplateInfoDal : IDal
    {
        IEnumerable<PrintTemplateInfoDto> FetchByIds(IEnumerable<Object> ids);
        IEnumerable<PrintTemplateInfoDto> FetchByEntityId(Int32 entityId);
    }
}
