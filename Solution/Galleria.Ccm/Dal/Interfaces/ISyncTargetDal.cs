﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    /// The sync target dal interface
    /// </summary>
    public interface ISyncTargetDal : IDal
    {
        SyncTargetDto FetchById(Int32 id);
        IEnumerable<SyncTargetDto> FetchAll();
        void Insert(SyncTargetDto dto);
        void Update(SyncTargetDto dto, SyncTargetIsSetDto isSetDto);
        void DeleteById(Int32 id);
    }
}