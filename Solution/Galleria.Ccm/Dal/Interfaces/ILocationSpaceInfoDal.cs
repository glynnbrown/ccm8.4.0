﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface ILocationSpaceInfoDal : IDal
    {
        /// <summary>
        /// Returns List of Location Spaces by entity id
        /// </summary>
        /// <returns></returns>
        IEnumerable<LocationSpaceInfoDto> FetchByEntityId(Int32 entityid);
    }
}
