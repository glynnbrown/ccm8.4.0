﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchById", "Id")]
    public interface IFolderDal : IDal
    {
        FolderDto FetchById(Object id);
        IEnumerable<FolderDto> FetchByParentFolderId(Object parentFolderId);
        void Insert(FolderDto dto);
        void Update(FolderDto dto);
        void DeleteById(Object id);
    }
}
