﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    ///     Dal interface for Renumbering Strategy Info.
    /// </summary>
    [DefaultFetchMethod("FetchByEntityId", "EntityId")]
    public interface IRenumberingStrategyInfoDal : IDal
    {
        #region Fetch

        /// <summary>
        ///     Returns an enumeration of <see cref="RenumberingStrategyInfoDto" /> matching the provided <paramref name="entityId" />.
        /// </summary>
        /// <param name="entityId">Id of the <see cref="Entity" /> that will be matched for fetching.</param>
        /// <returns>An enumeration of instances of <see cref="RenumberingStrategyDto" /> wmatching the provided <paramref name="entityId" />.</returns>
        IEnumerable<RenumberingStrategyInfoDto> FetchByEntityId(Int32 entityId);

        /// <summary>
        ///     Returns an enumeration of <see cref="RenumberingStrategyInfoDto" /> matching the provided <paramref name="entityId" />, including deleted records.
        /// </summary>
        /// <param name="entityId">Id of the <see cref="Entity" /> that will be matched for fetching.</param>
        /// <returns>An enumeration of instances of <see cref="RenumberingStrategyInfoDto" /> wmatching the provided <paramref name="entityId" />.</returns>
        IEnumerable<RenumberingStrategyInfoDto> FetchByEntityIdIncludingDeleted(Int32 entityId);

        #endregion
    }
}
