﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 820)
// V8-30791 : D.Pleasance
//		Created 
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchAll")]
    public interface IUserEditorSettingColorDal : IDal
    {
        IEnumerable<UserEditorSettingColorDto> FetchAll();
        void Insert(UserEditorSettingColorDto dto);
        void Update(UserEditorSettingColorDto dto);
        void DeleteById(Int32 id);
    }
}