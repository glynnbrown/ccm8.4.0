﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created (Auto-generated)
// V8-26815 : A.Silva ~ Changed type of validationTemplateId in FetchByValidationTemplateId to Object.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    /// <summary>
    ///     Declares the dal interface to interact with Validation Template Groups.
    /// </summary>
    /// <remarks>The Default Fetch Method is FetchByValidationTemplateId, using ValidationTemplateId as the criteria.</remarks>
    [DefaultFetchMethod("FetchByValidationTemplateId", "ValidationTemplateId")]
    public interface IValidationTemplateGroupDal : IDal
    {
        /// <summary>
        ///     Deletes the Validation Template Group matching the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">Unique id value to determine with Validation Template Group to delete.</param>
        void DeleteById(Int32 id);

        /// <summary>
        ///     Fetch the <see cref="ValidationTemplateGroupDto"/> items corresponding to the given <paramref name="validationTemplateId"/>.
        /// </summary>
        /// <param name="validationTemplateId">The unique id value for the Validation Tempate containing the groups to be retrieved.</param>
        /// <returns>An instance implementing <see cref="IEnumerable{ValidationTemplateGroupDto}"/> with all the matching groups.</returns>
        IEnumerable<ValidationTemplateGroupDto> FetchByValidationTemplateId(Object validationTemplateId);
    
        /// <summary>
        ///     Inserts the data contained in the given <paramref name="dto"/> into the dal.
        /// </summary>
        /// <param name="dto">Instance of <see cref="ValidationTemplateGroupDto"/> containing the data to be persisted.</param>
        void Insert(ValidationTemplateGroupDto dto);

        /// <summary>
        ///     Updates the data contained in the given <paramref name="dto"/> in the dal
        /// </summary>
        /// <param name="dto">Instance of <see cref="ValidationTemplateGroupDto"/> containing the data to be persisted.</param>
        void Update(ValidationTemplateGroupDto dto);
    }
}