﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25559 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;


namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchAll")]
    public interface IEntityInfoDal : IDal
    {
        /// <summary>
        /// Returns all entity info dtos in the data store
        /// </summary>
        /// <returns>All entity info dtos</returns>
        IEnumerable<EntityInfoDto> FetchAll();
    }
}
