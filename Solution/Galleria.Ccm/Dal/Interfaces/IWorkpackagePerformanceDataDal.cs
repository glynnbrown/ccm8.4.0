﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26773 : N.Foster
//  Created
#endregion
#region Version History: CCM803
// V8-29491 : D.Pleasance
//  Removed FetchByWorkpackageIdPerformanceSelectionId
//  Added FetchByWorkpackageIdPerformanceSelectionIdDataTypeDataId
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface IWorkpackagePerformanceDataDal : IDal
    {
        IEnumerable<WorkpackagePerformanceDataDto> FetchByWorkpackageIdPerformanceSelectionIdDataTypeDataId(Int32 workpackageId, Int32 performanceSelectionId, Byte dataType, Int32 dataId);
        void BulkInsert(Int32 workpackageId, IEnumerable<WorkpackagePerformanceDataDto> dtoList);
        void DeleteByWorkpackageId(Int32 workpackageId);
    }
}
