﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByBlockingId", "BlockingId")]
    public interface IBlockingDividerDal : IDal
    {
        IEnumerable<BlockingDividerDto> FetchByBlockingId(Int32 blockingId);
        void Insert(BlockingDividerDto dto);
        void Update(BlockingDividerDto dto);
        void DeleteById(Int32 id);
    }
}
