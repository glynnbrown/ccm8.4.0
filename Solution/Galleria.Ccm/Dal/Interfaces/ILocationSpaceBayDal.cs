﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface ILocationSpaceBaySearchCriteriaDal : IDal
    {
        IEnumerable<LocationSpaceBaySearchCriteriaDto> FetchByEntityId(Int32 entityId);
    }

    public interface ILocationSpaceBayDal : IDal
    {
        /// <summary>
        /// Returns Location Space Bay by ID
        /// </summary>
        /// <returns></returns>
        LocationSpaceBayDto FetchById(Int32 id);

        /// <summary>
        /// Returns Location Space Bay by Location Space Product Group ID
        /// </summary>
        /// <returns></returns>
        IEnumerable<LocationSpaceBayDto> FetchByLocationSpaceProductGroupId(Int32 id);

        /// <summary>
        /// Inserts the given dto
        /// </summary>
        /// <param name="dto"></param>
        void Insert(LocationSpaceBayDto dto);

        /// <summary>
        /// Inserts a list of given dtos
        /// </summary>
        /// <param name="dtoList"></param>
        void Insert(IEnumerable<LocationSpaceBayDto> dtoList);

        /// <summary>
        /// Updates the given dto
        /// </summary>
        /// <param name="dto"></param>
        void Update(LocationSpaceBayDto dto);


        /// <summary>
        /// Delete the given Location Space Bay ID
        /// </summary>
        /// <param name="Int32"></param>
        void DeleteById(Int32 id);

        /// <summary>
        /// Delete by entity Id
        /// </summary>
        /// <param name="Int32"></param>
        void DeleteByEntityId(Int32 id);

        /// <summary>
        /// Upsert collection of location space bay dto's
        /// </summary>
        /// <param name="dtoList"></param>
        void Upsert(IEnumerable<LocationSpaceBayDto> dtoList);

    }
}
