﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25546 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchByWorkflowTaskParameterId", "WorkflowTaskParameterId")]
    public interface IWorkflowTaskParameterValueDal : IDal
    {
        IEnumerable<WorkflowTaskParameterValueDto> FetchByWorkflowTaskParameterId(Int32 workflowTaskParameterId);
        void Insert(WorkflowTaskParameterValueDto dto);
        void Update(WorkflowTaskParameterValueDto dto);
        void DeleteById(Int32 id);
    }
}
