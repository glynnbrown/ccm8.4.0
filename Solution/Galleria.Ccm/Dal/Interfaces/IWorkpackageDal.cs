﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25546 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.Interfaces
{
    [DefaultFetchMethod("FetchById", "Id")]
    public interface IWorkpackageDal : IBatchDal<WorkpackageDto>
    {
        WorkpackageDto FetchById(Int32 id);
        void Insert(WorkpackageDto dto);
        void Update(WorkpackageDto dto);
        void DeleteById(Int32 id);
    }
}
