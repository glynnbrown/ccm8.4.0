﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// PrintTemplateComponentDetail Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class PrintTemplateComponentDetailDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public PrintTemplateComponentDetailDtoKey DtoKey
        {
            get
            {
                return new PrintTemplateComponentDetailDtoKey()
                {
                    PrintTemplateComponentId = this.PrintTemplateComponentId,
                    Key = this.Key
                };
            }
        }
        [ForeignKey(typeof(PrintTemplateComponentDto), typeof(IPrintTemplateComponentDal), DeleteBehavior.Cascade)]
        public Int32 PrintTemplateComponentId { get; set; }
        public String Key { get; set; }
        public String Value { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PrintTemplateComponentDetailDto other = obj as PrintTemplateComponentDetailDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.PrintTemplateComponentId != this.PrintTemplateComponentId) { return false; }
                if (other.Key != this.Key) { return false; }
                if (other.Value != this.Value) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    /// <summary>
    /// PrintTemplateComponentDetail Dto key
    /// </summary>
    [Serializable]
    public sealed class PrintTemplateComponentDetailDtoKey
    {

        #region Properties
        public Int32 PrintTemplateComponentId { get; set; }
        public String Key { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return PrintTemplateComponentId.GetHashCode() ^
                Key.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PrintTemplateComponentDetailDtoKey other = obj as PrintTemplateComponentDetailDtoKey;
            if (other != null)
            {
                if (other.PrintTemplateComponentId != this.PrintTemplateComponentId) { return false; }
                if (other.Key != this.Key) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


}
