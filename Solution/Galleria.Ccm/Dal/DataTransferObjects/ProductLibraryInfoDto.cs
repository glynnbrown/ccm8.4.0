﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-25395 : A.Probyn
//  Created
#endregion
#region Version History: (CCM 8.3)
//V8-32361 : L.Ineson
//  Added Name
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// ProductLibraryInfo data transfer object
    /// </summary>
    [Serializable]
    public class ProductLibraryInfoDto
    {
        #region Properties
        public Object Id { get; set; }
        public String Name { get; set; }
        #endregion

        #region Methods

        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override bool Equals(Object obj)
        {
            ProductLibraryInfoDto other = obj as ProductLibraryInfoDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }

                if (other.Name != this.Name) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
