﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM v8.0.2)
// CCM-29010 : D.Pleasance
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// PlanogramNameTemplateInfo Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramNameTemplateInfoDto
    {
        #region Properties

        public PlanogramNameTemplateDtoKey DtoKey
        {
            get
            {
                return new PlanogramNameTemplateDtoKey
                {
                    EntityId = EntityId,
                    Name = Name
                };
            }
        }
        public Int32 Id { get; set; }
        public Int32 EntityId { get; set; }
        public String Name { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramNameTemplateInfoDto other = obj as PlanogramNameTemplateInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
        
    }
}