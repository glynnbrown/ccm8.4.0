﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)

// CCM-26234 : L.Ineson
//		Copied from GFS
// V8-26322 : A.Silva
//      Amended UserId Property to have DeleteBehavior.Leave (FK record is marked as deleted, not really deleted).

#endregion

#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// RoleMemberDto Data Transfer Object
    /// </summary>
    [Serializable]
    public class RoleMemberDto
    {
        #region Properties

        public Int32 Id { get; set; }

        public RowVersion RowVersion { get; set; }

        public RoleMemberDtoKey DtoKey
        {
            get
            {
                return new RoleMemberDtoKey()
                {
                    UserId = this.RoleId,
                    RoleId = this.UserId
                };
            }
        }

        [ForeignKey(typeof(UserDto), typeof(IUserDal), DeleteBehavior.Leave)]
        public Int32 UserId { get; set; }

        [ForeignKey(typeof(RoleDto), typeof(IRoleDal))]
        public Int32 RoleId { get; set; }

        #endregion

        #region Methods

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            RoleMemberDto other = obj as RoleMemberDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.UserId != this.UserId) { return false; }
                if (other.RoleId != this.RoleId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        #endregion
    }

    [Serializable]
    public class RoleMemberDtoKey
    {
        #region Properties
        public Int32 UserId { get; set; }
        public Int32 RoleId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return UserId.GetHashCode()
                + RoleId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            RoleMemberDtoKey other = obj as RoleMemberDtoKey;
            if (other != null)
            {
                if (other.RoleId != this.RoleId) { return false; }
                if (other.UserId != this.UserId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
