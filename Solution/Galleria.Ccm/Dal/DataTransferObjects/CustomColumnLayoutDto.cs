#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.
// V8-26201 : A.Silva ~ Corrected Equals.

#endregion

#region Version History: (CCM 8.3)
// V8-31542 : L.Ineson
//  Added more properties to support additional datasheet 
#endregion

#endregion

using System;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class CustomColumnLayoutDto
    {
        #region Properties

        public Object Id { get; set; }

        public CustomColumnLayoutDtoKey DtoKey
        {
            get { return new CustomColumnLayoutDtoKey { Name = Name }; }
        }

        public String Name { get; set; }
        public Byte Type { get; set; }
        public String Description { get; set; }
        public Boolean HasTotals { get; set; }
        public Byte FrozenColumnCount { get; set; }
        public Boolean IsTitleShown { get; set; }
        public Boolean IsGroupDetailHidden { get; set; }
        public Boolean IsGroupBlankRowShown { get; set; }
        public Byte DataOrderType { get; set; }
        public Boolean IncludeAllPlanograms { get; set; }

        #endregion

        #region Equals overrides

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            CustomColumnLayoutDto other = obj as CustomColumnLayoutDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }

                if (other.Name != this.Name) { return false; }
                if (other.Type != this.Type) { return false; }
                if (other.Description != this.Description) { return false; }
                if (other.HasTotals != this.HasTotals) { return false; }
                if (other.IsTitleShown != this.IsTitleShown) { return false; }
                if (other.FrozenColumnCount != this.FrozenColumnCount) { return false; }
                if (other.IsGroupDetailHidden != this.IsGroupDetailHidden) { return false; }
                if (other.IsGroupBlankRowShown != this.IsGroupBlankRowShown) { return false; }
                if (other.DataOrderType != this.DataOrderType) { return false; }
                if (other.IncludeAllPlanograms != this.IncludeAllPlanograms) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        #endregion
    }

    /// <summary>
    ///     CustomColumnLayout Dto Key
    /// </summary>
    [Serializable]
    public sealed class CustomColumnLayoutDtoKey
    {
        #region Properties
        public String Name { get; set; }
        #endregion

        #region Equals Override

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            CustomColumnLayoutDtoKey other = obj as CustomColumnLayoutDtoKey;
            if (other != null)
            {
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Name.GetHashCode();
        }
        #endregion
    }
}