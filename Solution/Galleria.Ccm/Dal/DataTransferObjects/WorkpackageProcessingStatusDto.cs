﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-26895 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  Aded description, DateStarted, DateLastUpdated properties
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class WorkpackageProcessingStatusDto
    {
        #region Properties
        public Int32 WorkpackageId { get; set; }
        public RowVersion RowVersion { get; set; }
        public Byte Status { get; set; }
        public String StatusDescription { get; set; }
        public Int32 ProgressMax { get; set; }
        public Int32 ProgressCurrent { get; set; }
        public DateTime? DateStarted { get; set; }
        public DateTime? DateLastUpdated { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this instance
        /// </summary>
        public override Int32 GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Compares that two instances are equal
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            WorkpackageProcessingStatusDto other = obj as WorkpackageProcessingStatusDto;
            if (other != null)
            {
                if (other.WorkpackageId != this.WorkpackageId) return false;
                if (other.RowVersion != this.RowVersion) return false;
                if (other.Status != this.Status) return false;
                if (other.StatusDescription != this.StatusDescription) return false;
                if (other.ProgressMax != this.ProgressMax) return false;
                if (other.ProgressCurrent != this.ProgressCurrent) return false;
                if (other.DateStarted != this.DateStarted) return false;
                if (other.DateLastUpdated != this.DateLastUpdated) return false;
            }
            else
            {
                return false;
            }
            return true;
        }

        #endregion
    }
}
