﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    #region MOVE

    [Serializable]
    public class LocationSpaceElementSearchCriteriaDto
    {
        #region Properties
        public String LocationCode { get; set; }
        public String ProductGroupCode { get; set; }
        public Byte BayOrder { get; set; }
        public Byte Order { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return LocationCode.GetHashCode() + ProductGroupCode.GetHashCode() + BayOrder.GetHashCode() + Order.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            LocationSpaceElementSearchCriteriaDto other = obj as LocationSpaceElementSearchCriteriaDto;
            if (other != null)
            {
                if (other.LocationCode != this.LocationCode) { return false; }
                if (other.ProductGroupCode != this.ProductGroupCode) { return false; }
                if (other.BayOrder != this.BayOrder) { return false; }
                if (other.Order != this.Order) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    #endregion

    /// <summary>
    /// Location Space Data Transfer Object
    /// </summary>
    [Serializable]
    public class LocationSpaceElementDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public LocationSpaceElementDtoKey DtoKey
        {
            get
            {
                return new LocationSpaceElementDtoKey()
                {
                    LocationSpaceBayId = this.LocationSpaceBayId,
                    Order = this.Order
                };
            }
        }
        public Byte Order { get; set; }
        public Single X { get; set; }
        public Single Y { get; set; }
        public Single Z { get; set; }
        public Single MerchandisableHeight { get; set; }
        public Single Height { get; set; }
        public Single Width { get; set; }
        public Single Depth { get; set; }
        public Single FaceThickness { get; set; }
        public Nullable<Single> NotchNumber { get; set; }
        [ForeignKey(typeof(LocationSpaceBayDto), typeof(ILocationSpaceBayDal))]
        public Int32 LocationSpaceBayId { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override bool Equals(object obj)
        {
            LocationSpaceElementDto other = obj as LocationSpaceElementDto;
            if (other != null)
            {
                if (other.Id != this.Id)
                {
                    return false;
                }
                if (other.Order != this.Order)
                {
                    return false;
                }
                if (other.X != this.X)
                {
                    return false;
                }
                if (other.Y != this.Y)
                {
                    return false;
                }
                if (other.Z != this.Z)
                {
                    return false;
                }
                if (other.MerchandisableHeight != this.MerchandisableHeight)
                {
                    return false;
                }
                if (other.Height != this.Height)
                {
                    return false;
                }
                if (other.Width != this.Width)
                {
                    return false;
                }
                if (other.Depth != this.Depth)
                {
                    return false;
                }
                if (other.FaceThickness != this.FaceThickness)
                {
                    return false;
                }
                if (other.NotchNumber != this.NotchNumber)
                {
                    return false;
                }
                if (other.LocationSpaceBayId != this.LocationSpaceBayId)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class LocationSpaceElementDtoKey
    {
        #region Properties
        public Int32 LocationSpaceBayId { get; set; }
        public Byte Order { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                LocationSpaceBayId.GetHashCode()
                    + Order.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            LocationSpaceElementDtoKey other = obj as LocationSpaceElementDtoKey;
            if (other != null)
            {
                if (other.LocationSpaceBayId != this.LocationSpaceBayId) { return false; }
                if (other.Order != this.Order) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class LocationSpaceElementIsSetDto
    {
        #region Properties
        public Boolean IsOrderSet { get; set; }
        public Boolean IsXSet { get; set; }
        public Boolean IsYSet { get; set; }
        public Boolean IsZSet { get; set; }
        public Boolean IsMerchandisableHeightSet { get; set; }
        public Boolean IsHeightSet { get; set; }
        public Boolean IsWidthSet { get; set; }
        public Boolean IsDepthSet { get; set; }
        public Boolean IsFaceThicknessSet { get; set; }
        public Boolean IsNotchNumberSet { get; set; }
        public Boolean IsLocationSpaceBayIdSet { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public LocationSpaceElementIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public LocationSpaceElementIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsOrderSet.GetHashCode() +
                IsXSet.GetHashCode() +
                IsYSet.GetHashCode() +
                IsZSet.GetHashCode() +
                IsMerchandisableHeightSet.GetHashCode() +
                IsHeightSet.GetHashCode() +
                IsWidthSet.GetHashCode() +
                IsDepthSet.GetHashCode() +
                IsFaceThicknessSet.GetHashCode() +
                IsNotchNumberSet.GetHashCode() +
                IsLocationSpaceBayIdSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            LocationSpaceElementIsSetDto other = obj as LocationSpaceElementIsSetDto;
            if (other != null)
            {
                if (other.IsOrderSet != this.IsOrderSet) { return false; }
                if (other.IsXSet != this.IsXSet) { return false; }
                if (other.IsYSet != this.IsYSet) { return false; }
                if (other.IsZSet != this.IsZSet) { return false; }
                if (other.IsMerchandisableHeightSet != this.IsMerchandisableHeightSet) { return false; }
                if (other.IsHeightSet != this.IsHeightSet) { return false; }
                if (other.IsWidthSet != this.IsWidthSet) { return false; }
                if (other.IsDepthSet != this.IsDepthSet) { return false; }
                if (other.IsFaceThicknessSet != this.IsFaceThicknessSet) { return false; }
                if (other.IsNotchNumberSet != this.IsNotchNumberSet) { return false; }
                if (other.IsLocationSpaceBayIdSet != this.IsLocationSpaceBayIdSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsOrderSet = isSet;
            IsXSet = isSet;
            IsYSet = isSet;
            IsZSet = isSet;
            IsMerchandisableHeightSet = isSet;
            IsHeightSet = isSet;
            IsWidthSet = isSet;
            IsDepthSet = isSet;
            IsFaceThicknessSet = isSet;
            IsNotchNumberSet = isSet;
            IsLocationSpaceBayIdSet.GetHashCode();
        }
        #endregion
    }
}
