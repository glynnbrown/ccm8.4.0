﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)

// CCM-25587 : L.Ineson
//		Created (Auto-generated)
// CCM-25534 - I.George
//      added dto key property
// V8-27964 : A.Silva
//      Added ProductGroupId.

#endregion
#region Version History: (CCM 801)

// V8-28878 : D.Pleasance
//  Added missing property IsProductGroupIdSet to PlanogramGroupIsSetDto

#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// PlanogramGroup Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class PlanogramGroupDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public PlanogramGroupDtoKey DtoKey
        {
            get
            {
                return new PlanogramGroupDtoKey()
                {
                    ParentGroupId = this.ParentGroupId,
                    Name = this.Name,
                    PlanogramHierarchyId = this.PlanogramHierarchyId
                };
            }
        }
        [ForeignKey(typeof(PlanogramHierarchyDto), typeof(IPlanogramHierarchyDal), DeleteBehavior.Leave)]
        public Int32 PlanogramHierarchyId { get; set; }
        public String Name { get; set; }
        public Int32? ParentGroupId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        public Int32? ProductGroupId { get; set; }
        public Guid Code { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramGroupDto other = obj as PlanogramGroupDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.PlanogramHierarchyId != this.PlanogramHierarchyId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.ParentGroupId != this.ParentGroupId) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.DateDeleted != this.DateDeleted) { return false; }
                if (other.ProductGroupId != this.ProductGroupId) { return false; }
                if (other.Code != this.Code) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class PlanogramGroupDtoKey
    {
        #region Properties
        public Int32 PlanogramHierarchyId { get; set; }
        public String Name { get; set; }
        public Int32? ParentGroupId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
             PlanogramHierarchyId.GetHashCode() +
             Name.GetHashCode() +
             ParentGroupId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramGroupDtoKey other = obj as PlanogramGroupDtoKey;
            if (other != null)
            {
                if (other.PlanogramHierarchyId != this.PlanogramHierarchyId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.ParentGroupId != this.ParentGroupId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class PlanogramGroupIsSetDto
    {
        #region Properties
        public Boolean IsPlanogramHierarchyIdSet { get; set; }
        public Boolean IsNameSet { get; set; }
        public Boolean IsParentGroupIdSet { get; set; }
        public Boolean IsProductGroupIdSet { get; set; }
        #endregion

        #region Constructors

        /// <summary>
        /// Default
        /// </summary>
        public PlanogramGroupIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public PlanogramGroupIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsPlanogramHierarchyIdSet.GetHashCode() +
                IsNameSet.GetHashCode() +
                IsParentGroupIdSet.GetHashCode() +
                IsProductGroupIdSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramGroupIsSetDto other = obj as PlanogramGroupIsSetDto;
            if (other != null)
            {

                if (other.IsPlanogramHierarchyIdSet != this.IsPlanogramHierarchyIdSet) { return false; }
                if (other.IsNameSet != this.IsNameSet) { return false; }
                if (other.IsParentGroupIdSet != this.IsParentGroupIdSet) { return false; }
                if (other.IsProductGroupIdSet != this.IsProductGroupIdSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsPlanogramHierarchyIdSet = isSet;
            IsNameSet = isSet;
            IsParentGroupIdSet = isSet;
            IsProductGroupIdSet = isSet;
        }
        #endregion
    }
}
