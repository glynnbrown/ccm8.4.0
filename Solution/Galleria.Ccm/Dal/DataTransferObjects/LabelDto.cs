﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
// V8-24265 : N.Haywood
//  Added label text formatting (unfinished)
// V8-24265 : J.Pickup
//  Finished Re-implemented & renamed from 'LabelSettingDto'
// V8-27940 : L.Luong
//  Added RowVersion, EntityId, DateCreated, DateModified  and DateDeleted 
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    ///     Label Data Transfer Object
    /// </summary>
    [Serializable]
    public class LabelDto
    {
        #region Properties
        public Object Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public LabelDtoKey DtoKey
        {
            get 
            { 
                return new LabelDtoKey() 
                { 
                    Name = this.Name,
                    EntityId = EntityId
                }; 
            }
        }
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        public String Name { get; set; }
        public Byte Type { get; set; }
        public Int32 BackgroundColour { get; set; }
        public Int32 BorderColour { get; set; }
        public Int32 BorderThickness { get; set; }
        public String Text { get; set; }
        public Int32 TextColour { get; set; }
        public String Font { get; set; }
        public Int32 FontSize { get; set; }
        public Boolean IsRotateToFitOn { get; set; }
        public Boolean IsShrinkToFitOn { get; set; }
        public Boolean TextBoxFontBold { get; set; }
        public Boolean TextBoxFontItalic { get; set; }
        public Boolean TextBoxFontUnderlined { get; set; }
        public Byte LabelHorizontalPlacement { get; set; }
        public Byte LabelVerticalPlacement { get; set; }
        public Byte TextBoxTextAlignment { get; set; }
        public Byte TextDirection { get; set; }
        public Byte TextWrapping { get; set; }
        public Single BackgroundTransparency { get; set; }
        public Boolean ShowOverImages { get; set; }
        public Boolean ShowLabelPerFacing { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            LabelDto other = obj as LabelDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.Type != this.Type) { return false; }
                if (other.BackgroundColour != this.BackgroundColour) { return false; }
                if (other.BorderColour != this.BorderColour) { return false; }
                if (other.BorderThickness != this.BorderThickness) { return false; }
                if (other.Text != this.Text) { return false; }
                if (other.TextColour != this.TextColour) { return false; }
                if (other.Font != this.Font) { return false; }
                if (other.FontSize != this.FontSize) { return false; }
                if (other.IsRotateToFitOn != this.IsRotateToFitOn) { return false; }
                if (other.IsShrinkToFitOn != this.IsShrinkToFitOn) { return false; }
                if (other.TextBoxFontBold != this.TextBoxFontBold) { return false; }
                if (other.TextBoxFontItalic != this.TextBoxFontItalic) { return false; }
                if (other.TextBoxFontUnderlined != this.TextBoxFontUnderlined) { return false; }
                if (other.LabelHorizontalPlacement != this.LabelHorizontalPlacement) { return false; }
                if (other.LabelVerticalPlacement != this.LabelVerticalPlacement) { return false; }
                if (other.TextBoxTextAlignment != this.TextBoxTextAlignment) { return false; }
                if (other.TextDirection != this.TextDirection) { return false; }
                if (other.TextWrapping != this.TextWrapping) { return false; }
                if (other.BackgroundTransparency != this.BackgroundTransparency) { return false; }
                if (other.ShowOverImages != this.ShowOverImages) { return false; }
                if (other.ShowLabelPerFacing != this.ShowLabelPerFacing) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.DateDeleted != this.DateDeleted) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class LabelDtoKey
    {
        #region Properties
        public String Name { get; set; }
        public Int32 EntityId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return EntityId.GetHashCode() + Name.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the ISOme
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            LabelDtoKey other = obj as LabelDtoKey;
            if (other != null)
            {
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
