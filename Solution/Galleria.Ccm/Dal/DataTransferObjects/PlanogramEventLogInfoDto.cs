﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM810)
// V8-29861 : A.Probyn
//	Created
#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// PlanogramEventLogInfo Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramEventLogInfoDto
    {
        #region Properties

        public Int32 Id { get; set; }
        public Int32 PlanogramId { get; set; }
        public Byte EventType { get; set; }
        public Byte EntryType { get; set; }
        public Byte? AffectedType { get; set; }
        public Object AffectedId { get; set; }
        public String WorkpackageSource { get; set; }
        public String TaskSource { get; set; }
        public Int32 EventId { get; set; }
        public DateTime DateTime { get; set; }
        public String Description { get; set; }
        public String Content { get; set; }
        public Byte Score { get; set; }
        public String PlanogramName { get; set; }
        public Int64 ExecutionOrder { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        ///     Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramEventLogInfoDto other = obj as PlanogramEventLogInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.AffectedId != this.AffectedId) { return false; }
                if (other.AffectedType != this.AffectedType) { return false; }
                if (other.Content != this.Content) { return false; }
                if (other.DateTime != this.DateTime) { return false; }
                if (other.Description != this.Description) { return false; }
                if (other.EntryType != this.EntryType) { return false; }
                if (other.EventId != this.EventId) { return false; }
                if (other.EventType != this.EventType) { return false; }
                if (other.ExecutionOrder != this.ExecutionOrder) { return false; }
                if (other.PlanogramId != this.PlanogramId) { return false; }
                if (other.PlanogramName != this.PlanogramName) { return false; }
                if (other.Score != this.Score) { return false; }
                if (other.TaskSource != this.TaskSource) { return false; }
                if (other.WorkpackageSource != this.WorkpackageSource) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
