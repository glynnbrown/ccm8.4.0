﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-25455 : J.Pickup
//  Created
// CCM-27059 : J.Pickup
//      Reviewed FK attributes
// V8-26322 : A.Silva
//      Amended LocationId Property to have DeleteBehavior.Leave (parent is marked as deleted, not really deleted).

#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// AssortmentMinorRevisionListActionLocation Data Transfer Object
    /// </summary>
    [Serializable]
    public class AssortmentMinorRevisionListActionLocationDto
    {
        #region Properties

        public Int32 Id { get; set; }
        [ForeignKey(typeof(AssortmentMinorRevisionListActionDto), typeof(IAssortmentMinorRevisionListActionDal))]
        public Int32 AssortmentMinorRevisionListActionId { get; set; }
        public String LocationCode { get; set; }
        public String LocationName { get; set; }
        [ForeignKey(typeof(LocationDto), typeof(ILocationDal), DeleteBehavior.Leave)]
        public Int16? LocationId { get; set; }
        public Int32 Units { get; set; }
        public Int32 Facings { get; set; }
        public AssortmentMinorRevisionListActionLocationDtoKey DtoKey
        {
            get
            {
                return new AssortmentMinorRevisionListActionLocationDtoKey()
                {
                     AssortmentMinorRevisionListActionId = this.AssortmentMinorRevisionListActionId,
                     LocationName = this.LocationName
                };
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentMinorRevisionListActionLocationDto other = obj as AssortmentMinorRevisionListActionLocationDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.AssortmentMinorRevisionListActionId != this.AssortmentMinorRevisionListActionId) { return false; }
                if (other.LocationCode != this.LocationCode) { return false; }
                if (other.LocationName != this.LocationName) { return false; }
                if (other.LocationId != this.LocationId) { return false; }
                if (other.Units != this.Units) { return false; }
                if (other.Facings != this.Facings) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentMinorRevisionListActionLocationDtoKey
    {
        #region Properties

        public Int32 AssortmentMinorRevisionListActionId { get; set; }
        public String LocationName { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                AssortmentMinorRevisionListActionId.GetHashCode() +
                LocationName.GetHashCode();

        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentMinorRevisionListActionLocationDtoKey other = obj as AssortmentMinorRevisionListActionLocationDtoKey;
            if (other != null)
            {
                if (other.AssortmentMinorRevisionListActionId != this.AssortmentMinorRevisionListActionId) { return false; }
                if (other.LocationName != this.LocationName) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


    [Serializable]
    public class AssortmentMinorRevisionListActionLocationIsSetDto
    {
        #region Properties

        public Boolean IsIdSet { get; set; }
        public Boolean IsAssortmentMinorRevisionListActionIdSet { get; set; }
        public Boolean IsLocationCodeSet { get; set; }
        public Boolean IsLocationNameSet { get; set; }
        public Boolean IsLocationIdSet { get; set; }
        public Boolean IsUnitsSet { get; set; }
        public Boolean IsFacingsSet { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Default
        /// </summary>
        public AssortmentMinorRevisionListActionLocationIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public AssortmentMinorRevisionListActionLocationIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsIdSet.GetHashCode() +
                IsAssortmentMinorRevisionListActionIdSet.GetHashCode() +
                IsLocationCodeSet.GetHashCode() +
                IsLocationNameSet.GetHashCode() +
                IsLocationIdSet.GetHashCode() +
                IsUnitsSet.GetHashCode() +
                IsFacingsSet.GetHashCode();

        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentMinorRevisionListActionLocationIsSetDto other = obj as AssortmentMinorRevisionListActionLocationIsSetDto;
            if (other != null)
            {
                if (other.IsIdSet != this.IsIdSet) { return false; }
                if (other.IsAssortmentMinorRevisionListActionIdSet != this.IsAssortmentMinorRevisionListActionIdSet) { return false; }
                if (other.IsLocationCodeSet != this.IsLocationCodeSet) { return false; }
                if (other.IsLocationNameSet != this.IsLocationNameSet) { return false; }
                if (other.IsLocationIdSet != this.IsLocationIdSet) { return false; }
                if (other.IsUnitsSet != this.IsUnitsSet) { return false; }
                if (other.IsFacingsSet != this.IsFacingsSet) { return false; }

            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsIdSet = isSet;
            IsAssortmentMinorRevisionListActionIdSet = isSet;
            IsLocationCodeSet = isSet;
            IsLocationNameSet = isSet;
            IsLocationIdSet = isSet;
            IsFacingsSet = isSet;
            IsUnitsSet = isSet;
        }
        #endregion
    }
}
