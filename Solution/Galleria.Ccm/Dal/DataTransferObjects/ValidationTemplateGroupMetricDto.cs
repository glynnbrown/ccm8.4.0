﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26474 : A.Silva ~ Created (Auto-generated)
// V8-26514 : A.Silva ~ Added Score Properties.
// V8-26815 : A.Silva ~ Refactored the Equality methods.
// V8-26812 : A.Silva ~ Added ValidationType property.
#endregion

#region Version History: CCM803
// V8-29596 : L.Luong
//  Added Criteria
#endregion

#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    ///     ValidationTemplateGroupMetric Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class ValidationTemplateGroupMetricDto
    {
        #region Properties

        public Byte AggregationType { get; set; }

        public ValidationTemplateGroupMetricDtoKey DtoKey
        {
            get
            {
                return new ValidationTemplateGroupMetricDtoKey
                {
                    AggregationType = AggregationType,
                    Field = Field,
                    ValidationTemplateGroupId = ValidationTemplateGroupId
                };
            }
        }

        public String Field { get; set; }
        public Int32 Id { get; set; }
        public Single Score1 { get; set; }
        public Single Score2 { get; set; }
        public Single Score3 { get; set; }
        public Single Threshold1 { get; set; }
        public Single Threshold2 { get; set; }
        public String Criteria { get; set; }

        [ForeignKey(typeof (ValidationTemplateGroupDto), typeof (IValidationTemplateGroupDal), DeleteBehavior.Cascade)]
        public Int32 ValidationTemplateGroupId { get; set; }

        /// <summary>
        ///     Gets or sets the validation calculation type to be used with this instance.
        /// </summary>
        public Byte ValidationType { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            var other = obj as ValidationTemplateGroupMetricDto;
            return other != null
                   && other.Id == Id
                   && other.ValidationTemplateGroupId == ValidationTemplateGroupId
                   && other.Field == Field
                   && other.Threshold1 == Threshold1
                   && other.Threshold2 == Threshold2
                   && other.Score1 == Score1
                   && other.Score2 == Score2
                   && other.Score3 == Score3
                   && other.AggregationType == AggregationType
                   && other.ValidationType == ValidationType
                   && other.Criteria == Criteria;
        }

        /// <summary>
        ///     Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode() +
                   ValidationTemplateGroupId.GetHashCode() +
                   Field.GetHashCode() +
                   Threshold1.GetHashCode() +
                   Threshold2.GetHashCode() +
                   Score1.GetHashCode() +
                   Score2.GetHashCode() +
                   Score3.GetHashCode() +
                   AggregationType.GetHashCode() + 
                   ValidationType.GetHashCode();
        }

        #endregion
    }

    [Serializable]
    public sealed class ValidationTemplateGroupMetricDtoKey
    {
        #region Properties

        public Byte AggregationType { get; set; }
        public String Field { get; set; }

        [ForeignKey(typeof(ValidationTemplateGroupDto), typeof(IValidationTemplateGroupDal), DeleteBehavior.Cascade)]
        public Int32 ValidationTemplateGroupId { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            var other = obj as ValidationTemplateGroupMetricDtoKey;
            return other != null &&
                   other.AggregationType == AggregationType &&
                   other.Field == Field &&
                   other.ValidationTemplateGroupId == ValidationTemplateGroupId;
        }

        /// <summary>
        ///     Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return AggregationType.GetHashCode() +
                   Field.GetHashCode() +
                   ValidationTemplateGroupId.GetHashCode();
        }

        #endregion
    }
}