﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)

// CCM-25559 : L.Hodson
//		Created (Auto-generated)
//CCM-26179: I.George
// added more fields
// CCM-26306 : L.Ineson
//  Moved uom values into system settings
// V8-27964 : A.Silva
//      Added ProductLevelId and DateLastMerchSynced.

#endregion

#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Entity Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class EntityDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public EntityDtoKey DtoKey
        {
            get
            {
                return new EntityDtoKey()
                {
                    Name = this.Name
                };
            }
        }
        public Int32? GFSId { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        
        public Int32? ProductLevelId { get; set; }
        public DateTime? DateLastMerchSynced { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            EntityDto other = obj as EntityDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.GFSId != this.GFSId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.Description != this.Description) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.DateDeleted != this.DateDeleted) { return false; }
                if (other.ProductLevelId != this.ProductLevelId) { return false; }
                if (other.DateLastMerchSynced != this.DateLastMerchSynced) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class EntityDtoKey
    {
        #region Properties
        public String Name { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Name.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            EntityDtoKey other = obj as EntityDtoKey;
            if (other != null)
            {
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
