﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25559 : L.Ineson
//		Copied from SA (Auto-generated)
#endregion

#region Version History: (CCM 8.0.3)
// V8-29174 : M.Shelley
//  Add setting to save user's last Plan Assignment "View By" selection
#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class UserSystemSettingDto
    {
        #region Properties
        public UserSystemSettingDtoKey DtoKey
        {
            get
            {
                return new UserSystemSettingDtoKey()
                {
                    DisplayLanguage = this.DisplayLanguage,
                    DisplayCEIPWindow = this.DisplayCEIPWindow,
                    SendCEIPInformation = this.SendCEIPInformation,
                    IsDatabaseSelectionRemembered = this.IsDatabaseSelectionRemembered,
                    IsEntitySelectionRemembered = this.IsEntitySelectionRemembered,
                    PlanAssignmentViewBy = this.PlanAssignmentViewBy,
                };
            }
        }
        public String DisplayLanguage { get; set; }
        public Boolean DisplayCEIPWindow { get; set; }
        public Boolean SendCEIPInformation { get; set; }
        public Boolean IsDatabaseSelectionRemembered { get; set; }
        public Boolean IsEntitySelectionRemembered { get; set; }
        public PlanAssignmentViewType PlanAssignmentViewBy { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            UserSystemSettingDto other = obj as UserSystemSettingDto;
            if (other != null)
            {
                if (other.DisplayLanguage != this.DisplayLanguage) { return false; }
                if (other.DisplayCEIPWindow != this.DisplayCEIPWindow) { return false; }
                if (other.SendCEIPInformation != this.SendCEIPInformation) { return false; }
                if (other.IsDatabaseSelectionRemembered != this.IsDatabaseSelectionRemembered) { return false; }
                if (other.IsEntitySelectionRemembered != this.IsEntitySelectionRemembered) { return false; }
                if (other.PlanAssignmentViewBy != this.PlanAssignmentViewBy) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class UserSystemSettingDtoKey
    {
        #region Properties

        public String DisplayLanguage { get; set; }
        public Boolean DisplayCEIPWindow { get; set; }
        public Boolean SendCEIPInformation { get; set; }
        public Boolean IsDatabaseSelectionRemembered { get; set; }
        public Boolean IsEntitySelectionRemembered { get; set; }
        public PlanAssignmentViewType PlanAssignmentViewBy { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return DisplayLanguage.GetHashCode() +
                DisplayCEIPWindow.GetHashCode() +
                SendCEIPInformation.GetHashCode() +
                IsDatabaseSelectionRemembered.GetHashCode() +
                IsEntitySelectionRemembered.GetHashCode() + 
                PlanAssignmentViewBy.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            UserSystemSettingDtoKey other = obj as UserSystemSettingDtoKey;
            if (other != null)
            {
                if (other.DisplayLanguage != this.DisplayLanguage) { return false; }
                if (other.DisplayCEIPWindow != this.DisplayCEIPWindow) { return false; }
                if (other.SendCEIPInformation != this.SendCEIPInformation) { return false; }
                if (other.IsDatabaseSelectionRemembered != this.IsDatabaseSelectionRemembered) { return false; }
                if (other.IsEntitySelectionRemembered != this.IsEntitySelectionRemembered) { return false; }
                if (other.PlanAssignmentViewBy != this.PlanAssignmentViewBy) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
