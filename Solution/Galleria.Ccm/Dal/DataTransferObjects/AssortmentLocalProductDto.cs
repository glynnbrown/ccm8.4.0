﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)

// CCM-25454 : J.Pickup
//		Created (Auto-generated)
// V8-26704 : A.Kuszyk
//  Added LocationCode and ProductGtin properties.
//  Added ForeignKey and InheritedProperties attributes.
// V8-26322 : A.Silva
//      Amended LocationId and ProductId Properties to have DeleteBehavior.Leave (parent is marked as deleted, not really deleted).

#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// AssortmentLocalProduct Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class AssortmentLocalProductDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public Int32 AssortmentId { get; set; }
        public AssortmentLocalProductDtoKey DtoKey
        {
            get
            {
                return new AssortmentLocalProductDtoKey()
                {
                    LocationId = this.LocationId,
                    ProductId = this.ProductId
                };
            }
        }
        [ForeignKey(typeof(LocationDto),typeof(ILocationDal), DeleteBehavior.Leave)]
        public Int16 LocationId { get; set; }
        [ForeignKey(typeof(ProductDto), typeof(IProductDal), DeleteBehavior.Leave)]
        public Int32 ProductId { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal),ForeignKeyPropertyName="LocationId",ParentPropertyName="Code")]
        public String LocationCode { get; set; }
        [InheritedProperty(typeof(ProductDto), typeof(IProductDal),ForeignKeyPropertyName="ProductId",ParentPropertyName="Gtin")]
        public String ProductGTIN { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentLocalProductDto other = obj as AssortmentLocalProductDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.LocationId != this.LocationId) { return false; }
                if (other.ProductId != this.ProductId) { return false; }
                if (other.AssortmentId != this.AssortmentId) { return false; }
                if (other.ProductGTIN != this.ProductGTIN) { return false; }
                if (other.LocationCode != this.LocationCode) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentLocalProductDtoKey
    {
        #region Properties
        public Int32 LocationId { get; set; }
        public Int32 ProductId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                LocationId.GetHashCode() +
                ProductId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentLocalProductDtoKey other = obj as AssortmentLocalProductDtoKey;
            if (other != null)
            {
                if (other.LocationId != this.LocationId) { return false; }
                if (other.ProductId != this.ProductId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentLocalProductIsSetDto
    {
        #region Properties
        public Boolean IsLocationIdSet { get; set; }
        public Boolean IsProductIdSet { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public AssortmentLocalProductIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public AssortmentLocalProductIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsLocationIdSet.GetHashCode() +
                IsProductIdSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentLocalProductIsSetDto other = obj as AssortmentLocalProductIsSetDto;
            if (other != null)
            {
                if (other.IsLocationIdSet != this.IsLocationIdSet) { return false; }
                if (other.IsProductIdSet != this.IsProductIdSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {

            IsLocationIdSet = isSet;
            IsProductIdSet = isSet;
        }
        #endregion
    }
}
