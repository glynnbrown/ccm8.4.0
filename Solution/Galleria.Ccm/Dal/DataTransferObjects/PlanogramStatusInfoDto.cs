﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public sealed class PlanogramStatusInfoDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public String Name { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the instance has code
        /// </summary>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Compares two instances to each other
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            PlanogramStatusInfoDto other = obj as PlanogramStatusInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.Name != this.Name) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
