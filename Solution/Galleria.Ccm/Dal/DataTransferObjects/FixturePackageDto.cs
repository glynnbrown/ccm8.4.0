﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created
// V8-25873 : A.Probyn
//  Added Custom Attribute 1 - 5
#endregion
#endregion

using System;
using System.Linq;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// FixturePackage Data Transfer Object
    /// </summary>
    [Serializable]
    public class FixturePackageDto
    {
        #region Properties
        public Object Id { get; set; }
        public FixturePackageDtoKey DtoKey
        {
            get
            {
                return new FixturePackageDtoKey()
                {
                    Id = this.Id,
                };
            }
        }
        public String Name { get; set; }
        public String Description { get; set; }
        public Single Height { get; set; }
        public Single Width { get; set; }
        public Single Depth { get; set; }
        public Int32 FixtureCount { get; set; }
        public Int32 AssemblyCount { get; set; }
        public Int32 ComponentCount { get; set; }
        public Byte[] ThumbnailImageData { get; set; }
        public Byte LengthUnitsOfMeasure { get; set; }
        public Byte AreaUnitsOfMeasure { get; set; }
        public Byte VolumeUnitsOfMeasure { get; set; }
        public Byte WeightUnitsOfMeasure { get; set; }
        public Byte AngleUnitsOfMeasure { get; set; }
        public Byte CurrencyUnitsOfMeasure { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public String CreatedBy { get; set; }
        public String LastModifiedBy { get; set; }
        public Object ExtendedData { get; set; }
        public String CustomAttribute1 { get; set; }
        public String CustomAttribute2 { get; set; }
        public String CustomAttribute3 { get; set; }
        public String CustomAttribute4 { get; set; }
        public String CustomAttribute5 { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FixturePackageDto other = obj as FixturePackageDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }

                if (other.Name != this.Name) { return false; }
                if (other.Description != this.Description) { return false; }
                if (other.Height != this.Height) { return false; }
                if (other.Width != this.Width) { return false; }
                if (other.Depth != this.Depth) { return false; }
                if (other.FixtureCount != this.FixtureCount) { return false; }
                if (other.AssemblyCount != this.AssemblyCount) { return false; }
                if (other.ComponentCount != this.ComponentCount) { return false; }

                //ThumbnailImageData
                if ((other.ThumbnailImageData != null) && (this.ThumbnailImageData == null))
                {
                    return false;
                }
                if ((other.ThumbnailImageData == null) && (this.ThumbnailImageData != null))
                {
                    return false;
                }
                if ((other.ThumbnailImageData != null) && (this.ThumbnailImageData != null) && (!other.ThumbnailImageData.SequenceEqual(this.ThumbnailImageData)))
                {
                    return false;
                }
                
                
                if (other.LengthUnitsOfMeasure != this.LengthUnitsOfMeasure) { return false; }
                if (other.AreaUnitsOfMeasure != this.AreaUnitsOfMeasure) { return false; }
                if (other.VolumeUnitsOfMeasure != this.VolumeUnitsOfMeasure) { return false; }
                if (other.WeightUnitsOfMeasure != this.WeightUnitsOfMeasure) { return false; }
                if (other.AngleUnitsOfMeasure != this.AngleUnitsOfMeasure) { return false; }
                if (other.CurrencyUnitsOfMeasure != this.CurrencyUnitsOfMeasure) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.CreatedBy != this.CreatedBy) { return false; }
                if (other.LastModifiedBy != this.LastModifiedBy) { return false; }

                if (other.CustomAttribute1 != this.CustomAttribute1) { return false; }
                if (other.CustomAttribute2 != this.CustomAttribute2) { return false; }
                if (other.CustomAttribute3 != this.CustomAttribute3) { return false; }
                if (other.CustomAttribute4 != this.CustomAttribute4) { return false; }
                if (other.CustomAttribute5 != this.CustomAttribute5) { return false; }


                //ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData != null))
                {
                    if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
                }
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class FixturePackageDtoKey
    {
        #region Properties
        public Object Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FixturePackageDtoKey other = obj as FixturePackageDtoKey;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
