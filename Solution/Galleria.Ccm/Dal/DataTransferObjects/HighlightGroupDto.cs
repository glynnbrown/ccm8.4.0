﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 L.Hodson
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;


namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// HighlightGroup data transfer object
    /// </summary>
    [Serializable]
    public sealed class HighlightGroupDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public HighlightGroupDtoKey DtoKey
        {
            get { return new HighlightGroupDtoKey() { Id = this.Id }; }
        }
        [ForeignKey(typeof(HighlightDto), typeof(IHighlightDal))]
        public Object HighlightId { get; set; }
        public Int32 Order { get; set; }
        public String Name { get; set; }
        public String DisplayName { get; set; }
        public Int32 FillColour { get; set; }
        public Byte FillPatternType { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            HighlightGroupDto other = obj as HighlightGroupDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if ((other.HighlightId != null) && (this.HighlightId != null))
                {
                    if (!other.HighlightId.Equals(this.HighlightId)) { return false; }
                }
                if ((other.HighlightId != null) && (this.HighlightId == null)) { return false; }
                if ((other.HighlightId == null) && (this.HighlightId != null)) { return false; }
                if (other.Order != this.Order) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.DisplayName != this.DisplayName) { return false; }
                if (other.FillColour != this.FillColour) { return false; }
                if (other.FillPatternType != this.FillPatternType) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    /// <summary>
    /// HighlightGroup data transfer object key
    /// </summary>
    [Serializable]
    public sealed class HighlightGroupDtoKey
    {
        #region Properties
        public Int32 Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the ISOme
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            HighlightGroupDtoKey other = obj as HighlightGroupDtoKey;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
