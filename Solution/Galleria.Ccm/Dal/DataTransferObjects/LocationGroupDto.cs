﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// LocationGroup Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class LocationGroupDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public LocationGroupDtoKey DtoKey
        {
            get
            {
                return new LocationGroupDtoKey()
                {
                    LocationLevelId = this.LocationLevelId,
                    Code = this.Code
                };
            }
        }
        [InheritedProperty(typeof(LocationLevelDto), typeof(ILocationLevelDal))]
        [ForeignKey(typeof(LocationHierarchyDto), typeof(ILocationHierarchyDal), DeleteBehavior.Leave)]
        public Int32 LocationHierarchyId { get; set; }
        [ForeignKey(typeof(LocationLevelDto), typeof(ILocationLevelDal), DeleteBehavior.MoveUpToParent)]
        public Int32 LocationLevelId { get; set; }
        public String Name { get; set; }
        public String Code { get; set; }
        public Int32 AssignedLocationsCount { get; set; }
        public Int32? ParentGroupId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            LocationGroupDto other = obj as LocationGroupDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.LocationHierarchyId != this.LocationHierarchyId) { return false; }
                if (other.LocationLevelId != this.LocationLevelId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.Code != this.Code) { return false; }
                if (other.AssignedLocationsCount != this.AssignedLocationsCount) { return false; }
                if (other.ParentGroupId != this.ParentGroupId) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.DateDeleted != this.DateDeleted) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class LocationGroupDtoKey
    {
        #region Properties
        public Int32 LocationLevelId { get; set; }
        public String Code { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                LocationLevelId.GetHashCode() +
                Code.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            LocationGroupDtoKey other = obj as LocationGroupDtoKey;
            if (other != null)
            {
                if (other.LocationLevelId != this.LocationLevelId) { return false; }
                if (other.Code != this.Code) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class LocationGroupIsSetDto
    {
        #region Properties
        public Boolean IsLocationHierarchyIdSet { get; set; }
        public Boolean IsLocationLevelIdSet { get; set; }
        public Boolean IsNameSet { get; set; }
        public Boolean IsCodeSet { get; set; }
        public Boolean IsParentGroupIdSet { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public LocationGroupIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public LocationGroupIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsLocationHierarchyIdSet.GetHashCode() +
                IsLocationLevelIdSet.GetHashCode() +
                IsNameSet.GetHashCode() +
                IsCodeSet.GetHashCode() +
                IsParentGroupIdSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            LocationGroupIsSetDto other = obj as LocationGroupIsSetDto;
            if (other != null)
            {
                if (other.IsLocationHierarchyIdSet != this.IsLocationHierarchyIdSet) { return false; }
                if (other.IsLocationLevelIdSet != this.IsLocationLevelIdSet) { return false; }
                if (other.IsNameSet != this.IsNameSet) { return false; }
                if (other.IsCodeSet != this.IsCodeSet) { return false; }
                if (other.IsParentGroupIdSet != this.IsParentGroupIdSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsLocationHierarchyIdSet = isSet;
            IsLocationLevelIdSet = isSet;
            IsNameSet = isSet;
            IsCodeSet = isSet;
            IsParentGroupIdSet = isSet;
        }
        #endregion
    }
}
