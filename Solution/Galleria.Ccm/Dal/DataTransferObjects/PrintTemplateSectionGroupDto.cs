﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// PrintTemplateSectionGroup Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class PrintTemplateSectionGroupDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public PrintTemplateSectionGroupDtoKey DtoKey
        {
            get
            {
                return new PrintTemplateSectionGroupDtoKey()
                {
                    PrintTemplateId = this.PrintTemplateId,
                    Number = this.Number
                };
            }
        }
        [ForeignKey(typeof(PrintTemplateDto), typeof(IPrintTemplateDal), DeleteBehavior.Cascade)]
        public Object PrintTemplateId { get; set; }
        public Byte Number { get; set; }
        public String Name { get; set; }
        public Byte BaysPerPage { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PrintTemplateSectionGroupDto other = obj as PrintTemplateSectionGroupDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }

                //PrintTemplateId
                if ((other.PrintTemplateId != null) && (this.PrintTemplateId != null))
                {
                    if (!other.PrintTemplateId.Equals(this.PrintTemplateId)) { return false; }
                }
                if ((other.PrintTemplateId != null) && (this.PrintTemplateId == null)) { return false; }
                if ((other.PrintTemplateId == null) && (this.PrintTemplateId != null)) { return false; }

               
                if (other.Number != this.Number) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.BaysPerPage != this.BaysPerPage) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    /// <summary>
    /// PrintTemplateSectionGroup Dto Key
    /// </summary>
    [Serializable]
    public sealed class PrintTemplateSectionGroupDtoKey
    {
        #region Properties
        public Object PrintTemplateId { get; set; }
        public Byte Number { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return PrintTemplateId.GetHashCode() ^
                Number.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PrintTemplateSectionGroupDtoKey other = obj as PrintTemplateSectionGroupDtoKey;
            if (other != null)
            {

                //PrintTemplateId
                if ((other.PrintTemplateId != null) && (this.PrintTemplateId != null))
                {
                    if (!other.PrintTemplateId.Equals(this.PrintTemplateId)) { return false; }
                }
                if ((other.PrintTemplateId != null) && (this.PrintTemplateId == null)) { return false; }
                if ((other.PrintTemplateId == null) && (this.PrintTemplateId != null)) { return false; }

                if (other.Number != this.Number) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

}
