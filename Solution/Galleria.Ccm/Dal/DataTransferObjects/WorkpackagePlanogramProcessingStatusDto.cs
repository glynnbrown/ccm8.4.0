﻿
using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class WorkpackagePlanogramProcessingStatusDto
    {
        #region Properties

        [ForeignKey(typeof(PlanogramDto), typeof(IPlanogramDal), DeleteBehavior.Leave)]
        public Int32 PlanogramId { get; set; }

        [ForeignKey(typeof(WorkpackageDto), typeof(IWorkpackageDal), DeleteBehavior.Leave)]
        public Int32 WorkpackageId { get; set; }
        public RowVersion RowVersion { get; set; }
        public Byte AutomationStatus { get; set; }
        public String AutomationStatusDescription { get; set; }
        public Int32 AutomationProgressMax { get; set; }
        public Int32 AutomationProgressCurrent { get; set; }
        public DateTime? AutomationDateStarted { get; set; }
        public DateTime? AutomationDateLastUpdated { get; set; }
        

        public WorkpackagePlanogramProcessingStatusDtoKey DtoKey
        {
            get { return new WorkpackagePlanogramProcessingStatusDtoKey { PlanogramId = PlanogramId, WorkpackageId = WorkpackageId }; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a hash code for this instance
        /// </summary>
        public override Int32 GetHashCode()
        {
            return HashCode.Start
                .Hash(PlanogramId)
                .Hash(WorkpackageId)
                .GetHashCode();
        }

        /// <summary>
        /// Compares that two instances are equal
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            WorkpackagePlanogramProcessingStatusDto other = obj as WorkpackagePlanogramProcessingStatusDto;
            if (other != null)
            {
                if (other.PlanogramId != this.PlanogramId) return false;
                if (other.WorkpackageId != this.WorkpackageId) return false;
                if (other.RowVersion != this.RowVersion) return false;
                if (other.AutomationStatus != this.AutomationStatus) return false;
                if (other.AutomationStatusDescription != this.AutomationStatusDescription) return false;
                if (other.AutomationProgressMax != this.AutomationProgressMax) return false;
                if (other.AutomationProgressCurrent != this.AutomationProgressCurrent) return false;
                if (other.AutomationDateStarted != this.AutomationDateStarted) return false;
                if (other.AutomationDateLastUpdated != this.AutomationDateLastUpdated) return false;
            }
            else
                return false;
            return true;
        }

        #endregion
    }

    [Serializable]
    public class WorkpackagePlanogramProcessingStatusDtoKey
    {
        #region Properties

        public Int32 PlanogramId { get; set; }
        public Int32 WorkpackageId { get; set; }

        #endregion

        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return HashCode.Start
                .Hash(PlanogramId)
                .Hash(WorkpackageId)
                .GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {

            WorkpackagePlanogramProcessingStatusDtoKey other = obj as WorkpackagePlanogramProcessingStatusDtoKey;
            if (other != null)
            {
                if (other.PlanogramId != this.PlanogramId) { return false; }
                if (other.WorkpackageId != this.WorkpackageId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
    }
}
