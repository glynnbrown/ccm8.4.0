﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created (Auto-generated)
// V8-26815 : A.Silva ~ Changed Id to be of type Object (as it can either be a normal integer Id or a file path Id).
// V8-26981 : A.Silva ~ Corrected Equality implementation.
// V8-27076 : A.Silva ~ Corrected Equality for Object properties.

#endregion

#region Version History: (CCM v8.03)
// V8-29219 : L.Ineson
//  Removed date deleted
#endregion

#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    ///     ValidationTemplate Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class ValidationTemplateDto
    {
        #region Properties

        public Object Id { get; set; }

        public ValidationTemplateDtoKey DtoKey
        {
            get
            {
                return new ValidationTemplateDtoKey
                {
                    Name = Name,
                    EntityId = EntityId
                };
            }
        }

        [ForeignKey(typeof (EntityDto), typeof (IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }

        public String Name { get; set; }
        public RowVersion RowVersion { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ValidationTemplateDto other = obj as ValidationTemplateDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
            }
            else
            {
                return false;
            }
            return true;

        }

        /// <summary>
        ///     Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode() +
                   RowVersion.GetHashCode() +
                   EntityId.GetHashCode() +
                   Name.GetHashCode() +
                   DateCreated.GetHashCode() +
                   DateLastModified.GetHashCode() ;
        }

        #endregion
    }

    [Serializable]
    public sealed class ValidationTemplateDtoKey
    {
        #region Properties

        [ForeignKey(typeof (EntityDto), typeof (IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { private get; set; }

        public String Name { private get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ValidationTemplateDtoKey other = obj as ValidationTemplateDtoKey;
            if (other != null)
            {
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        ///     Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return EntityId.GetHashCode() +
                   Name.GetHashCode();
        }

        #endregion
    }
}