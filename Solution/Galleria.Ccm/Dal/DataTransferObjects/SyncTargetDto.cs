﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class SyncTargetDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public Byte TargetType { get; set; }
        public String ConnectionString { get; set; }
        public Byte Status { get; set; }
        public Byte Frequency { get; set; }
        public DateTime? LastSync { get; set; }
        public Byte Error { get; set; }
        public Boolean IsActive { get; set; }
        public Int32 EntityId { get; set; }
        public Int32 RetryCount { get; set; }
        public Int32 FailedCount { get; set; }
        public DateTime? LastRun { get; set; }
        public SyncTargetDtoKey DtoKey
        {
            get
            {
                return new SyncTargetDtoKey()
                {
                    EntityId = this.EntityId,
                    ConnectionString = this.ConnectionString
                };
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the hash code for this instance
        /// </summary>
        /// <returns>The instance hash code</returns>
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Determines if an instance is equal to this one
        /// </summary>
        /// <param name="obj">The instance to compare to</param>
        /// <returns>True if equal, else false</returns>
        public override bool Equals(object obj)
        {
            SyncTargetDto other = obj as SyncTargetDto;
            if (other != null)
            {
                if (other.Id != this.Id)
                {
                    return false;
                }
                if (other.TargetType != this.TargetType)
                {
                    return false;
                }
                if (other.ConnectionString != this.ConnectionString)
                {
                    return false;
                }
                if (other.Status != this.Status)
                {
                    return false;
                }
                if (other.Frequency != this.Frequency)
                {
                    return false;
                }
                if (other.LastSync != this.LastSync)
                {
                    return false;
                }
                if (other.Error != this.Error)
                {
                    return false;
                }
                if (other.IsActive != this.IsActive)
                {
                    return false;
                }
                if (other.EntityId != this.EntityId)
                {
                    return false;
                }
                if (other.RetryCount != this.RetryCount)
                {
                    return false;
                }
                if (other.FailedCount != this.FailedCount)
                {
                    return false;
                }
                if (other.LastRun != this.LastRun)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class SyncTargetIsSetDto
    {
        #region Properties
        public Boolean IsTargetTypeSet { get; set; }
        public Boolean IsConnectionStringSet { get; set; }
        public Boolean IsStatusSet { get; set; }
        public Boolean IsFrequencySet { get; set; }
        public Boolean IsLastSyncSet { get; set; }
        public Boolean IsErrorSet { get; set; }
        public Boolean IsIsActiveSet { get; set; }
        public Boolean IsEntityIdSet { get; set; }
        public Boolean IsRetryCountSet { get; set; }
        public Boolean IsFailedCountSet { get; set; }
        public Boolean IsLastRunSet { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the hash code for this instance
        /// </summary>
        /// <returns>The instance hash code</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Determines if an instance is equal to this one
        /// </summary>
        /// <param name="obj">The instance to compare to</param>
        /// <returns>True if equal, else false</returns>
        public override bool Equals(object obj)
        {
            SyncTargetIsSetDto other = obj as SyncTargetIsSetDto;
            if (other != null)
            {
                if (other.IsTargetTypeSet != this.IsTargetTypeSet)
                {
                    return false;
                }
                if (other.IsConnectionStringSet != this.IsConnectionStringSet)
                {
                    return false;
                }
                if (other.IsStatusSet != this.IsStatusSet)
                {
                    return false;
                }
                if (other.IsFrequencySet != this.IsFrequencySet)
                {
                    return false;
                }
                if (other.IsLastSyncSet != this.IsLastSyncSet)
                {
                    return false;
                }
                if (other.IsErrorSet != this.IsErrorSet)
                {
                    return false;
                }
                if (other.IsIsActiveSet != this.IsIsActiveSet)
                {
                    return false;
                }
                if (other.IsEntityIdSet != this.IsEntityIdSet)
                {
                    return false;
                }
                if (other.IsRetryCountSet != this.IsRetryCountSet)
                {
                    return false;
                }
                if (other.IsFailedCountSet != this.IsFailedCountSet)
                {
                    return false;
                }
                if (other.IsLastRunSet != this.IsLastRunSet)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    public class SyncTargetDtoKey
    {
        #region Properties
        public Int32 EntityId { get; set; }
        public String ConnectionString { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return EntityId.GetHashCode() +
                ConnectionString.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            SyncTargetDtoKey other = obj as SyncTargetDtoKey;
            if (other != null)
            {

                if (other.EntityId != this.EntityId) { return false; }
                if (other.ConnectionString != this.ConnectionString) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}