﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    ///     Planogram Comparison Template Data Transfer Object.
    /// </summary>
    [Serializable]
    public sealed class PlanogramComparisonTemplateDto
    {
        #region Properties

        public Object Id { get; set; }
        public PlanogramComparisonTemplateDtoKey DtoKey { get { return new PlanogramComparisonTemplateDtoKey {Name = Name, EntityId = EntityId}; } }

        [ForeignKey(typeof (EntityDto), typeof (IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }

        public String Name { get; set; }
        public String Description { get; set; }
        public Boolean IgnoreNonPlacedProducts { get; set; }
        public Byte DataOrderType { get; set; }

        public RowVersion RowVersion { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>. </param>
        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramComparisonTemplateDto;
            return other != null &&
                   Equals(other.Id, Id) &&
                   Equals(other.RowVersion, RowVersion) &&
                   Equals(other.EntityId, EntityId) &&
                   String.Equals(other.Name, Name) &&
                   String.Equals(other.Description, Description) &&
                   Equals(other.IgnoreNonPlacedProducts, IgnoreNonPlacedProducts) &&
                   Equals(other.DataOrderType, DataOrderType) &&
                   Equals(other.DateCreated, DateCreated) &&
                   Equals(other.DateLastModified, DateLastModified);
        }

        /// <summary>
        /// Serves as a hash function for a particular type. 
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override Int32 GetHashCode()
        {
            return HashCode.Start.Hash(Id).Hash(EntityId).GetHashCode();
        }

        #endregion
    }

    [Serializable]
    public sealed class PlanogramComparisonTemplateDtoKey
    {
        #region Properties

        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        public String Name { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>. </param>
        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramComparisonTemplateDtoKey;
            return other != null &&
                   Equals(other.EntityId, EntityId) &&
                   Equals(other.Name, Name);
        }

        /// <summary>
        /// Serves as a hash function for a particular type. 
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override Int32 GetHashCode()
        {
            return HashCode.Start.Hash(EntityId).Hash(Name).GetHashCode();
        }

        #endregion
    }
}
