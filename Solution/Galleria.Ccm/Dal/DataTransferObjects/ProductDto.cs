﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8)
// CCM-0 : L.Hodson
//		Created (Auto-generated)
// V8-25630 : A.Kuszyk
//      Added DtoKey property. Removed IsProductGroupIdSet and IsIsCreatedSet
//      from IsProductDtoSet class inline with ProductDto.
// V8-25630 : A.Kuszyk
//      Added EntityId property.
// CCM-25452 : N.Haywood
//	    Readded product group id
// V8-26041 : A.Kuszyk
//      Changed FillPattern (String) to FillPatternType (Byte) and IsNew to IsNewProduct.
//      Added ShapeType property and changed FillColour to Int32.
// V8-27424 : L.Luong
//      Added FinancialGroupCode and FinancialGroupName
// V8-26385 : L.Luong
//      Added RowVersion
// V8-26777 : L.Ineson
//  Removed CanMerch properties

#endregion
#region Version History: CCM830
//  V8-31531 : A.Heathcote
//      Removed IsTrayProduct
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Product Data Transfer Object
    /// </summary>
    [Serializable]
    public class ProductDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public ProductDtoKey DtoKey
        {
            get
            {
                return new ProductDtoKey
                {
                    Gtin = this.Gtin,
                    EntityId = this.EntityId
                };
            }
        }
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        [ForeignKey(typeof(ProductGroupDto), typeof(IProductGroupDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32? ProductGroupId { get; set; }
        [ForeignKey(typeof(ProductDto), typeof(IProductDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32? ReplacementProductId { get; set; }
        public String Gtin { get; set; }
        public String Name { get; set; }
        public Single Height { get; set; }
        public Single Width { get; set; }
        public Single Depth { get; set; }
        public Single DisplayHeight { get; set; }
        public Single DisplayWidth { get; set; }
        public Single DisplayDepth { get; set; }
        public Single AlternateHeight { get; set; }
        public Single AlternateWidth { get; set; }
        public Single AlternateDepth { get; set; }
        public Single PointOfPurchaseHeight { get; set; }
        public Single PointOfPurchaseWidth { get; set; }
        public Single PointOfPurchaseDepth { get; set; }
        public Byte NumberOfPegHoles { get; set; }
        public Single PegX { get; set; }
        public Single PegX2 { get; set; }
        public Single PegX3 { get; set; }
        public Single PegY { get; set; }
        public Single PegY2 { get; set; }
        public Single PegY3 { get; set; }
        //public Single PegProngOffset { get; set; }
        public Single PegProngOffsetX { get; set; }
        public Single PegProngOffsetY { get; set; }
        public Single PegDepth { get; set; }
        public Single SqueezeHeight { get; set; }
        public Single SqueezeWidth { get; set; }
        public Single SqueezeDepth { get; set; }
        public Single NestingHeight { get; set; }
        public Single NestingWidth { get; set; }
        public Single NestingDepth { get; set; }
        public Int16 CasePackUnits { get; set; }
        public Byte CaseHigh { get; set; }
        public Byte CaseWide { get; set; }
        public Byte CaseDeep { get; set; }
        public Single CaseHeight { get; set; }
        public Single CaseWidth { get; set; }
        public Single CaseDepth { get; set; }
        public Byte MaxStack { get; set; }
        public Byte MaxTopCap { get; set; }
        public Byte MaxRightCap { get; set; }
        public Byte MinDeep { get; set; }
        public Byte MaxDeep { get; set; }
        public Int16 TrayPackUnits { get; set; }
        public Byte TrayHigh { get; set; }
        public Byte TrayWide { get; set; }
        public Byte TrayDeep { get; set; }
        public Single TrayHeight { get; set; }
        public Single TrayWidth { get; set; }
        public Single TrayDepth { get; set; }
        public Single TrayThickHeight { get; set; }
        public Single TrayThickWidth { get; set; }
        public Single TrayThickDepth { get; set; }
        public Single FrontOverhang { get; set; }
        public Single FingerSpaceAbove { get; set; }
        public Single FingerSpaceToTheSide { get; set; }
        public Byte StatusType { get; set; }
        public Byte OrientationType { get; set; }
        public Byte MerchandisingStyle { get; set; }
        public Boolean IsFrontOnly { get; set; }
        public Boolean IsPlaceHolderProduct { get; set; }
        public Boolean IsActive { get; set; }
        public Boolean CanBreakTrayUp { get; set; }
        public Boolean CanBreakTrayDown { get; set; }
        public Boolean CanBreakTrayBack { get; set; }
        public Boolean CanBreakTrayTop { get; set; }
        public Boolean ForceMiddleCap { get; set; }
        public Boolean ForceBottomCap { get; set; }
        public String FinancialGroupCode { get; set; }
        public String FinancialGroupName { get; set; }
        //New CCM properties
        public String Shape { get; set; }
        public Byte ShapeType { get; set; }
        public Int32 FillColour { get; set; }
        public Byte FillPatternType { get; set; }
        //Start of old attribute data
        public String PointOfPurchaseDescription { get; set; }
        public String ShortDescription { get; set; }
        public String Subcategory { get; set; }
        public String CustomerStatus { get; set; }
        public String Colour { get; set; }
        public String Flavour { get; set; }
        public String PackagingShape { get; set; }
        public String PackagingType { get; set; }
        public String CountryOfOrigin { get; set; }
        public String CountryOfProcessing { get; set; }
        public Int16 ShelfLife { get; set; }
        public Single? DeliveryFrequencyDays { get; set; }
        public String DeliveryMethod { get; set; }
        public String Brand { get; set; }
        public String VendorCode { get; set; }
        public String Vendor { get; set; }
        public String ManufacturerCode { get; set; }
        public String Manufacturer { get; set; }
        public String Size { get; set; }
        public String UnitOfMeasure { get; set; }
        public DateTime? DateIntroduced { get; set; }
        public DateTime? DateDiscontinued { get; set; }
        public DateTime? DateEffective { get; set; }
        public String Health { get; set; }
        public String CorporateCode { get; set; }
        public String Barcode { get; set; }
        public Single? SellPrice { get; set; }
        public Int16? SellPackCount { get; set; }
        public String SellPackDescription { get; set; }
        public Single? RecommendedRetailPrice { get; set; }
        public Single? ManufacturerRecommendedRetailPrice { get; set; }
        public Single? CostPrice { get; set; }
        public Single? CaseCost { get; set; }
        public Single? TaxRate { get; set; }
        public String ConsumerInformation { get; set; }
        public String Texture { get; set; }
        public Int16? StyleNumber { get; set; }
        public String Pattern { get; set; }
        public String Model { get; set; }
        public String GarmentType { get; set; }
        public Boolean IsPrivateLabel { get; set; }
        public Boolean IsNewProduct { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ProductDto other = obj as ProductDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.ProductGroupId != this.ProductGroupId) { return false; }
                if (other.ReplacementProductId != this.ReplacementProductId) { return false; }
                if (other.Gtin != this.Gtin) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.Brand != this.Brand) { return false; }
                if (other.Height != this.Height) { return false; }
                if (other.Width != this.Width) { return false; }
                if (other.Depth != this.Depth) { return false; }
                if (other.DisplayHeight != this.DisplayHeight) { return false; }
                if (other.DisplayWidth != this.DisplayWidth) { return false; }
                if (other.DisplayDepth != this.DisplayDepth) { return false; }
                if (other.AlternateHeight != this.AlternateHeight) { return false; }
                if (other.AlternateWidth != this.AlternateWidth) { return false; }
                if (other.AlternateDepth != this.AlternateDepth) { return false; }
                if (other.PointOfPurchaseHeight != this.PointOfPurchaseHeight) { return false; }
                if (other.PointOfPurchaseWidth != this.PointOfPurchaseWidth) { return false; }
                if (other.PointOfPurchaseDepth != this.PointOfPurchaseDepth) { return false; }
                if (other.NumberOfPegHoles != this.NumberOfPegHoles) { return false; }
                if (other.PegX != this.PegX) { return false; }
                if (other.PegX2 != this.PegX2) { return false; }
                if (other.PegX3 != this.PegX3) { return false; }
                if (other.PegY != this.PegY) { return false; }
                if (other.PegY2 != this.PegY2) { return false; }
                if (other.PegY3 != this.PegY3) { return false; }
                if (other.PegProngOffsetX != this.PegProngOffsetX) { return false; }
                if (other.PegProngOffsetY != this.PegProngOffsetY) { return false; }
                if (other.PegDepth != this.PegDepth) { return false; }
                if (other.SqueezeHeight != this.SqueezeHeight) { return false; }
                if (other.SqueezeWidth != this.SqueezeWidth) { return false; }
                if (other.SqueezeDepth != this.SqueezeDepth) { return false; }
                if (other.NestingHeight != this.NestingHeight) { return false; }
                if (other.NestingWidth != this.NestingWidth) { return false; }
                if (other.NestingDepth != this.NestingDepth) { return false; }
                if (other.CasePackUnits != this.CasePackUnits) { return false; }
                if (other.CaseHigh != this.CaseHigh) { return false; }
                if (other.CaseWide != this.CaseWide) { return false; }
                if (other.CaseDeep != this.CaseDeep) { return false; }
                if (other.CaseHeight != this.CaseHeight) { return false; }
                if (other.CaseWidth != this.CaseWidth) { return false; }
                if (other.CaseDepth != this.CaseDepth) { return false; }
                if (other.MaxStack != this.MaxStack) { return false; }
                if (other.MaxTopCap != this.MaxTopCap) { return false; }
                if (other.MaxRightCap != this.MaxRightCap) { return false; }
                if (other.MinDeep != this.MinDeep) { return false; }
                if (other.MaxDeep != this.MaxDeep) { return false; }
                if (other.TrayPackUnits != this.TrayPackUnits) { return false; }
                if (other.TrayHigh != this.TrayHigh) { return false; }
                if (other.TrayWide != this.TrayWide) { return false; }
                if (other.TrayDeep != this.TrayDeep) { return false; }
                if (other.TrayHeight != this.TrayHeight) { return false; }
                if (other.TrayWidth != this.TrayWidth) { return false; }
                if (other.TrayDepth != this.TrayDepth) { return false; }
                if (other.TrayThickHeight != this.TrayThickHeight) { return false; }
                if (other.TrayThickWidth != this.TrayThickWidth) { return false; }
                if (other.TrayThickDepth != this.TrayThickDepth) { return false; }
                if (other.FrontOverhang != this.FrontOverhang) { return false; }
                if (other.FingerSpaceAbove != this.FingerSpaceAbove) { return false; }
                if (other.FingerSpaceToTheSide != this.FingerSpaceToTheSide) { return false; }
                if (other.StatusType != this.StatusType) { return false; }
                if (other.OrientationType != this.OrientationType) { return false; }
                if (other.MerchandisingStyle != this.MerchandisingStyle) { return false; }
                if (other.IsFrontOnly != this.IsFrontOnly) { return false; }
                if (other.IsPlaceHolderProduct != this.IsPlaceHolderProduct) { return false; }
                if (other.IsActive != this.IsActive) { return false; }
                if (other.CanBreakTrayUp != this.CanBreakTrayUp) { return false; }
                if (other.CanBreakTrayDown != this.CanBreakTrayDown) { return false; }
                if (other.CanBreakTrayBack != this.CanBreakTrayBack) { return false; }
                if (other.CanBreakTrayTop != this.CanBreakTrayTop) { return false; }
                if (other.ForceMiddleCap != this.ForceMiddleCap) { return false; }
                if (other.ForceBottomCap != this.ForceBottomCap) { return false; }
                if (other.FinancialGroupCode != this.FinancialGroupCode) { return false; }
                if (other.FinancialGroupName != this.FinancialGroupName) { return false; }
                //New CCM properties
                if (other.Shape != this.Shape) { return false; }
                if (other.ShapeType != this.ShapeType) { return false; }
                if (other.FillColour != this.FillColour) { return false; }
                if (other.FillPatternType != this.FillPatternType) { return false; }
                //Start of old attribute data
                if (other.PointOfPurchaseDescription != this.PointOfPurchaseDescription) { return false; }
                if (other.ShortDescription != this.ShortDescription) { return false; }
                if (other.Subcategory != this.Subcategory) { return false; }
                if (other.CustomerStatus != this.CustomerStatus) { return false; }
                if (other.Colour != this.Colour) { return false; }
                if (other.Flavour != this.Flavour) { return false; }
                if (other.PackagingShape != this.PackagingShape) { return false; }
                if (other.PackagingType != this.PackagingType) { return false; }
                if (other.CountryOfOrigin != this.CountryOfOrigin) { return false; }
                if (other.CountryOfProcessing != this.CountryOfProcessing) { return false; }
                if (other.ShelfLife != this.ShelfLife) { return false; }
                if (other.DeliveryFrequencyDays != this.DeliveryFrequencyDays) { return false; }
                if (other.DeliveryMethod != this.DeliveryMethod) { return false; }
                if (other.Brand != this.Brand) { return false; }
                if (other.VendorCode != this.VendorCode) { return false; }
                if (other.Vendor != this.Vendor) { return false; }
                if (other.ManufacturerCode != this.ManufacturerCode) { return false; }
                if (other.Manufacturer != this.Manufacturer) { return false; }
                if (other.Size != this.Size) { return false; }
                if (other.UnitOfMeasure != this.UnitOfMeasure) { return false; }
                if (other.DateIntroduced != this.DateIntroduced) { return false; }
                if (other.DateDiscontinued != this.DateDiscontinued) { return false; }
                if (other.DateEffective != this.DateEffective) { return false; }
                if (other.Health != this.Health) { return false; }
                if (other.CorporateCode != this.CorporateCode) { return false; }
                if (other.Barcode != this.Barcode) { return false; }
                if (other.SellPrice != this.SellPrice) { return false; }
                if (other.SellPackCount != this.SellPackCount) { return false; }
                if (other.SellPackDescription != this.SellPackDescription) { return false; }
                if (other.RecommendedRetailPrice != this.RecommendedRetailPrice) { return false; }
                if (other.ManufacturerRecommendedRetailPrice != this.ManufacturerRecommendedRetailPrice) { return false; }
                if (other.CostPrice != this.CostPrice) { return false; }
                if (other.CaseCost != this.CaseCost) { return false; }
                if (other.TaxRate != this.TaxRate) { return false; }
                if (other.ConsumerInformation != this.ConsumerInformation) { return false; }
                if (other.Texture != this.Texture) { return false; }
                if (other.StyleNumber != this.StyleNumber) { return false; }
                if (other.Pattern != this.Pattern) { return false; }
                if (other.Model != this.Model) { return false; }
                if (other.GarmentType != this.GarmentType) { return false; }
                if (other.IsPrivateLabel != this.IsPrivateLabel) { return false; }
                if (other.IsNewProduct != this.IsNewProduct) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.DateDeleted != this.DateDeleted) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class ProductDtoKey
    {
        #region Properties
        public Int32 EntityId { get; set; }
        public String Gtin { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override int GetHashCode()
        {
            return EntityId.GetHashCode() + Gtin.ToLowerInvariant().GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override bool Equals(object obj)
        {
            ProductDtoKey other = obj as ProductDtoKey;
            if (other != null)
            {
                if (StringComparer.OrdinalIgnoreCase.Compare(other.Gtin, this.Gtin) != 0) { return false; }
                if (other.EntityId != this.EntityId) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class ProductIsSetDto
    {
        #region Properties
        public Boolean IsEntityIdSet { get; set; }
        public Boolean IsProductGroupIdSet { get; set; }
        public Boolean IsReplacementProductIdSet { get; set; }
        public Boolean IsGtinSet { get; set; }
        public Boolean IsNameSet { get; set; }
        public Boolean IsHeightSet { get; set; }
        public Boolean IsWidthSet { get; set; }
        public Boolean IsDepthSet { get; set; }
        public Boolean IsDisplayHeightSet { get; set; }
        public Boolean IsDisplayWidthSet { get; set; }
        public Boolean IsDisplayDepthSet { get; set; }
        public Boolean IsAlternateHeightSet { get; set; }
        public Boolean IsAlternateWidthSet { get; set; }
        public Boolean IsAlternateDepthSet { get; set; }
        public Boolean IsPointOfPurchaseHeightSet { get; set; }
        public Boolean IsPointOfPurchaseWidthSet { get; set; }
        public Boolean IsPointOfPurchaseDepthSet { get; set; }
        public Boolean IsNumberOfPegHolesSet { get; set; }
        public Boolean IsPegXSet { get; set; }
        public Boolean IsPegX2Set { get; set; }
        public Boolean IsPegX3Set { get; set; }
        public Boolean IsPegYSet { get; set; }
        public Boolean IsPegY2Set { get; set; }
        public Boolean IsPegY3Set { get; set; }
        //public Boolean IsPegProngOffsetSet { get; set; }
        public Boolean IsPegProngOffsetXSet { get; set; }
        public Boolean IsPegProngOffsetYSet { get; set; }
        public Boolean IsPegDepthSet { get; set; }
        public Boolean IsSqueezeHeightSet { get; set; }
        public Boolean IsSqueezeWidthSet { get; set; }
        public Boolean IsSqueezeDepthSet { get; set; }
        public Boolean IsNestingHeightSet { get; set; }
        public Boolean IsNestingWidthSet { get; set; }
        public Boolean IsNestingDepthSet { get; set; }
        public Boolean IsCasePackUnitsSet { get; set; }
        public Boolean IsCaseHighSet { get; set; }
        public Boolean IsCaseWideSet { get; set; }
        public Boolean IsCaseDeepSet { get; set; }
        public Boolean IsCaseHeightSet { get; set; }
        public Boolean IsCaseWidthSet { get; set; }
        public Boolean IsCaseDepthSet { get; set; }
        public Boolean IsMaxStackSet { get; set; }
        public Boolean IsMaxTopCapSet { get; set; }
        public Boolean IsMaxRightCapSet { get; set; }
        public Boolean IsMinDeepSet { get; set; }
        public Boolean IsMaxDeepSet { get; set; }
        public Boolean IsTrayPackUnitsSet { get; set; }
        public Boolean IsTrayHighSet { get; set; }
        public Boolean IsTrayWideSet { get; set; }
        public Boolean IsTrayDeepSet { get; set; }
        public Boolean IsTrayHeightSet { get; set; }
        public Boolean IsTrayWidthSet { get; set; }
        public Boolean IsTrayDepthSet { get; set; }
        public Boolean IsTrayThickHeightSet { get; set; }
        public Boolean IsTrayThickWidthSet { get; set; }
        public Boolean IsTrayThickDepthSet { get; set; }
        public Boolean IsFrontOverhangSet { get; set; }
        public Boolean IsFingerSpaceAboveSet { get; set; }
        public Boolean IsFingerSpaceToTheSideSet { get; set; }
        public Boolean IsStatusTypeSet { get; set; }
        public Boolean IsOrientationTypeSet { get; set; }
        public Boolean IsMerchandisingStyleSet { get; set; }
        public Boolean IsIsFrontOnlySet { get; set; }
        public Boolean IsIsPlaceHolderProductSet { get; set; }
        public Boolean IsIsActiveSet { get; set; }
        public Boolean IsCanBreakTrayUpSet { get; set; }
        public Boolean IsCanBreakTrayDownSet { get; set; }
        public Boolean IsCanBreakTrayBackSet { get; set; }
        public Boolean IsCanBreakTrayTopSet { get; set; }
        public Boolean IsForceMiddleCapSet { get; set; }
        public Boolean IsForceBottomCapSet { get; set; }
        public Boolean IsShapeSet { get; set; }
        public Boolean IsShapeTypeSet { get; set; }
        public Boolean IsFillColourSet { get; set; }
        public Boolean IsFillPatternTypeSet { get; set; }
        public Boolean IsPointOfPurchaseDescriptionSet { get; set; }
        public Boolean IsShortDescriptionSet { get; set; }
        public Boolean IsSubcategorySet { get; set; }
        public Boolean IsCustomerStatusSet { get; set; }
        public Boolean IsColourSet { get; set; }
        public Boolean IsFlavourSet { get; set; }
        public Boolean IsPackagingShapeSet { get; set; }
        public Boolean IsPackagingTypeSet { get; set; }
        public Boolean IsCountryOfOriginSet { get; set; }
        public Boolean IsCountryOfProcessingSet { get; set; }
        public Boolean IsShelfLifeSet { get; set; }
        public Boolean IsDeliveryFrequencyDaysSet { get; set; }
        public Boolean IsDeliveryMethodSet { get; set; }
        public Boolean IsBrandSet { get; set; }
        public Boolean IsVendorCodeSet { get; set; }
        public Boolean IsVendorSet { get; set; }
        public Boolean IsManufacturerCodeSet { get; set; }
        public Boolean IsManufacturerSet { get; set; }
        public Boolean IsSizeSet { get; set; }
        public Boolean IsUnitOfMeasureSet { get; set; }
        public Boolean IsDateIntroducedSet { get; set; }
        public Boolean IsDateDiscontinuedSet { get; set; }
        public Boolean IsDateEffectiveSet { get; set; }
        public Boolean IsHealthSet { get; set; }
        public Boolean IsCorporateCodeSet { get; set; }
        public Boolean IsBarcodeSet { get; set; }
        public Boolean IsSellPriceSet { get; set; }
        public Boolean IsSellPackCountSet { get; set; }
        public Boolean IsSellPackDescriptionSet { get; set; }
        public Boolean IsRecommendedRetailPriceSet { get; set; }
        public Boolean IsManufacturerRecommendedRetailPriceSet { get; set; }
        public Boolean IsCostPriceSet { get; set; }
        public Boolean IsCaseCostSet { get; set; }
        public Boolean IsTaxRateSet { get; set; }
        public Boolean IsConsumerInformationSet { get; set; }
        public Boolean IsTextureSet { get; set; }
        public Boolean IsStyleNumberSet { get; set; }
        public Boolean IsPatternSet { get; set; }
        public Boolean IsModelSet { get; set; }
        public Boolean IsGarmentTypeSet { get; set; }
        public Boolean IsIsPrivateLabelSet { get; set; }
        public Boolean IsIsNewProductSet { get; set; }
        public Boolean IsFinancialGroupCodeSet { get; set; }
        public Boolean IsFinancialGroupNameSet { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public ProductIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public ProductIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsEntityIdSet.GetHashCode() +
                IsProductGroupIdSet.GetHashCode() +
                IsReplacementProductIdSet.GetHashCode() +
                IsGtinSet.GetHashCode() +
                IsNameSet.GetHashCode() +
                IsHeightSet.GetHashCode() +
                IsWidthSet.GetHashCode() +
                IsDepthSet.GetHashCode() +
                IsDisplayHeightSet.GetHashCode() +
                IsDisplayWidthSet.GetHashCode() +
                IsDisplayDepthSet.GetHashCode() +
                IsAlternateHeightSet.GetHashCode() +
                IsAlternateWidthSet.GetHashCode() +
                IsAlternateDepthSet.GetHashCode() +
                IsPointOfPurchaseHeightSet.GetHashCode() +
                IsPointOfPurchaseWidthSet.GetHashCode() +
                IsPointOfPurchaseDepthSet.GetHashCode() +
                IsNumberOfPegHolesSet.GetHashCode() +
                IsPegXSet.GetHashCode() +
                IsPegX2Set.GetHashCode() +
                IsPegX3Set.GetHashCode() +
                IsPegYSet.GetHashCode() +
                IsPegY2Set.GetHashCode() +
                IsPegY3Set.GetHashCode() +
                //IsPegProngOffsetSet.GetHashCode() +
                IsPegProngOffsetXSet.GetHashCode() +
                IsPegProngOffsetYSet.GetHashCode() +
                IsPegDepthSet.GetHashCode() +
                IsSqueezeHeightSet.GetHashCode() +
                IsSqueezeWidthSet.GetHashCode() +
                IsSqueezeDepthSet.GetHashCode() +
                IsNestingHeightSet.GetHashCode() +
                IsNestingWidthSet.GetHashCode() +
                IsNestingDepthSet.GetHashCode() +
                IsCasePackUnitsSet.GetHashCode() +
                IsCaseHighSet.GetHashCode() +
                IsCaseWideSet.GetHashCode() +
                IsCaseDeepSet.GetHashCode() +
                IsCaseHeightSet.GetHashCode() +
                IsCaseWidthSet.GetHashCode() +
                IsCaseDepthSet.GetHashCode() +
                IsMaxStackSet.GetHashCode() +
                IsMaxTopCapSet.GetHashCode() +
                IsMaxRightCapSet.GetHashCode() +
                IsMinDeepSet.GetHashCode() +
                IsMaxDeepSet.GetHashCode() +
                IsTrayPackUnitsSet.GetHashCode() +
                IsTrayHighSet.GetHashCode() +
                IsTrayWideSet.GetHashCode() +
                IsTrayDeepSet.GetHashCode() +
                IsTrayHeightSet.GetHashCode() +
                IsTrayWidthSet.GetHashCode() +
                IsTrayDepthSet.GetHashCode() +
                IsTrayThickHeightSet.GetHashCode() +
                IsTrayThickWidthSet.GetHashCode() +
                IsTrayThickDepthSet.GetHashCode() +
                IsFrontOverhangSet.GetHashCode() +
                IsFingerSpaceAboveSet.GetHashCode() +
                IsFingerSpaceToTheSideSet.GetHashCode() +
                IsStatusTypeSet.GetHashCode() +
                IsOrientationTypeSet.GetHashCode() +
                IsMerchandisingStyleSet.GetHashCode() +
                IsIsFrontOnlySet.GetHashCode() +
                IsIsPlaceHolderProductSet.GetHashCode() +
                IsIsActiveSet.GetHashCode() +
                IsCanBreakTrayUpSet.GetHashCode() +
                IsCanBreakTrayDownSet.GetHashCode() +
                IsCanBreakTrayBackSet.GetHashCode() +
                IsCanBreakTrayTopSet.GetHashCode() +
                IsForceMiddleCapSet.GetHashCode() +
                IsForceBottomCapSet.GetHashCode() +
                IsShapeSet.GetHashCode() +
                IsShapeTypeSet.GetHashCode() +
                IsFillColourSet.GetHashCode() +
                IsFillPatternTypeSet.GetHashCode() +
                IsPointOfPurchaseDescriptionSet.GetHashCode() +
                IsShortDescriptionSet.GetHashCode() +
                IsSubcategorySet.GetHashCode() +
                IsCustomerStatusSet.GetHashCode() +
                IsColourSet.GetHashCode() +
                IsFlavourSet.GetHashCode() +
                IsPackagingShapeSet.GetHashCode() +
                IsPackagingTypeSet.GetHashCode() +
                IsCountryOfOriginSet.GetHashCode() +
                IsCountryOfProcessingSet.GetHashCode() +
                IsShelfLifeSet.GetHashCode() +
                IsDeliveryFrequencyDaysSet.GetHashCode() +
                IsDeliveryMethodSet.GetHashCode() +
                IsBrandSet.GetHashCode() +
                IsVendorCodeSet.GetHashCode() +
                IsVendorSet.GetHashCode() +
                IsManufacturerCodeSet.GetHashCode() +
                IsManufacturerSet.GetHashCode() +
                IsSizeSet.GetHashCode() +
                IsUnitOfMeasureSet.GetHashCode() +
                IsDateIntroducedSet.GetHashCode() +
                IsDateDiscontinuedSet.GetHashCode() +
                IsDateEffectiveSet.GetHashCode() +
                IsHealthSet.GetHashCode() +
                IsCorporateCodeSet.GetHashCode() +
                IsBarcodeSet.GetHashCode() +
                IsSellPriceSet.GetHashCode() +
                IsSellPackCountSet.GetHashCode() +
                IsSellPackDescriptionSet.GetHashCode() +
                IsRecommendedRetailPriceSet.GetHashCode() +
                IsManufacturerRecommendedRetailPriceSet.GetHashCode() +
                IsCostPriceSet.GetHashCode() +
                IsCaseCostSet.GetHashCode() +
                IsTaxRateSet.GetHashCode() +
                IsConsumerInformationSet.GetHashCode() +
                IsTextureSet.GetHashCode() +
                IsStyleNumberSet.GetHashCode() +
                IsPatternSet.GetHashCode() +
                IsModelSet.GetHashCode() +
                IsGarmentTypeSet.GetHashCode() +
                IsIsPrivateLabelSet.GetHashCode() +
                IsIsNewProductSet.GetHashCode() +
                IsFinancialGroupCodeSet.GetHashCode() +
                IsFinancialGroupNameSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ProductIsSetDto other = obj as ProductIsSetDto;
            if (other != null)
            {
                if (other.IsEntityIdSet != this.IsEntityIdSet) { return false; }
                if (other.IsProductGroupIdSet != this.IsProductGroupIdSet) { return false; }
                if (other.IsReplacementProductIdSet != this.IsReplacementProductIdSet) { return false; }
                if (other.IsGtinSet != this.IsGtinSet) { return false; }
                if (other.IsNameSet != this.IsNameSet) { return false; }
                if (other.IsHeightSet != this.IsHeightSet) { return false; }
                if (other.IsWidthSet != this.IsWidthSet) { return false; }
                if (other.IsDepthSet != this.IsDepthSet) { return false; }
                if (other.IsDisplayHeightSet != this.IsDisplayHeightSet) { return false; }
                if (other.IsDisplayWidthSet != this.IsDisplayWidthSet) { return false; }
                if (other.IsDisplayDepthSet != this.IsDisplayDepthSet) { return false; }
                if (other.IsAlternateHeightSet != this.IsAlternateHeightSet) { return false; }
                if (other.IsAlternateWidthSet != this.IsAlternateWidthSet) { return false; }
                if (other.IsAlternateDepthSet != this.IsAlternateDepthSet) { return false; }
                if (other.IsPointOfPurchaseHeightSet != this.IsPointOfPurchaseHeightSet) { return false; }
                if (other.IsPointOfPurchaseWidthSet != this.IsPointOfPurchaseWidthSet) { return false; }
                if (other.IsPointOfPurchaseDepthSet != this.IsPointOfPurchaseDepthSet) { return false; }
                if (other.IsNumberOfPegHolesSet != this.IsNumberOfPegHolesSet) { return false; }
                if (other.IsPegXSet != this.IsPegXSet) { return false; }
                if (other.IsPegX2Set != this.IsPegX2Set) { return false; }
                if (other.IsPegX3Set != this.IsPegX3Set) { return false; }
                if (other.IsPegYSet != this.IsPegYSet) { return false; }
                if (other.IsPegY2Set != this.IsPegY2Set) { return false; }
                if (other.IsPegY3Set != this.IsPegY3Set) { return false; }
                //if (other.IsPegProngOffsetSet != this.IsPegProngOffsetSet) { return false; }
                if (other.IsPegProngOffsetXSet != this.IsPegProngOffsetXSet) { return false; }
                if (other.IsPegProngOffsetYSet != this.IsPegProngOffsetYSet) { return false; }
                if (other.IsPegDepthSet != this.IsPegDepthSet) { return false; }
                if (other.IsSqueezeHeightSet != this.IsSqueezeHeightSet) { return false; }
                if (other.IsSqueezeWidthSet != this.IsSqueezeWidthSet) { return false; }
                if (other.IsSqueezeDepthSet != this.IsSqueezeDepthSet) { return false; }
                if (other.IsNestingHeightSet != this.IsNestingHeightSet) { return false; }
                if (other.IsNestingWidthSet != this.IsNestingWidthSet) { return false; }
                if (other.IsNestingDepthSet != this.IsNestingDepthSet) { return false; }
                if (other.IsCasePackUnitsSet != this.IsCasePackUnitsSet) { return false; }
                if (other.IsCaseHighSet != this.IsCaseHighSet) { return false; }
                if (other.IsCaseWideSet != this.IsCaseWideSet) { return false; }
                if (other.IsCaseDeepSet != this.IsCaseDeepSet) { return false; }
                if (other.IsCaseHeightSet != this.IsCaseHeightSet) { return false; }
                if (other.IsCaseWidthSet != this.IsCaseWidthSet) { return false; }
                if (other.IsCaseDepthSet != this.IsCaseDepthSet) { return false; }
                if (other.IsMaxStackSet != this.IsMaxStackSet) { return false; }
                if (other.IsMaxTopCapSet != this.IsMaxTopCapSet) { return false; }
                if (other.IsMaxRightCapSet != this.IsMaxRightCapSet) { return false; }
                if (other.IsMinDeepSet != this.IsMinDeepSet) { return false; }
                if (other.IsMaxDeepSet != this.IsMaxDeepSet) { return false; }
                if (other.IsTrayPackUnitsSet != this.IsTrayPackUnitsSet) { return false; }
                if (other.IsTrayHighSet != this.IsTrayHighSet) { return false; }
                if (other.IsTrayWideSet != this.IsTrayWideSet) { return false; }
                if (other.IsTrayDeepSet != this.IsTrayDeepSet) { return false; }
                if (other.IsTrayHeightSet != this.IsTrayHeightSet) { return false; }
                if (other.IsTrayWidthSet != this.IsTrayWidthSet) { return false; }
                if (other.IsTrayDepthSet != this.IsTrayDepthSet) { return false; }
                if (other.IsTrayThickHeightSet != this.IsTrayThickHeightSet) { return false; }
                if (other.IsTrayThickWidthSet != this.IsTrayThickWidthSet) { return false; }
                if (other.IsTrayThickDepthSet != this.IsTrayThickDepthSet) { return false; }
                if (other.IsFrontOverhangSet != this.IsFrontOverhangSet) { return false; }
                if (other.IsFingerSpaceAboveSet != this.IsFingerSpaceAboveSet) { return false; }
                if (other.IsFingerSpaceToTheSideSet != this.IsFingerSpaceToTheSideSet) { return false; }
                if (other.IsStatusTypeSet != this.IsStatusTypeSet) { return false; }
                if (other.IsOrientationTypeSet != this.IsOrientationTypeSet) { return false; }
                if (other.IsMerchandisingStyleSet != this.IsMerchandisingStyleSet) { return false; }
                if (other.IsIsFrontOnlySet != this.IsIsFrontOnlySet) { return false; }
                if (other.IsIsPlaceHolderProductSet != this.IsIsPlaceHolderProductSet) { return false; }
                if (other.IsIsActiveSet != this.IsIsActiveSet) { return false; }
                if (other.IsCanBreakTrayUpSet != this.IsCanBreakTrayUpSet) { return false; }
                if (other.IsCanBreakTrayDownSet != this.IsCanBreakTrayDownSet) { return false; }
                if (other.IsCanBreakTrayBackSet != this.IsCanBreakTrayBackSet) { return false; }
                if (other.IsCanBreakTrayTopSet != this.IsCanBreakTrayTopSet) { return false; }
                if (other.IsForceMiddleCapSet != this.IsForceMiddleCapSet) { return false; }
                if (other.IsForceBottomCapSet != this.IsForceBottomCapSet) { return false; }
                if (other.IsShapeSet != this.IsShapeSet) { return false; }
                if (other.IsShapeTypeSet != this.IsShapeTypeSet) { return false; }
                if (other.IsFillColourSet != this.IsFillColourSet) { return false; }
                if (other.IsFillPatternTypeSet != this.IsFillPatternTypeSet) { return false; }
                if (other.IsPointOfPurchaseDescriptionSet != this.IsPointOfPurchaseDescriptionSet) { return false; }
                if (other.IsShortDescriptionSet != this.IsShortDescriptionSet) { return false; }
                if (other.IsSubcategorySet != this.IsSubcategorySet) { return false; }
                if (other.IsCustomerStatusSet != this.IsCustomerStatusSet) { return false; }
                if (other.IsColourSet != this.IsColourSet) { return false; }
                if (other.IsFlavourSet != this.IsFlavourSet) { return false; }
                if (other.IsPackagingShapeSet != this.IsPackagingShapeSet) { return false; }
                if (other.IsPackagingTypeSet != this.IsPackagingTypeSet) { return false; }
                if (other.IsCountryOfOriginSet != this.IsCountryOfOriginSet) { return false; }
                if (other.IsCountryOfProcessingSet != this.IsCountryOfProcessingSet) { return false; }
                if (other.IsShelfLifeSet != this.IsShelfLifeSet) { return false; }
                if (other.IsDeliveryFrequencyDaysSet != this.IsDeliveryFrequencyDaysSet) { return false; }
                if (other.IsDeliveryMethodSet != this.IsDeliveryMethodSet) { return false; }
                if (other.IsBrandSet != this.IsBrandSet) { return false; }
                if (other.IsVendorCodeSet != this.IsVendorCodeSet) { return false; }
                if (other.IsVendorSet != this.IsVendorSet) { return false; }
                if (other.IsManufacturerCodeSet != this.IsManufacturerCodeSet) { return false; }
                if (other.IsManufacturerSet != this.IsManufacturerSet) { return false; }
                if (other.IsSizeSet != this.IsSizeSet) { return false; }
                if (other.IsUnitOfMeasureSet != this.IsUnitOfMeasureSet) { return false; }
                if (other.IsDateIntroducedSet != this.IsDateIntroducedSet) { return false; }
                if (other.IsDateDiscontinuedSet != this.IsDateDiscontinuedSet) { return false; }
                if (other.IsDateEffectiveSet != this.IsDateEffectiveSet) { return false; }
                if (other.IsHealthSet != this.IsHealthSet) { return false; }
                if (other.IsCorporateCodeSet != this.IsCorporateCodeSet) { return false; }
                if (other.IsBarcodeSet != this.IsBarcodeSet) { return false; }
                if (other.IsSellPriceSet != this.IsSellPriceSet) { return false; }
                if (other.IsSellPackCountSet != this.IsSellPackCountSet) { return false; }
                if (other.IsSellPackDescriptionSet != this.IsSellPackDescriptionSet) { return false; }
                if (other.IsRecommendedRetailPriceSet != this.IsRecommendedRetailPriceSet) { return false; }
                if (other.IsManufacturerRecommendedRetailPriceSet != this.IsManufacturerRecommendedRetailPriceSet) { return false; }
                if (other.IsCostPriceSet != this.IsCostPriceSet) { return false; }
                if (other.IsCaseCostSet != this.IsCaseCostSet) { return false; }
                if (other.IsTaxRateSet != this.IsTaxRateSet) { return false; }
                if (other.IsConsumerInformationSet != this.IsConsumerInformationSet) { return false; }
                if (other.IsTextureSet != this.IsTextureSet) { return false; }
                if (other.IsStyleNumberSet != this.IsStyleNumberSet) { return false; }
                if (other.IsPatternSet != this.IsPatternSet) { return false; }
                if (other.IsModelSet != this.IsModelSet) { return false; }
                if (other.IsGarmentTypeSet != this.IsGarmentTypeSet) { return false; }
                if (other.IsIsPrivateLabelSet != this.IsIsPrivateLabelSet) { return false; }
                if (other.IsIsNewProductSet != this.IsIsNewProductSet) { return false; }
                if (other.IsFinancialGroupCodeSet != this.IsFinancialGroupCodeSet) { return false; }
                if (other.IsFinancialGroupNameSet != this.IsFinancialGroupNameSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsEntityIdSet = isSet;
            IsProductGroupIdSet = isSet;
            IsReplacementProductIdSet = isSet;
            IsGtinSet = isSet;
            IsNameSet = isSet;
            IsHeightSet = isSet;
            IsWidthSet = isSet;
            IsDepthSet = isSet;
            IsDisplayHeightSet = isSet;
            IsDisplayWidthSet = isSet;
            IsDisplayDepthSet = isSet;
            IsAlternateHeightSet = isSet;
            IsAlternateWidthSet = isSet;
            IsAlternateDepthSet = isSet;
            IsPointOfPurchaseHeightSet = isSet;
            IsPointOfPurchaseWidthSet = isSet;
            IsPointOfPurchaseDepthSet = isSet;
            IsNumberOfPegHolesSet = isSet;
            IsPegXSet = isSet;
            IsPegX2Set = isSet;
            IsPegX3Set = isSet;
            IsPegYSet = isSet;
            IsPegY2Set = isSet;
            IsPegY3Set = isSet;
            //IsPegProngOffsetSet = isSet;
            IsPegProngOffsetXSet = isSet;
            IsPegProngOffsetYSet = isSet;
            IsPegDepthSet = isSet;
            IsSqueezeHeightSet = isSet;
            IsSqueezeWidthSet = isSet;
            IsSqueezeDepthSet = isSet;
            IsNestingHeightSet = isSet;
            IsNestingWidthSet = isSet;
            IsNestingDepthSet = isSet;
            IsCasePackUnitsSet = isSet;
            IsCaseHighSet = isSet;
            IsCaseWideSet = isSet;
            IsCaseDeepSet = isSet;
            IsCaseHeightSet = isSet;
            IsCaseWidthSet = isSet;
            IsCaseDepthSet = isSet;
            IsMaxStackSet = isSet;
            IsMaxTopCapSet = isSet;
            IsMaxRightCapSet = isSet;
            IsMinDeepSet = isSet;
            IsMaxDeepSet = isSet;
            IsTrayPackUnitsSet = isSet;
            IsTrayHighSet = isSet;
            IsTrayWideSet = isSet;
            IsTrayDeepSet = isSet;
            IsTrayHeightSet = isSet;
            IsTrayWidthSet = isSet;
            IsTrayDepthSet = isSet;
            IsTrayThickHeightSet = isSet;
            IsTrayThickWidthSet = isSet;
            IsTrayThickDepthSet = isSet;
            IsFrontOverhangSet = isSet;
            IsFingerSpaceAboveSet = isSet;
            IsFingerSpaceToTheSideSet = isSet;
            IsStatusTypeSet = isSet;
            IsOrientationTypeSet = isSet;
            IsMerchandisingStyleSet = isSet;
            IsIsFrontOnlySet = isSet;
            IsIsPlaceHolderProductSet = isSet;
            IsIsActiveSet = isSet;
            IsCanBreakTrayUpSet = isSet;
            IsCanBreakTrayDownSet = isSet;
            IsCanBreakTrayBackSet = isSet;
            IsCanBreakTrayTopSet = isSet;
            IsForceMiddleCapSet = isSet;
            IsForceBottomCapSet = isSet;
            //IsPlacementStyleSet = isSet;
            IsShapeSet = isSet;
            IsShapeTypeSet = isSet;
            IsFillColourSet = isSet;
            IsFillPatternTypeSet = isSet;
            IsPointOfPurchaseDescriptionSet = isSet;
            IsShortDescriptionSet = isSet;
            IsSubcategorySet = isSet;
            IsCustomerStatusSet = isSet;
            IsColourSet = isSet;
            IsFlavourSet = isSet;
            IsPackagingShapeSet = isSet;
            IsPackagingTypeSet = isSet;
            IsCountryOfOriginSet = isSet;
            IsCountryOfProcessingSet = isSet;
            IsShelfLifeSet = isSet;
            IsDeliveryFrequencyDaysSet = isSet;
            IsDeliveryMethodSet = isSet;
            IsBrandSet = isSet;
            IsVendorCodeSet = isSet;
            IsVendorSet = isSet;
            IsManufacturerCodeSet = isSet;
            IsManufacturerSet = isSet;
            IsSizeSet = isSet;
            IsUnitOfMeasureSet = isSet;
            IsDateIntroducedSet = isSet;
            IsDateDiscontinuedSet = isSet;
            IsDateEffectiveSet = isSet;
            IsHealthSet = isSet;
            IsCorporateCodeSet = isSet;
            IsBarcodeSet = isSet;
            IsSellPriceSet = isSet;
            IsSellPackCountSet = isSet;
            IsSellPackDescriptionSet = isSet;
            IsRecommendedRetailPriceSet = isSet;
            IsManufacturerRecommendedRetailPriceSet = isSet;
            IsCostPriceSet = isSet;
            IsCaseCostSet = isSet;
            IsTaxRateSet = isSet;
            IsConsumerInformationSet = isSet;
            IsTextureSet = isSet;
            IsStyleNumberSet = isSet;
            IsPatternSet = isSet;
            IsModelSet = isSet;
            IsGarmentTypeSet = isSet;
            IsIsPrivateLabelSet = isSet;
            IsIsNewProductSet = isSet;
            IsFinancialGroupCodeSet = isSet;
            IsFinancialGroupNameSet = isSet;
        }
        #endregion
    }

}
