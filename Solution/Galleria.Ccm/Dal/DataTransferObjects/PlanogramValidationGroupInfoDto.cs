﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26944 : A.Silva ~ Created.
// V8-27004 : A.Silva ~ Some corrections to properties, split up the classes to different files.

#endregion

#endregion

using System;
using System.Collections.Generic;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    ///     Data transfer object for Planogram Validation Group Infos.
    /// </summary>
    [Serializable]
    public sealed class PlanogramValidationGroupInfoDto
    {
        #region Properties

        public Int32 Id { get; set; }
        public Int32 PlanogramValidationId { get; set; }
        public String Name { get; set; }
        public Byte ResultType { get; set; }

        #endregion

        #region Equality Overrides

        /// <summary>
        /// Serves as a hash function for a particular type. 
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode() +
                   PlanogramValidationId.GetHashCode() +
                   Name.GetHashCode() +
                   ResultType.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>. </param>
        public override bool Equals(Object obj)
        {
            var other = obj as PlanogramValidationGroupInfoDto;
            return other != null &&
                   other.Id == Id &&
                   other.PlanogramValidationId == PlanogramValidationId &&
                   other.Name == Name &&
                   other.ResultType == ResultType;
        }

        #endregion
    }
}