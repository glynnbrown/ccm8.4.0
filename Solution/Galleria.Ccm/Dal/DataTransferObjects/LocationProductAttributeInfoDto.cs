﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
#endregion
#region Version History: (CCM 8.03)
//V8-29267 : L.Ineson
//  Removed date deleted.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class LocationProductAttributeInfoDto
    {
        #region Properties
        public Int32 ProductId { get; set; }
        public Int16 LocationId { get; set; }
        public String LocationName { get; set; }
        public String ProductName { get; set; }
        public Int32 EntityId { get; set; }
        public String GTIN { get; set; }
        public Int16? CasePackUnits { get; set; }
        public Byte? DaysOfSupply { get; set; }
        public Single? DeliveryFrequencyDays { get; set; }
        public Int16? ShelfLife { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return base.GetHashCode();//this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override bool Equals(object obj)
        {
            LocationProductAttributeInfoDto other = obj as LocationProductAttributeInfoDto;
            if (other != null)
            {
                if (other.ProductId != this.ProductId) { return false; }
                if (other.LocationId != this.LocationId) { return false; }
                if (other.LocationName != this.LocationName) { return false; }
                if (other.ProductName != this.ProductName) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.GTIN != this.GTIN) { return false; }
                if (other.CasePackUnits != this.CasePackUnits) { return false; }
                if (other.DaysOfSupply != this.DaysOfSupply) { return false; }
                if (other.DeliveryFrequencyDays != this.DeliveryFrequencyDays) { return false; }
                if (other.ShelfLife != this.ShelfLife) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
