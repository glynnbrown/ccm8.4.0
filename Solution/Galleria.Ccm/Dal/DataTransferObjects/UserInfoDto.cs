﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26222 : L.Luong
//		Created (Auto-generated)
// V8-26322 : A.Silva
//      Added PreloadedRecordCount(1) as the database creation script will add at least one user.

#endregion

#endregion

using System;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// UserInfo Data Transfer Object
    /// </summary>
    [Serializable, PreloadedRecordCount(1)]
    public sealed class UserInfoDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public UserDtoKey DtoKey
        {
            get
            {
                return new UserDtoKey()
                {
                    UserName = this.UserName
                };
            }
        }
        public String UserName { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            UserInfoDto other = obj as UserInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.UserName != this.UserName) { return false; }
                if (other.FirstName != this.FirstName) { return false; }
                if (other.LastName != this.LastName) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
