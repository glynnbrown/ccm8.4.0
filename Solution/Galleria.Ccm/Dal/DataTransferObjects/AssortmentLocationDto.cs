﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)

// CCM-25454 : J.Pickup
//		Created (Auto-generated)
// V8-26322 : A.Silva
//      Amended LocationId and AssormentId FK do that the delete behavior is Leave (parents are marked as deleted, not deleted).

#endregion

#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// AssortmentLocation Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class AssortmentLocationDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public AssortmentLocationDtoKey DtoKey
        {
            get
            {
                return new AssortmentLocationDtoKey()
                {
                    LocationId = this.LocationId
                };
            }
        }
        [ForeignKey(typeof(LocationDto), typeof(ILocationDal), DeleteBehavior.Leave)]
        public Int16 LocationId { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal), ForeignKeyPropertyName = "LocationId", ParentPropertyName = "Code")]
        public String Code { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal), ForeignKeyPropertyName = "LocationId", ParentPropertyName = "Name")]
        public String Name { get; set; }
        [ForeignKey(typeof(AssortmentDto), typeof(IAssortmentDal), DeleteBehavior.Leave)]
        public Int32 AssortmentId { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentLocationDto other = obj as AssortmentLocationDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.LocationId != this.LocationId) { return false; }
                if (other.Code != this.Code) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.AssortmentId != this.AssortmentId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentLocationDtoKey
    {
        #region Properties
        public Int32 LocationId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                LocationId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentLocationDtoKey other = obj as AssortmentLocationDtoKey;
            if (other != null)
            {

                if (other.LocationId != this.LocationId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentLocationIsSetDto
    {
        #region Properties
        public Boolean IsLocationIdSet { get; set; }
        public Boolean IsCodeSet { get; set; }
        public Boolean IsNameSet { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public AssortmentLocationIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public AssortmentLocationIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsLocationIdSet.GetHashCode() +
                IsCodeSet.GetHashCode() +
                IsNameSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentLocationIsSetDto other = obj as AssortmentLocationIsSetDto;
            if (other != null)
            {
                if (other.IsLocationIdSet != this.IsLocationIdSet) { return false; }
                if (other.IsCodeSet != this.IsCodeSet) { return false; }
                if (other.IsNameSet != this.IsNameSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsLocationIdSet = isSet;
            IsCodeSet = isSet;
            IsNameSet = isSet;
        }
        #endregion
    }
}
