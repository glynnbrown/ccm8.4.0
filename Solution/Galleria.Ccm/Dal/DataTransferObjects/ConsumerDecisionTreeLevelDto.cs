﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
#endregion
#region Version History: (CCM 8.0.3)
// V8-29215 : D.Pleasance
//  Removed ConsumerDecisionTree DateDeleted columns
#endregion
#endregion

using System;

using Galleria.Framework.DataStructures;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// ConsumerDecisionTreeLevel Data Transfer Object
    /// </summary>
    [Serializable]
    public class ConsumerDecisionTreeLevelDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public ConsumerDecisionTreeLevelDtoKey DtoKey
        {
            get
            {
                return new ConsumerDecisionTreeLevelDtoKey()
                {
                    ConsumerDecisionTreeId = this.ConsumerDecisionTreeId,
                    Name = this.Name
                };
            }
        }
        [ForeignKey(typeof(ConsumerDecisionTreeDto), typeof(IConsumerDecisionTreeDal))]
        public Int32 ConsumerDecisionTreeId { get; set; }
        public String Name { get; set; }
        [ForeignKey(typeof(ConsumerDecisionTreeLevelDto), typeof(IConsumerDecisionTreeLevelDal))]
        public Int32? ParentLevelId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ConsumerDecisionTreeLevelDto other = obj as ConsumerDecisionTreeLevelDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.ConsumerDecisionTreeId != this.ConsumerDecisionTreeId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.ParentLevelId != this.ParentLevelId) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    public class ConsumerDecisionTreeLevelDtoKey
    {
        #region Properties
        public Int32 ConsumerDecisionTreeId { get; set; }
        public String Name { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                ConsumerDecisionTreeId.GetHashCode() +
                Name.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ConsumerDecisionTreeLevelDtoKey other = obj as ConsumerDecisionTreeLevelDtoKey;
            if (other != null)
            {
                if (other.ConsumerDecisionTreeId != this.ConsumerDecisionTreeId) { return false; }
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

}
