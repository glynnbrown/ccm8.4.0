﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// V8-29010 : D.Pleasance
//  Created
#endregion
#region Version History: (CCM 8.3.0)
//V8-31831 : A.Probyn
//  Added PlanogramAttribute
//  Removed SeparatorType
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Planogram Name Template Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramNameTemplateDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public PlanogramNameTemplateDtoKey DtoKey
        {
            get 
            {
                return new PlanogramNameTemplateDtoKey() 
                { 
                    Name = this.Name,
                    EntityId = EntityId
                }; 
            }
        }
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        public String Name { get; set; }
        public Byte PlanogramAttribute { get ; set; }
        public String BuilderFormula { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            PlanogramNameTemplateDto other = obj as PlanogramNameTemplateDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.PlanogramAttribute != this.PlanogramAttribute) { return false; }
                if (other.BuilderFormula != this.BuilderFormula) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class PlanogramNameTemplateDtoKey
    {
        #region Properties
        public String Name { get; set; }
        public Int32 EntityId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return EntityId.GetHashCode() + Name.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramNameTemplateDtoKey other = obj as PlanogramNameTemplateDtoKey;
            if (other != null)
            {
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}