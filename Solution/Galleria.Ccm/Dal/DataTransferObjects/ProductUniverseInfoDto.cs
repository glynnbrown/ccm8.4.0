﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630 : A.Kuszyk
//  Created (copied from SA).
#endregion
#region Version History: (CCM CCM803)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class ProductUniverseInfoDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public Guid UniqueContentReference { get; set; }
        public String Name { get; set; }
        public Int32? ProductGroupId { get; set; }
        public String ProductGroupCode { get; set; }
        public String ProductGroupName { get; set; }
        public Boolean IsMaster { get; set; }
        public Int32 ProductCount { get; set; }
        public Int32 EntityId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public Guid? ParentUniqueContentReference { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the objects hash code
        /// </summary>
        /// <returns>The objects hash code</returns>
        public override Int32 GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Compares two instance of this type
        /// </summary>
        /// <param name="obj">The object to compare to</param>
        /// <returns>True if equal, else false</returns>
        public override Boolean Equals(object obj)
        {
            ProductUniverseInfoDto other = obj as ProductUniverseInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.UniqueContentReference != this.UniqueContentReference) { return false; }
                if (other.ProductGroupId != this.ProductGroupId) { return false; }
                if (other.ProductGroupCode != this.ProductGroupCode) { return false; }
                if (other.ProductGroupName != this.ProductGroupName) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.IsMaster != this.IsMaster) { return false; }
                if (other.ProductCount != this.ProductCount) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.ParentUniqueContentReference != this.ParentUniqueContentReference) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
