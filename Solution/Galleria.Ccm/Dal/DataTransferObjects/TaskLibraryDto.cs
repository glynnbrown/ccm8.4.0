﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-28258 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Task Library Data Transfer Object
    /// </summary>
    [Serializable]
    public class TaskLibraryDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public String FullName { get; set; }
        public Int32 Major { get; set; }
        public Int32 Minor { get; set; }
        public Int32 Revision { get; set; }
        public Int32 Build { get; set; }
        public Byte[] Data { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            TaskLibraryDto other = obj as TaskLibraryDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.RowVersion != this.RowVersion) return false;
                if (other.FullName != this.FullName) return false;
                if (other.Major != this.Major) return false;
                if (other.Minor != this.Minor) return false;
                if (other.Revision != this.Revision) return false;
                if (other.Build != this.Build) return false;
                if ((other.Data == null) && (this.Data != null)) return false;
                if ((other.Data != null) && (this.Data == null)) return false;
                if ((other.Data != null) && (this.Data != null)) if (!other.Data.SequenceEqual(this.Data)) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
