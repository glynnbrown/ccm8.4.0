﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-25630 : A.Kuszyk
//  Created (copied from SA).
// V8-26041 : A.Kuszyk ~ Amended delete behaviour on Product foreign key attribute.
// V8-26322 : A.Silva
//      Amended ProductId Property to have DeleteBehavior.Leave (parent is marked as deleted, not really deleted).

#endregion

#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// ProductUniverseProduct Data Transfer Object
    /// </summary>
    [Serializable]
    public class ProductUniverseProductDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public ProductUniverseProductDtoKey DtoKey
        {
            get
            {
                return new ProductUniverseProductDtoKey()
                {
                    ProductUniverseId = this.ProductUniverseId,
                    ProductId = this.ProductId
                };
            }
        }
        [ForeignKey(typeof(ProductUniverseDto), typeof(IProductUniverseDal))]
        public Int32 ProductUniverseId { get; set; }
        [ForeignKey(typeof(ProductDto), typeof(IProductDal),DeleteBehavior.Leave)]
        public Int32 ProductId { get; set; }
        [InheritedProperty(typeof(ProductDto), typeof(IProductDal))]
        public String Gtin { get; set; }
        [InheritedProperty(typeof(ProductDto), typeof(IProductDal))]
        public String Name { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ProductUniverseProductDto other = obj as ProductUniverseProductDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.ProductUniverseId != this.ProductUniverseId) { return false; }
                if (other.ProductId != this.ProductId) { return false; }
                if (other.Gtin != this.Gtin) { return false; }
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class ProductUniverseProductDtoKey
    {
        #region Properties
        public Int32 ProductUniverseId { get; set; }
        public Int32 ProductId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                ProductUniverseId.GetHashCode() + ProductId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ProductUniverseProductDtoKey other = obj as ProductUniverseProductDtoKey;
            if (other != null)
            {

                if (other.ProductUniverseId != this.ProductUniverseId) { return false; }
                if (other.ProductId != this.ProductId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class ProductUniverseProductIsSetDto
    {
        #region Properties
        public Boolean IsProductUniverseIdSet { get; set; }
        public Boolean IsProductIdSet { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public ProductUniverseProductIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public ProductUniverseProductIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsProductUniverseIdSet.GetHashCode() +
                IsProductIdSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ProductUniverseProductIsSetDto other = obj as ProductUniverseProductIsSetDto;
            if (other != null)
            {
                if (other.IsProductUniverseIdSet != this.IsProductUniverseIdSet) { return false; }
                if (other.IsProductIdSet != this.IsProductIdSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsProductUniverseIdSet = isSet;
            IsProductIdSet = isSet;
        }
        #endregion
    }
}
