﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25546 : N.Foster
//  Created
#endregion
#region Version History: CCM830
// V8-32357 : M.Brumby
//  [PCR01561] added DisplayName and DisplayDescription
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public sealed class WorkflowTaskDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public WorkflowTaskDtoKey DtoKey
        {
            get
            {
                return new WorkflowTaskDtoKey() { WorkflowId = this.WorkflowId, SequenceId = SequenceId };
            }
        }
        [ForeignKey(typeof(WorkflowDto), typeof(IWorkflowDal))]
        public Int32 WorkflowId { get; set; }
        public Byte SequenceId { get; set; }
        public String TaskType { get; set; }
        public String DisplayName { get; set; }
        public String DisplayDescription { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the instance has code
        /// </summary>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Compares two instances to each other
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            WorkflowTaskDto other = obj as WorkflowTaskDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.WorkflowId != this.WorkflowId) return false;
                if (other.SequenceId != this.SequenceId) return false;
                if (other.TaskType != this.TaskType) return false;
                if (other.DisplayName != this.DisplayName) return false;
                if (other.DisplayDescription != this.DisplayDescription) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class WorkflowTaskDtoKey
    {
        #region Properties
        public Int32 WorkflowId { get; set; }
        public Byte SequenceId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return HashCode.Start.Hash(WorkflowId).Hash(SequenceId).GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the ISOme
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            WorkflowTaskDtoKey other = obj as WorkflowTaskDtoKey;
            if (other != null)
            {
                if (other.WorkflowId != this.WorkflowId) return false;
                if (other.SequenceId != this.SequenceId) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
