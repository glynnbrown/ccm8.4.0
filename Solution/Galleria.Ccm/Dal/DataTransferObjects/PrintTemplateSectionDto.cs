﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// PrintTemplateSection Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class PrintTemplateSectionDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public PrintTemplateSectionDtoKey DtoKey
        {
            get
            {
                return new PrintTemplateSectionDtoKey()
                {
                    PrintTemplateSectionGroupId = this.PrintTemplateSectionGroupId,
                    Number = this.Number
                };
            }
        }
        [ForeignKey(typeof(PrintTemplateSectionGroupDto), typeof(IPrintTemplateSectionGroupDal), DeleteBehavior.Cascade)]
        public Int32 PrintTemplateSectionGroupId { get; set; }
        public Byte Number { get; set; }
        public Byte Orientation { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PrintTemplateSectionDto other = obj as PrintTemplateSectionDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.PrintTemplateSectionGroupId != this.PrintTemplateSectionGroupId) { return false; }
                if (other.Number != this.Number) { return false; }
                if (other.Orientation != this.Orientation) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    /// <summary>
    /// PrintTemplateSection Dto key
    /// </summary>
    [Serializable]
    public sealed class PrintTemplateSectionDtoKey
    {

        #region Properties
        public Int32 PrintTemplateSectionGroupId { get; set; }
        public Byte Number { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return  PrintTemplateSectionGroupId.GetHashCode() ^
                Number.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PrintTemplateSectionDtoKey other = obj as PrintTemplateSectionDtoKey;
            if (other != null)
            {
                if (other.PrintTemplateSectionGroupId != this.PrintTemplateSectionGroupId) { return false; }
                if (other.Number != this.Number) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


}
