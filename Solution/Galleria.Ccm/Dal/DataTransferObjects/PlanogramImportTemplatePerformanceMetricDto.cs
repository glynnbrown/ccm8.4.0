﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.1)
//V8-28622 : D.Pleasance
//  Created
#endregion
#region Version History: (CCM 8.0.3)
// V8-29682 : A.Probyn
//  Added AggregationType
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// PlanogramImportTemplatePerformanceMetric Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramImportTemplatePerformanceMetricDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public PlanogramImportTemplatePerformanceMetricDtoKey DtoKey
        {
            get
            {
                return new PlanogramImportTemplatePerformanceMetricDtoKey()
                {
                    PlanogramImportTemplateId = this.PlanogramImportTemplateId,
                    Name = this.Name,
                    MetricId = this.MetricId
                }; 
            }
        }
        [ForeignKey(typeof(PlanogramImportTemplateDto), typeof(IPlanogramImportTemplateDal))]
        public Object PlanogramImportTemplateId { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Byte Direction { get; set; }
        public Byte SpecialType { get; set; }
        public Byte MetricType { get; set; }
        public Byte MetricId { get; set; }
        public String ExternalField { get; set; }
        public Byte AggregationType { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramImportTemplatePerformanceMetricDto other = obj as PlanogramImportTemplatePerformanceMetricDto;
            if (other != null)
            {
                //Id
                if (other.Id != this.Id) { return false; }

                //PlanogramImportTemplateId
                if ((other.PlanogramImportTemplateId != null) && (this.PlanogramImportTemplateId != null))
                {
                    if (!other.PlanogramImportTemplateId.Equals(this.PlanogramImportTemplateId)) { return false; }
                }
                if ((other.PlanogramImportTemplateId != null) && (this.PlanogramImportTemplateId == null)) { return false; }
                if ((other.PlanogramImportTemplateId == null) && (this.PlanogramImportTemplateId != null)) { return false; }

                if (other.Name != this.Name) { return false; }
                if (other.Description != this.Description) { return false; }
                if (other.Direction != this.Direction) { return false; }
                if (other.SpecialType != this.SpecialType) { return false; }
                if (other.MetricType != this.MetricType) { return false; }
                if (other.MetricId != this.MetricId) { return false; }
                if (other.ExternalField != this.ExternalField) { return false; }
                if (other.AggregationType != this.AggregationType) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion

    }

    [Serializable]
    public class PlanogramImportTemplatePerformanceMetricDtoKey
    {
        #region Properties

        public Object PlanogramImportTemplateId { get; set; }
        public String Name { get; set; }
        public Byte MetricId { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return PlanogramImportTemplateId.GetHashCode() +
                Name.GetHashCode()+
                MetricId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramImportTemplatePerformanceMetricDtoKey other = obj as PlanogramImportTemplatePerformanceMetricDtoKey;
            if (other != null)
            {
                //PlanogramImportTemplateId
                if ((other.PlanogramImportTemplateId != null) && (this.PlanogramImportTemplateId != null))
                {
                    if (!other.PlanogramImportTemplateId.Equals(this.PlanogramImportTemplateId)) { return false; }
                }
                if ((other.PlanogramImportTemplateId != null) && (this.PlanogramImportTemplateId == null)) { return false; }
                if ((other.PlanogramImportTemplateId == null) && (this.PlanogramImportTemplateId != null)) { return false; }

                if (other.Name != this.Name) { return false; }
                if (other.MetricId != this.MetricId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}