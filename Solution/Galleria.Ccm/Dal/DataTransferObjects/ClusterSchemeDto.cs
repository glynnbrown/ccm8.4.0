﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-25629 : A.Kuszyk
//		Created (Auto-generated)
// CCM-27078 : I.George
//  Added ProductGroupId to the dto
#endregion
#region Version History: (CCM v8.0.3)
// V8-29216 : D.Pleasance
//  Removed DateDeleted
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion 
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// ClusterScheme Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class ClusterSchemeDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public ClusterSchemeDtoKey DtoKey
        {
            get
            {
                return new ClusterSchemeDtoKey()
                {
                    EntityId = this.EntityId,
                    Name = this.Name
                };
            }
        }
        public Guid UniqueContentReference { get; set; }
        public String Name { get; set; }
        public Boolean IsDefault { get; set; }
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        [ForeignKey(typeof(ProductGroupDto), typeof(IProductGroupDal), DeleteBehavior.Leave)]
        public Int32? ProductGroupId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public Guid? ParentUniqueContentReference { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ClusterSchemeDto other = obj as ClusterSchemeDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.UniqueContentReference != this.UniqueContentReference) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.IsDefault != this.IsDefault) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.ProductGroupId != this.ProductGroupId) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.ParentUniqueContentReference != this.ParentUniqueContentReference) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class ClusterSchemeDtoKey
    {
        #region Properties
        public Int32 EntityId { get; set; }
        public String Name { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return EntityId.GetHashCode() + Name.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ClusterSchemeDtoKey other = obj as ClusterSchemeDtoKey;
            if (other != null)
            {
                if (other.Name != this.Name) return false;
                if (other.EntityId != this.EntityId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class ClusterSchemeIsSetDto
    {
        #region Properties
        public Boolean IsUniqueContentReferenceSet { get; set; }
        public Boolean IsNameSet { get; set; }
        public Boolean IsIsDefaultSet { get; set; }
        public Boolean IsEntityIdSet { get; set; }
        public Boolean IsProductGroupIdSet { get; set; }

        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public ClusterSchemeIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public ClusterSchemeIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsUniqueContentReferenceSet.GetHashCode() +
                IsNameSet.GetHashCode() +
                IsIsDefaultSet.GetHashCode() +
                IsProductGroupIdSet.GetHashCode() +
                IsEntityIdSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ClusterSchemeIsSetDto other = obj as ClusterSchemeIsSetDto;
            if (other != null)
            {
                if (other.IsUniqueContentReferenceSet != this.IsUniqueContentReferenceSet) { return false; }
                if (other.IsNameSet != this.IsNameSet) { return false; }
                if (other.IsIsDefaultSet != this.IsIsDefaultSet) { return false; }
                if (other.IsEntityIdSet != this.IsEntityIdSet) { return false; }
                if (other.IsProductGroupIdSet != this.IsProductGroupIdSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsUniqueContentReferenceSet = isSet;
            IsNameSet = isSet;
            IsIsDefaultSet = isSet;
            IsEntityIdSet = isSet;
            IsProductGroupIdSet = isSet;
        }
        #endregion
    }
}
