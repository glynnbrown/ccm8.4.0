﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class RepositorySyncTargetParameterDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Value { get; set; }
        public Int32 RepositorySyncTargetId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the instance hash code
        /// </summary>
        /// <returns>The instance hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Indicates if two instances are equal
        /// </summary>
        /// <param name="obj">The instance to compare to</param>
        /// <returns>True if equal, else false</returns>
        public override Boolean Equals(object obj)
        {
            RepositorySyncTargetParameterDto other = obj as RepositorySyncTargetParameterDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.Name != this.Name) return false;
                if (other.Value != this.Value) return false;
                if (other.RepositorySyncTargetId != this.RepositorySyncTargetId) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
