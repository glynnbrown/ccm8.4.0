﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Location Space Data Transfer Object
    /// </summary>
    [Serializable]
    public class LocationSpaceBayDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public LocationSpaceBayDtoKey DtoKey
        {
            get
            {
                return new LocationSpaceBayDtoKey()
                {
                    LocationSpaceProductGroupId = this.LocationSpaceProductGroupId,
                    Order = this.Order
                };
            }
        }
        public Byte Order { get; set; }
        public Single Height { get; set; }
        public Single Width { get; set; }
        public Single Depth { get; set; }
        public Single BaseHeight { get; set; }
        public Single BaseWidth { get; set; }
        public Single BaseDepth { get; set; }
        [ForeignKey(typeof(LocationSpaceProductGroupDto), typeof(ILocationSpaceProductGroupDal))]
        public Int32 LocationSpaceProductGroupId { get; set; }
        public Byte BayType { get; set; }
        public String BayTypePostFix { get; set; }
        public Single? BayTypeCalculation { get; set; }
        public Single? NotchPitch { get; set; }
        public String FixtureName { get; set; }
        public Byte FixtureType { get; set; }
        public Boolean IsPromotional { get; set; }
        public Boolean IsAisleStart { get; set; }
        public Boolean IsAisleEnd { get; set; }
        public Boolean IsAisleRight { get; set; }
        public Boolean IsAisleLeft { get; set; }
        public String ManufacturerName { get; set; }
        public String Barcode { get; set; }
        public Boolean HasPower { get; set; }
        public String AssetNumber { get; set; }
        public Single? Temperature { get; set; }
        public String Colour { get; set; }
        public String BayLocationRef { get; set; }
        public String BayFloorDrawingNo { get; set; }
        public Single NotchStartY { get; set; }
        public Byte FixtureShape { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override bool Equals(object obj)
        {
            LocationSpaceBayDto other = obj as LocationSpaceBayDto;
            if (other != null)
            {
                if (other.Id != this.Id)
                {
                    return false;
                }
                if (other.Order != this.Order)
                {
                    return false;
                }
                if (other.Height != this.Height)
                {
                    return false;
                }
                if (other.Width != this.Width)
                {
                    return false;
                }
                if (other.Depth != this.Depth)
                {
                    return false;
                }
                if (other.BaseHeight != this.BaseHeight)
                {
                    return false;
                }
                if (other.BaseWidth != this.BaseWidth)
                {
                    return false;
                }
                if (other.BaseDepth != this.BaseDepth)
                {
                    return false;
                }
                if (other.LocationSpaceProductGroupId != this.LocationSpaceProductGroupId)
                {
                    return false;
                }
                if (other.BayType != this.BayType)
                {
                    return false;
                }
                if (other.BayTypePostFix != this.BayTypePostFix)
                {
                    return false;
                }
                if (other.BayTypeCalculation != this.BayTypeCalculation)
                {
                    return false;
                }
                if (other.NotchPitch != this.NotchPitch)
                {
                    return false;
                }
                if (other.FixtureName != this.FixtureName)
                {
                    return false;
                }
                if (other.FixtureType != this.FixtureType)
                {
                    return false;
                }
                if (other.IsPromotional != this.IsPromotional)
                {
                    return false;
                }
                if (other.IsAisleStart != this.IsAisleStart)
                {
                    return false;
                }
                if (other.IsAisleEnd != this.IsAisleEnd)
                {
                    return false;
                }
                if (other.IsAisleRight != this.IsAisleRight)
                {
                    return false;
                }
                if (other.IsAisleLeft != this.IsAisleLeft)
                {
                    return false;
                }
                if (other.ManufacturerName != this.ManufacturerName)
                {
                    return false;
                }
                if (other.Barcode != this.Barcode)
                {
                    return false;
                }
                if (other.HasPower != this.HasPower)
                {
                    return false;
                }
                if (other.AssetNumber != this.AssetNumber)
                {
                    return false;
                }
                if (other.Temperature != this.Temperature)
                {
                    return false;
                }
                if (other.Colour != this.Colour)
                {
                    return false;
                }
                if (other.BayLocationRef != this.BayLocationRef)
                {
                    return false;
                }
                if (other.BayFloorDrawingNo != this.BayFloorDrawingNo)
                {
                    return false;
                }
                if (other.NotchStartY != this.NotchStartY)
                {
                    return false;
                }
                if (other.FixtureShape != this.FixtureShape)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class LocationSpaceBayDtoKey
    {
        #region Properties
        public Int32 LocationSpaceProductGroupId { get; set; }
        public Byte Order { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                LocationSpaceProductGroupId.GetHashCode() +
                Order.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            LocationSpaceBayDtoKey other = obj as LocationSpaceBayDtoKey;
            if (other != null)
            {
                if (other.LocationSpaceProductGroupId != this.LocationSpaceProductGroupId) { return false; }
                if (other.Order != this.Order) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class LocationSpaceBaySearchCriteriaDto
    {
        #region Properties
        public Int32 LocationSpaceBayId { get; set; }
        public String LocationCode { get; set; }
        public String ProductGroupCode { get; set; }
        public Byte Order { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                LocationCode.ToLowerInvariant().GetHashCode()
                + ProductGroupCode.ToLowerInvariant().GetHashCode()
                + Order.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            LocationSpaceBaySearchCriteriaDto other = obj as LocationSpaceBaySearchCriteriaDto;
            if (other != null)
            {
                //Dont compare bay id as this is only used in the Location Space Element import
                //for lookup purposes
                StringComparer s = StringComparer.OrdinalIgnoreCase;


                if (s.Compare(other.LocationCode, this.LocationCode) != 0) { return false; }
                if (s.Compare(other.ProductGroupCode, this.ProductGroupCode) != 0) { return false; }
                if (other.Order != this.Order) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
