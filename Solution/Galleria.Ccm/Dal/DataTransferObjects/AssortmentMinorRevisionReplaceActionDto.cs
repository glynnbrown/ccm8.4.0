﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
// CCM-27059 : J.Pickup
//      Reviewed FK attributes & added DtoKey, IsSetDto
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// AssortmentMinorRevisionReplaceAction Data Transfer Object
    /// </summary>
    [Serializable]
    public class AssortmentMinorRevisionReplaceActionDto
    {
        #region Properties
        public Int32 Id { get; set; }
        [ForeignKey(typeof(AssortmentMinorRevisionDto), typeof(IAssortmentMinorRevisionDal), DeleteBehavior.Cascade)]
        public Int32 AssortmentMinorRevisionId { get; set; }
        [ForeignKey(typeof(ProductDto), typeof(IProductDal), DeleteBehavior.Leave)]
        public Int32? ProductId { get; set; }
        public String ProductGtin { get; set; }
        public String ProductName { get; set; }
        public Int32? ReplacementProductId { get; set; }
        public String ReplacementProductGtin { get; set; }
        public String ReplacementProductName { get; set; }
        public Int32 Priority { get; set; }
        public String Comments { get; set; }
        public AssortmentMinorRevisionReplaceActionDtoKey DtoKey
        {
            get
            {
                return new AssortmentMinorRevisionReplaceActionDtoKey()
                {
                    AssortmentMinorRevisionId = this.AssortmentMinorRevisionId,
                    ProductGtin = this.ProductGtin,
                    ReplacementProductGtin = this.ReplacementProductGtin
                };
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentMinorRevisionReplaceActionDto other = obj as AssortmentMinorRevisionReplaceActionDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.AssortmentMinorRevisionId != this.AssortmentMinorRevisionId) { return false; }
                if (other.ProductId != this.ProductId) { return false; }
                if (other.ProductGtin != this.ProductGtin) { return false; }
                if (other.ProductName != this.ProductName) { return false; }
                if (other.ReplacementProductId != this.ReplacementProductId) { return false; }
                if (other.ReplacementProductGtin != this.ReplacementProductGtin) { return false; }
                if (other.ReplacementProductName != this.ReplacementProductName) { return false; }
                if (other.Priority != this.Priority) { return false; }
                if (other.Comments != this.Comments) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


    [Serializable]
    public class AssortmentMinorRevisionReplaceActionDtoKey
    {
        #region Properties

        public Int32 AssortmentMinorRevisionId { get; set; }
        public String ProductGtin { get; set; }
        public String ReplacementProductGtin { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                AssortmentMinorRevisionId.GetHashCode() +
                ProductGtin.GetHashCode() +
                ReplacementProductGtin.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentMinorRevisionReplaceActionDtoKey other = obj as AssortmentMinorRevisionReplaceActionDtoKey;
            if (other != null)
            {
                if (other.AssortmentMinorRevisionId != this.AssortmentMinorRevisionId) { return false; }
                if (other.ProductGtin != this.ProductGtin) { return false; }
                if (other.ReplacementProductGtin != this.ReplacementProductGtin) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


    [Serializable]
    public class AssortmentMinorRevisionReplaceActionIsSetDto
    {
        #region Properties

        public Boolean IsIdSet { get; set; }
        public Boolean IsAssortmentMinorRevisionIdSet { get; set; }
        public Boolean IsProductIdSet { get; set; }
        public Boolean IsProductGtinSet { get; set; }
        public Boolean IsProductNameSet { get; set; }
        public Boolean IsReplacementProductIdSet { get; set; }
        public Boolean IsReplacementProductGtinSet { get; set; }
        public Boolean IsReplacementProductNameSet { get; set; }
        public Boolean IsPrioritySet { get; set; }
        public Boolean IsCommentsSet { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Default
        /// </summary>
        public AssortmentMinorRevisionReplaceActionIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public AssortmentMinorRevisionReplaceActionIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsIdSet.GetHashCode() +
                IsAssortmentMinorRevisionIdSet.GetHashCode() +
                IsProductIdSet.GetHashCode() +
                IsProductGtinSet.GetHashCode() +
                IsProductNameSet.GetHashCode() +
                IsReplacementProductIdSet.GetHashCode() +
                IsReplacementProductGtinSet.GetHashCode() +
                IsReplacementProductNameSet.GetHashCode() +
                IsPrioritySet.GetHashCode() +
                IsCommentsSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentMinorRevisionReplaceActionIsSetDto other = obj as AssortmentMinorRevisionReplaceActionIsSetDto;
            if (other != null)
            {
                if (other.IsIdSet != this.IsIdSet) { return false; }
                if (other.IsAssortmentMinorRevisionIdSet != this.IsAssortmentMinorRevisionIdSet) { return false; }
                if (other.IsProductIdSet != this.IsProductIdSet) { return false; }
                if (other.IsProductGtinSet != this.IsProductGtinSet) { return false; }
                if (other.IsProductNameSet != this.IsProductNameSet) { return false; }
                if (other.IsReplacementProductIdSet != this.IsReplacementProductIdSet) { return false; }
                if (other.IsReplacementProductGtinSet != this.IsReplacementProductGtinSet) { return false; }
                if (other.IsReplacementProductNameSet != this.IsReplacementProductNameSet) { return false; }
                if (other.IsPrioritySet != this.IsPrioritySet) { return false; }
                if (other.IsCommentsSet != this.IsCommentsSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsIdSet = isSet;
            IsAssortmentMinorRevisionIdSet = isSet;
            IsProductIdSet = isSet;
            IsProductGtinSet = isSet;
            IsProductNameSet = isSet;
            IsReplacementProductIdSet = isSet;
            IsReplacementProductGtinSet = isSet;
            IsReplacementProductNameSet = isSet;
            IsPrioritySet = isSet;
            IsCommentsSet = isSet;
        }
        #endregion
    }

}
