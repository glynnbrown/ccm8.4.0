﻿#region Header Information 
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31934 : A.Heathcote
//      Created.
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public sealed class UserEditorSettingsSelectedColumnDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public String FieldPlaceHolder { get; set; }
        public Byte ColumnType { get; set; }
        public UserEditorSettingsSelectedColumnDtoKey DtoKey
        {
            get
            {
                return new UserEditorSettingsSelectedColumnDtoKey()
                {
                    FieldPlaceHolder = this.FieldPlaceHolder,
                };
            }
        }
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return HashCode.Start.Hash(Id).Hash(FieldPlaceHolder).Hash(ColumnType);
        }

        public override bool Equals(Object obj)
        {
            UserEditorSettingsSelectedColumnDto other = obj as UserEditorSettingsSelectedColumnDto;
            if (other != null)
            {
                return
                    Equals(this.Id, other.Id) &&
                    Equals(this.FieldPlaceHolder, other.FieldPlaceHolder) &&
                    Equals(this.ColumnType, other.ColumnType);

            }
            else
            {
                return false;
            }
        }

        #endregion
        public String ToString()
        {
            return this.Id.ToString();
        }
    }


    [Serializable]
    public sealed class UserEditorSettingsSelectedColumnDtoKey
    {
        #region Properties
        public Object FieldPlaceHolder { get; set; }
        #endregion

        #region methods
        public override int GetHashCode()
        {
            return FieldPlaceHolder.GetHashCode();
        }


        public override bool Equals(object obj)
        {
            UserEditorSettingsSelectedColumnDtoKey other = obj as UserEditorSettingsSelectedColumnDtoKey;
            if (other != null)
            {
                if (!Equals(other.FieldPlaceHolder, this.FieldPlaceHolder)) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
    
