﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25546 : N.Foster
//  Created
// V8-25886 : L.Ineson
//  Removed value
// Added IsHidden and IsReadOnly.
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public sealed class WorkflowTaskParameterDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public WorkflowTaskParameterDtoKey DtoKey 
        { 
            get 
            { return new WorkflowTaskParameterDtoKey() 
                {
                    ParameterId = ParameterId,
                    WorkflowTaskId = WorkflowTaskId
                }; 
            }
        }
        public Int32 ParameterId { get; set; }
        [ForeignKey(typeof(WorkflowTaskDto), typeof(IWorkflowTaskDal))]
        public Int32 WorkflowTaskId { get; set; }
        public Boolean IsHidden { get; set; }
        public Boolean IsReadOnly { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the instance hash code
        /// </summary>
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Compares two instances
        /// </summary>
        public override bool Equals(object obj)
        {
            WorkflowTaskParameterDto other = obj as WorkflowTaskParameterDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.ParameterId != this.ParameterId) return false;
                if (other.WorkflowTaskId != this.WorkflowTaskId) return false;
                if (other.IsHidden != this.IsHidden) return false;
                if (other.IsReadOnly != this.IsReadOnly) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class WorkflowTaskParameterDtoKey
    {
        #region Properties
        public Int32 ParameterId { get; set; }
        public Int32 WorkflowTaskId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return HashCode.Start.Hash(ParameterId).Hash(WorkflowTaskId).GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the ISOme
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            WorkflowTaskParameterDtoKey other = obj as WorkflowTaskParameterDtoKey;
            if (other != null)
            {
                if (other.ParameterId != this.ParameterId) return false;
                if (other.WorkflowTaskId != this.WorkflowTaskId) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
