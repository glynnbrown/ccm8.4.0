﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26124 : I.George
//		Created (Auto-generated)
#endregion
#region Version History: (CCM 820)
// CCM-30759 : J.Pickup
//		InventoryProfileType added.
#endregion

#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// InventoryProfile Data Transfer Object
    /// </summary>
    [Serializable]
    public class InventoryProfileDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public InventoryProfileDtoKey DtoKey
        {
            get
            {
                return new InventoryProfileDtoKey()
                {
                    EntityId = this.EntityId,
                    Name = this.Name
                };
            }
        }
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        public String Name { get; set; }
        public Single CasePack { get; set; }
        public Single DaysOfSupply { get; set; }
        public Single ShelfLife { get; set; }
        public Single ReplenishmentDays { get; set; }
        public Single WasteHurdleUnits { get; set; }
        public Single WasteHurdleCasePack { get; set; }
        public Int32 MinUnits { get; set; }
        public Int32 MinFacings { get; set; }
        public Byte InventoryProfileType { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            InventoryProfileDto other = obj as InventoryProfileDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.CasePack != this.CasePack) { return false; }
                if (other.DaysOfSupply != this.DaysOfSupply) { return false; }
                if (other.ShelfLife != this.ShelfLife) { return false; }
                if (other.ReplenishmentDays != this.ReplenishmentDays) { return false; }
                if (other.WasteHurdleUnits != this.WasteHurdleUnits) { return false; }
                if (other.WasteHurdleCasePack != this.WasteHurdleCasePack) { return false; }
                if (other.MinUnits != this.MinUnits) { return false; }
                if (other.MinFacings != this.MinFacings) { return false; }
                if (other.InventoryProfileType != this.InventoryProfileType) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.DateDeleted != this.DateDeleted) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


    public class InventoryProfileDtoKey
    {

        #region Properties

        public Int32 EntityId { get; set; }
        public String Name { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
     EntityId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            InventoryProfileDtoKey other = obj as InventoryProfileDtoKey;
            if (other != null)
            {

                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


    [Serializable]
    public class InventoryProfileIsSetDto
    {

        #region Properties

        public Boolean IsEntityIdSet { get; set; }
        public Boolean IsNameSet { get; set; }
        public Boolean IsCasePackSet { get; set; }
        public Boolean IsDaysOfSupplySet { get; set; }
        public Boolean IsShelfLifeSet { get; set; }
        public Boolean IsReplenishmentDaysSet { get; set; }
        public Boolean IsWasteHurdleUnitsSet { get; set; }
        public Boolean IsWasteHurdleCasePackSet { get; set; }
        public Boolean IsMinUnitsSet { get; set; }
        public Boolean IsMinFacingsSet { get; set; }
        public Boolean IsInventoryProfileSet { get; set; }
        #endregion

        #region Constructors

        /// <summary>
        /// Default
        /// </summary>
        public InventoryProfileIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public InventoryProfileIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
     IsEntityIdSet.GetHashCode() +
     IsNameSet.GetHashCode() +
     IsCasePackSet.GetHashCode() +
     IsDaysOfSupplySet.GetHashCode() +
     IsShelfLifeSet.GetHashCode() +
     IsReplenishmentDaysSet.GetHashCode() +
     IsWasteHurdleUnitsSet.GetHashCode() +
     IsWasteHurdleCasePackSet.GetHashCode() +
     IsMinUnitsSet.GetHashCode() +
     IsMinFacingsSet.GetHashCode() + 
     IsInventoryProfileSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            InventoryProfileIsSetDto other = obj as InventoryProfileIsSetDto;
            if (other != null)
            {

                if (other.IsEntityIdSet != this.IsEntityIdSet) { return false; }
                if (other.IsNameSet != this.IsNameSet) { return false; }
                if (other.IsCasePackSet != this.IsCasePackSet) { return false; }
                if (other.IsDaysOfSupplySet != this.IsDaysOfSupplySet) { return false; }
                if (other.IsShelfLifeSet != this.IsShelfLifeSet) { return false; }
                if (other.IsReplenishmentDaysSet != this.IsReplenishmentDaysSet) { return false; }
                if (other.IsWasteHurdleUnitsSet != this.IsWasteHurdleUnitsSet) { return false; }
                if (other.IsWasteHurdleCasePackSet != this.IsWasteHurdleCasePackSet) { return false; }
                if (other.IsMinUnitsSet != this.IsMinUnitsSet) { return false; }
                if (other.IsMinFacingsSet != this.IsMinFacingsSet) { return false; }
                if (other.IsInventoryProfileSet != this.IsInventoryProfileSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsEntityIdSet = isSet;
            IsNameSet = isSet;
            IsCasePackSet = isSet;
            IsDaysOfSupplySet = isSet;
            IsShelfLifeSet = isSet;
            IsReplenishmentDaysSet = isSet;
            IsWasteHurdleUnitsSet = isSet;
            IsWasteHurdleCasePackSet = isSet;
            IsMinUnitsSet = isSet;
            IsMinFacingsSet = isSet;
            IsInventoryProfileSet = isSet;
        }
        #endregion
    }
}

