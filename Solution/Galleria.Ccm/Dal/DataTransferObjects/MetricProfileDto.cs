﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26123 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// MetricProfile Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class MetricProfileDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public MetricProfileDtoKey DtoKey
        {
            get
            {
                return new MetricProfileDtoKey()
                {
                    EntityId = this.EntityId,
                    Name = this.Name
                };
            }
        }
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        public String Name { get; set; }
        public Int32? Metric1MetricId { get; set; }
        public Single? Metric1Ratio { get; set; }
        public Int32? Metric2MetricId { get; set; }
        public Single? Metric2Ratio { get; set; }
        public Int32? Metric3MetricId { get; set; }
        public Single? Metric3Ratio { get; set; }
        public Int32? Metric4MetricId { get; set; }
        public Single? Metric4Ratio { get; set; }
        public Int32? Metric5MetricId { get; set; }
        public Single? Metric5Ratio { get; set; }
        public Int32? Metric6MetricId { get; set; }
        public Single? Metric6Ratio { get; set; }
        public Int32? Metric7MetricId { get; set; }
        public Single? Metric7Ratio { get; set; }
        public Int32? Metric8MetricId { get; set; }
        public Single? Metric8Ratio { get; set; }
        public Int32? Metric9MetricId { get; set; }
        public Single? Metric9Ratio { get; set; }
        public Int32? Metric10MetricId { get; set; }
        public Single? Metric10Ratio { get; set; }
        public Int32? Metric11MetricId { get; set; }
        public Single? Metric11Ratio { get; set; }
        public Int32? Metric12MetricId { get; set; }
        public Single? Metric12Ratio { get; set; }
        public Int32? Metric13MetricId { get; set; }
        public Single? Metric13Ratio { get; set; }
        public Int32? Metric14MetricId { get; set; }
        public Single? Metric14Ratio { get; set; }
        public Int32? Metric15MetricId { get; set; }
        public Single? Metric15Ratio { get; set; }
        public Int32? Metric16MetricId { get; set; }
        public Single? Metric16Ratio { get; set; }
        public Int32? Metric17MetricId { get; set; }
        public Single? Metric17Ratio { get; set; }
        public Int32? Metric18MetricId { get; set; }
        public Single? Metric18Ratio { get; set; }
        public Int32? Metric19MetricId { get; set; }
        public Single? Metric19Ratio { get; set; }
        public Int32? Metric20MetricId { get; set; }
        public Single? Metric20Ratio { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            MetricProfileDto other = obj as MetricProfileDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.Metric1MetricId != this.Metric1MetricId) { return false; }
                if (other.Metric1Ratio != this.Metric1Ratio) { return false; }
                if (other.Metric2MetricId != this.Metric2MetricId) { return false; }
                if (other.Metric2Ratio != this.Metric2Ratio) { return false; }
                if (other.Metric3MetricId != this.Metric3MetricId) { return false; }
                if (other.Metric3Ratio != this.Metric3Ratio) { return false; }
                if (other.Metric4MetricId != this.Metric4MetricId) { return false; }
                if (other.Metric4Ratio != this.Metric4Ratio) { return false; }
                if (other.Metric5MetricId != this.Metric5MetricId) { return false; }
                if (other.Metric5Ratio != this.Metric5Ratio) { return false; }
                if (other.Metric6MetricId != this.Metric6MetricId) { return false; }
                if (other.Metric6Ratio != this.Metric6Ratio) { return false; }
                if (other.Metric7MetricId != this.Metric7MetricId) { return false; }
                if (other.Metric7Ratio != this.Metric7Ratio) { return false; }
                if (other.Metric8MetricId != this.Metric8MetricId) { return false; }
                if (other.Metric8Ratio != this.Metric8Ratio) { return false; }
                if (other.Metric9MetricId != this.Metric9MetricId) { return false; }
                if (other.Metric9Ratio != this.Metric9Ratio) { return false; }
                if (other.Metric10MetricId != this.Metric10MetricId) { return false; }
                if (other.Metric10Ratio != this.Metric10Ratio) { return false; }
                if (other.Metric11MetricId != this.Metric11MetricId) { return false; }
                if (other.Metric11Ratio != this.Metric11Ratio) { return false; }
                if (other.Metric12MetricId != this.Metric12MetricId) { return false; }
                if (other.Metric12Ratio != this.Metric12Ratio) { return false; }
                if (other.Metric13MetricId != this.Metric13MetricId) { return false; }
                if (other.Metric13Ratio != this.Metric13Ratio) { return false; }
                if (other.Metric14MetricId != this.Metric14MetricId) { return false; }
                if (other.Metric14Ratio != this.Metric14Ratio) { return false; }
                if (other.Metric15MetricId != this.Metric15MetricId) { return false; }
                if (other.Metric15Ratio != this.Metric15Ratio) { return false; }
                if (other.Metric16MetricId != this.Metric16MetricId) { return false; }
                if (other.Metric16Ratio != this.Metric16Ratio) { return false; }
                if (other.Metric17MetricId != this.Metric17MetricId) { return false; }
                if (other.Metric17Ratio != this.Metric17Ratio) { return false; }
                if (other.Metric18MetricId != this.Metric18MetricId) { return false; }
                if (other.Metric18Ratio != this.Metric18Ratio) { return false; }
                if (other.Metric19MetricId != this.Metric19MetricId) { return false; }
                if (other.Metric19Ratio != this.Metric19Ratio) { return false; }
                if (other.Metric20MetricId != this.Metric20MetricId) { return false; }
                if (other.Metric20Ratio != this.Metric20Ratio) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.DateDeleted != this.DateDeleted) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


    public class MetricProfileDtoKey
    {

        #region Properties

        public Int32 EntityId { get; set; }
        public String Name { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
     EntityId.GetHashCode() +
     Name.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            MetricProfileDtoKey other = obj as MetricProfileDtoKey;
            if (other != null)
            {

                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


}
