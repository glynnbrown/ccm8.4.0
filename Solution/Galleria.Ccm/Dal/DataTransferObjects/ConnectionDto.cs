﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25559 : L.Ineson
//		Copied from SA (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// User Data Transfer Object
    /// </summary>
    [Serializable]
    public class ConnectionDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public ConnectionDtoKey DtoKey
        {
            get
            {
                return new ConnectionDtoKey()
                {
                    Id = this.Id,
                };
            }
        }
        //public Byte Type { get; set; }
        public String ServerName { get; set; }
        public String DatabaseName { get; set; }
        public String DatabaseFileName { get; set; }
        public String HostName { get; set; }
        public Byte PortNumber { get; set; }
        public Int32 BusinessEntityId { get; set; }
        public Boolean IsAutoConnect { get; set; }
        public Boolean IsCurrentConnection { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ConnectionDto other = obj as ConnectionDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                //if (other.Type != this.Type) { return false; }
                if (other.ServerName != this.ServerName) { return false; }
                if (other.DatabaseName != this.DatabaseName) { return false; }
                if (other.DatabaseFileName != this.DatabaseFileName) { return false; }
                if (other.HostName != this.HostName) { return false; }
                if (other.PortNumber != this.PortNumber) { return false; }
                if (other.BusinessEntityId != this.BusinessEntityId) { return false; }
                if (other.IsAutoConnect != this.IsAutoConnect) { return false; }
                if (other.IsCurrentConnection != this.IsCurrentConnection) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class ConnectionDtoKey
    {
        #region Properties
        public Int32 Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ConnectionDtoKey other = obj as ConnectionDtoKey;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
