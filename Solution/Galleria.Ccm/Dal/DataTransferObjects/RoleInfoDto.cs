﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-26234 : L.Ineson
//		Copied from GFS
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class RoleInfoDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Boolean IsAdministrator { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Compares an object to this instance
        /// </summary>
        /// <param name="obj">The object to compare</param>
        /// <returns>True if the items are equal, else false</returns>
        public override bool Equals(object obj)
        {
            RoleInfoDto other = obj as RoleInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.Description != this.Description) { return false; }
                if (other.IsAdministrator != this.IsAdministrator) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
