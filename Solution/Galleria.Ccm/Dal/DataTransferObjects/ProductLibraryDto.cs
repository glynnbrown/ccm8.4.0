﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25395 : A.Probyn
//		Created (Auto-generated)
#endregion
#region Version History: (CCM V8.3)
// V8-32361 : L.Ineson
//  Added Name,EntityId,  DateCreated and DateLastModified & rowversion
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// ProductLibrary Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class ProductLibraryDto
    {
        #region Properties
        public Object Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public ProductLibraryDtoKey DtoKey
        {
            get
            {
                return new ProductLibraryDtoKey()
                {
                    Name = this.Name,
                    EntityId = this.EntityId
                };
            }
        }
        public String Name { get; set; }
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        public Byte FileType { get; set; }
        public String ExcelWorkbookSheet { get; set; }
        public Int32 StartAtRow { get; set; }
        public Boolean IsFirstRowHeaders { get; set; }
        public Byte? TextDataFormat { get; set; }
        public Byte? TextDelimiterType { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ProductLibraryDto other = obj as ProductLibraryDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }

                if (other.RowVersion != this.RowVersion) return false;
                if (other.Name != this.Name) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.FileType != this.FileType) { return false; }
                if (other.ExcelWorkbookSheet != this.ExcelWorkbookSheet) { return false; }
                if (other.StartAtRow != this.StartAtRow) { return false; }
                if (other.IsFirstRowHeaders != this.IsFirstRowHeaders) { return false; }
                if (other.TextDataFormat != this.TextDataFormat) { return false; }
                if (other.TextDelimiterType != this.TextDelimiterType) { return false; }
                if (other.DateCreated != this.DateCreated) return false;
                if (other.DateLastModified != this.DateLastModified) return false; 
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class ProductLibraryDtoKey
    {
        #region Properties
        public String Name { get; set; }
        public Int32 EntityId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return HashCode.Start
                .Hash(EntityId)
                .Hash(Name)
                .GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ProductLibraryDtoKey other = obj as ProductLibraryDtoKey;
            if (other != null)
            {
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
