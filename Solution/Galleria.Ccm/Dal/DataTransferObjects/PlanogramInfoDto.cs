﻿#region Header Information

// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800

// V8-25608 : N.Foster
//  Created
// V8-25787 : N.Foster
//  Added ProcessingStatus, RowVersion
// V8-25880 : N.Haywood
//  Added CategoryCode and CategoryName
// V8-25881 : A.Probyn
//  Added MetaData properties
// V8-26492 : L.Ineson
//  Added uom properties
// V8-27058 : A.Probyn
//  Updated for up to date meta data properties
// V8-27154 : L.Luong
//  Added ProductPlacement X,Y,Z
// V8-27411 : M.Pettit
//  Added Locking fields
//  Added AutomationDateStarted, AutomationDateLastUpdated properties
//  Added MetaData and Validation status properties
//  Changed Status to a byte as it is now an enum value
//  Added LockUserDisplayName property
//  Added PlanogramInfo_ValidationIsOutOfDate property
//  Added UserName property

#endregion

#region Version History: CCM803

// V8-29326 : M.Shelley
//	Added ClusterSchemeName and ClusterName Properties for displaying in plan assignment
//	Added LocationCode and LocationName Properties for displaying in plan assignment
// V8-29590 : A.Probyn
//	Extended for new Added MetaNoOfErrors, MetaNoOfWarnings, 
//	MetaTotalErrorScore, MetaHighestErrorScore
// V8-28242 : M.Shelley
//  Added Status, DateWIP, DateApproved, DateArchived 
// V8-29860 : M.Shelley
//  Added the PlanogramType property

#endregion

#region Version History: CCM810

// V8-29590 : A.Probyn
//  Added MetaNoOfDebugEvents & MetaNoOfInformationEvents

#endregion

#region Version History: CCM811

// V8-30164 : A.Silva
//  Amended MetaAverageFacings, MetaAverageUnits, MetaSpaceToUnitsIndex to be <Single?>.
// V8-30225 : M.Shelley
//  Added the Planogram UCR
// V8-30359 : A.Probyn
//  Added new MostRecentLinkedWorkpackageName

#endregion

#region Version History: CCM820
// V8-31131 : A.Probyn
//  Removed MetaSpaceToUnitsIndex
#endregion

#region Version History: CCM830
// CCM-32015 : N.Haywood
//  Added mission fields
// CCM-31831 : A.Probyn
//  Adding more missing fields.
// V8-32396 : A.Probyn
//  Added MetaNumberOfDelistFamilyRulesBroken
// V8-32521 : J.Pickup
//	Added MetaHasComponentsOutsideOfFixtureArea
// V8-32814 : M.Pettit
//  Added MetaCountOfProductsBuddied
// V8-33003 : J.Pickup
//  PlanogramInfo_AssignedLocationCount Added.
#endregion

#region Version History: CCM840
// V8-19069 : J.Mendes
//  Five new Note field attributes for the Custom Attribute Data
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public sealed class PlanogramInfoDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public Guid UniqueContentReference { get; set; }
        public RowVersion RowVersion { get; set; }
        public String Name { get; set; }
        public Byte Status { get; set; }
        public Single Height { get; set; }
        public Single Width { get; set; }
        public Single Depth { get; set; }
        public String UserName { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        public String CategoryCode { get; set; }
        public String CategoryName { get; set; }
        public Byte LengthUnitsOfMeasure { get; set; }
        public Byte AreaUnitsOfMeasure { get; set; }
        public Byte VolumeUnitsOfMeasure { get; set; }
        public Byte WeightUnitsOfMeasure { get; set; }
        public Byte CurrencyUnitsOfMeasure { get; set; }
        public Byte ProductPlacementX { get; set; }
        public Byte ProductPlacementY { get; set; }
        public Byte ProductPlacementZ { get; set; }
        public Int32? MetaBayCount { get; set; }
        public Int32? MetaUniqueProductCount { get; set; }
        public Int32? MetaComponentCount { get; set; }
        public Single? MetaTotalMerchandisableLinearSpace { get; set; }
        public Single? MetaTotalMerchandisableAreaSpace { get; set; }
        public Single? MetaTotalMerchandisableVolumetricSpace { get; set; }
        public Single? MetaTotalLinearWhiteSpace { get; set; }
        public Single? MetaTotalAreaWhiteSpace { get; set; }
        public Single? MetaTotalVolumetricWhiteSpace { get; set; }
        public Int32? MetaProductsPlaced { get; set; }
        public Int32? MetaProductsUnplaced { get; set; }
        public Int32? MetaNewProducts { get; set; }
        public Int32? MetaChangesFromPreviousCount { get; set; }
        public Int32? MetaChangeFromPreviousStarRating { get; set; }
        public Int32? MetaBlocksDropped { get; set; }
        public Int32? MetaNotAchievedInventory { get; set; }
        public Int32? MetaTotalFacings { get; set; }
        public Single? MetaAverageFacings { get; set; }
        public Int32? MetaTotalUnits { get; set; }
        public Single? MetaAverageUnits { get; set; }
        public Single? MetaMinDos { get; set; }
        public Single? MetaMaxDos { get; set; }
        public Single? MetaAverageDos { get; set; }
        public Single? MetaMinCases { get; set; }
        public Single? MetaAverageCases { get; set; }
        public Single? MetaMaxCases { get; set; }
        public Int32? PlanogramGroupId { get; set; }
        public String PlanogramGroupName { get; set; }
        public Byte AutomationProcessingStatus { get; set; }
        public String AutomationProcessingStatusDescription { get; set; }
        public Int32 AutomationProgressMax { get; set; }
        public Int32 AutomationProgressCurrent { get; set; }
        public DateTime? AutomationDateStarted { get; set; }
        public DateTime? AutomationDateLastUpdated { get; set; }
        public Byte MetaDataProcessingStatus { get; set; }
        public String MetaDataProcessingStatusDescription { get; set; }
        public Int32 MetaDataProgressMax { get; set; }
        public Int32 MetaDataProgressCurrent { get; set; }
        public DateTime? MetaDataDateStarted { get; set; }
        public DateTime? MetaDataDateLastUpdated { get; set; }
        public Byte ValidationProcessingStatus { get; set; }
        public String ValidationProcessingStatusDescription { get; set; }
        public Int32 ValidationProgressMax { get; set; }
        public Int32 ValidationProgressCurrent { get; set; }
        public DateTime? ValidationDateStarted { get; set; }
        public DateTime? ValidationDateLastUpdated { get; set; }
        public Int32? MetaTotalComponentCollisions { get; set; }
        public Int32? MetaTotalComponentsOverMerchandisedDepth { get; set; }
        public Int32? MetaTotalComponentsOverMerchandisedHeight { get; set; }
        public Int32? MetaTotalComponentsOverMerchandisedWidth { get; set; }
        public Boolean? MetaHasComponentsOutsideOfFixtureArea { get; set; }
        public Int32? MetaTotalPositionCollisions { get; set; }
        public Int16? MetaTotalFrontFacings { get; set; }
        public Single? MetaAverageFrontFacings { get; set; }
        public Int32? MetaNoOfErrors { get; set; }
        public Int32? MetaNoOfWarnings { get; set; }
        public Int32? MetaNoOfInformationEvents { get; set; }
        public Int32? MetaNoOfDebugEvents { get; set; }
        public Int32? MetaTotalErrorScore { get; set; }
        public Byte? MetaHighestErrorScore { get; set; }
        public DateTime? DateMetadataCalculated { get; set; }
        public DateTime? DateValidationDataCalculated { get; set; }
        public Boolean IsLocked { get; set; }
        public Byte? LockType { get; set; }
        public Int32? LockUserId { get; set; }
        public String LockUserName { get; set; }
        public String LockUserDisplayName { get; set; }
        public DateTime? LockDate { get; set; }
        public Boolean ValidationIsOutOfDate { get; set; }
        public String ClusterSchemeName { get; set; }
        public String ClusterName { get; set; }
        public String LocationCode { get; set; }
        public String LocationName { get; set; }
        public DateTime? DateWip { get; set; }
        public DateTime? DateApproved { get; set; }
        public DateTime? DateArchived { get; set; }
        public Byte PlanogramType { get; set; }
        public String MostRecentLinkedWorkpackageName { get; set; }

        public Byte AngleUnitsOfMeasure { get; set; }
        public String HighlightSequenceStrategy { get; set; }
        public Int32 AssignedLocationCount { get; set; }

        public Int32? MetaCountOfPlacedProductsNotRecommendedInAssortment { get; set; }
        public Int32? MetaCountOfPlacedProductsRecommendedInAssortment { get; set; }
        public Int32? MetaCountOfPositionsOutsideOfBlockSpace { get; set; }
        public Int32? MetaCountOfProductNotAchievedCases { get; set; }
        public Int32? MetaCountOfProductNotAchievedDeliveries { get; set; }
        public Int32? MetaCountOfProductNotAchievedDOS { get; set; }
        public Int32? MetaCountOfProductOverShelfLifePercent { get; set; }
        public Int32? MetaCountOfRecommendedProductsInAssortment { get; set; }
        public Int16? MetaNumberOfAssortmentRulesBroken { get; set; }
        public Int16? MetaNumberOfCoreRulesBroken { get; set; }
        public Int16? MetaNumberOfDelistProductRulesBroken { get; set; }
        public Int16? MetaNumberOfDependencyFamilyRulesBroken { get; set; }
        public Int16? MetaNumberOfDelistFamilyRulesBroken { get; set; }
        public Int16? MetaNumberOfDistributionRulesBroken { get; set; }
        public Int16? MetaNumberOfFamilyRulesBroken { get; set; }
        public Int16? MetaNumberOfForceProductRulesBroken { get; set; }
        public Int16? MetaNumberOfInheritanceRulesBroken { get; set; }
        public Int16? MetaNumberOfLocalProductRulesBroken { get; set; }
        public Int16? MetaNumberOfMaximumProductFamilyRulesBroken { get; set; }
        public Int16? MetaNumberOfMinimumHurdleProductRulesBroken { get; set; }
        public Int16? MetaNumberOfMinimumProductFamilyRulesBroken { get; set; }
        public Int16? MetaNumberOfPreserveProductRulesBroken { get; set; }
        public Int16? MetaNumberOfProductRulesBroken { get; set; }
        public Single? MetaPercentageOfAssortmentRulesBroken { get; set; }
        public Single? MetaPercentageOfCoreRulesBroken { get; set; }
        public Single? MetaPercentageOfDistributionRulesBroken { get; set; }
        public Single? MetaPercentageOfFamilyRulesBroken { get; set; }
        public Single? MetaPercentageOfInheritanceRulesBroken { get; set; }
        public Single? MetaPercentageOfLocalProductRulesBroken { get; set; }
        public Single? MetaPercentageOfProductRulesBroken { get; set; }
        public Single? MetaPercentOfPlacedProductsRecommendedInAssortment { get; set; }
        public Single? MetaPercentOfPositionsOutsideOfBlockSpace { get; set; }
        public Single? MetaPercentOfProductNotAchievedCases { get; set; }
        public Single? MetaPercentOfProductNotAchievedDeliveries { get; set; }
        public Single? MetaPercentOfProductNotAchievedDOS { get; set; }
        public Single? MetaPercentOfProductNotAchievedInventory { get; set; }
        public Single? MetaPercentOfProductOverShelfLifePercent { get; set; }
        public Int16? MetaCountOfProductsBuddied { get; set; }

        public String Text1 { get; set; }
        public String Text2 { get; set; }
        public String Text3 { get; set; }
        public String Text4 { get; set; }
        public String Text5 { get; set; }
        public String Text6 { get; set; }
        public String Text7 { get; set; }
        public String Text8 { get; set; }
        public String Text9 { get; set; }
        public String Text10 { get; set; }
        public String Text11 { get; set; }
        public String Text12 { get; set; }
        public String Text13 { get; set; }
        public String Text14 { get; set; }
        public String Text15 { get; set; }
        public String Text16 { get; set; }
        public String Text17 { get; set; }
        public String Text18 { get; set; }
        public String Text19 { get; set; }
        public String Text20 { get; set; }
        public String Text21 { get; set; }
        public String Text22 { get; set; }
        public String Text23 { get; set; }
        public String Text24 { get; set; }
        public String Text25 { get; set; }
        public String Text26 { get; set; }
        public String Text27 { get; set; }
        public String Text28 { get; set; }
        public String Text29 { get; set; }
        public String Text30 { get; set; }
        public String Text31 { get; set; }
        public String Text32 { get; set; }
        public String Text33 { get; set; }
        public String Text34 { get; set; }
        public String Text35 { get; set; }
        public String Text36 { get; set; }
        public String Text37 { get; set; }
        public String Text38 { get; set; }
        public String Text39 { get; set; }
        public String Text40 { get; set; }
        public String Text41 { get; set; }
        public String Text42 { get; set; }
        public String Text43 { get; set; }
        public String Text44 { get; set; }
        public String Text45 { get; set; }
        public String Text46 { get; set; }
        public String Text47 { get; set; }
        public String Text48 { get; set; }
        public String Text49 { get; set; }
        public String Text50 { get; set; }
        public Single? Value1 { get; set; }
        public Single? Value2 { get; set; }
        public Single? Value3 { get; set; }
        public Single? Value4 { get; set; }
        public Single? Value5 { get; set; }
        public Single? Value6 { get; set; }
        public Single? Value7 { get; set; }
        public Single? Value8 { get; set; }
        public Single? Value9 { get; set; }
        public Single? Value10 { get; set; }
        public Single? Value11 { get; set; }
        public Single? Value12 { get; set; }
        public Single? Value13 { get; set; }
        public Single? Value14 { get; set; }
        public Single? Value15 { get; set; }
        public Single? Value16 { get; set; }
        public Single? Value17 { get; set; }
        public Single? Value18 { get; set; }
        public Single? Value19 { get; set; }
        public Single? Value20 { get; set; }
        public Single? Value21 { get; set; }
        public Single? Value22 { get; set; }
        public Single? Value23 { get; set; }
        public Single? Value24 { get; set; }
        public Single? Value25 { get; set; }
        public Single? Value26 { get; set; }
        public Single? Value27 { get; set; }
        public Single? Value28 { get; set; }
        public Single? Value29 { get; set; }
        public Single? Value30 { get; set; }
        public Single? Value31 { get; set; }
        public Single? Value32 { get; set; }
        public Single? Value33 { get; set; }
        public Single? Value34 { get; set; }
        public Single? Value35 { get; set; }
        public Single? Value36 { get; set; }
        public Single? Value37 { get; set; }
        public Single? Value38 { get; set; }
        public Single? Value39 { get; set; }
        public Single? Value40 { get; set; }
        public Single? Value41 { get; set; }
        public Single? Value42 { get; set; }
        public Single? Value43 { get; set; }
        public Single? Value44 { get; set; }
        public Single? Value45 { get; set; }
        public Single? Value46 { get; set; }
        public Single? Value47 { get; set; }
        public Single? Value48 { get; set; }
        public Single? Value49 { get; set; }
        public Single? Value50 { get; set; }
        public Boolean? Flag1 { get; set; }
        public Boolean? Flag2 { get; set; }
        public Boolean? Flag3 { get; set; }
        public Boolean? Flag4 { get; set; }
        public Boolean? Flag5 { get; set; }
        public Boolean? Flag6 { get; set; }
        public Boolean? Flag7 { get; set; }
        public Boolean? Flag8 { get; set; }
        public Boolean? Flag9 { get; set; }
        public Boolean? Flag10 { get; set; }
        public DateTime? Date1 { get; set; }
        public DateTime? Date2 { get; set; }
        public DateTime? Date3 { get; set; }
        public DateTime? Date4 { get; set; }
        public DateTime? Date5 { get; set; }
        public DateTime? Date6 { get; set; }
        public DateTime? Date7 { get; set; }
        public DateTime? Date8 { get; set; }
        public DateTime? Date9 { get; set; }
        public DateTime? Date10 { get; set; }
        public String Note1 { get; set; }
        public String Note2 { get; set; }
        public String Note3 { get; set; }
        public String Note4 { get; set; }
        public String Note5 { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the instance has code
        /// </summary>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Compares two instances to each other
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            PlanogramInfoDto other = obj as PlanogramInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.UniqueContentReference != this.UniqueContentReference) return false;
                if (other.RowVersion != this.RowVersion) return false;
                if (other.Name != this.Name) return false;
                if (other.Status != this.Status) return false;
                if (other.Height != this.Height) return false;
                if (other.Width != this.Width) return false;
                if (other.Depth != this.Depth) return false;
                if (other.UserName != this.UserName) return false;
                if (other.DateCreated != this.DateCreated) return false;
                if (other.DateLastModified != this.DateLastModified) return false;
                if (other.DateDeleted != this.DateDeleted) return false;
                if (other.CategoryCode != this.CategoryCode) return false;
                if (other.CategoryName != this.CategoryName) return false;
                if (other.LengthUnitsOfMeasure != this.LengthUnitsOfMeasure) return false;
                if (other.AreaUnitsOfMeasure != this.AreaUnitsOfMeasure) return false;
                if (other.VolumeUnitsOfMeasure != this.VolumeUnitsOfMeasure) return false;
                if (other.WeightUnitsOfMeasure != this.WeightUnitsOfMeasure) return false;
                if (other.CurrencyUnitsOfMeasure != this.CurrencyUnitsOfMeasure) return false;
                if (other.ProductPlacementX != this.ProductPlacementX) return false;
                if (other.ProductPlacementY != this.ProductPlacementY) return false;
                if (other.ProductPlacementZ != this.ProductPlacementZ) return false;
                if (other.MetaBayCount != this.MetaBayCount) return false;
                if (other.MetaUniqueProductCount != this.MetaUniqueProductCount) return false;
                if (other.MetaComponentCount != this.MetaComponentCount) return false;
                if (other.MetaTotalMerchandisableLinearSpace != this.MetaTotalMerchandisableLinearSpace) return false;
                if (other.MetaTotalMerchandisableAreaSpace != this.MetaTotalMerchandisableAreaSpace) return false;
                if (other.MetaTotalMerchandisableVolumetricSpace != this.MetaTotalMerchandisableVolumetricSpace) return false;
                if (other.MetaTotalLinearWhiteSpace != this.MetaTotalLinearWhiteSpace) return false;
                if (other.MetaTotalAreaWhiteSpace != this.MetaTotalAreaWhiteSpace) return false;
                if (other.MetaTotalVolumetricWhiteSpace != this.MetaTotalVolumetricWhiteSpace) return false;
                if (other.MetaProductsPlaced != this.MetaProductsPlaced) return false;
                if (other.MetaProductsUnplaced != this.MetaProductsUnplaced) return false;
                if (other.MetaNewProducts != this.MetaNewProducts) return false;
                if (other.MetaChangesFromPreviousCount != this.MetaChangesFromPreviousCount) return false;
                if (other.MetaChangeFromPreviousStarRating != this.MetaChangeFromPreviousStarRating) return false;
                if (other.MetaBlocksDropped != this.MetaBlocksDropped) return false;
                if (other.MetaNotAchievedInventory != this.MetaNotAchievedInventory) return false;
                if (other.MetaTotalFacings != this.MetaTotalFacings) return false;
                if (other.MetaAverageFacings != this.MetaAverageFacings) return false;
                if (other.MetaTotalUnits != this.MetaTotalUnits) return false;
                if (other.MetaAverageUnits != this.MetaAverageUnits) return false;
                if (other.MetaMinDos != this.MetaMinDos) return false;
                if (other.MetaMaxDos != this.MetaMaxDos) return false;
                if (other.MetaAverageDos != this.MetaAverageDos) return false;
                if (other.MetaMinCases != this.MetaMinCases) return false;
                if (other.MetaAverageCases != this.MetaAverageCases) return false;
                if (other.MetaMaxCases != this.MetaMaxCases) return false;
                if (other.PlanogramGroupId != this.PlanogramGroupId) return false;
                if (other.PlanogramGroupName != this.PlanogramGroupName) return false;
                if (other.AutomationProcessingStatus != this.AutomationProcessingStatus) return false;
                if (other.AutomationProcessingStatusDescription != this.AutomationProcessingStatusDescription) return false;
                if (other.AutomationProgressMax != this.AutomationProgressMax) return false;
                if (other.AutomationProgressCurrent != this.AutomationProgressCurrent) return false;
                if (other.AutomationDateStarted != this.AutomationDateStarted) return false;
                if (other.AutomationDateLastUpdated != this.AutomationDateLastUpdated) return false;
                if (other.MetaDataProcessingStatus != this.MetaDataProcessingStatus) return false;
                if (other.MetaDataProcessingStatusDescription != this.MetaDataProcessingStatusDescription) return false;
                if (other.MetaDataProgressMax != this.MetaDataProgressMax) return false;
                if (other.MetaDataProgressCurrent != this.MetaDataProgressCurrent) return false;
                if (other.MetaDataDateStarted != this.MetaDataDateStarted) return false;
                if (other.MetaDataDateLastUpdated != this.MetaDataDateLastUpdated) return false;
                if (other.ValidationProcessingStatus != this.ValidationProcessingStatus) return false;
                if (other.ValidationProcessingStatusDescription != this.ValidationProcessingStatusDescription) return false;
                if (other.ValidationProgressMax != this.ValidationProgressMax) return false;
                if (other.ValidationProgressCurrent != this.ValidationProgressCurrent) return false;
                if (other.ValidationDateStarted != this.ValidationDateStarted) return false;
                if (other.ValidationDateLastUpdated != this.ValidationDateLastUpdated) return false;
                if (other.MetaTotalComponentCollisions != this.MetaTotalComponentCollisions) return false;
                if (other.MetaTotalComponentsOverMerchandisedDepth != this.MetaTotalComponentsOverMerchandisedDepth) return false;
                if (other.MetaTotalComponentsOverMerchandisedHeight != this.MetaTotalComponentsOverMerchandisedHeight) return false;
                if (other.MetaTotalComponentsOverMerchandisedWidth != this.MetaTotalComponentsOverMerchandisedWidth) return false;
                if (other.MetaTotalPositionCollisions != this.MetaTotalPositionCollisions) return false;
                if (other.MetaTotalFrontFacings != this.MetaTotalFrontFacings) return false;
                if (other.MetaAverageFrontFacings != this.MetaAverageFrontFacings) return false;
                if (other.MetaNoOfErrors != this.MetaNoOfErrors) return false;
                if (other.MetaNoOfWarnings != this.MetaNoOfWarnings) return false;
                if (other.MetaNoOfInformationEvents != this.MetaNoOfInformationEvents) return false;
                if (other.MetaNoOfDebugEvents != this.MetaNoOfDebugEvents) return false;
                if (other.MetaTotalErrorScore != this.MetaTotalErrorScore) return false;
                if (other.MetaHighestErrorScore != this.MetaHighestErrorScore) return false;
                if (other.DateMetadataCalculated != this.DateMetadataCalculated) return false;
                if (other.DateValidationDataCalculated != this.DateValidationDataCalculated) return false;
                if (other.IsLocked != this.IsLocked) return false;
                if (other.LockType != this.LockType) return false;
                if (other.LockUserId != this.LockUserId) return false;
                if (other.LockUserName != this.LockUserName) return false;
                if (other.LockUserDisplayName != this.LockUserDisplayName) return false;
                if (other.LockDate != this.LockDate) return false;
                if (other.ValidationIsOutOfDate != this.ValidationIsOutOfDate) return false;
                if (other.ClusterSchemeName != this.ClusterSchemeName) return false;
                if (other.ClusterName != this.ClusterName) return false;
                if (other.LocationCode != this.LocationCode) return false;
                if (other.LocationName != this.LocationName) return false;
                if (other.DateWip != this.DateWip) return false;
                if (other.DateApproved != this.DateApproved) return false;
                if (other.DateArchived != this.DateArchived) return false;
                if (other.PlanogramType != this.PlanogramType) return false;
                if (other.MostRecentLinkedWorkpackageName != this.MostRecentLinkedWorkpackageName) return false;
                if (other.AngleUnitsOfMeasure != this.AngleUnitsOfMeasure) return false;
                if (other.HighlightSequenceStrategy != this.HighlightSequenceStrategy) return false;
                if (other.AssignedLocationCount != this.AssignedLocationCount) return false;

                if (other.MetaCountOfPlacedProductsNotRecommendedInAssortment != this.MetaCountOfPlacedProductsNotRecommendedInAssortment) return false;
                if (other.MetaCountOfPlacedProductsRecommendedInAssortment != this.MetaCountOfPlacedProductsRecommendedInAssortment) return false;
                if (other.MetaCountOfPositionsOutsideOfBlockSpace != this.MetaCountOfPositionsOutsideOfBlockSpace) return false;
                if (other.MetaCountOfProductNotAchievedCases != this.MetaCountOfProductNotAchievedCases) return false;
                if (other.MetaCountOfProductNotAchievedDeliveries != this.MetaCountOfProductNotAchievedDeliveries) return false;
                if (other.MetaCountOfProductNotAchievedDOS != this.MetaCountOfProductNotAchievedDOS) return false;
                if (other.MetaCountOfProductOverShelfLifePercent != this.MetaCountOfProductOverShelfLifePercent) return false;
                if (other.MetaCountOfRecommendedProductsInAssortment != this.MetaCountOfRecommendedProductsInAssortment) return false;
                if (other.MetaNumberOfAssortmentRulesBroken != this.MetaNumberOfAssortmentRulesBroken) return false;
                if (other.MetaNumberOfCoreRulesBroken != this.MetaNumberOfCoreRulesBroken) return false;
                if (other.MetaNumberOfDelistProductRulesBroken != this.MetaNumberOfDelistProductRulesBroken) return false;
                if (other.MetaNumberOfDependencyFamilyRulesBroken != this.MetaNumberOfDependencyFamilyRulesBroken) return false;
                if (other.MetaNumberOfDelistFamilyRulesBroken != this.MetaNumberOfDelistFamilyRulesBroken) return false;
                if (other.MetaNumberOfDistributionRulesBroken != this.MetaNumberOfDistributionRulesBroken) return false;
                if (other.MetaNumberOfFamilyRulesBroken != this.MetaNumberOfFamilyRulesBroken) return false;
                if (other.MetaNumberOfForceProductRulesBroken != this.MetaNumberOfForceProductRulesBroken) return false;
                if (other.MetaNumberOfInheritanceRulesBroken != this.MetaNumberOfInheritanceRulesBroken) return false;
                if (other.MetaNumberOfLocalProductRulesBroken != this.MetaNumberOfLocalProductRulesBroken) return false;
                if (other.MetaNumberOfMaximumProductFamilyRulesBroken != this.MetaNumberOfMaximumProductFamilyRulesBroken) return false;
                if (other.MetaNumberOfMinimumHurdleProductRulesBroken != this.MetaNumberOfMinimumHurdleProductRulesBroken) return false;
                if (other.MetaNumberOfMinimumProductFamilyRulesBroken != this.MetaNumberOfMinimumProductFamilyRulesBroken) return false;
                if (other.MetaNumberOfDependencyFamilyRulesBroken != this.MetaNumberOfDependencyFamilyRulesBroken) return false;
                if (other.MetaNumberOfPreserveProductRulesBroken != this.MetaNumberOfPreserveProductRulesBroken) return false;
                if (other.MetaNumberOfProductRulesBroken != this.MetaNumberOfProductRulesBroken) return false;
                if (other.MetaPercentageOfAssortmentRulesBroken != this.MetaPercentageOfAssortmentRulesBroken) return false;
                if (other.MetaPercentageOfCoreRulesBroken != this.MetaPercentageOfCoreRulesBroken) return false;
                if (other.MetaPercentageOfDistributionRulesBroken != this.MetaPercentageOfDistributionRulesBroken) return false;
                if (other.MetaPercentageOfFamilyRulesBroken != this.MetaPercentageOfFamilyRulesBroken) return false;
                if (other.MetaPercentageOfInheritanceRulesBroken != this.MetaPercentageOfInheritanceRulesBroken) return false;
                if (other.MetaPercentageOfLocalProductRulesBroken != this.MetaPercentageOfLocalProductRulesBroken) return false;
                if (other.MetaPercentageOfProductRulesBroken != this.MetaPercentageOfProductRulesBroken) return false;
                if (other.MetaPercentOfPlacedProductsRecommendedInAssortment != this.MetaPercentOfPlacedProductsRecommendedInAssortment) return false;
                if (other.MetaPercentOfPositionsOutsideOfBlockSpace != this.MetaPercentOfPositionsOutsideOfBlockSpace) return false;
                if (other.MetaPercentOfProductNotAchievedCases != this.MetaPercentOfProductNotAchievedCases) return false;
                if (other.MetaPercentOfProductNotAchievedDeliveries != this.MetaPercentOfProductNotAchievedDeliveries) return false;
                if (other.MetaPercentOfProductNotAchievedDOS != this.MetaPercentOfProductNotAchievedDOS) return false;
                if (other.MetaPercentOfProductNotAchievedInventory != this.MetaPercentOfProductNotAchievedInventory) return false;
                if (other.MetaPercentOfProductOverShelfLifePercent != this.MetaPercentOfProductOverShelfLifePercent) return false;
                if (other.MetaHasComponentsOutsideOfFixtureArea != this.MetaHasComponentsOutsideOfFixtureArea) return false;
                if (other.MetaCountOfProductsBuddied != this.MetaCountOfProductsBuddied) return false;

                if (other.Text1 != this.Text1) return false;
                if (other.Text2 != this.Text2) return false;
                if (other.Text3 != this.Text3) return false;
                if (other.Text4 != this.Text4) return false;
                if (other.Text5 != this.Text5) return false;
                if (other.Text6 != this.Text6) return false;
                if (other.Text7 != this.Text7) return false;
                if (other.Text8 != this.Text8) return false;
                if (other.Text9 != this.Text9) return false;
                if (other.Text10 != this.Text10) return false;
                if (other.Text11 != this.Text11) return false;
                if (other.Text12 != this.Text12) return false;
                if (other.Text13 != this.Text13) return false;
                if (other.Text14 != this.Text14) return false;
                if (other.Text15 != this.Text15) return false;
                if (other.Text16 != this.Text16) return false;
                if (other.Text17 != this.Text17) return false;
                if (other.Text18 != this.Text18) return false;
                if (other.Text19 != this.Text19) return false;
                if (other.Text20 != this.Text20) return false;
                if (other.Text21 != this.Text21) return false;
                if (other.Text22 != this.Text22) return false;
                if (other.Text23 != this.Text23) return false;
                if (other.Text24 != this.Text24) return false;
                if (other.Text25 != this.Text25) return false;
                if (other.Text26 != this.Text26) return false;
                if (other.Text27 != this.Text27) return false;
                if (other.Text28 != this.Text28) return false;
                if (other.Text29 != this.Text29) return false;
                if (other.Text30 != this.Text30) return false;
                if (other.Text31 != this.Text31) return false;
                if (other.Text32 != this.Text32) return false;
                if (other.Text33 != this.Text33) return false;
                if (other.Text34 != this.Text34) return false;
                if (other.Text35 != this.Text35) return false;
                if (other.Text36 != this.Text36) return false;
                if (other.Text37 != this.Text37) return false;
                if (other.Text38 != this.Text38) return false;
                if (other.Text39 != this.Text39) return false;
                if (other.Text40 != this.Text40) return false;
                if (other.Text41 != this.Text41) return false;
                if (other.Text42 != this.Text42) return false;
                if (other.Text43 != this.Text43) return false;
                if (other.Text44 != this.Text44) return false;
                if (other.Text45 != this.Text45) return false;
                if (other.Text46 != this.Text46) return false;
                if (other.Text47 != this.Text47) return false;
                if (other.Text48 != this.Text48) return false;
                if (other.Text49 != this.Text49) return false;
                if (other.Text50 != this.Text50) return false;
                if (other.Value1 != this.Value1) return false;
                if (other.Value2 != this.Value2) return false;
                if (other.Value3 != this.Value3) return false;
                if (other.Value4 != this.Value4) return false;
                if (other.Value5 != this.Value5) return false;
                if (other.Value6 != this.Value6) return false;
                if (other.Value7 != this.Value7) return false;
                if (other.Value8 != this.Value8) return false;
                if (other.Value9 != this.Value9) return false;
                if (other.Value10 != this.Value10) return false;
                if (other.Value11 != this.Value11) return false;
                if (other.Value12 != this.Value12) return false;
                if (other.Value13 != this.Value13) return false;
                if (other.Value14 != this.Value14) return false;
                if (other.Value15 != this.Value15) return false;
                if (other.Value16 != this.Value16) return false;
                if (other.Value17 != this.Value17) return false;
                if (other.Value18 != this.Value18) return false;
                if (other.Value19 != this.Value19) return false;
                if (other.Value20 != this.Value20) return false;
                if (other.Value21 != this.Value21) return false;
                if (other.Value22 != this.Value22) return false;
                if (other.Value23 != this.Value23) return false;
                if (other.Value24 != this.Value24) return false;
                if (other.Value25 != this.Value25) return false;
                if (other.Value26 != this.Value26) return false;
                if (other.Value27 != this.Value27) return false;
                if (other.Value28 != this.Value28) return false;
                if (other.Value29 != this.Value29) return false;
                if (other.Value30 != this.Value30) return false;
                if (other.Value31 != this.Value31) return false;
                if (other.Value32 != this.Value32) return false;
                if (other.Value33 != this.Value33) return false;
                if (other.Value34 != this.Value34) return false;
                if (other.Value35 != this.Value35) return false;
                if (other.Value36 != this.Value36) return false;
                if (other.Value37 != this.Value37) return false;
                if (other.Value38 != this.Value38) return false;
                if (other.Value39 != this.Value39) return false;
                if (other.Value40 != this.Value40) return false;
                if (other.Value41 != this.Value41) return false;
                if (other.Value42 != this.Value42) return false;
                if (other.Value43 != this.Value43) return false;
                if (other.Value44 != this.Value44) return false;
                if (other.Value45 != this.Value45) return false;
                if (other.Value46 != this.Value46) return false;
                if (other.Value47 != this.Value47) return false;
                if (other.Value48 != this.Value48) return false;
                if (other.Value49 != this.Value49) return false;
                if (other.Value50 != this.Value50) return false;
                if (other.Flag1 != this.Flag1) return false;
                if (other.Flag2 != this.Flag2) return false;
                if (other.Flag3 != this.Flag3) return false;
                if (other.Flag4 != this.Flag4) return false;
                if (other.Flag5 != this.Flag5) return false;
                if (other.Flag6 != this.Flag6) return false;
                if (other.Flag7 != this.Flag7) return false;
                if (other.Flag8 != this.Flag8) return false;
                if (other.Flag9 != this.Flag9) return false;
                if (other.Flag10 != this.Flag10) return false;
                if (other.Date1 != this.Date1) return false;
                if (other.Date2 != this.Date2) return false;
                if (other.Date3 != this.Date3) return false;
                if (other.Date4 != this.Date4) return false;
                if (other.Date5 != this.Date5) return false;
                if (other.Date6 != this.Date6) return false;
                if (other.Date7 != this.Date7) return false;
                if (other.Date8 != this.Date8) return false;
                if (other.Date9 != this.Date9) return false;
                if (other.Date10 != this.Date10) return false;
                if (other.Note1 != this.Note1) return false;
                if (other.Note2 != this.Note2) return false;
                if (other.Note3 != this.Note3) return false;
                if (other.Note4 != this.Note4) return false;
                if (other.Note5 != this.Note5) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}