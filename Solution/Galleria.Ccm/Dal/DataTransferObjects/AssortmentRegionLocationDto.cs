﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)

// CCM-25454 : J.Pickup
//		Created (Auto-generated)
// V8-26704 : A.Kuszyk
//  Added LocationCode and ForeignKey/InheritedProperty attributes.
// V8-26322 : A.Silva
//      Amended LocationId Property to have DeleteBehavior.Leave (parent is marked as deleted, not really deleted).

#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// AssortmentRegionLocation Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class AssortmentRegionLocationDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public AssortmentRegionLocationDtoKey DtoKey
        {
            get
            {
                return new AssortmentRegionLocationDtoKey()
                {
                    LocationId = this.LocationId
                };
            }
        }
        [ForeignKey(typeof(LocationDto),typeof(ILocationDal), DeleteBehavior.Leave)]
        public Int16 LocationId { get; set; }
        [ForeignKey(typeof(AssortmentRegionDto), typeof(IAssortmentRegionDal))]
        public Int32 AssortmentRegionId { get; set; }
        [InheritedProperty(typeof(LocationDto),typeof(ILocationDal),ForeignKeyPropertyName="LocationId",ParentPropertyName="Code")]
        public String LocationCode { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentRegionLocationDto other = obj as AssortmentRegionLocationDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.LocationId != this.LocationId) { return false; }
                if (other.LocationCode != this.LocationCode) { return false; }
                if (other.AssortmentRegionId != this.AssortmentRegionId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentRegionLocationDtoKey
    {
        #region Properties
        public Int32 LocationId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                LocationId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentRegionLocationDtoKey other = obj as AssortmentRegionLocationDtoKey;
            if (other != null)
            {
                if (other.LocationId != this.LocationId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentRegionLocationIsSetDto
    {
        #region Properties
        public Boolean IsLocationIdSet { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public AssortmentRegionLocationIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public AssortmentRegionLocationIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsLocationIdSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentRegionLocationIsSetDto other = obj as AssortmentRegionLocationIsSetDto;
            if (other != null)
            {
                if (other.IsLocationIdSet != this.IsLocationIdSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsLocationIdSet = isSet;
        }
        #endregion
    }
}
