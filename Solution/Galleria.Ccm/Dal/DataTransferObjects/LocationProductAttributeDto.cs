﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
// V8-26041 : A.Kuszyk ~ Amended delete behaviour on Product foreign key attribute.
// V8-27630 : I.George ~ Removed EntityId as part of DtoKey

#endregion
#region Version History: (CCM 8.03)
//V8-29267 : L.Ineson
//  Removed date deleted.
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class LocationProductAttributeDto
    {
        #region Properties
        [ForeignKey(typeof(ProductDto), typeof(IProductDal), DeleteBehavior.Leave)]
        public Int32 ProductId { get; set; }
        [ForeignKey(typeof(LocationDto), typeof(ILocationDal), DeleteBehavior.Leave)]
        public Int16 LocationId { get; set; }
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        public RowVersion RowVersion { get; set; }
        public LocationProductAttributeDtoKey DtoKey
        {
            get
            {
                return new LocationProductAttributeDtoKey()
                { 
                    ProductId = this.ProductId,
                    LocationId = this.LocationId,
                };
            }
        }
        public String GTIN { get; set; }
        public String Description { get; set; }
        public Single? Height { get; set; }
        public Single? Width { get; set; }
        public Single? Depth { get; set; }
        public Int16? CasePackUnits { get; set; }
        public Byte? CaseHigh { get; set; }
        public Byte? CaseWide { get; set; }
        public Byte? CaseDeep { get; set; }
        public Single? CaseHeight { get; set; }
        public Single? CaseWidth { get; set; }
        public Single? CaseDepth { get; set; }
        public Byte? DaysOfSupply { get; set; }
        public Byte? MinDeep { get; set; }
        public Byte? MaxDeep { get; set; }
        public Int16? TrayPackUnits { get; set; }
        public Byte? TrayHigh { get; set; }
        public Byte? TrayWide { get; set; }
        public Byte? TrayDeep { get; set; }
        public Single? TrayHeight { get; set; }
        public Single? TrayWidth { get; set; }
        public Single? TrayDepth { get; set; }
        public Single? TrayThickHeight { get; set; }
        public Single? TrayThickDepth { get; set; }
        public Single? TrayThickWidth { get; set; }
        public Byte? StatusType { get; set; }
        public Int16? ShelfLife { get; set; }
        public Single? DeliveryFrequencyDays { get; set; }
        public String DeliveryMethod { get; set; }
        public String VendorCode { get; set; }
        public String Vendor { get; set; }
        public String ManufacturerCode { get; set; }
        public String Manufacturer { get; set; }
        public String Size { get; set; }
        public String UnitOfMeasure { get; set; }
        public Single? SellPrice { get; set; }
        public Int16? SellPackCount { get; set; }
        public String SellPackDescription { get; set; }
        public Single? RecommendedRetailPrice { get; set; }
        public Single? CostPrice { get; set; }
        public Single? CaseCost { get; set; }
        public String ConsumerInformation { get; set; }
        public String Pattern { get; set; }
        public String Model { get; set; }
        public String CorporateCode { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }

        //#region Custom Attributes
        //public Object CustomAttribute01 { get; set; }
        //public Object CustomAttribute02 { get; set; }
        //public Object CustomAttribute03 { get; set; }
        //public Object CustomAttribute04 { get; set; }
        //public Object CustomAttribute05 { get; set; }
        //public Object CustomAttribute06 { get; set; }
        //public Object CustomAttribute07 { get; set; }
        //public Object CustomAttribute08 { get; set; }
        //public Object CustomAttribute09 { get; set; }
        //public Object CustomAttribute10 { get; set; }
        //public Object CustomAttribute11 { get; set; }
        //public Object CustomAttribute12 { get; set; }
        //public Object CustomAttribute13 { get; set; }
        //public Object CustomAttribute14 { get; set; }
        //public Object CustomAttribute15 { get; set; }
        //public Object CustomAttribute16 { get; set; }
        //public Object CustomAttribute17 { get; set; }
        //public Object CustomAttribute18 { get; set; }
        //public Object CustomAttribute19 { get; set; }
        //public Object CustomAttribute20 { get; set; }
        //public Object CustomAttribute21 { get; set; }
        //public Object CustomAttribute22 { get; set; }
        //public Object CustomAttribute23 { get; set; }
        //public Object CustomAttribute24 { get; set; }
        //public Object CustomAttribute25 { get; set; }
        //public Object CustomAttribute26 { get; set; }
        //public Object CustomAttribute27 { get; set; }
        //public Object CustomAttribute28 { get; set; }
        //public Object CustomAttribute29 { get; set; }
        //public Object CustomAttribute30 { get; set; }
        //public Object CustomAttribute31 { get; set; }
        //public Object CustomAttribute32 { get; set; }
        //public Object CustomAttribute33 { get; set; }
        //public Object CustomAttribute34 { get; set; }
        //public Object CustomAttribute35 { get; set; }
        //public Object CustomAttribute36 { get; set; }
        //public Object CustomAttribute37 { get; set; }
        //public Object CustomAttribute38 { get; set; }
        //public Object CustomAttribute39 { get; set; }
        //public Object CustomAttribute40 { get; set; }
        //public Object CustomAttribute41 { get; set; }
        //public Object CustomAttribute42 { get; set; }
        //public Object CustomAttribute43 { get; set; }
        //public Object CustomAttribute44 { get; set; }
        //public Object CustomAttribute45 { get; set; }
        //public Object CustomAttribute46 { get; set; }
        //public Object CustomAttribute47 { get; set; }
        //public Object CustomAttribute48 { get; set; }
        //public Object CustomAttribute49 { get; set; }
        //public Object CustomAttribute50 { get; set; }
        //#endregion
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The Object to compare against</param>
        /// <returnstrue if Objects are equal</returns>
        public override bool Equals(Object obj)
        {
            LocationProductAttributeDto other = obj as LocationProductAttributeDto;
            if (other != null)
            {
                if (other.ProductId != this.ProductId) { return false; }
                if (other.LocationId != this.LocationId) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.GTIN != this.GTIN) { return false; }
                if (other.Description != this.Description) { return false; }
                if (other.Height != this.Height) { return false; }
                if (other.Width != this.Width) { return false; }
                if (other.Depth != this.Depth) { return false; }
                if (other.CasePackUnits != this.CasePackUnits) { return false; }
                if (other.CaseHigh != this.CaseHigh) { return false; }
                if (other.CaseWide != this.CaseWide) { return false; }
                if (other.CaseDeep != this.CaseDeep) { return false; }
                if (other.CaseHeight != this.CaseHeight) { return false; }
                if (other.CaseWidth != this.CaseWidth) { return false; }
                if (other.CaseDepth != this.CaseDepth) { return false; }
                if (other.DaysOfSupply != this.DaysOfSupply) { return false; }
                if (other.MinDeep != this.MinDeep) { return false; }
                if (other.MaxDeep != this.MaxDeep) { return false; }
                if (other.TrayPackUnits != this.TrayPackUnits) { return false; }
                if (other.TrayHigh != this.TrayHigh) { return false; }
                if (other.TrayWide != this.TrayWide) { return false; }
                if (other.TrayDeep != this.TrayDeep) { return false; }
                if (other.TrayHeight != this.TrayHeight) { return false; }
                if (other.TrayWidth != this.TrayWidth) { return false; }
                if (other.TrayDepth != this.TrayDepth) { return false; }
                if (other.TrayThickHeight != this.TrayThickHeight) { return false; }
                if (other.TrayThickDepth != this.TrayThickDepth) { return false; }
                if (other.TrayThickWidth != this.TrayThickWidth) { return false; }
                if (other.StatusType != this.StatusType) { return false; }
                if (other.ShelfLife != this.ShelfLife) { return false; }
                if (other.DeliveryFrequencyDays != this.DeliveryFrequencyDays) { return false; }
                if (other.DeliveryMethod != this.DeliveryMethod) { return false; }
                if (other.VendorCode != this.VendorCode) { return false; }
                if (other.Vendor != this.Vendor) { return false; }
                if (other.ManufacturerCode != this.ManufacturerCode) { return false; }
                if (other.Manufacturer != this.Manufacturer) { return false; }
                if (other.Size != this.Size) { return false; }
                if (other.UnitOfMeasure != this.UnitOfMeasure) { return false; }
                if (other.SellPrice != this.SellPrice) { return false; }
                if (other.SellPackCount != this.SellPackCount) { return false; }
                if (other.SellPackDescription != this.SellPackDescription) { return false; }
                if (other.RecommendedRetailPrice != this.RecommendedRetailPrice) { return false; }
                if (other.CostPrice != this.CostPrice) { return false; }
                if (other.CaseCost != this.CaseCost) { return false; }
                if (other.ConsumerInformation != this.ConsumerInformation) { return false; }
                if (other.Pattern != this.Pattern) { return false; }
                if (other.Model != this.Model) { return false; }
                if (other.CorporateCode != this.CorporateCode) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }

                //#region Custom Attributes
                //if (((other.CustomAttribute01 == null) && (this.CustomAttribute01 != null)) || ((other.CustomAttribute01 != null) && (!other.CustomAttribute01.Equals(this.CustomAttribute01)))) return false;
                //if (((other.CustomAttribute02 == null) && (this.CustomAttribute02 != null)) || ((other.CustomAttribute02 != null) && (!other.CustomAttribute02.Equals(this.CustomAttribute02)))) return false;
                //if (((other.CustomAttribute03 == null) && (this.CustomAttribute03 != null)) || ((other.CustomAttribute03 != null) && (!other.CustomAttribute03.Equals(this.CustomAttribute03)))) return false;
                //if (((other.CustomAttribute04 == null) && (this.CustomAttribute04 != null)) || ((other.CustomAttribute04 != null) && (!other.CustomAttribute04.Equals(this.CustomAttribute04)))) return false;
                //if (((other.CustomAttribute05 == null) && (this.CustomAttribute05 != null)) || ((other.CustomAttribute05 != null) && (!other.CustomAttribute05.Equals(this.CustomAttribute05)))) return false;
                //if (((other.CustomAttribute06 == null) && (this.CustomAttribute06 != null)) || ((other.CustomAttribute06 != null) && (!other.CustomAttribute06.Equals(this.CustomAttribute06)))) return false;
                //if (((other.CustomAttribute07 == null) && (this.CustomAttribute07 != null)) || ((other.CustomAttribute07 != null) && (!other.CustomAttribute07.Equals(this.CustomAttribute07)))) return false;
                //if (((other.CustomAttribute08 == null) && (this.CustomAttribute08 != null)) || ((other.CustomAttribute08 != null) && (!other.CustomAttribute08.Equals(this.CustomAttribute08)))) return false;
                //if (((other.CustomAttribute09 == null) && (this.CustomAttribute09 != null)) || ((other.CustomAttribute09 != null) && (!other.CustomAttribute09.Equals(this.CustomAttribute09)))) return false;
                //if (((other.CustomAttribute10 == null) && (this.CustomAttribute10 != null)) || ((other.CustomAttribute10 != null) && (!other.CustomAttribute10.Equals(this.CustomAttribute10)))) return false;
                //if (((other.CustomAttribute11 == null) && (this.CustomAttribute11 != null)) || ((other.CustomAttribute11 != null) && (!other.CustomAttribute11.Equals(this.CustomAttribute11)))) return false;
                //if (((other.CustomAttribute12 == null) && (this.CustomAttribute12 != null)) || ((other.CustomAttribute12 != null) && (!other.CustomAttribute12.Equals(this.CustomAttribute12)))) return false;
                //if (((other.CustomAttribute13 == null) && (this.CustomAttribute13 != null)) || ((other.CustomAttribute13 != null) && (!other.CustomAttribute13.Equals(this.CustomAttribute13)))) return false;
                //if (((other.CustomAttribute14 == null) && (this.CustomAttribute14 != null)) || ((other.CustomAttribute14 != null) && (!other.CustomAttribute14.Equals(this.CustomAttribute14)))) return false;
                //if (((other.CustomAttribute15 == null) && (this.CustomAttribute15 != null)) || ((other.CustomAttribute15 != null) && (!other.CustomAttribute15.Equals(this.CustomAttribute15)))) return false;
                //if (((other.CustomAttribute16 == null) && (this.CustomAttribute16 != null)) || ((other.CustomAttribute16 != null) && (!other.CustomAttribute16.Equals(this.CustomAttribute16)))) return false;
                //if (((other.CustomAttribute17 == null) && (this.CustomAttribute17 != null)) || ((other.CustomAttribute17 != null) && (!other.CustomAttribute17.Equals(this.CustomAttribute17)))) return false;
                //if (((other.CustomAttribute18 == null) && (this.CustomAttribute18 != null)) || ((other.CustomAttribute18 != null) && (!other.CustomAttribute18.Equals(this.CustomAttribute18)))) return false;
                //if (((other.CustomAttribute19 == null) && (this.CustomAttribute19 != null)) || ((other.CustomAttribute19 != null) && (!other.CustomAttribute19.Equals(this.CustomAttribute19)))) return false;
                //if (((other.CustomAttribute20 == null) && (this.CustomAttribute20 != null)) || ((other.CustomAttribute20 != null) && (!other.CustomAttribute20.Equals(this.CustomAttribute20)))) return false;
                //if (((other.CustomAttribute21 == null) && (this.CustomAttribute21 != null)) || ((other.CustomAttribute21 != null) && (!other.CustomAttribute21.Equals(this.CustomAttribute21)))) return false;
                //if (((other.CustomAttribute22 == null) && (this.CustomAttribute22 != null)) || ((other.CustomAttribute22 != null) && (!other.CustomAttribute22.Equals(this.CustomAttribute22)))) return false;
                //if (((other.CustomAttribute23 == null) && (this.CustomAttribute23 != null)) || ((other.CustomAttribute23 != null) && (!other.CustomAttribute23.Equals(this.CustomAttribute23)))) return false;
                //if (((other.CustomAttribute24 == null) && (this.CustomAttribute24 != null)) || ((other.CustomAttribute24 != null) && (!other.CustomAttribute24.Equals(this.CustomAttribute24)))) return false;
                //if (((other.CustomAttribute25 == null) && (this.CustomAttribute25 != null)) || ((other.CustomAttribute25 != null) && (!other.CustomAttribute25.Equals(this.CustomAttribute25)))) return false;
                //if (((other.CustomAttribute26 == null) && (this.CustomAttribute26 != null)) || ((other.CustomAttribute26 != null) && (!other.CustomAttribute26.Equals(this.CustomAttribute26)))) return false;
                //if (((other.CustomAttribute27 == null) && (this.CustomAttribute27 != null)) || ((other.CustomAttribute27 != null) && (!other.CustomAttribute27.Equals(this.CustomAttribute27)))) return false;
                //if (((other.CustomAttribute28 == null) && (this.CustomAttribute28 != null)) || ((other.CustomAttribute28 != null) && (!other.CustomAttribute28.Equals(this.CustomAttribute28)))) return false;
                //if (((other.CustomAttribute29 == null) && (this.CustomAttribute29 != null)) || ((other.CustomAttribute29 != null) && (!other.CustomAttribute29.Equals(this.CustomAttribute29)))) return false;
                //if (((other.CustomAttribute30 == null) && (this.CustomAttribute30 != null)) || ((other.CustomAttribute30 != null) && (!other.CustomAttribute30.Equals(this.CustomAttribute30)))) return false;
                //if (((other.CustomAttribute31 == null) && (this.CustomAttribute31 != null)) || ((other.CustomAttribute31 != null) && (!other.CustomAttribute31.Equals(this.CustomAttribute31)))) return false;
                //if (((other.CustomAttribute32 == null) && (this.CustomAttribute32 != null)) || ((other.CustomAttribute32 != null) && (!other.CustomAttribute32.Equals(this.CustomAttribute32)))) return false;
                //if (((other.CustomAttribute33 == null) && (this.CustomAttribute33 != null)) || ((other.CustomAttribute33 != null) && (!other.CustomAttribute33.Equals(this.CustomAttribute33)))) return false;
                //if (((other.CustomAttribute34 == null) && (this.CustomAttribute34 != null)) || ((other.CustomAttribute34 != null) && (!other.CustomAttribute34.Equals(this.CustomAttribute34)))) return false;
                //if (((other.CustomAttribute35 == null) && (this.CustomAttribute35 != null)) || ((other.CustomAttribute35 != null) && (!other.CustomAttribute35.Equals(this.CustomAttribute35)))) return false;
                //if (((other.CustomAttribute36 == null) && (this.CustomAttribute36 != null)) || ((other.CustomAttribute36 != null) && (!other.CustomAttribute36.Equals(this.CustomAttribute36)))) return false;
                //if (((other.CustomAttribute37 == null) && (this.CustomAttribute37 != null)) || ((other.CustomAttribute37 != null) && (!other.CustomAttribute37.Equals(this.CustomAttribute37)))) return false;
                //if (((other.CustomAttribute38 == null) && (this.CustomAttribute38 != null)) || ((other.CustomAttribute38 != null) && (!other.CustomAttribute38.Equals(this.CustomAttribute38)))) return false;
                //if (((other.CustomAttribute39 == null) && (this.CustomAttribute39 != null)) || ((other.CustomAttribute39 != null) && (!other.CustomAttribute39.Equals(this.CustomAttribute39)))) return false;
                //if (((other.CustomAttribute40 == null) && (this.CustomAttribute40 != null)) || ((other.CustomAttribute40 != null) && (!other.CustomAttribute40.Equals(this.CustomAttribute40)))) return false;
                //if (((other.CustomAttribute41 == null) && (this.CustomAttribute41 != null)) || ((other.CustomAttribute41 != null) && (!other.CustomAttribute41.Equals(this.CustomAttribute41)))) return false;
                //if (((other.CustomAttribute42 == null) && (this.CustomAttribute42 != null)) || ((other.CustomAttribute42 != null) && (!other.CustomAttribute42.Equals(this.CustomAttribute42)))) return false;
                //if (((other.CustomAttribute43 == null) && (this.CustomAttribute43 != null)) || ((other.CustomAttribute43 != null) && (!other.CustomAttribute43.Equals(this.CustomAttribute43)))) return false;
                //if (((other.CustomAttribute44 == null) && (this.CustomAttribute44 != null)) || ((other.CustomAttribute44 != null) && (!other.CustomAttribute44.Equals(this.CustomAttribute44)))) return false;
                //if (((other.CustomAttribute45 == null) && (this.CustomAttribute45 != null)) || ((other.CustomAttribute45 != null) && (!other.CustomAttribute45.Equals(this.CustomAttribute45)))) return false;
                //if (((other.CustomAttribute46 == null) && (this.CustomAttribute46 != null)) || ((other.CustomAttribute46 != null) && (!other.CustomAttribute46.Equals(this.CustomAttribute46)))) return false;
                //if (((other.CustomAttribute47 == null) && (this.CustomAttribute47 != null)) || ((other.CustomAttribute47 != null) && (!other.CustomAttribute47.Equals(this.CustomAttribute47)))) return false;
                //if (((other.CustomAttribute48 == null) && (this.CustomAttribute48 != null)) || ((other.CustomAttribute48 != null) && (!other.CustomAttribute48.Equals(this.CustomAttribute48)))) return false;
                //if (((other.CustomAttribute49 == null) && (this.CustomAttribute49 != null)) || ((other.CustomAttribute49 != null) && (!other.CustomAttribute49.Equals(this.CustomAttribute49)))) return false;
                //if (((other.CustomAttribute50 == null) && (this.CustomAttribute50 != null)) || ((other.CustomAttribute50 != null) && (!other.CustomAttribute50.Equals(this.CustomAttribute50)))) return false;
                //#endregion
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


    [Serializable]
    public class LocationProductAttributeIsSetDto
    {
        #region Properties
        public Boolean IsProductIdSet { get; set; }
        public Boolean IsLocationIdSet { get; set; }
        public Boolean IsGTINSet { get; set; }
        public Boolean IsDescriptionSet { get; set; }
        public Boolean IsHeightSet { get; set; }
        public Boolean IsWidthSet { get; set; }
        public Boolean IsDepthSet { get; set; }
        public Boolean IsCasePackUnitsSet { get; set; }
        public Boolean IsCaseHighSet { get; set; }
        public Boolean IsCaseWideSet { get; set; }
        public Boolean IsCaseDeepSet { get; set; }
        public Boolean IsCaseHeightSet { get; set; }
        public Boolean IsCaseWidthSet { get; set; }
        public Boolean IsCaseDepthSet { get; set; }
        public Boolean IsDaysOfSupplySet { get; set; }
        public Boolean IsMinDeepSet { get; set; }
        public Boolean IsMaxDeepSet { get; set; }
        public Boolean IsTrayPackUnitsSet { get; set; }
        public Boolean IsTrayHighSet { get; set; }
        public Boolean IsTrayWideSet { get; set; }
        public Boolean IsTrayDeepSet { get; set; }
        public Boolean IsTrayHeightSet { get; set; }
        public Boolean IsTrayWidthSet { get; set; }
        public Boolean IsTrayDepthSet { get; set; }
        public Boolean IsTrayThickHeightSet { get; set; }
        public Boolean IsTrayThickDepthSet { get; set; }
        public Boolean IsTrayThickWidthSet { get; set; }
        public Boolean IsStatusTypeSet { get; set; }
        public Boolean IsShelfLifeSet { get; set; }
        public Boolean IsDeliveryFrequencyDaysSet { get; set; }
        public Boolean IsDeliveryMethodSet { get; set; }
        public Boolean IsVendorCodeSet { get; set; }
        public Boolean IsVendorSet { get; set; }
        public Boolean IsManufacturerCodeSet { get; set; }
        public Boolean IsManufacturerSet { get; set; }
        public Boolean IsSizeSet { get; set; }
        public Boolean IsUnitOfMeasureSet { get; set; }
        public Boolean IsSellPriceSet { get; set; }
        public Boolean IsSellPackCountSet { get; set; }
        public Boolean IsSellPackDescriptionSet { get; set; }
        public Boolean IsRecommendedRetailPriceSet { get; set; }
        public Boolean IsCostPriceSet { get; set; }
        public Boolean IsCaseCostSet { get; set; }
        public Boolean IsConsumerInformationSet { get; set; }
        public Boolean IsPatternSet { get; set; }
        public Boolean IsModelSet { get; set; }
        public Boolean IsCorporateCodeSet { get; set; }

        //#region Custom Attributes
        //public Boolean IsCustomAttribute01Set { get; set; }
        //public Boolean IsCustomAttribute02Set { get; set; }
        //public Boolean IsCustomAttribute03Set { get; set; }
        //public Boolean IsCustomAttribute04Set { get; set; }
        //public Boolean IsCustomAttribute05Set { get; set; }
        //public Boolean IsCustomAttribute06Set { get; set; }
        //public Boolean IsCustomAttribute07Set { get; set; }
        //public Boolean IsCustomAttribute08Set { get; set; }
        //public Boolean IsCustomAttribute09Set { get; set; }
        //public Boolean IsCustomAttribute10Set { get; set; }
        //public Boolean IsCustomAttribute11Set { get; set; }
        //public Boolean IsCustomAttribute12Set { get; set; }
        //public Boolean IsCustomAttribute13Set { get; set; }
        //public Boolean IsCustomAttribute14Set { get; set; }
        //public Boolean IsCustomAttribute15Set { get; set; }
        //public Boolean IsCustomAttribute16Set { get; set; }
        //public Boolean IsCustomAttribute17Set { get; set; }
        //public Boolean IsCustomAttribute18Set { get; set; }
        //public Boolean IsCustomAttribute19Set { get; set; }
        //public Boolean IsCustomAttribute20Set { get; set; }
        //public Boolean IsCustomAttribute21Set { get; set; }
        //public Boolean IsCustomAttribute22Set { get; set; }
        //public Boolean IsCustomAttribute23Set { get; set; }
        //public Boolean IsCustomAttribute24Set { get; set; }
        //public Boolean IsCustomAttribute25Set { get; set; }
        //public Boolean IsCustomAttribute26Set { get; set; }
        //public Boolean IsCustomAttribute27Set { get; set; }
        //public Boolean IsCustomAttribute28Set { get; set; }
        //public Boolean IsCustomAttribute29Set { get; set; }
        //public Boolean IsCustomAttribute30Set { get; set; }
        //public Boolean IsCustomAttribute31Set { get; set; }
        //public Boolean IsCustomAttribute32Set { get; set; }
        //public Boolean IsCustomAttribute33Set { get; set; }
        //public Boolean IsCustomAttribute34Set { get; set; }
        //public Boolean IsCustomAttribute35Set { get; set; }
        //public Boolean IsCustomAttribute36Set { get; set; }
        //public Boolean IsCustomAttribute37Set { get; set; }
        //public Boolean IsCustomAttribute38Set { get; set; }
        //public Boolean IsCustomAttribute39Set { get; set; }
        //public Boolean IsCustomAttribute40Set { get; set; }
        //public Boolean IsCustomAttribute41Set { get; set; }
        //public Boolean IsCustomAttribute42Set { get; set; }
        //public Boolean IsCustomAttribute43Set { get; set; }
        //public Boolean IsCustomAttribute44Set { get; set; }
        //public Boolean IsCustomAttribute45Set { get; set; }
        //public Boolean IsCustomAttribute46Set { get; set; }
        //public Boolean IsCustomAttribute47Set { get; set; }
        //public Boolean IsCustomAttribute48Set { get; set; }
        //public Boolean IsCustomAttribute49Set { get; set; }
        //public Boolean IsCustomAttribute50Set { get; set; }
        //#endregion
        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public LocationProductAttributeIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public LocationProductAttributeIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods

        public override bool Equals(Object obj)
        {
            LocationProductAttributeIsSetDto other = obj as LocationProductAttributeIsSetDto;
            if (other != null)
            {
                if (other.IsCaseCostSet != this.IsCaseCostSet)
                {
                    return false;
                }
                if (other.IsCaseDeepSet != this.IsCaseDeepSet)
                {
                    return false;
                }
                if (other.IsCaseDepthSet != this.IsCaseDepthSet)
                {
                    return false;
                }
                if (other.IsCaseHeightSet != this.IsCaseHeightSet)
                {
                    return false;
                }
                if (other.IsCaseHighSet != this.IsCaseHighSet)
                {
                    return false;
                }
                if (other.IsCasePackUnitsSet != this.IsCasePackUnitsSet)
                {
                    return false;
                }
                if (other.IsCaseWideSet != this.IsCaseWideSet)
                {
                    return false;
                }
                if (other.IsCaseWidthSet != this.IsCaseWidthSet)
                {
                    return false;
                }
                if (other.IsConsumerInformationSet != this.IsConsumerInformationSet)
                {
                    return false;
                }
                if (other.IsCostPriceSet != this.IsCostPriceSet)
                {
                    return false;
                }
                if (other.IsDaysOfSupplySet != this.IsDaysOfSupplySet)
                {
                    return false;
                }
                if (other.IsDeliveryFrequencyDaysSet != this.IsDeliveryFrequencyDaysSet)
                {
                    return false;
                }
                if (other.IsDeliveryMethodSet != this.IsDeliveryMethodSet)
                {
                    return false;
                }
                if (other.IsDepthSet != this.IsDepthSet)
                {
                    return false;
                }
                if (other.IsDescriptionSet != this.IsDescriptionSet)
                {
                    return false;
                }
                if (other.IsGTINSet != this.IsGTINSet)
                {
                    return false;
                }
                if (other.IsHeightSet != this.IsHeightSet)
                {
                    return false;
                }
                if (other.IsLocationIdSet != this.IsLocationIdSet)
                {
                    return false;
                }
                if (other.IsManufacturerCodeSet != this.IsManufacturerCodeSet)
                {
                    return false;
                }
                if (other.IsManufacturerSet != this.IsManufacturerSet)
                {
                    return false;
                }
                if (other.IsMaxDeepSet != this.IsMaxDeepSet)
                {
                    return false;
                }
                if (other.IsMinDeepSet != this.IsMinDeepSet)
                {
                    return false;
                }
                if (other.IsModelSet != this.IsModelSet)
                {
                    return false;
                }
                if (other.IsPatternSet != this.IsPatternSet)
                {
                    return false;
                }
                if (other.IsProductIdSet != this.IsProductIdSet)
                {
                    return false;
                }
                if (other.IsRecommendedRetailPriceSet != this.IsRecommendedRetailPriceSet)
                {
                    return false;
                }
                if (other.IsSellPackCountSet != this.IsSellPackCountSet)
                {
                    return false;
                }
                if (other.IsSellPackDescriptionSet != this.IsSellPackDescriptionSet)
                {
                    return false;
                }
                if (other.IsSellPriceSet != this.IsSellPriceSet)
                {
                    return false;
                }
                if (other.IsShelfLifeSet != this.IsShelfLifeSet)
                {
                    return false;
                }
                if (other.IsSizeSet != this.IsSizeSet)
                {
                    return false;
                }
                if (other.IsStatusTypeSet != this.IsStatusTypeSet)
                {
                    return false;
                }
                if (other.IsTrayDeepSet != this.IsTrayDeepSet)
                {
                    return false;
                }
                if (other.IsTrayDepthSet != this.IsTrayDepthSet)
                {
                    return false;
                }
                if (other.IsTrayHeightSet != this.IsTrayHeightSet)
                {
                    return false;
                }
                if (other.IsTrayHighSet != this.IsTrayHighSet)
                {
                    return false;
                }
                if (other.IsTrayPackUnitsSet != this.IsTrayPackUnitsSet)
                {
                    return false;
                }
                if (other.IsTrayThickDepthSet != this.IsTrayThickDepthSet)
                {
                    return false;
                }
                if (other.IsTrayThickHeightSet != this.IsTrayThickHeightSet)
                {
                    return false;
                }
                if (other.IsTrayThickWidthSet != this.IsTrayThickWidthSet)
                {
                    return false;
                }
                if (other.IsTrayWideSet != this.IsTrayWideSet)
                {
                    return false;
                }
                if (other.IsTrayWidthSet != this.IsTrayWidthSet)
                {
                    return false;
                }
                if (other.IsUnitOfMeasureSet != this.IsUnitOfMeasureSet)
                {
                    return false;
                }
                if (other.IsVendorCodeSet != this.IsVendorCodeSet)
                {
                    return false;
                }
                if (other.IsVendorSet != this.IsVendorSet)
                {
                    return false;
                }
                if (other.IsWidthSet != this.IsWidthSet)
                {
                    return false;
                }
                if (other.IsCorporateCodeSet != this.IsCorporateCodeSet)
                {
                    return false;
                }

                // custom attributes
                //if (other.IsCustomAttribute01Set != this.IsCustomAttribute01Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute02Set != this.IsCustomAttribute02Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute03Set != this.IsCustomAttribute03Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute04Set != this.IsCustomAttribute04Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute05Set != this.IsCustomAttribute05Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute06Set != this.IsCustomAttribute06Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute07Set != this.IsCustomAttribute07Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute08Set != this.IsCustomAttribute08Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute09Set != this.IsCustomAttribute09Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute10Set != this.IsCustomAttribute10Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute11Set != this.IsCustomAttribute11Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute12Set != this.IsCustomAttribute12Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute13Set != this.IsCustomAttribute13Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute14Set != this.IsCustomAttribute14Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute15Set != this.IsCustomAttribute15Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute16Set != this.IsCustomAttribute16Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute17Set != this.IsCustomAttribute17Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute18Set != this.IsCustomAttribute18Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute19Set != this.IsCustomAttribute19Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute20Set != this.IsCustomAttribute20Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute21Set != this.IsCustomAttribute21Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute22Set != this.IsCustomAttribute22Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute23Set != this.IsCustomAttribute23Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute24Set != this.IsCustomAttribute24Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute25Set != this.IsCustomAttribute25Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute26Set != this.IsCustomAttribute26Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute27Set != this.IsCustomAttribute27Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute28Set != this.IsCustomAttribute28Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute29Set != this.IsCustomAttribute29Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute30Set != this.IsCustomAttribute30Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute31Set != this.IsCustomAttribute31Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute32Set != this.IsCustomAttribute32Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute33Set != this.IsCustomAttribute33Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute34Set != this.IsCustomAttribute34Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute35Set != this.IsCustomAttribute35Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute36Set != this.IsCustomAttribute36Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute37Set != this.IsCustomAttribute37Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute38Set != this.IsCustomAttribute38Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute39Set != this.IsCustomAttribute39Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute40Set != this.IsCustomAttribute40Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute41Set != this.IsCustomAttribute41Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute42Set != this.IsCustomAttribute42Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute43Set != this.IsCustomAttribute43Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute44Set != this.IsCustomAttribute44Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute45Set != this.IsCustomAttribute45Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute46Set != this.IsCustomAttribute46Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute47Set != this.IsCustomAttribute47Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute48Set != this.IsCustomAttribute48Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute49Set != this.IsCustomAttribute49Set)
                //{
                //    return false;
                //}
                //if (other.IsCustomAttribute50Set != this.IsCustomAttribute50Set)
                //{
                //    return false;
                //}

            }
            else
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            this.IsCaseCostSet = isSet;
            this.IsCaseDeepSet = isSet;
            this.IsCaseDepthSet = isSet;
            this.IsCaseHeightSet = isSet;
            this.IsCaseHighSet = isSet;
            this.IsCasePackUnitsSet = isSet;
            this.IsCaseWideSet = isSet;
            this.IsCaseWidthSet = isSet;
            this.IsConsumerInformationSet = isSet;
            this.IsCostPriceSet = isSet;
            this.IsDaysOfSupplySet = isSet;
            this.IsDeliveryFrequencyDaysSet = isSet;
            this.IsDeliveryMethodSet = isSet;
            this.IsDepthSet = isSet;
            this.IsDescriptionSet = isSet;
            this.IsGTINSet = isSet;
            this.IsHeightSet = isSet;
            this.IsLocationIdSet = isSet;
            this.IsManufacturerCodeSet = isSet;
            this.IsManufacturerSet = isSet;
            this.IsMaxDeepSet = isSet;
            this.IsMinDeepSet = isSet;
            this.IsModelSet = isSet;
            this.IsPatternSet = isSet;
            this.IsProductIdSet = isSet;
            this.IsRecommendedRetailPriceSet = isSet;
            this.IsSellPackCountSet = isSet;
            this.IsSellPackDescriptionSet = isSet;
            this.IsSellPriceSet = isSet;
            this.IsShelfLifeSet = isSet;
            this.IsSizeSet = isSet;
            this.IsStatusTypeSet = isSet;
            this.IsTrayDeepSet = isSet;
            this.IsTrayDepthSet = isSet;
            this.IsTrayHeightSet = isSet;
            this.IsTrayHighSet = isSet;
            this.IsTrayPackUnitsSet = isSet;
            this.IsTrayThickDepthSet = isSet;
            this.IsTrayThickHeightSet = isSet;
            this.IsTrayThickWidthSet = isSet;
            this.IsTrayWideSet = isSet;
            this.IsTrayWidthSet = isSet;
            this.IsUnitOfMeasureSet = isSet;
            this.IsVendorCodeSet = isSet;
            this.IsVendorSet = isSet;
            this.IsWidthSet = isSet;
            this.IsCorporateCodeSet = isSet;

            //#region Custom Attributes
            //this.IsCustomAttribute01Set = isSet;
            //this.IsCustomAttribute02Set = isSet;
            //this.IsCustomAttribute03Set = isSet;
            //this.IsCustomAttribute04Set = isSet;
            //this.IsCustomAttribute05Set = isSet;
            //this.IsCustomAttribute06Set = isSet;
            //this.IsCustomAttribute07Set = isSet;
            //this.IsCustomAttribute08Set = isSet;
            //this.IsCustomAttribute09Set = isSet;
            //this.IsCustomAttribute10Set = isSet;
            //this.IsCustomAttribute11Set = isSet;
            //this.IsCustomAttribute12Set = isSet;
            //this.IsCustomAttribute13Set = isSet;
            //this.IsCustomAttribute14Set = isSet;
            //this.IsCustomAttribute15Set = isSet;
            //this.IsCustomAttribute16Set = isSet;
            //this.IsCustomAttribute17Set = isSet;
            //this.IsCustomAttribute18Set = isSet;
            //this.IsCustomAttribute19Set = isSet;
            //this.IsCustomAttribute20Set = isSet;
            //this.IsCustomAttribute21Set = isSet;
            //this.IsCustomAttribute22Set = isSet;
            //this.IsCustomAttribute23Set = isSet;
            //this.IsCustomAttribute24Set = isSet;
            //this.IsCustomAttribute25Set = isSet;
            //this.IsCustomAttribute26Set = isSet;
            //this.IsCustomAttribute27Set = isSet;
            //this.IsCustomAttribute28Set = isSet;
            //this.IsCustomAttribute29Set = isSet;
            //this.IsCustomAttribute30Set = isSet;
            //this.IsCustomAttribute31Set = isSet;
            //this.IsCustomAttribute32Set = isSet;
            //this.IsCustomAttribute33Set = isSet;
            //this.IsCustomAttribute34Set = isSet;
            //this.IsCustomAttribute35Set = isSet;
            //this.IsCustomAttribute36Set = isSet;
            //this.IsCustomAttribute37Set = isSet;
            //this.IsCustomAttribute38Set = isSet;
            //this.IsCustomAttribute39Set = isSet;
            //this.IsCustomAttribute40Set = isSet;
            //this.IsCustomAttribute41Set = isSet;
            //this.IsCustomAttribute42Set = isSet;
            //this.IsCustomAttribute43Set = isSet;
            //this.IsCustomAttribute44Set = isSet;
            //this.IsCustomAttribute45Set = isSet;
            //this.IsCustomAttribute46Set = isSet;
            //this.IsCustomAttribute47Set = isSet;
            //this.IsCustomAttribute48Set = isSet;
            //this.IsCustomAttribute49Set = isSet;
            //this.IsCustomAttribute50Set = isSet;
            //#endregion
        }

        #endregion
    }

    [Serializable]
    public class LocationProductAttributeDtoKey
    {
        #region Properties
        public Int16 LocationId { get; set; }
        public Int32 ProductId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                 LocationId.GetHashCode() +
                 ProductId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            LocationProductAttributeDtoKey other = obj as LocationProductAttributeDtoKey;
            if (other != null)
            {
                if (other.LocationId != this.LocationId) { return false; }
                if (other.ProductId != this.ProductId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
