﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using AutoMapper;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class EntityComparisonAttributeDto
    {
        public EntityComparisonAttributeDto()
        {
            
        }

        public EntityComparisonAttributeDto(EntityDto parent)
        {
            EntityId = parent.Id;
        }

        public Int32 Id { get; set; }
        public Int32 EntityId { get; set; }
        public String PropertyName { get; set; }
        public String PropertydisplayName { get; set; }
        public Byte ItemType { get; set; }

        public static EntityComparisonAttributeDto CreateFromDataReader(SqlDataReader dr)
        {
            if (dr == null || !dr.HasRows || dr.IsDBNull(dr.GetOrdinal("EntityProductAttributeComparisonAttribute_Id"))) return null;

            
            var itemToReturn = new EntityComparisonAttributeDto()
            {
                Id = dr.GetInt32(dr.GetOrdinal("EntityProductAttributeComparisonAttribute_Id")),
                EntityId = dr.GetInt32(dr.GetOrdinal("Entity_Id")),
                ItemType = dr.GetByte(dr.GetOrdinal("EntityProductAttributeComparisonAttribute_ItemType")),
                PropertyName = !dr.IsDBNull(dr.GetOrdinal("EntityProductAttributeComparisonAttribute_PropertyName")) ? dr.GetString(dr.GetOrdinal("EntityProductAttributeComparisonAttribute_PropertyName")) : String.Empty,
                PropertydisplayName = !dr.IsDBNull(dr.GetOrdinal("EntityProductAttributeComparisonAttribute_PropertyDisplayName"))  ? dr.GetString(dr.GetOrdinal("EntityProductAttributeComparisonAttribute_PropertyDisplayName")) : String.Empty,
            };
            
            return itemToReturn;
        }

        public override Boolean Equals(Object obj)
        {
            return base.Equals(obj);
        }

        protected bool Equals(EntityComparisonAttributeDto other)
        {
            return Id == other.Id && EntityId == other.EntityId && string.Equals(PropertyName, other.PropertyName) && string.Equals(PropertydisplayName, other.PropertydisplayName) && string.Equals(ItemType, other.ItemType);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Id;
                hashCode = (hashCode*397) ^ EntityId;
                hashCode = (hashCode*397) ^ (PropertyName != null ? PropertyName.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (PropertydisplayName != null ? PropertydisplayName.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ ItemType.GetHashCode();
                return hashCode;
            }
        }
    }
}