﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25546 : N.Foster
//  Created
// V8-25868 : L.Ineson
//  Added Description
// V8-25787 : N.Foster
//  Added RowVersion
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public sealed class WorkflowInfoDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the instance has code
        /// </summary>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Compares two instances to each other
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            WorkflowInfoDto other = obj as WorkflowInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.RowVersion != this.RowVersion) return false;
                if (other.Name != this.Name) return false;
                if (other.Description != this.Description) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
