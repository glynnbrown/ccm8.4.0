﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25444 : L.Ineson
//		Created (Auto-generated)
// V8-25619 : A.Kuszyk
//      LocationTypeId property removed and ForeignKey attributes added.
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Location Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class LocationDto
    {
        #region Properties
        public Int16 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public LocationDtoKey DtoKey
        {
            get
            {
                return new LocationDtoKey()
                {
                    EntityId = this.EntityId,
                    Code = this.Code
                };
            }
        }
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        //public Int32 LocationTypeId { get; set; }
        [ForeignKey(typeof(LocationGroupDto), typeof(ILocationGroupDal), DeleteBehavior.MoveUpToRoot)]
        public Int32 LocationGroupId { get; set; }
        public String Code { get; set; }
        public String Name { get; set; }
        public String Region { get; set; }
        public String TVRegion { get; set; }
        public String Location { get; set; }
        public String DefaultClusterAttribute { get; set; }
        public String Address1 { get; set; }
        public String Address2 { get; set; }
        public String City { get; set; }
        public String County { get; set; }
        public String State { get; set; }
        public String PostalCode { get; set; }
        public String Country { get; set; }
        public Single? Longitude { get; set; }
        public Single? Latitude { get; set; }
        public DateTime? DateOpen { get; set; }
        public DateTime? DateLastRefitted { get; set; }
        public DateTime? DateClosed { get; set; }
        public Int16? CarParkSpaces { get; set; }
        public String CarParkManagement { get; set; }
        public String PetrolForecourtType { get; set; }
        public String Restaurant { get; set; }
        public Int32? SizeGrossFloorArea { get; set; }
        public Int32? SizeNetSalesArea { get; set; }
        public Int32? SizeMezzSalesArea { get; set; }
        public String TelephoneCountryCode { get; set; }
        public String TelephoneAreaCode { get; set; }
        public String TelephoneNumber { get; set; }
        public String FaxCountryCode { get; set; }
        public String FaxAreaCode { get; set; }
        public String FaxNumber { get; set; }
        public String OpeningHours { get; set; }
        public Single? AverageOpeningHours { get; set; }
        public String ManagerName { get; set; }
        public String ManagerEmail { get; set; }
        public String RegionalManagerName { get; set; }
        public String RegionalManagerEmail { get; set; }
        public String DivisionalManagerName { get; set; }
        public String DivisionalManagerEmail { get; set; }
        public String AdvertisingZone { get; set; }
        public String DistributionCentre { get; set; }
        public Byte? NoOfCheckouts { get; set; }
        public Boolean? IsMezzFitted { get; set; }
        public Boolean? IsFreehold { get; set; }
        public Boolean? Is24Hours { get; set; }
        public Boolean? IsOpenMonday { get; set; }
        public Boolean? IsOpenTuesday { get; set; }
        public Boolean? IsOpenWednesday { get; set; }
        public Boolean? IsOpenThursday { get; set; }
        public Boolean? IsOpenFriday { get; set; }
        public Boolean? IsOpenSaturday { get; set; }
        public Boolean? IsOpenSunday { get; set; }
        public Boolean? HasPetrolForecourt { get; set; }
        public Boolean? HasNewsCube { get; set; }
        public Boolean? HasAtmMachines { get; set; }
        public Boolean? HasCustomerWC { get; set; }
        public Boolean? HasBabyChanging { get; set; }
        public Boolean? HasInStoreBakery { get; set; }
        public Boolean? HasHotFoodToGo { get; set; }
        public Boolean? HasRotisserie { get; set; }
        public Boolean? HasFishmonger { get; set; }
        public Boolean? HasButcher { get; set; }
        public Boolean? HasPizza { get; set; }
        public Boolean? HasDeli { get; set; }
        public Boolean? HasSaladBar { get; set; }
        public Boolean? HasOrganic { get; set; }
        public Boolean? HasGrocery { get; set; }
        public Boolean? HasMobilePhones { get; set; }
        public Boolean? HasDryCleaning { get; set; }
        public Boolean? HasHomeShoppingAvailable { get; set; }
        public Boolean? HasOptician { get; set; }
        public Boolean? HasPharmacy { get; set; }
        public Boolean? HasTravel { get; set; }
        public Boolean? HasPhotoDepartment { get; set; }
        public Boolean? HasCarServiceArea { get; set; }
        public Boolean? HasGardenCentre { get; set; }
        public Boolean? HasClinic { get; set; }
        public Boolean? HasAlcohol { get; set; }
        public Boolean? HasFashion { get; set; }
        public Boolean? HasCafe { get; set; }
        public Boolean? HasRecycling { get; set; }
        public Boolean? HasPhotocopier { get; set; }
        public Boolean? HasLottery { get; set; }
        public Boolean? HasPostOffice { get; set; }
        public Boolean? HasMovieRental { get; set; }
        public Boolean? HasJewellery { get; set; }
        public String LocationType { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            LocationDto other = obj as LocationDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                //if (other.LocationTypeId != this.LocationTypeId) { return false; }
                if (other.LocationGroupId != this.LocationGroupId) { return false; }
                if (other.Code != this.Code) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.Region != this.Region) { return false; }
                if (other.TVRegion != this.TVRegion) { return false; }
                if (other.Location != this.Location) { return false; }
                if (other.DefaultClusterAttribute != this.DefaultClusterAttribute) { return false; }
                if (other.Address1 != this.Address1) { return false; }
                if (other.Address2 != this.Address2) { return false; }
                if (other.City != this.City) { return false; }
                if (other.County != this.County) { return false; }
                if (other.State != this.State) { return false; }
                if (other.PostalCode != this.PostalCode) { return false; }
                if (other.Country != this.Country) { return false; }
                if (other.Longitude != this.Longitude) { return false; }
                if (other.Latitude != this.Latitude) { return false; }
                if (other.DateOpen != this.DateOpen) { return false; }
                if (other.DateLastRefitted != this.DateLastRefitted) { return false; }
                if (other.DateClosed != this.DateClosed) { return false; }
                if (other.CarParkSpaces != this.CarParkSpaces) { return false; }
                if (other.CarParkManagement != this.CarParkManagement) { return false; }
                if (other.PetrolForecourtType != this.PetrolForecourtType) { return false; }
                if (other.Restaurant != this.Restaurant) { return false; }
                if (other.SizeGrossFloorArea != this.SizeGrossFloorArea) { return false; }
                if (other.SizeNetSalesArea != this.SizeNetSalesArea) { return false; }
                if (other.SizeMezzSalesArea != this.SizeMezzSalesArea) { return false; }
                if (other.TelephoneCountryCode != this.TelephoneCountryCode) { return false; }
                if (other.TelephoneAreaCode != this.TelephoneAreaCode) { return false; }
                if (other.TelephoneNumber != this.TelephoneNumber) { return false; }
                if (other.FaxCountryCode != this.FaxCountryCode) { return false; }
                if (other.FaxAreaCode != this.FaxAreaCode) { return false; }
                if (other.FaxNumber != this.FaxNumber) { return false; }
                if (other.OpeningHours != this.OpeningHours) { return false; }
                if (other.AverageOpeningHours != this.AverageOpeningHours) { return false; }
                if (other.ManagerName != this.ManagerName) { return false; }
                if (other.ManagerEmail != this.ManagerEmail) { return false; }
                if (other.RegionalManagerName != this.RegionalManagerName) { return false; }
                if (other.RegionalManagerEmail != this.RegionalManagerEmail) { return false; }
                if (other.DivisionalManagerName != this.DivisionalManagerName) { return false; }
                if (other.DivisionalManagerEmail != this.DivisionalManagerEmail) { return false; }
                if (other.AdvertisingZone != this.AdvertisingZone) { return false; }
                if (other.DistributionCentre != this.DistributionCentre) { return false; }
                if (other.NoOfCheckouts != this.NoOfCheckouts) { return false; }
                if (other.IsMezzFitted != this.IsMezzFitted) { return false; }
                if (other.IsFreehold != this.IsFreehold) { return false; }
                if (other.Is24Hours != this.Is24Hours) { return false; }
                if (other.IsOpenMonday != this.IsOpenMonday) { return false; }
                if (other.IsOpenTuesday != this.IsOpenTuesday) { return false; }
                if (other.IsOpenWednesday != this.IsOpenWednesday) { return false; }
                if (other.IsOpenThursday != this.IsOpenThursday) { return false; }
                if (other.IsOpenFriday != this.IsOpenFriday) { return false; }
                if (other.IsOpenSaturday != this.IsOpenSaturday) { return false; }
                if (other.IsOpenSunday != this.IsOpenSunday) { return false; }
                if (other.HasPetrolForecourt != this.HasPetrolForecourt) { return false; }
                if (other.HasNewsCube != this.HasNewsCube) { return false; }
                if (other.HasAtmMachines != this.HasAtmMachines) { return false; }
                if (other.HasCustomerWC != this.HasCustomerWC) { return false; }
                if (other.HasBabyChanging != this.HasBabyChanging) { return false; }
                if (other.HasInStoreBakery != this.HasInStoreBakery) { return false; }
                if (other.HasHotFoodToGo != this.HasHotFoodToGo) { return false; }
                if (other.HasRotisserie != this.HasRotisserie) { return false; }
                if (other.HasFishmonger != this.HasFishmonger) { return false; }
                if (other.HasButcher != this.HasButcher) { return false; }
                if (other.HasPizza != this.HasPizza) { return false; }
                if (other.HasDeli != this.HasDeli) { return false; }
                if (other.HasSaladBar != this.HasSaladBar) { return false; }
                if (other.HasOrganic != this.HasOrganic) { return false; }
                if (other.HasGrocery != this.HasGrocery) { return false; }
                if (other.HasMobilePhones != this.HasMobilePhones) { return false; }
                if (other.HasDryCleaning != this.HasDryCleaning) { return false; }
                if (other.HasHomeShoppingAvailable != this.HasHomeShoppingAvailable) { return false; }
                if (other.HasOptician != this.HasOptician) { return false; }
                if (other.HasPharmacy != this.HasPharmacy) { return false; }
                if (other.HasTravel != this.HasTravel) { return false; }
                if (other.HasPhotoDepartment != this.HasPhotoDepartment) { return false; }
                if (other.HasCarServiceArea != this.HasCarServiceArea) { return false; }
                if (other.HasGardenCentre != this.HasGardenCentre) { return false; }
                if (other.HasClinic != this.HasClinic) { return false; }
                if (other.HasAlcohol != this.HasAlcohol) { return false; }
                if (other.HasFashion != this.HasFashion) { return false; }
                if (other.HasCafe != this.HasCafe) { return false; }
                if (other.HasRecycling != this.HasRecycling) { return false; }
                if (other.HasPhotocopier != this.HasPhotocopier) { return false; }
                if (other.HasLottery != this.HasLottery) { return false; }
                if (other.HasPostOffice != this.HasPostOffice) { return false; }
                if (other.HasMovieRental != this.HasMovieRental) { return false; }
                if (other.HasJewellery != this.HasJewellery) { return false; }
                if (other.LocationType != this.LocationType) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.DateDeleted != this.DateDeleted) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class LocationDtoKey
    {
        #region Properties
        public Int32 EntityId { get; set; }
        public String Code{ get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                EntityId.GetHashCode() +
                Code.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            LocationDtoKey other = obj as LocationDtoKey;
            if (other != null)
            {
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Code != this.Code) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class LocationIsSetDto
    {
        #region Properties
        public Boolean IsEntityIdSet { get; set; }
       // public Boolean IsLocationTypeIdSet { get; set; }
        public Boolean IsLocationGroupIdSet { get; set; }
        public Boolean IsCodeSet { get; set; }
        public Boolean IsNameSet { get; set; }
        public Boolean IsRegionSet { get; set; }
        public Boolean IsTVRegionSet { get; set; }
        public Boolean IsLocationSet { get; set; }
        public Boolean IsDefaultClusterAttributeSet { get; set; }
        public Boolean IsAddress1Set { get; set; }
        public Boolean IsAddress2Set { get; set; }
        public Boolean IsCitySet { get; set; }
        public Boolean IsCountySet { get; set; }
        public Boolean IsStateSet { get; set; }
        public Boolean IsPostalCodeSet { get; set; }
        public Boolean IsCountrySet { get; set; }
        public Boolean IsLongitudeSet { get; set; }
        public Boolean IsLatitudeSet { get; set; }
        public Boolean IsDateOpenSet { get; set; }
        public Boolean IsDateLastRefittedSet { get; set; }
        public Boolean IsDateClosedSet { get; set; }
        public Boolean IsCarParkSpacesSet { get; set; }
        public Boolean IsCarParkManagementSet { get; set; }
        public Boolean IsPetrolForecourtTypeSet { get; set; }
        public Boolean IsRestaurantSet { get; set; }
        public Boolean IsSizeGrossFloorAreaSet { get; set; }
        public Boolean IsSizeNetSalesAreaSet { get; set; }
        public Boolean IsSizeMezzSalesAreaSet { get; set; }
        public Boolean IsTelephoneCountryCodeSet { get; set; }
        public Boolean IsTelephoneAreaCodeSet { get; set; }
        public Boolean IsTelephoneNumberSet { get; set; }
        public Boolean IsFaxCountryCodeSet { get; set; }
        public Boolean IsFaxAreaCodeSet { get; set; }
        public Boolean IsFaxNumberSet { get; set; }
        public Boolean IsOpeningHoursSet { get; set; }
        public Boolean IsAverageOpeningHoursSet { get; set; }
        public Boolean IsManagerNameSet { get; set; }
        public Boolean IsManagerEmailSet { get; set; }
        public Boolean IsRegionalManagerNameSet { get; set; }
        public Boolean IsRegionalManagerEmailSet { get; set; }
        public Boolean IsDivisionalManagerNameSet { get; set; }
        public Boolean IsDivisionalManagerEmailSet { get; set; }
        public Boolean IsAdvertisingZoneSet { get; set; }
        public Boolean IsDistributionCentreSet { get; set; }
        public Boolean IsNoOfCheckoutsSet { get; set; }
        public Boolean IsIsMezzFittedSet { get; set; }
        public Boolean IsIsFreeholdSet { get; set; }
        public Boolean IsIs24HoursSet { get; set; }
        public Boolean IsIsOpenMondaySet { get; set; }
        public Boolean IsIsOpenTuesdaySet { get; set; }
        public Boolean IsIsOpenWednesdaySet { get; set; }
        public Boolean IsIsOpenThursdaySet { get; set; }
        public Boolean IsIsOpenFridaySet { get; set; }
        public Boolean IsIsOpenSaturdaySet { get; set; }
        public Boolean IsIsOpenSundaySet { get; set; }
        public Boolean IsHasPetrolForecourtSet { get; set; }
        public Boolean IsHasNewsCubeSet { get; set; }
        public Boolean IsHasAtmMachinesSet { get; set; }
        public Boolean IsHasCustomerWCSet { get; set; }
        public Boolean IsHasBabyChangingSet { get; set; }
        public Boolean IsHasInStoreBakerySet { get; set; }
        public Boolean IsHasHotFoodToGoSet { get; set; }
        public Boolean IsHasRotisserieSet { get; set; }
        public Boolean IsHasFishmongerSet { get; set; }
        public Boolean IsHasButcherSet { get; set; }
        public Boolean IsHasPizzaSet { get; set; }
        public Boolean IsHasDeliSet { get; set; }
        public Boolean IsHasSaladBarSet { get; set; }
        public Boolean IsHasOrganicSet { get; set; }
        public Boolean IsHasGrocerySet { get; set; }
        public Boolean IsHasMobilePhonesSet { get; set; }
        public Boolean IsHasDryCleaningSet { get; set; }
        public Boolean IsHasHomeShoppingAvailableSet { get; set; }
        public Boolean IsHasOpticianSet { get; set; }
        public Boolean IsHasPharmacySet { get; set; }
        public Boolean IsHasTravelSet { get; set; }
        public Boolean IsHasPhotoDepartmentSet { get; set; }
        public Boolean IsHasCarServiceAreaSet { get; set; }
        public Boolean IsHasGardenCentreSet { get; set; }
        public Boolean IsHasClinicSet { get; set; }
        public Boolean IsHasAlcoholSet { get; set; }
        public Boolean IsHasFashionSet { get; set; }
        public Boolean IsHasCafeSet { get; set; }
        public Boolean IsHasRecyclingSet { get; set; }
        public Boolean IsHasPhotocopierSet { get; set; }
        public Boolean IsHasLotterySet { get; set; }
        public Boolean IsHasPostOfficeSet { get; set; }
        public Boolean IsHasMovieRentalSet { get; set; }
        public Boolean IsHasJewellerySet { get; set; }
        public Boolean IsLocationTypeSet { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public LocationIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public LocationIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsEntityIdSet.GetHashCode() +
                //IsLocationTypeIdSet.GetHashCode() +
                IsLocationGroupIdSet.GetHashCode() +
                IsCodeSet.GetHashCode() +
                IsNameSet.GetHashCode() +
                IsRegionSet.GetHashCode() +
                IsTVRegionSet.GetHashCode() +
                IsLocationSet.GetHashCode() +
                IsDefaultClusterAttributeSet.GetHashCode() +
                IsAddress1Set.GetHashCode() +
                IsAddress2Set.GetHashCode() +
                IsCitySet.GetHashCode() +
                IsCountySet.GetHashCode() +
                IsStateSet.GetHashCode() +
                IsPostalCodeSet.GetHashCode() +
                IsCountrySet.GetHashCode() +
                IsLongitudeSet.GetHashCode() +
                IsLatitudeSet.GetHashCode() +
                IsDateOpenSet.GetHashCode() +
                IsDateLastRefittedSet.GetHashCode() +
                IsDateClosedSet.GetHashCode() +
                IsCarParkSpacesSet.GetHashCode() +
                IsCarParkManagementSet.GetHashCode() +
                IsPetrolForecourtTypeSet.GetHashCode() +
                IsRestaurantSet.GetHashCode() +
                IsSizeGrossFloorAreaSet.GetHashCode() +
                IsSizeNetSalesAreaSet.GetHashCode() +
                IsSizeMezzSalesAreaSet.GetHashCode() +
                IsTelephoneCountryCodeSet.GetHashCode() +
                IsTelephoneAreaCodeSet.GetHashCode() +
                IsTelephoneNumberSet.GetHashCode() +
                IsFaxCountryCodeSet.GetHashCode() +
                IsFaxAreaCodeSet.GetHashCode() +
                IsFaxNumberSet.GetHashCode() +
                IsOpeningHoursSet.GetHashCode() +
                IsAverageOpeningHoursSet.GetHashCode() +
                IsManagerNameSet.GetHashCode() +
                IsManagerEmailSet.GetHashCode() +
                IsRegionalManagerNameSet.GetHashCode() +
                IsRegionalManagerEmailSet.GetHashCode() +
                IsDivisionalManagerNameSet.GetHashCode() +
                IsDivisionalManagerEmailSet.GetHashCode() +
                IsAdvertisingZoneSet.GetHashCode() +
                IsDistributionCentreSet.GetHashCode() +
                IsNoOfCheckoutsSet.GetHashCode() +
                IsIsMezzFittedSet.GetHashCode() +
                IsIsFreeholdSet.GetHashCode() +
                IsIs24HoursSet.GetHashCode() +
                IsIsOpenMondaySet.GetHashCode() +
                IsIsOpenTuesdaySet.GetHashCode() +
                IsIsOpenWednesdaySet.GetHashCode() +
                IsIsOpenThursdaySet.GetHashCode() +
                IsIsOpenFridaySet.GetHashCode() +
                IsIsOpenSaturdaySet.GetHashCode() +
                IsIsOpenSundaySet.GetHashCode() +
                IsHasPetrolForecourtSet.GetHashCode() +
                IsHasNewsCubeSet.GetHashCode() +
                IsHasAtmMachinesSet.GetHashCode() +
                IsHasCustomerWCSet.GetHashCode() +
                IsHasBabyChangingSet.GetHashCode() +
                IsHasInStoreBakerySet.GetHashCode() +
                IsHasHotFoodToGoSet.GetHashCode() +
                IsHasRotisserieSet.GetHashCode() +
                IsHasFishmongerSet.GetHashCode() +
                IsHasButcherSet.GetHashCode() +
                IsHasPizzaSet.GetHashCode() +
                IsHasDeliSet.GetHashCode() +
                IsHasSaladBarSet.GetHashCode() +
                IsHasOrganicSet.GetHashCode() +
                IsHasGrocerySet.GetHashCode() +
                IsHasMobilePhonesSet.GetHashCode() +
                IsHasDryCleaningSet.GetHashCode() +
                IsHasHomeShoppingAvailableSet.GetHashCode() +
                IsHasOpticianSet.GetHashCode() +
                IsHasPharmacySet.GetHashCode() +
                IsHasTravelSet.GetHashCode() +
                IsHasPhotoDepartmentSet.GetHashCode() +
                IsHasCarServiceAreaSet.GetHashCode() +
                IsHasGardenCentreSet.GetHashCode() +
                IsHasClinicSet.GetHashCode() +
                IsHasAlcoholSet.GetHashCode() +
                IsHasFashionSet.GetHashCode() +
                IsHasCafeSet.GetHashCode() +
                IsHasRecyclingSet.GetHashCode() +
                IsHasPhotocopierSet.GetHashCode() +
                IsHasLotterySet.GetHashCode() +
                IsHasPostOfficeSet.GetHashCode() +
                IsHasMovieRentalSet.GetHashCode() +
                IsLocationTypeSet.GetHashCode() +
                IsHasJewellerySet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            LocationIsSetDto other = obj as LocationIsSetDto;
            if (other != null)
            {
                if (other.IsEntityIdSet != this.IsEntityIdSet) { return false; }
                //if (other.IsLocationTypeIdSet != this.IsLocationTypeIdSet) { return false; }
                if (other.IsLocationGroupIdSet != this.IsLocationGroupIdSet) { return false; }
                if (other.IsCodeSet != this.IsCodeSet) { return false; }
                if (other.IsNameSet != this.IsNameSet) { return false; }
                if (other.IsRegionSet != this.IsRegionSet) { return false; }
                if (other.IsTVRegionSet != this.IsTVRegionSet) { return false; }
                if (other.IsLocationSet != this.IsLocationSet) { return false; }
                if (other.IsDefaultClusterAttributeSet != this.IsDefaultClusterAttributeSet) { return false; }
                if (other.IsAddress1Set != this.IsAddress1Set) { return false; }
                if (other.IsAddress2Set != this.IsAddress2Set) { return false; }
                if (other.IsCitySet != this.IsCitySet) { return false; }
                if (other.IsCountySet != this.IsCountySet) { return false; }
                if (other.IsStateSet != this.IsStateSet) { return false; }
                if (other.IsPostalCodeSet != this.IsPostalCodeSet) { return false; }
                if (other.IsCountrySet != this.IsCountrySet) { return false; }
                if (other.IsLongitudeSet != this.IsLongitudeSet) { return false; }
                if (other.IsLatitudeSet != this.IsLatitudeSet) { return false; }
                if (other.IsDateOpenSet != this.IsDateOpenSet) { return false; }
                if (other.IsDateLastRefittedSet != this.IsDateLastRefittedSet) { return false; }
                if (other.IsDateClosedSet != this.IsDateClosedSet) { return false; }
                if (other.IsCarParkSpacesSet != this.IsCarParkSpacesSet) { return false; }
                if (other.IsCarParkManagementSet != this.IsCarParkManagementSet) { return false; }
                if (other.IsPetrolForecourtTypeSet != this.IsPetrolForecourtTypeSet) { return false; }
                if (other.IsRestaurantSet != this.IsRestaurantSet) { return false; }
                if (other.IsSizeGrossFloorAreaSet != this.IsSizeGrossFloorAreaSet) { return false; }
                if (other.IsSizeNetSalesAreaSet != this.IsSizeNetSalesAreaSet) { return false; }
                if (other.IsSizeMezzSalesAreaSet != this.IsSizeMezzSalesAreaSet) { return false; }
                if (other.IsTelephoneCountryCodeSet != this.IsTelephoneCountryCodeSet) { return false; }
                if (other.IsTelephoneAreaCodeSet != this.IsTelephoneAreaCodeSet) { return false; }
                if (other.IsTelephoneNumberSet != this.IsTelephoneNumberSet) { return false; }
                if (other.IsFaxCountryCodeSet != this.IsFaxCountryCodeSet) { return false; }
                if (other.IsFaxAreaCodeSet != this.IsFaxAreaCodeSet) { return false; }
                if (other.IsFaxNumberSet != this.IsFaxNumberSet) { return false; }
                if (other.IsOpeningHoursSet != this.IsOpeningHoursSet) { return false; }
                if (other.IsAverageOpeningHoursSet != this.IsAverageOpeningHoursSet) { return false; }
                if (other.IsManagerNameSet != this.IsManagerNameSet) { return false; }
                if (other.IsManagerEmailSet != this.IsManagerEmailSet) { return false; }
                if (other.IsRegionalManagerNameSet != this.IsRegionalManagerNameSet) { return false; }
                if (other.IsRegionalManagerEmailSet != this.IsRegionalManagerEmailSet) { return false; }
                if (other.IsDivisionalManagerNameSet != this.IsDivisionalManagerNameSet) { return false; }
                if (other.IsDivisionalManagerEmailSet != this.IsDivisionalManagerEmailSet) { return false; }
                if (other.IsAdvertisingZoneSet != this.IsAdvertisingZoneSet) { return false; }
                if (other.IsDistributionCentreSet != this.IsDistributionCentreSet) { return false; }
                if (other.IsNoOfCheckoutsSet != this.IsNoOfCheckoutsSet) { return false; }
                if (other.IsIsMezzFittedSet != this.IsIsMezzFittedSet) { return false; }
                if (other.IsIsFreeholdSet != this.IsIsFreeholdSet) { return false; }
                if (other.IsIs24HoursSet != this.IsIs24HoursSet) { return false; }
                if (other.IsIsOpenMondaySet != this.IsIsOpenMondaySet) { return false; }
                if (other.IsIsOpenTuesdaySet != this.IsIsOpenTuesdaySet) { return false; }
                if (other.IsIsOpenWednesdaySet != this.IsIsOpenWednesdaySet) { return false; }
                if (other.IsIsOpenThursdaySet != this.IsIsOpenThursdaySet) { return false; }
                if (other.IsIsOpenFridaySet != this.IsIsOpenFridaySet) { return false; }
                if (other.IsIsOpenSaturdaySet != this.IsIsOpenSaturdaySet) { return false; }
                if (other.IsIsOpenSundaySet != this.IsIsOpenSundaySet) { return false; }
                if (other.IsHasPetrolForecourtSet != this.IsHasPetrolForecourtSet) { return false; }
                if (other.IsHasNewsCubeSet != this.IsHasNewsCubeSet) { return false; }
                if (other.IsHasAtmMachinesSet != this.IsHasAtmMachinesSet) { return false; }
                if (other.IsHasCustomerWCSet != this.IsHasCustomerWCSet) { return false; }
                if (other.IsHasBabyChangingSet != this.IsHasBabyChangingSet) { return false; }
                if (other.IsHasInStoreBakerySet != this.IsHasInStoreBakerySet) { return false; }
                if (other.IsHasHotFoodToGoSet != this.IsHasHotFoodToGoSet) { return false; }
                if (other.IsHasRotisserieSet != this.IsHasRotisserieSet) { return false; }
                if (other.IsHasFishmongerSet != this.IsHasFishmongerSet) { return false; }
                if (other.IsHasButcherSet != this.IsHasButcherSet) { return false; }
                if (other.IsHasPizzaSet != this.IsHasPizzaSet) { return false; }
                if (other.IsHasDeliSet != this.IsHasDeliSet) { return false; }
                if (other.IsHasSaladBarSet != this.IsHasSaladBarSet) { return false; }
                if (other.IsHasOrganicSet != this.IsHasOrganicSet) { return false; }
                if (other.IsHasGrocerySet != this.IsHasGrocerySet) { return false; }
                if (other.IsHasMobilePhonesSet != this.IsHasMobilePhonesSet) { return false; }
                if (other.IsHasDryCleaningSet != this.IsHasDryCleaningSet) { return false; }
                if (other.IsHasHomeShoppingAvailableSet != this.IsHasHomeShoppingAvailableSet) { return false; }
                if (other.IsHasOpticianSet != this.IsHasOpticianSet) { return false; }
                if (other.IsHasPharmacySet != this.IsHasPharmacySet) { return false; }
                if (other.IsHasTravelSet != this.IsHasTravelSet) { return false; }
                if (other.IsHasPhotoDepartmentSet != this.IsHasPhotoDepartmentSet) { return false; }
                if (other.IsHasCarServiceAreaSet != this.IsHasCarServiceAreaSet) { return false; }
                if (other.IsHasGardenCentreSet != this.IsHasGardenCentreSet) { return false; }
                if (other.IsHasClinicSet != this.IsHasClinicSet) { return false; }
                if (other.IsHasAlcoholSet != this.IsHasAlcoholSet) { return false; }
                if (other.IsHasFashionSet != this.IsHasFashionSet) { return false; }
                if (other.IsHasCafeSet != this.IsHasCafeSet) { return false; }
                if (other.IsHasRecyclingSet != this.IsHasRecyclingSet) { return false; }
                if (other.IsHasPhotocopierSet != this.IsHasPhotocopierSet) { return false; }
                if (other.IsHasLotterySet != this.IsHasLotterySet) { return false; }
                if (other.IsHasPostOfficeSet != this.IsHasPostOfficeSet) { return false; }
                if (other.IsHasMovieRentalSet != this.IsHasMovieRentalSet) { return false; }
                if (other.IsLocationTypeSet != this.IsLocationTypeSet) { return false; }
                if (other.IsHasJewellerySet != this.IsHasJewellerySet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsEntityIdSet = isSet;
            //IsLocationTypeIdSet = isSet;
            IsLocationGroupIdSet = isSet;
            IsCodeSet = isSet;
            IsNameSet = isSet;
            IsRegionSet = isSet;
            IsTVRegionSet = isSet;
            IsLocationSet = isSet;
            IsDefaultClusterAttributeSet = isSet;
            IsAddress1Set = isSet;
            IsAddress2Set = isSet;
            IsCitySet = isSet;
            IsCountySet = isSet;
            IsStateSet = isSet;
            IsPostalCodeSet = isSet;
            IsCountrySet = isSet;
            IsLongitudeSet = isSet;
            IsLatitudeSet = isSet;
            IsDateOpenSet = isSet;
            IsDateLastRefittedSet = isSet;
            IsDateClosedSet = isSet;
            IsCarParkSpacesSet = isSet;
            IsCarParkManagementSet = isSet;
            IsPetrolForecourtTypeSet = isSet;
            IsRestaurantSet = isSet;
            IsSizeGrossFloorAreaSet = isSet;
            IsSizeNetSalesAreaSet = isSet;
            IsSizeMezzSalesAreaSet = isSet;
            IsTelephoneCountryCodeSet = isSet;
            IsTelephoneAreaCodeSet = isSet;
            IsTelephoneNumberSet = isSet;
            IsFaxCountryCodeSet = isSet;
            IsFaxAreaCodeSet = isSet;
            IsFaxNumberSet = isSet;
            IsOpeningHoursSet = isSet;
            IsAverageOpeningHoursSet = isSet;
            IsManagerNameSet = isSet;
            IsManagerEmailSet = isSet;
            IsRegionalManagerNameSet = isSet;
            IsRegionalManagerEmailSet = isSet;
            IsDivisionalManagerNameSet = isSet;
            IsDivisionalManagerEmailSet = isSet;
            IsAdvertisingZoneSet = isSet;
            IsDistributionCentreSet = isSet;
            IsNoOfCheckoutsSet = isSet;
            IsIsMezzFittedSet = isSet;
            IsIsFreeholdSet = isSet;
            IsIs24HoursSet = isSet;
            IsIsOpenMondaySet = isSet;
            IsIsOpenTuesdaySet = isSet;
            IsIsOpenWednesdaySet = isSet;
            IsIsOpenThursdaySet = isSet;
            IsIsOpenFridaySet = isSet;
            IsIsOpenSaturdaySet = isSet;
            IsIsOpenSundaySet = isSet;
            IsHasPetrolForecourtSet = isSet;
            IsHasNewsCubeSet = isSet;
            IsHasAtmMachinesSet = isSet;
            IsHasCustomerWCSet = isSet;
            IsHasBabyChangingSet = isSet;
            IsHasInStoreBakerySet = isSet;
            IsHasHotFoodToGoSet = isSet;
            IsHasRotisserieSet = isSet;
            IsHasFishmongerSet = isSet;
            IsHasButcherSet = isSet;
            IsHasPizzaSet = isSet;
            IsHasDeliSet = isSet;
            IsHasSaladBarSet = isSet;
            IsHasOrganicSet = isSet;
            IsHasGrocerySet = isSet;
            IsHasMobilePhonesSet = isSet;
            IsHasDryCleaningSet = isSet;
            IsHasHomeShoppingAvailableSet = isSet;
            IsHasOpticianSet = isSet;
            IsHasPharmacySet = isSet;
            IsHasTravelSet = isSet;
            IsHasPhotoDepartmentSet = isSet;
            IsHasCarServiceAreaSet = isSet;
            IsHasGardenCentreSet = isSet;
            IsHasClinicSet = isSet;
            IsHasAlcoholSet = isSet;
            IsHasFashionSet = isSet;
            IsHasCafeSet = isSet;
            IsHasRecyclingSet = isSet;
            IsHasPhotocopierSet = isSet;
            IsHasLotterySet = isSet;
            IsHasPostOfficeSet = isSet;
            IsHasMovieRentalSet = isSet;
            IsHasJewellerySet = isSet;
            IsLocationTypeSet = isSet;
        }
        #endregion
    }
}
