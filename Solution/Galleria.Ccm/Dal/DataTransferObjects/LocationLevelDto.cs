﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// LocationLevel Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class LocationLevelDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public LocationLevelDtoKey DtoKey
        {
            get
            {
                return new LocationLevelDtoKey()
                {
                    LocationHierarchyId = this.LocationHierarchyId,
                    Name = this.Name
                };
            }
        }
        [ForeignKey(typeof(LocationHierarchyDto), typeof(ILocationHierarchyDal), DeleteBehavior.Leave)]
        public Int32 LocationHierarchyId { get; set; }
        public String Name { get; set; }
        public Byte ShapeNo { get; set; }
        public Int32 Colour { get; set; }
        [ForeignKey(typeof(LocationLevelDto), typeof(ILocationLevelDal))]
        public Int32? ParentLevelId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            LocationLevelDto other = obj as LocationLevelDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.LocationHierarchyId != this.LocationHierarchyId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.ShapeNo != this.ShapeNo) { return false; }
                if (other.Colour != this.Colour) { return false; }
                if (other.ParentLevelId != this.ParentLevelId) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.DateDeleted != this.DateDeleted) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class LocationLevelDtoKey
    {
        #region Properties
        public Int32 LocationHierarchyId { get; set; }
        public String Name { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                LocationHierarchyId.GetHashCode() +
                Name.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            LocationLevelDtoKey other = obj as LocationLevelDtoKey;
            if (other != null)
            {
                if (other.LocationHierarchyId != this.LocationHierarchyId) { return false; }
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
