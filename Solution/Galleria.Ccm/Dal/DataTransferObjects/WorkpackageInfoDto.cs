﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  Added ProcessingStatusDescription, ProgressDateStarted, ProgressDateLastUpdated properties
// V8-27411 : M.Pettit
//  Added DateCompleted and UserId, UserFullName properties
#endregion
#region Version History: CCM810
// V8-29811 : A.Probyn
//  Added PlanogramLocationType
// V8-29842 : A.Probyn
//  Extended to include WorkpackageType
// V8-29590 : A.Probyn
//      Extended for new PlanogramWarningCount, PlanogramTotalErrorScore, PlanogramHighestErrorScore
#endregion
#region Version History: CCM820
// V8-30887 : L.Ineson
//  Added workflow name
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public sealed class WorkpackageInfoDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public String Name { get; set; }
        public Int32 UserId { get; set; }
        public Byte PlanogramLocationType { get; set; }
        public Byte WorkpackageType { get; set; }
        public String UserFullName { get; set; }
        public Int32 PlanogramCount { get; set; }
        public Int32 PlanogramPendingCount { get; set; }
        public Int32 PlanogramQueuedCount { get; set; }
        public Int32 PlanogramProcessingCount { get; set; }
        public Int32 PlanogramCompleteCount { get; set; }
        public Int32 PlanogramFailedCount { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateCompleted { get; set; }
        public Byte ProcessingStatus { get; set; }
        public String ProcessingStatusDescription { get; set; }
        public Int32 ProgressMax { get; set; }
        public Int32 ProgressCurrent { get; set; }
        public DateTime? ProgressDateStarted { get; set; }
        public DateTime? ProgressDateLastUpdated { get; set; }
        public Int32 EntityId { get; set; }
        public Int32 PlanogramWarningCount { get; set; }
        public Int32 PlanogramTotalErrorScore { get; set; }
        public Int32 PlanogramHighestErrorScore { get; set; }
        public String WorkflowName { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the instance has code
        /// </summary>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Compares two instances to each other
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            WorkpackageInfoDto other = obj as WorkpackageInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.RowVersion != this.RowVersion) return false;
                if (other.Name != this.Name) return false;
                if (other.UserId != this.UserId) return false;
                if (other.UserFullName != this.UserFullName) return false;
                if (other.PlanogramLocationType != this.PlanogramLocationType) return false;
                if (other.WorkpackageType != this.WorkpackageType) return false;
                if (other.PlanogramCount != this.PlanogramCount) return false;
                if (other.PlanogramPendingCount != this.PlanogramPendingCount) return false;
                if (other.PlanogramQueuedCount != this.PlanogramQueuedCount) return false;
                if (other.PlanogramProcessingCount != this.PlanogramProcessingCount) return false;
                if (other.PlanogramCompleteCount != this.PlanogramCompleteCount) return false;
                if (other.PlanogramFailedCount != this.PlanogramFailedCount) return false;
                if (other.DateCreated != this.DateCreated) return false;
                if (other.DateLastModified != this.DateLastModified) return false;
                if (other.DateCompleted != this.DateCompleted) return false;
                if (other.ProcessingStatus != this.ProcessingStatus) return false;
                if (other.ProcessingStatusDescription != this.ProcessingStatusDescription) return false;
                if (other.ProgressMax != this.ProgressMax) return false;
                if (other.ProgressCurrent != this.ProgressCurrent) return false;
                if (other.ProgressDateStarted != this.ProgressDateStarted) return false;
                if (other.ProgressDateLastUpdated != this.ProgressDateLastUpdated) return false;
                if (other.EntityId != this.EntityId) return false;
                if (other.PlanogramWarningCount != this.PlanogramWarningCount) return false;
                if (other.PlanogramHighestErrorScore != this.PlanogramHighestErrorScore) return false;
                if (other.PlanogramTotalErrorScore != this.PlanogramTotalErrorScore) return false;
                if (other.WorkflowName != this.WorkflowName) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
