﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629 : A.Kuszyk
//		Created (copied from SA).
#endregion
#region Version History: (CCM CCM803)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#region Version History: (CCM 8.1.0)
// V8-29598 : L.Luong
//  Added ProductGroupId and DateLastModified
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// ClusterSchemeInfo Data Transfer Object
    /// </summary>
    [Serializable]
    public class ClusterSchemeInfoDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public Guid UniqueContentReference { get; set; }
        public String Name { get; set; }
        public Boolean IsDefault { get; set; }
        public Int32 ClusterCount { get; set; }
        public Int32 EntityId { get; set; }
        public Int32? ProductGroupId { get; set; }
        public DateTime DateLastModified { get; set; }
        public Guid? ParentUniqueContentReference { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ClusterSchemeInfoDto other = obj as ClusterSchemeInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.UniqueContentReference != this.UniqueContentReference) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.IsDefault != this.IsDefault) { return false; }
                if (other.ClusterCount != this.ClusterCount) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.ProductGroupId != this.ProductGroupId) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.ParentUniqueContentReference != this.ParentUniqueContentReference) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
