﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// AssortmentInventoryRule Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class AssortmentInventoryRuleDto
    {
        #region Properties
        public Int32 Id { get; set; }
        [ForeignKey(typeof(AssortmentDto), typeof(ILocationDal), DeleteBehavior.Cascade)]
        public Int32 AssortmentId { get; set; }
        public AssortmentInventoryRuleDtoKey DtoKey
        {
            get
            {
                return new AssortmentInventoryRuleDtoKey()
                {
                    ProductId = this.ProductId,
                    AssortmentId = this.AssortmentId
                };
            }
        }
        [ForeignKey(typeof(ProductDto), typeof(IProductDal), DeleteBehavior.Leave)]
        public Int32 ProductId { get; set; }
        [InheritedProperty(typeof(ProductDto), typeof(IProductDal), ForeignKeyPropertyName = "ProductId", ParentPropertyName = "Gtin")]
        public String ProductGtin { get; set; }
        public Single CasePack { get; set; }
        public Single DaysOfSupply { get; set; }
        public Single ShelfLife { get; set; }
        public Single ReplenishmentDays { get; set; }
        public Single WasteHurdleUnits { get; set; }
        public Single WasteHurdleCasePack { get; set; }
        public Int32 MinUnits { get; set; }
        public Int32 MinFacings { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentInventoryRuleDto other = obj as AssortmentInventoryRuleDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.ProductId != this.ProductId) { return false; }
                if (other.ProductGtin != this.ProductGtin) { return false; }
                if (other.CasePack != this.CasePack) { return false; }
                if (other.DaysOfSupply != this.DaysOfSupply) { return false; }
                if (other.ShelfLife != this.ShelfLife) { return false; }
                if (other.ReplenishmentDays != this.ReplenishmentDays) { return false; }
                if (other.WasteHurdleUnits != this.WasteHurdleUnits) { return false; }
                if (other.WasteHurdleCasePack != this.WasteHurdleCasePack) { return false; }
                if (other.MinUnits != this.MinUnits) { return false; }
                if (other.MinFacings != this.MinFacings) { return false; }
                if (other.AssortmentId != this.AssortmentId) { return false; }

            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentInventoryRuleDtoKey
    {
        #region Properties
        public Int32 ProductId { get; set; }
        public Int32 AssortmentId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                ProductId.GetHashCode() +
                AssortmentId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentInventoryRuleDtoKey other = obj as AssortmentInventoryRuleDtoKey;
            if (other != null)
            {
                if (other.ProductId != this.ProductId) { return false; }
                if (other.AssortmentId != this.AssortmentId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentInventoryRuleIsSetDto
    {
        #region Properties

        public Boolean IsProductIdSet { get; set; }
        public Boolean IsCasePackSet { get; set; }
        public Boolean IsDaysOfSupplySet { get; set; }
        public Boolean IsShelfLifeSet { get; set; }
        public Boolean IsReplenishmentDaysSet { get; set; }
        public Boolean IsWasteHurdleUnitsSet { get; set; }
        public Boolean IsWasteHurdleCasePackSet { get; set; }
        public Boolean IsMinUnitsSet { get; set; }
        public Boolean IsMinFacingsSet { get; set; }
        public Boolean IsAssortmentIdSet { get; set; }

        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public AssortmentInventoryRuleIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public AssortmentInventoryRuleIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                 IsProductIdSet.GetHashCode() +
                 IsCasePackSet.GetHashCode() +
                 IsDaysOfSupplySet.GetHashCode() +
                 IsShelfLifeSet.GetHashCode() +
                 IsReplenishmentDaysSet.GetHashCode() +
                 IsWasteHurdleUnitsSet.GetHashCode() +
                 IsWasteHurdleCasePackSet.GetHashCode() +
                 IsMinUnitsSet.GetHashCode() +
                 IsMinFacingsSet.GetHashCode() +
                 IsAssortmentIdSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentInventoryRuleIsSetDto other = obj as AssortmentInventoryRuleIsSetDto;
            if (other != null)
            {
                if (other.IsProductIdSet != this.IsProductIdSet) { return false; }
                if (other.IsCasePackSet != this.IsCasePackSet) { return false; }
                if (other.IsDaysOfSupplySet != this.IsDaysOfSupplySet) { return false; }
                if (other.IsShelfLifeSet != this.IsShelfLifeSet) { return false; }
                if (other.IsReplenishmentDaysSet != this.IsReplenishmentDaysSet) { return false; }
                if (other.IsWasteHurdleUnitsSet != this.IsWasteHurdleUnitsSet) { return false; }
                if (other.IsWasteHurdleCasePackSet != this.IsWasteHurdleCasePackSet) { return false; }
                if (other.IsMinUnitsSet != this.IsMinUnitsSet) { return false; }
                if (other.IsMinFacingsSet != this.IsMinFacingsSet) { return false; }
                if (other.IsAssortmentIdSet != this.IsAssortmentIdSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsProductIdSet = isSet;
            IsCasePackSet = isSet;
            IsDaysOfSupplySet = isSet;
            IsShelfLifeSet = isSet;
            IsReplenishmentDaysSet = isSet;
            IsWasteHurdleUnitsSet = isSet;
            IsWasteHurdleCasePackSet = isSet;
            IsMinUnitsSet = isSet;
            IsMinFacingsSet = isSet;
            IsAssortmentIdSet = isSet;
        }
        #endregion
    }
}
    
