﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25436 L.Luong
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// HighlightInfo data transfer object
    /// </summary>
    [Serializable]
    public sealed class HighlightInfoDto
    {
        #region Properties
        public Object Id { get; set; }
        public String Name { get; set; }
        #endregion

        #region Methods
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override bool Equals(Object obj)
        {
            HighlightInfoDto other = obj as HighlightInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
