﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Compression Transfer Object
    /// </summary>
    [Serializable]
    public class CompressionDto
    {
        #region Properties

        public Int32 Id { get; set; }
        public CompressionDtoKey DtoKey
        {
            get
            {
                return new CompressionDtoKey()
                {
                    Name = this.Name
                };
            }
        }
        public RowVersion RowVersion { get; set; }
        public String Name { get; set; }
        public Int32 Width { get; set; }
        public Int32 Height { get; set; }
        public Int32 ColourDepth { get; set; }
        public Boolean Enabled { get; set; }
        public Boolean MaintainAspectRatio { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns the objects hash code
        /// </summary>
        /// <returns>The objects </returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Compares an object to this one
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            CompressionDto other = obj as CompressionDto;
            if (other != null)
            {
                if (!CompareCompressionProperties(other))
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Compares the properties of a content dto
        /// </summary>
        /// <typeparam name="T">The type of dto to compare</typeparam>
        /// <param name="obj">The dto to compare</param>
        /// <returns>True if the properties are equal, else false</returns>
        protected bool CompareCompressionProperties<T>(T obj)
            where T : CompressionDto
        {
            if (obj.Id != this.Id)
            {
                return false;
            }
            if (obj.RowVersion != this.RowVersion)
            {
                return false;
            }
            if (obj.Name != this.Name)
            {
                return false;
            }
            if (obj.Width != this.Width)
            {
                return false;
            }
            if (obj.Height != this.Height)
            {
                return false;
            }
            if (obj.ColourDepth != this.ColourDepth)
            {
                return false;
            }
            if (obj.MaintainAspectRatio != this.MaintainAspectRatio)
            {
                return false;
            }
            if (obj.Enabled != this.Enabled)
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    public class CompressionDtoKey
    {
        public String Name { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as CompressionDtoKey;
            if (other==null) return false;
            return other.Name==this.Name;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}
