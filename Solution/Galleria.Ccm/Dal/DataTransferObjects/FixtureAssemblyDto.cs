﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// FixtureAssembly Data Transfer Object
    /// </summary>
    [Serializable]
    public class FixtureAssemblyDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public FixtureAssemblyDtoKey DtoKey
        {
            get
            {
                return new FixtureAssemblyDtoKey()
                {
                    Id = this.Id,
                };
            }
        }
        public Object FixturePackageId { get; set; }
        public String Name { get; set; }
        public Single Height { get; set; }
        public Single Width { get; set; }
        public Single Depth { get; set; }
        public Int16 NumberOfComponents { get; set; }
        public Single TotalComponentCost { get; set; }
        public Object ExtendedData { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FixtureAssemblyDto other = obj as FixtureAssemblyDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }

                //FixturePackageId
                if ((other.FixturePackageId != null) && (this.FixturePackageId != null))
                {
                    if (!other.FixturePackageId.Equals(this.FixturePackageId)) { return false; }
                }
                if ((other.FixturePackageId != null) && (this.FixturePackageId == null)) { return false; }
                if ((other.FixturePackageId == null) && (this.FixturePackageId != null)) { return false; }

                if (other.Name != this.Name) { return false; }
                if (other.Height != this.Height) { return false; }
                if (other.Width != this.Width) { return false; }
                if (other.Depth != this.Depth) { return false; }
                if (other.NumberOfComponents != this.NumberOfComponents) { return false; }
                if (other.TotalComponentCost != this.TotalComponentCost) { return false; }

                //ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData != null))
                {
                    if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
                }
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class FixtureAssemblyDtoKey
    {
        #region Properties
        public Int32 Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FixtureAssemblyDtoKey other = obj as FixtureAssemblyDtoKey;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

}
