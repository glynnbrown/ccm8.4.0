﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25919 : L.Ineson
//  Created
#endregion
#region Version History: CCM830
// V8-32357 : M.Brumby
//  [PCR01561] added DisplayName and DisplayDescription
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;


namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public sealed class WorkflowTaskInfoDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public Byte SequenceId { get; set; }
        public String TaskType { get; set; }
        public String DisplayName { get; set; }
        public String DisplayDescription { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the instance has code
        /// </summary>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Compares two instances to each other
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            WorkflowTaskInfoDto other = obj as WorkflowTaskInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.SequenceId != this.SequenceId) return false;
                if (other.TaskType != this.TaskType) return false;
                if (other.DisplayName != this.DisplayName) return false;
                if (other.DisplayDescription != this.DisplayDescription) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
