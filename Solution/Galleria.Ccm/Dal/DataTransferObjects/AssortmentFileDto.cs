﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25454 : J.Pickup
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// AssortmentFile Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class AssortmentFileDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public AssortmentFileDtoKey DtoKey
        {
            get
            {
                return new AssortmentFileDtoKey()
                {
                    FileId = this.FileId
                };
            }
        }
        public Int32 AssortmentId { get; set; }
        public Int32 FileId { get; set; }
        public String FileName { get; set; }
        public String FileType { get; set; }
        public Int64 SizeInBytes { get; set; }
        public String SourceFilePath { get; set; }
        public String UserName { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentFileDto other = obj as AssortmentFileDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.AssortmentId != this.AssortmentId) { return false; }
                if (other.FileId != this.FileId) { return false; }
                if (other.FileName != this.FileName) { return false; }
                if (other.FileType != this.FileType) { return false; }
                if (other.SizeInBytes != this.SizeInBytes) { return false; }
                if (other.SourceFilePath != this.SourceFilePath) { return false; }
                if (other.UserName != this.UserName) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentFileDtoKey
    {
        #region Properties
        public Int32 FileId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                FileId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentFileDtoKey other = obj as AssortmentFileDtoKey;
            if (other != null)
            {
                if (other.FileId != this.FileId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentFileIsSetDto
    {
        #region Properties
        public Boolean IsAssortmentIdSet { get; set; }
        public Boolean IsFileIdSet { get; set; }
        public Boolean IsFileNameSet { get; set; }
        public Boolean IsFileTypeSet { get; set; }
        public Boolean IsSizeInBytesSet { get; set; }
        public Boolean IsSourceFilePathSet { get; set; }
        public Boolean IsUserNameSet { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public AssortmentFileIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public AssortmentFileIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsAssortmentIdSet.GetHashCode() +
                IsFileIdSet.GetHashCode() +
                IsFileNameSet.GetHashCode() +
                IsFileTypeSet.GetHashCode() +
                IsSizeInBytesSet.GetHashCode() +
                IsSourceFilePathSet.GetHashCode() +
                IsUserNameSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentFileIsSetDto other = obj as AssortmentFileIsSetDto;
            if (other != null)
            {
                if (other.IsAssortmentIdSet != this.IsAssortmentIdSet) { return false; }
                if (other.IsFileIdSet != this.IsFileIdSet) { return false; }
                if (other.IsFileNameSet != this.IsFileNameSet) { return false; }
                if (other.IsFileTypeSet != this.IsFileTypeSet) { return false; }
                if (other.IsSizeInBytesSet != this.IsSizeInBytesSet) { return false; }
                if (other.IsSourceFilePathSet != this.IsSourceFilePathSet) { return false; }
                if (other.IsUserNameSet != this.IsUserNameSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsAssortmentIdSet = isSet;
            IsFileIdSet = isSet;
            IsFileNameSet = isSet;
            IsFileTypeSet = isSet;
            IsSizeInBytesSet = isSet;
            IsSourceFilePathSet = isSet;
            IsUserNameSet = isSet;
        }
        #endregion
    }
}
