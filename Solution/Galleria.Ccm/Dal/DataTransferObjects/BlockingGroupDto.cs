﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created (Auto-generated)
// V8-27476 : L.Luong
//      Added BlockPlacementXType and BlockPlacementYType
#endregion

#region Version History : CCM 802
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information
// V8-28995 : A.Kuszyk
//	Removed BlockPlacementX/YType and added Primary/Secondary/Tertiary type.
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// BlockingGroup Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class BlockingGroupDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public BlockingGroupDtoKey DtoKey
        {
            get
            {
                return new BlockingGroupDtoKey()
                {
                    BlockingId = this.BlockingId,
                    Name = this.Name
                };
            }
        }
        [ForeignKey(typeof(BlockingDto), typeof(IBlockingDal), DeleteBehavior.Leave)]
        public Int32 BlockingId { get; set; }
        public String Name { get; set; }
        public Int32 Colour { get; set; }
        public Byte FillPatternType { get; set; }
        public Boolean CanCompromiseSequence { get; set; }
        public Boolean IsRestrictedByComponentType { get; set; }
        public Boolean CanOptimise { get; set; }
        public Boolean CanMerge { get; set; }
        public Byte BlockPlacementPrimaryType { get; set; }
        public Byte BlockPlacementSecondaryType { get; set; }
        public Byte BlockPlacementTertiaryType { get; set; }
        public Single TotalSpacePercentage { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            BlockingGroupDto other = obj as BlockingGroupDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.BlockingId != this.BlockingId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.Colour != this.Colour) { return false; }
                if (other.FillPatternType != this.FillPatternType) { return false; }
                if (other.CanCompromiseSequence != this.CanCompromiseSequence) { return false; }
                if (other.IsRestrictedByComponentType != this.IsRestrictedByComponentType) { return false; }
                if (other.CanOptimise != this.CanOptimise) { return false; }
                if (other.CanMerge != this.CanMerge) { return false; }
                if (other.BlockPlacementPrimaryType != this.BlockPlacementPrimaryType) { return false; }
                if (other.BlockPlacementSecondaryType != this.BlockPlacementSecondaryType) { return false; }
                if (other.BlockPlacementTertiaryType != this.BlockPlacementTertiaryType) { return false; }
                if (other.TotalSpacePercentage != this.TotalSpacePercentage) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


    public class BlockingGroupDtoKey
    {

        #region Properties

        public Int32 BlockingId { get; set; }
        public String Name { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
             BlockingId.GetHashCode() ^
             Name.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            BlockingGroupDtoKey other = obj as BlockingGroupDtoKey;
            if (other != null)
            {

                if (other.BlockingId != this.BlockingId) { return false; }
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

}
