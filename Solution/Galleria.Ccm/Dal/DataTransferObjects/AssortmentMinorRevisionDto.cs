﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
#endregion
#region Version History: (CCM 8.0.3)
// CCM-29214 : D.Pleasance
//  Removed DateDeleted column.
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
// V8-29474 : L.Ineson
//  Removed ucr from dto key.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Assortment data Transfer Object
    /// </summary>
    [Serializable]
    public class AssortmentMinorRevisionDto
    {
        #region Properties
        public Int32 Id { get; set; } 
        public RowVersion RowVersion { get; set; }
        [ForeignKey(typeof(ConsumerDecisionTreeDto), typeof(IConsumerDecisionTreeDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32? ConsumerDecisionTreeId { get; set; }
        public Guid UniqueContentReference { get; set; }
        public String Name { get; set;}
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        [ForeignKey(typeof(ProductGroupDto), typeof(IProductGroupDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32 ProductGroupId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public Guid? ParentUniqueContentReference { get; set; }
        public AssortmentMinorRevisionDtoKey DtoKey
        {
            get
            {
                return new AssortmentMinorRevisionDtoKey()
                {
                    EntityId = this.EntityId,
                    Name = this.Name
                };
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the objects hash code
        /// </summary>
        /// <returns>The objects hash code</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Compares two instance of this type
        /// </summary>
        /// <param name="obj">The object to compare to</param>
        /// <returns>True if equal, else false</returns>
        public override bool Equals(object obj)
        {
            AssortmentMinorRevisionDto other = obj as AssortmentMinorRevisionDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.RowVersion != this.RowVersion) return false;
                if (other.ConsumerDecisionTreeId != this.ConsumerDecisionTreeId) return false;
                if (other.UniqueContentReference != this.UniqueContentReference) return false;
                if (other.Name != this.Name) return false;
                if (other.EntityId != this.EntityId) return false;
                if (other.ProductGroupId != this.ProductGroupId) return false;
                if (other.DateCreated != this.DateCreated) return false;
                if (other.DateLastModified != this.DateLastModified) return false;
                if (other.ParentUniqueContentReference != this.ParentUniqueContentReference) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentMinorRevisionDtoKey
    {
        #region Properties

        public Int32 EntityId { get; set; }
        public String Name { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                EntityId.GetHashCode() +
                Name.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentMinorRevisionDtoKey other = obj as AssortmentMinorRevisionDtoKey;
            if (other != null)
            {
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        #endregion
    }

    [Serializable]
    public class AssortmentMinorRevisionIsSetDto
    {
        #region Properties

        public Boolean IsConsumerDecisionTreeIdSet { get; set; }
        public Boolean IsUniqueContentReferenceSet { get; set; }
        public Boolean IsEntityIdSet { get; set; }
        public Boolean IsProductGroupIdSet { get; set; }

        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public AssortmentMinorRevisionIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public AssortmentMinorRevisionIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsConsumerDecisionTreeIdSet.GetHashCode() +
                IsUniqueContentReferenceSet.GetHashCode() +
                IsEntityIdSet.GetHashCode() +
                IsProductGroupIdSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentMinorRevisionIsSetDto other = obj as AssortmentMinorRevisionIsSetDto;
            if (other != null)
            {
                if (other.IsConsumerDecisionTreeIdSet != this.IsConsumerDecisionTreeIdSet) { return false; }
                if (other.IsUniqueContentReferenceSet != this.IsUniqueContentReferenceSet) { return false; }
                if (other.IsEntityIdSet != this.IsEntityIdSet) { return false; }
                if (other.IsProductGroupIdSet != this.IsProductGroupIdSet) { return false; }     
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsConsumerDecisionTreeIdSet = isSet;
            IsUniqueContentReferenceSet = isSet;
            IsEntityIdSet = isSet;
            IsProductGroupIdSet = isSet;
        }
        #endregion
    }

}
