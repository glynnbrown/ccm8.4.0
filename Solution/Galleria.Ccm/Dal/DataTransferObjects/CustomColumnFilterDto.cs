#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26076 : A.Silva ~ Removed Id Property and DtoKey.
// V8-26201 : A.Silva ~ Readded DtoKey and corrected Equals.

#endregion


#region Version History: (CCM 8.3)
// V8-31542 : L.Ineson
//  Added Id and updated implementation.
#endregion

#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class CustomColumnFilterDto
    {
        #region Properties

        public Int32 Id { get; set; }

        [ForeignKey(typeof (CustomColumnLayoutDto), typeof (ICustomColumnLayoutDal))]
        public Object CustomColumnLayoutId { get; set; }

        public CustomColumnFilterDtoKey DtoKey
        {
            get { return new CustomColumnFilterDtoKey 
            {
                CustomColumnLayoutId = this.CustomColumnLayoutId,
                Path = this.Path,
            }; }
        }

        public String Header { get; set; }
        public String Path { get; set; }
        public String Text { get; set; }

        #endregion

        #region Equals override

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            CustomColumnFilterDto other = obj as CustomColumnFilterDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;

                // CustomColumnLayoutId
                if ((other.CustomColumnLayoutId != null) && (this.CustomColumnLayoutId != null))
                {
                    if (!other.CustomColumnLayoutId.Equals(this.CustomColumnLayoutId)) { return false; }
                }
                if ((other.CustomColumnLayoutId != null) && (this.CustomColumnLayoutId == null)) { return false; }
                if ((other.CustomColumnLayoutId == null) && (this.CustomColumnLayoutId != null)) { return false; }

                if (other.Header != this.Header) return false;
                if (other.Path != this.Path) return false;
                if (other.Text != this.Text) return false;
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        #endregion
    }

    /// <summary>
    /// CustomColumnFilter dto key
    /// </summary>
    [Serializable]
    public sealed class CustomColumnFilterDtoKey
    {
        #region Properties
        public Object CustomColumnLayoutId { get; set; }
        public String Path { get; set; }
        #endregion

        #region Equals override
        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            CustomColumnFilterDtoKey other = obj as CustomColumnFilterDtoKey;
            if (other != null)
            {
                // CustomColumnLayoutId
                if ((other.CustomColumnLayoutId != null) && (this.CustomColumnLayoutId != null))
                {
                    if (!other.CustomColumnLayoutId.Equals(this.CustomColumnLayoutId)) { return false; }
                }
                if ((other.CustomColumnLayoutId != null) && (this.CustomColumnLayoutId == null)) { return false; }
                if ((other.CustomColumnLayoutId == null) && (this.CustomColumnLayoutId != null)) { return false; }

                if (other.Path != this.Path) return false;
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                this.CustomColumnLayoutId.GetHashCode() ^
                this.Path.GetHashCode();
        }
        #endregion
    }
}