﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
// V8-26041 : A.Kuszyk ~ Amended delete behaviour on Product Foreign Key attribute.
// V8-27132 : A.Kuszyk
//  Added Product Gtin property.
#endregion
#region Version History: (CCM 8.0.3)
// V8-29215 : D.Pleasance
//  Removed ConsumerDecisionTree DateDeleted columns
#endregion
#endregion

using System;

using Galleria.Framework.DataStructures;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// ConsumerDecisionTreeNodeProduct Data Transfer Object
    /// </summary>
    [Serializable]
    public class ConsumerDecisionTreeNodeProductDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public ConsumerDecisionTreeNodeProductDtoKey DtoKey
        {
            get
            {
                return new ConsumerDecisionTreeNodeProductDtoKey()
                {
                    ConsumerDecisionTreeNodeId = this.ConsumerDecisionTreeNodeId,
                    ProductId = this.ProductId
                };
            }
        }
        [ForeignKey(typeof(ConsumerDecisionTreeNodeDto), typeof(IConsumerDecisionTreeNodeDal), DeleteBehavior.MoveUpToRoot)]
        public Int32 ConsumerDecisionTreeNodeId { get; set; }
        [ForeignKey(typeof(ProductDto), typeof(IProductDal),DeleteBehavior.Leave)]
        public Int32 ProductId { get; set; }
        [InheritedProperty(typeof(ProductDto), typeof(IProductDal), parentPropertyName: "Gtin", foreignKeyPropertyName: "ProductId")]
        public String ProductGtin { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ConsumerDecisionTreeNodeProductDto other = obj as ConsumerDecisionTreeNodeProductDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.ConsumerDecisionTreeNodeId != this.ConsumerDecisionTreeNodeId) { return false; }
                if (other.ProductId != this.ProductId) { return false; }
                if (other.ProductGtin != this.ProductGtin) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class ConsumerDecisionTreeNodeProductDtoKey
    {
        #region Properties
        public Int32 ConsumerDecisionTreeNodeId { get; set; }
        public Int32 ProductId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                ConsumerDecisionTreeNodeId.GetHashCode() +
                ProductId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ConsumerDecisionTreeNodeProductDtoKey other = obj as ConsumerDecisionTreeNodeProductDtoKey;
            if (other != null)
            {
                if (other.ConsumerDecisionTreeNodeId != this.ConsumerDecisionTreeNodeId) { return false; }
                if (other.ProductId != this.ProductId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
