﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// BlockingDivider Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class BlockingDividerDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public BlockingDividerDtoKey DtoKey
        {
            get
            {
                return new BlockingDividerDtoKey()
                {
                    BlockingId = this.BlockingId,
                    X = this.X,
                    Y = this.Y
                };
            }
        }
        [ForeignKey(typeof(BlockingDto), typeof(IBlockingDal), DeleteBehavior.Leave)]
        public Int32 BlockingId { get; set; }
        public Byte Type { get; set; }
        public Byte Level { get; set; }
        public Single X { get; set; }
        public Single Y { get; set; }
        public Single Length { get; set; }
        public Boolean IsSnapped { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            BlockingDividerDto other = obj as BlockingDividerDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.BlockingId != this.BlockingId) { return false; }
                if (other.Type != this.Type) { return false; }
                if (other.Level != this.Level) { return false; }
                if (other.X != this.X) { return false; }
                if (other.Y != this.Y) { return false; }
                if (other.Length != this.Length) { return false; }
                if (other.IsSnapped != this.IsSnapped) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


    public class BlockingDividerDtoKey
    {

        #region Properties

        public Int32 BlockingId { get; set; }
        public Single X { get; set; }
        public Single Y { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
             BlockingId.GetHashCode() ^
             X.GetHashCode() ^
             Y.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            BlockingDividerDtoKey other = obj as BlockingDividerDtoKey;
            if (other != null)
            {

                if (other.BlockingId != this.BlockingId) { return false; }
                if (other.X != this.X) { return false; }
                if (other.Y != this.Y) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


}
