﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25879 : N.Haywood
//		Created (Auto-generated)
// V8-25879 : M.Shelley
//  Changed the property set to reflect DB changes
#endregion

#region Version History: (CCM 8.0.2)
// CCM-29026 : D.Pleasance
//  Amended DtoKey to include location
#endregion 

#region Version History: (CCM 8.0.3)
// V8-28242 : M.Shelley
//  Added DateCommunicated and DateLive fields
// V8-30508 : M.Shelley
//  Added a new field : PublishStatus
#endregion

#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// LocationPlanAssignment Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class LocationPlanAssignmentDto
    {
        #region Properties
        public Int32 Id { get; set; }

        public LocationPlanAssignmentDtoKey DtoKey
        {
            get
            {
                return new LocationPlanAssignmentDtoKey()
                {
                    PlanogramId = this.PlanogramId,
                    ProductGroupId = this.ProductGroupId,
                    LocationId = this.LocationId
                };
            }
        }
        //[ForeignKey(typeof(PlanogramDto), typeof(IPlanogramDal))]
        public Int32 PlanogramId { get; set; }
        public String PlanogramName { get; set; }

        [ForeignKey(typeof(ProductGroupDto), typeof(IProductGroupDal), DeleteBehavior.Leave)]
        public Int32 ProductGroupId { get; set; }

        [ForeignKey(typeof(LocationDto), typeof(ILocationDal), DeleteBehavior.Leave)]
        public Int16 LocationId { get; set; }

        public Int32? AssignedByUserId { get; set; }
        public Int32? PublishUserId { get; set; }

        public DateTime? DateAssigned { get; set; }
        public DateTime? DatePublished { get; set; }

        public DateTime? DateCommunicated { get; set; }
        public DateTime? DateLive { get; set; }

        public Byte? PublishType { get; set; }
        public String OutputDestination { get; set; }
        public Byte? PublishStatus { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            LocationPlanAssignmentDto other = obj as LocationPlanAssignmentDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.PlanogramId != this.PlanogramId) { return false; }
                if (other.PlanogramName != this.PlanogramName) { return false; }
                if (other.ProductGroupId != this.ProductGroupId) { return false; }
                if (other.LocationId != this.LocationId) { return false; }
                if (other.AssignedByUserId != this.AssignedByUserId) { return false; }
                if (other.PublishUserId != this.PublishUserId) { return false; }
                if (other.DateAssigned != this.DateAssigned) { return false; }
                if (other.DatePublished != this.DatePublished) { return false; }
                if (other.DateCommunicated != this.DateCommunicated) { return false; }
                if (other.DateLive != this.DateLive) { return false; }
                if (other.PublishType != this.PublishType) { return false; }
                if (other.OutputDestination != this.OutputDestination) { return false; }
                if (other.PublishStatus != this.PublishStatus) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class LocationPlanAssignmentDtoKey
    {
        #region Properties
        public Int32 PlanogramId { get; set; }
        public Int32 ProductGroupId { get; set; }
        public Int16 LocationId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this instance
        /// </summary>
        public override Int32 GetHashCode()
        {
            return this.PlanogramId.GetHashCode() + this.ProductGroupId.GetHashCode();
        }

        /// <summary>
        /// Compares two instances
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            LocationPlanAssignmentDtoKey other = obj as LocationPlanAssignmentDtoKey;
            if (other != null)
            {
                if (other.PlanogramId != this.PlanogramId) { return false; }
                if (other.ProductGroupId != this.ProductGroupId) { return false; }
                if (other.LocationId != this.LocationId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    /// <summary>
    /// Code Criteria for use when searching
    /// </summary>
    [Serializable]
    public struct LocationPlanAssignmentSearchCriteriaDto
    {
        public Int32 LocationPlanAssignmentId { get; set; }
        public String LocationCode { get; set; }
        public String ProductGroupCode { get; set; }
        public String PlanogramName { get; set; }

        public LocationPlanAssignmentSearchCriteriaDto(
            String locationCode,
            String productGroupCode,
            String planogramName = null)
            : this()
        {
            LocationCode = locationCode;
            ProductGroupCode = productGroupCode;
            PlanogramName = planogramName;
        }
        
        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + this.LocationCode.ToLowerInvariant().GetHashCode();
                hash = hash * 23 + this.ProductGroupCode.ToLowerInvariant().GetHashCode();
                if (this.PlanogramName != null)
                {
                    hash = hash * 23 + this.PlanogramName.ToLowerInvariant().GetHashCode();
                }
                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            LocationPlanAssignmentSearchCriteriaDto other = (LocationPlanAssignmentSearchCriteriaDto)obj;

            //Dont compare id as this is only used in the Location Space import
            //for lookup purposes
            StringComparer s = StringComparer.OrdinalIgnoreCase;

            if (s.Compare(other.LocationCode, this.LocationCode) != 0) { return false; }
            if (s.Compare(other.ProductGroupCode, this.ProductGroupCode) != 0) { return false; }
            if (s.Compare(other.PlanogramName, this.PlanogramName) != 0) { return false; }

            return true;
        }

        public bool Equals(LocationPlanAssignmentSearchCriteriaDto other)
        {
            StringComparer s = StringComparer.OrdinalIgnoreCase;

            if (s.Compare(other.LocationCode, this.LocationCode) != 0) { return false; }
            if (s.Compare(other.ProductGroupCode, this.ProductGroupCode) != 0) { return false; }
            if (s.Compare(other.PlanogramName, this.PlanogramName) != 0) { return false; }

            return true;
        }
    }
}