﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26474 : A.Silva ~ Created (Auto-generated)
// V8-26815 : A.Silva ~ Changed ValidationTemplateId to be of type Object (as it can either be a normal integer Id or a file path Id).
// V8-26981 : A.Silva ~ Corrected Equality implementation.
// V8-26812 : A.Silva ~ Added ValidationType property.
// V8-27076 : A.Silva ~ Corrected Equality for Object properties.

#endregion

#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    ///     ValidationTemplateGroup Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class ValidationTemplateGroupDto
    {
        #region Properties

        public ValidationTemplateGroupDtoKey DtoKey
        {
            get
            {
                return new ValidationTemplateGroupDtoKey
                {
                    Name = Name,
                    ValidationTemplateId = ValidationTemplateId
                };
            }
        }

        public Int32 Id { get; set; }

        public String Name { get; set; }
        public Single Threshold1 { get; set; }
        public Single Threshold2 { get; set; }

        [ForeignKey(typeof (ValidationTemplateDto), typeof (IValidationTemplateDal), DeleteBehavior.Cascade)]
        public Object ValidationTemplateId { get; set; }

        /// <summary>
        ///     Gets or sets the validation calculation type to be used with this instance.
        /// </summary>
        public Byte ValidationType { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            var other = obj as ValidationTemplateGroupDto;
            return other != null &&
                   other.Id == Id &&
                   Equals(other.ValidationTemplateId,ValidationTemplateId) &&
                   other.Name == Name &&
                   other.Threshold1 == Threshold1 &&
                   other.Threshold2 == Threshold2 &&
                   other.ValidationType == ValidationType;
        }

        /// <summary>
        ///     Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode() +
                   ValidationTemplateId.GetHashCode() +
                   Name.GetHashCode() +
                   Threshold1.GetHashCode() +
                   Threshold2.GetHashCode() + 
                   ValidationType.GetHashCode();
        }

        #endregion
    }

    [Serializable]
    public sealed class ValidationTemplateGroupDtoKey
    {
        #region Properties

        public String Name { private get; set; }

        [ForeignKey(typeof(ValidationTemplateDto), typeof(IValidationTemplateDal), DeleteBehavior.Cascade)]
        public Object ValidationTemplateId { private get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            var other = obj as ValidationTemplateGroupDtoKey;
            return other != null &&
                   other.Name == Name &&
                   Equals(other.ValidationTemplateId,ValidationTemplateId);
        }

        /// <summary>
        ///     Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Name.GetHashCode() +
                   ValidationTemplateId.GetHashCode();
        }

        #endregion
    }
}