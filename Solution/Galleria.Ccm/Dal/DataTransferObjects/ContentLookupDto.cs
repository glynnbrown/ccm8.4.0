﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26520 : J.Pickup
//  Created Initial Version
// V8-27891 : J.Pickup
//  RenumberingStrategyName & validation added. Fixed bug with DtoKey which resulted in a false negative.
#endregion
#region Version History: (CCM 801)
// CCM-28519 :J.Pickup
//	Removed Date Deleted
#endregion
#region Version History: (CCM 803)
// V8-29746 : A.Probyn
//  Updated to use data Id's as well as name
#endregion
#region Version History: (CCM 830)
// V8-31831 : A.Probyn
//  Updated to include PlanogramNameTemplate
// V8-32810 : M.Pettit
//  Added PrintTemplate_Id
#endregion

#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// ContentLookup Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class ContentLookupDto
    {
        #region Properties

        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        [ForeignKey(typeof(UserDto), typeof(IUserDal), DeleteBehavior.Leave)]
        public Int32 UserId { get; set; }
        [InheritedProperty(typeof(PlanogramDto), typeof(IPlanogramDal),  ForeignKeyPropertyName = "PlanogramId", ParentPropertyName = "Name")]
        public String PlanogramName { get; set; }
        [ForeignKey(typeof(PlanogramDto), typeof(IPlanogramDal), DeleteBehavior.Cascade)]
        public Int32 PlanogramId { get; set; }
        [InheritedProperty(typeof(ProductUniverseDto), typeof(IProductUniverseDal), ForeignKeyPropertyName = "ProductUniverseId", ParentPropertyName = "Name")]
        public String ProductUniverseName { get; set; }
        [ForeignKey(typeof(ProductUniverseDto), typeof(IProductUniverseDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32? ProductUniverseId { get; set; }
        [InheritedProperty(typeof(AssortmentDto), typeof(IAssortmentDal), ForeignKeyPropertyName = "AssortmentId", ParentPropertyName = "Name")]
        public String AssortmentName { get; set; }
        [ForeignKey(typeof(AssortmentDto), typeof(IAssortmentDal), DeleteBehavior.LeaveButNullOffParentReference, CommonParentDtoType = typeof(AssortmentDto))]
        public Int32? AssortmentId { get; set; }
        [InheritedProperty(typeof(PlanogramDto), typeof(IPlanogramDal), ForeignKeyPropertyName = "BlockingId", ParentPropertyName = "Name")]
        public String BlockingName { get; set; }
        [ForeignKey(typeof(PlanogramDto), typeof(IPlanogramDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32? BlockingId { get; set; }
        [InheritedProperty(typeof(PlanogramDto), typeof(IPlanogramDal), ForeignKeyPropertyName = "SequenceId", ParentPropertyName = "Name")]
        public String SequenceName { get; set; }
        [ForeignKey(typeof(PlanogramDto), typeof(IPlanogramDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32? SequenceId { get; set; }
        [InheritedProperty(typeof(MetricProfileDto), typeof(IMetricProfileDal), ForeignKeyPropertyName = "MetricProfileId", ParentPropertyName = "Name")]
        public String MetricProfileName { get; set; }
        [ForeignKey(typeof(MetricProfileDto), typeof(IMetricProfileDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32? MetricProfileId { get; set; }
        [InheritedProperty(typeof(InventoryProfileDto), typeof(IInventoryProfileDal), ForeignKeyPropertyName = "InventoryProfileId", ParentPropertyName = "Name")]
        public String InventoryProfileName { get; set; }
        [ForeignKey(typeof(InventoryProfileDto), typeof(IInventoryProfileDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32? InventoryProfileId { get; set; }
        [InheritedProperty(typeof(ConsumerDecisionTreeDto), typeof(IConsumerDecisionTreeDal), ForeignKeyPropertyName = "ConsumerDecisionTreeId", ParentPropertyName = "Name")]
        public String ConsumerDecisionTreeName { get; set; }
        [ForeignKey(typeof(ConsumerDecisionTreeDto), typeof(IConsumerDecisionTreeDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32? ConsumerDecisionTreeId { get; set; }
        [InheritedProperty(typeof(AssortmentMinorRevisionDto), typeof(IAssortmentMinorRevisionDal), ForeignKeyPropertyName = "AssortmentMinorRevisionId", ParentPropertyName = "Name")]
        public String AssortmentMinorRevisionName { get; set; }
        [ForeignKey(typeof(AssortmentMinorRevisionDto), typeof(IAssortmentMinorRevisionDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32? AssortmentMinorRevisionId { get; set; }
        [InheritedProperty(typeof(PerformanceSelectionDto), typeof(IPerformanceSelectionDal), ForeignKeyPropertyName = "PerformanceSelectionId", ParentPropertyName = "Name")]
        public String PerformanceSelectionName { get; set; }
        [ForeignKey(typeof(PerformanceSelectionDto), typeof(IConsumerDecisionTreeDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32? PerformanceSelectionId { get; set; }
        [InheritedProperty(typeof(ClusterSchemeDto), typeof(IClusterSchemeDal), ForeignKeyPropertyName = "ClusterSchemeId", ParentPropertyName = "Name")]
        public String ClusterSchemeName { get; set; }
        [ForeignKey(typeof(ClusterSchemeDto), typeof(IClusterSchemeDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32? ClusterSchemeId { get; set; }
        [InheritedProperty(typeof(ClusterDto), typeof(IClusterDal), ForeignKeyPropertyName = "ClusterId", ParentPropertyName = "Name")]
        public String ClusterName { get; set; }
        [ForeignKey(typeof(ClusterDto), typeof(IClusterDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32? ClusterId { get; set; }
        [InheritedProperty(typeof(RenumberingStrategyDto), typeof(IRenumberingStrategyDal), ForeignKeyPropertyName = "RenumberingStrategyId", ParentPropertyName = "Name")]
        public String RenumberingStrategyName { get; set; }
        [ForeignKey(typeof(RenumberingStrategyDto), typeof(IRenumberingStrategyDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32? RenumberingStrategyId { get; set; }
        [InheritedProperty(typeof(ValidationTemplateDto), typeof(IValidationTemplateDal), ForeignKeyPropertyName = "ValidationTemplateId", ParentPropertyName = "Name")]
        public String ValidationTemplateName { get; set; }
        [ForeignKey(typeof(ValidationTemplateDto), typeof(IValidationTemplateDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32? ValidationTemplateId { get; set; }
        [InheritedProperty(typeof(PlanogramNameTemplateDto), typeof(IPlanogramNameTemplateDal), ForeignKeyPropertyName = "PlanogramNameTemplateId", ParentPropertyName = "Name")]
        public String PlanogramNameTemplateName { get; set; }
        [ForeignKey(typeof(PlanogramNameTemplateDto), typeof(IPlanogramNameTemplateDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32? PlanogramNameTemplateId { get; set; }
        [InheritedProperty(typeof(PrintTemplateDto), typeof(IPrintTemplateDal), ForeignKeyPropertyName = "PrintTemplateId", ParentPropertyName = "Name")]
        public String PrintTemplateName { get; set; }
        [ForeignKey(typeof(PrintTemplateDto), typeof(IPrintTemplateDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32? PrintTemplateId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        public ContentLookupDtoKey DtoKey
        {
            get
            {
                return new ContentLookupDtoKey()
                {
                    PlanogramId = this.PlanogramId
                };
            }
        }
       
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ContentLookupDto other = obj as ContentLookupDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.UserId != this.UserId) { return false; }
                if (other.PlanogramName != this.PlanogramName) { return false; }
                if (other.PlanogramId != this.PlanogramId) { return false; }
                if (other.ProductUniverseName != this.ProductUniverseName) { return false; }
                if (other.ProductUniverseId != this.ProductUniverseId) { return false; }
                if (other.AssortmentName != this.AssortmentName) { return false; }
                if (other.AssortmentId != this.AssortmentId) { return false; }
                if (other.BlockingName != this.BlockingName) { return false; }
                if (other.BlockingId != this.BlockingId) { return false; }
                if (other.SequenceName != this.SequenceName) { return false; }
                if (other.SequenceId != this.SequenceId) { return false; }
                if (other.MetricProfileName != this.MetricProfileName) { return false; }
                if (other.MetricProfileId != this.MetricProfileId) { return false; }
                if (other.InventoryProfileName != this.InventoryProfileName) { return false; }
                if (other.InventoryProfileId != this.InventoryProfileId) { return false; }
                if (other.ConsumerDecisionTreeName != this.ConsumerDecisionTreeName) { return false; }
                if (other.ConsumerDecisionTreeId != this.ConsumerDecisionTreeId) { return false; }
                if (other.AssortmentMinorRevisionName != this.AssortmentMinorRevisionName) { return false; }
                if (other.AssortmentMinorRevisionId != this.AssortmentMinorRevisionId) { return false; }
                if (other.ClusterSchemeName != this.ClusterSchemeName) { return false; }
                if (other.ClusterSchemeId != this.ClusterSchemeId) { return false; }
                if (other.ClusterName != this.ClusterName) { return false; }
                if (other.ClusterId != this.ClusterId) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.PerformanceSelectionName != this.PerformanceSelectionName) { return false; }
                if (other.PerformanceSelectionId != this.PerformanceSelectionId) { return false; }
                if (other.RenumberingStrategyName != this.RenumberingStrategyName) { return false; }
                if (other.RenumberingStrategyId != this.RenumberingStrategyId) { return false; }
                if (other.ValidationTemplateName != this.ValidationTemplateName) { return false; }
                if (other.ValidationTemplateId != this.ValidationTemplateId) { return false; }
                if (other.PlanogramNameTemplateName != this.PlanogramNameTemplateName) { return false; }
                if (other.PlanogramNameTemplateId != this.PlanogramNameTemplateId) { return false; }
                if (other.PrintTemplateName != this.PrintTemplateName) { return false; }
                if (other.PrintTemplateId != this.PrintTemplateId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class ContentLookupDtoKey
    {
        #region Properties

        public Int32 PlanogramId { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                PlanogramId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ContentLookupDtoKey other = obj as ContentLookupDtoKey;
            if (other != null)
            {
                if (other.PlanogramId != this.PlanogramId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


    [Serializable]
    public class ContentLookupIsSetDto
    {
        #region Properties

        public Boolean IsUserIdSet { get; set; }
        public Boolean IsPlanogramIdSet { get; set; }
        public Boolean IsProductUniverseIdSet { get; set; }
        public Boolean IsBlockingIdSet { get; set; }
        public Boolean IsSequenceIdSet { get; set; }
        public Boolean IsMetricProfileIdSet { get; set; }
        public Boolean IsInventoryProfileIdSet { get; set; }
        public Boolean IsConsumerDecisionTreeIdSet { get; set; }
        public Boolean IsClusterSchemeIdSet { get; set; }
        public Boolean IsClusterIdSet { get; set; }
        public Boolean IsMinorAssortmentIdSet { get; set; }
        public Boolean IsPerformanceSelectionIdSet { get; set; }
        public Boolean IsEntityIdSet { get; set; }
        public Boolean IsRenumberingStrategyIdSet { get; set; }
        public Boolean IsValidationTemplateIdSet { get; set; }
        public Boolean IsPlanogramNameTemplateIdSet { get; set; }
        public Boolean IsPrintTemplateIdSet { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Default
        /// </summary>
        public ContentLookupIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public ContentLookupIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsUserIdSet.GetHashCode() +
                IsPlanogramIdSet.GetHashCode() +
                IsProductUniverseIdSet.GetHashCode() +
                IsBlockingIdSet.GetHashCode() +
                IsSequenceIdSet.GetHashCode() +
                IsMetricProfileIdSet.GetHashCode() +
                IsInventoryProfileIdSet.GetHashCode() +
                IsConsumerDecisionTreeIdSet.GetHashCode() +
                IsClusterSchemeIdSet.GetHashCode() +
                IsClusterIdSet.GetHashCode() +
                IsPerformanceSelectionIdSet.GetHashCode() +
                IsEntityIdSet.GetHashCode() +
                IsMinorAssortmentIdSet.GetHashCode() +
                IsRenumberingStrategyIdSet.GetHashCode() +
                IsValidationTemplateIdSet.GetHashCode() +
                IsPlanogramNameTemplateIdSet.GetHashCode() +
                IsPrintTemplateIdSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ContentLookupIsSetDto other = obj as ContentLookupIsSetDto;
            if (other != null)
            {
                if (other.IsEntityIdSet != this.IsEntityIdSet) { return false; }
                if (other.IsInventoryProfileIdSet != this.IsInventoryProfileIdSet) { return false; }
                if (other.IsBlockingIdSet != this.IsBlockingIdSet) { return false; }
                if (other.IsMetricProfileIdSet != this.IsMetricProfileIdSet) { return false; }
                if (other.IsPlanogramIdSet != this.IsPlanogramIdSet) { return false; }
                if (other.IsProductUniverseIdSet != this.IsProductUniverseIdSet) { return false; }
                if (other.IsSequenceIdSet != this.IsSequenceIdSet) { return false; }
                if (other.IsUserIdSet != this.IsUserIdSet) { return false; }
                if (other.IsConsumerDecisionTreeIdSet != this.IsConsumerDecisionTreeIdSet) { return false; }
                if (other.IsPerformanceSelectionIdSet != this.IsPerformanceSelectionIdSet) { return false; }
                if (other.IsClusterIdSet != this.IsClusterIdSet) { return false; }
                if (other.IsClusterSchemeIdSet != this.IsClusterSchemeIdSet) { return false; }
                if (other.IsMinorAssortmentIdSet != this.IsMinorAssortmentIdSet) { return false; }
                if (other.IsRenumberingStrategyIdSet != this.IsRenumberingStrategyIdSet) { return false; }
                if (other.IsValidationTemplateIdSet != this.IsValidationTemplateIdSet) { return false; }
                if (other.IsPlanogramNameTemplateIdSet != this.IsPlanogramNameTemplateIdSet) { return false; }
                if (other.IsPrintTemplateIdSet != this.IsPrintTemplateIdSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsEntityIdSet = isSet;
            IsInventoryProfileIdSet = isSet;
            IsBlockingIdSet = isSet;
            IsMetricProfileIdSet = isSet;
            IsPlanogramIdSet = isSet;
            IsProductUniverseIdSet = isSet;
            IsUserIdSet = isSet;
            IsConsumerDecisionTreeIdSet = isSet;
            IsPerformanceSelectionIdSet = isSet;
            IsClusterIdSet = isSet;
            IsClusterSchemeIdSet = isSet;
            IsSequenceIdSet = isSet;
            IsMinorAssortmentIdSet = isSet;
            IsRenumberingStrategyIdSet = isSet;
            IsValidationTemplateIdSet = isSet;
            IsPlanogramNameTemplateIdSet = isSet;
            IsPrintTemplateIdSet = isSet;
        }
        #endregion
    }
}
