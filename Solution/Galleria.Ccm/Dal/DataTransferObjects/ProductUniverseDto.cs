﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25630 : A.Kuszyk
//  Created (copied from SA).
#endregion
#region Version History: (CCM 8.03)
// V8-29257 : L.Ineson
// Removed date deleted
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion

using System;

using Galleria.Framework.DataStructures;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// ProductUniverse Data Transfer Object
    /// </summary>
    [Serializable]
    public class ProductUniverseDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public ProductUniverseDtoKey DtoKey
        {
            get
            {
                return new ProductUniverseDtoKey()
                {
                    Name = this.Name,
                    EntityId = this.EntityId,
                    UniqueContentReference = this.UniqueContentReference
                };
            }
        }
        public Guid UniqueContentReference { get; set; }
        public String Name { get; set; }
        [ForeignKey(typeof(ProductGroupDto), typeof(IProductGroupDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32? ProductGroupId { get; set; }
        public Boolean IsMaster { get; set; }
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public Guid? ParentUniqueContentReference { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ProductUniverseDto other = obj as ProductUniverseDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.UniqueContentReference != this.UniqueContentReference) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.ProductGroupId != this.ProductGroupId) { return false; }
                if (other.IsMaster != this.IsMaster) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.ParentUniqueContentReference != this.ParentUniqueContentReference) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class ProductUniverseDtoKey
    {
        #region Properties
        public String Name { get; set; }
        public Guid UniqueContentReference { get; set; }
        public Int32 EntityId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
             Name.GetHashCode() +
             UniqueContentReference.GetHashCode() +
             EntityId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ProductUniverseDtoKey other = obj as ProductUniverseDtoKey;
            if (other != null)
            {

                if (other.Name != this.Name) { return false; }
                if (other.UniqueContentReference != this.UniqueContentReference) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class ProductUniverseIsSetDto
    {
        #region Properties
        public Boolean IsUniqueContentReferenceSet { get; set; }
        public Boolean IsNameSet { get; set; }
        public Boolean IsProductGroupIdSet { get; set; }
        public Boolean IsIsMasterSet { get; set; }
        public Boolean IsEntityIdSet { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public ProductUniverseIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public ProductUniverseIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsUniqueContentReferenceSet.GetHashCode() +
                IsNameSet.GetHashCode() +
                IsProductGroupIdSet.GetHashCode() +
                IsIsMasterSet.GetHashCode() +
                IsEntityIdSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ProductUniverseIsSetDto other = obj as ProductUniverseIsSetDto;
            if (other != null)
            {

                if (other.IsUniqueContentReferenceSet != this.IsUniqueContentReferenceSet) { return false; }
                if (other.IsNameSet != this.IsNameSet) { return false; }
                if (other.IsProductGroupIdSet != this.IsProductGroupIdSet) { return false; }
                if (other.IsIsMasterSet != this.IsIsMasterSet) { return false; }
                if (other.IsEntityIdSet != this.IsEntityIdSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsUniqueContentReferenceSet = isSet;
            IsNameSet = isSet;
            IsProductGroupIdSet = isSet;
            IsIsMasterSet = isSet;
            IsEntityIdSet = isSet;
        }
        #endregion
    }
}
