﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class SyncTargetGfsPerformanceDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public Int32 SyncTargetId { get; set; }
        public RowVersion RowVersion { get; set; }
        public String EntityName { get; set; }
        public Int32 SourceId { get; set; }
        public String SourceName { get; set; }
        public Byte SourceType { get; set; }
        public Byte? SourceSyncPeriod { get; set; }

        public SyncTargetGfsPerformanceDtoKey DtoKey
        {
            get
            {
                return new SyncTargetGfsPerformanceDtoKey()
                {
                    SourceId = this.SourceId,
                    SyncTargetId = this.SyncTargetId
                };
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Returns the hash code for this instance
        /// </summary>
        /// <returns>The instance hash code</returns>
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Determines if an instance is equal to this one
        /// </summary>
        /// <param name="obj">The instance to compare to</param>
        /// <returns>True if equal, else false</returns>
        public override bool Equals(object obj)
        {
            SyncTargetGfsPerformanceDto other = obj as SyncTargetGfsPerformanceDto;
            if (other != null)
            {
                if (other.Id != this.Id)
                {
                    return false;
                }
                if (other.EntityName != this.EntityName)
                {
                    return false;
                }
                if (other.RowVersion != this.RowVersion)
                {
                    return false;
                }
                if (other.SourceId != this.SourceId)
                {
                    return false;
                }
                if (other.SourceName != this.SourceName)
                {
                    return false;
                }
                if (other.SourceType != this.SourceType)
                {
                    return false;
                }
                if (other.SourceSyncPeriod != this.SourceSyncPeriod)
                {
                    return false;
                }
                if (other.SyncTargetId != this.SyncTargetId)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class SyncTargetGfsPerformanceIsSetDto
    {
        #region Properties
        public Boolean IsIdSet { get; set; }
        public Boolean IsSyncTargetIdSet { get; set; }
        public Boolean IsRowVersionSet { get; set; }
        public Boolean IsSourceTypeSet { get; set; }
        public Boolean IsEntityNameSet { get; set; }
        public Boolean IsSourceIdSet { get; set; }
        public Boolean IsSourceNameSet { get; set; }
        public Boolean IsSourceSyncPeriodSet { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the hash code for this instance
        /// </summary>
        /// <returns>The instance hash code</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Determines if an instance is equal to this one
        /// </summary>
        /// <param name="obj">The instance to compare to</param>
        /// <returns>True if equal, else false</returns>
        public override bool Equals(object obj)
        {
            SyncTargetGfsPerformanceIsSetDto other = obj as SyncTargetGfsPerformanceIsSetDto;
            if (other != null)
            {
                if (other.IsSourceTypeSet != this.IsSourceTypeSet)
                {
                    return false;
                }
                if (other.IsEntityNameSet != this.IsEntityNameSet)
                {
                    return false;
                }
                if (other.IsIdSet != this.IsIdSet)
                {
                    return false;
                }
                if (other.IsSourceIdSet != this.IsSourceIdSet)
                {
                    return false;
                }
                if (other.IsSourceNameSet != this.IsSourceNameSet)
                {
                    return false;
                }
                if (other.IsSourceSyncPeriodSet != this.IsSourceSyncPeriodSet)
                {
                    return false;
                }
                if (other.IsSyncTargetIdSet != this.IsSyncTargetIdSet)
                {
                    return false;
                }
                if (other.IsRowVersionSet != this.IsRowVersionSet)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class SyncTargetGfsPerformanceDtoKey
    {
        #region Properties
        public Int32 SyncTargetId { get; set; }
        public Int32 SourceId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return SyncTargetId.GetHashCode() +
                SourceId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            SyncTargetGfsPerformanceDtoKey other = obj as SyncTargetGfsPerformanceDtoKey;
            if (other != null)
            {

                if (other.SyncTargetId != this.SyncTargetId) { return false; }
                if (other.SourceId != this.SourceId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}