﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25454 : J.Pickup
//		Created (Auto-generated)
// CCM-26322 : L.Ineson
//  Made ProductGroupId not null to match table script.
#endregion
#region Version History: (CCM CCM803)
// CCM-29134 : D.Pleasance
//  Removed DateDeleted column.
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Assortment Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class AssortmentDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public AssortmentDtoKey DtoKey
        {
            get
            {
                return new AssortmentDtoKey()
                {
                    EntityId = this.EntityId,
                    Name = this.Name
                };
            }
        }
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        [ForeignKey(typeof(ProductGroupDto), typeof(IProductGroupDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32 ProductGroupId { get; set; }

        [ForeignKey(typeof(ConsumerDecisionTreeDto), typeof(IConsumerDecisionTreeDal), DeleteBehavior.LeaveButNullOffParentReference)]
        public Int32? ConsumerDecisionTreeId { get; set; }
        public String Name { get; set; }
        public Guid UniqueContentReference { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public Guid? ParentUniqueContentReference { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentDto other = obj as AssortmentDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.ProductGroupId != this.ProductGroupId) { return false; }
                if (other.ConsumerDecisionTreeId != this.ConsumerDecisionTreeId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.UniqueContentReference != this.UniqueContentReference) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.ParentUniqueContentReference != this.ParentUniqueContentReference) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentDtoKey
    {
        #region Properties

        public Int32 EntityId { get; set; }
        public String Name { get; set; }
        public Guid UniqueContentReference { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                EntityId.GetHashCode() + Name.GetHashCode() + UniqueContentReference.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentDtoKey other = obj as AssortmentDtoKey;
            if (other != null)
            {
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.UniqueContentReference != this.UniqueContentReference) { return false; } 
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentIsSetDto
    {
        #region Properties
        public Boolean IsEntityIdSet { get; set; }
        public Boolean IsProductGroupIdSet { get; set; }
        public Boolean IsConsumerDecisionTreeIdSet { get; set; }
        public Boolean IsNameSet { get; set; }
        public Boolean IsUniqueContentReferenceSet { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public AssortmentIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public AssortmentIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsEntityIdSet.GetHashCode() +
                IsProductGroupIdSet.GetHashCode() +
                IsConsumerDecisionTreeIdSet.GetHashCode() +
                IsNameSet.GetHashCode() +
                IsUniqueContentReferenceSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentIsSetDto other = obj as AssortmentIsSetDto;
            if (other != null)
            {
                if (other.IsEntityIdSet != this.IsEntityIdSet) { return false; }
                if (other.IsProductGroupIdSet != this.IsProductGroupIdSet) { return false; }
                if (other.IsConsumerDecisionTreeIdSet != this.IsConsumerDecisionTreeIdSet) { return false; }
                if (other.IsNameSet != this.IsNameSet) { return false; }
                if (other.IsUniqueContentReferenceSet != this.IsUniqueContentReferenceSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsEntityIdSet = isSet;
            IsProductGroupIdSet = isSet;
            IsConsumerDecisionTreeIdSet = isSet;
            IsNameSet = isSet;
            IsUniqueContentReferenceSet = isSet;
        }
        #endregion
    }
}
