﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// AssortmentLocationBuddy Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class AssortmentLocationBuddyDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public AssortmentLocationBuddyDtoKey DtoKey
        {
            get
            {
                return new AssortmentLocationBuddyDtoKey()
                {
                    LocationId = this.LocationId,
                    AssortmentId = this.AssortmentId
                };
            }
        }
        [ForeignKey(typeof(LocationDto), typeof(ILocationDal), DeleteBehavior.Leave)]
        public Int16 LocationId { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal), ForeignKeyPropertyName = "LocationId", ParentPropertyName = "Code")]
        public String LocationCode { get; set; }
        public Byte TreatmentType { get; set; }
        [ForeignKey(typeof(LocationDto), typeof(ILocationDal), DeleteBehavior.Leave)]
        public Int16? S1LocationId { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal), ForeignKeyPropertyName = "S1LocationId", ParentPropertyName = "Code")]
        public String S1LocationCode { get; set; }
        public Single? S1Percentage { get; set; }
        [ForeignKey(typeof(LocationDto), typeof(ILocationDal), DeleteBehavior.Leave)]
        public Int16? S2LocationId { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal), ForeignKeyPropertyName = "S2LocationId", ParentPropertyName = "Code")]
        public String S2LocationCode { get; set; }
        public Single? S2Percentage { get; set; }
        [ForeignKey(typeof(LocationDto), typeof(ILocationDal), DeleteBehavior.Leave)]
        public Int16? S3LocationId { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal), ForeignKeyPropertyName = "S3LocationId", ParentPropertyName = "Code")]
        public String S3LocationCode { get; set; }
        public Single? S3Percentage { get; set; }
        [ForeignKey(typeof(LocationDto), typeof(ILocationDal), DeleteBehavior.Leave)]
        public Int16? S4LocationId { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal), ForeignKeyPropertyName = "S4LocationId", ParentPropertyName = "Code")]
        public String S4LocationCode { get; set; }
        public Single? S4Percentage { get; set; }
        [ForeignKey(typeof(LocationDto), typeof(ILocationDal), DeleteBehavior.Leave)]
        public Int16? S5LocationId { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal), ForeignKeyPropertyName = "S5LocationId", ParentPropertyName = "Code")]
        public String S5LocationCode { get; set; }
        public Single? S5Percentage { get; set; }
        [ForeignKey(typeof(AssortmentDto), typeof(ILocationDal), DeleteBehavior.Cascade)]
        public Int32 AssortmentId { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentLocationBuddyDto other = obj as AssortmentLocationBuddyDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.LocationId != this.LocationId) { return false; }
                if (other.LocationCode != this.LocationCode) { return false; }
                if (other.TreatmentType != this.TreatmentType) { return false; }
                if (other.S1LocationId != this.S1LocationId) { return false; }
                if (other.S1LocationCode != this.S1LocationCode) { return false; }
                if (other.S1Percentage != this.S1Percentage) { return false; }
                if (other.S2LocationId != this.S2LocationId) { return false; }
                if (other.S2LocationCode != this.S2LocationCode) { return false; }
                if (other.S2Percentage != this.S2Percentage) { return false; }
                if (other.S3LocationId != this.S3LocationId) { return false; }
                if (other.S3LocationCode != this.S3LocationCode) { return false; }
                if (other.S3Percentage != this.S3Percentage) { return false; }
                if (other.S4LocationId != this.S4LocationId) { return false; }
                if (other.S4LocationCode != this.S4LocationCode) { return false; }
                if (other.S4Percentage != this.S4Percentage) { return false; }
                if (other.S5LocationId != this.S5LocationId) { return false; }
                if (other.S5LocationCode != this.S5LocationCode) { return false; }
                if (other.S5Percentage != this.S5Percentage) { return false; }
                if (other.AssortmentId != this.AssortmentId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentLocationBuddyDtoKey
    {
        #region Properties
        public Int16 LocationId { get; set; }
        public Int32 AssortmentId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                LocationId.GetHashCode() +
                AssortmentId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentLocationBuddyDtoKey other = obj as AssortmentLocationBuddyDtoKey;
            if (other != null)
            {
                if (other.LocationId != this.LocationId) { return false; }
                if (other.AssortmentId != this.AssortmentId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentLocationBuddyIsSetDto
    {
        #region Properties

        public Boolean IsLocationIdSet { get; set; }
        public Boolean IsTreatmentTypeSet { get; set; }
        public Boolean IsTreatmentTypePercentageSet { get; set; }
        public Boolean IsS1LocationIdSet { get; set; }
        public Boolean IsS1PercentageSet { get; set; }
        public Boolean IsS2LocationIdSet { get; set; }
        public Boolean IsS2PercentageSet { get; set; }
        public Boolean IsS3LocationIdSet { get; set; }
        public Boolean IsS3PercentageSet { get; set; }
        public Boolean IsS4LocationIdSet { get; set; }
        public Boolean IsS4PercentageSet { get; set; }
        public Boolean IsS5LocationIdSet { get; set; }
        public Boolean IsS5PercentageSet { get; set; }
        public Boolean IsAssortmentIdSet { get; set; }


        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public AssortmentLocationBuddyIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public AssortmentLocationBuddyIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsLocationIdSet.GetHashCode() +
                IsTreatmentTypeSet.GetHashCode() +
                IsTreatmentTypePercentageSet.GetHashCode()  +
                IsS1LocationIdSet.GetHashCode() +
                IsS1PercentageSet.GetHashCode() +
                IsS2LocationIdSet.GetHashCode() +
                IsS2PercentageSet.GetHashCode() +
                IsS3LocationIdSet.GetHashCode() +
                IsS3PercentageSet.GetHashCode() +
                IsS4LocationIdSet.GetHashCode() +
                IsS4PercentageSet.GetHashCode() +
                IsS5LocationIdSet.GetHashCode() +
                IsS5PercentageSet.GetHashCode() +
                IsAssortmentIdSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentLocationBuddyIsSetDto other = obj as AssortmentLocationBuddyIsSetDto;
            if (other != null)
            {
                if (other.IsLocationIdSet != this.IsLocationIdSet) { return false; }
                if (other.IsTreatmentTypeSet != this.IsTreatmentTypeSet) { return false; }
                if (other.IsTreatmentTypePercentageSet != this.IsTreatmentTypePercentageSet) { return false; }
                if (other.IsS1LocationIdSet != this.IsS1LocationIdSet) { return false; }
                if (other.IsS1PercentageSet != this.IsS1PercentageSet) { return false; }
                if (other.IsS2LocationIdSet != this.IsS2LocationIdSet) { return false; }
                if (other.IsS2PercentageSet != this.IsS2PercentageSet) { return false; }
                if (other.IsS3LocationIdSet != this.IsS3LocationIdSet) { return false; }
                if (other.IsS3PercentageSet != this.IsS3PercentageSet) { return false; }
                if (other.IsS4LocationIdSet != this.IsS4LocationIdSet) { return false; }
                if (other.IsS4PercentageSet != this.IsS4PercentageSet) { return false; }
                if (other.IsS5LocationIdSet != this.IsS5LocationIdSet) { return false; }
                if (other.IsS5PercentageSet != this.IsS5PercentageSet) { return false; }
                if (other.IsAssortmentIdSet != this.IsAssortmentIdSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsLocationIdSet = isSet;
            IsTreatmentTypeSet = isSet;
            IsTreatmentTypePercentageSet = isSet;
            IsS1LocationIdSet = isSet;
            IsS1PercentageSet = isSet;
            IsS2LocationIdSet = isSet;
            IsS2PercentageSet = isSet;
            IsS3LocationIdSet = isSet;
            IsS3PercentageSet = isSet;
            IsS4LocationIdSet = isSet;
            IsS4PercentageSet = isSet;
            IsS5LocationIdSet = isSet;
            IsS5PercentageSet = isSet;
            IsAssortmentIdSet = isSet;
        }
        #endregion
    }
}

