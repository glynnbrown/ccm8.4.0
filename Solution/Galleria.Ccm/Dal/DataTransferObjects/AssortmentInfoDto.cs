﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25454 : J.Pickup
//		Created (Auto-generated)
// CCM-26140 :I.George
//     Added a dto key
// V8-27128 : L.Luong
//     Removed dto key and related methods not needed in info dto 
#endregion
#region Version History: (CCM CCM803)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Assortment Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class AssortmentInfoDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public Int32 EntityId { get; set; }
        public Int32 ProductGroupId { get; set; }
        public Int32? ConsumerDecisionTreeId { get; set; }
        public String ProductGroupName { get; set; }
        public String ProductGroupCode { get; set; }
        public String Name { get; set; }
        public Guid UniqueContentReference { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public Guid? ParentUniqueContentReference { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentInfoDto other = obj as AssortmentInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.ProductGroupId != this.ProductGroupId) { return false; }
                if (other.ConsumerDecisionTreeId != this.ConsumerDecisionTreeId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.ProductGroupName != this.ProductGroupName) { return false; }
                if (other.ProductGroupCode != this.ProductGroupCode) { return false; }
                if (other.UniqueContentReference != this.UniqueContentReference) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.ParentUniqueContentReference != this.ParentUniqueContentReference) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
