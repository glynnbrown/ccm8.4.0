﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-25664 : A.Kuszyk
//		Created (Auto-generated)
// V8-26322 : A.Silva
//      Added PreloadedRecordCount(1) as the database creation script will add at least one user.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
	/// <summary>
	///     User Data Transfer Object. At Creation time, the database always adds one at the very least as administrator.
	/// </summary>
	[Serializable, PreloadedRecordCount(1)]
	public sealed class UserDto
	{
		#region Properties
		public Int32 Id { get; set; }
		public RowVersion RowVersion { get; set; }
		public UserDtoKey DtoKey
		{
			get 
			{
				return new UserDtoKey() 
				{
					UserName = this.UserName
				};
			}
		} 
		public String UserName { get; set; } 
		public String FirstName { get; set; } 
		public String LastName { get; set; } 
		public String Description { get; set; } 
		public String Office { get; set; } 
		public String TelephoneNumber { get; set; } 
		public String EMailAddress { get; set; } 
		public String Title { get; set; } 
		public String Department { get; set; } 
		public String Company { get; set; } 
		public String Manager { get; set; }
        public DateTime? DateDeleted { get; set; }
		#endregion 

		#region Methods
		/// <summary>
		/// Returns a hash code for this object
		/// </summary>
		/// <returns>The object hash code</returns>
		public override Int32 GetHashCode()
		{
			return this.Id.GetHashCode();
		}

		/// <summary>
		/// Check to see if two dtos are the same
		/// </summary>
		/// <param name="obj">The object to compare against</param>
		/// <returns>true if objects are equal</returns>
		public override Boolean Equals(Object obj)
		{
			UserDto other = obj as UserDto;
			if (other != null)
			{
                if (other.Id != this.Id) return false;
                if (other.RowVersion != this.RowVersion) return false;
                if (other.UserName != this.UserName) return false;
                if (other.FirstName != this.FirstName) return false;
                if (other.LastName != this.LastName) return false;
                if (other.Description != this.Description) return false;
                if (other.Office != this.Office) return false;
                if (other.TelephoneNumber != this.TelephoneNumber) return false;
                if (other.EMailAddress != this.EMailAddress) return false;
                if (other.Title != this.Title) return false;
                if (other.Department != this.Department) return false;
                if (other.Company != this.Company) return false;
                if (other.Manager != this.Manager) return false;
                if (other.DateDeleted != this.DateDeleted) return false;
			}
			else
			{
				return false;
			}
			return true;
		}
		#endregion 
	}

    [Serializable]
	public class UserDtoKey 
	{
		#region Properties
		public String UserName { get; set; }
		#endregion

		#region Methods
		/// <summary>
		/// Returns a hash code for this object
		/// </summary>
		/// <returns>The object hash code</returns>
		public override Int32 GetHashCode()
		{
			return UserName.GetHashCode();
		}

		/// <summary>
		/// Check to see if two keys are the same
		/// </summary>
		/// <param name="obj">The object to compare against</param>
		/// <returns>true if objects are equal</returns>
		public override Boolean Equals(Object obj)
		{
			UserDtoKey other = obj as UserDtoKey;
			if (other != null)
			{
                if (other.UserName != this.UserName) return false;
			}
			else
			{
				return false;
			}
			return true;
		}
		#endregion 
	}
}
