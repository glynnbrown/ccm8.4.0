﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25454 : J.Pickup
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Blob Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class BlobDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public BlobDtoKey DtoKey
        {
	        get 
	        {
		        return new BlobDtoKey() 
		        {Id = this.Id,
		        };
	        }
        } 
        public Byte[] Data  { get; set; } 
        public DateTime DateCreated { get; set; } 
        public DateTime DateLastModified { get; set; } 
        public DateTime? DateDeleted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            BlobDto other = obj as BlobDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.RowVersion != this.RowVersion) return false;
                if (other.DateCreated != this.DateCreated) return false;
                if (other.DateLastModified != this.DateLastModified) return false;
                if (other.DateDeleted != this.DateDeleted) return false;
                if ((other.Data == null) && (this.Data != null)) return false;
                if ((other.Data != null) && (this.Data == null)) return false;
                if ((other.Data != null) && (this.Data != null)) if (!other.Data.SequenceEqual(this.Data)) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion 
    }

    [Serializable]
    public class BlobDtoKey
    {
        #region Properties
        public Int32 Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            BlobDtoKey other = obj as BlobDtoKey;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class BlobIsSetDto
    {
        #region Properties
        public Boolean IsDataSet { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public BlobIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public BlobIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsDataSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            BlobIsSetDto other = obj as BlobIsSetDto;
            if (other != null)
            {
                if (other.IsDataSet != this.IsDataSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsDataSet = isSet;
        }
        #endregion
    }
}
