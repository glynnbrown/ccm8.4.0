﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-26234 : L.Ineson
//		Copied from GFS
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// RoleEntity Data Transfer Object
    /// </summary>
    [Serializable]
    public class RoleEntityDto
    {
        #region Properties

        public Int32 Id { get; set; }

        public RoleEntityDtoKey DtoKey
        {
            get
            {
                return new RoleEntityDtoKey()
                {
                    RoleId = this.RoleId,
                    EntityId = this.EntityId
                };
            }
        }

        [ForeignKey(typeof(RoleDto), typeof(IRoleDal))]
        public Int32 RoleId { get; set; }

        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }

        #endregion

        #region Methods

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            RoleEntityDto other = obj as RoleEntityDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.RoleId != this.RoleId) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        #endregion
    }

    [Serializable]
    public class RoleEntityDtoKey
    {
        #region Properties
        public Int32 RoleId { get; set; }
        public Int32 EntityId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return RoleId.GetHashCode()
                + EntityId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            RoleEntityDtoKey other = obj as RoleEntityDtoKey;
            if (other != null)
            {
                if (other.RoleId != this.RoleId) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
