﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : Luong
//   Created (Copied From GFS)
#endregion

#endregion

using System;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Event Log Data Transfer Object
    /// </summary>
    [Serializable]
    public class EventLogNameDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public String Name { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override bool Equals(object obj)
        {
            EventLogNameDto other = obj as EventLogNameDto;
            if (other != null)
            {
                if (other.Id != this.Id)
                {
                    return false;
                }
                if (other.Name != this.Name)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}

