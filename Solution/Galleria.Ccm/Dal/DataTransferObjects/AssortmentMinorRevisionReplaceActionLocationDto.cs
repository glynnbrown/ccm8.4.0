﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-25455 : J.Pickup
//  Created
// CCM-27059 : J.Pickup
//      Reviewed FK attributes & Added DtoKey, IsSetDto
// V8-26322 : A.Silva
//      Amended LocationId Property to have DeleteBehavior.Leave (parent is marked as deleted, not really deleted).

#endregion

#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// AssortmentMinorRevisionReplaceActionLocation Data Transfer Object
    /// </summary>
    [Serializable]
    public class AssortmentMinorRevisionReplaceActionLocationDto
    {
        #region Properties

        public Int32 Id { get; set; }
        [ForeignKey(typeof(AssortmentMinorRevisionReplaceActionDto), typeof(IAssortmentMinorRevisionReplaceActionDal))]
        public Int32 AssortmentMinorRevisionReplaceActionId { get; set; }
        public String LocationCode { get; set; }
        public String LocationName { get; set; }
        [ForeignKey(typeof(LocationDto), typeof(ILocationDal), DeleteBehavior.Leave)]
        public Int16? LocationId { get; set; }
        public Int32 Units { get; set; }
        public Int32 Facings { get; set; }
        public AssortmentMinorRevisionReplaceActionLocationDtoKey DtoKey
        {
            get
            {
                return new AssortmentMinorRevisionReplaceActionLocationDtoKey()
                {
                     AssortmentMinorRevisionReplaceActionId = this.AssortmentMinorRevisionReplaceActionId,
                };
            }
        }

        #endregion

        #region Methods
        
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentMinorRevisionReplaceActionLocationDto other = obj as AssortmentMinorRevisionReplaceActionLocationDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.AssortmentMinorRevisionReplaceActionId != this.AssortmentMinorRevisionReplaceActionId) { return false; }
                if (other.LocationCode != this.LocationCode) { return false; }
                if (other.LocationName != this.LocationName) { return false; }
                if (other.LocationId != this.LocationId) { return false; }
                if (other.Units != this.Units) { return false; }
                if (other.Facings != this.Facings) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


    [Serializable]
    public class AssortmentMinorRevisionReplaceActionLocationDtoKey
    {
        #region Properties

        public Int32 AssortmentMinorRevisionReplaceActionId { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                AssortmentMinorRevisionReplaceActionId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentMinorRevisionReplaceActionLocationDtoKey other = obj as AssortmentMinorRevisionReplaceActionLocationDtoKey;
            if (other != null)
            {
                if (other.AssortmentMinorRevisionReplaceActionId != this.AssortmentMinorRevisionReplaceActionId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


    [Serializable]
    public class AssortmentMinorRevisionReplaceActionLocationIsSetDto
    {
        #region Properties

        public Boolean IsIdSet { get; set; }
        public Boolean IsAssortmentMinorRevisionReplaceActionIdSet { get; set; }
        public Boolean IsLocationCodeSet { get; set; }
        public Boolean IsLocationNameSet { get; set; }
        public Boolean IsLocationIdSet { get; set; }
        public Boolean IsUnitsSet { get; set; }
        public Boolean IsFacingsSet { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Default
        /// </summary>
        public AssortmentMinorRevisionReplaceActionLocationIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public AssortmentMinorRevisionReplaceActionLocationIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsIdSet.GetHashCode() +
                IsAssortmentMinorRevisionReplaceActionIdSet.GetHashCode() +
                IsLocationCodeSet.GetHashCode() +
                IsLocationNameSet.GetHashCode() +
                IsLocationIdSet.GetHashCode() +
                IsUnitsSet.GetHashCode() +
                IsFacingsSet.GetHashCode();
                
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentMinorRevisionReplaceActionLocationIsSetDto other = obj as AssortmentMinorRevisionReplaceActionLocationIsSetDto;
            if (other != null)
            {
                if (other.IsIdSet != this.IsIdSet) { return false; }
                if (other.IsAssortmentMinorRevisionReplaceActionIdSet != this.IsAssortmentMinorRevisionReplaceActionIdSet) { return false; }
                if (other.IsLocationCodeSet != this.IsLocationCodeSet) { return false; }
                if (other.IsLocationNameSet != this.IsLocationNameSet) { return false; }
                if (other.IsLocationIdSet != this.IsLocationIdSet) { return false; }
                if (other.IsUnitsSet != this.IsUnitsSet) { return false; }
                if (other.IsFacingsSet != this.IsFacingsSet) { return false; }
               
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsIdSet = isSet;
            IsAssortmentMinorRevisionReplaceActionIdSet = isSet;
            IsLocationCodeSet = isSet;
            IsLocationNameSet = isSet;
            IsLocationIdSet = isSet;
            IsFacingsSet = isSet;
            IsUnitsSet = isSet;
        }
        #endregion
    }
}

