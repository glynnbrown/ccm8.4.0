﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-26317 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public sealed class WorkpackagePlanogramDebugDto
    {
        #region Properties
        [ForeignKey(typeof(WorkpackagePlanogramDto), typeof(IWorkpackagePlanogramDal), DeleteBehavior.Cascade)]
        public Int32 SourcePlanogramId { get; set; }
        public Int32 WorkflowTaskId { get; set; }
        [ForeignKey(typeof(PlanogramDto), typeof(IPlanogramDal), DeleteBehavior.Cascade)]
        public Int32 DebugPlanogramId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the hash code for this instance
        /// </summary>
        public override int GetHashCode()
        {
            return
                this.SourcePlanogramId.GetHashCode() +
                this.DebugPlanogramId.GetHashCode();
        }

        /// <summary>
        /// Compares two instances
        /// </summary>
        public override bool Equals(Object obj)
        {
            WorkpackagePlanogramDebugDto other = obj as WorkpackagePlanogramDebugDto;
            if (other != null)
            {
                if (other.SourcePlanogramId != this.SourcePlanogramId) return false;
                if (other.WorkflowTaskId != this.WorkflowTaskId) return false;
                if (other.DebugPlanogramId != this.DebugPlanogramId) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
