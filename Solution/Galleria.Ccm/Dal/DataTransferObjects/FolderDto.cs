﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;


namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Folder Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class FolderDto
    {
        #region Properties
        public Object Id { get; set; }
        public FolderDtoKey DtoKey
        {
            get
            {
                return new FolderDtoKey()
                {
                    Id = this.Id,
                };
            }
        }
        public Object ParentFolderId { get; set; }
        public String Name { get; set; }
        public String FileSystemPath { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FolderDto other = obj as FolderDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }


                // ParentFolderId
                if ((other.ParentFolderId != null) && (this.ParentFolderId != null))
                {
                    if (!other.ParentFolderId.Equals(this.ParentFolderId)) { return false; }
                }
                if ((other.ParentFolderId != null) && (this.ParentFolderId == null)) { return false; }
                if ((other.ParentFolderId == null) && (this.ParentFolderId != null)) { return false; }

                if (other.Name != this.Name) { return false; }
                if (other.FileSystemPath != this.FileSystemPath) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class FolderDtoKey
    {
        #region Properties
        public Object Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FolderDtoKey other = obj as FolderDtoKey;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
