﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created (Auto-generated)
// V8-27468 : L.Ineson
//  Added IsMerchandisedTopDown
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// FixtureComponent Data Transfer Object
    /// </summary>
    [Serializable]
    public class FixtureComponentDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public FixtureComponentDtoKey DtoKey
        {
            get
            {
                return new FixtureComponentDtoKey()
                {
                    Id = this.Id,
                };
            }
        }
        public Object FixturePackageId { get; set; }
        public Int32? Mesh3DId { get; set; }
        public Int32? ImageIdFront { get; set; }
        public Int32? ImageIdBack { get; set; }
        public Int32? ImageIdTop { get; set; }
        public Int32? ImageIdBottom { get; set; }
        public Int32? ImageIdLeft { get; set; }
        public Int32? ImageIdRight { get; set; }
        public String Name { get; set; }
        public Single Height { get; set; }
        public Single Width { get; set; }
        public Single Depth { get; set; }
        public Int16 NumberOfSubComponents { get; set; }
        public Int16 NumberOfMerchandisedSubComponents { get; set; }
        public Int16 NumberOfShelfEdgeLabels { get; set; }
        public Boolean IsMoveable { get; set; }
        public Boolean IsDisplayOnly { get; set; }
        public Boolean CanAttachShelfEdgeLabel { get; set; }
        public String RetailerReferenceCode { get; set; }
        public String BarCode { get; set; }
        public String Manufacturer { get; set; }
        public String ManufacturerPartName { get; set; }
        public String ManufacturerPartNumber { get; set; }
        public String SupplierName { get; set; }
        public String SupplierPartNumber { get; set; }
        public Single? SupplierCostPrice { get; set; }
        public Single? SupplierDiscount { get; set; }
        public Single? SupplierLeadTime { get; set; }
        public Int32? MinPurchaseQty { get; set; }
        public Single? WeightLimit { get; set; }
        public Single? Weight { get; set; }
        public Single? Volume { get; set; }
        public Single? Diameter { get; set; }
        public Int16? Capacity { get; set; }
        public Byte ComponentType { get; set; }
        public Boolean IsMerchandisedTopDown { get; set; }
        public Object ExtendedData { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FixtureComponentDto other = obj as FixtureComponentDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                
                //FixturePackageId
                if ((other.FixturePackageId != null) && (this.FixturePackageId != null))
                {
                    if (!other.FixturePackageId.Equals(this.FixturePackageId)) { return false; }
                }
                if ((other.FixturePackageId != null) && (this.FixturePackageId == null)) { return false; }
                if ((other.FixturePackageId == null) && (this.FixturePackageId != null)) { return false; }

                if (other.Mesh3DId != this.Mesh3DId) { return false; }
                if (other.ImageIdFront != this.ImageIdFront) { return false; }
                if (other.ImageIdBack != this.ImageIdBack) { return false; }
                if (other.ImageIdTop != this.ImageIdTop) { return false; }
                if (other.ImageIdBottom != this.ImageIdBottom) { return false; }
                if (other.ImageIdLeft != this.ImageIdLeft) { return false; }
                if (other.ImageIdRight != this.ImageIdRight) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.Height != this.Height) { return false; }
                if (other.Width != this.Width) { return false; }
                if (other.Depth != this.Depth) { return false; }
                if (other.NumberOfSubComponents != this.NumberOfSubComponents) { return false; }
                if (other.NumberOfMerchandisedSubComponents != this.NumberOfMerchandisedSubComponents) { return false; }
                if (other.NumberOfShelfEdgeLabels != this.NumberOfShelfEdgeLabels) { return false; }
                if (other.IsMoveable != this.IsMoveable) { return false; }
                if (other.IsDisplayOnly != this.IsDisplayOnly) { return false; }
                if (other.CanAttachShelfEdgeLabel != this.CanAttachShelfEdgeLabel) { return false; }
                if (other.RetailerReferenceCode != this.RetailerReferenceCode) { return false; }
                if (other.BarCode != this.BarCode) { return false; }
                if (other.Manufacturer != this.Manufacturer) { return false; }
                if (other.ManufacturerPartName != this.ManufacturerPartName) { return false; }
                if (other.ManufacturerPartNumber != this.ManufacturerPartNumber) { return false; }
                if (other.SupplierName != this.SupplierName) { return false; }
                if (other.SupplierPartNumber != this.SupplierPartNumber) { return false; }
                if (other.SupplierCostPrice != this.SupplierCostPrice) { return false; }
                if (other.SupplierDiscount != this.SupplierDiscount) { return false; }
                if (other.SupplierLeadTime != this.SupplierLeadTime) { return false; }
                if (other.MinPurchaseQty != this.MinPurchaseQty) { return false; }
                if (other.WeightLimit != this.WeightLimit) { return false; }
                if (other.Weight != this.Weight) { return false; }
                if (other.Volume != this.Volume) { return false; }
                if (other.Diameter != this.Diameter) { return false; }
                if (other.Capacity != this.Capacity) { return false; }
                if (other.ComponentType != this.ComponentType) { return false; }
                if (other.IsMerchandisedTopDown != this.IsMerchandisedTopDown) { return false; }


                //ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData != null))
                {
                    if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
                }
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class FixtureComponentDtoKey
    {
        #region Properties
        public Int32 Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FixtureComponentDtoKey other = obj as FixtureComponentDtoKey;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
