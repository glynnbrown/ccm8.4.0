﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GFS 1.0)
// CCM-25445 : L.Ineson
//		Copied from GFS 
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <remarks>
    /// Note - this provides access to properties from location.
    /// </remarks>
    [Serializable]
    public class LocationGroupLocationDto
    {
        #region Properties
        public Int16 LocationId { get; set; }
        public Int32 LocationGroupId { get; set; }
        public String Code { get; set; }
        public String Name { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.LocationId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            LocationGroupLocationDto other = obj as LocationGroupLocationDto;
            if (other != null)
            {
                if (other.LocationId != this.LocationId) { return false; }
                if (other.LocationGroupId != this.LocationGroupId) { return false; }
                if (other.Code != this.Code) { return false; }
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
