﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26401 : A.Silva ~ Created.

#endregion

#region Version History: (CCM 8.03)
// V8-29219 : L.Ineson
//Removed date deleted
#endregion

#endregion

using System;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    ///     Data Transfer Object for <see cref="ValidationTemplateInfo" />.
    /// </summary>
    [Serializable]
    public sealed class ValidationTemplateInfoDto
    {
        #region Properties

        public ValidationTemplateDtoKey DtoKey
        {
            get
            {
                return new ValidationTemplateDtoKey
                {
                    EntityId = EntityId,
                    Name = Name
                };
            }
        }

        public Int32 EntityId { get; set; }
        public Int32 Id { get; set; }
        public String Name { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        ///     <see cref="T:System.Object" />.
        /// </summary>
        /// <returns>
        ///     true if the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />;
        ///     otherwise, false.
        /// </returns>
        /// <param name="obj">The <see cref="T:System.Object" /> to compare with the current <see cref="T:System.Object" />. </param>
        public override bool Equals(object obj)
        {
            var other = obj as ValidationTemplateInfoDto;
            return other != null
                   && other.Id == Id
                   && other.EntityId == EntityId
                   && other.Name == Name;
        }

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current <see cref="T:System.Object" />.
        /// </returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode()
                   + EntityId.GetHashCode()
                   + Name.GetHashCode();
        }

        #endregion
    }
}