﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26159 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// PerformanceSelectionTimelineGroup Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class PerformanceSelectionTimelineGroupDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public PerformanceSelectionTimelineGroupDtoKey DtoKey
        {
            get
            {
                return new PerformanceSelectionTimelineGroupDtoKey()
                {
                    PerformanceSelectionId = this.PerformanceSelectionId,
                    Code = this.Code
                };
            }
        }

        [ForeignKey(typeof(PerformanceSelectionDto), typeof(IPerformanceSelectionDal), DeleteBehavior.Leave)]
        public Int32 PerformanceSelectionId { get; set; }

        public Int64 Code { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PerformanceSelectionTimelineGroupDto other = obj as PerformanceSelectionTimelineGroupDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.PerformanceSelectionId != this.PerformanceSelectionId) { return false; }
                if (other.Code != this.Code) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


    public class PerformanceSelectionTimelineGroupDtoKey
    {

        #region Properties

        public Int32 PerformanceSelectionId { get; set; }
        public Int64 Code { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return PerformanceSelectionId.GetHashCode()
                + Code.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PerformanceSelectionTimelineGroupDtoKey other = obj as PerformanceSelectionTimelineGroupDtoKey;
            if (other != null)
            {

                if (other.PerformanceSelectionId != this.PerformanceSelectionId) { return false; }
                if (other.Code != this.Code) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

}
