﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26773 : N.Foster
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class WorkpackagePerformanceDto
    {
        #region Properties
        public Int32 WorkpackageId { get; set; }
        public Int32 Key { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this instance
        /// </summary>
        public override Int32 GetHashCode()
        {
            return this.WorkpackageId.GetHashCode();
        }

        /// <summary>
        /// Indicates of two instances are equal
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            WorkpackagePerformanceDto other = obj as WorkpackagePerformanceDto;
            if (other != null)
            {
                if (other.WorkpackageId != this.WorkpackageId) return false;
                if (other.Key != this.Key) return false;
            }
            else
            {
                return false;
            }
            return true;
        }

        #endregion
    }
}
