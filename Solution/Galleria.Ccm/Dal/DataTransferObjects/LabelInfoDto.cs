﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24265 : N.Haywood
//  Created
// V8-27940 : L.Luong
//  Added Entity and dateDeleted
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// LabelInfo data transfer object
    /// </summary>
    [Serializable]
    public sealed class LabelInfoDto
    {
        #region Properties
        public Object Id { get; set; }
        public Int32 EntityId { get; set; }
        public String Name { get; set; }
        public Byte Type { get; set; }
        public DateTime? DateDeleted { get; set; }
        #endregion

        #region Methods
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override bool Equals(Object obj)
        {
            LabelInfoDto other = obj as LabelInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.Type != this.Type) { return false; }
                if (other.DateDeleted != this.DateDeleted) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
