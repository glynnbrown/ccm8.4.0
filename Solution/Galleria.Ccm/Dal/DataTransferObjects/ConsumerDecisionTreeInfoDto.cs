﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
#endregion
#region Version History: (CCM CCM803)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// ConsumerDecisionTreeInfo Data Transfer Object
    /// </summary>
    [Serializable]
    public class ConsumerDecisionTreeInfoDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Guid UniqueContentReference { get; set; }
        public Int32? ProductGroupId { get; set; }
        public Int32 EntityId { get; set; }
        public String ProductGroupCode { get; set; }
        public String ProductGroupName { get; set; }
        public Guid? ParentUniqueContentReference { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override bool Equals(object obj)
        {
            ConsumerDecisionTreeInfoDto other = obj as ConsumerDecisionTreeInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.UniqueContentReference != this.UniqueContentReference) { return false; }
                if (other.ProductGroupId != this.ProductGroupId) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.ProductGroupCode != this.ProductGroupCode) { return false; }
                if (other.ProductGroupName != this.ProductGroupName) { return false; }
                if (other.ParentUniqueContentReference != this.ParentUniqueContentReference) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
