﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27804 : L.Ineson
//  Created
// V8-24779 : L.Luong
//  Added RowVersion, EntityId and Date properties
#endregion

#region Version History: (CCM 8.03)
// V8-29220 : L.Ineson
//Removed date deleted
#endregion

#region Version History: (CCM 8.30)
// V8-32360 : M.Brumby
//  [PCR01564] Auto split bays by a fixed value
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// PlanogramImportTemplate Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramImportTemplateDto
    {
        #region Properties
        public Object Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public PlanogramImportTemplateDtoKey DtoKey
        {
            get 
            { 
                return new PlanogramImportTemplateDtoKey() 
                { 
                    Name = this.Name,
                    EntityId = this.EntityId
                }; 
            }
        }
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        public String Name { get; set; }
        public Byte FileType { get; set; }
        public String FileVersion { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public Boolean AllowAdvancedBaySplitting { get; set; }
        public Boolean CanSplitComponents { get; set; }
        public Boolean SplitBaysByComponentStart { get; set; }
        public Boolean SplitBaysByComponentEnd { get; set; }
        public Boolean SplitByFixedSize { get; set; }
        public Boolean SplitByRecurringPattern { get; set; }
        public Boolean SplitByBayCount { get; set; }
        public Double SplitFixedSize { get; set; }
        public String SplitRecurringPattern { get; set; }
        public Int32 SplitBayCount { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramImportTemplateDto other = obj as PlanogramImportTemplateDto;
            if (other != null)
            {
                //Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }

                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.FileType != this.FileType) { return false; }
                if (other.FileVersion != this.FileVersion) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.AllowAdvancedBaySplitting != this.AllowAdvancedBaySplitting) { return false; }
                if (other.CanSplitComponents != this.CanSplitComponents) { return false; }
                if (other.SplitBaysByComponentStart != this.SplitBaysByComponentStart) { return false; }
                if (other.SplitBaysByComponentEnd != this.SplitBaysByComponentEnd) { return false; }
                if (other.SplitByFixedSize != this.SplitByFixedSize) { return false; }
                if (other.SplitByRecurringPattern != this.SplitByRecurringPattern) { return false; }
                if (other.SplitByBayCount != this.SplitByBayCount) { return false; }
                if (other.SplitFixedSize != this.SplitFixedSize) { return false; }
                if (other.SplitRecurringPattern != this.SplitRecurringPattern) { return false; }
                if (other.SplitBayCount != this.SplitBayCount) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class PlanogramImportTemplateDtoKey
    {
        #region Properties

        public String Name { get; set; }
        public Int32 EntityId { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Name.GetHashCode() + 
                EntityId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramImportTemplateDtoKey other = obj as PlanogramImportTemplateDtoKey;
            if (other != null)
            {
                if (other.Name != this.Name) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
