﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26773 : N.Foster
//  Created
#endregion
#region Version History: CCM803
// V8-29491 : D.Pleasance
//  removed Location code and added DataType \ DataId so that we can hold performance data at a variety of levels 
//      ((DataType=Enterprise, DataId=0), (DataType=Cluster, DataId=ClusterId), (DataType=Store, DataId=LocationId))
#endregion
#region Version History : CCM830
// V8-13996 : J.Pickup
//  CalculateCombinedFinancialMetricValue works with  an interface to satisfy changes. 
#endregion
#endregion

using System;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class WorkpackagePerformanceDataDto
    {
        #region Properties
        public Int32 WorkpackageId { get; set; }
        public Int32 PerformanceSelectionId { get; set; }
        public Byte DataType { get; set; }
        public Int32? DataId { get; set; }
        public String ProductGTIN { get; set; }
        public Single? P1 { get; set; }
        public Single? P2 { get; set; }
        public Single? P3 { get; set; }
        public Single? P4 { get; set; }
        public Single? P5 { get; set; }
        public Single? P6 { get; set; }
        public Single? P7 { get; set; }
        public Single? P8 { get; set; }
        public Single? P9 { get; set; }
        public Single? P10 { get; set; }
        public Single? P11 { get; set; }
        public Single? P12 { get; set; }
        public Single? P13 { get; set; }
        public Single? P14 { get; set; }
        public Single? P15 { get; set; }
        public Single? P16 { get; set; }
        public Single? P17 { get; set; }
        public Single? P18 { get; set; }
        public Single? P19 { get; set; }
        public Single? P20 { get; set; }
        public Single? CombinedFinancialMetricValue { get; set; }
        #endregion
        
        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public WorkpackagePerformanceDataDto()
        { }

        /// <summary>
        /// Static method to create a blank dummy dto based on parameters for use in buddies
        /// where no existing performance data existed in the dal.
        /// </summary>
        /// <param name="dataId"></param>
        /// <param name="dataType"></param>
        /// <param name="performanceSelectionId"></param>
        /// <param name="productGTIN"></param>
        /// <param name="workpackageId"></param>
        /// <param name="resetPerformanceValuesTo0"></param>
        /// <returns></returns>
        public WorkpackagePerformanceDataDto(Int32? dataId,
                                    Byte dataType,
                                    Int32 performanceSelectionId,
                                    String productGTIN,
                                    Int32 workpackageId)
        {
            //Set values with parameters
            this.DataId = dataId;
            this.DataType = dataType;
            this.PerformanceSelectionId = performanceSelectionId;
            this.ProductGTIN = productGTIN;
            this.WorkpackageId = workpackageId;
        }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this instance
        /// </summary>
        public override int GetHashCode()
        {
            return
                this.WorkpackageId.GetHashCode() +
                this.PerformanceSelectionId.GetHashCode() +
                this.ProductGTIN.GetHashCode() +
                this.DataType.GetHashCode() +
                this.DataId.GetHashCode();
        }

        /// <summary>
        /// Indicates if two instances are equal
        /// </summary>
        public override bool Equals(Object obj)
        {
            WorkpackagePerformanceDataDto other = obj as WorkpackagePerformanceDataDto;
            if (other != null)
            {
                if (other.WorkpackageId != this.WorkpackageId) return false;
                if (other.PerformanceSelectionId != this.PerformanceSelectionId) return false;
                if (other.DataType != this.DataType) return false;
                if (other.DataId != this.DataId) return false;
                if (other.ProductGTIN != this.ProductGTIN) return false;

                if ((other.P1 == null) && (this.P1 != null)) return false;
                if ((other.P1 != null) && (this.P1 == null)) return false;
                if ((other.P1 != null) && (!other.P1.Equals(this.P1))) return false;

                if ((other.P2 == null) && (this.P2 != null)) return false;
                if ((other.P2 != null) && (this.P2 == null)) return false;
                if ((other.P2 != null) && (!other.P2.Equals(this.P2))) return false;

                if ((other.P3 == null) && (this.P3 != null)) return false;
                if ((other.P3 != null) && (this.P3 == null)) return false;
                if ((other.P3 != null) && (!other.P3.Equals(this.P3))) return false;

                if ((other.P4 == null) && (this.P4 != null)) return false;
                if ((other.P4 != null) && (this.P4 == null)) return false;
                if ((other.P4 != null) && (!other.P4.Equals(this.P4))) return false;

                if ((other.P5 == null) && (this.P5 != null)) return false;
                if ((other.P5 != null) && (this.P5 == null)) return false;
                if ((other.P5 != null) && (!other.P5.Equals(this.P5))) return false;

                if ((other.P6 == null) && (this.P6 != null)) return false;
                if ((other.P6 != null) && (this.P6 == null)) return false;
                if ((other.P6 != null) && (!other.P6.Equals(this.P6))) return false;

                if ((other.P7 == null) && (this.P7 != null)) return false;
                if ((other.P7 != null) && (this.P7 == null)) return false;
                if ((other.P7 != null) && (!other.P7.Equals(this.P7))) return false;

                if ((other.P8 == null) && (this.P8 != null)) return false;
                if ((other.P8 != null) && (this.P8 == null)) return false;
                if ((other.P8 != null) && (!other.P8.Equals(this.P8))) return false;

                if ((other.P9 == null) && (this.P9 != null)) return false;
                if ((other.P9 != null) && (this.P9 == null)) return false;
                if ((other.P9 != null) && (!other.P9.Equals(this.P9))) return false;

                if ((other.P10 == null) && (this.P10 != null)) return false;
                if ((other.P10 != null) && (this.P10 == null)) return false;
                if ((other.P10 != null) && (!other.P10.Equals(this.P10))) return false;

                if ((other.P11 == null) && (this.P11 != null)) return false;
                if ((other.P11 != null) && (this.P11 == null)) return false;
                if ((other.P11 != null) && (!other.P11.Equals(this.P11))) return false;

                if ((other.P12 == null) && (this.P12 != null)) return false;
                if ((other.P12 != null) && (this.P12 == null)) return false;
                if ((other.P12 != null) && (!other.P12.Equals(this.P12))) return false;

                if ((other.P13 == null) && (this.P13 != null)) return false;
                if ((other.P13 != null) && (this.P13 == null)) return false;
                if ((other.P13 != null) && (!other.P13.Equals(this.P13))) return false;

                if ((other.P14 == null) && (this.P14 != null)) return false;
                if ((other.P14 != null) && (this.P14 == null)) return false;
                if ((other.P14 != null) && (!other.P14.Equals(this.P14))) return false;

                if ((other.P15 == null) && (this.P15 != null)) return false;
                if ((other.P15 != null) && (this.P15 == null)) return false;
                if ((other.P15 != null) && (!other.P15.Equals(this.P15))) return false;

                if ((other.P16 == null) && (this.P16 != null)) return false;
                if ((other.P16 != null) && (this.P16 == null)) return false;
                if ((other.P16 != null) && (!other.P16.Equals(this.P16))) return false;

                if ((other.P17 == null) && (this.P17 != null)) return false;
                if ((other.P17 != null) && (this.P17 == null)) return false;
                if ((other.P17 != null) && (!other.P17.Equals(this.P17))) return false;

                if ((other.P18 == null) && (this.P18 != null)) return false;
                if ((other.P18 != null) && (this.P18 == null)) return false;
                if ((other.P18 != null) && (!other.P18.Equals(this.P18))) return false;

                if ((other.P19 == null) && (this.P19 != null)) return false;
                if ((other.P19 != null) && (this.P19 == null)) return false;
                if ((other.P19 != null) && (!other.P19.Equals(this.P19))) return false;

                if ((other.P20 == null) && (this.P20 != null)) return false;
                if ((other.P20 != null) && (this.P20 == null)) return false;
                if ((other.P20 != null) && (!other.P20.Equals(this.P20))) return false;

                if ((other.CombinedFinancialMetricValue == null) && (this.CombinedFinancialMetricValue != null)) return false;
                if ((other.CombinedFinancialMetricValue != null) && (this.CombinedFinancialMetricValue == null)) return false;
                if ((other.CombinedFinancialMetricValue != null) && (!other.CombinedFinancialMetricValue.Equals(this.CombinedFinancialMetricValue))) return false;
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Applies weighting value to the current item
        /// </summary>
        /// <param name="weighting">The weighting value to apply</param>
        public void ApplyWeighting(Single weighting)
        {
            if (weighting == 0 || weighting == 1) { return; }

            this.P1 = Add(null, this.P1 == null ? null : (this.P1 * weighting));
            this.P2 = Add(null, this.P2 == null ? null : (this.P2 * weighting));
            this.P3 = Add(null, this.P3 == null ? null : (this.P3 * weighting));
            this.P4 = Add(null, this.P4 == null ? null : (this.P4 * weighting));
            this.P5 = Add(null, this.P5 == null ? null : (this.P5 * weighting));
            this.P6 = Add(null, this.P6 == null ? null : (this.P6 * weighting));
            this.P7 = Add(null, this.P7 == null ? null : (this.P7 * weighting));
            this.P8 = Add(null, this.P8 == null ? null : (this.P8 * weighting));
            this.P9 = Add(null, this.P9 == null ? null : (this.P9 * weighting));
            this.P10 = Add(null, this.P10 == null ? null : (this.P10 * weighting));
            this.P11 = Add(null, this.P11 == null ? null : (this.P11 * weighting));
            this.P12 = Add(null, this.P12 == null ? null : (this.P12 * weighting));
            this.P13 = Add(null, this.P13 == null ? null : (this.P13 * weighting));
            this.P14 = Add(null, this.P14 == null ? null : (this.P14 * weighting));
            this.P15 = Add(null, this.P15 == null ? null : (this.P15 * weighting));
            this.P16 = Add(null, this.P16 == null ? null : (this.P16 * weighting));
            this.P17 = Add(null, this.P17 == null ? null : (this.P17 * weighting));
            this.P18 = Add(null, this.P18 == null ? null : (this.P18 * weighting));
            this.P19 = Add(null, this.P19 == null ? null : (this.P19 * weighting));
            this.P20 = Add(null, this.P20 == null ? null : (this.P20 * weighting));
        }

        /// <summary>
        /// Applies WorkpackagePerformanceData and weighting to the current item.
        /// </summary>
        /// <param name="workpackagePerformanceDataDto">The WorkpackagePerformanceDataDto</param>
        /// <param name="weighting">The weighting value</param>
        public void ApplyPerformance(WorkpackagePerformanceDataDto workpackagePerformanceDataDto, Single weighting)
        {
            ApplyAvgBuddyPerformance(workpackagePerformanceDataDto, weighting, 1);
        }

        public void ApplyAvgBuddyPerformance(WorkpackagePerformanceDataDto workpackagePerformanceDataDto, Single weighting, Int32 numSourceBuddys)
        {
            if (workpackagePerformanceDataDto == null || weighting == 0) { return; }

            this.P1 = Add(this.P1, workpackagePerformanceDataDto.P1 == null ? null : ((workpackagePerformanceDataDto.P1 / numSourceBuddys) * weighting));
            this.P2 = Add(this.P2, workpackagePerformanceDataDto.P2 == null ? null : ((workpackagePerformanceDataDto.P2 / numSourceBuddys) * weighting));
            this.P3 = Add(this.P3, workpackagePerformanceDataDto.P3 == null ? null : ((workpackagePerformanceDataDto.P3 / numSourceBuddys) * weighting));
            this.P4 = Add(this.P4, workpackagePerformanceDataDto.P4 == null ? null : ((workpackagePerformanceDataDto.P4 / numSourceBuddys) * weighting));
            this.P5 = Add(this.P5, workpackagePerformanceDataDto.P5 == null ? null : ((workpackagePerformanceDataDto.P5 / numSourceBuddys) * weighting));
            this.P6 = Add(this.P6, workpackagePerformanceDataDto.P6 == null ? null : ((workpackagePerformanceDataDto.P6 / numSourceBuddys) * weighting));
            this.P7 = Add(this.P7, workpackagePerformanceDataDto.P7 == null ? null : ((workpackagePerformanceDataDto.P7 / numSourceBuddys) * weighting));
            this.P8 = Add(this.P8, workpackagePerformanceDataDto.P8 == null ? null : ((workpackagePerformanceDataDto.P8 / numSourceBuddys) * weighting));
            this.P9 = Add(this.P9, workpackagePerformanceDataDto.P9 == null ? null : ((workpackagePerformanceDataDto.P9 / numSourceBuddys) * weighting));
            this.P10 = Add(this.P10, workpackagePerformanceDataDto.P10 == null ? null : ((workpackagePerformanceDataDto.P10 / numSourceBuddys) * weighting));
            this.P11 = Add(this.P11, workpackagePerformanceDataDto.P11 == null ? null : ((workpackagePerformanceDataDto.P11 / numSourceBuddys) * weighting));
            this.P12 = Add(this.P12, workpackagePerformanceDataDto.P12 == null ? null : ((workpackagePerformanceDataDto.P12 / numSourceBuddys) * weighting));
            this.P13 = Add(this.P13, workpackagePerformanceDataDto.P13 == null ? null : ((workpackagePerformanceDataDto.P13 / numSourceBuddys) * weighting));
            this.P14 = Add(this.P14, workpackagePerformanceDataDto.P14 == null ? null : ((workpackagePerformanceDataDto.P14 / numSourceBuddys) * weighting));
            this.P15 = Add(this.P15, workpackagePerformanceDataDto.P15 == null ? null : ((workpackagePerformanceDataDto.P15 / numSourceBuddys) * weighting));
            this.P16 = Add(this.P16, workpackagePerformanceDataDto.P16 == null ? null : ((workpackagePerformanceDataDto.P16 / numSourceBuddys) * weighting));
            this.P17 = Add(this.P17, workpackagePerformanceDataDto.P17 == null ? null : ((workpackagePerformanceDataDto.P17 / numSourceBuddys) * weighting));
            this.P18 = Add(this.P18, workpackagePerformanceDataDto.P18 == null ? null : ((workpackagePerformanceDataDto.P18 / numSourceBuddys) * weighting));
            this.P19 = Add(this.P19, workpackagePerformanceDataDto.P19 == null ? null : ((workpackagePerformanceDataDto.P19 / numSourceBuddys) * weighting));
            this.P20 = Add(this.P20, workpackagePerformanceDataDto.P20 == null ? null : ((workpackagePerformanceDataDto.P20 / numSourceBuddys) * weighting));
        }

        /// <summary>
        /// Adds two nullable Singles together.
        /// </summary>
        /// <param name="x">The first number.</param>
        /// <param name="y">The second number.</param>
        /// <returns>Null if both x and y are null, x if x is not null but y is, y if y is not null but x is, and
        /// x.Value + y.Value if both x and y are not null.</returns>
        private static Single? Add(Single? x, Single? y)
        {
            Single? returnValue = null;
            if ((x != null) || (y != null))
            {
                returnValue = 0;
            }
            if (x != null)
            {
                returnValue += x.Value;
            }
            if (y != null)
            {
                returnValue += y.Value;
            }
            return returnValue;
        }

        /// <summary>
        /// Method to clear the performance P values
        /// </summary>
        public void ResetPerformanceValues()
        {
            this.P1 = 0;
            this.P2 = 0;
            this.P3 = 0;
            this.P4 = 0;
            this.P5 = 0;
            this.P6 = 0;
            this.P7 = 0;
            this.P8 = 0;
            this.P9 = 0;
            this.P10 = 0;
            this.P11 = 0;
            this.P12 = 0;
            this.P13 = 0;
            this.P14 = 0;
            this.P15 = 0;
            this.P16 = 0;
            this.P17 = 0;
            this.P18 = 0;
            this.P19 = 0;
            this.P20 = 0;
        }

        /// <summary>
        /// Method to calculate the combined financial metric value based on a specific metric profile
        /// </summary>
        /// <param name="metricProfile"></param>
        public void CalculateCombinedFinancialMetricValue(IPlanogramMetricProfile metricProfile)
        {
            Single combinedFinancialMetricValue = 0;
            if ((metricProfile.Metric1Ratio != null) && (P1 != null))
            {
                combinedFinancialMetricValue += metricProfile.Metric1Ratio.Value * P1.Value;
            }
            if ((metricProfile.Metric2Ratio != null) && (P2 != null))
            {
                combinedFinancialMetricValue += metricProfile.Metric2Ratio.Value * P2.Value;
            }
            if ((metricProfile.Metric3Ratio != null) && (P3 != null))
            {
                combinedFinancialMetricValue += metricProfile.Metric3Ratio.Value * P3.Value;
            }
            if ((metricProfile.Metric4Ratio != null) && (P4 != null))
            {
                combinedFinancialMetricValue += metricProfile.Metric4Ratio.Value * P4.Value;
            }
            if ((metricProfile.Metric5Ratio != null) && (P5 != null))
            {
                combinedFinancialMetricValue += metricProfile.Metric5Ratio.Value * P5.Value;
            }
            if ((metricProfile.Metric6Ratio != null) && (P6 != null))
            {
                combinedFinancialMetricValue += metricProfile.Metric6Ratio.Value * P6.Value;
            }
            if ((metricProfile.Metric7Ratio != null) && (P7 != null))
            {
                combinedFinancialMetricValue += metricProfile.Metric7Ratio.Value * P7.Value;
            }
            if ((metricProfile.Metric8Ratio != null) && (P8 != null))
            {
                combinedFinancialMetricValue += metricProfile.Metric8Ratio.Value * P8.Value;
            }
            if ((metricProfile.Metric9Ratio != null) && (P9 != null))
            {
                combinedFinancialMetricValue += metricProfile.Metric9Ratio.Value * P9.Value;
            }
            if ((metricProfile.Metric10Ratio != null) && (P10 != null))
            {
                combinedFinancialMetricValue += metricProfile.Metric10Ratio.Value * P10.Value;
            }
            if ((metricProfile.Metric11Ratio != null) && (P11 != null))
            {
                combinedFinancialMetricValue += metricProfile.Metric11Ratio.Value * P11.Value;
            }
            if ((metricProfile.Metric12Ratio != null) && (P12 != null))
            {
                combinedFinancialMetricValue += metricProfile.Metric12Ratio.Value * P12.Value;
            }
            if ((metricProfile.Metric13Ratio != null) && (P13 != null))
            {
                combinedFinancialMetricValue += metricProfile.Metric13Ratio.Value * P13.Value;
            }
            if ((metricProfile.Metric14Ratio != null) && (P14 != null))
            {
                combinedFinancialMetricValue += metricProfile.Metric14Ratio.Value * P14.Value;
            }
            if ((metricProfile.Metric15Ratio != null) && (P15 != null))
            {
                combinedFinancialMetricValue += metricProfile.Metric15Ratio.Value * P15.Value;
            }
            if ((metricProfile.Metric16Ratio != null) && (P16 != null))
            {
                combinedFinancialMetricValue += metricProfile.Metric16Ratio.Value * P16.Value;
            }
            if ((metricProfile.Metric17Ratio != null) && (P17 != null))
            {
                combinedFinancialMetricValue += metricProfile.Metric17Ratio.Value * P17.Value;
            }
            if ((metricProfile.Metric18Ratio != null) && (P18 != null))
            {
                combinedFinancialMetricValue += metricProfile.Metric18Ratio.Value * P18.Value;
            }
            if ((metricProfile.Metric19Ratio != null) && (P19 != null))
            {
                combinedFinancialMetricValue += metricProfile.Metric19Ratio.Value * P19.Value;
            }
            if ((metricProfile.Metric20Ratio != null) && (P20 != null))
            {
                combinedFinancialMetricValue += metricProfile.Metric20Ratio.Value * P20.Value;
            }
            this.CombinedFinancialMetricValue = combinedFinancialMetricValue;
        }
        
        /// <summary>
        /// Returns a copy of this dto
        /// </summary>
        /// <returns></returns>
        public WorkpackagePerformanceDataDto Clone()
        {
            WorkpackagePerformanceDataDto copy = new WorkpackagePerformanceDataDto();
            copy.P1 = this.P1;
            copy.P2 = this.P2;
            copy.P3 = this.P3;
            copy.P4 = this.P4;
            copy.P5 = this.P5;
            copy.P6 = this.P6;
            copy.P7 = this.P7;
            copy.P8 = this.P8;
            copy.P9 = this.P9;
            copy.P10 = this.P10;
            copy.P11 = this.P11;
            copy.P12 = this.P12;
            copy.P13 = this.P13;
            copy.P14 = this.P14;
            copy.P15 = this.P15;
            copy.P16 = this.P16;
            copy.P17 = this.P17;
            copy.P18 = this.P18;
            copy.P19 = this.P19;
            copy.P20 = this.P20;
            copy.CombinedFinancialMetricValue = this.CombinedFinancialMetricValue;
            copy.ProductGTIN = this.ProductGTIN;
            copy.DataId = this.DataId;
            copy.PerformanceSelectionId = this.PerformanceSelectionId;
            copy.WorkpackageId = this.WorkpackageId;

            return copy;
        }

        #endregion
    }
}
