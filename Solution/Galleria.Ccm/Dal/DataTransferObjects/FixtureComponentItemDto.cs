﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// FixtureComponentItem Data Transfer Object
    /// </summary>
    [Serializable]
    public class FixtureComponentItemDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public FixtureComponentItemDtoKey DtoKey
        {
            get
            {
                return new FixtureComponentItemDtoKey()
                {
                    Id = this.Id,
                };
            }
        }
        public Int32 FixtureId { get; set; }
        public Int32 FixtureComponentId { get; set; }
        public Single X { get; set; }
        public Single Y { get; set; }
        public Single Z { get; set; }
        public Single Slope { get; set; }
        public Single Angle { get; set; }
        public Single Roll { get; set; }
        public Object ExtendedData { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FixtureComponentItemDto other = obj as FixtureComponentItemDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.FixtureId != this.FixtureId) { return false; }
                if (other.FixtureComponentId != this.FixtureComponentId) { return false; }
                if (other.X != this.X) { return false; }
                if (other.Y != this.Y) { return false; }
                if (other.Z != this.Z) { return false; }
                if (other.Slope != this.Slope) { return false; }
                if (other.Angle != this.Angle) { return false; }
                if (other.Roll != this.Roll) { return false; }

                //ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData != null))
                {
                    if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
                }
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class FixtureComponentItemDtoKey
    {
        #region Properties
        public Int32 Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FixtureComponentItemDtoKey other = obj as FixtureComponentItemDtoKey;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
