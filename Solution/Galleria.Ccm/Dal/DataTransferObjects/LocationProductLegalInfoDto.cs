﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class LocationProductLegalInfoDto
    {
        #region Properties
        public Int16 LocationId { get; set; }
        public Int32 ProductId { get; set; }
        public Int32 EntityId { get; set; }
        public String LocationCode { get; set; }
        public String GTIN { get; set; }
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            LocationProductLegalInfoDto other = obj as LocationProductLegalInfoDto;
            if (other != null)
            {
                if (other.ProductId != this.ProductId) { return false; }
                if (other.LocationId != this.LocationId) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.LocationCode != this.LocationCode) { return false; }
                if (other.GTIN != this.GTIN) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
