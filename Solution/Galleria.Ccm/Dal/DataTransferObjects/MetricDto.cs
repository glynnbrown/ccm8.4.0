﻿#region Header Information

#region Version History:(CCM800)
//CCM-26158 : I.George
// Initial Version
#endregion

#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Metric Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class MetricDto
    {
        #region Properties

        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public MetricDtoKey DtoKey
        {
            get
            {
                return new MetricDtoKey()
                {
                    Name = this.Name,
                    EntityId = this.EntityId,
                };
            }
        }
        [ForeignKey(typeof(EntityDto), typeof(IMetricDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Byte Direction { get; set; }
        public Byte Type { get; set; }
        public Byte SpecialType { get; set; }
        public Byte DataModelType { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// returns the hash code for this object
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(Object obj)
        {
            MetricDto other = obj as MetricDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.Description != this.Description) { return false; }
                if (other.Direction != this.Direction) { return false; }
                if (other.Type != this.Type) { return false; }
                if (other.SpecialType != this.SpecialType) { return false; }
                if (other.DataModelType != this.DataModelType) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.DateDeleted != this.DateDeleted) { return false; }

            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class MetricDtoKey
    {
        #region Properties

        public String Name { get; set; }
        public Int32 EntityId { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// returns a hash code for this object
        /// </summary>
        /// <returns></returns>
        public override Int32 GetHashCode()
        {
            return
                EntityId.GetHashCode();
        }

        public override Boolean Equals(object obj)
        {
            MetricDtoKey other = obj as MetricDtoKey;
            if (other != null)
            {
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
