﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014.

#region Version History: (CCM 8.0)
// V8-28234 : A.Probyn
//      Adapted from SA.
#endregion
#endregion

using System;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Our data info Dto Class
    /// </summary>
    [Serializable]
    public class DataInfoDto
    {
        #region Properties
        public Boolean HasProductHierarchy { get; set; }
        public Boolean HasLocationHierarchy { get; set; } // Change to LocationHierarchyCount
        public Boolean HasProduct { get; set; }
        public Boolean HasLocation { get; set; } // Change to LocationCount
        public Boolean HasAssortment { get; set; }
        public Boolean HasLocationSpace { get; set; }
        public Boolean HasLocationSpaceBay { get; set; }
        public Boolean HasLocationSpaceElement { get; set; }
        public Boolean HasClusterScheme { get; set; }
        public Boolean HasLocationProductAttribute { get; set; }
        public Boolean HasLocationProductIllegal { get; set; }
        public Boolean HasLocationProductLegal { get; set; }
        public Boolean EntityId { get; set; }
        #endregion

        #region Methods

        public override bool Equals(object obj)
        {
            DataInfoDto other = obj as DataInfoDto;
            if (other != null)
            {
                if (other.HasProductHierarchy != this.HasProductHierarchy)
                {
                    return false;
                }
                if (other.HasProduct != this.HasProduct)
                {
                    return false;
                }
                if (other.HasLocation != this.HasLocation)
                {
                    return false;
                }
                if (other.HasLocationHierarchy != this.HasLocationHierarchy)
                {
                    return false;
                }
                if (other.HasAssortment != this.HasAssortment)
                {
                    return false;
                }
                if (other.HasClusterScheme != this.HasClusterScheme)
                {
                    return false;
                }
                if (other.HasLocationSpace != this.HasLocationSpace)
                {
                    return false;
                }
                if (other.HasLocationSpaceBay != this.HasLocationSpaceBay)
                {
                    return false;
                }
                if (other.HasLocationSpaceElement != this.HasLocationSpaceElement)
                {
                    return false;
                }
                if (other.HasLocationProductAttribute != this.HasLocationProductAttribute)
                {
                    return false;
                }
                if (other.HasLocationProductIllegal != this.HasLocationProductIllegal)
                {
                    return false;
                }
                if (other.HasLocationProductLegal != this.HasLocationProductLegal)
                {
                    return false;
                }
                if (other.EntityId != this.EntityId)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
