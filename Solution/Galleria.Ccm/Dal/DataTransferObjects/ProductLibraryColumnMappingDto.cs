﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25395 : A.Probyn
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// ProductLibraryColumnMapping Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class ProductLibraryColumnMappingDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public ProductLibraryColumnMappingDtoKey DtoKey
        {
            get
            {
                return new ProductLibraryColumnMappingDtoKey()
                {
                    ProductLibraryId = this.ProductLibraryId,
                    Destination = this.Destination,
                };
            }
        }
        [ForeignKey(typeof(ProductLibraryDto), typeof(IProductLibraryDal))]
        public Object ProductLibraryId { get; set; }
        public String Source { get; set; }
        public String Destination { get; set; }
        public Boolean IsMandatory { get; set; }
        public String Type { get; set; }
        public Single Min { get; set; }
        public Single Max { get; set; }
        public Boolean CanDefault { get; set; }
        public String Default { get; set; }
        public Int32 TextFixedWidthColumnStart { get; set; }
        public Int32 TextFixedWidthColumnEnd { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ProductLibraryColumnMappingDto other = obj as ProductLibraryColumnMappingDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }

                // ProductLibraryId
                if ((other.ProductLibraryId != null) && (this.ProductLibraryId != null))
                {
                    if (!other.ProductLibraryId.Equals(this.ProductLibraryId)) { return false; }
                }
                if ((other.ProductLibraryId != null) && (this.ProductLibraryId == null)) { return false; }
                if ((other.ProductLibraryId == null) && (this.ProductLibraryId != null)) { return false; }

                if (other.Source != this.Source) { return false; }
                if (other.Destination != this.Destination) { return false; }
                if (other.IsMandatory != this.IsMandatory) { return false; }
                if (other.Type != this.Type) { return false; }
                if (other.Min != this.Min) { return false; }
                if (other.Max != this.Max) { return false; }
                if (other.CanDefault != this.CanDefault) { return false; }
                if (other.Default != this.Default) { return false; }
                if (other.TextFixedWidthColumnStart != this.TextFixedWidthColumnStart) { return false; }
                if (other.TextFixedWidthColumnEnd != this.TextFixedWidthColumnEnd) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class ProductLibraryColumnMappingDtoKey
    {
        #region Properties
        public Object ProductLibraryId { get; set; }
        public String Destination { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return HashCode.Start
               .Hash(ProductLibraryId)
               .Hash(Destination)
               .GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ProductLibraryColumnMappingDtoKey other = obj as ProductLibraryColumnMappingDtoKey;
            if (other != null)
            {
                // ProductLibraryId
                if ((other.ProductLibraryId != null) && (this.ProductLibraryId != null))
                {
                    if (!other.ProductLibraryId.Equals(this.ProductLibraryId)) { return false; }
                }
                if ((other.ProductLibraryId != null) && (this.ProductLibraryId == null)) { return false; }
                if ((other.ProductLibraryId == null) && (this.ProductLibraryId != null)) { return false; }

                if (other.Destination != this.Destination) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
