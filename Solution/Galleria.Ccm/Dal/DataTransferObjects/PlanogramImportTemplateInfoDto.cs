﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-24779 : L.Luong
//		Created
#endregion

#region Version History: (CCM 8.03)
// V8-29220 : L.Ineson
//Removed date deleted
#endregion
#endregion

using System;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// PlanogramImportTemplateInfo Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramImportTemplateInfoDto
    {
        #region Properties

        public PlanogramImportTemplateDtoKey DtoKey
        {
            get
            {
                return new PlanogramImportTemplateDtoKey
                {
                    EntityId = EntityId,
                    Name = Name
                };
            }
        }
        public Object Id { get; set; }
        public Int32 EntityId { get; set; }
        public String Name { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramImportTemplateInfoDto other = obj as PlanogramImportTemplateInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
        
    }
}
