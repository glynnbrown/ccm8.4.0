﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class LocationProductLegalDto
    {
        #region Properties

        [ForeignKey(typeof(LocationDto), typeof(ILocationDal), DeleteBehavior.Leave)]
        public Int16 LocationId { get; set; }

        [ForeignKey(typeof(ProductDto), typeof(IProductDal), DeleteBehavior.Leave)]
        public Int32 ProductId { get; set; }

        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }

        public LocationProductLegalDtoKey DtoKey
        {
            get
            {
                return new LocationProductLegalDtoKey()
                {
                    EntityId = this.EntityId,
                    ProductId = this.ProductId,
                    LocationId = this.LocationId,
                };
            }
        }
        public RowVersion RowVersion { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }

        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            LocationProductLegalDto other = obj as LocationProductLegalDto;
            if (other != null)
            {
                if (other.ProductId != this.ProductId) { return false; }
                if (other.LocationId != this.LocationId) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class LocationProductLegalDtoKey
    {
        #region Properties
        public Int32 EntityId { get; set; }
        public Int16 LocationId { get; set; }
        public Int32 ProductId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                EntityId.GetHashCode() +
                LocationId.GetHashCode() +
                ProductId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            LocationProductLegalDtoKey other = obj as LocationProductLegalDtoKey;
            if (other != null)
            {
                if (other.EntityId != this.EntityId) { return false; }
                if (other.LocationId != this.LocationId) { return false; }
                if (other.ProductId != this.ProductId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
