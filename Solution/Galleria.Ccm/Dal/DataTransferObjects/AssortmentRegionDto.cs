﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25454 : J.Pickup
//		Created (Auto-generated)
// V8-26704 : A.Kuszyk
//  Added ForeignKey attribute.
// GFS-26739 : J.Pickup
//  Removed tempId 
#endregion

#region Version History : CCM830
// V8-31531 : A.Kuszyk
//  Changed DtoKey property from Id to AssortmentId in order to fix broken unit tests.
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// AssortmentRegion Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class AssortmentRegionDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public AssortmentRegionDtoKey DtoKey
        {
	        get 
	        {
		        return new AssortmentRegionDtoKey() 
		        {
                    AssortmentId = this.AssortmentId,
		        };
	        }
        } 
        public String Name { get; set; }
        [ForeignKey(typeof(AssortmentDto),typeof(IAssortmentDal))]
        public Int32 AssortmentId { get; set; } 
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentRegionDto other = obj as AssortmentRegionDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if(other.Name != this.Name) { return false; }
                if (other.AssortmentId != this.AssortmentId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion 
    }

    [Serializable]
    public class AssortmentRegionDtoKey
    {
        #region Properties
        public Int32 AssortmentId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return AssortmentId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentRegionDtoKey other = obj as AssortmentRegionDtoKey;
            if (other != null)
            {
                if (other.AssortmentId != this.AssortmentId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


    [Serializable]
    public class AssortmentRegionIsSetDto
    {
        #region Properties
        public Boolean IsNameSet { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public AssortmentRegionIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public AssortmentRegionIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsNameSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentRegionIsSetDto other = obj as AssortmentRegionIsSetDto;
            if (other != null)
            {
                if (other.IsNameSet != this.IsNameSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsNameSet = isSet;
        }
        #endregion
    }
}
