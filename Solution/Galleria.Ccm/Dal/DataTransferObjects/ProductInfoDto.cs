﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
// V8-25669 : A.Kuszyk
//  Added EntityId property.
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// ProductInfo data transfer object
    /// </summary>
    [Serializable]
    public sealed class ProductInfoDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public Int32 EntityId { get; set; }
        public String Gtin { get; set; }
        public String Name { get; set; }
        #endregion

        #region Methods

        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override bool Equals(Object obj)
        {
            ProductInfoDto other = obj as ProductInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Gtin != this.Gtin) { return false; }
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        #endregion
    }
}
