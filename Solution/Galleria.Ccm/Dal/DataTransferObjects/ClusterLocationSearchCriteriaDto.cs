﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion
#region Version History: (CCM 8.3.0)
// V8-31877 : N.Haywood
//  Added Product Group Code
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class ClusterLocationSearchCriteriaDto
    {
        #region Properties
        [InheritedProperty(typeof(ClusterSchemeDto), typeof(IClusterSchemeDal), "Name")]
        public String ClusterSchemeName { get; set; }
        [InheritedProperty(typeof(ClusterDto), typeof(IClusterDal), "Name")]
        public String ClusterName { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal), "Code")]
        public String LocationCode { get; set; }
        [InheritedProperty(typeof(ProductGroupDto), typeof(IProductGroupDal), "Code")]
        public String ProductGroupCode { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return ClusterSchemeName.ToLowerInvariant().GetHashCode()
                + ClusterName.ToLowerInvariant().GetHashCode()
                + LocationCode.ToLowerInvariant().GetHashCode()
                + ProductGroupCode.ToLowerInvariant().GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ClusterLocationSearchCriteriaDto other = obj as ClusterLocationSearchCriteriaDto;
            if (other != null)
            {
                StringComparer s = StringComparer.OrdinalIgnoreCase;

                if (s.Compare(other.ClusterSchemeName, this.ClusterSchemeName) != 0) { return false; }
                if (s.Compare(other.ClusterName, this.ClusterName) != 0) { return false; }
                if (s.Compare(other.LocationCode, this.LocationCode) != 0) { return false; }
                if (s.Compare(other.ProductGroupCode, this.ProductGroupCode) != 0) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
