﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// BlockingLocation Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class BlockingLocationDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public BlockingLocationDtoKey DtoKey
        {
            get
            {
                return new BlockingLocationDtoKey()
                {
                    Id = this.Id,
                };
            }
        }

        [ForeignKey(typeof(BlockingDto), typeof(IBlockingDal), DeleteBehavior.Leave)]
        public Int32 BlockingId { get; set; }

        [ForeignKey(typeof(BlockingGroupDto), typeof(IBlockingGroupDal))]
        public Int32 BlockingGroupId { get; set; }

        public Int32? BlockingDividerTopId { get; set; }
        public Int32? BlockingDividerBottomId { get; set; }
        public Int32? BlockingDividerLeftId { get; set; }
        public Int32? BlockingDividerRightId { get; set; }
        public Single SpacePercentage { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            BlockingLocationDto other = obj as BlockingLocationDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.BlockingId != this.BlockingId) { return false; }
                if (other.BlockingGroupId != this.BlockingGroupId) { return false; }
                if (other.BlockingDividerTopId != this.BlockingDividerTopId) { return false; }
                if (other.BlockingDividerBottomId != this.BlockingDividerBottomId) { return false; }
                if (other.BlockingDividerLeftId != this.BlockingDividerLeftId) { return false; }
                if (other.BlockingDividerRightId != this.BlockingDividerRightId) { return false; }
                if (other.SpacePercentage != this.SpacePercentage) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


    public class BlockingLocationDtoKey
    {

        #region Properties
        public Int32 Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            BlockingLocationDtoKey other = obj as BlockingLocationDtoKey;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }



}
