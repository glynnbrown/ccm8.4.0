﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)

// CCM-25454 : J.Pickup
//		Created (Auto-generated)
// CCM-27059 : J.Pickup
//      Reviewed FK attributes
// V8-26322 : A.Silva
//      Amended ProductId Property to have DeleteBehavior.Leave (parent is marked as deleted, not really deleted).

#endregion

#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// AssortmentProduct Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class AssortmentProductDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public AssortmentProductDtoKey DtoKey
        {
	        get 
	        {
		        return new AssortmentProductDtoKey() 
		        { 
                    AssortmentId = this.AssortmentId,
                    ProductId = this.ProductId
		        };
	        }
        }
        [ForeignKey(typeof(ProductDto), typeof(IProductDal), DeleteBehavior.Leave)]
        public Int32 ProductId { get; set; }
        [ForeignKey(typeof(AssortmentDto), typeof(IAssortmentDal))]
        public Int32 AssortmentId { get; set; }
        [InheritedProperty(typeof(ProductDto), typeof(IProductDal), ForeignKeyPropertyName = "ProductId", ParentPropertyName = "Gtin")]
        public String GTIN { get; set; }
        [InheritedProperty(typeof(ProductDto), typeof(IProductDal), ForeignKeyPropertyName = "ProductId", ParentPropertyName = "Name")]
        public String Name { get; set; } 
        public Boolean IsRanged { get; set; } 
        public Int16 Rank { get; set; } 
        public String Segmentation { get; set; } 
        public Byte Facings { get; set; } 
        public Int16 Units { get; set; } 
        public Byte ProductTreatmentType { get; set; } 
        public Byte ProductLocalizationType { get; set; } 
        public String Comments { get; set; } 
        public Byte? ExactListFacings { get; set; } 
        public Int16? ExactListUnits { get; set; } 
        public Byte? PreserveListFacings { get; set; } 
        public Int16? PreserveListUnits { get; set; } 
        public Byte? MaxListFacings { get; set; } 
        public Int16? MaxListUnits { get; set; } 
        public Byte? MinListFacings { get; set; } 
        public Int16? MinListUnits { get; set; } 
        public String FamilyRuleName { get; set; } 
        public Byte FamilyRuleType { get; set; } 
        public Byte? FamilyRuleValue { get; set; } 
        public Boolean IsPrimaryRegionalProduct { get; set; }
        [InheritedProperty(typeof(ProductDto), typeof(IProductDal), ForeignKeyPropertyName = "ProductId", ParentPropertyName = "DateDeleted")]
        public DateTime? ProductDateDeleted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentProductDto other = obj as AssortmentProductDto;
            if (other != null)
            {
                 if (other.Id != this.Id) { return false; }
                 if (other.GTIN != this.GTIN) { return false; }
                 if (other.Name != this.Name) { return false; } 
                 if(other.ProductId != this.ProductId) { return false; } 
                 if(other.IsRanged != this.IsRanged) { return false; } 
                 if(other.Rank != this.Rank) { return false; } 
                 if(other.Segmentation != this.Segmentation) { return false; } 
                 if(other.Facings != this.Facings) { return false; } 
                 if(other.Units != this.Units) { return false; } 
                 if(other.ProductTreatmentType != this.ProductTreatmentType) { return false; } 
                 if(other.ProductLocalizationType != this.ProductLocalizationType) { return false; } 
                 if(other.Comments != this.Comments) { return false; } 
                 if(other.ExactListFacings != this.ExactListFacings) { return false; } 
                 if(other.ExactListUnits != this.ExactListUnits) { return false; } 
                 if(other.PreserveListFacings != this.PreserveListFacings) { return false; } 
                 if(other.PreserveListUnits != this.PreserveListUnits) { return false; } 
                 if(other.MaxListFacings != this.MaxListFacings) { return false; } 
                 if(other.MaxListUnits != this.MaxListUnits) { return false; } 
                 if(other.MinListFacings != this.MinListFacings) { return false; } 
                 if(other.MinListUnits != this.MinListUnits) { return false; } 
                 if(other.FamilyRuleName != this.FamilyRuleName) { return false; } 
                 if(other.FamilyRuleType != this.FamilyRuleType) { return false; } 
                 if(other.FamilyRuleValue != this.FamilyRuleValue) { return false; } 
                 if(other.IsPrimaryRegionalProduct != this.IsPrimaryRegionalProduct) { return false; }
                 if (other.ProductDateDeleted != this.ProductDateDeleted) { return false; }
                 if (other.AssortmentId != this.AssortmentId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion 
    }

    [Serializable]
    public class AssortmentProductDtoKey
    {
        #region Properties
        public Int32 AssortmentId { get; set; }
        public Int32 ProductId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                AssortmentId.GetHashCode() ^
                ProductId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentProductDtoKey other = obj as AssortmentProductDtoKey;
            if (other != null)
            {
                if (other.AssortmentId != this.AssortmentId) { return false; }
                if (other.ProductId != this.ProductId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentProductIsSetDto
    {
        #region Properties
        public Boolean IsProductIdSet { get; set; }
        public Boolean IsGTINSet { get; set; }
        public Boolean IsNameSet { get; set; }
        public Boolean IsIsRangedSet { get; set; }
        public Boolean IsRankSet { get; set; }
        public Boolean IsSegmentationSet { get; set; }
        public Boolean IsFacingsSet { get; set; }
        public Boolean IsUnitsSet { get; set; }
        public Boolean IsProductTreatmentTypeSet { get; set; }
        public Boolean IsProductLocalizationTypeSet { get; set; }
        public Boolean IsCommentsSet { get; set; }
        public Boolean IsExactListFacingsSet { get; set; }
        public Boolean IsExactListUnitsSet { get; set; }
        public Boolean IsPreserveListFacingsSet { get; set; }
        public Boolean IsPreserveListUnitsSet { get; set; }
        public Boolean IsMaxListFacingsSet { get; set; }
        public Boolean IsMaxListUnitsSet { get; set; }
        public Boolean IsMinListFacingsSet { get; set; }
        public Boolean IsMinListUnitsSet { get; set; }
        public Boolean IsFamilyRuleNameSet { get; set; }
        public Boolean IsFamilyRuleTypeSet { get; set; }
        public Boolean IsFamilyRuleValueSet { get; set; }
        public Boolean IsIsPrimaryRegionalProductSet { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public AssortmentProductIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public AssortmentProductIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsProductIdSet.GetHashCode() +
                IsGTINSet.GetHashCode() +
                IsNameSet.GetHashCode() +
                IsIsRangedSet.GetHashCode() +
                IsRankSet.GetHashCode() +
                IsSegmentationSet.GetHashCode() +
                IsFacingsSet.GetHashCode() +
                IsUnitsSet.GetHashCode() +
                IsProductTreatmentTypeSet.GetHashCode() +
                IsProductLocalizationTypeSet.GetHashCode() +
                IsCommentsSet.GetHashCode() +
                IsExactListFacingsSet.GetHashCode() +
                IsExactListUnitsSet.GetHashCode() +
                IsPreserveListFacingsSet.GetHashCode() +
                IsPreserveListUnitsSet.GetHashCode() +
                IsMaxListFacingsSet.GetHashCode() +
                IsMaxListUnitsSet.GetHashCode() +
                IsMinListFacingsSet.GetHashCode() +
                IsMinListUnitsSet.GetHashCode() +
                IsFamilyRuleNameSet.GetHashCode() +
                IsFamilyRuleTypeSet.GetHashCode() +
                IsFamilyRuleValueSet.GetHashCode() +
                IsIsPrimaryRegionalProductSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentProductIsSetDto other = obj as AssortmentProductIsSetDto;
            if (other != null)
            {
                if (other.IsProductIdSet != this.IsProductIdSet) { return false; }
                if (other.IsGTINSet != this.IsGTINSet) { return false; }
                if (other.IsNameSet != this.IsNameSet) { return false; }
                if (other.IsIsRangedSet != this.IsIsRangedSet) { return false; }
                if (other.IsRankSet != this.IsRankSet) { return false; }
                if (other.IsSegmentationSet != this.IsSegmentationSet) { return false; }
                if (other.IsFacingsSet != this.IsFacingsSet) { return false; }
                if (other.IsUnitsSet != this.IsUnitsSet) { return false; }
                if (other.IsProductTreatmentTypeSet != this.IsProductTreatmentTypeSet) { return false; }
                if (other.IsProductLocalizationTypeSet != this.IsProductLocalizationTypeSet) { return false; }
                if (other.IsCommentsSet != this.IsCommentsSet) { return false; }
                if (other.IsExactListFacingsSet != this.IsExactListFacingsSet) { return false; }
                if (other.IsExactListUnitsSet != this.IsExactListUnitsSet) { return false; }
                if (other.IsPreserveListFacingsSet != this.IsPreserveListFacingsSet) { return false; }
                if (other.IsPreserveListUnitsSet != this.IsPreserveListUnitsSet) { return false; }
                if (other.IsMaxListFacingsSet != this.IsMaxListFacingsSet) { return false; }
                if (other.IsMaxListUnitsSet != this.IsMaxListUnitsSet) { return false; }
                if (other.IsMinListFacingsSet != this.IsMinListFacingsSet) { return false; }
                if (other.IsMinListUnitsSet != this.IsMinListUnitsSet) { return false; }
                if (other.IsFamilyRuleNameSet != this.IsFamilyRuleNameSet) { return false; }
                if (other.IsFamilyRuleTypeSet != this.IsFamilyRuleTypeSet) { return false; }
                if (other.IsFamilyRuleValueSet != this.IsFamilyRuleValueSet) { return false; }
                if (other.IsIsPrimaryRegionalProductSet != this.IsIsPrimaryRegionalProductSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsProductIdSet = isSet;
            IsGTINSet = isSet;
            IsNameSet = isSet;
            IsIsRangedSet = isSet;
            IsRankSet = isSet;
            IsSegmentationSet = isSet;
            IsFacingsSet = isSet;
            IsUnitsSet = isSet;
            IsProductTreatmentTypeSet = isSet;
            IsProductLocalizationTypeSet = isSet;
            IsCommentsSet = isSet;
            IsExactListFacingsSet = isSet;
            IsExactListUnitsSet = isSet;
            IsPreserveListFacingsSet = isSet;
            IsPreserveListUnitsSet = isSet;
            IsMaxListFacingsSet = isSet;
            IsMaxListUnitsSet = isSet;
            IsMinListFacingsSet = isSet;
            IsMinListUnitsSet = isSet;
            IsFamilyRuleNameSet = isSet;
            IsFamilyRuleTypeSet = isSet;
            IsFamilyRuleValueSet = isSet;
            IsIsPrimaryRegionalProductSet = isSet;
        }
        #endregion
    }
}
