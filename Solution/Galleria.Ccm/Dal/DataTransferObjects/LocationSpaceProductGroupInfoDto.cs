﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// V8-29026 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Location Space Product Group Info Data Transfer Object
    /// </summary>
    [Serializable]
    public class LocationSpaceProductGroupInfoDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public LocationSpaceProductGroupInfoDtoKey DtoKey
        {
            get
            {
                return new LocationSpaceProductGroupInfoDtoKey()
                {
                    LocationId = this.LocationId,
                    ProductGroupId = this.ProductGroupId
                };
            }
        }
        
        public Int32 ProductGroupId { get; set; }
        public Int16 LocationId { get; set; }
        public Single BayCount { get; set; }
        public Int32? ProductCount { get; set; }
        public Single AverageBayWidth { get; set; }
        public String AisleName { get; set; }
        public String ValleyName { get; set; }
        public String ZoneName { get; set; }
        public String CustomAttribute01 { get; set; }
        public String CustomAttribute02 { get; set; }
        public String CustomAttribute03 { get; set; }
        public String CustomAttribute04 { get; set; }
        public String CustomAttribute05 { get; set; }


        #endregion

        #region Methods
        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override bool Equals(object obj)
        {
            LocationSpaceProductGroupInfoDto other = obj as LocationSpaceProductGroupInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id)
                {
                    return false;
                }
                if (other.ProductGroupId != this.ProductGroupId)
                {
                    return false;
                }
                if (other.LocationId != this.LocationId)
                {
                    return false;
                }
                if (other.BayCount != this.BayCount)
                {
                    return false;
                }
                if (other.ProductCount != this.ProductCount)
                {
                    return false;
                }
                if (other.AverageBayWidth != this.AverageBayWidth)
                {
                    return false;
                }
                if (other.AisleName != this.AisleName)
                {
                    return false;
                }
                if (other.ValleyName != this.ValleyName)
                {
                    return false;
                }
                if (other.ZoneName != this.ZoneName)
                {
                    return false;
                }
                if (other.CustomAttribute01 != this.CustomAttribute01)
                {
                    return false;
                }
                if (other.CustomAttribute02 != this.CustomAttribute02)
                {
                    return false;
                }
                if (other.CustomAttribute03 != this.CustomAttribute03)
                {
                    return false;
                }
                if (other.CustomAttribute04 != this.CustomAttribute04)
                {
                    return false;
                }
                if (other.CustomAttribute05 != this.CustomAttribute05)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class LocationSpaceProductGroupInfoDtoKey
    {
        #region Properties
        public Int16 LocationId { get; set; }
        public Int32 ProductGroupId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                LocationId.GetHashCode() +
                ProductGroupId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            LocationSpaceProductGroupInfoDtoKey other = obj as LocationSpaceProductGroupInfoDtoKey;
            if (other != null)
            {
                if (other.LocationId != this.LocationId) { return false; }
                if (other.ProductGroupId != this.ProductGroupId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}