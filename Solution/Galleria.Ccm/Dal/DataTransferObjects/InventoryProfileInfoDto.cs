﻿#region Header Information

#region Version History:(CCM800)
//CCM-26124 : I.George
// Initial Version
#endregion
#region Version History:(CCM820)
//CCM-30836 : J.Pickup
// Introduced IPtype.
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// InventoryProfile Data transfer object
    /// </summary>
    [Serializable]
    public sealed class InventoryProfileInfoDto
    {
        #region Properties

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Byte InventoryProfileType { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>the object hash code</returns>
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are thesame
        /// </summary>
        /// <param name="obj">the object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override bool Equals(object obj)
        {
            InventoryProfileInfoDto other = obj as InventoryProfileInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.InventoryProfileType != this.InventoryProfileType) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion

    }
}
