﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-25664 : A.Kuszyk
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
	/// <summary>
	/// UserPlanogramGroup Data Transfer Object
	/// </summary>
	[Serializable]
	public sealed class UserPlanogramGroupDto
	{
		#region Properties
		public Int32 Id { get; set; }
		public UserPlanogramGroupDtoKey DtoKey
		{
			get 
			{
				return new UserPlanogramGroupDtoKey() 
				{ 
					UserId = this.UserId, 
					PlanogramGroupId = this.PlanogramGroupId
				};
			}
		} 
		[ForeignKey(typeof(UserDto),typeof(IUserDal),DeleteBehavior.Leave)]
        public Int32 UserId { get; set; } 
		[ForeignKey(typeof(PlanogramGroupDto),typeof(IPlanogramGroupDal), DeleteBehavior.Leave)]
        public Int32 PlanogramGroupId { get; set; }
		#endregion

		#region Methods
		/// <summary>
		/// Returns a hash code for this object
		/// </summary>
		/// <returns>The object hash code</returns>
		public override Int32 GetHashCode()
		{
			return this.Id.GetHashCode();
		}

		/// <summary>
		/// Check to see if two dtos are the same
		/// </summary>
		/// <param name="obj">The object to compare against</param>
		/// <returns>true if objects are equal</returns>
		public override Boolean Equals(Object obj)
		{
			UserPlanogramGroupDto other = obj as UserPlanogramGroupDto;
			if (other != null)
			{
				if (other.Id != this.Id) { return false; }
				if(other.UserId != this.UserId) { return false; } 
				if(other.PlanogramGroupId != this.PlanogramGroupId) { return false; }
			}
			else
			{
				return false;
			}
			return true;
		}
		#endregion 
	}

    [Serializable]
	public class UserPlanogramGroupDtoKey 
	{
		#region Properties
		public Int32 UserId { get; set; } 
		public Int32 PlanogramGroupId { get; set; }
		#endregion

		#region Methods
		/// <summary>
		/// Returns a hash code for this object
		/// </summary>
		/// <returns>The object hash code</returns>
		public override Int32 GetHashCode()
		{
			return UserId.GetHashCode() + PlanogramGroupId.GetHashCode();
		}

		/// <summary>
		/// Check to see if two keys are the same
		/// </summary>
		/// <param name="obj">The object to compare against</param>
		/// <returns>true if objects are equal</returns>
		public override Boolean Equals(Object obj)
		{
			UserPlanogramGroupDtoKey other = obj as UserPlanogramGroupDtoKey;
			if (other != null)
			{
				if(other.UserId != this.UserId) { return false; } 
				if(other.PlanogramGroupId != this.PlanogramGroupId) { return false; }
			}
			else
			{
				return false;
			}
			return true;
		}
		#endregion 
	}
}
