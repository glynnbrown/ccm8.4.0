﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 L.Hodson
//  Created
#endregion

#region Version History: (CCM 8.0)
// V8-30202 : M.Shelley
//  Added HighlightId to the DTOKey as the Name is not unique across multiple Highlights
#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// HighlightCharacteristic data transfer object
    /// </summary>
    [Serializable]
    public sealed class HighlightCharacteristicDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public HighlightCharacteristicDtoKey DtoKey
        {
            get
            {
                return new HighlightCharacteristicDtoKey()
                {
                    HighlightId = this.HighlightId,
                    Name = this.Name,
                };
            }
        }
        [ForeignKey(typeof(HighlightDto), typeof(IHighlightDal))]
        public Object HighlightId { get; set; }
        public String Name { get; set; }
        public Boolean IsAndFilter { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            HighlightCharacteristicDto other = obj as HighlightCharacteristicDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }

                //HighlightId
                if ((other.HighlightId != null) && (this.HighlightId != null))
                {
                    if (other.HighlightId != (this.HighlightId)) { return false; }
                }
                if ((other.HighlightId != null) && (this.HighlightId == null)) { return false; }
                if ((other.HighlightId == null) && (this.HighlightId != null)) { return false; }

                if (other.Name!= this.Name) { return false; }
                if (other.IsAndFilter != this.IsAndFilter) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    /// <summary>
    /// HighlightCharacteristic data transfer object key
    /// </summary>
    [Serializable]
    public sealed class HighlightCharacteristicDtoKey
    {
        #region Properties
        public Object HighlightId { get; set; }
        public String Name { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return HighlightId.GetHashCode() ^ Name.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the ISOme
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            HighlightCharacteristicDtoKey other = obj as HighlightCharacteristicDtoKey;
            if (other != null)
            {
				if ((other.HighlightId != null) && (this.HighlightId != null))
				{
                    if (other.HighlightId != this.HighlightId) { return false; }
				}
                if ((other.HighlightId != null) && (this.HighlightId == null)) { return false; }
                if ((other.HighlightId == null) && (this.HighlightId != null)) { return false; }

                if (other.Name !=  this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
