﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25454 : J.Pickup
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// File Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class FileInfoDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public Int32 EntityId { get; set; }
        public String Name { get; set; }
        public String SourceFilePath { get; set; }
        public Int64 SizeInBytes { get; set; }
        public DateTime DateLastModified { get; set; }
        public Int32? UserId { get; set; } 
        public FileInfoDtoKey DtoKey
        {
            get
            {
                return new FileInfoDtoKey()
                {
                    EntityId = this.EntityId,
                    UserId = this.UserId
                };
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FileInfoDto other = obj as FileInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.EntityId != this.EntityId) return false;
                if (other.Name != this.Name) return false;
                if (other.SourceFilePath != this.SourceFilePath) return false;
                if (other.SizeInBytes != this.SizeInBytes) return false;
                if (other.DateLastModified != this.DateLastModified) return false;
                if (other.UserId != this.UserId) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class FileInfoDtoKey
    {
        #region Properties
        public Int32 EntityId { get; set; }
        public Int32 BlobId { get; set; }
        public Int32? UserId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                EntityId.GetHashCode() +
                BlobId.GetHashCode() +
                UserId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FileInfoDtoKey other = obj as FileInfoDtoKey;
            if (other != null)
            {
                if (other.EntityId != this.EntityId) return false;
                if (other.BlobId != this.BlobId) return false;
                if (other.UserId != this.UserId) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class FileInfoIsSetDto
    {
        #region Properties
        public Boolean IsEntityIdSet { get; set; }
        public Boolean IsBlobIdSet { get; set; }
        public Boolean IsUserIdSet { get; set; }
        public Boolean IsNameSet { get; set; }
        public Boolean IsSourceFilePathSet { get; set; }
        public Boolean IsSizeInBytesSet { get; set; }
        public Boolean IsDataSet { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public FileInfoIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public FileInfoIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsEntityIdSet.GetHashCode() +
                IsBlobIdSet.GetHashCode() +
                IsUserIdSet.GetHashCode() +
                IsNameSet.GetHashCode() +
                IsSourceFilePathSet.GetHashCode() +
                IsSizeInBytesSet.GetHashCode() +
                IsDataSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FileInfoIsSetDto other = obj as FileInfoIsSetDto;
            if (other != null)
            {
                if (other.IsEntityIdSet != this.IsEntityIdSet) return false;
                if (other.IsBlobIdSet != this.IsBlobIdSet) return false;
                if (other.IsUserIdSet != this.IsUserIdSet) return false;
                if (other.IsNameSet != this.IsNameSet) return false;
                if (other.IsSourceFilePathSet != this.IsSourceFilePathSet) return false;
                if (other.IsSizeInBytesSet != this.IsSizeInBytesSet) return false;
                if (other.IsDataSet != this.IsDataSet) return false;
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsEntityIdSet = isSet;
            IsBlobIdSet = isSet;
            IsUserIdSet = isSet;
            IsNameSet = isSet;
            IsSourceFilePathSet = isSet;
            IsSizeInBytesSet = isSet;
            IsDataSet = isSet;
        }
        #endregion
    }
}
