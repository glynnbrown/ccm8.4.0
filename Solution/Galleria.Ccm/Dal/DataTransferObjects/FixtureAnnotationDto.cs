﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM CCM830)
// CCM-32524 : L.Ineson
//		Created
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class FixtureAnnotationDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public FixtureAnnotationDtoKey DtoKey
        {
            get 
            { 
                 return new FixtureAnnotationDtoKey() 
                 {
                     FixturePackageId = this.FixturePackageId,
                     FixtureItemId = this.FixtureItemId,
                     FixtureAssemblyItemId = this.FixtureAssemblyItemId,
                     FixtureComponentItemId = this.FixtureComponentItemId,
                     FixtureAssemblyComponentId = this.FixtureAssemblyComponentId,
                     FixtureSubComponentId = this.FixtureSubComponentId
                 }; 
            }
        }
        [ForeignKey(typeof(FixturePackageDto), typeof(IFixturePackageDal), DeleteBehavior.Cascade)]
        public Object FixturePackageId { get; set; }
        [ForeignKey(typeof(FixtureItemDto), typeof(IFixtureItemDal), DeleteBehavior.Cascade)]
        public Int32? FixtureItemId { get; set; }
        [ForeignKey(typeof(FixtureAssemblyItemDto), typeof(IFixtureAssemblyItemDal), DeleteBehavior.Cascade)]
        public Int32? FixtureAssemblyItemId { get; set; }
        [ForeignKey(typeof(FixtureComponentItemDto), typeof(IFixtureComponentItemDal), DeleteBehavior.Cascade)]
        public Int32? FixtureComponentItemId { get; set; }
        [ForeignKey(typeof(FixtureAssemblyComponentDto), typeof(IFixtureAssemblyComponentDal), DeleteBehavior.Cascade)]
        public Int32? FixtureAssemblyComponentId { get; set; }
        [ForeignKey(typeof(FixtureSubComponentDto), typeof(IFixtureSubComponentDal), DeleteBehavior.Cascade)]
        public Int32? FixtureSubComponentId { get; set; }
        public Byte AnnotationType { get; set; }
        public String Text { get; set; }
        public Single? X { get; set; }
        public Single? Y { get; set; }
        public Single? Z { get; set; }
        public Single Height { get; set; }
        public Single Width { get; set; }
        public Single Depth { get; set; }
        public Int32 BorderColour { get; set; }
        public Single BorderThickness { get; set; }
        public Int32 BackgroundColour { get; set; }
        public Int32 FontColour { get; set; }
        public Single FontSize { get; set; }
        public String FontName { get; set; }
        public Boolean CanReduceFontToFit { get; set; }
        public Object ExtendedData { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FixtureAnnotationDto other = obj as FixtureAnnotationDto;
            if (other != null)
            {
                // Id
                if (other.Id != this.Id) return false;

                // FixturePackageId
                if ((other.FixturePackageId != null) && (this.FixturePackageId != null))
                {
                    if (!other.FixturePackageId.Equals(this.FixturePackageId)) { return false; }
                }
                if ((other.FixturePackageId != null) && (this.FixturePackageId == null)) { return false; }
                if ((other.FixturePackageId == null) && (this.FixturePackageId != null)) { return false; }


                if (other.FixtureItemId != this.FixtureItemId) return false;
                if (other.FixtureAssemblyItemId != this.FixtureAssemblyItemId) return false;
                if (other.FixtureComponentItemId != this.FixtureComponentItemId) return false;
                if (other.FixtureAssemblyComponentId != this.FixtureAssemblyComponentId) return false;
                if (other.FixtureSubComponentId != this.FixtureSubComponentId) return false;
                if (other.AnnotationType != this.AnnotationType) { return false; }
                if (other.Text != this.Text) { return false; }
                if (other.X != this.X) { return false; }
                if (other.Y != this.Y) { return false; }
                if (other.Z != this.Z) { return false; }
                if (other.Height != this.Height) { return false; }
                if (other.Width != this.Width) { return false; }
                if (other.Depth != this.Depth) { return false; }
                if (other.BorderColour != this.BorderColour) { return false; }
                if (other.BorderThickness != this.BorderThickness) { return false; }
                if (other.BackgroundColour != this.BackgroundColour) { return false; }
                if (other.FontColour != this.FontColour) { return false; }
                if (other.FontSize != this.FontSize) { return false; }
                if (other.FontName != this.FontName) { return false; }
                if (other.CanReduceFontToFit != this.CanReduceFontToFit) { return false; }
                // ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData != null))
                {
                    if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
                }
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    public class FixtureAnnotationDtoKey
    {

        #region Properties
        public Object FixturePackageId { get; set; }
        public Int32? FixtureItemId { get; set; }
        public Int32? FixtureAssemblyItemId { get; set; }
        public Int32? FixtureComponentItemId { get; set; }
        public Int32? FixtureAssemblyComponentId { get; set; }
        public Int32? FixtureSubComponentId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                HashCode.Start
                .Hash(FixturePackageId)
                .Hash(FixtureItemId)
                .Hash(FixtureAssemblyItemId)
                .Hash(FixtureComponentItemId)
                .Hash(FixtureAssemblyComponentId)
                .Hash(FixtureSubComponentId);
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FixtureAnnotationDtoKey other = obj as FixtureAnnotationDtoKey;
            if (other != null)
            {
                // FixturePackageId
                if ((other.FixturePackageId != null) && (this.FixturePackageId != null))
                {
                    if (!other.FixturePackageId.Equals(this.FixturePackageId)) { return false; }
                }
                if ((other.FixturePackageId != null) && (this.FixturePackageId == null)) { return false; }
                if ((other.FixturePackageId == null) && (this.FixturePackageId != null)) { return false; }

                if (other.FixtureItemId != this.FixtureItemId) return false;
                if (other.FixtureAssemblyItemId != this.FixtureAssemblyItemId) return false;
                if (other.FixtureComponentItemId != this.FixtureComponentItemId) return false;
                if (other.FixtureAssemblyComponentId != this.FixtureAssemblyComponentId) return false;
                if (other.FixtureSubComponentId != this.FixtureSubComponentId) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
