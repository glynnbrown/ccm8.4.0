﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Ineson
//		Created (Auto-generated)
// CCM-26091 : L.Ineson
//  Added more facethick properties
#endregion
#region Version History: (CCM CCM802)
// V8-29023 : M.Pettit
//		Added MerchandisableDepth
// V8-29203 : L.Ineson
//  Added IsProductSqueezeAllowed
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// FixtureSubComponent Data Transfer Object
    /// </summary>
    [Serializable]
    public class FixtureSubComponentDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public FixtureSubComponentDtoKey DtoKey
        {
            get
            {
                return new FixtureSubComponentDtoKey()
                {
                    FixtureComponentId = this.FixtureComponentId,
                    Name = this.Name
                };
            }
        }
        public Int32 FixtureComponentId { get; set; }
        public Int32? Mesh3DId { get; set; }
        public Int32? ImageIdFront { get; set; }
        public Int32? ImageIdBack { get; set; }
        public Int32? ImageIdTop { get; set; }
        public Int32? ImageIdBottom { get; set; }
        public Int32? ImageIdLeft { get; set; }
        public Int32? ImageIdRight { get; set; }
        public String Name { get; set; }
        public Single Height { get; set; }
        public Single Width { get; set; }
        public Single Depth { get; set; }
        public Single X { get; set; }
        public Single Y { get; set; }
        public Single Z { get; set; }
        public Single Slope { get; set; }
        public Single Angle { get; set; }
        public Single Roll { get; set; }
        public Byte ShapeType { get; set; }
        public Single MerchandisableHeight { get; set; }
        public Single MerchandisableDepth { get; set; }
        public Boolean IsVisible { get; set; }
        public Boolean HasCollisionDetection { get; set; }
        public Byte FillPatternTypeFront { get; set; }
        public Byte FillPatternTypeBack { get; set; }
        public Byte FillPatternTypeTop { get; set; }
        public Byte FillPatternTypeBottom { get; set; }
        public Byte FillPatternTypeLeft { get; set; }
        public Byte FillPatternTypeRight { get; set; }
        public Int32 FillColourFront { get; set; }
        public Int32 FillColourBack { get; set; }
        public Int32 FillColourTop { get; set; }
        public Int32 FillColourBottom { get; set; }
        public Int32 FillColourLeft { get; set; }
        public Int32 FillColourRight { get; set; }
        public Int32 LineColour { get; set; }
        public Int32 TransparencyPercentFront { get; set; }
        public Int32 TransparencyPercentBack { get; set; }
        public Int32 TransparencyPercentTop { get; set; }
        public Int32 TransparencyPercentBottom { get; set; }
        public Int32 TransparencyPercentLeft { get; set; }
        public Int32 TransparencyPercentRight { get; set; }
        public Single FaceThicknessFront { get; set; }
        public Single FaceThicknessBack { get; set; }
        public Single FaceThicknessTop { get; set; }
        public Single FaceThicknessBottom { get; set; }
        public Single FaceThicknessLeft { get; set; }
        public Single FaceThicknessRight { get; set; }
        public Single RiserHeight { get; set; }
        public Single RiserThickness { get; set; }
        public Boolean IsRiserPlacedOnFront { get; set; }
        public Boolean IsRiserPlacedOnBack { get; set; }
        public Boolean IsRiserPlacedOnLeft { get; set; }
        public Boolean IsRiserPlacedOnRight { get; set; }
        public Byte RiserFillPatternType { get; set; }
        public Int32 RiserColour { get; set; }
        public Int32 RiserTransparencyPercent { get; set; }
        public Single NotchStartX { get; set; }
        public Single NotchSpacingX { get; set; }
        public Single NotchStartY { get; set; }
        public Single NotchSpacingY { get; set; }
        public Single NotchHeight { get; set; }
        public Single NotchWidth { get; set; }
        public Boolean IsNotchPlacedOnFront { get; set; }
        public Boolean IsNotchPlacedOnBack { get; set; }
        public Boolean IsNotchPlacedOnLeft { get; set; }
        public Boolean IsNotchPlacedOnRight { get; set; }
        public Byte NotchStyleType { get; set; }
        public Single DividerObstructionHeight { get; set; }
        public Single DividerObstructionWidth { get; set; }
        public Single DividerObstructionDepth { get; set; }
        public Single DividerObstructionStartX { get; set; }
        public Single DividerObstructionSpacingX { get; set; }
        public Single DividerObstructionStartY { get; set; }
        public Single DividerObstructionSpacingY { get; set; }
        public Single DividerObstructionStartZ { get; set; }
        public Single DividerObstructionSpacingZ { get; set; }
        public Boolean IsDividerObstructionAtStart { get; set; }
        public Boolean IsDividerObstructionAtEnd { get; set; }
        public Boolean IsDividerObstructionByFacing { get; set; }
        public Int32 DividerObstructionFillColour { get; set; }
        public Byte DividerObstructionFillPattern { get; set; }
        public Single MerchConstraintRow1StartX { get; set; }
        public Single MerchConstraintRow1SpacingX { get; set; }
        public Single MerchConstraintRow1StartY { get; set; }
        public Single MerchConstraintRow1SpacingY { get; set; }
        public Single MerchConstraintRow1Height { get; set; }
        public Single MerchConstraintRow1Width { get; set; }
        public Single MerchConstraintRow2StartX { get; set; }
        public Single MerchConstraintRow2SpacingX { get; set; }
        public Single MerchConstraintRow2StartY { get; set; }
        public Single MerchConstraintRow2SpacingY { get; set; }
        public Single MerchConstraintRow2Height { get; set; }
        public Single MerchConstraintRow2Width { get; set; }
        public Single LineThickness { get; set; }
        public Byte MerchandisingType { get; set; }
        public Byte CombineType { get; set; }
        public Boolean IsProductOverlapAllowed { get; set; }
        public Boolean IsProductSqueezeAllowed { get; set; }
        public Byte MerchandisingStrategyX { get; set; }
        public Byte MerchandisingStrategyY { get; set; }
        public Byte MerchandisingStrategyZ { get; set; }
        public Single LeftOverhang { get; set; }
        public Single RightOverhang { get; set; }
        public Single FrontOverhang { get; set; }
        public Single BackOverhang { get; set; }
        public Single TopOverhang { get; set; }
        public Single BottomOverhang { get; set; }
        public Object ExtendedData { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FixtureSubComponentDto other = obj as FixtureSubComponentDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.FixtureComponentId != this.FixtureComponentId) { return false; }
                if (other.Mesh3DId != this.Mesh3DId) { return false; }
                if (other.ImageIdFront != this.ImageIdFront) { return false; }
                if (other.ImageIdBack != this.ImageIdBack) { return false; }
                if (other.ImageIdTop != this.ImageIdTop) { return false; }
                if (other.ImageIdBottom != this.ImageIdBottom) { return false; }
                if (other.ImageIdLeft != this.ImageIdLeft) { return false; }
                if (other.ImageIdRight != this.ImageIdRight) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.Height != this.Height) { return false; }
                if (other.Width != this.Width) { return false; }
                if (other.Depth != this.Depth) { return false; }
                if (other.X != this.X) { return false; }
                if (other.Y != this.Y) { return false; }
                if (other.Z != this.Z) { return false; }
                if (other.Slope != this.Slope) { return false; }
                if (other.Angle != this.Angle) { return false; }
                if (other.Roll != this.Roll) { return false; }
                if (other.ShapeType != this.ShapeType) { return false; }
                if (other.MerchandisableHeight != this.MerchandisableHeight) { return false; }
                if (other.MerchandisableDepth != this.MerchandisableDepth) { return false; }
                if (other.IsVisible != this.IsVisible) { return false; }
                if (other.HasCollisionDetection != this.HasCollisionDetection) { return false; }
                if (other.FillPatternTypeFront != this.FillPatternTypeFront) { return false; }
                if (other.FillPatternTypeBack != this.FillPatternTypeBack) { return false; }
                if (other.FillPatternTypeTop != this.FillPatternTypeTop) { return false; }
                if (other.FillPatternTypeBottom != this.FillPatternTypeBottom) { return false; }
                if (other.FillPatternTypeLeft != this.FillPatternTypeLeft) { return false; }
                if (other.FillPatternTypeRight != this.FillPatternTypeRight) { return false; }
                if (other.FillColourFront != this.FillColourFront) { return false; }
                if (other.FillColourBack != this.FillColourBack) { return false; }
                if (other.FillColourTop != this.FillColourTop) { return false; }
                if (other.FillColourBottom != this.FillColourBottom) { return false; }
                if (other.FillColourLeft != this.FillColourLeft) { return false; }
                if (other.FillColourRight != this.FillColourRight) { return false; }
                if (other.LineColour != this.LineColour) { return false; }
                if (other.TransparencyPercentFront != this.TransparencyPercentFront) { return false; }
                if (other.TransparencyPercentBack != this.TransparencyPercentBack) { return false; }
                if (other.TransparencyPercentTop != this.TransparencyPercentTop) { return false; }
                if (other.TransparencyPercentBottom != this.TransparencyPercentBottom) { return false; }
                if (other.TransparencyPercentLeft != this.TransparencyPercentLeft) { return false; }
                if (other.TransparencyPercentRight != this.TransparencyPercentRight) { return false; }
                if (other.FaceThicknessFront != this.FaceThicknessFront) { return false; }
                if (other.FaceThicknessBack != this.FaceThicknessBack) { return false; }
                if (other.FaceThicknessTop != this.FaceThicknessTop) { return false; }
                if (other.FaceThicknessBottom != this.FaceThicknessBottom) { return false; }
                if (other.FaceThicknessLeft != this.FaceThicknessLeft) { return false; }
                if (other.FaceThicknessRight != this.FaceThicknessRight) { return false; }
                if (other.RiserHeight != this.RiserHeight) { return false; }
                if (other.RiserThickness != this.RiserThickness) { return false; }
                if (other.IsRiserPlacedOnFront != this.IsRiserPlacedOnFront) { return false; }
                if (other.IsRiserPlacedOnBack != this.IsRiserPlacedOnBack) { return false; }
                if (other.IsRiserPlacedOnLeft != this.IsRiserPlacedOnLeft) { return false; }
                if (other.IsRiserPlacedOnRight != this.IsRiserPlacedOnRight) { return false; }
                if (other.RiserFillPatternType != this.RiserFillPatternType) { return false; }
                if (other.RiserColour != this.RiserColour) { return false; }
                if (other.RiserTransparencyPercent != this.RiserTransparencyPercent) { return false; }
                if (other.NotchStartX != this.NotchStartX) { return false; }
                if (other.NotchSpacingX != this.NotchSpacingX) { return false; }
                if (other.NotchStartY != this.NotchStartY) { return false; }
                if (other.NotchSpacingY != this.NotchSpacingY) { return false; }
                if (other.NotchHeight != this.NotchHeight) { return false; }
                if (other.NotchWidth != this.NotchWidth) { return false; }
                if (other.IsNotchPlacedOnFront != this.IsNotchPlacedOnFront) { return false; }
                if (other.IsNotchPlacedOnBack != this.IsNotchPlacedOnBack) { return false; }
                if (other.IsNotchPlacedOnLeft != this.IsNotchPlacedOnLeft) { return false; }
                if (other.IsNotchPlacedOnRight != this.IsNotchPlacedOnRight) { return false; }
                if (other.NotchStyleType != this.NotchStyleType) { return false; }
                if (other.DividerObstructionHeight != this.DividerObstructionHeight) { return false; }
                if (other.DividerObstructionWidth != this.DividerObstructionWidth) { return false; }
                if (other.DividerObstructionDepth != this.DividerObstructionDepth) { return false; }
                if (other.DividerObstructionStartX != this.DividerObstructionStartX) { return false; }
                if (other.DividerObstructionSpacingX != this.DividerObstructionSpacingX) { return false; }
                if (other.DividerObstructionStartY != this.DividerObstructionStartY) { return false; }
                if (other.DividerObstructionSpacingY != this.DividerObstructionSpacingY) { return false; }
                if (other.DividerObstructionStartZ != this.DividerObstructionStartZ) { return false; }
                if (other.DividerObstructionSpacingZ != this.DividerObstructionSpacingZ) { return false; }
                if (other.IsDividerObstructionAtStart != this.IsDividerObstructionAtStart) { return false; }
                if (other.IsDividerObstructionAtEnd != this.IsDividerObstructionAtEnd) { return false; }
                if (other.IsDividerObstructionByFacing != this.IsDividerObstructionByFacing) { return false; }
                if(other.DividerObstructionFillColour != this.DividerObstructionFillColour) { return false; }
                if(other.DividerObstructionFillPattern != this.DividerObstructionFillPattern) { return false; }
                if (other.MerchConstraintRow1StartX != this.MerchConstraintRow1StartX) { return false; }
                if (other.MerchConstraintRow1SpacingX != this.MerchConstraintRow1SpacingX) { return false; }
                if (other.MerchConstraintRow1StartY != this.MerchConstraintRow1StartY) { return false; }
                if (other.MerchConstraintRow1SpacingY != this.MerchConstraintRow1SpacingY) { return false; }
                if (other.MerchConstraintRow1Height != this.MerchConstraintRow1Height) { return false; }
                if (other.MerchConstraintRow1Width != this.MerchConstraintRow1Width) { return false; }
                if (other.MerchConstraintRow2StartX != this.MerchConstraintRow2StartX) { return false; }
                if (other.MerchConstraintRow2SpacingX != this.MerchConstraintRow2SpacingX) { return false; }
                if (other.MerchConstraintRow2StartY != this.MerchConstraintRow2StartY) { return false; }
                if (other.MerchConstraintRow2SpacingY != this.MerchConstraintRow2SpacingY) { return false; }
                if (other.MerchConstraintRow2Height != this.MerchConstraintRow2Height) { return false; }
                if (other.MerchConstraintRow2Width != this.MerchConstraintRow2Width) { return false; }
                if (other.LineThickness != this.LineThickness) { return false; }
                if (other.MerchandisingType != this.MerchandisingType) { return false; }
                if (other.CombineType != this.CombineType) { return false; }
                if (other.IsProductOverlapAllowed != this.IsProductOverlapAllowed) { return false; }
                if (other.IsProductSqueezeAllowed != this.IsProductSqueezeAllowed) { return false; }
                if (other.MerchandisingStrategyX != this.MerchandisingStrategyX) { return false; }
                if (other.MerchandisingStrategyY != this.MerchandisingStrategyY) { return false; }
                if (other.MerchandisingStrategyZ != this.MerchandisingStrategyZ) { return false; }
                if (other.LeftOverhang != this.LeftOverhang) { return false; }
                if (other.RightOverhang != this.RightOverhang) { return false; }
                if (other.FrontOverhang != this.FrontOverhang) { return false; }
                if (other.BackOverhang != this.BackOverhang) { return false; }
                if (other.TopOverhang != this.TopOverhang) { return false; }
                if (other.BottomOverhang != this.BottomOverhang) { return false; }

                //ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData != null))
                {
                    if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
                }
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class FixtureSubComponentDtoKey
    {
        #region Properties
        public Int32 FixtureComponentId { get; set; }
        public String Name { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                 FixtureComponentId.GetHashCode() +
                 Name.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FixtureSubComponentDtoKey other = obj as FixtureSubComponentDtoKey;
            if (other != null)
            {
                if (other.FixtureComponentId != this.FixtureComponentId) { return false; }
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
