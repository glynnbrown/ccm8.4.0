﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class RepositorySyncTargetDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public Byte RepositoryType { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the instance has code
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Indicates if two instances are equal
        /// </summary>
        /// <param name="obj">The instance to compare to</param>
        /// <returns>True if equal, else false</returns>
        public override bool Equals(object obj)
        {
            RepositorySyncTargetDto other = obj as RepositorySyncTargetDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.RepositoryType != this.RepositoryType) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
