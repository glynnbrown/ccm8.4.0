﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class ImageDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public ImageDtoKey DtoKey
        {
            get
            {
                return new ImageDtoKey()
                {
                    Id=this.Id
                };
            }
        }
        [ForeignKey(typeof(FileDto),typeof(IFileDal), DeleteBehavior.Leave)]
        public Int32 OriginalFileId { get; set; }
        [InheritedProperty(typeof(FileDto), typeof(IFileDal), "EntityId", "OriginalFileId")]
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        [ForeignKey(typeof(BlobDto), typeof(IBlobDal), DeleteBehavior.Leave)]
        public Int32 BlobId { get; set; }
        [ForeignKey(typeof(CompressionDto),typeof(ICompressionDal),DeleteBehavior.Leave)]
        public Int32 CompressionId { get; set; }
        public Int16 Height { get; set; }
        public Int16 Width { get; set; }
        public Int64 SizeInBytes { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override bool Equals(object obj)
        {
            ImageDto other = obj as ImageDto;
            if (other != null)
            {
                if (other.Id != this.Id)
                {
                    return false;
                }
                if (other.RowVersion != this.RowVersion)
                {
                    return false;
                }
                if (other.EntityId != this.EntityId)
                {
                    return false;
                }
                if (other.OriginalFileId != this.OriginalFileId)
                {
                    return false;
                }
                if (other.Height != this.Height)
                {
                    return false;
                }
                if (other.Width != this.Width)
                {
                    return false;
                }
                if (other.SizeInBytes != this.SizeInBytes)
                {
                    return false;
                }
                if (other.BlobId != this.BlobId)
                {
                    return false;
                }
                if (other.CompressionId != this.CompressionId)
                {
                    return false;
                }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.DateDeleted != this.DateDeleted) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    public class ImageDtoKey
    {
        public Int32 Id { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as ImageDtoKey;
            if (other == null) return false;
            return other.Id == this.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
