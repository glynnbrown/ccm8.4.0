#region Header Information

// Copyright � Galleria RTS Ltd 2015

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva
//  Created.
// V8-26076 : A.Silva 
//  Removed Id Property and DtoKey.
// V8-26201 : A.Silva 
//  Readded DtoKey and corrected Equals.
// V8-26408 : L.Ineson
//  Removed mapping id.
// V8-26472 : A.Probyn
//      Removed obsolete CustomColumn fields.
//      Consolodated ColumnTotalType and TotalType which were being used in Model Object and user cache differently.
// V8-27504 : A.Silva
//      Added HeaderGroupNumber property.

#endregion
#region Version History: (CCM 8.2)
// V8-30870 : L.Ineson
//  Added Width and DisplayName
#endregion

#region Version History: (CCM 8.3)
// V8-31542 : L.Ineson
//  Added Id and updated implementation.
//V8-32122 : L.Ineson
//  Removed HeaderGroupNumber
#endregion

#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// CustomColumn Data Transfer Object
    /// </summary>
    [Serializable]
    public class CustomColumnDto
    {
        #region Properties

        public Int32 Id { get; set; }

        [ForeignKey(typeof (CustomColumnLayoutDto), typeof (ICustomColumnLayoutDal))]
        public Object CustomColumnLayoutId { get; set; }

        public CustomColumnDtoKey DtoKey
        {
            get { return new CustomColumnDtoKey {CustomColumnLayoutId =CustomColumnLayoutId,  Path = Path}; }
        }

        public Boolean IsVisibleOnLoad { get; set; }
        public Int32 Number { get; set; }
        public String Path { get; set; }
        public Byte TotalType { get; set; }
        public Int32 Width { get; set; }
        public String DisplayName { get; set; }

        #endregion

        #region Equals override

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            CustomColumnDto other = obj as CustomColumnDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;

                // CustomColumnLayoutId
                if ((other.CustomColumnLayoutId != null) && (this.CustomColumnLayoutId != null))
                {
                    if (!other.CustomColumnLayoutId.Equals(this.CustomColumnLayoutId)) { return false; }
                }
                if ((other.CustomColumnLayoutId != null) && (this.CustomColumnLayoutId == null)) { return false; }
                if ((other.CustomColumnLayoutId == null) && (this.CustomColumnLayoutId != null)) { return false; }

                if (other.IsVisibleOnLoad != this.IsVisibleOnLoad) return false;
                if (other.Number != this.Number) return false;
                if (other.Path != this.Path) return false;
                if (other.TotalType != this.TotalType) return false;
                if (other.Width != this.Width) return false;
                if (other.DisplayName != this.DisplayName) return false;
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        #endregion
    }

    /// <summary>
    /// CustomColumn Data Transfer Object Key
    /// </summary>
    [Serializable]
    public sealed class CustomColumnDtoKey
    {
        #region Properties

        public Object CustomColumnLayoutId { get; set; }
        public String Path { get; set; }

        #endregion

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            CustomColumnDtoKey other = obj as CustomColumnDtoKey;
            if (other == null) return false;

            // CustomColumnLayoutId
            if ((other.CustomColumnLayoutId != null) && (this.CustomColumnLayoutId != null))
            {
                if (!other.CustomColumnLayoutId.Equals(this.CustomColumnLayoutId)) { return false; }
            }
            if ((other.CustomColumnLayoutId != null) && (this.CustomColumnLayoutId == null)) { return false; }
            if ((other.CustomColumnLayoutId == null) && (this.CustomColumnLayoutId != null)) { return false; }
            
            if (other.Path != Path) return false;

            return true;
        }

        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return CustomColumnLayoutId.GetHashCode() ^
                Path.GetHashCode();
        }
    }
}