﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// ProductGroup Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class ProductGroupDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public ProductGroupDtoKey DtoKey
        {
            get
            {
                return new ProductGroupDtoKey()
                {
                    ProductLevelId = this.ProductLevelId,
                    Code = this.Code
                };
            }
        }

        [InheritedProperty(typeof(ProductLevelDto), typeof(IProductLevelDal))]
        [ForeignKey(typeof(ProductHierarchyDto), typeof(IProductHierarchyDal), DeleteBehavior.Leave)]
        public Int32 ProductHierarchyId { get; set; }

        [ForeignKey(typeof(ProductLevelDto), typeof(IProductLevelDal), DeleteBehavior.MoveUpToParent)]
        public Int32 ProductLevelId { get; set; }

        public String Name { get; set; }
        public String Code { get; set; }
        public Int32? ParentGroupId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ProductGroupDto other = obj as ProductGroupDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.ProductHierarchyId != this.ProductHierarchyId) { return false; }
                if (other.ProductLevelId != this.ProductLevelId) { return false; }
                if (other.ParentGroupId != this.ParentGroupId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.Code != this.Code) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.DateDeleted != this.DateDeleted) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class ProductGroupDtoKey
    {
        #region Properties
        public Int32 ProductLevelId { get; set; }
        public String Code { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                 ProductLevelId.GetHashCode() +
                 Code.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ProductGroupDtoKey other = obj as ProductGroupDtoKey;
            if (other != null)
            {
                if (other.ProductLevelId != this.ProductLevelId) { return false; }
                if (other.Code != this.Code) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class ProductGroupIsSetDto
    {
        #region Properties

        public Boolean IsProductHierarchyIdSet { get; set; }
        public Boolean IsProductLevelIdSet { get; set; }
        public Boolean IsNameSet { get; set; }
        public Boolean IsCodeSet { get; set; }
        public Boolean IsParentGroupIdSet { get; set; }
        #endregion

        #region Constructors

        /// <summary>
        /// Default
        /// </summary>
        public ProductGroupIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public ProductGroupIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
             IsProductHierarchyIdSet.GetHashCode() +
             IsProductLevelIdSet.GetHashCode() +
             IsNameSet.GetHashCode() +
             IsCodeSet.GetHashCode() +
             IsParentGroupIdSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ProductGroupIsSetDto other = obj as ProductGroupIsSetDto;
            if (other != null)
            {

                if (other.IsProductHierarchyIdSet != this.IsProductHierarchyIdSet) { return false; }
                if (other.IsProductLevelIdSet != this.IsProductLevelIdSet) { return false; }
                if (other.IsNameSet != this.IsNameSet) { return false; }
                if (other.IsCodeSet != this.IsCodeSet) { return false; }
                if (other.IsParentGroupIdSet != this.IsParentGroupIdSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {

            IsProductHierarchyIdSet = isSet;
            IsProductLevelIdSet = isSet;
            IsNameSet = isSet;
            IsCodeSet = isSet;
            IsParentGroupIdSet = isSet;
        }
        #endregion
    }
}
