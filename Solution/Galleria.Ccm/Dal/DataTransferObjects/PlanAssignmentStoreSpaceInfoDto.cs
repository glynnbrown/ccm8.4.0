#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8)
// V8-25879 : Martin Shelley
//		Created (Auto-generated)
// V8-28448 : A.Silva
//      Added ProductGroupCode comparison to Equals method.

#endregion

#region Version History: (CCM 810)
// V8-29598 : Lewis Luong
//		Added BayHeight

#endregion

#region Version History: CCM840

// CCM-13408 : J.Mendes
//  Added LocationSpaceProductGroup fields to the Planogram Assignment

#endregion
#endregion

using System;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// PlanAssignmentStoreSpaceInfo Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class PlanAssignmentStoreSpaceInfoDto
    {
        #region Properties

        public Int32 Id { get; set; }
        //public RowVersion RowVersion { get; set; }
        public PlanAssignmentStoreSpaceInfoDtoKey DtoKey
        {
            get
            {
                return new PlanAssignmentStoreSpaceInfoDtoKey()
                {
                    Id = this.Id,
                };
            }
        }
        public Int16 LocationId { get; set; }
        public String LocationCode { get; set; }
        public String LocationName { get; set; }
        public String LocationAddress { get; set; }
        public Int32 ProductGroupId { get; set; }
        public String ProductGroupCode { get; set; }
        public String ProductGroupName { get; set; }
        public String BayDimensions { get; set; }
        public Double BayTotalWidth { get; set; }
        public Double BayHeight { get; set; }
        public Int32 BayCount { get; set; }
        public String ClusterSchemeName { get; set; }
        public String ClusterName { get; set; }
        public Int32 ProductCount { get; set; }
        public Double AverageBayWidth { get; set; }
        public String AisleName { get; set; }
        public String ValleyName { get; set; }
        public String ZoneName { get; set; }
        public String CustomAttribute01 { get; set; }
        public String CustomAttribute02 { get; set; }
        public String CustomAttribute03 { get; set; }
        public String CustomAttribute04 { get; set; }
        public String CustomAttribute05 { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanAssignmentStoreSpaceInfoDto other = obj as PlanAssignmentStoreSpaceInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.LocationId != this.LocationId) { return false; }
                if (other.LocationCode != this.LocationCode) { return false; }
                if (other.LocationName != this.LocationName) { return false; }
                if (other.LocationAddress != this.LocationAddress) { return false; }
                if (other.ProductGroupId != this.ProductGroupId) { return false; }
                if (other.ProductGroupCode != this.ProductGroupCode) { return false; }
                if (other.ProductGroupName != this.ProductGroupName) { return false; }
                if (other.BayDimensions != this.BayDimensions) { return false; }
                if (other.BayTotalWidth != this.BayTotalWidth) { return false; }
                if (other.BayHeight != this.BayHeight) { return false; }
                if (other.BayCount != this.BayCount) { return false; }
                if (other.ClusterSchemeName != this.ClusterSchemeName) { return false; }
                if (other.ClusterName != this.ClusterName) { return false; }
                if (other.ProductCount != this.ProductCount) { return false; }
                if (other.AverageBayWidth != this.AverageBayWidth) { return false; }
                if (other.AisleName != this.AisleName) { return false; }
                if (other.ValleyName != this.ValleyName) { return false; }
                if (other.ZoneName != this.ZoneName) { return false; }
                if (other.CustomAttribute01 != this.CustomAttribute01) { return false; }
                if (other.CustomAttribute02 != this.CustomAttribute02) { return false; }
                if (other.CustomAttribute03 != this.CustomAttribute03) { return false; }
                if (other.CustomAttribute04 != this.CustomAttribute04) { return false; }
                if (other.CustomAttribute05 != this.CustomAttribute05) { return false; }
            }
            else
            {
                return false;
            }

            return true;
        }

        #endregion
    }

    public class PlanAssignmentStoreSpaceInfoDtoKey
    {
        #region Properties

        public Int32 Id { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanAssignmentStoreSpaceInfoDtoKey other = obj as PlanAssignmentStoreSpaceInfoDtoKey;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        #endregion
    }
}
