﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8)
// CCM-25586 : A.Probyn
//		Copied from SA
#endregion
#endregion


using System;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// ProductAttribute Data Transfer Object
    /// </summary>
    [Serializable]
    public class ProductAttributeDto
    {
        #region Properties
        public Int32 Id { get; set; }
        //public RowVersion RowVersion { get; set; }
        public ProductAttributeDtoKey DtoKey
        {
            get
            {
                return new ProductAttributeDtoKey()
                {
                    Name = this.Name
                };
            }
        }
        public String Name { get; set; }
        public Byte DataType { get; set; }
        public Byte RegularExpressionType { get; set; }
        public Byte ColumnNumber { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ProductAttributeDto other = obj as ProductAttributeDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                //if (other.RowVersion != this.RowVersion) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.DataType != this.DataType) { return false; }
                if (other.RegularExpressionType != this.RegularExpressionType) { return false; }
                if (other.ColumnNumber != this.ColumnNumber) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.DateDeleted != this.DateDeleted) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    public class ProductAttributeDtoKey
    {

        #region Properties

        public String Name { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                Name.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ProductAttributeDtoKey other = obj as ProductAttributeDtoKey;
            if (other != null)
            {
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
