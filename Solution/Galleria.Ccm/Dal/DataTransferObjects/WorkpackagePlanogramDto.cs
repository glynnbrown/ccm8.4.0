﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  Added UserName property
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public sealed class WorkpackagePlanogramDto
    {
        #region Properties
        public Int32 Id { get; set; }
        [ForeignKey(typeof(WorkpackageDto), typeof(IWorkpackageDal), DeleteBehavior.Leave)]
        public Int32 WorkpackageId { get; set; }
        [ForeignKey(typeof(PlanogramDto), typeof(IPlanogramDal), DeleteBehavior.Leave)]
        public Int32? SourcePlanogramId { get; set; }
        [ForeignKey(typeof(PlanogramDto), typeof(IPlanogramDal), DeleteBehavior.Leave)]
        public Int32 DestinationPlanogramId { get; set; }
        public String Name { get; set; }
        public Byte ProcessingStatus { get; set; }
        public String UserName { get; set; }
        public WorkpackagePlanogramDtoKey DtoKey
        {
            get
            {
                return new WorkpackagePlanogramDtoKey()
                {
                    Id = this.Id
                };
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the instance has code
        /// </summary>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Compares two instances to each other
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            WorkpackagePlanogramDto other = obj as WorkpackagePlanogramDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.WorkpackageId != this.WorkpackageId) return false;
                if (other.SourcePlanogramId != this.SourcePlanogramId) return false;
                if (other.DestinationPlanogramId != this.DestinationPlanogramId) return false;
                if (other.Name != this.Name) return false;
                if (other.UserName != this.UserName) return false;
                if (other.ProcessingStatus != this.ProcessingStatus) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class WorkpackagePlanogramDtoKey
    {
        #region Properties
        public Int32 Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the hash code for this instance
        /// </summary>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Compares two instances to see if they are equal
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            WorkpackagePlanogramDtoKey other = obj as WorkpackagePlanogramDtoKey;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
