﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    ///     Planogram Comparison Template Info Data Transfer Object.
    /// </summary>
    [Serializable]
    public sealed class PlanogramComparisonTemplateInfoDto
    {
        #region Properties

        public Object Id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>. </param>
        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramComparisonTemplateInfoDto;
            return other != null &&
                   Equals(other.Id, Id) &&
                   String.Equals(other.Name, Name) &&
                   String.Equals(other.Description, Description);
        }

        /// <summary>
        /// Serves as a hash function for a particular type. 
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override Int32 GetHashCode()
        {
            return HashCode.Start.Hash(Id).GetHashCode();
        }

        #endregion
    }
}