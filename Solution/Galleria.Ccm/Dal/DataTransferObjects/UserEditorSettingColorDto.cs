﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// V8-30791 : D.Pleasance
//		Created
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// SystemSetting Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class UserEditorSettingColorDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public UserEditorSettingColorDtoKey DtoKey
        {
            get
            {
                return new UserEditorSettingColorDtoKey()
                {
                    Color = this.Color,
                };
            }
        }
        public Int32 Color { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            UserEditorSettingColorDto other = obj as UserEditorSettingColorDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.Color != this.Color) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion

        public String ToString()
        {
            return this.Color.ToString();
        }
    }

    [Serializable]
    public sealed class UserEditorSettingColorDtoKey
    {
        #region Properties
        public Int32 Color { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Color.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            UserEditorSettingColorDtoKey other = obj as UserEditorSettingColorDtoKey;
            if (other != null)
            {
                if (other.Color != this.Color) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
