﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// ViewLayoutItem data transfer object
    /// </summary>
    [Serializable]
    public sealed class ViewLayoutItemDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public Int32 ViewLayoutId { get; set; }
        public Int32? ParentViewLayoutItemId { get; set; }
        public Byte Order { get; set; }
        public Boolean IsVertical { get; set; }
        public Boolean IsNewPane { get; set; }
        public Byte DocumentType { get; set; }
        #endregion

        #region Methods

        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ViewLayoutItemDto other = obj as ViewLayoutItemDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.ViewLayoutId != this.ViewLayoutId) { return false; }
                if (other.ParentViewLayoutItemId != this.ParentViewLayoutItemId) { return false; }
                if (other.Order != this.Order) { return false; }
                if (other.IsVertical != this.IsVertical) { return false; }
                if (other.IsNewPane != this.IsNewPane) { return false; }
                if (other.DocumentType != this.DocumentType) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        #endregion
    }
}
