﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
// V8-26140 :I.George
//  Added AssortmentMinorRevisionInfoDtoKey
// V8-27059 : J.Pickup
//  Tidied code & added IsSetDto.
// V8-27128 : L.Luong
//  Removed Dto key and methods not needed in info dto
#endregion
#region Version History: (CCM CCM803)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class AssortmentMinorRevisionInfoDto
    {
        #region Properties

        public Int32 Id { get; set; }
        public Int32? ConsumerDecisionTreeId { get; set; }
        public Guid UniqueContentReference { get; set; }
        public String Name { get; set; }
        public Int32 EntityId { get; set; }
        public Int32 ProductGroupId { get; set; }
        public String ProductGroupName { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public Guid? ParentUniqueContentReference { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the objects hash code
        /// </summary>
        /// <returns>The objects hash code</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Compares two instance of this type
        /// </summary>
        /// <param name="obj">The object to compare to</param>
        /// <returns>True if equal, else false</returns>
        public override bool Equals(object obj)
        {
            AssortmentMinorRevisionInfoDto other = obj as AssortmentMinorRevisionInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                //if (other.RowVersion != this.RowVersion) { return false; }
                if (other.ConsumerDecisionTreeId != this.ConsumerDecisionTreeId) { return false; }
                if (other.UniqueContentReference != this.UniqueContentReference) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.ProductGroupId != this.ProductGroupId) { return false; }
                if (other.ProductGroupName != this.ProductGroupName) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.ParentUniqueContentReference != this.ParentUniqueContentReference) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
