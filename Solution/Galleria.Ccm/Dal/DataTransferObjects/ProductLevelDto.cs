﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// ProductLevel Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class ProductLevelDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public ProductLevelDtoKey DtoKey
        {
            get
            {
                return new ProductLevelDtoKey()
                {
                    ProductHierarchyId = this.ProductHierarchyId,
                    Name = this.Name
                };
            }
        }

        [ForeignKey(typeof(ProductHierarchyDto), typeof(IProductHierarchyDal), DeleteBehavior.Leave)]
        public Int32 ProductHierarchyId { get; set; }

        public String Name { get; set; }
        public Byte ShapeNo { get; set; }
        public Int32 Colour { get; set; }
        [ForeignKey(typeof(ProductLevelDto), typeof(IProductLevelDal))]
        public Int32? ParentLevelId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ProductLevelDto other = obj as ProductLevelDto;
            if (other != null)
            {
                // Id
                if (other.Id != this.Id) { return false; }
                if (other.ProductHierarchyId != this.ProductHierarchyId) { return false; }
                if (other.ParentLevelId != this.ParentLevelId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.ShapeNo != this.ShapeNo) { return false; }
                if (other.Colour != this.Colour) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.DateDeleted != this.DateDeleted) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class ProductLevelDtoKey
    {
        #region Properties
        public Object ProductHierarchyId { get; set; }
        public String Name { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                ProductHierarchyId.GetHashCode() +
                Name.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ProductLevelDtoKey other = obj as ProductLevelDtoKey;
            if (other != null)
            {
                // Id
                if ((other.ProductHierarchyId != null) && (this.ProductHierarchyId != null))
                {
                    if (!other.ProductHierarchyId.Equals(this.ProductHierarchyId)) { return false; }
                }
                if ((other.ProductHierarchyId != null) && (this.ProductHierarchyId == null)) { return false; }
                if ((other.ProductHierarchyId == null) && (this.ProductHierarchyId != null)) { return false; }

                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
