﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public sealed class PlanogramGroupPlanogramDto
    {
        #region Properties
        //[ForeignKey(typeof(PlanogramGroupDto), typeof(IPlanogramGroupDal))]
        public Int32 PlanogramGroupId { get; set; }
        // [ForeignKey(typeof(PlanogramDto), typeof(IPlanogramDal))] - NF - Commented out due to bug in test framework
        public Int32 PlanogramId { get; set; }
        public PlanogramGroupPlanogramDtoKey DtoKey
        {
            get
            {
                return new PlanogramGroupPlanogramDtoKey()
                {
                    PlanogramGroupId = this.PlanogramGroupId,
                    PlanogramId = this.PlanogramId
                };
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the instance has code
        /// </summary>
        public override Int32 GetHashCode()
        {
            return
                this.PlanogramGroupId.GetHashCode() +
                this.PlanogramId.GetHashCode();
        }

        /// <summary>
        /// Compares two instances to each other
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            PlanogramGroupPlanogramDto other = obj as PlanogramGroupPlanogramDto;
            if (other != null)
            {
                if (other.PlanogramGroupId != this.PlanogramGroupId) return false;
                if (other.PlanogramId != this.PlanogramId) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class PlanogramGroupPlanogramDtoKey
    {
        #region Properties
        public Int32 PlanogramGroupId { get; set; }
        public Int32 PlanogramId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the hash code for this instance
        /// </summary>
        public override Int32 GetHashCode()
        {
            return
                this.PlanogramGroupId.GetHashCode() +
                this.PlanogramId.GetHashCode();
        }

        /// <summary>
        /// Compares two instances to see if they are equal
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            PlanogramGroupPlanogramDtoKey other = obj as PlanogramGroupPlanogramDtoKey;
            if (other != null)
            {
                if (other.PlanogramGroupId != this.PlanogramGroupId) return false;
                if (other.PlanogramId != this.PlanogramId) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
