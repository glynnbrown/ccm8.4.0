#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700 : A.Silva ~ Created.
// V8-26076 : A.Silva ~ Removed Id Property and DtoKey.
// V8-26201 : A.Silva ~ Readded DtoKey and corrected Equals.

#endregion

#region Version History: (CCM 8.3)
// V8-31542 : L.Ineson
//  Added Id and updated implementation.
#endregion

#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class CustomColumnGroupDto
    {
        #region Properties

        public Int32 Id { get; set; }

        [ForeignKey(typeof (CustomColumnLayoutDto), typeof (ICustomColumnLayoutDal))]
        public Object CustomColumnLayoutId { get; set; }

        public CustomColumnGroupDtoKey DtoKey
        {
            get { return new CustomColumnGroupDtoKey 
            {
                CustomColumnLayoutId = this.CustomColumnLayoutId,
                Grouping = Grouping
            }; }
        }

        public String Grouping { get; set; }

        #endregion

        #region Equals override

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            CustomColumnGroupDto other = obj as CustomColumnGroupDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;

                // CustomColumnLayoutId
                if ((other.CustomColumnLayoutId != null) && (this.CustomColumnLayoutId != null))
                {
                    if (!other.CustomColumnLayoutId.Equals(this.CustomColumnLayoutId)) { return false; }
                }
                if ((other.CustomColumnLayoutId != null) && (this.CustomColumnLayoutId == null)) { return false; }
                if ((other.CustomColumnLayoutId == null) && (this.CustomColumnLayoutId != null)) { return false; }

                if (other.Grouping != this.Grouping) return false;
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        #endregion
    }


    /// <summary>
    /// CustomColumnGroup dto key
    /// </summary>
    [Serializable]
    public sealed class CustomColumnGroupDtoKey
    {
        #region Properties
        public Object CustomColumnLayoutId { get; set; }
        public String Grouping { get; set; }
        #endregion

        #region Equals override
        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            CustomColumnGroupDtoKey other = obj as CustomColumnGroupDtoKey;
            if (other != null)
            {
                // CustomColumnLayoutId
                if ((other.CustomColumnLayoutId != null) && (this.CustomColumnLayoutId != null))
                {
                    if (!other.CustomColumnLayoutId.Equals(this.CustomColumnLayoutId)) { return false; }
                }
                if ((other.CustomColumnLayoutId != null) && (this.CustomColumnLayoutId == null)) { return false; }
                if ((other.CustomColumnLayoutId == null) && (this.CustomColumnLayoutId != null)) { return false; }

                if (other.Grouping != this.Grouping) return false;
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return 
                this.CustomColumnLayoutId.GetHashCode() ^
                this.Grouping.GetHashCode();
        }
        #endregion
    }
}