﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 830)
// V8-31546 : M.Pettit
//  Created
#endregion

#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// PlanogramExportTemplate Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramExportTemplateDto
    {
        #region Properties
        public Object Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public PlanogramExportTemplateDtoKey DtoKey
        {
            get
            {
                return new PlanogramExportTemplateDtoKey()
                {
                    Name = this.Name,
                    EntityId = this.EntityId
                };
            }
        }
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        public String Name { get; set; }
        public Byte FileType { get; set; }
        public String FileVersion { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramExportTemplateDto other = obj as PlanogramExportTemplateDto;
            if (other != null)
            {
                //Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }

                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.FileType != this.FileType) { return false; }
                if (other.FileVersion != this.FileVersion) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class PlanogramExportTemplateDtoKey
    {
        #region Properties

        public String Name { get; set; }
        public Int32 EntityId { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Name.GetHashCode() +
                EntityId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramExportTemplateDtoKey other = obj as PlanogramExportTemplateDtoKey;
            if (other != null)
            {
                if (other.Name != this.Name) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}