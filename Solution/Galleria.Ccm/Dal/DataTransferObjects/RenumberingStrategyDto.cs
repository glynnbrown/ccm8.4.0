﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    ///     Data transfer object for <c>RenumberingStrategy</c>.
    /// </summary>
    [Serializable]
    public sealed class RenumberingStrategyDto
    {
        #region Properties

        public DateTime DateCreated { get; set; }
        public DateTime? DateDeleted { get; set; }
        public DateTime DateLastModified { get; set; }

        public RenumberingStrategyDtoKey DtoKey
        {
            get
            {
                return new RenumberingStrategyDtoKey
                {
                    EntityId = EntityId,
                    Name = Name
                };
            }
        }

        [ForeignKey(typeof (EntityDto), typeof (IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public RowVersion RowVersion { get; set; }
        public Byte BayComponentXRenumberStrategyPriority { get; set; }
        public Byte BayComponentYRenumberStrategyPriority { get; set; }
        public Byte BayComponentZRenumberStrategyPriority { get; set; }
        public Byte PositionXRenumberStrategyPriority { get; set; }
        public Byte PositionYRenumberStrategyPriority { get; set; }
        public Byte PositionZRenumberStrategyPriority { get; set; }
        public Byte BayComponentXRenumberStrategy { get; set; }
        public Byte BayComponentYRenumberStrategy { get; set; }
        public Byte BayComponentZRenumberStrategy { get; set; }
        public Byte PositionXRenumberStrategy { get; set; }
        public Byte PositionYRenumberStrategy { get; set; }
        public Byte PositionZRenumberStrategy { get; set; }
        public Boolean RestartComponentRenumberingPerBay { get; set; }
        public Boolean IgnoreNonMerchandisingComponents { get; set; }
        public Boolean RestartPositionRenumberingPerComponent { get; set; }
        public Boolean UniqueNumberMultiPositionProductsPerComponent { get; set; }
        public Boolean ExceptAdjacentPositions { get; set; }
        public Boolean RestartPositionRenumberingPerBay { get; set; }
        public Boolean RestartComponentRenumberingPerComponentType { get; set; }

        #endregion

        #region Equality Overrides

        /// <summary>
        ///     Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            var other = obj as RenumberingStrategyDto;
            return other != null &&
                   other.RowVersion == RowVersion &&
                   other.Id == Id &&
                   other.EntityId == EntityId &&
                   other.Name == Name &&
                   other.DateCreated == DateCreated &&
                   other.DateLastModified == DateLastModified &&
                   other.DateDeleted == DateDeleted &&
                   other.BayComponentXRenumberStrategyPriority == BayComponentXRenumberStrategyPriority &&
                   other.BayComponentYRenumberStrategyPriority == BayComponentYRenumberStrategyPriority &&
                   other.BayComponentZRenumberStrategyPriority == BayComponentZRenumberStrategyPriority &&
                   other.PositionXRenumberStrategyPriority == PositionXRenumberStrategyPriority &&
                   other.PositionYRenumberStrategyPriority == PositionYRenumberStrategyPriority &&
                   other.PositionZRenumberStrategyPriority == PositionZRenumberStrategyPriority &&
                   other.BayComponentXRenumberStrategy == BayComponentXRenumberStrategy &&
                   other.BayComponentYRenumberStrategy == BayComponentYRenumberStrategy &&
                   other.BayComponentZRenumberStrategy == BayComponentZRenumberStrategy &&
                   other.PositionXRenumberStrategy == PositionXRenumberStrategy &&
                   other.PositionYRenumberStrategy == PositionYRenumberStrategy &&
                   other.PositionZRenumberStrategy == PositionZRenumberStrategy &&
                   other.RestartComponentRenumberingPerBay == RestartComponentRenumberingPerBay &&
                   other.IgnoreNonMerchandisingComponents == IgnoreNonMerchandisingComponents &&
                   other.RestartPositionRenumberingPerComponent == RestartPositionRenumberingPerComponent &&
                   other.UniqueNumberMultiPositionProductsPerComponent == UniqueNumberMultiPositionProductsPerComponent &&
                   other.ExceptAdjacentPositions == ExceptAdjacentPositions &&
                   other.RestartPositionRenumberingPerBay == RestartPositionRenumberingPerBay &&
                   other.RestartComponentRenumberingPerComponentType == RestartComponentRenumberingPerComponentType;
        }

        /// <summary>
        ///     Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode() +
                   RowVersion.GetHashCode() +
                   EntityId.GetHashCode() +
                   Name.GetHashCode() +
                   DateCreated.GetHashCode() +
                   DateLastModified.GetHashCode() +
                   DateDeleted.GetHashCode() + 
                   BayComponentXRenumberStrategyPriority.GetHashCode() +
                   BayComponentYRenumberStrategyPriority.GetHashCode() +
                   BayComponentZRenumberStrategyPriority.GetHashCode() +
                   PositionXRenumberStrategyPriority.GetHashCode() +
                   PositionYRenumberStrategyPriority.GetHashCode() +
                   PositionZRenumberStrategyPriority.GetHashCode() + 
                   BayComponentXRenumberStrategy.GetHashCode() +
                   BayComponentYRenumberStrategy.GetHashCode() +
                   BayComponentZRenumberStrategy.GetHashCode() +
                   PositionXRenumberStrategy.GetHashCode() +
                   PositionYRenumberStrategy.GetHashCode() +
                   PositionZRenumberStrategy.GetHashCode() +
                   RestartComponentRenumberingPerBay.GetHashCode() + 
                   IgnoreNonMerchandisingComponents.GetHashCode() +
                   RestartPositionRenumberingPerComponent.GetHashCode() +
                   UniqueNumberMultiPositionProductsPerComponent.GetHashCode() +
                   ExceptAdjacentPositions.GetHashCode() +
                   RestartPositionRenumberingPerBay.GetHashCode() +
                   RestartComponentRenumberingPerComponentType.GetHashCode();
        }

        #endregion
    }

    [Serializable]
    public sealed class RenumberingStrategyDtoKey
    {
        #region Properties

        [ForeignKey(typeof (EntityDto), typeof (IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }

        public String Name { get; set; }

        #endregion

        #region Equality Overrides

        /// <summary>
        ///     Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            var other = obj as RenumberingStrategyDtoKey;
            return other != null &&
                   other.EntityId == EntityId &&
                   other.Name == Name;
        }

        /// <summary>
        ///     Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return EntityId.GetHashCode() +
                   Name.GetHashCode();
        }

        #endregion
    }
}