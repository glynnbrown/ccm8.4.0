﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Fixture Data Transfer Object
    /// </summary>
    [Serializable]
    public class FixtureDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public FixtureDtoKey DtoKey
        {
            get
            {
                return new FixtureDtoKey()
                {
                    Id = this.Id,
                };
            }
        }
        public Object FixturePackageId { get; set; }
        public String Name { get; set; }
        public Single Height { get; set; }
        public Single Width { get; set; }
        public Single Depth { get; set; }
        public Int16 NumberOfAssemblies { get; set; }
        public Int16 NumberOfMerchandisedSubComponents { get; set; }
        public Single TotalFixtureCost { get; set; }
        public Object ExtendedData { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FixtureDto other = obj as FixtureDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }

                // FixturePackageId
                if ((other.FixturePackageId != null) && (this.FixturePackageId != null))
                {
                    if (!other.FixturePackageId.Equals(this.FixturePackageId)) { return false; }
                }
                if ((other.FixturePackageId != null) && (this.FixturePackageId == null)) { return false; }
                if ((other.FixturePackageId == null) && (this.FixturePackageId != null)) { return false; }

                if (other.Name != this.Name) { return false; }
                if (other.Height != this.Height) { return false; }
                if (other.Width != this.Width) { return false; }
                if (other.Depth != this.Depth) { return false; }
                if (other.NumberOfAssemblies != this.NumberOfAssemblies) { return false; }
                if (other.NumberOfMerchandisedSubComponents != this.NumberOfMerchandisedSubComponents) { return false; }
                if (other.TotalFixtureCost != this.TotalFixtureCost) { return false; }


                //ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData != null))
                {
                    if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
                }
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class FixtureDtoKey
    {
        #region Properties
        public Int32 Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FixtureDtoKey other = obj as FixtureDtoKey;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
