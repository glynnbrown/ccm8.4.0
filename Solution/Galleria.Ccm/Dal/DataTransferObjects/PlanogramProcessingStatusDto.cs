﻿#region Header Information

// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800

// V8-26895 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  Altered names of existing properties
//  Added AutomationDateStarted, AutomationDateLastUpdated
//  Added MetaData, Validation properties
// V8-26322 : A.Silva
//      Added DTO Key.

#endregion

#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class PlanogramProcessingStatusDto
    {
        #region Properties

        public Int32 PlanogramId { get; set; }
        public RowVersion RowVersion { get; set; }
        public Byte MetaDataStatus { get; set; }
        public String MetaDataStatusDescription { get; set; }
        public Int32 MetaDataProgressMax { get; set; }
        public Int32 MetaDataProgressCurrent { get; set; }
        public DateTime? MetaDataDateStarted { get; set; }
        public DateTime? MetaDataDateLastUpdated { get; set; }
        public Byte ValidationStatus { get; set; }
        public String ValidationStatusDescription { get; set; }
        public Int32 ValidationProgressMax { get; set; }
        public Int32 ValidationProgressCurrent { get; set; }
        public DateTime? ValidationDateStarted { get; set; }
        public DateTime? ValidationDateLastUpdated { get; set; }

        public PlanogramProcessingStatusDtoKey DtoKey
        {
            get { return new PlanogramProcessingStatusDtoKey {PlanogramId = PlanogramId}; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a hash code for this instance
        /// </summary>
        public override Int32 GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Compares that two instances are equal
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            PlanogramProcessingStatusDto other = obj as PlanogramProcessingStatusDto;
            if (other != null)
            {
                if (other.PlanogramId != this.PlanogramId) return false;
                if (other.RowVersion != this.RowVersion) return false;
                if (other.MetaDataStatus != this.MetaDataStatus) return false;
                if (other.MetaDataStatusDescription != this.MetaDataStatusDescription) return false;
                if (other.MetaDataProgressMax != this.MetaDataProgressMax) return false;
                if (other.MetaDataProgressCurrent != this.MetaDataProgressCurrent) return false;
                if (other.MetaDataDateStarted != this.MetaDataDateStarted) return false;
                if (other.MetaDataDateLastUpdated != this.MetaDataDateLastUpdated) return false;
                if (other.ValidationStatus != this.ValidationStatus) return false;
                if (other.ValidationStatusDescription != this.ValidationStatusDescription) return false;
                if (other.ValidationProgressMax != this.ValidationProgressMax) return false;
                if (other.ValidationProgressCurrent != this.ValidationProgressCurrent) return false;
                if (other.ValidationDateStarted != this.ValidationDateStarted) return false;
                if (other.ValidationDateLastUpdated != this.ValidationDateLastUpdated) return false;
            }
            else
                return false;
            return true;
        }

        #endregion
    }

    [Serializable]
    public class PlanogramProcessingStatusDtoKey
    {
        #region Properties

        public Int32 PlanogramId { get; set; }

        #endregion

        /// <summary>
        /// Serves as a hash function for a particular type. 
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override Int32 GetHashCode()
        {
            return PlanogramId.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>. </param>
        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramProcessingStatusDtoKey;
            return other != null &&
                   other.PlanogramId == PlanogramId;
        }
    }
}