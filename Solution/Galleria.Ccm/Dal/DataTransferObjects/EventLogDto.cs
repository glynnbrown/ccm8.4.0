﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : L.Luong
//   Created (Copied From GFS)
// V8-27404 : L.Luong
//   Changed LevelId to EntryType
#endregion

#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//	Extended for new WorkpackageName, WorkpackageId property
#endregion

#endregion

using System;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Event Log Data Transfer Object
    /// </summary>
    [Serializable]
    public class EventLogDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public Int32? EntityId { get; set; }
        public String EntityName { get; set; }
        public Int32 EventLogNameId { get; set; }
        public String EventLogName { get; set; }
        public String Source { get; set; }
        public Int32 EventId { get; set; }
        public Int16 EntryType { get; set; }
        public Int32? UserId { get; set; }
        public String UserName { get; set; }
        public DateTime DateTime { get; set; }
        public String Process { get; set; }
        public String ComputerName { get; set; }
        public String URLHelperLink { get; set; }
        public String Description { get; set; }
        public String Content { get; set; }
        public String GibraltarSessionId { get; set; }
        public String WorkpackageName { get; set; }
        public Int32? WorkpackageId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            EventLogDto other = obj as EventLogDto;
            if (other != null)
            {
                if (other.Id != this.Id)
                {
                    return false;
                }
                if (other.EntityId != this.EntityId)
                {
                    return false;
                }
                if (other.EntityName != this.EntityName)
                {
                    return false;
                }
                if (other.EventLogNameId != this.EventLogNameId)
                {
                    return false;
                }
                if (other.EventLogName != this.EventLogName)
                {
                    return false;
                }
                if (other.Source != this.Source)
                {
                    return false;
                }
                if (other.EventId != this.EventId)
                {
                    return false;
                }
                if (other.EntryType != this.EntryType)
                {
                    return false;
                }
                if (other.UserId != this.UserId)
                {
                    return false;
                }
                if (other.UserName != this.UserName)
                {
                    return false;
                }
                if (other.DateTime != this.DateTime)
                {
                    return false;
                }
                if (other.Process != this.Process)
                {
                    return false;
                }
                if (other.ComputerName != this.ComputerName)
                {
                    return false;
                }
                if (other.URLHelperLink != this.URLHelperLink)
                {
                    return false;
                }
                if (other.Description != this.Description)
                {
                    return false;
                }
                if (other.Content != this.Content)
                {
                    return false;
                }
                if (other.GibraltarSessionId != this.GibraltarSessionId)
                {
                    return false;
                }
                if (other.WorkpackageName != this.WorkpackageName)
                {
                    return false;
                }
                if (other.WorkpackageId != this.WorkpackageId)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}