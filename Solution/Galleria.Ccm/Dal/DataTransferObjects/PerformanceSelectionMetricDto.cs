﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)

// CCM800-26953 : I.George
//   Initial Version
// V8-26322 : A.Silva
//  Added DtoKey property for unit testing.

#endregion

#region Version History: (CCM 8.0.1)
// V8-28444 : M.Shelley
//  Changed the DtoKey id so that it is unique - previously it was using the PerformanceSelectionId 
//  from the parent object as the id
#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// PerformanceSelectionMetric Data Transfer Objects
    /// </summary>
      #region Properties
    [Serializable]
    public sealed class PerformanceSelectionMetricDto
    {
        public PerformanceSelectionMetricDtoKey DtoKey
        {
            get { return new PerformanceSelectionMetricDtoKey {Id = this.Id}; }
        }
        [ForeignKey(typeof(PerformanceSelectionDto), typeof(IPerformanceSelectionDal), DeleteBehavior.Leave)]
        public Int32 PerformanceSelectionId { get; set; }
        public Int32 Id { get; set; }
        public Int32 GFSPerformanceSourceMetricId { get; set; }
        public Byte AggregationType { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PerformanceSelectionMetricDto other = obj as PerformanceSelectionMetricDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.PerformanceSelectionId != this.PerformanceSelectionId) { return false; }
                if (other.GFSPerformanceSourceMetricId != this.GFSPerformanceSourceMetricId) { return false; }
                if (other.AggregationType != this.AggregationType) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}

public class PerformanceSelectionMetricDtoKey
{
    #region Properties

    public Int32 Id { get; set; }

    #endregion

    #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PerformanceSelectionMetricDtoKey other = obj as PerformanceSelectionMetricDtoKey;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
 
}
