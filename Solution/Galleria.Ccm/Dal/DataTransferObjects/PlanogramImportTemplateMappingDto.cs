﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-27804 : L.Ineson
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// PlanogramImportTemplateMapping Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramImportTemplateMappingDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public PlanogramImportTemplateMappingDtoKey DtoKey
        {
            get
            {
                return new PlanogramImportTemplateMappingDtoKey()
                {
                    PlanogramImportTemplateId = this.PlanogramImportTemplateId,
                    Field = this.Field,
                    FieldType = this.FieldType
                }; 
            }
        }
        [ForeignKey(typeof(PlanogramImportTemplateDto), typeof(IPlanogramImportTemplateDal))]
        public Object PlanogramImportTemplateId { get; set; }
        public Byte FieldType { get; set; }
        public String Field { get; set; }
        public String ExternalField { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramImportTemplateMappingDto other = obj as PlanogramImportTemplateMappingDto;
            if (other != null)
            {
                //Id
                if (other.Id != this.Id) { return false; }

                //PlanogramImportTemplateId
                if ((other.PlanogramImportTemplateId != null) && (this.PlanogramImportTemplateId != null))
                {
                    if (!other.PlanogramImportTemplateId.Equals(this.PlanogramImportTemplateId)) { return false; }
                }
                if ((other.PlanogramImportTemplateId != null) && (this.PlanogramImportTemplateId == null)) { return false; }
                if ((other.PlanogramImportTemplateId == null) && (this.PlanogramImportTemplateId != null)) { return false; }

                if (other.FieldType != this.FieldType) { return false; }
                if (other.Field != this.Field) { return false; }
                if (other.ExternalField != this.ExternalField) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion

    }

    [Serializable]
    public class PlanogramImportTemplateMappingDtoKey
    {
        #region Properties

        public Object PlanogramImportTemplateId { get; set; }
        public String Field { get; set; }
        public Byte FieldType { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return PlanogramImportTemplateId.GetHashCode() +
                Field.GetHashCode() + FieldType.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramImportTemplateMappingDtoKey other = obj as PlanogramImportTemplateMappingDtoKey;
            if (other != null)
            {
                //PlanogramImportTemplateId
                if ((other.PlanogramImportTemplateId != null) && (this.PlanogramImportTemplateId != null))
                {
                    if (!other.PlanogramImportTemplateId.Equals(this.PlanogramImportTemplateId)) { return false; }
                }
                if ((other.PlanogramImportTemplateId != null) && (this.PlanogramImportTemplateId == null)) { return false; }
                if ((other.PlanogramImportTemplateId == null) && (this.PlanogramImportTemplateId != null)) { return false; }

                if (other.Field != this.Field) { return false; }
                if (other.FieldType != this.FieldType) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
