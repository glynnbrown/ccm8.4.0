﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
// V8-25556 : J.Pickup
//		Introduced GFSId (To be used for the sync).
#endregion
#region Version History: (CCM 8.0.3)
// V8-29215 : D.Pleasance
//  Removed ConsumerDecisionTree DateDeleted columns
#endregion
#endregion

using System;

using Galleria.Framework.DataStructures;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// ConsumerDecisionTreeNode Data Transfer Object
    /// </summary>
    [Serializable]
    public class ConsumerDecisionTreeNodeDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public Int32? GFSId { get; set; }
        public ConsumerDecisionTreeNodeDtoKey DtoKey
        {
            get
            {
                return new ConsumerDecisionTreeNodeDtoKey()
                {
                    ConsumerDecisionTreeId = this.ConsumerDecisionTreeId,
                    ConsumerDecisionTreeLevelId = this.ConsumerDecisionTreeLevelId,
                    Name = this.Name,
                    ParentNodeId = this.ParentNodeId
                };
            }
        }
        [InheritedProperty(typeof(ConsumerDecisionTreeLevelDto), typeof(IConsumerDecisionTreeLevelDal))]
        [ForeignKey(typeof(ConsumerDecisionTreeDto), typeof(IConsumerDecisionTreeDal))]
        public Int32 ConsumerDecisionTreeId { get; set; }
        [ForeignKey(typeof(ConsumerDecisionTreeLevelDto), typeof(IConsumerDecisionTreeLevelDal), DeleteBehavior.MoveUpToParent)]
        public Int32 ConsumerDecisionTreeLevelId { get; set; }
        public String Name { get; set; }
        [ForeignKey(typeof(ConsumerDecisionTreeNodeDto), typeof(IConsumerDecisionTreeNodeDal), DeleteBehavior.MoveUpToParent)]
        public Int32? ParentNodeId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ConsumerDecisionTreeNodeDto other = obj as ConsumerDecisionTreeNodeDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.GFSId != this.GFSId) { return false; }
                if (other.ConsumerDecisionTreeId != this.ConsumerDecisionTreeId) { return false; }
                if (other.ConsumerDecisionTreeLevelId != this.ConsumerDecisionTreeLevelId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.ParentNodeId != this.ParentNodeId) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class ConsumerDecisionTreeNodeDtoKey
    {
        #region Properties
        public Int32 ConsumerDecisionTreeId { get; set; }
        public Int32 ConsumerDecisionTreeLevelId { get; set; }
        public String Name { get; set; }
        public Int32? ParentNodeId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            if (ParentNodeId != null)
            {
                return
                    ConsumerDecisionTreeId.GetHashCode() +
                    ConsumerDecisionTreeLevelId.GetHashCode() +
                    Name.GetHashCode() + ParentNodeId.GetHashCode();
            }
            return
                ConsumerDecisionTreeId.GetHashCode() +
                ConsumerDecisionTreeLevelId.GetHashCode() +
                Name.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ConsumerDecisionTreeNodeDtoKey other = obj as ConsumerDecisionTreeNodeDtoKey;
            if (other != null)
            {

                if (other.ConsumerDecisionTreeId != this.ConsumerDecisionTreeId) { return false; }
                if (other.ConsumerDecisionTreeLevelId != this.ConsumerDecisionTreeLevelId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.ParentNodeId != null || this.ParentNodeId != null)
                {
                    if (other.ParentNodeId != this.ParentNodeId)
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
