﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    ///     <c>Data Transfer Object</c> for a <c>Planogram Comparison Template Field</c> model object.
    /// </summary>
    [Serializable]
    public class PlanogramComparisonTemplateFieldDto
    {
        #region Properties

        public Object Id { get; set; }

        public PlanogramComparisonTemplateFieldDtoKey DtoKey
        {
            get
            {
                return new PlanogramComparisonTemplateFieldDtoKey
                       {
                           PlanogramComparisonTemplateId = PlanogramComparisonTemplateId,
                           ItemType = ItemType,
                           FieldPlaceholder = FieldPlaceholder
                       };
            }
        }

        [ForeignKey(typeof (PlanogramComparisonTemplateDto), typeof (IPlanogramComparisonTemplateDal), DeleteBehavior.Cascade)]
        public Object PlanogramComparisonTemplateId { get; set; }

        public Byte ItemType { get; set; }

        public String FieldPlaceholder { get; set; }

        public String DisplayName { get; set; }

        public Int16 Number { get; set; }

        public Boolean Display { get; set; }

        #endregion

        #region Methods

        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramComparisonTemplateFieldDto;
            return other != null &&
                   Equals(other.Id, Id) &&
                   Equals(other.ItemType, ItemType) &&
                   Equals(other.PlanogramComparisonTemplateId, PlanogramComparisonTemplateId) &&
                   Equals(other.DisplayName, DisplayName) &&
                   Equals(other.Number, Number) &&
                   Equals(other.Display, Display) &&
                   Equals(other.FieldPlaceholder, FieldPlaceholder);
        }

        #endregion
    }

    [Serializable]
    public class PlanogramComparisonTemplateFieldDtoKey
    {
        #region Properties

        public Object PlanogramComparisonTemplateId { get; set; }

        public Byte ItemType { get; set; }

        public String FieldPlaceholder { get; set; }

        #endregion

        #region Methods

        public override Int32 GetHashCode()
        {
            return HashCode.Start.Hash(PlanogramComparisonTemplateId).Hash(ItemType).Hash(FieldPlaceholder).GetHashCode();
        }

        public override Boolean Equals(Object obj)
        {
            var other = obj as PlanogramComparisonTemplateFieldDtoKey;
            return other != null &&
                   Equals(other.PlanogramComparisonTemplateId, PlanogramComparisonTemplateId) &&
                   Equals(other.ItemType, ItemType) &&
                   Equals(other.FieldPlaceholder, FieldPlaceholder);
        }

        #endregion
    }
}