﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Planogram Export Template Performance Metric Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramExportTemplatePerformanceMetricDto
    {
         #region Properties
        public Int32 Id { get; set; }
        public PlanogramExportTemplatePerformanceMetricDtoKey DtoKey
        {
            get
            {
                return new PlanogramExportTemplatePerformanceMetricDtoKey()
                {
                    PlanogramExportTemplateId = this.PlanogramExportTemplateId,
                    Name = this.Name,
                    MetricId = this.MetricId
                }; 
            }
        }
        [ForeignKey(typeof(PlanogramExportTemplateDto), typeof(IPlanogramExportTemplateDal))]
        public Object PlanogramExportTemplateId { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Byte Direction { get; set; }
        public Byte SpecialType { get; set; }
        public Byte MetricType { get; set; }
        public Byte MetricId { get; set; }
        public String ExternalField { get; set; }
        public Byte AggregationType { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramExportTemplatePerformanceMetricDto other = obj as PlanogramExportTemplatePerformanceMetricDto;
            if (other != null)
            {
                //Id
                if (other.Id != this.Id) { return false; }

                //PlanogramExportTemplateId
                if ((other.PlanogramExportTemplateId != null) && (this.PlanogramExportTemplateId != null))
                {
                    if (!other.PlanogramExportTemplateId.Equals(this.PlanogramExportTemplateId)) { return false; }
                }
                if ((other.PlanogramExportTemplateId != null) && (this.PlanogramExportTemplateId == null)) { return false; }
                if ((other.PlanogramExportTemplateId == null) && (this.PlanogramExportTemplateId != null)) { return false; }

                if (other.Name != this.Name) { return false; }
                if (other.Description != this.Description) { return false; }
                if (other.Direction != this.Direction) { return false; }
                if (other.SpecialType != this.SpecialType) { return false; }
                if (other.MetricType != this.MetricType) { return false; }
                if (other.MetricId != this.MetricId) { return false; }
                if (other.ExternalField != this.ExternalField) { return false; }
                if (other.AggregationType != this.AggregationType) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion

    }

    [Serializable]
    public class PlanogramExportTemplatePerformanceMetricDtoKey
    {
        #region Properties

        public Object PlanogramExportTemplateId { get; set; }
        public String Name { get; set; }
        public Byte MetricId { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return PlanogramExportTemplateId.GetHashCode() +
                Name.GetHashCode()+
                MetricId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramExportTemplatePerformanceMetricDtoKey other = obj as PlanogramExportTemplatePerformanceMetricDtoKey;
            if (other != null)
            {
                //PlanogramExportTemplateId
                if ((other.PlanogramExportTemplateId != null) && (this.PlanogramExportTemplateId != null))
                {
                    if (!other.PlanogramExportTemplateId.Equals(this.PlanogramExportTemplateId)) { return false; }
                }
                if ((other.PlanogramExportTemplateId != null) && (this.PlanogramExportTemplateId == null)) { return false; }
                if ((other.PlanogramExportTemplateId == null) && (this.PlanogramExportTemplateId != null)) { return false; }

                if (other.Name != this.Name) { return false; }
                if (other.MetricId != this.MetricId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
