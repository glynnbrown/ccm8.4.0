﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-V8-24863 : L.Hodson
//		Created
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// SystemSetting Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class SystemSettingsDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public SystemSettingsDtoKey DtoKey
        {
            get
            {
                return new SystemSettingsDtoKey()
                {
                    Key = this.Key,
                    UserId = this.UserId,
                    EntityId = this.EntityId
                };
            }
        }
        public String Key { get; set; }
        public String Value { get; set; }

        [ForeignKey(typeof(UserDto), typeof(IUserDal), DeleteBehavior.Leave)]
        public Int32? UserId { get; set; }

        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32? EntityId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            SystemSettingsDto other = obj as SystemSettingsDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.Key != this.Key) { return false; }
                if (other.Value != this.Value) { return false; }
                if (other.UserId != this.UserId) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class SystemSettingsDtoKey
    {
        #region Properties
        public String Key { get; set; }
        public Int32? UserId { get; set; }
        public Int32? EntityId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Key.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            SystemSettingsDtoKey other = obj as SystemSettingsDtoKey;
            if (other != null)
            {
                if (other.Key != this.Key) { return false; }
                if (other.UserId != this.UserId) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
