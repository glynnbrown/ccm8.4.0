﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#region Version History: (CCM CCM803)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Location Space Info Data Transfer Object
    /// </summary>
    [Serializable]
    public class LocationSpaceInfoDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public Guid UniqueContentReference { get; set; }
        public Int16 LocationId { get; set; }
        public String LocationName { get; set; }
        public String LocationCode { get; set; }
        public Int32 EntityId { get; set; }
        public Guid? ParentUniqueContentReference { get; set; }
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override bool Equals(object obj)
        {
            LocationSpaceInfoDto other = obj as LocationSpaceInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id)
                {
                    return false;
                }
                if (other.UniqueContentReference != this.UniqueContentReference)
                {
                    return false;
                }
                if (other.LocationId != this.LocationId)
                {
                    return false;
                }
                if (other.LocationName != this.LocationName)
                {
                    return false;
                }
                if (other.LocationCode != this.LocationCode)
                {
                    return false;
                }
                if (other.EntityId != this.EntityId)
                {
                    return false;
                }
                if (other.ParentUniqueContentReference != this.ParentUniqueContentReference) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
