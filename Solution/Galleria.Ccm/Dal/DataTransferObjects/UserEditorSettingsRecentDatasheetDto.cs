﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM 830
// V8-31699 : A.Heathcote
//  Created.
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.DataStructures;


namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public sealed class UserEditorSettingsRecentDatasheetDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public Object DatasheetId { get; set; }
        public UserEditorSettingsRecentDatasheetDtoKey DtoKey
        {
            get
            {
                return new UserEditorSettingsRecentDatasheetDtoKey()
                {
                    DatasheetId = this.DatasheetId,
                };
            }
        }
        #endregion

        #region Methods

        public override int GetHashCode()
        {
            return HashCode.Start.Hash(Id).Hash(DatasheetId);
        }

        public override bool Equals(Object obj)
        {
            UserEditorSettingsRecentDatasheetDto other = obj as UserEditorSettingsRecentDatasheetDto;
            if (other != null)
            {
                return
                    Equals(this.Id, other.Id) &&
                    Equals(this.DatasheetId, other.DatasheetId);
            }
            else
            {
                return false;
            }

        }
        public String ToString()
        {
            return this.Id.ToString();
        }
    }
        #endregion
    [Serializable]
    public sealed class UserEditorSettingsRecentDatasheetDtoKey
    {
        #region Properties
        public Object DatasheetId { get; set; }

        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return DatasheetId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            UserEditorSettingsRecentDatasheetDtoKey other = obj as UserEditorSettingsRecentDatasheetDtoKey;
            if (other != null)
            {
                if (!Equals(other.DatasheetId, this.DatasheetId)) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

}





