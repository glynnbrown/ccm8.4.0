﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created (Auto-generated)
// V8-25873 : A.Probyn
//  Added Custom Attribute 1 - 5
#endregion
#endregion

using System;
using System.Linq;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// FixturePackageInfo Data Transfer Object
    /// </summary>
    [Serializable]
    public class FixturePackageInfoDto
    {
        #region Properties
        public Object Id { get; set; }
        public FixturePackageInfoDtoKey DtoKey
        {
            get
            {
                return new FixturePackageInfoDtoKey()
                {
                    Id = this.Id,
                };
            }
        }
        public Object FolderId { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Single Height { get; set; }
        public Single Width { get; set; }
        public Single Depth { get; set; }
        public Int32 FixtureCount { get; set; }
        public Int32 AssemblyCount { get; set; }
        public Int32 ComponentCount { get; set; }
        public Byte[] ThumbnailImageData { get; set; }
        public String CustomAttribute1 { get; set; }
        public String CustomAttribute2 { get; set; }
        public String CustomAttribute3 { get; set; }
        public String CustomAttribute4 { get; set; }
        public String CustomAttribute5 { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FixturePackageInfoDto other = obj as FixturePackageInfoDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }

                // FolderId
                if ((other.FolderId != null) && (this.FolderId != null))
                {
                    if (!other.FolderId.Equals(this.FolderId)) { return false; }
                }
                if ((other.FolderId != null) && (this.FolderId == null)) { return false; }
                if ((other.FolderId == null) && (this.FolderId != null)) { return false; }

                if (other.Name != this.Name) { return false; }
                if (other.Description != this.Description) { return false; }
                if (other.Height != this.Height) { return false; }
                if (other.Width != this.Width) { return false; }
                if (other.Depth != this.Depth) { return false; }
                if (other.FixtureCount != this.FixtureCount) { return false; }
                if (other.AssemblyCount != this.AssemblyCount) { return false; }
                if (other.ComponentCount != this.ComponentCount) { return false; }

                if (other.CustomAttribute1 != this.CustomAttribute1) { return false; }
                if (other.CustomAttribute2 != this.CustomAttribute2) { return false; }
                if (other.CustomAttribute3 != this.CustomAttribute3) { return false; }
                if (other.CustomAttribute4 != this.CustomAttribute4) { return false; }
                if (other.CustomAttribute5 != this.CustomAttribute5) { return false; }


                //ThumbnailImageData
                if ((other.ThumbnailImageData != null) && (this.ThumbnailImageData == null))
                {
                    return false;
                }
                if ((other.ThumbnailImageData == null) && (this.ThumbnailImageData != null))
                {
                    return false;
                }
                if ((other.ThumbnailImageData != null) && (this.ThumbnailImageData != null) && (!other.ThumbnailImageData.SequenceEqual(this.ThumbnailImageData)))
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class FixturePackageInfoDtoKey
    {
        #region Properties
        public Object Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FixturePackageInfoDtoKey other = obj as FixturePackageInfoDtoKey;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
