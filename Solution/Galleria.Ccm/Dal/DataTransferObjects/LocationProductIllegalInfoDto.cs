﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25447 : N.Haywood
//  Copied over from GFS
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class LocationProductIllegalInfoDto
    {
        #region Properties
        public Int16 LocationId { get; set; }
        public Int32 ProductId { get; set; }
        public Int32 EntityId { get; set; }
        public String LocationCode { get; set; }
        public String GTIN { get; set; }
        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            LocationProductIllegalInfoDto other = obj as LocationProductIllegalInfoDto;
            if (other != null)
            {
                if (other.ProductId != this.ProductId) { return false; }
                if (other.LocationId != this.LocationId) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.LocationCode != this.LocationCode) { return false; }
                if (other.GTIN != this.GTIN) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
