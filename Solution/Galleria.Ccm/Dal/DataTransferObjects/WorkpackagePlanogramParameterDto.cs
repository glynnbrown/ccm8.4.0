﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
// V8-25886 : L.Ineson
//  Removed value
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public sealed class WorkpackagePlanogramParameterDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public WorkpackagePlanogramParameterDtoKey DtoKey { get { return new WorkpackagePlanogramParameterDtoKey() { Id = this.Id }; } }
        
        [ForeignKey(typeof(WorkpackagePlanogramDto), typeof(IWorkpackagePlanogramDal))]
        public Int32 WorkpackagePlanogramId { get; set; }
        
        [ForeignKey(typeof(WorkflowTaskParameterDto), typeof(IWorkflowTaskParameterDal))]
        public Int32 WorkflowTaskParameterId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the instance hash code
        /// </summary>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Compares two instances
        /// </summary>
        public override Boolean Equals(object obj)
        {
            WorkpackagePlanogramParameterDto other = obj as WorkpackagePlanogramParameterDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.WorkpackagePlanogramId != this.WorkpackagePlanogramId) return false;
                if (other.WorkflowTaskParameterId != this.WorkflowTaskParameterId) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class WorkpackagePlanogramParameterDtoKey
    {
        #region Properties
        public Int32 Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the ISOme
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            WorkpackagePlanogramParameterDtoKey other = obj as WorkpackagePlanogramParameterDtoKey;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
