﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25886 : L.Ineson
//  Created
// V8-27426 : L.Ineson
//  Added 2 more values
#endregion
#region Version History: CCM830
// V8-32396 : A.Probyn
//  Corrected DtoKey.
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public sealed class WorkflowTaskParameterValueDto
    {
        #region Properties
        public Int32 Id { get; set; }
        [ForeignKey(typeof(WorkflowTaskParameterDto), typeof(IWorkflowTaskParameterDal))]
        public Int32 WorkflowTaskParameterId { get; set; }
        public Object Value1 { get; set; }
        public Object Value2 { get; set; }
        public Object Value3 { get; set; }
        public WorkflowTaskParameterValueDtoKey DtoKey { get { return new WorkflowTaskParameterValueDtoKey() { Id = this.Id }; } }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the instance hash code
        /// </summary>
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Compares two instances
        /// </summary>
        public override Boolean Equals(object obj)
        {
            WorkflowTaskParameterValueDto other = obj as WorkflowTaskParameterValueDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.WorkflowTaskParameterId != this.WorkflowTaskParameterId) return false;
                if (((other.Value1 == null) && (this.Value1 != null)) ||
                    ((other.Value1 !=  null) && (!other.Value1.Equals(this.Value1))))
                {
                    return false;
                }

                if (((other.Value2 == null) && (this.Value2 != null)) ||
                    ((other.Value2 != null) && (!other.Value2.Equals(this.Value2))))
                {
                    return false;
                }

                if (((other.Value3 == null) && (this.Value3 != null)) ||
                    ((other.Value3 != null) && (!other.Value3.Equals(this.Value3))))
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class WorkflowTaskParameterValueDtoKey
    {
        #region Properties
        public Int32 Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the ISOme
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            WorkflowTaskParameterValueDtoKey other = obj as WorkflowTaskParameterValueDtoKey;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
