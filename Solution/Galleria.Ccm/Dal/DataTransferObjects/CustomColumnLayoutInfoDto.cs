#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.
// V8-26201 : A.Silva ~ Corrected Equals.

#endregion

#endregion

using System;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    ///     Data transfer object for CustomColumnLayoutInfo
    /// </summary>
    [Serializable]
    public sealed class CustomColumnLayoutInfoDto
    {
        #region Properties

        public Object Id { get; set; }
        public String Name { get; set; }
        public Byte Type { get; set; }

        #endregion

        #region IEquatable Support

        /// <summary>
        ///     Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        ///     <see cref="T:System.Object" />.
        /// </summary>
        /// <returns>
        ///     true if the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />;
        ///     otherwise, false.
        /// </returns>
        /// <param name="obj">The <see cref="T:System.Object" /> to compare with the current <see cref="T:System.Object" />. </param>
        public override bool Equals(object obj)
        {
            var other = obj as CustomColumnLayoutInfoDto;

            if (other == null) return false;
            if (other.Id != null && Id != null &&
                !other.Id.Equals(Id)) return false;
            if (other.Id == null ^ Id == null) return false;
            if (other.Name != Name) return false;
            if (other.Type != Type) return false;

            return true;
        }

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current <see cref="T:System.Object" />.
        /// </returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        #endregion
    }
}