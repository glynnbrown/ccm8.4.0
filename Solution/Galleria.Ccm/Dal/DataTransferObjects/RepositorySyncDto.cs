﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class RepositorySyncDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public Int32 SourceTargetId { get; set; }
        public Int32 DestinationTargetId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the instance hash code
        /// </summary>
        /// <returns>The instance hash code</returns>
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Indicates if two instances are equal
        /// </summary>
        /// <param name="obj">The instance to compare to</param>
        /// <returns>True if equal, else false</returns>
        public override bool Equals(Object obj)
        {
            RepositorySyncDto other = obj as RepositorySyncDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.SourceTargetId != this.SourceTargetId) return false;
                if (other.DestinationTargetId != this.DestinationTargetId) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
