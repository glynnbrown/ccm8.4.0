﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// PrintTemplateComponent Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class PrintTemplateComponentDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public PrintTemplateComponentDtoKey DtoKey
        {
            get
            {
                return new PrintTemplateComponentDtoKey()
                {
                    PrintTemplateSectionId = this.PrintTemplateSectionId,
                    X = this.X,
                    Y = this.Y
                };
            }
        }
        [ForeignKey(typeof(PrintTemplateSectionDto), typeof(IPrintTemplateSectionDal), DeleteBehavior.Cascade)]
        public Int32 PrintTemplateSectionId { get; set; }
        public Byte Type { get; set; }
        public Single X { get; set; }
        public Single Y { get; set; }
        public Single Height { get; set; }
        public Single Width { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PrintTemplateComponentDto other = obj as PrintTemplateComponentDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.PrintTemplateSectionId != this.PrintTemplateSectionId) { return false; }
                if (other.Type != this.Type) { return false; }
                if (other.X != this.X) { return false; }
                if (other.Y != this.Y) { return false; }
                if (other.Height != this.Height) { return false; }
                if (other.Width != this.Width) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    /// <summary>
    /// PrintTemplateComponent Dto key
    /// </summary>
    [Serializable]
    public sealed class PrintTemplateComponentDtoKey
    {

        #region Properties

        public Int32 PrintTemplateSectionId { get; set; }
        public Single X { get; set; }
        public Single Y { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return PrintTemplateSectionId.GetHashCode() ^
                X.GetHashCode() ^
                Y.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PrintTemplateComponentDtoKey other = obj as PrintTemplateComponentDtoKey;
            if (other != null)
            {
                if (other.PrintTemplateSectionId != this.PrintTemplateSectionId) { return false; }
                if (other.X != this.X) { return false; }
                if (other.Y != this.Y) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


}
