﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-V8-26306 : L.Ineson
//		Created
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// SystemSetting Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class UserEditorSettingDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public UserEditorSettingDtoKey DtoKey
        {
            get
            {
                return new UserEditorSettingDtoKey()
                {
                    Key = this.Key,
                };
            }
        }
        public String Key { get; set; }
        public String Value { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            UserEditorSettingDto other = obj as UserEditorSettingDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.Key != this.Key) { return false; }
                if (other.Value != this.Value) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion

        public String ToString()
        {
            return this.Key.ToString();
        }
    }

    [Serializable]
    public sealed class UserEditorSettingDtoKey
    {
        #region Properties
        public String Key { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Key.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            UserEditorSettingDtoKey other = obj as UserEditorSettingDtoKey;
            if (other != null)
            {
                if (other.Key != this.Key) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
