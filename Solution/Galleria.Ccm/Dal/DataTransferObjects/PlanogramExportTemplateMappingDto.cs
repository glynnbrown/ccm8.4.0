﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Planogram Export Template Performance Metric Data Transfer Object
    /// </summary>
    [Serializable]
    public class PlanogramExportTemplateMappingDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public PlanogramExportTemplateMappingDtoKey DtoKey
        {
            get
            {
                return new PlanogramExportTemplateMappingDtoKey()
                {
                    PlanogramExportTemplateId = this.PlanogramExportTemplateId,
                    Field = this.Field,
                    FieldType = this.FieldType
                };
            }
        }
        [ForeignKey(typeof(PlanogramExportTemplateDto), typeof(IPlanogramExportTemplateDal))]
        public Object PlanogramExportTemplateId { get; set; }
        public Byte FieldType { get; set; }
        public String Field { get; set; }
        public String ExternalField { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramExportTemplateMappingDto other = obj as PlanogramExportTemplateMappingDto;
            if (other != null)
            {
                //Id
                if (other.Id != this.Id) { return false; }

                //PlanogramExportTemplateId
                if ((other.PlanogramExportTemplateId != null) && (this.PlanogramExportTemplateId != null))
                {
                    if (!other.PlanogramExportTemplateId.Equals(this.PlanogramExportTemplateId)) { return false; }
                }
                if ((other.PlanogramExportTemplateId != null) && (this.PlanogramExportTemplateId == null)) { return false; }
                if ((other.PlanogramExportTemplateId == null) && (this.PlanogramExportTemplateId != null)) { return false; }

                if (other.FieldType != this.FieldType) { return false; }
                if (other.Field != this.Field) { return false; }
                if (other.ExternalField != this.ExternalField) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


    [Serializable]
    public class PlanogramExportTemplateMappingDtoKey
    {
        #region Properties

        public Object PlanogramExportTemplateId { get; set; }
        public String Field { get; set; }
        public Byte FieldType { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return PlanogramExportTemplateId.GetHashCode() +
                Field.GetHashCode() + FieldType.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PlanogramExportTemplateMappingDtoKey other = obj as PlanogramExportTemplateMappingDtoKey;
            if (other != null)
            {
                //PlanogramExportTemplateId
                if ((other.PlanogramExportTemplateId != null) && (this.PlanogramExportTemplateId != null))
                {
                    if (!other.PlanogramExportTemplateId.Equals(this.PlanogramExportTemplateId)) { return false; }
                }
                if ((other.PlanogramExportTemplateId != null) && (this.PlanogramExportTemplateId == null)) { return false; }
                if ((other.PlanogramExportTemplateId == null) && (this.PlanogramExportTemplateId != null)) { return false; }

                if (other.Field != this.Field) { return false; }
                if (other.FieldType != this.FieldType) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
