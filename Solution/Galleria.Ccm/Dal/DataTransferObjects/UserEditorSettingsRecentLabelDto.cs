﻿#region Header Information
//Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
//V8-31699 : A.Heathcote
//Created
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    //<summary>
    //SystemSettingsRecentLabel Data Transfer Object
    //</summary>
    [Serializable]
    public sealed class UserEditorSettingsRecentLabelDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Byte LabelType { get; set; }
        public Byte SaveType { get; set; } 
        public Object LabelId { get; set; }
        public UserEditorSettingsRecentLabelDtoKey DtoKey  
        {
            get
            {
                return new UserEditorSettingsRecentLabelDtoKey()
                {
                    
                    LabelId = this.LabelId, 
                };
            }
        }
        

        #endregion

        #region Methods
        //<summary>
        //Returns a hash code for this object
        //</summary>
        //<returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return HashCode.Start.Hash(Id).Hash(LabelId).Hash(Name).Hash(LabelType).Hash(SaveType);
        }

        //<summary>
        //Check to see if two dtos are the same
        //</summary>
        //<param name="obj">The object to compare against</param>
        //<returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            UserEditorSettingsRecentLabelDto other = obj as UserEditorSettingsRecentLabelDto;
            if (other != null)
            {
                return
                    Equals(this.LabelId, other.LabelId) &&
                    Equals(this.Id, other.Id) &&
                    Equals(this.Name, other.Name) &&
                    Equals(this.LabelType, other.LabelType) &&
                    Equals(this.SaveType, other.SaveType);

            }
            else
            {
                return false;
            }
        }

          
        #endregion

        public String ToString()
        {
            return this.Id.ToString(); 
        }
    }

    [Serializable]
    public sealed class UserEditorSettingsRecentLabelDtoKey
    {
        #region Properties
        public Object LabelId { get; set; }
        #endregion

        #region Methods
        //<summary>
        //Returns a hash code for this object
        //</summary>
        //<returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return LabelId.GetHashCode();
        }

        //<summary>
        // Check to see if two keys are the same
        // </summary>
        // <param name="obj">The object to compare against</param>
        // <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            UserEditorSettingsRecentLabelDtoKey other = obj as UserEditorSettingsRecentLabelDtoKey;
            if (other != null)
            {
                if (!Equals(other.LabelId, this.LabelId)) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
