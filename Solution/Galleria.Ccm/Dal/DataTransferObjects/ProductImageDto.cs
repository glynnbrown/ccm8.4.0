﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public sealed class ProductImageDto
    {
        #region Properties

        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public ProductImageDtoKey DtoKey
        {
            get
            {
                return new ProductImageDtoKey() 
                { 
                    Id = this.Id
                };
            }
        }
        [ForeignKey(typeof(ProductDto), typeof(IProductDal), DeleteBehavior.Leave)]
        public Int32 ProductId { get; set; }
        [InheritedProperty(typeof(ProductDto), typeof(IProductDal), "EntityId","ProductId")]
        [ForeignKey(typeof(EntityDto),typeof(IEntityDal),DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        [ForeignKey(typeof(ImageDto), typeof(IImageDal), DeleteBehavior.Leave)]
        public Int32 ImageId { get; set; }
        public Byte ImageType { get; set; }
        public Byte FacingType { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }

        #endregion

        #region Methods

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override bool Equals(object obj)
        {
            ProductImageDto other = obj as ProductImageDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.ProductId != this.ProductId) { return false; }
                if (other.ImageId != this.ImageId) { return false; }
                if (other.ImageType != this.ImageType) { return false; }
                if (other.FacingType != this.FacingType) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        #endregion

    }

    public class ProductImageDtoKey
    {
        public Int32 Id { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as ProductImageDtoKey;
            if (other == null) return false;
            return other.Id == this.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
