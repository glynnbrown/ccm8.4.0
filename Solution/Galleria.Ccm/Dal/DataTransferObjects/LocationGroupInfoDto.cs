﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//		Created 
#endregion
#endregion

using System;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// LocationGroupInfo Data Transfer Object
    /// </summary>
    [Serializable]
    public class LocationGroupInfoDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Code { get; set; }
        public DateTime? DateDeleted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            LocationGroupInfoDto other = obj as LocationGroupInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.Code != this.Code) { return false; }
                if (other.DateDeleted != this.DateDeleted) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
