﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public sealed class PlanogramStatusDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public PlanogramStatusDtoKey DtoKey { get { return new PlanogramStatusDtoKey() { Id = this.Id }; } }
        public RowVersion RowVersion { get; set; }
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal))]
        public Int32 EntityId { get; set; }
        public String Name { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the instance has code
        /// </summary>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Compares two instances to each other
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            PlanogramStatusDto other = obj as PlanogramStatusDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.RowVersion != this.RowVersion) return false;
                if (other.EntityId != this.EntityId) return false;
                if (other.Name != this.Name) return false;
                if (other.DateCreated != this.DateCreated) return false;
                if (other.DateLastModified != this.DateLastModified) return false;
                if (other.DateDeleted != this.DateDeleted) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class PlanogramStatusDtoKey
    {
        #region Properties
        public Int32 Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the hash code for this instance
        /// </summary>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Compares two instances to see if they are equal
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            PlanogramStatusDtoKey other = obj as PlanogramStatusDtoKey;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
