﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created 
#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// AssortmentProductBuddy Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class AssortmentProductBuddyDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public AssortmentProductBuddyDtoKey DtoKey
        {
            get
            {
                return new AssortmentProductBuddyDtoKey()
                {
                    ProductId = this.ProductId,
                    AssortmentId = this.AssortmentId
                };
            }
        }
        [ForeignKey(typeof(ProductDto), typeof(IProductDal), DeleteBehavior.Leave)]
        public Int32 ProductId { get; set; }
        [InheritedProperty(typeof(ProductDto), typeof(IProductDal), ForeignKeyPropertyName = "ProductId", ParentPropertyName = "Gtin")]
        public String ProductGtin { get; set; }
        public Byte SourceType { get; set; }
        public Byte TreatmentType { get; set; }
        public Single TreatmentTypePercentage { get; set; }
        public Byte ProductAttributeType { get; set; }
        [ForeignKey(typeof(ProductDto), typeof(IProductDal), DeleteBehavior.Leave)]
        public Int32? S1ProductId { get; set; }
        [InheritedProperty(typeof(ProductDto), typeof(IProductDal), ForeignKeyPropertyName = "S1ProductId", ParentPropertyName = "Gtin")]
        public String S1ProductGtin { get; set; }
        public Single? S1Percentage { get; set; }
        [ForeignKey(typeof(ProductDto), typeof(IProductDal), DeleteBehavior.Leave)]
        public Int32? S2ProductId { get; set; }
        [InheritedProperty(typeof(ProductDto), typeof(IProductDal), ForeignKeyPropertyName = "S2ProductId", ParentPropertyName = "Gtin")]
        public String S2ProductGtin { get; set; }
        public Single? S2Percentage { get; set; }
        [ForeignKey(typeof(ProductDto), typeof(IProductDal), DeleteBehavior.Leave)]
        public Int32? S3ProductId { get; set; }
        [InheritedProperty(typeof(ProductDto), typeof(IProductDal), ForeignKeyPropertyName = "S3ProductId", ParentPropertyName = "Gtin")]
        public String S3ProductGtin { get; set; }
        public Single? S3Percentage { get; set; }
        [ForeignKey(typeof(ProductDto), typeof(IProductDal), DeleteBehavior.Leave)]
        public Int32? S4ProductId { get; set; }
        [InheritedProperty(typeof(ProductDto), typeof(IProductDal), ForeignKeyPropertyName = "S4ProductId", ParentPropertyName = "Gtin")]
        public String S4ProductGtin { get; set; }
        public Single? S4Percentage { get; set; }
        [ForeignKey(typeof(ProductDto), typeof(IProductDal), DeleteBehavior.Leave)]
        public Int32? S5ProductId { get; set; }
        [InheritedProperty(typeof(ProductDto), typeof(IProductDal), ForeignKeyPropertyName = "S5ProductId", ParentPropertyName = "Gtin")]
        public String S5ProductGtin { get; set; }
        public Single? S5Percentage { get; set; }
        [ForeignKey(typeof(AssortmentDto), typeof(ILocationDal), DeleteBehavior.Cascade)]
        public Int32 AssortmentId { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentProductBuddyDto other = obj as AssortmentProductBuddyDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.ProductId != this.ProductId) { return false; }
                if (other.ProductGtin != this.ProductGtin) { return false; }
                if (other.SourceType != this.SourceType) { return false; }
                if (other.TreatmentType != this.TreatmentType) { return false; }
                if (other.TreatmentTypePercentage != this.TreatmentTypePercentage) { return false; }
                if (other.ProductAttributeType != this.ProductAttributeType) { return false; }
                if (other.S1ProductId != this.S1ProductId) { return false; }
                if (other.S1ProductGtin != this.S1ProductGtin) { return false; }
                if (other.S1Percentage != this.S1Percentage) { return false; }
                if (other.S2ProductId != this.S2ProductId) { return false; }
                if (other.S2ProductGtin != this.S2ProductGtin) { return false; }
                if (other.S2Percentage != this.S2Percentage) { return false; }
                if (other.S3ProductId != this.S3ProductId) { return false; }
                if (other.S3ProductGtin != this.S3ProductGtin) { return false; }
                if (other.S3Percentage != this.S3Percentage) { return false; }
                if (other.S4ProductId != this.S4ProductId) { return false; }
                if (other.S4ProductGtin != this.S4ProductGtin) { return false; }
                if (other.S4Percentage != this.S4Percentage) { return false; }
                if (other.S5ProductId != this.S5ProductId) { return false; }
                if (other.S5ProductGtin != this.S5ProductGtin) { return false; }
                if (other.S5Percentage != this.S5Percentage) { return false; }
                if (other.AssortmentId != this.AssortmentId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentProductBuddyDtoKey
    {
        #region Properties
        public Int32 ProductId { get; set; }
        public Int32 AssortmentId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                ProductId.GetHashCode() +
                AssortmentId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentProductBuddyDtoKey other = obj as AssortmentProductBuddyDtoKey;
            if (other != null)
            {
                if (other.ProductId != this.ProductId) { return false; }
                if (other.AssortmentId != this.AssortmentId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentProductBuddyIsSetDto
    {
        #region Properties

        public Boolean IsProductIdSet { get; set; }
        public Boolean IsSourceTypeSet { get; set; }
        public Boolean IsTreatmentTypeSet { get; set; }
        public Boolean IsTreatmentTypePercentageSet { get; set; }
        public Boolean IsProductAttributeTypeSet { get; set; }
        public Boolean IsS1ProductIdSet { get; set; }
        public Boolean IsS1PercentageSet { get; set; }
        public Boolean IsS2ProductIdSet { get; set; }
        public Boolean IsS2PercentageSet { get; set; }
        public Boolean IsS3ProductIdSet { get; set; }
        public Boolean IsS3PercentageSet { get; set; }
        public Boolean IsS4ProductIdSet { get; set; }
        public Boolean IsS4PercentageSet { get; set; }
        public Boolean IsS5ProductIdSet { get; set; }
        public Boolean IsS5PercentageSet { get; set; }
        public Boolean IsAssortmentIdSet { get; set; }


        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public AssortmentProductBuddyIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public AssortmentProductBuddyIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsProductIdSet.GetHashCode() +
                IsSourceTypeSet.GetHashCode() +
                IsTreatmentTypeSet.GetHashCode() +
                IsTreatmentTypePercentageSet.GetHashCode() +
                IsProductAttributeTypeSet.GetHashCode() +
                IsS1ProductIdSet.GetHashCode() +
                IsS1PercentageSet.GetHashCode() +
                IsS2ProductIdSet.GetHashCode() +
                IsS2PercentageSet.GetHashCode() +
                IsS3ProductIdSet.GetHashCode() +
                IsS3PercentageSet.GetHashCode() +
                IsS4ProductIdSet.GetHashCode() +
                IsS4PercentageSet.GetHashCode() +
                IsS5ProductIdSet.GetHashCode() +
                IsS5PercentageSet.GetHashCode() +
                IsAssortmentIdSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentProductBuddyIsSetDto other = obj as AssortmentProductBuddyIsSetDto;
            if (other != null)
            {
                if (other.IsProductIdSet != this.IsProductIdSet) { return false; }
                if (other.IsSourceTypeSet != this.IsSourceTypeSet) { return false; }
                if (other.IsTreatmentTypeSet != this.IsTreatmentTypeSet) { return false; }
                if (other.IsTreatmentTypePercentageSet != this.IsTreatmentTypePercentageSet) { return false; }
                if (other.IsProductAttributeTypeSet != this.IsProductAttributeTypeSet) { return false; }
                if (other.IsS1ProductIdSet != this.IsS1ProductIdSet) { return false; }
                if (other.IsS1PercentageSet != this.IsS1PercentageSet) { return false; }
                if (other.IsS2ProductIdSet != this.IsS2ProductIdSet) { return false; }
                if (other.IsS2PercentageSet != this.IsS2PercentageSet) { return false; }
                if (other.IsS3ProductIdSet != this.IsS3ProductIdSet) { return false; }
                if (other.IsS3PercentageSet != this.IsS3PercentageSet) { return false; }
                if (other.IsS4ProductIdSet != this.IsS4ProductIdSet) { return false; }
                if (other.IsS4PercentageSet != this.IsS4PercentageSet) { return false; }
                if (other.IsS5ProductIdSet != this.IsS5ProductIdSet) { return false; }
                if (other.IsS5PercentageSet != this.IsS5PercentageSet) { return false; }
                if (other.IsAssortmentIdSet != this.IsAssortmentIdSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsProductIdSet = isSet;
            IsSourceTypeSet = isSet;
            IsTreatmentTypeSet = isSet;
            IsTreatmentTypePercentageSet = isSet;
            IsProductAttributeTypeSet = isSet;
            IsS1ProductIdSet = isSet;
            IsS1PercentageSet = isSet;
            IsS2ProductIdSet = isSet;
            IsS2PercentageSet = isSet;
            IsS3ProductIdSet = isSet;
            IsS3PercentageSet = isSet;
            IsS4ProductIdSet = isSet;
            IsS4PercentageSet = isSet;
            IsS5ProductIdSet = isSet;
            IsS5PercentageSet = isSet;
            IsAssortmentIdSet = isSet;
        }
        #endregion
    }
}

