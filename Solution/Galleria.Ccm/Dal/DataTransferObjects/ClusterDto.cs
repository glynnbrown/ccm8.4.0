﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-25629 : A.Kuszyk
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Cluster Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class ClusterDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public ClusterDtoKey DtoKey
        {
            get
            {
                return new ClusterDtoKey()
                {
                    Name = this.Name,
                    ClusterSchemeId = this.ClusterSchemeId
                };
            }
        }
        public String Name { get; set; }
        [ForeignKey(typeof(ClusterSchemeDto),typeof(IClusterSchemeDal),DeleteBehavior.Cascade)]
        public Int32 ClusterSchemeId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ClusterDto other = obj as ClusterDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.ClusterSchemeId != this.ClusterSchemeId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class ClusterDtoKey
    {
        #region Properties
        public String Name { get; set; }
        public Int32 ClusterSchemeId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Name.GetHashCode() + ClusterSchemeId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ClusterDtoKey other = obj as ClusterDtoKey;
            if (other != null)
            {
                if (other.Name != this.Name) { return false; }
                if (other.ClusterSchemeId != this.ClusterSchemeId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class ClusterIsSetDto
    {
        #region Properties
        public Boolean IsNameSet { get; set; }
        public Boolean IsClusterSchemeIdSet { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public ClusterIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public ClusterIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsNameSet.GetHashCode() +
                IsClusterSchemeIdSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            ClusterIsSetDto other = obj as ClusterIsSetDto;
            if (other != null)
            {

                if (other.IsNameSet != this.IsNameSet) { return false; }
                if (other.IsClusterSchemeIdSet != this.IsClusterSchemeIdSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsNameSet = isSet;
            IsClusterSchemeIdSet = isSet;
        }
        #endregion
    }
}
