﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
// CCM-27059 : J.Pickup
//      Reviewed FK attributes
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// AssortmentMinorRevisionAmendDistributionActionLocation Data Transfer Object
    /// </summary>
    [Serializable]
    public class AssortmentMinorRevisionAmendDistributionActionLocationDto
    {
        #region Properties

        public Int32 Id { get; set; }
        [ForeignKey(typeof(AssortmentMinorRevisionAmendDistributionActionDto), typeof(IAssortmentMinorRevisionAmendDistributionActionDal), DeleteBehavior.Cascade)]
        public Int32 AssortmentMinorRevisionAmendDistributionActionId { get; set; }
        public String LocationCode { get; set; }
        public String LocationName { get; set; }
        [ForeignKey(typeof(LocationDto), typeof(ILocationDal), DeleteBehavior.Leave)]
        public Int16? LocationId { get; set; }
        public Int32 Units { get; set; }
        public Int32 Facings { get; set; }
        public AssortmentMinorRevisionAmendDistributionActionLocationDtoKey DtoKey
        {
            get
            {
                return new AssortmentMinorRevisionAmendDistributionActionLocationDtoKey()
                {
                     AssortmentMinorRevisionAmendDistributionActionId = this.AssortmentMinorRevisionAmendDistributionActionId,
                     LocationName = this.LocationName
                };
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentMinorRevisionAmendDistributionActionLocationDto other = obj as AssortmentMinorRevisionAmendDistributionActionLocationDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.AssortmentMinorRevisionAmendDistributionActionId != this.AssortmentMinorRevisionAmendDistributionActionId) { return false; }
                if (other.LocationCode != this.LocationCode) { return false; }
                if (other.LocationName != this.LocationName) { return false; }
                if (other.LocationId != this.LocationId) { return false; }
                if (other.Units != this.Units) { return false; }
                if (other.Facings != this.Facings) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentMinorRevisionAmendDistributionActionLocationDtoKey
    {
        #region Properties

        public Int32 AssortmentMinorRevisionAmendDistributionActionId { get; set; }
        public String LocationName { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                AssortmentMinorRevisionAmendDistributionActionId.GetHashCode() +
                LocationName.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentMinorRevisionAmendDistributionActionLocationDtoKey other = obj as AssortmentMinorRevisionAmendDistributionActionLocationDtoKey;
            if (other != null)
            {
                if (other.AssortmentMinorRevisionAmendDistributionActionId != this.AssortmentMinorRevisionAmendDistributionActionId) { return false; }
                if (other.LocationName != this.LocationName) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentMinorRevisionAmendDistributionActionLocationIsSetDto
    {
        #region Properties

        public Boolean IsIdSet { get; set; }
        public Boolean IsAssortmentMinorRevisionAmendDistributionActionIdSet { get; set; }
        public Boolean IsLocationCodeSet { get; set; }
        public Boolean IsLocationNameSet { get; set; }
        public Boolean IsLocationIdSet { get; set; }
        public Boolean IsUnitsSet { get; set; }
        public Boolean IsFacingsSet { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Default
        /// </summary>
        public AssortmentMinorRevisionAmendDistributionActionLocationIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public AssortmentMinorRevisionAmendDistributionActionLocationIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsIdSet.GetHashCode() +
                IsAssortmentMinorRevisionAmendDistributionActionIdSet.GetHashCode() +
                IsLocationCodeSet.GetHashCode() +
                IsLocationNameSet.GetHashCode() +
                IsLocationIdSet.GetHashCode() +
                IsUnitsSet.GetHashCode() +
                IsFacingsSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentMinorRevisionAmendDistributionActionLocationIsSetDto other = obj as AssortmentMinorRevisionAmendDistributionActionLocationIsSetDto;
            if (other != null)
            {
                if (other.IsIdSet != this.IsIdSet) { return false; }
                if (other.IsAssortmentMinorRevisionAmendDistributionActionIdSet != this.IsAssortmentMinorRevisionAmendDistributionActionIdSet) { return false; }
                if (other.IsLocationCodeSet != this.IsLocationCodeSet) { return false; }
                if (other.IsLocationNameSet != this.IsLocationNameSet) { return false; }
                if (other.IsLocationIdSet != this.IsLocationIdSet) { return false; }
                if (other.IsUnitsSet != this.IsUnitsSet) { return false; }
                if (other.IsFacingsSet != this.IsFacingsSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsIdSet = isSet;
            IsAssortmentMinorRevisionAmendDistributionActionIdSet = isSet;
            IsLocationCodeSet = isSet;
            IsLocationNameSet = isSet;
            IsLocationIdSet = isSet;
            IsUnitsSet = isSet;
            IsFacingsSet = isSet;
        }

        #endregion
    }
}
