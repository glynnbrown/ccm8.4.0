﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM 830
// V8-31699 : A.Heathcote
//  Created.
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.DataStructures;


namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public sealed class UserEditorSettingsRecentHighlightDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public Object HighlightId { get; set; }
        public Byte SaveType { get; set; }
        public UserEditorSettingsRecentHighlightDtoKey DtoKey
        {
            get
            {
                return new UserEditorSettingsRecentHighlightDtoKey()
                {
                    HighlightId = this.HighlightId,
                };
            }
        }
        #endregion

        #region Methods

        public override int GetHashCode()
        {
            return HashCode.Start.Hash(Id).Hash(HighlightId).Hash(SaveType);
        }

        public override bool Equals(Object obj)
        {
            UserEditorSettingsRecentHighlightDto other = obj as UserEditorSettingsRecentHighlightDto;
            if (other != null)
            {
                return
                    Equals(this.Id, other.Id) &&
                    Equals(this.HighlightId, other.HighlightId) &&
                    Equals(this.SaveType, other.SaveType);
            }
            else
            {
                return false;
            }

        }
        public String ToString()
        {
            return this.Id.ToString();
        }
    }
        #endregion
    [Serializable]
    public sealed class UserEditorSettingsRecentHighlightDtoKey
    {
        #region Properties
        public Object HighlightId { get; set; }

        #endregion

        #region Methods
        public override int GetHashCode()
        {
            return HighlightId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            UserEditorSettingsRecentHighlightDtoKey other = obj as UserEditorSettingsRecentHighlightDtoKey;
            if (other != null)
            {
                if (!Equals(other.HighlightId, this.HighlightId)) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

}


       


