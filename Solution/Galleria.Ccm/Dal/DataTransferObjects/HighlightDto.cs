﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 L.Hodson
//  Created
// V8-27941 J.Pickup
//  Introduced new properties for respository. (rowversion etc).
#endregion
#region Version History: (CCM 8.1.0)
// V8-30290 : A.Probyn
//  Fixed bug with DtoKey not setup correctly. A highlight is unique based on name AND entity id
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Highlight data transfer object
    /// </summary>
    [Serializable]
    public sealed class HighlightDto
    {
        #region Properties
        public Object Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public HighlightDtoKey DtoKey
        {
            get
            {
                return new HighlightDtoKey()
                {
                    Name = this.Name,
                    EntityId = this.EntityId
                };
            }
        }
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        public String Name { get; set; }
        public Byte Type { get; set; }
        public Byte MethodType { get; set; }
        public Boolean IsFilterEnabled { get; set; }
        public Boolean IsPreFilter { get; set; }
        public Boolean IsAndFilter { get; set; }
        public String Field1 { get; set; }
        public String Field2 { get; set; }
        public Byte QuadrantXType { get; set; }
        public Single QuadrantXConstant { get; set; }
        public Byte QuadrantYType { get; set; }
        public Single QuadrantYConstant { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            HighlightDto other = obj as HighlightDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.Type != this.Type) { return false; }
                if (other.MethodType != this.MethodType) { return false; }
                if (other.IsFilterEnabled != this.IsFilterEnabled) { return false; }
                if (other.IsPreFilter != this.IsPreFilter) { return false; }
                if (other.IsAndFilter != this.IsAndFilter) { return false; }
                if (other.Field1 != this.Field1) { return false; }
                if (other.Field2 != this.Field2) { return false; }
                if (other.QuadrantXType != this.QuadrantXType) { return false; }
                if (other.QuadrantXConstant != this.QuadrantXConstant) { return false; }
                if (other.QuadrantYType != this.QuadrantYType) { return false; }
                if (other.QuadrantYConstant != this.QuadrantYConstant) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.DateDeleted != this.DateDeleted) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    /// <summary>
    /// Highlight data transfer object key
    /// </summary>
    [Serializable]
    public sealed class HighlightDtoKey
    {
        #region Properties
        public String Name { get; set; }
        public Int32 EntityId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return EntityId.GetHashCode() + Name.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the ISOme
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            HighlightDtoKey other = obj as HighlightDtoKey;
            if (other != null)
            {
                if (other.EntityId != this.EntityId) { return false; }
                if (other.Name != this.Name) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
