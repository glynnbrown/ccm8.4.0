﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class RepositoryInfoDto
    {
        #region Properties
        public Guid UniqueId { get; set; }
        public String Name { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the instance hash code
        /// </summary>
        /// <returns>The instance hash code</returns>
        public override int GetHashCode()
        {
            return this.UniqueId.GetHashCode();
        }

        /// <summary>
        /// Indicates if two instance are equal
        /// </summary>
        /// <param name="obj">The instance to compare to</param>
        /// <returns>True if equal, else false</returns>
        public override bool Equals(Object obj)
        {
            RepositoryInfoDto other = obj as RepositoryInfoDto;
            if (other != null)
            {
                if (other.UniqueId != this.UniqueId) return false;
                if (other.Name != this.Name) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
