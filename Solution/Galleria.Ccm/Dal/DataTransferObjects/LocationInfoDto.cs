﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-25628 : A.Kuszyk
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// LocationInfo Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class LocationInfoDto
    {
        #region Properties
        public Int16 Id { get; set; }
        public LocationDtoKey DtoKey
        {
            get
            {
                return new LocationDtoKey()
                {
                    EntityId = this.EntityId,
                    Code = this.Code
                };
            }
        }
        public Int32 EntityId { get; set; }
        public Int32 LocationGroupId { get; set; }
        public String Code { get; set; }
        public String Name { get; set; }
        public String Region { get; set; }
        public String County { get; set; }
        public String TVRegion { get; set; }
        public String Address1 { get; set; }
        public String Address2 { get; set; }
        public String PostalCode { get; set; }
        public String City { get; set; }
        public String Country { get; set; }
        public DateTime? DateDeleted { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            LocationInfoDto other = obj as LocationInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.LocationGroupId != this.LocationGroupId) { return false; }
                if (other.Code != this.Code) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.Region != this.Region) { return false; }
                if (other.County != this.County) { return false; }
                if (other.TVRegion != this.TVRegion) { return false; }
                if (other.Address1 != this.Address1) { return false; }
                if (other.Address2 != this.Address2) { return false; }
                if (other.PostalCode != this.PostalCode) { return false; }
                if (other.City != this.City) { return false; }
                if (other.Country != this.Country) { return false; }
                if (other.DateDeleted != this.DateDeleted) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class LocationInfoIsSetDto
    {
        #region Properties
        public Boolean IsEntityIdSet { get; set; }
        public Boolean IsLocationGroupIdSet { get; set; }
        public Boolean IsCodeSet { get; set; }
        public Boolean IsNameSet { get; set; }
        public Boolean IsRegionSet { get; set; }
        public Boolean IsCountySet { get; set; }
        public Boolean IsTVRegionSet { get; set; }
        public Boolean IsAddress1Set { get; set; }
        public Boolean IsAddress2Set { get; set; }
        public Boolean IsPostalCodeSet { get; set; }
        public Boolean IsCitySet { get; set; }
        public Boolean IsCountrySet { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public LocationInfoIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public LocationInfoIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsEntityIdSet.GetHashCode() +
                IsLocationGroupIdSet.GetHashCode() +
                IsCodeSet.GetHashCode() +
                IsNameSet.GetHashCode() +
                IsRegionSet.GetHashCode() +
                IsCountySet.GetHashCode() +
                IsTVRegionSet.GetHashCode() +
                IsAddress1Set.GetHashCode() +
                IsAddress2Set.GetHashCode() +
                IsPostalCodeSet.GetHashCode() +
                IsCitySet.GetHashCode() +
                IsCountrySet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            LocationInfoIsSetDto other = obj as LocationInfoIsSetDto;
            if (other != null)
            {
                if (other.IsEntityIdSet != this.IsEntityIdSet) { return false; }
                if (other.IsLocationGroupIdSet != this.IsLocationGroupIdSet) { return false; }
                if (other.IsCodeSet != this.IsCodeSet) { return false; }
                if (other.IsNameSet != this.IsNameSet) { return false; }
                if (other.IsRegionSet != this.IsRegionSet) { return false; }
                if (other.IsCountySet != this.IsCountySet) { return false; }
                if (other.IsTVRegionSet != this.IsTVRegionSet) { return false; }
                if (other.IsAddress1Set != this.IsAddress1Set) { return false; }
                if (other.IsAddress2Set != this.IsAddress2Set) { return false; }
                if (other.IsPostalCodeSet != this.IsPostalCodeSet) { return false; }
                if (other.IsCitySet != this.IsCitySet) { return false; }
                if (other.IsCountrySet != this.IsCountrySet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsEntityIdSet = isSet;
            IsLocationGroupIdSet = isSet;
            IsCodeSet = isSet;
            IsNameSet = isSet;
            IsRegionSet = isSet;
            IsCountySet = isSet;
            IsTVRegionSet = isSet;
            IsAddress1Set = isSet;
            IsAddress2Set = isSet;
            IsPostalCodeSet = isSet;
            IsCitySet = isSet;
            IsCountrySet = isSet;
        }
        #endregion
    }
}
