﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-25455 : J.Pickup
//  Created
// CCM-27059 : J.Pickup
//      Reviewed FK attributes & Added DtoKey, IsSetDto
// V8-26322 : A.Silva
//      Amended ProductId Property to have DeleteBehavior.Leave (parent is marked as deleted, not really deleted).

#endregion

#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// AssortmentMinorRevisionListAction Data Transfer Object
    /// </summary>
    [Serializable]
    public class AssortmentMinorRevisionListActionDto
    {
        #region Properties

        public Int32 Id { get; set; }
        [ForeignKey(typeof(AssortmentMinorRevisionDto), typeof(IAssortmentMinorRevisionDal), DeleteBehavior.Cascade)]
        public Int32 AssortmentMinorRevisionId { get; set; }
        public String ProductGtin { get; set; }
        public String ProductName { get; set; }
        [ForeignKey(typeof(ProductDto), typeof(IProductDal), DeleteBehavior.Leave)]
        public Int32? ProductId { get; set; }
        [ForeignKey(typeof(ConsumerDecisionTreeNodeDto), typeof(IConsumerDecisionTreeNodeDal), DeleteBehavior.Leave)] 
        public Int32? AssortmentConsumerDecisionTreeNodeId { get; set; }
        public Int32 Priority { get; set; }
        public String Comments { get; set; }
        public AssortmentMinorRevisionListActionDtoDtoKey DtoKey
        {
            get
            {
                return new AssortmentMinorRevisionListActionDtoDtoKey()
                {
                     AssortmentMinorRevisionId = this.AssortmentMinorRevisionId,
                     ProductGtin = this.ProductGtin
                };
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentMinorRevisionListActionDto other = obj as AssortmentMinorRevisionListActionDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.AssortmentMinorRevisionId != this.AssortmentMinorRevisionId) { return false; }
                if (other.ProductGtin != this.ProductGtin) { return false; }
                if (other.ProductName != this.ProductName) { return false; }
                if (other.ProductId != this.ProductId) { return false; }
                if (other.AssortmentConsumerDecisionTreeNodeId != this.AssortmentConsumerDecisionTreeNodeId) { return false; }
                if (other.Priority != this.Priority) { return false; }
                if (other.Comments != this.Comments) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


    [Serializable]
    public class AssortmentMinorRevisionListActionDtoDtoKey
    {
        #region Properties

        public Int32 AssortmentMinorRevisionId { get; set; }
        public String ProductGtin { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                AssortmentMinorRevisionId.GetHashCode() +
                ProductGtin.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentMinorRevisionListActionDtoDtoKey other = obj as AssortmentMinorRevisionListActionDtoDtoKey;
            if (other != null)
            {
                if (other.AssortmentMinorRevisionId != this.AssortmentMinorRevisionId) { return false; }
                if (other.ProductGtin != this.ProductGtin) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }


    [Serializable]
    public class AssortmentMinorRevisionListActionDtoIsSetDto
    {
        #region Properties

        public Boolean IsIdSet { get; set; }
        public Boolean IsAssortmentMinorRevisionIdSet{ get; set; }
        public Boolean IsProductGtinSet { get; set; }
        public Boolean IsProductNameSet { get; set; }
        public Boolean IsProductIdSet { get; set; }
        public Boolean IsAssortmentConsumerDecisionTreeNodeIdSet { get; set; }
        public Boolean IsPrioritySet { get; set; }
        public Boolean IsCommentsSet { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Default
        /// </summary>
        public AssortmentMinorRevisionListActionDtoIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public AssortmentMinorRevisionListActionDtoIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsIdSet.GetHashCode() +
                IsAssortmentMinorRevisionIdSet.GetHashCode() +
                IsProductGtinSet.GetHashCode() +
                IsProductNameSet.GetHashCode() +
                IsProductIdSet.GetHashCode() +
                IsAssortmentConsumerDecisionTreeNodeIdSet.GetHashCode() +
                IsPrioritySet.GetHashCode() + 
                IsCommentsSet.GetHashCode();

        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentMinorRevisionListActionDtoIsSetDto other = obj as AssortmentMinorRevisionListActionDtoIsSetDto;
            if (other != null)
            {
                if (other.IsIdSet != this.IsIdSet) { return false; }
                if (other.IsAssortmentMinorRevisionIdSet != this.IsAssortmentMinorRevisionIdSet) { return false; }
                if (other.IsProductGtinSet != this.IsProductGtinSet) { return false; }
                if (other.IsProductNameSet != this.IsProductNameSet) { return false; }
                if (other.IsProductIdSet != this.IsProductIdSet) { return false; }
                if (other.IsAssortmentConsumerDecisionTreeNodeIdSet != this.IsAssortmentConsumerDecisionTreeNodeIdSet) { return false; }
                if (other.IsPrioritySet != this.IsPrioritySet) { return false; }
                if (other.IsCommentsSet != this.IsCommentsSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsIdSet = isSet;
            IsAssortmentMinorRevisionIdSet = isSet;
            IsProductGtinSet = isSet;
            IsProductNameSet = isSet;
            IsProductIdSet = isSet;
            IsAssortmentConsumerDecisionTreeNodeIdSet = isSet;
            IsPrioritySet = isSet;
            IsCommentsSet = isSet;
        }
        #endregion
    }
}
