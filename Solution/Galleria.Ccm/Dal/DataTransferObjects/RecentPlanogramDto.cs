﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-V8-24261 : L.Hodson
//		Created (Auto-generated)
#endregion

#region Version History: CCM 802
// V8-27433 : A.Kuszyk
//  Added Path property.
#endregion
#endregion

using System;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// RecentPlanogram Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class RecentPlanogramDto
    {
        #region Properties
        public Object Id { get; set; }
        public RecentPlanogramDtoKey DtoKey
        {
            get
            {
                return new RecentPlanogramDtoKey()
                {
                    Id = this.Id,
                };
            }
        }
        public String Path { get; set; }
        public String Name { get; set; }
        public Byte Type { get; set; }
        public DateTime DateLastAccessed { get; set; }
        public Boolean IsPinned { get; set; }
        public String Location { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            RecentPlanogramDto other = obj as RecentPlanogramDto;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }

                if (other.Path != this.Path) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.Type != this.Type) { return false; }
                if (other.DateLastAccessed != this.DateLastAccessed) { return false; }
                if (other.IsPinned != this.IsPinned) { return false; }
                if (other.Location != this.Location) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class RecentPlanogramDtoKey
    {
        #region Properties
        public Object Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            RecentPlanogramDtoKey other = obj as RecentPlanogramDtoKey;
            if (other != null)
            {
                // Id
                if ((other.Id != null) && (this.Id != null))
                {
                    if (!other.Id.Equals(this.Id)) { return false; }
                }
                if ((other.Id != null) && (this.Id == null)) { return false; }
                if ((other.Id == null) && (this.Id != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
