﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class SyncSourceDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public String ServerName { get; set; }
        public String SiteName { get; set; }
        public Int32 PortNumber { get; set; }
        public SyncSourceDtoKey DtoKey
        {
            get
            {
                return new SyncSourceDtoKey()
                {
                    ServerName = this.ServerName,
                    PortNumber = this.PortNumber,
                    SiteName = this.SiteName
                };
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the hash code for this instance
        /// </summary>
        /// <returns>The instance hash code</returns>
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Determines if an instance is equal to this one
        /// </summary>
        /// <param name="obj">The instance to compare to</param>
        /// <returns>True if equal, else false</returns>
        public override bool Equals(object obj)
        {
            SyncSourceDto other = obj as SyncSourceDto;
            if (other != null)
            {
                if (other.Id != this.Id)
                {
                    return false;
                }
                if (other.RowVersion != this.RowVersion)
                {
                    return false;
                }
                if (other.ServerName != this.ServerName)
                {
                    return false;
                }
                if (other.SiteName != this.SiteName)
                {
                    return false;
                }
                if (other.PortNumber != this.PortNumber)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class SyncSourceDtoKey
    {
        #region Properties
        public String ServerName { get; set; }
        public String SiteName { get; set; }
        public Int32 PortNumber { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return ServerName.GetHashCode() +
                SiteName.GetHashCode() +
                PortNumber.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            SyncSourceDtoKey other = obj as SyncSourceDtoKey;
            if (other != null)
            {

                if (other.ServerName != this.ServerName) { return false; }
                if (other.SiteName != this.SiteName) { return false; }
                if (other.PortNumber != this.PortNumber) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}