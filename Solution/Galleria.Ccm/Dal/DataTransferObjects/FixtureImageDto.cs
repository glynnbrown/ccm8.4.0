﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// FixtureImage Data Transfer Object
    /// </summary>
    [Serializable]
    public class FixtureImageDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public FixtureImageDtoKey DtoKey
        {
            get
            {
                return new FixtureImageDtoKey()
                {
                    Id = this.Id,
                };
            }
        }
        public Object FixturePackageId { get; set; }
        public String FileName { get; set; }
        public String Description { get; set; }
        public Byte[] ImageData { get; set; }
        public Object ExtendedData { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FixtureImageDto other = obj as FixtureImageDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }

                // FixturePackageId
                if ((other.FixturePackageId != null) && (this.FixturePackageId != null))
                {
                    if (!other.FixturePackageId.Equals(this.FixturePackageId)) { return false; }
                }
                if ((other.FixturePackageId != null) && (this.FixturePackageId == null)) { return false; }
                if ((other.FixturePackageId == null) && (this.FixturePackageId != null)) { return false; }

                if (other.FileName != this.FileName) { return false; }
                if (other.Description != this.Description) { return false; }

                //ImageData
                if ((other.ImageData != null) && (this.ImageData == null))
                {
                    return false;
                }
                if ((other.ImageData == null) && (this.ImageData != null))
                {
                    return false;
                }
                if ((other.ImageData != null) && (this.ImageData != null) && (!other.ImageData.SequenceEqual(this.ImageData)))
                {
                    return false;
                }

                //ExtendedData
                if ((other.ExtendedData != null) && (this.ExtendedData != null))
                {
                    if (!other.ExtendedData.Equals(this.ExtendedData)) { return false; }
                }
                if ((other.ExtendedData != null) && (this.ExtendedData == null)) { return false; }
                if ((other.ExtendedData == null) && (this.ExtendedData != null)) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class FixtureImageDtoKey
    {
        #region Properties
        public Int32 Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            FixtureImageDtoKey other = obj as FixtureImageDtoKey;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
