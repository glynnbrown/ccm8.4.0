﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24801 L.Hodson
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;


namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// HighlightFilter data transfer object
    /// </summary>
    [Serializable]
    public sealed class HighlightFilterDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public HighlightFilterDtoKey DtoKey
        {
            get { return new HighlightFilterDtoKey() { Id = this.Id }; }
        }
        [ForeignKey(typeof(HighlightDto), typeof(IHighlightDal))]
        public Object HighlightId { get; set; }
        public String Field { get; set; }
        public Byte Type { get; set; }
        public String Value { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            HighlightFilterDto other = obj as HighlightFilterDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }

                //HighlightId
                if ((other.HighlightId != null) && (this.HighlightId != null))
                {
                    if (!other.HighlightId.Equals(this.HighlightId)) { return false; }
                }
                if ((other.HighlightId != null) && (this.HighlightId == null)) { return false; }
                if ((other.HighlightId == null) && (this.HighlightId != null)) { return false; }

                if (other.Field != this.Field) { return false; }
                if (other.Type != this.Type) { return false; }
                if (other.Value != this.Value) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    /// <summary>
    /// HighlightFilter data transfer object key
    /// </summary>
    [Serializable]
    public sealed class HighlightFilterDtoKey
    {
        #region Properties
        public Int32 Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the ISOme
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            HighlightFilterDtoKey other = obj as HighlightFilterDtoKey;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
