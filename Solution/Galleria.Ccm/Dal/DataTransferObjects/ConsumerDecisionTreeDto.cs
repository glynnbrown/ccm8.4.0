﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
#endregion
#region Version History: (CCM 8.0.3)
// V8-29215 : D.Pleasance
//  Removed ConsumerDecisionTree DateDeleted columns
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
// V8-29474 : L.Ineson
//  Removed ucr from dtokey.
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// ConsumerDecisionTree Data Transfer Object
    /// </summary>
    [Serializable]
    public class ConsumerDecisionTreeDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public ConsumerDecisionTreeDtoKey DtoKey
        {
            get
            {
                return new ConsumerDecisionTreeDtoKey()
                {
                    Name = this.Name,
                    EntityId = this.EntityId,
                };
            }
        }
        public Guid UniqueContentReference { get; set; }
        public String Name { get; set; }
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        [ForeignKey(typeof(ProductGroupDto), typeof(IProductGroupDal), DeleteBehavior.Leave)]
        public Int32? ProductGroupId { get; set; }
        public Byte Type { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public Guid? ParentUniqueContentReference { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ConsumerDecisionTreeDto other = obj as ConsumerDecisionTreeDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.RowVersion != this.RowVersion) { return false; }
                if (other.UniqueContentReference != this.UniqueContentReference) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
                if (other.ProductGroupId != this.ProductGroupId) { return false; }
                if (other.Type != this.Type) { return false; }
                if (other.DateCreated != this.DateCreated) { return false; }
                if (other.DateLastModified != this.DateLastModified) { return false; }
                if (other.ParentUniqueContentReference != this.ParentUniqueContentReference) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class ConsumerDecisionTreeDtoKey
    {
        #region Properties
        public String Name { get; set; }
        public Int32 EntityId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                Name.GetHashCode() +
                EntityId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ConsumerDecisionTreeDtoKey other = obj as ConsumerDecisionTreeDtoKey;
            if (other != null)
            {
                if (other.Name != this.Name) { return false; }
                if (other.EntityId != this.EntityId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
