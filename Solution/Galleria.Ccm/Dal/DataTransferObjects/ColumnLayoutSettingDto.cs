﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.

#endregion

#endregion

using System;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// User Data Transfer Object
    /// </summary>
    [Serializable]
    public class ColumnLayoutSettingDto
    {
        #region Properties

        public Int32 Id { get; set; }
        public ColumnLayoutSettingDtoKey DtoKey { get { return new ColumnLayoutSettingDtoKey() { Id = Id }; } }
        public String ScreenKey { get; set; }
        public String ColumnLayoutName { get; set; }

        #endregion

        #region Methods

        /// <summary>
        ///     Compare an object against this instance to check if they are equal.
        /// </summary>
        /// <param name="obj"><see cref="Object"/> to compare this instance against.</param>
        /// <returns>Returns <c>true</c> if both objects are equal, <c>false</c> otherwise.</returns>
        public override Boolean Equals(Object obj)
        {
            var other = obj as ColumnLayoutSettingDto;
            return other != null &&
                   other.Id == Id &&
                   other.ScreenKey == ScreenKey &&
                   other.ColumnLayoutName == ColumnLayoutName;
        }

        /// <summary>
        /// Serves as a hash function for a particular type. 
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode() + ScreenKey.GetHashCode() + ColumnLayoutName.GetHashCode();
        }

        #endregion
    }

    public class ColumnLayoutSettingDtoKey
    {
        #region Properties

        public Int32 Id { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Serves as a hash function for a particular type. 
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override Int32 GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        ///     Compare an object against this instance to check if they are equal.
        /// </summary>
        /// <param name="obj"><see cref="Object"/> to compare this instance against.</param>
        /// <returns>Returns <c>true</c> if both objects are equal, <c>false</c> otherwise.</returns>
        public override Boolean Equals(Object obj)
        {
            var other = obj as ColumnLayoutSettingDtoKey;
            return other != null &&
                   other.Id == Id;
        }

        #endregion
    }
}
