﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// ViewLayout data transfer object
    /// </summary>
    [Serializable]
    public sealed class ViewLayoutDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Boolean IsVertical { get; set; }
        #endregion

        #region Methods

        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override bool Equals(Object obj)
        {
            ViewLayoutDto other = obj as ViewLayoutDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.IsVertical != this.IsVertical) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        #endregion
    }
}
