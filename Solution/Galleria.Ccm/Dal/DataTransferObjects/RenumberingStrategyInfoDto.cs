﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    ///     Data Transfer Object for <see cref="RenumberingStrategyInfo"/>.
    /// </summary>
    [Serializable]
    public sealed class RenumberingStrategyInfoDto
    {
        #region Properties

        public Int32 EntityId { get; set; }
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public DateTime? DateDeleted { get; set; }
        
        #endregion

        #region Equality Overrides

        /// <summary>
        ///     Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        ///     <see cref="T:System.Object" />.
        /// </summary>
        /// <returns>
        ///     true if the specified <see cref="T:System.Object" /> is equal to the current <see cref="T:System.Object" />;
        ///     otherwise, false.
        /// </returns>
        /// <param name="obj">The <see cref="T:System.Object" /> to compare with the current <see cref="T:System.Object" />. </param>
        public override bool Equals(object obj)
        {
            var other = obj as RenumberingStrategyInfoDto;
            return other != null
                   && other.Id == Id
                   && other.EntityId == EntityId
                   && other.Name == Name
                   && other.DateDeleted == DateDeleted;
        }

        /// <summary>
        ///     Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        ///     A hash code for the current <see cref="T:System.Object" />.
        /// </returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode()
                   + EntityId.GetHashCode()
                   + Name.GetHashCode()
                   + DateDeleted.GetHashCode();
        }
        
        #endregion
    }
}
