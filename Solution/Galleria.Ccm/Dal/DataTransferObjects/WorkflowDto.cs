﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25546 : N.Foster
//  Created
// V8-25868 : L.Ineson
//  Added Description field, changed dtokey fields.
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public sealed class WorkflowDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateDeleted { get; set; }
        public WorkflowDtoKey DtoKey { get { return new WorkflowDtoKey() { EntityId= this.EntityId, Name = this.Name }; } }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the instance has code
        /// </summary>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Compares two instances to each other
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            WorkflowDto other = obj as WorkflowDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.RowVersion != this.RowVersion) return false;
                if (other.EntityId != this.EntityId) return false;
                if (other.Name != this.Name) return false;
                if (other.Description != this.Description) { return false; }
                if (other.DateCreated != this.DateCreated) return false;
                if (other.DateLastModified != this.DateLastModified) return false;
                if (other.DateDeleted != this.DateDeleted) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class WorkflowDtoKey
    {
        #region Properties
        public Int32 EntityId { get; set; }
        public String Name { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the hash code for this instance
        /// </summary>
        public override Int32 GetHashCode()
        {
            return HashCode.Start.Hash(this.EntityId).Hash(this.Name).GetHashCode();
        }

        /// <summary>
        /// Compares two instances to see if they are equal
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            WorkflowDtoKey other = obj as WorkflowDtoKey;
            if (other != null)
            {
                if (other.EntityId != this.EntityId) return false;
                if (other.Name != this.Name) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
