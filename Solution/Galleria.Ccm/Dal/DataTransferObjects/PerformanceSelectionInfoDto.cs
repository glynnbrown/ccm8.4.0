﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26159 : L.Ineson
//		Created (Auto-generated)
#endregion
#region Version History: (CCM 8.3.0)
// V8-31830 : A.Probyn
//  Updated to include GFSPerformanceSourceId, SelectionType, TimeType, DynamicValue
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// PerformanceSelectionInfo Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class PerformanceSelectionInfoDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Int32 GFSPerformanceSourceId { get; set; }
        public Byte TimeType { get; set; }
        public Byte SelectionType { get; set; }
        public Int32 DynamicValue { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            PerformanceSelectionInfoDto other = obj as PerformanceSelectionInfoDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.GFSPerformanceSourceId != this.GFSPerformanceSourceId) { return false; }
                if (other.TimeType != this.TimeType) { return false; }
                if (other.SelectionType != this.SelectionType) { return false; }
                if (other.DynamicValue != this.DynamicValue) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

}
