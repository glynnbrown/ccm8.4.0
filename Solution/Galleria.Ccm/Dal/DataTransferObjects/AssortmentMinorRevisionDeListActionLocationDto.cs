﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25455 : J.Pickup
//  Created
// CCM-27059 : J.Pickup
//      Reviewed FK attributes & Added DtoKey, IsSetDto
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// AssortmentMinorRevisionDeListActionLocation Data Transfer Object
    /// </summary>
    [Serializable]
    public class AssortmentMinorRevisionDeListActionLocationDto
    {
        #region Properties

        public Int32 Id { get; set; }
        [ForeignKey(typeof(AssortmentMinorRevisionDeListActionDto), typeof(IAssortmentMinorRevisionDeListActionDal), DeleteBehavior.Cascade)]
        public Int32 AssortmentMinorRevisionDeListActionId { get; set; }
        public String LocationCode { get; set; }
        public String LocationName { get; set; }
        [ForeignKey(typeof(LocationDto), typeof(ILocationDal), DeleteBehavior.Leave)]
        public Int16? LocationId { get; set; }
        public AssortmentMinorRevisionDeListActionLocationDtoKey DtoKey
        {
            get
            {
                return new AssortmentMinorRevisionDeListActionLocationDtoKey()
                {
                     AssortmentMinorRevisionDeListActionId = this.AssortmentMinorRevisionDeListActionId,
                     LocationName = this.LocationName
                };
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentMinorRevisionDeListActionLocationDto other = obj as AssortmentMinorRevisionDeListActionLocationDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.AssortmentMinorRevisionDeListActionId != this.AssortmentMinorRevisionDeListActionId) { return false; }
                if (other.LocationCode != this.LocationCode) { return false; }
                if (other.LocationName != this.LocationName) { return false; }
                if (other.LocationId != this.LocationId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentMinorRevisionDeListActionLocationDtoKey
    {
        #region Properties

        public Int32 AssortmentMinorRevisionDeListActionId { get; set; }
        public String LocationName { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                AssortmentMinorRevisionDeListActionId.GetHashCode() +
                LocationName.GetHashCode();
        }
        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentMinorRevisionDeListActionLocationDtoKey other = obj as AssortmentMinorRevisionDeListActionLocationDtoKey;
            if (other != null)
            {
                if (other.AssortmentMinorRevisionDeListActionId != this.AssortmentMinorRevisionDeListActionId) { return false; }
                if (other.LocationName != this.LocationName) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentMinorRevisionDeListActionLocationIsSetDto
    {
        #region Properties

        public Boolean IsIdSet { get; set; }
        public Boolean AssortmentMinorRevisionDeListActionId { get; set; }
        public Boolean LocationCode { get; set; }
        public Boolean LocationName { get; set; }
        public Boolean LocationId { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Default
        /// </summary>
        public AssortmentMinorRevisionDeListActionLocationIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public AssortmentMinorRevisionDeListActionLocationIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsIdSet.GetHashCode() +
                AssortmentMinorRevisionDeListActionId.GetHashCode() +
                LocationCode.GetHashCode() +
                LocationName.GetHashCode() +
                LocationId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentMinorRevisionDeListActionLocationIsSetDto other = obj as AssortmentMinorRevisionDeListActionLocationIsSetDto;
            if (other != null)
            {
                if (other.IsIdSet != this.IsIdSet) { return false; }
                if (other.AssortmentMinorRevisionDeListActionId != this.AssortmentMinorRevisionDeListActionId) { return false; }
                if (other.LocationCode != this.LocationCode) { return false; }
                if (other.LocationName != this.LocationName) { return false; }
                if (other.LocationId != this.LocationId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsIdSet = isSet;
            AssortmentMinorRevisionDeListActionId = isSet;
            LocationCode = isSet;
            LocationName = isSet;
            LocationId = isSet;
        }
        #endregion
    }
}
