﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public class LocationAttributeDto
    {
        #region Properties
        public Boolean HasRegion { get; set; }
        public Boolean HasCounty { get; set; }
        public Boolean HasTVRegion { get; set; }
        public Boolean HasAddress1 { get; set; }
        public Boolean HasAddress2 { get; set; }
        public Boolean Has24Hours { get; set; }
        public Boolean HasPetrolForecourt { get; set; }
        public Boolean HasPetrolForecourtType { get; set; }
        public Boolean HasRestaurant { get; set; }
        public Boolean HasMezzFitted { get; set; }
        public Boolean HasSizeGrossArea { get; set; }
        public Boolean HasSizeNetSalesArea { get; set; }
        public Boolean HasSizeMezzSalesArea { get; set; }
        public Boolean HasCity { get; set; }
        public Boolean HasCountry { get; set; }
        public Boolean HasManagerName { get; set; }
        public Boolean HasRegionalManagerName { get; set; }
        public Boolean HasDivisionalManagerName { get; set; }
        public Boolean HasAdvertisingZone { get; set; }
        public Boolean HasDistributionCentre { get; set; }
        public Boolean HasNewsCube { get; set; }
        public Boolean HasAtmMachines { get; set; }
        public Boolean HasNoOfCheckouts { get; set; }
        public Boolean HasCustomerWC { get; set; }
        public Boolean HasBabyChanging { get; set; }
        public Boolean HasInLocationBakery { get; set; }
        public Boolean HasHotFoodToGo { get; set; }
        public Boolean HasRotisserie { get; set; }
        public Boolean HasFishmonger { get; set; }
        public Boolean HasButcher { get; set; }
        public Boolean HasPizza { get; set; }
        public Boolean HasDeli { get; set; }
        public Boolean HasSaladBar { get; set; }
        public Boolean HasOrganic { get; set; }
        public Boolean HasGrocery { get; set; }
        public Boolean HasMobilePhones { get; set; }
        public Boolean HasDryCleaning { get; set; }
        public Boolean HasHomeShoppingAvailable { get; set; }
        public Boolean HasOptician { get; set; }
        public Boolean HasPharmacy { get; set; }
        public Boolean HasTravel { get; set; }
        public Boolean HasPhotoDepartment { get; set; }
        public Boolean HasCarServiceArea { get; set; }
        public Boolean HasGardenCentre { get; set; }
        public Boolean HasClinic { get; set; }
        public Boolean HasAlcohol { get; set; }
        public Boolean HasFashion { get; set; }
        public Boolean HasCafe { get; set; }
        public Boolean HasRecycling { get; set; }
        public Boolean HasPhotocopier { get; set; }
        public Boolean HasLottery { get; set; }
        public Boolean HasPostOffice { get; set; }
        public Boolean HasMovieRental { get; set; }
        public Boolean HasOpenMonday { get; set; }
        public Boolean HasOpenTuesday { get; set; }
        public Boolean HasOpenWednesday { get; set; }
        public Boolean HasOpenThursday { get; set; }
        public Boolean HasOpenFriday { get; set; }
        public Boolean HasOpenSaturday { get; set; }
        public Boolean HasOpenSunday { get; set; }
        public Boolean HasJewellery { get; set; }
        #endregion

        #region Methods

        public override bool Equals(object obj)
        {
            LocationAttributeDto other = obj as LocationAttributeDto;
            if (other != null)
            {
                if (other.HasRegion != this.HasRegion)
                {
                    return false;
                }
                if (other.HasCounty != this.HasCounty)
                {
                    return false;
                }
                if (other.HasTVRegion != this.HasTVRegion)
                {
                    return false;
                }
                if (other.HasAddress1 != this.HasAddress1)
                {
                    return false;
                }
                if (other.HasAddress2 != this.HasAddress2)
                {
                    return false;
                }
                if (other.Has24Hours != this.Has24Hours)
                {
                    return false;
                }
                if (other.HasPetrolForecourt != this.HasPetrolForecourt)
                {
                    return false;
                }
                if (other.HasPetrolForecourtType != this.HasPetrolForecourtType)
                {
                    return false;
                }
                if (other.HasRestaurant != this.HasRestaurant)
                {
                    return false;
                }
                if (other.HasMezzFitted != this.HasMezzFitted)
                {
                    return false;
                }
                if (other.HasSizeGrossArea != this.HasSizeGrossArea)
                {
                    return false;
                }
                if (other.HasSizeNetSalesArea != this.HasSizeNetSalesArea)
                {
                    return false;
                }
                if (other.HasSizeMezzSalesArea != this.HasSizeMezzSalesArea)
                {
                    return false;
                }
                if (other.HasCity != this.HasCity)
                {
                    return false;
                }
                if (other.HasCountry != this.HasCountry)
                {
                    return false;
                }
                if (other.HasManagerName != this.HasManagerName)
                {
                    return false;
                }
                if (other.HasRegionalManagerName != this.HasRegionalManagerName)
                {
                    return false;
                }
                if (other.HasDivisionalManagerName != this.HasDivisionalManagerName)
                {
                    return false;
                }
                if (other.HasAdvertisingZone != this.HasAdvertisingZone)
                {
                    return false;
                }
                if (other.HasDistributionCentre != this.HasDistributionCentre)
                {
                    return false;
                }
                if (other.HasNewsCube != this.HasNewsCube)
                {
                    return false;
                }
                if (other.HasAtmMachines != this.HasAtmMachines)
                {
                    return false;
                }
                if (other.HasNoOfCheckouts != this.HasNoOfCheckouts)
                {
                    return false;
                }
                if (other.HasCustomerWC != this.HasCustomerWC)
                {
                    return false;
                }
                if (other.HasBabyChanging != this.HasBabyChanging)
                {
                    return false;
                }
                if (other.HasInLocationBakery != this.HasInLocationBakery)
                {
                    return false;
                }
                if (other.HasHotFoodToGo != this.HasHotFoodToGo)
                {
                    return false;
                }
                if (other.HasRotisserie != this.HasRotisserie)
                {
                    return false;
                }
                if (other.HasFishmonger != this.HasFishmonger)
                {
                    return false;
                }
                if (other.HasButcher != this.HasButcher)
                {
                    return false;
                }
                if (other.HasPizza != this.HasPizza)
                {
                    return false;
                }
                if (other.HasDeli != this.HasDeli)
                {
                    return false;
                }
                if (other.HasSaladBar != this.HasSaladBar)
                {
                    return false;
                }
                if (other.HasOrganic != this.HasOrganic)
                {
                    return false;
                }
                if (other.HasGrocery != this.HasGrocery)
                {
                    return false;
                }
                if (other.HasMobilePhones != this.HasMobilePhones)
                {
                    return false;
                }
                if (other.HasDryCleaning != this.HasDryCleaning)
                {
                    return false;
                }
                if (other.HasHomeShoppingAvailable != this.HasHomeShoppingAvailable)
                {
                    return false;
                }
                if (other.HasOptician != this.HasOptician)
                {
                    return false;
                }
                if (other.HasPharmacy != this.HasPharmacy)
                {
                    return false;
                }
                if (other.HasTravel != this.HasTravel)
                {
                    return false;
                }
                if (other.HasPhotoDepartment != this.HasPhotoDepartment)
                {
                    return false;
                }
                if (other.HasCarServiceArea != this.HasCarServiceArea)
                {
                    return false;
                }
                if (other.HasGardenCentre != this.HasGardenCentre)
                {
                    return false;
                }
                if (other.HasClinic != this.HasClinic)
                {
                    return false;
                }
                if (other.HasAlcohol != this.HasAlcohol)
                {
                    return false;
                }
                if (other.HasFashion != this.HasFashion)
                {
                    return false;
                }
                if (other.HasCafe != this.HasCafe)
                {
                    return false;
                }
                if (other.HasRecycling != this.HasRecycling)
                {
                    return false;
                }
                if (other.HasPhotocopier != this.HasPhotocopier)
                {
                    return false;
                }
                if (other.HasLottery != this.HasLottery)
                {
                    return false;
                }
                if (other.HasPostOffice != this.HasPostOffice)
                {
                    return false;
                }
                if (other.HasMovieRental != this.HasMovieRental)
                {
                    return false;
                }
                if (other.HasOpenMonday != this.HasOpenMonday)
                {
                    return false;
                }
                if (other.HasOpenTuesday != this.HasOpenTuesday)
                {
                    return false;
                }
                if (other.HasOpenWednesday != this.HasOpenWednesday)
                {
                    return false;
                }
                if (other.HasOpenThursday != this.HasOpenThursday)
                {
                    return false;
                }
                if (other.HasOpenFriday != this.HasOpenFriday)
                {
                    return false;
                }
                if (other.HasOpenSaturday != this.HasOpenSaturday)
                {
                    return false;
                }
                if (other.HasOpenSunday != this.HasOpenSunday)
                {
                    return false;
                }
                if (other.HasJewellery != this.HasJewellery)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion
    }
}
