﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
// V8-25757 : L.Ineson
//  Removed Entity_Id link
//  Added Entity_Id link
// V8-27411 : M.Pettit
//  Added DateCompleted and UserId (owner) properties
#endregion
#region Version History: CCM810
// V8-29811 : A.Probyn
// Added PlanogramLocationType
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    [Serializable]
    public sealed class WorkpackageDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        [ForeignKey(typeof(WorkflowDto), typeof(IWorkflowDal), DeleteBehavior.Leave)]
        public Int32 WorkflowId { get; set; }
        public String Name { get; set; }
        public Byte WorkpackageType { get; set; }
        public Byte PlanogramLocationType { get; set; }
        [ForeignKey(typeof(UserDto), typeof(IUserDal), DeleteBehavior.Leave)]
        public Int32 UserId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public DateTime? DateCompleted { get; set; }
        public WorkpackageDtoKey DtoKey
        {
            get
            {
                return new WorkpackageDtoKey() { Id = this.Id };
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the instance has code
        /// </summary>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Compares two instances to each other
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            WorkpackageDto other = obj as WorkpackageDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.RowVersion != this.RowVersion) return false;
                if (other.EntityId != this.EntityId) return false;
                if (other.WorkflowId != this.WorkflowId) return false;
                if (other.Name != this.Name) return false;
                if (other.WorkpackageType != this.WorkpackageType) return false;
                if (other.PlanogramLocationType != this.PlanogramLocationType) return false;
                if (other.UserId != this.UserId) return false;
                if (other.DateCreated != this.DateCreated) return false;
                if (other.DateLastModified != this.DateLastModified) return false;
                if (other.DateCompleted != this.DateCompleted) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public sealed class WorkpackageDtoKey
    {
        #region Properties
        public Int32 Id { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the hash code for this instance
        /// </summary>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Compares two instances to see if they are equal
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            WorkpackageDtoKey other = obj as WorkpackageDtoKey;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
