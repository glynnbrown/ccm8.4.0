﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)

// CCM-25454 : J.Pickup
//		Created (Auto-generated)
// V8-26322 : A.Silva
//      Amended RegionalProductId and PrimaryProductId Properties to have DeleteBehavior.Leave (parent is marked as deleted, not really deleted).

#endregion

#endregion

using System;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// AssortmentRegionProduct Data Transfer Object
    /// </summary>
    [Serializable]
    public sealed class AssortmentRegionProductDto
    {
        #region Properties
        public Int32 Id { get; set; }

        public AssortmentRegionProductDtoKey DtoKey
        {
            get
            {
                return new AssortmentRegionProductDtoKey()
                {
                    RegionalProductId = this.RegionalProductId,
                    PrimaryProductId = this.PrimaryProductId
                };
            }
        }

        [ForeignKey(typeof(AssortmentRegionDto), typeof(IAssortmentRegionDal), DeleteBehavior.Cascade)]
        public Int32 AssortmentRegionId { get; set; }

        [ForeignKey(typeof(ProductDto), typeof(IProductDal), DeleteBehavior.Leave)]
        public Int32? RegionalProductId { get; set; }

        [ForeignKey(typeof(ProductDto), typeof(IProductDal), DeleteBehavior.Leave)]
        public Int32 PrimaryProductId { get; set; }

        [InheritedProperty(typeof(ProductDto), typeof(IProductDal), ForeignKeyPropertyName = "PrimaryProductId", ParentPropertyName = "Gtin")]
        public String PrimaryProductGTIN { get; set; }

        [InheritedProperty(typeof(ProductDto), typeof(IProductDal), ForeignKeyPropertyName = "PrimaryProductId", ParentPropertyName = "Name")]
        public String PrimaryProductDescription { get; set; }

        [InheritedProperty(typeof(ProductDto), typeof(IProductDal), ForeignKeyPropertyName = "PrimaryProductId", ParentPropertyName = "Gtin")]
        public String RegionalProductGTIN { get; set; }

        [InheritedProperty(typeof(ProductDto), typeof(IProductDal), ForeignKeyPropertyName = "PrimaryProductId", ParentPropertyName = "Name")]
        public String RegionalProductDescription { get; set; }

        [InheritedProperty(typeof(AssortmentRegionDto), typeof(IAssortmentRegionDal), ForeignKeyPropertyName = "AssortmentRegionId", ParentPropertyName = "Name")]
        public String RegionName { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentRegionProductDto other = obj as AssortmentRegionProductDto;
            if (other != null)
            {
                if (other.Id != this.Id) return false;
                if (other.RegionalProductId != this.RegionalProductId) return false;
                if (other.PrimaryProductId != this.PrimaryProductId) return false;
                if (other.PrimaryProductGTIN != this.PrimaryProductGTIN) return false;
                if (other.PrimaryProductDescription != this.PrimaryProductDescription) return false;
                if (other.RegionalProductGTIN != this.RegionalProductGTIN) return false;
                if (other.RegionalProductDescription != this.RegionalProductDescription) return false;
                if (other.RegionName != this.RegionName) return false;
                if (other.AssortmentRegionId != this.AssortmentRegionId) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentRegionProductDtoKey
    {
        #region Properties
        public Int32? RegionalProductId { get; set; }
        public Int32 PrimaryProductId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                 RegionalProductId.GetHashCode() +
                 PrimaryProductId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentRegionProductDtoKey other = obj as AssortmentRegionProductDtoKey;
            if (other != null)
            {
                if (other.RegionalProductId != this.RegionalProductId) return false;
                if (other.PrimaryProductId != this.PrimaryProductId) return false;
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class AssortmentRegionProductIsSetDto
    {
        #region Properties
        public Boolean IsRegionalProductIdSet { get; set; }
        public Boolean IsPrimaryProductIdSet { get; set; }
        public Boolean IsPrimaryProductGtinSet { get; set; }
        public Boolean IsPrimaryProductDescriptionSet { get; set; }
        public Boolean IsRegionalProductGtinSet { get; set; }
        public Boolean IsRegionalProductDescriptionSet { get; set; }
        public Boolean IsRegionNameSet { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public AssortmentRegionProductIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public AssortmentRegionProductIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                IsRegionalProductIdSet.GetHashCode() +
                IsPrimaryProductIdSet.GetHashCode() +
                IsPrimaryProductGtinSet.GetHashCode() +
                IsPrimaryProductDescriptionSet.GetHashCode() +
                IsRegionalProductGtinSet.GetHashCode() +
                IsRegionalProductDescriptionSet.GetHashCode() +
                IsRegionNameSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            AssortmentRegionProductIsSetDto other = obj as AssortmentRegionProductIsSetDto;
            if (other != null)
            {

                if (other.IsRegionalProductIdSet != this.IsRegionalProductIdSet) return false;
                if (other.IsPrimaryProductIdSet != this.IsPrimaryProductIdSet) return false;
                if (other.IsPrimaryProductGtinSet != this.IsPrimaryProductGtinSet) return false;
                if (other.IsPrimaryProductDescriptionSet != this.IsPrimaryProductDescriptionSet) return false;
                if (other.IsRegionalProductGtinSet != this.IsRegionalProductGtinSet) return false;
                if (other.IsRegionalProductDescriptionSet != this.IsRegionalProductDescriptionSet) return false;
                if (other.IsRegionNameSet != this.IsRegionNameSet) return false;
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {

            IsRegionalProductIdSet = isSet;
            IsPrimaryProductIdSet = isSet;
            IsPrimaryProductGtinSet = isSet;
            IsPrimaryProductDescriptionSet = isSet;
            IsRegionalProductGtinSet = isSet;
            IsRegionalProductDescriptionSet = isSet;
            IsRegionNameSet = isSet;
        }
        #endregion
    }
}
