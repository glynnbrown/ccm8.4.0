﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// ClusterLocation Data Transfer Object
    /// </summary>
    [Serializable]
    public class ClusterLocationDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public ClusterLocationDtoKey DtoKey
        {
            get
            {
                return new ClusterLocationDtoKey()
                {
                    ClusterId = this.ClusterId,
                    LocationId = this.LocationId
                };
            }
        }
        [ForeignKey(typeof(ClusterDto), typeof(IClusterDal), DeleteBehavior.Cascade)]
        public Int32 ClusterId { get; set; }
        [ForeignKey(typeof(LocationDto), typeof(ILocationDal), DeleteBehavior.Leave)]
        public Int16 LocationId { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal))]
        public String Address1 { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal))]
        public String Address2 { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal))]
        public String City { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal))]
        public String Code { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal))]
        public String Country { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal))]
        public String County { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal))]
        public String Name { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal))]
        public String PostalCode { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal))]
        public String Region { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal))]
        public String TVRegion { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ClusterLocationDto other = obj as ClusterLocationDto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }
                if (other.ClusterId != this.ClusterId) { return false; }
                if (other.LocationId != this.LocationId) { return false; }
                if (other.Address1 != this.Address1) { return false; }
                if (other.Address2 != this.Address2) { return false; }
                if (other.City != this.City) { return false; }
                if (other.Code != this.Code) { return false; }
                if (other.Country != this.Country) { return false; }
                if (other.County != this.County) { return false; }
                if (other.Name != this.Name) { return false; }
                if (other.PostalCode != this.PostalCode) { return false; }
                if (other.Region != this.Region) { return false; }
                if (other.TVRegion != this.TVRegion) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class ClusterLocationDtoKey
    {
        #region Properties
        public Int32 ClusterId { get; set; }
        public Int16 LocationId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                 ClusterId.GetHashCode() +
                 LocationId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ClusterLocationDtoKey other = obj as ClusterLocationDtoKey;
            if (other != null)
            {

                if (other.ClusterId != this.ClusterId) { return false; }
                if (other.LocationId != this.LocationId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class ClusterLocationIsSetDto
    {
        #region Properties
        public Boolean IsClusterIdSet { get; set; }
        public Boolean IsLocationIdSet { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Default
        /// </summary>
        public ClusterLocationIsSetDto() { }

        /// <summary>
        /// Create a new instance with all fields set to 
        /// the passed in Boolean value
        /// </summary>
        /// <param name="initialSet">Boolean value to set all fields</param>
        public ClusterLocationIsSetDto(Boolean initialSet)
        {
            SetAllFieldsToBoolean(initialSet);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                 IsClusterIdSet.GetHashCode() +
                 IsLocationIdSet.GetHashCode();
        }

        /// <summary>
        /// Check to see if two isSet objects are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            ClusterLocationIsSetDto other = obj as ClusterLocationIsSetDto;
            if (other != null)
            {

                if (other.IsClusterIdSet != this.IsClusterIdSet) { return false; }
                if (other.IsLocationIdSet != this.IsLocationIdSet) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets all fields to the passed in boolean value
        /// </summary>
        /// <param name="isSet">Boolean value to set all fields</param>
        public void SetAllFieldsToBoolean(Boolean isSet)
        {
            IsClusterIdSet = isSet;
            IsLocationIdSet = isSet;
        }
        #endregion
    }
}
