﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#region Version History: (CCM 803)
// V8-29223 : D.Pleasance
//      Removed DateDeleted
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion 
#endregion
using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Dal.DataTransferObjects
{
    /// <summary>
    /// Location Space Data Transfer Object
    /// </summary>
    [Serializable]
    public class LocationSpaceDto
    {
        #region Properties
        public Int32 Id { get; set; }
        public RowVersion RowVersion { get; set; }
        public Guid UniqueContentReference { get; set; }
        public LocationSpaceDtoKey DtoKey
        {
            get
            {
                return new LocationSpaceDtoKey()
                {
                    LocationId = this.LocationId
                };
            }
        }
        [ForeignKey(typeof(LocationDto), typeof(ILocationDal), DeleteBehavior.Leave)]
        public Int16 LocationId { get; set; }
        [InheritedProperty(typeof(LocationDto), typeof(ILocationDal))]
        [ForeignKey(typeof(EntityDto), typeof(IEntityDal), DeleteBehavior.Leave)]
        public Int32 EntityId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }
        public Guid? ParentUniqueContentReference { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the objects hash code
        /// </summary>
        /// <returns>The objects hash code</returns>
        public override Int32 GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returnstrue if objects are equal</returns>
        public override bool Equals(object obj)
        {
            LocationSpaceDto other = obj as LocationSpaceDto;
            if (other != null)
            {
                //check each parameter
                if (other.Id != this.Id)
                {
                    return false;
                }
                if (other.UniqueContentReference != this.UniqueContentReference)
                {
                    return false;
                }
                if (other.LocationId != this.LocationId)
                {
                    return false;
                }
                if (other.RowVersion != this.RowVersion)
                {
                    return false;
                }
                if (other.EntityId != this.EntityId)
                {
                    return false;
                }
                if (other.DateCreated != this.DateCreated)
                {
                    return false;
                }
                if (other.DateLastModified != this.DateLastModified)
                {
                    return false;
                }
                if (other.ParentUniqueContentReference != this.ParentUniqueContentReference) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    [Serializable]
    public class LocationSpaceDtoKey
    {
        #region Properties
        public Int32 LocationId { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return
                LocationId.GetHashCode();
        }

        /// <summary>
        /// Check to see if two keys are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(object obj)
        {
            LocationSpaceDtoKey other = obj as LocationSpaceDtoKey;
            if (other != null)
            {
                if (other.LocationId != this.LocationId) { return false; }
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion
    }

    /// <summary>
    /// Code Criteria for use when searching
    /// </summary>
    [Serializable]
    public struct LocationSpaceSearchCriteriaDto
    {
        public Int32 LocationSpaceId { get; set; }
        public String LocationCode { get; set; }
        public String ProductGroupCode { get; set; }

        public LocationSpaceSearchCriteriaDto(
            String locationCode,
            String productGroupCode)
            : this()
        {
            LocationCode = locationCode;
            ProductGroupCode = productGroupCode;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + this.LocationCode.ToLowerInvariant().GetHashCode();
                hash = hash * 23 + this.ProductGroupCode.ToLowerInvariant().GetHashCode();
                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            LocationSpaceSearchCriteriaDto other = (LocationSpaceSearchCriteriaDto)obj;

            //Dont compare id as this is only used in the Location Space import
            //for lookup purposes
            StringComparer s = StringComparer.OrdinalIgnoreCase;

            if (s.Compare(other.LocationCode, this.LocationCode) != 0) { return false; }
            if (s.Compare(other.ProductGroupCode, this.ProductGroupCode) != 0) { return false; }

            return true;
        }

        public bool Equals(LocationSpaceSearchCriteriaDto other)
        {
            StringComparer s = StringComparer.OrdinalIgnoreCase;

            if (s.Compare(other.LocationCode, this.LocationCode) != 0) { return false; }
            if (s.Compare(other.ProductGroupCode, this.ProductGroupCode) != 0) { return false; }

            return true;
        }
    }
}
