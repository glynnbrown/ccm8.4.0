﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27732 : L.Ineson
//  Copied from GFS
#endregion
#endregion

using System;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Editor.Client.Wpf.Common;

namespace Galleria.Ccm.Editor.Client.Wpf.Startup
{
    public enum ParticipateCEIP
    {
        Yes,
        No
    }

    public class CEIPViewModel : ViewModelAttachedControlObject<CEIPWindow>
    {

        #region Fields

        private UserSystemSetting _currentSettings;

        private Boolean _isShowCEIPWindowNextTime;
        private ParticipateCEIP _participate = ParticipateCEIP.Yes;

        #endregion

        #region Binding PropertyPaths

        public static PropertyPath IsShowCEIPWindowNextTimeProperty = WpfHelper.GetPropertyPath<CEIPViewModel>(p => p.IsShowCEIPWindowNextTime);
        public static PropertyPath ParticipateProperty = WpfHelper.GetPropertyPath<CEIPViewModel>(p => p.Participate);

        #endregion

        #region properties

        /// <summary>
        /// Returns/Sets the user's choice for showing CEIP window next time.
        /// </summary>
        public Boolean IsShowCEIPWindowNextTime
        {
            get { return _isShowCEIPWindowNextTime; }
            set
            {
                _isShowCEIPWindowNextTime = value;
                OnPropertyChanged(IsShowCEIPWindowNextTimeProperty);
            }
        }

        /// <summary>
        /// Returns/Sets the user's choice for sending gibraltar information to Galleria.
        /// </summary>
        public ParticipateCEIP Participate
        {
            get { return _participate; }
            set
            {
                _participate = value;
                OnPropertyChanged(ParticipateProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public CEIPViewModel(UserSystemSetting systemSettings)
        {
            _currentSettings = systemSettings;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validates if CEIP window required
        /// </summary>
        /// <returns>True if CEIP window required, else false</returns>
        public Boolean CEIPWindowRequired()
        {
            Boolean showCEIPWindow = false;

            showCEIPWindow = _currentSettings.DisplayCEIPWindow;

            // Load the CEIP window settings for gilbraltar
            CEIPWindowLoadSettings();

            return showCEIPWindow;
        }

        /// <summary>
        /// Validates if security window required
        /// </summary>
        /// <returns></returns>
        public void CEIPWindowLoadSettings()
        {
            if (_currentSettings.SendCEIPInformation == true)
            {
                Participate = ParticipateCEIP.Yes;
            }
            else
            {
                Participate = ParticipateCEIP.No;
            }
        }

        /// <summary>
        /// Save the user settings to the ISOUsers.xml file
        /// </summary>
        /// <returns></returns>
        public void SaveUserSettings()
        {
            _currentSettings.SendCEIPInformation = IsParticipate();

            _currentSettings.DisplayCEIPWindow = !IsShowCEIPWindowNextTime;

            try
            {
                _currentSettings = _currentSettings.Save();
            }
            catch (Exception ex)
            {
                LocalHelper.RecordException(ex.GetBaseException(), "CEIPWindow");
            }
        }

        /// <summary>
        /// Save the user settings to the ISOUsers.xml file
        /// </summary>
        /// <returns></returns>
        public Boolean IsParticipate()
        {
            Boolean result = false;

            if (Participate == ParticipateCEIP.Yes)
            {
                result = true;
            }

            return result;
        }

        #endregion

        #region IDisposable

        /// <summary>
        /// Clears up on dispose.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(Boolean disposing)
        {
            if (!this.IsDisposed)
            {
                if (disposing)
                {
                    this.AttachedControl = null;
                }
                this.IsDisposed = true;
            }
        }

        #endregion

    }
}
