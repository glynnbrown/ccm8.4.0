﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)

// CCM-25645 : A.Silva
//  Created

#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Threading;

namespace Galleria.Ccm.Editor.Client.Wpf.Startup
{
    /// <summary>
    ///     Interaction logic for DbConnectionWizardWindow.xaml
    /// </summary>
    public partial class DbConnectionWizardWindow
    {
        #region Properties

        #region ViewModel property

        /// <summary>
        ///     Viewmodel controller for the <see cref="DbConnectionWizardWindow"/>.
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(DbConnectionWizardViewModel), typeof(DbConnectionWizardWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        ///     Gets or sets the viewmodel controller for the <see cref="DbConnectionWizardWindow"/>.
        /// </summary>
        public DbConnectionWizardViewModel ViewModel
        {
            get { return (DbConnectionWizardViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var dbConnectionWizardWindow = (DbConnectionWizardWindow)obj;
      
            var oldViewModel = e.OldValue as DbConnectionWizardViewModel;
            if (oldViewModel != null) oldViewModel.AttachedControl = null;

            var newViewModel = e.NewValue as DbConnectionWizardViewModel;
            if (newViewModel != null) newViewModel.AttachedControl = dbConnectionWizardWindow;
        }

        #endregion

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="DbConnectionWizardWindow"/> class.
        /// </summary>
        public DbConnectionWizardWindow()
        {
            InitializeComponent();

            HelpFileKeys.SetHelpFile(this, HelpFileKeys.DatabaseConnectionWindow);

            ViewModel = new DbConnectionWizardViewModel();
        }

        #endregion

        #region Window close

        /// <summary>
        ///     Raises the <see cref="E:System.Windows.Window.Closed" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            if (ViewModel.CurrentStep == DbConnectionWizardViewModel.DbConnectionStep.EntitySelection) 
            {
                ViewModel.CancelCommand.Execute();
            }

            Dispatcher.BeginInvoke((Action)(DisposeViewModel), DispatcherPriority.Background);
        }

        private void DisposeViewModel()
        {
            ViewModel.Dispose();
            ViewModel = null;
        }

        #endregion
    }
}