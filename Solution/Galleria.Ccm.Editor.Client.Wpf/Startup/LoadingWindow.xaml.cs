﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24537 : L.Hodson 
//  Created
// V8-25720 : A.Silva
//  Added silent auto-connect at startup.
// V8-26396 : L.Ineson
//  Updated silent auto connect so that it no longer shows an error
//  dialog when the user has no db connection.
// V8-26410 : J.Pickup
//  The product date is now rendered in code as opposed to being provided on the background graphic
// V8-27957 : N.Foster
//  Implement CCM security
// V8-27732 : L.Ineson
//  Moved the licensing check to here.
#endregion
#region Version History: (CCM 8.1.0)
// V8-29953 : M.Pettit
//      Updated product date
#endregion
#endregion

using System;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows;
using Csla;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Editor.Client.Wpf.Startup
{
    /// <summary>
    /// Splash screen code behind.
    /// </summary>
    public sealed partial class LoadingWindow
    {
        #region Fields
        private const Int32 _totalSteps = 4;
        private Int32 _currentStep;
        private const String _statusStringFormat = "{2} ..";
        private const String _productDate = "2016";

        private Int32 _entityId;
        #endregion

        #region Properties

        #region Status Text Property
        public static readonly DependencyProperty StatusTextProperty =
            DependencyProperty.Register("StatusText", typeof(String), typeof(LoadingWindow));

        public String StatusText
        {
            get { return (String)GetValue(StatusTextProperty); }
            private set { SetValue(StatusTextProperty, value); }
        }
        #endregion

        #region Build Version Property
        public static readonly DependencyProperty BuildVersionProperty =
            DependencyProperty.Register("BuildVersion", typeof(String), typeof(LoadingWindow));

        public String BuildVersion
        {
            get { return (String)GetValue(BuildVersionProperty); }
            private set { SetValue(BuildVersionProperty, value); }
        }
        #endregion

        #region Product Date Property

        public String ProductDate
        {
            get { return _productDate; }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public LoadingWindow()
        {
            InitializeComponent();

            // stop this window showing in the taskbar
            this.ShowInTaskbar = false;

            // set the build version
            this.BuildVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();

            // start the loading process
            this.Loaded += new RoutedEventHandler(LoadingWindow_Loaded);

        }

        /// <summary>
        /// Carries out initial loaded actions.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadingWindow_Loaded(Object sender, RoutedEventArgs e)
        {
            this.Loaded -= LoadingWindow_Loaded;

            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    ViewState state = App.ViewState;
                    _currentStep = 1;
                    LoadStep(_currentStep);

                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Controls the load order
        /// </summary>
        /// <param name="stepNo"></param>
        /// <remarks> Only load items necessary to startup here. Otherwise we risk filling 
        /// up the user's memory with objects they might not use.</remarks>
        private void LoadStep(Int32 stepNo)
        {
            switch (stepNo)
            {
                case 1:
                    CheckLicense();
                    break;

                case 2:
                    AttemptConnectionToRepository();
                    break;

                case 3:
                    LoadSettings();
                    break;

                case 4:
                    LoadApplication();
                    break;
            }
        }


        #region 1) Check License

        /// <summary>
        /// Checks that the app is correctly licensed.
        /// </summary>
        private void CheckLicense()
        {
            // set the status text
            this.StatusText = String.Format(CultureInfo.InvariantCulture, _statusStringFormat, _currentStep, _totalSteps, Message.LoadingWindow_CheckLicense);

            // have to queue the action so the status text gets chance to render
            Galleria.Framework.Controls.Wpf.Helpers.QueueAction(() =>
            {
               Boolean isLiscensed = ((Galleria.Ccm.Editor.Client.Wpf.App)App.Current).CheckLicense();

               if (isLiscensed)
               {
                   // move to the next step
                   _currentStep++;
                   LoadStep(_currentStep);
               }
            });
        }


        #endregion

        #region 2) AttemptConnectionToRepository
        /// <summary>
        /// Attempts a connection to the repository
        /// </summary>
        private void AttemptConnectionToRepository()
        {
            // set the status text
            this.StatusText = String.Format(CultureInfo.InvariantCulture, _statusStringFormat, _currentStep, _totalSteps, Message.LoadingWindow_AttemptConnectionToRepository);

            // have to queue the action so the status text gets chance to render
            Galleria.Framework.Controls.Wpf.Helpers.QueueAction(() =>
            {
                // ensure that we are logged out
                DomainPrincipal.Logout();

                // get the server and database details from the user settings
                String serverName = null;
                String databaseName = null;
                UserSystemSetting settings = UserSystemSetting.FetchUserSettings();
                Connection currentConnection = settings.Connections.FirstOrDefault(connection => connection.IsCurrentConnection);
                if (currentConnection != null)
                {
                    serverName = currentConnection.ServerName;
                    databaseName = currentConnection.DatabaseName;
                    _entityId = currentConnection.BusinessEntityId;
                }

                // enure that we have some details
                if (!String.IsNullOrEmpty(serverName) && !String.IsNullOrEmpty(databaseName))
                {
                    // set the repository connection details
                    Boolean upgradeRequired;
                    String dbInvalidReason;
                    App.ViewState.SetRepositoryConnection(serverName, databaseName, out upgradeRequired, out dbInvalidReason);

                    // attempt to authenticate against the repository
                    DomainPrincipal.BeginAuthentication(OnAuthenticationComplete);
                }
                else
                {
                    _currentStep++;
                    LoadStep(_currentStep);
                }
            });
        }

        /// <summary>
        /// Called when the authentication process is complete
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">The event arguments</param>
        private void OnAuthenticationComplete(Object sender, DomainAuthenticationResultEventArgs e)
        {
            // determine if we are connection to the repository
            Boolean isConnectedToRepository = (e.Exception == null && ApplicationContext.User.Identity.IsAuthenticated);
            if (isConnectedToRepository)
            {
                // get the entity details from the repository
                EntityInfoList entities = EntityInfoList.FetchAllEntityInfos();
                isConnectedToRepository = (entities.Count > 0);
                if (isConnectedToRepository)
                {
                    EntityInfo selectedEntity = entities.FirstOrDefault(f => f.Id == _entityId);
                    if (selectedEntity == null)
                    {
                        selectedEntity = entities.FirstOrDefault();
                    }
                    App.ViewState.EntityId = selectedEntity.Id;
                }
            }

            // if we are not connected to the repository
            // then clear the repository connection
            if (!isConnectedToRepository)
            {
                App.ViewState.ClearRepositoryConnection();
            }

            // move to the next step
            _currentStep++;
            LoadStep(_currentStep);
        }

        #endregion

        #region 3) LoadSettings

        private void LoadSettings()
        {
            _currentStep++;
            this.StatusText = String.Format(CultureInfo.InvariantCulture, _statusStringFormat, _currentStep, _totalSteps, Message.LoadingWindow_Application);

            //have to queue the action so the status text gets chance to render
            Galleria.Framework.Controls.Wpf.Helpers.QueueAction(
                () =>
                {
                    App.ViewState.Settings.CreateDirectories();
                    LoadStep(_currentStep);
                });
        }

        #endregion

        #region 4) Load Application

        private void LoadApplication()
        {
            _currentStep++;

            this.StatusText = String.Format(CultureInfo.InvariantCulture, _statusStringFormat, _currentStep, _totalSteps, Message.LoadingWindow_Application);

            //have to queue the action so the status text gets chance to render
            Galleria.Framework.Controls.Wpf.Helpers.QueueAction(
                () =>
                {
                    this.StatusText = String.Empty;

                    MainPageOrganiser mainPageOrganiser = new MainPageOrganiser();
                    mainPageOrganiser.Activated += new EventHandler(MainPageOrganiser_Activated);
                    mainPageOrganiser.Show();
                });
        }

        /// <summary>
        /// Event handler for the main page organiser becoming activated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainPageOrganiser_Activated(Object sender, EventArgs e)
        {
            MainPageOrganiser mainPageOrganiser = (MainPageOrganiser)sender;
            mainPageOrganiser.Activated -= MainPageOrganiser_Activated;

            App.Current.MainWindow = mainPageOrganiser;
            App.ProcessArgs();

            //set the shutdown mode back to last window close.
            ((Galleria.Ccm.Editor.Client.Wpf.App)App.Current).ShutdownMode = ShutdownMode.OnMainWindowClose;
            
            this.Close();
        }

        #endregion

        #endregion

    }
}
