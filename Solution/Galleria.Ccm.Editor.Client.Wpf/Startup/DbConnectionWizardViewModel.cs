﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)

// V8-25645 : A.Silva ~ Created
// V8-25720 : A.Silva ~ Corrected a defect where AttachedControl might be made to Close without having a reference.
// V8-26064 : A.Silva ~ ViewState.EntityId gets set when authentication is complete.
// V8-27138 : L.Luong
//   Added Prompt when user disconnects from repository
// V8-27151 : M.Shelley
//  Reset the ViewState IsConnectedToRepository property when a user has no rights to view the database, to prevent 
//  trying to open the database later which causes an authentication exception. 
// V8-28018 : L.ineson
// SetRepositoryConnection now returns a true or false rather than deliberately throwing an exception.
// V8-26927 : A.Kuszyk
//  Changed the key for the Cancel command from Return to Escape.
// V8-27950 : J.Pickup
//  Database validity now assessed. (IsUpgradeRequired).
// V8-28028 : A.Kuszyk
//  Added null reference check to EndAttemptConnection.
// V8-28600 : A.Kuszyk
//  Ensured _currentConnection is assigned to a new connection if no user settings file exists.
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Windows;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.Startup
{
    public sealed class DbConnectionWizardViewModel : ViewModelAttachedControlObject<DbConnectionWizardWindow>
    {
        #region Fields

        private Connection _currentConnection = Connection.NewConnection();
        private UserSystemSetting _currentSettings;
        private Boolean _isClosing;

        #endregion

        #region Property Bindings

        public static readonly PropertyPath CurrentStepProperty = GetPropertyPath(o => o.CurrentStep);

        public static readonly PropertyPath IsDatabaseAvailableProperty = GetPropertyPath(o => o.IsDatabaseAvailable);

        public static readonly PropertyPath IsEntitySelectionRememberedProperty = GetPropertyPath(o => o.IsEntitySelectionRemembered);

        /// <summary>
        ///     Holds the path for the IsNextCommandVisible property.
        /// </summary>
        public static readonly PropertyPath IsNextCommandVisibleProperty = GetPropertyPath(o => o.IsNextCommandVisible);

        public static readonly PropertyPath IsRetryingConnectionProperty = GetPropertyPath(o => o.IsRetryingConnection);

        public static readonly PropertyPath MasterEntityListProperty = GetPropertyPath(o => o.MasterEntityList);

        public static readonly PropertyPath NewDatabaseNameProperty = GetPropertyPath(o => o.NewDatabaseName);

        public static readonly PropertyPath NewServerNameProperty = GetPropertyPath(o => o.NewServerName);

        public static readonly PropertyPath ConnectionTypeProperty = GetPropertyPath(o => o.ConnectionType);

        public static readonly PropertyPath SelectedEntityProperty = GetPropertyPath(o => o.SelectedEntity);

        public static readonly PropertyPath ServerNameProperty = GetPropertyPath(o => o.ServerName);

        public static readonly PropertyPath DatabaseNameProperty = GetPropertyPath(o => o.DatabaseName);

        public static readonly PropertyPath WindowTitleProperty = GetPropertyPath(o => o.WindowTitle);

        public static readonly PropertyPath DatabaseInvalidReasonProperty = GetPropertyPath(o => o.DatabaseInvalidReason);

        #endregion

        #region Command Bindings

        public static readonly PropertyPath BackCommandProperty = GetPropertyPath(o => o.BackCommand);

        public static readonly PropertyPath CancelCommandProperty = GetPropertyPath(o => o.CancelCommand);

        public static readonly PropertyPath NextCommandProperty = GetPropertyPath(o => o.NextCommand);

        #endregion

        #region Properties

        #region CurrentStep property

        private DbConnectionStep _currentStep;

        /// <summary>
        ///     Gets or sets the current step in the wizard.
        /// </summary>
        public DbConnectionStep CurrentStep
        {
            get { return _currentStep; }
            set
            {
                _currentStep = value;
                OnPropertyChanged(CurrentStepProperty);
                OnPropertyChanged(WindowTitleProperty);
                OnPropertyChanged(IsNextCommandVisibleProperty);

                OnCurrentStepChanged(value);
            }
        }

        #endregion

        #region IsDatabaseAvailable property

        private Boolean _isDatabaseAvailable;

        /// <summary>
        ///     Gets or sets whether to database is available or not.
        /// </summary>
        public Boolean IsDatabaseAvailable
        {
            get { return _isDatabaseAvailable; }
            set
            {
                _isDatabaseAvailable = value;
                OnPropertyChanged(IsDatabaseAvailableProperty);
            }
        }

        #endregion

        #region IsEntitySelectionRemembered property

        private Boolean _isEntitySelectionRemembered;

        /// <summary>
        ///     Gets or sets wether to remember the user's entity selection or not.
        /// </summary>
        public Boolean IsEntitySelectionRemembered
        {
            get { return _isEntitySelectionRemembered; }
            set
            {
                _isEntitySelectionRemembered = value;
                OnPropertyChanged(IsEntitySelectionRememberedProperty);
            }
        }

        #endregion

        #region IsNextCommandVisible property

        /// <summary>
        ///     Gets whether the Next button is visible or not.
        /// </summary>
        /// <remarks>By default the Next button is hidden, it needs to be made visible explicitly.</remarks>
        public Boolean IsNextCommandVisible
        {
            get
            {
                if (NextCommand != null) NextCommand.RaiseCanExecuteChanged();
                switch (CurrentStep)
                {
                    case DbConnectionStep.CurrentConnection:
                        return !_isClosing;
                    case DbConnectionStep.SqlEnterDetails:
                    case DbConnectionStep.ConnectionFailed:
                    case DbConnectionStep.ErrorOccured:
                    case DbConnectionStep.EntitySelection:
                        return true;
                    default:
                        return false;
                }
            }
        }

        #endregion

        #region IsRetryingConnection property

        private Boolean _isRetryingConnection;

        /// <summary>
        ///     Gets or sets wether to database is available or not.
        /// </summary>
        public Boolean IsRetryingConnection
        {
            get { return _isRetryingConnection; }
            set
            {
                _isRetryingConnection = value;
                OnPropertyChanged(IsRetryingConnectionProperty);
            }
        }

        #endregion

        #region MasterEntityList

        private EntityInfoList _masterEntityList;

        public EntityInfoList MasterEntityList
        {
            get { return _masterEntityList; }
            set
            {
                _masterEntityList = value;
                OnPropertyChanged(MasterEntityListProperty);
            }
        }

        #endregion

        #region NewDatabaseName property

        private string _newDatabaseName;

        /// <summary>
        ///     Gets or sets the name of the new database the user wants to use.
        /// </summary>
        public String NewDatabaseName
        {
            get { return _newDatabaseName; }
            set
            {
                _newDatabaseName = value;
                OnPropertyChanged(NewDatabaseNameProperty);
            }
        }

        #endregion

        #region NewServerName property

        private string _newServerName;

        /// <summary>
        ///     Gets or sets the name of the new server the user wants to use.
        /// </summary>
        public String NewServerName
        {
            get { return _newServerName; }
            set
            {
                _newServerName = value;
                OnPropertyChanged(NewServerNameProperty);
            }
        }

        #endregion

        #region ConnectionTypeProperty

        private DbConnectionType _connectionType;

        /// <summary>
        ///     Gets or sets the current connection type.
        /// </summary>
        public DbConnectionType ConnectionType
        {
            get
            {
                return _connectionType;
            }
            set
            {
                _connectionType = value;
                OnPropertyChanged(ConnectionTypeProperty);
            }
        }
        #endregion

        #region SelectedEntity

        private EntityInfo _selectedEntity;

        /// <summary>
        ///     Gets or sets the selected entity.
        /// </summary>
        public EntityInfo SelectedEntity
        {
            get { return _selectedEntity; }
            set
            {
                _selectedEntity = value;
                OnPropertyChanged(SelectedEntityProperty);
            }
        }

        #endregion

        #region ServerName

        private String _serverName;

        /// <summary>
        ///     Gets or sets the selected entity.
        /// </summary>
        public String ServerName
        {
            get { return _serverName; }
            set
            {
                _serverName = value;
                OnPropertyChanged(ServerNameProperty);
            }
        }

        #endregion

        #region DatabaseName

        private String _databaseName;

        /// <summary>
        ///     Gets or sets the selected entity.
        /// </summary>
        public String DatabaseName
        {
            get { return _databaseName; }
            set
            {
                _databaseName = value;
                OnPropertyChanged(DatabaseNameProperty);
            }
        }

        #endregion

        #region WindowTitle property

        /// <summary>
        ///     Gets the current window title depending on the current step.
        /// </summary>
        public String WindowTitle
        {
            get
            {
                switch (CurrentStep)
                {
                    case DbConnectionStep.CurrentConnection:
                        return Message.DbConnectionWizard_WindowTitle_CurrentConnection;
                    case DbConnectionStep.SqlEnterDetails:
                        return Message.DbConnectionWizard_WindowTitle_SqlEnterDetails;
                    case DbConnectionStep.EntitySelection:
                        return Message.DbConnectionWizard_WindowTitle_EntitySelection;
                    case DbConnectionStep.ConnectionFailed:
                        return Message.DbConnectionWizard_WindowTitle_ConnectionFailed;
                    case DbConnectionStep.ErrorOccured:
                        return Message.DbConnectionWizard_WindowTitle_ErrorOccured;
                    case DbConnectionStep.AttemptingConnection:
                        return Message.DbConnectionWizard_WindowTitle_AttemptingConnection;
                    default:
                        return String.Empty;
                }
            }
        }

        #endregion

        #region DatabaseInvalidReason

        private String _databaseInvalidReason;

        /// <summary>
        ///     Gets or sets the database invalid reason.
        /// </summary>
        public String DatabaseInvalidReason
        {
            get { return _databaseInvalidReason; }
            set
            {
                _databaseInvalidReason = value;
                OnPropertyChanged(DatabaseInvalidReasonProperty);
            }
        }

        #endregion

        #endregion

        #region Events

        /// <summary>
        ///     When the connection is established correctly.
        /// </summary>
        public event EventHandler ConnectionCompleted;

        #endregion

        #region Constructors

        public DbConnectionWizardViewModel()
            : this(false)
        {
        }

        public DbConnectionWizardViewModel(Boolean unitTesting)
        {
            if (unitTesting) return;

            _currentSettings = UserSystemSetting.FetchUserSettings();
            _currentConnection =
                _currentSettings.Connections.FirstOrDefault(connection => connection.IsCurrentConnection) ??
                _currentSettings.Connections.FirstOrDefault(connection => connection.IsAutoConnect) ??
                Connection.NewConnection();

            var viewState = App.ViewState;
            CurrentStep = DbConnectionStep.CurrentConnection;
            if (viewState.IsConnectedToRepository)
            {
                GetExistingConnectionValues();
            }
            else
            {
                ConnectionType = DbConnectionType.None;

                _currentConnection = _currentSettings.Connections.LastOrDefault() ?? Connection.NewConnection();

                if (_currentConnection != null)
                {
                    NewServerName = _currentConnection.ServerName;
                    NewDatabaseName = _currentConnection.DatabaseName;
                }

                NextCommand.Execute();

                if (_currentSettings.IsDatabaseSelectionRemembered)
                {
                    NextCommand.Execute();
                }
            }
        }

        private void GetExistingConnectionValues()
        {
            var viewState = App.ViewState;
            var dalFactory = DalContainer.GetDalFactory();

            ConnectionType = DbConnectionType.Sql;

            ServerName = dalFactory.DalFactoryConfig.DalParameters["Server"].Value;
            DatabaseName = dalFactory.DalFactoryConfig.DalParameters["Database"].Value;
            FetchAllEntityInfos();
            SetSelectedEntity(viewState.EntityId);
            IsDatabaseAvailable = true;
        }

        #endregion

        #region Commands

        #region Back command

        private RelayCommand _backCommand;

        public RelayCommand BackCommand
        {
            get
            {
                if (_backCommand != null) return _backCommand;

                _backCommand = new RelayCommand(o => Back_Executed(), o => Back_CanExecute())
                {
                    FriendlyName = Message.Generic_Previous
                };
                ViewModelCommands.Add(_backCommand);

                return _backCommand;
            }
        }

        private Boolean Back_CanExecute()
        {
            return CurrentStep == DbConnectionStep.SqlEnterDetails ||
                   CurrentStep == DbConnectionStep.ErrorOccured ||
                   CurrentStep == DbConnectionStep.EntitySelection ||
                   CurrentStep == DbConnectionStep.InvalidConnection ||
                   (CurrentStep == DbConnectionStep.CurrentConnection && IsDatabaseAvailable && !_isClosing);
        }

        private void Back_Executed()
        {
            switch (CurrentStep)
            {
                case DbConnectionStep.CurrentConnection:
                    {
                        if (App.ViewState.ClearRepositoryConnection())
                        {
                            ShowUserDisconnected();
                            CancelCommand.Execute();
                        }
                    }
                    break;
                case DbConnectionStep.SqlEnterDetails:
                    CurrentStep = DbConnectionStep.CurrentConnection;
                    break;
                case DbConnectionStep.ErrorOccured:
                case DbConnectionStep.EntitySelection:
                case DbConnectionStep.InvalidConnection:
                    CurrentStep = DbConnectionStep.SqlEnterDetails;
                    break;
            }
        }

        #endregion

        #region Cancel command

        private RelayCommand _cancelCommand;

        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand != null) return _cancelCommand;

                _cancelCommand = new RelayCommand(
                    o => Cancel_Executed(), o => Cancel_CanExecute())
                {
                    FriendlyName = Message.Generic_Cancel,
                    InputGestureModifiers = ModifierKeys.None,
                    InputGestureKey = Key.Escape
                };
                ViewModelCommands.Add(_cancelCommand);

                return _cancelCommand;
            }
        }

        private Boolean Cancel_CanExecute()
        {
            return true;
        }

        private void Cancel_Executed()
        {
            switch (CurrentStep)
            {
                case DbConnectionStep.EntitySelection:
                    App.ViewState.ClearRepositoryConnection();
                    if (AttachedControl != null) AttachedControl.Close();
                    break;
                default:
                    if (AttachedControl != null) AttachedControl.Close();
                    break;
            }
        }

        #endregion

        #region Next command

        private RelayCommand _nextCommand;

        public RelayCommand NextCommand
        {
            get
            {
                if (_nextCommand != null) return _nextCommand;

                _nextCommand = new RelayCommand(
                    o => Next_Executed(), o => Next_CanExecute())
                {
                    FriendlyName = Message.DbConnectionWizard_Change,
                    InputGestureModifiers = ModifierKeys.None,
                    InputGestureKey = Key.Enter
                };
                ViewModelCommands.Add(_nextCommand);

                return _nextCommand;
            }
        }

        public String SqlConnectionDetails
        {
            get
            {
                return string.Format(Message.DbConnectionWizard_SqlConnectionDetailsFormat, _serverName, _databaseName);
            }
        }

        private Boolean Next_CanExecute()
        {
            switch (CurrentStep)
            {
                case DbConnectionStep.SqlEnterDetails:
                    return !(String.IsNullOrEmpty(NewServerName) || String.IsNullOrEmpty(NewDatabaseName));
                default:
                    return true;
            }
        }

        private void Next_Executed()
        {
            switch (CurrentStep)
            {
                case DbConnectionStep.CurrentConnection:
                    CurrentStep = DbConnectionStep.SqlEnterDetails;
                    if (ServerName != null || DatabaseName != null)
                    {
                        NewServerName = ServerName;
                        NewDatabaseName = DatabaseName;
                    }
                    break;

                case DbConnectionStep.SqlEnterDetails:
                    CurrentStep = DbConnectionStep.AttemptingConnection;
                    BeginAttemptConnection();
                    break;

                case DbConnectionStep.ConnectionFailed:
                    //if (IsDatabaseAvailable)
                    //{
                    //    IsRetryingConnection = true;
                    //    RetryConnection();
                    //}
                    //else
                    //{
                    //    if (AttachedControl != null) AttachedControl.Close();
                    //}
                    CurrentStep = DbConnectionStep.SqlEnterDetails;
                    break;

                case DbConnectionStep.InvalidConnection:
                    CurrentStep = DbConnectionStep.SqlEnterDetails;
                    break;

                case DbConnectionStep.EntitySelection:
                    DatabaseAndEntitySelectionComplete();
                    if (AttachedControl != null) AttachedControl.Close();
                    break;

                case DbConnectionStep.ErrorOccured:
                    CurrentStep = DbConnectionStep.AttemptingConnection;
                    BeginAttemptConnection();
                    break;
            }
        }

        private void RetryConnection()
        {
            DomainPrincipal.BeginAuthentication(OnAuthenticationComplete);
        }

        #endregion

        #endregion

        #region Event handlers

        /// <summary>
        ///     Responds to a change of current step
        /// </summary>
        /// <param name="newValue"></param>
        private void OnCurrentStepChanged(DbConnectionStep newValue)
        {
            switch (newValue)
            {
                case DbConnectionStep.CurrentConnection:
                    BackCommand.FriendlyName = Message.DbConnectionWizard_Disconnect;
                    NextCommand.FriendlyName = Message.DbConnectionWizard_Change;
                    CancelCommand.FriendlyName = _isClosing ? Message.Generic_Close : Message.Generic_Cancel;
                    break;
                case DbConnectionStep.SqlEnterDetails:
                    BackCommand.FriendlyName = Message.Generic_Previous;
                    NextCommand.FriendlyName = Message.DbConnectionWizard_Connect;
                    CancelCommand.FriendlyName = Message.Generic_Cancel;
                    break;
                case DbConnectionStep.AttemptingConnection:
                    break;
                case DbConnectionStep.ConnectionFailed:
                    BackCommand.FriendlyName = Message.DbConnectionWizard_Retry;
                    NextCommand.FriendlyName = Message.Generic_Previous;
                    CancelCommand.FriendlyName = Message.Generic_Cancel;
                    break;
                case DbConnectionStep.ErrorOccured:
                    BackCommand.FriendlyName = Message.Generic_Previous;
                    NextCommand.FriendlyName = Message.DbConnectionWizard_Retry;
                    CancelCommand.FriendlyName = Message.Generic_Cancel;
                    break;
                case DbConnectionStep.EntitySelection:
                    BackCommand.FriendlyName = Message.Generic_Previous;
                    NextCommand.FriendlyName = Message.Generic_OK;
                    CancelCommand.FriendlyName = Message.Generic_Cancel;
                    if (IsEntitySelectionRemembered)
                    {
                        var existingConnection = GetExistingConnection();
                        var loadEntity = (existingConnection != null) && existingConnection.IsCurrentConnection;
                        if (loadEntity)
                        {
                            //load the entity straight away if it exists else reset
                            SelectedEntity =
                                MasterEntityList.FirstOrDefault(e => Equals(e.Id, existingConnection.BusinessEntityId));
                            if (SelectedEntity != null)
                            {
                                NextCommand.Execute();
                            }
                            else
                            {
                                IsEntitySelectionRemembered = false;
                            }
                        }
                        else
                        {
                            IsEntitySelectionRemembered = false;
                        }
                    }
                    else if (MasterEntityList.Count == 1)
                    {
                        SelectedEntity = MasterEntityList.First();
                        NextCommand.Execute();
                    }
                    break;
            }
        }

        #endregion

        #region Methods

        #region Attempt Connection

        private void AttemptConnection(object sender, DoWorkEventArgs e)
        {
            var state = (DbConnectionState)e.Argument;
            var viewState = App.ViewState;
            Boolean upgradeRequired = false;
            String databaseInvalidReason;

            Boolean isConnected = viewState.SetRepositoryConnection(state.ServerName, state.DatabaseName, out upgradeRequired, out databaseInvalidReason);
            if (isConnected)
            {
                e.Result = _currentStep;
                IsDatabaseAvailable = true;
            }
            else
            {
                if (!upgradeRequired)
                {
                    e.Result = null;
                    IsDatabaseAvailable = false;
                }
                else
                {
                    this.DatabaseInvalidReason = databaseInvalidReason;
                    e.Result = DbConnectionStep.InvalidConnection;
                    IsDatabaseAvailable = false;
                }
            }

        }

        private void BeginAttemptConnection()
        {
            //first clear any existing connection.
            // Don't do this in the background otherwise any ui affected by the entityid change
            // falls over.
            Boolean clearConnection = App.ViewState.ClearRepositoryConnection();
            if (!clearConnection)
            {
                //the action was cancelled so just go back to prev step
                this.CurrentStep = DbConnectionStep.SqlEnterDetails;
                return;
            }


            var state = new DbConnectionState { DatabaseName = NewDatabaseName, ServerName = NewServerName };
            var worker = new BackgroundWorker();

            worker.DoWork += AttemptConnection;
            worker.RunWorkerCompleted += EndAttemptConnection;

            worker.RunWorkerAsync(state);
        }

        private void EndAttemptConnection(object sender, RunWorkerCompletedEventArgs e)
        {
            var worker = (BackgroundWorker)sender;
            worker.DoWork -= AttemptConnection;
            worker.RunWorkerCompleted -= EndAttemptConnection;

            if (e.Result != null && (DbConnectionStep)e.Result != DbConnectionStep.InvalidConnection)
            {
                DomainPrincipal.BeginAuthentication(OnAuthenticationComplete);
                ConnectionType = DbConnectionType.Sql;
                ServerName = NewServerName;
                DatabaseName = NewDatabaseName;
            }
            else if (e.Result != null && (DbConnectionStep)e.Result == DbConnectionStep.InvalidConnection)
            {
                ConnectionType = DbConnectionType.None;
                CurrentStep = DbConnectionStep.InvalidConnection;
                ServerName = null;
                DatabaseName = null;
                SelectedEntity = null;
            }
            else
            {
                ConnectionType = DbConnectionType.None;
                CurrentStep = DbConnectionStep.ConnectionFailed;
                ServerName = null;
                DatabaseName = null;
                SelectedEntity = null;
            }
        }

        private void FetchAllEntityInfos()
        {
            var infos = EntityInfoList.FetchAllEntityInfos();

            if (infos.Count == 0)
            {
                //insert a new default entity
                var defaultEntity = Entity.NewEntity();
                defaultEntity.Name = Message.DbConnectionWizard_Default;
                defaultEntity.Save();

                infos = EntityInfoList.FetchAllEntityInfos();
            }

            MasterEntityList = infos;
        }

        /// <summary>
        ///     Determines the next step after a credentials error.
        /// </summary>
        private void NextStepAfterCredentialsError()
        {
            switch (CurrentStep)
            {
                case DbConnectionStep.AttemptingConnection:
                    CurrentStep = DbConnectionStep.ConnectionFailed;
                    break;
                case DbConnectionStep.ConnectionFailed:
                    IsRetryingConnection = false;
                    break;
            }
        }

        /// <summary>
        ///     Notifies the user about a credentials error when accesing the database.
        /// </summary>
        /// <param name="errorMessage"></param>
        private void NotifyCredentialsError(string errorMessage)
        {
            var msg = new ModalMessage
            {
                MessageIcon = ImageResources.Error_32,
                Description = String.Format(CultureInfo.CurrentCulture, errorMessage),
                Header = Message.DbConnectionWizard_NotifyCredentialsError_Header,
                Button1Content = Message.Generic_OK,
                ButtonCount = 1,
                Owner = AttachedControl,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };

            msg.ShowDialog();
        }

        private void OnAuthenticationComplete(object sender, DomainAuthenticationResultEventArgs e)
        {
            if (!(e.Exception == null && ApplicationContext.User.Identity.IsAuthenticated))
            {
                var errorMessage = e.Exception == null
                    ? Message.Error_UnauthorizedUser
                    : Message.DbConnectionWizard_OnAuthenticationComplete_ErrorMessage;
                NotifyCredentialsError(errorMessage);
                NextStepAfterCredentialsError();

                // reset the viewstate so we do not try to re-connect without a valid connection
                App.ViewState.IsConnectedToRepository = false;

                return;
            }

            // the user has been successfully authenticated, Retrieve available business entities 
            FetchAllEntityInfos();

            // set default entity
            SetSelectedEntity(_currentConnection.BusinessEntityId);
            _currentConnection.BusinessEntityId = SelectedEntity.Id;
            App.ViewState.EntityId = SelectedEntity.Id;

            CurrentStep = DbConnectionStep.EntitySelection;
        }

        private void SetSelectedEntity(int businessEntityId)
        {
            EntityInfo match = null;
            if (businessEntityId != 0) match = MasterEntityList.FirstOrDefault(n => n.Id == businessEntityId);

            if (match == null) match = MasterEntityList.FirstOrDefault();

            if (match != null)
            {
                SelectedEntity = match;
            }
        }

        #endregion

        private void DatabaseAndEntitySelectionComplete()
        {
            var connection = GetExistingConnection();
            if (connection == null)
            {
                connection = Connection.NewConnection();
                connection.ServerName = _serverName;
                connection.DatabaseName = _databaseName;

                _currentSettings.Connections.Add(connection);
            }

            connection.BusinessEntityId = SelectedEntity.Id;
            connection.IsAutoConnect = _currentSettings.IsDatabaseSelectionRemembered &&
                                       _currentSettings.IsEntitySelectionRemembered;
            connection.IsCurrentConnection = true;

            _currentSettings.ResetCurrentConnection(connection);
            _currentSettings.Save();

            App.ViewState.EntityId = connection.BusinessEntityId;

            if (ConnectionCompleted != null)
            {
                ConnectionCompleted(this, new EventArgs());
            }

            _isClosing = true;
            CurrentStep = DbConnectionStep.CurrentConnection;
        }

        private void ShowUserDisconnected()
        {
            ModalMessage dialog =
                       new ModalMessage()
                       {
                           Title = Message.DbConnectionWizard_Disconnected_Title,
                           Header = Message.DbConnectionWizard_Disconnected_Header,
                           Description = Message.DbConnectionWizard_Disconnected_Description,
                           ButtonCount = 1,
                           Button1Content = Message.Generic_OK,
                           DefaultButton = ModalMessageButton.Button1,
                           CancelButton = ModalMessageButton.Button1,
                           MessageIcon = ImageResources.Dialog_Information
                       };

            if (this.AttachedControl != null)
            {
                dialog.Owner = this.AttachedControl;
                WindowStartupLocation startupLocation = WindowStartupLocation.CenterOwner;
                dialog.WindowStartupLocation = startupLocation;
            }

            dialog.ShowDialog();
        }

        #endregion

        #region Helper methods

        /// <summary>
        ///     Returns the connection matching the currently selected values or null.
        /// </summary>
        /// <returns>A Connection instance, or null if there are no matches.</returns>
        private Connection GetExistingConnection()
        {
            return _currentSettings.Connections
                .FirstOrDefault(connection => connection.ServerName == ServerName &&
                                              connection.DatabaseName == DatabaseName);
        }

        /// <summary>
        ///     Helper method to retrieve the <see cref="PropertyPath"/> for the property indicated in <paramref name="expression"/>.
        /// </summary>
        /// <param name="expression">Lambda expression used to retrieve the bound property.</param>
        /// <returns>Returns a <see cref="PropertyPath"/> instance for the property indicated in <paramref name="expression"/>.</returns>
        private static PropertyPath GetPropertyPath(Expression<Func<DbConnectionWizardViewModel, Object>> expression)
        {
            return WpfHelper.GetPropertyPath(expression);
        }

        #endregion

        #region IDisposable support

        protected override void Dispose(Boolean disposing)
        {
            if (IsDisposed) return;
            if (disposing)
            {
                ConnectionCompleted = null;
                _currentConnection = null;
                _currentSettings = null;
                _selectedEntity = null;
                AttachedControl = null;
            }
            IsDisposed = true;
        }

        #endregion

        #region DbConnectionStep Enum

        /// <summary>
        ///     Each of the steps in the DB connection wizard.
        /// </summary>
        public enum DbConnectionStep
        {
            CurrentConnection,
            SqlEnterDetails,
            AttemptingConnection,
            ConnectionFailed,
            ErrorOccured,
            EntitySelection,
            InvalidConnection
        }

        #endregion

        #region DbConnectionType enum

        /// <summary>
        ///     Denotes the different connection types.
        /// </summary>
        public enum DbConnectionType
        {
            None = 0,
            Sql = 1,
        }

        #endregion

        #region Nested type: DbConnectionState

        private class DbConnectionState
        {
            public String ServerName { get; set; }
            public String DatabaseName { get; set; }
        }

        #endregion
    }
}