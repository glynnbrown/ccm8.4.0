﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

//  V8-26686 : A.Silva ~ Created.
//  V8-26721 : A.Silva ~ Refactored to use the common interface for Validation Templates.

#endregion

#endregion

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Editor.Client.Wpf.Common;

namespace Galleria.Ccm.Editor.Client.Wpf.ValidationTemplateEditor
{
    /// <summary>
    ///     Interaction logic for ValidationTemplateEditorWindow.xaml
    /// </summary>
    public partial class ValidationTemplateEditorWindow
    {
        #region ViewModel

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof (ValidationTemplateEditorViewModel), typeof (ValidationTemplateEditorWindow),
            new PropertyMetadata(null, ViewModel_PropertyChanged));

        private static void ViewModel_PropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = sender as ValidationTemplateEditorWindow;
            if (senderControl == null) return;

            var oldModel = e.OldValue as ValidationTemplateEditorViewModel;
            if (oldModel != null) oldModel.AttachedControl = null;

            var newModel = e.NewValue as ValidationTemplateEditorViewModel;
            if(newModel == null) return;

            newModel.AttachedControl = senderControl;
        }

        public ValidationTemplateEditorViewModel ViewModel
        {
            get { return (ValidationTemplateEditorViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        public ValidationTemplateEditorWindow(ValidationTemplateEditorViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //set the help file key for this window.
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.PlanogramValidationTemplate);

            ViewModel = viewModel;

            Loaded += ValidationTemplateEditorWindow_Loaded;
        }

        private void ValidationTemplateEditorWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Loaded -= ValidationTemplateEditorWindow_Loaded;
            Dispatcher.BeginInvoke((Action) (() => Mouse.OverrideCursor = null), DispatcherPriority.Background);
        }

        #endregion

        #region window close

        public Boolean IsClosing
        {
            get;
            private set;
        }

        /// <summary>
        /// Called when the window close cross is pressed.
        /// </summary>
        protected override void OnCrossCloseRequested(CancelEventArgs e)
        {
            IsClosing = true;

            ViewModel.CancelCommand.Execute();
            base.OnCrossCloseRequested(e);
        }

        /// <summary>
        /// Called when this window is closing.
        /// </summary>
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            //DialogResult = false;
            if (ViewModel.DialogResult == true)
            {
                this.UpdateBindingSources();
            }
        }

        /// <summary>
        /// Disposes of the viewmodel when the window has closed.
        /// </summary>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (ViewModel == null) return;
                var dis = ViewModel as IDisposable;
                ViewModel = null;

                if (dis != null) dis.Dispose();
            }), DispatcherPriority.Background);
        }

        #endregion
    }
}