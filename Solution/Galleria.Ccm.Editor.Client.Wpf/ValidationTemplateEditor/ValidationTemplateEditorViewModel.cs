﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0.0)

// V8-26686 : A.Silva ~ Created.
// V8-26721 : A.Silva ~ Refactored to use the common interface for Validation Templates.
// V8-26782 : A.Silva ~ Minor changes to use the Common user controls for Validation Templates.
// V8-26684 : A.Silva ~ Added localization strings.
// V8-26815 : A.Silva ~ Added commands to load and save Validation Templates.
// V8-26817 : A.Silva ~ Added cloning of the original template. The changes to it are now applied ONLY if accepted by the user.
// V8-26812 : A.Silva ~ Added Validation error descriptions for ValidationTemplate.
// V8-27635 : L.Ineson 
//  Updated save and open commands to allow unit testing.
// V8-28553 : A.Silva
//      Amended OkCommand_CanExecute() so that the metric will not be saved if the parent would be invalid.

#endregion

#region Version History: (CCM v8.0.1)
// V8-28023: L.Ineson
//  Amendments to allow default score group names to be loaded.
//  Brought code up to standard.
// V8-28841 : L.Ineson
//  Window no longer requires that every group have a metric before ok is enabled.
// V8-28884 : M.Shelley
//  Changed the window title after saving to reflect the name of the validation template
#endregion

#region Version History: (CCM v8.1.0)
// V8-30030 : M.Shelley
//  Add code to check that the validation group names are unique
#endregion

#region Version History: (CCM v8.20)
// V8-31533 : A.Probyn
//  Updated where LoadFrom is used to cast the object first
#endregion

#endregion

using System;
using System.IO;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.ValidationTemplateEditor;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Common.Selectors;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using File = System.IO.File;
using FrameworkHelpers = Galleria.Framework.Controls.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.ValidationTemplateEditor
{
    /// <summary>
    ///     Viewmodel implementation for the ValidationTemplateEditorWindow.
    /// </summary>
    public sealed class ValidationTemplateEditorViewModel : ViewModelAttachedControlObject<ValidationTemplateEditorWindow>, IValidationTemplateEditorViewModel
    {
        #region Constants

        private const String _exceptionCategory = "ValidationTemplateEditor";

        #endregion

        #region Fields

        private readonly ModelPermission<ValidationTemplate> _itemRepositoryPerms;

        private Boolean? _dialogResult;
        private readonly PlanogramValidationTemplate _validationTemplateOriginal;

        private readonly IValidationTemplate _editingValidationTemplate;

        #endregion

        #region Binding Property Paths

        //Commands
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<ValidationTemplateEditorViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath OkCommandProperty = WpfHelper.GetPropertyPath<ValidationTemplateEditorViewModel>(p => p.OkCommand);
        public static readonly PropertyPath OpenFromFileCommandProperty = WpfHelper.GetPropertyPath<ValidationTemplateEditorViewModel>(p => p.OpenFromFileCommand);
        public static readonly PropertyPath OpenFromRepositoryCommandProperty = WpfHelper.GetPropertyPath<ValidationTemplateEditorViewModel>(p => p.OpenFromRepositoryCommand);
        public static readonly PropertyPath OpenValidationTemplate = WpfHelper.GetPropertyPath<ValidationTemplateEditorViewModel>(p => p.OpenCommand);
        public static readonly PropertyPath SaveAsCommandProperty = WpfHelper.GetPropertyPath<ValidationTemplateEditorViewModel>(p => p.SaveAsCommand);
        public static readonly PropertyPath SaveToFileCommandProperty = WpfHelper.GetPropertyPath<ValidationTemplateEditorViewModel>(p => p.SaveToFileCommand);
        public static readonly PropertyPath SaveToRepositoryCommandProperty = WpfHelper.GetPropertyPath<ValidationTemplateEditorViewModel>(p => p.SaveToRepositoryCommand);

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the dialog result for the attached control.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;
                if (AttachedControl != null && !AttachedControl.IsClosing) AttachedControl.DialogResult = value;
            }
        }

        /// <summary>
        /// Returns the validation template currently being edited.
        /// </summary>
        public IValidationTemplate ValidationTemplateEdit
        {
            get { return _editingValidationTemplate; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public ValidationTemplateEditorViewModel(PlanogramView planogram)
        {
            //get permissions for current repository.
            _itemRepositoryPerms = (App.ViewState.IsConnectedToRepository)?
                new ModelPermission<ValidationTemplate>(ValidationTemplate.GetUserPermissions())
                : ModelPermission<ValidationTemplate>.DenyAll();


            _validationTemplateOriginal = planogram.Model.ValidationTemplate;

            //clone the template to be edited so that we can easily cancel changes
            _editingValidationTemplate = _validationTemplateOriginal.Clone();

            //prep the template for editing
            _editingValidationTemplate.PrepTemplateForEdit();
        }

        #endregion

        #region Commands

        #region Open

        private RelayCommand _openCommand;

        /// <summary>
        /// Main open command - executes open from repository or file based on the
        /// availability of a repository connection.
        /// </summary>
        public RelayCommand OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand(
                    p => Open_Executed())
                    {
                        FriendlyName = Message.Generic_Open,
                        SmallIcon = ImageResources.Open_16
                    };

                    base.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }


        private void Open_Executed()
        {
            if (this.OpenFromRepositoryCommand.CanExecute())
            {
                this.OpenFromRepositoryCommand.Execute();
            }
            else
            {
                this.OpenFromFileCommand.Execute();
            }
        }

        #endregion

        #region OpenFromRepository

        private RelayCommand _openFromRepositoryCommand;

        /// <summary>
        /// Allows the user to select a repository template to load data from.
        /// </summary>
        public RelayCommand OpenFromRepositoryCommand
        {
            get
            {
                if (_openFromRepositoryCommand == null)
                {
                    _openFromRepositoryCommand = new RelayCommand(
                        o => OpenFromRepository_Executed(o),
                        o => OpenFromRepository_CanExecute())
                    {
                        FriendlyName = Message.ValidationTemplateEditor_OpenFromRepository
                    };
                    ViewModelCommands.Add(_openFromRepositoryCommand);
                }
                return _openFromRepositoryCommand;
            }
        }

        private Boolean OpenFromRepository_CanExecute()
        {
            //must be connected
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.OpenFromRepositoryCommand.DisabledReason = Message.ValidationTemplateEditor_OpenFromRepository_DisabledReasonNoConnection;
                return false;
            }

            //must have open permission
            if (!_itemRepositoryPerms.CanFetch)
            {
                this.OpenFromRepositoryCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void OpenFromRepository_Executed(Object args)
        {
            //Get the template id to load.
            Object id = args;
            if (id == null)
            {
                ValidationTemplateSelectorWindow win = new ValidationTemplateSelectorWindow();
                App.ShowWindow(win, this.AttachedControl, true);
                if (win.DialogResult != true) return;

                id = win.SelectedItem.Id;
            }


            //load the template
            base.ShowWaitCursor(true);
            try
            {
                ValidationTemplate template = ValidationTemplate.FetchById(id);
                (this.ValidationTemplateEdit as PlanogramValidationTemplate).LoadFrom(template);
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);

                LocalHelper.RecordException(ex, _exceptionCategory);
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }

            //update control binding sources 
            // TODO: Am not sure why this is here?
            if (AttachedControl != null) AttachedControl.UpdateBindingSources();

            base.ShowWaitCursor(false);
        }

        #endregion

        #region OpenFromFile

        private RelayCommand _openFromFileCommand;

        /// <summary>
        /// Allows the user to select a file template to load data from.
        /// </summary>
        public RelayCommand OpenFromFileCommand
        {
            get
            {
                if (_openFromFileCommand == null)
                {
                    _openFromFileCommand = new RelayCommand(p => OpenFromFile_Executed(p))
                    {
                        FriendlyName = Message.ValidationTemplateEditor_OpenFromFile
                    };
                    base.ViewModelCommands.Add(_openFromFileCommand);
                }
                return _openFromFileCommand;
            }
        }


        private void OpenFromFile_Executed(Object args)
        {
            String filePath = args as String;
            if (this.AttachedControl != null)
            {
                //show dialog to get file
                var dialog = new System.Windows.Forms.OpenFileDialog()
                {
                    Filter = String.Format(Message.ValidationTemplate_FileExtension_Description, ValidationTemplate.FileExtension),
                    AddExtension = true,
                    DefaultExt = ValidationTemplate.FileExtension,
                    CheckPathExists = true,
                    InitialDirectory = App.ViewState.GetSessionDirectory(SessionDirectory.ValidationTemplate),
                    ReadOnlyChecked = true
                };

                if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;

                //update the session directory
                App.ViewState.SetSessionDirectory(SessionDirectory.ValidationTemplate, Path.GetDirectoryName(dialog.FileName));

                filePath = dialog.FileName;
            }
            else
            {
                //testing
                filePath = (String)args;
            }

            base.ShowWaitCursor(true);
            try
            {
                //unlock the original file as we dont need to hold it
                using (ValidationTemplate template = ValidationTemplate.FetchByFilename(filePath))
                {
                    (ValidationTemplateEdit as PlanogramValidationTemplate).LoadFrom(template);
                }

                //TODO: Am not sure why this is required?
                if (AttachedControl != null) AttachedControl.UpdateBindingSources();
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);

                LocalHelper.RecordException(ex, _exceptionCategory);
                Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }
            base.ShowWaitCursor(false);
        }

        #endregion

        #region SaveAs

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Main save as command - executes saveas to repository or file 
        /// based on connection availability
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(o => SaveAs_Executed(), o => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        SmallIcon = ImageResources.SaveAs_16
                    };
                    ViewModelCommands.Add(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        private Boolean SaveAs_CanExecute()
        {
            //item must be valid.
            if (!this.ValidationTemplateEdit.IsValid)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }


            return true;
        }

        private void SaveAs_Executed()
        {
            if (this.SaveToRepositoryCommand.CanExecute())
            {
                this.SaveToRepositoryCommand.Execute();
            }
            else
            {
                this.SaveToFileCommand.Execute();
            }
        }

        #endregion

        #region SaveToRepository

        private RelayCommand _saveToRepositoryCommand;

        /// <summary>
        /// Saves the currrent template as a new template in the repository.
        /// </summary>
        public RelayCommand SaveToRepositoryCommand
        {
            get
            {
                if (_saveToRepositoryCommand == null)
                {
                    _saveToRepositoryCommand = new RelayCommand(o => SaveToRepository_Executed(),
                        o => SaveToRepository_CanExecute())
                    {
                        FriendlyName = Message.ValidationTemplateEditor_SaveToRepository
                    };
                    ViewModelCommands.Add(_saveToRepositoryCommand);

                }
                return _saveToRepositoryCommand;
            }
        }


        private Boolean SaveToRepository_CanExecute()
        {
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.SaveToRepositoryCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }

            //must have permission
            if (!_itemRepositoryPerms.CanCreate)
            {
                this.SaveToRepositoryCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        /// <summary>
        ///     Invoked whenever the <see cref="SaveToRepositoryCommand" /> is executed.
        /// </summary>
        private void SaveToRepository_Executed()
        {
            String titleName = this.ValidationTemplateEdit.Name;

            ValidationTemplate itemToSave =
                ValidationTemplate.NewValidationTemplate(App.ViewState.EntityId, this.ValidationTemplateEdit);

            String newName;

            ValidationTemplateInfoList existingItems =
                   ValidationTemplateInfoList.FetchByEntityId(App.ViewState.EntityId);

            Boolean nameAccepted = true;
            if (this.AttachedControl != null)
            {
                Predicate<String> isUniqueCheck =
                    (s) =>
                    {
                        Boolean returnValue = true;
                        ShowWaitCursor(true);

                        foreach (ValidationTemplateInfo info in existingItems)
                        {
                            if (String.Compare(info.Name, s, StringComparison.InvariantCultureIgnoreCase) == 0)
                            {
                                returnValue = false;
                                break;
                            }
                        }

                        ShowWaitCursor(false);
                        return returnValue;
                    };

                nameAccepted =
                    FrameworkHelpers.PromptUserForValue(Message.Generic_SaveAs,
                        Message.Generic_SaveAs_NameDescription,
                        Message.ValidationTemplateEditor_NotUniqueDescription,
                        ValidationTemplate.NameProperty.FriendlyName,
                    /*maxLength*/50,
                    /*forceFirstShow*/true,
                        isUniqueCheck,
                        itemToSave.Name, out newName);

            }
            else
            {
                //force a unique value for unit testing
                newName = FrameworkHelpers.GenerateUniqueString(itemToSave.Name, existingItems.Select(v => v.Name));
            }


            //cancel out if we did not get a valid name
            if (!nameAccepted) return;


            //set
            itemToSave.Name = newName;
            titleName = itemToSave.Name;

            //Perform the save.
            ShowWaitCursor(true);

            try
            {
                itemToSave.Save();
            }
            catch (Exception ex)
            {
                ShowWaitCursor(false);
                LocalHelper.RecordException(ex, _exceptionCategory);
                FrameworkHelpers.ShowErrorOccurredMessage(itemToSave.Name, OperationType.Save);
                ShowWaitCursor(true);
            }

            // Set the title bar name
            this.ValidationTemplateEdit.Name = titleName;

            ShowWaitCursor(false);
        }

        #endregion

        #region SaveToFile


        private RelayCommand _saveToFileCommand;

        /// <summary>
        /// Saves the current template as a new file template.
        /// </summary>
        public RelayCommand SaveToFileCommand
        {
            get
            {
                if (_saveToFileCommand == null)
                {
                    _saveToFileCommand = new RelayCommand(p => SaveToFile_Executed(p))
                    {
                        FriendlyName = Message.ValidationTemplateEditor_SaveToFile
                    };
                    ViewModelCommands.Add(_saveToFileCommand);
                }
                return _saveToFileCommand;
            }
        }


        private void SaveToFile_Executed(Object args)
        {
            var titleName = this.ValidationTemplateEdit.Name;
            ValidationTemplate itemToSave = ValidationTemplate.NewValidationTemplate(App.ViewState.EntityId, ValidationTemplateEdit);

            String saveAsPath = args as String;
            if (String.IsNullOrEmpty(saveAsPath))
            {
                //show dialog to get path
                var dialog = new System.Windows.Forms.SaveFileDialog()
                {
                    Filter = String.Format(Message.ValidationTemplate_FileExtension_Description, ValidationTemplate.FileExtension),
                    AddExtension = true,
                    DefaultExt = ValidationTemplate.FileExtension,
                    OverwritePrompt = true,
                    InitialDirectory = App.ViewState.GetSessionDirectory(SessionDirectory.ValidationTemplate)
                };
                if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;

                //update the session directory
                App.ViewState.SetSessionDirectory(SessionDirectory.ValidationTemplate, Path.GetDirectoryName(dialog.FileName));


                saveAsPath = dialog.FileName;
            }

            base.ShowWaitCursor(true);

            try
            {
                itemToSave.Name = Path.GetFileNameWithoutExtension(saveAsPath);
                titleName = itemToSave.Name;
                itemToSave = itemToSave.SaveAs(saveAsPath);
            }
            catch (Exception)
            {
                base.ShowWaitCursor(false);

                // remove any half saved new files.
                if (itemToSave.IsNew && File.Exists(saveAsPath))
                {
                    try
                    {
                        File.Delete(saveAsPath);
                    }
                    catch (Exception)
                    {
                        // If it fails to delete, just move on.
                    }
                }
                base.ShowWaitCursor(true);
            }

            //dispose of the file to unlock it
            itemToSave.Dispose();
            itemToSave = null;

            // Set the title bar name
            this.ValidationTemplateEdit.Name = titleName;

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Ok

        private RelayCommand _okCommand;

        /// <summary>
        /// Commits changes made to the validation template.
        /// </summary>
        public RelayCommand OkCommand
        {
            get
            {
                if (_okCommand != null) return _okCommand;

                _okCommand = new RelayCommand(o => Ok_Executed(), o => Ok_CanExecute())
                {
                    FriendlyName = Message.Generic_OK
                };
                ViewModelCommands.Add(_okCommand);
                return _okCommand;
            }
        }

        private Boolean Ok_CanExecute()
        {
            if (!ValidationTemplateEdit.IsValid)
            {
                OkCommand.DisabledReason = Message.ValidationTemplateEditor_Ok_DisabledReasonOneOrMoreGroupsInvalid;
                return false;
            }
            if (!ValidationTemplateEdit.Groups.Any())
            {
                OkCommand.DisabledReason = Message.ValidationTemplateEditorl_Ok_DisabledReasonMissingGroups;
                return false;
            }
            if (ValidationTemplateEdit.Groups.Any(g => !g.IsValid))
            {
                OkCommand.DisabledReason = Message.ValidationTemplateEditor_Ok_DisabledReasonInvalidGroups;
                return false;
            }

            // Check if all the validation group names are unique
            var groups = ValidationTemplateEdit.Groups;
            var duplicateQuery = groups.GroupBy(x => x.Name).Where(y => y.Count() > 1).Select(z => z.Key);
            if (duplicateQuery.Any())
            {
                OkCommand.DisabledReason = Message.ValidationTemplateEditor_NotUniqueDescription;
                return false;
            }

            return true;
        }

        private void Ok_Executed()
        {
            _validationTemplateOriginal.LoadFrom(ValidationTemplateEdit);
            DialogResult = true;
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels changes made to the validation template.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand != null) return _cancelCommand;

                _cancelCommand = new RelayCommand(o => Cancel_Executed())
                {
                    FriendlyName = Message.Generic_Cancel
                };
                ViewModelCommands.Add(_cancelCommand);
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            if (AttachedControl != null) AttachedControl.Close();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Deletes the metric from the given group.
        /// </summary>
        public void DeleteGroupMetric(IValidationTemplateGroup validationTemplateGroup, IValidationTemplateGroupMetric metric)
        {
            validationTemplateGroup.RemoveMetric(metric);
        }

        /// <summary>
        /// Edits the metric in the given group.
        /// </summary>
        public void EditGroupMetric(IValidationTemplateGroup validationTemplateGroup, IValidationTemplateGroupMetric metric, String metricName)
        {
            if (AttachedControl == null) return;

            ValidationTemplateEdit.BeginEdit();

            //if (validationTemplateGroup == null)
            //{
            //    validationTemplateGroup = ValidationTemplateEdit.AddNewGroup();
            //}

            if (metric == null)
            {
                metric = validationTemplateGroup.AddNewMetric();
            }

            var editorWindow = new ValidationTemplateGroupMetricEditorWindow(metricName, metric);

            App.ShowWindow(editorWindow, AttachedControl, true);

            if (editorWindow.DialogResult != true)
            {
                ValidationTemplateEdit.CancelEdit();
            }
            else
            {
                ValidationTemplateEdit.ApplyEdit();
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //nothing to do.
                }

                base.IsDisposed = true;
            }
        }

        #endregion

    }
}