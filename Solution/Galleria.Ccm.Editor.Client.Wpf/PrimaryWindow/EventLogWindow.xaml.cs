﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26860 : L.Luong 
//   Created (Auto-generated)
#endregion

#region Version History: (CCM v8.3.0)
// V8-32200 : M.Pettit 
//   Export To Excel is now bespoke for this grid and always includes the added but initially invisible Content column
#endregion

#endregion

using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Controls;
using System.IO;
using Microsoft.Win32;
using System.Collections.Generic;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for EventLogWindow.xaml
    /// </summary>
    public partial class EventLogWindow
    {
        #region Fields
        private ContextMenu _columnHeaderContextMenu; //the column header menu
        #endregion

        #region Export to Excel Command

        private static RoutedCommand _exportGridToExcelCommand = new RoutedCommand();
        /// <summary>
        /// Returns the command to export this grid to excel
        /// </summary>
        public static RoutedCommand ExportGridToExcel
        {
            get { return _exportGridToExcelCommand; }
        }
        private static void ExportGridToExcelCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ExportDataGridToExcel((EventLogWindow)sender);
        }
        private static Boolean ExportGridToExcelCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            return true;
        }

        #endregion

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty = 
            DependencyProperty.Register("ViewModel", typeof(EventLogViewModel), typeof(EventLogWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public EventLogViewModel ViewModel
        {
            get { return (EventLogViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            EventLogWindow senderControl = (EventLogWindow)obj;

            if (e.OldValue != null)
            {
                EventLogViewModel oldModel = (EventLogViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                EventLogViewModel newModel = (EventLogViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #region Constructor

        public EventLogWindow(EventLogViewModel viewModel)
        {
            InitializeComponent();

            //set the help file key for this window.
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.PlanogramEventLog);

            ViewModel = viewModel;

            Loaded += EventLogWindow_Loaded;

            //register the export to excel command
            CommandManager.RegisterClassCommandBinding(typeof(EventLogWindow),
                new CommandBinding(_exportGridToExcelCommand,
                    ExportGridToExcelCommand_Executed));

            this.EventLogMainGrid.ColumnContextMenuOpening += ColumnContextMenuOpened;
        }

        private void EventLogWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Loaded -= EventLogWindow_Loaded;
            Dispatcher.BeginInvoke((Action)(() => Mouse.OverrideCursor = null), DispatcherPriority.Background);
        }

        #endregion

        #region Methods
        /// <summary>
        /// Exports the given grid to excel.
        /// </summary>
        public static void ExportDataGridToExcel(EventLogWindow window)
        {
            string[] stringSeparators = new string[] { "\r\n" };
            
            ExtendedDataGrid grid = window.EventLogMainGrid as ExtendedDataGrid;
            if (grid == null) return;
            
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "Excel Workbook|*.xlsx|Excel 97-2003|*.xls";

            if (saveDialog.ShowDialog() == true)
            {
                String fileName = saveDialog.FileName;

                //create a new workbook to hold the export
                Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
                Aspose.Cells.Worksheet workSheet = workbook.Worksheets[0];

                List<DataGridColumn> colList = new List<DataGridColumn>();
                foreach (DataGridColumn column in grid.Columns.OrderBy(c => c.DisplayIndex))
                {
                    //Content column is always exported as it is permanently visible
                    //in the bottom pane of the control
                    if (column.Header != Message.EventLog_ColumnHeader_Content)
                    {
                        if (column.Visibility != Visibility.Visible) continue;
                    }
                    if (column is DataGridTemplateColumn && column.ClipboardContentBinding == null) continue;
                    colList.Add(column);
                }


                Int32 curCol = 0;
                Int32 curRow = 0;

                //copy grid column headers
                foreach (DataGridColumn column in colList)
                {
                    workSheet.Cells[0, curCol].Value = column.Header;
                    curCol++;
                }
                curRow++;

                //is the content column included - should always be available now
                DataGridColumn contentCol = colList.FirstOrDefault(c => c.Header == Message.EventLog_ColumnHeader_Content);
                if (contentCol != null)
                {
                    //We have a content column included so we need to add a 
                    //row for each content item - these are CR delimited
                    Int32 contentColIndex = colList.IndexOf(contentCol);

                    foreach (var row in grid.Items)
                    {
                        String contentValue = contentCol.OnCopyingCellClipboardContent(row).ToString();

                        //split content into array of strings
                        String[] contentRows = contentValue.Split(stringSeparators, StringSplitOptions.None);

                        //loop through array, creating row for each content item
                        foreach (String contentRow in contentRows)
                        {
                            curCol = 0;

                            foreach (DataGridColumn column in colList)
                            {
                                if (colList.IndexOf(column) == contentColIndex)
                                {
                                    //use the content array value for the cell
                                    workSheet.Cells[curRow, curCol].Value = contentRow;
                                }
                                else
                                {
                                    workSheet.Cells[curRow, curCol].Value = column.OnCopyingCellClipboardContent(row);
                                }
                                curCol++;
                            }
                            curRow++;
                        }
                    }
                }
                else
                {
                    //just export all rows
                    foreach (var row in grid.Items)
                    {
                        curCol = 0;
                        foreach (DataGridColumn column in colList)
                        {
                            workSheet.Cells[curRow, curCol].Value = column.OnCopyingCellClipboardContent(row);
                            curCol++;
                        }

                        curRow++;
                    }
                }

                ////copy all rows
                //foreach (var row in grid.Items)
                //{
                //    //is the content column included - should always be available now
                //    DataGridColumn contentCol = colList.FirstOrDefault(c => c.Header == Message.EventLog_ColumnHeader_Content);
                //    if (contentCol != null)
                //    {
                //        //We have a content column included so we need to add a 
                //        //row for each content item - these are CR delimited
                //        Int32 contentColIndex = colList.IndexOf(contentCol);
                //        String contentValue = contentCol.OnCopyingCellClipboardContent(row).ToString();

                //        //split content into array of strings
                //        String[] contentRows = contentValue.Split(stringSeparators, StringSplitOptions.None);

                //        //loop through array, creating row for each content item
                //        foreach (String contentRow in contentRows)
                //        {
                //            curCol = 0;

                //            foreach (DataGridColumn column in colList)
                //            {
                //                if (colList.IndexOf(column) == contentColIndex)
                //                {
                //                    //use the content array value for the cell
                //                    workSheet.Cells[curRow, curCol].Value = contentRow;
                //                }
                //                else
                //                {
                //                    workSheet.Cells[curRow, curCol].Value = column.OnCopyingCellClipboardContent(row);
                //                }
                //                curCol++;
                //            }
                //            curRow++;
                //        }
                //    }
                //    else
                //    {

                //        curCol = 0;
                //        foreach (DataGridColumn column in colList)
                //        {
                //            workSheet.Cells[curRow, curCol].Value = column.OnCopyingCellClipboardContent(row);
                //            curCol++;
                //        }

                //        curRow++;
                //    }
                //}

                try
                {
                    //save the file.
                    workbook.Save(fileName);
                }
                catch (IOException)
                {
                    //The file is in use
                    ModalMessage dlg = new ModalMessage();
                    dlg.Header = Galleria.Framework.Controls.Wpf.Resources.Language.Generic_FileIsOpenMessageHeader;
                    dlg.Description = String.Format(Galleria.Framework.Controls.Wpf.Resources.Language.Generic_FileIsOpenMessageDescription, fileName);
                    dlg.MessageIcon = ImageResources.Warning_32;
                    dlg.ButtonCount = 1;
                    dlg.Button1Content = Galleria.Framework.Controls.Wpf.Resources.Language.Generic_Ok;
                    dlg.Owner = window;

                    dlg.ShowDialog();
                }

            }

        }

        #endregion

        #region Events

        private void ColumnContextMenuOpened(object sender, ExtendedDataGridContextMenuOpeningEventArgs e)
        {
            _columnHeaderContextMenu = e.ContextMenu as ContextMenu;
            if (_columnHeaderContextMenu == null) return;

            MenuItem exportOption = new MenuItem();
            exportOption.Header = Galleria.Framework.Controls.Wpf.Resources.Language.ExtendedDataGrid_ExportToExcel;
            exportOption.Command = _exportGridToExcelCommand; // ExtendedDataGrid.ExportGridToExcel;
            exportOption.CommandTarget = this;
            _columnHeaderContextMenu.Items.Insert(0, exportOption);

            //show the context menu
            _columnHeaderContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Mouse;
            _columnHeaderContextMenu.IsOpen = true;
            _columnHeaderContextMenu.StaysOpen = true;
        }


        #endregion

        #region Window close

        /// <summary>
        /// Disposes of the viewmodel on close
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;
                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
    }
}
