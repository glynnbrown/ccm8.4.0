﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25164 : L.Hodson ~ Created
#endregion
#endregion


using System.Windows;
using Fluent;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for ComponentContextTab.xaml
    /// </summary>
    public sealed partial class ComponentContextTab : RibbonTabItem
    {
        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(MainPageViewModel), typeof(ComponentContextTab),
            new PropertyMetadata(null));

        public MainPageViewModel ViewModel
        {
            get { return (MainPageViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        public ComponentContextTab()
        {
            InitializeComponent();
        }

        #endregion

    }
}
