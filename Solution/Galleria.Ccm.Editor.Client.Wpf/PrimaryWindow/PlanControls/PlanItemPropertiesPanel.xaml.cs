﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion

#endregion

using System;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Framework.Model;
using System.Collections.Generic;
using Galleria.Ccm.Model;
using System.Windows.Controls;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf.PropertyEditor;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow.PlanControls
{
    /// <summary>
    /// Interaction logic for PlanItemPropertiesPanel.xaml
    /// </summary>
    public sealed partial class PlanItemPropertiesPanel : DockingPanelControl, IDisposable
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanItemSelection), typeof(PlanItemPropertiesPanel),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanItemPropertiesPanel senderControl = (PlanItemPropertiesPanel)obj;

            if (e.OldValue != null)
            {
                PlanItemSelection oldModel = (PlanItemSelection)e.OldValue;
                oldModel.BulkCollectionChanged -= senderControl.ViewModel_BulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                PlanItemSelection newModel = (PlanItemSelection)e.NewValue;
                newModel.BulkCollectionChanged += senderControl.ViewModel_BulkCollectionChanged;
            }

            senderControl.xPropertyGrid.SourceObject = e.NewValue;
            senderControl.UpdatePropertyDefinitions();

        }


        /// <summary>
        /// Gets/Sets the context.
        /// </summary>
        public PlanItemSelection ViewModel
        {
            get { return (PlanItemSelection)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public PlanItemPropertiesPanel()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the selection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            UpdatePropertyDefinitions();
        }

        /// <summary>
        /// Called on property grid right click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xPropertyGrid_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (this.ViewModel.Count > 0
                && (this.ViewModel.HasPositions || this.ViewModel.HasComponents))
            {
                Fluent.ContextMenu contextMenu = new Fluent.ContextMenu();

                contextMenu.Items.Add(
                    new Fluent.MenuItem()
                    {
                        Command = MainPageCommands.Options,
                        Header = Message.PlanItemPropertiesPanel_Fields,
                        Icon = MainPageCommands.Options.SmallIcon,
                        CommandParameter = (this.ViewModel.HasPositions) ?
                            OptionsViewModel.OptionsWindowTab.PositionProperties :
                            OptionsViewModel.OptionsWindowTab.ComponentProperties
                    });

                contextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint;
                contextMenu.IsOpen = true;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the property definitions on the grid.
        /// </summary>
        private void UpdatePropertyDefinitions()
        {
            PlanItemSelection propContext = this.ViewModel;
            PropertyEditor propGrid = this.xPropertyGrid;


            if (propGrid.PropertyDescriptions.Count > 0)
            {
                propGrid.PropertyDescriptions.Clear();
            }

            //add in property definitions
            if (propContext != null)
            {
                if (propContext.Count > 0)
                {
                    //add in property definitions if req
                    Int32 orderNo = 1;
                    List<PropertyItemDescription> descriptions = new List<PropertyItemDescription>();
                    foreach (String propertyName in propContext.GetSelectionPropertyPanelNames(App.ViewState.Settings.Model))
                    {
                        descriptions.Add(new PropertyItemDescription() { PropertyName = propertyName,  });
                    }
                    propGrid.PropertyDescriptions.AddRange(descriptions);
                }
            }

        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (!_isDisposed)
            {
                this.ViewModel = null;

                GC.SuppressFinalize(this);
                _isDisposed = true;
            }
        }

        #endregion



    }
}
