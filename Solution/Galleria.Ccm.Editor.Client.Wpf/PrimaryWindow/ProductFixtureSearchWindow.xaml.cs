﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-23726 : N.Haywood
//  Created
#endregion

#region Version History: (CCM 801)
// V8-28664 : I.George
//  ~ Removed mouse item double click event method
#endregion
#region Version History: (CCM 810)
// V8-29600 : I.George
//  ~ Search box now takes focus
#endregion
#region Version History CCM 830
// V8-31386 : A.Heathcote
//      Added Advanced button and radio button events as well as readonly column creation, using user selected fields
#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Controls;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.Linq;
using Galleria.Ccm.Model;
using System.Collections.Generic;
using Galleria.Framework.ViewModel;
using System.Collections.ObjectModel;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for ProductFixtureSearchWindow.xaml
    /// </summary>
    public sealed partial class ProductFixtureSearchWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ProductFixtureSearchViewModel), typeof(ProductFixtureSearchWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ProductFixtureSearchWindow senderControl = (ProductFixtureSearchWindow)obj;

            if (e.OldValue != null)
            {
                ProductFixtureSearchViewModel oldModel = (ProductFixtureSearchViewModel)e.OldValue;
               oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                ProductFixtureSearchViewModel newModel = (ProductFixtureSearchViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        

        public ProductFixtureSearchViewModel ViewModel
        {
            get { return (ProductFixtureSearchViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value);}
        }


       
        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ProductFixtureSearchWindow(MainPageViewModel mainPageViewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.positionResultsGrid.PreviewMouseUp += PositionResultsGrid_MouseDown;
            this.ViewModel = new ProductFixtureSearchViewModel(mainPageViewModel);
            this.searchBox.Focus();
            this.Loaded += OnLoaded;
            positionResultsGrid.ColumnSet = ViewModel.ResultsDataGridFieldsCollection; 
         }

        /// <summary>
        /// Called on initial load of this window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= OnLoaded;
            this.SizeChanged += new SizeChangedEventHandler(Window_SizeChanged);

         

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null;}));
        }
        
        #endregion

        #region EventHandlers
        private void PositionResultsGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (ViewModel == null) return;
            ViewModel.OnPositionSearchResultsMouseDown(sender);
        }
       
        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (this.SizeToContent == SizeToContent.WidthAndHeight)
            {
               this.positionResultsGrid.Height = 200;
            }
            else
            {
                this.positionResultsGrid.Height = Double.NaN;
                this.SizeChanged -= Window_SizeChanged;
            }
        }

        #endregion

        #region  Methods

        
        
         #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.positionResultsGrid != null)
                {
                    this.positionResultsGrid.MouseDown -= PositionResultsGrid_MouseDown;
                }


                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;

                    if (dis != null)
                    {
                       
                        this.SizeChanged -= Window_SizeChanged;
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion

     }
}
