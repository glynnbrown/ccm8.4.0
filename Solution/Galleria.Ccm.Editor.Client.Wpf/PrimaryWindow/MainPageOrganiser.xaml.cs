﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
//  V8-25395 : A.Probyn 
//      ~ Added and plumbed in Product library context tab
//  V8-24539 : A.Probyn
//      Turned float back on
// V8-25863 : A.Probyn
//      Set default placement to be top and turned float back off
// V8-26568 : L.Ineson
//  Amended so that keypresses are not processed if they originate from a textbox.
// V8-27755 : A.Silva
//      Added event handler for double click on the Validation Waring status label.
// V8-27999 : L.Ineson
//  Added window on close handler
// V8-28342 : A.Kuszyk
//  Added polling to backstage save content.
// V8-28443 : J.Pickup
//  Force the backstage to show 'recent' tab when being displayed for 'Print' performance issue.
#endregion

#region Version History : CCM 802
//V8-25378 : M.Pettit
//  Added DockAllWindows() method
#endregion

#region Version History : CCM 820
//V8-30465 : L.Ineson
//  Added ActivePlanController dependency property
// V8-30635 : L.Ineson
//  Added call to docking manager dispose.
// V8-30791 : D.Pleasance
//  Added Handler for ColorPickerBox.SelectedColorChangedEvent, to track user colors
// V8-31403 : A.Probyn
//  Updated help ids
#endregion

#region Version History: CCM830

// V8-31747 : A.Silva
//  Added planogramCompareContextTab to RibbonTab_IsVisibleChanged so that it is selected when the planogram compare document becomes active.
// V8-32085 : N.Haywood
//  Added xStatusPanel_MouseRightButtonDown
// V8-32255 : A.Silva
//  Amended xDockingManager_DocumentClosing so that we can close several documents with just one warning.
// V8-32823  : J.Pickup
//  Export visbiltity work
// V8-32810 : L.Ineson
//  Removed IsExport available as it is no longer required.
// V8-32867 : M.Brumby
//  Added key up override
#endregion

#endregion

using Fluent;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.PrintTemplateSetup;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.FixtureLibrary;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Editor.Client.Wpf.ProductLibrary;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using Xceed.Wpf.AvalonDock;
using Xceed.Wpf.AvalonDock.Layout;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for MainPageOrganiser.xaml
    /// </summary>
    public sealed partial class MainPageOrganiser : ILayoutUpdateStrategy
    {
        #region Fields

        private RibbonTabItem _lastClickedTab;

        #endregion

        #region Properties

        #region ViewModel Property

        /// <summary>
        /// ViewModel dependency property definition.
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(MainPageViewModel), typeof(MainPageOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MainPageOrganiser senderControl = (MainPageOrganiser)obj;

            if (e.OldValue != null)
            {
                MainPageViewModel oldModel = (MainPageViewModel)e.OldValue;
                oldModel.AttachedControl = null;

                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                oldModel.PlanControllers.BulkCollectionChanged -= senderControl.ViewModel_PlanControllersBulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                MainPageViewModel newModel = (MainPageViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
                newModel.PlanControllers.BulkCollectionChanged += senderControl.ViewModel_PlanControllersBulkCollectionChanged;
            }


            senderControl.ReloadControllers();
            senderControl.ShowHideLibraryPanels();
        }

        /// <summary>
        /// Gets the viewmodel controller for this window.
        /// </summary>
        public MainPageViewModel ViewModel
        {
            get { return (MainPageViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region PlanControls Property

        public static readonly DependencyProperty PlanControlsProperty =
            DependencyProperty.Register("PlanControls", typeof(ObservableCollection<PlanController>), typeof(MainPageOrganiser),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the editable collection of document controls.
        /// </summary>
        public ObservableCollection<PlanController> PlanControls
        {
            get { return (ObservableCollection<PlanController>)GetValue(PlanControlsProperty); }
        }


        #endregion

        #region Panels Property

        public static readonly DependencyProperty PanelsProperty =
            DependencyProperty.Register("Panels", typeof(ObservableCollection<UIElement>), typeof(MainPageOrganiser),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the collection of panels to be displayed by the docking manager.
        /// </summary>
        public ObservableCollection<UIElement> Panels
        {
            get { return (ObservableCollection<UIElement>)GetValue(PanelsProperty); }
        }

        #endregion

        #region ActivePlanController Property

        /// <summary>
        /// ActivePlanController dependency property definition.
        /// </summary>
        public static readonly DependencyProperty ActivePlanControllerProperty =
            DependencyProperty.Register("ActivePlanController", typeof(Object), typeof(MainPageOrganiser),
            new PropertyMetadata(null, OnActivePlanControllerPropertyChanged));

        /// <summary>
        /// Called whenever the ActivePlanController dependency property value changes.
        /// </summary>
        private static void OnActivePlanControllerPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((MainPageOrganiser)obj).OnActivePlanControllerChanged(e.NewValue as PlanController);
        }

        /// <summary>
        /// Called whenever the ActivePlanController dependency property value changes.
        /// </summary>
        private void OnActivePlanControllerChanged(PlanController newValue)
        {
            //do nothing if the controller is null.
            //- the viewmodel should always have an active document.
            if (newValue == null) return;

            if (this.ViewModel == null) return;

            DockingManager dockingManager = this.xDockingManager;
            if (dockingManager == null) return;


            //set the new value against the viewmodel and docking control.
            PlanControllerViewModel controllerViewModel = newValue.ViewModel;

            this.ViewModel.ActivePlanController = controllerViewModel;
            dockingManager.ActiveContent = newValue;

        }

        /// <summary>
        /// Gets/Sets the active plan controller.
        /// </summary>
        public PlanController ActivePlanController
        {
            get { return (PlanController)GetValue(ActivePlanControllerProperty); }
            set { SetValue(ActivePlanControllerProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        static MainPageOrganiser()
        {
            //set common help file keys
            ColumnLayoutEditorOrganiser.HelpFileKey = HelpFileKeys.CustomColumnsEditor;
            PrintTemplateSetupWindow.HelpFileKey = HelpFileKeys.PrintTemplateSetup;
        }

        public MainPageOrganiser()
        {
            SetValue(PlanControlsProperty, new ObservableCollection<PlanController>());
            SetValue(PanelsProperty, new ObservableCollection<UIElement>());

            InitializeComponent();

            this.ViewModel = new MainPageViewModel();

            this.AddHandler(ColorPickerBox.SelectedColorChangedEvent, (RoutedEventHandler)OnSelectedColorChanged);

            this.Loaded += MainPageOrganiser_Loaded;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out initial loaded actions.
        /// </summary>
        private void MainPageOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= MainPageOrganiser_Loaded;

            MainPageRibbonHelper.IntializeQuickAccessToolbar(this.ribbon);
            MainPageRibbonHelper.LoadRibbonQuickAccessState(this.ribbon);

            //attach to state changes
            MainPageRibbonHelper.GetQuickAccessToolbar(this.ribbon).ItemsChanged += OnQuickAccessToolbarItemsChanged;
        }

        /// <summary>
        /// Called on a key down preview.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewKeyDown(System.Windows.Input.KeyEventArgs e)
        {
            //[GEM:26568] - Do not process if source is a textbox.
            if (e.OriginalSource is System.Windows.Controls.TextBox) return;
            if (e.OriginalSource is Fluent.TextBox) return;

            if (MainPageCommands.ProcessKeyDown(e.Key, Keyboard.Modifiers))
            {
                e.Handled = true;
            }

            if (e.Key == Key.P && Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (App.MainPageViewModel.ActivePlanController != null)
                {
                    // takes you the print tab
                    ribbonBackstage.IsOpen = true;
                    BackstagePrintTab.IsSelected = true;
                }
            }

            base.OnPreviewKeyDown(e);
        }

        /// <summary>
        /// Called on a key up preview
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewKeyUp(KeyEventArgs e)
        {
            //Do not process if source is a textbox.
            if (e.OriginalSource is System.Windows.Controls.TextBox) return;
            if (e.OriginalSource is Fluent.TextBox) return;

            if (MainPageCommands.ProcessKeyUp(e.Key, Keyboard.Modifiers))
            {
                e.Handled = true;
            }

            base.OnPreviewKeyUp(e);
        }

        #region ViewModel Handlers

        /// <summary>
        /// Responds to property changes on the viewmodel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == MainPageViewModel.ActivePlanControllerProperty.Path)
            {
                OnViewModelActivePlanControllerChanged();
            }
            else if (e.PropertyName == MainPageViewModel.IsLibraryPanelVisibleProperty.Path)
            {
                ShowHideLibraryPanels();
            }
        }

        /// <summary>
        /// Responds to changes to the viewmodel documents collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PlanControllersBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (PlanControllerViewModel controllerView in e.ChangedItems)
                        {
                            this.PlanControls.Add(new PlanController(controllerView));
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        //find the document and remove it.
                        foreach (PlanControllerViewModel controllerView in e.ChangedItems)
                        {
                            PlanController controller = this.PlanControls.FirstOrDefault(p => p.ViewModel == controllerView);
                            if (controller != null)
                            {
                                this.PlanControls.Remove(controller);
                            }
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    ReloadControllers();
                    break;

            }
        }

        /// <summary>
        /// Called whenever the viewmodel active plan controller value changes.
        /// </summary>
        private void OnViewModelActivePlanControllerChanged()
        {
            if (this.ViewModel == null
                || this.ViewModel.ActivePlanController == null
                || this.ViewModel.ActivePlanController.AttachedControl == null) return;


            DockingManager dockingManager = this.xDockingManager;
            if (dockingManager == null) return;


            this.ActivePlanController = this.ViewModel.ActivePlanController.AttachedControl;
        }

        #endregion

        #region Docking Manager Handlers

        /// <summary>
        ///     Invoked every time a document is about to be closed.
        /// </summary>
        /// <remarks>   
        ///     Before allowing it to be closed, wait to see if others need to be closed to and do so in a batch.
        /// </remarks>
        private void xDockingManager_DocumentClosing(Object sender, DocumentClosingEventArgs e)
        {
            //  If documents are already in a batch to be closed, just do so.
            if (ViewModel.IsCloseDocumentAllowed) return;

            PlanController controller;
            if (!TryGetPlanController(e.Document, out controller)) return;

            ViewModel.RequestClosePlanController(new MainPageViewModel.ClosePlanControllerRequest(controller, e.Document.Close));
            e.Cancel = true; // Cancel here as we are delagating to the view model when to actually close the document.
        }

        /// <summary>
        /// Called when a document is closed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xDockingManager_DocumentClosed(Object sender, DocumentClosedEventArgs e)
        {
            PlanController controller;
            if (!TryGetPlanController(e.Document, out controller)) return;

            ViewModel.ClosePlanController(controller.ViewModel);
            controller.Dispose();
        }

        /// <summary>
        /// Called a panel is hiding.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Panel_Hiding(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            LayoutAnchorable senderControl = (LayoutAnchorable)sender;
            senderControl.Close();
        }

        /// <summary>
        /// Called a panel is closing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Panel_Closed(object sender, EventArgs e)
        {
            LayoutAnchorable senderControl = (LayoutAnchorable)sender;
            senderControl.Hiding -= Panel_Hiding;
            senderControl.Closed -= Panel_Closed;

            IDisposable disposableContent = senderControl.Content as IDisposable;
            if (disposableContent != null)
            {
                disposableContent.Dispose();
            }

            if (senderControl.Content is ProductLibraryPanel
                || senderControl.Content is FixtureLibraryPanel)
            {
                //turn the toggle off.
                this.ViewModel.IsLibraryPanelVisible = false;
            }
        }

        #endregion

        #region Ribbon Handlers

        /// <summary>
        /// Called to save the ribbon state when the access toolbar changes.
        /// </summary>
        private void OnQuickAccessToolbarItemsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            MainPageRibbonHelper.SaveRibbonQuickAccessState(this.ribbon);
        }

        /// <summary>
        /// Called whenever the backstage opens and closes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Backstage_IsOpenChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            SetHelpFileKey();
        }

        /// <summary>
        /// Called whenever the backstage selected tab changes.
        /// </summary>
        private void BackstageTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetHelpFileKey();
        }

        /// <summary>
        /// Called whenever the ribbon selected tab changes
        /// </summary>
        private void Ribbon_SelectedTabChanged(object sender, SelectionChangedEventArgs e)
        {
            SetHelpFileKey();

            //Force the BackstageRibbon tab to 'Recent' when print previously selected.
            if (this.BackstagePrintTab.IsSelected == true)
            {
                this.backstageRecentTab.IsSelected = true;
            }
        }

        /// <summary>
        /// Called whenever a mouse down event occurs on a ribbon tab.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RibbonTab_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _lastClickedTab = sender as RibbonTabItem;
        }

        /// <summary>
        /// Called when the visibility of a ribbon tab changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RibbonTab_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            RibbonTabItem senderControl = sender as RibbonTabItem;
            if (senderControl != null && (Boolean)e.NewValue)
            {
                if ((senderControl == _lastClickedTab)
                    || (senderControl == this.positionContextTab && (_lastClickedTab == this.componentContextTab || _lastClickedTab == this.productLibraryContextTab))
                    || (senderControl == this.componentContextTab && (_lastClickedTab == this.positionContextTab || _lastClickedTab == this.productLibraryContextTab))
                    || (senderControl == this.productLibraryContextTab && (_lastClickedTab == this.positionContextTab || _lastClickedTab == this.componentContextTab))
                    )
                {
                    //force the tab to be reselected as we are losing the context tabs when we change selected item.
                    this.ribbon.SelectedTabItem = senderControl;
                }
                else if (senderControl == this.blockingDocumentContextTab
                    || senderControl == this.assortmentDocumentContextTab
                    || senderControl == this.consumerDecisionTreeDocumentContextTab
                    || senderControl.Equals(planogramCompareContextTab))
                {
                    //if the blocking context tab became visible then we must have clicked on a blocking view.
                    // As this context tab contains the main actions for this view and the others arent really that useful
                    // just switch straight to it.
                    this.ribbon.SelectedTabItem = senderControl;
                }
            }
        }

        #endregion

        /// <summary>
        ///     Called when the validation warning status label on the status bar is double clicked.
        /// </summary>
        private void xValidationWarningStatus_OnMouseDoubleClick(Object sender, MouseButtonEventArgs e)
        {
            if (MainPageCommands.ShowHidePropertiesPanel.CanExecute())
                MainPageCommands.ShowHidePropertiesPanel.Execute();
        }

        /// <summary>
        /// tracks color changes to keep recent user colors
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSelectedColorChanged(object sender, RoutedEventArgs e)
        {
            var colorPicker = e.OriginalSource as ColorPickerBox;
            if (colorPicker != null)
            {
                App.ViewState.ApplyRecentColor(colorPicker.SelectedColor);
            }
        }

        /// <summary>
        /// Called on property grid right click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xStatusPanel_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (this.ViewModel != null)
            {
                Fluent.ContextMenu contextMenu = new Fluent.ContextMenu();

                contextMenu.Items.Add(
                    new Fluent.MenuItem()
                    {
                        Command = MainPageCommands.Options,
                        Header = Message.MainPageOrganiser_OptionsRightClickMenu,
                        Icon = MainPageCommands.Options.SmallIcon
                        //CommandParameter = MainPageCommands.Options
                    });

                contextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint;
                contextMenu.IsOpen = true;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Reloads all controllers
        /// </summary>
        private void ReloadControllers()
        {
            if (this.PlanControls.Count > 0)
            {
                this.PlanControls.Clear();
            }

            if (this.ViewModel != null)
            {
                foreach (PlanControllerViewModel controllerView in this.ViewModel.PlanControllers)
                {
                    this.PlanControls.Add(new PlanController(controllerView));
                }
            }

        }

        /// <summary>
        /// Tiles all  plan documents horizontally.
        /// </summary>
        public void TileWindows(Orientation orientation)
        {
            Orientation tileOrientation = Orientation.Horizontal;
            if (orientation == Orientation.Horizontal) tileOrientation = Orientation.Vertical;

            if (this.xDockingManager != null)
            {
                //get a list of all layout content
                List<LayoutDocument> contentList = this.xDockingManager.Layout.Descendents().OfType<LayoutDocument>().ToList();

                //create a temp pane and move all children there
                LayoutDocumentPane tempPane = new LayoutDocumentPane();
                LayoutDocumentPaneGroup tempGroup = new LayoutDocumentPaneGroup(tempPane);
                this.xDockingManager.Layout.RootPanel.Children.Add(tempGroup);

                foreach (LayoutDocument documentTab in contentList)
                {
                    tempPane.Children.Add(documentTab);
                }

                //remove all other panes
                foreach (ILayoutPanelElement ele in this.xDockingManager.Layout.RootPanel.Children.ToList())
                {
                    var descendent = ele.Descendents().OfType<LayoutAnchorable>().FirstOrDefault(l => l.ContentId.Equals("productLibraryPanel"));
                    if (ele != tempGroup && descendent == null)
                    {
                        this.xDockingManager.Layout.RootPanel.Children.Remove(ele);
                    }
                }

                this.xDockingManager.Layout.RootPanel.Orientation = Orientation.Horizontal;

                //create the new  group with a pane per doc
                LayoutDocumentPaneGroup newGroup = new LayoutDocumentPaneGroup();
                newGroup.Orientation = tileOrientation;
                if (this.ViewModel.IsLibraryPanelVisible)
                {
                    this.xDockingManager.Layout.RootPanel.Children.Insert(1, newGroup);
                }
                else
                {
                    this.xDockingManager.Layout.RootPanel.Children.Insert(0, newGroup);
                }

                foreach (LayoutDocument documentTab in contentList)
                {
                    newGroup.Children.Add(new LayoutDocumentPane(documentTab));
                }

                //remove the temporary pane again
                this.xDockingManager.Layout.RootPanel.Children.Remove(tempGroup);

                //queue a zoom to fit all
                Dispatcher.BeginInvoke(
                    (Action)(() =>
                        {
                            if (this.ViewModel != null)
                            {
                                foreach (var controller in this.ViewModel.PlanControllers)
                                {
                                    controller.ZoomToFitAll();
                                }
                            }

                        }), priority: System.Windows.Threading.DispatcherPriority.Render);

            }
        }

        /// <summary>
        /// Docks all plan documents.
        /// </summary>
        public void DockAllWindows()
        {
            if (this.xDockingManager != null)
            {
                //get a list of all layout content
                List<LayoutDocument> contentList = this.xDockingManager.Layout.Descendents().OfType<LayoutDocument>().ToList();

                //create a temp pane and move all children there
                LayoutDocumentPane tempPane = new LayoutDocumentPane();
                LayoutDocumentPaneGroup tempGroup = new LayoutDocumentPaneGroup(tempPane);
                this.xDockingManager.Layout.RootPanel.Children.Add(tempGroup);

                foreach (LayoutDocument documentTab in contentList)
                {
                    tempPane.Children.Add(documentTab);
                }

                //remove all other panes
                foreach (ILayoutPanelElement ele in this.xDockingManager.Layout.RootPanel.Children.ToList())
                {
                    var descendent = ele.Descendents().OfType<LayoutAnchorable>().FirstOrDefault(l => l.ContentId.Equals("productLibraryPanel"));
                    if (ele != tempGroup && descendent == null)
                    {
                        this.xDockingManager.Layout.RootPanel.Children.Remove(ele);
                    }
                }

                //queue a zoom to fit by height
                Dispatcher.BeginInvoke(
                    (Action)(() =>
                    {
                        if (this.ViewModel != null)
                        {
                            foreach (var controller in this.ViewModel.PlanControllers)
                            {
                                controller.ZoomToFitHeight();
                            }
                        }

                    }), priority: System.Windows.Threading.DispatcherPriority.Render);

            }
        }

        /// <summary>
        /// Shows/Hides the fixture library panel
        /// </summary>
        private void ShowHideLibraryPanels()
        {
            foreach (UIElement panel in this.Panels.ToList())
            {
                if (panel is ProductLibraryPanel
                    || panel is FixtureLibraryPanel)
                {
                    this.Panels.Remove(panel);
                }
            }


            if (this.ViewModel != null
                && this.ViewModel.IsLibraryPanelVisible)
            {
                ProductLibraryPanel productLibrary = new ProductLibraryPanel(this.ViewModel.ProductLibraryPanelViewModel);
                productLibrary.GroupName = "Library";
                this.Panels.Add(productLibrary);

                FixtureLibraryPanel fixtureLibrary = new FixtureLibraryPanel();
                fixtureLibrary.GroupName = "Library";
                this.Panels.Add(fixtureLibrary);
            }
        }

        /// <summary>
        /// Updates the currently active help file key.
        /// </summary>
        private void SetHelpFileKey()
        {
            if (this.ribbonBackstage.IsOpen)
            {
                var tabControl = ribbonBackstageTabControl;

                if (tabControl.SelectedItem == this.BackstagePrintTab)
                {
                    HelpFileKeys.SetHelpFile(this, HelpFileKeys.BackstagePrint);
                }
                else if (tabControl.SelectedItem == this.BackstageHelpTab)
                {
                    HelpFileKeys.SetHelpFile(this, HelpFileKeys.BackstageHelp);
                }
                else
                {
                    HelpFileKeys.SetHelpFile(this, HelpFileKeys.RibbonFileTab);
                }
            }
            else
            {
                //Set the current help file key based on the selected tab:
                if (this.ribbon.SelectedTabItem == RibbonHomeTab)
                {
                    HelpFileKeys.SetHelpFile(this, HelpFileKeys.RibbonHomeTab);
                }
                else if (this.ribbon.SelectedTabItem == RibbonViewTab)
                {
                    HelpFileKeys.SetHelpFile(this, HelpFileKeys.RibbonViewTab);
                }
                else if (this.ribbon.SelectedTabItem == RibbonInsertTab)
                {
                    HelpFileKeys.SetHelpFile(this, HelpFileKeys.RibbonInsertTab);
                }
                else if (this.ribbon.SelectedTabItem == RibbonDesignTab)
                {
                    HelpFileKeys.SetHelpFile(this, HelpFileKeys.RibbonDesignTab);
                }
                else if (this.ribbon.SelectedTabItem == RibbonContentTab)
                {
                    HelpFileKeys.SetHelpFile(this, HelpFileKeys.RibbonContentTab);
                }
                else if (this.ribbon.SelectedTabItem == componentContextTab)
                {
                    HelpFileKeys.SetHelpFile(this, HelpFileKeys.RibbonFixtureComponentTab);
                }
                else if (this.ribbon.SelectedTabItem == positionContextTab)
                {
                    HelpFileKeys.SetHelpFile(this, HelpFileKeys.RibbonPositionTab);
                }
                else if (this.ribbon.SelectedTabItem == productLibraryContextTab)
                {
                    HelpFileKeys.SetHelpFile(this, HelpFileKeys.RibbonProductUniverseTab);
                }
                else if (this.ribbon.SelectedTabItem == assortmentDocumentContextTab)
                {
                    HelpFileKeys.SetHelpFile(this, HelpFileKeys.RibbonAssortmentTab);
                }
                else if (this.ribbon.SelectedTabItem == consumerDecisionTreeDocumentContextTab)
                {
                    HelpFileKeys.SetHelpFile(this, HelpFileKeys.RibbonConsumerDecisionTree);
                }
                else if (this.ribbon.SelectedTabItem == blockingDocumentContextTab)
                {
                    HelpFileKeys.SetHelpFile(this, HelpFileKeys.RibbonBlockingTab);
                }
            }
        }

        private static Boolean TryGetPlanController(LayoutContent document, out PlanController controller)
        {
            controller = document == null ? null : document.Content as PlanController;

            return controller != null;
        }

        #endregion

        #region ILayoutUpdateStrategy Members

        /// <summary>
        /// Called before a document has been inserted
        /// </summary>
        /// <param name="layout">the layout to which it will be added.</param>
        /// <param name="anchorableToShow">the document itself.</param>
        /// <param name="destinationContainer">the specific destination panel or null if the document is new.</param>
        /// <returns>True if the document has been added by this method</returns>
        Boolean ILayoutUpdateStrategy.BeforeInsertDocument(LayoutRoot layout, LayoutDocument anchorableToShow, ILayoutContainer destinationContainer)
        {
            //Turn document floating off as it is causing
            // preview mouse move to trigger and lock the screen up.
            anchorableToShow.CanFloat = true;

            return false;
        }

        /// <summary>
        /// Called after the insertion of a document
        /// </summary>
        /// <param name="layout"></param>
        /// <param name="anchorableShown"></param>
        void ILayoutUpdateStrategy.AfterInsertDocument(LayoutRoot layout, LayoutDocument anchorableShown)
        {

        }


        /// <summary>
        /// Called before an anchorable is to be inserted.
        /// </summary>
        /// <param name="layout">the layout to which it will be added.</param>
        /// <param name="anchorableToShow">the anchorable itself.</param>
        /// <param name="destinationContainer">the specific destination panel or null if the anchorable is new.</param>
        /// <returns>True if the anchorable has been added by this method</returns>
        Boolean ILayoutUpdateStrategy.BeforeInsertAnchorable(LayoutRoot layout, LayoutAnchorable anchorableToShow, ILayoutContainer destinationContainer)
        {
            //Turn document floating off as it is causing
            // preview mouse move to trigger and lock the screen up.
            anchorableToShow.CanFloat = true;

            //If the pane has a specific destination then continue.
            if (destinationContainer != null
                /*&& destinationContainer.FindParent<LayoutFloatingWindow>() != null*/)
            {
                return false;
            }

            DockingPanelControl panel = anchorableToShow.Content as DockingPanelControl;
            if (panel != null)
            {
                LayoutAnchorablePane destPane =
                    layout.Descendents().OfType<LayoutAnchorablePane>().FirstOrDefault(d => d.Name == panel.GroupName);
                if (destPane == null)
                {
                    destPane = new LayoutAnchorablePane(anchorableToShow);
                    destPane.Name = panel.GroupName;
                    destPane.DockWidth = new GridLength(panel.DockedWidth, GridUnitType.Pixel);


                    //Add the pane acording to the placement strategy
                    AnchorableShowStrategy strategy = panel.PlacementStrategy;

                    Boolean most = (strategy & AnchorableShowStrategy.Most) == AnchorableShowStrategy.Most;
                    Boolean left = (strategy & AnchorableShowStrategy.Left) == AnchorableShowStrategy.Left;
                    Boolean right = (strategy & AnchorableShowStrategy.Right) == AnchorableShowStrategy.Right;
                    Boolean top = (strategy & AnchorableShowStrategy.Top) == AnchorableShowStrategy.Top;
                    Boolean bottom = (strategy & AnchorableShowStrategy.Bottom) == AnchorableShowStrategy.Bottom;


                    if (layout.RootPanel == null)
                        layout.RootPanel = new LayoutPanel() { Orientation = (left || right ? Orientation.Horizontal : Orientation.Vertical) };

                    if (left || right)
                    {
                        if (layout.RootPanel.Orientation == Orientation.Vertical &&
                            layout.RootPanel.ChildrenCount > 1)
                        {
                            layout.RootPanel = new LayoutPanel(layout.RootPanel);
                        }

                        layout.RootPanel.Orientation = Orientation.Horizontal;

                        if (left)
                            layout.RootPanel.Children.Insert(0, destPane);
                        else
                            layout.RootPanel.Children.Add(destPane);
                    }
                    else
                    {
                        if (layout.RootPanel.Orientation == Orientation.Horizontal &&
                            layout.RootPanel.ChildrenCount > 1)
                        {
                            layout.RootPanel = new LayoutPanel(layout.RootPanel);
                        }

                        layout.RootPanel.Orientation = Orientation.Vertical;

                        if (top)
                            layout.RootPanel.Children.Insert(0, destPane);
                        else
                            layout.RootPanel.Children.Add(destPane);
                    }

                    return true;
                }
                else
                {
                    destPane.Children.Add(anchorableToShow);
                    return true;
                }
            }


            return false;
        }

        /// <summary>
        /// Called after an anchorable has been inserted.
        /// </summary>
        /// <param name="layout"></param>
        /// <param name="anchorableShown"></param>
        void ILayoutUpdateStrategy.AfterInsertAnchorable(LayoutRoot layout, LayoutAnchorable anchorableShown)
        {
            anchorableShown.Hiding += Panel_Hiding;
            anchorableShown.Closed += Panel_Closed;
        }

        #endregion

        #region Window close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            if (this.xDockingManager != null)
            {
                this.xDockingManager.Dispose();
            }

            if (this.ViewModel != null)
            {
                IDisposable dis = this.ViewModel;
                this.ViewModel = null;

                if (dis != null)
                {
                    dis.Dispose();
                }
            }
        }

        #endregion
    }

    public sealed class StatusBarHeightConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return Binding.DoNothing;
            }

            Int32 height = (Int32)value;
            return new GridLength(height);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return Binding.DoNothing;
            }

            GridLength length = (GridLength)value;
            return (Int32)length.Value;
        }
    }


}
