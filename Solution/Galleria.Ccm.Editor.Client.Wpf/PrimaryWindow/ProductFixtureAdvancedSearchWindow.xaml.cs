﻿#region Header Information 
// // Copyright © Galleria RTS Ltd 2015
#region Version History CCM 830
//  V8-31386 : A.Heathcote
//      Created

#endregion
#endregion

using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
	/// <summary>
	/// Interaction logic for ProductFixtureAdvancedSearchWindow.xaml
	/// </summary>
	public sealed partial class ProductFixtureAdvancedSearchWindow
	{
		
        #region Properties
        /// <summary>
        /// Product and Component Tabs have been assigned number
        /// values, so that different methods can be used dependednt on which tab is selected.
        /// </summary>
        private Int32 _tabNumber = 0;
        
        public Int32 TabNumber
        {
            get { return _tabNumber;}
            set { _tabNumber = value;} 
        }

		#region ViewModel Property

		/// <summary>
		/// ViewModel dependency property definition.
		/// </summary>
		public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
			typeof(ProductFixtureAdvancedSearchViewModel), typeof(ProductFixtureAdvancedSearchWindow),
			new PropertyMetadata(null, OnViewModelPropertyChanged));

		/// <summary>
		/// Called whenever the viewmodel dependency property value changes.
		/// </summary>
		private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			ProductFixtureAdvancedSearchWindow senderControl = (ProductFixtureAdvancedSearchWindow)obj;

			if (e.OldValue != null)
			{
				ProductFixtureAdvancedSearchViewModel oldModel = (ProductFixtureAdvancedSearchViewModel)e.OldValue;
				oldModel.UnregisterWindowControl();                
				oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
			}

			if (e.NewValue != null)
			{
				ProductFixtureAdvancedSearchViewModel newModel = (ProductFixtureAdvancedSearchViewModel)e.NewValue;
				newModel.RegisterWindowControl(senderControl);
            }
		}

		/// <summary>
		/// Gets the viewmodel context.
		/// </summary>
		public ProductFixtureAdvancedSearchViewModel ViewModel
		{
			get { return (ProductFixtureAdvancedSearchViewModel)GetValue(ViewModelProperty); }
			private set { SetValue(ViewModelProperty, value);}
		}

		#endregion

		#endregion

		#region Constructor

		/// <summary>
		/// Creates a new instance of this type.
		/// </summary>
		public ProductFixtureAdvancedSearchWindow(ProductFixtureAdvancedSearchViewModel viewModel)
		{
			Mouse.OverrideCursor = Cursors.Wait;
			InitializeComponent();
			//The HelpFileKey goes here  
            this.ViewModel = viewModel;
			this.Loaded += ProductFixtureAdvancedSearchWindow_Loaded;
		}

		#endregion

		#region Event Handlers

		/// <summary>
		/// Carries out initial load actions.
		/// </summary>
		private void ProductFixtureAdvancedSearchWindow_Loaded(object sender, RoutedEventArgs e)
		{
			this.Loaded -= ProductFixtureAdvancedSearchWindow_Loaded;

			//cancel the busy cursor
			Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null;}));
		}

		/// <summary>
		/// Called whenever a property changes on the viewmodel.
		/// </summary>
		private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == ProductFixtureAdvancedSearchViewModel.SelectedFieldGroupProperty.Path)
			{
				if (!String.IsNullOrWhiteSpace(this.FieldGrid.PrefilterText))				{
					//force the grid to refilter.
					this.FieldGrid.BeginFilter();
				}
			}
		}


       private void TabChange_1(object sender, MouseButtonEventArgs e)
            {
               TabNumber = 0;  
            }

        private void TabChange_2(object sender, MouseButtonEventArgs e)
           {
               TabNumber = 1;
           }


    	/// <summary>
		/// Called whenever the user double clicks on the field grid
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void FieldGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
		{   
			if (e.RowItem == null) return;
			if (this.ViewModel == null) return;
            if(TabNumber == 0)
            {
                 this.ViewModel.AddSelectedProductFieldCommand.Execute();
            }
            else if (TabNumber == 1)
            {
                this.ViewModel.AddSelectedComponentFieldsCommand.Execute();
		    }
		}

		/// <summary>
		/// Called whenever rows are dropped from the assigned columns grid.
		/// </summary>
		private void FieldGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
		{
			if (this.ViewModel == null) return;
            if (TabNumber == 0)
            {
                this.ViewModel.RemoveSelectedProductFieldsCommand.Execute();
            }
            else if (TabNumber == 1)
            {
                this.ViewModel.RemoveSelectedComponentFieldsCommand.Execute();
            }
		}

		/// <summary>
		/// Called when the user double clicks on a row in the grid.
		/// </summary>
		private void AssignedColumnsGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
		{
			//remove the selected columns
			if (this.ViewModel == null) return;
            if (TabNumber == 0)
            {
                this.ViewModel.RemoveSelectedProductFieldsCommand.Execute();
            }
            else if (TabNumber == 1)
            {
                this.ViewModel.RemoveSelectedComponentFieldsCommand.Execute();
            }
		}

		/// <summary>
		/// Called whenever rows are dropped from the field grid to this one.
		/// </summary>
		private void AssignedColumnsGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
		{
			if (e.SourceGrid != FieldGrid) return;
			if (this.ViewModel == null) return;
            if (TabNumber == 0)
            {
                this.ViewModel.AddSelectedProductFieldCommand.Execute();
            }
            else if(TabNumber == 1)
            {
                this.ViewModel.AddSelectedComponentFieldsCommand.Execute();
            }
		}

	

		#endregion

		#region window close

		/// <summary>
		/// Disposes of the viewmodel on close.
		/// </summary>
		protected override void OnClosed(EventArgs e)
		{
			base.OnClosed(e);

			Dispatcher.BeginInvoke(
			(Action)(() =>
			{
				if (this.ViewModel != null)
				{
					IDisposable dis = this.ViewModel;
					this.ViewModel = null;
					if (dis != null) dis.Dispose();
				}

			}), priority: DispatcherPriority.Background);
		}

		#endregion
	}
		
}
		
	

