﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion


using System.Windows;
using Fluent;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for FixtureContextTab.xaml
    /// </summary>
    public sealed partial class FixtureContextTab : RibbonTabItem
    {
        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(MainPageViewModel), typeof(FixtureContextTab),
            new PropertyMetadata(null));

        public MainPageViewModel ViewModel
        {
            get { return (MainPageViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        public FixtureContextTab()
        {
            InitializeComponent();
        }

        #endregion

    }
}
