﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GFS 1.0)
// GFS-13485 : L.Hodson
//  Created
// GFS-13720 : N.Foster
//  Updated application framework
//GFS-13786 : L.Hodson
//  IsDataValid is now based on warnings only. It is now a method.
//GFS-13846 : L.Hodson
//     ApplyDefaultData for group command now only executes for items where
//      apply default data is available.
//GFS-14129 : L.Hodson
//      IgnoreRowsGroup command no longer uses backgroundworker
//GFS-14241 : L.Hodson
//      Removed an offensive line of code.
// GFS-14289 : S.Bourke
//  Moved the _worksheetError.Clear as it was only clearing when the import file had errors
// GFS-14303 : J.Freeborough
//  Altered isDataValid method to also check if valid data rows remain before enabling the Next button
// GFS-14303 : J.Freeborough
//  Added ImportDataAvailable flag so that a message can be displayed in the preview data grid if no import data remains
// GFS-16350 : A.Probyn
//  Ensured all of the single commands are taking into account invalid columns. If not I've added the code required.
//  Also tweaked the code for the removal of error items inline with what I changed in ISO. It now clears down all
//  errors with the invalid column number if the selected error is to do with this column. Otherwise it just 
//  removes the selected error item.
// GFS-15189 : K.Pickup
//  Allow nullable DateTime properties to be rest to bound.
#endregion
#region Version History: (GFS 1.01)
// GFS-17639 : M.Folwarczny
//  Change to IgnoreRowsGroupCommand_Executed() to preserve one of the duplicates as original entry 
// GFS-17706 : M.Folwarczny
//  Change to IgnoreRowCommand_Executed(). Added check against PropertyIsUniqueIdentifier value, when removing duplicate error 
// GFS-16536 : N.Donohoe
//  Set modal message default buttons
#endregion
#region Version History: (GFS 2.1)
// GFS-20146 : A.Probyn
//  Updated references to max property in framework due to change of type.
#endregion
#region Version History: (GFS 2.1.1)
// GFS-21370 : M.Folwarczny
//  IgnoreRowsGroupCommand_Executed now uses more dictionaries than linq queries
// GFS-22103 : M.Folwarczny
//  Corrected how error rows flagged in the group to ignore are removed for rows in other, remaining groups 
#endregion
#region Version History: (GFS 2.1.2)
// GFS-22103 : M.Folwarczny
//  Change to IgnoreRowsGroupCommand_Executed : if no validation rows are left remove all errors and their parent error groups
#endregion
#region Version History: (GFS 2.1.3)
// GFS-21656 : M.Toomer
//  No rows get removed from Rows collection. Rows to remove are marked ignored instead.
// GFS-23149 : M.Toomer
//  Change to how preview item is found and brought into view
#endregion
#region Version History: (GFS 2.1.4)
// GFS-23823 : M.Pettit
//  When resetting the whole group of invalid rows to bounds and there were more than the error limit allowed, 
//   read each value individually from the cell to determine whether the row's field value was actually invalid
#endregion
#region Version History: (CCM V8)
// V8-25395 : A.Probyn
//      Copied from GFS
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;
using Galleria.Framework.ViewModel;

using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow.DataManagement
{
    public class ImportDataValidationViewModel : ViewModelAttachedControlObject<ImportDataValidation>
    {
        #region Fields

        private ImportFileData _validatedFileData;
        private DataGridColumnCollection _validatedfileDataColumnSet = new DataGridColumnCollection();
        private String _errorPercentageText;
        private String _rowsToProcessText;
        private String _originalRowCountText;
        private String _ignoredRowsCountText;

        private ValidationErrorGroup _selectedErrorGroup;
        private ValidationErrorItem _selectedErrorItem;

        private BulkObservableCollection<ValidationErrorGroup> _worksheetErrors = new BulkObservableCollection<ValidationErrorGroup>();
        private ReadOnlyBulkObservableCollection<ValidationErrorGroup> _worksheetErrorsRO;

        private ModalBusy _busyDialog;

        private Boolean _importDataAvailable = false;
        //private CollectionViewSource _validatedFileDataRowsViewSource;

        #endregion

        #region Binding Property Path
        public static readonly PropertyPath ValidatedFileDataProperty = WpfHelper.GetPropertyPath<ImportDataValidationViewModel>(p => p.ValidatedFileData);
        public static readonly PropertyPath WorksheetErrorsProperty = WpfHelper.GetPropertyPath<ImportDataValidationViewModel>(p => p.WorksheetErrors);
        public static readonly PropertyPath ErrorPercentageTextProperty = WpfHelper.GetPropertyPath<ImportDataValidationViewModel>(p => p.ErrorPercentageText);
        public static readonly PropertyPath RowsToProcessTextProperty = WpfHelper.GetPropertyPath<ImportDataValidationViewModel>(p => p.RowsToProcessText);
        public static readonly PropertyPath OriginalRowCountTextProperty = WpfHelper.GetPropertyPath<ImportDataValidationViewModel>(p => p.OriginalRowCountText);
        public static readonly PropertyPath IgnoredRowsCountTextProperty = WpfHelper.GetPropertyPath<ImportDataValidationViewModel>(p => p.IgnoredRowsCountText);
        public static readonly PropertyPath ValidatedFileDataColumnSetProperty = WpfHelper.GetPropertyPath<ImportDataValidationViewModel>(p => p.ValidatedFileDataColumnSet);
        public static readonly PropertyPath SelectedErrorGroupProperty = WpfHelper.GetPropertyPath<ImportDataValidationViewModel>(p => p.SelectedErrorGroup);
        public static readonly PropertyPath SelectedErrorItemProperty = WpfHelper.GetPropertyPath<ImportDataValidationViewModel>(p => p.SelectedErrorItem);
        public static readonly PropertyPath ImportDataAvailableProperty = WpfHelper.GetPropertyPath<ImportDataValidationViewModel>(p => p.ImportDataAvailable);

        public static readonly PropertyPath ValidatedFileDataRowsProperty = WpfHelper.GetPropertyPath<ImportDataValidationViewModel>(p => p.ValidatedFileData.Rows);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the validated file data
        /// </summary>
        public ImportFileData ValidatedFileData
        {
            get { return _validatedFileData; }
            private set
            {
                _validatedFileData = value;
                OnPropertyChanged(ValidatedFileDataProperty);
                OnValidatedFileDataChanged();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnValidatedFileDataChanged()
        {
            if (this.ValidatedFileData != null)
            {
                //set row information labels
                Int32 validRowsCount = ValidatedFileData.GetValidRows().Count();
                this.OriginalRowCountText
                    = String.Format(Message.BackstageDataManagement_Validation_OriginalRowCount,
                        this.ValidatedFileData.Rows.Count());
                this.RowsToProcessText
                    = validRowsCount == 0 ?
                    Message.BackstageDataManagement_Validation_NoRowsLeftToProcess
                    : String.Format(Message.BackstageDataManagement_Validation_RowsLeftToProcess,
                        validRowsCount);
                this.IgnoredRowsCountText
                    = String.Format(Message.BackstageDataManagement_Validation_IgnoredRowCount,
                        this.ValidatedFileData.Rows.Count() - validRowsCount);
            }
        }

        /// <summary>
        /// Returns the column set to be used to display the validated file data
        /// </summary>
        public DataGridColumnCollection ValidatedFileDataColumnSet
        {
            get { return _validatedfileDataColumnSet; }
        }

        ///// <summary>
        ///// Returns an filtered rows view
        ///// </summary>
        //public ICollectionView ValidatedFileDataRowsViewSource
        //{
        //    get
        //    {
        //        if (_validatedFileDataRowsViewSource == null)
        //        {
        //            _validatedFileDataRowsViewSource = new CollectionViewSource();

        //            if (this.ValidatedFileData != null)
        //            {
        //                _validatedFileDataRowsViewSource.Source = this.ValidatedFileData.Rows;
        //                //_validatedFileDataRowsViewSource.View.Filter = o =>
        //                //{
        //                //    ImportFileDataRow row = o as ImportFileDataRow;
        //                //    Boolean filter = false;
        //                //    if (row != null)
        //                //    { filter = !row.IsIgnored; }
        //                //    return filter;
        //                //};
        //            }
        //        }
        //        return _validatedFileDataRowsViewSource.View;
        //    }
        //}

        /// <summary>
        /// Returns the collection of validation errors.
        /// </summary>
        public ReadOnlyBulkObservableCollection<ValidationErrorGroup> WorksheetErrors
        {
            get
            {
                if (_worksheetErrorsRO == null)
                {
                    _worksheetErrorsRO = new ReadOnlyBulkObservableCollection<ValidationErrorGroup>(_worksheetErrors);
                }
                return _worksheetErrorsRO;
            }
        }

        /// <summary>
        /// Gets/sets the selected error group 
        /// </summary>
        public ValidationErrorGroup SelectedErrorGroup
        {
            get { return _selectedErrorGroup; }
            set
            {
                if (value != null)
                {
                    _selectedErrorGroup = value;
                }
                OnPropertyChanged(SelectedErrorGroupProperty);
            }
        }

        /// <summary>
        /// Gets/setse the selected error item
        /// </summary>
        public ValidationErrorItem SelectedErrorItem
        {
            get { return _selectedErrorItem; }
            set
            {
                _selectedErrorItem = value;
                OnPropertyChanged(SelectedErrorItemProperty);
            }
        }

        /// <summary>
        /// Returns friendly text to diplay the percentage of errors
        /// </summary>
        public String ErrorPercentageText
        {
            get { return _errorPercentageText; }
            private set
            {
                _errorPercentageText = value;
                OnPropertyChanged(ErrorPercentageTextProperty);
            }
        }

        /// <summary>
        /// Returns friendly text to diplay the number of rows left to process
        /// </summary>
        public String RowsToProcessText
        {
            get { return _rowsToProcessText; }
            private set
            {
                _rowsToProcessText = value;
                OnPropertyChanged(RowsToProcessTextProperty);
            }
        }

        /// <summary>
        /// Returns friendly text to diplay the original row count 
        /// </summary>
        public String OriginalRowCountText
        {
            get { return _originalRowCountText; }
            private set
            {
                _originalRowCountText = value;
                OnPropertyChanged(OriginalRowCountTextProperty);
            }
        }

        /// <summary>
        /// Returns friendly text to diplay the ignored row count
        /// </summary>
        public String IgnoredRowsCountText
        {
            get { return _ignoredRowsCountText; }
            private set
            {
                _ignoredRowsCountText = value;
                OnPropertyChanged(IgnoredRowsCountTextProperty);
            }
        }

        /// <summary>
        /// Returns boolean whether import data is available
        /// </summary>
        public Boolean ImportDataAvailable
        {
            get
            {
                return _importDataAvailable = this.ValidatedFileData.Rows.Count > 0 ? true : false;
            }
            private set
            {
                _importDataAvailable = value;
                OnPropertyChanged(ImportDataAvailableProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="fileData"></param>
        public ImportDataValidationViewModel()
        {
            _worksheetErrors.BulkCollectionChanged += WorksheetErrors_BulkCollectionChanged;
        }

        /// <summary>
        /// Testing Constructor
        /// </summary>
        /// <param name="importFileData"></param>
        public ImportDataValidationViewModel(ImportFileData importFileData)
        {
            _validatedFileData = importFileData;
        }


        #endregion

        #region Commands

        #region IgnoreRowsGroupCommand

        private RelayCommand _ignoreRowsGroupCommand;

        /// <summary>
        /// Ignore row command for selected error group's errors
        /// </summary>
        public RelayCommand IgnoreRowsGroupCommand
        {
            get
            {
                if (_ignoreRowsGroupCommand == null)
                {
                    _ignoreRowsGroupCommand =
                    new RelayCommand(p => IgnoreRowsGroupCommand_Executed(), p => IgnoreRowsGroupCommand_CanExecute())
                    {
                        FriendlyName = Message.BackstageDataManagement_Validation_IgnoreRowsCommand,
                        FriendlyDescription = Message.BackstageDataManagement_Validation_IgnoreRowsCommand_Desc
                    };
                    base.ViewModelCommands.Add(_ignoreRowsGroupCommand);
                }
                return _ignoreRowsGroupCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean IgnoreRowsGroupCommand_CanExecute()
        {
            return (this.SelectedErrorGroup != null);
        }

        private void IgnoreRowsGroupCommand_Executed()
        {
            base.ShowWaitCursor(true);

            // GFS-17639 used when removing validation errors
            IEnumerable<ValidationErrorItem> errors = this.SelectedErrorGroup.Errors.OrderBy(i => i.Mapping.PropertyIsUniqueIdentifier); // to get this as IEnumerable
            List<ImportFileDataRow> rowsToPreserve = new List<ImportFileDataRow>();
            List<Int32> uniqueRowNumbers = new List<int>();
            // end of GFS-17639 

            List<int> removeRowNumbers = new List<int>();

            //If not any invalid column numbers exist across the groups
            if (this.SelectedErrorGroup.ContainsInvalidColumnErrorsWarnings)
            {
                //clear all rows
                //this.ValidatedFileData.Rows.Clear();
                this.ValidatedFileData.IgnoreRows();
            }
            else
            {
                // GFS-17639 
                //get a list of rows to be removed
                List<ImportFileDataRow> rowsToRemove = new List<ImportFileDataRow>();
                Dictionary<Int32, String> duplicateRows = new Dictionary<Int32, String>();
                List<Int32> tempUniqueRowNumbers = new List<int>();

                // error items divided into groups
                Dictionary<Int32, List<ValidationErrorItem>> uniqueIdentifierErrorItems = new Dictionary<Int32, List<ValidationErrorItem>>();
                Dictionary<Int32, List<ValidationErrorItem>> nonDuplicateErrorItems = new Dictionary<Int32, List<ValidationErrorItem>>();
                Dictionary<Int32, List<ValidationErrorItem>> duplicateErrorItems = new Dictionary<Int32, List<ValidationErrorItem>>();

                // build up rows cache
                Dictionary<Int32, ImportFileDataRow> rowNumberToRowCache = this.ValidatedFileData.Rows.ToDictionary(row => row.RowNumber);
                // build up error cache
                Dictionary<Int32, List<ValidationErrorItem>> rowNumberToErrorsCache = new Dictionary<Int32, List<ValidationErrorItem>>();
                foreach (ValidationErrorItem errorItem in this.SelectedErrorGroup.Errors)
                {
                    // populate row number to error item cache
                    if (!rowNumberToErrorsCache.ContainsKey(errorItem.RowNumber))
                    {
                        List<ValidationErrorItem> errorItemList = new List<ValidationErrorItem>();
                        errorItemList.Add(errorItem);
                        rowNumberToErrorsCache.Add(errorItem.RowNumber, errorItemList);
                    }
                    else
                    {
                        rowNumberToErrorsCache[errorItem.RowNumber].Add(errorItem);
                    }

                    // populate row number to unique identifier error item cache
                    if (errorItem.Mapping.PropertyIsUniqueIdentifier == true)
                    {
                        if (!uniqueIdentifierErrorItems.ContainsKey(errorItem.RowNumber))
                        {
                            List<ValidationErrorItem> errorItemList = new List<ValidationErrorItem>();
                            errorItemList.Add(errorItem);
                            uniqueIdentifierErrorItems.Add(errorItem.RowNumber, errorItemList);
                            //add to rows to remove as at the moment all those are to be removed regardless of other errors
                            removeRowNumbers.Add(errorItem.RowNumber);
                        }
                        else
                        {
                            uniqueIdentifierErrorItems[errorItem.RowNumber].Add(errorItem);
                        }
                    }
                    else if (errorItem.IsDuplicateError == false) // populate row number to remaining non duplicate error item cache
                    {

                        if (!nonDuplicateErrorItems.ContainsKey(errorItem.RowNumber))
                        {
                            List<ValidationErrorItem> errorItemList = new List<ValidationErrorItem>();
                            errorItemList.Add(errorItem);
                            nonDuplicateErrorItems.Add(errorItem.RowNumber, errorItemList);
                            //add to rows to remove as at the moment all those are to be removed regardless of other errors
                            removeRowNumbers.Add(errorItem.RowNumber);
                        }
                        else
                        {
                            nonDuplicateErrorItems[errorItem.RowNumber].Add(errorItem);
                        }
                    }
                    else // populate row number to duplicate error item cache
                    {
                        //duplicateErrorItems.Add(errorItem);
                        if (!duplicateErrorItems.ContainsKey(errorItem.RowNumber))
                        {
                            List<ValidationErrorItem> errorItemList = new List<ValidationErrorItem>();
                            errorItemList.Add(errorItem);
                            duplicateErrorItems.Add(errorItem.RowNumber, errorItemList);
                        }
                        else
                        {
                            duplicateErrorItems[errorItem.RowNumber].Add(errorItem);
                        }
                    }
                }

                // as other error types are always ignored, we are left with duplicate errors - some of those should be retained
                foreach (KeyValuePair<Int32, List<ValidationErrorItem>> duplicateErrorItem in duplicateErrorItems)
                {
                    foreach (ValidationErrorItem errorItem in duplicateErrorItem.Value)
                    {
                        duplicateRows.Add(duplicateErrorItem.Key, errorItem.DataAffected);
                    }
                }
                Dictionary<String, int> uniqueRows = new Dictionary<string, int>();
                //'sort' duplicate rows into unique entries and those to be removed
                for (int i = 0; i < duplicateRows.Count(); i++)
                {
                    if (!uniqueRows.ContainsKey(duplicateRows[duplicateRows.Keys.ElementAt(i)])) // identify first unique item
                    {
                        uniqueRowNumbers.Add(duplicateRows.Keys.ElementAt(i));
                        uniqueRows.Add(duplicateRows[duplicateRows.Keys.ElementAt(i)], duplicateRows.Keys.ElementAt(i));
                    }
                    else
                    {
                        removeRowNumbers.Add(duplicateRows.Keys.ElementAt(i)); // mark as duplicate otherwise
                    }
                }

                //Enumerate through and remove
                foreach (Int32 rowId in removeRowNumbers)
                {
                    rowsToRemove.Add(rowNumberToRowCache[rowId]);

                }
                this.ValidatedFileData.IgnoreRows(rowsToRemove);

                //end of GFS-17639 
            }


            //[ISO-13318] Rebind the data table to the grid
            //if (this.AttachedControl != null)
            //{
            //    Binding worksheetDataBinding = new Binding();
            //    worksheetDataBinding.Path = ValidatedFileDataProperty;
            //    BindingOperations.SetBinding(this.AttachedControl.validationDataGrid, ExtendedDataGrid.ItemsSourceExtendedProperty, worksheetDataBinding);
            //}

            // GFS-17639 
            // identify errors to remove and errors for rows which we're keeping

            IEnumerable<ValidationErrorItem> errorsToRemove = this.SelectedErrorGroup.Errors.Where(p => !uniqueRowNumbers.Contains(p.RowNumber));
            List<Int32> rowIdsToRemove = errorsToRemove.Select(r => r.RowNumber).ToList();
            IEnumerable<ValidationErrorItem> retainedWithErrors = this.SelectedErrorGroup.Errors.Except(errorsToRemove);

            if (this.SelectedErrorGroup.ContainsInvalidColumnErrorsWarnings)
            {
                // GFS-22103 if no validation rows are left remove all errors and their parent error groups
                if (!this.ValidatedFileData.GetValidRows().Any())
                {
                    foreach (ValidationErrorGroup groupedError in this.WorksheetErrors.ToList())
                    {
                        //Enumerate through remove list
                        foreach (ValidationErrorItem error in groupedError.Errors.ToList())
                        {
                            //Remove from grouped error row errors
                            groupedError.Errors.Remove(error);
                        }
                        //remove completely if no errors left
                        _worksheetErrors.Remove(groupedError);
                    }
                }
                else
                {
                    foreach (ValidationErrorItem item in this.SelectedErrorGroup.Errors.ToList())
                    {
                        this.SelectedErrorGroup.Errors.Remove(item);
                    }
                }
            }
            else
            {
                //Enumerate through and remove errors for the rows we kept
                foreach (ValidationErrorItem error in retainedWithErrors.ToList())
                {
                    this.SelectedErrorGroup.Errors.Remove(error);
                }
                // for the rows we removed also remove other errors that might have been flaged for those
                foreach (ValidationErrorItem error in errorsToRemove.ToList())
                {
                    this.SelectedErrorGroup.Errors.Remove(error);
                }
                //remove any other errors on the error rows as the row are no longer present
                RemoveOtherErrorWithMatchingRowNumbers(rowIdsToRemove);
            }

            // end of GFS-17639

            //[ISO-13318] Manually Update error information
            foreach (ValidationErrorGroup errorGroup in this.WorksheetErrors.ToList())
            {
                if (errorGroup.Errors.Count == 0)
                {
                    //remove completely if no errors left
                    _worksheetErrors.Remove(errorGroup);
                }
                else
                {
                    errorGroup.UpdateOnErrorsChanged();
                }
            }

            //Update error percentage text
            CalculateErrorPercentage();

            //Check there are still import rows remaining
            Int32 remainingRowsCount = this.ValidatedFileData.GetValidRows().Count();
            ImportDataAvailable = remainingRowsCount > 0 ? true : false;
            this.RowsToProcessText
                    = remainingRowsCount == 0 ?
                    Message.BackstageDataManagement_Validation_NoRowsLeftToProcess
                    : String.Format(Message.BackstageDataManagement_Validation_RowsLeftToProcess,
                        remainingRowsCount);
            this.IgnoredRowsCountText
                = String.Format(Message.BackstageDataManagement_Validation_IgnoredRowCount,
                    this.ValidatedFileData.Rows.Count() - remainingRowsCount);

            // trigger data refresh
            this.ValidatedFileData.Rows.Reset();

            base.ShowWaitCursor(false);

        }

        #endregion

        #region IgnoreRowCommand

        private RelayCommand _ignoreRowCommand;
        /// <summary>
        /// Ignore row command for selected error item
        /// </summary>
        public RelayCommand IgnoreRowCommand
        {
            get
            {
                if (_ignoreRowCommand == null)
                {
                    _ignoreRowCommand =
                    new RelayCommand(p => IgnoreRowCommand_Executed(), p => IgnoreRowCommand_CanExecute())
                    {
                        FriendlyName = Message.BackstageDataManagement_Validation_IgnoreRowCommand,
                        FriendlyDescription = Message.BackstageDataManagement_Validation_IgnoreRowCommand_Desc
                    };
                    base.ViewModelCommands.Add(_ignoreRowCommand);
                }
                return _ignoreRowCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean IgnoreRowCommand_CanExecute()
        {
            return (this.SelectedErrorItem != null);
        }

        private void IgnoreRowCommand_Executed()
        {
            base.ShowWaitCursor(true);

            if (this.SelectedErrorItem != null)
            {
                //Check if selected error item column is marked as an invalid column
                bool columnInvalid = (this.SelectedErrorGroup.InvalidColumnNumbers.Contains(this.SelectedErrorItem.ColumnNumber));

                if (columnInvalid) //remove all rows
                {
                    ClearAllErrorsAndData();

                    //[ISO-13442] Updated all groups error headers
                    foreach (ValidationErrorGroup errorGroup in this.WorksheetErrors.ToList())
                    {
                        if (errorGroup.Errors.Count == 0)
                        {
                            //remove completely if no errors left
                            _worksheetErrors.Remove(errorGroup);
                        }
                        else
                        {
                            errorGroup.UpdateOnErrorsChanged();
                        }
                    }
                }
                else //remove specific rows
                {
                    Int32 removeRowNumber = this.SelectedErrorItem.RowNumber;
                    ImportFileDataRow rowToRemove = this.ValidatedFileData.Rows.FirstOrDefault(r => r.RowNumber == removeRowNumber);
                    //this.ValidatedFileData.Rows.Remove(rowToRemove);
                    this.ValidatedFileData.IgnoreRows(new List<ImportFileDataRow>() { rowToRemove });


                    //Take copy of the error item and its number
                    ValidationErrorItem originalSelectedError = this.SelectedErrorItem;

                    //Remove the error item from the group
                    this.SelectedErrorGroup.Errors.Remove(this.SelectedErrorItem);

                    //Call method to remove any other errors on the same row as this row is no longer in the data table
                    RemoveOtherErrorWithMatchingRowNumber(originalSelectedError.RowNumber);



                    //Select new error item within the same group
                    this.SelectedErrorItem = this.SelectedErrorGroup.Errors.FirstOrDefault();

                    //[ISO-12922]If selected error item is a duplicate error, check if ignoring has 
                    //resolved the duplicate issue.
                    if (originalSelectedError.IsDuplicateError)
                    {
                        //Check if only 1 left with the same data and type
                        if (this.SelectedErrorGroup.Errors.Where(e => e.DataAffected == originalSelectedError.DataAffected && e.IsDuplicateError).Count() == 1)
                        {
                            //Get error to remove
                            ValidationErrorItem errorToRemove = this.SelectedErrorGroup.Errors.Where(e => e.DataAffected == originalSelectedError.DataAffected && e.IsDuplicateError).FirstOrDefault();

                            if (errorToRemove != null && !errorToRemove.Mapping.PropertyIsUniqueIdentifier) //GFS-17706
                            {
                                //If so remove this error too making the import possible
                                this.SelectedErrorGroup.Errors.Remove(errorToRemove);
                            }
                        }
                    }

                    //[ISO-13373] Manually Update error information
                    UpdateErrorInformation();
                }

                Int32 remainingRowsCount = this.ValidatedFileData.GetValidRows().Count();
                this.RowsToProcessText
                    = remainingRowsCount == 0 ?
                    Message.BackstageDataManagement_Validation_NoRowsLeftToProcess
                    : String.Format(Message.BackstageDataManagement_Validation_RowsLeftToProcess,
                        remainingRowsCount);

                this.IgnoredRowsCountText
                    = String.Format(Message.BackstageDataManagement_Validation_IgnoredRowCount,
                        this.ValidatedFileData.Rows.Count() - remainingRowsCount);

                // trigger data refresh
                this.ValidatedFileData.Rows.Reset();

                // Re-select new error item within the same group
                //  this is done because something is overriding this selection 
                //  and leads to the preview item not being selected (no time to look for the culprit any further)
                this.SelectedErrorItem = this.SelectedErrorGroup.Errors.FirstOrDefault();

            }

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Method to clear all errors and data
        /// </summary>
        private void ClearAllErrorsAndData()
        {
            //Clear all data table rows
            //this.ValidatedFileData.Rows.Clear();
            this.ValidatedFileData.IgnoreRows();

            //Clear all errors
            foreach (ValidationErrorGroup error in this.WorksheetErrors)
            {
                error.Errors.Clear();
            }
        }

        #endregion

        #region ApplyDefaultDataGroupCommand

        private RelayCommand _applyDefaultDataGroupCommand;
        /// <summary>
        /// Apply default data for the selected error group's errors
        /// </summary>
        public RelayCommand ApplyDefaultDataGroupCommand
        {
            get
            {
                if (_applyDefaultDataGroupCommand == null)
                {
                    _applyDefaultDataGroupCommand =
                    new RelayCommand(p => ApplyDefaultDataGroupCommand_Executed(), p => ApplyDefaultDataGroupCommand_CanExecute())
                    {
                        FriendlyName = Message.BackstageDataManagement_Validation_DefaultDataCommand,
                        FriendlyDescription = Message.BackstageDataManagement_Validation_DefaultDataCommand_Desc
                    };
                    base.ViewModelCommands.Add(_applyDefaultDataGroupCommand);
                }
                return _applyDefaultDataGroupCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ApplyDefaultDataGroupCommand_CanExecute()
        {
            return (this.SelectedErrorGroup != null);
        }

        /// <summary>
        /// Apply default data for the selected error group's errors. 
        /// Shows progress bar to user and carries out the work via a background worker.
        /// </summary>
        private void ApplyDefaultDataGroupCommand_Executed()
        {
            //Setup background worker with event handlers attached
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += ApplyDefaultDataBackgroundWorker_DoWork;
            worker.RunWorkerCompleted += ApplyDefaultDataBackgroundWorker_RunWorkerCompleted;
            worker.WorkerReportsProgress = false;
            worker.WorkerSupportsCancellation = false;
            worker.RunWorkerAsync();

            //show busy
            if (this.AttachedControl != null)
            {
                _busyDialog = new ModalBusy();
                _busyDialog.Description = Message.BackstageDataManagement_UpdateMessage;
                _busyDialog.IsDeterminate = false;
                App.ShowWindow(_busyDialog, /*isModal*/true);
            }

        }

        /// <summary>
        /// DoWork handler for the background worker
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyDefaultDataBackgroundWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            List<Int32> processedColumns = new List<Int32>();

            foreach (ValidationErrorItem item in this.SelectedErrorGroup.Errors.Where(i => i.IsApplyDefaultDataAvailable).ToList())
            {
                //If column is invalid
                if (this.SelectedErrorGroup.InvalidColumnNumbers.Contains(item.ColumnNumber))
                {
                    //[ISO-13318] Only enumerate through rows if column not already processed
                    if (!processedColumns.Contains(item.ColumnNumber))
                    {
                        foreach (ImportFileDataRow row in this.ValidatedFileData.GetValidRows())
                        {
                            //Set data to be the default value for this mapping
                            row[item.ColumnNumber].CurrentValue = (item.Mapping.PropertyDefault == null) ? DBNull.Value : item.Mapping.PropertyDefault;
                        }

                        processedColumns.Add(item.ColumnNumber);
                    }
                }
                else
                {
                    ApplyDefaultData(item);
                }
            }
        }

        /// <summary>
        /// Event handler for the background work completing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplyDefaultDataBackgroundWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            BackgroundWorker senderWorker = (BackgroundWorker)sender;
            senderWorker.DoWork -= ApplyDefaultDataBackgroundWorker_DoWork;
            senderWorker.RunWorkerCompleted -= ApplyDefaultDataBackgroundWorker_RunWorkerCompleted;


            //Hide busy
            if (_busyDialog != null)
            {
                _busyDialog.Close();
                _busyDialog = null;
            }


            if (e.Error != null)
            {
                //Throw error window
                ModalMessage errorMessageBox = new ModalMessage();
                errorMessageBox.Header = Message.BackstageDataManagement_ImportFileTitle;
                errorMessageBox.Description = Message.BackstageDataManagement_UpdateErrorMessage;
                errorMessageBox.ButtonCount = 1;
                errorMessageBox.Button1Content = Message.Generic_OK;
                errorMessageBox.DefaultButton = ModalMessageButton.Button1;
                App.ShowWindow(errorMessageBox, /*isModal*/true);
            }
            else
            {
                //Clear all errors
                this.SelectedErrorGroup.Errors.RemoveList(this.SelectedErrorGroup.Errors.Where(i => i.IsApplyDefaultDataAvailable).ToList());
                this.SelectedErrorGroup.IsApplyDefaultDataAvailable = false;

                UpdateErrorInformation();

            }

        }

        #endregion

        #region ApplyDefaultDataCommand

        private RelayCommand _applyDefaultDataCommand;
        /// <summary>
        /// Apply default data for the selected error item
        /// </summary>
        public RelayCommand ApplyDefaultDataCommand
        {
            get
            {
                if (_applyDefaultDataCommand == null)
                {
                    _applyDefaultDataCommand =
                    new RelayCommand(p => ApplyDefaultDataCommand_Executed(), p => ApplyDefaultDataCommand_CanExecute())
                    {
                        FriendlyName = Message.BackstageDataManagement_Validation_DefaultDataCommand,
                        FriendlyDescription = Message.BackstageDataManagement_Validation_DefaultDataCommand_Desc
                    };
                    base.ViewModelCommands.Add(_applyDefaultDataCommand);
                }
                return _applyDefaultDataCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ApplyDefaultDataCommand_CanExecute()
        {
            if (this.SelectedErrorItem == null)
            {
                return false;
            }

            if (!this.SelectedErrorItem.IsApplyDefaultDataAvailable)
            {
                return false;
            }

            return true;
        }

        private void ApplyDefaultDataCommand_Executed()
        {
            base.ShowWaitCursor(true);

            if (this.SelectedErrorItem != null)
            {
                ApplyDefaultData(this.SelectedErrorItem);

                //If this error items column is flagged as being invalid
                if (this.SelectedErrorGroup.InvalidColumnNumbers.Contains(this.SelectedErrorItem.ColumnNumber))
                {
                    //Apply default data to that column for every row
                    foreach (ImportFileDataRow row in this.ValidatedFileData.GetValidRows())
                    {
                        ApplyDefaultDataToRow(row, this.SelectedErrorItem);
                    }

                    //Remove any other errors with the same column number that have also been fixed
                    foreach (ValidationErrorItem item in
                        this.SelectedErrorGroup.Errors.Where(e => e.ColumnNumber == this.SelectedErrorItem.ColumnNumber).ToList())
                    {
                        this.SelectedErrorGroup.Errors.Remove(item);
                    }
                }
                else
                {
                    //Remove error item from the list
                    this.SelectedErrorGroup.Errors.Remove(this.SelectedErrorItem);
                }

                //Select new error item within the same group
                this.SelectedErrorItem = this.SelectedErrorGroup.Errors.FirstOrDefault();

                //[ISO-13318] Manually Update error information
                UpdateErrorInformation();
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region ResetToBoundGroupCommand

        private RelayCommand _resetToBoundGroupCommand;

        /// <summary>
        /// Reset the data to bound command for selected error group's errors
        /// </summary>
        public RelayCommand ResetToBoundGroupCommand
        {
            get
            {
                if (_resetToBoundGroupCommand == null)
                {
                    _resetToBoundGroupCommand =
                    new RelayCommand(p => ResetToBoundGroupCommand_Executed(), p => ResetToBoundGroupCommand_CanExecute())
                    {
                        FriendlyName = Message.BackstageDataManagement_Validation_ResetToBoundCommand,
                        FriendlyDescription = Message.BackstageDataManagement_Validation_ResetToBoundCommand_Desc
                    };
                    base.ViewModelCommands.Add(_resetToBoundGroupCommand);
                }
                return _resetToBoundGroupCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ResetToBoundGroupCommand_CanExecute()
        {
            return (this.SelectedErrorGroup != null);
        }

        private void ResetToBoundGroupCommand_Executed()
        {
            //Setup background worker with event handlers attached
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += ResetToBoundBackgroundWorker_DoWork;
            worker.RunWorkerCompleted += ResetToBoundBackgroundWorker_RunWorkerCompleted;
            worker.WorkerReportsProgress = false;
            worker.WorkerSupportsCancellation = false;
            worker.RunWorkerAsync();

            //Show the user the validation has commenced
            if (this.AttachedControl != null)
            {
                _busyDialog = new ModalBusy();
                _busyDialog.Description = Message.BackstageDataManagement_UpdateMessage;
                _busyDialog.IsDeterminate = false;
                App.ShowWindow(_busyDialog, /*isModal*/true);
            }
        }

        /// <summary>
        /// DoWork handler for the background worker
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResetToBoundBackgroundWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            List<Int32> processedColumns = new List<Int32>();

            foreach (ValidationErrorItem item in this.SelectedErrorGroup.Errors)
            {
                //If column is invalid
                if (this.SelectedErrorGroup.InvalidColumnNumbers.Contains(item.ColumnNumber))
                {
                    //[ISO-13318] Only enumerate through rows if column not already processed
                    if (!processedColumns.Contains(item.ColumnNumber))
                    {
                        //Apply default data to that column for every row
                        foreach (ImportFileDataRow row in this.ValidatedFileData.GetValidRows())
                        {
                            ResetDataRowToBound(row, item);
                        }

                        processedColumns.Add(item.ColumnNumber);
                    }
                }
                else
                {
                    //Fix Single error
                    ResetToBound(item);
                }
            }
        }

        /// <summary>
        /// Event handler for the background work completing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResetToBoundBackgroundWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            BackgroundWorker senderWorker = (BackgroundWorker)sender;
            senderWorker.DoWork -= ResetToBoundBackgroundWorker_DoWork;
            senderWorker.RunWorkerCompleted -= ResetToBoundBackgroundWorker_RunWorkerCompleted;

            //Hide busy
            if (_busyDialog != null)
            {
                _busyDialog.Close();
                _busyDialog = null;
            }

            if (e.Error != null)
            {
                //Throw error window
                ModalMessage errorMessageBox = new ModalMessage();
                errorMessageBox.Header = Message.BackstageDataManagement_ImportFileTitle;
                errorMessageBox.Description = Message.BackstageDataManagement_UpdateErrorMessage;
                errorMessageBox.ButtonCount = 1;
                errorMessageBox.Button1Content = Message.Generic_OK;
                errorMessageBox.DefaultButton = ModalMessageButton.Button1;
                App.ShowWindow(errorMessageBox, /*isModal*/true);
            }
            else
            {
                //Clear all errors
                this.SelectedErrorGroup.Errors.Clear();

                //[ISO-13318] Manually Update error information
                UpdateErrorInformation();
            }
        }

        #endregion

        #region ResetToBoundCommand

        private RelayCommand _resetToBoundCommand;
        /// <summary>
        /// Reset the data to bound command for selected error item
        /// </summary>
        public RelayCommand ResetToBoundCommand
        {
            get
            {
                if (_resetToBoundCommand == null)
                {
                    _resetToBoundCommand =
                    new RelayCommand(p => ResetToBoundCommand_Executed(), p => ResetToBoundCommand_CanExecute())
                    {
                        FriendlyName = Message.BackstageDataManagement_Validation_ResetToBoundCommand,
                        FriendlyDescription = Message.BackstageDataManagement_Validation_ResetToBoundCommand_Desc
                    };
                    base.ViewModelCommands.Add(_resetToBoundCommand);
                }
                return _resetToBoundCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ResetToBoundCommand_CanExecute()
        {
            return (this.SelectedErrorItem != null);
        }

        private void ResetToBoundCommand_Executed()
        {
            this.ShowWaitCursor(true);

            //Call reset to bound method
            ResetToBound(this.SelectedErrorItem);

            //If this error items column is flagged as being invalid
            if (this.SelectedErrorGroup.InvalidColumnNumbers.Contains(this.SelectedErrorItem.ColumnNumber))
            {
                //Apply default data to that column for every row
                foreach (ImportFileDataRow row in this.ValidatedFileData.GetValidRows())
                {
                    ResetDataRowToBound(row, this.SelectedErrorItem);
                }

                //Remove any other errors with the same column number that have also been fixed
                foreach (ValidationErrorItem item in
                    this.SelectedErrorGroup.Errors.Where(e => e.ColumnNumber == this.SelectedErrorItem.ColumnNumber).ToList())
                {
                    this.SelectedErrorGroup.Errors.Remove(item);
                }
            }
            else
            {
                //Remove error item from the list
                this.SelectedErrorGroup.Errors.Remove(this.SelectedErrorItem);
            }

            //Select new error item within the same group
            this.SelectedErrorItem = this.SelectedErrorGroup.Errors.FirstOrDefault();

            //[ISO-13318] Manually Update error information
            UpdateErrorInformation();

            base.ShowWaitCursor(false);
        }

        #endregion

        #region TruncateDataGroupCommand

        private RelayCommand _truncateDataGroupCommand;
        /// <summary>
        /// Truncate data command for selected error group's errors
        /// </summary>
        public RelayCommand TruncateDataGroupCommand
        {
            get
            {
                if (_truncateDataGroupCommand == null)
                {
                    _truncateDataGroupCommand =
                    new RelayCommand(p => TruncateDataGroupCommand_Executed(), p => TruncateDataGroupCommand_CanExecute())
                    {
                        FriendlyName = Message.BackstageDataManagement_Validation_TruncateDataCommand,
                        FriendlyDescription = Message.BackstageDataManagement_Validation_TruncateDataCommand_Desc
                    };
                    base.ViewModelCommands.Add(_truncateDataGroupCommand);
                }
                return _truncateDataGroupCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean TruncateDataGroupCommand_CanExecute()
        {
            return (this.SelectedErrorGroup != null) ? true : false;
        }

        private void TruncateDataGroupCommand_Executed()
        {
            //Setup background worker with event handlers attached
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += TruncateDataBackgroundWorker_DoWork;
            worker.RunWorkerCompleted += TruncateDataBackgroundWorker_RunWorkerCompleted;
            worker.WorkerReportsProgress = false;
            worker.WorkerSupportsCancellation = false;
            worker.RunWorkerAsync();

            //Show the user the validation has commenced
            if (this.AttachedControl != null)
            {
                _busyDialog = new ModalBusy();
                _busyDialog.Description = Message.BackstageDataManagement_UpdateMessage;
                _busyDialog.IsDeterminate = false;
                App.ShowWindow(_busyDialog, /*isModal*/true);
            }
        }

        /// <summary>
        /// DoWork handler for the background worker
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TruncateDataBackgroundWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            List<Int32> processedColumns = new List<Int32>();

            foreach (ValidationErrorItem item in this.SelectedErrorGroup.Errors)
            {
                //If column is invalid
                if (this.SelectedErrorGroup.InvalidColumnNumbers.Contains(item.ColumnNumber))
                {
                    //[ISO-13318] Only enumerate through rows if column not already processed
                    if (!processedColumns.Contains(item.ColumnNumber))
                    {
                        //Apply default data to that column for every row
                        foreach (ImportFileDataRow row in this.ValidatedFileData.GetValidRows())
                        {
                            //Reset each data row's column to bound
                            TruncateDataRow(row, item);
                        }

                        processedColumns.Add(item.ColumnNumber);
                    }
                }
                else
                {
                    //Fix Single error
                    TruncateData(item);
                }
            }
        }

        /// <summary>
        /// Event handler for the background work completing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TruncateDataBackgroundWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            BackgroundWorker senderworker = new BackgroundWorker();
            senderworker.DoWork -= TruncateDataBackgroundWorker_DoWork;
            senderworker.RunWorkerCompleted -= TruncateDataBackgroundWorker_RunWorkerCompleted;

            //Hide busy
            if (_busyDialog != null)
            {
                _busyDialog.Close();
                _busyDialog = null;
            }

            if (e.Error != null)
            {
                //Throw error window
                ModalMessage errorMessageBox = new ModalMessage();
                errorMessageBox.Header = Message.BackstageDataManagement_ImportFileTitle;
                errorMessageBox.Description = Message.BackstageDataManagement_UpdateErrorMessage;
                errorMessageBox.ButtonCount = 1;
                errorMessageBox.Button1Content = Message.Generic_OK;
                errorMessageBox.DefaultButton = ModalMessageButton.Button1;
                App.ShowWindow(errorMessageBox, /*isModal*/true);
            }
            else
            {
                //Clear all errors
                this.SelectedErrorGroup.Errors.Clear();

                //[ISO-13318] Manually Update error information
                UpdateErrorInformation();
            }
        }

        #endregion

        #region TruncateDataCommand

        private RelayCommand _truncateDataCommand;
        /// <summary>
        /// Truncate data command for selected error item
        /// </summary>
        public RelayCommand TruncateDataCommand
        {
            get
            {
                if (_truncateDataCommand == null)
                {
                    _truncateDataCommand =
                    new RelayCommand(p => TruncateDataCommand_Executed(), p => TruncateDataCommand_CanExecute())
                    {
                        FriendlyName = Message.BackstageDataManagement_Validation_TruncateDataCommand,
                        FriendlyDescription = Message.BackstageDataManagement_Validation_TruncateDataCommand_Desc
                    };
                    base.ViewModelCommands.Add(_truncateDataCommand);
                }
                return _truncateDataCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean TruncateDataCommand_CanExecute()
        {
            return (this.SelectedErrorItem != null);
        }

        private void TruncateDataCommand_Executed()
        {
            base.ShowWaitCursor(true);

            //Call truncate data method
            TruncateData(this.SelectedErrorItem);

            //If this error items column is flagged as being invalid
            if (this.SelectedErrorGroup.InvalidColumnNumbers.Contains(this.SelectedErrorItem.ColumnNumber))
            {
                //Apply default data to that column for every row
                foreach (ImportFileDataRow row in this.ValidatedFileData.GetValidRows())
                {
                    //Truncate data
                    TruncateDataRow(row, this.SelectedErrorItem);
                }

                //Remove any other errors with the same column number that have also been fixed
                foreach (ValidationErrorItem item in
                    this.SelectedErrorGroup.Errors.Where(e => e.ColumnNumber == this.SelectedErrorItem.ColumnNumber).ToList())
                {
                    this.SelectedErrorGroup.Errors.Remove(item);
                }
            }
            else
            {
                //Remove error item from the list
                this.SelectedErrorGroup.Errors.Remove(this.SelectedErrorItem);
            }

            //Select new error item within the same group
            this.SelectedErrorItem = this.SelectedErrorGroup.Errors.FirstOrDefault();

            //[ISO-13318] Manually Update error information
            UpdateErrorInformation();

            base.ShowWaitCursor(false);
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to changes in the worksheet errors collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorksheetErrors_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            CalculateErrorPercentage();
        }


        #endregion

        #region Methods

        /// <summary>
        /// Loads in the given process results
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="e"></param>
        public void LoadProcessResults<T>(ProcessCompletedEventArgs<T> e)
            where T : ValidateProcessBase<T>
        {
            this.ValidatedFileData = e.Process.ValidatedData;

            //Clear the worksheetErrors even if the new data has no errors
            if (_worksheetErrors.Count > 0)
            {
                _worksheetErrors.Clear();
            }

            //if the validation completed with no validation errors
            //then mark this window as complete
            if (e.Process.Errors.Count != 0)
            {
                //update the errors collection
                _worksheetErrors.AddRange(e.Process.Errors);
            }

            //update the display columnset
            if (_validatedfileDataColumnSet.Count > 0)
            {
                _validatedfileDataColumnSet.Clear();
            }

            //If there is no valid import data, set variable to put message in the preview grid
            ImportDataAvailable = this.ValidatedFileData.GetValidRows().Any();

            //add the row number column
            DataGridColumn rowNumCol = ExtendedDataGrid.CreateReadOnlyTextColumn(
                Message.BackstageDataManagement_RowNumberColHeader, ImportFileDataRow.RowNumberProperty.Name, HorizontalAlignment.Center);
            _validatedfileDataColumnSet.Add(rowNumCol);

            foreach (ImportFileDataColumn colDef in this.ValidatedFileData.Columns)
            {
                DataGridColumn col = ExtendedDataGrid.CreateReadOnlyTextColumn(colDef.Header,
                          String.Format("[{0}].CurrentValue", colDef.ColumnNumber), FileHelper.ConvertAlignment(colDef.CellAlignment));
                BindingOperations.SetBinding(col, DataGridColumn.HeaderProperty,
                    new Binding(ImportFileDataColumn.HeaderProperty.Name) { Source = colDef });
                _validatedfileDataColumnSet.Add(col);
            }
        }

        /// <summary>
        /// Returns true if all data is valid
        /// and we may proceed to the next import step
        /// </summary>
        public Boolean IsDataValid()
        {
            bool _valid = true;
            Int32 errorCount = this.WorksheetErrors.SelectMany(e => e.Errors.Where(f => f.ErrorType == ValidationErrorType.Error)).Count();

            // ensure that there are no errors and valid data rows still remain
            _valid = (errorCount == 0 && this.ValidatedFileData.GetValidRows().Any());
            return _valid;
        }

        /// <summary>
        /// Method to calculate the error percentage
        /// </summary>
        private void CalculateErrorPercentage()
        {
            Int32 totalRowCount = this.ValidatedFileData.GetValidRows().Count();

            //Merge all grouped errors into one error collection
            IEnumerable<ValidationErrorItem> allErrors = this.WorksheetErrors.SelectMany(p => p.Errors);

            //Get number of unique row errors
            Int32 distinctRowCount = allErrors.Select(p => p.RowNumber).Distinct().Count();

            //Calculate percentage - take account of case where there are no valid record left
            String percentRowCount =
                (totalRowCount > 0) ?
                String.Format("{0:P}", (Convert.ToDouble(distinctRowCount) / Convert.ToDouble(totalRowCount))) :
                String.Format("{0:P}", 0); //[ISO:13442] Default to 0

            //Update the error percentage text
            this.ErrorPercentageText = String.Format(Message.BackstageDataManagement_Validation_ErrorPercentagetext, percentRowCount);
        }

        /// <summary>
        /// Method to set the cell data to be the mappings default data
        /// </summary>
        private void ApplyDefaultData(ValidationErrorItem item)
        {
            if (item != null)
            {
                //Find corresponding row in the data
                Int32 rowNumber = item.RowNumber;
                ImportFileDataRow row = this.ValidatedFileData.GetValidRows().FirstOrDefault(r => r.RowNumber == rowNumber);

                //If corresponding row has been located
                if (row != null)
                {
                    //If the default is Null then need to set it to DBNull otherwise it is not changed
                    if (item.Mapping.PropertyDefault != null)
                    {
                        //Set data to be the default value for this mapping
                        row.Cells[item.ColumnNumber].CurrentValue = item.Mapping.PropertyDefault;
                    }
                    else
                    {
                        row.Cells[item.ColumnNumber].CurrentValue = DBNull.Value;
                    }
                }
            }
        }


        /// <summary>
        /// Method to set the cell data to be the mappings default data
        /// </summary>
        private void ApplyDefaultDataToRow(ImportFileDataRow row, ValidationErrorItem item)
        {
            if (item != null)
            {
                //If corresponding row has been located
                if (row != null)
                {
                    //If the default is Null then need to set it to DBNull otherwise it is not changed
                    if (item.Mapping.PropertyDefault != null)
                    {
                        //Set data to be the default value for this mapping
                        row.Cells[item.ColumnNumber].CurrentValue = item.Mapping.PropertyDefault;
                    }
                    else
                    {
                        row.Cells[item.ColumnNumber].CurrentValue = DBNull.Value;
                    }
                }
            }
        }

        /// <summary>
        /// Method to update the selected error groups error counter and
        /// windows error percentage text.
        /// </summary>
        private void UpdateErrorInformation()
        {
            //Update error group friendly name
            this.SelectedErrorGroup.UpdateOnErrorsChanged();

            if (this.SelectedErrorGroup.Errors.Count == 0)
            {
                //remove from the main list
                _worksheetErrors.Remove(this.SelectedErrorGroup);
            }

            //Update error percentage text
            this.CalculateErrorPercentage();
        }

        /// <summary>
        /// Method to reset to boun affecting a datarow's column
        /// </summary>
        /// <param name="row"></param>
        /// <param name="parentErrorItem"></param>
        private void ResetDataRowToBound(ImportFileDataRow row, ValidationErrorItem parentErrorItem)
        {
            //Convert affected data to correct data type (only numeric and datetime can have bound errors
            if (parentErrorItem.Mapping.PropertyType == typeof(DateTime) ||
                parentErrorItem.Mapping.PropertyType == typeof(DateTime?))
            {
                //Convert dataAffected to datetime
                DateTime data = Convert.ToDateTime(row[parentErrorItem.ColumnNumber].CurrentValue);

                //Get min and max date time values
                DateTime minDateTime = new DateTime(1900, 1, 1);
                DateTime maxDateTime = new DateTime(2079, 6, 6);

                //If date exceeds max
                if (data > maxDateTime)
                {
                    //set value to be upper bound limit
                    row[parentErrorItem.ColumnNumber].CurrentValue = maxDateTime;
                }
                //If date is less than min
                else if (data < minDateTime)
                {
                    //set value to be lower bound limit
                    row[parentErrorItem.ColumnNumber].CurrentValue = minDateTime;
                }
            }
            else
            {
                //Convert dataAffected to double
                double data = Convert.ToDouble(row[parentErrorItem.ColumnNumber].CurrentValue);

                //If date exceeds max
                if (data > parentErrorItem.Mapping.PropertyMax)
                {
                    //set value to be upper bound limit
                    row[parentErrorItem.ColumnNumber].CurrentValue = parentErrorItem.Mapping.PropertyMax;
                }
                //If date is less than min
                else if (data < parentErrorItem.Mapping.PropertyMin)
                {
                    //set value to be lower bound limit
                    row[parentErrorItem.ColumnNumber].CurrentValue = parentErrorItem.Mapping.PropertyMin;
                }
            }
        }

        /// <summary>
        /// Method to reset the bounds of a selected error item
        /// </summary>
        private void ResetToBound(ValidationErrorItem item)
        {
            if (item != null)
            {
                //Find corresponding row in the data
                Int32 rowNumber = item.RowNumber;
                ImportFileDataRow row = this.ValidatedFileData.Rows.FirstOrDefault(r => r.RowNumber == rowNumber);

                if (row != null)
                {
                    //Convert affected data to correct data type (only numeric and datetime can have bound errors
                    if (item.Mapping.PropertyType == typeof(DateTime) ||
                        item.Mapping.PropertyType == typeof(DateTime?))
                    {
                        //Convert dataAffected to datetime
                        DateTime data = Convert.ToDateTime(item.DataAffected);

                        //Get min and max date time values
                        DateTime minDateTime = new DateTime(1900, 1, 1);
                        DateTime maxDateTime = new DateTime(2079, 6, 6);

                        //If date exceeds max
                        if (data > maxDateTime)
                        {
                            //set value to be upper bound limit
                            row.Cells[item.ColumnNumber].CurrentValue = maxDateTime;
                        }
                        //If date is less than min
                        else if (data < minDateTime)
                        {
                            //set value to be lower bound limit
                            row.Cells[item.ColumnNumber].CurrentValue = minDateTime;
                        }
                    }
                    else
                    {
                        //Convert dataAffected to double
                        Double data = Convert.ToDouble(item.DataAffected);

                        //If date exceeds max
                        if (data > item.Mapping.PropertyMax)
                        {
                            //set value to be upper bound limit
                            row.Cells[item.ColumnNumber].CurrentValue = item.Mapping.PropertyMax;
                        }
                        //If date is less than min
                        else if (data < item.Mapping.PropertyMin)
                        {
                            //set value to be lower bound limit
                            row.Cells[item.ColumnNumber].CurrentValue = item.Mapping.PropertyMin;
                        }

                    }
                }
            }
        }


        private void TruncateDataRow(ImportFileDataRow row, ValidationErrorItem parentErrorItem)
        {
            //If data is string
            if (parentErrorItem.Mapping.PropertyType == typeof(String))
            {
                //Get original value
                String stringValue = row[parentErrorItem.ColumnNumber].CurrentValue.ToString();

                //Convert max property to int32
                Int32 maxValue = Convert.ToInt32(parentErrorItem.Mapping.PropertyMax);

                //If upper limit breached
                if (stringValue.Length > maxValue)
                {
                    //truncate the string
                    row[parentErrorItem.ColumnNumber].CurrentValue = stringValue.Substring(0, maxValue);
                }
                //If lower limit breached
                else if (stringValue.Length < parentErrorItem.Mapping.PropertyMin)
                {
                    row[parentErrorItem.ColumnNumber].CurrentValue = parentErrorItem.Mapping.PropertyMin;
                }
            }
        }

        /// <summary>
        /// Method to truncate the data within a cell to match the mapping's limit
        /// </summary>
        private void TruncateData(ValidationErrorItem item)
        {
            if (item != null)
            {
                //Find corresponding row in the data
                Int32 rowNumber = item.RowNumber;
                ImportFileDataRow row = this.ValidatedFileData.Rows.FirstOrDefault(r => r.RowNumber == rowNumber);

                if (row != null)
                {
                    //If data is string
                    if (item.Mapping.PropertyType == typeof(string))
                    {
                        //Convert max property to int32
                        Int32 maxValue = Convert.ToInt32(item.Mapping.PropertyMax);

                        if (item.DataAffected.Length > maxValue)
                        {
                            //truncate the string
                            row.Cells[item.ColumnNumber].CurrentValue = item.DataAffected.Substring(0, maxValue);
                        }
                        else if (item.DataAffected.Length < item.Mapping.PropertyMin)
                        {
                            row.Cells[item.ColumnNumber].CurrentValue = item.Mapping.PropertyMin;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Remove to remove any matching row numbers for use with the ignore row command
        /// </summary>
        /// <param name="rowNumber"></param>
        private void RemoveOtherErrorWithMatchingRowNumber(int rowNumber)
        {
            foreach (ValidationErrorGroup groupedError in this.WorksheetErrors.ToList())
            {
                List<ValidationErrorItem> errorsToRemove = new List<ValidationErrorItem>();

                //Add errors with matching row number
                errorsToRemove.AddRange(groupedError.Errors.Where(p => p.RowNumber == rowNumber));

                //Enumerate through remove list
                foreach (ValidationErrorItem error in errorsToRemove)
                {
                    //Remove from grouped error row errors
                    groupedError.Errors.Remove(error);
                }


                if (groupedError.Errors.Count == 0)
                {
                    //remove completely if no errors left
                    _worksheetErrors.Remove(groupedError);
                }
                else
                {
                    //[ISO-13442]Refresh group header
                    groupedError.UpdateOnErrorsChanged();
                }
            }
        }

        /// <summary>
        /// Removes all remaining errors for rows related to passed error row IDs of a given group to ignore; for use with the ignore rows group command
        /// </summary>
        /// <param name="rowNumber"></param>
        private void RemoveOtherErrorWithMatchingRowNumbers(List<Int32> errorRowIds)
        {
            foreach (ValidationErrorGroup groupedError in this.WorksheetErrors.ToList())
            {
                List<ValidationErrorItem> otherErrorsToRemove = new List<ValidationErrorItem>();

                //Add errors with matching row number
                otherErrorsToRemove.AddRange(groupedError.Errors.Where(p => errorRowIds.Contains(p.RowNumber)));

                //Enumerate through remove list
                foreach (ValidationErrorItem error in otherErrorsToRemove)
                {
                    //Remove from grouped error row errors
                    groupedError.Errors.Remove(error);
                }

                if (groupedError.Errors.Count == 0)
                {
                    //remove completely if no errors left
                    _worksheetErrors.Remove(groupedError);
                }
                else
                {
                    //[ISO-13442]Refresh group header
                    groupedError.UpdateOnErrorsChanged();
                }
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _worksheetErrors.BulkCollectionChanged -= WorksheetErrors_BulkCollectionChanged;

                    _validatedFileData = null;
                    _worksheetErrors.Clear();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
