﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (GFS 1.0)
// GFS-13485 : L.Hodson
//  Created
// GFS-13720 : N.Foster
//  Updated application framework
#endregion
#region Version History: (GFS 2.1.3)
// GFS-23149 : M.Toomer
//  Change to how preview item is found and brought into view
#endregion
#region Version History: (CCM V8)
// V8-25395 : A.Probyn
//      Copied from GFS
#endregion
#endregion

using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

using Galleria.Framework.Imports;
using Galleria.Ccm.Editor.Client.Wpf.Common;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow.DataManagement
{
    /// <summary>
    /// Interaction logic for ImportDefinitionValidation.xaml
    /// </summary>
    public partial class ImportDataValidation : UserControl
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ImportDataValidationViewModel), typeof(ImportDataValidation),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public ImportDataValidationViewModel ViewModel
        {
            get { return (ImportDataValidationViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ImportDataValidation senderControl = (ImportDataValidation)obj;

            if (e.OldValue != null)
            {
                ImportDataValidationViewModel oldModel = (ImportDataValidationViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                ImportDataValidationViewModel newModel = (ImportDataValidationViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }
        }



        #endregion

        #endregion

        #region Constructor

        public ImportDataValidation(ImportDataValidationViewModel viewModel)
        {
            InitializeComponent();

            this.ViewModel = viewModel;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to property changes in the viewModel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ImportDataValidationViewModel.SelectedErrorItemProperty.Path)
            {
                BringSelectedErrorItemIntoView();
            }
        }

        /// <summary>
        /// Event handler for the error row entry being expanded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xRowExpander_Expanded(object sender, RoutedEventArgs e)
        {
            //Cast object to dependency
            DependencyObject obj = (DependencyObject)e.OriginalSource;

            //Find the objects parent in the visual tree
            obj = VisualTreeHelper.GetParent(obj);

            //Find the visual ancester of the parent
            DataGridRow dataGridRow = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<DataGridRow>(obj);

            //If data grid row is found
            if (dataGridRow != null)
            {
                dataGridRow.DetailsVisibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Event handler for the error row entry being collapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xRowExpander_Collapsed(object sender, RoutedEventArgs e)
        {
            //Cast object to dependency
            DependencyObject obj = (DependencyObject)e.OriginalSource;

            //Find the objects parent in the visual tree
            obj = VisualTreeHelper.GetParent(obj);

            //Find the visual ancester of the parent
            DataGridRow dataGridRow = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<DataGridRow>(obj);

            //If data grid row is found
            if (dataGridRow != null)
            {
                dataGridRow.DetailsVisibility = Visibility.Collapsed;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Reponds to a change of selected error item -
        /// scrolls to the cell containing the error
        /// </summary>
        private void BringSelectedErrorItemIntoView()
        {
            ValidationErrorItem errorItem = this.ViewModel.SelectedErrorItem;

            //Check if attached control is not null
            if (errorItem != null)
            {
                int columnIndex = (errorItem.ColumnNumber + 1); //-1 for 0 based index, +2 to compensate for first 2 columns

                //[ISO-13318] Actually find the row in question and get its index
                ImportFileDataRow matchingRow = this.ViewModel.ValidatedFileData.Rows.FirstOrDefault(r => r.RowNumber == errorItem.RowNumber);

                if (matchingRow != null)
                {
                    // the items source is the rows collection -> we can select item directly
                    this.validationDataGrid.SelectedItem = matchingRow;

                    //Check indexes are valid
                    if (columnIndex <= (this.validationDataGrid.Columns.Count - 1))
                    {
                        //Get column at the index
                        DataGridColumn viewColumn = this.validationDataGrid.Columns[columnIndex];
                        // Scroll to view cell
                        this.validationDataGrid.ScrollIntoView(matchingRow, viewColumn);//this.validationDataGrid.Items[rowIndex], viewColumn);
                    }
                }
            }
        }

        #endregion
    }
}
