﻿<!--Header Information-->
<!--Copyright © Galleria RTS Ltd 2011.-->
<!-- Version History : (1.0)
    GFS-13485 : L.Hodson ~ Created
    GFS-13913 : L.Hodson ~ Row details col 1 is now extended
    GFS-14303 : J.Freeborough ~ Added message to bottom grid if no import data is left
    GFS-15012 : N.Donohoe ~ Updated how the invalid column text is formatted and removed the line below the grid. Also gave the
          error grid a border as it looked weird without one.
    GFS-16821 : N.Donohoe ~ Changed Error Description column width to correctly show longer messages
-->
<!--Copyright © Galleria RTS Ltd 2013-->
<!-- Version History : (2.1.3)
    GFS-21656 : M.Toomer ~ Added row count related text
-->

<UserControl x:Class="Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow.DataManagement.ImportDataValidation"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:lg="clr-namespace:Galleria.Ccm.Editor.Client.Wpf.Resources.Language"
             xmlns:g="clr-namespace:Galleria.Framework.Controls.Wpf;assembly=Galleria.Framework.Controls.Wpf"
             xmlns:root="clr-namespace:Galleria.Ccm.Editor.Client.Wpf"
             xmlns:local="clr-namespace:Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow.DataManagement"
             xmlns:sys="clr-namespace:System;assembly=mscorlib"
             xmlns:common="clr-namespace:Galleria.Ccm.Editor.Client.Wpf.Common"
             x:Name="rootControl"
             >
    <UserControl.Resources>
        <!--Override for header style with no minimum width-->
        <Style x:Key="DataManagement_tmpNarrowHeaderOverrideStyle" TargetType="{x:Type DataGridColumnHeader}" >
            <Setter Property="VerticalContentAlignment" Value="Center" />
            <Setter Property="Background" Value="Transparent"/>
            <Setter Property="BorderBrush" Value="{StaticResource {x:Static g:ResourceKeys.ThemeColour_BrushDataGridHeaderBorder}}" />
            <Setter Property="BorderThickness" Value="0,1,0,1" />
            <Setter Property="SnapsToDevicePixels" Value="True" />
            <Setter Property="HorizontalContentAlignment" Value="Center" />
            <Setter Property="MinWidth" Value="0" />
            <Setter Property="MinHeight" Value="30" />
            <Setter Property="Cursor" Value="Hand" />
            <Style.Triggers>
                <Trigger Property="IsMouseOver" Value="True">
                    <Setter Property="Background" 
									Value="{StaticResource {x:Static g:ResourceKeys.ThemeColour_BrushDataGridHeaderHovered}}" />
                </Trigger>
                <Trigger Property="IsPressed" Value="True">
                    <Setter Property="BorderBrush" 
									Value="{StaticResource {x:Static g:ResourceKeys.ThemeColour_BrushDataGridHeaderPressed}}" />
                </Trigger>
            </Style.Triggers>
        </Style>


        <DataTemplate x:Key="ImportDataValidation_DTRowDetails">
            <!-- Details Grid Only Visible if row expanded-->
            <g:ExtendedDataGrid Margin="30,0,0,0" MaxHeight="150" BorderThickness="1,0,1,1"
                                        HorizontalContentAlignment="Stretch" VerticalContentAlignment="Stretch"
                                        ItemsSourceExtended="{Binding Path=Errors}"
                                        CanUserReorderColumns="False" 
                                        CanUserDragRows="False" ExtendColumn="1"
                                        CanUserResizeColumns="True" CanUserSelectRows="True"
                                        SelectedItem="{Binding Path=ViewModel.SelectedErrorItem,Mode=TwoWay,UpdateSourceTrigger=PropertyChanged, ElementName=rootControl}"
                                        Visibility="{Binding Path=Errors.Count, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToVisibility}}, ConverterParameter=notEquals:0}">
                <g:ExtendedDataGrid.Columns>
                    <DataGridTextColumn Header="{x:Static lg:Message.BackstageDataManagement_Validation_ErrorRowHeader}"
                                                            Width="40" MinWidth="10"
                                                            Binding="{Binding Path=RowNumber}"
                                                            IsReadOnly="True"
                                                            HeaderStyle="{StaticResource DataManagement_tmpNarrowHeaderOverrideStyle}"
                                                            />
                    <DataGridTextColumn Header="{x:Static lg:Message.BackstageDataManagement_Validation_ErrorColumnHeader}"
                                                            Width="100" MinWidth="10"
                                                            IsReadOnly="True"
                                                            Binding="{Binding Path=ColumnName}"
                                                            HeaderStyle="{StaticResource DataManagement_tmpNarrowHeaderOverrideStyle}"/>
                    <DataGridTextColumn Header="{x:Static lg:Message.BackstageDataManagement_Validation_ErrorDataAffectedHeader}"
                                                            IsReadOnly="True"
                                                            Width="140" MinWidth="10"
                                                            Binding="{Binding Path=DataAffected}"
                                                            />
                    <DataGridTextColumn Header="{x:Static lg:Message.BackstageDataManagement_Validation_ErrorProblemDescHeader}"
                                                            IsReadOnly="True"
                                                            MinWidth="200"
                                                            Binding="{Binding Path=ProblemDescription}" />

                    <DataGridTemplateColumn Header="{x:Static lg:Message.BackstageDataManagement_Validation_ErrorGroupActionHeader}"
                                                        IsReadOnly="True"
                                                            MinWidth="200">
                        <DataGridTemplateColumn.CellTemplate>
                            <DataTemplate>
                                <StackPanel Orientation="Horizontal" >
                                    <StackPanel Visibility="{Binding Path=IsIgnoreRowsAvailable, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToVisibility}}, ConverterParameter=equals:True}">
                                        <Button DataContext="{Binding Path=ViewModel, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type local:ImportDataValidation}}}"
                                                                    Command="{Binding IgnoreRowCommand}"
                                                                    Style="{StaticResource {x:Static root:ResourceKeys.Buttons_StyHyperlinkButton}}"
                                                                    OverridesDefaultStyle="True">
                                            <TextBlock Text="{Binding IgnoreRowCommand.FriendlyName}" Style="{StaticResource Text_StyDataManagementErrorCommands}"/>
                                            <Button.ToolTip>
                                                <TextBlock Text="{Binding IgnoreRowCommand.FriendlyDescription}"/>
                                            </Button.ToolTip>
                                        </Button>
                                    </StackPanel>
                                    <StackPanel Visibility="{Binding Path=IsApplyDefaultDataAvailable, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToVisibility}}, ConverterParameter=equals:True}">
                                        <Button DataContext="{Binding Path=ViewModel, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type local:ImportDataValidation}}}"
                                                                    Command="{Binding ApplyDefaultDataCommand}"
                                                                    Style="{StaticResource {x:Static root:ResourceKeys.Buttons_StyHyperlinkButton}}"
                                                                    OverridesDefaultStyle="True">
                                            <TextBlock Text="{Binding ApplyDefaultDataCommand.FriendlyName}" Style="{StaticResource Text_StyDataManagementErrorCommands}"/>
                                            <Button.ToolTip>
                                                <TextBlock Text="{Binding ApplyDefaultDataCommand.FriendlyDescription}"/>
                                            </Button.ToolTip>
                                        </Button>
                                    </StackPanel>
                                    <StackPanel Visibility="{Binding Path=IsResetToBoundAvailable, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToVisibility}}, ConverterParameter=equals:True}">
                                        <Button DataContext="{Binding Path=ViewModel, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type local:ImportDataValidation}}}"
                                                                    Command="{Binding ResetToBoundCommand}"
                                                                    Style="{StaticResource {x:Static root:ResourceKeys.Buttons_StyHyperlinkButton}}"
                                                                    OverridesDefaultStyle="True">
                                            <TextBlock Text="{Binding ResetToBoundCommand.FriendlyName}" Style="{StaticResource Text_StyDataManagementErrorCommands}"/>
                                            <Button.ToolTip>
                                                <TextBlock Text="{Binding ResetToBoundCommand.FriendlyDescription}"/>
                                            </Button.ToolTip>
                                        </Button>
                                    </StackPanel>
                                    <StackPanel Visibility="{Binding Path=IsTruncateDataAvailable, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToVisibility}}, ConverterParameter=equals:True}">
                                        <Button DataContext="{Binding Path=ViewModel, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type local:ImportDataValidation}}}"
                                                                    Command="{Binding TruncateDataCommand}"
                                                                    Style="{StaticResource {x:Static root:ResourceKeys.Buttons_StyHyperlinkButton}}"
                                                                    OverridesDefaultStyle="True">
                                            <TextBlock Text="{Binding TruncateDataCommand.FriendlyName}" Style="{StaticResource Text_StyDataManagementErrorCommands}"/>
                                            <Button.ToolTip>
                                                <TextBlock Text="{Binding TruncateDataCommand.FriendlyDescription}"/>
                                            </Button.ToolTip>
                                        </Button>
                                    </StackPanel>
                                </StackPanel>
                            </DataTemplate>
                        </DataGridTemplateColumn.CellTemplate>
                    </DataGridTemplateColumn>
                </g:ExtendedDataGrid.Columns>
            </g:ExtendedDataGrid>
        </DataTemplate>

    </UserControl.Resources>

    <Grid DataContext="{Binding Path=ViewModel, ElementName=rootControl}" Margin="10">
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="2*"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="*"/>
        </Grid.RowDefinitions>
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="0.5*" />
            <ColumnDefinition Width="0.5*" />
        </Grid.ColumnDefinitions>

        <TextBlock Text ="{x:Static lg:Message.BackstageDataManagement_Validation_ViewItemsTitle}"
                    Grid.Row="0" Grid.ColumnSpan="2" VerticalAlignment="Center" HorizontalAlignment="Left" Margin="0,15,0,5" />

        <TextBlock Text ="{x:Static lg:Message.BackstageDataManagement_Validation_ProblemsTitle}"
                           Grid.Row="1" Grid.ColumnSpan="2" VerticalAlignment="Center" HorizontalAlignment="Left" Margin="0,10,0,5"  />

        <g:ExtendedDataGrid Grid.Row="2" Grid.ColumnSpan="2"
                                     Margin="0" 
                                     BorderThickness="1"
                                     ItemsSourceExtended="{Binding Path={x:Static local:ImportDataValidationViewModel.WorksheetErrorsProperty}}"
                                     HorizontalContentAlignment="Stretch"
                                     VerticalContentAlignment="Top"
                                     CanUserReorderColumns="False" 
                                     CanUserDragRows="False"
                                     CanUserResizeColumns="False"
                                     RowDetailsVisibilityMode="Collapsed"
                                     CanUserSelectRows="False"
                                     RowDetailsTemplate="{StaticResource ImportDataValidation_DTRowDetails}"
                                     SelectedItem="{Binding Path={x:Static local:ImportDataValidationViewModel.SelectedErrorGroupProperty},Mode=TwoWay,UpdateSourceTrigger=PropertyChanged}">
            <g:ExtendedDataGrid.Columns>

                <DataGridTemplateColumn Width="35">
                    <DataGridTemplateColumn.CellTemplate>
                        <DataTemplate>
                            <Expander HorizontalAlignment="Stretch" HorizontalContentAlignment="Center"
                                      Expanded="xRowExpander_Expanded" 
                                              Collapsed="xRowExpander_Collapsed"
                                      />
                        </DataTemplate>
                    </DataGridTemplateColumn.CellTemplate>
                </DataGridTemplateColumn>

                <DataGridTextColumn Header="{x:Static lg:Message.BackstageDataManagement_Validation_ErrorGroupDescriptionHeader}"
                                    IsReadOnly="True"
                                    Width="300"
                                    Binding="{Binding Path=Description}" />

                <DataGridTemplateColumn Header="{x:Static lg:Message.BackstageDataManagement_Validation_ErrorGroupProblemsHeader}"
                                        IsReadOnly="True"
                                        Width="200">
                    <DataGridTemplateColumn.CellTemplate>
                        <DataTemplate>
                            <StackPanel>
                                <TextBlock Text="{Binding Path=ErrorFriendlyName}">
                                    <TextBlock.Style>
                                        <Style TargetType="TextBlock">
                                            <Style.Triggers>
                                                <DataTrigger Binding="{Binding Path=ContainsInvalidColumnErrorsWarnings}" Value="True">
                                                    <Setter Property="TextDecorations">
                                                        <Setter.Value>
                                                            <TextDecorationCollection>
                                                                <TextDecoration
                                                                    PenThicknessUnit="FontRecommended">
                                                                    <TextDecoration.Pen>
                                                                        <Pen Brush="Red" Thickness="1">
                                                                            <Pen.DashStyle>
                                                                                <DashStyle Dashes="5"/>
                                                                            </Pen.DashStyle>
                                                                        </Pen>
                                                                    </TextDecoration.Pen>
                                                                </TextDecoration>
                                                            </TextDecorationCollection>
                                                        </Setter.Value>
                                                    </Setter>
                                                    <Setter Property="Foreground" Value="Red"/>
                                                </DataTrigger>
                                            </Style.Triggers>
                                        </Style>
                                    </TextBlock.Style>
                                    <TextBlock.ToolTip>
                                        <ToolTip Visibility="{Binding Path=ContainsInvalidColumnErrorsWarnings, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToVisibility}}, ConverterParameter=equals:True}">
                                            <TextBlock Text="{x:Static lg:Message.BackstageDataManagement_Validation_InvalidColumnTooltip}"/>
                                        </ToolTip>
                                    </TextBlock.ToolTip>
                                </TextBlock>
                            </StackPanel>
                        </DataTemplate>
                    </DataGridTemplateColumn.CellTemplate>
                </DataGridTemplateColumn>

                <DataGridTemplateColumn Header="{x:Static lg:Message.BackstageDataManagement_Validation_ErrorGroupActionHeader}"
                                        IsReadOnly="True"  MinWidth="200" >
                    <DataGridTemplateColumn.CellTemplate>
                        <DataTemplate>
                            <StackPanel Orientation="Horizontal">

                                <Button Visibility="{Binding Path=IsIgnoreRowsAvailable, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToVisibility}}, ConverterParameter=equals:True}"              
                                        DataContext="{Binding Path=ViewModel, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type local:ImportDataValidation}}}"
                                        Command="{Binding IgnoreRowsGroupCommand}"
                                        Style="{StaticResource {x:Static root:ResourceKeys.Buttons_StyHyperlinkButton}}" >
                                    <TextBlock Text="{Binding IgnoreRowsGroupCommand.FriendlyName}" 
                                               Style="{StaticResource {x:Static root:ResourceKeys.Text_StyDataManagementErrorCommands}}"/>
                                    <Button.ToolTip>
                                        <TextBlock Text="{Binding IgnoreRowsGroupCommand.FriendlyDescription}"/>
                                    </Button.ToolTip>
                                </Button>

                                <StackPanel Visibility="{Binding Path=IsApplyDefaultDataAvailable, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToVisibility}}, ConverterParameter=equals:True}">
                                    <Button DataContext="{Binding Path=ViewModel, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type local:ImportDataValidation}}}"
                                                    Command="{Binding ApplyDefaultDataGroupCommand}"
                                                    Style="{StaticResource {x:Static root:ResourceKeys.Buttons_StyHyperlinkButton}}"
                                                    OverridesDefaultStyle="True">
                                        <TextBlock Text="{Binding ApplyDefaultDataGroupCommand.FriendlyName}" Style="{StaticResource {x:Static root:ResourceKeys.Text_StyDataManagementErrorCommands}}"/>
                                        <Button.ToolTip>
                                            <TextBlock Text="{Binding ApplyDefaultDataGroupCommand.FriendlyDescription}"/>
                                        </Button.ToolTip>
                                    </Button>
                                </StackPanel>
                                <StackPanel Visibility="{Binding Path=IsResetToBoundAvailable, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToVisibility}}, ConverterParameter=equals:True}">
                                    <Button DataContext="{Binding Path=ViewModel, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type local:ImportDataValidation}}}"
                                                    Command="{Binding ResetToBoundGroupCommand}"
                                                    Style="{StaticResource {x:Static root:ResourceKeys.Buttons_StyHyperlinkButton}}"
                                                    OverridesDefaultStyle="True">
                                        <TextBlock Text="{Binding ResetToBoundGroupCommand.FriendlyName}" Style="{StaticResource Text_StyDataManagementErrorCommands}"/>
                                        <Button.ToolTip>
                                            <TextBlock Text="{Binding ResetToBoundGroupCommand.FriendlyDescription}"/>
                                        </Button.ToolTip>
                                    </Button>
                                </StackPanel>
                                <StackPanel Visibility="{Binding Path=IsTruncateDataAvailable, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToVisibility}}, ConverterParameter=equals:True}">
                                    <Button DataContext="{Binding Path=ViewModel, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type local:ImportDataValidation}}}"
                                                Command="{Binding TruncateDataGroupCommand}"
                                                Style="{StaticResource {x:Static root:ResourceKeys.Buttons_StyHyperlinkButton}}"
                                                OverridesDefaultStyle="True">
                                        <TextBlock Text="{Binding TruncateDataGroupCommand.FriendlyName}" Style="{StaticResource {x:Static root:ResourceKeys.Text_StyDataManagementErrorCommands}}"/>
                                        <Button.ToolTip>
                                            <TextBlock Text="{Binding TruncateDataGroupCommand.FriendlyDescription}"/>
                                        </Button.ToolTip>
                                    </Button>
                                </StackPanel>
                            </StackPanel>
                        </DataTemplate>
                    </DataGridTemplateColumn.CellTemplate>
                </DataGridTemplateColumn>
            </g:ExtendedDataGrid.Columns>
        </g:ExtendedDataGrid>

        <TextBlock Grid.Row="3" Grid.Column="0" HorizontalAlignment="Left"                 
                Text="{Binding Path={x:Static local:ImportDataValidationViewModel.OriginalRowCountTextProperty}}"/>

        <TextBlock Grid.Row="4"  Grid.Column="0" HorizontalAlignment="Left"
                Text="{Binding Path={x:Static local:ImportDataValidationViewModel.RowsToProcessTextProperty}}"/>

        <TextBlock Grid.Row="4"  Grid.Column="1"  HorizontalAlignment="Right"
                Text="{Binding Path={x:Static local:ImportDataValidationViewModel.IgnoredRowsCountTextProperty}}"/>

        <TextBlock Grid.Row="3"  Grid.Column="1" HorizontalAlignment="Right"
                Text="{Binding Path={x:Static local:ImportDataValidationViewModel.ErrorPercentageTextProperty}}"/>


        <TextBlock Text ="{x:Static lg:Message.BackstageDataManagement_Validation_DataPreviewTitle}"
                           Grid.Row="5" VerticalAlignment="Center" HorizontalAlignment="Left" Margin="0,15,0,5"
                           />


        <g:ExtendedDataGrid x:Name="validationDataGrid" 
                            Grid.Row="6" Grid.ColumnSpan="2" Margin="0,15,0,5"
                            ItemsSourceExtended="{Binding Path=ValidatedFileData.Rows}" 
                            CanUserReorderColumns="False" 

                            CanUserDragRows="False"
                            CanUserResizeColumns="False"
                            CanUserSelectRows="True"
                            ColumnSet="{Binding Path={x:Static local:ImportDataValidationViewModel.ValidatedFileDataColumnSetProperty}}"
                            CanUserGroupBy="True"
                            >

        </g:ExtendedDataGrid>

        <!-- Message displayed in preview data grid if no import data remains -->
        <Grid Grid.Row="5" Grid.ColumnSpan="2" Visibility="{Binding Path=ImportDataAvailable, Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToVisibility}}, 
                                        ConverterParameter=equals:False}"
                  HorizontalAlignment="Center" VerticalAlignment="Center">
            <Grid.RowDefinitions>
                <RowDefinition Height="Auto"></RowDefinition>
            </Grid.RowDefinitions>

            <TextBlock Grid.Row="0" Margin="4" Padding="4" 
                Text="{x:Static lg:Message.BackstageDataManagement_NoImportDataAvailable}"
                HorizontalAlignment="Center" VerticalAlignment="Center"
                />
        </Grid>

    </Grid>
</UserControl>
