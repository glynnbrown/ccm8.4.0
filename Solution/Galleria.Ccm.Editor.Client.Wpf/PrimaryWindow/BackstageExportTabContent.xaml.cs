﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-30620 : M.Pettit
//  Created
// V8-32823  : J.Pickup
//  Export visbiltity work
// V8-32810 : L.Ineson
// Changes to support pdf export.
#endregion
#endregion

using System;
using System.Windows;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for BackstageExportTabContent.xaml
    /// </summary>
    public sealed partial class BackstageExportTabContent
    {
        #region Fields

        private Int32? _lastProductGroupIdScrolledIntoView;

        #endregion

        #region Properties

        public Visibility SpacemanExportVisibility
        {
            get
            {
                if (App.ViewState.IsExportSpacemanAvailable)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
        }

        public Visibility ApolloExportVisibility
        {
            get
            {
                if (App.ViewState.IsExportApolloAvailable)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
        }

        public Visibility JDAExportVisibility
        {
            get
            {
                if (App.ViewState.IsExportJDAAvailable)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
        }


        #region ViewModel Property

        /// <summary>
        /// Viewmodel dependency property definition
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(BackstageExportTabViewModel), typeof(BackstageExportTabContent),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Called whenever the viewmodel property changes.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            //  Validate the sender.
            var senderControl = obj as BackstageExportTabContent;
            if (senderControl == null) return;

            //  Unsubscribe from the old model, if there was one.
            var oldModel = e.OldValue as BackstageExportTabViewModel;
            if (oldModel != null)
            {
                oldModel.UnregisterControl();

                senderControl.Resources.Remove("SetPdfPrintTemplateCommand");
                senderControl.Resources.Remove("SetPdfPrintTemplateFromRepositoryCommand");
                senderControl.Resources.Remove("SetPdfPrintTemplateFromFileCommand");
            }

            //  Subscribe to the new model, if there is one.
            var newModel = e.NewValue as BackstageExportTabViewModel;
            if (newModel != null)
            {
                newModel.RegisterControl(senderControl);

                senderControl.Resources.Add("SetPdfPrintTemplateCommand", newModel.SetPdfPrintTemplateCommand);
                senderControl.Resources.Add("SetPdfPrintTemplateFromRepositoryCommand", newModel.SetPdfPrintTemplateFromRepositoryCommand);
                senderControl.Resources.Add("SetPdfPrintTemplateFromFileCommand", newModel.SetPdfPrintTemplateFromFileCommand);
            }
        }

        /// <summary>
        /// Gets/Sets the viewmodel context.
        /// </summary>
        public BackstageExportTabViewModel ViewModel
        {
            get { return (BackstageExportTabViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region IsActiveProperty

        public static readonly DependencyProperty IsActiveProperty
            = DependencyProperty.Register("IsActive", typeof(Boolean), typeof(BackstageExportTabContent),
            new PropertyMetadata(false, OnIsActivePropertyChanged));

        private static void OnIsActivePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BackstageExportTabContent senderControl = (BackstageExportTabContent)obj;
            if (senderControl.ViewModel != null)
            {
                senderControl.ViewModel.IsActive = (Boolean)e.NewValue;
            }
        }

        /// <summary>
        /// Gets/Sets whether this tab is active.
        /// </summary>
        public Boolean IsActive
        {
            get { return (Boolean)GetValue(IsActiveProperty); }
            set { SetValue(IsActiveProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public BackstageExportTabContent()
        {
            //dont initialize until visible.
            this.IsVisibleChanged += BackstageExportTabContent_IsVisibleChanged;
        }

        /// <summary>
        /// Called when this first becomes visible.
        /// </summary>
        private void BackstageExportTabContent_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.IsVisible)
            {
                this.IsVisibleChanged -= BackstageExportTabContent_IsVisibleChanged;

                InitializeComponent();
                this.ViewModel = new BackstageExportTabViewModel();
            }
        }

        #endregion

    }
}
