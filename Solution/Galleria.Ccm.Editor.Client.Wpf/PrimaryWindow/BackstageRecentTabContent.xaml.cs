﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24261 : L.Hodson ~ Created
#endregion
#region Version History : CCM811
// V8-30590 : A.Kuszyk
//  Removed path converter
#endregion
#endregion

using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for BackstageRecentTabContent.xaml
    /// </summary>
    public sealed partial class BackstageRecentTabContent : UserControl
    {
        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(BackstageRecentTabViewModel), typeof(BackstageRecentTabContent),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BackstageRecentTabContent senderControl = (BackstageRecentTabContent)obj;

            if (e.OldValue != null)
            {
                BackstageRecentTabViewModel oldModel = (BackstageRecentTabViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                BackstageRecentTabViewModel newModel = (BackstageRecentTabViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }


        /// <summary>
        /// Gets/Sets the viewmodel context.
        /// </summary>
        public BackstageRecentTabViewModel ViewModel
        {
            get { return (BackstageRecentTabViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public BackstageRecentTabContent()
        {
            InitializeComponent();

            this.ViewModel = new BackstageRecentTabViewModel();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever an item it right clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Item_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            Panel senderControl = (Panel)sender;

            RecentPlanogram item = senderControl.DataContext as RecentPlanogram;
            if (item != null)
            {
                //create the context menu
                ContextMenu cm = new ContextMenu();

                //open
                if(item.Type != RecentPlanogramType.Repository
                   || (item.Type == RecentPlanogramType.Repository && item.IsForCurrentRepository))
                {
                    cm.Items.Add(
                        new MenuItem()
                        {
                            Header = MainPageCommands.OpenPlanogramFromFile.FriendlyName,
                            Command = MainPageCommands.OpenPlanogramFromFile,
                            CommandParameter = item.PlanogramId,
                        });

                    cm.Items.Add(new Separator());
                }



                if (item.IsPinned)
                {
                    //unpin
                    cm.Items.Add(
                    new MenuItem()
                    {
                        Header = this.ViewModel.UnpinCommand.FriendlyName,
                        Command = this.ViewModel.UnpinCommand,
                        CommandParameter = item
                    });

                }
                else
                {
                    //pin
                    cm.Items.Add(
                    new MenuItem()
                    {
                        Header = this.ViewModel.PinCommand.FriendlyName,
                        Command = this.ViewModel.PinCommand,
                        CommandParameter = item
                    });

                    //remove from list
                    cm.Items.Add(
                    new MenuItem()
                    {
                        Header = this.ViewModel.RemoveItemCommand.FriendlyName,
                        Command = this.ViewModel.RemoveItemCommand,
                        CommandParameter = item
                    });
                }

                cm.Items.Add(new Separator());

                //clear unpinned
                cm.Items.Add(
                    new MenuItem()
                    {
                        Header = this.ViewModel.ClearUnpinnedCommand.FriendlyName,
                        Command = this.ViewModel.ClearUnpinnedCommand
                    });


                cm.Placement = PlacementMode.MousePoint;
                cm.StaysOpen = false;
                cm.IsOpen = true;
            }

        }

        /// <summary>
        /// Called whenever the item pin button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pinBtn_Click(object sender, RoutedEventArgs e)
        {
            Button senderControl = (Button)sender;
            if (senderControl.IsFocused)
            {
                FocusManager.SetFocusedElement(this, null);
                Keyboard.ClearFocus();
            }
        }

        #endregion
    }
}
