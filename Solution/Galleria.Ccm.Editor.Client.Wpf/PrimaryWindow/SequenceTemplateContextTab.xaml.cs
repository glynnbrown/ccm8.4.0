﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32661 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for SequenceTemplateContextTab.xaml
    /// </summary>
    public partial class SequenceTemplateContextTab
    {
        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanVisualDocument), typeof(SequenceTemplateContextTab),
            new PropertyMetadata(null));

        public PlanVisualDocument ViewModel
        {
            get { return (PlanVisualDocument)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor
        public SequenceTemplateContextTab()
        {
            InitializeComponent();
        } 
        #endregion
    }
}
