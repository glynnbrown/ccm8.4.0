﻿// Copyright © Galleria RTS Ltd 2016
using Fluent;
using Galleria.Framework.ViewModel;
using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Provides helper methods for dealing with the main Space Planning ribbon control
    /// </summary>
    internal static class MainPageRibbonHelper
    {
        /// <summary>
        /// The name of the file in isolated storage which contains the state info for this
        /// app's ribbon.
        /// </summary>
        const String RibbonStateIsolatedStorageFileName = "SpacePlanningRibbonState";


        /// <summary>
        /// Loads the controls for the initial quick access toolbar items.
        /// </summary>
        internal static void IntializeQuickAccessToolbar(Ribbon ribbonControl)
        {
            RelayCommand[] commands =
                new RelayCommand[]
                {
                   MainPageCommands.SavePlanogram,
                    MainPageCommands.Undo,
                    MainPageCommands.Redo
                };

            foreach (var cmd in commands)
            {
                Control control = TraverseTreeForCommandControl(ribbonControl, cmd, null) as Control;
                if (control != null)
                {
                    ribbonControl.QuickAccessItems.Add(
                        new QuickAccessMenuItem()
                        {
                            IsChecked = true,
                            Target = control
                        });

                }
            }
        }

        /// <summary>
        /// Saves the current state of the ribbon quick access toolbar.
        /// </summary>
        internal static void SaveRibbonQuickAccessState(Ribbon ribbonControl)
        {
            //build the state string
            StringBuilder builder = new StringBuilder();
            builder.Append(ribbonControl.IsMinimized.ToString(CultureInfo.InvariantCulture));
            builder.Append(',');
            builder.Append(ribbonControl.ShowQuickAccessToolBarAboveRibbon.ToString(CultureInfo.InvariantCulture));
            builder.Append('|');

            //QAT items:
            var qatPaths = ribbonControl.GetQATElementPaths();
            foreach (var qatItemEntry in ribbonControl.EnumerateCurrentQuickAccessItems())
            {
                UIElement origRibbonItem = qatItemEntry.Key;

                //Append the item path so that regardless of the item type we have a start point
                // to find it again.
                if (qatPaths.ContainsKey((FrameworkElement)origRibbonItem))
                    builder.Append(qatPaths[(FrameworkElement)origRibbonItem]);

                // because we need to be able to track command buttons which are dynamically created based on
                // the users most recently accessed lists (e.g. Product Labels)
                // Using the path alone does not always work.
                // So if the element is a command button, we should add extra info about the command and parameter.
                if (origRibbonItem is Fluent.Button || origRibbonItem is Fluent.MenuItem)
                {
                    ICommandSource cmdSrc = origRibbonItem as ICommandSource;
                    if (cmdSrc != null && cmdSrc.Command != null)
                    {
                        //Find the command the button is linked to
                        foreach (PropertyInfo commandField in
                            typeof(MainPageCommands).GetProperties(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public))
                        {
                            if (commandField.PropertyType != typeof(RelayCommand)) continue;

                            RelayCommand command = commandField.GetValue(null, null) as RelayCommand;
                            if (command == cmdSrc.Command)
                            {
                                //Append the command values
                                builder.Append(':');
                                builder.Append(commandField.Name);
                                builder.Append(':');
                                builder.Append(Convert.ToString(cmdSrc.CommandParameter, CultureInfo.InvariantCulture));
                                break;
                            }
                        }
                    }
                }

                //add the item separator.
                builder.Append(';');
            }

            //write to storage
            IsolatedStorageFile storage = GetIsolatedStorageFile();
            using (IsolatedStorageFileStream stream =
                new IsolatedStorageFileStream(RibbonStateIsolatedStorageFileName, FileMode.Create, FileAccess.Write, storage))
            {
                StreamWriter writer = new StreamWriter(stream);
                writer.Write(builder.ToString());
                writer.Flush();
            }
        }

        /// <summary>
        /// Loads the state of this ribbon that was previously saved to isolated storage.
        /// </summary>
        internal static void LoadRibbonQuickAccessState(Ribbon ribbonControl)
        {
            IsolatedStorageFile storage = GetIsolatedStorageFile();
            if (!FileExists(storage, RibbonStateIsolatedStorageFileName)) return;

            using (IsolatedStorageFileStream stream =
                new IsolatedStorageFileStream(RibbonStateIsolatedStorageFileName, FileMode.Open, FileAccess.Read, storage))
            {
                StreamReader reader = new StreamReader(stream);
                string[] splitted = reader.ReadToEnd().Split('|');

                if (splitted.Length != 2) return;


                // Load Ribbon State
                string[] ribbonProperties = splitted[0].Split(',');
                ribbonControl.IsMinimized = Boolean.Parse(ribbonProperties[0]);
                ribbonControl.ShowQuickAccessToolBarAboveRibbon = Boolean.Parse(ribbonProperties[1]);

                // Load items
                string[] items = splitted[1].Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < items.Length; i++) ParseAndAddItemToQAT(ribbonControl, items[i]);


                // Sync QAT menu items
                foreach (QuickAccessMenuItem menuItem in ribbonControl.QuickAccessItems)
                {
                    menuItem.IsChecked = ribbonControl.IsInQuickAccessToolBar(menuItem.Target);
                }
            }
        }

        /// <summary>
        /// Parses the item state data string to add the required control to the 
        /// qat bar.
        /// </summary>
        /// <param name="data"></param>
        private static void ParseAndAddItemToQAT(Ribbon ribbonControl, String data)
        {
            //unpack the data
            String[] splitted = data.Split(new char[] { ':' });

            if (splitted.Length == 3)
            {
                //this has been saved with command information so try to use this first.
                String commandName = splitted[1];
                String commandParameter = splitted[2];

                //try to get the command from MainPageCommands.
                RelayCommand command = null;
                foreach (PropertyInfo commandField in
                    typeof(MainPageCommands).GetProperties(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public))
                {
                    if (commandField.Name == commandName)
                    {
                        command = commandField.GetValue(null, null) as RelayCommand;
                        break;
                    }
                }
                if (command != null)
                {
                    //search for the control for this command in the ribbon.
                    UIElement control = TraverseTreeForCommandControl(ribbonControl, command, commandParameter);
                    if (control != null)
                    {
                        //add it to the quick access bar and we are done.
                        ribbonControl.AddToQuickAccessToolBar(control);
                        return;
                    }
                }

                Debug.WriteLine(String.Format("Command qat item not found: {0}", commandName));
            }


            // if we got this far then attempt to use the path info 
            //using the normal ribbon method.
            if (splitted.Length == 1)
            {
                ribbonControl.ParseAndAddToQuickAccessToolBar(splitted[0]);
            }
        }

        /// <summary>
        /// Traverses the logical tree looking for a button linked to the given command and parameter.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="commandToFind"></param>
        /// <param name="commandParameter"></param>
        /// <returns></returns>
        private static UIElement TraverseTreeForCommandControl(DependencyObject item, RelayCommand commandToFind, String commandParameter)
        {
            //if the item if the one we are looking for then return it.
            ICommandSource commandSrc = item as ICommandSource;
            if (commandSrc != null
                && commandSrc.Command == commandToFind)
            {
                if ((String.IsNullOrEmpty(commandParameter) && commandSrc.CommandParameter == null)
                    || (commandSrc.CommandParameter != null && commandSrc.CommandParameter.ToString() == commandParameter))
                {
                    return (UIElement)item;
                }
            }

            object[] children = LogicalTreeHelper.GetChildren(item).Cast<object>().ToArray();
            for (int i = 0; i < children.Length; i++)
            {
                DependencyObject child = children[i] as DependencyObject;
                if (child == null) continue;

                UIElement foundItem = TraverseTreeForCommandControl(child, commandToFind, commandParameter);
                if (foundItem != null) return foundItem;
            }
            return null;
        }

        /// <summary>
        /// Returns the isolate storage file.
        /// </summary>
        /// <returns></returns>
        private static IsolatedStorageFile GetIsolatedStorageFile()
        {
            try
            {
                return IsolatedStorageFile.GetUserStoreForDomain();
            }
            catch
            {
                return IsolatedStorageFile.GetUserStoreForAssembly();
            }
        }

        /// <summary>
        /// Determinates whether the given file exists in the given storage
        /// </summary>
        /// <param name="storage"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private static Boolean FileExists(IsolatedStorageFile storage, string fileName)
        {
            string[] files = storage.GetFileNames(fileName);
            return files.Length != 0;
        }

        /// <summary>
        /// Resets automatically saved state
        /// </summary>
        public static void ResetState()
        {
            IsolatedStorageFile storage = GetIsolatedStorageFile();
            foreach (string filename in storage.GetFileNames(RibbonStateIsolatedStorageFileName))
            {
                storage.DeleteFile(filename);
            }
        }

        /// <summary>
        /// Returns the quick access toolbar child of the given ribbon.
        /// </summary>
        /// <param name="ribbonControl"></param>
        /// <returns></returns>
        public static QuickAccessToolBar GetQuickAccessToolbar(Ribbon ribbonControl)
        {
            return ribbonControl.Template.FindName("PART_QuickAccessToolBar", ribbonControl) as QuickAccessToolBar;
        }

    }
}
