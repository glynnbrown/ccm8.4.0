﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31547 : M.Pettit
//  Created
// V8-32823  : J.Pickup
//  Export visbiltity work (new method to select default function type).
// V8-32810 : L.Ineson
//  Added pdf export and set this to aways be the default.
// V8-32810 : M.Pettit
//  Added Pdf Override functionality for Labels, Highlights and Images
// V8-33001 : M.Pettit
//  UpdateForActivePlanogram now correctly handles file and repository based planograms 
// V8-33002 : M.Pettit
//  Export to pdf command now disabled if a selected plan has no template
// V8-32988 : M.Pettit
//  Now sets ExportFunction correctly if certain export options not available
// V8-32988 : M.Pettit
//  Default option set to ExportToPdf. Column widths altered for readability
// CCM-18441 : M.Pettit
//  Export CanExecute now checks for valid overide settings
// CCM-18553 : M.Pettit
//  Update for active planogram method failed if no repostory connected
// CCM-18648 : A.Silva
//  Amended UpdateForActivePlanogram to not try to get plan content lookups without a repository connection.
#endregion

#region Version History: (CCM 8.4.0)
// CCM-19219 : J.Mendes
//  Optimezed the pdf exporting speed by reusing a clone of the existing Plan3Ddata that is already calculated in the plan visual documents.
#endregion

#endregion

using System;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using CommonImageResources = Galleria.Ccm.Common.Wpf.Resources.ImageResources;
using CommonMessage = Galleria.Ccm.Common.Wpf.Resources.Language.Message;
using System.Collections.ObjectModel;
using Galleria.Ccm.Common.Wpf;
using Galleria.Ccm.Common.Wpf.ViewModel;
using System.Collections.Generic;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Framework.Planograms.Rendering;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// View Model controller for BackstageExportTabContent control.
    /// </summary>
    public sealed class BackstageExportTabViewModel : ControlViewModelBase
    {
        #region Nested Classes
        /// <summary>
        /// Denotes the avaialble export options.
        /// </summary>
        public enum ExportFunctionType
        {
            ExportToPdfFile,
            ExportToJdaFile,
            ExportToApolloFile,
            ExportToSpacemanFile
        }

        #endregion

        #region Fields

        private Boolean _isUpdatingActivePlanogram = false;
        private Boolean _isActive;//Flag indicating whether this tab is currently active.

        private readonly PlanogramViewObjectView _activePlanogramView; // View to the currently active Planogram

        private ExportFunctionType _exportFunction = ExportFunctionType.ExportToPdfFile;
        private Boolean _isContentEnabled;

        private Boolean _includePerformanceData = true;
        private Boolean _viewErrorLogsOnCompletion = true;
        private Boolean _retainSourcePlanLookAndFeel = true;
        private Boolean _allowComplexPositions = true;

        private String _pdfOutputDirectory;
        private readonly BulkObservableCollection<PdfExportItem> _pdfExportPlanograms = new BulkObservableCollection<PdfExportItem>();
        private readonly ObservableCollection<PdfExportItem> _highlightedPdfExportRows = new ObservableCollection<PdfExportItem>();
        private Boolean _exportAllPdfPlanograms = true;

        private Boolean _canOverridePrintTemplateShowRealImages = false;
        private Boolean _canOverridePrintTemplateLabels = false;
        private Boolean _canOverridePrintTemplateHighlights = false;
        private Highlight _overridePrintTemplateHighlight = null;
        private Label _overridePrintTemplatLabel = null;
        private Boolean _overridePrintTemplatShowImages = false;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath IsActiveProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.IsActive);
        public static readonly PropertyPath ActivePlanogramProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.ActivePlanogram);
        public static readonly PropertyPath ExportFunctionProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.ExportFunction);
        public static readonly PropertyPath IsValidProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.IsValid);
        public static readonly PropertyPath IsContentEnabledProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.IsContentEnabled);
        public static readonly PropertyPath IncludePerformanceDataProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.IncludePerformanceData);
        public static readonly PropertyPath ViewErrorLogsOnCompletionProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.ViewErrorLogsOnCompletion);
        public static readonly PropertyPath RetainSourcePlanLookAndFeelProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.RetainSourcePlanLookAndFeel);
        public static readonly PropertyPath AllowComplexPositionsProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.AllowComplexPositions);
        public static readonly PropertyPath PdfOutputDirectoryProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.PdfOutputDirectory);
        public static readonly PropertyPath HighlightedPdfExportRowsProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.HighlightedPdfExportRows);
        public static readonly PropertyPath PdfExportPlanogramsProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.PdfExportPlanograms);
        public static readonly PropertyPath ExportAllPdfPlanogramsProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.ExportAllPdfPlanograms);
        public static readonly PropertyPath PdfExportCountProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.PdfExportCount);

        public static readonly PropertyPath CanOverrideShowRealImagesProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.CanOverridePrintTemplateShowRealImages);
        public static readonly PropertyPath CanOverridePrintTemplateLabelsProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.CanOverridePrintTemplateLabels);
        public static readonly PropertyPath CanOverridePrintTemplateHighlightsProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.CanOverridePrintTemplateHighlights);
        public static readonly PropertyPath OverridePrintTemplateShowRealImagesProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.OverridePrintTemplateShowRealImages);
        public static readonly PropertyPath OverridePrintTemplateLabelProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.OverridePrintTemplateLabel);
        public static readonly PropertyPath OverridePrintTemplateHighlightProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.OverridePrintTemplateHighlight);

        //Commands
        public static readonly PropertyPath ExportToJdaCommandProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.ExportToJdaCommand);
        public static readonly PropertyPath ExportToApolloCommandProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.ExportToApolloCommand);
        public static readonly PropertyPath ExportToSpacemanCommandProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.ExportToSpacemanCommand);
        public static readonly PropertyPath SetPdfPrintTemplateCommandProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.SetPdfPrintTemplateCommand);
        public static readonly PropertyPath SetPdfOutputDirectoryCommandProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.SetPdfOutputDirectoryCommand);
        public static readonly PropertyPath ExportToPdfCommandProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.ExportToPdfCommand);

        public static readonly PropertyPath SetLabelCommandProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.SetLabelCommand);
        public static readonly PropertyPath SetLabelFromFileCommandProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.SetLabelFromFileCommand);
        public static readonly PropertyPath SetLabelFromRepositoryCommandProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.SetLabelFromRepositoryCommand);
        public static readonly PropertyPath SetHighlightCommandProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.SetHighlightCommand);
        public static readonly PropertyPath SetHighlightFromFileCommandProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.SetHighlightFromFileCommand);
        public static readonly PropertyPath SetHighlightFromRepositoryCommandProperty = GetPropertyPath<BackstageExportTabViewModel>(p => p.SetHighlightFromRepositoryCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether this tab is active
        /// and should be updating.
        /// </summary>
        public Boolean IsActive
        {
            get { return _isActive; }
            set
            {
                if (_isActive != value)
                {
                    _isActive = value;
                    OnPropertyChanged(IsActiveProperty);

                    OnIsActiveChanged();
                }
            }
        }

        /// <summary>
        /// Returns the active planogram.
        /// </summary>
        public PlanogramView ActivePlanogram
        {
            get { return _activePlanogramView.Model; }
        }

        /// <summary>
        /// Gets which Export function the user wants
        /// </summary>
        public ExportFunctionType ExportFunction
        {
            get { return _exportFunction; }
            set
            {
                _exportFunction = value;
                OnPropertyChanged(ExportFunctionProperty);
            }
        }

        /// <summary>
        /// Checks if the ExportToFile is valid
        /// </summary>
        public Boolean IsValid
        {
            get
            {
                return !(String.IsNullOrEmpty(ActivePlanogram.Name));
            }
        }

        /// <summary>
        /// Gets/Sets whether the content is enabled
        /// </summary>
        public Boolean IsContentEnabled
        {
            get { return _isContentEnabled; }
            private set
            {
                _isContentEnabled = value;
                OnPropertyChanged(IsContentEnabledProperty);
            }
        }

        /// <summary>
        /// Should the export also include performance data
        /// </summary>
        public Boolean IncludePerformanceData
        {
            get { return _includePerformanceData; }
            set
            {
                _includePerformanceData = value;
                OnPropertyChanged(IncludePerformanceDataProperty);
            }
        }

        /// <summary>
        /// Spaceman-specific. Prevents the exported plan from containing complex positions
        /// </summary>
        public Boolean AllowComplexPositions
        {
            get { return _allowComplexPositions; }
            set
            {
                _allowComplexPositions = value;
                OnPropertyChanged(AllowComplexPositionsProperty);
            }
        }

        /// <summary>
        /// Should any error logs be displayed to the user on completion of the export?
        /// </summary>
        public Boolean ViewErrorLogsOnCompletion
        {
            get { return _viewErrorLogsOnCompletion; }
            set
            {
                _viewErrorLogsOnCompletion = value;
                OnPropertyChanged(ViewErrorLogsOnCompletionProperty);
            }
        }

        /// <summary>
        /// Determines whether the exported plan only exports vlaues that are directly compatible with the export type (Apollo etc)
        /// or if additional efforts should be made to retain the look and feel of the source plan. 
        /// For example if users check this, a position in a V8 planogram with right caps will ensure that the exported Apollo plan 
        /// will have 2 positions, one for the front facings and one for the right caps. If they uncheck it, since Apollo does not 
        /// support right caps, only one position will exist on the exported pan with no right cappings applied.
        /// </summary>
        public Boolean RetainSourcePlanLookAndFeel
        {
            get { return _retainSourcePlanLookAndFeel; }
            set
            {
                _retainSourcePlanLookAndFeel = value;
                OnPropertyChanged(RetainSourcePlanLookAndFeelProperty);
            }
        }

        /// <summary>
        /// Returns the collection of plans that can be selected for the pdf export.
        /// </summary>
        public ReadOnlyBulkObservableCollection<PdfExportItem> PdfExportPlanograms
        {
            get;
            private set;
        }

        /// <summary>
        /// Returns the collection indicating which rows the user has selected in the pdf export grid.
        /// </summary>
        public ObservableCollection<PdfExportItem> HighlightedPdfExportRows
        {
            get { return _highlightedPdfExportRows; }
        }

        /// <summary>
        /// Gets/Sets the pdf directory to export to.
        /// </summary>
        public String PdfOutputDirectory
        {
            get { return _pdfOutputDirectory; }
            set
            {
                _pdfOutputDirectory = value;
                OnPropertyChanged(PdfOutputDirectoryProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether all plans are selected.
        /// </summary>
        public Boolean ExportAllPdfPlanograms
        {
            get { return _exportAllPdfPlanograms; }
            set
            {
                _exportAllPdfPlanograms = value;
                OnPropertyChanged(ExportAllPdfPlanogramsProperty);

                foreach (var row in this.PdfExportPlanograms)
                    row.IsSelected = value;
            }
        }

        /// <summary>
        /// Returns a count of export plans that are selected.
        /// </summary>
        public Int32 PdfExportCount
        {
            get { return this.PdfExportPlanograms.Count(p => p.IsSelected); }
        }


        #region Override Properties

        public Boolean CanOverridePrintTemplateShowRealImages
        {
            get { return _canOverridePrintTemplateShowRealImages; }
            set
            {
                _canOverridePrintTemplateShowRealImages = value;
                OnPropertyChanged(CanOverrideShowRealImagesProperty);
            }
        }

        public Boolean CanOverridePrintTemplateLabels
        {
            get { return _canOverridePrintTemplateLabels; }
            set
            {
                _canOverridePrintTemplateLabels = value;
                OnPropertyChanged(CanOverridePrintTemplateLabelsProperty);
            }
        }

        public Boolean CanOverridePrintTemplateHighlights
        {
            get { return _canOverridePrintTemplateHighlights; }
            set
            {
                _canOverridePrintTemplateHighlights = value;
                OnPropertyChanged(CanOverridePrintTemplateHighlightsProperty);
            }
        }

        public Boolean OverridePrintTemplateShowRealImages
        {
            get { return _overridePrintTemplatShowImages; }
            set
            {
                _overridePrintTemplatShowImages = value;
                OnPropertyChanged(OverridePrintTemplateShowRealImagesProperty);
            }
        }

        public Label OverridePrintTemplateLabel
        {
            get { return _overridePrintTemplatLabel; }
            set
            {
                _overridePrintTemplatLabel = value;
                OnPropertyChanged(OverridePrintTemplateLabelProperty);
            }
        }

        public Highlight OverridePrintTemplateHighlight
        {
            get { return _overridePrintTemplateHighlight; }
            set
            {
                _overridePrintTemplateHighlight = value;
                OnPropertyChanged(OverridePrintTemplateHighlightProperty);
            }
        }
        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public BackstageExportTabViewModel()
        {
            PlanogramViewObjectView planView = App.ViewState.ActivePlanogramView;

            _activePlanogramView = planView;

            //Other defaults
            this.ViewErrorLogsOnCompletion = true;
            this.RetainSourcePlanLookAndFeel = true;
            this.IncludePerformanceData = true;
            this.AllowComplexPositions = true;
            this.ExportFunction = ExportFunctionType.ExportToPdfFile;

            this.PdfExportPlanograms = new ReadOnlyBulkObservableCollection<PdfExportItem>(_pdfExportPlanograms);
            this.PdfOutputDirectory = CCMClient.ViewState.GetSessionDirectory(SessionDirectory.PlanogramExport);

            //if we are unit testing then set then set the view model to active
            if (App.IsUnitTesting) this.IsActive = true;
        }

        #endregion

        #region Commands

        #region ExportToJdaCommand

        private RelayCommand _exportToJdaCommand;

        /// <summary>
        /// sets the visibility of the SaveToJda Option
        /// </summary>
        public RelayCommand ExportToJdaCommand
        {
            get
            {
                if (_exportToJdaCommand == null)
                {
                    _exportToJdaCommand = RegisterCommand(
                        p => ExportToJda_Executed(),
                        friendlyName:  Message.BackstageExportTab_ExportToJda_Title,
                        friendlyDescription: Message.BackstageExportTab_ExportToJda_Desc,
                        icon: ImageResources.SaveToFile_32);
                }
                return _exportToJdaCommand;
            }
        }

        public void ExportToJda_Executed()
        {
            ExportFunction = ExportFunctionType.ExportToJdaFile;
            if (this.ActivePlanogram != null)
            {
                this.ActivePlanogram.SaveSettings.SaveAsFileType = PlanogramExportFileType.JDA;
            }  
        }

        #endregion

        #region ExportToApolloCommand

        private RelayCommand _exportToApolloCommand;

        /// <summary>
        /// sets the visibility of the SaveToApollo Option
        /// </summary>
        public RelayCommand ExportToApolloCommand
        {
            get
            {
                if (_exportToApolloCommand == null)
                {
                    _exportToApolloCommand = RegisterCommand(
                        p => ExportToApollo_Executed(),
                        friendlyName: Message.BackstageExportTab_ExportToApollo_Title,
                        friendlyDescription: Message.BackstageExportTab_ExportToApollo_Desc,
                        icon: ImageResources.SaveToFile_32);
                }
                return _exportToApolloCommand;
            }
        }

        public void ExportToApollo_Executed()
        {
            ExportFunction = ExportFunctionType.ExportToApolloFile;
            if (this.ActivePlanogram != null)
            {
                this.ActivePlanogram.SaveSettings.SaveAsFileType = PlanogramExportFileType.Apollo;
            } 
        }

        #endregion

        #region ExportToSpacemanCommand

        private RelayCommand _exportToSpacemanCommand;

        /// <summary>
        /// sets the visibility of the SaveToSpaceman Option
        /// </summary>
        public RelayCommand ExportToSpacemanCommand
        {
            get
            {
                if (_exportToSpacemanCommand == null)
                {
                    _exportToSpacemanCommand = RegisterCommand(
                        p => ExportToSpaceman_Executed(),
                        friendlyName:Message.BackstageExportTab_ExportToSpaceman_Title,
                        friendlyDescription: Message.BackstageExportTab_ExportToSpaceman_Desc,
                        icon: ImageResources.SaveToFile_32);
                }
                return _exportToSpacemanCommand;
            }
        }

        public void ExportToSpaceman_Executed()
        {
            ExportFunction = ExportFunctionType.ExportToSpacemanFile;
            if (this.ActivePlanogram != null)
            {
                this.ActivePlanogram.SaveSettings.SaveAsFileType = PlanogramExportFileType.Spaceman;
            } 
        }

        #endregion


        #region SetPdfPrintTemplateCommand

        private RelayCommand _setPrintTemplateCommand;

        /// <summary>
        /// Show the dialog to select a print template from the repository.
        /// </summary>
        public RelayCommand SetPdfPrintTemplateCommand
        {
            get
            {
                if (_setPrintTemplateCommand == null)
                {
                    _setPrintTemplateCommand =RegisterCommand(
                        p => SetPdfPrintTemplate_Executed(),
                        icon: CommonImageResources.Open_16,
                        smallIcon: CommonImageResources.Open_16);
                }
                return _setPrintTemplateCommand;
            }
        }

        private void SetPdfPrintTemplate_Executed()
        {
            if (App.ViewState.IsConnectedToRepository)
            {
                this.SetPdfPrintTemplateFromRepositoryCommand.Execute();
            }
            else
            {
                this.SetPdfPrintTemplateFromFileCommand.Execute();
            }
        }

        #endregion

        #region SetPdfPrintTemplateFromFileCommand

        private RelayCommand _setPrintTemplateFromFileCommand;

        /// <summary>
        /// Show the dialog to select a print template from the repository.
        /// </summary>
        public RelayCommand SetPdfPrintTemplateFromFileCommand
        {
            get
            {
                if (_setPrintTemplateFromFileCommand == null)
                {
                    _setPrintTemplateFromFileCommand = RegisterCommand(
                        p => SetPdfPrintTemplateFromFile_Executed(),
                        friendlyName:CommonMessage.Generic_OpenFromFile,
                        icon: CommonImageResources.Open_16,
                        smallIcon: CommonImageResources.Open_16);
                }
                return _setPrintTemplateFromFileCommand;
            }
        }

        private void SetPdfPrintTemplateFromFile_Executed()
        {
            PrintTemplateInfo selectedItem = PrintTemplateUIHelper.SelectPrintTemplateFromFile();
            if (selectedItem != null)
            {
                foreach (var row in this.HighlightedPdfExportRows)
                    row.TemplateInfo = selectedItem;
            }
        }

        #endregion

        #region SetPdfPrintTemplateFromRepositoryCommand

        private RelayCommand _setPrintTemplateFromRepositoryCommand;

        /// <summary>
        /// Show the dialog to select a print template from the repository.
        /// </summary>
        public RelayCommand SetPdfPrintTemplateFromRepositoryCommand
        {
            get
            {
                if (_setPrintTemplateFromRepositoryCommand == null)
                {
                    _setPrintTemplateFromRepositoryCommand = RegisterCommand(
                        p => SetPdfPrintTemplateFromRepository_Executed(),
                        p => SetPdfPrintTemplateFromRepository_CanExecute(),
                        friendlyName: CommonMessage.Generic_OpenFromRepository,
                        icon: CommonImageResources.Open_16,
                        smallIcon: CommonImageResources.Open_16);
                }
                return _setPrintTemplateFromRepositoryCommand;
            }
        }

        private Boolean SetPdfPrintTemplateFromRepository_CanExecute()
        {
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.SetPdfPrintTemplateFromRepositoryCommand.DisabledReason = CommonMessage.Generic_NoRepositoryConnection;
                return false;
            }
            return true;
        }

        private void SetPdfPrintTemplateFromRepository_Executed()
        {
            if (!this.HasAttachedControl()) return;

            PrintTemplateInfo selectedItem = PrintTemplateUIHelper.SelectRepositoryPrintTemplate();
            if (selectedItem != null)
            {
                foreach(var row in this.HighlightedPdfExportRows) 
                    row.TemplateInfo = selectedItem;
            }
        }

        #endregion

        #region SetPdfOutputDirectoryCommand

        private RelayCommand _setPdfOutputDirectoryCommand;

        /// <summary>
        /// Show the dialog to select the directory to output the pdf files.
        /// </summary>
        public RelayCommand SetPdfOutputDirectoryCommand
        {
            get
            {
                if (_setPdfOutputDirectoryCommand == null)
                {
                    _setPdfOutputDirectoryCommand = RegisterCommand(
                        p => SetPdfOutputDirectory_Executed(),
                        friendlyName: "...",
                        icon: CommonImageResources.Open_16,
                        smallIcon:  CommonImageResources.Open_16);
                }

                return _setPdfOutputDirectoryCommand;
            }
        }

        private void SetPdfOutputDirectory_Executed()
        {
            String selectedDir;
            if (CommonHelper.GetWindowService()
                .ShowSelectDirectoryDialog(this.PdfOutputDirectory, out selectedDir))
            {
                this.PdfOutputDirectory = selectedDir;
            }
        }

        #endregion

        #region ExportToPdfCommand

        private RelayCommand _exportToPdfCommand;

        /// <summary>
        /// Show the dialog to select the directory to output the pdf files.
        /// </summary>
        public RelayCommand ExportToPdfCommand
        {
            get
            {
                if (_exportToPdfCommand == null)
                {
                    _exportToPdfCommand = RegisterCommand(
                        p => ExportToPdf_Executed(),
                        p => ExportToPdf_CanExecute(),
                        friendlyName:  Message.Ribbon_ExportPlanogramToFile,
                        friendlyDescription: Message.Ribbon_ExportPlanogramToFile_Desc,
                        icon:  ImageResources.SaveToFile_32,
                        smallIcon: ImageResources.SaveToFile_16);
                }
                return _exportToPdfCommand;
            }
        }

        private Boolean ExportToPdf_CanExecute()
        {
            OnPropertyChanged(PdfExportCountProperty);
            _exportAllPdfPlanograms = this.PdfExportPlanograms.All(p => p.IsSelected);
            OnPropertyChanged(ExportAllPdfPlanogramsProperty);

            _exportToPdfCommand.DisabledReason = String.Empty;

            if (this.PdfOutputDirectory == null) return false;
            if (!this.PdfExportPlanograms.Any(p => p.IsSelected)) return false;
            if (this.PdfExportPlanograms.Any(p => p.IsSelected && p.TemplateInfo == null))
            {
                _exportToPdfCommand.DisabledReason = CommonMessage.ExportPlanograms_DisabledReason_NoTemplate;
                return false;
            }

            //check overrides
            if (this.CanOverridePrintTemplateLabels)
            {
                if (this.OverridePrintTemplateLabel == null)
                {
                    _exportToPdfCommand.DisabledReason = CommonMessage.ExportPlanograms_DisabledReason_NoOverrideLabel;
                    return false;
                }
            }
            if (this.CanOverridePrintTemplateHighlights)
            {
                if (this.OverridePrintTemplateHighlight == null)
                {
                    _exportToPdfCommand.DisabledReason = CommonMessage.ExportPlanograms_DisabledReason_NoOverrideHighlight;
                    return false;
                }
            }
            return true;
        }

        private void ExportToPdf_Executed()
        {
            //apply any override values tot he export items
            Boolean? showRealImages = this.CanOverridePrintTemplateShowRealImages == true ? (Boolean?)this.OverridePrintTemplateShowRealImages : null;
            Highlight overrideHighlight = this.CanOverridePrintTemplateHighlights == true ? this.OverridePrintTemplateHighlight : null;
            Label overrideLabel = this.CanOverridePrintTemplateLabels == true ? this.OverridePrintTemplateLabel : null;

            foreach (PdfExportItem item in this.PdfExportPlanograms.Where(p => p.IsSelected))
            {
                item.OverrideShowRealImages = showRealImages;
                item.OverrideHighlight = overrideHighlight;
                item.OverrideLabel = overrideLabel;
            }

            PlanogramExportHelper.ExportPlanogramsToPdf(
                this.PdfExportPlanograms.Where(p => p.IsSelected).ToList(),
                this.PdfOutputDirectory);
        }

        #endregion


        #region Set Override Label Commands

        #region SetLabelCommand

        private RelayCommand _setLabelCommand;

        /// <summary>
        /// Show the dialog to select a print template from the repository.
        /// </summary>
        public RelayCommand SetLabelCommand
        {
            get
            {
                if (_setLabelCommand == null)
                {
                    _setLabelCommand =
                        RegisterCommand(
                            p => SetLabel_Executed(),
                            p => SetLabel_CanExecute(),
                        icon: CommonImageResources.Open_16,
                        smallIcon: CommonImageResources.Open_16);
                }
                return _setLabelCommand;
            }
        }

        private Boolean SetLabel_CanExecute()
        {
            if (!this.CanOverridePrintTemplateLabels) return false;
            return true;
        }

        private void SetLabel_Executed()
        {
            if (CCMClient.ViewState.IsConnectedToRepository)
            {
                this.SetLabelFromRepositoryCommand.Execute();
            }
            else
            {
                this.SetLabelFromFileCommand.Execute();
            }
        }

        #endregion

        #region SetLabelFromFileCommand

        private RelayCommand _setLabelFromFileCommand;

        /// <summary>
        /// Show the dialog to select a label from the repository.
        /// </summary>
        public RelayCommand SetLabelFromFileCommand
        {
            get
            {
                if (_setLabelFromFileCommand == null)
                {
                    _setLabelFromFileCommand =
                        RegisterCommand(
                            p => SetLabelFromFile_Executed(),
                            p => SetLabelFromFile_CanExecute(),
                            friendlyName: CommonMessage.Generic_OpenFromFile,
                            icon: CommonImageResources.Open_16,
                            smallIcon: CommonImageResources.Open_16);
                }
                return _setLabelFromFileCommand;
            }
        }

        private Boolean SetLabelFromFile_CanExecute()
        {
            if (!this.CanOverridePrintTemplateLabels) return false;
            return true;
        }

        private void SetLabelFromFile_Executed()
        {
            String file = LabelUIHelper.ShowOpenFileDialog();

            if (String.IsNullOrEmpty(file)) return;

            Label selectedItem = LabelUIHelper.FetchLabelAsReadonly(file);
            if (selectedItem != null)
            {
                this.OverridePrintTemplateLabel = selectedItem;
            }
        }

        #endregion

        #region SetLabelFromRepositoryCommand

        private RelayCommand _setLabelFromRepositoryCommand;

        /// <summary>
        /// Show the dialog to select a Label from the repository.
        /// </summary>
        public RelayCommand SetLabelFromRepositoryCommand
        {
            get
            {
                if (_setLabelFromRepositoryCommand == null)
                {
                    _setLabelFromRepositoryCommand =
                        RegisterCommand(
                        p => SetLabelFromRepository_Executed(),
                        p => SetLabelFromRepository_CanExecute(),
                        friendlyName: CommonMessage.Generic_OpenFromRepository,
                        icon: CommonImageResources.Open_16,
                        smallIcon: CommonImageResources.Open_16);
                }
                return _setLabelFromRepositoryCommand;
            }
        }

        private Boolean SetLabelFromRepository_CanExecute()
        {
            if (!this.CanOverridePrintTemplateLabels) return false;
            if (!CCMClient.ViewState.IsConnectedToRepository)
            {
                this.SetLabelFromRepositoryCommand.DisabledReason = CommonMessage.Generic_NoRepositoryConnection;
                return false;
            }
            this.SetLabelFromRepositoryCommand.DisabledReason = String.Empty;
            return true;
        }

        private void SetLabelFromRepository_Executed()
        {
            if (!this.HasAttachedControl()) return;
            LabelInfo selectedItemInfo = LabelUIHelper.SelectRepositoryLabel();
            if (selectedItemInfo != null)
            {
                this.OverridePrintTemplateLabel = LabelUIHelper.FetchLabelAsReadonly(selectedItemInfo.Id);
            }
        }

        #endregion

        #endregion

        #region Set Override Highlight Commands

        #region SetHighlightCommand

        private RelayCommand _setHighlightCommand;

        /// <summary>
        /// Show the dialog to select a Highlight from the repository.
        /// </summary>
        public RelayCommand SetHighlightCommand
        {
            get
            {
                if (_setHighlightCommand == null)
                {
                    _setHighlightCommand =
                        RegisterCommand(
                        p => SetHighlight_Executed(),
                        p => SetHighlight_CanExecute(),
                        icon: CommonImageResources.Open_16,
                        smallIcon: CommonImageResources.Open_16);
                }
                return _setHighlightCommand;
            }
        }

        private Boolean SetHighlight_CanExecute()
        {
            if (!this.CanOverridePrintTemplateHighlights) return false;
            return true;
        }

        private void SetHighlight_Executed()
        {
            if (CCMClient.ViewState.IsConnectedToRepository)
            {
                this.SetHighlightFromRepositoryCommand.Execute();
            }
            else
            {
                this.SetHighlightFromFileCommand.Execute();
            }
        }

        #endregion

        #region SetHighlightFromFileCommand

        private RelayCommand _setHighlightFromFileCommand;

        /// <summary>
        /// Show the dialog to select a Highlight from the repository.
        /// </summary>
        public RelayCommand SetHighlightFromFileCommand
        {
            get
            {
                if (_setHighlightFromFileCommand == null)
                {
                    _setHighlightFromFileCommand =
                        RegisterCommand(
                        p => SetHighlightFromFile_Executed(),
                        p => SetHighlightFromFile_CanExecute(),
                        friendlyName: CommonMessage.Generic_OpenFromFile,
                        icon: CommonImageResources.Open_16,
                        smallIcon: CommonImageResources.Open_16);
                }
                return _setHighlightFromFileCommand;
            }
        }

        private Boolean SetHighlightFromFile_CanExecute()
        {
            if (!this.CanOverridePrintTemplateHighlights) return false;
            return true;
        }

        private void SetHighlightFromFile_Executed()
        {
            String file = HighlightUIHelper.ShowOpenFileDialog();

            if (String.IsNullOrEmpty(file)) return;

            Highlight selectedItem = HighlightUIHelper.FetchHighlightAsReadonly(file);
            if (selectedItem != null)
            {
                this.OverridePrintTemplateHighlight = selectedItem;
            }
        }

        #endregion

        #region SetHighlightFromRepositoryCommand

        private RelayCommand _setHighlightFromRepositoryCommand;

        /// <summary>
        /// Show the dialog to select a Highlight from the repository.
        /// </summary>
        public RelayCommand SetHighlightFromRepositoryCommand
        {
            get
            {
                if (_setHighlightFromRepositoryCommand == null)
                {
                    _setHighlightFromRepositoryCommand =
                        RegisterCommand(
                        p => SetHighlightFromRepository_Executed(),
                        p => SetHighlightFromRepository_CanExecute(),
                        friendlyName: Message.Generic_OpenFromRepository,
                        icon: CommonImageResources.Open_16,
                        smallIcon: CommonImageResources.Open_16);
                }
                return _setHighlightFromRepositoryCommand;
            }
        }

        private Boolean SetHighlightFromRepository_CanExecute()
        {
            if (!this.CanOverridePrintTemplateHighlights) return false;
            if (!CCMClient.ViewState.IsConnectedToRepository)
            {
                this.SetHighlightFromRepositoryCommand.DisabledReason = CommonMessage.Generic_NoRepositoryConnection;
                return false;
            }
            this.SetHighlightFromRepositoryCommand.DisabledReason = String.Empty;
            return true;
        }

        private void SetHighlightFromRepository_Executed()
        {
            if (!this.HasAttachedControl()) return;
            HighlightInfo selectedItemInfo = HighlightUIHelper.SelectRepositoryHighlight();
            if (selectedItemInfo != null)
            {
                this.OverridePrintTemplateHighlight = HighlightUIHelper.FetchHighlightAsReadonly(selectedItemInfo.Id);
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out actions when the value of the IsActive property changes.
        /// </summary>
        private void OnIsActiveChanged()
        {
            UpdateForActivePlanogram();
        }

        /// <summary>
        /// Called whenever a property changes on this view.
        /// </summary>
        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);

            if (this.ActivePlanogram == null) return;
            if (_isUpdatingActivePlanogram) return;
            if (propertyName == IsActiveProperty.Path) return;
            if (propertyName == IsContentEnabledProperty.Path) return;
            if (propertyName == ActivePlanogramProperty.Path) return;
            if (propertyName == IsValidProperty.Path) return;

            if (propertyName == ViewErrorLogsOnCompletionProperty.Path)
            {
                this.ActivePlanogram.SaveSettings.ExportViewErrorLogsOnCompletion = _viewErrorLogsOnCompletion;
                return;
            }
            if (propertyName == IncludePerformanceDataProperty.Path)
            {
                this.ActivePlanogram.SaveSettings.ExportIncludePerformanceData = _includePerformanceData;
                return;
            }
            if (propertyName == RetainSourcePlanLookAndFeelProperty.Path)
            {
                this.ActivePlanogram.SaveSettings.ExportRetainSourcePlanLookAndFeel = _retainSourcePlanLookAndFeel;
                return;
            }
            if (propertyName == AllowComplexPositionsProperty.Path)
            {
                this.ActivePlanogram.SaveSettings.ExportAllowComplexPositions = _allowComplexPositions;
                return;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates values based on the currently active planogram.
        /// </summary>
        private void UpdateForActivePlanogram()
        {
            PlanogramView activePlan = this.ActivePlanogram;
            OnPropertyChanged(ActivePlanogramProperty);

            this.IsContentEnabled = (activePlan != null);
            OnPropertyChanged(IsValidProperty);

            //If there is an active plan, switch the export function to match the current active plan
            if (activePlan != null)
            {
                _isUpdatingActivePlanogram = true; 
                
                //If exporting, we must override the default POG option to the default external type
                if (activePlan.SaveSettings.SaveAsFileType == PlanogramExportFileType.POG)
                {
                    activePlan.SaveSettings.SaveAsFileType = GetSaveAsFileType(this.ExportFunction);
                }
                else
                {
                    switch (activePlan.SaveSettings.SaveAsFileType)
                    {
                        case PlanogramExportFileType.Apollo:
                            this.ExportFunction = App.ViewState.IsExportApolloAvailable ? ExportFunctionType.ExportToApolloFile : ExportFunctionType.ExportToPdfFile;
                            break;

                        case PlanogramExportFileType.JDA:
                            this.ExportFunction = App.ViewState.IsExportJDAAvailable ? ExportFunctionType.ExportToJdaFile : ExportFunctionType.ExportToPdfFile;
                            break;

                        case PlanogramExportFileType.Spaceman:
                            this.ExportFunction = App.ViewState.IsExportSpacemanAvailable ? ExportFunctionType.ExportToSpacemanFile : ExportFunctionType.ExportToPdfFile;
                            break;

                        default:
                            this.ExportFunction = ExportFunctionType.ExportToPdfFile;
                            break;
                    }
                }
                
                this.IncludePerformanceData = activePlan.SaveSettings.ExportIncludePerformanceData;
                this.RetainSourcePlanLookAndFeel = activePlan.SaveSettings.ExportRetainSourcePlanLookAndFeel;
                this.AllowComplexPositions = activePlan.SaveSettings.ExportAllowComplexPositions;
                this.ViewErrorLogsOnCompletion = activePlan.SaveSettings.ExportViewErrorLogsOnCompletion;
                _isUpdatingActivePlanogram = false;
            }
      
            //update the export plans collection
            if(_pdfExportPlanograms.Any()) _pdfExportPlanograms.Clear();
            this.HighlightedPdfExportRows.Clear();
            if (!IsActive || App.MainPageViewModel == null) return;

            PrintTemplateInfoList templates = null;
            ContentLookupList contentLookups = null;
            PrintTemplateInfo defaultTemplate = PrintTemplateUIHelper.GetDefaultPrintTemplateInfo();

            //Find the repository packages - their planograms may have content lookup records
            List<Int32> planIds = App.MainPageViewModel.PlanControllers
                                     .Where(p => p.SourcePlanogram.ParentPackageView.Model.Id is Int32)
                                     .Select(p => (Int32)p.SourcePlanogram.Model.Id).ToList();
            //only fetch info is connected to a repository
            Boolean isConnectedToRepository = App.ViewState.IsConnectedToRepository;
            if (isConnectedToRepository)
            {
                templates = PrintTemplateInfoList.FetchByEntityId(CCMClient.ViewState.EntityId);
                //Fetch the content lookups for all plans in one fetch
                contentLookups = ContentLookupList.FetchByPlanogramIds(planIds);
            }

            //assign the template (either default or saved in contentlookup)
            foreach (var planController in App.MainPageViewModel.PlanControllers.OrderBy(p=> p.SourcePlanogram.Name))
            {
                //See if a template is already assigned in the content lookup for the planogram.
                //if not, use the default
                PrintTemplateInfo contentTemplate = null;
                ContentLookup planContentLookup;
                Boolean hasRepositoryId = planController.SourcePlanogram.ParentPackageView.Model.Id is Int32;
                Boolean tryGetPlanContentLookup = isConnectedToRepository && hasRepositoryId;
                if (tryGetPlanContentLookup)
                {
                    //plan is repository plan so try to get content lookup saved template
                    planContentLookup = contentLookups
                        .FirstOrDefault(c => c.PlanogramId.Equals(planController.SourcePlanogram.Model.Id));
                    if (planContentLookup != null)
                    {
                        contentTemplate = templates.FirstOrDefault(t => t.Id.Equals(planContentLookup.PrintTemplateId));
                    }
                }

                List<Plan3DData> planogramModelDataList = null;
                PlanVisualDocument planVisualModel = (PlanVisualDocument)planController.SelectedPlanDocument;
                if (planVisualModel != null && planVisualModel.PlanogramModelData != null)
                {
                    if (planVisualModel.PlanogramModelData.Settings.ShowProductImages)
                    {
                        Plan3DData newPlanogramModelData = planVisualModel.PlanogramModelData.Clone();
                        planogramModelDataList = new List<Plan3DData>();
                        planogramModelDataList.Add(newPlanogramModelData);
                    }
                }
                _pdfExportPlanograms.Add(new PdfExportItem(planController.SourcePlanogram.Model, contentTemplate ?? defaultTemplate, null, null, null, planogramModelDataList));
            }
            this.ExportAllPdfPlanograms = true;
        }

        /// <summary>
        /// Returns the apporipate SaveAs File Type for an Export type
        /// </summary>
        /// <param name="functionType"></param>
        /// <returns></returns>
        private PlanogramExportFileType GetSaveAsFileType(ExportFunctionType functionType)
        {
            switch (functionType)
            {
                case ExportFunctionType.ExportToApolloFile:
                    return PlanogramExportFileType.Apollo;

                case ExportFunctionType.ExportToJdaFile:
                    return PlanogramExportFileType.JDA;

                case ExportFunctionType.ExportToSpacemanFile:
                    return PlanogramExportFileType.Spaceman;
            }

            return PlanogramExportFileType.JDA;
        }

        #endregion
    }
}