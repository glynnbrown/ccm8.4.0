﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created
#endregion
#endregion

using System.Windows;
using System.Windows.Controls;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for BackstageNewTabContent.xaml
    /// </summary>
    public sealed partial class BackstageNewTabContent : UserControl
    {
        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(MainPageViewModel), typeof(BackstageNewTabContent),
            new PropertyMetadata(null));

        public MainPageViewModel ViewModel
        {
            get { return (MainPageViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        public BackstageNewTabContent()
        {
            InitializeComponent();
        }

        #endregion
    }
}
