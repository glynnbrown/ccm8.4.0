﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830
// V8-32661 : A.Kuszyk
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Editor.Client.Wpf.Common;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for SequenceTemplateGroupsPanel.xaml
    /// </summary>
    public partial class SequenceTemplateGroupsPanel : DockingPanelControl, IDisposable
    {
        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanVisualDocument), typeof(SequenceTemplateGroupsPanel),
            new PropertyMetadata(null));

        public PlanVisualDocument ViewModel
        {
            get { return (PlanVisualDocument)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        public SequenceTemplateGroupsPanel()
        {
            InitializeComponent();
        }

        #region Methods

        private void xRowExpander_Expanded(object sender, RoutedEventArgs e)
        {
            //Cast object to dependency
            DependencyObject obj = (DependencyObject)e.OriginalSource;

            //Find the objects parent in the visual tree
            obj = VisualTreeHelper.GetParent(obj);

            //Find the visual ancester of the parent
            DataGridRow dataGridRow = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<DataGridRow>(obj);

            //If data grid row is found
            if (dataGridRow != null)
            {
                dataGridRow.DetailsVisibility = Visibility.Visible;
            }
        }

        private void xRowExpander_Collapsed(object sender, RoutedEventArgs e)
        {
            //Cast object to dependency
            DependencyObject obj = (DependencyObject)e.OriginalSource;

            //Find the objects parent in the visual tree
            obj = VisualTreeHelper.GetParent(obj);

            //Find the visual ancester of the parent
            DataGridRow dataGridRow = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<DataGridRow>(obj);

            //If data grid row is found
            if (dataGridRow != null)
            {
                dataGridRow.DetailsVisibility = Visibility.Collapsed;
            }
        }

        public void Dispose()
        {
            // Nothing to do yet.
        } 
        #endregion
    }
}
