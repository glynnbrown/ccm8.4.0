﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24265 : J.Pickup
//  Label Setting view changed to LabelItem.
// CCM-25126 : N.Haywood
//  Can now deselect labels or highlights from their dropdown lists
// V8-26171 : A.Silva ~ Remvoved references to the old Custom Column Layout Editor, and the obsolete code for the now non existan button to open that editor.

#endregion

#endregion

using System.Windows;
using System.Linq;
using Fluent;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.Controls;
using System;
using System.Globalization;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using System.Windows.Data;
using Galleria.Ccm.Common.Wpf.Converters;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    ///     Interaction logic for MainPageViewTab.xaml
    /// </summary>
    public sealed partial class MainPageViewTab
    {
        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof (MainPageViewModel), typeof (MainPageViewTab),
                new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        ///     Gets the viewmodel controller for this window.
        /// </summary>
        public MainPageViewModel ViewModel
        {
            get { return (MainPageViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        /// <summary>
        /// Called whenever the viewmodel property changes.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MainPageViewTab senderControl = (MainPageViewTab)obj;

            MainPageViewModel oldModel = e.OldValue as MainPageViewModel;
            if (oldModel != null)
            {
                oldModel.AvailableFixtureLabels.BulkCollectionChanged -= senderControl.ViewModel_AvailableFixtureLabelsBulkCollectionChanged;
            }


            MainPageViewModel newModel = e.NewValue as MainPageViewModel;
            if (newModel != null)
            {
                newModel.AvailableProductLabels.BulkCollectionChanged += senderControl.ViewModel_AvailableFixtureLabelsBulkCollectionChanged;
            }

            senderControl.UpdateFixtureLabelsDropdownList();
        }

        #endregion

        #region Constructor

        public MainPageViewTab()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Called whenever the user clicks on a fixture label menu item.
        /// </summary>
        private void FixtureLabelMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel == null) return;

            var senderControl = sender as MenuItem;
            if (senderControl == null) return;

            if (senderControl.IsChecked)
            {
                LabelInfo info = senderControl.CommandParameter as LabelInfo;
                if (info != null)
                {
                    this.ViewModel.SetFixtureLabel(info);
                }
                else
                {
                    LabelItem item = senderControl.CommandParameter as LabelItem;
                    if (item != null)
                    {
                        this.ViewModel.SetFixtureLabel(item);
                    }
                    else
                    {
                        this.ViewModel.ClearFixtureLabel();
                    }
                }
            }
            else
            {
                this.ViewModel.ClearFixtureLabel();
            }
        }

        /// <summary>
        /// Called when a product label dynamic menu item is to be added to the quick access toolbar.
        /// </summary>
        private void FixtureLabelMenuItem_CreatingQuickAccessItem(object sender, DynamicMenuItem.CreateQuickAccessItemArgs e)
        {
            Button button = e.Element as Button;
            if (button == null) return;

            DynamicMenuItem senderControl = (DynamicMenuItem)sender;
            button.Icon = new System.Windows.Controls.Image() { Source = MainPageCommands.SelectProductLabel.SmallIcon, Height = 16, Width = 16 };
            button.ToolTip = String.Format(CultureInfo.CurrentCulture, Message.Ribbon_FixtureLabelQuickAccessTooltipFormat, senderControl.Header as String);

            e.Element = button;
        }

        /// <summary>
        /// Called whenever the available product labels collection changes to update the drop down menu.
        /// </summary>
        private void ViewModel_AvailableFixtureLabelsBulkCollectionChanged(object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
            UpdateFixtureLabelsDropdownList();
        }

        #endregion

        /// <summary>
        /// Updates the available labels dropdown menu.
        /// </summary>
        /// <remarks>We have to do it like this rather than use datatemplates otherwise the ribbon quick access 
        /// toobar save flakes out.</remarks>
        private void UpdateFixtureLabelsDropdownList()
        {
            //clean up:
            foreach (var item in this.AvailableFixtureLabelsDisplay.Items.OfType<DynamicMenuItem>())
            {
                item.CreatingQuickAccessItem -= FixtureLabelMenuItem_CreatingQuickAccessItem;
            }
            this.AvailableFixtureLabelsDisplay.Items.Clear();


            if (this.ViewModel == null) return;

            //Add the items
            foreach (var info in this.ViewModel.AvailableFixtureLabels)
            {
                DynamicMenuItem item = new DynamicMenuItem()
                {
                    Header = info.Name,
                    IsCheckable = true,
                    Command = MainPageCommands.SelectFixtureLabel,
                    CommandParameter = info,
                    CanAddToQuickAccessToolBar = true,
                };

                //hook into quick access item creation so that we can add an icon and tooltip
                item.CreatingQuickAccessItem += FixtureLabelMenuItem_CreatingQuickAccessItem;

                MultiBinding mb = new MultiBinding() { Converter = new AreEqualConverter(), Mode = BindingMode.OneWay };
                mb.Bindings.Add(new Binding("Id") { Source = info });
                mb.Bindings.Add(new Binding("ViewModel.ActivePlanController.SelectedPlanDocument.FixtureLabel.Id") { Source = this });
                BindingOperations.SetBinding(item, DynamicMenuItem.IsCheckedProperty, mb);


                this.AvailableFixtureLabelsDisplay.Items.Add(item);
            }

        }
    }
}