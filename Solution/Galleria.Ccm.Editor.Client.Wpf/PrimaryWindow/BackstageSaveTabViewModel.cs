﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
//  V8-27496 : L.Luong
//      Created
// V8-28018 : L.ineson
//  Added checks to make sure that this doesn't fall over when there is no
// repository connection.
// V8-27964 : A.Silva
//      Added automatic selection of Merchandising Group if the Planogram Group selected is linked to one, and there is no current selection.
// V8-28342 : A.Kuszyk
//  Added polling refreshes to the Planogram Hierarchy.
// V8-28377 : L.Luong
//  Added selected Merchandising group command
// V8-28474 : L.Ineson
//  Added IsActive property.
// V8-28465 : J.Pickup
//  SaveToRepository now only accessible when the user has permission to do so.
// V8-28562 : J.Pickup
//  Fixed a bug whereby the whole app crashes when clicking backstage save without a connection to the repo.
#endregion

#region Version History: (CCM 8.0.1)
// V8-28684 : L.Ineson
//  Removed is not readonly check from save as to repository.
#endregion

#region Version History: (CCM 8.1.0)
// V8-29042 : L.Ineson
//  Removed save planogram option.
// V8-30279 : D.Pleasance
//  Amended so that PlanogramGroup defaults to the ParentPackageView's planogram group values (if loading from repository this will be the initial planogram group), 
//  and keeps track of the planograms planogram group if the user changes it.
// V8-30320 : D.Pleasance
//  Amended to update ParentPackageView.PlanogramGroupId when selected planogram group changes.
#endregion

#region Version History: (CCM 8.2.0)
// V8-30620 : A.Silva
//  Added two way syncing between Category and Product Group.
// V8-30151 : M.Shelley
//  Changed the "Save As" panel to use the merchandising group selector window
//  from the project Galleria.Ccm.Common.Wpf.Selectors
#endregion

#region Version History: (CCM 8.3.0)
//V8-32185 : L.Ineson
//  Added the ability to create and edit planogram groups.
// V8-32219 : M.Pettit
//  Fixed issue where the SaveAs window could be populated with a non-POG file type
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// View Model controller for BackstageSaveTabContent control.
    /// </summary>
    public sealed class BackstageSaveTabViewModel : ViewModelAttachedControlObject<BackstageSaveTabContent>
    {
        #region Nested Classes
        public enum SaveFunctionType
        {
            SaveToRepository,
            SavePlanogramAsFile
        }
        #endregion

        #region Fields

        /// <summary>
        ///     Flag indicating whether this tab is currently active.
        /// </summary>
        private Boolean _isActive;

        private readonly PlanogramHierarchyViewModel _masterPlanogramHierarchyView = new PlanogramHierarchyViewModel();

        /// <summary>
        ///     View to the currently active <c>Planogram</c>.
        /// </summary>
        private readonly PlanogramViewObjectView _activePlanogramView;

        private SaveFunctionType _saveFunction;
        private PlanogramGroup _selectedGroup;
        private Boolean _isContentEnabled;

        private Boolean _canUserEditPlanogramGroups;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath IsActiveProperty = WpfHelper.GetPropertyPath<BackstageSaveTabViewModel>(p => p.IsActive);
        public static readonly PropertyPath ActivePlanogramProperty = WpfHelper.GetPropertyPath<BackstageSaveTabViewModel>(p => p.ActivePlanogram);
        public static readonly PropertyPath SaveFunctionProperty = WpfHelper.GetPropertyPath<BackstageSaveTabViewModel>(p => p.SaveFunction);
        public static readonly PropertyPath SelectedGroupProperty = WpfHelper.GetPropertyPath<BackstageSaveTabViewModel>(p => p.SelectedGroup);
        public static readonly PropertyPath PlanogramHierarchyViewProperty = WpfHelper.GetPropertyPath<BackstageSaveTabViewModel>(o => o.PlanogramHierarchyView);
        public static readonly PropertyPath IsValidProperty = WpfHelper.GetPropertyPath<BackstageSaveTabViewModel>(p => p.IsValid);
        public static readonly PropertyPath IsContentEnabledProperty = WpfHelper.GetPropertyPath<BackstageSaveTabViewModel>(p => p.IsContentEnabled);

        //Commands
        public static readonly PropertyPath SaveToRepositoryCommandProperty = WpfHelper.GetPropertyPath<BackstageSaveTabViewModel>(p => p.SaveToRepositoryCommand);
        public static readonly PropertyPath SavePlanogramAsFileCommandProperty = WpfHelper.GetPropertyPath<BackstageSaveTabViewModel>(p => p.SavePlanogramAsFileCommand);
        public static readonly PropertyPath SelectMerchGroupCommandProperty = WpfHelper.GetPropertyPath<BackstageSaveTabViewModel>(p => p.SelectMerchGroupCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether this tab is active
        /// and should be updating.
        /// </summary>
        public Boolean IsActive
        {
            get { return _isActive; }
            set
            {
                if (_isActive != value)
                {
                    _isActive = value;
                    OnPropertyChanged(IsActiveProperty);

                    OnIsActiveChanged();
                }
            }
        }

        /// <summary>
        /// Returns the active planogram.
        /// </summary>
        public PlanogramView ActivePlanogram
        {
            get { return _activePlanogramView.Model; }
        }

        /// <summary>
        /// Gets which save function the user wants
        /// </summary>
        public SaveFunctionType SaveFunction
        {
            get { return _saveFunction; }
            set
            {
                _saveFunction = value;
                OnPropertyChanged(SaveFunctionProperty);
            }
        }

        /// <summary>
        /// gets these selected group
        /// </summary>
        public PlanogramGroup SelectedGroup
        {
            get { return _selectedGroup; }
            set
            {
                if (Equals(_selectedGroup, value)) return;
                Boolean isInstanceChange = value != null &&
                                           _selectedGroup != null &&
                                           _selectedGroup.ProductGroupId != null &&
                                           value.ProductGroupId != null &&
                                           _selectedGroup.ProductGroupId == value.ProductGroupId;
                _selectedGroup = value;
                OnPropertyChanged(SelectedGroupProperty);

                if (!isInstanceChange) OnSelectedGroupChanged(value);
            }
        }

        /// <summary>
        /// Checks if the SaveToRepository is valid
        /// </summary>
        public Boolean IsValid
        {
            get
            {
                return this.SelectedGroup != null &&
                        !(String.IsNullOrEmpty(ActivePlanogram.Name));
            }
        }

        /// <summary>
        /// Returns the master planogram hierarchy view.
        /// </summary>
        public PlanogramHierarchyViewModel PlanogramHierarchyView
        {
            get { return _masterPlanogramHierarchyView; }
        }

        /// <summary>
        /// Gets/Sets whether the content is enabled
        /// </summary>
        public Boolean IsContentEnabled
        {
            get { return _isContentEnabled; }
            private set
            {
                _isContentEnabled = value;
                OnPropertyChanged(IsContentEnabledProperty);
            }
        }

        #endregion

        #region Constructor

        public BackstageSaveTabViewModel()
            : this(App.ViewState.ActivePlanogramView)
        { }

        /// <summary>
        /// Testing constructor
        /// </summary>
        /// <param name="planView"></param>
        internal BackstageSaveTabViewModel(PlanogramViewObjectView planView)
        {
            //check if the user can edit plan groups.
            _canUserEditPlanogramGroups =
                (App.ViewState.IsConnectedToRepository)?
                   ApplicationContext.User.IsInRole(DomainPermission.PlanogramHierarchyEdit.ToString())
                   : false;


            App.ViewState.EntityIdChanged += ViewState_EntityIdChanged;
            _activePlanogramView = planView;
            _activePlanogramView.ModelChanged += ActivePlanogramViewOnModelChanged;
            
            //default the selected save function to repository if we are connected or
            // file if we are not.
            this.SaveFunction = (App.ViewState.IsConnectedToRepository)? 
                SaveFunctionType.SaveToRepository : SaveFunctionType.SavePlanogramAsFile;

            //if we are unit testing then set then set the view model to active
            if (App.IsUnitTesting) this.IsActive = true;
        }


        #endregion

        #region Commands

        #region SavePlanogramCommand

        //private RelayCommand _savePlanogramCommand;

        ///// <summary>
        ///// sets the visibility of the SavePlanogram Option
        ///// </summary>
        //public RelayCommand SavePlanogramCommand
        //{
        //    get
        //    {
        //        if (_savePlanogramCommand == null)
        //        {
        //            _savePlanogramCommand = new RelayCommand(
        //                p => SavePlanogram_Executed(),
        //                p => SavePlanogram_CanExecute())
        //            {
        //                FriendlyName = Message.Generic_Save,
        //                FriendlyDescription = Message.SavePlanogram_Desc,
        //                Icon = ImageResources.Save_32,
        //            };
        //            base.ViewModelCommands.Add(_savePlanogramCommand);
        //        }
        //        return _savePlanogramCommand;
        //    }
        //}

        //public Boolean SavePlanogram_CanExecute()
        //{
        //    if (ActivePlanogram != null)
        //    {
        //        if (ActivePlanogram.Model.IsNew)
        //        {
        //            this.SavePlanogramCommand.DisabledReason = Message.DisabledReason_PlanogramIsNew;
        //            return false;
        //        }

        //        if (ActivePlanogram.ParentPackageView.Model.IsReadOnly)
        //        {
        //            this.SavePlanogramCommand.DisabledReason = Message.DisabledReason_IsReadOnly;
        //            return false;
        //        }
        //    }
        //    return true;
        //}

        //public void SavePlanogram_Executed()
        //{
        //    SaveFunction = SaveFunctionType.SavePlanogram;
        //}

        #endregion

        #region SaveToRepositoryCommand

        private RelayCommand _saveToRepositoryCommand;

        /// <summary>
        /// sets the visibility of the SaveToRepository Option
        /// </summary>
        public RelayCommand SaveToRepositoryCommand
        {
            get
            {
                if (_saveToRepositoryCommand == null)
                {
                    _saveToRepositoryCommand = new RelayCommand(
                        p => SaveToRepository_Executed(),
                        P => SaveToRepository_CanExecute())
                    {
                        FriendlyName = Message.BackstageSaveTab_SaveAsRepository_Title,
                        FriendlyDescription = Message.BackstageSaveTab_SaveToRepository_Desc,
                        Icon = ImageResources.SaveToDatabase_32,
                    };
                    base.ViewModelCommands.Add(_saveToRepositoryCommand);
                }
                return _saveToRepositoryCommand;
            }
        }

        public Boolean SaveToRepository_CanExecute()
        {
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.SaveToRepositoryCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }
            
            return true;
        }

        public void SaveToRepository_Executed()
        {
            SaveFunction = SaveFunctionType.SaveToRepository;
        }

        #endregion

        #region SavePlanogramAsFileCommand

        private RelayCommand _savePlanogramAsFileCommand;

        /// <summary>
        /// sets the visibility of the SavePlanogramAsFile Option
        /// </summary>
        public RelayCommand SavePlanogramAsFileCommand
        {
            get
            {
                if (_savePlanogramAsFileCommand == null)
                {
                    _savePlanogramAsFileCommand = new RelayCommand(
                        p => SavePlanogramAsFile_Executed())
                    {
                        FriendlyName = Message.Ribbon_SaveAsFile,
                        FriendlyDescription = Message.Ribbon_SavePlanogram_Desc,
                        Icon = ImageResources.SaveToFile_32,
                    };
                    base.ViewModelCommands.Add(_savePlanogramAsFileCommand);
                }
                return _savePlanogramAsFileCommand;
            }
        }

        public void SavePlanogramAsFile_Executed()
        {
            SaveFunction = SaveFunctionType.SavePlanogramAsFile;
        }

        #endregion

        #region SelectMerchGroupCommand

        private RelayCommand _selectMerchGroupCommand;

        /// <summary>
        /// Cancels any changes made and closes
        /// the window.
        /// </summary>
        public RelayCommand SelectMerchGroupCommand
        {
            get
            {
                if (_selectMerchGroupCommand == null)
                {
                    _selectMerchGroupCommand = new RelayCommand(
                        p => SelectMerchGroup_Executed(),
                        p => SelectMerchGroup_CanExecute())
                    {
                        FriendlyName = Message.PlanogramProperties_SelectMerch_Select,
                        FriendlyDescription = Message.PlanogramProperties_SelectMerch_SelectCategory
                    };
                    base.ViewModelCommands.Add(_selectMerchGroupCommand);
                }
                return _selectMerchGroupCommand;
            }
        }

        private Boolean SelectMerchGroup_CanExecute()
        {
            if (App.ViewState.IsConnectedToRepository)
            {
                return true;
            }
            else
            {
                _selectMerchGroupCommand.DisabledReason = Message.PlanogramProperties_SelectMerch_Disabled_NoConnection;
                return false;
            }
        }

        private void SelectMerchGroup_Executed()
        {
            if (App.ViewState.IsConnectedToRepository)
            {
                //Show product universe selector
                MerchGroupSelectionWindow win = new MerchGroupSelectionWindow();
                win.SelectionMode = System.Windows.Controls.DataGridSelectionMode.Single;
                App.ShowWindow(win, true);

                //If dialog returned true
                if (win.SelectionResult != null && win.SelectionResult.Count > 0)
                {
                    //set category code and name
                    this.ActivePlanogram.CategoryCode = win.SelectionResult.First().Code;
                    this.ActivePlanogram.CategoryName = win.SelectionResult.First().Name;
                }
            }
        }

        #endregion

        #region NewPlanogramGroupCommand

        private RelayCommand _newPlanogramGroupCommand;

        /// <summary>
        /// Adds a new PlanogramGroup to the Hierarchy.
        /// </summary>
        public RelayCommand NewPlanogramGroupCommand
        {
            get
            {
                if (_newPlanogramGroupCommand == null)
                {
                    _newPlanogramGroupCommand = new RelayCommand(
                        p => NewPlanogramGroup_Executed(),
                        p => NewPlanogramGroup_CanExecute())
                    {
                        FriendlyName = Message.Generic_New,
                        FriendlyDescription = Message.PlanRepository_NewPlanogramGroup_Description,
                        SmallIcon = ImageResources.PlanogramRepository_NewGroup_16
                    };
                    base.ViewModelCommands.Add(_newPlanogramGroupCommand);
                }
                return _newPlanogramGroupCommand;
            }
        }

        private Boolean NewPlanogramGroup_CanExecute()
        {
            if (!App.ViewState.IsConnectedToRepository) return false;

            if (!_canUserEditPlanogramGroups)
            {
                this.NewPlanogramGroupCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }
            

            if (this.SelectedGroup == null)
            {
                this.NewPlanogramGroupCommand.DisabledReason = Message.PlanRepository_DisabledReason_NoPlanogramGroupSelected;
                return false;
            }

            return true;
        }

        private void NewPlanogramGroup_Executed()
        {
            //stop polling
            _masterPlanogramHierarchyView.StopPollingAsync();

            Int32? groupIdToSelect = PlanogramRepositoryUIHelper.CreateNewPlanogramGroup(this.SelectedGroup);

            //update immediately then restart polling
            _masterPlanogramHierarchyView.FetchForCurrentEntity();
            _masterPlanogramHierarchyView.BeginPollingFetchForCurrentEntity();

            //try to select the new group
            if (groupIdToSelect.HasValue)
            {
                PlanogramGroup newGroup = _masterPlanogramHierarchyView.Model.EnumerateAllGroups().FirstOrDefault(i => i.Id == groupIdToSelect.Value);
                if (newGroup != null) this.SelectedGroup = newGroup;
            }
        }

        #endregion

        #region EditPlanogramGroupCommand

        private RelayCommand _editPlanogramGroupCommand;

        /// <summary>
        /// Edits a PlanogramGroup in the Hierarchy.
        /// </summary>
        public RelayCommand EditPlanogramGroupCommand
        {
            get
            {
                if (_editPlanogramGroupCommand == null)
                {
                    _editPlanogramGroupCommand = new RelayCommand(
                        p => EditPlanogramGroup_Executed(),
                        p => EditPlanogramGroup_CanExecute())
                    {
                        FriendlyName = Message.Generic_Edit,
                        FriendlyDescription = Message.PlanRepository_PlanogramGroupEdit_Description,
                        Icon = ImageResources.PlanogramRepository_EditGroup_32,
                        SmallIcon = ImageResources.PlanogramRepository_EditGroup_16,
                    };
                    base.ViewModelCommands.Add(_editPlanogramGroupCommand);
                }
                return _editPlanogramGroupCommand;
            }
        }

        private Boolean EditPlanogramGroup_CanExecute()
        {
            if (!App.ViewState.IsConnectedToRepository) return false;

            if (!_canUserEditPlanogramGroups)
            {
                this.NewPlanogramGroupCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            if (this.SelectedGroup == null)
            {
                this.EditPlanogramGroupCommand.DisabledReason = Message.PlanRepository_DisabledReason_NoPlanogramGroupSelected;
                return false;
            }

            return true;
        }

        private void EditPlanogramGroup_Executed()
        {
            //stop polling
            _masterPlanogramHierarchyView.StopPollingAsync();


            Boolean showMerchGroupSelector = ApplicationContext.User.IsInRole(DomainPermission.SyncWithMerchandisingHierarchy.ToString());
            PlanogramRepositoryUIHelper.ShowPlanogramGroupEditWindow(this.SelectedGroup, showMerchGroupSelector);


            //start polling
            _masterPlanogramHierarchyView.BeginPollingFetchForCurrentEntity();
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out actions when the value of the IsActive property changes.
        /// </summary>
        private void OnIsActiveChanged()
        {
            if (this.IsActive && App.ViewState.IsConnectedToRepository)
            {
                if (_masterPlanogramHierarchyView.Model == null)
                {
                    //if we have no model yet then fetch immediately.
                    _masterPlanogramHierarchyView.FetchForCurrentEntity();

                    this.SelectedGroup = (_masterPlanogramHierarchyView.Model != null) ?
                        _masterPlanogramHierarchyView.Model.RootGroup : null;
                }

                //start polling
                _masterPlanogramHierarchyView.BeginPollingFetchForCurrentEntity();
            }
            else
            {
                //stop polling
                _masterPlanogramHierarchyView.StopPollingAsync();


            }

            UpdateForActivePlanogram();
        }

        /// <summary>
        /// Checks for entity changes to update groups
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewState_EntityIdChanged(Object sender, EventArgs e)
        {
            if (App.ViewState.IsConnectedToRepository)
            {
                _canUserEditPlanogramGroups =
                    ApplicationContext.User.IsInRole(DomainPermission.PlanogramHierarchyEdit.ToString());

                _masterPlanogramHierarchyView.FetchForCurrentEntity();

                this.SelectedGroup = (_masterPlanogramHierarchyView.Model != null) ?
                    _masterPlanogramHierarchyView.Model.RootGroup : null;

            }
            else
            {
                _canUserEditPlanogramGroups = false;

                //we are not connected to a repository to make sure the file save function is selected.
                this.SaveFunction = SaveFunctionType.SavePlanogramAsFile;
            }
        }

        /// <summary>
        ///     Invoked whenever the user selects a <see cref="PlanogramGroup"/>.
        /// </summary>
        /// <param name="value">The current <c>Planogram Group</c> to sync the <c>Category</c> to, if there is one linked.</param>
        private void OnSelectedGroupChanged(PlanogramGroup value)
        {
            //  Notify a change affecting the validity.
            OnPropertyChanged(IsValidProperty);

            //  Check the required values are present.
            if (value == null || ActivePlanogram == null) return;

            //  Sync the Planogram's Package Planogram Group association.
            PackageViewModel packageView = ActivePlanogram.ParentPackageView;
            if (packageView != null)
            {
                packageView.PlanogramGroupId = value.Id;
                packageView.PlanogramGroupName = value.Name;
            }

            //  If already syncing, no need to sync from here.
            if (_isSyncingCategory) return;

            using (CategorySyncing)
            {
                //  Nothing else to do is there is no Product Group chosen.
                if (!value.ProductGroupId.HasValue) return;

                //  Nothing else to do if the Product Group has no Category linked.
                ProductGroupInfo info;
                if (!TryFetchProductGroupInfo(value.ProductGroupId.Value, out info)) return;

                //  Update the Category to agree with the current Product Group.
                ActivePlanogram.CategoryCode = info.Code;
                ActivePlanogram.CategoryName = info.Name;
            }
        }

        /// <summary>
        ///     Called whenever the Active Planogram View Model changes.
        /// </summary>
        private void ActivePlanogramViewOnModelChanged(Object sender, ViewStateObjectModelChangedEventArgs<PlanogramView> e)
        {
            //  Unsubscribe from PropertyChanged on the old model.
            if (e.OldModel != null)
            {
                e.OldModel.PropertyChanged -= ActivePlanogramViewOnPropertyChanged;
            }

            //  Subscribe to PropertyChanged on the new model.
            if (e.NewModel != null)
            {
                e.NewModel.PropertyChanged += ActivePlanogramViewOnPropertyChanged;
            }
        }

        /// <summary>
        ///     Called whenever a property in the <see cref="ActivePlanogram"/> changes.
        /// </summary>
        private void ActivePlanogramViewOnPropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            //  Process changes to the Active Planogram's Category Code property.
            if (String.Equals(e.PropertyName, Planogram.CategoryCodeProperty.Name))
            {
                //  If already syncing, no need to sync from here.
                if (_isSyncingCategory) return;

                using (CategorySyncing)
                {
                    //  Obtain the Category Code, and continue only if there is any.
                    String code = ActivePlanogram.CategoryCode;
                    if (String.IsNullOrWhiteSpace(code)) return;

                    //  Get the Product Group Info for the Active Planogram's Category Code,
                    //  continue only if there is any.
                    ProductGroupInfo info;
                    if (!TryFetchProductGroupInfo(code, out info)) return;

                    //  Update the Product Group to agree with the current Category.
                    PlanogramGroup match = PlanogramHierarchyView.RootGroup
                                                                 .EnumerateAllChildGroups()
                                                                 .Select(g => g.PlanogramGroup)
                                                                 .FirstOrDefault(g => g.ProductGroupId == info.Id);

                    if (match != null) SelectedGroup = match;
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates values based on the currently active planogram.
        /// </summary>
        private void UpdateForActivePlanogram()
        {
            PlanogramView activePlan = this.ActivePlanogram;
            OnPropertyChanged(ActivePlanogramProperty);

            this.IsContentEnabled = (activePlan != null);
            OnPropertyChanged(IsValidProperty);

            //update the save function back to file if we are not
            // connected to a repository.
            if (this.SaveFunction == SaveFunctionType.SaveToRepository
                    && !App.ViewState.IsConnectedToRepository)
            {
                this.SaveFunction = SaveFunctionType.SavePlanogramAsFile; 
            }

            //revert default plan type to the default as we are potentially 
            //performing a SaveAs not an Export
            if (activePlan != null)
            {
                activePlan.SaveSettings.SaveAsFileType = PlanogramExportFileType.POG;
            }

            if (activePlan != null && activePlan.ParentPackageView != null && _masterPlanogramHierarchyView.Model != null)
            {
                PlanogramGroup groupToSelect = null;
                if (activePlan.ParentPackageView.PlanogramGroupId != null)
                {
                    groupToSelect = _masterPlanogramHierarchyView.Model.EnumerateAllGroups().Where(p => p.Id == activePlan.ParentPackageView.PlanogramGroupId).FirstOrDefault();
                }
                else if (!String.IsNullOrWhiteSpace(activePlan.CategoryCode))
                {
                    //  Get the Product Group Info for the Active Planogram's Category Code,
                    //  continue only if there is any.
                    ProductGroupInfo info;
                    if (TryFetchProductGroupInfo(activePlan.CategoryCode, out info))
                    {
                        //  Update the Product Group to agree with the current Category.
                        groupToSelect = _masterPlanogramHierarchyView.Model.EnumerateAllGroups().FirstOrDefault(g => g.ProductGroupId == info.Id);
                    }
                }

                if (groupToSelect == null && _masterPlanogramHierarchyView.Model.RootGroup != null)
                {
                    groupToSelect = _masterPlanogramHierarchyView.Model.RootGroup;
                }
                this.SelectedGroup = groupToSelect;
            }            
        }

        /// <summary>
        ///     Try fetching the <see cref="ProductGroupInfo"/> corresponding to the provided Category <paramref name="code"/>.
        /// </summary>
        /// <param name="code">The <c>Category Code</c> to fetch the corresponding <c>Product Group Info</c> for.</param>
        /// <param name="info">The corresponding <c>Product Group Info</c>, or <c>null</c> if there is none.</param>
        /// <returns><c>True</c> if the <c>Category Code</c> had a corresponding <c>Product Group Info</c>, <c>False</c> otherwise.</returns>
        private static Boolean TryFetchProductGroupInfo(String code, out ProductGroupInfo info)
        {
            info = ProductGroupInfoList.FetchByProductGroupCodes(new List<String> { code }).FirstOrDefault();
            return info != null;
        }

        /// <summary>
        ///     Try fetching the <see cref="ProductGroupInfo"/> corresponding to the provided <c>Product Group</c>  <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The <c>Product Group Id</c> to fetch the corresponding <c>Product Group Info</c> for.</param>
        /// <param name="info">The corresponding <c>Product Group Info</c>, or <c>null</c> if there is none.</param>
        /// <returns><c>True</c> if the <c>Product Group Id</c> had a corresponding <c>Product Group Info</c>, <c>False</c> otherwise.</returns>
        private static Boolean TryFetchProductGroupInfo(Int32 id, out ProductGroupInfo info)
        {
            info = ProductGroupInfoList.FetchByProductGroupIds(new List<Int32> { id }).FirstOrDefault();
            return info != null;
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (IsDisposed || !disposing) return;

            //  Remove any existing subscription to ActivePlanogramView events.
            if (_activePlanogramView != null)
            {
                if (_activePlanogramView.Model != null) _activePlanogramView.Model.PropertyChanged -= ActivePlanogramViewOnPropertyChanged;
                _activePlanogramView.ModelChanged -= ActivePlanogramViewOnModelChanged;
            }

            IsDisposed = true;
        }

        #endregion

        #region Category Syncing State

        private Boolean _isSyncingCategory;

        private CategorySyncingState CategorySyncing
        {
            get { return new CategorySyncingState(this); }
        }

        /// <summary>
        ///     Class to signal this instance is syncing Categories and Product Groups.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        private class CategorySyncingState : IDisposable
        {
            private BackstageSaveTabViewModel _source;

            internal CategorySyncingState(BackstageSaveTabViewModel source)
            {
                _source = source;
                _source._isSyncingCategory = true;
            }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            public void Dispose()
            {
                _source._isSyncingCategory = false;
                _source = null;
            }
        }

        #endregion
    }
}