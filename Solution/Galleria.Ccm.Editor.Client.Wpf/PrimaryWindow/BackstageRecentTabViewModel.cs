﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24261 : L.Hodson 
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Framework.Collections;
using Galleria.Ccm.Model;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Csla;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Viewmodel controller for the backstage recent tab content control.
    /// </summary>
    public sealed class BackstageRecentTabViewModel : ViewModelAttachedControlObject<BackstageRecentTabContent>
    {
        #region Fields
        private readonly RecentPlanogramListView _recentPlanogramsView;
        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath RecentPlanogramsProperty = WpfHelper.GetPropertyPath<BackstageRecentTabViewModel>(p => p.RecentPlanograms);

        //Commands
        public static readonly PropertyPath PinCommandProperty = WpfHelper.GetPropertyPath<BackstageRecentTabViewModel>(p => p.PinCommand);
        public static readonly PropertyPath UnpinCommandProperty = WpfHelper.GetPropertyPath<BackstageRecentTabViewModel>(p => p.UnpinCommand);
        public static readonly PropertyPath RemoveItemCommandProperty = WpfHelper.GetPropertyPath<BackstageRecentTabViewModel>(p => p.RemoveItemCommand);
        public static readonly PropertyPath ClearUnpinnedCommandProperty = WpfHelper.GetPropertyPath<BackstageRecentTabViewModel>(p => p.ClearUnpinnedCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the Planograms MRU list.
        /// </summary>
        public ReadOnlyBulkObservableCollection<RecentPlanogram> RecentPlanograms
        {
            get { return _recentPlanogramsView.BindableCollection; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public BackstageRecentTabViewModel()
        {
            _recentPlanogramsView = App.ViewState.RecentPlanogramsView;
        }

        #endregion

        #region Commands

        #region PinCommand

        private RelayCommand _pinCommand;

        /// <summary>
        /// Pins the given item
        /// </summary>
        public RelayCommand PinCommand
        {
            get
            {
                if (_pinCommand == null)
                {
                    _pinCommand = new RelayCommand(
                        p => Pin_Executed(p))
                        {
                            FriendlyName = Message.BackstageRecent_Pin,
                            SmallIcon = ImageResources.BackstageRecent_Pin
                        };
                    base.ViewModelCommands.Add(_pinCommand);
                }
                return _pinCommand;
            }
        }

        private void Pin_Executed(Object args)
        {
            RecentPlanogram item = args as RecentPlanogram;
            if (item != null && !item.IsPinned)
            {
                item.IsPinned = true;

                SaveChanges();
            }
        }

        #endregion

        #region UnpinCommand

        private RelayCommand _unpinCommand;

        /// <summary>
        /// Unpins the given item
        /// </summary>
        public RelayCommand UnpinCommand
        {
            get
            {
                if (_unpinCommand == null)
                {
                    _unpinCommand = new RelayCommand(
                        p => Unpin_Executed(p))
                    {
                        FriendlyName = Message.BackstageRecent_Unpin,
                        SmallIcon = ImageResources.BackstageRecent_Unpin
                    };
                    base.ViewModelCommands.Add(_unpinCommand);
                }
                return _unpinCommand;
            }
        }

        private void Unpin_Executed(Object args)
        {
            RecentPlanogram item = args as RecentPlanogram;
            if (item != null && item.IsPinned)
            {
                item.IsPinned = false;

                SaveChanges();
            }
        }

        #endregion

        #region RemoveItemCommand

        private RelayCommand _removeItemCommand;

        /// <summary>
        /// Removes the given item from the list
        /// </summary>
        public RelayCommand RemoveItemCommand
        {
            get
            {
                if (_removeItemCommand == null)
                {
                    _removeItemCommand = new RelayCommand(
                        p => RemoveItem_Executed(p))
                    {
                        FriendlyName = Message.BackstageRecent_RemoveItem
                    };
                    base.ViewModelCommands.Add(_removeItemCommand);
                }
                return _removeItemCommand;
            }
        }

        private void RemoveItem_Executed(Object args)
        {
            RecentPlanogram item = args as RecentPlanogram;
            if (item != null)
            {
                _recentPlanogramsView.Model.Remove(item);

                SaveChanges();
            }
        }

        #endregion

        #region ClearUnpinnedCommand

        private RelayCommand _clearUnpinnedCommand;

        /// <summary>
        /// Removes all unpinned items from the list
        /// </summary>
        public RelayCommand ClearUnpinnedCommand
        {
            get
            {
                if (_clearUnpinnedCommand == null)
                {
                    _clearUnpinnedCommand = new RelayCommand(
                        p => ClearUnpinned_Executed())
                    {
                        FriendlyName = Message.BackstageRecent_ClearUnpinned
                    };
                    base.ViewModelCommands.Add(_clearUnpinnedCommand);
                }
                return _clearUnpinnedCommand;
            }
        }

        private void ClearUnpinned_Executed()
        {
            _recentPlanogramsView.Model.RemoveList(
                _recentPlanogramsView.Model.Where(i => !i.IsPinned).ToArray());

            SaveChanges();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Saves changes to the list.
        /// </summary>
        private void SaveChanges()
        {
            try
            {
                _recentPlanogramsView.Save();
            }
            catch (DataPortalException) { }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                //nothing to do.

                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
