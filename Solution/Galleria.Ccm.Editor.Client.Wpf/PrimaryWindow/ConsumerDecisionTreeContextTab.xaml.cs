﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26956 : L.Luong
//      Created

#endregion

#endregion

using System.Windows;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews.ConsumerDecisionTree;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for ConsumerDecisionTreeContextTab.xaml
    /// </summary>
    public sealed partial class ConsumerDecisionTreeContextTab
    {
        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ConsumerDecisionTreePlanDocument), typeof(ConsumerDecisionTreeContextTab),
                new PropertyMetadata(null));

        public ConsumerDecisionTreePlanDocument ViewModel
        {
            get { return (ConsumerDecisionTreePlanDocument)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructors
        public ConsumerDecisionTreeContextTab()
        {
            InitializeComponent();
        } 
        #endregion
    }
}
