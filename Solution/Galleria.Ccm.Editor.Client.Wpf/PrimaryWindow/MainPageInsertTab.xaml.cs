﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Media3D;
using Fluent;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for MainPageInsertTab.xaml
    /// </summary>
    public sealed partial class MainPageInsertTab : RibbonTabItem
    {
        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(MainPageViewModel), typeof(MainPageInsertTab),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets the viewmodel controller for this window.
        /// </summary>
        public MainPageViewModel ViewModel
        {
            get { return (MainPageViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        public MainPageInsertTab()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        private void InsertComponent_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            //if the left button is pressed and we are not aready dragging
            if ((e.LeftButton == MouseButtonState.Pressed) && !DragDropBehaviour.IsDragging)
            {
                Grid galleryItem = (Grid)sender;

                InsertComponentType? componentType = galleryItem.Tag as InsertComponentType?;
                if (componentType.HasValue)
                {
                    InsertNewComponentRequest request = new InsertNewComponentRequest();
                    request.ComponentType = componentType.Value;

                    Mouse.OverrideCursor = Cursors.Hand;

                    DragDropBehaviour requestDrag = new DragDropBehaviour(request);
                    requestDrag.DragArea = App.Current.MainWindow;
                    requestDrag.SetAdornerFromImageSource(ImageResources.TODO);

                    requestDrag.DragComplete += new EventHandler(requestDrag_DragComplete);
                    

                    requestDrag.BeginDrag();
                }

            }
        }

        private void requestDrag_DragComplete(object sender, EventArgs e)
        {
            DragDropBehaviour requestDrag = (DragDropBehaviour)sender;
            requestDrag.DragComplete -= requestDrag_DragComplete;

            Mouse.OverrideCursor = null;
        }

        #endregion
    }

    public enum InsertComponentType
    {
        Bay,
        Shelf,
        Pegboard,
        Bar,
        Chest,
        Pallet,
        TextBox,
        Box
    }


    public sealed class InsertNewComponentRequest
    {
        public InsertComponentType ComponentType { get; set; }
        public Point3D PlacementPosition { get; set; }
        public IPlanItem HitPlanItem { get; set; }

        public InsertNewComponentRequest() { }
    }
}
