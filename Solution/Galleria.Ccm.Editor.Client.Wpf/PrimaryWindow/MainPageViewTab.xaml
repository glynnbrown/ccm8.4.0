﻿<!--Header Information-->
<!--Copyright © Galleria RTS Ltd 2013.-->
<!-- Version History : (8.0)
    L.Hodson ~ Created
    V8-25395 : A.Probyn
        ~ Moved library toggle button to here
    CCM-25963 : N.Haywood
        ~ Changed click behaviour for custom fixtures
    V8-24124 : A.Silva ~ Added button to edit the custom column layout IF supported by the selected plan 
        (must implement ICustomColumnLayoutView).
    V8-25797 : A.Silva ~ Modified the edit custom column layout button to find the active content 
        supporting ICustomColumnLayoutView from ViewModel.ActiveCustomColumnLayoutView, 
        which now includes both plans and panels.
    V8-26143 : A.Silva ~ Removed the button to manage Custom Column Layouts.
    V8-25436 : L.Luong
        ~Added custom highlight
    V8-27914 : L.Ineson ~ Hid auto size gridlines.
-->
<!-- Version History : (801)
    V8-25939 : J.Pickup
        ~ Reviewed the tooltips
    V8-27554 : I.George
        ~ Set  in RibbonGroupBox and SplitButton 
-->
<!-- Version History : (830)
    V8-32636 : A.Probyn
        ~ Added ShowAnnotations
-->
<fluent:RibbonTabItem x:Class="Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow.MainPageViewTab"
                      xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
                      xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
                      xmlns:fluent="clr-namespace:Fluent;assembly=Fluent"
                      xmlns:g="clr-namespace:Galleria.Framework.Controls.Wpf;assembly=Galleria.Framework.Controls.Wpf"
                      xmlns:commonControls="clr-namespace:Galleria.Ccm.Common.Wpf.Controls;assembly=Galleria.Ccm.Common.Wpf"
                      xmlns:root="clr-namespace:Galleria.Ccm.Editor.Client.Wpf"
                      xmlns:lg="clr-namespace:Galleria.Ccm.Editor.Client.Wpf.Resources.Language"
                      xmlns:local="clr-namespace:Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow"
                      xmlns:planControls="clr-namespace:Galleria.Ccm.Editor.Client.Wpf.PlanViews"
                      xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
                      xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                      xmlns:fg="clr-namespace:Galleria.Framework.ViewModel;assembly=Galleria.Framework"
                      mc:Ignorable="d"
                      Header="{x:Static lg:Message.Ribbon_TabHeader_View}"
                      x:Name="mainPageViewTab">

    <fluent:RibbonTabItem.Resources>

        <Style x:Key="MainPageViewTab_StyMenuListBox"
               TargetType="{x:Type ListBox}" BasedOn="{StaticResource {x:Type ListBox}}">
            <Setter Property="Background" Value="Transparent" />
            <Setter Property="BorderThickness" Value="0" />
            <Setter Property="Template">
                <Setter.Value>
                    <ControlTemplate TargetType="ListBox">
                        <ScrollViewer Margin="0" Focusable="false"
                                      BorderBrush="{TemplateBinding BorderBrush}"
                                      BorderThickness="{TemplateBinding BorderThickness}"
                                      Background="{TemplateBinding Background}">
                            <StackPanel Margin="2" IsItemsHost="True" />
                        </ScrollViewer>
                        <ControlTemplate.Triggers>
                            <Trigger Property="IsGrouping" Value="true">
                                <Setter Property="ScrollViewer.CanContentScroll" Value="false" />
                            </Trigger>
                        </ControlTemplate.Triggers>
                    </ControlTemplate>
                </Setter.Value>
            </Setter>
        </Style>

    </fluent:RibbonTabItem.Resources>

    <fluent:RibbonGroupBox Header="{x:Static lg:Message.Ribbon_GroupHeader_ViewPosition}"
                           DataContext="{Binding Path=ViewModel, ElementName=mainPageViewTab}"
                           IsEnabled="{Binding Path=ActivePlanController, ConverterParameter=isNotNull, 
                           Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}}"
                           >

        <fluent:ToggleButton Header="{x:Static lg:Message.Ribbon_ShowPositions}"
                             DataContext="{Binding Path=ActivePlanController.SelectedPlanDocument}"
                             IsChecked="{Binding Path={x:Static planControls:PlanVisualDocument.ShowPositionsProperty}}"
                             SizeDefinition="Large"
                             LargeIcon="{x:Static root:ImageResources.View_Positions_32}"
                             Icon="{x:Static root:ImageResources.View_Positions_32}"
                             IsEnabled="{Binding Path=ArePlanogramViewSettingsSupported}"
                             CanAddToQuickAccessToolBar="True"
                             fluent:KeyTip.Keys="{x:Static lg:KeyTips.Paste}"
                             fluent:KeyTip.AutoPlacement="False"
                             fluent:KeyTip.HorizontalAlignment="Left">
            <fluent:ToggleButton.ToolTip>
                <fluent:ScreenTip Title="{x:Static lg:Message.Ribbon_ShowPositions}"
                                  Text="{x:Static lg:Message.Ribbon_ShowPositions_Desc}"
                                  DisableReason="{x:Static lg:Message.DisabledReason_NotSupportedByDocument}" />
            </fluent:ToggleButton.ToolTip>
        </fluent:ToggleButton>

        <fluent:ToggleButton Header="{x:Static lg:Message.Ribbon_ShowImages}"
                             DataContext="{Binding Path=ActivePlanController.SelectedPlanDocument}"
                             IsChecked="{Binding Path={x:Static planControls:PlanVisualDocument.ShowProductImagesProperty}}"
                             SizeDefinition="Large"
                             LargeIcon="{x:Static root:ImageResources.View_RealImages_32}"
                             Icon="{x:Static root:ImageResources.View_RealImages_16}"
                             IsEnabled="{Binding Path=ArePlanogramViewSettingsSupported}"
                             CanAddToQuickAccessToolBar="True"
                             fluent:KeyTip.Keys="{x:Static lg:KeyTips.Generic_Import}"
                             fluent:KeyTip.AutoPlacement="False"
                             fluent:KeyTip.HorizontalAlignment="Left">
            <fluent:ToggleButton.ToolTip>
                <fluent:ScreenTip Title="{x:Static lg:Message.Ribbon_ShowImages}"
                                  Text="{x:Static lg:Message.Ribbon_ShowImages_Desc}"
                                  DisableReason="{x:Static lg:Message.DisabledReason_NotSupportedByDocument}" />
            </fluent:ToggleButton.ToolTip>
        </fluent:ToggleButton>

        <fluent:ToggleButton Header="{x:Static lg:Message.Ribbon_ShowPositionUnits}"
                             DataContext="{Binding Path=ActivePlanController.SelectedPlanDocument}"
                             IsChecked="{Binding Path={x:Static planControls:PlanVisualDocument.ShowPositionUnitsProperty}}"
                             SizeDefinition="Middle"
                             Icon="{x:Static root:ImageResources.View_ProductUnits_16}"
                             IsEnabled="{Binding Path=ArePlanogramViewSettingsSupported}"
                             CanAddToQuickAccessToolBar="True"
                             fluent:KeyTip.Keys="{x:Static lg:KeyTips.Unassign}"
                             fluent:KeyTip.AutoPlacement="False"
                             fluent:KeyTip.HorizontalAlignment="Left">
            <fluent:ToggleButton.ToolTip>
                <fluent:ScreenTip Title="{x:Static lg:Message.Ribbon_ShowPositionUnits}"
                                  Text="{x:Static lg:Message.Ribbon_ShowPositionUnits_Desc}"
                                  DisableReason="{x:Static lg:Message.DisabledReason_NotSupportedByDocument}" />
            </fluent:ToggleButton.ToolTip>
        </fluent:ToggleButton>

        <fluent:ToggleButton Header="{x:Static lg:Message.Ribbon_ShowProductShapes}"
                             DataContext="{Binding Path=ActivePlanController.SelectedPlanDocument}"
                             SizeDefinition="Middle"
                             Icon="{x:Static root:ImageResources.View_ProductShapes_16}"
                             IsEnabled="{Binding Path=ArePlanogramViewSettingsSupported}"
                             IsChecked="{Binding Path={x:Static planControls:PlanVisualDocument.ShowProductShapesProperty}}"
                             CanAddToQuickAccessToolBar="True"
                             fluent:KeyTip.Keys="{x:Static lg:KeyTips.Save}"
                             fluent:KeyTip.AutoPlacement="False"
                             fluent:KeyTip.HorizontalAlignment="Left">
            <fluent:ToggleButton.ToolTip>
                <fluent:ScreenTip Title="{x:Static lg:Message.Ribbon_ShowProductShapes}"
                                  Text="{x:Static lg:Message.Ribbon_ShowProductShapes_Desc}"
                                  DisableReason="{x:Static lg:Message.DisabledReason_NotSupportedByDocument}" />
            </fluent:ToggleButton.ToolTip>
        </fluent:ToggleButton>


        <fluent:ToggleButton Header="{x:Static lg:Message.Ribbon_ShowFillPatterns}"
                             DataContext="{Binding Path=ActivePlanController.SelectedPlanDocument}"
                             SizeDefinition="Middle"
                             Icon="{x:Static root:ImageResources.View_ProductFillPatterns_16}"
                             IsEnabled="{Binding Path=ArePlanogramViewSettingsSupported}"
                             IsChecked="{Binding Path={x:Static planControls:PlanVisualDocument.ShowProductFillPatternsProperty}}"
                             CanAddToQuickAccessToolBar="True"
                             fluent:KeyTip.Keys="{x:Static lg:KeyTips.Filter}"
                             fluent:KeyTip.AutoPlacement="False"
                             fluent:KeyTip.HorizontalAlignment="Left">
            <fluent:ToggleButton.ToolTip>
                <fluent:ScreenTip Title="{x:Static lg:Message.Ribbon_ShowFillPatterns}"
                                  Text="{x:Static lg:Message.Ribbon_ShowFillPatterns_Desc}"
                                  DisableReason="{x:Static lg:Message.DisabledReason_NotSupportedByDocument}" />
            </fluent:ToggleButton.ToolTip>
        </fluent:ToggleButton>




    </fluent:RibbonGroupBox>

    <fluent:RibbonGroupBox Header="{x:Static lg:Message.Ribbon_GroupHeader_ViewFixture}"
                           DataContext="{Binding Path=ViewModel, ElementName=mainPageViewTab}"
                           >
        <fluent:RibbonGroupBox.IsEnabled>
            <MultiBinding Converter="{StaticResource {x:Static g:ResourceKeys.ConverterMultiConditionToBool}}">
                <Binding Path="ActivePlanController" ConverterParameter="isNotNull"
                         Converter="{StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}" />
                <Binding Path="ActivePlanController.SelectedPlanDocument.ArePlanogramViewSettingsSupported" />
            </MultiBinding>
        </fluent:RibbonGroupBox.IsEnabled>

        <fluent:ToggleButton Header="{x:Static lg:Message.Ribbon_ShowImages}"
                             DataContext="{Binding Path=ActivePlanController.SelectedPlanDocument}"
                             IsChecked="{Binding Path={x:Static planControls:PlanVisualDocument.ShowFixtureImagesProperty}}"
                             SizeDefinition="Middle"
                             LargeIcon="{x:Static root:ImageResources.View_RealImages_32}"
                             Icon="{x:Static root:ImageResources.View_RealImages_16}"
                             CanAddToQuickAccessToolBar="True"
                             fluent:KeyTip.Keys="{x:Static lg:KeyTips.Margins}"
                             fluent:KeyTip.AutoPlacement="False"
                             fluent:KeyTip.HorizontalAlignment="Left">
            <fluent:ToggleButton.ToolTip>
                <fluent:ScreenTip Title="{x:Static lg:Message.Ribbon_ShowImages}"
                                  Text="{x:Static lg:Message.Ribbon_ShowImages_Desc}"
                                  DisableReason="{x:Static lg:Message.DisabledReason_NotSupportedByDocument}" />
            </fluent:ToggleButton.ToolTip>
        </fluent:ToggleButton>

        <fluent:ToggleButton Header="{x:Static lg:Message.Ribbon_ShowFillPatterns}"
                             DataContext="{Binding Path=ActivePlanController.SelectedPlanDocument}"
                             SizeDefinition="Middle"
                             Icon="{x:Static root:ImageResources.View_ProductFillPatterns_16}"
                             IsEnabled="{Binding Path=ArePlanogramViewSettingsSupported}"
                             IsChecked="{Binding Path={x:Static planControls:PlanVisualDocument.ShowFixtureFillPatternsProperty}}"
                             CanAddToQuickAccessToolBar="True"
                             fluent:KeyTip.Keys="{x:Static lg:KeyTips.TextBox}"
                             fluent:KeyTip.AutoPlacement="False"
                             fluent:KeyTip.HorizontalAlignment="Left">
            <fluent:ToggleButton.ToolTip>
                <fluent:ScreenTip Title="{x:Static lg:Message.Ribbon_ShowFillPatterns}"
                                  Text="{x:Static lg:Message.Ribbon_ShowFillPatterns_Desc}"
                                  DisableReason="{x:Static lg:Message.DisabledReason_NotSupportedByDocument}" />
            </fluent:ToggleButton.ToolTip>
        </fluent:ToggleButton>


        <fluent:SplitButton Command="{x:Static local:MainPageCommands.ShowFixtureLabelEditor}"
                            SizeDefinition="Middle"
                            Header="{Binding Path=Command.FriendlyName, RelativeSource={RelativeSource Self}}"
                            LargeIcon="{Binding Path=Command.Icon, RelativeSource={RelativeSource Self}}"
                            Icon="{Binding Path=Command.Icon, RelativeSource={RelativeSource Self}}"
                            g:CommandToolTip.SourceCommand="{x:Static local:MainPageCommands.ShowFixtureLabelEditor}"
                            CanAddToQuickAccessToolBar="True"
                            fluent:KeyTip.Keys="{x:Static lg:KeyTips.Copy}"
                            fluent:KeyTip.AutoPlacement="False"
                            fluent:KeyTip.HorizontalAlignment="Left">
        


        <commonControls:DynamicMenuItem Header="{x:Static lg:Message.Generic_None}"
                             DataContext="{Binding Path=ActivePlanController.SelectedPlanDocument}"
                             IsCheckable="True"
                             IsChecked="{Binding Path=FixtureLabel, Mode=OneWay,
                                Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}},
                                ConverterParameter=isNull}"
                            Command="{x:Static local:MainPageCommands.SelectFixtureLabel}"      
                            CommandParameter="{x:Null}" 
                            CanAddToQuickAccessToolBar="True"
                            CreatingQuickAccessItem="FixtureLabelMenuItem_CreatingQuickAccessItem"
                            fluent:KeyTip.Keys="{x:Static lg:KeyTips.New}"
                            fluent:KeyTip.AutoPlacement="False"
                            fluent:KeyTip.HorizontalAlignment="Left"/>

            <Separator />

        <fluent:MenuItem Header="{x:Static lg:Message.LabelEditor_Custom}"
                             DataContext="{Binding Path=ActivePlanController.SelectedPlanDocument}"
                             IsCheckable="True"
                             IsEnabled="{Binding Path=CustomFixtureLabel, Mode=OneWay,
                                Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}},
                                ConverterParameter=isNotNull}"
                             Click="FixtureLabelMenuItem_Click"
                             CommandParameter="{Binding Path=CustomFixtureLabel}"
                             IsChecked="{Binding Path=IsCustomFixtureLabelSelected, Mode=OneWay}"
                             fluent:KeyTip.Keys="{x:Static lg:KeyTips.Copy}"
                             fluent:KeyTip.AutoPlacement="False"
                             fluent:KeyTip.HorizontalAlignment="Left"/>

            <Separator />

            <ItemsControl x:Name="AvailableFixtureLabelsDisplay"
                ScrollViewer.CanContentScroll="False" ScrollViewer.HorizontalScrollBarVisibility="Hidden"
                ScrollViewer.VerticalScrollBarVisibility="Hidden"/>


            <Separator />

            <fluent:MenuItem Command="{x:Static local:MainPageCommands.ShowFixtureLabelEditor}"
                             Header="{x:Static lg:Message.Ribbon_ShowFixtureLabelEditor_MenuHeader}"
                             SizeDefinition="Middle"
                             Icon="{Binding Path=Command.Icon, RelativeSource={RelativeSource Self}}" 
                             CanAddToQuickAccessToolBar="True"
                             fluent:KeyTip.Keys="{x:Static lg:KeyTips.Margins}"
                             fluent:KeyTip.AutoPlacement="False"
                             fluent:KeyTip.HorizontalAlignment="Left"/>

        </fluent:SplitButton>


        <Separator />

        <fluent:CheckBox Header="{x:Static lg:Message.Ribbon_ShowChestWalls}"
                         DataContext="{Binding Path=ActivePlanController.SelectedPlanDocument}"
                         IsChecked="{Binding Path={x:Static planControls:PlanVisualDocument.ShowChestWallsProperty}}" 
                         ToolTip="{x:Static lg:Message.Ribbon_ShowChestWalls}"
                         CanAddToQuickAccessToolBar="True"
                         fluent:KeyTip.Keys="{x:Static lg:KeyTips.ViewLevelWeeks}"
                         fluent:KeyTip.AutoPlacement="False"
                         fluent:KeyTip.HorizontalAlignment="Left"/>

        <fluent:CheckBox Header="{x:Static lg:Message.Ribbon_RotateTopDownComponents}"
                         DataContext="{Binding Path=ActivePlanController.SelectedPlanDocument}"
                         IsChecked="{Binding Path={x:Static planControls:PlanVisualDocument.ShowChestsTopDownProperty}}" 
                         ToolTip="{x:Static lg:Message.Ribbon_RotateTopDownComponents}"
                         CanAddToQuickAccessToolBar="True"
                         fluent:KeyTip.Keys="{x:Static lg:KeyTips.Generic_Open}"
                         fluent:KeyTip.AutoPlacement="False"
                         fluent:KeyTip.HorizontalAlignment="Left"/>

        <fluent:CheckBox Header="{x:Static lg:Message.Ribbon_ShowShelfRisers}"
                         DataContext="{Binding Path=ActivePlanController.SelectedPlanDocument}"
                         IsChecked="{Binding Path={x:Static planControls:PlanVisualDocument.ShowShelfRisersProperty}}"
                         ToolTip="{x:Static lg:Message.Ribbon_ShowShelfRisers}"
                         CanAddToQuickAccessToolBar="True"
                         fluent:KeyTip.Keys="{x:Static lg:KeyTips.Remove}"
                         fluent:KeyTip.AutoPlacement="False"
                         fluent:KeyTip.HorizontalAlignment="Left"/>

        <fluent:CheckBox Header="{x:Static lg:Message.Ribbon_ShowNotches}"
                         DataContext="{Binding Path=ActivePlanController.SelectedPlanDocument}"
                         IsChecked="{Binding Path={x:Static planControls:PlanVisualDocument.ShowNotchesProperty}}" 
                         ToolTip="{x:Static lg:Message.Ribbon_ShowNotches}"
                         CanAddToQuickAccessToolBar="True"
                         fluent:KeyTip.Keys="{x:Static lg:KeyTips.New}"
                         fluent:KeyTip.AutoPlacement="False"
                         fluent:KeyTip.HorizontalAlignment="Left"/>

        <fluent:CheckBox Header="{x:Static lg:Message.Ribbon_ShowPegHoles}"
                         DataContext="{Binding Path=ActivePlanController.SelectedPlanDocument}"
                         IsChecked="{Binding Path={x:Static planControls:PlanVisualDocument.ShowPegHolesProperty}}"
                         ToolTip="{x:Static lg:Message.Ribbon_ShowPegHoles}"
                         CanAddToQuickAccessToolBar="True"
                         fluent:KeyTip.Keys="{x:Static lg:KeyTips.Help}"
                         fluent:KeyTip.AutoPlacement="False"
                         fluent:KeyTip.HorizontalAlignment="Left"/>

        <fluent:CheckBox Header="{x:Static lg:Message.Ribbon_ShowPegs}"
                         DataContext="{Binding Path=ActivePlanController.SelectedPlanDocument}"
                         IsChecked="{Binding Path={x:Static planControls:PlanVisualDocument.ShowPegsProperty}}" 
                         ToolTip="{x:Static lg:Message.Ribbon_ShowPegs}"
                         CanAddToQuickAccessToolBar="True"
                         fluent:KeyTip.Keys="{x:Static lg:KeyTips.AltExpandAll}"
                         fluent:KeyTip.AutoPlacement="False"
                         fluent:KeyTip.HorizontalAlignment="Left"/>

        <fluent:CheckBox Header="{x:Static lg:Message.Ribbon_ShowDividerLines}"
                         DataContext="{Binding Path=ActivePlanController.SelectedPlanDocument}"
                         IsChecked="{Binding Path={x:Static planControls:PlanVisualDocument.ShowDividerLinesProperty}}"
                         ToolTip="{x:Static lg:Message.Ribbon_ShowDividerLines}"
                         CanAddToQuickAccessToolBar="True"
                         fluent:KeyTip.Keys="{x:Static lg:KeyTips.PaperSize}"
                         fluent:KeyTip.AutoPlacement="False"
                         fluent:KeyTip.HorizontalAlignment="Left"/>

        <fluent:CheckBox Header="{x:Static lg:Message.Ribbon_ShowDividers}"
                         DataContext="{Binding Path=ActivePlanController.SelectedPlanDocument}"
                         IsChecked="{Binding Path={x:Static planControls:PlanVisualDocument.ShowDividersProperty}}" 
                         ToolTip="{x:Static lg:Message.Ribbon_ShowDividers}"
                         CanAddToQuickAccessToolBar="True"
                         fluent:KeyTip.Keys="{x:Static lg:KeyTips.Delete}"
                         fluent:KeyTip.AutoPlacement="False"
                         fluent:KeyTip.HorizontalAlignment="Left"/>

        <fluent:CheckBox Header="{x:Static lg:Message.Ribbon_ShowAnnotations}"
                         DataContext="{Binding Path=ActivePlanController.SelectedPlanDocument}"
                         IsChecked="{Binding Path={x:Static planControls:PlanVisualDocument.ShowAnnotationsProperty}}" 
                         ToolTip="{x:Static lg:Message.Ribbon_ShowAnnotations}"
                         CanAddToQuickAccessToolBar="True"
                         fluent:KeyTip.Keys="{x:Static lg:KeyTips.Add}"
                         fluent:KeyTip.AutoPlacement="False"
                         fluent:KeyTip.HorizontalAlignment="Left"/>


    </fluent:RibbonGroupBox>

    <fluent:RibbonGroupBox Header="{x:Static lg:Message.Ribbon_GroupHeader_ViewGridlines}"
                           DataContext="{Binding Path=ViewModel, ElementName=mainPageViewTab}"
                           IsEnabled="{Binding Path=ActivePlanController, ConverterParameter=isNotNull, 
                           Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}}"
                           >

        <StackPanel Orientation="Horizontal">

            <StackPanel Orientation="Vertical">


                <fluent:CheckBox Header="{x:Static lg:Message.Ribbon_ShowGridlines}"
                                 DataContext="{Binding Path=ActivePlanController.SelectedPlanDocument}"
                                 IsChecked="{Binding Path={x:Static planControls:PlanVisualDocument.ShowGridlinesProperty}}" 
                                 ToolTip="{x:Static lg:Message.Ribbon_ShowGridlines}"
                                 fluent:KeyTip.Keys="{x:Static lg:KeyTips.Group}"
                                 fluent:KeyTip.AutoPlacement="False"
                                 fluent:KeyTip.HorizontalAlignment="Left"/>

                <!--<fluent:CheckBox Header="Autosize Gridlines"
                                 DataContext="{Binding Path=ActivePlanController}"
                                 IsChecked="{Binding Path={x:Static planControls:PlanControllerViewModel.AutosizeGridlinesProperty}}" />-->

            </StackPanel>

            <!--<StackPanel Orientation="Vertical"
                        DataContext="{Binding Path=ActivePlanController}"
                        IsEnabled="{Binding Path={x:Static planControls:PlanControllerViewModel.AutosizeGridlinesProperty},
                                    ConverterParameter=equals:False,
                                    Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}}">

                <fluent:TextBox Header="Height" Width="80"
                        Text="{Binding Path={x:Static planControls:PlanControllerViewModel.GridlineFixedHeightProperty}}"/>

                <fluent:TextBox Header="Width" Width="80"
                        Text="{Binding Path={x:Static planControls:PlanControllerViewModel.GridlineFixedWidthProperty}}"/>

                <fluent:TextBox Header="Depth" Width="80"
                        Text="{Binding Path={x:Static planControls:PlanControllerViewModel.GridlineFixedDepthProperty}}"/>
            </StackPanel>-->

        </StackPanel>

    </fluent:RibbonGroupBox>

    <fluent:RibbonGroupBox Header="{x:Static lg:Message.Ribbon_GroupHeader_Library}"
                           DataContext="{Binding Path=ViewModel, ElementName=mainPageViewTab}"
                           >

        <fluent:ToggleButton Command="{x:Static local:MainPageCommands.ShowHideLibraryPanel}"
                             Header="{Binding Path=Command.FriendlyName, RelativeSource={RelativeSource Self}}"
                             LargeIcon="{Binding Path=Command.Icon, RelativeSource={RelativeSource Self}}"
                             Icon="{Binding Path=Command.SmallIcon, RelativeSource={RelativeSource Self}}"
                             SizeDefinition="Large"
                             g:CommandToolTip.SourceCommand="{Binding Path=Command, RelativeSource={RelativeSource Self}}"
                             IsChecked="{Binding Path={x:Static local:MainPageViewModel.IsLibraryPanelVisibleProperty}, Mode=OneWay}" 
                             CanAddToQuickAccessToolBar="True"
                             fluent:KeyTip.Keys="{x:Static lg:KeyTips.LocationType}"
                             fluent:KeyTip.AutoPlacement="False"
                             fluent:KeyTip.HorizontalAlignment="Left"/>


    </fluent:RibbonGroupBox>


</fluent:RibbonTabItem>