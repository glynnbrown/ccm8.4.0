﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-25042 : A.Silva ~ Created.
// V8-25798 : A.Silva ~ Now the Planogram Group Selector is used to select the value.
// V8-25959 : A.Silva ~ Corrected new Planograms not being saved to the selected Planogram Group.
// V8-26284 : A.Kuszyk ~ Amended to use Common.Wpf selector.
// V8-27143 : A.Silva ~ Amended so it will return the selected group.

#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    ///     Dialog window to provide a method by which a name and planogram group for <c>Save As</c> operations may be provided
    /// </summary>
    public partial class SaveAsRepositoryDialog
    {
        #region Properties

        #region Description Property

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof (String), typeof (SaveAsRepositoryDialog),
                new PropertyMetadata(String.Empty));

        /// <summary>
        ///     Gets or sets the description text for the window.
        /// </summary>
        public String Description
        {
            get { return (String) GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        #endregion

        #region IsValid Property

        public static readonly DependencyProperty IsValidProperty = DependencyProperty.Register("IsValid",
            typeof (Boolean), typeof (SaveAsRepositoryDialog), new PropertyMetadata(false));

        public Boolean IsValid
        {
            get { return (Boolean) GetValue(IsValidProperty); }
            set { SetValue(IsValidProperty, value); }
        }

        #endregion

        #region PlanogramNameValue Property

        public static readonly DependencyProperty PlanogramNameValueProperty =
            DependencyProperty.Register("PlanogramNameValue", typeof (String), typeof (SaveAsRepositoryDialog), new PropertyMetadata(String.Empty,Validate));

        /// <summary>
        ///     Gets/Sets the actual value for input 1
        /// </summary>
        public String PlanogramNameValue
        {
            get { return (String) GetValue(PlanogramNameValueProperty); }
            set { SetValue(PlanogramNameValueProperty, value); }
        }

        #endregion

        #region SelectedGroup Property

        public static readonly DependencyProperty SelectedGroupProperty =
            DependencyProperty.Register("SelectedGroup", typeof(PlanogramGroup), typeof (SaveAsRepositoryDialog),
            new PropertyMetadata(null, Validate));

        public PlanogramGroup SelectedGroup
        {
            get { return (PlanogramGroup)GetValue(SelectedGroupProperty); }
            set { SetValue(SelectedGroupProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        ///     Constructor
        /// </summary>
        public SaveAsRepositoryDialog()
        {
            // Show the busy cursor while loading the screen.
            Mouse.OverrideCursor = Cursors.Wait; 

            InitializeComponent();
            Loaded += SaveAsRepositoryDialog_Loaded;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Whenever the state of the properties on the screen changes, checks whether the current state of the screen is valid.
        /// </summary>
        private static void Validate(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = sender as SaveAsRepositoryDialog;
            if (senderControl == null) return;

            senderControl.IsValid = senderControl.SelectedGroup != null &&
                                    !(String.IsNullOrEmpty(senderControl.PlanogramNameValue));
        }

        /// <summary>
        ///     Focus on the Planogram's name and select all.
        /// </summary>
        private void SaveAsRepositoryDialog_Loaded(Object sender, RoutedEventArgs e)
        {
            Loaded -= SaveAsRepositoryDialog_Loaded;

            // Stop showing the busy cursor as loading the screen has finished.
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; })
                , DispatcherPriority.Background, null);

            // focus on the text and select all,
            // had to do this on a delay otherwise it loses it.
            Dispatcher.BeginInvoke(
                (Action) (() =>
                {
                    xPlanogramName.Focus();
                    xPlanogramName.SelectAll();
                }), DispatcherPriority.Background);
        }

        /// <summary>
        ///     Rejects the values for the dialog when clicking the <c>Cancel</c> button.
        /// </summary>
        private void XCancelButton_Click(Object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        /// <summary>
        ///     Accepts the values for the dialog when pressing <c>Enter</c>.
        /// </summary>
        private void XPlanogramName_KeyUp(Object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter &&
                SelectedGroup.Id > 0)
                DialogResult = true;
        }

        /// <summary>
        ///     Accepts the values for the dialog when clicking the <c>Save</c> button.
        /// </summary>
        private void XSaveButton_Click(Object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        #endregion
    }
}