﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// V8-27569 : A.Probyn
//  Created
// V8-28053 : L.Ineson
//  Amended GetPlanRenderSettings to copy all render settings from the selected doc.
// V8-28047 : A.Probyn 
//  Updated to include validation on NumberOfCopies
// V8-28077 : J.Pickup
//  Available document sizes now updated based on the selected printer. (LoadPaperSizes).
// V8-28094 : A.Probyn 
//  ~ Extended validation for BaysPerPage too
// V8-28079 : J.Pickup
//  ~ Printer will now use the specified media when printing. i.e User selects A3, then prints out in A3 not A4.
// V8-28054 : J.Pickup
//  ~ Document is now rendered to proper scale - Takes into account HardMargins. 
// V8-28355 : L.Ineson
//  Added IsActive property and made sure that reloads are only performed when this is true.
// V8-27896 : A.Probyn
//  Updated so that the planogram is zoomed in more appropriately.
#endregion

#region Version History: (CCM 8.0.3)
// V8-29663 : M.Brumby
//  Added defensive coding when fetching print capabilities.
#endregion

#region Version History: (CCM 8.1.0)
// V8-30163 : A.Probyn
//  Updated calculation of number of whole pages required as rounding was cutting pages off for small decimals. Replaced with
//  Math.Floor + 1.
// V8-30045 : M.Shelley
//  Changed the displayed date/time to show the current local time rather than the UTC date/time
// V8-30102 : A.Probyn
//  Updated UpdateDocumentPreview to create the preview based on the selected paper size, and not the selectedPrintCapabilities
//  as I dont believe the printer margins need to be factored into it until the document is sent to print. We are only rendering at
//  this point.
#endregion

#region Version History: (CCM 8.1.1)
// V8-30667 : A.Probyn
//  Added ZoomToExtents in RenderControl.SetCamera() to ensure the camera is correctly positioned before we look for the
//  bounds.
#endregion

#region Version History: (CCM 8.2.0)
//V8-30738 : L.Ineson
//  Added print template functionality and removed old settings.
// V8-31051 : L.Ineson
//  Made sure preview cancels properly.
// V8-31376 : D.Pleasance
//  Amended RefreshAvailablePrinters() to validate if the printer can be accessed before adding it as an available printer.
#endregion

#region Version History: (CCM 8.3.0)
// V8-31496 : L.Ineson
//  Added Initialize method to stop this from loading any data before it is active.
// CCM-18454 : L.Ineson
//  Print preview now ensures that the current planogram images collection is loaded if required
//  so as to prevent threading issues.
#endregion

#region Version History: (CCM 8.4.0)
// CCM-19219 : J.Mendes
//  Optimezed the print template rendering speed by reusing a clone of the existing Plan3Ddata that is already calculated in the active plan visual document.
#endregion

#endregion

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Printing;
using System.Windows;
using System.Windows.Controls;
using Aspose.Pdf.Generator;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.PrintTemplateSetup;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Processes;
using Galleria.Framework.ViewModel;
using CommonImageResources = Galleria.Ccm.Common.Wpf.Resources.ImageResources;
using CommonMessage = Galleria.Ccm.Common.Wpf.Resources.Language.Message;
using PlanogramReportProcess = Galleria.Ccm.Processes.Reports.Planogram.Process;
using Aspose.Pdf;
using System.Windows.Xps.Packaging;
using Galleria.Ccm.Common.Wpf;
using Galleria.Framework.Planograms.Rendering;
using System.Collections.Generic;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Viewmodel controller for the backstage recent tab content control.
    /// </summary>
    public sealed class BackstagePrintTabViewModel : ViewModelAttachedControlObject<BackstagePrintTabContent>, IDataErrorInfo
    {
        #region Fields

        private Boolean _isInitialized;
        private Boolean _isActive = true;//flag to indicate if this tab is actually active.

        private PlanogramViewObjectView _activePlanogramView;
        private ObservableCollection<PrinterItem> _availablePrinters = new ObservableCollection<PrinterItem>();
        private ReadOnlyObservableCollection<PrinterItem> _availablePrintersRO;
        private PrinterItem _selectedPrinter = null;
        private Int32 _numberOfCopies = 1;

        private PrintTemplateInfo _selectedPrintTemplate;

        private PlanogramReportProcess _currentPreviewProcess;
        private Boolean _isGeneratingPreview;
        private Boolean _isPreviewRefreshQueued;

        private Byte[] _previewReportData;
        private Boolean _useActiveHighlight = true;
        private Boolean _useActiveLabels = true;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath IsActiveProperty = WpfHelper.GetPropertyPath<BackstagePrintTabViewModel>(p => p.IsActive);
        public static readonly PropertyPath AvailablePrintersProperty = WpfHelper.GetPropertyPath<BackstagePrintTabViewModel>(p => p.AvailablePrinters);
        public static readonly PropertyPath SelectedPrinterProperty = WpfHelper.GetPropertyPath<BackstagePrintTabViewModel>(p => p.SelectedPrinter);
        public static readonly PropertyPath NumberOfCopiesProperty = WpfHelper.GetPropertyPath<BackstagePrintTabViewModel>(p => p.NumberOfCopies);
        public static readonly PropertyPath SelectedPrintTemplateProperty = WpfHelper.GetPropertyPath<BackstagePrintTabViewModel>(p => p.SelectedPrintTemplate);
        public static readonly PropertyPath IsGeneratingPreviewProperty = WpfHelper.GetPropertyPath<BackstagePrintTabViewModel>(p => p.IsGeneratingPreview);
        public static readonly PropertyPath PreviewReportDataProperty = WpfHelper.GetPropertyPath<BackstagePrintTabViewModel>(p => p.PreviewReportData);
        public static readonly PropertyPath UseActiveLabelsProperty = WpfHelper.GetPropertyPath<BackstagePrintTabViewModel>(p => p.UseActiveLabels);
        public static readonly PropertyPath UseActiveHighlightProperty = WpfHelper.GetPropertyPath<BackstagePrintTabViewModel>(p => p.UseActiveHighlight);

        //Commands
        public static readonly PropertyPath PrintCommandProperty = WpfHelper.GetPropertyPath<BackstagePrintTabViewModel>(p => p.PrintCommand);
        public static readonly PropertyPath ShowPrintTemplateSetupCommandProperty = WpfHelper.GetPropertyPath<BackstagePrintTabViewModel>(p => p.ShowPrintTemplateSetupCommand);
        public static readonly PropertyPath SetPrintTemplateCommandProperty = WpfHelper.GetPropertyPath<BackstagePrintTabViewModel>(p => p.SetPrintTemplateCommand);
        public static readonly PropertyPath SetPrintTemplateFromFileCommandProperty = WpfHelper.GetPropertyPath<BackstagePrintTabViewModel>(p => p.SetPrintTemplateFromFileCommand);
        public static readonly PropertyPath SetPrintTemplateFromRepositoryCommandProperty = WpfHelper.GetPropertyPath<BackstagePrintTabViewModel>(p => p.SetPrintTemplateFromRepositoryCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether this tab is active
        /// and should be updating.
        /// </summary>
        public Boolean IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                OnPropertyChanged(IsActiveProperty);

                OnIsActiveChanged();
            }
        }

        /// <summary>
        /// Returns the active planogram view
        /// </summary>
        public PlanogramViewObjectView ActivePlanogramView
        {
            get { return _activePlanogramView; }
        }

        /// <summary>
        /// Get the available printers
        /// </summary>
        public ReadOnlyObservableCollection<PrinterItem> AvailablePrinters
        {
            get
            {
                if (_availablePrintersRO == null)
                {
                    _availablePrintersRO = new ReadOnlyObservableCollection<PrinterItem>(_availablePrinters);
                }
                return _availablePrintersRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected printer
        /// </summary>
        public PrinterItem SelectedPrinter
        {
            get { return _selectedPrinter; }
            set
            {
                _selectedPrinter = value;
                OnPropertyChanged(SelectedPrinterProperty);

                OnSelectedPrinterChanged();
            }
        }

        /// <summary>
        /// Gets/sets the number of copies
        /// </summary>
        public Int32 NumberOfCopies
        {
            get { return _numberOfCopies; }
            set
            {
                _numberOfCopies = value;
                OnPropertyChanged(NumberOfCopiesProperty);
            }
        }

        /// <summary>
        /// Gets the current print template
        /// </summary>
        public PrintTemplateInfo SelectedPrintTemplate
        {
            get { return _selectedPrintTemplate; }
            private set
            {
                _selectedPrintTemplate = value;
                OnPropertyChanged(SelectedPrintTemplateProperty);
            }
        }

        /// <summary>
        /// Returns true if the preview is currently processing.
        /// </summary>
        public Boolean IsGeneratingPreview
        {
            get { return _isGeneratingPreview; }
            private set
            {
                _isGeneratingPreview = value;
                OnPropertyChanged(IsGeneratingPreviewProperty);

                //force the print button can execute to update as it is not behaving atm
                System.Windows.Input.CommandManager.InvalidateRequerySuggested();
            }
        }

        /// <summary>
        /// Returns the preview report data.
        /// </summary>
        public Byte[] PreviewReportData
        {
            get { return _previewReportData; }
            private set
            {
                _previewReportData = value;
                OnPropertyChanged(PreviewReportDataProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether the label from the active view
        /// should be used.
        /// </summary>
        public Boolean UseActiveLabels
        {
            get { return _useActiveLabels; }
            set
            {
                _useActiveLabels = value;
                OnPropertyChanged(UseActiveLabelsProperty);

                BeginGeneratePreview();
            }
        }

        /// <summary>
        /// Gets/Sets whether the highlight applied from the 
        /// active view should be used.
        /// </summary>
        public Boolean UseActiveHighlight
        {
            get { return _useActiveHighlight; }
            set
            {
                _useActiveHighlight = value;
                OnPropertyChanged(UseActiveHighlightProperty);

                BeginGeneratePreview();
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public BackstagePrintTabViewModel()
        {
            _activePlanogramView = App.ViewState.ActivePlanogramView;
            if (App.IsUnitTesting) Initialize();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Model changed event for the active planogram
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ActivePlanogram_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<PlanogramView> e)
        {
            if (e.NewModel != null && (e.NewModel != e.OldModel))
            {
                //flag that the defaults now need resetting. 
                //_isDefaultsResetRequired = true;

                BeginGeneratePreview();
            }
        }

        /// <summary>
        /// Property changed event handler for selected printer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSelectedPrinterChanged()
        {
            //New Printer so update the available paper sizes based on the new printer. Also Updates the selected Paper.
            //this.LoadPaperSizes();
        }

        /// <summary>
        /// Called whenever the is active property value changes.
        /// </summary>
        private void OnIsActiveChanged()
        {
            if (!_isInitialized)
            {
                if (!IsActive) return;
                Initialize();
            }


            //Update the status of all printers.
            else if (this.IsActive)
            {
                UpdatePrinterStatuses();
            }

            BeginGeneratePreview();
        }

        #endregion

        #region Commands

        #region PrintCommand

        private RelayCommand _printCommand;

        /// <summary>
        /// Prints the given item
        /// </summary>
        public RelayCommand PrintCommand
        {
            get
            {
                if (_printCommand == null)
                {
                    _printCommand = new RelayCommand(
                        p => Print_Executed(),
                        p => Print_CanExecute())
                    {
                        FriendlyName = Message.BackstagePrint_Print,
                        FriendlyDescription = Message.BackstagePrint_Print_Description,
                        Icon = ImageResources.BackstagePrint_Print,
                        DisabledReason = Message.BackstagePrint_Print_DisabledReason
                    };
                    base.ViewModelCommands.Add(_printCommand);
                }
                return _printCommand;
            }
        }

        private Boolean Print_CanExecute()
        {
            if (this.NumberOfCopies < 1 || this.NumberOfCopies > 100)
            {
                this.PrintCommand.DisabledReason = Message.BackstagePrint_Print_DisabledReasonInvalidNumberCopies;
                return false;
            }

            if (this.PreviewReportData == null)
            {
                return false;
            }

            return true;
        }

        private void Print_Executed()
        {
            try
            {
                base.ShowWaitCursor(true);
                using (MemoryStream ms = new MemoryStream(this.PreviewReportData))
                {
                    Aspose.Pdf.Facades.PdfViewer viewer = new Aspose.Pdf.Facades.PdfViewer();

                    //do not show print dialog.
                    viewer.PrintPageDialog = false;
                    viewer.AutoResize = true;
                    viewer.AutoRotate = true;

                    System.Drawing.Printing.PrinterSettings ps = new System.Drawing.Printing.PrinterSettings();
                    ps.PrinterName = this.SelectedPrinter.FullName;
                    ps.Copies = (Int16)this.NumberOfCopies;

                    System.Drawing.Printing.PageSettings pgs = new System.Drawing.Printing.PageSettings();
                    pgs.Margins = new Margins(0, 0, 0, 0);


                    viewer.PrintLargePdf(ms, ps);
                    viewer.Close();
                }
                base.ShowWaitCursor(false);

            }
            catch (Exception)
            {
                base.ShowWaitCursor(false);
                CommonHelper.GetWindowService().ShowErrorMessage(
                    Message.BackstagePrint_Print,
                    Message.BackstagePrint_Print_Error);
                return;
            }
        }

        #endregion

        #region SetPrintTemplateCommand

        private RelayCommand _setPrintTemplateCommand;

        /// <summary>
        /// Show the dialog to select a print template from the repository.
        /// </summary>
        public RelayCommand SetPrintTemplateCommand
        {
            get
            {
                if (_setPrintTemplateCommand == null)
                {
                    _setPrintTemplateCommand = new RelayCommand(
                        p => SetPrintTemplate_Executed())
                    {
                        Icon = CommonImageResources.Open_16,
                        SmallIcon = CommonImageResources.Open_16,
                    };
                    base.ViewModelCommands.Add(_setPrintTemplateCommand);
                }

                return _setPrintTemplateCommand;
            }
        }

        private void SetPrintTemplate_Executed()
        {
            if (App.ViewState.IsConnectedToRepository)
            {
                this.SetPrintTemplateFromRepositoryCommand.Execute();
            }
            else
            {
                this.SetPrintTemplateFromFileCommand.Execute();
            }
        }

        #endregion

        #region SetPrintTemplateFromFileCommand

        private RelayCommand _setPrintTemplateFromFileCommand;

        /// <summary>
        /// Show the dialog to select a print template from the repository.
        /// </summary>
        public RelayCommand SetPrintTemplateFromFileCommand
        {
            get
            {
                if (_setPrintTemplateFromFileCommand == null)
                {
                    _setPrintTemplateFromFileCommand = new RelayCommand(
                        p => SetPrintTemplateFromFile_Executed())
                    {
                        FriendlyName = Message.BackstagePrintTab_SetPrintingTemplateFromFile,
                        Icon = CommonImageResources.Open_16,
                        SmallIcon = CommonImageResources.Open_16,
                    };
                    base.ViewModelCommands.Add(_setPrintTemplateFromFileCommand);
                }

                return _setPrintTemplateFromFileCommand;
            }
        }

        private void SetPrintTemplateFromFile_Executed()
        {
            //show the open file dialog.
            String file = null;
            if (String.IsNullOrEmpty(file))
            {
                Boolean result =
                CommonHelper.GetWindowService().ShowOpenFileDialog(
                    CCMClient.ViewState.GetSessionDirectory(SessionDirectory.PrintTemplate),
                    String.Format(CultureInfo.InvariantCulture, CommonMessage.PrintTemplateSetup_ImportFilter, PrintTemplate.FileExtension),
                    out file);

                if (result)
                {
                    //update the session directory
                    CCMClient.ViewState.SetSessionDirectory(SessionDirectory.PrintTemplate, Path.GetDirectoryName(file));
                }

                if (!result) return;
            }

            try
            {
                PrintTemplateInfo selectedItem = PrintTemplateInfoList.FetchByFileNames(new String[] { file }).FirstOrDefault();
                if (selectedItem != null)
                {
                    this.SelectedPrintTemplate = selectedItem.Clone();
                }
            }
            catch (DataPortalException ex)
            {
                CommonHelper.RecordException(ex);
                return;
            }

            BeginGeneratePreview();
            UpdateUserSettings();
        }

        #endregion

        #region SetPrintTemplateFromRepositoryCommand

        private RelayCommand _setPrintTemplateFromRepositoryCommand;

        /// <summary>
        /// Show the dialog to select a print template from the repository.
        /// </summary>
        public RelayCommand SetPrintTemplateFromRepositoryCommand
        {
            get
            {
                if (_setPrintTemplateFromRepositoryCommand == null)
                {
                    _setPrintTemplateFromRepositoryCommand = new RelayCommand(
                        p => SetPrintTemplateFromRepository_Executed(),
                        p => SetPrintTemplateFromRepository_CanExecute())
                        {
                            FriendlyName = Message.BackstagePrintTab_SetPrintingTemplateFromRepository,
                            Icon = CommonImageResources.Open_16,
                            SmallIcon = CommonImageResources.Open_16,
                        };
                    base.ViewModelCommands.Add(_setPrintTemplateFromRepositoryCommand);
                }

                return _setPrintTemplateFromRepositoryCommand;
            }
        }

        private Boolean SetPrintTemplateFromRepository_CanExecute()
        {
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.SetPrintTemplateFromRepositoryCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }
            return true;
        }

        private void SetPrintTemplateFromRepository_Executed()
        {
            if (this.AttachedControl == null) return;

            PrintTemplateInfoList printTemplateInfos;
            try
            {
                printTemplateInfos = PrintTemplateInfoList.FetchByEntityId(App.ViewState.EntityId);
            }
            catch (DataPortalException ex)
            {
                CommonHelper.RecordException(ex);
                return;
            }


            GenericSingleItemSelectorWindow win = new GenericSingleItemSelectorWindow();
            win.SelectionMode = DataGridSelectionMode.Single;
            win.ItemSource = printTemplateInfos;

            CommonHelper.GetWindowService().ShowDialog<GenericSingleItemSelectorWindow>(win);
            if (win.DialogResult != true) return;

            PrintTemplateInfo selectedItem = win.SelectedItems.Cast<PrintTemplateInfo>().FirstOrDefault();
            if (selectedItem != null) this.SelectedPrintTemplate = selectedItem.Clone();


            BeginGeneratePreview();
            UpdateUserSettings();
        }

        #endregion

        #region ShowPrintTemplateSetupCommand

        private RelayCommand _showPrintTemplateSetupCommand;

        /// <summary>
        /// Shows the print template editor window.
        /// </summary>
        public RelayCommand ShowPrintTemplateSetupCommand
        {
            get
            {
                if (_showPrintTemplateSetupCommand == null)
                {
                    _showPrintTemplateSetupCommand = new RelayCommand(
                        p => ShowPrintTemplateSetupCommand_Executed())
                    {
                        FriendlyName = Message.BackstagePrintTab_ShowPrintingTemplateSetup,
                        FriendlyDescription = CommonMessage.PrintTemplateSetup_TitleDesc,
                        Icon = CommonImageResources.PrintTemplateSetup,
                        SmallIcon = CommonImageResources.PrintTemplateSetup_16
                    };
                    base.ViewModelCommands.Add(_showPrintTemplateSetupCommand);
                }
                return _showPrintTemplateSetupCommand;

            }
        }

        private void ShowPrintTemplateSetupCommand_Executed()
        {
            Planogram plan = null;
            if (this.ActivePlanogramView.Model != null)
            {
                plan = this.ActivePlanogramView.Model.Model;
            }

            //show the dialog.
            Boolean planChanged = false;

            PrintTemplateSetupViewModel viewModel = new PrintTemplateSetupViewModel(App.ViewState.EntityId, plan);

            Object curId = (SelectedPrintTemplate != null) ? SelectedPrintTemplate.Id : null;

            viewModel.PrintTemplateSaved +=
                (s, e) =>
                {
                    if (Object.Equals(e.ReturnValue, curId))
                    {
                        planChanged = true;
                    }
                };

            CommonHelper.GetWindowService().ShowDialog<PrintTemplateSetupWindow>(viewModel);


            //if the print template changed then refresh
            if (planChanged)
            {
                BeginGeneratePreview();
            }
        }

        #endregion

        #region SavePDFCommand

        private RelayCommand _savePDFCommand;

        /// <summary>
        /// Saves the preview to pdf.
        /// </summary>
        public RelayCommand SavePDFCommand
        {
            get
            {
                if (_savePDFCommand == null)
                {
                    _savePDFCommand = new RelayCommand(
                        p => SavePDF_Executed(),
                        p => SavePDF_CanExecute())
                        {
                            FriendlyName = Message.Generic_Save,
                            SmallIcon = ImageResources.Save_16
                        };

                }
                return _savePDFCommand;
            }
        }

        private Boolean SavePDF_CanExecute()
        {
            if (this.PreviewReportData == null)
            {
                return false;
            }

            return true;
        }

        private void SavePDF_Executed()
        {
            String fileName;

            //show save as dialog
            Boolean result =
            CommonHelper.GetWindowService().ShowSaveFileDialog(
                this.ActivePlanogramView.Model.Name,
                null,
                "PDF (*.pdf)|*.pdf",
                out fileName);
            if (!result) return;

            base.ShowWaitCursor(true);

            try
            {
                System.IO.File.WriteAllBytes(fileName, this.PreviewReportData);
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);
                CommonHelper.RecordException(ex);
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(
                    fileName,
                    Framework.Controls.Wpf.OperationType.Save);
                return;
            }


            base.ShowWaitCursor(false);


        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Initializes this viewmodel.
        /// </summary>
        private void Initialize()
        {
            if (_isInitialized) return;
            _isInitialized = true;

            //Refresh available printers
            RefreshAvailablePrinters();
            PrinterSettings printerSettings = new PrinterSettings();
            PrinterItem printerItem = this.AvailablePrinters.FirstOrDefault(p => p.FullName == printerSettings.PrinterName);
            this.SelectedPrinter = (printerItem != null) ? printerItem : this.AvailablePrinters.FirstOrDefault();


            //Setup and set to respond to active planogram changed
            _activePlanogramView.ModelChanged += ActivePlanogram_ModelChanged;


            //load the default print template info
            this.SelectedPrintTemplate = PrintTemplateUIHelper.GetDefaultPrintTemplateInfo();
        }

        #region Print Preview

        /// <summary>
        /// Starts generating a new preview.
        /// </summary>
        private void BeginGeneratePreview()
        {
            if (_isPreviewRefreshQueued) return;

            //if a process is already running then abort it.
            if (_currentPreviewProcess != null)
            {
                _isPreviewRefreshQueued = true;
                AbortCurrentPreviewProcess();
                return;
            }


            //clear any existing preview
            this.PreviewReportData = null;

            //return out if we have no preview to generate.
            if (!this.IsActive || _selectedPrintTemplate == null || this.ActivePlanogramView.Model == null) return;

            this.IsGeneratingPreview = true;

            //start the process
            ProcessFactory<PlanogramReportProcess> processFactory = new ProcessFactory<PlanogramReportProcess>();
            processFactory.ProcessCompleted += OnGeneratePreviewProcessCompleted;

            Planogram plan = this.ActivePlanogramView.Model.Model;
            
            //ensure that the planogram image collection is loaded.
            base.ShowWaitCursor(true);
            PlanogramImageList forceImmediateImageLoad = plan.Images;
            base.ShowWaitCursor(false);

            PlanogramReportProcess process = new PlanogramReportProcess(_selectedPrintTemplate.Id, plan);
            _currentPreviewProcess = process;

            //we are working off the client so turn off the software render.
            process.IsSoftwareRender = false;

            //apply active label and highlight if override is required.
            IPlanDocument activeDesignDoc = GetActiveDesignDocument();
            if (activeDesignDoc != null)
            {
                if (this.UseActiveHighlight)
                {
                    process.IsOverridingPlanogramHighlights = true;
                    if (activeDesignDoc.Highlight != null)
                    {
                        process.PlanogramHighlightOverride = activeDesignDoc.Highlight.ToHighlight();
                    }
                }
                if (this.UseActiveLabels)
                {
                    process.IsOverridingPlanogramLabels = true;
                    if (activeDesignDoc.ProductLabel != null)
                    {
                        process.PlanogramProductLabelOverride = activeDesignDoc.ProductLabel.ToLabel();
                    }
                    if (activeDesignDoc.FixtureLabel != null)
                    {
                        process.PlanogramFixtureLabelOverride = activeDesignDoc.FixtureLabel.ToLabel();
                    }
                }
            }

            PlanVisualDocument planVisualModel = GetActivePlanVisualDocument();
            if (planVisualModel != null && planVisualModel.PlanogramModelData != null)
            {
                if (planVisualModel.PlanogramModelData.Settings.ShowProductImages)
                {
                    Plan3DData newPlanogramModelData = planVisualModel.PlanogramModelData.Clone();
                    process.PlanogramModelDataList = new List<Plan3DData>();
                    process.PlanogramModelDataList.Add(newPlanogramModelData);
                }                
            }
            
            //execute.
            processFactory.Execute(process);
        }

        /// <summary>
        /// Called when the preview generation completes.
        /// </summary>
        private void OnGeneratePreviewProcessCompleted(object sender, ProcessCompletedEventArgs<PlanogramReportProcess> e)
        {
            if (!_isPreviewRefreshQueued && e.Error == null && e.Process.ReportData != null)
            {
                this.PreviewReportData = e.Process.ReportData;
                if (e.Process.PlanogramModelDataList != null && e.Process.PlanogramModelDataList.Count > 0)
                {
                    foreach (Plan3DData item in e.Process.PlanogramModelDataList)
                    {
                        item.Dispose();
                    }

                    e.Process.PlanogramModelDataList = null;
                }
            }

            this.IsGeneratingPreview = false;
            _currentPreviewProcess = null;

            //if a refresh is queued then try again.
            if (_isPreviewRefreshQueued)
            {
                _isPreviewRefreshQueued = false;
                BeginGeneratePreview();
            }
        }

        /// <summary>
        /// Aborts the current preview process.
        /// </summary>
        private void AbortCurrentPreviewProcess()
        {
            if (_currentPreviewProcess != null)
            {
                _currentPreviewProcess.Abort();
                _currentPreviewProcess = null;
            }
        }

        /// <summary>
        /// Returns the currently active design plan document.
        /// </summary>
        private IPlanDocument GetActiveDesignDocument()
        {
            var planController = App.MainPageViewModel.ActivePlanController;
            if (planController == null) return null;

            IPlanDocument doc = planController.SelectedPlanDocument;
            if (doc == null || doc.DocumentType != DocumentType.Design)
            {
                doc = App.MainPageViewModel.ActivePlanController
                    .PlanDocuments.FirstOrDefault(d => d.DocumentType == DocumentType.Design);
            }

            return doc;
        }

        /// <summary>
        /// Returns the currently active design plan document.
        /// </summary>
        private PlanVisualDocument GetActivePlanVisualDocument()
        {
            PlanControllerViewModel planControllerVM = App.MainPageViewModel.ActivePlanController;
            if (planControllerVM == null) return null;

            PlanVisualDocument planController = (PlanVisualDocument)planControllerVM.SelectedPlanDocument;
            if (planController == null) return null;
            
            return planController;
        }

        #endregion

        #region Printing

        /// <summary>
        /// Refresh the available printers collection
        /// </summary> 
        private void RefreshAvailablePrinters()
        {
            //Clear any prexisting
            if (_availablePrinters.Count > 0) _availablePrinters.Clear();

            //Get local server
            LocalPrintServer localPrinterServer = new LocalPrintServer();

            //Add printers available (local and network)
            PrintQueueCollection collection = collection = new PrintServer().GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections });

            foreach (PrintQueue printer in collection.OrderBy(p => p.FullName))
            {
                try
                {
                    printer.Refresh();

                    //Work out if network printer
                    Boolean isNetworkPrinter = (printer.HostingPrintServer.Name != localPrinterServer.Name);

                    _availablePrinters.Add(new PrinterItem(printer, isNetworkPrinter));
                }
                catch { }
            }
        }

        /// <summary>
        /// Refreshes the status of all printer items.
        /// </summary>
        private void UpdatePrinterStatuses()
        {
            foreach (PrinterItem printer in this.AvailablePrinters)
            {
                printer.Refresh();
            }
        }

        #endregion

        /// <summary>
        /// Updates the DefaultPrintTemplateId value on 
        /// the user editor settings.
        /// </summary>
        private void UpdateUserSettings()
        {
            if (this.SelectedPrintTemplate == null) return;

            try
            {
                App.ViewState.Settings.Model.DefaultPrintTemplateId = this.SelectedPrintTemplate.Id.ToString();
                App.ViewState.Settings.Save();
            }
            catch (DataPortalException ex)
            {
                //dont worry about it - just record and carry on.
                CommonHelper.RecordException(ex);
                return;
            }
        }

        #endregion

        #region IDataErrorInfo

        String IDataErrorInfo.Error
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Implementation of the IDataErrorInfo interface
        /// </summary>
        /// <param name="columnName">Property name</param>
        /// <returns>String containing error</returns>
        String IDataErrorInfo.this[String columnName]
        {
            get
            {
                String result = null;

                //Number of copies
                if (columnName == BackstagePrintTabViewModel.NumberOfCopiesProperty.Path)
                {
                    if (this.NumberOfCopies < 1 || this.NumberOfCopies > 100)
                    {
                        result = String.Format(CultureInfo.CurrentCulture, Message.BackstagePrint_Print_DisabledReasonInvalidNumberCopies);
                    }
                }

                return result; // return result
            }
        }
        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                _activePlanogramView.ModelChanged -= ActivePlanogram_ModelChanged;
                _activePlanogramView = null;

                this.PreviewReportData = null;

                base.IsDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Class representing a printer item
    /// </summary>
    public sealed class PrinterItem : INotifyPropertyChanged
    {
        #region Fields

        private PrintQueue _printer;
        private Boolean _isNetworkPrinter;
        #endregion

        #region Properties

        /// <summary>
        /// Returns the printer name
        /// </summary>
        public String DisplayName
        {
            get
            {
                if (this.IsNetworkPrinter
                    && _printer.HostingPrintServer != null)
                {
                    return
                        String.Format(CultureInfo.CurrentCulture,
                        Message.BackstagePrint_PrinterFullNameFormat,
                        _printer.Name,
                        _printer.HostingPrintServer.Name.TrimStart('\\'));
                }
                else return _printer.Name;
            }
        }

        /// <summary>
        /// Returns the printer name
        /// </summary>
        public String FullName
        {
            get { return _printer.FullName; }
        }

        /// <summary>
        /// Returns the printer name
        /// </summary>
        public Boolean IsNetworkPrinter
        {
            get { return _isNetworkPrinter; }
        }

        /// <summary>
        /// Returns the status of the printer
        /// </summary>
        public String Status
        {
            get
            {
                if (this.IsNetworkPrinter && _printer.IsServerUnknown)
                    return Message.BackstagePrint_PrinterStatusServerOffline;

                switch (_printer.QueueStatus)
                {
                    case PrintQueueStatus.None:
                    case PrintQueueStatus.Processing:
                    case PrintQueueStatus.Printing:
                        return Message.BackstagePrint_PrinterStatusReady;

                    case PrintQueueStatus.TonerLow:
                        return Message.BackstagePrint_PrinterStatusTonerLow;

                    case PrintQueueStatus.PagePunt:
                    case PrintQueueStatus.PaperJam:
                    case PrintQueueStatus.PaperOut:
                    case PrintQueueStatus.PaperProblem:
                    case PrintQueueStatus.OutOfMemory:
                    case PrintQueueStatus.DoorOpen:
                    case PrintQueueStatus.Error:
                    case PrintQueueStatus.UserIntervention:
                        return Message.BackstagePrint_PrinterStatusNotAvailable;

                    case PrintQueueStatus.NotAvailable:
                    case PrintQueueStatus.Offline:
                    case PrintQueueStatus.ServerUnknown:
                        return Message.BackstagePrint_PrinterStatusNotAvailable;


                    case PrintQueueStatus.Busy:
                    case PrintQueueStatus.Initializing:
                    case PrintQueueStatus.WarmingUp:
                        return Message.BackstagePrint_PrinterStatusBusy;

                }


                return String.Empty;
            }
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public PrinterItem(PrintQueue printer, Boolean isNetworkPrinter)
        {
            _printer = printer;
            _isNetworkPrinter = isNetworkPrinter;
        }

        #endregion

        #region Methods

        public void Refresh()
        {
            try
            {
                _printer.Refresh();
            }
            catch { }

            OnPropertyChanged("FullName");
            OnPropertyChanged("DisplayName");
            OnPropertyChanged("Status");
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }

}
