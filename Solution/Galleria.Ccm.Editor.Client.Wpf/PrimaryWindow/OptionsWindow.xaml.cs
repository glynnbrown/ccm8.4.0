﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.

#endregion

#region Version History: CCM801

// V8-28676 : A.Silva
//      Added handling of Product Attribute Requested event.

#endregion

#region Version History: CCM820

// V8-30909 : J.Pickup
//      Added blink speed slider and associated blink preview.

#endregion

#region Version History : CCM 830
// V8-32823  : J.Pickup
//  Export visbiltity work (New properties).
#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Framework.Planograms.Model;
using System.ComponentModel;
using System.Windows.Media;
using System.Threading;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for OptionsWindow.xaml
    /// </summary>
    public sealed partial class OptionsWindow
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(OptionsViewModel), typeof(OptionsWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            OptionsWindow senderControl = (OptionsWindow)obj;

            if (e.OldValue != null)
            {
                OptionsViewModel oldModel = (OptionsViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.MovePropertiesFieldUpCommand.Executed -= senderControl.ViewModel_MovePropertiesFieldUpCommandExecuted;
                oldModel.MovePropertiesFieldDownCommand.Executed -= senderControl.ViewModel_MovePropertiesFieldDownCommandExecuted;
            }

            if (e.NewValue != null)
            {
                OptionsViewModel newModel = (OptionsViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.MovePropertiesFieldUpCommand.Executed += senderControl.ViewModel_MovePropertiesFieldUpCommandExecuted;
                newModel.MovePropertiesFieldDownCommand.Executed += senderControl.ViewModel_MovePropertiesFieldDownCommandExecuted;
            }
        }

        public OptionsViewModel ViewModel
        {
            get { return (OptionsViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value);}
        }

        #endregion

        #region Export feature availability

        public Visibility SpacemanExportVisibility
        {
            get
            {
                if (App.ViewState.IsExportSpacemanAvailable)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
        }

        public Visibility ApolloExportVisibility
        {
            get
            {
                if (App.ViewState.IsExportApolloAvailable)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
        }

        public Visibility JDAExportVisibility
        {
            get
            {
                if (App.ViewState.IsExportJDAAvailable)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public OptionsWindow(OptionsViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //set the help file key for this window.
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.OptionsWindow);

            this.ViewModel = viewModel;

            this.Loaded += This_Loaded;
        }

        /// <summary>
        /// Called on initial load of this window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void This_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= This_Loaded;

            //Load the blink speed from model.
            _myThreadBlinkSpeed = ViewModel.Settings.BlinkSpeed;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when the viewmodel MovePropertiesFieldUp command is executed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_MovePropertiesFieldUpCommandExecuted(object sender, EventArgs e)
        {
            xAssignedPositionPropertiesSource.View.Refresh();
            xAssignedComponentPropertiesSource.View.Refresh();
        }

        /// <summary>
        /// Called when the viewmodel MovePropertiesFieldDown command is executed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_MovePropertiesFieldDownCommandExecuted(object sender, EventArgs e)
        {
            xAssignedPositionPropertiesSource.View.Refresh();
            xAssignedComponentPropertiesSource.View.Refresh();
        }

        #endregion

        #region BlinkSpeedPreview

        BackgroundWorker _blinkSpeedPreviewWorker;
        Boolean _shouldContinueBlinkPreview = false;
        static int _myThreadBlinkSpeed = 600;
        const String cBlack = "#FF000000";

        private void xBlinkColourSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Console.WriteLine(xBlinkColourSlider.Value);
            _myThreadBlinkSpeed = Convert.ToInt16(xBlinkColourSlider.Value);
        }

        private void xBlinkColourSlider_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            StartBlinkSpeedPreview();
        }

        private void StartBlinkSpeedPreview()
        {
            xBlinkSpeedPreview.Visibility = Visibility.Visible;
            _shouldContinueBlinkPreview = true;

            _blinkSpeedPreviewWorker = new BackgroundWorker();
            _blinkSpeedPreviewWorker.WorkerReportsProgress = true;
            _blinkSpeedPreviewWorker.DoWork += new DoWorkEventHandler(blinkSpeedPreviewWorker_DoWork);
            _blinkSpeedPreviewWorker.ProgressChanged += new ProgressChangedEventHandler(blinkSpeedPreviewWorker_ProgressChanged);

            _blinkSpeedPreviewWorker.RunWorkerAsync();
        }

        private void StopBlinkSpeedPreview()
        {
            _shouldContinueBlinkPreview = false;
            _blinkSpeedPreviewWorker = null;
            xBlinkColourSampleRect.Stroke = new SolidColorBrush(System.Windows.Media.Colors.Orange);
            xBlinkSpeedPreview.Visibility = Visibility.Collapsed;
        }

        private void xBlinkColourSlider_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            StopBlinkSpeedPreview();
        }

        private void blinkSpeedPreviewWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (_shouldContinueBlinkPreview)
            {
                _blinkSpeedPreviewWorker.ReportProgress(0);

                for (int x = 0; x < _myThreadBlinkSpeed; x++)
                {
                    Thread.Sleep(1);
                }
            }
        }

        private void blinkSpeedPreviewWorker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            if (xBlinkColourSampleRect.Stroke.ToString() != cBlack)
            {
                xBlinkColourSampleRect.Stroke = new SolidColorBrush(System.Windows.Media.Colors.Black);
            }
            else
            {
                xBlinkColourSampleRect.Stroke = new SolidColorBrush(System.Windows.Media.Colors.Orange);
            }
        }

        #endregion

        #region Window Close

        public Boolean IsClosing { get; private set; }

        /// <summary>
        /// Called when the window close cross has been pressed
        /// </summary>
        /// <param name="e"></param>
        protected override void OnCrossCloseRequested(System.ComponentModel.CancelEventArgs e)
        {
            IsClosing = true;
            this.ViewModel.CancelCommand.Execute();

            base.OnCrossCloseRequested(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion

    }
}
