﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27230 : L.Ineson ~ Created.
#endregion

#endregion


using System.Windows;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for MainPageContentTab.xaml
    /// </summary>
    public sealed partial class MainPageContentTab
    {
        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(MainPageViewModel), typeof(MainPageContentTab),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets the viewmodel controller for this window.
        /// </summary>
        public MainPageViewModel ViewModel
        {
            get { return (MainPageViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        public MainPageContentTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
