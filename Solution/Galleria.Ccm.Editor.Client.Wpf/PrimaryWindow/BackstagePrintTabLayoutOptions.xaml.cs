﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25395 : A.Probyn 
//  Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow.DataManagement;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for BackstagePrintTabLayoutOptions.xaml
    /// </summary>
    public partial class BackstagePrintTabLayoutOptions : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(BackstagePrintTabViewModel), typeof(BackstagePrintTabLayoutOptions));

        /// <summary>
        /// Returns the viewmodel for this control
        /// </summary>
        public BackstagePrintTabViewModel ViewModel
        {
            get { return (BackstagePrintTabViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public BackstagePrintTabLayoutOptions(BackstagePrintTabViewModel viewModel)
        {
            this.ViewModel = viewModel;

            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Whenever the user clicks on the Accept Button.
        /// </summary>
        private void xAcceptButton_Click(Object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        /// <summary>
        ///     Whenever the user click on the Cancel Button.
        /// </summary>
        private void xCancelButton_Click(Object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        #endregion
    }
}
