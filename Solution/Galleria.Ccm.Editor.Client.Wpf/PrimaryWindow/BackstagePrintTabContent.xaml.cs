﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27569 : A.Probyn ~ Created
// V8-28355 : L.Ineson
//  Added IsActive property
#endregion
#region Version History: (CCM 8.03)
// V8-29663 : M.Brumby
//  Removed print update on load as it is handled by the reload functionality.
//  This was causing the print preview to render twice.
#endregion

#region Version History: (CCM 8.2.0)
//V8-30738 : L.Ineson
//  Added print template functionality and removed old settings.
#endregion
#endregion

using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Xps.Packaging;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.Windows.Media;
using System.Collections.Generic;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for BackstagePrintTabContent.xaml
    /// </summary>
    public sealed partial class BackstagePrintTabContent : UserControl
    {
        #region Fields
        private XpsDocument _previewXps;
        private Csla.Threading.BackgroundWorker _worker;
        private Boolean _isRestartRequired;
        private List<String> _oldFiles = new List<String>();
        #endregion

        #region Properties

        #region ViewModel Property

        /// <summary>
        /// ViewModel dependency property definition
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(BackstagePrintTabViewModel), typeof(BackstagePrintTabContent),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Called whenever the viewmodel dependency property value changes.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BackstagePrintTabContent senderControl = (BackstagePrintTabContent)obj;

            if (e.OldValue != null)
            {
                BackstagePrintTabViewModel oldModel = (BackstagePrintTabViewModel)e.OldValue;
                oldModel.AttachedControl = null;

                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                BackstagePrintTabViewModel newModel = (BackstagePrintTabViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.IsActive = senderControl.IsActive;

                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }
        }


        /// <summary>
        /// Gets/Sets the viewmodel context.
        /// </summary>
        public BackstagePrintTabViewModel ViewModel
        {
            get { return (BackstagePrintTabViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region IsActiveProperty

        public static readonly DependencyProperty IsActiveProperty
            = DependencyProperty.Register("IsActive", typeof(Boolean), typeof(BackstagePrintTabContent),
            new PropertyMetadata(false, OnIsActivePropertyChanged));

        private static void OnIsActivePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BackstagePrintTabContent senderControl = (BackstagePrintTabContent)obj;
            if (senderControl.ViewModel != null)
            {
                senderControl.ViewModel.IsActive = (Boolean)e.NewValue;
            }
        }

        /// <summary>
        /// Gets/Sets whether this tab is active.
        /// </summary>
        public Boolean IsActive
        {
            get { return (Boolean)GetValue(IsActiveProperty); }
            set { SetValue(IsActiveProperty, value); }
        }

        #endregion

        #region IsCreatingPreviewFileProperty

        public static readonly DependencyProperty IsCreatingPreviewFileProperty =
            DependencyProperty.Register("IsCreatingPreviewFile", typeof(Boolean), typeof(BackstagePrintTabContent),
            new PropertyMetadata(false));

        public Boolean IsCreatingPreviewFile
        {
            get { return (Boolean)GetValue(IsCreatingPreviewFileProperty); }
            private set { SetValue(IsCreatingPreviewFileProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public BackstagePrintTabContent()
        {
            //dont initialize until first made visible.
            this.IsVisibleChanged += BackstagePrintTabContent_IsVisibleChanged;
        }

        private void BackstagePrintTabContent_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.IsVisible)
            {
                this.IsVisibleChanged -= BackstagePrintTabContent_IsVisibleChanged;

                //Initialiaze
                InitializeComponent();

                this.ViewModel = new BackstagePrintTabViewModel();

                this.docViewer.Loaded += DocViewer_Loaded;

            }
        }

        #endregion

        #region Event Handlers

        private void DocViewer_Loaded(object sender, RoutedEventArgs e)
        {
            this.docViewer.Loaded -= DocViewer_Loaded;

            DependencyObject obj = this.docViewer;
            while (obj != null
                && !(obj is ToolBar))
            {
                if (VisualTreeHelper.GetChildrenCount(obj) == 0)
                {
                    ((FrameworkElement)obj).ApplyTemplate();
                    if (VisualTreeHelper.GetChildrenCount(obj) == 0) break;
                }

                obj = VisualTreeHelper.GetChild(obj, 0);
            }


            ToolBar docViewerToolbar = obj as ToolBar;
            if (docViewerToolbar != null)
            {
                //add the save button
                Button savePdfBtn = new Button()
                {
                    Command = this.ViewModel.SavePDFCommand,
                    Content = new Image() { Source = this.ViewModel.SavePDFCommand.SmallIcon, Height = 14, Width = 14 },
                    Background = Brushes.Transparent,
                    BorderBrush = Brushes.Transparent
                };
                docViewerToolbar.Items.Insert(1, savePdfBtn);
            }

            //hide the search toolbar.
            ContentControl searchToolbar = this.docViewer.Template.FindName("PART_FindToolBarHost", this.docViewer) as ContentControl;
            if (searchToolbar != null) searchToolbar.Visibility = System.Windows.Visibility.Collapsed;

            //hide the context menu
            this.docViewer.ContextMenu = null;
        }

        /// <summary>
        /// Called whenever a viewmodel property changes.
        /// </summary>
        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == BackstagePrintTabViewModel.PreviewReportDataProperty.Path)
            {
                UpdatePreview();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the preview document
        /// </summary>
        private void UpdatePreview()
        {
            //XPS Version:
            if (this.docViewer == null) return;

            //close the old doc.
            if (_previewXps != null)
            {
                this.docViewer.Document = null;

                try
                {
                    _previewXps.Close();
                    _previewXps = null;
                }
                catch (Exception) { }
            }

            if (this.ViewModel == null || this.ViewModel.PreviewReportData == null)
                return;

            String pdfPath = Path.GetTempFileName();
            _oldFiles.Add(pdfPath);

            if (_worker == null)
            {
                _worker = new Csla.Threading.BackgroundWorker();
                _worker.DoWork += new DoWorkEventHandler(worker_DoWork);
                _worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
                _worker.WorkerSupportsCancellation = true;
            }

            if (_worker.IsBusy)
            {
                _worker.CancelAsync();
                _isRestartRequired = true;
                return;
            }

            this.IsCreatingPreviewFile = true;
            _worker.RunWorkerAsync(new Object[] { pdfPath, this.ViewModel.PreviewReportData });
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            Csla.Threading.BackgroundWorker worker = (Csla.Threading.BackgroundWorker)sender;

            Object[] args = e.Argument as Object[];
            String pdfPath = args[0] as String;
            Byte[] data = args[1] as Byte[];

            if (File.Exists(pdfPath))
            {
                try
                {
                    File.Delete(pdfPath);
                }
                catch (Exception)
                {
                    return;
                }
            }


            try
            {
                using (CodePerformanceMetric m = new CodePerformanceMetric())
                {
                    //write the xps file.
                    using (MemoryStream ms = new MemoryStream(data))
                    {
                        Aspose.Pdf.Document doc = new Aspose.Pdf.Document(ms);
                        doc.Save(pdfPath, new Aspose.Pdf.XpsSaveOptions());
                    }
                }
            }
            catch (Exception)
            {
                return;
            }


            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                e.Result = pdfPath;
            }
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Csla.Threading.BackgroundWorker worker = (Csla.Threading.BackgroundWorker)sender;

            if (!e.Cancelled && e.Error == null)
            {
                String pdfPath = e.Result as String;

                //load the xps document into the viewer.
                try
                {
                    _previewXps = new XpsDocument(pdfPath, FileAccess.Read);
                    this.docViewer.Document = _previewXps.GetFixedDocumentSequence();


                    //try and clean up other old files
                    foreach (String old in _oldFiles.ToArray())
                    {
                        if (old == pdfPath) continue;

                        if (File.Exists(old))
                        {
                            try
                            {
                                File.Delete(old);
                                _oldFiles.Remove(old);
                            }
                            catch (Exception)
                            {
                                return;
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    CommonHelper.RecordException(ex);
                }
            }

            this.IsCreatingPreviewFile = false;

            if (_isRestartRequired)
            {
                _isRestartRequired = false;
                UpdatePreview();
            }
        }

        #endregion
    }

}
