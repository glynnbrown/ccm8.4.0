﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// L.Ineson 
//  Created.
// V8-24265 : J.Pickup
//  Label Setting view changed to LabelItem & added the RO BulkObservable collections
// CCM-24265 : N.Haywood
//  Added additional label properties
// V8-25395 : A.Probyn 
//  Added additional properties and code for Product Library
//  Added additonal timer mechanism for status text
// CCM-25653: L.Hodson
//  Refactored slightly to allow for plans to be opened and saved from the db.
// V8-25042 : A.Silva 
//  Refactored the saving of plans to the DB.
// V8-25798 : A.Silva 
//  Simplified the call to the SaveAs dialog.
// V8-25959 : A.Silva 
//  Corrected new Planograms not being saved to the selected Planogram Group.
// V8-24700 : A.Silva  
//  Added ShowColumnLayoutEditor.
// V8-24124 : A.Silva 
//  Added selection of current column layout if the current plan document supports it.
// V8-25797 : A.Silva 
//  Added ActiveCustomColumnLayoutView property and handling of xDockingManager.ActiveContentChanged, to monitor changes to which panel is active.
// V8-26171 : A.Silva 
//  Removed or corrected references to the old Custom Column Layout Editor.
// V8-26318 : A.Silva 
//  Ensure a new layout is created when managng Custom Column Layouts from the back stage.
// V8-26329 : A.Silva 
//  Added SaveWhenChangesApplied = true when calling the Custom Column Layout Editor, so apply changes persists them too.
// V8-26364 : L.Ineson
// Removed references to custom view layouts.
// V8-26248 : L.Luong
//  Changed Product and Fixture labels to work with new Designs
// V8-26396 : L.Ineson
//  Product library no longer throws an exception if the default library is not available.
// V8-25436 : L.Luong
//  added custom highlight and changed highlight to fit new design
// V8-26491 : A.Kuszyk
//  Added SelectedPlanDocument, IsAssortmentDocumentVisible and OnSelectedPlanDocumentChanged.
// V8-26253 : I.George
//  Added SetLoadedProductLibrary
// V8-26956 : L.Luong
//  Added IsConsumerDecisionTreeDocumentVisable 
// V8-27143 : A.Silva ~ Corrected PromptUserForValue() so that the correct group selected by the user is returned.
// V8.27154 : L.Luong
//  Added insert defaults ProductPlacement X,Y,Z to new planograms
// V8-26578 : L.Ineson
//  Made sure that product library tries to load on startup regardless of repository connection state.
// V8-27496 : L.Luong
//  Changed Save Methods to fit new Save tab interface
// V8-25191 : L.Luong
//  Added Error Dialog if an already opened planogram is opened again
// V8-27742 : A.Silva
//      Amended to update the status bar icon when the validation rows collection for the active Planogram View changes.
// V8-27804 : L.ineson
//  Added support for opening a spaceman file.
// V8-27938 : N.Haywood
//  Added reports/data screen
// V8-27999 : L.Ineson
//  Made sure that the attached control closing handler actaully closes each plan controller
// so that it get unlocked.
// V8-27938 : N.Haywood
//  Added data sheets
// V8-28199 : N.Haywood
//  Changed favourites to be a most recently used list
// V8-28191 : N.Haywood
//  Added plan controller as property to include in the data sheet constructor and added custom data sheets
// V8-27733 : L.Luong
//  Added Read Only closing prompt
// V8-28362 : L.Ineson
//  Available labels and highlights collections are now infos.
// V8-27805 : L.ineson
//  Added dialog if connection is being changed when repository plans are open.
// V8-27785 : L.Ineson
//  Made sure that all save exceptions are caught and added a message for file io ones.
// V8-28219 : N.Haywood
//  changed PlanogramImportFileType.Spaceman to SpacemanV9
// V8-27881 : A.Kuszyk
//  Amended SaveAsRepository to allow for overwriting plans.
// V8-28235 : J.Pickup
//  Warns user when planogram has an assocociated product group ad the category codes and names do not match.
// V8-28587 : A.Kuszyk
//  Added owner parameter to PlanogramHierarchySelectorViewModel.CheckForOverwrite.

#endregion

#region Version History: CCM 801

// V8-28684 : A.Kuszyk
//  Added readonly check to Save method so that backstage save tab is shown for readonly plans.
// V8-25148 : I.George
//  Added a boolean that does a check based on the result from ShowPlanProperties method
// V8-28760 : A.Kuszyk
//  Updated Open from and Save to file methods to use session planogram location.
// V8-28774 : A.Kuszyk
//  Updated Save method to catch PackageLockException when saving package and handle appropriately.
// V8-28838 : L.Ineson
//  Updated SaveAs to repository and how it deals with locked plans.

#endregion

#region Version History : CCM 802

// V8-28907 : A.Kuszyk
//  Added overwrite check to Save method and refactored SaveToRepository to use shared methods.
// V8-25378 : M.Pettit
//  Added DockWindows() method
// V8-28948 : M.Shelley
//  Added a check when loading a POG file to see if there are actually any plans in it.
//  Warn the user if the package contains no plans.
// V8-28954 : J.Pickup
//  Now filters labels and highlights alphanumerically. 
// V8-29039 : D.Pleasance
//  Amended OnActivePlanControllerChanged to determine if image provider required to fetch images.

#endregion

#region Version History : CCM 803

// V8-29751 : L.Ineson
//  Added highlight as a default view setting.

#endregion

#region Version History : CCM 810

// V8-29785 : L.Ineson
//  Ammended save prompt when closing planograms.
// V8-29811 : A.Probyn
//  Updated SaveAsRepository to copy content lookup too.
// V8-29533 : D.Pleasance
//  Amended FetchPackageWorker_RunWorkerCompleted to check for PackageDuplicateProductGtinException
// V8-29550 : A.Probyn
//  Fixed bug in SelectedProductLibrarySource where ProductLibrary name wasn't showing.
// V8-29462 : M.Shelley
//  Allow the user to change the product label directory for the duration of the session
//  rather than always defaulting to the default settings location each time the product label
//  management window is opened
// V8-30211 : M.Brumby
//  Allow user to save a locked plan if they are the user who locked it. Otherwise you can't save
//  a plan you opened and edited.
// V8-30242 : L.Ineson
//  Validation warnings for all open plans are now rechecked when the user settings model changes.
//  Also made sure the validation warning status bar icon ignores the warnings that should be ignored.
// V8-30318 : D.Pleasance
//  Amended how selected planogram group is obtained. Now gets this from new properties on ParentPackageView.

#endregion

#region Version History : CCM 811

// V8-29304 : N.Haywood
//  Fixed data sheets where they were creating a new data sheet where it should modify an existing one
// V8-30344 : D.Pleasance
//  Amended ConfirmPlanogramOverwriteWithUser to include PlanogramGroupName so we can see the target folder of the plan being overwritten.
// V8-29406 : N.Haywood
//  Added file type validation for OpenFileDialog
// V8-30594 : A.Silva
//  Added RefreshCurrentProductLabel, and called after each SaveAs() so that the new document has the same label applied again.
// V8-30626 : I.George
//  Fixture label default now gets applied when plan is opened
#endregion

#region Version History : CCM820

// V8-30756 : A.Silva
//  Modified OpenPlanogramsFromFileSystem to deal with ProSpace and Apollo files.
// V8-30794 : A.Kuszyk
//  Added ApplyHighlightAsProductColour method.
// V8-30754 : A.Silva
//  Amended ProSpace enum value in PlanogramImportFileType.
// V8-29968 : A.Kuszyk
//  Amended ContinueWithPlanContollerClose to take account of product sequence update status and prompt the user accordingly.
// V8-30791 : D.Pleasance
//  Added flag to determine if ApplyHighlightAsProductColour is being processed (_isApplyHighlightAsProductColour), 
//  used when hooked up to product list event for product color sets changing. 
// V8-30890 : A.Kuszyk
//  Modified ApplyHighlightAsProductColour to allow selected products only to be updated.
// V8-30932 : L.Ineson
//  Added SelectionType property
// V8-30725 : M.Brumby
//  PCR01417 Import from Apollo
// V8-30754 : A.Silva
//  Modified OpenPlanogramsFromFileSystem so that temp files are created for multi planogram files when importing from ProSpace.
// V8-31022 : A.Silva
//  Amended OpenPlanogramsFromFileSystem so that the mapping screen is requested only once per type.
// V8-31019 : A.Silva
//  Added CorrectFileName() so that filenames can be checked for invalid characters.
// V8-31017 : L.Ineson
//  SplitProSpaceFiles now opens the file as readonly, also added try catch to display an error just in case.
// V8-31087 : M.Brumby
//  Enable silent opening of external file types
// V8-31165 : D.Pleasance
//  Removed logic to validate if product sequence is out of date. This is now always done if its a sequence template.
// V8-31093 : A.Silva
//  Amended the way psa files are imported to account for multi planogram package files.
// V8-31017 : M.Pettit
//	Users are now informed if a planogram file is locked by another application when opening
// V8-31198 : D.Pleasance
//  Amended so that plan name information is also provided on message if plan is already open.
// V8-31175 : A.Silva
//  Removed modifying the request by adding the package id for space planning plans as it is no longer necessary.
// V8-31404 : L.Ineson
//  Options window now only refreshes things if it needs to.
// V8-31430 : D.Pleasance
//  Replaced AppSettingsView_ModelChanged with UpdateAppSettings() to check for changes to app settings.
#endregion

#region Version History : CCM830
// V8-31608 : L.Ineson
//  Amended duplicate planogram warning to show the request description rather than the plan id.
// V8-31546 : M.Pettit
//  Added code to handle saving out to different file types
// V8-31541 : L.Ineson
//  Amended show report editor.
// V8-31546 : M.Pettit
//  Added SaveAsFile to include islent option if a filepath is specified (if executed by a task for example)
//  Refactored PlanogramFileType to Framework.Plangoram.Model and renamed PlanogramExportFileType
// V8-31699 : A.Heathcote
//  Re-wrote the RefreashAvailableLabels method
// V8-31557 : M.Shelley
//  Add the functionality to append a selected planogram to the right of the current planogram.
//  Add the ability to append a plan from a POG file.
// V8-31699 : A.Heathcote
//  Re-wrote the RefreashAvailableHighlight method
// V8-32033 : A.Kuszyk
//  Resolved an issue relating to loading highlights from the database.
// V8-31547 : M.Pettit
//  Added Export backstage option and simplified SaveAs to support POG only
// V8-32085 : N.Haywood
//  Added StatusBarFontSize
// V8-31547 : M.Pettit
//  Added SavePlanToExternalFileTypeWork for 'Export To External File Type' to show busy window
//V8-32256 : L.Ineson
//  Updated default label & highlight load as items may now not be in available lists.
// V8-32311 : M.Pettit
//  Cancelling export when editing mapping template now returns user to backstage
// V8-32321 : M.Pettit
//  Exporting a planogram should not change the name of the open planogram. 
//  Exported plan should also retain this name even if filename is changed by the user from default value.
// V8-32360 : M.Brumby
//  [PCR01564] Auto split bays by a fixed value
//V8-32389 : J.Pickup
//  Added keyboard shortcut functionality to toggle the set highlight, product and fixture labels inc new commands. 
//V8-32446 : L.Inesons
//  Stopped fav datasheets list reloading the full list everytime a datasheet is opened.
// V8-32392: M.Pettit
//  Added specific message for package lock exceptions when saving to external file type and file is already open in external application 
// V8-32255 : A.Silva
//  Added SyncActionQueue, ClosePlanControllerRequest and amended ContinueWithPlanContollerClose among others to allow closing multiple documents with just one warning.
// V8-31720 : A.Silva
//  Amended Save to account for concurrency errors when saving a package. If that happens the plan will need to be saved as.
// V8-32359 : A.Silva
//  Added UpdatePlanogramFromProductAttribute and UpdateAllPlanogramsFromProductAttribute
// V8-32740 : A.Heathcote 
//  Fixed bug in SetFixtureLabel.
//CCM-18483 : L.Ineson
//  Made sure that save as to file closes the backstage.
// CCM-18559 : M.Pettit
//  Reset RestructureBaysByCount to explicitly force split components
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Timers;
using System.Windows;
using System.Windows.Threading;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Editor.Client.Wpf.ProductLibrary;
using Galleria.Ccm.Editor.Client.Wpf.ProductLibraryMaintenance;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Editor.Client.Wpf.Settings;
using Galleria.Ccm.Editor.Client.Wpf.Settings.HighlightEditor;
using Galleria.Ccm.Editor.Client.Wpf.Settings.PlanogramExportFileTemplateEditor;
using Galleria.Ccm.Editor.Client.Wpf.Settings.PlanogramFileTemplateEditor;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.External;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Microsoft.Win32;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Viewmodel controller for the MainPageOrganiser
    /// </summary>
    public sealed class MainPageViewModel : ViewModelAttachedControlObject<MainPageOrganiser>
    {
        #region Fields

        private PlanogramImportTemplate _silentApolloTemplate = null;
        private PlanogramImportTemplate _silentSpacemanTemplate = null;
        private PlanogramImportTemplate _silentProspaceTemplate = null;
        private Object _silentLock = new Object();

        private readonly UserEditorSettingsViewModel _appSettingsView;
        private readonly RecentPlanogramListView _recentPlanogramsView;
        private readonly ProductLibraryViewModel _productLibraryView;

        private readonly BulkObservableCollection<PlanControllerViewModel> _planControllers = new BulkObservableCollection<PlanControllerViewModel>();
        private readonly PlanogramViewObjectView _activePlanogramView;
        private PlanControllerViewModel _activePlanController;


        //highlights
        private readonly BulkObservableCollection<HighlightInfo> _availableHighlights = new BulkObservableCollection<HighlightInfo>();
        private ReadOnlyBulkObservableCollection<HighlightInfo> _availableHighlightsRO;

        //labels
        private readonly BulkObservableCollection<LabelInfo> _availableProductLabels = new BulkObservableCollection<LabelInfo>();
        private ReadOnlyBulkObservableCollection<LabelInfo> _availableProductLabelsRO;
        private readonly BulkObservableCollection<LabelInfo> _availableFixtureLabels = new BulkObservableCollection<LabelInfo>();
        private ReadOnlyBulkObservableCollection<LabelInfo> _availableFixtureLabelsRO;

        //data sheets
        private readonly BulkObservableCollection<CustomColumnLayout> _availableDataSheets = new BulkObservableCollection<CustomColumnLayout>();
        private ReadOnlyBulkObservableCollection<CustomColumnLayout> _availableDataSheetsRO;
        private CustomColumnLayout _customDataSheet;

        private Boolean _isSelectedProductLibrarySourceDefault;
        private ReadOnlyBulkObservableCollection<PlanControllerViewModel> _planControllersRO;

        //product library/universe
        private ProductLibraryPanelViewModel _productLibraryPanelViewModel = new ProductLibraryPanelViewModel();
        private String _selectedProductLibrary;
        private ProductUniverseInfo _selectedProductUniverseInfo;

        private Timer _statusTextTimer;
        private String _statusBarMouseText;
        private String _statusBarText;

        private Boolean _isApplyHighlightAsProductColour = false;

        private readonly SyncActionQueue _syncActionQueue;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath ActivePlanControllerProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ActivePlanController);
        public static readonly PropertyPath AvailableDataSheetsProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.AvailableDataSheets);
        public static readonly PropertyPath AvailableFixtureLabelsProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.AvailableFixtureLabels);
        public static readonly PropertyPath AvailableHighlightsProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.AvailableHighlights);
        public static readonly PropertyPath AvailableProductLabelsProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.AvailableProductLabels);
        public static readonly PropertyPath ExitApplicationCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ExitApplicationCommand);
        public static readonly PropertyPath IsLibraryPanelVisibleProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.IsLibraryPanelVisible);
        public static readonly PropertyPath IsSelectedProductLibraryDefaultSourceProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.IsSelectedProductLibraryDefaultSource);
        public static readonly PropertyPath PlanControllersProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.PlanControllers);
        public static readonly PropertyPath ProductLibraryPanelViewModelProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.ProductLibraryPanelViewModel);
        public static readonly PropertyPath SelectedProductLibraryProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.SelectedProductLibrary);
        public static readonly PropertyPath SelectedProductLibrarySourceProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.SelectedProductLibrarySource);
        public static readonly PropertyPath SelectedProductUniverseInfoProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.SelectedProductUniverseInfo);
        public static readonly PropertyPath StatusBarHeightProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.StatusBarHeight);
        public static readonly PropertyPath StatusBarMouseTextProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.StatusBarMouseText);
        public static readonly PropertyPath StatusBarTextProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.StatusBarText);
        public static readonly PropertyPath CustomDataSheetProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.CustomDataSheet);
        public static readonly PropertyPath SelectionTypeProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(o => o.SelectionType);
        public static readonly PropertyPath StatusBarFontSizeProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.StatusBarFontSize);


        #endregion

        #region Properties

        /// <summary>
        ///     Gets/Sets the active plan controller
        /// </summary>
        public PlanControllerViewModel ActivePlanController
        {
            get { return _activePlanController; }
            set
            {
                if (_activePlanController != value)
                {
                    _activePlanController = value;
                    OnPropertyChanged(ActivePlanControllerProperty);

                    OnActivePlanControllerChanged(value);
                }
            }
        }

        /// <summary>
        /// Gets the currently selected Planogram document on the ActivePlanController.
        /// </summary>
        public IPlanDocument SelectedPlanDocument
        {
            get
            {

                if (ActivePlanController == null) return null;
                return ActivePlanController.SelectedPlanDocument;

            }
        }

        /// <summary>
        ///     Returns the collection of available highlights
        /// </summary>
        public ReadOnlyBulkObservableCollection<HighlightInfo> AvailableHighlights
        {
            get
            {
                if (_availableHighlightsRO == null)
                {
                    _availableHighlightsRO = new ReadOnlyBulkObservableCollection<HighlightInfo>(_availableHighlights);
                }
                return _availableHighlightsRO;
            }
        }

        /// <summary>
        ///     Returns the collection of available product labels
        /// </summary>
        public ReadOnlyBulkObservableCollection<LabelInfo> AvailableFixtureLabels
        {
            get
            {
                if (_availableFixtureLabelsRO == null)
                {
                    _availableFixtureLabelsRO = new ReadOnlyBulkObservableCollection<LabelInfo>(_availableFixtureLabels);
                }
                return _availableFixtureLabelsRO;
            }
        }

        /// <summary>
        ///     Returns the collection of available product labels
        /// </summary>
        public ReadOnlyBulkObservableCollection<LabelInfo> AvailableProductLabels
        {
            get
            {
                if (_availableProductLabelsRO == null)
                {
                    _availableProductLabelsRO = new ReadOnlyBulkObservableCollection<LabelInfo>(_availableProductLabels);
                }
                return _availableProductLabelsRO;
            }
        }

        /// <summary>
        ///     Returns the collection of available product labels
        /// </summary>
        public ReadOnlyBulkObservableCollection<CustomColumnLayout> AvailableDataSheets
        {
            get
            {
                if (_availableDataSheetsRO == null)
                {
                    _availableDataSheetsRO = new ReadOnlyBulkObservableCollection<CustomColumnLayout>(_availableDataSheets);
                }
                return _availableDataSheetsRO;
            }
        }

        /// <summary>
        ///     Gets/Sets whether the fixture library panel should be shown.
        /// </summary>
        public Boolean IsLibraryPanelVisible
        {
            get { return _appSettingsView.Model.IsLibraryPanelVisible; }
            set
            {
                _appSettingsView.Model.IsLibraryPanelVisible = value;
                OnIsLibraryPanelVisibleChanged();
                OnPropertyChanged(IsLibraryPanelVisibleProperty);
            }
        }

        /// <summary>
        ///     Returns the collection of open plan controllers
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanControllerViewModel> PlanControllers
        {
            get
            {
                if (_planControllersRO == null)
                {
                    _planControllersRO = new ReadOnlyBulkObservableCollection<PlanControllerViewModel>(_planControllers);
                }
                return _planControllersRO;
            }
        }

        /// <summary>
        ///     Gets/Sets the height of the status bar.
        /// </summary>
        public Int32 StatusBarHeight
        {
            get { return _appSettingsView.Model.StatusBarHeight; }
            set { _appSettingsView.Model.StatusBarHeight = value; }
        }

        /// <summary>
        ///     Gets/Sets the mouse position text in the status bar.
        /// </summary>
        public String StatusBarMouseText
        {
            get { return _statusBarMouseText; }
            set
            {
                _statusBarMouseText = value;
                OnPropertyChanged(StatusBarMouseTextProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets the main text to display in the status bar.
        /// </summary>
        public String StatusBarText
        {
            get { return _statusBarText; }
            set
            {
                _statusBarText = value;
                OnPropertyChanged(StatusBarTextProperty);

                //If timer is active, restart it
                if (_statusTextTimer.Enabled)
                {
                    _statusTextTimer.Stop();
                    _statusTextTimer.Start();
                }
                else
                {
                    _statusTextTimer.Start();
                }
                //_statusTextTimer.Elapsed += (sender, args) =>
                //{
                //    _statusBarText = String.Empty;
                //    OnPropertyChanged(StatusBarTextProperty);
                //};
                OnPropertyChanged(StatusBarFontSizeProperty);
            }
        }

        public Byte StatusBarFontSize
        {
            get { return _appSettingsView.Model.StatusBarTextFontSize; }
        }

        #region Product Library

        //TODO: Move into product library viewmodel.

        /// <summary>
        ///     Gets/sets whether the currently selected product library is default
        /// </summary>
        public Boolean IsSelectedProductLibraryDefaultSource
        {
            get { return _isSelectedProductLibrarySourceDefault; }
            set
            {
                _isSelectedProductLibrarySourceDefault = value;
                OnIsSelectedProductLibraryDefaultSourceChanged();
                OnPropertyChanged(IsSelectedProductLibraryDefaultSourceProperty);
            }
        }

        /// <summary>
        ///     Get/Private set the product library panel view model so that we can control the controller
        ///     from the main page view model and keep it in memory/clear when necessary.
        /// </summary>
        //[Obsolete("See App.Viewstate.ProductLibraryView instead")]
        public ProductLibraryPanelViewModel ProductLibraryPanelViewModel
        {
            get { return _productLibraryPanelViewModel; }
            private set
            {
                ProductLibraryPanelViewModel oldValue = _productLibraryPanelViewModel;

                _productLibraryPanelViewModel = value;
                OnPropertyChanged(ProductLibraryPanelViewModelProperty);

                if (oldValue != null)
                {
                    oldValue.Dispose();
                }
            }
        }

        /// <summary>
        ///     Gets/sets the currently selected product library info
        /// </summary>
        public String SelectedProductLibrary
        {
            get { return _selectedProductLibrary; }
            set
            {
                _selectedProductLibrary = value;
                OnSelectedProductLibraryChanged();
                OnPropertyChanged(SelectedProductLibraryProperty);
                OnPropertyChanged(SelectedProductLibrarySourceProperty);
            }
        }

        /// <summary>
        ///     Gets/sets the currently selected product library info
        /// </summary>
        public String SelectedProductLibrarySource
        {
            get
            {
                if (SelectedProductUniverseInfo != null && SelectedProductLibrary == null)
                {
                    return SelectedProductUniverseInfo.ToString();
                }
                if (SelectedProductLibrary != null)
                {
                    return SelectedProductLibrary.ToString();
                }
                return String.Empty;
            }
        }

        /// <summary>
        ///     Gets/sets the currently selected product universe
        /// </summary>
        public ProductUniverseInfo SelectedProductUniverseInfo
        {
            get { return _selectedProductUniverseInfo; }
            set
            {
                _selectedProductUniverseInfo = value;
                OnSelectedProductUniverseInfoChanged();
                OnPropertyChanged(SelectedProductUniverseInfoProperty);
                OnPropertyChanged(SelectedProductLibrarySourceProperty);
            }
        }

        #endregion

        public CustomColumnLayout CustomDataSheet
        {
            get { return _customDataSheet; }
            set
            {
                _customDataSheet = value;
                OnPropertyChanged(CustomDataSheetProperty);
            }
        }

        public Boolean IsApplyHighlightAsProductColour
        {
            get { return _isApplyHighlightAsProductColour; }
        }

        /// <summary>
        /// Gets/Sets the current selection type.
        /// </summary>
        public PlanItemSelectionType SelectionType
        {
            get { return App.ViewState.SelectionMode; }
            set { App.ViewState.SelectionMode = value; }
        }

        #region Temp Import Folder

        private String _tempImportFolder;

        private String TempImportFolder
        {
            get
            {
                return _tempImportFolder ??
                       (_tempImportFolder = LocalHelper.GetTempImportFilesFolder());
            }
        }

        #endregion

        /// <summary>
        ///     Get or set whether closing a document directly is allowed or not.
        /// </summary>
        public Boolean IsCloseDocumentAllowed { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        ///  Constructor
        /// </summary>
        public MainPageViewModel()
        {
            //Get viewstate references
            _activePlanogramView = App.ViewState.ActivePlanogramView;
            _recentPlanogramsView = App.ViewState.RecentPlanogramsView;
            _productLibraryView = App.ViewState.ProductLibraryView;

            _appSettingsView = App.ViewState.Settings;
            _syncActionQueue = new SyncActionQueue(PromptClosingDocuments);

            App.ViewState.RepositoryConnectionChanging += ViewState_RepositoryConnectionChanging;
            App.ViewState.SelectionModeChanged += ViewState_SelectionModeChanged;

            //setup the status bar timer
            _statusTextTimer = new Timer(30000) { Enabled = true };
            _statusTextTimer.Elapsed += (sender, args) =>
            {
                _statusBarText = String.Empty;
                OnPropertyChanged(StatusBarTextProperty);
            };


            RefreshAvailableLabels();
            RefreshAvailableHighlights();
            RefreshFavouriteDataSheets();

            ResetAutosaveTimer();

            OnIsLibraryPanelVisibleChanged();
        }

        #endregion

        #region Commands

        #region ExitApplication

        private RelayCommand _exitApplicationCommand;

        /// <summary>
        ///     Closes the application.
        /// </summary>
        public RelayCommand ExitApplicationCommand
        {
            get
            {
                if (_exitApplicationCommand == null)
                {
                    _exitApplicationCommand =
                        new RelayCommand(p => ExitApplication_Executed())
                        {
                            FriendlyName = Message.MainPage_ExitApplication,
                            SmallIcon = ImageResources.MainPage_ExitApplication
                        };
                    ViewModelCommands.Add(_exitApplicationCommand);
                }
                return _exitApplicationCommand;
            }
        }

        private void ExitApplication_Executed()
        {
            if (AttachedControl != null)
            {
                AttachedControl.Close();
            }
        }

        #endregion

        //nb - see MainPageCommands

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Called when the attached control changes.
        /// </summary>
        /// <param name="oldControl"></param>
        /// <param name="newControl"></param>
        protected override void OnAttachedControlChanged(MainPageOrganiser oldControl, MainPageOrganiser newControl)
        {
            base.OnAttachedControlChanged(oldControl, newControl);

            if (oldControl != null)
            {
                oldControl.Closing -= AttachedControl_Closing;
            }

            if (newControl != null)
            {
                newControl.Closing += AttachedControl_Closing;
            }
        }

        /// <summary>
        ///     Called whenever the settings model changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void UpdateAppSettings(OptionsViewModel optionsViewModel)
        {
            //Update highlights if they have changed.
            if (optionsViewModel.HasHighlightLocationChanged)
            {
                RefreshAvailableHighlights();
            }

            //Update labels if they have changed.
            if (optionsViewModel.HasLabelLocationChanged)
            {
                RefreshAvailableLabels();
            }

            //Update datasheets if they have changed
            if (optionsViewModel.HasDataSheetFavouritesChanged)
            {
                RefreshFavouriteDataSheets();
            }

            //Update autosave if they have changed
            if (optionsViewModel.HasAutosaveChanged)
            {
                ResetAutosaveTimer();
            }

            //Update warnings if they have changed
            if (optionsViewModel.HasWarningsSettingsChanged)
            {
                foreach (PlanControllerViewModel planController in this.PlanControllers)
                {
                    planController.SourcePlanogram.CheckValidationWarnings();
                }
            }
        }

        /// <summary>
        ///     Called when the attached control is closing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AttachedControl_Closing(Object sender, CancelEventArgs e)
        {
            //  Check with the user whether to continue with the group close or not.
            if (!ContinueWithPlanContollerClose(PlanControllers))
            {
                e.Cancel = true;
                return;
            }

            //  If the user wants to continue closing, close them.
            foreach (PlanControllerViewModel controller in PlanControllers.ToList())
            {
                ClosePlanController(controller);
            }

            e.Cancel = false;

            //Save the current settings.
            _appSettingsView.Save();
        }

        /// <summary>
        ///     Called when the active plan controller property value changes.
        /// </summary>
        private void OnActivePlanControllerChanged(PlanControllerViewModel newModel)
        {
            //update the active plan view
            _activePlanogramView.Model = (newModel != null) ? newModel.SourcePlanogram : null;

            IRealImageProvider realImageProvider = PlanogramImagesHelper.RealImageProvider;
            if (realImageProvider == null) return;

            if (PlanControllers.Any())
            {
                //update flag to indicate to each controller if it is the active one.
                foreach (var controller in PlanControllers)
                {
                    controller.IsActivePlanController = (controller == newModel);
                }
            }
            else
            {
                //stop fetching anymore.
                realImageProvider.StopFetchAsync();
            }
        }

        /// <summary>
        /// Called whenever the repository connection is about to change.
        /// </summary>
        private void ViewState_RepositoryConnectionChanging(object sender, ViewState.RepositoryConnectionChangingEventArgs e)
        {
            //check if any plans are open for the current repository.
            Boolean hasRespositoryPlanOpen = false;

            foreach (PlanControllerViewModel controller in this.PlanControllers)
            {
                PackageViewModel parentPackage = controller.SourcePlanogram.ParentPackageView;
                if (parentPackage == null) continue;

                if (parentPackage.PackageConnectionType == PackageConnectionType.Repository
                    && !parentPackage.Model.IsNew)
                {
                    hasRespositoryPlanOpen = true;
                    break;
                }
            }


            if (hasRespositoryPlanOpen)
            {
                //cancel the connection change
                e.Cancel = true;

                //show the dialog telling the user
                // to sort out the plans first.
                CommonHelper.GetWindowService()
                    .ShowOkMessage(MessageWindowType.Warning,
                    Message.MainPage_DisconnectingRepostoryPlansOpen_Header,
                    Message.MainPage_DisconnectingRepostoryPlansOpen_Description);
            }
        }

        /// <summary>
        /// Called whenever the viewstate indicates that the selection mode has changed.
        /// </summary>
        private void ViewState_SelectionModeChanged(object sender, EventArgs e)
        {
            OnPropertyChanged(SelectionTypeProperty);
        }

        #endregion

        #region Methods

        #region Planogram File Methods

        #region Create

        /// <summary>
        ///     Creates a new planogram
        /// </summary>
        public PlanControllerViewModel CreateNewPlanogram()
        {

            //close the backstage
            CommonHelper.SetRibbonBackstageState(this.AttachedControl, false);

            base.ShowWaitCursor(true);
            //Create a new package
            PackageViewModel packageView = PackageViewModel.CreateNewPackage( /*initialPlan*/true);
            PlanogramView planView = packageView.PlanogramViews.First();

            //Create and add the new plan controller
            // nb - the plan properties window will add the initial fixture.
            var newController = new PlanControllerViewModel(planView);
            _planControllers.Add(newController);
            ActivePlanController = newController;

            //Apply default settings
            ApplyDefaultSettings(newController, /*isNewPlan*/true);

            base.ShowWaitCursor(false);

            //show the planogram properties
            Boolean dialogOkayed = newController.ShowPlanProperties();

            if (!dialogOkayed)
            {
                //new plan creation was cancelled so close the controller
                ClosePlanController(newController);
                return null;
            }

            //zoom to fit all so that the view gets adjusted for any new fixtures
            newController.ZoomToFitAll();

            return newController;
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     Show the open planogram file dialog
        /// </summary>
        public void OpenPlanogramsFromFileSystem(String filePath = null, PlanogramImportTemplate externalFileTemplate = null)
        {
            //const Boolean splitProSpaceFiles = true;
            String[] fileNames = null;
            List<String> unOpenableFiles = new List<String>();
            Boolean isSilent = false;

            var extensions =
                        new List<String>(new[]
                                         {
                                             Constants.PlanogramFileExtension,
                                             PlanogramImportFileTypeHelper.Extension[PlanogramImportFileType.SpacemanV9],
                                             PlanogramImportFileTypeHelper.Extension[PlanogramImportFileType.ProSpace],
                                             PlanogramImportFileTypeHelper.Extension[PlanogramImportFileType.Apollo]
                                         });

            //+ get the filenames to open
            if (!String.IsNullOrEmpty(filePath)
                 && extensions.Any(s => String.Equals(Path.GetExtension(filePath), s, StringComparison.OrdinalIgnoreCase)))
            {
                if (System.IO.File.Exists(filePath))
                {
                    fileNames = new[] { filePath };
                    isSilent = true;
                }
                else
                {
                    CommonHelper.GetWindowService().ShowErrorMessage(
                        Message.MainPage_FileNotFoundError_Header,
                        Message.MainPage_FileNotFoundError_Text);
                }
            }
            else
            {
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.InitialDirectory = App.ViewState.GetSessionDirectory(SessionDirectory.Planogram);
                dlg.Filter = Message.MainPage_OpenFileDialogFilter;
                dlg.Multiselect = true;

                if (dlg.ShowDialog(AttachedControl) == true)
                {
                    if (!extensions.Any(s => String.Equals(Path.GetExtension(dlg.FileName), s, StringComparison.OrdinalIgnoreCase)))
                    {
                        CommonHelper.GetWindowService().ShowOkMessage(
                            MessageWindowType.Warning,
                            Message.General_WrongFileType_Title,
                            Message.General_WrongFileType_Description);
                    }
                    else
                    {
                        fileNames = dlg.FileNames.ToArray();
                        String firstFileName = fileNames.FirstOrDefault();

                        if (!String.IsNullOrEmpty(firstFileName))
                        {
                            App.ViewState.SetSessionDirectory(SessionDirectory.Planogram, Path.GetDirectoryName(firstFileName));
                        }
                    }
                }
            }


            //  Check that there are files to open.
            if (fileNames == null ||
                !fileNames.Any()) return;

            //  If ProSpace files are to be imported as separate packages, split the file into single planogram files.
            //if (splitProSpaceFiles) fileNames = SplitProSpaceFiles(fileNames, TempImportFolder);

            //  Order the filenames by the extension so that the same types are processed together.
            fileNames = fileNames.OrderBy(Path.GetExtension).ToArray();

            //+ create the requests
            List<PackageFetchRequest> requests = new List<PackageFetchRequest>();
            foreach (String fileName in fileNames)
            {
                //  If the file is a POG file, just add the new request and continue on to the next file.
                if (fileName.EndsWith(Constants.PlanogramFileExtension, StringComparison.OrdinalIgnoreCase))
                {
                    requests.Add(PackageFetchRequest.NewPogFileRequest(fileName, Path.GetFileName(fileName)));
                    continue;
                }

                //  Process other file extensions.
                PlanogramImportFileType fileType = PlanogramImportFileTypeHelper.GetTypeFromExtension(Path.GetExtension(fileName));

                lock (_silentLock)
                {
                    //If we are running silent then use the last silent template
                    //we had.
                    if (isSilent && (externalFileTemplate == null ||
                                     externalFileTemplate.FileType != fileType))
                    {
                        switch (fileType)
                        {
                            case PlanogramImportFileType.SpacemanV9:
                                externalFileTemplate = _silentSpacemanTemplate;
                                break;
                            case PlanogramImportFileType.ProSpace:
                                externalFileTemplate = _silentProspaceTemplate;
                                break;
                            case PlanogramImportFileType.Apollo:
                                externalFileTemplate = _silentApolloTemplate;
                                break;
                        }
                    }

                    if (externalFileTemplate == null ||
                        externalFileTemplate.FileType != fileType)
                    {
                        String defaultFileTemplate;
                        switch (fileType)
                        {
                            case PlanogramImportFileType.SpacemanV9:
                                defaultFileTemplate = App.ViewState.Settings.Model.DefaultSpacemanFileTemplate;
                                break;
                            case PlanogramImportFileType.ProSpace:
                                defaultFileTemplate = App.ViewState.Settings.Model.DefaultProspaceFileTemplate;
                                break;
                            case PlanogramImportFileType.Apollo:
                                defaultFileTemplate = App.ViewState.Settings.Model.DefaultApolloFileTemplate;
                                break;
                            default:
                                //  Don't add the request as it is for an unknown file type.
                                Debug.Fail("Unknown file type when calling OpenPlanogramsFromFileSystem()");
                                continue;
                        }
                        externalFileTemplate = RequestPlanogramFileTemplate(defaultFileTemplate, fileType, !String.IsNullOrWhiteSpace(defaultFileTemplate) && isSilent);
                    }

                    //  If there is no external file template at this point, 
                    //  the user must have cancelled - stop processing.
                    if (externalFileTemplate == null) return;

                    if (isSilent)
                    {
                        //Make this our next silent template for this filetype
                        switch (fileType)
                        {
                            case PlanogramImportFileType.SpacemanV9:
                                _silentSpacemanTemplate = externalFileTemplate;
                                break;
                            case PlanogramImportFileType.ProSpace:
                                _silentProspaceTemplate = externalFileTemplate;
                                break;
                            case PlanogramImportFileType.Apollo:
                                _silentApolloTemplate = externalFileTemplate;
                                break;
                        }
                    }
                }

                //  Account for multi planogram packages in JDA Space Planning files.
                if (fileType == PlanogramImportFileType.ProSpace)
                {
                    String packageIdMask = String.Format("{0}::{{0}}", fileName);
                    PlanogramImportTemplate template = externalFileTemplate;
                    //  Create a unique request per planogram in the file.
                    IEnumerable<PackageFetchRequest> psaPlanogramRequests =
                        FileHelper.GetPsaPlanogramsFromFile(fileName)
                                  .Select(planName =>
                                  {
                                      String packageId = String.Format(packageIdMask, planName);
                                      return PackageFetchRequest.NewExternalFileRequest(packageId, planName, template);
                                  }).ToList();
                    // NB This should not be necessary as files are now opened in readonly mode and should always be available.
                    //Check we actually created a request
                    if (!psaPlanogramRequests.Any())
                    {
                        //no planograms could be added as the file is locked 
                        unOpenableFiles.Add(fileName);
                    }
                    else
                    {
                        requests.AddRange(psaPlanogramRequests);
                    }
                }
                else
                {
                    requests.Add(PackageFetchRequest.NewExternalFileRequest(fileName, Path.GetFileName(fileName), externalFileTemplate));
                }
            }

            if (unOpenableFiles.Count > 0 && !isSilent)
            {
                //build message to inform user of any files that could not be opened
                StringBuilder msgDescription = new StringBuilder(Message.General_CannotOpenLockedFile_Description);
                msgDescription.AppendLine();
                msgDescription.AppendLine();
                foreach (String fileName in unOpenableFiles)
                {
                    msgDescription.AppendLine(fileName);
                }
                //Show the message
                CommonHelper.GetWindowService().ShowOkMessage(
                            MessageWindowType.Warning,
                            Message.General_CannotOpenLockedFile_Title,
                            msgDescription.ToString());
            }

            //+ open.
            OpenPlanograms(requests);
        }

        /// <summary>
        /// Shows the window for the user to select a file template.
        /// </summary>
        /// <param name="initialPath">the initial file to display.</param>
        /// <param name="fileType"></param>
        /// <returns></returns>
        private PlanogramImportTemplate RequestPlanogramFileTemplate(String initialPath, PlanogramImportFileType fileType, Boolean silent)
        {
            PlanogramImportTemplate returnItem = null;

            //show the template editor
            using (var templateEditor = new PlanogramFileTemplateEditorViewModel(initialPath, fileType))
            {
                //if silent rely on the constructor to populate the default template                
                if (silent &&
                    templateEditor.SelectedItem != null &&
                    templateEditor.MappingTemplateName != PlanogramFileTemplateEditorViewModel.DefaultName)
                {
                    returnItem = templateEditor.SelectedItem;
                }
                else
                {
                    templateEditor.ShowDialog(AttachedControl);

                    if (templateEditor.DialogResult == true) returnItem = templateEditor.SelectedItem;
                }
            }

            return returnItem;
        }

        /// <summary>
        ///     Opens the given plan ids from the repository.
        /// </summary>
        public void OpenPlanogramsFromRepository(IEnumerable<PlanogramInfo> planInfos)
        {
            List<PackageFetchRequest> requests = new List<PackageFetchRequest>();
            foreach (PlanogramInfo info in planInfos)
            {
                requests.Add(PackageFetchRequest.NewRepositoryRequest(info.Id, info.Name));
            }
            OpenPlanograms(requests);
        }

        /// <summary>
        /// Opens the repository planograms for the given infos.
        /// </summary>
        public void OpenPlanogramsFromRepository(IEnumerable<String> planInfos)
        {
            OpenPlanograms(planInfos
                .Select(info => info.Split('|'))
                .Select(strings => PackageFetchRequest.NewRepositoryRequest(Convert.ToInt32(strings[0]), strings[1]))
                .ToList());
        }

        /// <summary>
        ///  Opens a planogram from a recent plan detail
        /// </summary>
        /// <param name="recentPlan"></param>
        public void OpenRecentPlanogram(RecentPlanogram recentPlan)
        {
            if (recentPlan == null) return;

            PackageFetchRequest request;

            switch (recentPlan.Type)
            {
                case RecentPlanogramType.Pog:
                    String fileName = recentPlan.PlanogramId as String;
                    request = PackageFetchRequest.NewPogFileRequest(fileName, Path.GetFileName(fileName));
                    break;

                case RecentPlanogramType.Repository:
                    if (App.ViewState.IsConnectedToRepository)
                    {
                        request = PackageFetchRequest.NewRepositoryRequest(recentPlan.PlanogramId, recentPlan.Name);
                    }
                    else
                    {
                        //show an error.
                        CommonHelper.GetWindowService().ShowErrorMessage(String.Empty, Message.Error_RepositoryNotFound_desc);
                        return;
                    }
                    break;

                default:
                    Debug.Fail("Type not recognised");
                    return;
            }

            OpenPlanograms(new List<PackageFetchRequest> { request });

        }

        #region Process Fetch Requests

        private readonly Queue<PackageFetchRequest> _packagesToFetch = new Queue<PackageFetchRequest>();
        private Stopwatch _packageFetchTimer;
        private PackageFetchRequest _currentRequest;

        /// <summary>
        ///     Class used to make a request to fetch a package.
        /// </summary>
        private sealed class PackageFetchRequest
        {
            #region Properties

            public PackageConnectionType PackageType { get; private set; }
            public Boolean IsExternalFileType { get; private set; }
            public PlanogramImportTemplate RequestImportTemplate { get; private set; }
            public Int32 PercentageValue { get; set; }
            public List<PlanControllerViewModel> PlanogramViews { get; set; }
            public Object RequestId { get; private set; }
            public String RequestDescription { get; private set; }
            public PackageViewModel Result { get; set; }
            public Boolean IsAlreadyOpen { get; set; }
            #endregion

            #region Constructor

            private PackageFetchRequest() { }

            #endregion

            #region Factory Methods

            public static PackageFetchRequest NewRepositoryRequest(Object id, String description)
            {
                PackageFetchRequest item = new PackageFetchRequest();
                item.PackageType = PackageConnectionType.Repository;
                item.RequestId = id;
                item.RequestDescription = description;
                return item;
            }

            public static PackageFetchRequest NewPogFileRequest(Object id, String description)
            {
                PackageFetchRequest item = new PackageFetchRequest();
                item.PackageType = PackageConnectionType.FileSystem;
                item.IsExternalFileType = false;
                item.RequestId = id;
                item.RequestDescription = description;
                return item;
            }

            public static PackageFetchRequest NewExternalFileRequest(Object id, String description, PlanogramImportTemplate template)
            {
                PackageFetchRequest item = new PackageFetchRequest();
                item.PackageType = PackageConnectionType.FileSystem;
                item.IsExternalFileType = true;
                item.RequestId = id;
                item.RequestDescription = description;
                item.RequestImportTemplate = template;

                return item;
            }

            /// <summary>
            ///     Create a new <see cref="PackageFetchRequest"/> for a multi-planogram package file, requesting a specific <paramref name="packageId"/>.
            /// </summary>
            /// <param name="id"></param>
            /// <param name="description"></param>
            /// <param name="template"></param>
            /// <returns></returns>
            public static PackageFetchRequest NewExternalFileRequest(String id, String description, PlanogramImportTemplate template)
            {
                return new PackageFetchRequest
                {
                    PackageType = PackageConnectionType.FileSystem,
                    IsExternalFileType = true,
                    RequestId = id,
                    RequestDescription = description,
                    RequestImportTemplate = template
                };
            }

            #endregion
        }

        /// <summary>
        ///     Processes the next queued fetch request.
        /// </summary>
        private void FetchNextPackage()
        {
            if (_packagesToFetch.Count > 0)
            {
                PackageFetchRequest request = _packagesToFetch.Dequeue();
                _currentRequest = request;

                //TODO: Switch to use ModalBusyWorkerService.
                if (AttachedControl != null)
                {
                    var fetchPackageWorker = new BackgroundWorker();
                    fetchPackageWorker.DoWork += FetchPackageWorker_DoWork;
                    fetchPackageWorker.RunWorkerCompleted += FetchPackageWorker_RunWorkerCompleted;
                    fetchPackageWorker.RunWorkerAsync(request);

                    var busy = Galleria.Framework.Controls.Wpf.Helpers.GetWindow<ModalBusy>();
                    if (busy == null)
                    {
                        busy = new ModalBusy
                        {
                            Header = Message.MainPage_PackageFetchBusyHeader,
                            //don't bother being determinate if we are only loading one plan.
                            IsDeterminate = (request.PercentageValue < 100),
                            ProgressPercentage = request.PercentageValue,
                            Description = request.RequestDescription
                        };
                        App.ShowWindow(busy, true);
                    }
                    else
                    {
                        //update progress
                        busy.ProgressPercentage = request.PercentageValue;
                        busy.Description = request.RequestDescription;
                    }
                }
                else
                {
                    //for unit testing purposes
                    var fetchPackageWorker = new BackgroundWorker();
                    var doWorkArgs = new DoWorkEventArgs(request);
                    Exception error = null;
                    try
                    {
                        FetchPackageWorker_DoWork(fetchPackageWorker, doWorkArgs);
                    }
                    catch (Exception e)
                    {
                        error = e;
                    }
                    FetchPackageWorker_RunWorkerCompleted(fetchPackageWorker,
                        new RunWorkerCompletedEventArgs(doWorkArgs.Result, error, false));
                }
            }
            else
            {
                ActivePlanController = _planControllers.LastOrDefault();
                //close the busy window.
                if (AttachedControl != null)
                {
                    ModalBusy busy = Galleria.Framework.Controls.Wpf.Helpers.GetWindow<ModalBusy>();
                    if (busy != null) busy.Close();
                }
            }
        }

        /// <summary>
        /// Carries out the planogram fetch.
        /// </summary>
        private void FetchPackageWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var request = e.Argument as PackageFetchRequest;
            PackageViewModel packageView = null;

            //check that the requested plan has not already been loaded
            if (PlanControllers.Any(controller => Equals(controller.SourcePlanogram.ParentPackageView.Model.Id, request.RequestId)))
            {
                request.IsAlreadyOpen = true;
                e.Result = request;
                return;
            }

            //fetch
            switch (request.PackageType)
            {
                case PackageConnectionType.FileSystem:
                    {
                        var fileName = request.RequestId as String;
                        if (!String.IsNullOrEmpty(fileName))
                        {
                            try
                            {
                                if (request.IsExternalFileType)
                                {
                                    packageView = PackageViewModel.ImportPackageByFileName(fileName, request.RequestImportTemplate);
                                }
                                else
                                {
                                    packageView = PackageViewModel.FetchPackageByFileName(fileName);
                                }
                            }
                            catch (Exception ex)
                            {
                                //package load failed for some reason.
                                //make sure that the package does not get stuck locked
                                Package.TryUnlockPackageById(PackageType.FileSystem, fileName, DomainPrincipal.CurrentUserId, PackageLockType.User);
                                throw ex;
                            }
                        }
                    }
                    break;

                case PackageConnectionType.Repository:
                    {
                        try
                        {
                            if (App.ViewState.IsConnectedToRepository)
                            {
                                packageView = PackageViewModel.FetchPackageById(request.RequestId);
                            }
                            else
                            {
                                throw new InvalidOperationException(Message.Error_RepositoryNotFound_desc);
                            }
                        }
                        catch (Exception ex)
                        {
                            //package load failed for some reason.
                            //make sure that the package does not get stuck locked
                            Package.TryUnlockPackageById(request.RequestId, DomainPrincipal.CurrentUserId, PackageLockType.User);
                            throw ex;
                        }
                    }
                    break;

                default:
                    throw new NotImplementedException();
            }

            //load the package into new views.
            if (packageView != null)
            {
                request.Result = packageView;

                if (packageView.Model != null)
                {
                    request.PlanogramViews = new List<PlanControllerViewModel>(packageView.PlanogramViews.Count);
                    foreach (var planView in packageView.PlanogramViews)
                    {
                        try
                        {
                            var controller = new PlanControllerViewModel(planView);
                            request.PlanogramViews.Add(controller);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }
            }

            e.Result = request;
        }

        /// <summary>
        /// Called when a planogram fetch has completed.
        /// </summary>
        private void FetchPackageWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var fetchPackageWorker = (BackgroundWorker)sender;
            fetchPackageWorker.DoWork -= FetchPackageWorker_DoWork;
            fetchPackageWorker.RunWorkerCompleted -= FetchPackageWorker_RunWorkerCompleted;

            PackageFetchRequest lastRequest = _currentRequest;
            _currentRequest = null;

            if (e.Error != null)
            {
                Exception rootEx = e.Error.GetBaseException();
                LocalHelper.RecordException(rootEx);

                if (rootEx is PackageDuplicateProductGtinException)
                {
                    //notify the user of duplicate product gtins
                    CommonHelper.GetWindowService().ShowOkMessage(
                            MessageWindowType.Error,
                             Message.MainPage_PackageDuplicateGtinErrorHeader,
                             String.Format(Message.MainPage_PackageDuplicateGtinErrorHeader_Description, lastRequest.RequestDescription, rootEx.Message));
                }
                else
                {
                    String msg = String.Format(Message.MainPage_PackageFetchErrorDescription, lastRequest.RequestDescription);
                    if (rootEx.Message == Message.Error_RepositoryNotFound_desc) msg = rootEx.Message;


                    //notify the user of the error.
                    CommonHelper.GetWindowService().ShowOkMessage(
                            MessageWindowType.Info, Message.MainPage_PackageFetchErrorHeader, msg);
                }

                //remove from the mru if it was a file entry
                if (lastRequest.PackageType == PackageConnectionType.FileSystem && !lastRequest.IsExternalFileType)
                {
                    RemoveFromMru(RecentPlanogramType.Pog, lastRequest.RequestId);
                }

            }
            else
            {
                var request = e.Result as PackageFetchRequest;
                if (request != null && !request.IsAlreadyOpen)
                {
                    if (request.PlanogramViews != null)
                    {
                        // Check if there are actually any plans - if not, warn the user
                        if (request.PlanogramViews.Count == 0)
                        {
                            CommonHelper.GetWindowService().ShowOkMessage(
                                MessageWindowType.Info,
                                Message.MainPage_PackageFetchErrorHeader,
                                String.Format(Message.MainPage_PackageFetchNoPlanograms_Description, request.RequestDescription));
                        }
                        else
                        {
                            //if the plan is from the repository and it is already locked by another person then inform the user.
                            if (request.PlanogramViews.First().SourcePlanogram.ParentPackageView.Model.IsReadOnly 
                                && !request.IsExternalFileType)
                            {
                                //check who has it locked and show the message
                                String lockUserName = request.PlanogramViews.First().SourcePlanogram.ParentPackageView.GetLockedByOtherUserName();
                                if (!String.IsNullOrEmpty(lockUserName))
                                {
                                    CommonHelper.GetWindowService().ShowOkMessage(
                                    MessageWindowType.Info,
                                    Message.MainPage_PackageIsLockedByAnotherUser,
                                    String.Format(Message.MainPage_PackageIsLockedByAnotherUser_Description, request.RequestDescription, lockUserName));
                                }
                            }


                            foreach (var plan in request.PlanogramViews)
                            {

                                //Perform import type specific post actions
                                if (request.IsExternalFileType && request.RequestImportTemplate != null)
                                {
                                    Planogram planogram = plan.SourcePlanogram.Model;
                                    RestructurePlanogramSettings rSettings = request.RequestImportTemplate.GetRestructurePlanogramSettings();
                                    if (rSettings.Enabled)
                                    {
                                        rSettings.Restructure(planogram);
                                    }
                                    else if (planogram.MetaBayCount > 1)
                                    {
                                        switch (request.RequestImportTemplate.FileType)
                                        {
                                            case PlanogramImportFileType.Apollo:
                                                PlanogramRestructuringHelper.RestructureBaysByCount(planogram, planogram.MetaBayCount.Value, true);
                                                break;
                                        }
                                    }
                                }

                                //add the new controller.
                                _planControllers.Add(plan);
                                ApplyDefaultSettings(plan, /*isNewPlan*/false);


                                //mark the plan as initialized so that the reprocess does not flag as a change.
                                plan.SourcePlanogram.ParentPackageView.Model.MarkGraphAsInitialized();
                            }

                            //update the mru
                            AddToMRU(request.Result);
                        }
                    }
                }
                else
                {
                    //tell user that the plan is already open
                    CommonHelper.GetWindowService().ShowOkMessage(
                        MessageWindowType.Info,
                        Message.Error_DuplicatePlanogramOpen_Header,
                        String.Format(Message.Error_DuplicatePlanogramOpen_Desc, request != null ? request.RequestDescription : null));
                }
            }

            FetchNextPackage();
        }

        /// <summary>
        ///     Opens the planograms specified by the given fetch requests.
        /// </summary>
        /// <param name="fetchRequests"></param>
        private void OpenPlanograms(IEnumerable<PackageFetchRequest> fetchRequests)
        {
            // Set backstage to false when opening planograms
            CommonHelper.SetRibbonBackstageState(this.AttachedControl, false);

            _packageFetchTimer = new Stopwatch();
            _packageFetchTimer.Start();

            //create the fetch queue
            _packagesToFetch.Clear();

            for (Int32 i = 0; i < fetchRequests.Count(); i++)
            {
                PackageFetchRequest req = fetchRequests.ElementAt(i);
                req.PercentageValue = (100 / fetchRequests.Count()) * (i + 1);
                _packagesToFetch.Enqueue(req);
            }

            FetchNextPackage();
            ActivePlanController = _planControllers.LastOrDefault();
        }

        #endregion

        #endregion

        #region Save

        /// <summary>
        ///     Saves the given planogram.
        /// </summary>
        /// <param name="controller"></param>
        public void Save(PlanControllerViewModel controller, Boolean checkForOverwrites = true)
        {
            PackageViewModel packageView = controller.SourcePlanogram.ParentPackageView;

            // If the plan is new or read only, display the backstage save options.
            if (packageView.Model.IsNew || packageView.Model.IsReadOnly)
            {
                if (this.AttachedControl != null)
                {
                    // takes you the backstage Save Tab  under the save to repository option
                    this.AttachedControl.SetRibbonBackstageState(true);
                    this.AttachedControl.xBackstageSaveTab.IsSelected = true;
                }
            }
            else
            {
                //if the package is not dirty then don't bother doing anything.
                if (!packageView.Model.IsDirty) return;

                Boolean isFileSystem = (packageView.PackageConnectionType == PackageConnectionType.FileSystem);
                String packageName = (isFileSystem) ? packageView.Model.Name : packageView.PlanogramViews.First().Name;

                // Check for overwrites if we're saving to the repository.
                if (!isFileSystem && checkForOverwrites)
                {
                    PlanogramGroup selectedPlanogramGroup = GetBackstagePlanogramGroup(controller.SourcePlanogram.ParentPackageView.PlanogramGroupId, controller.SourcePlanogram.ParentPackageView.PlanogramGroupName);
                    if (selectedPlanogramGroup == null) return;
                    switch (CheckPlanOverwrite(controller, selectedPlanogramGroup, controller.SourcePlanogram))
                    {
                        case false:
                            // If don't overwrite, then return. 
                            return;

                        case true:
                        case null:
                            //Otherwise, if overwrite or continue to save, then continue.
                            break;
                    }
                }

                base.ShowWaitCursor(true);

                try
                {
                    //just save
                    packageView.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    // If we couldn't get a lock, then the planogram has been unlocked and relocked by
                    // another user and is now read-only. In this case, we can't edit the plan anymore, 
                    // so all we can do is view the plan read-only.
                    Exception baseException = ex.GetBaseException();
                    if (baseException is PackageLockException)
                    {
                        NotifyUserSaveHadErrors(Message.Ribbon_Save_PackageIsLocked_Header, Message.Ribbon_Save_PackageIsLocked_Description);

                        RecreatePlanogramViews(packageView);
                    }
                    else if (baseException is ConcurrencyException)
                    {
                        //  The planogram was modified by someone else while the user had changes to commit.
                        //  Notify the user and make the plan "new" so that it must be saved as.
                        LocalHelper.RecordException(baseException);
                        NotifyUserSaveHadErrors(Message.Ribbon_Save_ConcurrencyError_Header, Message.Ribbon_Save_ConcurrencyError_Description);

                        RecreatePlanogramViews(packageView);

                        // Set the file as new, so that any further attempts to save it are handled correctly.
                        packageView.Model.MarkGraphAsNew();
                    }
                    else
                    {
                        // Something totally unexpected happened. Log the error and notify the user.
                        LocalHelper.RecordException(baseException);
                        CommonHelper.GetWindowService().ShowErrorOccurredMessage(packageName, OperationType.Save);
                    }
                    return;
                }
                catch (Exception ex)
                {
                    // Something totally unexpected happened. Log the error and notify the user.
                    base.ShowWaitCursor(false);
                    LocalHelper.RecordException(ex.GetBaseException());
                    CommonHelper.GetWindowService().ShowErrorOccurredMessage(packageName, OperationType.Save);
                    return;
                }


                //update the mru
                AddToMRU(packageView);

                base.ShowWaitCursor(false);

                //close the backstage.
                this.AttachedControl.SetRibbonBackstageState(false);
            }
        }

        /// <summary>
        ///     Saves the given controller as a new file.
        /// </summary>
        /// <param name="controller">the plan controller to be saved.</param>
        public void SaveAsFile(PlanControllerViewModel controller, String filePath = null)
        {
            PackageViewModel packageView = controller.SourcePlanogram.ParentPackageView;
            //Boolean isNew = packageView.Model.IsNew;

            Window parentWindow = AttachedControl;
            PlanogramView planView = App.ViewState.ActivePlanogramView.Model;
            Boolean isSilent = false;
            Boolean isSuccessful = false;

            //Check if path is provided - if so, method is called from a task so run silently
            if (filePath != null)
            {
                if (System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(filePath)))
                {
                    isSilent = true;
                }
            }

            String saveAsPath = filePath;
            Boolean canContinue = true;

            if (saveAsPath == null)
            {
                canContinue = false;

                #region Get File Path
                if (parentWindow != null)
                {
                    while (!canContinue)
                    {
                        //Show the dialog so that the user can pick a new filepath.
                        var dlg = new SaveFileDialog
                        {
                            Filter = PlanogramExportFileTypeHelper.Filters[planView.SaveSettings.SaveAsFileType],
                            FileName = planView.Name,
                            AddExtension = true,
                            DefaultExt = PlanogramExportFileTypeHelper.Extensions[planView.SaveSettings.SaveAsFileType],
                            OverwritePrompt = true,
                            InitialDirectory = App.ViewState.GetSessionDirectory(SessionDirectory.Planogram)
                        };

                        if (dlg.ShowDialog(parentWindow) == true)
                        {
                            if (String.Equals(packageView.FileName, dlg.FileName, StringComparison.OrdinalIgnoreCase))
                            {
                                canContinue = false;

                                CommonHelper.GetWindowService().ShowOkMessage(MessageWindowType.Warning,
                                    Message.ExportToExternalFileType_ExportFileIsOpen_Header,
                                    Message.ExportToExternalFileType_ExportFileIsOpen_Description);
                            }
                            else
                            {
                                //ok to continue
                                canContinue = true;

                                saveAsPath = dlg.FileName;

                                //update the session directory
                                App.ViewState.SetSessionDirectory(
                                    SessionDirectory.Planogram, Path.GetDirectoryName(dlg.FileName));
                            }
                        }
                        else
                        {
                            //user cancelled - just continue and exit
                            saveAsPath = String.Empty;
                            canContinue = true;
                        }
                    }
                }
                #endregion
            }

            if (!String.IsNullOrEmpty(saveAsPath) && canContinue)
            {
                base.ShowWaitCursor(true);

                //planView.Name = Path.GetFileNameWithoutExtension(saveAsPath);

                //Save the file depending on the type of export we are performing
                switch (planView.SaveSettings.SaveAsFileType)
                {
                    case PlanogramExportFileType.POG:
                        #region Save POG file
                        try
                        {
                            //For save as change the underlying planogram name. We do not do this for exports (V8-32321)
                            planView.Name = Path.GetFileNameWithoutExtension(saveAsPath);
                            //Save a copy of the plan
                            packageView.SaveAsFile(saveAsPath);

                            isSuccessful = true;
                        }
                        catch (Exception ex)
                        {
                            base.ShowWaitCursor(false);

                            Exception rootException = ex.GetBaseException();
                            LocalHelper.RecordException(rootException);

                            if (rootException is System.IO.IOException)
                            {
                                CommonHelper.GetWindowService().ShowErrorMessage(
                                    Message.MainPage_Save_FileWriteFailedHeader,
                                    Message.MainPage_Save_FileWriteFailedDesc);
                            }
                            else
                            {
                                CommonHelper.GetWindowService().ShowErrorOccurredMessage(saveAsPath, OperationType.Save);
                            }

                            return;
                        }

                        //update the mru
                        AddToMRU(packageView);

                        break;
                    #endregion

                    case PlanogramExportFileType.Apollo:
                    case PlanogramExportFileType.JDA:
                    case PlanogramExportFileType.Spaceman:
                        isSuccessful = SaveAsExternalFileType(planView, saveAsPath, isSilent);
                        break;
                }

                if (isSuccessful)
                {
                    //close the backstage
                    this.AttachedControl.SetRibbonBackstageState(false);
                }

                base.ShowWaitCursor(false);
            }
        }

        /// <summary>
        /// Saves the given plan to a file in the given format
        /// </summary>
        /// <param name="planView">The active planogram to save</param>
        /// <param name="saveAsPath">The path to save the planogram to</param>
        /// <param name="isSilent">Should the user be prompted for a template?</param>
        /// <returns>True if save was successful, otherwise false</returns>
        private Boolean SaveAsExternalFileType(PlanogramView planView, String saveAsPath, Boolean isSilent = false)
        {
            base.ShowWaitCursor(true);

            PackageViewModel packageView = planView.ParentPackageView;

            PlanogramExportTemplate exportFileTemplate = null;
            String defaultFileTemplate = null;

            //Select the default templates here depending on file type
            #region Get Default Template Path
            switch (planView.SaveSettings.SaveAsFileType)
            {
                case PlanogramExportFileType.JDA:
                    defaultFileTemplate = App.ViewState.Settings.Model.DefaultExportJDAFileTemplate;
                    break;
                case PlanogramExportFileType.Apollo:
                    defaultFileTemplate = App.ViewState.Settings.Model.DefaultExportApolloFileTemplate;
                    break;
                case PlanogramExportFileType.Spaceman:
                    defaultFileTemplate = App.ViewState.Settings.Model.DefaultExportSpacemanFileTemplate;
                    break;
            }
            #endregion

            base.ShowWaitCursor(false);

            //get or create template
            exportFileTemplate = RequestPlanogramExportFileTemplate(defaultFileTemplate, planView.SaveSettings.SaveAsFileType, isSilent);

            //  If there is no external file template at this point, 
            //  the user must have cancelled - stop processing.
            if (exportFileTemplate == null) return false;

            #region Save Planogram To File

            try
            {
                base.ShowWaitCursor(true);

                //Take a copy of the plan
                Package package = packageView.Model.Clone(true);

                base.ShowWaitCursor(false);

                //Save the plan to external format
                SavePlanToExternalFileTypeWork work = new SavePlanToExternalFileTypeWork(package, planView.SaveSettings, planView.Name, saveAsPath, exportFileTemplate);
                CommonHelper.GetModalBusyWorkerService().RunWorker(work);
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);

                Exception rootException = ex.GetBaseException();
                LocalHelper.RecordException(rootException);

                if (rootException is System.IO.IOException)
                {
                    CommonHelper.GetWindowService().ShowErrorMessage(
                        Message.MainPage_Save_FileWriteFailedHeader,
                        Message.MainPage_Save_FileWriteFailedDesc);
                }
                else
                {
                    CommonHelper.GetWindowService().ShowErrorOccurredMessage(saveAsPath, OperationType.Save);
                }
                return false;
            }

            return true;
            #endregion
        }

        private PlanogramExportTemplate RequestPlanogramExportFileTemplate(String initialPath, PlanogramExportFileType fileType, Boolean silent)
        {
            PlanogramExportTemplate returnItem = null;

            //show the export template editor
            using (var exportTemplateEditor = new PlanogramExportFileTemplateEditorViewModel(initialPath, fileType))
            {
                if (silent &&
                    exportTemplateEditor.SelectedItem != null &&
                    exportTemplateEditor.MappingTemplateName != PlanogramExportFileTemplateEditorViewModel.DefaultName)
                {
                    returnItem = exportTemplateEditor.SelectedItem;
                }
                else
                {
                    exportTemplateEditor.ShowDialog(AttachedControl);
                    if (exportTemplateEditor.DialogResult == true) returnItem = exportTemplateEditor.SelectedItem;
                }
            }

            return returnItem;
        }

        /// <summary>
        ///     Saves the given controller as a new repository record.
        /// </summary>
        /// <param name="controller">the plan controller to be saved.</param>
        public void SaveAsRepository(PlanControllerViewModel controller)
        {
            PackageViewModel packageView = controller.SourcePlanogram.ParentPackageView;
            PlanogramView planogram = controller.SourcePlanogram;

            PlanogramGroup selectedPlanogramGroup = GetBackstagePlanogramGroup(packageView.PlanogramGroupId, packageView.PlanogramGroupName);
            if (selectedPlanogramGroup == null) return;

            //carry on with the save
            SaveAsRepository(controller, selectedPlanogramGroup);
        }

        /// <summary>
        /// Returns the <see cref="PlanogramGroup"/> that is currently selected in the backstage save screen.
        /// </summary>
        /// <param name="planogramGroupId">The planogram's group id</param>
        /// <param name="planogramGroupName">The planogram's group name.</param>
        /// <returns>The selected <see cref="PlanogramGroup"/>, or <c>null</c> if nothing could be found.</returns>        
        private PlanogramGroup GetBackstagePlanogramGroup(Int32? planogramGroupId, String planogramGroupName)
        {
            //TODO: refactor so that we aren't using the attached control here.
            PlanogramHierarchy hierarchy;
            if (AttachedControl?.xBackstageSave.ViewModel != null)
            {
                hierarchy = AttachedControl.xBackstageSave.ViewModel.PlanogramHierarchyView.Model;
            }
            else
            {
                try
                {
                    hierarchy = PlanogramHierarchy.FetchByEntityId(App.ViewState.EntityId);
                }
                catch (Exception)
                {
                    hierarchy = null;
                    // ignored
                }
            }


            PlanogramGroup selectedPlanogramGroup = null;
            if (planogramGroupId != null)
            {
                selectedPlanogramGroup = hierarchy?.EnumerateAllGroups()?.FirstOrDefault(p => p.Id == planogramGroupId);
            }

            // If that didn't work, just use the root group's Id.
            if (selectedPlanogramGroup == null)
            {
                selectedPlanogramGroup = hierarchy?.RootGroup;
            }

            //if still null then tell the user we failed.
            if (selectedPlanogramGroup == null)
            {
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(planogramGroupName, OperationType.Save);
            }

            return selectedPlanogramGroup;
        }

        internal void SaveAsRepository(PlanControllerViewModel controller, PlanogramGroup selectedPlanogramGroup)
        {
            PackageViewModel packageView = controller.SourcePlanogram.ParentPackageView;
            PlanogramView planogram = controller.SourcePlanogram;

            //Now check whether there is an association between specified category code and category name and warn user.
            if (selectedPlanogramGroup.ProductGroupId.HasValue)
            {
                ProductGroupInfo productGroup = ProductGroupInfoList.FetchByProductGroupIds(new List<Int32> { selectedPlanogramGroup.ProductGroupId.Value }).FirstOrDefault();
                if (productGroup != null)
                {
                    String productGroupCode = productGroup.Code;
                    String productGroupName = productGroup.Name;

                    if (planogram.CategoryCode != productGroupCode && planogram.CategoryName != productGroupName)
                    {
                        //Both do not match - confirm user is ok with this.
                        if (CommonHelper.GetWindowService().ShowMessage(MessageWindowType.Warning,
                            Message.MainPageViewModel_CheckUnexpected_CodeAndNameTitle,
                            Message.MainPageViewModel_CheckUnexpected_CodeAndNameMessage,
                            Message.Generic_Yes, Message.Generic_No)
                            != ModalMessageResult.Button1) return;
                    }
                    else if (planogram.CategoryCode != productGroupCode)
                    {
                        //Just category code does not match - confirm user is ok with this.
                        if (CommonHelper.GetWindowService().ShowMessage(MessageWindowType.Warning,
                           Message.MainPageViewModel_CheckUnexpected_CodeTitle,
                           Message.MainPageViewModel_CheckUnexpected_CodeMessage,
                           Message.Generic_Yes, Message.Generic_No)
                           != ModalMessageResult.Button1) return;
                    }
                    else if (planogram.CategoryName != productGroupName)
                    {
                        //Just name does not match - confirm user is ok with this.
                        if (CommonHelper.GetWindowService().ShowMessage(MessageWindowType.Warning,
                           Message.MainPageViewModel_CheckUnexpected_NameTitle,
                           Message.MainPageViewModel_CheckUnexpected_NameMessage,
                           Message.Generic_Yes, Message.Generic_No)
                           != ModalMessageResult.Button1) return;
                    }
                }
            }

            // Check if we need to overwrite.
            switch (CheckPlanOverwrite(controller, selectedPlanogramGroup, planogram))
            {
                case true:
                    // If overwrite, continue.
                    break;

                case false:
                    // If don't overwrite, just return.
                    return;

                case null:
                    // If no overwrite is necessary, just save.
                    Save(controller, checkForOverwrites: false);
                    return;
            }

            Int32 entityId = selectedPlanogramGroup.ParentHierarchy.EntityId;

            //Now perfom the actual save as.
            ShowWaitCursor(true);
            try
            {
                //Take copy of existing id
                Int32? originalPackageId = null;
                if (!packageView.Model.IsNew)
                {
                    originalPackageId = packageView.Model.Id as Int32?;
                }

                packageView.SaveAsToRepository(entityId, selectedPlanogramGroup);

                PlanogramGroup.AssociatePlanograms(selectedPlanogramGroup.Id,
                    packageView.PlanogramViews.Select(view => (Int32)view.Model.Id).ToList());

                //If we have any potential content that we may need to copy across too
                if (originalPackageId.HasValue)
                {
                    PackageHelper.CopyPackageContent(originalPackageId.Value, (Int32)packageView.Model.Id);
                }
            }
            catch (Exception ex)
            {
                ShowWaitCursor(false);
                LocalHelper.RecordException(ex.GetBaseException());
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(packageView.Model.Name, OperationType.Save);
                return;
            }


            ShowWaitCursor(false);

            //update the mru
            AddToMRU(packageView);


            //close the backstage.
            this.AttachedControl.SetRibbonBackstageState(false);
        }

        /// <summary>
        /// Check for duplicate plans that may need to be overwritten by saving the plan.
        /// </summary>
        /// <param name="controller">The active plan controller.</param>
        /// <param name="selectedPlanogramGroup">The selected planogram group.</param>
        /// <param name="planogram">The plan to save.</param>
        /// <returns>True if the the plan should overwrite, false if it should not and null if there are no duplicates.</returns>
        private Boolean? CheckPlanOverwrite(
            PlanControllerViewModel controller, PlanogramGroup selectedPlanogramGroup, PlanogramView planogram)
        {
            //check if the plan overwrite warning should be displayed
            PlanogramInfoList existingPlansInGroup = PlanogramInfoList.FetchByPlanogramGroupId(selectedPlanogramGroup.Id);
            PlanogramInfo planToOverwrite = existingPlansInGroup.FirstOrDefault(p => String.Compare(p.Name, planogram.Name, true) == 0);
            if (planToOverwrite != null)
            {
                //if the planogram with the same name is locked then there
                // is nothing we can do so just tell the user to rename.
                // Edit: Unless the locking user is the person saving!
                if (planToOverwrite.IsLocked && planToOverwrite.LockUserId != DomainPrincipal.CurrentUserId)
                {
                    CommonHelper.GetWindowService().ShowOkMessage(MessageWindowType.Warning,
                      Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_CopyPlanograms_DuplicateHeader,
                      Message.MainPage_SaveAs_PlanToOverwriteIsLocked);
                    return false;
                }

                //if we found the same plan as the one we are trying to save
                // then we don't need to worry about overwriting - just perform a normal save.
                if (Object.Equals((Object)planToOverwrite.Id, planogram.ParentPackageView.Model.Id))
                {
                    return null;
                }

                //otherwise confirm the overwrite with the user.
                if (!PlanogramRepositoryUIHelper.ConfirmPlanogramOverwriteWithUser(planogram.Name, planogram.ParentPackageView.PlanogramGroupName)) return false;

                //try to delete the existing plan
                ShowWaitCursor(true);
                try
                {
                    Package.DeletePackageById(planToOverwrite.Id, DomainPrincipal.CurrentUserId, PackageLockType.User);
                }
                catch (DataPortalException ex)
                {
                    if (ex.GetBaseException() is PackageLockException)
                    {
                        //tell the user the plan is locked.
                        CommonHelper.GetWindowService().ShowOkMessage(MessageWindowType.Warning,
                          Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_CopyPlanograms_DuplicateHeader,
                          Message.MainPage_SaveAs_PlanToOverwriteIsLocked);
                    }
                    else
                    {
                        LocalHelper.RecordException(ex);
                        CommonHelper.GetWindowService().ShowErrorOccurredMessage(planToOverwrite.Name, OperationType.Save);
                    }
                    return false;
                }
                finally
                {
                    ShowWaitCursor(false);
                }
            }

            return true;
        }

        private void RecreatePlanogramViews(PackageViewModel packageView)
        {
            // As part of the package view's save, all of the planogram view models will
            // have been disposed. We need to re-build them manually here.
            foreach (PlanogramView planogramView in packageView.PlanogramViews)
            {
                planogramView.UpdateView(planogramView.Model);
            }

            // Ensure that any bindings to the plan are updated for the read only status.
            OnPropertyChanged(ActivePlanControllerProperty);
        }

        #endregion

        #region Autosave

        private Timer _autosaveTimer;

        /// <summary>
        ///     Called whenever the autosave timer elapses
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutosaveTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            String autosaveDirectory = _appSettingsView.Model.AutosaveLocation;
            if (!Directory.Exists(autosaveDirectory))
            {
                try
                {
                    Directory.CreateDirectory(autosaveDirectory);
                }
                catch (Exception)
                {
                    return;
                }
            }

            //cycle through autosaving all savable packages
            foreach (var packageView in
                PlanControllers.Select(p => p.SourcePlanogram.ParentPackageView).Distinct())
            {
                packageView.AutoSave(autosaveDirectory);
            }
        }

        /// <summary>
        ///     Starts a new autosave timer.
        /// </summary>
        /// <param name="interval"></param>
        private void ResetAutosaveTimer()
        {
            var settings = _appSettingsView.Model;

            if (settings.IsAutosaveEnabled
                && settings.AutosaveInterval > 0)
            {
                Double timerInterval = new TimeSpan(0, settings.AutosaveInterval, 0).TotalMilliseconds;

                Boolean newTimer = true;

                //clear the existing timer;
                if (_autosaveTimer != null)
                {
                    if (_autosaveTimer.Interval == timerInterval)
                    {
                        //the timer interval has not changed so don't
                        // bother restarting.
                        newTimer = false;
                    }
                    else
                    {
                        StopAutosaveTimer();
                    }
                }


                if (newTimer)
                {
                    //setup a new timer
                    var timer = new Timer(timerInterval);
                    _autosaveTimer = timer;
                    timer.Elapsed += AutosaveTimer_Elapsed;

                    timer.Start();
                }
            }
            else
            {
                StopAutosaveTimer();
            }
        }

        /// <summary>
        ///     Stops the current autosave timer
        /// </summary>
        private void StopAutosaveTimer()
        {
            if (_autosaveTimer != null)
            {
                _autosaveTimer.Stop();
                _autosaveTimer.Elapsed -= AutosaveTimer_Elapsed;
                _autosaveTimer = null;
            }
        }

        #endregion

        #region Closing

        /// <summary>
        ///     Queue the given <paramref name="request"/> to close a plan controller when all other closing documents have been queued too.
        /// </summary>
        /// <param name="request"></param>
        public void RequestClosePlanController(ClosePlanControllerRequest request)
        {
            IsCloseDocumentAllowed = false;
            _syncActionQueue.Add(request);
        }

        /// <summary>
        ///     Invoked by the <see cref="SyncActionQueue"/> instance when the delay time is up and no other documents are being added to be closed in a batch.
        /// </summary>
        /// <param name="syncActionQueueItems">The items that were included in the current batch to be closed.</param>
        private void PromptClosingDocuments(ReadOnlyCollection<ISyncActionQueueItem> syncActionQueueItems)
        {
            IsCloseDocumentAllowed = true;

            List<ClosePlanControllerRequest> requests = syncActionQueueItems.OfType<ClosePlanControllerRequest>().ToList();

            //  Check with the user whether to continue with the group close or not.
            if (ContinueWithPlanContollerClose(requests.Select(request => request.Controller.ViewModel)))
            {
                //  If the user wants to continue closing, close them.
                foreach (ClosePlanControllerRequest request in requests)
                {
                    try
                    {
                        request.PendingCloseAction.Invoke();
                    }
                    catch(Exception ex)
                    {
                        CommonHelper.RecordException(ex);
                    }
                }
            }

            IsCloseDocumentAllowed = false;
        }

        /// <summary>
        ///     Called when a plan controller docking tab is closed.
        /// </summary>
        /// <param name="controller"></param>
        public void ClosePlanController(PlanControllerViewModel controller)
        {
            base.ShowWaitCursor(true);

            _planControllers.Remove(controller);

            //update the active plan controller
            if (ActivePlanController == controller)
            {
                ActivePlanController = PlanControllers.FirstOrDefault();
            }

            PackageViewModel packageView = controller.SourcePlanogram.ParentPackageView;

            controller.Dispose();
            controller = null;

            //dispose of the package if no other plans exist.
            if (packageView != null &&
                !PlanControllers.Any(p => p.SourcePlanogram.ParentPackageView == packageView))
            {
                try
                {
                    packageView.Dispose();
                }
                catch (Exception ex)
                {
                    //just record the exception and carry on.
                    LocalHelper.RecordException(ex.GetBaseException());
                }
            }

            base.ShowWaitCursor(false);
        }

        /// <summary>
        ///     Called whenever a plan controller is closing
        /// </summary>
        /// <param name="controller"></param>
        /// <returns>true if the close may continue</returns>
        [Obsolete("To be removed, should be using ContinueWithPlanContollerClose(IEnumerable<PlanControllerViewModel> controllers)", true)]
        public Boolean ContinueWithPlanContollerClose(PlanControllerViewModel controller)
        {
            Planogram planogram = controller.SourcePlanogram.Model;

            //just return out if the plan has not changed and the product sequence is up to date.
            if (planogram.IsInitialized || !planogram.IsDirty || App.IsUnitTesting) return true;

            IWindowService windowService = CommonHelper.GetWindowService();

            //  If the plan exists prompt the user to accept continuing if the item has changes and save, not save or cancel.
            if (!planogram.Parent.IsReadOnly &&
                !planogram.IsNew)
                return windowService.ContinueWithItemChange(planogram, MainPageCommands.SavePlanogram, controller);

            //  If the plan is readonly or new, prompt the user to save as, not save or cancel.
            String header = planogram.Parent.IsReadOnly ? Message.MainPage_SaveReadOnly_Header : Message.MainPage_SaveNew_Header;
            String description = planogram.Parent.IsReadOnly ? Message.MainPage_SaveReadOnly_Description : Message.MainPage_SaveNew_Description;

            //Show the dialog
            ModalMessageResult result = windowService.ShowMessage(
                MessageWindowType.Warning,
                String.Format(CultureInfo.CurrentCulture, header, planogram.Name),
                description,
                /*buttonCount*/3,
                Message.Generic_Save,
                Message.MainPage_SaveReadOnly_DontSave,
                Message.Generic_Cancel);

            Boolean saveAs = result == ModalMessageResult.Button1;
            Boolean continueWithClose = result == ModalMessageResult.Button2;

            if (!saveAs) return continueWithClose;

            // If we flagged to save as, then take the user to the backstage.
            AttachedControl.xBackstageSaveTab.IsSelected = true;
            AttachedControl.SetRibbonBackstageState(true);
            return continueWithClose;
        }

        /// <summary>
        ///     Prompt the user to continue or not with changes. When continuing, the user may decide to either save all or none of the planograms.
        /// </summary>
        /// <param name="controllers"></param>
        /// <returns></returns>
        /// <remarks>   When there are more than one planogram needing saving as only the currently selected 
        /// OR the first one if none was selected will be the active one in the backstage screen.
        /// <para />    Only planograms that can be saved right away will be closed when the user selectes to continue. 
        /// Any that need to be Saved As will remain open.
        /// </remarks>
        private Boolean ContinueWithPlanContollerClose(IEnumerable<PlanControllerViewModel> controllers)
        {
            List<PlanControllerViewModel> planograms = controllers.Where(model => !model.SourcePlanogram.Model.IsInitialized && model.SourcePlanogram.Model.IsDirty).ToList();
            if (!planograms.Any()) return true;

            IWindowService windowService = CommonHelper.GetWindowService();

            String items = String.Join(Environment.NewLine, planograms.Select(planogram => String.Format(@"    {0}", planogram.SourcePlanogram.Model.Name)));
            ModalMessageResult result = windowService.ShowMessage(MessageWindowType.Warning,
                                                                  Ccm.Common.Wpf.Resources.Language.Message.SavePendingChanges_Title,
                                                                  String.Format(Ccm.Common.Wpf.Resources.Language.Message.SavePendingChanges_Description,
                                                                                Environment.NewLine,
                                                                                items), 3, Message.Generic_Save, Message.MainPage_SaveReadOnly_DontSave, Message.Generic_Cancel);
            Boolean save = result == ModalMessageResult.Button1;
            Boolean continueWithClose = result == ModalMessageResult.Button2;

            if (!save) return continueWithClose;

            IEnumerable<PlanControllerViewModel> saveAsPlanograms = planograms.Where(planogram => planogram.SourcePlanogram.Model.IsNew || planogram.SourcePlanogram.Model.Parent.IsReadOnly).ToList();
            IEnumerable<PlanControllerViewModel> existingPlanograms = planograms.Except(saveAsPlanograms);
            foreach (PlanControllerViewModel controller in existingPlanograms)
            {
                Save(controller);
                ClosePlanController(controller);
            }

            if (!saveAsPlanograms.Any()) return continueWithClose;

            // If we at least one of the plans needs to save as, then take the user to the backstage.
            if (!saveAsPlanograms.Contains(ActivePlanController)) ActivePlanController = saveAsPlanograms.First();
            AttachedControl.xBackstageSaveTab.IsSelected = true;
            AttachedControl.SetRibbonBackstageState(true);
            return continueWithClose;
        }

        /// <summary>
        /// Closes all plans currently open in this app.
        /// </summary>
        public void CloseAllPlans()
        {
            foreach (PlanControllerViewModel controller in this.PlanControllers.ToList())
            {
                ClosePlanController(controller);
            }
        }

        /// <summary>
        ///     Clear the temporary folder used to import split files.
        /// </summary>
        private void ClearTempImportFiles()
        {
            if (!Directory.Exists(TempImportFolder)) return;

            var directoryInfo = new DirectoryInfo(TempImportFolder);
            foreach (System.IO.FileInfo fileInfo in directoryInfo.GetFiles())
            {
                fileInfo.Delete();
            }
        }

        #endregion

        #region Mru

        /// <summary>
        ///     Updates the given package view in the mru list.
        /// </summary>
        /// <param name="packageView"></param>
        private void AddToMRU(PackageViewModel packageView)
        {
            base.ShowWaitCursor(true);

            try
            {
                if (packageView.PackageConnectionType == PackageConnectionType.FileSystem)
                {
                    //only add if this was a pog file
                    if (packageView.FileName.EndsWith(Constants.PlanogramFileExtension))
                    {
                        _recentPlanogramsView.AddFromFileSystem(packageView.Model.Name, packageView.FileName);
                    }
                }
                else
                {
                    foreach (var planView in packageView.PlanogramViews)
                    {
                        _recentPlanogramsView.AddFromRepository(planView.Model.Id, planView.Name, packageView.RepositoryFolderPath);
                    }
                }

                _recentPlanogramsView.Save();
            }
            catch (Exception ex)
            {
                //dont worry if this fails - just log it and carry on.
                LocalHelper.RecordException(ex.GetBaseException());
            }

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Removes the corresponding entry from the mru list.
        /// </summary>
        private void RemoveFromMru(RecentPlanogramType type, Object planogramId)
        {
            RecentPlanogram mruEntry = _recentPlanogramsView.Model
                .FirstOrDefault(r => r.Type == type
                && r.PlanogramId == planogramId);

            if (mruEntry == null) return;

            try
            {
                _recentPlanogramsView.Model.Remove(mruEntry);
                _recentPlanogramsView.Save();
            }
            catch (Exception ex)
            {
                //dont worry.
                CommonHelper.RecordException(ex);
            }
        }

        #endregion

        #endregion

        #region PlanogramAppend methods

        private System.Threading.Thread DisplayAppendModal(String description)
        {
            // Create a new thread that will run under the main windows dispatcher
            System.Threading.Thread showThread = new System.Threading.Thread(
                () =>
                {
                    var modalWindow = new ModalBusy()
                    {
                        Header = Message.AppendPlanogram_Working_Header,
                        Description = description,
                        WindowStartupLocation = WindowStartupLocation.CenterScreen,
                    };

                    modalWindow.Show();

                    modalWindow.Closed += (sender2, e2) => modalWindow.Dispatcher.InvokeShutdown();

                    // Allow this thread to process events as both the busy dialog window and 
                    // the task to append the new planogram require WPF rendering.
                    System.Windows.Threading.Dispatcher.Run();
                });

            // Run the thread as a single threaded apartment for WPF
            showThread.SetApartmentState(System.Threading.ApartmentState.STA);
            showThread.Start();

            return showThread;
        }

        /// <summary>
        /// Append a planogram from the repository. Displays the planogram selector window and then
        /// appends the chosen planogram to the right of the currently open plan.
        /// </summary>
        public void AppendPlanogram()
        {
            Int32? planGroupId = null;

            // See if we can find out which planogram group the current planogram belongs to
            var currentPlan = ActivePlanController.SourcePlanogram;

            //PlanogramGroup planGroup = PlanogramGroup.FetchByEntityIdPlanogramId(App.ViewState.EntityId, (Int32) currentPlan.Model.Id);
            //if (planGroup != null)
            //{
            //    planGroupId = planGroup.Id;
            //}

            // Open the plan selection window
            var planSelectViewModel = new PlanogramSelectorViewModel(App.ViewState.CustomColumnLayoutFolderPath, App.ViewState.EntityId, planGroupId);
            CommonHelper.GetWindowService().ShowDialog<PlanogramSelectorWindow>(planSelectViewModel);

            if (planSelectViewModel.DialogResult != true)
            {
                return;
            }

            PlanogramInfo planInfoToAppend = planSelectViewModel.SelectedPlanogramInfo;

            String displayMessage = String.Format(Message.AppendPlanogram_Working_Description, planInfoToAppend.Name);
            var showThread = DisplayAppendModal(displayMessage);

            // Do the actual appending of the planogram
            Boolean appendResult = ActivePlanController.SourcePlanogram.AppendPlanogram(planInfoToAppend);

            // Terminate the thread with the modal busy window as we no longer need to display the working modal dialog
            showThread.Abort();
            showThread.Join();
        }

        /// <summary>
        /// Append a planogram from the file system. Displays the file system selector window and then
        /// appends the chosen planogram file to the right of the currently open plan.
        /// </summary>
        public void AppendPlanogramsFromFileSystem(PlanogramImportTemplate externalFileTemplate = null)
        {
            String[] fileNames = null;
            List<String> unOpenableFiles = new List<String>();
            //Boolean isSilent = false;

            var extensions = new List<String>(
                new[]
                {
                    Constants.PlanogramFileExtension,
                });

            OpenFileDialog dlg = new OpenFileDialog();
            dlg.InitialDirectory = App.ViewState.GetSessionDirectory(SessionDirectory.Planogram);
            dlg.Filter = Message.MainPage_OpenFileDialogFilter;
            dlg.Multiselect = false;

            if (dlg.ShowDialog(AttachedControl) == true)
            {
                if (!extensions.Any(s => String.Equals(Path.GetExtension(dlg.FileName), s, StringComparison.OrdinalIgnoreCase)))
                {
                    CommonHelper.GetWindowService().ShowOkMessage(
                        MessageWindowType.Warning,
                        Message.General_WrongFileType_Title,
                        Message.General_WrongFileType_Description);
                }
                else
                {
                    fileNames = dlg.FileNames.ToArray();
                    String firstFileName = fileNames.FirstOrDefault();

                    if (!String.IsNullOrEmpty(firstFileName))
                    {
                        App.ViewState.SetSessionDirectory(SessionDirectory.Planogram, Path.GetDirectoryName(firstFileName));
                    }
                }
            }

            //  Check that there are files to open.
            if (fileNames == null || !fileNames.Any())
            {
                return;
            }

            // Create the requests
            List<PackageFetchRequest> requests = new List<PackageFetchRequest>();
            foreach (String fileName in fileNames)
            {
                //  If the file is a POG file, just add the new request and continue on to the next file.
                if (fileName.EndsWith(Constants.PlanogramFileExtension, StringComparison.OrdinalIgnoreCase))
                {
                    requests.Add(PackageFetchRequest.NewPogFileRequest(fileName, Path.GetFileName(fileName)));
                    continue;
                }

                //  Process other file extensions.
                PlanogramImportFileType fileType = PlanogramImportFileTypeHelper.GetTypeFromExtension(Path.GetExtension(fileName));

                lock (_silentLock)
                {
                    //If we are running silent then use the last silent template we had.
                    if ((externalFileTemplate == null || externalFileTemplate.FileType != fileType))
                    {
                        switch (fileType)
                        {
                            case PlanogramImportFileType.SpacemanV9:
                                externalFileTemplate = _silentSpacemanTemplate;
                                break;
                            case PlanogramImportFileType.ProSpace:
                                externalFileTemplate = _silentProspaceTemplate;
                                break;
                            case PlanogramImportFileType.Apollo:
                                externalFileTemplate = _silentApolloTemplate;
                                break;
                        }
                    }

                    if (externalFileTemplate == null ||
                        externalFileTemplate.FileType != fileType)
                    {
                        String defaultFileTemplate;
                        switch (fileType)
                        {
                            case PlanogramImportFileType.SpacemanV9:
                                defaultFileTemplate = App.ViewState.Settings.Model.DefaultSpacemanFileTemplate;
                                break;
                            case PlanogramImportFileType.ProSpace:
                                defaultFileTemplate = App.ViewState.Settings.Model.DefaultProspaceFileTemplate;
                                break;
                            case PlanogramImportFileType.Apollo:
                                defaultFileTemplate = App.ViewState.Settings.Model.DefaultApolloFileTemplate;
                                break;
                            default:
                                //  Don't add the request as it is for an unknown file type.
                                Debug.Fail("Unknown file type when calling OpenPlanogramsFromFileSystem()");
                                continue;
                        }

                        externalFileTemplate = RequestPlanogramFileTemplate(defaultFileTemplate, fileType, !String.IsNullOrWhiteSpace(defaultFileTemplate));
                    }

                    //  If there is no external file template at this point, the user must have cancelled so stop processing.
                    if (externalFileTemplate == null) return;
                }

                //  Account for multi planogram packages in JDA Space Planning files.
                if (fileType == PlanogramImportFileType.ProSpace)
                {
                    String packageIdMask = String.Format("{0}::{{0}}", fileName);
                    PlanogramImportTemplate template = externalFileTemplate;
                    //  Create a unique request per planogram in the file.
                    IEnumerable<PackageFetchRequest> psaPlanogramRequests =
                        FileHelper.GetPsaPlanogramsFromFile(fileName)
                                  .Select(planName =>
                                          {
                                              String packageId = String.Format(packageIdMask, planName);
                                              return PackageFetchRequest.NewExternalFileRequest(packageId, planName, template);
                                          }).ToList();
                    // NB This should not be necessary as files are now opened in readonly mode and should always be available.
                    //Check we actually created a request
                    if (!psaPlanogramRequests.Any())
                    {
                        //no planograms could be added as the file is locked 
                        unOpenableFiles.Add(fileName);
                    }
                    else
                    {
                        requests.AddRange(psaPlanogramRequests);
                    }
                }
                else
                {
                    requests.Add(PackageFetchRequest.NewExternalFileRequest(fileName, Path.GetFileName(fileName), externalFileTemplate));
                }
            }

            if (unOpenableFiles.Count > 0)
            {
                // Create a message to inform user of any files that could not be opened
                StringBuilder msgDescription = new StringBuilder(Message.General_CannotOpenLockedFile_Description);
                msgDescription.AppendLine();
                msgDescription.AppendLine();
                foreach (String fileName in unOpenableFiles)
                {
                    msgDescription.AppendLine(fileName);
                }

                //Show the message
                CommonHelper.GetWindowService().ShowOkMessage(
                            MessageWindowType.Warning,
                            Message.General_CannotOpenLockedFile_Title,
                            msgDescription.ToString());
            }

            String displayMessage = String.Format(Message.AppendPlanogram_Working_Description, requests.First().RequestId.ToString());
            var displayThread = DisplayAppendModal(displayMessage);

            // Append the file as requested by the user
            PackageViewModel packageView = null;
            foreach (var requestItem in requests)
            {
                var fileName = requestItem.RequestId as String;
                if (!String.IsNullOrEmpty(fileName))
                {
                    try
                    {
                        if (requestItem.IsExternalFileType)
                        {
                            packageView = PackageViewModel.ImportPackageByFileName(fileName, requestItem.RequestImportTemplate);
                        }
                        else
                        {
                            packageView = PackageViewModel.FetchPackageByFileName(fileName);
                        }
                    }
                    catch (Exception ex)
                    {
                        //package load failed for some reason.
                        //make sure that the package does not get stuck locked
                        Package.TryUnlockPackageById(PackageType.FileSystem, fileName, DomainPrincipal.CurrentUserId, PackageLockType.User);
                        throw ex;
                    }
                }

                // And append the plans in the package
                foreach (var planViewItem in packageView.PlanogramViews)
                {
                    var planViewModel = planViewItem.Model;

                    // Do the actual appending of the planogram
                    Boolean appendResult = ActivePlanController.SourcePlanogram.AppendPlanogram(planViewModel);
                }
            }

            displayThread.Abort();
            displayThread.Join();
        }

        #endregion

        #region PlanogramUpdateFromProductAttribute methods

        public void UpdatePlanogramFromProductAttribute()
        {
            UpdatePlanogramsFromProductAttributes(new List<PlanogramView> { ActivePlanController.SourcePlanogram });
        }

        public void UpdateAllPlanogramsFromProductAttribute()
        {
            UpdatePlanogramsFromProductAttributes(PlanControllers.Select(model => model.SourcePlanogram));
        }

        private static void UpdatePlanogramsFromProductAttributes(IEnumerable<PlanogramView> planogramViews)
        {
            List<ProductAttributeUpdateValue> updateData = PrompUserForPropertyAttributeUpdateData();
            if (updateData == null) return;

            foreach (PlanogramView view in planogramViews)
            {
                using (PlanogramView.PlanogramUndoableAction.New(view, "Update Planogram Product Attributes", true))
                    view.Model.UpdateProductAttributes(updateData);
            }
        }

        private static List<ProductAttributeUpdateValue> PrompUserForPropertyAttributeUpdateData()
        {
            var viewModel = new ProductAttributeUpdateSelectorViewModel();
            CommonHelper.GetWindowService().ShowDialog<ProductAttributeUpdateSelectorWindow>(viewModel);
            return viewModel.DialogResult == true ? viewModel.SelectedAttributes.ToList() : null;
        }

        #endregion

        #region Fixture Update X Position methods

        public void FixtureUpdateXPositionsWithGaps()
        {
            //Do we have an active and selected plan 
            if (this.ActivePlanController != null && this.ActivePlanController.SelectedPlanDocument != null)
            {
                //Can we re calculate the fixture X Positions
                bool fixtureCalculatePositions = this.ActivePlanController.SelectedPlanDocument.Planogram.Model.CanReCalculateFixtureXPositions();
                Dictionary<PlanogramFixtureItem, float> fixtureSpacing = new Dictionary<PlanogramFixtureItem, float>();

                if (fixtureCalculatePositions)
                {
                    //Get the current spacing between each fixture
                    fixtureSpacing = this.ActivePlanController.SelectedPlanDocument.Planogram.Model.RenumberingStrategy.GetFixtureSpacing();
                }

                if (fixtureCalculatePositions)
                {
                    //Recalculate the fixture X positions including fixture spacings
                    this.ActivePlanController.SelectedPlanDocument.Planogram.Model.RecalculateFixtureXPositions(fixtureSpacing, true);
                }
                else
                {
                    //Continue with the normalise bay function if it cannot be recalculated
                    this.ActivePlanController.SelectedPlanDocument.Planogram.Model.NormalizeBays();
                }

                //Force the renumbering strategy
                this.ActivePlanController.SelectedPlanDocument.Planogram.Model.RenumberingStrategy.RenumberBays(forced: true);
            }
        }

        public void FixtureUpdateXPositionsWithoutGaps()
        {
            //Do we have an active and selected plan 
            if (this.ActivePlanController != null && this.ActivePlanController.SelectedPlanDocument != null)
            {
                //Can we re calculate the fixture X Positions
                bool fixtureCalculatePositions = this.ActivePlanController.SelectedPlanDocument.Planogram.Model.CanReCalculateFixtureXPositions();
                Dictionary<PlanogramFixtureItem, float> fixtureSpacing = new Dictionary<PlanogramFixtureItem, float>();

                if (fixtureCalculatePositions)
                {
                    //Get the current spacing between each fixture
                    fixtureSpacing = this.ActivePlanController.SelectedPlanDocument.Planogram.Model.RenumberingStrategy.GetFixtureSpacing();
                }

                if (fixtureCalculatePositions)
                {
                    //Recalculate the fixture X positions excluding fixture spacings
                    this.ActivePlanController.SelectedPlanDocument.Planogram.Model.RecalculateFixtureXPositions(fixtureSpacing, false);
                }
                else
                {
                    //Continue with the normalise bay function if it cannot be recalculated
                    this.ActivePlanController.SelectedPlanDocument.Planogram.Model.NormalizeBays();
                }

                //Force the renumbering strategy
                this.ActivePlanController.SelectedPlanDocument.Planogram.Model.RenumberingStrategy.RenumberBays(forced: true);
            }
        }
        
        #endregion

        #region Labels

        /// <summary>
        ///    Updates the collection of available labels
        /// </summary>
        public void RefreshAvailableLabels()
        {
            if (_availableProductLabels.Count > 0) _availableProductLabels.Clear();
            if (_availableFixtureLabels.Count > 0) _availableFixtureLabels.Clear();

            List<LabelInfo> recentProductLabelInfos = new List<LabelInfo>();
            List<LabelInfo> recentFixtureLabelInfos = new List<LabelInfo>();
            foreach (UserEditorSettingsRecentLabel recentLabel in App.ViewState.Settings.Model.RecentLabels)
            {
                if (recentLabel.SaveType == LabelDalFactoryType.Unknown && !App.ViewState.IsConnectedToRepository)
                {
                    continue;
                }

                if (recentLabel.LabelType == LabelType.Product)
                {
                    LabelInfo recentLabelInfo = LabelInfoList.FetchByIds(new List<Object>() { recentLabel.LabelId }, recentLabel.SaveType).FirstOrDefault();
                    if (recentLabelInfo == null) continue;
                    recentProductLabelInfos.Add(recentLabelInfo);
                }
                else if (recentLabel.LabelType == LabelType.Fixture)
                {
                    LabelInfo recentLabelInfo = LabelInfoList.FetchByIds(new List<object>() { recentLabel.LabelId }, recentLabel.SaveType).FirstOrDefault();
                    if (recentLabelInfo == null) continue;
                    recentFixtureLabelInfos.Add(recentLabelInfo);
                }
            }
            _availableProductLabels.AddRange(recentProductLabelInfos.OrderBy(l => l.Name));
            _availableFixtureLabels.AddRange(recentFixtureLabelInfos.OrderBy(l => l.Name));

        }

        public void SetFixtureLabel(LabelInfo info)
        {

            //just clear the item if nothing was passed.
            if (info == null)
            {
                ClearFixtureLabel();
                return;
            }

            base.ShowWaitCursor(true);

            LabelItem labelItem = null;
            try
            {
                using (Label label =LabelUIHelper.FetchLabelAsReadonly(info.Id))
                {
                    labelItem = new LabelItem(label);
                }
            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);

                LocalHelper.RecordException(ex);
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(info.Name, OperationType.Open);
            }

            base.ShowWaitCursor(false);


            if (labelItem != null)
            {
                SetFixtureLabel(labelItem);
            }
        }

        /// <summary>
        ///     Sets the given label against the active document.
        /// </summary>
        /// <param name="setting"></param>
        public void SetFixtureLabel(LabelItem setting)
        {
            SetFixtureLabel(setting, false);
        }

        public void SetFixtureLabel(LabelItem setting, Boolean isSetToAll)
        {
            Boolean isCustom = (setting != null && setting.Name == "");

            //Add to mru
            if (setting != null && setting.Id != null && !isCustom)
            {
                App.ViewState.Settings.Model.RecentLabels.AddLabelId(setting.Id, setting.Name, setting.Type);
            }

            if (isSetToAll)
            {
                foreach (var controller in PlanControllers)
                {
                    foreach (var doc in controller.PlanDocuments)
                    {
                        if (!doc.ArePlanogramViewSettingsSupported) continue;

                        if (isCustom) doc.CustomFixtureLabel = setting;
                        doc.FixtureLabel = setting;
                    }
                }
            }
            else if (ActivePlanController != null
                    && ActivePlanController.SelectedPlanDocument != null)
            {

                PlanControllerViewModel controller = ActivePlanController;
                IPlanDocument doc = ActivePlanController.SelectedPlanDocument;

                if (doc.ArePlanogramViewSettingsSupported)
                {
                    if (isCustom) doc.CustomFixtureLabel = setting;
                    doc.FixtureLabel = setting;
                }
            }
        }

        public void ClearFixtureLabel()
        {
            SetFixtureLabel(null, false);
        }

        public void ToggleFixtureLabelSet()
        {
            IPlanDocument doc = ActivePlanController.SelectedPlanDocument;
            if (doc == null) return;

            ///store temp label
            if (doc.ToggleFixtureLabelItem == null)
            {
                //Not currently hidden so store a temp copy
                doc.ToggleFixtureLabelItem = doc.FixtureLabel;

                //Then remove actual label
                SetFixtureLabel(null, false);
            }
            else if (doc.FixtureLabel == null)
            {
                //currently hidden so load temp copy then null of the temp
                SetFixtureLabel(doc.ToggleFixtureLabelItem, false);
                doc.ToggleFixtureLabelItem = null;
            }
            else
            {
                //If we reach here then a new highlight has been applied. refresh the toggle placeholder.
                doc.ToggleFixtureLabelItem = null;
            }
        }

        public void ToggleProductLabelSet()
        {
            IPlanDocument doc = ActivePlanController.SelectedPlanDocument;
            if (doc == null) return;

            ///store temp label
            if (doc.ToggleProductLabelItem == null)
            {
                //Not currently hidden so store a temp copy
                doc.ToggleProductLabelItem = doc.ProductLabel;

                //Then remove actual label
                SetProductLabel(null, false);
            }
            else if (doc.ProductLabel == null)
            {
                //currently hidden so load temp copy then null of the temp
                SetProductLabel(doc.ToggleProductLabelItem, false);
                doc.ToggleProductLabelItem = null;
            }
            else
            {
                //If we reach here then a new highlight has been applied. refresh the toggle placeholder.
                doc.ToggleProductLabelItem = null;
            }
        }


        public void ShowFixtureLabelEditor()
        {
            LabelItem currentSetting = null;
            PlanogramView currentPlan = null;

            if (ActivePlanController != null
                && ActivePlanController.SelectedPlanDocument != null)
            {
                currentPlan = ActivePlanController.SourcePlanogram;
                currentSetting = ActivePlanController.SelectedPlanDocument.FixtureLabel;
            }

            //Show the dialog.
            FixtureLabelEditorViewModel winViewModel =
                new FixtureLabelEditorViewModel(currentSetting, currentPlan);
            winViewModel.SetRequested += FixtureLabelEditor_SetLabelRequested;

            CommonHelper.GetWindowService().ShowDialog<FixtureLabelEditorOrganiser>(winViewModel);

            winViewModel.SetRequested -= FixtureLabelEditor_SetLabelRequested;

            //update the main label collection
            RefreshAvailableLabels();

        }

        private void FixtureLabelEditor_SetLabelRequested(object sender, FixtureLabelEditorEventArgs e)
        {
            SetFixtureLabel(e.LabelView, e.IsSetToAll);
        }



        public void SetProductLabel(LabelInfo info)
        {
            //just clear the item if nothing was passed.
            if (info == null)
            {
                ClearProductLabel();
                return;
            }

            base.ShowWaitCursor(true);

            LabelItem labelItem = null;
            try
            {
                using (Label label = LabelUIHelper.FetchLabelAsReadonly(info.Id))
                {
                    labelItem = new LabelItem(label);
                }
            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);

                LocalHelper.RecordException(ex);
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(info.Name, OperationType.Open);
            }

            base.ShowWaitCursor(false);


            if (labelItem != null)
            {
                SetProductLabel(labelItem);
            }
        }

        /// <summary>
        ///     Sets the given label against the active document.
        /// </summary>
        /// <param name="setting"></param>
        public void SetProductLabel(LabelItem setting)
        {
            SetProductLabel(setting, false);
        }

        public void SetProductLabel(LabelItem setting, Boolean isSetToAll)
        {
            Boolean isCustom = (setting != null && setting.Name == "");

            if (setting != null && setting.Id != null && !isCustom)
            {
                App.ViewState.Settings.Model.RecentLabels.AddLabelId(setting.Id, setting.Name, setting.Type);
            }


            if (isSetToAll)
            {
                foreach (var controller in PlanControllers)
                {
                    foreach (var doc in controller.PlanDocuments)
                    {
                        if (!doc.ArePlanogramViewSettingsSupported) continue;

                        if (isCustom) doc.CustomProductLabel = setting;
                        doc.ProductLabel = setting;
                    }
                }
            }
            else if (ActivePlanController != null
                    && ActivePlanController.SelectedPlanDocument != null)
            {

                PlanControllerViewModel controller = ActivePlanController;
                IPlanDocument doc = ActivePlanController.SelectedPlanDocument;

                if (doc.ArePlanogramViewSettingsSupported)
                {
                    if (isCustom) doc.CustomProductLabel = setting;
                    doc.ProductLabel = setting;
                }
            }
        }

        public void ClearProductLabel()
        {
            SetProductLabel(null, false);
        }

        public void ShowProductLabelEditor()
        {
            LabelItem currentSetting = null;
            PlanogramView currentPlan = null;

            if (ActivePlanController != null
                && ActivePlanController.SelectedPlanDocument != null)
            {
                currentPlan = ActivePlanController.SourcePlanogram;
                currentSetting = ActivePlanController.SelectedPlanDocument.ProductLabel;
            }

            ProductLabelEditorViewModel viewModel = new ProductLabelEditorViewModel(currentSetting, currentPlan);
            viewModel.SetRequested += ProductLabelEditor_SetLabelRequested;

            CommonHelper.GetWindowService().ShowDialog<ProductLabelEditorOrganiser>(viewModel);
            viewModel.SetRequested -= ProductLabelEditor_SetLabelRequested;

            //update the main label collection
            RefreshAvailableLabels();

        }

        private void ProductLabelEditor_SetLabelRequested(object sender, ProductLabelEditorEventArgs e)
        {
            SetProductLabel(e.LabelView, e.IsSetToAll);
        }


        #endregion

        #region Highlights

        public void RefreshAvailableHighlights()
        {
            if (_availableHighlights.Count > 0) _availableHighlights.Clear();
            List<HighlightInfo> recentHighlightInfos = new List<HighlightInfo>();
            foreach (UserEditorSettingsRecentHighlight recentHighlight in App.ViewState.Settings.Model.RecentHighlight)
            {
                if (recentHighlight.SaveType == HighlightDalFactoryType.Unknown && !App.ViewState.IsConnectedToRepository)
                {
                    continue;
                }

                if (recentHighlight.SaveType.Equals(HighlightDalFactoryType.Unknown))
                {
                    HighlightInfo recentHighlightInfo = HighlightInfoList.FetchByIds(new List<Object>() { recentHighlight.HighlightId }).FirstOrDefault();
                    if (recentHighlightInfo == null) continue;
                    recentHighlightInfos.Add(recentHighlightInfo);
                }
                else if (recentHighlight.SaveType.Equals(HighlightDalFactoryType.FileSystem))
                {
                    HighlightInfo recentHighlightInfo = HighlightInfoList.FetchByFileNames(new List<Object>() { recentHighlight.HighlightId }).FirstOrDefault();
                    if (recentHighlightInfo == null) continue;
                    recentHighlightInfos.Add(recentHighlightInfo);
                }
            }


            _availableHighlights.AddRange(recentHighlightInfos.OrderBy(l => l.Name));
        }


        public void ToggleHighlightSet()
        {
            IPlanDocument doc = ActivePlanController.SelectedPlanDocument;
            if (doc == null) return;



            ///store temp highlight
            if (doc.ToggleHighlightItem == null)
            {
                //Not currently hidden so store a temp copy
                doc.ToggleHighlightItem = doc.Highlight;

                //Then remove actual highlight
                SetHighlight(null, false);
            }
            else if (doc.Highlight == null)
            {
                //currently hidden and no highlight set so load temp copy then null of the temp
                SetHighlight(doc.ToggleHighlightItem, false);
                doc.ToggleHighlightItem = null;
            }
            else
            {
                //If we reach here then a new highlight has been applied. refresh the toggle placeholder.
                doc.ToggleHighlightItem = null;
            }
        }

        public void SetHighlight(HighlightInfo info)
        {
            //just clear the highlight if nothing was passed.
            if (info == null)
            {
                ClearHighlight();
                return;
            }

            base.ShowWaitCursor(true);

            HighlightItem highlightItem = null;
            try
            {
                using (Highlight highlight = HighlightUIHelper.FetchHighlightAsReadonly(info.Id))
                {
                    highlightItem = new HighlightItem(highlight);
                }
            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);

                LocalHelper.RecordException(ex);
                Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(info.Name, OperationType.Open);
            }

            base.ShowWaitCursor(false);


            if (highlightItem != null)
            {
                SetHighlight(highlightItem);
            }
        }

        public void SetHighlight(HighlightItem setting)
        {
            SetHighlight(setting, false);
        }

        public void SetHighlight(HighlightItem setting, Boolean isSetToAll)
        {
            Boolean shouldShowLegend = (setting != null);

            //TODO: Use a flag instead of the name.
            Boolean isCustom = (setting != null && setting.Name == "");

            //Add the highlight id to the mru list.
            if (setting != null && setting.Id != null && !isCustom)
            {
                App.ViewState.Settings.Model.RecentHighlight.AddHighlightId(setting.Id);
            }

            if (isSetToAll)
            {
                foreach (var controller in PlanControllers)
                {
                    foreach (var doc in controller.PlanDocuments)
                    {
                        if (!doc.IsHighlightingSupported) continue;

                        doc.Highlight = setting;
                        doc.IsHighlightLegendVisible = shouldShowLegend;
                        if (isCustom) doc.CustomHighlight = setting;
                    }
                }
            }
            else if (ActivePlanController != null
                    && ActivePlanController.SelectedPlanDocument != null)
            {
                IPlanDocument doc = ActivePlanController.SelectedPlanDocument;
                if (doc.IsHighlightingSupported)
                {
                    doc.Highlight = setting;
                    doc.IsHighlightLegendVisible = shouldShowLegend;

                    if (isCustom) doc.CustomHighlight = setting;
                }
            }
        }

        public void ClearHighlight()
        {
            SetHighlight(null, false);
        }

        public void ShowHighlightEditor()
        {
            HighlightItem currentSetting = null;
            PlanogramView currentPlan = null;

            //Boolean showSetButton = false;
            //Boolean showSetAllButton = false;

            if (ActivePlanController != null
                && ActivePlanController.SelectedPlanDocument != null)
            {
                currentPlan = ActivePlanController.SourcePlanogram;
                //showSetButton = ActivePlanController.SelectedPlanDocument.IsHighlightingSupported;
                currentSetting = ActivePlanController.SelectedPlanDocument.Highlight;
            }
            //foreach (var controller in PlanControllers)
            //{
            //    if (controller.PlanDocuments.Any(p => p.IsHighlightingSupported))
            //    {
            //        showSetAllButton = true;
            //        break;
            //    }
            //}

            HighlightEditorViewModel vm = new HighlightEditorViewModel(currentSetting, currentPlan);
            //vm.ShowSetButton = showSetButton;
            //vm.ShowSetAllButton = showSetAllButton;

            vm.SetRequested += HighlightEditor_SetRequested;

            CommonHelper.GetWindowService().ShowDialog<HighlightEditorWindow>(vm);

            vm.SetRequested -= HighlightEditor_SetRequested;

            //update the main collection
            RefreshAvailableHighlights();
        }

        private void HighlightEditor_SetRequested(object sender, HighlightEditorEventArgs e)
        {
            SetHighlight(e.HighlightView, e.IsSetToAll);
        }

        /// <summary>
        /// Applies the current highlight to the plan by updating its product colours to match.
        /// </summary>
        public void ApplyHighlightAsProductColour(Boolean selectedProductsOnly)
        {
            // Show the loading cursor and get the highlight of the current document.
            base.ShowWaitCursor(true);
            IPlanDocument activeDocumentWithHighlight = ActivePlanController.PlanDocuments
                .FirstOrDefault(d => d.IsActiveDocument && d.IsHighlightingSupported && d.Highlight != null);
            if (activeDocumentWithHighlight == null) return;

            IEnumerable<PlanogramProduct> selectedProducts =
                selectedProductsOnly ?
                activeDocumentWithHighlight.SelectedPlanItems.Where(i => i.Position != null).Select(p => p.Product.Model) :
                new List<PlanogramProduct>();

            _isApplyHighlightAsProductColour = true;

            // Apply the highlight and hide the loading cursor.
            IEnumerable<PlanogramProduct> productsUpdated =
                ActivePlanController.SourcePlanogram.Model.UpdateProductColoursFromHighlight(
                    activeDocumentWithHighlight.Highlight,
                    selectedProducts.Any() ? selectedProducts : null);
            base.ShowWaitCursor(false);

            // Notify the user of the update.
            Galleria.Ccm.Services.ServiceContainer.GetService<IWindowService>().ShowOkMessage(
                MessageWindowType.Info,
                Message.Ribbon_ApplyHighlightAsProductColour_MessageTitle,
                String.Format(Message.Ribbon_ApplyHighlightAsProductColour_MessageDescription, productsUpdated.Count(), activeDocumentWithHighlight.Highlight.Name));

            _isApplyHighlightAsProductColour = false;
        }

        #endregion

        #region DataSheets

        public void SetCustomDataSheet()
        {
            if (ActivePlanController.SelectedPlanDocument.DocumentType == DocumentType.DataSheet)
            {
                ReportDocument report = (ReportDocument)ActivePlanController.SelectedPlanDocument;

                if (String.Equals(report.Layout.Name, CustomDataSheet.Name))
                {
                    report.LoadLayout(this.CustomDataSheet);
                }
            }
            else
            {
                AddReportDocument(CustomDataSheet);
            }
        }

        public void AddReportDocument(CustomColumnLayout layout)
        {
            if (layout.IsDirty)
            {
                layout.Name = Message.DataSheet_CustomDataSheet;
                CustomDataSheet = layout;
            }
            else
            {
                AddFavouriteDataSheet(layout);
            }
            ReportDocument report = (ReportDocument)this.ActivePlanController.AddNewDocument(DocumentType.DataSheet, /*checkForExisting*/false);
            report.LoadLayout(layout);
        }

        /// <summary>
        ///     Add a new favourite to the list and bump down the others
        /// </summary>
        public void AddFavouriteDataSheet(CustomColumnLayout layout)
        {
            if (!App.ViewState.Settings.Model.RecentDatasheet.Any(d => Object.Equals(d.DatasheetId, layout.Id)))
            {
                App.ViewState.Settings.Model.RecentDatasheet.Add(layout);
                RefreshFavouriteDataSheets();
            }
        }

        /// <summary>
        ///     Updates the collection of available DataSheets
        /// </summary>
        private void RefreshFavouriteDataSheets()
        {
            UserEditorSettings settings = App.ViewState.Settings.Model;
            IEnumerable<Object> favouriteIds = settings.RecentDatasheet.Select(d => d.DatasheetId).Distinct();

            List<CustomColumnLayout> favourites = new List<CustomColumnLayout>();

            foreach (Object id in favouriteIds)
            {
                if (String.IsNullOrEmpty(Convert.ToString(id))) continue;


                CustomColumnLayout favItem = _availableDataSheets.FirstOrDefault(d => Object.Equals(d.Id, id));
                if (favItem == null)
                {
                    try
                    {
                        favItem = CustomColumnLayout.FetchByFilename(Convert.ToString(id), /*asReadOnly*/true);
                    }
                    catch (DataPortalException) { }
                }
                if (favItem != null) favourites.Add(favItem);
            }

            //update the main collection.
            if (_availableDataSheets.Count > 0) _availableDataSheets.Clear();
            _availableDataSheets.AddRange(favourites);

        }

        #endregion

        #region Product Library / Product Universe

        //TODO: Move into product library viewmodel.

        /// <summary>
        ///     Method to handle the effects of the library panel being made visible or not
        /// </summary>
        private void OnIsLibraryPanelVisibleChanged()
        {
            //Only trigger if set and nothing is already selected
            if (IsLibraryPanelVisible && SelectedProductLibrary == null && SelectedProductUniverseInfo == null)
            {
                //If default product library is set
                if (_appSettingsView.Model.DefaultProductLibrarySource != null &&
                    !String.IsNullOrEmpty(_appSettingsView.Model.DefaultProductLibrarySource))
                {
                    //Get type from system settings
                    var sourceType = _appSettingsView.Model.DefaultProductLibrarySourceType;

                    if (sourceType == DefaultProductLibrarySourceType.ProductLibrary)
                    {
                        //Trigger load of product library
                        SelectedProductLibrary = _appSettingsView.Model.DefaultProductLibrarySource;
                    }
                    else if (sourceType == DefaultProductLibrarySourceType.ProductUniverse
                        && App.ViewState.IsConnectedToRepository)
                    {
                        ProductUniverseInfo infoToSelect = null;

                        try
                        {
                            ProductUniverseInfoList universeInfoList =
                                ProductUniverseInfoList.FetchByEntityId(App.ViewState.EntityId);

                            infoToSelect = universeInfoList.FirstOrDefault(
                                p => p.ToString().Equals(_appSettingsView.Model.DefaultProductLibrarySource));
                        }
                        catch (Exception ex)
                        {
                            //just record
                            LocalHelper.RecordException(ex.GetBaseException());
                        }

                        //Trigger load of product universe
                        this.SelectedProductUniverseInfo = infoToSelect;
                    }
                }
            }
        }

        /// <summary>
        ///     Method to handle the effects ofthe selected product library being made visible
        /// </summary>
        private void OnIsSelectedProductLibraryDefaultSourceChanged()
        {
            UserEditorSettings userSettings = _appSettingsView.Model;

            if (SelectedProductLibrary != null)
            {
                if (IsSelectedProductLibraryDefaultSource)
                {
                    //Update system settings
                    userSettings.DefaultProductLibrarySource = SelectedProductLibrary;
                    userSettings.DefaultProductLibrarySourceType = DefaultProductLibrarySourceType.ProductLibrary;
                }
                else
                {
                    userSettings.DefaultProductLibrarySource = String.Empty;
                    userSettings.DefaultProductLibrarySourceType = DefaultProductLibrarySourceType.Unknown;
                }
            }
            else if (SelectedProductUniverseInfo != null)
            {
                if (IsSelectedProductLibraryDefaultSource)
                {
                    //Update system settings
                    userSettings.DefaultProductLibrarySource = SelectedProductUniverseInfo.ToString();
                    userSettings.DefaultProductLibrarySourceType = DefaultProductLibrarySourceType.ProductUniverse;
                }
                else
                {
                    userSettings.DefaultProductLibrarySource = String.Empty;
                    userSettings.DefaultProductLibrarySourceType = DefaultProductLibrarySourceType.Unknown;
                }
            }
            userSettings.Save();
        }

        /// <summary>
        ///     Method to handle the effects of the selected product library changing
        /// </summary>
        private void OnSelectedProductLibraryChanged()
        {
            if (SelectedProductLibrary != null)
            {
                //update the source file name.
                App.ViewState.ProductLibraryView.DataSourceName = this.SelectedProductLibrary;

                //Clean product universe selection 
                if (SelectedProductUniverseInfo != null)
                {
                    SelectedProductUniverseInfo = null;
                }


                //If the selection has changed
                if (_productLibraryView.Model == null || !_productLibraryView.Model.Id.Equals(SelectedProductLibrary))
                {
                    //Lookup POGPL file
                    Object productLibraryId =
                        ProductLibraryHelper.ResolveProductLibraryIdFromFilePath(SelectedProductLibrary);

                    //try to load the library
                    Boolean isLibrarySet = _productLibraryView.SetLoadedProductLibrary(productLibraryId);

                    //update the is default flag
                    if (SelectedProductLibrary != null)
                    {
                        //Check whether is default
                        if (_appSettingsView.Model.DefaultProductLibrarySource.Equals(SelectedProductLibrary))
                        {
                            //Silent set
                            _isSelectedProductLibrarySourceDefault = true;
                        }
                        else
                        {
                            _isSelectedProductLibrarySourceDefault = false;
                        }
                    }
                    else
                    {
                        _isSelectedProductLibrarySourceDefault = false;
                    }
                    OnPropertyChanged(IsSelectedProductLibraryDefaultSourceProperty);



                    if (isLibrarySet)
                    {
                        //the loaded library is not valid for the datasource
                        // so show the maintenance form immediately.
                        if (!_productLibraryView.IsModelValidForDataSource())
                        {
                            MainPageCommands.ProductLibraryMaintenance.Execute(SelectedProductLibrary);

                            if (this.AttachedControl != null)
                            {
                                //get the window that just opened
                                var win = Framework.Controls.Wpf.Helpers.GetWindow<ProductLibraryMaintenanceOrganiser>();
                                if (win != null)
                                {
                                    win.Closed +=
                                        (s, e) =>
                                        {
                                            //if the window is closed
                                            if (!_productLibraryView.SetLoadedProductLibrary(productLibraryId.ToString()))
                                            {
                                                //Clear selected library
                                                SelectedProductLibrary = null;
                                            }
                                        };
                                }
                            }
                        }
                    }



                    //if the library could not be loaded then:
                    else
                    {
                        //Missing template error display
                        if (AttachedControl != null)
                        {
                            var msg = new ModalMessage()
                            {
                                Title = Message.Ribbon_ProductLibraryContext_LoadProductLibrary,
                                Header = Message.ProductLibrary_MissingTemplate,
                                Description = String.Format(Message.ProductLibrary_MissingTemplate_Description, SelectedProductLibrary),
                                ButtonCount = 1,
                                Button1Content = Message.Generic_OK,
                                DefaultButton = ModalMessageButton.Button1,
                                MessageIcon = ImageResources.Warning_32
                            };


                            //Open
                            MainPageCommands.ProductLibraryMaintenance.Execute(SelectedProductLibrary);

                            //get the window that just opened
                            var win = Framework.Controls.Wpf.Helpers.GetWindow<ProductLibraryMaintenanceOrganiser>();
                            if (win != null)
                            {
                                win.Closed +=
                                    (s, e) =>
                                    {
                                        //if the window is closed
                                        if (!_productLibraryView.SetLoadedProductLibrary(productLibraryId.ToString()))
                                        {
                                            //Clear selected library
                                            SelectedProductLibrary = null;
                                        }
                                    };
                            }
                            else
                            {
                                //Clear selected library
                                SelectedProductLibrary = null;
                            }
                        }
                        else
                        {
                            //Clear selected library
                            SelectedProductLibrary = null;
                        }
                    }
                }

            }
            else
            {
                //Reset stats and status
                _isSelectedProductLibrarySourceDefault = false;
                OnPropertyChanged(IsSelectedProductLibraryDefaultSourceProperty);
                StatusBarText = String.Empty;

                //null off the current library model.
                if (_productLibraryView.Model != null)
                {
                    _productLibraryView.Model = null;
                }

            }
        }


        /// <summary>
        ///     Method to handle the effects of the selected product universe changing
        /// </summary>
        private void OnSelectedProductUniverseInfoChanged()
        {
            var libraryPanelView = ProductLibraryPanelViewModel;

            if (SelectedProductUniverseInfo != null)
            {
                //Clean product library selection 
                if (SelectedProductLibrary != null)
                {
                    SelectedProductLibrary = null;
                }

                //If panel is setup
                if (libraryPanelView != null)
                {
                    //If the selection has changed
                    if (libraryPanelView.CurrentProductUniverse == null ||
                        !libraryPanelView.CurrentProductUniverse.Id.Equals(SelectedProductUniverseInfo.Id))
                    {
                        //Load the full product library details into the panel
                        libraryPanelView.CurrentProductUniverse =
                            ProductUniverse.FetchById(SelectedProductUniverseInfo.Id);

                        if (SelectedProductUniverseInfo != null)
                        {
                            //Check whether is default
                            if (
                                _appSettingsView.Model.DefaultProductLibrarySource.Equals(
                                    SelectedProductUniverseInfo.ToString()))
                            {
                                //Silent set
                                _isSelectedProductLibrarySourceDefault = true;
                            }
                            else
                            {
                                _isSelectedProductLibrarySourceDefault = false;
                            }
                        }
                        else
                        {
                            _isSelectedProductLibrarySourceDefault = false;
                        }
                        OnPropertyChanged(IsSelectedProductLibraryDefaultSourceProperty);
                    }
                }
            }
            else
            {
                //Reset stats and status

                StatusBarText = String.Empty;

                //Ensure panel updates too
                if (libraryPanelView != null)
                {
                    if (libraryPanelView.CurrentProductUniverse != null)
                    {
                        libraryPanelView.CurrentProductUniverse = null;
                    }
                }
            }
        }

        #endregion

        #region Window Management

        public void SwitchToView(IPlanDocument doc)
        {
            if (doc != null)
            {
                //get the parent controller
                PlanControllerViewModel parentController =
                    PlanControllers.FirstOrDefault(p => p.PlanDocuments.Contains(doc));
                if (parentController != null)
                {
                    //select the controller and doc
                    if (ActivePlanController != parentController)
                    {
                        ActivePlanController = parentController;
                    }
                    if (parentController.SelectedPlanDocument != doc)
                    {
                        parentController.SelectedPlanDocument = doc;
                    }
                }
            }
        }

        public void TileWindows(System.Windows.Controls.Orientation direction)
        {
            if (AttachedControl != null)
            {
                AttachedControl.TileWindows(direction);
            }
        }

        /// <summary>
        /// Docks all child windows back into their rdefault tabbed state
        /// </summary>
        public void DockWindows()
        {
            if (AttachedControl != null)
            {
                AttachedControl.DockAllWindows();
            }
        }

        /// <summary>
        ///  Applies the current default settings to the given controller
        /// </summary>
        /// <param name="controller"></param>
        private void ApplyDefaultSettings(PlanControllerViewModel controller, Boolean isNewPlan)
        {
            var defaults = _appSettingsView.Model;
            var plan = controller.SourcePlanogram;
            UserEditorSettings settings = _appSettingsView.Model;

            //correct uom
            if (plan.LengthUnitsOfMeasure == PlanogramLengthUnitOfMeasureType.Unknown)
            {
                plan.LengthUnitsOfMeasure = settings.LengthUnitOfMeasure;
            }
            if (plan.AreaUnitsOfMeasure == PlanogramAreaUnitOfMeasureType.Unknown)
            {
                plan.AreaUnitsOfMeasure = PlanogramAreaUnitOfMeasureTypeHelper.FromLength(plan.LengthUnitsOfMeasure);
            }
            if (plan.CurrencyUnitsOfMeasure == PlanogramCurrencyUnitOfMeasureType.Unknown)
            {
                plan.CurrencyUnitsOfMeasure = settings.CurrencyUnitOfMeasure;
            }
            if (plan.WeightUnitsOfMeasure == PlanogramWeightUnitOfMeasureType.Unknown)
            {
                plan.WeightUnitsOfMeasure = settings.WeightUnitOfMeasure;
            }
            if (plan.VolumeUnitsOfMeasure == PlanogramVolumeUnitOfMeasureType.Unknown)
            {
                plan.VolumeUnitsOfMeasure = settings.VolumeUnitOfMeasure;
            }
            plan.ClearAllUndoRedoActions();


            //load the default layout
            controller.AddNewDocument(DocumentType.Design, false);


            //Apply document defaults
            foreach (var doc in controller.PlanDocuments)
            {
                ApplyDefaultSettings(doc, isNewPlan);
            }
        }

        /// <summary>
        ///     Applies the default system settings to the given document.
        /// </summary>
        /// <param name="doc"></param>
        private void ApplyDefaultSettings(IPlanDocument doc, Boolean isNewPlan)
        {
            var defaults = _appSettingsView.Model;

            //product label
            if (!String.IsNullOrEmpty(defaults.ProductLabel))
            {
                using (LabelInfoListViewModel labelInfos = new LabelInfoListViewModel(App.ViewState.Settings.Model.LabelLocation))
                {
                    labelInfos.FetchAllByType(LabelType.Product);

                    LabelInfo info = labelInfos.Model.FirstOrDefault(
                        p => p.Name == defaults.ProductLabel && p.Type == LabelType.Product);
                    if (info != null)
                    {
                        try
                        {
                            Label label = Label.FetchByFilename(info.Id as String, /*asReadOnly*/true);
                            doc.ProductLabel = new LabelItem(label);
                        }
                        catch (Exception)
                        {
                            //dont worry about it - file may no longer exist.
                        }
                    }
                }
            }

            //fixture label
            if (!String.IsNullOrEmpty(defaults.FixtureLabel))
            {
                using (LabelInfoListViewModel labelInfos = new LabelInfoListViewModel(App.ViewState.Settings.Model.LabelLocation))
                {
                    labelInfos.FetchAllByType(LabelType.Fixture);

                    LabelInfo info = labelInfos.Model.FirstOrDefault(
                       p => p.Name == defaults.FixtureLabel && p.Type == LabelType.Fixture);
                    if (info != null)
                    {
                        try
                        {
                            Label label = Label.FetchByFilename(info.Id as String, /*asReadOnly*/true);
                            doc.FixtureLabel = new LabelItem(label);
                        }
                        catch (Exception)
                        {
                            //dont worry about it - file may no longer exist.
                        }
                    }
                }
            }

            //highlight
            if (!String.IsNullOrEmpty(defaults.Highlight))
            {
                using (HighlightInfoListViewModel highlightInfos = new HighlightInfoListViewModel(App.ViewState.Settings.Model.HighlightLocation))
                {
                    highlightInfos.FetchAll();

                    HighlightInfo info = highlightInfos.Model.FirstOrDefault(p => p.Name == defaults.Highlight);
                    if (info != null)
                    {
                        try
                        {
                            Highlight highlight = Highlight.FetchByFilename(info.Id as String, /*asReadOnly*/true);
                            doc.Highlight = new HighlightItem(highlight);
                        }
                        catch (Exception)
                        {
                            //dont worry about it - file may no longer exist.
                        }
                    }
                }

            }

            doc.ShowGridlines = false;
        }

        #endregion

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    App.ViewState.RepositoryConnectionChanging -= ViewState_RepositoryConnectionChanging;
                    App.ViewState.SelectionModeChanged -= ViewState_SelectionModeChanged;

                    CloseAllPlans();

                    ClearTempImportFiles();

                    StopAutosaveTimer();
                }
                base.IsDisposed = true;
            }
        }

        #endregion

        #region Static Helpers

        /// <summary>
        ///     Splits any ProSpaceFiles that might be required to be imported so that each planogram is imported into a separate package.
        /// </summary>
        /// <param name="fileNames">List of files from which to split those of type ProSpace.</param>
        /// <param name="tempImportFolder">Temporary folder to store the split files.</param>
        /// <returns>A new collection of filenames with any ProSpace file split into single plan files.</returns>
        private static String[] SplitProSpaceFiles(String[] fileNames, String tempImportFolder)
        {
            IEnumerable<String> proSpaceFiles = fileNames
                .Where(fileName => PlanogramImportFileTypeHelper.GetTypeFromFileName(fileName) == PlanogramImportFileType.ProSpace).ToList();

            List<String> modifiedFileNameList = fileNames.ToList();

            foreach (String fileName in proSpaceFiles)
            {
                try
                {
                    modifiedFileNameList.Remove(fileName);
                    Boolean isCommonData = true;
                    List<String> commonData = new List<String>();
                    Dictionary<String, List<String>> planData = new Dictionary<String, List<String>>();

                    using (FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                    {
                        using (TextReader streamReader = new StreamReader(fileStream))
                        {
                            //  Read the file one line at a time.
                            //  Store the common data in one collection to reuse, 
                            //  store the plan specific data in a dictionary, 
                            //  using the name of the new file as the key.
                            String targetFileName = String.Empty;
                            while (streamReader.Peek() >= 0)
                            {
                                String rawLine = streamReader.ReadLine();
                                if (rawLine == null)
                                {
                                    Debug.Fail("Line could not be read and there was supposed to be one.");
                                    break;
                                }

                                String[] line = rawLine.Split(',');
                                if (String.Equals(line[0], "Planogram", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    isCommonData = false;
                                    targetFileName = CorrectFileName(line[ProSpaceImportHelper.GetIndex(ProSpaceImportHelper.PlanogramName)]);
                                }

                                if (isCommonData)
                                {
                                    commonData.Add(rawLine);
                                    continue; // Move on to the next line.
                                }

                                if (!planData.ContainsKey(targetFileName)) planData[targetFileName] = new List<String>();
                                planData[targetFileName].Add(rawLine);
                            }
                        }
                    }

                    if (!Directory.Exists(tempImportFolder)) Directory.CreateDirectory(tempImportFolder);

                    foreach (KeyValuePair<String, List<String>> pair in planData)
                    {
                        String targetFileName = Path.Combine(tempImportFolder, pair.Key + ".psa");
                        modifiedFileNameList.Add(targetFileName);
                        List<String> planLines = pair.Value;

                        using (FileStream fileStream = new FileStream(targetFileName, FileMode.Create))
                        using (TextWriter streamWriter = new StreamWriter(fileStream))
                        {
                            foreach (String line in commonData)
                                streamWriter.WriteLine(line);
                            foreach (String planLine in planLines)
                                streamWriter.WriteLine(planLine);
                        }
                    }

                }
                catch
                {
                    CommonHelper.GetWindowService().ShowErrorMessage(
                        Message.MainPage_OpenPlanogramsFromFileSystem_CouldNotOpenPsaHeader,
                        String.Format(CultureInfo.CurrentCulture, Message.MainPage_OpenPlanogramsFromFileSystem_CouldNotOpenPsaDesc, fileName));

                }

                fileNames = modifiedFileNameList.ToArray();

            }
            return fileNames;
        }

        /// <summary>
        ///     Correct the given <paramref name="fileName"/> so that it does not contain invalid characters.
        /// </summary>
        /// <param name="fileName">The source file name that will be corrected by removing invalid characters.</param>
        /// <returns>The source file name without any invalid characters.</returns>
        private static String CorrectFileName(String fileName)
        {
            String regexSearch = new String(Path.GetInvalidFileNameChars()) + new String(Path.GetInvalidPathChars());
            var illegalCharacterMask = new Regex(String.Format("[{0}]", Regex.Escape(regexSearch)));
            fileName = illegalCharacterMask.Replace(fileName, "");
            return fileName;
        }

        /// <summary>
        ///     Notify the user that an error message happened.
        /// </summary>
        /// <param name="header"></param>
        /// <param name="description"></param>
        private static void NotifyUserSaveHadErrors(String header, String description)
        {
            CommonHelper.GetWindowService().ShowMessage(MessageWindowType.Warning, header, description, 1, Message.Generic_OK, null, null);
        }

        #endregion

        #region Nested Classes

        /// <summary>
        ///     Worker to export plan to ecxternal file type whilst showing a modal busy.
        /// </summary>
        private sealed class SavePlanToExternalFileTypeWork : IModalBusyWork
        {
            private PackageSaveSettings _saveSettings;
            private String _planogramName;
            private String _filename;
            private PlanogramExportTemplate _exportTemplate;
            private Package _package;

            #region Properties

            public Boolean ReportsProgress
            {
                get { return false; }
            }

            public Boolean SupportsCancellation
            {
                get { return false; }
            }

            #endregion

            #region Constructor

            public SavePlanToExternalFileTypeWork(Package package, PackageSaveSettings saveSettings, String planogramName, String filename, PlanogramExportTemplate exportTemplate)
            {
                _saveSettings = saveSettings;
                _package = package;
                _planogramName = planogramName;
                _filename = filename;
                _exportTemplate = exportTemplate;
            }

            #endregion

            #region Methods

            public Object GetWorkerArgs()
            {
                return new Object[] { _saveSettings, _planogramName, _filename, _exportTemplate };
            }

            public void OnDoWork(IModalBusyWorkerService sender, DoWorkEventArgs e)
            {
                if (_saveSettings == null) return;
                if (_filename == String.Empty) return;
                if (_exportTemplate == null) return;

                //Create a package viewmodel from our cloned package to save
                PackageViewModel packageView = PackageViewModel.NewPackageViewModel(_package, true);

                //  Save the planogram to a file
                e.Result = packageView.SaveAsExternalFileType(_saveSettings, _filename, _saveSettings.SaveAsFileType, _exportTemplate);
            }


            public void OnProgressChanged(IModalBusyWorkerService sender, ProgressChangedEventArgs e)
            {
                //nothing to report.
            }

            public void OnWorkCompleted(IModalBusyWorkerService sender, RunWorkerCompletedEventArgs e)
            {
                if (e.Cancelled)
                {
                    //  Show the cancelled message to the user.
                    CommonHelper.GetWindowService().ShowOkMessage(MessageWindowType.Warning,
                        Message.ExportToExternalFileType_ExportCancelled_Header,
                        Message.ExportToExternalFileType_ExportCancelled_Description);

                    return;
                }

                if (e.Error == null && e.Result is ExportContext)
                {
                    ExportContext context = e.Result as ExportContext;
                    if (context.ViewErrorLogsOnCompletion &&
                        context.ExportEvents.Any())
                    {
                        //Show the event log
                        new EventLogViewModel(_planogramName, context.ExportEvents).ShowDialog();
                        return;
                    }
                }

                if (e.Error != null)
                {
                    Exception exception = e.Error.GetBaseException();
                    if (exception.GetType() == typeof(Galleria.Framework.Planograms.Model.PackageLockException))
                    {
                        CommonHelper.GetWindowService().ShowErrorMessage(
                            Message.ExportToExternalFileType_Error_Header,
                            Message.ExportToExternalFileType_UnlockPackageError_Description);
                    }
                    else
                    {
                        //  Show the generic error message to the user.
                        CommonHelper.GetWindowService().ShowErrorMessage(
                            Message.ExportToExternalFileType_Error_Header, String.Format(Message.ExportToExternalFileType_Error_Description, e.Error.Message));
                    }
                }
                else
                {
                    //  Show the completed message to the user.
                    CommonHelper.GetWindowService().ShowOkMessage(MessageWindowType.Info,
                        Message.ExportToExternalFileType_Complete_Header,
                        String.Format(Message.ExportToExternalFileType_Complete_Description, _planogramName, _filename));
                }
            }

            /// <summary>
            /// Invoked after the work has started asynchronously so that the modal busy dialog is displayed.
            /// </summary>
            public void ShowModalBusy(IModalBusyWorkerService sender, EventHandler cancelEventHandler)
            {
                CommonHelper.GetWindowService().ShowIndeterminateBusy(
                      Message.ExportToExternalFileType_Busy_Header,
                      String.Format(Message.ExportToExternalFileType_Busy_Description, _planogramName, _filename));
            }

            #endregion
        }

        #region Nested class - SyncActionQueue
        // TODO: Perhaps move to a more common place for reuse.

        /// <summary>
        ///     Interface to mark a class as one that can be used as an item in a <see cref="SyncActionQueue"/>.
        /// </summary>
        private interface ISyncActionQueueItem { }

        /// <summary>
        ///     Represents a delayed action to be performed on one or more items that are added one by one within the <see cref="_syncDelayTime"/>.
        /// </summary>
        private class SyncActionQueue
        {
            #region Properties

            #region SyncActionTimer

            /// <summary>
            ///     The <see cref="DispatcherTimer"/> in charge of firing the action when the time is up.
            /// </summary>
            private DispatcherTimer _syncActionTimer;

            /// <summary>
            ///     Get the <see cref="DispatcherTimer"/> that is used by this instance.
            /// </summary>
            /// <remarks>   If there is not one set yet, it will be created before returning.</remarks>
            private DispatcherTimer SyncActionTimer
            {
                get
                {
                    return _syncActionTimer ??
                           (_syncActionTimer = new DispatcherTimer(TimeSpan.FromMilliseconds(_syncDelayTime),
                                                                   DispatcherPriority.Background,
                                                                   SyncAddHandler,
                                                                   Dispatcher.CurrentDispatcher));
                }
            }

            #endregion

            #region SyncItemQueue

            /// <summary>
            ///     The list of items to be passed to the action when the time is up.
            /// </summary>
            private readonly List<ISyncActionQueueItem> _syncItemQueue = new List<ISyncActionQueueItem>();

            /// <summary>
            ///     Gets the read only collection of items to be processed by the action when the time is up.
            /// </summary>
            private ReadOnlyCollection<ISyncActionQueueItem> SyncItemQueue
            {
                get
                {
                    lock (SyncActionTimer)
                    {
                        return _syncItemQueue.AsReadOnly();
                    }
                }
            }

            #endregion

            private readonly Action<ReadOnlyCollection<ISyncActionQueueItem>> _action;
            private readonly Int32 _syncDelayTime;

            #endregion

            #region Constructor

            /// <summary>
            ///     Creates a new instance of <see cref="SyncActionQueue"/> that will invoke the given <paramref name="action"/> 
            ///     when the <paramref name="syncDelayTime"/> is up.
            /// </summary>
            /// <param name="action"></param>
            /// <param name="syncDelayTime"></param>
            /// <remarks>   The action accepts a list of <see cref="ISyncActionQueueItem"/> that is created <c>after</c> the queue has been created.
            /// <para />    Each time that a new item is added to the queue the delay time is restarted to give time for new items to be added.
            /// </remarks>
            public SyncActionQueue(Action<ReadOnlyCollection<ISyncActionQueueItem>> action, Int32 syncDelayTime = 10)
            {
                _action = action;
                _syncDelayTime = syncDelayTime;
            }

            #endregion

            #region Methods

            /// <summary>
            ///     Add a new <paramref name="item"/> to the internal list so that it will be processed by the instance's <see cref="_action"/> when the <see cref="_syncDelayTime"/> is up.
            /// </summary>
            /// <param name="item">The item to be added to the collection of items to pass to the action when the time comes.</param>
            /// <remarks>   Each time that a new item is added, the delay is reset, to give further time to other items to be added before processing.</remarks>
            public void Add(ISyncActionQueueItem item)
            {
                SyncActionTimer.Stop();
                lock (SyncActionTimer)
                {
                    _syncItemQueue.Add(item);
                }
                SyncActionTimer.Start();
            }

            /// <summary>
            ///     Invoked when the time is up for the delayed action. It will delegate the main functionality to the user indicated <see cref="_action"/>.
            /// </summary>
            /// <remarks>After each invocation, the timer is stopped and the list of items to process is cleared. To restart the process just add new items.</remarks>
            private void SyncAddHandler(Object sender, EventArgs e)
            {
                var timer = sender as DispatcherTimer;
                if (timer == null) return;

                timer.Stop();
                lock (timer)
                {
                    _action.Invoke(SyncItemQueue);
                    _syncItemQueue.Clear();
                }
            }

            #endregion

        }

        #endregion

        #region Nested class - ClosePlanControllerRequest

        /// <summary>
        ///     Parameter class to pass both a controller and an action to be invoked when the the syncing as completed.
        /// </summary>
        /// <remarks>Implements ISyncActionQueueItem to be able to use it as an item in a <see cref="SyncActionQueue"/>.</remarks>
        public class ClosePlanControllerRequest : ISyncActionQueueItem
        {
            #region Properties

            /// <summary>
            ///     The <see cref="PlanController"/> that needs to be closed as part of a group of other closing ones.
            /// </summary>
            public PlanController Controller { get; private set; }

            /// <summary>
            ///     An action to close the document if needed after the user prompt to continue with item changes (used to close the actual docked tab).
            /// </summary>
            public Action PendingCloseAction { get; private set; }

            #endregion

            #region Constructor

            /// <summary>
            ///     Creates a new instance of <see cref="ClosePlanControllerRequest"/> with the provided information.
            /// </summary>
            /// <param name="controller"></param>
            /// <param name="pendingCloseAction"></param>
            public ClosePlanControllerRequest(PlanController controller, Action pendingCloseAction)
            {
                Controller = controller;
                PendingCloseAction = pendingCloseAction;
            }

            #endregion
        }

        #endregion

        #endregion
    }
}