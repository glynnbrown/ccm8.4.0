﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
// V8-27938 : N.Haywood
//  Added data sheets
#endregion

#endregion


using System.Windows;
using System.Linq;
using Fluent;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Model;
using System;
using System.Globalization;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Controls;
using System.Windows.Data;
using Galleria.Ccm.Common.Wpf.Converters;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for MainPageHomeTab.xaml
    /// </summary>
    public sealed partial class MainPageHomeTab
    {

        #region ViewModel Property

        /// <summary>
        /// ViewModel dependency property definition.
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(MainPageViewModel), typeof(MainPageHomeTab),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets the viewmodel controller for this window.
        /// </summary>
        public MainPageViewModel ViewModel
        {
            get { return (MainPageViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        /// <summary>
        /// Called whenever the viewmodel property changes.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MainPageHomeTab senderControl = (MainPageHomeTab)obj;

            MainPageViewModel oldModel = e.OldValue as MainPageViewModel;
            if (oldModel != null)
            {
                oldModel.AvailableProductLabels.BulkCollectionChanged -= senderControl.ViewModel_AvailableProductLabelsBulkCollectionChanged;
                oldModel.AvailableHighlights.BulkCollectionChanged -= senderControl.ViewModel_AvailableHighlightsBulkCollectionChanged;
                oldModel.AvailableDataSheets.BulkCollectionChanged -= senderControl.ViewModel_AvailableDataSheetsBulkCollectionChanged;
            }


            MainPageViewModel newModel = e.NewValue as MainPageViewModel;
            if(newModel != null)
            {
                newModel.AvailableProductLabels.BulkCollectionChanged += senderControl.ViewModel_AvailableProductLabelsBulkCollectionChanged;
                newModel.AvailableHighlights.BulkCollectionChanged += senderControl.ViewModel_AvailableHighlightsBulkCollectionChanged;
                newModel.AvailableDataSheets.BulkCollectionChanged += senderControl.ViewModel_AvailableDataSheetsBulkCollectionChanged;
            }

            senderControl.UpdateProductLabelsDropdownList();
            senderControl.UpdateHighlightsDropdownList();
            senderControl.UpdateDataSheetsDropdownList();
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public MainPageHomeTab()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the available product labels collection changes to update the drop down menu.
        /// </summary>
        private void ViewModel_AvailableProductLabelsBulkCollectionChanged(object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
            UpdateProductLabelsDropdownList();
        }

        /// <summary>
        /// Called whenever the available highlights collection changes to update the drop down menu.
        /// </summary>
        private void ViewModel_AvailableHighlightsBulkCollectionChanged(object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
            UpdateHighlightsDropdownList();
        }

        /// <summary>
        /// Called whenever the available datasheets collection changes to update the drop down menu.
        /// </summary>
        private void ViewModel_AvailableDataSheetsBulkCollectionChanged(object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
            UpdateDataSheetsDropdownList();
        }



        /// <summary>
        /// Called whenever the user clicks on a highlight menu item.
        /// </summary>
        private void HighlightMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel == null) return;

            var senderControl = sender as MenuItem;
            if (senderControl == null) return;

            if (senderControl.IsChecked)
            {
                HighlightInfo info = senderControl.CommandParameter as HighlightInfo;
                if (info != null)
                {
                    this.ViewModel.SetHighlight(info);
                }
                else
                {
                    HighlightItem item = senderControl.CommandParameter as HighlightItem;
                    if (item != null)
                    {
                        this.ViewModel.SetHighlight(item);
                    }
                    else
                    {
                        this.ViewModel.ClearHighlight();
                    }
                }
            }
            else
            {
                ViewModel.ClearHighlight();
            }

        }

        /// <summary>
        /// Called when a highlight dynamic menu item is to be added to the quick access toolbar.
        /// </summary>
        private void HighlightMenuItem_CreatingQuickAccessItem(object sender, DynamicMenuItem.CreateQuickAccessItemArgs e)
        {
            Button button = e.Element as Button;
            if (button == null) return;

            DynamicMenuItem senderControl = (DynamicMenuItem)sender;

            //force an icon and tooltip
            button.Icon = new System.Windows.Controls.Image() { Source = MainPageCommands.SelectHighlight.SmallIcon, Height = 16, Width = 16 };
            button.ToolTip = String.Format(CultureInfo.CurrentCulture, Message.Ribbon_HighlightQuickAccessTooltipFormat, senderControl.Header as String);

            e.Element = button;
        }

        /// <summary>
        /// Called whenever the user clicks on a product label menu item.
        /// </summary>
        private void ProductLabelMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel == null) return;

            var senderControl = sender as MenuItem;
            if (senderControl == null) return;

            if (senderControl.IsChecked)
            {
                LabelInfo info = senderControl.CommandParameter as LabelInfo;
                if (info != null)
                {
                    this.ViewModel.SetProductLabel(info);
                }
                else
                {
                    LabelItem item = senderControl.CommandParameter as LabelItem;
                    if (item != null)
                    {
                        this.ViewModel.SetProductLabel(item);
                    }
                    else
                    {
                        this.ViewModel.ClearProductLabel();
                    }
                }
            }
            else
            {
                this.ViewModel.ClearProductLabel();
            }
        }

        /// <summary>
        /// Called when a product label dynamic menu item is to be added to the quick access toolbar.
        /// </summary>
        private void ProductLabelMenuItem_CreatingQuickAccessItem(object sender, DynamicMenuItem.CreateQuickAccessItemArgs e)
        {
            Button button = e.Element as Button;
            if (button == null) return;

            DynamicMenuItem senderControl = (DynamicMenuItem)sender;

            //force an icon and tooltip
            button.Icon = new System.Windows.Controls.Image() { Source = MainPageCommands.SelectProductLabel.SmallIcon, Height = 16, Width = 16 };
            button.ToolTip = String.Format(CultureInfo.CurrentCulture, Message.Ribbon_ProductLabelQuickAccessTooltipFormat, senderControl.Header as String);

            e.Element = button;
        }

        /// <summary>
        /// Called whenever the user clicks on a datasheet menu item.
        /// </summary>
        private void DataSheetMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (ViewModel != null)
            {
                var senderControl = sender as MenuItem;
                if (senderControl != null)
                {
                    ViewModel.AddReportDocument(senderControl.CommandParameter as CustomColumnLayout);
                }
            }
        }

        /// <summary>
        /// Called when a product label dynamic menu item is to be added to the quick access toolbar.
        /// </summary>
        private void DataSheetMenuItem_CreatingQuickAccessItem(object sender, DynamicMenuItem.CreateQuickAccessItemArgs e)
        {
            Button button = e.Element as Button;
            if (button == null) return;

            DynamicMenuItem senderControl = (DynamicMenuItem)sender;
            button.Icon = new System.Windows.Controls.Image() { Source = MainPageCommands.SelectDataSheet.SmallIcon, Height = 16, Width = 16 };
            button.ToolTip = String.Format(CultureInfo.CurrentCulture, Message.Ribbon_DataSheetQuickAccessTooltipFormat, senderControl.Header as String);

            e.Element = button;
        }

        /// <summary>
        /// Called whenever the user clicks on a datasheet menu item.
        /// </summary>
        private void CustomDataSheetMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (ViewModel != null) ViewModel.SetCustomDataSheet();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the available product labels dropdown menu.
        /// </summary>
        /// <remarks>We have to do it like this rather than use datatemplates otherwise the ribbon quick access 
        /// toobar save flakes out.</remarks>
        private void UpdateProductLabelsDropdownList()
        {
            //clean up:
            foreach (var item in this.AvailableProductLabelsDisplay.Items.OfType<DynamicMenuItem>())
            {
                item.CreatingQuickAccessItem -= ProductLabelMenuItem_CreatingQuickAccessItem;
            }
            this.AvailableProductLabelsDisplay.Items.Clear();

            if (this.ViewModel == null) return;

            //Add the items
            foreach(var productLabel in this.ViewModel.AvailableProductLabels)
            {
                DynamicMenuItem item = new DynamicMenuItem()
                {
                    Header = productLabel.Name,
                    IsCheckable = true,
                    Command = MainPageCommands.SelectProductLabel,
                    CommandParameter = productLabel,
                    CanAddToQuickAccessToolBar = true,
                };

                //hook into quick access item creation so that
                // we can add an icon and tooltip
                item.CreatingQuickAccessItem += ProductLabelMenuItem_CreatingQuickAccessItem;


                MultiBinding mb = new MultiBinding() { Converter = new AreEqualConverter(), Mode = BindingMode.OneWay };
                mb.Bindings.Add(new Binding("Id") { Source = productLabel });
                mb.Bindings.Add(new Binding("ViewModel.ActivePlanController.SelectedPlanDocument.ProductLabel.Id") { Source=this });
                BindingOperations.SetBinding(item, DynamicMenuItem.IsCheckedProperty, mb);


                this.AvailableProductLabelsDisplay.Items.Add(item);
            }
        }

        /// <summary>
        /// Updates the available highlights dropdown menu.
        /// </summary>
        /// <remarks>We have to do it like this rather than use datatemplates otherwise the ribbon quick access 
        /// toobar save flakes out.</remarks>
        private void UpdateHighlightsDropdownList()
        {
            //clean up:
            foreach (var item in this.AvailableHighlightsDisplay.Items.OfType<DynamicMenuItem>())
            {
                item.CreatingQuickAccessItem -= HighlightMenuItem_CreatingQuickAccessItem;
            }
            this.AvailableHighlightsDisplay.Items.Clear();

            if (this.ViewModel == null) return;

            //Add the items
            foreach (var info in this.ViewModel.AvailableHighlights)
            {
                DynamicMenuItem item = new DynamicMenuItem()
                {
                    Header = info.Name,
                    IsCheckable = true,
                    Command = MainPageCommands.SelectHighlight,
                    CommandParameter = info,
                    CanAddToQuickAccessToolBar = true,
                };

                //hook into quick access item creation so that
                // we can add an icon and tooltip
                item.CreatingQuickAccessItem += HighlightMenuItem_CreatingQuickAccessItem;

                MultiBinding mb = new MultiBinding() { Converter = new AreEqualConverter(), Mode = BindingMode.OneWay };
                mb.Bindings.Add(new Binding("Id") { Source = info });
                mb.Bindings.Add(new Binding("ViewModel.ActivePlanController.SelectedPlanDocument.Highlight.Id") { Source = this });
                BindingOperations.SetBinding(item, DynamicMenuItem.IsCheckedProperty, mb);


                this.AvailableHighlightsDisplay.Items.Add(item);
            }
        }

        /// <summary>
        /// Updates the available datasheets dropdown menu.
        /// </summary>
        /// <remarks>We have to do it like this rather than use datatemplates otherwise the ribbon quick access 
        /// toobar save flakes out.</remarks>
        private void UpdateDataSheetsDropdownList()
        {
            //clean up:
            foreach (var item in this.AvailableDataSheetsDisplay.Items.OfType<DynamicMenuItem>())
            {
                item.CreatingQuickAccessItem -= DataSheetMenuItem_CreatingQuickAccessItem;
            }
            this.AvailableDataSheetsDisplay.Items.Clear();

            if (this.ViewModel == null) return;

            //Add the items
            foreach (var info in this.ViewModel.AvailableDataSheets)
            {
                DynamicMenuItem item = new DynamicMenuItem()
                {
                    Header = info.Name,
                    Command = MainPageCommands.SelectDataSheet,
                    CommandParameter = info,
                    CanAddToQuickAccessToolBar = true,
                };

                //hook into quick access item creation so that
                // we can add an icon and tooltip
                item.CreatingQuickAccessItem += DataSheetMenuItem_CreatingQuickAccessItem;

                this.AvailableDataSheetsDisplay.Items.Add(item);
            }
        }

        #endregion

    }


}
