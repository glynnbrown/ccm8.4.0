﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for AssortmentDocumentContextTab.xaml
    /// </summary>
    public sealed partial class AssortmentDocumentContextTab
    {
        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentPlanDocument), typeof(AssortmentDocumentContextTab),
                new PropertyMetadata(null));

        public AssortmentPlanDocument ViewModel
        {
            get { return (AssortmentPlanDocument)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructors
        public AssortmentDocumentContextTab()
        {
            InitializeComponent();
        } 
        #endregion
    }
}
