﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (V8 1.0)

// V8-25395 : A.Probyn
//  Created
// V8-25797 : A.Silva ~ Added code to manage the Custom Column Layout button.
// V8-26143 : A.Silva ~ Changed the code managing the custom column layouts to use the extended grid context menu.

#endregion

#endregion

using System.Windows;
using Galleria.Ccm.Editor.Client.Wpf.Common;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    ///     Interaction logic for ProductLibraryContextTab.xaml
    /// </summary>
    public sealed partial class ProductLibraryContextTab
    {
        #region Properties

        #region ViewModel

        /// <summary>
        /// ViewModel dependency property definition
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof (MainPageViewModel), typeof (ProductLibraryContextTab),
                new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the reference to the viewmodel controller.
        /// </summary>
        public MainPageViewModel ViewModel
        {
            get { return (MainPageViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region ProductLibraryView

        /// <summary>
        /// ProductLibraryView dependency property definition.
        /// </summary>
        public static readonly DependencyProperty ProductLibraryViewProperty =
            DependencyProperty.Register("ProductLibraryView", typeof(ProductLibraryViewModel), typeof(ProductLibraryContextTab),
                new PropertyMetadata(null));

        /// <summary>
        /// Returns a reference to the viewmodel holding the current product library.
        /// </summary>
        public ProductLibraryViewModel ProductLibraryView
        {
            get { return (ProductLibraryViewModel)GetValue(ProductLibraryViewProperty); }
            private set { SetValue(ProductLibraryViewProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ProductLibraryContextTab()
        {
            InitializeComponent();
            this.ProductLibraryView = App.ViewState.ProductLibraryView;
        }

        #endregion
    }
}