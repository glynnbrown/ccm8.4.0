﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// V8-26860 : L.Luong 
//   Created (Auto-generated)
// V8-27404 : L.Luong
//   Renamed LevelType to EntryType
// V8-27764 : A.Kuszyk
//  Bound UI to friendly names of enum values and fixed Entry Type selection.
// V8-28061 : A.Kuszyk
//  Re-factored to PlanogramEventLog changes and added reading pane.
#endregion

#region Version History: (CCM 8.0.3)
//V8-29590 : A.Probyn
//  Layout updates and expanded for new Score property
#endregion

#region Version History: (CCM 8.1.0)
//V8-30234 : A.Probyn
//  Updated to refresh filtered event logs on load
//  Updated Dispose
#endregion

#region Version History: (CCM 8.3.0)
// CCM-31546 : M.Brumby
//  Added new constructor for use when displaying export logs (Apollo/JDA/Spaceman) where the
//  logs are not stored in a planogram but as a dto list.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Common.ModelObjectViewModels;
using System.Collections.ObjectModel;
using Galleria.Framework.Collections;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    public class EventLogViewModel : ViewModelAttachedControlObject<EventLogWindow>
    {
        #region Fields

        private String _windowTitle = "";
        private PlanogramEventLogView _selectedEventLog;
        private String _content;

        private Boolean _isErrorVisible = true;
        private Boolean _isWarningVisible = true;
        private Boolean _isInformationVisible = true;
        private Boolean _isDetailedVisible = false; //Off on load by default
        private List<PlanogramEventLogView> _availableEventLogs;
        private BulkObservableCollection<PlanogramEventLogView> _filteredEventLogs = new BulkObservableCollection<PlanogramEventLogView>();

        #endregion

        #region Binding Property Paths

        // Properties
        public static readonly PropertyPath FilteredEventLogsProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.FilteredEventLogs);
        public static readonly PropertyPath SelectedEventLogProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.SelectedEventLog);
        public static readonly PropertyPath ContentProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.Content);
        public static readonly PropertyPath IsErrorVisibleProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.IsErrorVisible);
        public static readonly PropertyPath IsWarningVisibleProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.IsWarningVisible);
        public static readonly PropertyPath IsInformationVisibleProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.IsInformationVisible);
        public static readonly PropertyPath IsDetailedVisibleProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.IsDetailedVisible);

        //Commands
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<EventLogViewModel>(p => p.CloseCommand);

        #endregion

        #region Properties

        public String WindowTitle
        {
            get { return _windowTitle; }
        }

        /// <summary>
        /// The filtered list of event logs that is displayed in the UI.
        /// </summary>
        public BulkObservableCollection<PlanogramEventLogView> FilteredEventLogs { get { return _filteredEventLogs; } }

        /// <summary>
        /// The currently selected event log row.
        /// </summary>
        public PlanogramEventLogView SelectedEventLog 
        {
            get
            {
                return _selectedEventLog;
            }
            set
            {
                _selectedEventLog = value;
                OnSelectedEventLogChanged();
            }
        }

        /// <summary>
        /// The content to display from the currently selected event log.
        /// </summary>
        public String Content
        {
            get
            {
                return _content;
            }
            set
            {
                _content = value;
                OnPropertyChanged("Content");
            }
        }

        /// <summary>
        /// Indicates if Error event logs are visible.
        /// </summary>
        public Boolean IsErrorVisible 
        { 
            get
            {
                return _isErrorVisible;
            }
            set
            {
                _isErrorVisible = value;
                UpdateFilteredEventLogs();
                OnPropertyChanged("IsErrorVisible");
            }
        }

        /// <summary>
        /// Indicates if Warning event logs are visible.
        /// </summary>
        public Boolean IsWarningVisible
        {
            get
            {
                return _isWarningVisible;
            }
            set
            {
                _isWarningVisible = value;
                UpdateFilteredEventLogs();
                OnPropertyChanged("IsWarningVisible");
            }
        }

        /// <summary>
        /// Indicates if Information event logs are visible.
        /// </summary>
        public Boolean IsInformationVisible
        {
            get
            {
                return _isInformationVisible;
            }
            set
            {
                _isInformationVisible = value;
                UpdateFilteredEventLogs();
                OnPropertyChanged("IsInformationVisible");
            }
        }

        /// <summary>
        /// Indicates if Detailed (formally named debug) event logs are visible.
        /// </summary>
        public Boolean IsDetailedVisible
        {
            get
            {
                return _isDetailedVisible;
            }
            set
            {
                _isDetailedVisible = value;
                UpdateFilteredEventLogs();
                OnPropertyChanged("IsDetailedVisible");
            }
        }
        
        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="EventLogViewModel"/> class.
        /// </summary>
        /// <exception cref="ArgumentNullException">If the planogram provided as a parameter is null.</exception>
        public EventLogViewModel(PlanogramView planogram)
        {
            if (planogram == null) throw new ArgumentNullException("planogram",@"The planogram cannot be null when initializing a EventLogViewModel.");

            _windowTitle = planogram.Name;

            // Add the planogram event logs to the view model, calculating exectution order based on the id order.
            _availableEventLogs = new List<PlanogramEventLogView>();
            Int32 executionOrder = 1;
            foreach (var eventLog in planogram.Model.EventLogs.OrderBy(e => e.Id))
            {
                _availableEventLogs.Add(new PlanogramEventLogView(eventLog, executionOrder));
                executionOrder++;
            }

            //Update filtered event logs
            UpdateFilteredEventLogs();

            SelectedEventLog = FilteredEventLogs.FirstOrDefault();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="EventLogViewModel"/> class. From a list of PlanogramEventLogDtos
        /// </summary>
        /// <exception cref="ArgumentNullException">If the eventLogs provided as a parameter is null.</exception>
        /// <remarks>Created for showing export to Apollo/JDA/Spaceman logs which are
        /// only temporarily stored as a list of dtos.</remarks>
        public EventLogViewModel(String title, List<PlanogramEventLogDto> eventLogs)
        {
            if (eventLogs == null) throw new ArgumentNullException("eventLogs", @"The eventLogs list cannot be null when initializing a EventLogViewModel.");

            _windowTitle = title;

            // Add the planogram event logs to the view model, calculating exectution order based on the id order.
            _availableEventLogs = new List<PlanogramEventLogView>();
            Int32 executionOrder = 1;
            foreach (var eventLog in eventLogs.OrderBy(e => e.Id))
            {
                _availableEventLogs.Add(new PlanogramEventLogView(PlanogramEventLog.NewPlanogramEventLog(eventLog), executionOrder));
                executionOrder++;
            }

            //Update filtered event logs
            UpdateFilteredEventLogs();

            SelectedEventLog = FilteredEventLogs.FirstOrDefault();
        }

        #endregion

        #region Commands

        #region CloseCommand

        private RelayCommand _closeCommand;

        /// <summary>
        /// Closes the event Log
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed())
                    {
                        FriendlyName = Message.Generic_Close,
                        SmallIcon = ImageResources.Close_16
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }


        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Called when the selected event log changes.
        /// </summary>
        private void OnSelectedEventLogChanged()
        {
            if (SelectedEventLog == null)
            {
                Content = String.Empty;
            }
            else
            {
                Content = SelectedEventLog.Content;
            }
        }

        /// <summary>
        /// Re-filters the FilteredEventLogs based on the visibility of the log types.
        /// </summary>
        private void UpdateFilteredEventLogs()
        {
            FilteredEventLogs.Clear();
            foreach (var eventLog in _availableEventLogs)
            {
                switch(eventLog.Model.EntryType)
                {
                    case PlanogramEventLogEntryType.Error:
                        if(IsErrorVisible) FilteredEventLogs.Add(eventLog);
                        break;
                    case PlanogramEventLogEntryType.Warning:
                        if(IsWarningVisible) FilteredEventLogs.Add(eventLog);
                        break;
                    case PlanogramEventLogEntryType.Information:
                        if(IsInformationVisible) FilteredEventLogs.Add(eventLog);
                        break;
                    case PlanogramEventLogEntryType.Debug:
                        if(IsDetailedVisible) FilteredEventLogs.Add(eventLog);
                        break;
                    default:
                        continue;
                }
            }
            SelectedEventLog = FilteredEventLogs.FirstOrDefault();
        }

        #endregion

        #region IDiposable Members

        protected override void Dispose(bool disposing)
        {
            if (IsDisposed)
            {
                return;
            }

            if (disposing)
            {
                // release resources here.
                this._selectedEventLog = null;
                this._filteredEventLogs.Clear();
                this._availableEventLogs.Clear();
            }

            IsDisposed = true;
        }

        #endregion

    }
}
