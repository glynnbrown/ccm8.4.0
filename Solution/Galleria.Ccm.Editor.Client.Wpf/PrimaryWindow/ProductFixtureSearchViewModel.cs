﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0)
// V8-23726 : N.Haywood
//  Created
//  V8-26247 : I.George
//  Added ModalMessage Dialogue box
// V8-27466 : A.Probyn
//  Added missing defensive code to NextProduct_Executed. 
#endregion

#region Version History: (CCM 801)
// V8-28664 : I.George
//  ~ Removed Go To Selected command and DoSearch method in the constructor
#endregion

#region Version History: (CCM 802)
// V8-29341 : J.Pickup
//  DoFixtureSearch() now does a filter on component sequence number and bay sequence number.
#endregion

#region Version History CCM 830
// V8-31386 : A.Heathcote
//      Changed UI for PCR as well as changed how the search function works (To enable searches using user selected fields), basically a complete overhall.
// V8-32286 : A.Heathcote
//      Refactored Search for both fixture and Product (extracted both to methods) and made sure that all user requested data is retrived.
#endregion

#endregion

using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows;
using System.Linq;
using System.Windows.Input;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Controls.Wpf;
using CommonImageResources = Galleria.Ccm.Common.Wpf.Resources.ImageResources;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using System.Collections.Generic;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using System.Reflection;
using System.Windows.Controls;
using Galleria.Ccm.Common.Wpf.Services;
using System.Collections;
using System.Text.RegularExpressions;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    public class ProductFixtureSearchViewModel : ViewModelAttachedControlObject<ProductFixtureSearchWindow>
    {
        #region Fields

        private MainPageViewModel _mainPageViewModel;
        private Boolean _supressActivePlanChanged = false;

        //User options
        private String _searchText = ""; 
        private Boolean _searchByProduct = true;
        private Boolean _searchByComponent = false;
        private Boolean _searchNeedsRefreshing = false;
        private Boolean _searchOnActivePlan = true;
        private Boolean _matchCase = false;
        private Boolean _selectAll = false;

        /// <summary>
        /// Curently Highlighted
        /// </summary> 
        private Int16 _currentHighlightedResultIndex;

        /// <summary>
        /// collection of currently highlighted items 
        /// </summary>
        private ObservableCollection<PlanogramSearchResult> _highlightedSearchResults = new ObservableCollection<PlanogramSearchResult>();
        
        /// <summary>
        /// this is the observable collection for both the product and the component search results
        /// (as they are the same type)
        /// (this is a collection of Dictionaries(PlanogramSearchResult) each containing all data for a search result that
        /// pertains to the selected fields)
        /// </summary>
        private ObservableCollection<PlanogramSearchResult> _searchResults = new ObservableCollection<PlanogramSearchResult>();   
        
        /// <summary>
        /// collection of ObjectFieldInfo's that match the users selected advanced screen fields
        /// </summary>
        private ObservableCollection<ObjectFieldInfo> _advancedScreenProductFieldInfos = new ObservableCollection<ObjectFieldInfo>();

        /// <summary>
        /// collection of ObjectFieldInfo's that match the user selected advanced screen fields
        /// </summary>
        private ObservableCollection<ObjectFieldInfo> _advancedScreenComponentFieldInfos = new ObservableCollection<ObjectFieldInfo>();

        /// <summary>
        /// Collection of columns to be displayed in the results datagrid
        /// </summary>
        private DataGridColumnCollection _resultsDataGridFieldCollection = new DataGridColumnCollection();

        /// <summary>
        /// List of all of the displayable fields on the planogram 
        /// </summary>
        List<ObjectFieldInfo> _masterFieldInfoList = PlanogramFieldHelper.EnumerateAllFields().ToList();      
        
        #endregion

        #region Nested Classes
        
        /// <summary>
        /// This nested class provides a type that can accomadate the results of both product and component searches.
        /// It inherits from dictionary and accepts a String(Usually a FieldPlaceHolder) and an Object.
        /// </summary>
        public class PlanogramSearchResult : Dictionary<String, Object>
        {
            public PlanControllerViewModel PlanController { get; private set; }
            public Object PlanItem { get; private set; }

            public PlanogramSearchResult(PlanControllerViewModel planController, Object planItem)
            {
                PlanItem = planItem;
                PlanController = planController;
            }
        }
        #endregion

        #region Binding Property Paths

        //Properties
                      
        /// <summary>
        /// Search Text provided by user
        /// </summary>
        public static readonly PropertyPath SearchTextProperty = WpfHelper.GetPropertyPath<ProductFixtureSearchViewModel>(p => p.SearchText);
        /// <summary>
        /// Number of results in the current search
        /// </summary>
        public static readonly PropertyPath NumResultsProperty = WpfHelper.GetPropertyPath<ProductFixtureSearchViewModel>(p => p.NumResults);
        /// <summary>
        /// Boolean for which type of search the user wants to perform (Product or Component)
        /// </summary>
        public static readonly PropertyPath SearchByComponentProperty = WpfHelper.GetPropertyPath<ProductFixtureSearchViewModel>(p => p.SearchByComponent);
        /// <summary>
        /// Boolean for which type of search the user wants to perform (Product or Component)
        /// </summary>
        public static readonly PropertyPath SearchByProductProperty = WpfHelper.GetPropertyPath<ProductFixtureSearchViewModel>(p => p.SearchByProduct);
        /// <summary>
        /// Boolean for if the results have become invalid
        /// </summary>
        public static readonly PropertyPath SearchNeedsRefreshingProperty = WpfHelper.GetPropertyPath<ProductFixtureSearchViewModel>(p => p.SearchNeedsRefreshing);
        /// <summary>
        /// Boolean for if the user wants to perform their search on all open planograms or just the active one
        /// </summary>
        public static readonly PropertyPath SearchOnActivePlanProperty = WpfHelper.GetPropertyPath<ProductFixtureSearchViewModel>(p => p.SearchOnActivePlan);
        /// <summary>
        /// Matchcase is a boolean that dictates if the user is searching with a matching (Upper Or Lower)case or if they dont care.
        /// </summary>
        public static readonly PropertyPath MatchCaseProperty = WpfHelper.GetPropertyPath<ProductFixtureSearchViewModel>(p => p.MatchCase);
        /// <summary>
        /// Boolean option that deturmins if all the results in the datagrid will be highlighted or just the first.
        /// </summary>
        public static readonly PropertyPath SelectAllProperty = WpfHelper.GetPropertyPath<ProductFixtureSearchViewModel>(p => p.SelectAll);
        /// <summary>
        /// Search results from the current search - Can be either Product or Component
        /// </summary>
        public static readonly PropertyPath SearchResultsProperty = WpfHelper.GetPropertyPath<ProductFixtureSearchViewModel>(p => p.SearchResults);
        /// <summary>
        /// All Highlighted search results
        /// </summary>
        public static readonly PropertyPath SelectedPositionSearchResultsProperty = WpfHelper.GetPropertyPath<ProductFixtureSearchViewModel>(p => p.HighlightedSearchResults);
        
        //Commands          

        //Command that initiates a search
        public static readonly PropertyPath SearchCommandProperty = WpfHelper.GetPropertyPath<ProductFixtureSearchViewModel>(p => p.SearchCommand);
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<ProductFixtureSearchViewModel>(p => p.CloseCommand);
        public static readonly PropertyPath AdvancedScreenOpenCommandProperty = WpfHelper.GetPropertyPath<ProductFixtureSearchViewModel>(p => p.AdvancedScreenOpenCommand);
        public static readonly PropertyPath NextResultCommandProperty = WpfHelper.GetPropertyPath<ProductFixtureSearchViewModel>(p => p.NextResultCommand);
        public static readonly PropertyPath PreviousResultCommandProperty = WpfHelper.GetPropertyPath<ProductFixtureSearchViewModel>(p => p.PreviousResultCommand);
        #endregion

        #region Properties
        /// <summary>
        /// Search text property
        /// </summary>
        public String SearchText
        {
            get { return _searchText; }
            set
            {
                _searchText = value;
                SearchNeedsRefreshing = true;
            }
        }

        /// <summary>
        /// Boolean for searching by product, also clears 
        /// the search results and updates the columns
        /// </summary>
        public Boolean SearchByProduct
        {
            get { return _searchByProduct; }
            set
            { 
                SearchResults.Clear();
                _searchByProduct = value;
                UpdateColumnSet();
                Search_Executed();
            }
        }

        /// <summary>
        /// Boolean for searching by fixture, also clears
        /// the results and updates the columns 
        /// </summary>
        public Boolean SearchByComponent
        {
            get { return _searchByComponent; }
            set
            {
                SearchResults.Clear();
               _searchByComponent = value;
                UpdateColumnSet();
                Search_Executed();
            }
        }

        /// <summary>
        /// Should the search be performed on active plan or all plans, True = Active plan/False = All open plans
        /// </summary>
        public Boolean SearchOnActivePlan
        {
            get { return _searchOnActivePlan; }
            set
            {
                _searchOnActivePlan = value;
                if(SearchText.Length >= 1)
                {                    
                    Search_Executed();
                }
            }
        }

        /// <summary>
        /// Number of results in the current search
        /// </summary>
        public int NumResults
        {
            get
            {
                return _searchResults.Count;
            }
        }

        /// <summary>
        /// Boolean for if the results have become invalid
        /// and clears the results if they are.
        /// </summary>
        public Boolean SearchNeedsRefreshing
        {
            get
            {
                return _searchNeedsRefreshing;
            }
            set
            {
                _searchNeedsRefreshing = value;
                OnPropertyChanged(SearchNeedsRefreshingProperty);
                SearchResults.Clear();
            }
        }

        /// <summary>
        /// Should the search match case when comparing strings
        /// </summary>
        public Boolean MatchCase
        {
            get { return _matchCase; }
            set
            {
                _matchCase = value;
                Search_Executed();
            }
        }

        /// <summary>
        /// Should all the results be highlighted 
        /// when the search is complete
        /// </summary>
        public Boolean SelectAll
        {
            get { return _selectAll; }
            set
            {
                _selectAll = value;               
                Search_Executed();
            }
        }

        /// <summary>
        /// Observable collection of search results, can be Product or
        /// component results dependent on user selection.
        /// </summary>
        public ObservableCollection<PlanogramSearchResult> SearchResults
        {
            get { return _searchResults; }
        }

        /// <summary>
        /// Collection of selected results to be highlighted
        /// </summary>
        public ObservableCollection<PlanogramSearchResult> HighlightedSearchResults
        {
            get { return _highlightedSearchResults; }
        }
                
        /// <summary>
        /// Returns component or product ObjectFieldInfo's depending upon
        /// if the user has selected SearchByProduct or not
        /// </summary>
        public ObservableCollection<ObjectFieldInfo> AdvancedScreenObjectFieldInfos
        {
            get
            {
                if (SearchByProduct)
                {
                    return _advancedScreenProductFieldInfos;
                }
                else 
                {
                    return _advancedScreenComponentFieldInfos;
                }
            }
        }

        public DataGridColumnCollection ResultsDataGridFieldsCollection
        {
            get { return _resultsDataGridFieldCollection; }
        }
        #endregion

        #region Constructor
       

        /// <summary>
        /// Constructor - 
        /// </summary>
        public ProductFixtureSearchViewModel(MainPageViewModel mainPageViewModel)
        {
            _mainPageViewModel = mainPageViewModel;
            _mainPageViewModel.PropertyChanged += new PropertyChangedEventHandler(OnActivePlanControllerChanged);
            _mainPageViewModel.PlanControllers.BulkCollectionChanged += PlanControllers_CollectionChanged;
            
            foreach (PlanControllerViewModel planController in _mainPageViewModel.PlanControllers)
            {
                planController.SourcePlanogram.PropertyChanged += new PropertyChangedEventHandler(SourcePlanogram_PropertyChanged);
            }
           
           _advancedScreenProductFieldInfos = ExtractObjectFieldInfos(App.ViewState.Settings.Model.SelectedColumns.Where(row => row.ColumnType == SearchColumnType.Product).ToList()).ToObservableCollection();           
           _advancedScreenComponentFieldInfos = ExtractObjectFieldInfos(App.ViewState.Settings.Model.SelectedColumns.Where(row => row.ColumnType == SearchColumnType.Component).ToList()).ToObservableCollection();
          
            if (_advancedScreenProductFieldInfos.Count == 0)
            {
                foreach (ObjectFieldInfo fieldInfo in DefaultProductSearchFields)
                {
                    _advancedScreenProductFieldInfos.Add(fieldInfo);
                }           
            }

            if (_advancedScreenComponentFieldInfos.Count == 0)
            {
                foreach (ObjectFieldInfo fieldInfo in DefaultComponentSearchFields)
                {                    
                    _advancedScreenComponentFieldInfos.Add(fieldInfo);                    
                }
            }
            UpdateColumnSet();
        }
           
        /// <summary>
        /// Event when the user selects a different plan
        /// </summary>
        private void OnActivePlanControllerChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!_supressActivePlanChanged && e.PropertyName == MainPageViewModel.ActivePlanControllerProperty.Path)
            {
                SearchNeedsRefreshing = true;
            }
        }
        
        /// <summary>
        /// Event for when the user makes a change to a plan
        /// </summary>
        private void SourcePlanogram_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsUpdating")
            {
                SearchNeedsRefreshing = true;
            }
        }

        /// <summary>
        /// Event for when the user opens or closes a plan - adds event handlers for changes made to plans
        /// </summary>
        private void PlanControllers_CollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            SearchNeedsRefreshing = true;
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (object o in e.ChangedItems)
                    {
                        ((PlanControllerViewModel)o).SourcePlanogram.PropertyChanged += new PropertyChangedEventHandler(SourcePlanogram_PropertyChanged);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (object o in e.ChangedItems)
                    {
                        ((PlanControllerViewModel)o).SourcePlanogram.PropertyChanged -= SourcePlanogram_PropertyChanged;
                    }
                    break;
                case NotifyCollectionChangedAction.Reset:
                    foreach (object o in e.ChangedItems)
                    {
                        ((PlanControllerViewModel)o).SourcePlanogram.PropertyChanged -= SourcePlanogram_PropertyChanged;
                    }
                    break;
            }
        }        

        #endregion

        #region Event Handlers

        public void OnPositionSearchResultsMouseDown(Object sender)
        {            
            if (!this.HighlightedSearchResults.Any()) return;

             PlanogramSearchResult selectedRow = this.HighlightedSearchResults.First();

            _currentHighlightedResultIndex = (Int16)this.SearchResults.IndexOf(selectedRow);

            _supressActivePlanChanged = true;
            _mainPageViewModel.ActivePlanController = selectedRow.PlanController;
             _supressActivePlanChanged = false;
            
            foreach (var selectedItem in this.HighlightedSearchResults)
            {
                selectedItem.PlanController.SelectedPlanItems.Clear();               
            }
            foreach (var selectedItem in this.HighlightedSearchResults)
            {
                IPlanItem planItem = selectedItem.PlanItem as IPlanItem;
                if(planItem == null) continue;
                selectedItem.PlanController.SelectedPlanItems.Add(planItem);
            }
        }      
        
        #endregion

        #region Commands

        #region Previous Result
        private RelayCommand _previousResultCommand;
        public RelayCommand PreviousResultCommand
        {
            get
            {
                if (_previousResultCommand == null)
                {
                    _previousResultCommand = new RelayCommand(

                       p => PreviousResult_Executed(),
                       p => PreviousResult_CanExecute())
                    {
                        SmallIcon = CommonImageResources.ColumnLayoutEditor_MoveColumnUp
                    };
                    base.ViewModelCommands.Add(_previousResultCommand);
                }
                return _previousResultCommand;
            }
        }
        private Boolean PreviousResult_CanExecute()
        {
            if (_currentHighlightedResultIndex > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void PreviousResult_Executed()
        {
            if (this._mainPageViewModel.PlanControllers.Count > 0)
            {
                ClearSearchResults();

                if (_selectAll)
                {
                    SelectAllSearchResults();
                }
                else
                {
                    //highlight each single product in turn
                    if (_searchResults.Count <= 0)
                    {
                        ShowSearchMessage(Message.ProductFixtureSearchViewModel_NoResultsFound);
                        return;
                    }

                    if (_currentHighlightedResultIndex > 0)
                    {
                        _currentHighlightedResultIndex--;

                        //set current planogram to search result planogram
                        _supressActivePlanChanged = true;
                        _mainPageViewModel.ActivePlanController = _searchResults[_currentHighlightedResultIndex].PlanController;
                        _supressActivePlanChanged = false;

                        //set selected item                            
                        _mainPageViewModel.ActivePlanController.SelectedPlanItems.Add(_searchResults[_currentHighlightedResultIndex].PlanItem as IPlanItem);
                        HighlightedSearchResults.Add(_searchResults[_currentHighlightedResultIndex]);
                    }
                    else
                    {                        
                        ShowSearchMessage(Message.ProductFixtureSearchViewModel_EndOfResults);
                        return;
                    }
                }
            }
        } 
        #endregion

        #region Next Result
        private RelayCommand _nextResultCommand;

        /// <summary>
        /// Goes to the next product
        /// </summary>
        public RelayCommand NextResultCommand
        {
            get
            {
                if (_nextResultCommand == null)
                {
                    _nextResultCommand = new RelayCommand(
                        p => NextResult_Executed(),
                        p => NextResult_CanExecute())
                    {
                        SmallIcon = CommonImageResources.ColumnLayoutEditor_MoveColumnDown,
                    };
                    base.ViewModelCommands.Add(_nextResultCommand);
                }
                return _nextResultCommand;
            }
        }
        private Boolean NextResult_CanExecute()
        {
            if (_currentHighlightedResultIndex < (_searchResults.Count)- 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void NextResult_Executed()
        {
            if (this._mainPageViewModel.PlanControllers.Count == 0) return;

            ClearSearchResults();

            if (_selectAll)
            {
                SelectAllSearchResults();
            }
            else
            {
                if (_searchResults.Count == 0)
                {
                    ShowSearchMessage(Message.ProductFixtureSearchViewModel_NoResultsFound);
                    return;
                }

                if (_currentHighlightedResultIndex < (_searchResults.Count)- 1)
                {
                    _currentHighlightedResultIndex++;

                    //set current planogram to search result planogram
                    _supressActivePlanChanged = true;
                    _mainPageViewModel.ActivePlanController = _searchResults[_currentHighlightedResultIndex].PlanController;
                    _supressActivePlanChanged = false;

                    //set selected item
                    _mainPageViewModel.ActivePlanController.SelectedPlanItems.Add(_searchResults[_currentHighlightedResultIndex].PlanItem as IPlanItem);
                    HighlightedSearchResults.Add(_searchResults[_currentHighlightedResultIndex]);
                }
                else
                {
                    ShowSearchMessage(Message.ProductFixtureSearchViewModel_EndOfResults);
                }
            }
        } 
        #endregion

        #region Select All Search Results 
        private void SelectAllSearchResults()
        {
            foreach (PlanogramSearchResult result in _searchResults)
            {
                IPlanItem newPlanItem = result.PlanItem as IPlanItem;
                if (newPlanItem == null) continue;
                result.PlanController.SelectedPlanItems.Add(newPlanItem);
                HighlightedSearchResults.Add(result);
            }
        } 
        #endregion

        #region Clear Search Results
        private void ClearSearchResults()
        {
            foreach (PlanControllerViewModel planController in _mainPageViewModel.PlanControllers)
            {
                planController.SelectedPlanItems.Clear();
                HighlightedSearchResults.Clear();
            }
        } 
        #endregion

        #region Message Service 
        private void ShowSearchMessage(String description)
        {
            CommonHelper.GetWindowService().ShowOkMessage(MessageWindowType.Warning, Message.ProductFixtureSeachViewModel_SearchResult, description);
        } 
        #endregion

        #region Open Advanced Screen
        private RelayCommand _advancedScreenOpen;

        public RelayCommand AdvancedScreenOpenCommand
        {
            get
            {
                if (_advancedScreenOpen == null)
                {
                    _advancedScreenOpen = new RelayCommand
                        (p => AdvancedScreenOpen_Executed())
                    {
                        FriendlyName = Message.ProductFixtureSearchViewModel_AdvancedScreen_Button,
                    };
                    base.ViewModelCommands.Add(_advancedScreenOpen);
                }
                return _advancedScreenOpen;
            }
        }
        private void AdvancedScreenOpen_Executed()
        {
            ProductFixtureAdvancedSearchViewModel viewModel = new ProductFixtureAdvancedSearchViewModel();
            CommonHelper.GetWindowService().ShowDialog<ProductFixtureAdvancedSearchWindow>(viewModel);
            if (viewModel.DialogResult == false) return;

            // Update the setttings with the user selection.
            IEnumerable<ProductFixtureAdvancedSearchViewModel.SelectedField> selectedFields = viewModel.SelectedProductFields.Union(viewModel.SelectedComponentFields);
            IEnumerable<UserEditorSettingsSelectedColumn> newUserSettingsSelection = selectedFields.Select(field => UserEditorSettingsSelectedColumn.NewUserEditorSettingsSelectedColumn(field.FieldPlaceHolder, field.ColumnType));
            UserEditorSettingsViewModel userSettings = App.ViewState.Settings;
            UserEditorSettingsSelectedColumnList settingsSelection = userSettings.Model.SelectedColumns;
            settingsSelection.Clear();
            settingsSelection.AddRange(newUserSettingsSelection);
            _advancedScreenProductFieldInfos = ExtractObjectFieldInfos(App.ViewState.Settings.Model.SelectedColumns.Where(row => row.ColumnType == SearchColumnType.Product).ToList()).ToObservableCollection();
            _advancedScreenComponentFieldInfos = ExtractObjectFieldInfos(App.ViewState.Settings.Model.SelectedColumns.Where(row => row.ColumnType == SearchColumnType.Component).ToList()).ToObservableCollection();

            UpdateColumnSet();
            ClearSearchResults();
        } 
        #endregion

        #region Search Command
        private RelayCommand _searchCommand;

        /// <summary>
        /// Goes to the next product
        /// </summary>
        public RelayCommand SearchCommand
        {
            get
            {
                if (_searchCommand == null)
                {
                    _searchCommand = new RelayCommand(
                        p => Search_Executed())
                    {
                        FriendlyName = Message.ProductFixtureSearchViewModel_NextProductCommand_FriendlyName,
                    };
                    base.ViewModelCommands.Add(_searchCommand);
                }
                return _searchCommand;
            }
        }
        private void Search_Executed()
        {
                SearchNeedsRefreshing = false;
                DoSearch();               
                HighlightedSearchResults.Clear();
                if (this._mainPageViewModel.PlanControllers.Count == 0) return;
               
                if (_searchByProduct)
                {
                    
                    if (_selectAll)
                    {
                        if (_searchResults.Count > 0)
                        {
                           SelectAllSearchResults();
                        }
                        else if (_searchResults.Count == 0)
                        {
                            ShowSearchMessage(Message.ProductFixtureSearchViewModel_NoResultsFound);
                            return;
                        }
                    }
                    else
                    {
                        //highlight each single product in turn
                        if (_searchResults.Count == 0)
                        {
                            ShowSearchMessage(Message.ProductFixtureSearchViewModel_NoResultsFound);
                            return;
                        }                        
                            _currentHighlightedResultIndex = 0;
                            //set current planogram to search result planogram
                            _supressActivePlanChanged = true;
                            foreach (PlanogramSearchResult result in _searchResults)
                            {
                                result.PlanController.SelectedPlanItems.Clear();
                            }
                            _mainPageViewModel.ActivePlanController = _searchResults[_currentHighlightedResultIndex].PlanController;
                            _supressActivePlanChanged = false;
                            
                            // Select currently selected search result.
                            _mainPageViewModel.ActivePlanController.SelectedPlanItems.Add(_searchResults[_currentHighlightedResultIndex].PlanItem as IPlanItem);
                            HighlightedSearchResults.Add(_searchResults[_currentHighlightedResultIndex]);                       
                    }
                }
                else
                {
                    if (_selectAll == true )
                    {
                        if(_searchResults.Count > 0)
                        {
                          SelectAllSearchResults();
                        }
                        else if(_searchResults.Count == 0)
                        {
                            ShowSearchMessage(Message.ProductFixtureSearchViewModel_NoResultsFound);
                            return;
                        }                        
                    }
                    else
                    {
                        //highlight each fixture in turn
                        if (_searchResults.Count <= 0)
                        {
                            ShowSearchMessage(Message.ProductFixtureSearchViewModel_NoResultsFound);
                            return;
                        }
                        if (_currentHighlightedResultIndex <= (_searchResults.Count - 1) && _currentHighlightedResultIndex > -1)
                        {
                            //set current planogram to search result planogram
                            _supressActivePlanChanged = true;
                            _mainPageViewModel.ActivePlanController = _searchResults[_currentHighlightedResultIndex].PlanController;
                            _supressActivePlanChanged = false;

                            //set selected item
                            _mainPageViewModel.ActivePlanController.SelectedPlanItems.Add(_searchResults[_currentHighlightedResultIndex].PlanItem as IPlanItem);
                            HighlightedSearchResults.Add(_searchResults[_currentHighlightedResultIndex]);
                        }
                        else
                        {
                           
                            ShowSearchMessage(Message.ProductFixtureSearchViewModel_EndOfResults);
                        }
                    }
                }
            
        }

        #endregion

        #region Close
        private RelayCommand _closeCommand;

        /// <summary>
        /// Closes the attached window.
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        p => Close_Executed())
                    {
                        FriendlyName = Message.Generic_Close
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #endregion
       
        #region Methods

        #region Default Component Search Fields

        public static IEnumerable<ObjectFieldInfo> DefaultComponentSearchFields
        {
            get
            {
                foreach (ObjectFieldInfo objectFieldInfo in PlanogramFieldHelper.EnumerateAllFields())
                {
                    if (objectFieldInfo.OwnerType.Equals(typeof(Planogram)) && objectFieldInfo.PropertyName.Equals(Planogram.NameProperty.Name))
                    {
                        yield return objectFieldInfo;
                    }
                    else if(objectFieldInfo.OwnerType.Equals(typeof(PlanogramComponent)) && objectFieldInfo.PropertyName.Equals(PlanogramComponent.NameProperty.Name))
                    {
                        yield return objectFieldInfo;
                    }
                }
            }
        }
        #endregion

        #region Default Product Search Fields
        public static IEnumerable<ObjectFieldInfo> DefaultProductSearchFields
        {
            get
            {
                foreach (ObjectFieldInfo objectFieldInfo in PlanogramFieldHelper.EnumerateAllFields())
                {
                    if (objectFieldInfo.OwnerType.Equals(typeof(Planogram)) && objectFieldInfo.PropertyName.Equals(Planogram.NameProperty.Name))
                    {
                        yield return objectFieldInfo;
                    }
                    else if (objectFieldInfo.OwnerType.Equals(typeof(PlanogramProduct)) && objectFieldInfo.PropertyName.Equals(Product.NameProperty.Name))
                    {
                        yield return objectFieldInfo;
                    }
                    else if (objectFieldInfo.OwnerType.Equals(typeof(PlanogramProduct)) && objectFieldInfo.PropertyName.Equals(Product.GtinProperty.Name))
                    {
                        yield return objectFieldInfo;
                    }
                    else if (objectFieldInfo.OwnerType.Equals(typeof(PlanogramProduct)) && objectFieldInfo.PropertyName.Equals(Product.BrandProperty.Name))
                    {
                        yield return objectFieldInfo;
                    }
                }
                yield break;
            }
        } 
        #endregion

        #region Do Search
        /// <summary>
        /// Perform the search
        /// </summary>
        private void DoSearch()
        {
            _searchResults.Clear();

            if (_searchByProduct == true && SearchText != String.Empty)
            {
                if (_searchOnActivePlan)
                {
                    DoProductSearch(_mainPageViewModel.ActivePlanController);
                }
                else
                {
                    foreach (PlanControllerViewModel plan in _mainPageViewModel.PlanControllers)
                    {
                        DoProductSearch(plan);
                    }
                }
            }
            if (_searchByComponent == true && SearchText != String.Empty)
            {
                if (_searchOnActivePlan)
                {
                    DoComponentSearch(_mainPageViewModel.ActivePlanController);
                }
                else
                {
                    foreach (PlanControllerViewModel plan in _mainPageViewModel.PlanControllers)
                    {
                        DoComponentSearch(plan);
                    }
                }
            }
            OnPropertyChanged(NumResultsProperty);
        } 
        #endregion

        #region Product Search
        private void DoProductSearch(PlanControllerViewModel plan)
        {
            foreach (PlanogramPositionView planogramPosition in plan.SourcePlanogram.EnumerateAllPositions())
            {
                //Uses the "GetMatchingProductFieldResults" method to return the fields that matched as well as the list of FieldGroups they had.
                PlanogramSearchResult planogramSearchResult;

                IEnumerable<IGrouping<Type, ObjectFieldInfo>> matchingGroups =
                    GetMatchingProductFieldResults(plan, planogramPosition, out planogramSearchResult);

                if (planogramSearchResult == null) continue;

                FindDataForProductResultFieldsThatDidNotMatch(planogramPosition, planogramSearchResult, matchingGroups);
            }
        } 
        #endregion

        #region Find Data For Product Result Fields That Did't Match
        private void FindDataForProductResultFieldsThatDidNotMatch(
           PlanogramPositionView planogramPosition, PlanogramSearchResult planogramSearchResult, IEnumerable<IGrouping<Type, ObjectFieldInfo>> matchingGroups)
        {
            foreach (IGrouping<Type, ObjectFieldInfo> fieldGroup in _advancedScreenProductFieldInfos.GroupBy(o => o.OwnerType))
            {
                if (matchingGroups.Contains(fieldGroup)) continue;
                Object searchObject = GetProductPositionSearchObjectFromFieldGroup(planogramPosition, fieldGroup);

                foreach (ObjectFieldInfo fieldGroupObjectFieldInfo in fieldGroup)
                {
                    AddResultIfFieldMatches(planogramPosition, planogramSearchResult, searchObject, fieldGroupObjectFieldInfo);
                }
            }
        } 
        #endregion

        #region Add Product Result If FieldMatches
        private static void AddResultIfFieldMatches(PlanogramPositionView planogramPosition,
            PlanogramSearchResult planogramSearchResult, Object searchObject, ObjectFieldInfo fieldGroupObjectFieldInfo)
        {
            if (fieldGroupObjectFieldInfo.PropertyName.Equals(Planogram.NameProperty.Name)) return;
            PropertyInfo fieldInfoProperty = searchObject.GetType().GetProperty(fieldGroupObjectFieldInfo.PropertyName);

            if (fieldGroupObjectFieldInfo.PropertyName.Contains(PlanogramProduct.CustomAttributesProperty.Name))
            {
                String propertyPattern = String.Format(@"{0}\.(\w+)", PlanogramProduct.CustomAttributesProperty.Name);
                if (!Regex.IsMatch(fieldGroupObjectFieldInfo.PropertyName, propertyPattern)) return;
                String propertyName = Regex.Match(fieldGroupObjectFieldInfo.PropertyName, propertyPattern).Groups[1].Value;

                searchObject = planogramPosition.Product.CustomAttributes;
                fieldInfoProperty = searchObject.GetType().GetProperty(propertyName);
            }

            if (fieldGroupObjectFieldInfo.PropertyName.Contains(PlanogramProduct.AssortmentFieldPrefix))
            {
                String propertyPattern = String.Format(@"{0}(\w+)", PlanogramProduct.AssortmentFieldPrefix);
                if (!Regex.IsMatch(fieldGroupObjectFieldInfo.PropertyName, propertyPattern)) return;
                String propertyName = Regex.Match(fieldGroupObjectFieldInfo.PropertyName, propertyPattern).Groups[1].Value;

                searchObject = planogramPosition.Product.AssortmentProduct;
                Type objectType = searchObject.GetType();
                fieldInfoProperty = searchObject.GetType().GetProperty(propertyName);
            }

            if (fieldGroupObjectFieldInfo.PropertyName.Contains(PlanogramPerformance.PerformanceDataProperty.Name))
            {
                String propertyPattern = String.Format(@"{0}\.(\w+)", PlanogramPerformance.PerformanceDataProperty.Name);
                if (!Regex.IsMatch(fieldGroupObjectFieldInfo.PropertyName, propertyPattern)) return;
                String propertyName = Regex.Match(fieldGroupObjectFieldInfo.PropertyName, propertyPattern).Groups[1].Value;

                searchObject = planogramPosition.Product.PerformanceData;
                fieldInfoProperty = searchObject.GetType().GetProperty(propertyName);
            }
            if (fieldInfoProperty == null) return;
            Object fieldInfoPropertyValue = fieldInfoProperty.GetValue(searchObject, null);

            if (!planogramSearchResult.ContainsKey(fieldGroupObjectFieldInfo.FieldFriendlyName))
            {
                planogramSearchResult.Add(fieldGroupObjectFieldInfo.FieldFriendlyName, fieldInfoPropertyValue);
            }
        } 
        #endregion

        #region Get Matching Product Field Results
        private IEnumerable<IGrouping<Type, ObjectFieldInfo>> GetMatchingProductFieldResults(
           PlanControllerViewModel plan, PlanogramPositionView planogramPosition, out PlanogramSearchResult planogramSearchResult)
        {
            planogramSearchResult = null;
            List<IGrouping<Type, ObjectFieldInfo>> fieldGroupsThatMatched = new List<IGrouping<Type, ObjectFieldInfo>>();
            foreach (IGrouping<Type, ObjectFieldInfo> fieldGroup in _advancedScreenProductFieldInfos.GroupBy(o => o.OwnerType))
            {
                // if the objectfieldInfo is of a specific type(e.g. product) then - object = that type 
                Object searchObject = GetProductPositionSearchObjectFromFieldGroup(planogramPosition, fieldGroup);
                if (searchObject == null) continue;

                foreach (ObjectFieldInfo searchFieldToMatch in fieldGroup)
                {
                    if(searchFieldToMatch.OwnerType.Equals(typeof(Planogram)) && searchFieldToMatch.PropertyName.Equals(Planogram.NameProperty.Name)) continue;
                    
                    PropertyInfo searchFieldToMatchProperty = searchObject.GetType().GetProperty(searchFieldToMatch.PropertyName);

                    if (searchFieldToMatch.PropertyName.Contains(PlanogramProduct.CustomAttributesProperty.Name))
                    {
                        String propertyPattern = String.Format(@"{0}\.(\w+)", PlanogramProduct.CustomAttributesProperty.Name);
                        if (!Regex.IsMatch(searchFieldToMatch.PropertyName, propertyPattern)) continue;
                        String propertyName = Regex.Match(searchFieldToMatch.PropertyName, propertyPattern).Groups[1].Value;

                        searchObject = planogramPosition.Product.CustomAttributes;
                        searchFieldToMatchProperty = searchObject.GetType().GetProperty(propertyName);
                    }

                    if (searchFieldToMatch.PropertyName.Contains(PlanogramProduct.AssortmentFieldPrefix))
                    {
                        String propertyPattern = String.Format(@"{0}(\w+)", PlanogramProduct.AssortmentFieldPrefix);
                        if (!Regex.IsMatch(searchFieldToMatch.PropertyName, propertyPattern)) continue;
                        String propertyName = Regex.Match(searchFieldToMatch.PropertyName, propertyPattern).Groups[1].Value;

                        searchObject = planogramPosition.Product.AssortmentProduct;
                        Type objectType = searchObject.GetType();
                        searchFieldToMatchProperty = searchObject.GetType().GetProperty(propertyName);
                    }

                    if (searchFieldToMatch.PropertyName.Contains(PlanogramPerformance.PerformanceDataProperty.Name))
                    {
                        String propertyPattern = String.Format(@"{0}\.(\w+)", PlanogramPerformance.PerformanceDataProperty.Name);
                        if (!Regex.IsMatch(searchFieldToMatch.PropertyName, propertyPattern)) continue;
                        String propertyName = Regex.Match(searchFieldToMatch.PropertyName, propertyPattern).Groups[1].Value;

                        searchObject = planogramPosition.Product.PerformanceData;
                        searchFieldToMatchProperty = searchObject.GetType().GetProperty(propertyName);
                    }

                    if (searchFieldToMatchProperty == null) continue;
                    Object searchFieldToMatchPropertyValue = searchFieldToMatchProperty.GetValue(searchObject, null);

                    if (!SearchTextAndValueMatch(searchFieldToMatchPropertyValue)) continue;


                    if (planogramSearchResult == null)
                    {
                        planogramSearchResult = new PlanogramSearchResult(plan, planogramPosition);
                        SearchResults.Add(planogramSearchResult);
                    }
                    planogramSearchResult.Add(searchFieldToMatch.FieldFriendlyName, searchFieldToMatchPropertyValue);
                    fieldGroupsThatMatched.Add(fieldGroup);

                    foreach (ObjectFieldInfo otherField in fieldGroup.Where(f => f != searchFieldToMatch))
                    {
                        PropertyInfo otherFieldProperty = searchObject.GetType().GetProperty(otherField.PropertyName);
                        if (otherFieldProperty == null) continue;
                        Object otherFieldPropertyValue = otherFieldProperty.GetValue(searchObject, null);

                        if (!planogramSearchResult.ContainsKey(otherField.FieldFriendlyName))
                        {
                            planogramSearchResult.Add(otherField.FieldFriendlyName, otherFieldPropertyValue);

                            if (!fieldGroupsThatMatched.Contains(fieldGroup))
                            {
                                fieldGroupsThatMatched.Add(fieldGroup);
                            }
                        }
                    }
                    break;
                }
            }
            return fieldGroupsThatMatched;
        } 
        #endregion

        #region Get Product Position Search Object from Field Group
        private static Object GetProductPositionSearchObjectFromFieldGroup(PlanogramPositionView planogramPosition, IGrouping<Type, ObjectFieldInfo> fieldGroup)
        {
            Object searchObject = null;

            if (fieldGroup.Key == typeof(Planogram))
            {
                searchObject = planogramPosition.Planogram;
            }
            else if (fieldGroup.Key == typeof(PlanogramProduct))
            {
                searchObject = planogramPosition.Product.Model;

            }
            else if (fieldGroup.Key == typeof(PlanogramPosition))
            {
                searchObject = planogramPosition.Model;
            }
            else if (fieldGroup.Key == typeof(PlanogramFixture) || fieldGroup.Key == typeof(PlanogramFixtureItem))
            {
                searchObject = planogramPosition.Fixture;

            }
            else if (fieldGroup.Key == typeof(PlanogramComponent) || fieldGroup.Key == typeof(PlanogramFixtureComponent))
            {
                searchObject = planogramPosition.Component;
            }

            else if (fieldGroup.Key == typeof(PlanogramSubComponent))
            {
                searchObject = planogramPosition.SubComponent;

            }
            else
            {
                searchObject = null;
            }
            return searchObject;
        } 
        #endregion

        #region Do Component Search
        private void DoComponentSearch(PlanControllerViewModel plan)
        {
            foreach (PlanogramComponentView componentPosition in plan.SourcePlanogram.EnumerateAllComponents())
            {
                PlanogramSearchResult planogramSearchResult;

                IEnumerable<IGrouping<Type, ObjectFieldInfo>> matchingGroups = GetMatchingComponentField(plan, componentPosition, out planogramSearchResult);
                if (planogramSearchResult == null) continue;

                FindDataForComponentResultFieldsThatDidntMatch(componentPosition, planogramSearchResult, matchingGroups);
            }
        } 
        #endregion

        #region Find data for Component Result Fields that didn't match
        private void FindDataForComponentResultFieldsThatDidntMatch(PlanogramComponentView componentPosition, PlanogramSearchResult planogramSearchResult, IEnumerable<IGrouping<Type, ObjectFieldInfo>> matchingGroups)
        {
            foreach (IGrouping<Type, ObjectFieldInfo> fieldGroup in _advancedScreenComponentFieldInfos.GroupBy(o => o.OwnerType))
            {
                List<Object> searchObjects = GetComponentPositionSearchObject(componentPosition, fieldGroup);

                if (matchingGroups.Contains(fieldGroup)) continue;

                foreach (ObjectFieldInfo fieldGroupObjectFieldInfo in fieldGroup)
                {
                    if (fieldGroupObjectFieldInfo.PropertyName.Equals(Planogram.NameProperty.Name)) continue;

                    foreach (object searchObject in searchObjects)
                    {
                        PropertyInfo fieldInfoProperty = searchObject.GetType().GetProperty(fieldGroupObjectFieldInfo.PropertyName);
                        if (fieldInfoProperty == null) continue;
                        Object fieldInfoPropertyValue = fieldInfoProperty.GetValue(searchObject, null);

                        if (!planogramSearchResult.ContainsKey(fieldGroupObjectFieldInfo.FieldFriendlyName))
                        {
                            planogramSearchResult.Add(fieldGroupObjectFieldInfo.FieldFriendlyName, fieldInfoPropertyValue);
                        }
                    }
                }
            }
        } 
        #endregion

        #region Get Matching Component Field
        private IEnumerable<IGrouping<Type, ObjectFieldInfo>> GetMatchingComponentField(
        PlanControllerViewModel plan, PlanogramComponentView componentPosition, out PlanogramSearchResult planogramSearchResult)
        {
            planogramSearchResult = null;
            List<IGrouping<Type, ObjectFieldInfo>> fieldGroupsThatMatched = new List<IGrouping<Type, ObjectFieldInfo>>();

            foreach (IGrouping<Type, ObjectFieldInfo> fieldGroup in _advancedScreenComponentFieldInfos.GroupBy(o => o.OwnerType))
            {
                // if the objectfieldInfo is of a specific type(e.g. product) then - object = that type 

                List<Object> searchObjects = GetComponentPositionSearchObject(componentPosition, fieldGroup);
                if (searchObjects == null) continue;
                foreach (ObjectFieldInfo searchFieldToMatch in fieldGroup)
                {
                    if (searchFieldToMatch.OwnerType.Equals(typeof(Planogram)) && searchFieldToMatch.PropertyName.Equals(Planogram.NameProperty.Name)) continue;
                    foreach (Object searchObject in searchObjects)
                    {
                        PropertyInfo searchFieldToMatchProperty = searchObject.GetType().GetProperty(searchFieldToMatch.PropertyName);

                        if (searchFieldToMatchProperty == null) continue;
                        Object searchFieldToMatchPropertyValue = searchFieldToMatchProperty.GetValue(searchObject, null);

                        if (!SearchTextAndValueMatch(searchFieldToMatchPropertyValue)) continue;

                        if (planogramSearchResult == null)
                        {
                            planogramSearchResult = new PlanogramSearchResult(plan, componentPosition);
                            SearchResults.Add(planogramSearchResult);
                        }

                        if (planogramSearchResult.ContainsKey(searchFieldToMatch.FieldFriendlyName)) continue;

                        planogramSearchResult.Add(searchFieldToMatch.FieldFriendlyName, searchFieldToMatchPropertyValue);
                        fieldGroupsThatMatched.Add(fieldGroup);

                        foreach (ObjectFieldInfo otherField in fieldGroup.Where(f => f != searchFieldToMatch))
                        {
                            PropertyInfo otherFieldProperty = searchObject.GetType().GetProperty(otherField.PropertyName);
                            if (otherFieldProperty == null) continue;
                            Object otherFieldPropertyValue = otherFieldProperty.GetValue(searchObject, null);

                            if (!planogramSearchResult.ContainsKey(otherField.FieldFriendlyName))
                            {
                                planogramSearchResult.Add(otherField.FieldFriendlyName, otherFieldPropertyValue);

                                if (!fieldGroupsThatMatched.Contains(fieldGroup))
                                {
                                    fieldGroupsThatMatched.Add(fieldGroup);
                                }
                            }
                        }
                        break;
                    }
                }

            }
            return fieldGroupsThatMatched;
        } 
        #endregion

        #region Get Component Position Search Object
        private static List<Object> GetComponentPositionSearchObject(PlanogramComponentView componentPosition, IGrouping<Type, ObjectFieldInfo> fieldGroup)
        {
            List<Object> searchObjects = new List<Object>();

            if (fieldGroup.Key == typeof(Planogram))
            {
                searchObjects.Add(componentPosition.Planogram);
            }
            else if (fieldGroup.Key == typeof(PlanogramProduct))
            {
                searchObjects.AddRange(componentPosition.SubComponents.SelectMany(s => s.Positions).Select(p => p.Product));

            }
            else if (fieldGroup.Key == typeof(PlanogramPosition)) //productview may be incorrect check in a few (otherwise tick)
            {
                searchObjects.AddRange(componentPosition.SubComponents.SelectMany(s => s.Positions));
            }
            else if (fieldGroup.Key == typeof(PlanogramFixture) || fieldGroup.Key == typeof(PlanogramFixtureItem))
            {
                searchObjects.Add(componentPosition.Fixture);

            }
            else if (fieldGroup.Key == typeof(PlanogramComponent) || fieldGroup.Key == typeof(PlanogramFixtureComponent))
            {
                searchObjects.Add(componentPosition.Component);
            }

            else if (fieldGroup.Key == typeof(PlanogramSubComponent))
            {
                searchObjects.AddRange(componentPosition.SubComponents);

            }
            else
            {
                searchObjects = null;
            }
            return searchObjects;
        } 
        #endregion

        #region Extract ObjectFieldInfos
        /// <summary>
        /// Extracts Object field info using the fieldplaceholders in a list, and seperates them based on type.
        /// </summary>
        /// <param name="givenList"></param>
        private IEnumerable<ObjectFieldInfo> ExtractObjectFieldInfos(List<UserEditorSettingsSelectedColumn> givenList)
        {
            foreach (UserEditorSettingsSelectedColumn column in givenList)
            {
                foreach (ObjectFieldInfo item in ObjectFieldInfo.ExtractFieldsFromText(column.FieldPlaceHolder, _masterFieldInfoList))
                {
                    yield return item;
                }
            }
            yield break;
        } 
        #endregion

        #region SearchText and Value Match
        private Boolean SearchTextAndValueMatch(Object value)
        {             
            if (SearchText != null && value != null)
            {
                try
                {
                    if (value.GetType().Equals(typeof(String)))
                    {
                        if (_matchCase)
                        {
                            if (((String)value).Contains(SearchText))
                            {
                                return true;
                            }
                        }
                        else
                        {
                            if (((String)value).ToUpperInvariant().Contains(SearchText.ToUpperInvariant()))
                            {
                                return true;
                            }
                         }
                    }
                    else if (value.GetType().Equals(typeof(Int16)))
                    {
                        if (((Int16)value) == (Convert.ToInt16(SearchText)))
                        {
                            return true;
                        }
                    }
                    else if (value.GetType().Equals(typeof(Int32)))
                    {
                        if (((Int32)value) == (Convert.ToInt32(SearchText)))
                        {
                            return true;
                        }
                    }
                    else if (value.GetType().Equals(typeof(Int64)))
                    {
                        if (((Int64)value) == (Convert.ToInt64(SearchText)))
                        {
                            return true;
                        }
                    }
                    else if (value.GetType().Equals(typeof(Single)))
                    {
                        if (((Single)value).EqualTo((Convert.ToSingle(SearchText))))
                        {
                            return true;
                        }
                    }
                    else if (value.GetType().Equals(typeof(Double)))
                    {
                        if (((Double)value).EqualTo((Convert.ToDouble(SearchText))))
                        {
                            return true;
                        }
                    }
                    else if (value.GetType().Equals(typeof(Boolean)))
                    {
                        if (((Boolean)value).Equals(Convert.ToBoolean(SearchText)))
                        {
                            return true;
                        }
                    }
                    else if (value.GetType().Equals(typeof(DateTime)))
                    {
                        //convert to string as this way the user doesnt have to be overly specific with the date time they are looking for.
                        if ((value).ToString().Contains(SearchText)) 
                        {
                            return true;
                        }
                    }
                    else if (value.GetType().Equals(typeof(Byte)))
                    {
                        if (((Byte)value) == (Convert.ToByte(SearchText)))
                        {
                            return true;
                        }
                    }
                    else if (value.GetType().IsEnum)    
                    {
                        String friendlyNameForComparison;
                        Type enumType = value.GetType();  //Reprisents the Enum Type
                        String enumText = Convert.ToString(value); //Converts the Enum to a string, so for example "1"
                        Object enumValue; // Object to be set during the course of this procedure.
                        try
                        {
                            enumValue = Enum.Parse(enumType, enumText);//sets the enum value to the converted value of the enum, so "2" would become "Enum_Value_3" 
                        }
                        catch (ArgumentException) // protects against arguments being wrong
                        {
                            enumValue = null;
                            return false;
                        }
                        catch (OverflowException) //protects against an infinate loop 
                        {
                            enumValue = null;
                            return false;
                        }
                        Type enumHelperType = Assembly.GetAssembly(enumType).GetType(String.Format("{0}Helper", enumType.FullName), throwOnError: false); //tries to get the helper enums friendly name using the enumType, which throws "False" if it doesnt work.
                        if (enumHelperType == null)
                        {
                            friendlyNameForComparison = enumText;
                        }
                        else
                        {
                            FieldInfo friendlyNamesField = enumHelperType.GetField("FriendlyNames", BindingFlags.Static | BindingFlags.Public);  //Finds the friendly names field for this enum helper type
                            if (friendlyNamesField == null)
                            {
                                friendlyNameForComparison = enumText;
                            }
                            else
                            {
                                IDictionary friendlyNamesDictionary = friendlyNamesField.GetValue(null) as IDictionary;  //The friendlyNames that where gathered from the enumHelper are now added to a dictionary 
                                if (friendlyNamesDictionary == null)
                                {
                                    friendlyNameForComparison = enumText;
                                }
                                else
                                {
                                    if (friendlyNamesDictionary.Contains(enumValue)) //Checks to see if the dictionary that was created contains the enumValue (parsed above)
                                    {
                                        String friendlyName = friendlyNamesDictionary[enumValue] as String; // new string friendly name becomes the value found in the dictionary (using enumValue) but as a string for comparison :)
                                        if (friendlyName == null)
                                        {
                                            friendlyNameForComparison = enumText;
                                        }
                                        else
                                        {
                                            friendlyNameForComparison = friendlyName; //previously created friendlynameForComparison is given the friendlyName value
                                        }
                                    }
                                    else
                                    {
                                        friendlyNameForComparison = enumText;
                                    }
                                }
                            }

                        }

                        if (MatchCase)
                        {
                            if (friendlyNameForComparison.Contains(Convert.ToString(SearchText)))   //compares the friendlyname we have just gained against the text the user entered, to be true the user text only has to contain some of the same words/letters
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            if (friendlyNameForComparison.ToLowerInvariant().Contains(Convert.ToString(SearchText).ToLowerInvariant()))  //again compares but this time lowers the text case so that capitals no longer matter to the search,
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (InvalidCastException)
                {
                    return false;
                }
                catch (FormatException)
                {
                    return false;
                }
                catch (OverflowException)
                {
                    return false;
                }
            } 
            
            return false;
        }
        #endregion 
      
        #region Update Columns
        private void UpdateColumnSet()
        {
            _resultsDataGridFieldCollection.Clear();
            foreach (ObjectFieldInfo item in AdvancedScreenObjectFieldInfos)
            {
               _resultsDataGridFieldCollection.Add(FieldToDataGridColumn(item));
            }
        }

        private DataGridColumn FieldToDataGridColumn(ObjectFieldInfo field)
        {

            String bindingPath;
            if (field.OwnerType == typeof(Planogram) && field.PropertyName.Equals(Planogram.NameProperty.Name))
            {
                bindingPath = "PlanController.SourcePlanogram.Name";
            }
            else
            {
                bindingPath = String.Format("[{0}]", field.FieldFriendlyName);
            }
            return CommonHelper.CreateReadOnlyColumn(bindingPath, typeof(String), field.FieldFriendlyName, Framework.Model.ModelPropertyDisplayType.None, App.ViewState.DisplayUnits);
        }
        #endregion 
        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    foreach (PlanControllerViewModel planController in _mainPageViewModel.PlanControllers)
                    {
                        planController.SourcePlanogram.PropertyChanged -= SourcePlanogram_PropertyChanged;
                    }
                    _mainPageViewModel.PropertyChanged -= OnActivePlanControllerChanged;
                    _searchResults.Clear();
                  
                    base.IsDisposed = true;
                }
            }
        }
        #endregion
    }
}
