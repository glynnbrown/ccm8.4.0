﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM800
// L.Hodson ~ Created.
// V8-23726 : N.Haywood
//  Added searching products and fixture components
// V8-25042 : D.Pleasance
//  Added OpenFromRepository
// V8-25395 : A.Probyn
//  Added ProductLibrary related commands
// V8-25042 : A.Silva
//      Modified OpenFromRepository to show PlanRepositoryOrganiser
// V8-25721 : A.Silva
//      Added Prompt for connection when opening a recent planogram if there is no active connection.
// V8-25751 : A.Silva
//      Implemented code to Manage Repositories from the Setup Backstage.
// V8-25753 : A.Silva
//      Implemented code for the Open File split button.
// V8-25514 : A.Probyn
//      Added extra code to AddToFixtureLibrary_Executed for different types/orders of selections
// V8-25584: A.Probyn
//      Changed AddToFixtureLibrary_Executed to check for taken names in sub folders too.
// V8-25873 : A.Probyn
//      Updated fixture related commands
// V8-25616 : A.Probyn
//      Wrapped CreateAssembly_Executed operations in an undo able action.
// V8-25797 : A.Silva ~ Updated ShowColumnLayoutEditorCommand to show a tool tip
// V8-26491 : A.Kuszyk
//      Added NewAssortmentView command, Add/Remove Assortment Product commands and Select Assortment command.
// V8-26685 : A.Silva ~ Added ShowValidationTemplateCommand.
// V8-26704 : A.Kuszyk
//      Updated the SelectAssortment Command.
// V8-26860 : L.Luong
//      Added EventLog command
// V8-26702 : A.Kuszyk
//  Changed references Product Library Panel ViewModel to App.ViewState.ProductLibraryView.
// V8-26956 : L.Luong
//      Added ConsumerDecisionTree
// V8-26891 : L.Ineson
//  Added Blocking
// V8-27120 : A.Kuszyk
//  Updated use of Product Selection windows in Assortment commands.
// V8-27177 : L.Ineson
//  Moved assortment commands into assortment plan document.
// V8-27178 : L.Luong
//  Moved consumer decision tree commands to consumer decision tree plan document
// V8-25316 : L.Ineson
//  Can no longer group as assembly if a backboard or base is selected
// V8-25985 : M.Shelley
//  Added OpenFromRepositoryCommand_CanExecute method to doable the Repository button when there is
//  no current repository connection
// V8-27569 : A.Probyn
//  Added CopyToClipboard command
// V8-27804 : L.Ineson
//  Removed import planogram command
// V8-27648 : A.Probyn ~ Updated to support ProductLibraryProduct on ProductLibrary commands
// V8-28091 : L.Luong
//  Changed ScreenshotCommand to screen shot other DocumentViews
// V8-27938 : N.Haywood
//  Added data sheets
// V8-26970 : A.Probyn
//  Updated ProductLibraryMaintenance_Executed to checked for existing window.
// V8-27774 : A.Probyn
//  Updated CopyToClipboard_Executed to hide the scroll bars before copying screen shot to clipboard.
// V8-28465 : J.Pickup
//  Save, Open, SaveToRepository, OpenFromRepository now requires the user to have Planogram Permissions & Disabled reasons now Displayed.// V8-28465 : J.Pickup
// V8-28473 : J.Pickup
//  Fill high, deep & Wide are now an undo-able actions.
// V8-28601 : A.Kuszyk
//  Made Planogram permissions dynamically fetched.
// V8-25939 : J.Pickup
//  Reviewed commands disabled reasons and friendlydescriptions. 
#endregion

#region Version History: CCM801
// V8-27897 : A.Kuszyk
//  Added UpdatePlanogramProducts command.
// V8-27554 : I.George 
//  Added SmallIcon Property to commands where they are missing.
// V8-28676 : A.silva
//      Added Image Commands.
// V8-28786 : I.George
//      Updated SmallIcon property to use 16 x 16 icon size
// V8-28684 : A.Kuszyk
//  Removed readonly disabled reason for Save Planogram command.
// V8-28837 : A.Kuszyk
//  Removed merch strategy restrictions to fill high/wide/deep commands.
#endregion

#region Version History: CCM802
// V8-25378 : M.Pettit
//  Added DockAllWindows command.
// V8-25995 : M.Pettit
//  Added SnapComponentToNotch button
// V8-29030 : J.Pickup
//  UpdatePlanogramProducts_Executed now handles source type ProductPerformanceData
// V8-25955 : D.Pleasance
//  Brought back in, AddNewPosition command
// V8-29037 : D.Pleasance
//  Amended Paste command so that this is only available if the items in the clipboard to be pasted are positions 
//  whilst also checking that a single component is selected.
// V8-29366 : D.Pleasance
//  Amended DoAddImagesToPlanogram so that Begin \ End UndoableAction is called around the baking of images into the plan.
#endregion

#region Version History: CCM803
// V8-29607 : L.Ineson
//  RemoveImageCommand can executes no longer trigger images to load immediately.
// V8-29046 : L.Luong
//  Added Update All Planograms Products Command
#endregion

#region Version History: CCM810
// V8-28766 : J.Pickup
//  FillSelectedPositionsOnGivenAxis() Introduced so that fill wide, high and deep now uses the autofill logic to fill the selected positions.
// V8-30103 : J.Pickup
//  Merchspace is shared and now pased down into isOverfilled for performance.
// V8-31057 : A.Probyn
//  Added validation to AddToFixtureLibrary_Executed to ensure the source fixture package is valid itself (old versions may not be).
// V8-30103 : N.Foster
//  Autofill performance enhancements
// V8-30054 : L.Ineson
//  Refactored fill commands so that the plan gets updated properly.
#endregion

#region Version History: CCM811
// V8-29406 : N.Haywood
//  Added file type validation for OpenFileDialog
// V8-30530 : A.Probyn
//  ROLLED BACK : Removed Updated Copy_Executed so that items are copied in the order of selection. 
// V8-28688 : A.Probyn
//  Updated reference to PositionPropertiesViewModel to take a new constructor depending on where it is called from.
#endregion

#region Version History: CCM820
// V8-30738 : L.Ineson
//  Added ShowPrintTemplateSetup.
// V8-30794 : A.Kuszyk
//  Added ApplyHighlightAsProductColour Command.
// V8-30795 : A.Kuszyk
//  Updated Copy_Executed to copy in selection order.
// V8-30890 : A.Kuszyk
//  Added ApplyHighlightAsProductColourToSelection command.
// V8-30932 : L.Ineson
//  Added commands for the new design tab.
// V8-31106 : L.Ineson
//  Remove unmerchandised components now only removes them from assemblies if all
// of the merchandisable assembly components will be removed.
// V8-31286 : A.Silva
//  Remove unmerchadised components now considers combined components when removing. 
//  If any combined component has positions, none of its combined components will be considered unmerchandised.
// V8-31430 : D.Pleasance
//  Amended Options_Executed() to call UpdateAppSettings()
#endregion

#region Version History: CCM830

// V8-31557 : M.Shelley
//  Add the functionality to append a selected planogram to the right of the current planogram.
//  Add the ability to append a plan from a POG file.
// V8-31612 : A.Kuszyk
//  Added wait cursor to Update Product Attributes command.
// V8-31742 : A.Silva
//  Added NewPlanogramCompareView Command.
// V8-31541 : L.Ineson
//  Updated show report editor.
// V8-31762 : M.Shelley
//  Add a check for AppendPlanogram to check if the repository is disconnected and disable the 
//  "append from repository" button / command.
// V8-31834 : L.Ineson
//  Added NewCategoryInsightView command.
// V8-31547  : M.Pettit
//  Added ExportPlanogramToFile command
// V8-32277 : L.Ineson
//  Swapped over can execute planogram/package is valid checks.
//V8-32188 : L.Ineson
//  Made sure that fill high wide and deep get disabled if the position is on a manual strategy component.
//V8-32389 : J.Pickup
//  Added keyboard shortcut functionality to toggle the set highlight, product and fixture labels inc new commands. 
//V8-32415 : J.Pickup
//  Added keyboard shortcut functionality to set units wide.
// V8-32518 : N.Haywood
//  Added paste to all plans
// V8-32523 : L.Ineson
//  Added LinkAnnotation and UnlinkAnnotation.
// V8-32584 : N.Haywood
//  Fixed issue where pasting to all would crash when having different components selected
//V8-32361 : L.Ineson
//  Updated show product library maintenance to load the currently selected library.
//V8-32359  : J.Pickup
//  Product attribute vm constructor change. 
// V8-32685 : N.Haywood
//  Removed dependency on selecting a fixture for paste to all
// V8-32359 : A.Silva
//  Removed change to Product Attribute Selector View Model constructor as there is no default value column anymore.
// V8-32700 : A.Silva
//  Added new button for Update Planogram From Product Attribute and its commands.
// V8-32359 : A.Silva
//  Added UpdatePlanogramFromProductAttributeCommand and UpdateAllPlanogramsFromProductAttributeCommand.
// V8-32536 : L.Ineson
// DecreasePositionFacings is now disabled when there are no facings left to remove.
// V8-32819 : M.Pettit
//  Search window now uses WindowService, is now non-modal, stays on top and is displayed in taskbar
// V8-32795 : L.Bailey
//  Dragging bays between plans mixes up products
// V8-32867 : M.Brumby
//  Added bearbones of process key up.
#endregion

#region Version History: CCM832

// CCM18936 : G.Richards
//  Prevented an unexpected error when the plan has no active or selected view. Also when no view is active or selected the correct buttons are disabled.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.PrintTemplateSetup;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;
using Galleria.Ccm.Editor.Client.Wpf.PlanRepository;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Editor.Client.Wpf.ProductLibraryMaintenance;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Editor.Client.Wpf.Startup;
using Galleria.Ccm.Editor.Client.Wpf.ValidationTemplateEditor;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Enums;
using Galleria.Framework.Imports;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using CommonImageResources = Galleria.Ccm.Common.Wpf.Resources.ImageResources;
using CommonMessage = Galleria.Ccm.Common.Wpf.Resources.Language.Message;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{

    /// <summary>
    /// Contains references to commands that are to be made available to the main page and all its children.
    /// </summary>
    public sealed class MainPageCommands
    {
        #region Singleton Pattern

        private static readonly MainPageCommands Instance = new MainPageCommands();

        static MainPageCommands() { }

        private MainPageCommands()
        {
            AssignKeyboardShortcuts();
        }

        #endregion

        #region Fields

        private readonly ObservableCollection<IRelayCommand> _viewModelCommands = new ObservableCollection<IRelayCommand>();
        private readonly List<IPlanItem> _clipboardList = new List<IPlanItem>();
        private readonly List<IPlanItem> _dragClipboardList = new List<IPlanItem>();

        #endregion

        #region Properties


        private Boolean _isInPlanogramGetRole
        {
            get { return ApplicationContext.User.IsInRole(DomainPermission.PlanogramGet.ToString()); }
        }

        private Boolean _isInPlanogramEditRole
        {
            get { return ApplicationContext.User.IsInRole(DomainPermission.PlanogramEdit.ToString()); }
        }

        private Boolean _isInPlanogramCreateRole
        {
            get { return ApplicationContext.User.IsInRole(DomainPermission.PlanogramCreate.ToString()); }
        }


        /// <summary>
        /// Returns the main page view model.
        /// </summary>
        public static MainPageViewModel MainViewModel
        {
            get { return App.MainPageViewModel; }
        }

        /// <summary>
        /// Returns the currently active plan controller
        /// </summary>
        public static PlanControllerViewModel ActivePlanController
        {
            get
            {
                return MainViewModel == null
                           ? null
                           : MainViewModel.ActivePlanController;
            }
        }

        private static Boolean NoActivePlanogram { get { return ActivePlanController == null || ActivePlanController.SourcePlanogram == null; } }

        #endregion

        #region Commands

        #region NewPlanogram

        /// <summary>
        /// Creates a new blank planogram
        /// </summary>
        public static RelayCommand NewPlanogram
        {
            get { return Instance.NewPlanogramCommand; }
        }


        private RelayCommand _newPlanogramCommand;

        /// <summary>
        /// Creates a new blank planogram
        /// </summary>
        public RelayCommand NewPlanogramCommand
        {
            get
            {
                if (_newPlanogramCommand == null)
                {
                    _newPlanogramCommand = new RelayCommand(
                        p => NewPlanogram_Executed())
                    {
                        FriendlyName = Message.Generic_New,
                        FriendlyDescription = Message.Ribbon_NewPlanogram_Desc,
                        Icon = ImageResources.NewDocument_32,
                        SmallIcon = ImageResources.NewDocument_16,
                    };
                    RegisterCommand(_newPlanogramCommand);
                }
                return _newPlanogramCommand;
            }
        }

        private void NewPlanogram_Executed()
        {
            MainViewModel.CreateNewPlanogram();
        }

        #endregion

        #region OpenPlanogram

        /// <summary>
        ///     Executes <see cref="OpenFromRepository" /> if there is a connection to a repository,
        ///     executes <see cref="OpenPlanogramFromFile" /> if there is none.
        /// </summary>
        public static object OpenPlanogram
        {
            get { return Instance.OpenPlanogramCommand; }
        }

        private RelayCommand _openPlanogramCommand;

        private RelayCommand OpenPlanogramCommand
        {
            get
            {
                if (_openPlanogramCommand == null)
                {
                    _openPlanogramCommand =
                        new RelayCommand(o => OpenPlanogram_Executed())
                        {
                            FriendlyName = Message.Ribbon_OpenPlanogram,
                            FriendlyDescription = Message.Ribbon_OpenPlanogram_Desc,
                            Icon = ImageResources.Open_16,
                            SmallIcon = ImageResources.Open_16
                        };
                    RegisterCommand(_openPlanogramCommand);

                }
                return _openPlanogramCommand;
            }
        }

        private void OpenPlanogram_Executed()
        {
            //  Get the correct open command.
            //  Open from repository IF _isInPlanogramGetRole
            //      AND IsConnectedToRepository.
            //  Otherwise, open from file.
            RelayCommand openPlanogramCommand = ((_isInPlanogramGetRole && App.ViewState.IsConnectedToRepository)
                                             ? OpenFromRepositoryCommand
                                             : OpenPlanogramFromFileCommand);
            openPlanogramCommand.Execute();
        }

        #endregion

        #region OpenPlanogramFromFile

        /// <summary>
        /// Shows the open file dialog to allow selection of a plan file to load.
        /// </summary>
        public static RelayCommand OpenPlanogramFromFile
        {
            get { return Instance.OpenPlanogramFromFileCommand; }
        }


        private RelayCommand _openFromFileCommand;

        /// <summary>
        /// Shows the open file dialog to allow selection of a plan file to load.
        /// </summary>
        public RelayCommand OpenPlanogramFromFileCommand
        {
            get
            {
                if (_openFromFileCommand != null) return _openFromFileCommand;

                //  Instantiate the command if it was not so yet.
                _openFromFileCommand = new RelayCommand(OpenFromFile_Executed)
                {
                    FriendlyName = Message.Ribbon_OpenFile,
                    FriendlyDescription = Message.Ribbon_OpenFile_Desc,
                    SmallIcon = ImageResources.Open_16,
                };
                RegisterCommand(_openFromFileCommand);

                return _openFromFileCommand;
            }
        }

        private static void OpenFromFile_Executed(Object filePath)
        {
            MainViewModel.OpenPlanogramsFromFileSystem(filePath as String);
        }

        #endregion

        #region OpenFromRepository

        /// <summary>
        ///     Opens the <see cref="PlanRepositoryOrganiser"/> to open planogram from.
        /// </summary>
        public static RelayCommand OpenFromRepository
        {
            get { return Instance.OpenFromRepositoryCommand; }
        }

        /// <summary>
        ///     Reference to the current instance for <see cref="OpenFromRepositoryCommand"/>.
        /// </summary>
        private RelayCommand _openFromRepositoryCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="OpenFromRepositoryCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        private RelayCommand OpenFromRepositoryCommand
        {
            get
            {
                if (_openFromRepositoryCommand != null) return _openFromRepositoryCommand;

                //  Instantiate the command if it was not so yet.
                _openFromRepositoryCommand = new RelayCommand(p => OpenFromRepositoryCommand_Executed(),
                                                              p => OpenFromRepositoryCommand_CanExecute())
                {
                    FriendlyName = Message.Ribbon_OpenFromRepository,
                    FriendlyDescription = Message.Ribbon_OpenFromRepository_Desc,
                    SmallIcon = ImageResources.Open_16,
                };
                RegisterCommand(_openFromRepositoryCommand);

                return _openFromRepositoryCommand;
            }
        }

        private Boolean OpenFromRepositoryCommand_CanExecute()
        {
            //  Check wether we have a connection to a repository.
            if (!App.ViewState.IsConnectedToRepository)
            {
                OpenFromRepositoryCommand.DisabledReason = Message.MainPage_NoActiveConnectionToRepository;
                return false;
            }

            //  Check whether we have permission to get planograms.
            if (!_isInPlanogramGetRole)
            {
                OpenFromRepositoryCommand.DisabledReason = Message.MainPage_NoPermissionToReadRepositoryPlanograms;
                return false;
            }

            return true;
        }

        /// <summary>
        ///     
        /// </summary>
        private static void OpenFromRepositoryCommand_Executed()
        {
            WindowHelper.ShowWindow(new PlanRepositoryOrganiser(), isModal: true);
        }

        #endregion

        #region OpenFromRecent

        /// <summary>
        /// Shows the open file dialog to allow selection of a plan file to load.
        /// </summary>
        public static RelayCommand OpenPlanogramFromRecent
        {
            get { return Instance.OpenPlanogramFromRecentCommand; }
        }

        private RelayCommand _openFromRecentCommand;

        /// <summary>
        /// Shows the open file dialog to allow selection of a plan file to load.
        /// </summary>
        public RelayCommand OpenPlanogramFromRecentCommand
        {
            get
            {
                if (_openFromRecentCommand != null) return _openFromRecentCommand;

                //  Instantiate the command if it was not so yet.
                _openFromRecentCommand = new RelayCommand(OpenFromRecent_Executed);
                RegisterCommand(_openFromRecentCommand);

                return _openFromRecentCommand;
            }
        }

        private static void OpenFromRecent_Executed(Object args)
        {
            RecentPlanogram recent = args as RecentPlanogram;
            if (recent == null) return;

            // Attempt to connect to the database repository if there is no current connection and the planogram is stored in it.
            if (recent.Type == RecentPlanogramType.Repository && !App.ViewState.IsConnectedToRepository)
            {
                WindowHelper.ShowWindow(new DbConnectionWizardWindow(), true);
            }

            //Are we connected and do we have permission.
            Boolean canGetPlanogram = ApplicationContext.User.IsInRole(DomainPermission.PlanogramGet.ToString());

            if (recent.Type != RecentPlanogramType.Repository || (App.ViewState.IsConnectedToRepository && canGetPlanogram))
            {
                MainViewModel.OpenRecentPlanogram(recent);
            }
            else
            {
                if (!canGetPlanogram)
                {
                    ShowNotSufficientPermissionWarning();
                }
                else
                {
                    ShowMissingConnectionWarning();
                }
            }
        }

        #endregion

        #region SavePlanogram

        public static RelayCommand SavePlanogram
        {
            get { return Instance.SavePlanogramCommand; }
        }

        private RelayCommand _savePlanogramCommand;

        /// <summary>
        /// Saves the active planogram to a specified file.
        /// </summary>
        public RelayCommand SavePlanogramCommand
        {
            get
            {
                if (_savePlanogramCommand == null)
                {
                    _savePlanogramCommand = new RelayCommand(
                        p => SavePlanogram_Executed(),
                        p => SavePlanogram_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Ribbon_SavePlanogram_Desc,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16,
                    };
                    RegisterCommand(_savePlanogramCommand);
                }
                return _savePlanogramCommand;
            }
        }

        private Boolean SavePlanogram_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.SavePlanogramCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            if (!ActivePlanController.SourcePlanogram.IsPlanogramModelValid)
            {
                this.SavePlanogramCommand.DisabledReason = Message.DisabledReason_Invalid;
                return false;
            }

            return true;
        }

        private void SavePlanogram_Executed()
        {
            MainViewModel.Save(ActivePlanController);
        }

        #endregion

        #region SavePlanogramAsFile

        public static RelayCommand SavePlanogramAsFile
        {
            get { return Instance.SavePlanogramAsFileCommand; }
        }

        private RelayCommand _savePlanogramAsFileCommand;

        /// <summary>
        /// Saves the active planogram to a new file.
        /// </summary>
        public RelayCommand SavePlanogramAsFileCommand
        {
            get
            {
                if (_savePlanogramAsFileCommand == null)
                {
                    _savePlanogramAsFileCommand = new RelayCommand(
                        p => SavePlanogramAsFile_Executed(),
                        p => SavePlanogramAsFile_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_SaveAsFile,
                        FriendlyDescription = Message.Ribbon_SavePlanogram_Desc,
                        Icon = ImageResources.SaveToFile_32,
                        SmallIcon = ImageResources.SaveToFile_16,
                    };
                    RegisterCommand(_savePlanogramAsFileCommand);
                }
                return _savePlanogramAsFileCommand;
            }
        }

        private Boolean SavePlanogramAsFile_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.SavePlanogramAsFileCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            if (ActivePlanController.SourcePlanogram.ParentPackageView == null)
            {
                this.SavePlanogramAsFileCommand.DisabledReason = Message.DisabledReason_Invalid;
                return false;
            }

            if (!ActivePlanController.SourcePlanogram.ParentPackageView.IsValid())
            {
                this.SavePlanogramAsFileCommand.DisabledReason = Message.DisabledReason_Invalid;
                return false;
            }

            return true;
        }

        private void SavePlanogramAsFile_Executed()
        {
            MainViewModel.SaveAsFile(ActivePlanController);
        }

        #endregion

        #region SaveToRepository

        public static RelayCommand SaveToRepository
        {
            get { return Instance.SaveToRepositoryCommand; }
        }

        private RelayCommand _saveToRepositoryCommand;

        /// <summary>
        /// Saves the active planogram as a new record in the repository.
        /// </summary>
        public RelayCommand SaveToRepositoryCommand
        {
            get
            {
                if (_saveToRepositoryCommand == null)
                {
                    _saveToRepositoryCommand = new RelayCommand(
                        p => SaveToRepository_Executed(),
                        p => SaveToRepository_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_SaveToRepository,
                        Icon = ImageResources.SaveToDatabase_32,
                        SmallIcon = ImageResources.SaveToDatabase_16,
                    };
                    RegisterCommand(_saveToRepositoryCommand);
                }
                return _saveToRepositoryCommand;
            }
        }

        private Boolean SaveToRepository_CanExecute()
        {
            //  Check whether we even have an active planogram.
            if (ActivePlanController == null)
            {
                this.SavePlanogramAsFileCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //  Check whether there is a source planogram and that it is correct.
            if (ActivePlanController.SourcePlanogram.ParentPackageView == null
                || !ActivePlanController.SourcePlanogram.ParentPackageView.IsValid())
            {
                this.SavePlanogramAsFileCommand.DisabledReason = Message.DisabledReason_Invalid;
                return false;
            }

            //  Check wether there is a connection to a repository.
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.SavePlanogramAsFileCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }

            //  Check whether the user has create planogram permission when the source planogram is new.
            if (ActivePlanController.SourcePlanogram.Model.IsNew &&
                !_isInPlanogramCreateRole)
            {
                SaveToRepositoryCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //  Check whether the user has edit planogram permission when the source planogram is not new.
            if (!ActivePlanController.SourcePlanogram.Model.IsNew &&
                !_isInPlanogramEditRole)
            {
                SaveToRepositoryCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            return true;
        }

        private static void SaveToRepository_Executed()
        {
            MainViewModel.SaveAsRepository(ActivePlanController);
        }

        #endregion

        #region ExportPlanogramToFile

        public static RelayCommand ExportPlanogramToFile
        {
            get { return Instance.ExportPlanogramToFileCommand; }
        }

        private RelayCommand _exportPlanogramToFileCommand;

        /// <summary>
        /// Exports the active planogram to a file.
        /// </summary>
        public RelayCommand ExportPlanogramToFileCommand
        {
            get
            {
                if (_exportPlanogramToFileCommand == null)
                {
                    _exportPlanogramToFileCommand = new RelayCommand(
                        p => ExportPlanogramToFile_Executed(),
                        p => ExportPlanogramToFile_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ExportPlanogramToFile,
                        FriendlyDescription = Message.Ribbon_ExportPlanogramToFile_Desc,
                        Icon = ImageResources.SaveToFile_32,
                        SmallIcon = ImageResources.SaveToFile_16,
                    };
                    RegisterCommand(_exportPlanogramToFileCommand);
                }
                return _exportPlanogramToFileCommand;
            }
        }

        private Boolean ExportPlanogramToFile_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.ExportPlanogramToFileCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            if (ActivePlanController.SourcePlanogram.ParentPackageView == null)
            {
                this.ExportPlanogramToFileCommand.DisabledReason = Message.DisabledReason_Invalid;
                return false;
            }

            if (!ActivePlanController.SourcePlanogram.ParentPackageView.IsValid())
            {
                this.ExportPlanogramToFileCommand.DisabledReason = Message.DisabledReason_Invalid;
                return false;
            }

            return true;
        }

        private void ExportPlanogramToFile_Executed()
        {
            MainViewModel.SaveAsFile(ActivePlanController);
        }

        #endregion

        #region Cut

        /// <summary>
        /// Cuts the selected items from the plan and puts them into the clipboard.
        /// </summary>
        public static RelayCommand Cut
        {
            get { return Instance.CutCommand; }
        }

        private RelayCommand _cutCommand;

        /// <summary>
        /// Cuts the selected items from the plan and puts them into the clipboard.
        /// </summary>
        public RelayCommand CutCommand
        {
            get
            {
                if (_cutCommand == null)
                {
                    _cutCommand = new RelayCommand(
                        p => Cut_Executed(),
                        p => Cut_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_Cut,
                        FriendlyDescription = Message.Ribbon_Cut_Desc,
                        SmallIcon = ImageResources.Cut_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.X
                    };
                    RegisterCommand(_cutCommand);
                }
                return _cutCommand;
            }
        }

        private Boolean Cut_CanExecute()
        {
            PlanControllerViewModel planController = ActivePlanController;

            if (planController == null)
            {
                this.CutCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            if (planController.SelectedPlanItems.Count == 0)
            {
                this.CutCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                return false;
            }

            this.CutCommand.DisabledReason = null;
            return true;
        }

        private void Cut_Executed()
        {
            //copy to the clipboard
            this.CopyCommand.Execute();

            //delete the item
            this.RemoveSelectedCommand.Execute();
        }

        #endregion

        #region Copy

        public static RelayCommand Copy
        {
            get { return Instance.CopyCommand; }
        }

        private RelayCommand _copyCommand;

        /// <summary>
        /// Copies the currently selected items
        /// </summary>
        public RelayCommand CopyCommand
        {
            get
            {
                if (_copyCommand == null)
                {
                    _copyCommand = new RelayCommand(
                       p => Copy_Executed(),
                       p => Copy_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_Copy,
                        FriendlyDescription = Message.Ribbon_Copy_Desc,
                        SmallIcon = ImageResources.Copy_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.C
                    };
                    RegisterCommand(_copyCommand);
                }
                return _copyCommand;
            }
        }

        private Boolean Copy_CanExecute()
        {
            var planController = ActivePlanController;

            if (planController == null)
            {
                this.CopyCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            if (planController.SelectedPlanItems.Count == 0)
            {
                this.CopyCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                return false;
            }

            this.CopyCommand.DisabledReason = null;
            return true;
        }

        private void Copy_Executed()
        {
            _clipboardList.Clear();

            //copy non-position items first
            _clipboardList.AddRange(ActivePlanController.SelectedPlanItems.Where(i => i.PlanItemType != PlanItemType.Position));

            // Copy in selection order and not position sequence order, because selecting across components renders the
            // position sequence values inaccurate.
            _clipboardList.AddRange(ActivePlanController.SelectedPlanItems.Where(i => i.PlanItemType == PlanItemType.Position));
        }

        #endregion

        #region DragCopy

        public static RelayCommand DragCopy
        {
            get { return Instance.DragCopyCommand; }
        }

        private RelayCommand _DragCopyCommand;

        /// <summary>
        /// Copies the currently selected items
        /// </summary>
        public RelayCommand DragCopyCommand
        {
            get
            {
                if (_DragCopyCommand == null)
                {
                    _DragCopyCommand = new RelayCommand(
                       p => DragCopy_Executed(),
                       p => DragCopy_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_Copy,
                        FriendlyDescription = Message.Ribbon_Copy_Desc,
                        SmallIcon = ImageResources.Copy_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.C
                    };
                    RegisterCommand(_DragCopyCommand);
                }
                return _DragCopyCommand;
            }
        }

        private Boolean DragCopy_CanExecute()
        {
            var planController = ActivePlanController;

            if (planController == null)
            {
                this.DragCopyCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            if (planController.SelectedPlanItems.Count == 0)
            {
                this.DragCopyCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                return false;
            }

            this.DragCopyCommand.DisabledReason = null;
            return true;
        }

        private void DragCopy_Executed()
        {
            _dragClipboardList.Clear();

            //copy non-position items first
            _dragClipboardList.AddRange(ActivePlanController.SelectedPlanItems.Where(i => i.PlanItemType != PlanItemType.Position));

            // Copy in selection order and not position sequence order, because selecting across components renders the
            // position sequence values inaccurate.
            _dragClipboardList.AddRange(ActivePlanController.SelectedPlanItems.Where(i => i.PlanItemType == PlanItemType.Position));
        }

        #endregion

        #region Screen shot

        public static RelayCommand Screenshot
        {
            get { return Instance.ScreenshotCommand; }
        }

        private RelayCommand _screenshotCommand;

        /// <summary>
        /// Copies the currently selected items
        /// </summary>
        public RelayCommand ScreenshotCommand
        {
            get
            {
                if (_screenshotCommand == null)
                {
                    _screenshotCommand = new RelayCommand(
                       p => Screenshot_Executed(),
                       p => Screenshot_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_CopyToClipboard,
                        FriendlyDescription = Message.Ribbon_CopyToClipboard_Desc,
                        SmallIcon = ImageResources.Screenshot_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.PrintScreen
                    };
                    RegisterCommand(_screenshotCommand);
                }
                return _screenshotCommand;
            }
        }

        private Boolean Screenshot_CanExecute()
        {
            var planController = ActivePlanController;

            if (planController == null)
            {
                this.ScreenshotCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            this.ScreenshotCommand.DisabledReason = null;
            return true;
        }

        private void Screenshot_Executed()
        {
            //TODO: This should not be accessing named parts of the plan visual document control
            // Instead create a method on the actual control to call.

            //Get active content
            PlanController controller = ActivePlanController.AttachedControl;

            //Active controller shouldn't be null, ensure the control has rendered
            if (controller == null ||
                controller.ActiveContent == null) return;

            //Render to bitmap
            RenderTargetBitmap renderBitmap;

            //Get active content as plan visual document
            PlanVisualDocumentView control = controller.ActiveContent as PlanVisualDocumentView;
            if (control != null)
            {
                //Set scrollbars to be hidden again
                control.xHorizontalScrollbar.Visibility = Visibility.Collapsed;
                control.xVerticalScrollbar.Visibility = Visibility.Collapsed;
                control.UpdateLayout();

                //Render to bitmap
                renderBitmap = new RenderTargetBitmap((Int32)control.ActualWidth, (Int32)control.ActualHeight, 96, 96, PixelFormats.Pbgra32);
                renderBitmap.Render(control);

                //Set scrollbars to be visible again
                control.xHorizontalScrollbar.Visibility = Visibility.Visible;
                control.xVerticalScrollbar.Visibility = Visibility.Visible;
                control.UpdateLayout();
            }
            else
            {
                //Support other view types
                renderBitmap = new RenderTargetBitmap((Int32)controller.ActualWidth, (Int32)controller.ActualHeight, 96, 96, PixelFormats.Pbgra32);
                renderBitmap.Render(controller);
            }

            //Set image
            Clipboard.SetImage(renderBitmap);
        }

        #endregion

        #region Paste

        /// <summary>
        /// Pastes the clipboard items.
        /// </summary>
        public static RelayCommand Paste
        {
            get { return Instance.PasteCommand; }
        }

        private RelayCommand _pasteCommand;

        /// <summary>
        /// Pastes the clipboard items.
        /// </summary>
        public RelayCommand PasteCommand
        {
            get
            {
                if (_pasteCommand == null)
                {
                    _pasteCommand = new RelayCommand(
                        p => Paste_Executed(),
                        p => Paste_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_Paste,
                        FriendlyDescription = Message.Ribbon_Paste_Desc,
                        SmallIcon = ImageResources.Paste_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.V
                    };
                    RegisterCommand(_pasteCommand);
                }
                return _pasteCommand;
            }
        }

        private Boolean Paste_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.PasteCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            if (_clipboardList.Count == 0)
            {
                this.PasteCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                return false;
            }

            // all clipboard items are positions
            if (!_clipboardList.Where(p => p.PlanItemType != PlanItemType.Position).Any())
            {
                var planController = ActivePlanController;

                // validate that a single component item is available for paste action, else command not available
                if (planController != null && ((planController.SelectedPlanItems.Count == 0 || planController.SelectedPlanItems.Count > 1)
                            || ((planController.SelectedPlanItems.Count == 1) && planController.SelectedPlanItems.Last().PlanItemType != PlanItemType.Component
                            && planController.SelectedPlanItems.Last().PlanItemType != PlanItemType.Assembly)))
                {
                    return false;
                }
            }

            return true;
        }

        private void Paste_Executed()
        {
            ActivePlanController.Paste(_clipboardList);
        }

        #endregion

        #region PasteToAllPlanograms

        /// <summary>
        /// PasteToAllPlanogramss the clipboard items.
        /// </summary>
        public static RelayCommand PasteToAllPlanograms
        {
            get { return Instance.PasteToAllPlanogramsCommand; }
        }

        private RelayCommand _pasteToAllPlanogramsCommand;

        /// <summary>
        /// PasteToAllPlanogramss the clipboard items.
        /// </summary>
        public RelayCommand PasteToAllPlanogramsCommand
        {
            get
            {
                if (_pasteToAllPlanogramsCommand == null)
                {
                    _pasteToAllPlanogramsCommand = new RelayCommand(
                        p => PasteToAllPlanograms_Executed(),
                        p => PasteToAllPlanograms_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_PasteToAllPlanograms,
                        FriendlyDescription = Message.Ribbon_PasteToAllPlanograms_Desc,
                        SmallIcon = ImageResources.Paste_16
                    };
                    RegisterCommand(_pasteToAllPlanogramsCommand);
                }
                return _pasteToAllPlanogramsCommand;
            }
        }

        private Boolean PasteToAllPlanograms_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.PasteToAllPlanogramsCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            if (_clipboardList.Count == 0)
            {
                this.PasteToAllPlanogramsCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                return false;
            }

            return true;
        }

        private void PasteToAllPlanograms_Executed()
        {
            foreach (PlanControllerViewModel pcvm in MainViewModel.PlanControllers)
            {
                IPlanItem planItem = _clipboardList.FirstOrDefault();

                if (planItem != null)
                {
                    if (pcvm.SourcePlanogram != planItem.Planogram)
                    {
                        pcvm.Paste(_clipboardList, true);
                    }
                }
            }
        }

        #endregion

        #region Redo

        /// <summary>
        /// Performs a redo on the active planogram
        ///  Param: optionally takes a model undo action to redo up to.
        /// </summary>
        public static RelayCommand Redo
        {
            get { return Instance.RedoCommand; }
        }

        private RelayCommand _redoCommand;

        /// <summary>
        /// Performs a redo on the active planogram
        ///  Param: optionally takes a model undo action to redo up to.
        /// </summary>
        public RelayCommand RedoCommand
        {
            get
            {
                if (_redoCommand == null)
                {
                    _redoCommand = new RelayCommand(
                        p => Redo_Executed(p),
                        p => Redo_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_Redo,
                        FriendlyDescription = Message.Ribbon_Redo_Desc,
                        SmallIcon = ImageResources.Redo_16,
                        InputGestureKey = Key.Y,
                        InputGestureModifiers = ModifierKeys.Control
                    };
                    RegisterCommand(_redoCommand);
                }
                return _redoCommand;
            }
        }

        private Boolean Redo_CanExecute()
        {
            var planController = ActivePlanController;

            if (planController == null)
            {
                this.RedoCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            if (planController.SourcePlanogram.RedoActions.Count == 0)
            {
                this.RedoCommand.DisabledReason = Message.DisabledReason_NoRedoActions;
                return false;
            }

            return true;
        }

        private void Redo_Executed(Object args)
        {
            ActivePlanController.SourcePlanogram.Redo(args as ModelUndoAction);
        }

        #endregion

        #region RemoveSelected

        /// <summary>
        /// Removes the selected items.
        /// </summary>
        public static RelayCommand RemoveSelected
        {
            get { return Instance.RemoveSelectedCommand; }
        }

        private RelayCommand _removeSelectedCommand;

        /// <summary>
        /// Deletes the currently selected items on the active plan
        /// </summary>
        public RelayCommand RemoveSelectedCommand
        {
            get
            {
                if (_removeSelectedCommand == null)
                {
                    _removeSelectedCommand = new RelayCommand(
                        p => RemoveSelected_Executed(),
                        p => RemoveSelected_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_DeleteSelected,
                        FriendlyDescription = Message.Ribbon_DeleteSelected_Desc,
                        SmallIcon = ImageResources.Delete_16,
                        InputGestureKey = Key.Delete
                    };
                    RegisterCommand(_removeSelectedCommand);
                }
                return _removeSelectedCommand;
            }
        }

        private Boolean RemoveSelected_CanExecute()
        {
            var planController = ActivePlanController;

            if (planController == null)
            {
                this.RemoveSelectedCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            if (planController.SelectedPlanItems.Count == 0)
            {
                this.RemoveSelectedCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                return false;
            }

            this.RemoveSelectedCommand.DisabledReason = null;
            return true;
        }

        private void RemoveSelected_Executed()
        {
            ActivePlanController.RemoveSelectedItems();
        }

        #endregion

        #region Search

        public static RelayCommand Search
        {
            get { return Instance.SearchCommand; }
        }

        private RelayCommand _searchCommand;

        /// <summary>
        /// Opens the search window.
        /// </summary>
        public RelayCommand SearchCommand
        {
            get
            {
                if (_searchCommand == null)
                {
                    _searchCommand = new RelayCommand(
                        p => Search_Executed(),
                        p => Search_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_Search,
                        FriendlyDescription = Message.Ribbon_Search_Desc,
                        SmallIcon = ImageResources.Ribbon_Search_16,
                        InputGestureKey = Key.F,
                        InputGestureModifiers = ModifierKeys.Control
                    };
                    RegisterCommand(_searchCommand);

                }
                return _searchCommand;
            }
        }

        private Boolean Search_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.SearchCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void Search_Executed()
        {
            //Try to activate an existing window:
            ProductFixtureSearchWindow win =
                CommonHelper.GetWindowService().ActivateWindow<ProductFixtureSearchWindow>();
            if (win != null)
            {
                win.WindowState = WindowState.Normal;
                return;
            }

            //Open a new instance
            CommonHelper.GetWindowService().ShowWindow<ProductFixtureSearchWindow>(MainViewModel);
        }

        #endregion

        #region Undo

        /// <summary>
        /// Performs and undo of the last action on the active planogram.
        /// Param: optionally takes a model undo action to undo up to.
        /// </summary>
        public static RelayCommand Undo
        {
            get { return Instance.UndoCommand; }
        }


        private RelayCommand _undoCommand;

        /// <summary>
        /// Performs and undo of the last action on the active planogram.
        /// Param: optionally takes a model undo action to undo up to.
        /// </summary>
        public RelayCommand UndoCommand
        {
            get
            {
                if (_undoCommand == null)
                {
                    _undoCommand = new RelayCommand(
                        p => Undo_Executed(p),
                        p => Undo_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_Undo,
                        FriendlyDescription = Message.Ribbon_Undo_Desc,
                        SmallIcon = ImageResources.Undo_16,
                        InputGestureKey = Key.Z,
                        InputGestureModifiers = ModifierKeys.Control
                    };
                    RegisterCommand(_undoCommand);
                }
                return _undoCommand;
            }
        }

        private Boolean Undo_CanExecute()
        {
            var planController = ActivePlanController;

            if (planController == null)
            {
                this.UndoCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            if (planController.SourcePlanogram.UndoActions.Count == 0)
            {
                this.UndoCommand.DisabledReason = Message.DisabledReason_NoUndoActions;
                return false;
            }

            return true;
        }

        private void Undo_Executed(Object args)
        {
            ActivePlanController.SourcePlanogram.Undo(args as ModelUndoAction);
        }

        #endregion

        #region ShowPlanogramProperties

        /// <summary>
        /// Shows the properties window for the active planogram.
        /// </summary>
        public static RelayCommand ShowPlanogramProperties
        {
            get { return Instance.ShowPlanogramPropertiesCommand; }
        }

        private RelayCommand _showPlanogramPropertiesCommand;

        /// <summary>
        /// Shows the properties window for the active planogram.
        /// </summary>
        public RelayCommand ShowPlanogramPropertiesCommand
        {
            get
            {
                if (_showPlanogramPropertiesCommand == null)
                {
                    _showPlanogramPropertiesCommand = new RelayCommand(
                        p => ShowPlanogramProperties_Executed(),
                        p => ShowPlanogramProperties_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_PlanogramProperties,
                        FriendlyDescription = Message.Ribbon_PlanogramProperties_Desc,
                        Icon = ImageResources.PlanogramProperties_32,
                        SmallIcon = ImageResources.PlanogramProperties_16,
                    };
                    RegisterCommand(_showPlanogramPropertiesCommand);
                }
                return _showPlanogramPropertiesCommand;
            }
        }

        private Boolean ShowPlanogramProperties_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.ShowPlanogramPropertiesCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void ShowPlanogramProperties_Executed()
        {
            ActivePlanController.ShowPlanProperties();
        }

        #endregion

        #region ShowPositionProperties

        /// <summary>
        /// Shows the plan item properties window for the currently 
        /// selected items.
        /// </summary>
        public static RelayCommand ShowPositionProperties
        {
            get { return Instance.ShowPositionPropertiesCommand; }
        }

        private RelayCommand _showPositionPropertiesCommand;

        /// <summary>
        /// Shows the plan item properties window for the currently 
        /// selected items.
        /// </summary>
        public RelayCommand ShowPositionPropertiesCommand
        {
            get
            {
                if (_showPositionPropertiesCommand == null)
                {
                    _showPositionPropertiesCommand = new RelayCommand(
                        p => ShowPositionProperties_Executed(p),
                        p => ShowPositionProperties_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_PositionContext_EditPosition,
                        FriendlyDescription = Message.Ribbon_PositionContext_EditPosition_Desc,
                        Icon = ImageResources.View_PositionProperties_32,
                        SmallIcon = ImageResources.View_PositionProperties_32
                    };
                    RegisterCommand(_showPositionPropertiesCommand);
                }
                return _showPositionPropertiesCommand;
            }
        }

        private Boolean ShowPositionProperties_CanExecute()
        {
            var planController = ActivePlanController;

            if (planController == null)
            {
                this.ShowPositionPropertiesCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            if (!planController.SelectedPlanItems.Any(i => i.Position != null))
            {
                this.ShowPositionPropertiesCommand.DisabledReason = Message.DisabledReason_NoSelectedItem;
                return false;
            }

            this.ShowPositionPropertiesCommand.DisabledReason = null;
            return true;
        }

        private void ShowPositionProperties_Executed(Object arg)
        {
            var active = ActivePlanController;
            new PositionPropertiesViewModel(active.SourcePlanogram, active.SelectedPlanItems, false).ShowDialog();
        }

        #endregion

        #region ShowProductProperties

        /// <summary>
        /// Shows the plan item properties window for the currently 
        /// selected items.
        /// </summary>
        public static RelayCommand ShowProductProperties
        {
            get { return Instance.ShowProductPropertiesCommand; }
        }

        private RelayCommand _showProductPropertiesCommand;

        /// <summary>
        /// Shows the plan item properties window for the currently 
        /// selected items.
        /// </summary>
        public RelayCommand ShowProductPropertiesCommand
        {
            get
            {
                if (_showProductPropertiesCommand == null)
                {
                    _showProductPropertiesCommand = new RelayCommand(
                        p => ShowProductProperties_Executed(p),
                        p => ShowProductProperties_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_PositionContext_EditProduct,
                        FriendlyDescription = Message.Ribbon_PositionContext_EditProduct_Desc,
                        Icon = ImageResources.PositionContext_EditProduct_32,
                        SmallIcon = ImageResources.PositionContext_EditProduct_16
                    };
                    RegisterCommand(_showProductPropertiesCommand);
                }
                return _showProductPropertiesCommand;
            }
        }

        private Boolean ShowProductProperties_CanExecute()
        {
            var planController = ActivePlanController;

            if (planController == null)
            {
                this.ShowProductPropertiesCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            if (planController.SelectedPlanItems.Count == 0)
            {
                this.ShowProductPropertiesCommand.DisabledReason = Message.DisabledReason_NoSelectedItem;
                return false;
            }

            this.ShowProductPropertiesCommand.DisabledReason = null;
            return true;
        }

        private void ShowProductProperties_Executed(Object arg)
        {
            PlanItemSelection selectedPlanItems = ActivePlanController.SelectedPlanItems;

            if (selectedPlanItems.Count > 0
                && selectedPlanItems.Any(p => p.PlanItemType == PlanItemType.Product || p.PlanItemType == PlanItemType.Position))
            {
                PlanItemSelection selection = new PlanItemSelection();
                //V8-28688 : Pass through position, and parameter will deal with extracting product
                selection.AddRange(selectedPlanItems.Where(p => p.Product != null));

                new PositionPropertiesViewModel(ActivePlanController.SourcePlanogram, selection, true).ShowDialog();

                selection.Dispose();
            }
        }

        #endregion

        #region ShowComponentProperties

        /// <summary>
        /// Shows the plan item properties window for the currently 
        /// selected items.
        /// </summary>
        public static RelayCommand ShowComponentProperties
        {
            get { return Instance.ShowComponentPropertiesCommand; }
        }

        private RelayCommand _showComponentPropertiesCommand;

        /// <summary>
        /// Shows the plan item properties window for the currently 
        /// selected items.
        /// </summary>
        public RelayCommand ShowComponentPropertiesCommand
        {
            get
            {
                if (_showComponentPropertiesCommand == null)
                {
                    _showComponentPropertiesCommand = new RelayCommand(
                        p => ShowComponentProperties_Executed(p),
                        p => ShowComponentProperties_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ComponentContext_EditComponent,
                        FriendlyDescription = Message.Ribbon_ComponentContext_EditComponent_Desc,
                        Icon = ImageResources.FixtureContext_EditFixture_32,
                        SmallIcon = ImageResources.FixtureContext_EditFixture_16
                    };
                    RegisterCommand(_showComponentPropertiesCommand);
                }
                return _showComponentPropertiesCommand;
            }
        }

        private Boolean ShowComponentProperties_CanExecute()
        {
            var planController = ActivePlanController;

            if (planController == null)
            {
                this.ShowComponentPropertiesCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            if (!planController.SelectedPlanItems.Any(i => i.Component != null || i.Assembly != null))
            {
                this.ShowComponentPropertiesCommand.DisabledReason = Message.DisabledReason_NoSelectedItem;
                return false;
            }

            this.ShowComponentPropertiesCommand.DisabledReason = null;
            return true;
        }

        private void ShowComponentProperties_Executed(Object arg)
        {
            var active = ActivePlanController;
            new ComponentPropertiesViewModel(active.SourcePlanogram, active.SelectedPlanItems).ShowDialog();


            //PlanItemSelection selectedPlanItems = ActivePlanController.SelectedPlanItems;

            //if (selectedPlanItems.Count > 0
            //    && selectedPlanItems.Any(p => p.PlanItemType == PlanItemType.Component))
            //{

            //    selection.AddRange(selectedPlanItems.Where(p => p.Component != null).Select(p => p.Component));

            //    new ComponentPropertiesViewModel(ActivePlanController.SourcePlanogram, selection).ShowDialog();

            //    selection.Dispose();
            //}
        }

        #endregion

        #region ShowSelectedItemProperties

        /// <summary>
        /// Shows the plan item properties window for the currently 
        /// selected items.
        /// </summary>
        public static RelayCommand ShowSelectedItemProperties
        {
            get { return Instance.ShowSelectedItemPropertiesCommand; }
        }

        private RelayCommand _showSelectedItemPropertiesCommand;

        /// <summary>
        /// Shows the plan item properties window for the currently 
        /// selected items.
        /// </summary>
        public RelayCommand ShowSelectedItemPropertiesCommand
        {
            get
            {
                if (_showSelectedItemPropertiesCommand == null)
                {
                    _showSelectedItemPropertiesCommand = new RelayCommand(
                        p => ShowSelectedItemProperties_Executed(p),
                        p => ShowSelectedItemProperties_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_Properties,
                        FriendlyDescription = Message.Ribbon_Properties_Desc,
                        Icon = ImageResources.PlanItemProperties_32,
                        SmallIcon = ImageResources.PlanItemProperties_16
                    };
                    RegisterCommand(_showSelectedItemPropertiesCommand);
                }
                return _showSelectedItemPropertiesCommand;
            }
        }

        private Boolean ShowSelectedItemProperties_CanExecute()
        {
            var planController = ActivePlanController;

            //must have an active planogram
            if (planController == null)
            {
                this.ShowSelectedItemPropertiesCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //must have items selected
            if (planController.SelectedPlanItems.Count == 0)
            {
                this.ShowSelectedItemPropertiesCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                return false;
            }

            // must not have a fixture selected
            if (planController.SelectedPlanItems.Any(p => p.PlanItemType == PlanItemType.Fixture))
            {
                this.ShowSelectedItemPropertiesCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                return false;
            }


            return true;
        }

        private void ShowSelectedItemProperties_Executed(Object arg)
        {
            var activeController = ActivePlanController;

            //If only fixtures are selected then show the planogram properties
            if (activeController.SelectedPlanItems.All(s => s.PlanItemType == PlanItemType.Fixture))
            {
                activeController.ShowPlanProperties();
            }
            else
            {
                switch (activeController.SelectedPlanItems.First().PlanItemType)
                {
                    case PlanItemType.Position:
                    case PlanItemType.Product:
                        new PositionPropertiesViewModel(activeController.SourcePlanogram, activeController.SelectedPlanItems, activeController.SelectedPlanItems.First().PlanItemType == PlanItemType.Product).ShowDialog();
                        break;

                    case PlanItemType.Component:
                    case PlanItemType.Assembly:
                        new ComponentPropertiesViewModel(activeController.SourcePlanogram, activeController.SelectedPlanItems).ShowDialog();
                        break;

                    case PlanItemType.Annotation:
                        new AnnotationPropertiesViewModel(activeController.SelectedPlanItems.AnnotationView).ShowDialog();
                        break;

                    default:
                        Debug.Fail("Show properties not handled for current selection.");
                        break;
                }
            }
        }

        #endregion

        #region ToggleHighlightVisibility

        /// <summary>
        /// Toggles the visibility of highlights on and off.
        /// </summary>
        public static RelayCommand ToggleHighlightVisibility
        {
            get { return Instance.ToggleHighlightVisibilityCommand; }
        }

        private RelayCommand _toggleHighlightVisibiltyCommand;

        /// <summary>
        /// Toggles the visibility of highlights on and off.
        /// </summary>
        public RelayCommand ToggleHighlightVisibilityCommand
        {
            get
            {
                if (_toggleHighlightVisibiltyCommand == null)
                {
                    _toggleHighlightVisibiltyCommand = new RelayCommand(
                        p => ToggleHighlightVisibilityCommand_Executed(),
                        p => ToggleHighlightVisibilityCommand_CanExecute())
                    {
                        FriendlyName = Message.ToggleHighlightVisibilityCommand_FriendlyName,
                        FriendlyDescription = Message.ToggleHighlightVisibilityCommand_Description
                    };
                    RegisterCommand(_toggleHighlightVisibiltyCommand);
                }
                return _toggleHighlightVisibiltyCommand;
            }
        }

        private Boolean ToggleHighlightVisibilityCommand_CanExecute()
        {
            var planController = ActivePlanController;

            //must have an active planogram
            if (planController == null)
            {
                this.ShowSelectedItemPropertiesCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void ToggleHighlightVisibilityCommand_Executed()
        {
            App.MainPageViewModel.ToggleHighlightSet();
        }

        #endregion

        #region ToggleProductLabelVisibility

        /// <summary>
        /// Toggles the visibility of labels on and off.
        /// </summary>
        public static RelayCommand ToggleProductLabelVisibility
        {
            get { return Instance.ToggleProductLabelVisibilityCommand; }
        }

        private RelayCommand _toggleProductLabelVisibiltyCommand;

        /// <summary>
        /// Toggles the visibility of labels on and off.
        /// </summary>
        public RelayCommand ToggleProductLabelVisibilityCommand
        {
            get
            {
                if (_toggleProductLabelVisibiltyCommand == null)
                {
                    _toggleProductLabelVisibiltyCommand = new RelayCommand(
                        p => ToggleProductLabelVisibilityCommand_Executed(),
                        p => ToggleProductLabelVisibilityCommand_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_Properties,
                        FriendlyDescription = Message.Ribbon_Properties_Desc
                    };
                    RegisterCommand(_toggleProductLabelVisibiltyCommand);
                }
                return _toggleProductLabelVisibiltyCommand;
            }
        }

        private Boolean ToggleProductLabelVisibilityCommand_CanExecute()
        {
            var planController = ActivePlanController;

            //must have an active planogram
            if (planController == null)
            {
                this.ShowSelectedItemPropertiesCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void ToggleProductLabelVisibilityCommand_Executed()
        {
            App.MainPageViewModel.ToggleProductLabelSet();
        }

        #endregion

        #region ToggleFixtureLabelVisibility

        /// <summary>
        /// Toggles the visibility of labels on and off.
        /// </summary>
        public static RelayCommand ToggleFixtureLabelVisibility
        {
            get { return Instance.ToggleFixtureLabelVisibilityCommand; }
        }

        private RelayCommand _toggleFixtureLabelVisibiltyCommand;

        /// <summary>
        /// Toggles the visibility of labels on and off.
        /// </summary>
        public RelayCommand ToggleFixtureLabelVisibilityCommand
        {
            get
            {
                if (_toggleFixtureLabelVisibiltyCommand == null)
                {
                    _toggleFixtureLabelVisibiltyCommand = new RelayCommand(
                        p => ToggleFixtureLabelVisibilityCommand_Executed(),
                        p => ToggleFixtureLabelVisibilityCommand_CanExecute())
                    {
                        FriendlyName = Message.ToggleFixtureLabelVisibilityCommand_Friendly,
                        FriendlyDescription = Message.ToggleFixtureLabelVisibilityCommand_Description
                    };
                    RegisterCommand(_toggleFixtureLabelVisibiltyCommand);
                }
                return _toggleFixtureLabelVisibiltyCommand;
            }
        }

        private Boolean ToggleFixtureLabelVisibilityCommand_CanExecute()
        {
            var planController = ActivePlanController;

            //must have an active planogram
            if (planController == null)
            {
                this.ShowSelectedItemPropertiesCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void ToggleFixtureLabelVisibilityCommand_Executed()
        {
            App.MainPageViewModel.ToggleFixtureLabelSet();
        }

        #endregion

        #region AddToFixtureLibrary

        public static RelayCommand AddToFixtureLibrary
        {
            get { return Instance.AddToFixtureLibraryCommand; }
        }


        private RelayCommand _addToFixtureLibraryCommand;

        /// <summary>
        /// Adds the currently selected fixture item
        /// to the fixture library.
        /// </summary>
        public RelayCommand AddToFixtureLibraryCommand
        {
            get
            {
                if (_addToFixtureLibraryCommand == null)
                {
                    _addToFixtureLibraryCommand = new RelayCommand(
                        p => AddToFixtureLibrary_Executed(p),
                        p => AddToFixtureLibrary_CanExecute(p))
                    {
                        FriendlyName = Message.Ribbon_AddToFixtureLibrary,
                        FriendlyDescription = Message.Ribbon_AddToFixtureLibrary_Desc,
                        SmallIcon = ImageResources.Ribbon_AddToFixtureLibrary_16,
                        Icon = ImageResources.Ribbon_AddToFixtureLibrary_32
                    };
                    RegisterCommand(_addToFixtureLibraryCommand);
                }
                return _addToFixtureLibraryCommand;
            }
        }

        private Boolean AddToFixtureLibrary_CanExecute(Object args)
        {
            var planController = ActivePlanController;
            if (planController == null)
            {
                this.AddToFixtureLibraryCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }


            //check that the current selection is valid
            List<IPlanItem> validatedSelection;
            if (args is IPlanItem)
            {
                if (!FixtureLibraryHelper.CanAddToFixtureLibrary(new List<IPlanItem> { (IPlanItem)args }, out validatedSelection))
                {
                    this.AddToFixtureLibraryCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                    return false;
                }
            }
            else if (args is IEnumerable<IPlanItem>)
            {
                if (!FixtureLibraryHelper.CanAddToFixtureLibrary((IEnumerable<IPlanItem>)args, out validatedSelection))
                {
                    this.AddToFixtureLibraryCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                    return false;
                }
            }
            else
            {
                if (!planController.SelectedPlanItems.Any())
                {
                    this.AddToFixtureLibraryCommand.DisabledReason = Message.DisabledReason_MultipleSelected;
                    return false;
                }

                if (!FixtureLibraryHelper.CanAddToFixtureLibrary(planController.SelectedPlanItems, out validatedSelection))
                {
                    this.AddToFixtureLibraryCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                    return false;
                }
            }



            this.AddToFixtureLibraryCommand.DisabledReason = null;
            return true;
        }

        private void AddToFixtureLibrary_Executed(Object args)
        {
            //get the list of items to go into the package.
            List<IPlanItem> itemsToAdd = new List<IPlanItem>();
            if (args != null)
            {
                IPlanItem item = args as IPlanItem;
                if (item != null)
                {
                    itemsToAdd.Add(item);
                }
                else
                {
                    IEnumerable<IPlanItem> items = args as IEnumerable<IPlanItem>;
                    if (items != null) itemsToAdd.AddRange(items);
                }
            }
            else
            {
                //TODO: The fixture library helper now validates the selection, 
                // so this should no longer be required..
                itemsToAdd.AddRange(ActivePlanController.SelectedPlanItems);

                //V8:25514 - Check to see if all items are selected, add fixture if so
                //if (ActivePlanController.SelectedPlanItems.Count > 0)
                //{
                //    //Get primary item
                //    IPlanItem item = ActivePlanController.SelectedPlanItems.ElementAt(0);
                //    if (item != null && item.Fixture != null)
                //    {
                //        //If check if all the components and assemblies are selected
                //        if (item.Fixture.Components.Count == ActivePlanController.SelectedPlanItems.Count &&
                //            item.Fixture.Assemblies.Count == ActivePlanController.SelectedPlanItems.Count)
                //        {
                //            itemsToAdd.Insert(0, (IPlanItem)item.Fixture);
                //        }
                //        else
                //        {
                //            //Otherwise add selected (default behavior)
                //            itemsToAdd.AddRange(ActivePlanController.SelectedPlanItems);
                //        }
                //    }
                //    else
                //    {
                //        //Otherwise add selected (default behavior)
                //        itemsToAdd.AddRange(ActivePlanController.SelectedPlanItems);
                //    }
                //}
            }
            if (itemsToAdd.Count == 0) return;

            //Add the items.
            FixtureLibraryHelper.AddToFixtureLibrary(itemsToAdd);
        }

        #endregion

        #region EditComponent

        public static RelayCommand EditComponent
        {
            get { return Instance.EditComponentCommand; }
        }

        private RelayCommand _editComponentCommand;

        public RelayCommand EditComponentCommand
        {
            get
            {
                if (_editComponentCommand == null)
                {
                    _editComponentCommand = new RelayCommand(
                        p => EditComponent_Executed(),
                        p => EditComponent_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_EditComponent,
                        FriendlyDescription = Message.Ribbon_EditComponent_Desc,
                        SmallIcon = ImageResources.Ribbon_EditComponent_16,
                    };
                    RegisterCommand(_editComponentCommand);
                }
                return _editComponentCommand;
            }
        }

        private Boolean EditComponent_CanExecute()
        {
            var controller = ActivePlanController;

            if (controller == null)
            {
                this.EditComponentCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            if (controller.SelectedPlanItems.Count == 0)
            {
                this.EditComponentCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                return false;
            }

            IPlanItem item = controller.SelectedPlanItems.FirstOrDefault();
            if (item.PlanItemType != PlanItemType.Component)
            {
                this.EditComponentCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                return false;
            }

            if (item.Component.SubComponents.Any(s => s.Positions.Count > 0))
            {
                this.EditComponentCommand.DisabledReason = Message.Ribbon_EditComponent_DisabledHasPositions;
                return false;
            }

            return true;
        }

        private void EditComponent_Executed()
        {
            ActivePlanController.EditComponentParts();
        }

        #endregion

        #region CreateAssembly

        public static RelayCommand CreateAssembly
        {
            get { return Instance.CreateAssemblyCommand; }
        }

        private RelayCommand _createAssemblyCommand;

        /// <summary>
        /// Groups the selected items as a new assembly.
        /// </summary>
        public RelayCommand CreateAssemblyCommand
        {
            get
            {
                if (_createAssemblyCommand == null)
                {
                    _createAssemblyCommand = new RelayCommand(
                        p => CreateAssembly_Executed(),
                        p => CreateAssembly_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_CreateAssembly,
                        FriendlyDescription = Message.Ribbon_CreateAssembly_Desc,
                        SmallIcon = ImageResources.Ribbon_CreateAssembly_16
                    };
                    RegisterCommand(_createAssemblyCommand);
                }
                return _createAssemblyCommand;
            }
        }

        private Boolean CreateAssembly_CanExecute()
        {
            var controller = ActivePlanController;

            if (controller == null)
            {
                this.CreateAssemblyCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //must have items selected
            if (controller.SelectedPlanItems.Count == 0)
            {
                this.CreateAssemblyCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                return false;
            }

            //must have more than 1 item selected
            if (controller.SelectedPlanItems.Count == 1)
            {
                this.CreateAssemblyCommand.DisabledReason = Message.DisabledReason_SelectedItemsNotValid;
                return false;
            }


            //must all be components without a parent assembly.
            if (!controller.SelectedPlanItems.All(s =>
                s.PlanItemType == PlanItemType.Component
                && s.Assembly == null
                && s.Component.ComponentType != PlanogramComponentType.Base
                && s.Component.ComponentType != PlanogramComponentType.Backboard))
            {
                this.CreateAssemblyCommand.DisabledReason = Message.DisabledReason_SelectedItemsNotValid;
                return false;
            }

            return true;
        }

        private void CreateAssembly_Executed()
        {
            //V8-25616 : Start an undo able action to ensure entire assembly creation can be undone
            if (ActivePlanController.SourcePlanogram != null)
            {
                ActivePlanController.SourcePlanogram.BeginUndoableAction();
            }

            List<PlanogramComponentView> components =
                ActivePlanController.SelectedPlanItems
                .Where(s => s.PlanItemType == PlanItemType.Component)
                .Select(s => s.Component).ToList();

            PlanogramAssemblyView assembly = components.First().Fixture.AddAssembly(components); //PlanogramAssemblyView.GroupAsAssembly(components);
            if (assembly != null)
            {
                ActivePlanController.SelectedPlanItems.Clear();
                ActivePlanController.SelectedPlanItems.Add(assembly);
            }

            //V8-25616 : End the undo able action

            if (ActivePlanController.SourcePlanogram != null)
            {
                ActivePlanController.SourcePlanogram.EndUndoableAction();
            }

        }

        #endregion

        #region SplitAssembly

        public static RelayCommand SplitAssembly
        {
            get { return Instance.SplitAssemblyCommand; }
        }

        private RelayCommand _splitAssemblyCommand;

        /// <summary>
        /// Groups the selected assembly.
        /// </summary>
        public RelayCommand SplitAssemblyCommand
        {
            get
            {
                if (_splitAssemblyCommand == null)
                {
                    _splitAssemblyCommand = new RelayCommand(
                        p => SplitAssembly_Executed(),
                        p => SplitAssembly_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_SplitAssembly,
                        FriendlyDescription = Message.Ribbon_SplitAssembly_Desc,
                        SmallIcon = ImageResources.Ribbon_SplitAssembly_16
                    };
                    RegisterCommand(_splitAssemblyCommand);
                }
                return _splitAssemblyCommand;
            }
        }

        private Boolean SplitAssembly_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.SplitAssemblyCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            if (ActivePlanController.SelectedPlanItems.Count != 1)
            {
                this.SplitAssemblyCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                return false;
            }

            //must have components with parent assemblies selected.
            if (!ActivePlanController.SelectedPlanItems.Any(s =>
                s.PlanItemType == PlanItemType.Assembly ||
                (s.PlanItemType == PlanItemType.Component
                && s.Assembly != null)))
            {
                return false;
            }

            return true;
        }

        private void SplitAssembly_Executed()
        {
            List<PlanogramComponentView> components = new List<PlanogramComponentView>();

            List<PlanogramAssemblyView> assembliesToSplit =
                ActivePlanController.SelectedPlanItems.Select(s => s.Assembly).Distinct().ToList();


            ActivePlanController.SelectedPlanItems.Clear();

            //split each assembly but take a note of the individual components
            foreach (PlanogramAssemblyView assembly in assembliesToSplit)
            {
                components.AddRange(assembly.Ungroup());
            }

            //select the split components.
            ActivePlanController.SelectedPlanItems.AddRange(components);
        }

        #endregion

        #region SnapComponentToNotch

        public static RelayCommand SnapComponentToNotch
        {
            get { return Instance.SnapComponentToNotchCommand; }
        }

        private RelayCommand _snapComponentToNotchCommand;

        public RelayCommand SnapComponentToNotchCommand
        {
            get
            {
                if (_snapComponentToNotchCommand == null)
                {
                    _snapComponentToNotchCommand = new RelayCommand(
                        p => SnapComponentToNotch_Executed(),
                        p => SnapComponentToNotch_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_SnapComponentToNotch,
                        FriendlyDescription = Message.Ribbon_SnapComponentToNotch_Desc,
                        Icon = ImageResources.PlanogramProperties_SnapToNotch,
                    };
                    RegisterCommand(_snapComponentToNotchCommand);
                }
                return _snapComponentToNotchCommand;
            }
        }

        private Boolean SnapComponentToNotch_CanExecute()
        {
            var controller = ActivePlanController;

            if (controller == null)
            {
                this.SnapComponentToNotchCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            if (controller.SelectedPlanItems.Count == 0)
            {
                this.SnapComponentToNotchCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                return false;
            }

            //All selected items must be components
            if (controller.SelectedPlanItems.Any(i => i.PlanItemType != PlanItemType.Component))
            {
                this.SnapComponentToNotchCommand.DisabledReason = Message.DisabledReason_SelectedItemsMustBeComponents;
                return false;
            }

            //Check all selected items' backboards have notches set up - how do I do this?
            Boolean allBackboardsUseNotches = true;
            foreach (IPlanItem planItem in controller.SelectedPlanItems)
            {
                //Get its backboard (if any - not compulsory)
                PlanogramComponentView backboard = planItem.Fixture.Components
                    .FirstOrDefault(c => c.ComponentType == PlanogramComponentType.Backboard);
                if (backboard != null)
                {
                    //is this correct way to determine if a backboard uses notches?
                    if (!backboard.SubComponents[0].IsNotchPlacedOnFront)
                    {
                        allBackboardsUseNotches = false;
                        break;
                    }
                }
            }
            if (!allBackboardsUseNotches)
            {
                this.SnapComponentToNotchCommand.DisabledReason =
                    Message.DisabledReason_SelectedItemsMustAllUseNotches;
                return false;
            }

            this.SnapComponentToNotchCommand.DisabledReason = String.Empty;
            return true;
        }

        private void SnapComponentToNotch_Executed()
        {
            var controller = ActivePlanController;

            if (controller != null)
            {
                //There may be multiple items selected
                foreach (IPlanItem planItem in controller.SelectedPlanItems
                    .Where(i => i.PlanItemType == PlanItemType.Component))
                {
                    if (planItem.Component != null)
                    {
                        //Snap the component to the nearest notch
                        planItem.Component.SnapToNearestNotch();
                    }
                }
            }
        }

        #endregion

        #region LinkAnnotation

        /// <summary>
        /// Links the selected annotation(s) to the selected fixture part
        /// </summary>
        public static RelayCommand LinkAnnotation
        {
            get { return Instance.LinkAnnotationCommand; }
        }

        private RelayCommand _linkAnnotationCommand;

        /// <summary>
        ///Links the selected annotation(s) to the selected fixture part
        /// </summary>
        public RelayCommand LinkAnnotationCommand
        {
            get
            {
                if (_linkAnnotationCommand == null)
                {
                    _linkAnnotationCommand = new RelayCommand(
                        p => LinkAnnotation_Executed(),
                        p => LinkAnnotation_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_LinkAnnotation,
                        SmallIcon = ImageResources.Ribbon_LinkAnnotation,
                    };
                    RegisterCommand(_linkAnnotationCommand);
                }
                return _linkAnnotationCommand;
            }
        }

        private Boolean LinkAnnotation_CanExecute()
        {
            var controller = ActivePlanController;

            if (controller == null)
            {
                this.LinkAnnotationCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //must have at least one annotation selected
            if (!controller.SelectedPlanItems.Any(c => c.PlanItemType == PlanItemType.Annotation)
                || controller.SelectedPlanItems.Count(c => c.PlanItemType == PlanItemType.Component) != 1)
            {
                this.LinkAnnotationCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                return false;
            }

            return true;
        }

        private void LinkAnnotation_Executed()
        {
            //get the selected component
            PlanogramComponentView component =
                (PlanogramComponentView)ActivePlanController.SelectedPlanItems.First(c => c.PlanItemType == PlanItemType.Component);

            //move all annotations to it.
            foreach (IPlanItem item in ActivePlanController.SelectedPlanItems.ToArray())
            {
                if (item.PlanItemType != PlanItemType.Annotation) continue;
                PlanogramAnnotationView oldAnno = item.Annotation;
                PlanogramAnnotationView newAnno = component.MoveAnnotation(oldAnno);

                ActivePlanController.SelectedPlanItems.Remove(oldAnno);
                ActivePlanController.SelectedPlanItems.Add(newAnno);
            }
        }

        #endregion

        #region UnlinkAnnotation

        /// <summary>
        /// Unlinks the selected annotation(s) from their fixture parts.
        /// </summary>
        public static RelayCommand UnlinkAnnotation
        {
            get { return Instance.UnlinkAnnotationCommand; }
        }

        private RelayCommand _unlinkAnnotationCommand;

        /// <summary>
        ///Unlinks the selected annotation(s) from their fixture parts.
        /// </summary>
        public RelayCommand UnlinkAnnotationCommand
        {
            get
            {
                if (_unlinkAnnotationCommand == null)
                {
                    _unlinkAnnotationCommand = new RelayCommand(
                        p => UnlinkAnnotation_Executed(),
                        p => UnlinkAnnotation_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_UnlinkAnnotation,
                        SmallIcon = ImageResources.Ribbon_UnlinkAnnotation,
                    };
                    RegisterCommand(_unlinkAnnotationCommand);
                }
                return _unlinkAnnotationCommand;
            }
        }

        private Boolean UnlinkAnnotation_CanExecute()
        {
            var controller = ActivePlanController;

            if (controller == null)
            {
                this.UnlinkAnnotationCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //must have at least one component level annotation selected
            if (!controller.SelectedPlanItems.Any(c => c.PlanItemType == PlanItemType.Annotation && c.Annotation.IsComponentAnnotation))
            {
                this.UnlinkAnnotationCommand.DisabledReason = Message.DisabledReason_NoSelectedItems;
                return false;
            }

            return true;
        }

        private void UnlinkAnnotation_Executed()
        {
            //move all annotations up to their parent fixture
            foreach (IPlanItem item in ActivePlanController.SelectedPlanItems.ToArray())
            {
                if (item.PlanItemType != PlanItemType.Annotation) continue;

                PlanogramAnnotationView oldAnno = item.Annotation;
                PlanogramAnnotationView newAnno = item.Fixture.MoveAnnotation(oldAnno);

                ActivePlanController.SelectedPlanItems.Remove(oldAnno);
                ActivePlanController.SelectedPlanItems.Add(newAnno);
            }
        }

        #endregion

        #region AddNewProduct

        public static RelayCommand AddNewProduct
        {
            get { return Instance.AddNewProductCommand; }
        }

        private RelayCommand _addNewProductCommand;

        /// <summary>
        /// Adds a new product to the active planogram an
        /// </summary>
        public RelayCommand AddNewProductCommand
        {
            get
            {
                if (_addNewProductCommand == null)
                {
                    _addNewProductCommand = new RelayCommand(
                        p => AddNewProduct_Executed(),
                        p => AddNewProduct_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_AddNewProduct,
                        FriendlyDescription = Message.Ribbon_AddNewProduct_Desc,
                        Icon = ImageResources.Ribbon_AddProduct,
                        SmallIcon = ImageResources.Ribbon_AddProduct,
                    };
                    RegisterCommand(_addNewProductCommand);
                }
                return _addNewProductCommand;
            }
        }

        private Boolean AddNewProduct_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.AddNewProductCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void AddNewProduct_Executed()
        {
            ActivePlanController.AddNewProduct();
        }

        #endregion

        #region AddNewPosition

        /// <summary>
        /// Adds a new position to the active planogram
        /// </summary>
        public static RelayCommand AddNewPosition
        {
            get { return Instance.AddNewPositionCommand; }
        }

        private RelayCommand _addNewPositionCommand;

        /// <summary>
        /// Adds a new position to the active planogram
        /// </summary>
        public RelayCommand AddNewPositionCommand
        {
            get
            {
                if (_addNewPositionCommand == null)
                {
                    _addNewPositionCommand = new RelayCommand(
                        p => AddNewPosition_Executed(),
                        p => AddNewPosition_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_AddPosition,
                        SmallIcon = ImageResources.Ribbon_AddPosition
                    };
                    RegisterCommand(_addNewPositionCommand);
                }
                return _addNewPositionCommand;
            }
        }

        private Boolean AddNewPosition_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.AddNewProductCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            if (!ActivePlanController.SelectedPlanItems.HasComponents)
            {
                this.AddNewProductCommand.DisabledReason = Message.DisabledReason_NoComponentSelected;
                return false;
            }

            if (ActivePlanController.SelectedPlanItems.ComponentView.Items.Count() > 1)
            {
                this.AddNewProductCommand.DisabledReason = Message.DisabledReason_TooManyComponentsSelected;
                return false;
            }

            if (!ActivePlanController.SelectedPlanItems.ComponentView.Items.First().SubComponents.Any(s => s.IsMerchandisable))
            {
                this.AddNewProductCommand.DisabledReason = Message.DisabledReason_ComponentNotIsMerchandisable;
                return false;
            }

            return true;
        }

        private void AddNewPosition_Executed()
        {
            ActivePlanController.AddNewPosition(ActivePlanController.SelectedPlanItems.ComponentView.Items.First().SubComponents.First());
        }

        #endregion

        #region FillPositionsHigh

        public static RelayCommand FillPositionsHigh
        {
            get { return Instance.FillPositionsHighCommand; }
        }

        private RelayCommand _fillPositionsHighCommand;

        /// <summary>
        /// Fills the selected positions to fit the subcomponent
        /// in the y axis direction
        /// </summary>
        public RelayCommand FillPositionsHighCommand
        {
            get
            {
                if (_fillPositionsHighCommand == null)
                {
                    _fillPositionsHighCommand = new RelayCommand(
                        p => FillPositionsHigh_Executed(),
                        p => FillPositionsHigh_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_FillPositionsHigh,
                        FriendlyDescription = Message.Ribbon_FillPositionsHigh_Desc,
                        //Icon = ImageResources.View_ProductUnits_32,
                        SmallIcon = ImageResources.View_FillPositionsHigh_16
                    };
                }
                return _fillPositionsHighCommand;
            }
        }

        private Boolean FillPositionsHigh_CanExecute()
        {


            //must have a controller
            if (ActivePlanController == null)
            {
                this.FillPositionsHighCommand.DisabledReason = Message.DisabledReason_NoPositionSelected;
                return false;
            }

            //must have a position selected
            if (ActivePlanController.SelectedPlanItems.All(s => s.PlanItemType != PlanItemType.Position))
            {
                this.FillPositionsHighCommand.DisabledReason = Message.DisabledReason_NoPositionSelected;
                return false;
            }

            //if any of the positions sit on a component with a manual strategy
            if (!ActivePlanController.SelectedPlanItems.Any(s => s.PlanItemType == PlanItemType.Position
                && s.SubComponent.MerchandisingStrategyY != PlanogramSubComponentYMerchStrategyType.Manual))
            {
                this.FillPositionsHighCommand.DisabledReason = Message.Ribbon_FillPositionsHigh_DisabledInvalidStrategy;
                return false;
            }

            return true;
        }

        private void FillPositionsHigh_Executed()
        {
            //Call filling method y.
            ActivePlanController.FillSelectedPositionsOnGivenAxis(AxisType.Y);
        }

        #endregion

        #region FillPositionsWide

        public static RelayCommand FillPositionsWide
        {
            get { return Instance.FillPositionsWideCommand; }
        }

        private RelayCommand _fillPositionsWideCommand;

        /// <summary>
        /// Fills the selected positions to fit the subcomponent
        /// in the x axis direction
        /// </summary>
        public RelayCommand FillPositionsWideCommand
        {
            get
            {
                if (_fillPositionsWideCommand == null)
                {
                    _fillPositionsWideCommand = new RelayCommand(
                        p => FillPositionsWide_Executed(),
                        p => FillPositionsWide_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_FillPositionsWide,
                        FriendlyDescription = Message.Ribbon_FillPositionsWide_Desc,
                        //Icon = ImageResources.View_ProductUnits_32,
                        SmallIcon = ImageResources.View_FillPositionsWide_16
                    };
                }
                return _fillPositionsWideCommand;
            }
        }

        private Boolean FillPositionsWide_CanExecute()
        {
            //must have a controller
            if (ActivePlanController == null)
            {
                this.FillPositionsWideCommand.DisabledReason = Message.DisabledReason_NoPositionSelected;
                return false;
            }

            //must have a position selected
            if (!ActivePlanController.SelectedPlanItems.Any(s => s.PlanItemType == PlanItemType.Position))
            {
                this.FillPositionsWideCommand.DisabledReason = Message.DisabledReason_NoPositionSelected;
                return false;
            }

            //if any of the positions sit on a component with a manual strategy
            if (!ActivePlanController.SelectedPlanItems.Any(s => s.PlanItemType == PlanItemType.Position
                && s.SubComponent.MerchandisingStrategyX != PlanogramSubComponentXMerchStrategyType.Manual))
            {
                this.FillPositionsWideCommand.DisabledReason = Message.Ribbon_FillPositionsWide_DisabledInvalidStrategy;
                return false;
            }

            return true;
        }

        private void FillPositionsWide_Executed()
        {
            //Call filling method x.
            ActivePlanController.FillSelectedPositionsOnGivenAxis(AxisType.X);
        }

        #endregion

        #region FillPositionsDeep

        /// <summary>
        /// Fills the selected positions to fit the subcomponent
        /// in the z axis direction
        /// </summary>
        public static RelayCommand FillPositionsDeep
        {
            get { return Instance.FillPositionsDeepCommand; }
        }

        private RelayCommand _fillPositionsDeepCommand;

        /// <summary>
        /// Fills the selected positions to fit the subcomponent
        /// in the x axis direction
        /// </summary>
        public RelayCommand FillPositionsDeepCommand
        {
            get
            {
                if (_fillPositionsDeepCommand == null)
                {
                    _fillPositionsDeepCommand = new RelayCommand(
                        p => FillPositionsDeep_Executed(),
                        p => FillPositionsDeep_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_FillPositionsDeep,
                        FriendlyDescription = Message.Ribbon_FillPositionsDeep_Desc,
                        //Icon = ImageResources.View_FillPositionsDeep_16,
                        SmallIcon = ImageResources.View_FillPositionsDeep_16
                    };
                }
                return _fillPositionsDeepCommand;
            }
        }

        private Boolean FillPositionsDeep_CanExecute()
        {
            //must have a controller
            if (ActivePlanController == null)
            {
                this.FillPositionsDeepCommand.DisabledReason = Message.DisabledReason_NoPositionSelected;
                return false;
            }

            //must have a position selected
            if (ActivePlanController.SelectedPlanItems.All(s => s.PlanItemType != PlanItemType.Position))
            {
                this.FillPositionsDeepCommand.DisabledReason = Message.DisabledReason_NoPositionSelected;
                return false;
            }

            //if any of the positions sit on a component with a manual strategy
            if (!ActivePlanController.SelectedPlanItems.Any(s => s.PlanItemType == PlanItemType.Position
                && s.SubComponent.MerchandisingStrategyZ != PlanogramSubComponentZMerchStrategyType.Manual))
            {
                this.FillPositionsDeepCommand.DisabledReason = Message.Ribbon_FillPositionsDeep_DisabledInvalidStrategy;
                return false;
            }

            return true;
        }

        private void FillPositionsDeep_Executed()
        {
            //Call filling method z.
            ActivePlanController.FillSelectedPositionsOnGivenAxis(AxisType.Z);
        }

        #endregion

        #region IncreasePositionFacings

        /// <summary>
        /// Increase the facings of the currently selected positions
        /// </summary>
        public static RelayCommand IncreasePositionFacings
        {
            get { return Instance.IncreasePositionFacingsCommand; }
        }

        private RelayCommand _increasePositionFacingsCommand;

        public RelayCommand IncreasePositionFacingsCommand
        {
            get
            {
                if (_increasePositionFacingsCommand == null)
                {
                    _increasePositionFacingsCommand = new RelayCommand(
                        p => IncreasePositionFacings_Executed(),
                        p => IncreasePositionFacings_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_IncreasePositionFacings,
                        FriendlyDescription = Message.Ribbon_IncreasePositionFacings_Desc,
                        Icon = ImageResources.Ribbon_IncreasePositionFacings_32,
                        SmallIcon = ImageResources.Ribbon_IncreasePositionFacings_16
                    };
                    RegisterCommand(_increasePositionFacingsCommand);
                }
                return _increasePositionFacingsCommand;
            }
        }

        private Boolean IncreasePositionFacings_CanExecute()
        {
            //must have an active controller
            if (ActivePlanController == null)
            {
                this.IncreasePositionFacingsCommand.DisabledReason = Message.DisabledReason_NoPositionSelected;
                return false;
            }

            //must have a position selected
            if (ActivePlanController.SelectedPlanItems.All(s => s.PlanItemType != PlanItemType.Position))
            {
                this.IncreasePositionFacingsCommand.DisabledReason = Message.DisabledReason_NoPositionSelected;
                return false;
            }

            return true;
        }

        private void IncreasePositionFacings_Executed()
        {
            foreach (IPlanItem item in ActivePlanController.SelectedPlanItems)
            {
                if (item.PlanItemType == PlanItemType.Position)
                {
                    item.Position.IncreaseFacings();
                }
            }
        }

        #endregion

        #region DecreasePositionFacings

        /// <summary>
        /// Decrease the facings of the currently selected positions
        /// </summary>
        public static RelayCommand DecreasePositionFacings
        {
            get { return Instance.DecreasePositionFacingsCommand; }
        }

        private RelayCommand _decreasePositionFacingsCommand;

        public RelayCommand DecreasePositionFacingsCommand
        {
            get
            {
                if (_decreasePositionFacingsCommand == null)
                {
                    _decreasePositionFacingsCommand = new RelayCommand(
                        p => DecreasePositionFacings_Executed(),
                        p => DecreasePositionFacings_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_DecreasePositionFacings,
                        FriendlyDescription = Message.Ribbon_DecreasePositionFacings_Desc,
                        Icon = ImageResources.Ribbon_DecreasePositionFacings_32,
                        SmallIcon = ImageResources.Ribbon_DecreasePositionFacings_16
                    };
                    RegisterCommand(_decreasePositionFacingsCommand);
                }
                return _decreasePositionFacingsCommand;
            }
        }

        private Boolean DecreasePositionFacings_CanExecute()
        {
            //must have an active controller
            if (ActivePlanController == null)
            {
                this.DecreasePositionFacingsCommand.DisabledReason = Message.DisabledReason_NoPositionSelected;
                return false;
            }

            //must have a position selected
            if (ActivePlanController.SelectedPlanItems.All(s => s.PlanItemType != PlanItemType.Position))
            {
                this.DecreasePositionFacingsCommand.DisabledReason = Message.DisabledReason_NoPositionSelected;
                return false;
            }

            //at least one of the position must have a facing to be removed.
            if (!ActivePlanController.SelectedPlanItems.Where(s => s.PlanItemType == PlanItemType.Position)
                .Any(p => p.Position.Model.GetFacings(p.SubComponent.ModelPlacement) > 1))
            {
                this.DecreasePositionFacingsCommand.DisabledReason = Message.Ribbon_DecreasePositionFacings_DisabledNoFacings;
                return false;
            }


            return true;
        }

        private void DecreasePositionFacings_Executed()
        {
            foreach (IPlanItem item in ActivePlanController.SelectedPlanItems)
            {
                if (item.PlanItemType == PlanItemType.Position)
                {
                    item.Position.DecreaseFacings();
                }
            }
        }

        #endregion

        #region ShowHighlightEditor

        /// <summary>
        /// Shows the highlight editor window.
        /// </summary>
        public static RelayCommand ShowHighlightEditor
        {
            get { return Instance.ShowHighlightEditorCommand; }
        }

        private RelayCommand _showHighlightEditorCommand;

        /// <summary>
        /// Shows the highlight editor window.
        /// </summary>
        public RelayCommand ShowHighlightEditorCommand
        {
            get
            {
                if (_showHighlightEditorCommand == null)
                {
                    _showHighlightEditorCommand = new RelayCommand(
                        p => ShowHighlightEditor_Executed(), p => ShowHighlightEditor_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ShowHighlightEditor,
                        FriendlyDescription = Message.Ribbon_ShowHighlightEditor_Desc,
                        Icon = ImageResources.View_ManageHighlights_32,
                        SmallIcon = ImageResources.View_ManageHighlights_16
                    };
                    RegisterCommand(_showHighlightEditorCommand);
                }
                return _showHighlightEditorCommand;
            }
        }

        private Boolean ShowHighlightEditor_CanExecute()
        {
            if (ActivePlanController == null || ActivePlanController.SelectedPlanDocument == null)
            {
                this.ShowHighlightEditorCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }
            return true;
        }

        private void ShowHighlightEditor_Executed()
        {
            MainViewModel.ShowHighlightEditor();
        }

        #endregion

        #region SelectHighlight

        public static RelayCommand SelectHighlight
        {
            get { return Instance.SelectHighlightCommand; }
        }

        private RelayCommand _selectHighlightCommand;

        /// <summary>
        /// Shows the product label editor window.
        /// </summary>
        public RelayCommand SelectHighlightCommand
        {
            get
            {
                if (_selectHighlightCommand == null)
                {
                    _selectHighlightCommand = new RelayCommand(
                        p => SelectHighlightCommand_Executed(p),
                        p => SelectHighlightCommand_CanExecute())
                    {
                        SmallIcon = ImageResources.View_ManageHighlights_16
                    };
                    RegisterCommand(_selectHighlightCommand);
                }
                return _selectHighlightCommand;

            }
        }

        private Boolean SelectHighlightCommand_CanExecute()
        {
            if (ActivePlanController == null || ActivePlanController.SelectedPlanDocument == null)
            {
                this.SelectHighlightCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }
            if (ActivePlanController.SelectedPlanDocument != null && !ActivePlanController.SelectedPlanDocument.IsHighlightingSupported)
            {
                this.SelectHighlightCommand.DisabledReason = Message.DisabledReason_NotSupportedByDocument;
                return false;
            }

            return true;
        }

        private void SelectHighlightCommand_Executed(Object arg)
        {
            HighlightItem item;

            HighlightInfo info = arg as HighlightInfo;
            if (info != null)
            {
                MainViewModel.SetHighlight(info);
                return;
            }
            else if ((item = arg as HighlightItem) != null)
            {
                MainViewModel.SetHighlight(item);
            }
            else
            {
                MainViewModel.ClearHighlight();
            }
        }

        #endregion

        #region ApplyHighlightAsProductColour

        /// <summary>
        /// Updates the product colours of the Planogram using the current highlight.
        /// </summary>
        public static RelayCommand ApplyHighlightAsProductColour
        {
            get { return Instance.ApplyHighlightAsProductColourCommand; }
        }

        private RelayCommand _applyHighlightAsProductColourCommand;

        /// <summary>
        /// Updates the product colours of the Planogram using the current highlight.
        /// </summary>
        public RelayCommand ApplyHighlightAsProductColourCommand
        {
            get
            {
                if (_applyHighlightAsProductColourCommand == null)
                {
                    _applyHighlightAsProductColourCommand = new RelayCommand(
                        p => ApplyHighlightAsProductColour_Executed(), p => ApplyHighlightAsProductColour_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ApplyHighlightAsProductColor,
                        FriendlyDescription = Message.Ribbon_ApplyHighlightAsProductColor_Description,
                        Icon = ImageResources.View_ManageHighlights_32,
                        SmallIcon = ImageResources.View_ManageHighlights_16
                    };
                    RegisterCommand(_applyHighlightAsProductColourCommand);
                }
                return _applyHighlightAsProductColourCommand;
            }
        }

        private Boolean ApplyHighlightAsProductColour_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.ApplyHighlightAsProductColourCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            IPlanDocument activeDocumentWithHighlight = ActivePlanController.PlanDocuments
                .FirstOrDefault(d => d.IsActiveDocument && d.IsHighlightingSupported && d.Highlight != null);
            if (activeDocumentWithHighlight == null)
            {
                ApplyHighlightAsProductColourCommand.DisabledReason = Message.Ribbon_ApplyHighlightAsProductColor_DisabledReason;
                return false;
            }

            return true;
        }

        private void ApplyHighlightAsProductColour_Executed()
        {
            MainViewModel.ApplyHighlightAsProductColour(selectedProductsOnly: false);
        }

        #endregion

        #region ApplyHighlightAsProductColourToSelection

        /// <summary>
        /// Updates the product colours of the Planogram using the current highlight.
        /// </summary>
        public static RelayCommand ApplyHighlightAsProductColourToSelection
        {
            get { return Instance.ApplyHighlightAsProductColourToSelectionCommand; }
        }

        private RelayCommand _applyHighlightAsProductColourToSelectionCommand;

        /// <summary>
        /// Updates the product colours of the Planogram using the current highlight.
        /// </summary>
        public RelayCommand ApplyHighlightAsProductColourToSelectionCommand
        {
            get
            {
                if (_applyHighlightAsProductColourToSelectionCommand == null)
                {
                    _applyHighlightAsProductColourToSelectionCommand = new RelayCommand(
                        p => ApplyHighlightAsProductColourToSelection_Executed(),
                        p => ApplyHighlightAsProductColour_CanExecute()) // Same logic as ApplyHighlightAsProductColour command
                    {
                        FriendlyName = Message.Ribbon_ApplyHighlightAsProductColor,
                        FriendlyDescription = Message.Ribbon_ApplyHighlightAsProductColor_Description,
                        Icon = ImageResources.View_ManageHighlights_32,
                        SmallIcon = ImageResources.View_ManageHighlights_16
                    };
                    RegisterCommand(_applyHighlightAsProductColourToSelectionCommand);
                }
                return _applyHighlightAsProductColourToSelectionCommand;
            }
        }

        private void ApplyHighlightAsProductColourToSelection_Executed()
        {
            MainViewModel.ApplyHighlightAsProductColour(selectedProductsOnly: true);
        }

        #endregion

        #region ShowProductLabelEditor

        /// <summary>
        /// Shows the product label editor window.
        /// </summary>
        public static RelayCommand ShowProductLabelEditor
        {
            get { return Instance.ShowProductLabelEditorCommand; }
        }

        private RelayCommand _showProductLabelEditorCommand;

        /// <summary>
        /// Shows the product label editor window.
        /// </summary>
        public RelayCommand ShowProductLabelEditorCommand
        {
            get
            {
                if (_showProductLabelEditorCommand == null)
                {
                    _showProductLabelEditorCommand = new RelayCommand(
                        p => ShowProductLabelEditorCommand_Executed(), p => ShowProductLabelEditorCommand_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ShowProductLabelEditor,
                        FriendlyDescription = Message.Ribbon_ShowProductLabelEditor_Desc,
                        Icon = ImageResources.View_ProductLabel_32,
                        SmallIcon = ImageResources.View_ProductLabel_16
                    };
                    RegisterCommand(_showProductLabelEditorCommand);
                }
                return _showProductLabelEditorCommand;

            }
        }

        private void ShowProductLabelEditorCommand_Executed()
        {
            MainViewModel.ShowProductLabelEditor();
        }

        private Boolean ShowProductLabelEditorCommand_CanExecute()
        {
            if (ActivePlanController == null || ActivePlanController.SelectedPlanDocument == null)
            {
                this.ShowProductLabelEditorCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        #endregion

        #region SelectProductLabel

        public static RelayCommand SelectProductLabel
        {
            get { return Instance.SelectProductLabelCommand; }
        }

        private RelayCommand _selectProductLabelCommand;

        /// <summary>
        /// Shows the product label editor window.
        /// </summary>
        public RelayCommand SelectProductLabelCommand
        {
            get
            {
                if (_selectProductLabelCommand == null)
                {
                    _selectProductLabelCommand = new RelayCommand(
                        p => SelectProductLabelCommand_Executed(p),
                        p => SelectProductLabelCommand_CanExecute())
                    {
                        SmallIcon = ImageResources.View_ProductLabel_16
                    };
                    RegisterCommand(_selectProductLabelCommand);
                }
                return _selectProductLabelCommand;

            }
        }

        private Boolean SelectProductLabelCommand_CanExecute()
        {
            if (ActivePlanController == null || ActivePlanController.SelectedPlanDocument == null)
            {
                this.SelectProductLabelCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }
            if (!ActivePlanController.SelectedPlanDocument.ArePlanogramViewSettingsSupported)
            {
                this.SelectProductLabelCommand.DisabledReason = Message.DisabledReason_NotSupportedByDocument;
                return false;
            }

            return true;
        }

        private void SelectProductLabelCommand_Executed(Object arg)
        {
            LabelItem item;

            LabelInfo info = arg as LabelInfo;
            if (info != null)
            {
                MainViewModel.SetProductLabel(info);
                return;
            }
            else if ((item = arg as LabelItem) != null)
            {
                MainViewModel.SetProductLabel(item);
            }
            else
            {
                MainViewModel.ClearProductLabel();
            }
        }

        #endregion

        #region ShowFixtureLabelEditor

        /// <summary>
        /// Shows the fixture label editor window.
        /// </summary>
        public static RelayCommand ShowFixtureLabelEditor
        {
            get { return Instance.ShowFixtureLabelEditorCommand; }
        }

        private RelayCommand _showFixtureLabelEditorCommand;

        /// <summary>
        /// Shows the fixture label editor window.
        /// </summary>
        public RelayCommand ShowFixtureLabelEditorCommand
        {
            get
            {
                if (_showFixtureLabelEditorCommand == null)
                {
                    _showFixtureLabelEditorCommand = new RelayCommand(
                        p => ShowFixtureLabelEditorCommand_Executed(), p => ShowFixtureLabelEditorCommand_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ShowFixtureLabelEditor,
                        FriendlyDescription = Message.Ribbon_ShowFixtureLabelEditor_Desc,
                        Icon = ImageResources.View_FixtureLabel_32
                    };
                    RegisterCommand(_showFixtureLabelEditorCommand);
                }
                return _showFixtureLabelEditorCommand;
            }
        }

        private Boolean ShowFixtureLabelEditorCommand_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.ShowFixtureLabelEditorCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }
            return true;
        }

        private void ShowFixtureLabelEditorCommand_Executed()
        {
            MainViewModel.ShowFixtureLabelEditor();
        }

        #endregion

        #region SelectFixtureLabel

        public static RelayCommand SelectFixtureLabel
        {
            get { return Instance.SelectFixtureLabelCommand; }
        }

        private RelayCommand _selectFixtureLabelCommand;

        /// <summary>
        /// Shows the product label editor window.
        /// </summary>
        public RelayCommand SelectFixtureLabelCommand
        {
            get
            {
                if (_selectFixtureLabelCommand == null)
                {
                    _selectFixtureLabelCommand = new RelayCommand(
                        p => SelectFixtureLabelCommand_Executed(p),
                        p => SelectFixtureLabelCommand_CanExecute())
                    {
                        SmallIcon = ImageResources.View_FixtureLabel_32
                    };
                    RegisterCommand(_selectFixtureLabelCommand);
                }
                return _selectFixtureLabelCommand;

            }
        }

        private Boolean SelectFixtureLabelCommand_CanExecute()
        {
            if (ActivePlanController == null || ActivePlanController.SelectedPlanDocument == null)
            {
                this.SelectFixtureLabelCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }
            if (!ActivePlanController.SelectedPlanDocument.ArePlanogramViewSettingsSupported)
            {
                this.SelectFixtureLabelCommand.DisabledReason = Message.DisabledReason_NotSupportedByDocument;
                return false;
            }

            return true;
        }

        private void SelectFixtureLabelCommand_Executed(Object arg)
        {
            LabelItem item;

            LabelInfo info = arg as LabelInfo;
            if (info != null)
            {
                MainViewModel.SetFixtureLabel(info);
                return;
            }
            else if ((item = arg as LabelItem) != null)
            {
                MainViewModel.SetFixtureLabel(item);
            }
            else
            {
                MainViewModel.ClearFixtureLabel();
            }
        }

        #endregion

        #region Options

        /// <summary>
        /// Launches the application options window.
        /// </summary>
        public static RelayCommand Options
        {
            get { return Instance.OptionsCommand; }
        }

        private RelayCommand _optionsCommand;

        public RelayCommand OptionsCommand
        {
            get
            {
                if (_optionsCommand == null)
                {
                    _optionsCommand = new RelayCommand(
                        p => Options_Executed(p))
                    {
                        FriendlyName = Message.MainPage_Options,
                        SmallIcon = ImageResources.MainPage_Options
                    };
                    RegisterCommand(_optionsCommand);
                }
                return _optionsCommand;
            }
        }

        private void Options_Executed(Object args)
        {
            OptionsViewModel viewModel = new OptionsViewModel();
            if (args is OptionsViewModel.OptionsWindowTab)
            {
                viewModel.SelectedTab = (OptionsViewModel.OptionsWindowTab)args;
            }
            viewModel.ShowDialog();
            App.MainPageViewModel.UpdateAppSettings(viewModel);
        }

        #endregion

        #region ShowReportEditor

        /// <summary>
        /// Shows the Report editor window.
        /// </summary>
        public static RelayCommand ShowReportEditor
        {
            get { return Instance.ShowReportEditorCommand; }
        }

        private RelayCommand _showReportEditorCommand;

        /// <summary>
        /// Shows the Report editor window.
        /// </summary>
        public RelayCommand ShowReportEditorCommand
        {
            get
            {
                if (_showReportEditorCommand == null)
                {
                    _showReportEditorCommand = new RelayCommand(
                        p => ShowReportEditor_Executed(false), p => ShowReportEditor_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ShowDataSheetEditor,
                        FriendlyDescription = Message.Ribbon_ShowDataSheetEditor_Desc,
                        Icon = ImageResources.Ribbon_ReportEditor_32,
                        SmallIcon = ImageResources.Ribbon_ReportEditor_32
                    };
                    RegisterCommand(_showReportEditorCommand);
                }
                return _showReportEditorCommand;
            }
        }

        private Boolean ShowReportEditor_CanExecute()
        {
            if (ActivePlanController == null || ActivePlanController.SelectedPlanDocument == null)
            {
                this.ShowReportEditorCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }
            return true;
        }

        public static RelayCommand ShowReportEditorAsNew
        {
            get { return Instance.ShowReportEditorAsNewCommand; }
        }

        private RelayCommand _showReportEditorAsNewCommand;

        /// <summary>
        /// Shows the Report EditorAsNew window.
        /// </summary>
        public RelayCommand ShowReportEditorAsNewCommand
        {
            get
            {
                if (_showReportEditorAsNewCommand == null)
                {
                    _showReportEditorAsNewCommand = new RelayCommand(
                        p => ShowReportEditor_Executed(true), p => ShowReportEditorAsNew_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ShowDataSheetEditor,
                        FriendlyDescription = Message.Ribbon_ShowHighlightEditor_Desc,
                        Icon = ImageResources.Ribbon_ReportEditor_32,
                        SmallIcon = ImageResources.Ribbon_ReportEditor_32
                    };
                    RegisterCommand(_showReportEditorAsNewCommand);
                }
                return _showReportEditorAsNewCommand;
            }
        }

        private Boolean ShowReportEditorAsNew_CanExecute()
        {
            if (ActivePlanController == null || ActivePlanController.SelectedPlanDocument == null)
            {
                this.ShowReportEditorAsNewCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }
            return true;
        }

        private void ShowReportEditor_Executed(Boolean showNew)
        {
            //Get the report document to show the editor for.
            Boolean isNewDocument = false;
            ReportDocument doc;

            if (ActivePlanController != null && ActivePlanController.SelectedPlanDocument != null)
            {
                if (!showNew && ActivePlanController.SelectedPlanDocument.DocumentType == DocumentType.DataSheet)
                {
                    doc = (ReportDocument)MainViewModel.ActivePlanController.SelectedPlanDocument;
                }
                else
                {
                    doc = (ReportDocument)MainViewModel.ActivePlanController.AddNewDocument(DocumentType.DataSheet, /*checkForExisting*/false);
                    isNewDocument = true;
                }
                
                //show the editor window
                Boolean layoutLoaded = doc.ShowDataSheetEditorWindow();

                //if this was a new document but no layout was loaded then close it.
                if (!layoutLoaded && isNewDocument)
                {
                    MainViewModel.ActivePlanController.CloseDocument(doc);
                }
            }
        }

        #endregion

        #region SelectDataSheet

        public static RelayCommand SelectDataSheet
        {
            get { return Instance.SelectDataSheetCommand; }
        }

        private RelayCommand _selectDataSheetCommand;

        /// <summary>
        /// Shows the product label editor window.
        /// </summary>
        public RelayCommand SelectDataSheetCommand
        {
            get
            {
                if (_selectDataSheetCommand == null)
                {
                    _selectDataSheetCommand = new RelayCommand(
                        p => SelectDataSheetCommand_Executed(p),
                        p => SelectDataSheetCommand_CanExecute())
                    {
                        SmallIcon = ImageResources.Ribbon_ReportEditor_32
                    };
                    RegisterCommand(_selectDataSheetCommand);
                }
                return _selectDataSheetCommand;

            }
        }

        private Boolean SelectDataSheetCommand_CanExecute()
        {
            if (ActivePlanController == null || ActivePlanController.SelectedPlanDocument == null)
            {
                this.SelectDataSheetCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }
            return true;
        }

        private void SelectDataSheetCommand_Executed(Object arg)
        {
            CustomColumnLayout info = arg as CustomColumnLayout;
            if (info != null)
            {
                MainViewModel.AddReportDocument(info);
            }
        }

        #endregion

        #region ShowPrintTemplateSetup

        /// <summary>
        /// Shows the print template editor window.
        /// </summary>
        public static RelayCommand ShowPrintTemplateSetup
        {
            get { return Instance.ShowPrintTemplateSetupCommand; }
        }

        private RelayCommand _showPrintTemplateSetupCommand;

        /// <summary>
        /// Shows the print template editor window.
        /// </summary>
        public RelayCommand ShowPrintTemplateSetupCommand
        {
            get
            {
                if (_showPrintTemplateSetupCommand == null)
                {
                    _showPrintTemplateSetupCommand = new RelayCommand(
                        p => ShowPrintTemplateSetupCommand_Executed())
                    {
                        FriendlyName = CommonMessage.PrintTemplateSetup_Title,
                        FriendlyDescription = CommonMessage.PrintTemplateSetup_TitleDesc,
                        Icon = CommonImageResources.PrintTemplateSetup,
                        SmallIcon = CommonImageResources.PrintTemplateSetup_16
                    };
                    RegisterCommand(_showPrintTemplateSetupCommand);
                }
                return _showPrintTemplateSetupCommand;

            }
        }

        private void ShowPrintTemplateSetupCommand_Executed()
        {
            //get the active planogram
            Planogram activePlan = null;
            if (ActivePlanController != null)
            {
                activePlan = ActivePlanController.SourcePlanogram.Model;
            }

            //show the dialog.
            PrintTemplateSetupViewModel viewModel = new PrintTemplateSetupViewModel(App.ViewState.EntityId, activePlan);
            CommonHelper.GetWindowService().ShowDialog<PrintTemplateSetupWindow>(viewModel);
        }

        #endregion

        #region ToggleZoomToAreaMode

        public static RelayCommand ToggleZoomToAreaMode
        {
            get { return Instance.ToggleZoomToAreaModeCommand; }
        }

        private RelayCommand _toggleZoomToAreaModeCommand;

        /// <summary>
        /// Starts a zoom to area drag
        /// </summary>
        public RelayCommand ToggleZoomToAreaModeCommand
        {
            get
            {
                if (_toggleZoomToAreaModeCommand == null)
                {
                    _toggleZoomToAreaModeCommand = new RelayCommand(
                        p => ToggleZoomToAreaMode_Executed(),
                        p => ToggleZoomToAreaMode_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ZoomToArea,
                        FriendlyDescription = Message.Ribbon_ZoomToArea_Desc,
                        Icon = ImageResources.ZoomToArea_32,
                        SmallIcon = ImageResources.ZoomToArea_16
                    };
                    RegisterCommand(_toggleZoomToAreaModeCommand);
                }
                return _toggleZoomToAreaModeCommand;
            }
        }

        private Boolean ToggleZoomToAreaMode_CanExecute()
        {
            //must have an active controller
            if (ActivePlanController == null)
            {
                this.ToggleZoomToAreaModeCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //must have a selected document.
            if (ActivePlanController.SelectedPlanDocument == null)
            {
                this.ToggleZoomToAreaModeCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //must be supported
            if (!ActivePlanController.SelectedPlanDocument.IsZoomingSupported)
            {
                this.ToggleZoomToAreaModeCommand.DisabledReason = Message.DisabledReason_NotSupportedByDocument;
                return false;
            }

            return true;
        }

        private void ToggleZoomToAreaMode_Executed()
        {
            ActivePlanController.SelectedPlanDocument.IsInZoomToAreaMode =
                !ActivePlanController.SelectedPlanDocument.IsInZoomToAreaMode;
        }

        #endregion

        #region TogglePanningMode

        public static RelayCommand TogglePanningMode
        {
            get { return Instance.TogglePanningModeCommand; }
        }

        private RelayCommand _togglePanningModeCommand;

        /// <summary>
        /// Turns panning mode on or off
        /// </summary>
        public RelayCommand TogglePanningModeCommand
        {
            get
            {
                if (_togglePanningModeCommand == null)
                {
                    _togglePanningModeCommand = new RelayCommand(
                        p => TogglePanningMode_Executed(),
                        p => TogglePanningMode_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_TogglePanningMode,
                        FriendlyDescription = Message.Ribbon_TogglePanningMode_Desc,
                        SmallIcon = ImageResources.TogglePanningMode_16,
                        InputGestureKey = Key.H,
                        InputGestureModifiers = ModifierKeys.Shift
                    };
                    RegisterCommand(_togglePanningModeCommand);
                }
                return _togglePanningModeCommand;
            }
        }

        private Boolean TogglePanningMode_CanExecute()
        {
            //must have an active controller
            if (ActivePlanController == null)
            {
                this.TogglePanningModeCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //must have a selected document.
            if (ActivePlanController.SelectedPlanDocument == null)
            {
                this.TogglePanningModeCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //must be supported
            if (!ActivePlanController.SelectedPlanDocument.IsZoomingSupported)
            {
                this.TogglePanningModeCommand.DisabledReason = Message.DisabledReason_NotSupportedByDocument;
                return false;
            }

            return true;
        }

        private void TogglePanningMode_Executed()
        {
            ActivePlanController.SelectedPlanDocument.IsInPanningMode =
                !ActivePlanController.SelectedPlanDocument.IsInPanningMode;
        }

        #endregion

        #region ZoomToFit

        /// <summary>
        /// Zooms the view on the active document to fit the available space
        /// </summary>
        public static RelayCommand ZoomToFit
        {
            get { return Instance.ZoomToFitCommand; }
        }

        private RelayCommand _zoomToFitCommand;

        /// <summary>
        /// Zooms the view on the active document to fit the available space
        /// </summary>
        public RelayCommand ZoomToFitCommand
        {
            get
            {
                if (_zoomToFitCommand == null)
                {
                    _zoomToFitCommand = new RelayCommand(
                        p => ZoomToFit_Executed(),
                        p => ZoomToFit_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ZoomFitToScreen,
                        FriendlyDescription = Message.Ribbon_ZoomFitToScreen_Desc,
                        Icon = ImageResources.ZoomToFit_32,
                        SmallIcon = ImageResources.ZoomToFit_32,
                        InputGestureKey = Key.Home,
                        InputGestureModifiers = ModifierKeys.Control
                    };
                    RegisterCommand(_zoomToFitCommand);
                }
                return _zoomToFitCommand;
            }
        }

        private Boolean ZoomToFit_CanExecute()
        {
            //must have an active controller
            if (ActivePlanController == null)
            {
                this.ZoomToFitCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //check on the controller
            String disabledReason = null;
            if (!ActivePlanController.CanZoom(out disabledReason))
            {
                this.ZoomToFitCommand.DisabledReason = disabledReason;
                return false;
            }

            return true;
        }

        private void ZoomToFit_Executed()
        {
            ActivePlanController.SelectedPlanDocument.ZoomToFit();
        }

        #endregion

        #region ZoomToFitHeight

        /// <summary>
        /// Zooms the active document to fit by height
        /// </summary>
        public static RelayCommand ZoomToFitHeight
        {
            get { return Instance.ZoomToFitHeightCommand; }
        }

        private RelayCommand _zoomToFitHeightCommand;

        public RelayCommand ZoomToFitHeightCommand
        {
            get
            {
                if (_zoomToFitHeightCommand == null)
                {
                    _zoomToFitHeightCommand = new RelayCommand(
                        p => ZoomToFitHeight_Executed(),
                        p => ZoomToFitHeight_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ZoomToFitHeight,
                        FriendlyDescription = Message.Ribbon_ZoomToFitHeight_Desc,
                        Icon = ImageResources.ZoomToFit_32,
                        SmallIcon = ImageResources.ZoomToFit_32
                    };
                    RegisterCommand(_zoomToFitHeightCommand);
                }
                return _zoomToFitHeightCommand;
            }
        }

        private Boolean ZoomToFitHeight_CanExecute()
        {
            //must have an active controller
            if (ActivePlanController == null)
            {
                this.ZoomToFitHeightCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //check on the controller
            String disabledReason = null;
            if (!ActivePlanController.CanZoom(out disabledReason))
            {
                this.ZoomToFitHeightCommand.DisabledReason = disabledReason;
                return false;
            }

            return true;
        }

        private void ZoomToFitHeight_Executed()
        {
            ActivePlanController.SelectedPlanDocument.ZoomToFitHeight();
        }

        #endregion

        #region ZoomToFitWidth

        /// <summary>
        /// Zooms the current document to fit by width.
        /// </summary>
        public static RelayCommand ZoomToFitWidth
        {
            get { return Instance.ZoomToFitWidthCommand; }
        }

        private RelayCommand _zoomToFitWidthCommand;

        public RelayCommand ZoomToFitWidthCommand
        {
            get
            {
                if (_zoomToFitWidthCommand == null)
                {
                    _zoomToFitWidthCommand = new RelayCommand(
                        p => ZoomToFitWidth_Executed(),
                        p => ZoomToFitWidth_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ZoomToFitWidth,
                        FriendlyDescription = Message.Ribbon_ZoomToFitWidth_Desc,
                        Icon = ImageResources.ZoomToFit_32,
                        SmallIcon = ImageResources.ZoomToFit_32
                    };
                    RegisterCommand(_zoomToFitWidthCommand);
                }
                return _zoomToFitWidthCommand;
            }
        }

        private Boolean ZoomToFitWidth_CanExecute()
        {
            //must have an active controller
            if (ActivePlanController == null)
            {
                this.ZoomToFitWidthCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //check on the controller
            String disabledReason = null;
            if (!ActivePlanController.CanZoom(out disabledReason))
            {
                this.ZoomToFitWidthCommand.DisabledReason = disabledReason;
                return false;
            }

            return true;
        }

        private void ZoomToFitWidth_Executed()
        {
            ActivePlanController.SelectedPlanDocument.ZoomToFitWidth();
        }


        #endregion

        #region ZoomInCommand

        public static RelayCommand ZoomIn
        {
            get { return Instance.ZoomInCommand; }
        }


        private RelayCommand _zoomInCommand;

        /// <summary>
        /// Zooms in the view on the active document by 1 point
        /// </summary>
        public RelayCommand ZoomInCommand
        {
            get
            {
                if (_zoomInCommand == null)
                {
                    _zoomInCommand = new RelayCommand(
                        p => ZoomIn_Executed(),
                        p => ZoomIn_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ZoomIn,
                        FriendlyDescription = Message.Ribbon_ZoomIn_Desc,
                        Icon = ImageResources.ZoomIn_32,
                        SmallIcon = ImageResources.ZoomIn_16
                    };
                    RegisterCommand(_zoomInCommand);
                }
                return _zoomInCommand;
            }
        }

        private Boolean ZoomIn_CanExecute()
        {
            //if zoom to fit is not supported
            if (!this.ZoomToFitCommand.CanExecute())
            {
                this.ZoomInCommand.DisabledReason = this.ZoomToFitCommand.DisabledReason;
                return false;
            }

            return true;
        }

        private void ZoomIn_Executed()
        {
            ActivePlanController.SelectedPlanDocument.ZoomIn();
        }

        #endregion

        #region ZoomOutCommand

        public static RelayCommand ZoomOut
        {
            get { return Instance.ZoomOutCommand; }
        }

        private RelayCommand _zoomOutCommand;

        /// <summary>
        /// Zooms in the view on the active document by 1 point
        /// </summary>
        public RelayCommand ZoomOutCommand
        {
            get
            {
                if (_zoomOutCommand == null)
                {
                    _zoomOutCommand = new RelayCommand(
                        p => ZoomOut_Executed(),
                        p => ZoomOut_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ZoomOut,
                        FriendlyDescription = Message.Ribbon_ZoomOut_Desc,
                        Icon = ImageResources.ZoomOut_32,
                        SmallIcon = ImageResources.ZoomOut_16
                    };
                    RegisterCommand(_zoomOutCommand);
                }
                return _zoomOutCommand;
            }
        }

        private Boolean ZoomOut_CanExecute()
        {
            //if zoom to fit is not supported
            if (!this.ZoomToFitCommand.CanExecute())
            {
                this.ZoomOutCommand.DisabledReason = this.ZoomToFitCommand.DisabledReason;
                return false;
            }

            return true;
        }

        private void ZoomOut_Executed()
        {
            ActivePlanController.SelectedPlanDocument.ZoomOut();
        }

        #endregion

        #region ZoomSelectedPlanItems

        public static RelayCommand ZoomSelectedItems
        {
            get { return Instance.ZoomSelectedItemsCommand; }
        }

        private RelayCommand _zoomSelectedPlanItemsCommand;

        /// <summary>
        /// Zooms in the view on the active document by 1 point
        /// </summary>
        public RelayCommand ZoomSelectedItemsCommand
        {
            get
            {
                if (_zoomSelectedPlanItemsCommand == null)
                {
                    _zoomSelectedPlanItemsCommand = new RelayCommand(
                        p => ZoomSelectedPlanItems_Executed(),
                        p => ZoomSelectedPlanItems_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ZoomSelectedPlanItems,
                        FriendlyDescription = Message.Ribbon_ZoomSelectedPlanItems_Desc,
                        Icon = ImageResources.ZoomSelectedPlanItems_32,
                        SmallIcon = ImageResources.ZoomSelectedPlanItems_32,
                    };
                    RegisterCommand(_zoomSelectedPlanItemsCommand);
                }
                return _zoomSelectedPlanItemsCommand;
            }
        }

        private Boolean ZoomSelectedPlanItems_CanExecute()
        {
            var controller = ActivePlanController;

            //must have an active controller
            if (controller == null)
            {
                this.ZoomSelectedItemsCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //must have a selected document.
            if (controller.SelectedPlanDocument == null)
            {
                this.ZoomSelectedItemsCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            // zoom must be supported on either the selected document 
            // or if not on the selected document then on one of the available documents
            if (!controller.SelectedPlanDocument.IsZoomingSupported
                && !(controller.PlanDocuments.Any(p => p.IsZoomingSupported)))
            {
                this.ZoomSelectedItemsCommand.DisabledReason = Message.DisabledReason_NotSupportedByDocument;
                return false;
            }

            return true;

        }

        private void ZoomSelectedPlanItems_Executed()
        {
            var controller = ActivePlanController;

            //if the selected plan document supports zooming then execute it there
            // otherwise execute on all supporting documents for the active plan controller

            if (controller.SelectedPlanDocument.IsZoomingSupported)
            {
                controller.SelectedPlanDocument.ZoomSelectedPlanItems();
            }
            else
            {
                foreach (IPlanDocument doc in controller.PlanDocuments)
                {
                    if (doc.IsZoomingSupported)
                    {
                        doc.ZoomSelectedPlanItems();
                    }
                }

            }

        }

        #endregion

        #region NewDesignView

        /// <summary>
        /// Creates a new design view document
        /// for the current plan.
        /// </summary>
        public static RelayCommand NewDesignView
        {
            get { return Instance.NewDesignViewCommand; }
        }

        private RelayCommand _newDesignViewCommand;

        public RelayCommand NewDesignViewCommand
        {
            get
            {
                if (_newDesignViewCommand == null)
                {
                    _newDesignViewCommand = new RelayCommand(
                        p => NewDesignView_Executed(p),
                        p => NewDesignView_CanExecute())
                    {
                        FriendlyName = DocumentTypeHelper.FriendlyNames[DocumentType.Design],
                        FriendlyDescription = Message.Ribbon_NewDesignView_Desc,
                        Icon = ImageResources.PlanDocumentType_OrthFront32,
                        SmallIcon = ImageResources.PlanDocumentType_OrthFront32,
                    };
                    RegisterCommand(_newDesignViewCommand);
                }
                return _newDesignViewCommand;
            }
        }

        private Boolean NewDesignView_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.NewDesignViewCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void NewDesignView_Executed(Object args)
        {
            Boolean checkForExisting = true;
            if (args != null && args is Boolean)
            {
                checkForExisting = (Boolean)args;
            }

            ActivePlanController.AddNewDocument(DocumentType.Design, checkForExisting);
        }

        #endregion

        #region NewPerspectiveView

        /// <summary>
        /// Creates a new design view document
        /// for the current plan.
        /// </summary>
        public static RelayCommand NewPerspectiveView
        {
            get { return Instance.NewPerspectiveViewCommand; }
        }

        private RelayCommand _newPerspectiveViewCommand;

        public RelayCommand NewPerspectiveViewCommand
        {
            get
            {
                if (_newPerspectiveViewCommand == null)
                {
                    _newPerspectiveViewCommand = new RelayCommand(
                        p => NewPerspectiveView_Executed(p),
                        p => NewPerspectiveView_CanExecute())
                    {
                        FriendlyName = DocumentTypeHelper.FriendlyNames[DocumentType.Perspective],
                        FriendlyDescription = Message.Ribbon_NewPerspectiveView_Desc,
                        Icon = ImageResources.PlanDocumentType_ThreeDimesional32,
                        SmallIcon = ImageResources.PlanDocumentType_ThreeDimesional16,
                    };
                    RegisterCommand(_newPerspectiveViewCommand);
                }
                return _newPerspectiveViewCommand;
            }
        }

        private Boolean NewPerspectiveView_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.NewPerspectiveViewCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void NewPerspectiveView_Executed(Object args)
        {
            Boolean checkForExisting = true;
            if (args != null && args is Boolean)
            {
                checkForExisting = (Boolean)args;
            }

            ActivePlanController.AddNewDocument(DocumentType.Perspective, checkForExisting);
        }

        #endregion

        #region NewFrontView


        /// <summary>
        /// Creates a new design view document
        /// for the current plan.
        /// </summary>
        public static RelayCommand NewFrontView
        {
            get { return Instance.NewFrontViewCommand; }
        }

        private RelayCommand _newFrontViewCommand;

        public RelayCommand NewFrontViewCommand
        {
            get
            {
                if (_newFrontViewCommand == null)
                {
                    _newFrontViewCommand = new RelayCommand(
                        p => NewFrontView_Executed(p),
                        p => NewFrontView_CanExecute())
                    {
                        FriendlyName = DocumentTypeHelper.FriendlyNames[DocumentType.Front],
                        FriendlyDescription = Message.Ribbon_NewFrontView_Desc,
                        Icon = ImageResources.PlanDocumentType_OrthFront32,
                        SmallIcon = ImageResources.PlanDocumentType_OrthFront16,
                    };
                    RegisterCommand(_newFrontViewCommand);
                }
                return _newFrontViewCommand;
            }
        }

        private Boolean NewFrontView_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.NewFrontViewCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void NewFrontView_Executed(Object args)
        {
            Boolean checkForExisting = true;
            if (args != null && args is Boolean)
            {
                checkForExisting = (Boolean)args;
            }

            ActivePlanController.AddNewDocument(DocumentType.Front, checkForExisting);
        }

        #endregion

        #region NewBackView

        /// <summary>
        /// Creates a new design view document
        /// for the current plan.
        /// </summary>
        public static RelayCommand NewBackView
        {
            get { return Instance.NewBackViewCommand; }
        }

        private RelayCommand _newBackViewCommand;

        public RelayCommand NewBackViewCommand
        {
            get
            {
                if (_newBackViewCommand == null)
                {
                    _newBackViewCommand = new RelayCommand(
                        p => NewBackView_Executed(p),
                        p => NewBackView_CanExecute())
                    {
                        FriendlyName = DocumentTypeHelper.FriendlyNames[DocumentType.Back],
                        FriendlyDescription = Message.Ribbon_NewBackView_Desc,
                        //Icon = ImageResources.PlanDocumentType_OrthBack32,
                        SmallIcon = ImageResources.PlanDocumentType_OrthBack16,
                    };
                    RegisterCommand(_newBackViewCommand);
                }
                return _newBackViewCommand;
            }
        }

        private Boolean NewBackView_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.NewBackViewCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void NewBackView_Executed(Object args)
        {
            Boolean checkForExisting = true;
            if (args != null && args is Boolean)
            {
                checkForExisting = (Boolean)args;
            }

            ActivePlanController.AddNewDocument(DocumentType.Back, checkForExisting);
        }

        #endregion

        #region NewLeftView

        /// <summary>
        /// Creates a new design view document
        /// for the current plan.
        /// </summary>
        public static RelayCommand NewLeftView
        {
            get { return Instance.NewLeftViewCommand; }
        }

        private RelayCommand _newLeftViewCommand;

        public RelayCommand NewLeftViewCommand
        {
            get
            {
                if (_newLeftViewCommand == null)
                {
                    _newLeftViewCommand = new RelayCommand(
                        p => NewLeftView_Executed(p),
                        p => NewLeftView_CanExecute())
                    {
                        FriendlyName = DocumentTypeHelper.FriendlyNames[DocumentType.Left],
                        FriendlyDescription = Message.Ribbon_NewLeftView_Desc,
                        //Icon = ImageResources.PlanDocumentType_OrthLeft32,
                        SmallIcon = ImageResources.PlanDocumentType_OrthLeft16,
                    };
                    RegisterCommand(_newLeftViewCommand);
                }
                return _newLeftViewCommand;
            }
        }

        private Boolean NewLeftView_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.NewLeftViewCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void NewLeftView_Executed(Object args)
        {
            Boolean checkForExisting = true;
            if (args != null && args is Boolean)
            {
                checkForExisting = (Boolean)args;
            }

            ActivePlanController.AddNewDocument(DocumentType.Left, checkForExisting);
        }

        #endregion

        #region NewRightView

        /// <summary>
        /// Creates a new design view document
        /// for the current plan.
        /// </summary>
        public static RelayCommand NewRightView
        {
            get { return Instance.NewRightViewCommand; }
        }

        private RelayCommand _newRightViewCommand;

        public RelayCommand NewRightViewCommand
        {
            get
            {
                if (_newRightViewCommand == null)
                {
                    _newRightViewCommand = new RelayCommand(
                        p => NewRightView_Executed(p),
                        p => NewRightView_CanExecute())
                    {
                        FriendlyName = DocumentTypeHelper.FriendlyNames[DocumentType.Right],
                        FriendlyDescription = Message.Ribbon_NewRightView_Desc,
                        //Icon = ImageResources.PlanDocumentType_OrthRight32,
                        SmallIcon = ImageResources.PlanDocumentType_OrthRight16,
                    };
                    RegisterCommand(_newRightViewCommand);
                }
                return _newRightViewCommand;
            }
        }

        private Boolean NewRightView_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.NewRightViewCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void NewRightView_Executed(Object args)
        {
            Boolean checkForExisting = true;
            if (args != null && args is Boolean)
            {
                checkForExisting = (Boolean)args;
            }

            ActivePlanController.AddNewDocument(DocumentType.Right, checkForExisting);
        }

        #endregion

        #region NewTopView

        /// <summary>
        /// Creates a new design view document
        /// for the current plan.
        /// </summary>
        public static RelayCommand NewTopView
        {
            get { return Instance.NewTopViewCommand; }
        }

        private RelayCommand _newTopViewCommand;

        public RelayCommand NewTopViewCommand
        {
            get
            {
                if (_newTopViewCommand == null)
                {
                    _newTopViewCommand = new RelayCommand(
                        p => NewTopView_Executed(p),
                        p => NewTopView_CanExecute())
                    {
                        FriendlyName = DocumentTypeHelper.FriendlyNames[DocumentType.Top],
                        FriendlyDescription = Message.Ribbon_NewTopView_Desc,
                        //Icon = ImageResources.PlanDocumentType_OrthTop32,
                        SmallIcon = ImageResources.PlanDocumentType_OrthTop16,
                    };
                    RegisterCommand(_newTopViewCommand);
                }
                return _newTopViewCommand;
            }
        }

        private Boolean NewTopView_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.NewTopViewCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void NewTopView_Executed(Object args)
        {
            Boolean checkForExisting = true;
            if (args != null && args is Boolean)
            {
                checkForExisting = (Boolean)args;
            }

            ActivePlanController.AddNewDocument(DocumentType.Top, checkForExisting);
        }

        #endregion

        #region NewBottomView

        /// <summary>
        /// Creates a new design view document
        /// for the current plan.
        /// </summary>
        public static RelayCommand NewBottomView
        {
            get { return Instance.NewBottomViewCommand; }
        }

        private RelayCommand _newBottomViewCommand;

        public RelayCommand NewBottomViewCommand
        {
            get
            {
                if (_newBottomViewCommand == null)
                {
                    _newBottomViewCommand = new RelayCommand(
                        p => NewBottomView_Executed(p),
                        p => NewBottomView_CanExecute())
                    {
                        FriendlyName = DocumentTypeHelper.FriendlyNames[DocumentType.Bottom],
                        FriendlyDescription = Message.Ribbon_NewBottomView_Desc,
                        //Icon = ImageResources.PlanDocumentType_OrthBottom32,
                        SmallIcon = ImageResources.PlanDocumentType_OrthBottom16,
                    };
                    RegisterCommand(_newBottomViewCommand);
                }
                return _newBottomViewCommand;
            }
        }

        private Boolean NewBottomView_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.NewBottomViewCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void NewBottomView_Executed(Object args)
        {
            Boolean checkForExisting = true;
            if (args != null && args is Boolean)
            {
                checkForExisting = (Boolean)args;
            }

            ActivePlanController.AddNewDocument(DocumentType.Bottom, checkForExisting);
        }

        #endregion

        #region NewProductListView

        /// <summary>
        /// Creates a new design view document
        /// for the current plan.
        /// </summary>
        public static RelayCommand NewProductListView
        {
            get { return Instance.NewProductListViewCommand; }
        }

        private RelayCommand _newProductListViewCommand;

        public RelayCommand NewProductListViewCommand
        {
            get
            {
                if (_newProductListViewCommand == null)
                {
                    _newProductListViewCommand = new RelayCommand(
                        p => NewProductListView_Executed(p),
                        p => NewProductListView_CanExecute())
                    {
                        FriendlyName = DocumentTypeHelper.FriendlyNames[DocumentType.ProductList],
                        FriendlyDescription = Message.Ribbon_NewProductListView_Desc,
                        Icon = ImageResources.PlanDocumentType_ProductList32,
                        SmallIcon = ImageResources.PlanDocumentType_ProductList16,
                    };
                    RegisterCommand(_newProductListViewCommand);
                }
                return _newProductListViewCommand;
            }
        }

        private Boolean NewProductListView_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.NewProductListViewCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void NewProductListView_Executed(Object args)
        {
            Boolean checkForExisting = true;
            if (args != null && args is Boolean)
            {
                checkForExisting = (Boolean)args;
            }

            ActivePlanController.AddNewDocument(DocumentType.ProductList, checkForExisting);
        }

        #endregion

        #region NewFixtureListView

        /// <summary>
        /// Creates a new design view document
        /// for the current plan.
        /// </summary>
        public static RelayCommand NewFixtureListView
        {
            get { return Instance.NewFixtureListViewCommand; }
        }

        private RelayCommand _newFixtureListViewCommand;

        public RelayCommand NewFixtureListViewCommand
        {
            get
            {
                if (_newFixtureListViewCommand == null)
                {
                    _newFixtureListViewCommand = new RelayCommand(
                        p => NewFixtureListView_Executed(p),
                        p => NewFixtureListView_CanExecute())
                    {
                        FriendlyName = DocumentTypeHelper.FriendlyNames[DocumentType.FixtureList],
                        FriendlyDescription = Message.Ribbon_NewFixtureListView_Desc,
                        Icon = ImageResources.PlanDocumentType_OrthFront32,
                        SmallIcon = ImageResources.PlanDocumentType_OrthFront32,
                    };
                    RegisterCommand(_newFixtureListViewCommand);
                }
                return _newFixtureListViewCommand;
            }
        }

        private Boolean NewFixtureListView_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.NewFixtureListViewCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void NewFixtureListView_Executed(Object args)
        {
            Boolean checkForExisting = true;
            if (args != null && args is Boolean)
            {
                checkForExisting = (Boolean)args;
            }


            ActivePlanController.AddNewDocument(DocumentType.FixtureList, checkForExisting);
        }

        #endregion

        #region NewAssortmentView

        /// <summary>
        /// Creates a new design view document
        /// for the current plan.
        /// </summary>
        public static RelayCommand NewAssortmentView
        {
            get { return Instance.NewAssortmentViewCommand; }
        }

        private RelayCommand _newAssortmentViewCommand;

        public RelayCommand NewAssortmentViewCommand
        {
            get
            {
                if (_newAssortmentViewCommand == null)
                {
                    _newAssortmentViewCommand = new RelayCommand(
                        p => NewAssortmentView_Executed(p),
                        p => NewAssortmentView_CanExecute())
                    {
                        FriendlyName = DocumentTypeHelper.FriendlyNames[DocumentType.Assortment],
                        FriendlyDescription = Message.Ribbon_AssortmentDocument_Desc,
                        Icon = ImageResources.PlanDocumentType_Assortment32,
                        SmallIcon = ImageResources.PlanDocumentType_Assortment16,
                    };
                    RegisterCommand(_newAssortmentViewCommand);
                }
                return _newAssortmentViewCommand;
            }
        }

        private Boolean NewAssortmentView_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.NewAssortmentViewCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void NewAssortmentView_Executed(Object args)
        {
            Boolean checkForExisting = true;
            if (args != null && args is Boolean)
            {
                checkForExisting = (Boolean)args;
            }

            ActivePlanController.AddNewDocument(DocumentType.Assortment, checkForExisting);
        }

        #endregion

        #region NewConsumerDecisionTreeView

        /// <summary>
        /// Creates a new design view document
        /// for the current plan.
        /// </summary>
        public static RelayCommand NewConsumerDecisionTreeView
        {
            get { return Instance.NewConsumerDecisionTreeViewCommand; }
        }

        private RelayCommand _newConsumerDecisionTreeViewCommand;

        public RelayCommand NewConsumerDecisionTreeViewCommand
        {
            get
            {
                if (_newConsumerDecisionTreeViewCommand == null)
                {
                    _newConsumerDecisionTreeViewCommand = new RelayCommand(
                        p => NewConsumerDecisionTreeView_Executed(p),
                        p => NewConsumerDecisionTreeView_CanExecute())
                    {
                        FriendlyName = DocumentTypeHelper.FriendlyNames[DocumentType.ConsumerDecisionTree],
                        FriendlyDescription = Message.Ribbon_ConsumerDecisionTreeDocument_Desc,
                        Icon = ImageResources.PlanDocumentType_CDT32,
                        SmallIcon = ImageResources.PlanDocumentType_CDT16
                    };
                    RegisterCommand(_newConsumerDecisionTreeViewCommand);
                }
                return _newConsumerDecisionTreeViewCommand;
            }
        }

        private Boolean NewConsumerDecisionTreeView_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.NewConsumerDecisionTreeViewCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void NewConsumerDecisionTreeView_Executed(Object args)
        {
            Boolean checkForExisting = true;
            if (args != null && args is Boolean)
            {
                checkForExisting = (Boolean)args;
            }

            ActivePlanController.AddNewDocument(DocumentType.ConsumerDecisionTree, checkForExisting);
        }

        #endregion

        #region NewBlockingView

        /// <summary>
        /// Creates a new design view document
        /// for the current plan.
        /// </summary>
        public static RelayCommand NewBlockingView
        {
            get { return Instance.NewBlockingViewCommand; }
        }

        private RelayCommand _newBlockingViewCommand;

        public RelayCommand NewBlockingViewCommand
        {
            get
            {
                if (_newBlockingViewCommand == null)
                {
                    _newBlockingViewCommand = new RelayCommand(
                        p => NewBlockingView_Executed(p),
                        p => NewBlockingView_CanExecute())
                    {
                        FriendlyName = DocumentTypeHelper.FriendlyNames[DocumentType.Blocking],
                        FriendlyDescription = Message.Ribbon_BlockingDocument_Desc,
                        Icon = ImageResources.PlanDocumentType_Blocking32,
                        SmallIcon = ImageResources.PlanDocumentType_Blocking16
                    };
                    RegisterCommand(_newBlockingViewCommand);
                }
                return _newBlockingViewCommand;
            }
        }

        private Boolean NewBlockingView_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.NewBlockingViewCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void NewBlockingView_Executed(Object args)
        {
            Boolean checkForExisting = true;
            if (args != null && args is Boolean)
            {
                checkForExisting = (Boolean)args;
            }

            ActivePlanController.AddNewDocument(DocumentType.Blocking, checkForExisting);
        }


        #endregion

        #region NewPlanogramCompareView

        /// <summary>
        ///     Provides static access to the command <see cref="NewPlanogramCompareViewCommand" />.
        /// </summary>
        public static RelayCommand NewPlanogramCompareView { get { return Instance.NewPlanogramCompareViewCommand; } }

        /// <summary>
        ///     Stores the command accessed through <see cref="NewPlanogramCompareViewCommand" />.
        /// </summary>
        /// <remarks>
        ///     This command is lazy initialized when <see cref="NewPlanogramCompareViewCommand" /> is used for the first
        ///     time.
        /// </remarks>
        private RelayCommand _newPlanogramCompareViewCommand;

        /// <summary>
        ///     This command creates/shows the Planogram Compare Plan Document for the current plan.
        /// </summary>
        /// <remarks> 
        ///     This command is disabled when there is no current plan selected.
        ///     <para />
        ///     The command is initialized lazily when used for the first time.
        /// </remarks>
        private RelayCommand NewPlanogramCompareViewCommand
        {
            get
            {
                if (_newPlanogramCompareViewCommand != null) return _newPlanogramCompareViewCommand;

                //  Initialize the command when used for the first time.
                _newPlanogramCompareViewCommand =
                    new RelayCommand(NewPlanogramCompareView_Executed, p => NewPlanogramCompareView_CanExecute())
                    {
                        FriendlyName = DocumentTypeHelper.FriendlyNames[DocumentType.PlanogramComparison],
                        FriendlyDescription = CommonMessage.Ribbon_PlanogramCompareDocument_Desc,
                        Icon = ImageResources.PlanDocumentType_PlanogramComparison32,
                        SmallIcon = ImageResources.PlanDocumentType_PlanogramComparison16
                    };
                RegisterCommand(_newPlanogramCompareViewCommand);

                return _newPlanogramCompareViewCommand;
            }
        }

        /// <summary>
        ///     Determine whether the command <see cref="NewPlanogramCompareViewCommand" /> can be executed or not.
        /// </summary>
        /// <returns><c>True</c> if the command can be executed, <c>false</c> if:
        ///     <para />
        ///     - There is no current plan selected.
        /// </returns>
        private Boolean NewPlanogramCompareView_CanExecute()
        {
            //  The command needs a current plan selected.
            if (ActivePlanController == null)
            {
                NewPlanogramCompareViewCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private static void NewPlanogramCompareView_Executed(Object args)
        {
            Boolean checkForExisting = !(args is Boolean) || (Boolean)args;

            ActivePlanController.AddNewDocument(DocumentType.PlanogramComparison, checkForExisting);
        }

        #endregion

        #region NewCategoryInsightView

        /// <summary>
        /// Creates a new category insight view document for the current plan.
        /// </summary>
        public static RelayCommand NewCategoryInsightView
        {
            get { return Instance.NewCategoryInsightViewCommand; }
        }

        private RelayCommand _newCategoryInsightViewCommand;

        public RelayCommand NewCategoryInsightViewCommand
        {
            get
            {
                if (_newCategoryInsightViewCommand == null)
                {
                    _newCategoryInsightViewCommand = new RelayCommand(
                        p => NewCategoryInsightView_Executed(p),
                        p => NewCategoryInsightView_CanExecute())
                    {
                        FriendlyName = DocumentTypeHelper.FriendlyNames[DocumentType.CategoryInsight],
                        FriendlyDescription = Message.Ribbon_CategoryInsightDocument_Desc,
                        Icon = ImageResources.PlanDocumentType_CategoryInsight32,
                        SmallIcon = ImageResources.PlanDocumentType_CategoryInsight16
                    };
                    RegisterCommand(_newCategoryInsightViewCommand);
                }
                return _newCategoryInsightViewCommand;
            }
        }

        private Boolean NewCategoryInsightView_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.NewCategoryInsightViewCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            if (!App.ViewState.IsConnectedToRepository)
            {
                this.NewCategoryInsightViewCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }

            return true;
        }

        private void NewCategoryInsightView_Executed(Object args)
        {
            Boolean checkForExisting = true;
            if (args != null && args is Boolean) checkForExisting = (Boolean)args;

            ActivePlanController.AddNewDocument(DocumentType.CategoryInsight, checkForExisting);
        }


        #endregion

        #region DuplicateCurrentView

        /// <summary>
        /// Duplicates the current view
        /// </summary>
        public static RelayCommand DuplicateCurrentView
        {
            get { return Instance.DuplicateCurrentViewCommand; }
        }

        private RelayCommand _duplicateCurrentViewCommand;

        public RelayCommand DuplicateCurrentViewCommand
        {
            get
            {
                if (_duplicateCurrentViewCommand == null)
                {
                    _duplicateCurrentViewCommand = new RelayCommand(
                        p => DuplicateCurrentViewView_Executed(),
                        p => DuplicateCurrentViewView_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_DuplicateCurrentView,
                        FriendlyDescription = Message.Ribbon_DuplicateCurrentView_Desc,
                        SmallIcon = ImageResources.Ribbon_DuplicateCurrentView_16,
                    };
                    RegisterCommand(_duplicateCurrentViewCommand);
                }
                return _duplicateCurrentViewCommand;
            }
        }

        private Boolean DuplicateCurrentViewView_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.DuplicateCurrentViewCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            if (ActivePlanController.SelectedPlanDocument == null)
            {
                this.DuplicateCurrentViewCommand.DisabledReason = Message.DisabledReason_NoSelectedItem;
                return false;
            }

            return true;
        }

        private void DuplicateCurrentViewView_Executed()
        {
            ActivePlanController.AddNewDocument(
                ActivePlanController.SelectedPlanDocument.DocumentType,
                /*checkForExisting*/false);
        }

        #endregion

        #region ArrangeAllViews

        /// <summary>
        /// Arranges all panels within the active controller
        /// </summary>
        public static RelayCommand ArrangeAllViews
        {
            get { return Instance.ArrangeAllViewsCommand; }
        }

        private RelayCommand _arrangeAllViewsCommand;

        /// <summary>
        /// Arranges all panels within the active controller
        /// </summary>
        public RelayCommand ArrangeAllViewsCommand
        {
            get
            {
                if (_arrangeAllViewsCommand == null)
                {
                    _arrangeAllViewsCommand = new RelayCommand(
                        p => ArrangeAllViews_Executed(),
                        p => ArrangeAllViews_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ViewsArrangeAll,
                        FriendlyDescription = Message.Ribbon_ViewsArrangeAll_Desc,
                        Icon = ImageResources.View_ViewsArrangeAll_32,
                        SmallIcon = ImageResources.View_ViewsArrangeAll_32
                    };
                    RegisterCommand(_arrangeAllViewsCommand);
                }
                return _arrangeAllViewsCommand;
            }
        }

        private Boolean ArrangeAllViews_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.ArrangeAllViewsCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void ArrangeAllViews_Executed()
        {
            ActivePlanController.ArrangeAllWindows();
        }

        #endregion

        #region DockAllViews

        public static RelayCommand DockAllViews
        {
            get { return Instance.DockAllViewsCommand; }
        }

        private RelayCommand _dockAllViewsCommand;

        /// <summary>
        /// Docks all views for the active planogram into a single tab group 
        /// with the active view selected.
        /// </summary>
        public RelayCommand DockAllViewsCommand
        {
            get
            {
                if (_dockAllViewsCommand == null)
                {
                    _dockAllViewsCommand = new RelayCommand(
                        p => DockAllViews_Executed(),
                        p => DockAllViews_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ViewsDockAll,
                        FriendlyDescription = Message.Ribbon_ViewsDockAll_Desc,
                        SmallIcon = ImageResources.View_ViewsDockAll_16
                    };
                    RegisterCommand(_dockAllViewsCommand);
                }
                return _dockAllViewsCommand;
            }
        }

        private Boolean DockAllViews_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.DockAllViewsCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void DockAllViews_Executed()
        {
            ActivePlanController.DockAllViews();
        }

        #endregion

        #region TileViewsHorizontal

        /// <summary>
        /// Tiles all view panels horizontally within the active planogram
        /// </summary>
        public static RelayCommand TileViewsHorizontal
        {
            get { return Instance.TileViewsHorizontalCommand; }
        }

        private RelayCommand _tileViewsHorizontalCommand;

        /// <summary>
        /// Tiles all view panels horizontally within the active planogram
        /// </summary>
        public RelayCommand TileViewsHorizontalCommand
        {
            get
            {
                if (_tileViewsHorizontalCommand == null)
                {
                    _tileViewsHorizontalCommand = new RelayCommand(
                        p => TileViewsHorizontal_Executed(),
                        p => TileViewsHorizontal_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ViewsTileHoriz,
                        FriendlyDescription = Message.Ribbon_ViewsTileHoriz_Desc,
                        SmallIcon = ImageResources.View_ViewsTileHoriz_16
                    };
                    RegisterCommand(_tileViewsHorizontalCommand);
                }
                return _tileViewsHorizontalCommand;
            }
        }

        private Boolean TileViewsHorizontal_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.TileViewsHorizontalCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void TileViewsHorizontal_Executed()
        {
            ActivePlanController.TileWindows(Orientation.Horizontal);
        }

        #endregion

        #region TileViewsVertical

        public static RelayCommand TileViewsVertical
        {
            get { return Instance.TileViewsVerticalCommand; }
        }

        private RelayCommand _tileViewsVerticalCommand;

        /// <summary>
        /// Tiles all view panels vertically within the active planogram window
        /// </summary>
        public RelayCommand TileViewsVerticalCommand
        {
            get
            {
                if (_tileViewsVerticalCommand == null)
                {
                    _tileViewsVerticalCommand = new RelayCommand(
                        p => TileViewsVertical_Executed(),
                        p => TileViewsVertical_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ViewsTileVert,
                        FriendlyDescription = Message.Ribbon_ViewsTileVert_Desc,
                        SmallIcon = ImageResources.View_ViewsTileVert_16
                    };
                    RegisterCommand(_tileViewsVerticalCommand);
                }
                return _tileViewsVerticalCommand;
            }
        }

        private Boolean TileViewsVertical_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.TileViewsVerticalCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void TileViewsVertical_Executed()
        {
            ActivePlanController.TileWindows(Orientation.Vertical);
        }

        #endregion

        #region SwitchToView

        /// <summary>
        /// Switches to the window holding the given planogram controller
        /// param: IPlanDocument to switch to.
        /// </summary>
        public static RelayCommand SwitchToView
        {
            get { return Instance.SwitchToViewCommand; }
        }

        private RelayCommand _switchToViewCommand;

        /// <summary>
        /// Switches to the window holding the given planogram controller
        /// param: IPlanDocument to switch to.
        /// </summary>
        public RelayCommand SwitchToViewCommand
        {
            get
            {
                if (_switchToViewCommand == null)
                {
                    _switchToViewCommand = new RelayCommand(
                        p => SwitchToView_Executed(p),
                        p => SwitchToView_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_WinSwitch,
                        FriendlyDescription = Message.Ribbon_WinSwitch_Desc,
                        SmallIcon = ImageResources.View_WinSwitch_16
                    };
                    RegisterCommand(_switchToViewCommand);
                }
                return _switchToViewCommand;
            }
        }

        private Boolean SwitchToView_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.SwitchToViewCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void SwitchToView_Executed(Object args)
        {
            MainViewModel.SwitchToView(args as IPlanDocument);
        }

        #endregion

        #region TileWindowsHorizontal

        /// <summary>
        /// Tiles all view panels horizontally within the active planogram
        /// </summary>
        public static RelayCommand TileWindowsHorizontal
        {
            get { return Instance.TileWindowsHorizontalCommand; }
        }

        private RelayCommand _tileWindowsHorizontalCommand;

        /// <summary>
        /// Tiles all view panels horizontally within the active planogram
        /// </summary>
        public RelayCommand TileWindowsHorizontalCommand
        {
            get
            {
                if (_tileWindowsHorizontalCommand == null)
                {
                    _tileWindowsHorizontalCommand = new RelayCommand(
                        p => TileWindowsHorizontal_Executed(),
                        p => TileWindowsHorizontal_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_WinTileHoriz,
                        FriendlyDescription = Message.Ribbon_WinTileHoriz_Desc,
                        SmallIcon = ImageResources.View_WinTileHoriz_16
                    };
                    RegisterCommand(_tileWindowsHorizontalCommand);
                }
                return _tileWindowsHorizontalCommand;
            }
        }

        private Boolean TileWindowsHorizontal_CanExecute()
        {
            if (MainViewModel == null || MainViewModel.PlanControllers.Count < 2)
            {
                return false;
            }

            return true;
        }

        private void TileWindowsHorizontal_Executed()
        {
            MainViewModel.TileWindows(Orientation.Horizontal);
        }

        #endregion

        #region TileWindowsVertical

        /// <summary>
        /// Tiles all view panels vertically within the active planogram window
        /// </summary>
        public static RelayCommand TileWindowsVertical
        {
            get { return Instance.TileWindowsVerticalCommand; }
        }

        private RelayCommand _tileWindowsVerticalCommand;

        /// <summary>
        /// Tiles all view panels vertically within the active planogram window
        /// </summary>
        public RelayCommand TileWindowsVerticalCommand
        {
            get
            {
                if (_tileWindowsVerticalCommand == null)
                {
                    _tileWindowsVerticalCommand = new RelayCommand(
                        p => TileWindowsVertical_Executed(),
                        p => TileWindowsVertical_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_WinTileVert,
                        FriendlyDescription = Message.Ribbon_WinTileVert_Desc,
                        SmallIcon = ImageResources.View_WinTileVert_16
                    };
                    RegisterCommand(_tileWindowsVerticalCommand);
                }
                return _tileWindowsVerticalCommand;
            }
        }

        private Boolean TileWindowsVertical_CanExecute()
        {
            if (MainViewModel == null || MainViewModel.PlanControllers.Count < 2)
            {
                return false;
            }

            return true;
        }

        private void TileWindowsVertical_Executed()
        {
            MainViewModel.TileWindows(Orientation.Vertical);
        }

        #endregion

        #region DockAllWindows

        /// <summary>
        /// Docks all view panels into default tabbed view within the active planogram
        /// </summary>
        public static RelayCommand DockAllWindows
        {
            get { return Instance.DockAllWindowsCommand; }
        }

        private RelayCommand _dockAllWindowsCommand;

        /// <summary>
        /// Docks all view panels into default tabbed view within the active planogram
        /// </summary>
        public RelayCommand DockAllWindowsCommand
        {
            get
            {
                if (_dockAllWindowsCommand == null)
                {
                    _dockAllWindowsCommand = new RelayCommand(
                        p => DockAllWindows_Executed(),
                        p => DockAllWindows_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_WinDockAll,
                        FriendlyDescription = Message.Ribbon_WinDockAll_Desc,
                        SmallIcon = ImageResources.View_WinDockAll_16
                    };
                    RegisterCommand(_dockAllWindowsCommand);
                }
                return _dockAllWindowsCommand;
            }
        }

        private Boolean DockAllWindows_CanExecute()
        {
            if (MainViewModel == null || MainViewModel.PlanControllers.Count < 2)
            {
                return false;
            }

            return true;
        }

        private void DockAllWindows_Executed()
        {
            MainViewModel.DockWindows();
        }

        #endregion

        #region ShowHideLibraryPanel

        public static RelayCommand ShowHideLibraryPanel
        {
            get { return Instance.ShowHideLibraryPanelCommand; }
        }

        private RelayCommand _showHideLibraryPanelCommand;

        /// <summary>
        /// Toggles the visibility of the library panel.
        /// </summary>
        public RelayCommand ShowHideLibraryPanelCommand
        {
            get
            {
                if (_showHideLibraryPanelCommand == null)
                {
                    _showHideLibraryPanelCommand = new RelayCommand(
                        p => ShowHideLibraryPanel_Executed())
                    {
                        FriendlyName = Message.Ribbon_ShowHideLibraryPanel,
                        FriendlyDescription = Message.Ribbon_ShowHideLibraryPanel_Desc,
                        Icon = ImageResources.Ribbon_ShowHideLibraryPanel_32,
                        SmallIcon = ImageResources.Ribbon_ShowHideLibraryPanel_32
                    };
                    RegisterCommand(_showHideLibraryPanelCommand);
                }
                return _showHideLibraryPanelCommand;
            }
        }

        private void ShowHideLibraryPanel_Executed()
        {
            MainViewModel.IsLibraryPanelVisible = !MainViewModel.IsLibraryPanelVisible;
        }

        #endregion

        #region ShowHidePropertiesPanel

        /// <summary>
        /// Toggles the visibility of the properties panel.
        /// </summary>
        public static RelayCommand ShowHidePropertiesPanel
        {
            get { return Instance.ShowHidePropertiesPanelCommand; }
        }

        private RelayCommand _showHidePropertiesPanelCommand;

        /// <summary>
        /// Toggles the visibility of the properties panel.
        /// </summary>
        public RelayCommand ShowHidePropertiesPanelCommand
        {
            get
            {
                if (_showHidePropertiesPanelCommand == null)
                {
                    _showHidePropertiesPanelCommand = new RelayCommand(
                        p => ShowHidePropertiesPanel_Executed(),
                        p => ShowHidePropertiesPanel_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_Properties,
                        FriendlyDescription = Message.Ribbon_PropertiesPanel,
                        Icon = ImageResources.PlanItemProperties_32,
                        SmallIcon = ImageResources.PlanItemProperties_16
                    };
                    RegisterCommand(_showHidePropertiesPanelCommand);
                }
                return _showHidePropertiesPanelCommand;
            }
        }

        private Boolean ShowHidePropertiesPanel_CanExecute()
        {
            var planController = ActivePlanController;

            if (planController == null)
            {
                this.ShowHidePropertiesPanelCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            this.ShowHidePropertiesPanelCommand.DisabledReason = null;
            return true;
        }

        private void ShowHidePropertiesPanel_Executed()
        {
            ActivePlanController.IsPropertiesPanelVisible = !ActivePlanController.IsPropertiesPanelVisible;
        }

        #endregion

        #region Product Library

        /// <summary>
        /// Launches the application product library maintenance window.
        /// </summary>
        public static RelayCommand ProductLibraryMaintenance
        {
            get { return Instance.ProductLibraryMaintenanceCommand; }
        }

        private RelayCommand _productLibraryMaintenanceCommand;

        public RelayCommand ProductLibraryMaintenanceCommand
        {
            get
            {
                if (_productLibraryMaintenanceCommand == null)
                {
                    _productLibraryMaintenanceCommand = new RelayCommand(
                        p => ProductLibraryMaintenance_Executed(p))
                    {
                        FriendlyName = Message.Ribbon_ProductLibraryContext_ProductLibraryMaintenance,
                        FriendlyDescription = Message.Ribbon_ProductLibraryContext_ProductLibraryMaintenance_Desc,
                        Icon = ImageResources.ProductLibraryContext_ProductLibraryMaintenance_32,
                        SmallIcon = ImageResources.ProductLibraryContext_ProductLibraryMaintenance_32
                    };
                    RegisterCommand(_productLibraryMaintenanceCommand);
                }
                return _productLibraryMaintenanceCommand;
            }
        }

        private void ProductLibraryMaintenance_Executed(Object arg)
        {
            //Try to activate an existing window:
            ProductLibraryMaintenanceOrganiser win =
                CommonHelper.GetWindowService().ActivateWindow<ProductLibraryMaintenanceOrganiser>();
            if (win != null) return;

            //No existing window, so create a new one.
            ProductLibraryMaintenanceViewModel vm = new ProductLibraryMaintenanceViewModel(App.ViewState.ActivePlanogramView);


            String libraryName = arg as String;
            if (libraryName != null && System.IO.File.Exists(libraryName)
                && ProductLibraryHelper.GetValidDataSourceExtensions().Contains(System.IO.Path.GetExtension(libraryName)))
            {
                //load a new template using the library name as the sample
                vm.LoadSampleFile(MainViewModel.SelectedProductLibrary);
            }
            else
            {
                //open the template currently in use.
                Model.ProductLibrary currentLibrary = App.ViewState.ProductLibraryView.Model;
                if (currentLibrary != null)
                {
                    if (currentLibrary.IsFile)
                    {
                        vm.OpenFromFile(currentLibrary.Id as String);
                    }
                    else if (App.ViewState.IsConnectedToRepository)
                    {
                        vm.OpenFromRepository((Int32)currentLibrary.Id);
                    }

                    //also load the sample data
                    vm.LoadSampleFile(MainViewModel.SelectedProductLibrary);
                }
            }

            //show the window:
            CommonHelper.GetWindowService().ShowWindow<ProductLibraryMaintenanceOrganiser>(vm);

        }



        #endregion

        #region SelectProductLibrary

        /// <summary>
        /// Top level command to select product library 
        /// </summary>
        public static RelayCommand SelectProductLibrary
        {
            get { return Instance.SelectProductLibraryCommand; }
        }

        private RelayCommand _selectProductLibraryCommand;

        public RelayCommand SelectProductLibraryCommand
        {
            get
            {
                if (_selectProductLibraryCommand == null)
                {
                    _selectProductLibraryCommand = new RelayCommand(
                        p => SelectProductLibrary_Executed())
                    {
                        FriendlyName = Message.Ribbon_ProductLibraryContext_LoadProductLibrary,
                        FriendlyDescription = Message.Ribbon_ProductLibraryContext_LoadProductLibrary_Desc,
                        Icon = ImageResources.ProductLibraryContext_SelectProductLibrary_32,
                        SmallIcon = ImageResources.ProductLibraryContext_SelectProductLibrary_32
                    };
                    RegisterCommand(_selectProductLibraryCommand);
                }
                return _selectProductLibraryCommand;
            }
        }

        private void SelectProductLibrary_Executed()
        {
            //If no database
            if (!App.ViewState.IsConnectedToRepository)
            {
                //Select from file
                this.SelectProductLibraryFromFileCommand.Execute();
            }
            else
            {
                //Otherwise default to database selection
                this.SelectProductLibraryFromDatabaseCommand.Execute();
            }
        }

        #endregion

        #region SelectProductLibraryFromFile

        /// <summary>
        /// Launches windows explorer to select product library from file
        /// </summary>
        public static RelayCommand SelectProductLibraryFromFile
        {
            get { return Instance.SelectProductLibraryFromFileCommand; }
        }

        private RelayCommand _selectProductLibraryFromFileCommand;

        public RelayCommand SelectProductLibraryFromFileCommand
        {
            get
            {
                if (_selectProductLibraryFromFileCommand == null)
                {
                    _selectProductLibraryFromFileCommand = new RelayCommand(
                        p => SelectProductLibraryFromFile_Executed(),
                        p => SelectProductLibraryFromFile_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ProductLibraryContext_SelectProductLibraryFromFile,
                        FriendlyDescription = Message.Ribbon_ProductLibraryContext_SelectProductLibraryFromFile_Desc,
                        Icon = ImageResources.ProductLibraryContext_SelectProductLibraryFromFile_32,
                        SmallIcon = ImageResources.ProductLibraryContext_SelectProductLibraryFromFile_32
                    };
                    RegisterCommand(_selectProductLibraryFromFileCommand);
                }
                return _selectProductLibraryFromFileCommand;
            }
        }

        private Boolean SelectProductLibraryFromFile_CanExecute()
        {
            if (App.ViewState.IsConnectedToRepository)
            {
                //Must have get and add permissions
                this.SelectProductLibraryFromFileCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return ApplicationContext.User.IsInRole(DomainPermission.ProductGet.ToString()) && ApplicationContext.User.IsInRole(DomainPermission.ProductCreate.ToString());
            }

            return true;
        }

        private void SelectProductLibraryFromFile_Executed()
        {
            //Show file dialog
            String fileName;

            Boolean dialogResult =
                CommonHelper.GetWindowService().ShowOpenFileDialog(
                App.ViewState.GetSessionDirectory(SessionDirectory.ProductUniverse),
                Message.ImportDefinition_ImportFileTypeFilter_Excel2007 + "|"
                + Message.ImportDefinition_ImportFileTypeFilter_Excel2003 + "|"
                + Message.ImportDefinition_ImportFileTypeFilter_Text + "|"
                + Message.ImportDefinition_ImportFileTypeFilter_Csv,
                out fileName);

            if (!dialogResult) return;

            //update the selected file to the file path if returned a value

            List<String> extensions = new List<String>(new String[] { ".xlsx", ".xls", ".txt", ".csv" });
            if (!extensions.Any(s => String.Equals(Path.GetExtension(fileName), s, StringComparison.OrdinalIgnoreCase)))
            {
                CommonHelper.GetWindowService()
                    .ShowOkMessage(MessageWindowType.Warning,
                    Message.General_WrongFileType_Title,
                    Message.General_WrongFileType_Description);
            }
            else
            {
                //update the session directory
                App.ViewState.SetSessionDirectory(SessionDirectory.ProductUniverse, Path.GetDirectoryName(fileName));

                //Load excel file
                MainViewModel.SelectedProductLibrary = fileName;
            }

        }

        #endregion

        #region SelectProductLibraryFromDatabase

        /// <summary>
        /// Launches windows explorer to select product library from file
        /// </summary>
        public static RelayCommand SelectProductLibraryFromDatabase
        {
            get { return Instance.SelectProductLibraryFromDatabaseCommand; }
        }

        private RelayCommand _selectProductLibraryFromDatabaseCommand;

        public RelayCommand SelectProductLibraryFromDatabaseCommand
        {
            get
            {
                if (_selectProductLibraryFromDatabaseCommand == null)
                {
                    _selectProductLibraryFromDatabaseCommand = new RelayCommand(
                        p => SelectProductLibraryFromDatabase_Executed(),
                        p => SelectProductLibraryFromDatabase_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ProductLibraryContext_SelectProductLibraryFromDatabase,
                        FriendlyDescription = Message.Ribbon_ProductLibraryContext_SelectProductLibraryFromDatabase_Desc,
                        Icon = ImageResources.ProductLibraryContext_SelectProductLibraryFromDatabase_32,
                        SmallIcon = ImageResources.ProductLibraryContext_SelectProductLibraryFromDatabase_32
                    };
                    RegisterCommand(_selectProductLibraryFromDatabaseCommand);
                }
                return _selectProductLibraryFromDatabaseCommand;
            }
        }

        private Boolean SelectProductLibraryFromDatabase_CanExecute()
        {
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.SelectProductLibraryFromDatabaseCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }

            if (App.ViewState.IsConnectedToRepository)
            {
                //Must have get and add permissions
                this.SelectProductLibraryFromDatabaseCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return ApplicationContext.User.IsInRole(DomainPermission.ProductGet.ToString()) && ApplicationContext.User.IsInRole(DomainPermission.ProductCreate.ToString());
            }

            return true;
        }

        private void SelectProductLibraryFromDatabase_Executed()
        {
            if (App.ViewState.IsConnectedToRepository)
            {
                //Show product universe selector
                ProductUniverseSelector win = new ProductUniverseSelector();
                WindowHelper.ShowWindow(win, true);

                //If dialog returned true
                if (win.SelectedProductUniverse != null)
                {
                    //Load product universe products
                    MainViewModel.SelectedProductUniverseInfo = win.SelectedProductUniverse;
                }
            }
        }

        #endregion

        #region SelectProductLibraryRibbonTab

        /// <summary>
        /// Selected the product library contextual tab
        /// </summary>
        public static RelayCommand SelectProductLibraryRibbonTab
        {
            get { return Instance.SelectProductLibraryRibbonTabComand; }
        }

        private RelayCommand _selectProductLibraryRibbonTabCommand;

        public RelayCommand SelectProductLibraryRibbonTabComand
        {
            get
            {
                if (_selectProductLibraryRibbonTabCommand == null)
                {
                    _selectProductLibraryRibbonTabCommand = new RelayCommand(
                        p => SelectProductLibraryRibbonTab_Executed())
                    {
                        FriendlyName = Message.Ribbon_ProductLibraryContext_SelectProductLibraryTab,
                        Icon = ImageResources.ProductLibraryContext_SelectProductLibraryFromDatabase_32,
                        SmallIcon = ImageResources.ProductLibraryContext_ProductLibraryMaintenance_32
                    };
                    RegisterCommand(_selectProductLibraryRibbonTabCommand);
                }
                return _selectProductLibraryRibbonTabCommand;
            }
        }

        private void SelectProductLibraryRibbonTab_Executed()
        {
            //check UI is attached
            if (MainViewModel != null && MainViewModel.AttachedControl != null)
            {
                //check tab is not already selected
                if (MainViewModel.AttachedControl.productLibraryContextTab.IsVisible
                        && !MainViewModel.AttachedControl.productLibraryContextTab.IsSelected)
                {
                    //select tab
                    MainViewModel.AttachedControl.ribbon.SelectedTabItem = MainViewModel.AttachedControl.productLibraryContextTab;
                }
            }
        }

        #endregion

        #region UpdatePlanogramProducts

        public static RelayCommand UpdatePlanogramProducts
        {
            get { return Instance.UpdatePlanogramProductsCommand; }
        }

        private RelayCommand _updatePlanogramProductsCommand;

        public RelayCommand UpdatePlanogramProductsCommand
        {
            get
            {
                if (_updatePlanogramProductsCommand == null)
                {
                    _updatePlanogramProductsCommand = new RelayCommand(
                        p => UpdatePlanogramProducts_Executed(),
                        p => UpdatePlanogramProducts_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ProductLibraryContext_UpdatePlanogram,
                        FriendlyDescription = Message.Ribbon_ProductLibraryContext_UpdatePlanogram_Description,
                        Icon = ImageResources.ProductLibraryContext_UpdatePlanogramFromUniverse_32,
                        SmallIcon = ImageResources.ProductLibraryContext_UpdatePlanogramFromUniverse_32
                    };
                    RegisterCommand(_updatePlanogramProductsCommand);
                }
                return _updatePlanogramProductsCommand;
            }
        }

        private Boolean UpdatePlanogramProducts_CanExecute()
        {
            if (App.ViewState == null ||
                App.ViewState.ProductLibraryView == null ||
                App.ViewState.ProductLibraryView.Products.Count == 0)
            {
                this.UpdatePlanogramProductsCommand.DisabledReason = Message.Ribbon_ProductLibraryContext_UpdatePlanogram_DisabledReason_NoProductUniverse;
                return false;
            }

            if (App.MainPageViewModel.ActivePlanController == null)
            {
                this.UpdatePlanogramProductsCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void UpdatePlanogramProducts_Executed()
        {
            if (App.MainPageViewModel.ActivePlanController == null ||
                App.MainPageViewModel.ActivePlanController.SourcePlanogram == null ||
                App.ViewState.ProductLibraryView == null) return;

            UpdatePlanogramProducts_Executed(App.ViewState.ProductLibraryView, App.MainPageViewModel.ActivePlanController.SourcePlanogram);
        }

        /// <summary>
        /// Updates the Planogram Products with the selected attributes.
        /// </summary>
        /// <param name="productLibraryViewModel">The product library view from which to update products.</param>
        /// <param name="planogramView">The planogram view whose products should be updated.</param>
        internal static void UpdatePlanogramProducts_Executed(ProductLibraryViewModel productLibraryViewModel, PlanogramView planogramView)
        {
            // Identify which attributes are available for selection. To do this, we need to evaluate
            // the columns that are mapped in the product library and get their source types and property names.
            List<Tuple<ProductAttributeSourceType, String>> includeProperties = null;
            if (productLibraryViewModel.Model != null)
            {
                includeProperties = new List<Tuple<ProductAttributeSourceType, String>>();
                ProductLibraryImportMappingList importMappingList = ProductLibraryImportMappingList.NewProductLibraryImportMappingList();

                foreach (ProductLibraryColumnMapping mappedProperty in productLibraryViewModel.Model.ColumnMappings.Where(m => m.IsMapped))
                {
                    // Get the binding path of the property from the Product Library Import mapping.
                    // This should either be in the format "PropertyName", or "CustomAttributes.PropertyName" 
                    // or "PerformanceData.PropertyName".
                    ImportMapping importMapping = importMappingList.FirstOrDefault(i => i.PropertyName.Equals(mappedProperty.Destination));
                    if (importMapping == null) continue;
                    String propertyName = importMappingList.GetBindingPath(importMapping.PropertyIdentifier);

                    // Check the binding path for a prefix.
                    ProductAttributeSourceType sourceType = ProductAttributeSourceType.ProductAttributes;
                    if (propertyName.Contains(Product.CustomAttributesProperty.Name))
                    {
                        sourceType = ProductAttributeSourceType.ProductCustomAttributes;
                    }
                    else if (propertyName.Contains(PlanogramPerformance.PerformanceDataProperty.Name))
                    {
                        sourceType = ProductAttributeSourceType.ProductPerformanceData;
                    }

                    includeProperties.Add(new Tuple<ProductAttributeSourceType, String>(sourceType, propertyName));
                }
            }


            //If we are using a DB we don't allow the user to select the performace attributes else from file we do.
            Boolean shouldDisplayPerformanceAttributes = (MainViewModel.ProductLibraryPanelViewModel.CurrentProductUniverse == null);

            // Prompt the user for the attributes to update.
            ProductAttributeSelectorViewModel viewModel = new ProductAttributeSelectorViewModel(
                includeCustomFields: true,
                includePerformanceFields: shouldDisplayPerformanceAttributes,
                includeProperties: includeProperties);
            CommonHelper.GetWindowService().ShowDialog<ProductAttributeSelectorWindow>(viewModel);

            // Get the selected attributes and update the plan.
            if (viewModel.DialogResult == true)
            {
                Mouse.OverrideCursor = Cursors.Wait;
                Int32 updatedProducts =
                    planogramView.UpdateProducts(productLibraryViewModel.Products, viewModel.SelectedAttributes);
                System.Windows.Threading.Dispatcher.CurrentDispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));

                if (updatedProducts != 0)
                {
                    CommonHelper.GetWindowService().ShowMessage(
                        Ccm.Common.Wpf.Services.MessageWindowType.Info,
                        Message.Ribbon_ProductLibraryContext_UpdatePlanogram_UpdateSuccessful,
                        String.Format(Message.Ribbon_ProductLibraryContext_UpdatePlanogram_UpdateSuccessful_Description, updatedProducts),
                        1, // One button.
                        Message.Generic_OK,
                        null, // No content for buttons 2 and 3.
                        null);
                }
                else
                {
                    CommonHelper.GetWindowService().ShowMessage(
                        Ccm.Common.Wpf.Services.MessageWindowType.Info,
                        Message.Ribbon_ProductLibraryContext_UpdatePlanogram_NoProductsToUpdate,
                        Message.Ribbon_ProductLibraryContext_UpdatePlanogram_NoProductsToUpdate_Description,
                        1, // One button.
                        Message.Generic_OK,
                        null, // No content for buttons 2 and 3.
                        null);
                }
            }
        }

        #endregion

        #region UpdateAllPlanogramsProducts

        public static RelayCommand UpdateAllPlanogramsProducts
        {
            get { return Instance.UpdateAllPlanogramsProductsCommand; }
        }

        private RelayCommand _updateAllPlanogramsProductsCommand;

        public RelayCommand UpdateAllPlanogramsProductsCommand
        {
            get
            {
                if (_updateAllPlanogramsProductsCommand == null)
                {
                    _updateAllPlanogramsProductsCommand = new RelayCommand(
                        p => UpdateAllPlanogramsProducts_Executed(),
                        p => UpdateAllPlanogramsProducts_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_ProductLibraryContext_UpdateAllPlanograms,
                        FriendlyDescription = Message.Ribbon_ProductLibraryContext_UpdateAllPlanograms_Description,
                        Icon = ImageResources.ProductLibraryContext_UpdateAllPlanogramsFromUniverse_32,
                        SmallIcon = ImageResources.ProductLibraryContext_UpdateAllPlanogramsFromUniverse_32
                    };
                    RegisterCommand(_updateAllPlanogramsProductsCommand);
                }
                return _updateAllPlanogramsProductsCommand;
            }
        }

        private Boolean UpdateAllPlanogramsProducts_CanExecute()
        {
            if (App.ViewState == null ||
                App.ViewState.ProductLibraryView == null ||
                App.ViewState.ProductLibraryView.Products.Count == 0)
            {
                this.UpdatePlanogramProductsCommand.DisabledReason = Message.Ribbon_ProductLibraryContext_UpdatePlanogram_DisabledReason_NoProductUniverse;
                return false;
            }

            if (!App.MainPageViewModel.PlanControllers.Any())
            {
                this.UpdatePlanogramProductsCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void UpdateAllPlanogramsProducts_Executed()
        {
            if (!App.MainPageViewModel.PlanControllers.Any() ||
                App.ViewState.ProductLibraryView == null) return;

            UpdateAllPlanogramsProducts_Executed(App.ViewState.ProductLibraryView, App.MainPageViewModel.PlanControllers.Select(p => p.SourcePlanogram));
        }

        /// <summary>
        /// Updates the Planogram Products with the selected attributes.
        /// </summary>
        /// <param name="productLibraryViewModel">The product library view from which to update products.</param>
        /// <param name="planogramView">The planogram view whose products should be updated.</param>
        internal static void UpdateAllPlanogramsProducts_Executed(ProductLibraryViewModel productLibraryViewModel, IEnumerable<PlanogramView> planogramViews)
        {
            // Identify which attributes are available for selection. To do this, we need to evaluate
            // the columns that are mapped in the product library and get their source types and property names.
            List<Tuple<ProductAttributeSourceType, String>> includeProperties = null;
            if (productLibraryViewModel.Model != null)
            {
                includeProperties = new List<Tuple<ProductAttributeSourceType, String>>();
                ProductLibraryImportMappingList importMappingList = ProductLibraryImportMappingList.NewProductLibraryImportMappingList();

                foreach (ProductLibraryColumnMapping mappedProperty in productLibraryViewModel.Model.ColumnMappings.Where(m => m.IsMapped))
                {
                    // Get the binding path of the property from the Product Library Import mapping.
                    // This should either be in the format "PropertyName", or "CustomAttributes.PropertyName" 
                    // or "PerformanceData.PropertyName".
                    ImportMapping importMapping = importMappingList.FirstOrDefault(i => i.PropertyName.Equals(mappedProperty.Destination));
                    if (importMapping == null) continue;
                    String propertyName = importMappingList.GetBindingPath(importMapping.PropertyIdentifier);

                    // Check the binding path for a prefix.
                    ProductAttributeSourceType sourceType = ProductAttributeSourceType.ProductAttributes;
                    if (propertyName.Contains(Product.CustomAttributesProperty.Name))
                    {
                        sourceType = ProductAttributeSourceType.ProductCustomAttributes;
                    }
                    else if (propertyName.Contains(PlanogramPerformance.PerformanceDataProperty.Name))
                    {
                        sourceType = ProductAttributeSourceType.ProductPerformanceData;
                    }

                    includeProperties.Add(new Tuple<ProductAttributeSourceType, String>(sourceType, propertyName));
                }
            }


            //If we are using a DB we don't allow the user to select the performace attributes else from file we do.
            Boolean shouldDisplayPerformanceAttributes = (MainViewModel.ProductLibraryPanelViewModel.CurrentProductUniverse == null);

            // Prompt the user for the attributes to update.
            ProductAttributeSelectorViewModel viewModel =
                new ProductAttributeSelectorViewModel(includeCustomFields: true, includePerformanceFields: shouldDisplayPerformanceAttributes, includeProperties: includeProperties);
            CommonHelper.GetWindowService().ShowDialog<ProductAttributeSelectorWindow>(viewModel);

            // Get the selected attributes and update the plan.
            if (viewModel.DialogResult == true)
            {
                Int32 updatedProducts = 0;
                foreach (PlanogramView planogramView in planogramViews)
                {
                    updatedProducts +=
                        planogramView.UpdateProducts(productLibraryViewModel.Products, viewModel.SelectedAttributes);
                }

                if (updatedProducts != 0)
                {
                    CommonHelper.GetWindowService().ShowMessage(
                        Ccm.Common.Wpf.Services.MessageWindowType.Info,
                        Message.Ribbon_ProductLibraryContext_UpdatePlanogram_UpdateSuccessful,
                        String.Format(Message.Ribbon_ProductLibraryContext_UpdatePlanogram_UpdateSuccessful_Description, updatedProducts),
                        1, // One button.
                        Message.Generic_OK,
                        null, // No content for buttons 2 and 3.
                        null);
                }
                else
                {
                    CommonHelper.GetWindowService().ShowMessage(
                        Ccm.Common.Wpf.Services.MessageWindowType.Info,
                        Message.Ribbon_ProductLibraryContext_UpdatePlanogram_NoProductsToUpdate,
                        Message.Ribbon_ProductLibraryContext_UpdatePlanogram_NoProductsToUpdate_Description,
                        1, // One button.
                        Message.Generic_OK,
                        null, // No content for buttons 2 and 3.
                        null);
                }
            }
        }

        #endregion

        #region ManageRepositories

        /// <summary>
        ///     Opens the <see cref="DbConnectionWizardWindow"/> to manage repository connections.
        /// </summary>
        public static RelayCommand ManageRepositories
        {
            get
            {
                return Instance.ManageRepositoriesCommand;
            }
        }

        /// <summary>
        ///     Reference to the current instance for <see cref="ManageRepositoriesCommand"/>.
        /// </summary>
        private RelayCommand _manageRepositoriesCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="ManageRepositoriesCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        private RelayCommand ManageRepositoriesCommand
        {
            get
            {
                if (_manageRepositoriesCommand == null)
                {
                    RegisterCommand(_manageRepositoriesCommand = new RelayCommand(o => ManageRepositories_Executed())
                    {
                        FriendlyName = Message.Backstage_RepositoriesLabel,
                        Icon = ImageResources.BackstageSetup_Repositories,
                        SmallIcon = ImageResources.BackstageSetup_Repositories
                    });
                }
                return _manageRepositoriesCommand;
            }
        }

        /// <summary>
        ///     Code to run when <see cref="ManageRepositoriesCommand"/> is executed.
        /// </summary>
        private void ManageRepositories_Executed()
        {
            WindowHelper.ShowWindow(new DbConnectionWizardWindow(), true);
        }

        #endregion

        #region ShowValidationTemplate

        /// <summary>
        ///     Opens the <see cref="ValidationTemplateEditorWindow" /> to manage the planogram's validation template.
        /// </summary>
        public static RelayCommand ShowValidationTemplate
        {
            get { return Instance.ShowValidationTemplateCommand; }
        }

        /// <summary>
        ///     Reference to the current instance for <see cref="ShowValidationTemplateCommand" />.
        /// </summary>
        private RelayCommand _showValidationTemplateCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="ShowValidationTemplateCommand" />.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand ShowValidationTemplateCommand
        {
            get
            {
                if (_showValidationTemplateCommand != null) return _showValidationTemplateCommand;

                _showValidationTemplateCommand = new RelayCommand(o => ShowValidationTemplate_Executed(),
                    o => ShowValidationTemplate_CanExecute())
                {
                    FriendlyName = Message.Ribbon_ValidationTemplate,
                    FriendlyDescription = Message.Ribbon_ValidationTemplate_Description,
                    Icon = ImageResources.ValidationTemplateMaintenance_32,
                    SmallIcon = ImageResources.ValidationTemplateMaintenance_32
                };
                RegisterCommand(_showValidationTemplateCommand);
                return _showValidationTemplateCommand;
            }
        }

        /// <summary>
        ///     Invoked whenever the UI needs to assess the availability of the <see cref="ShowValidationTemplateCommand" />.
        /// </summary>
        /// <remarks>The needs to be an active planogram to show its validation template.</remarks>
        private Boolean ShowValidationTemplate_CanExecute()
        {
            if (ActivePlanController == null)
                ShowValidationTemplateCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
            else
                return true;

            return false;
        }

        /// <summary>
        ///     Invoked whenever the <see cref="ShowValidationTemplateCommand" /> is executed.
        /// </summary>
        private void ShowValidationTemplate_Executed()
        {
            new ValidationTemplateEditorViewModel(ActivePlanController.SourcePlanogram).ShowDialog();
        }

        #endregion

        #region ShowEventLog

        /// <summary>
        ///     Opens the <see cref="EventLogWindow" /> to manage the planogram's Event Logs.
        /// </summary>
        public static RelayCommand ShowEventLog
        {
            get { return Instance.ShowEventLogCommand; }
        }

        /// <summary>
        ///     Reference to the current instance for <see cref="ShowEventLogCommand" />.
        /// </summary>
        private RelayCommand _showEventLogCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="ShowEventLogCommand" />.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand ShowEventLogCommand
        {
            get
            {
                if (_showEventLogCommand != null) return _showEventLogCommand;

                _showEventLogCommand = new RelayCommand(o => ShowEventLog_Executed(),
                    o => ShowEventLog_CanExecute())
                {
                    FriendlyName = Message.Ribbon_EventLog,
                    FriendlyDescription = Message.Ribbon_EventLog_Description,
                    Icon = ImageResources.EventLog_32,
                    SmallIcon = ImageResources.EventLog_16
                };
                RegisterCommand(_showEventLogCommand);
                return _showEventLogCommand;
            }
        }

        /// <summary>
        ///     Invoked whenever the UI needs to assess the availability of the <see cref="ShowEventLogCommand" />.
        /// </summary>
        /// <remarks>Thre needs to be an active planogram to show its Event Logs.</remarks>
        private Boolean ShowEventLog_CanExecute()
        {
            if (ActivePlanController == null)
                ShowEventLogCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
            else
                return true;

            return false;
        }

        /// <summary>
        ///     Invoked whenever the <see cref="ShowEventLogCommand" /> is executed.
        /// </summary>
        private void ShowEventLog_Executed()
        {
            new EventLogViewModel(ActivePlanController.SourcePlanogram).ShowDialog();
        }

        #endregion

        #region Remove Images

        /// <summary>
        ///     Command to remove images from the current planogram.
        /// </summary>
        public static RelayCommand RemoveImagesFromPlanogram
        {
            get { return Instance.RemoveImagesFromPlanogramCommand; }
        }

        private RelayCommand _removeImagesFromPlanogramCommand;

        /// <summary>
        /// Clears all images from the planogram model.
        /// </summary>
        private RelayCommand RemoveImagesFromPlanogramCommand
        {
            get
            {
                if (_removeImagesFromPlanogramCommand != null) return _removeImagesFromPlanogramCommand;

                _removeImagesFromPlanogramCommand = new RelayCommand(
                    o => RemoveImagesFromPlanogram_Executed(),
                    o => RemoveImagesFromPlanogram_CanExecute())
                {
                    FriendlyName = Message.Ribbon_RemoveImages,
                    FriendlyDescription = Message.Ribbon_RemoveImages_Description,
                    Icon = ImageResources.Ribbon_RemoveImages_32,
                    SmallIcon = ImageResources.Ribbon_RemoveImages_16
                };
                RegisterCommand(_removeImagesFromPlanogramCommand);
                return _removeImagesFromPlanogramCommand;
            }
        }

        private Boolean RemoveImagesFromPlanogram_CanExecute()
        {
            // There needs to be an active Planogram.
            if (ActivePlanController == null ||
                ActivePlanController.SourcePlanogram == null)
            {
                this.RemoveImagesFromPlanogramCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                this.RemoveProductImagesFromPlanogramCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                this.RemoveComponentImagesFromPlanogramCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void RemoveImagesFromPlanogram_Executed()
        {
            //check if there are any images to be removed.
            PlanogramView planogram = ActivePlanController.SourcePlanogram;
            if (planogram.Images == null || planogram.Images.Count == 0)
            {
                CommonHelper.GetWindowService().ShowOkMessage(MessageWindowType.Info, null,
                    Message.DisabledReason_NoImagesInPlanogram);

                return;
            }

            ActivePlanController.RemoveImagesFromPlanogram(true, true);
        }

        #endregion

        #region Remove Product Images

        /// <summary>
        ///     Command to remove product images from the current planogram.
        /// </summary>
        public static RelayCommand RemoveProductImagesFromPlanogram
        {
            get { return Instance.RemoveProductImagesFromPlanogramCommand; }
        }

        private RelayCommand _removeProductImagesFromPlanogramCommand;

        /// <summary>
        ///     Command to remove product images from the current planogram.
        /// </summary>
        private RelayCommand RemoveProductImagesFromPlanogramCommand
        {
            get
            {
                if (_removeProductImagesFromPlanogramCommand != null) return _removeProductImagesFromPlanogramCommand;

                _removeProductImagesFromPlanogramCommand = new RelayCommand(
                    o => RemoveProductImagesFromPlanogram_Executed(),
                    o => RemoveProductImagesFromPlanogram_CanExecute())
                {
                    FriendlyName = Message.Ribbon_RemoveProductImages,
                    FriendlyDescription = Message.Ribbon_RemoveProductImages_Description,
                    Icon = ImageResources.Ribbon_RemoveImages_32,
                    SmallIcon = ImageResources.Ribbon_RemoveImages_16
                };
                RegisterCommand(_removeProductImagesFromPlanogramCommand);
                return _removeProductImagesFromPlanogramCommand;
            }
        }

        private Boolean RemoveProductImagesFromPlanogram_CanExecute()
        {
            // Images in general need to be allowed to be removed.
            if (!RemoveImagesFromPlanogram_CanExecute()) return false;

            PlanogramView planogram = ActivePlanController.SourcePlanogram;

            // There need to be products with images in the planogram.
            if (planogram.Products.All(o => !o.Model.EnumerateImageIds().Any()))
            {
                this.RemoveProductImagesFromPlanogramCommand.DisabledReason = Message.DisabledReason_NoProductImagesInPlanogram;
                return false;
            }

            this.RemoveProductImagesFromPlanogramCommand.DisabledReason = String.Empty;
            return true;
        }

        private void RemoveProductImagesFromPlanogram_Executed()
        {
            //check if there are any images to be removed.
            PlanogramView planogram = ActivePlanController.SourcePlanogram;
            if (planogram.Images == null || planogram.Images.Count == 0)
            {
                CommonHelper.GetWindowService().ShowOkMessage(MessageWindowType.Info, null,
                    Message.DisabledReason_NoImagesInPlanogram);

                return;
            }

            ActivePlanController.RemoveImagesFromPlanogram(true, false);
        }

        #endregion

        #region Remove Component Images

        /// <summary>
        ///     Command to remove component images from the current planogram.
        /// </summary>
        public static RelayCommand RemoveComponentImagesFromPlanogram
        {
            get { return Instance.RemoveComponentImagesFromPlanogramCommand; }
        }

        private RelayCommand _removeComponentImagesFromPlanogramCommand;

        /// <summary>
        ///     Command to remove component images from the current planogram.
        /// </summary>
        private RelayCommand RemoveComponentImagesFromPlanogramCommand
        {
            get
            {
                if (_removeComponentImagesFromPlanogramCommand != null) return _removeComponentImagesFromPlanogramCommand;

                _removeComponentImagesFromPlanogramCommand = new RelayCommand(
                    o => RemoveComponentImagesFromPlanogram_Executed(),
                    o => RemoveComponentImagesFromPlanogram_CanExecute())
                {
                    FriendlyName = Message.Ribbon_RemoveComponentImages,
                    FriendlyDescription = Message.Ribbon_RemoveComponentImages_Description,
                    Icon = ImageResources.Ribbon_RemoveImages_32,
                    SmallIcon = ImageResources.Ribbon_RemoveImages_16
                };
                RegisterCommand(_removeComponentImagesFromPlanogramCommand);
                return _removeComponentImagesFromPlanogramCommand;
            }
        }

        private Boolean RemoveComponentImagesFromPlanogram_CanExecute()
        {
            // Images in general need to be allowed to be removed.
            if (!RemoveImagesFromPlanogram_CanExecute()) return false;

            PlanogramView planogram = ActivePlanController.SourcePlanogram;

            // There need to be products with images in the planogram.
            if (planogram.EnumerateAllSubComponents().All(o => !o.Model.EnumerateImageIds().Any()))
            {
                this.RemoveComponentImagesFromPlanogramCommand.DisabledReason = Message.DisabledReason_NoComponentImagesInPlanogram;
                return false;
            }

            this.RemoveComponentImagesFromPlanogramCommand.DisabledReason = String.Empty;
            return true;
        }

        private void RemoveComponentImagesFromPlanogram_Executed()
        {
            //check if there are any images to be removed.
            PlanogramView planogram = ActivePlanController.SourcePlanogram;
            if (planogram.Images == null || planogram.Images.Count == 0)
            {
                CommonHelper.GetWindowService().ShowOkMessage(MessageWindowType.Info, null,
                    Message.DisabledReason_NoImagesInPlanogram);

                return;
            }

            ActivePlanController.RemoveImagesFromPlanogram(false, true);
        }

        #endregion

        #region Add Images

        /// <summary>
        ///     Command to add images to the current planogram.
        /// </summary>
        public static RelayCommand AddImagesToPlanogram
        {
            get { return Instance.AddImagesToPlanogramCommand; }
        }

        private RelayCommand _addImagesToPlanogramCommand;

        private RelayCommand AddImagesToPlanogramCommand
        {
            get
            {
                if (_addImagesToPlanogramCommand != null) return _addImagesToPlanogramCommand;

                _addImagesToPlanogramCommand = new RelayCommand(
                    o => AddImagesToPlanogram_Executed(),
                    o => AddImagesToPlanogram_CanExecute())
                {
                    FriendlyName = Message.Ribbon_AddImages,
                    FriendlyDescription = Message.Ribbon_AddImagesDescription,
                    Icon = ImageResources.Ribbon_AddImages_32,
                    SmallIcon = ImageResources.Ribbon_AddImages_16
                };
                RegisterCommand(_addImagesToPlanogramCommand);
                return _addImagesToPlanogramCommand;
            }
        }


        private Boolean AddImagesToPlanogram_CanExecute()
        {
            // There needs to be an active Planogram.
            if (ActivePlanController == null ||
                ActivePlanController.SourcePlanogram == null)
            {
                AddImagesToPlanogramCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                AddProductImagesToPlanogramCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                AddComponentImagesToPlanogramCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            AddImagesToPlanogramCommand.DisabledReason = String.Empty;
            return true;
        }

        private void AddImagesToPlanogram_Executed()
        {
            AddProductImagesToPlanogramCommand.Execute();
            //DoAddImagesToPlanogram(addProducts: true, addComponents: true);
        }

        #endregion

        #region Add Product Images

        /// <summary>
        ///     Command to add product images to the current planogram.
        /// </summary>
        public static RelayCommand AddProductImagesToPlanogram
        {
            get { return Instance.AddProductImagesToPlanogramCommand; }
        }

        private RelayCommand _addProductImagesToPlanogramCommand;

        private RelayCommand AddProductImagesToPlanogramCommand
        {
            get
            {
                if (_addProductImagesToPlanogramCommand != null) return _addProductImagesToPlanogramCommand;

                _addProductImagesToPlanogramCommand = new RelayCommand(
                    o => AddProductImagesToPlanogram_Executed(),
                    o => AddProductImagesToPlanogram_CanExecute())
                {
                    FriendlyName = Message.Ribbon_AddProductImages,
                    FriendlyDescription = Message.Ribbon_AddProductImages_Description,
                    Icon = ImageResources.Ribbon_AddImages_32,
                    SmallIcon = ImageResources.Ribbon_AddImages_16
                };
                RegisterCommand(_addProductImagesToPlanogramCommand);
                return _addProductImagesToPlanogramCommand;
            }
        }

        private Boolean AddProductImagesToPlanogram_CanExecute()
        {
            // There needs to be an active Planogram.
            if (ActivePlanController == null ||
                ActivePlanController.SourcePlanogram == null)
            {
                AddProductImagesToPlanogramCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            AddProductImagesToPlanogramCommand.DisabledReason = String.Empty;
            return true;
        }

        private void AddProductImagesToPlanogram_Executed()
        {
            ActivePlanController.AddProductImagesToPlanogram();
        }

        #endregion

        #region Add Component Images

        /// <summary>
        ///     Command to add Component images to the current planogram.
        /// </summary>
        public static RelayCommand AddComponentImagesToPlanogram
        {
            get { return Instance.AddComponentImagesToPlanogramCommand; }
        }

        private RelayCommand _addComponentImagesToPlanogramCommand;

        private RelayCommand AddComponentImagesToPlanogramCommand
        {
            get
            {
                if (_addComponentImagesToPlanogramCommand != null) return _addComponentImagesToPlanogramCommand;

                _addComponentImagesToPlanogramCommand = new RelayCommand(
                    o => AddComponentImagesToPlanogram_Executed(),
                    o => AddComponentImagesToPlanogram_CanExecute())
                {
                    FriendlyName = Message.Ribbon_AddComponentImages,
                    FriendlyDescription = Message.Ribbon_AddComponentImages_Description,
                    Icon = ImageResources.Ribbon_AddImages_32,
                    SmallIcon = ImageResources.Ribbon_AddImages_16
                };
                RegisterCommand(_addComponentImagesToPlanogramCommand);
                return _addComponentImagesToPlanogramCommand;
            }
        }

        private Boolean AddComponentImagesToPlanogram_CanExecute()
        {
            // Images in general need to be allowed to be add.
            if (!AddImagesToPlanogram_CanExecute()) return false;

            PlanogramView planogram = ActivePlanController.SourcePlanogram;

            // There needs to be at least one product with images not stored.
            //if (!planogram.EnumerateAllSubComponents().Any(o => o.GetRealImages().Any()))
            //{
            //    this.AddComponentImagesToPlanogramCommand.DisabledReason =
            //        Message.DisabledReason_NoComponentsWithImagesToStore;
            //}

            this.AddComponentImagesToPlanogramCommand.DisabledReason = String.Empty;
            return true;
        }

        private void AddComponentImagesToPlanogram_Executed()
        {
            Debug.Fail("TODO");
            //DoAddImagesToPlanogram(addComponents: true);
        }

        #endregion

        #region SelectAllPositions

        /// <summary>
        /// Selects all positions on the active plan
        /// </summary>
        public static RelayCommand SelectAllPositions
        {
            get { return Instance.SelectAllPositionsCommand; }
        }

        private RelayCommand _selectAllPositionsCommand;

        /// <summary>
        /// Selects all positions on the active plan
        /// </summary>
        public RelayCommand SelectAllPositionsCommand
        {
            get
            {
                if (_selectAllPositionsCommand == null)
                {
                    _selectAllPositionsCommand =
                        new RelayCommand(
                            p => SelectAllPositions_Executed(),
                            p => SelectAllPositions_CanExecute())
                        {
                            FriendlyName = Message.Ribbon_SelectAllPositions,
                            FriendlyDescription = Message.Ribbon_SelectAllPositions_Desc,
                            SmallIcon = ImageResources.Ribbon_SelectAllPositions_16
                        };
                    RegisterCommand(_selectAllPositionsCommand);

                }
                return _selectAllPositionsCommand;
            }
        }

        private Boolean SelectAllPositions_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.SelectAllPositionsCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void SelectAllPositions_Executed()
        {
            var planController = ActivePlanController;

            planController.SelectedPlanItems.SetSelection(
                planController.SourcePlanogram.EnumerateAllPositions());
        }

        #endregion

        #region SelectSameProduct

        /// <summary>
        /// Selects all positions for the currently selected products on the plan.
        /// </summary>
        public static RelayCommand SelectSameProduct
        {
            get { return Instance.SelectSameProductCommand; }
        }

        private RelayCommand _selectSameProductCommand;

        /// <summary>
        /// Selects all positions on the active plan for the currently selected products
        /// </summary>
        public RelayCommand SelectSameProductCommand
        {
            get
            {
                if (_selectSameProductCommand == null)
                {
                    _selectSameProductCommand =
                        new RelayCommand(
                            p => SelectSameProduct_Executed(),
                            p => SelectSameProduct_CanExecute())
                        {
                            FriendlyName = Message.Ribbon_SelectSameProduct,
                            FriendlyDescription = Message.Ribbon_SelectSameProduct_Desc,
                            SmallIcon = ImageResources.Ribbon_SelectSameProduct_16
                        };
                    RegisterCommand(_selectSameProductCommand);

                }
                return _selectSameProductCommand;
            }
        }

        private Boolean SelectSameProduct_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.SelectSameProductCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }
            return true;
        }

        private void SelectSameProduct_Executed()
        {
            var planController = ActivePlanController;

            //get a list of products currently selected
            List<Object> productIds =
            planController.SelectedPlanItems.Where(p => p.Product != null).Select(p => p.Product.Model.Id)
            .ToList();

            //select all positions for these products
            planController.SelectedPlanItems.SetSelection(
                planController.SourcePlanogram.EnumerateAllPositions()
                .Where(p => productIds.Contains(p.Product.Model.Id)));

        }

        #endregion

        #region SelectAllComponents

        /// <summary>
        /// Selects all components on the active plan
        /// </summary>
        public static RelayCommand SelectAllComponents
        {
            get { return Instance.SelectAllComponentsCommand; }
        }

        private RelayCommand _selectAllComponentsCommand;

        /// <summary>
        /// Selects all components on the active plan
        /// </summary>
        public RelayCommand SelectAllComponentsCommand
        {
            get
            {
                if (_selectAllComponentsCommand == null)
                {
                    _selectAllComponentsCommand =
                        new RelayCommand(
                            p => SelectAllComponents_Executed(),
                            p => SelectAllComponents_CanExecute())
                        {
                            FriendlyName = Message.Ribbon_SelectAllComponents,
                            FriendlyDescription = Message.Ribbon_SelectAllComponents_Desc,
                            SmallIcon = ImageResources.Ribbon_SelectAllComponents_16
                        };
                    RegisterCommand(_selectAllComponentsCommand);

                }
                return _selectAllComponentsCommand;
            }
        }

        private Boolean SelectAllComponents_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.SelectAllComponentsCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void SelectAllComponents_Executed()
        {
            var planController = ActivePlanController;

            planController.SelectedPlanItems.SetSelection(
                planController.SourcePlanogram.EnumerateAllComponents()
                .Where(c => c.ComponentType != PlanogramComponentType.Base
                && c.ComponentType != PlanogramComponentType.Backboard));
        }

        #endregion

        #region SelectCombinedComponents

        /// <summary>
        /// Selects all components which are combined with the ones selected.
        /// </summary>
        public static RelayCommand SelectCombinedComponents
        {
            get { return Instance.SelectCombinedComponentsCommand; }
        }

        private RelayCommand _selectCombinedComponentsCommand;

        /// <summary>
        /// Selects all components which are combined with the ones selected.
        /// </summary>
        public RelayCommand SelectCombinedComponentsCommand
        {
            get
            {
                if (_selectCombinedComponentsCommand == null)
                {
                    _selectCombinedComponentsCommand =
                        new RelayCommand(
                            p => SelectCombinedComponents_Executed(),
                            p => SelectCombinedComponents_CanExecute())
                        {
                            FriendlyName = Message.Ribbon_SelectCombinedComponents,
                            FriendlyDescription = Message.Ribbon_SelectCombinedComponents_Desc,
                            SmallIcon = ImageResources.Ribbon_SelectCombinedComponents_16
                        };
                    RegisterCommand(_selectCombinedComponentsCommand);

                }
                return _selectCombinedComponentsCommand;
            }
        }

        private Boolean SelectCombinedComponents_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.SelectAllComponentsCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void SelectCombinedComponents_Executed()
        {
            var planController = ActivePlanController;

            //get a full list of components to be selected
            List<Object> componentIds = new List<Object>();
            foreach (IPlanItem planItem in planController.SelectedPlanItems)
            {
                if (planItem.Component == null) continue;
                if (componentIds.Contains(planItem.Component.ComponentModel.Id)) continue;

                foreach (PlanogramSubComponentView sub in planItem.Component.SubComponents)
                {
                    if (!sub.IsMerchandisable) continue;

                    componentIds.AddRange(
                        sub.ModelPlacement.GetCombinedWithList()
                        .Select(s => s.Component.Id).Distinct());
                }
            }

            if (!componentIds.Any()) return;


            //update the selection.
            planController.SelectedPlanItems.SetSelection(
                planController.SourcePlanogram.EnumerateAllComponents()
                .Where(c => componentIds.Contains(c.ComponentModel.Id)));

        }

        #endregion

        #region RemoveUnmerchandisedComponents

        /// <summary>
        /// Selects all positions on the active plan
        /// </summary>
        public static RelayCommand RemoveUnmerchandisedComponents
        {
            get { return Instance.RemoveUnmerchandisedComponentsCommand; }
        }

        private RelayCommand _removeUnmerchandisedComponentsCommand;

        /// <summary>
        /// Selects all positions on the active plan
        /// </summary>
        public RelayCommand RemoveUnmerchandisedComponentsCommand
        {
            get
            {
                if (_removeUnmerchandisedComponentsCommand == null)
                {
                    _removeUnmerchandisedComponentsCommand =
                        new RelayCommand(
                            p => RemoveUnmerchandisedComponents_Executed(),
                            p => RemoveUnmerchandisedComponents_CanExecute())
                        {
                            FriendlyName = Message.Ribbon_RemoveUnmerchandisedComponents,
                            FriendlyDescription = Message.Ribbon_RemoveUnmerchandisedComponents_Desc,
                            Icon = ImageResources.Ribbon_RemoveUnmerchandisedComponents_32
                        };
                    RegisterCommand(_removeUnmerchandisedComponentsCommand);

                }
                return _removeUnmerchandisedComponentsCommand;
            }
        }

        private Boolean RemoveUnmerchandisedComponents_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.RemoveUnmerchandisedComponentsCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void RemoveUnmerchandisedComponents_Executed()
        {
            var planController = ActivePlanController;

            //get all unmerchandised fixture components.
            List<PlanogramComponentView> unmerchandisedComponents =
                planController.SourcePlanogram.EnumerateAllFixtureComponents().Where(IsUnMerchandised).ToList();

            //now check assemblies - should only remove if all merchandisable assembly components are unmerchandised.
            List<PlanogramAssemblyView> assembliesToRemove = new List<PlanogramAssemblyView>();
            foreach (PlanogramAssemblyView assembly in planController.SourcePlanogram.EnumerateAllAssemblies())
            {
                if (assembly.Components
                    .Where(c => c.SubComponents.Any(s => s.IsMerchandisable))
                    .All(c => !c.SubComponents.Any(s => s.Positions.Any())))
                {
                    unmerchandisedComponents.AddRange(assembly.Components);
                    assembliesToRemove.Add(assembly);
                }
            }

            if (!unmerchandisedComponents.Any())
            {
                //no items so tell the user this.
                CommonHelper.GetWindowService().ShowOkMessage(
                    MessageWindowType.Info,
                    Message.Ribbon_RemoveUnmerchandisedComponents,
                    Message.Ribbon_RemoveUnmerchandisedComponents_None);
            }
            else
            {

                planController.SourcePlanogram.BeginUndoableAction();

                //remove the components & assemblies
                foreach (PlanogramComponentView component in unmerchandisedComponents)
                {
                    planController.SourcePlanogram.RemovePlanItem(component);
                }
                foreach (PlanogramAssemblyView assembly in assembliesToRemove)
                {
                    planController.SourcePlanogram.RemovePlanItem(assembly);
                }

                //if there are now bays without any components or assemblies ask the user if they want to remove them too.
                List<PlanogramFixtureView> baysToRemove =
                        planController.SourcePlanogram.Fixtures
                        .Where(f =>
                        !f.Components.Any(c => c.ComponentType != PlanogramComponentType.Backboard && c.ComponentType != PlanogramComponentType.Base)
                        && !f.Assemblies.Any())
                        .ToList();

                Int32 removedBaysCount = baysToRemove.Count;
                if (removedBaysCount > 0)
                {
                    //Ask user if they want to remove these too
                    ModalMessageResult result =
                    CommonHelper.GetWindowService().ShowMessage(
                        MessageWindowType.Question,
                        Message.Ribbon_RemoveUnmerchandisedComponents,
                        Message.Ribbon_RemoveUnmerchandisedComponents_RemoveBaysTooQuestion,
                        Message.Generic_Yes,
                        Message.Generic_No);

                    if (result == ModalMessageResult.Button1)
                    {
                        foreach (PlanogramFixtureView fixture in baysToRemove)
                        {
                            planController.SourcePlanogram.RemovePlanItem(fixture);
                        }
                    }
                    else
                    {
                        removedBaysCount = 0;
                    }
                }



                planController.SourcePlanogram.EndUndoableAction();

                //tell the user how many have gone.
                String msg = (removedBaysCount == 0) ?
                    String.Format(CultureInfo.CurrentCulture,
                        Message.Ribbon_RemoveUnmerchandisedComponents_Count, unmerchandisedComponents.Count)
                        : String.Format(CultureInfo.CurrentCulture,
                        Message.Ribbon_RemoveUnmerchandisedComponents_BayCount, unmerchandisedComponents.Count, removedBaysCount);

                CommonHelper.GetWindowService().ShowOkMessage(
                        MessageWindowType.Info,
                        Message.Ribbon_RemoveUnmerchandisedComponents,
                        msg);
            }
        }

        #endregion

        #region RemoveUnplacedProducts

        /// <summary>
        /// Selects all positions on the active plan
        /// </summary>
        public static RelayCommand RemoveUnplacedProducts
        {
            get { return Instance.RemoveUnplacedProductsCommand; }
        }

        private RelayCommand _removeUnplacedProductsCommand;

        /// <summary>
        /// Selects all positions on the active plan
        /// </summary>
        public RelayCommand RemoveUnplacedProductsCommand
        {
            get
            {
                if (_removeUnplacedProductsCommand == null)
                {
                    _removeUnplacedProductsCommand =
                        new RelayCommand(
                            p => RemoveUnplacedProducts_Executed(),
                            p => RemoveUnplacedProducts_CanExecute())
                        {
                            FriendlyName = Message.Ribbon_RemoveUnplacedProducts,
                            FriendlyDescription = Message.Ribbon_RemoveUnplacedProducts_Desc,
                            Icon = ImageResources.Ribbon_RemoveUnplacedProducts_32
                        };
                    RegisterCommand(_removeUnplacedProductsCommand);

                }
                return _removeUnplacedProductsCommand;
            }
        }

        private Boolean RemoveUnplacedProducts_CanExecute()
        {
            if (ActivePlanController == null)
            {
                this.RemoveUnplacedProductsCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            return true;
        }

        private void RemoveUnplacedProducts_Executed()
        {
            var planController = ActivePlanController;

            IEnumerable<Object> placedProductIds =
                planController.SourcePlanogram.EnumerateAllPositions()
                .Select(p => p.Model.PlanogramProductId);

            List<PlanogramProductView> unplacedProducts =
                planController.SourcePlanogram.Products
                .Where(p => !placedProductIds.Contains(p.Model.Id))
                .ToList();

            if (!unplacedProducts.Any())
            {
                //no products so tell the user this.
                CommonHelper.GetWindowService().ShowOkMessage(
                    MessageWindowType.Info,
                    Message.Ribbon_RemoveUnplacedProducts,
                    Message.Ribbon_RemoveUnplacedProducts_None);
            }
            else
            {
                //remove the products
                planController.SourcePlanogram.BeginUndoableAction();

                foreach (PlanogramProductView p in unplacedProducts)
                {
                    planController.SourcePlanogram.RemovePlanItem(p);
                }

                planController.SourcePlanogram.EndUndoableAction();

                //tell the user how many have gone.
                CommonHelper.GetWindowService().ShowOkMessage(
                        MessageWindowType.Info,
                        Message.Ribbon_RemoveUnplacedProducts,
                        String.Format(CultureInfo.CurrentCulture,
                        Message.Ribbon_RemoveUnplacedProducts_Count, unplacedProducts.Count));
            }
        }

        #endregion

        #region AppendPlanogram

        public static RelayCommand AppendPlanogram
        {
            get { return Instance.AppendPlanogramCommand; }
        }

        private RelayCommand _appendPlanogramCommand;

        /// <summary>
        /// Append a selected planogram to the current planogram
        /// </summary>
        public RelayCommand AppendPlanogramCommand
        {
            get
            {
                if (_appendPlanogramCommand == null)
                {
                    _appendPlanogramCommand =
                        new RelayCommand(
                            p => AppendPlanogramCommand_Executed(),
                            p => AppendPlanogramCommand_CanExecute())
                        {
                            FriendlyName = Message.Ribbon_AppendPlanogram,
                            FriendlyDescription = Message.Ribbon_AppendPlanogram_Desc,
                            Icon = ImageResources.PlanogramAppend_32,
                            SmallIcon = ImageResources.PlanogramAppend_32,
                        };

                    RegisterCommand(_appendPlanogramCommand);
                }

                return _appendPlanogramCommand;
            }
        }

        private Boolean AppendPlanogramCommand_CanExecute()
        {
            Boolean isEnabled = true;
            String disabledReason = null;

            if (ActivePlanController == null)
            {
                // Check if there is an active plan
                disabledReason = Message.DisabledReason_NoActivePlanogram;
                isEnabled = false;
            }
            else if (!App.ViewState.IsConnectedToRepository)
            {
                // Check if there is a repository connection
                disabledReason = Message.MainPage_NoActiveConnectionToRepository;
                isEnabled = false;
            }

            this.AppendPlanogramCommand.DisabledReason = disabledReason;
            return isEnabled;
        }

        private void AppendPlanogramCommand_Executed()
        {
            MainViewModel.AppendPlanogram();
        }

        #endregion

        #region AppendPlanogramFile

        public static RelayCommand AppendPlanogramFile
        {
            get { return Instance.AppendPlanogramFileCommand; }
        }

        private RelayCommand _appendPlanogramFileCommand;

        /// <summary>
        /// Append a selected planogram to the current planogram
        /// </summary>
        public RelayCommand AppendPlanogramFileCommand
        {
            get
            {
                if (_appendPlanogramFileCommand == null)
                {
                    _appendPlanogramFileCommand =
                        new RelayCommand(
                            p => AppendPlanogramFileCommand_Executed(),
                            p => AppendPlanogramFileCommand_CanExecute())
                        {
                            FriendlyName = Message.Ribbon_AppendPlanogramFile,
                            FriendlyDescription = Message.Ribbon_AppendPlanogramFile_Desc,
                            Icon = ImageResources.PlanogramAppend_32,
                            SmallIcon = ImageResources.PlanogramAppend_32,
                        };

                    RegisterCommand(_appendPlanogramFileCommand);
                }

                return _appendPlanogramFileCommand;
            }
        }

        private Boolean AppendPlanogramFileCommand_CanExecute()
        {
            Boolean isEnabled = true;
            String disabledReason = null;

            if (ActivePlanController == null)
            {
                // Check if there is an active plan
                disabledReason = Message.DisabledReason_NoActivePlanogram;
                isEnabled = false;
            }

            this.AppendPlanogramCommand.DisabledReason = disabledReason;
            return isEnabled;
        }

        private static void AppendPlanogramFileCommand_Executed()
        {
            MainViewModel.AppendPlanogramsFromFileSystem();
        }

        #endregion

        #region UpdatePlanogramFromProductAttributeCommand

        /// <summary>
        ///     The command to update the current <c>Planogram</c>'s <c>Product Attributes</c>.
        /// </summary>
        private RelayCommand _updatePlanogramFromProductAttributeCommand;

        /// <summary>
        ///     Static reference to the <see cref="Instance"/> <see cref="UpdatePlanogramFromProductAttributeCommand"/>.
        /// </summary>
        public static RelayCommand UpdatePlanogramFromProductAttribute { get { return Instance.UpdatePlanogramFromProductAttributeCommand; } }

        /// <summary>
        ///     Get the command to update the current <c>Planogram</c>'s <c>Product Attributes</c>.
        /// </summary>
        /// <remarks>This command is created lazily, when requested for the first time.</remarks>
        public RelayCommand UpdatePlanogramFromProductAttributeCommand
        {
            get
            {
                if (_updatePlanogramFromProductAttributeCommand != null) return _updatePlanogramFromProductAttributeCommand;

                _updatePlanogramFromProductAttributeCommand =
                    new RelayCommand(o => UpdatePlanogramFromProductAttributeCommand_Executed(),
                                     o => UpdatePlanogramFromProductAttributeCommand_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_DesignContext_UpdatePlanogramFromProductAttribute,
                        FriendlyDescription = Message.Ribbon_DesignContext_UpdatePlanogramFromProductAttribute_Description,
                        Icon = ImageResources.DesignContext_UpdatePlanogramFromProductAttribute_32,
                        SmallIcon = ImageResources.DesignContext_UpdatePlanogramFromProductAttribute_32
                    };
                RegisterCommand(_updatePlanogramFromProductAttributeCommand);
                return _updatePlanogramFromProductAttributeCommand;
            }
        }

        /// <summary>
        ///     Invoked when checking whether <see cref="UpdatePlanogramFromProductAttributeCommand"/> can be executed.
        /// </summary>
        /// <returns><c>True</c> if the command can be executed, <c>false</c> otherwise.</returns>
        private Boolean UpdatePlanogramFromProductAttributeCommand_CanExecute()
        {
            if (NoActivePlanogram)
            {
                UpdatePlanogramFromProductAttributeCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            UpdatePlanogramFromProductAttributeCommand.DisabledReason = String.Empty;
            return true;
        }

        /// <summary>
        ///     Invoked whenever the <see cref="UpdatePlanogramFromProductAttributeCommand"/> is executed.
        /// </summary>
        private void UpdatePlanogramFromProductAttributeCommand_Executed()
        {
            MainViewModel.UpdatePlanogramFromProductAttribute();
        }

        #endregion

        #region UpdateAllPlanogramsFromProductAttributeCommand

        /// <summary>
        ///     The command to update all open <c>Planogram</c>'s <c>Product Attributes</c>.
        /// </summary>
        private RelayCommand _updateAllPlanogramsFromProductAttributeCommand;

        /// <summary>
        ///     Static reference to the <see cref="Instance"/> <see cref="UpdateAllPlanogramsFromProductAttributeCommand"/>.
        /// </summary>
        public static RelayCommand UpdateAllPlanogramsFromProductAttribute { get { return Instance.UpdateAllPlanogramsFromProductAttributeCommand; } }

        /// <summary>
        ///     Get the command to update all open <c>Planogram</c>'s <c>Product Attributes</c>.
        /// </summary>
        /// <remarks>This command is created lazily, when requested for the first time.</remarks>
        public RelayCommand UpdateAllPlanogramsFromProductAttributeCommand
        {
            get
            {
                if (_updateAllPlanogramsFromProductAttributeCommand != null) return _updateAllPlanogramsFromProductAttributeCommand;

                _updateAllPlanogramsFromProductAttributeCommand =
                    new RelayCommand(o => UpdateAllPlanogramProductAttributesCommand_Executed(),
                                     o => UpdateAllPlanogramProductAttributesCommand_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_DesignContext_UpdateAllPlanogramsFromProductAttribute,
                        FriendlyDescription = Message.Ribbon_DesignContext_UpdateAllPlanogramsFromProductAttribute_Description,
                        Icon = ImageResources.DesignContext_UpdateAllPlanogramsFromProductAttribute_32,
                        SmallIcon = ImageResources.DesignContext_UpdateAllPlanogramsFromProductAttribute_32
                    };
                RegisterCommand(_updateAllPlanogramsFromProductAttributeCommand);
                return _updateAllPlanogramsFromProductAttributeCommand;
            }
        }

        /// <summary>
        ///     Invoked when checking whether <see cref="UpdateAllPlanogramsFromProductAttributeCommand"/> can be executed.
        /// </summary>
        /// <returns><c>True</c> if the command can be executed, <c>false</c> otherwise.</returns>
        private Boolean UpdateAllPlanogramProductAttributesCommand_CanExecute()
        {
            if (NoActivePlanogram)
            {
                UpdateAllPlanogramsFromProductAttributeCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            UpdateAllPlanogramsFromProductAttributeCommand.DisabledReason = String.Empty;
            return true;
        }

        /// <summary>
        ///     Invoked whenever the <see cref="UpdateAllPlanogramProductAttributesCommand"/> is executed.
        /// </summary>
        private void UpdateAllPlanogramProductAttributesCommand_Executed()
        {
            MainViewModel.UpdateAllPlanogramsFromProductAttribute();
        }

        #endregion

        #region FixtureUpdateXPositionsWithGapsCommand

        /// <summary>
        ///     The command to update the current <c>Planogram</c>'s <c>Bay X Positions With Gaps</c>.
        /// </summary>
        private RelayCommand _fixtureUpdateXPositionsWithGapsCommand;

        /// <summary>
        ///     Static reference to the <see cref="Instance"/> <see cref="FixtureUpdateXPositionsWithGapsCommand"/>.
        /// </summary>
        public static RelayCommand FixtureUpdateXPositionsWithGaps { get { return Instance.FixtureUpdateXPositionsWithGapsCommand; } }

        /// <summary>
        ///     Get the command to update the current <c>Planogram</c>'s <c>Bay X Positions With Gaps</c>.
        /// </summary>
        /// <remarks>This command is created lazily, when requested for the first time.</remarks>
        public RelayCommand FixtureUpdateXPositionsWithGapsCommand
        {
            get
            {
                if (_fixtureUpdateXPositionsWithGapsCommand != null) return _fixtureUpdateXPositionsWithGapsCommand;

                _fixtureUpdateXPositionsWithGapsCommand =
                    new RelayCommand(o => FixtureUpdateXPositionsWithGapsCommand_Executed(),
                                     o => FixtureUpdateXPositionsWithGapsCommand_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_DesignContext_FixtureUpdateXPositionsWithGaps,
                        FriendlyDescription = Message.Ribbon_DesignContext_FixtureUpdateXPositionsWithGaps_Description,
                        Icon = ImageResources.DesignContext_FixtureUpdateXPositionsWithGaps_32,
                        SmallIcon = ImageResources.DesignContext_FixtureUpdateXPositionsWithGaps_32
                    };
                RegisterCommand(_fixtureUpdateXPositionsWithGapsCommand);
                return _fixtureUpdateXPositionsWithGapsCommand;
            }
        }

        /// <summary>
        ///     Invoked when checking whether <see cref="FixtureUpdateXPositionsWithGapsCommand"/> can be executed.
        /// </summary>
        /// <returns><c>True</c> if the command can be executed, <c>false</c> otherwise.</returns>
        private Boolean FixtureUpdateXPositionsWithGapsCommand_CanExecute()
        {
            //Do we have an active planogram
            if (ActivePlanController == null)
            {
                FixtureUpdateXPositionsWithGapsCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //Do we have a selected plan
            if (ActivePlanController.SelectedPlanDocument == null)
            {
                FixtureUpdateXPositionsWithGapsCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //Can the selected plan recalculate the fixture x positions
            if (!ActivePlanController.SelectedPlanDocument.Planogram.Model.CanReCalculateFixtureXPositions())
            {
                FixtureUpdateXPositionsWithGapsCommand.DisabledReason = Message.DisabledReason_FixtureUpdateComplex;
                return false;
            }

            FixtureUpdateXPositionsWithGapsCommand.DisabledReason = String.Empty;
            return true;
        }

        /// <summary>
        ///     Invoked whenever the <see cref="FixtureUpdateXPositionsWithGapsCommand"/> is executed.
        /// </summary>
        private void FixtureUpdateXPositionsWithGapsCommand_Executed()
        {
            MainViewModel.FixtureUpdateXPositionsWithGaps();
        }

        #endregion

        #region FixtureUpdateXPositionsWithoutGapsCommand

        /// <summary>
        ///     The command to update the current <c>Planogram</c>'s <c>Bay X Positions Without Gaps</c>.
        /// </summary>
        private RelayCommand _fixtureUpdateXPositionsWithoutGapsCommand;

        /// <summary>
        ///     Static reference to the <see cref="Instance"/> <see cref="FixtureUpdateXPositionsWithoutGapsCommand"/>.
        /// </summary>
        public static RelayCommand FixtureUpdateXPositionsWithoutGaps { get { return Instance.FixtureUpdateXPositionsWithoutGapsCommand; } }

        /// <summary>
        ///     Get the command to update the current <c>Planogram</c>'s <c>Bay X Positions Without Gaps</c>.
        /// </summary>
        /// <remarks>This command is created lazily, when requested for the first time.</remarks>
        public RelayCommand FixtureUpdateXPositionsWithoutGapsCommand
        {
            get
            {
                if (_fixtureUpdateXPositionsWithoutGapsCommand != null) return _fixtureUpdateXPositionsWithoutGapsCommand;

                _fixtureUpdateXPositionsWithoutGapsCommand =
                    new RelayCommand(o => FixtureUpdateXPositionsWithoutGapsCommand_Executed(),
                                     o => FixtureUpdateXPositionsWithoutGapsCommand_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_DesignContext_FixtureUpdateXPositionsWithoutGaps,
                        FriendlyDescription = Message.Ribbon_DesignContext_FixtureUpdateXPositionsWithoutGaps_Description,
                        Icon = ImageResources.DesignContext_FixtureUpdateXPositionsWithoutGaps_32,
                        SmallIcon = ImageResources.DesignContext_FixtureUpdateXPositionsWithoutGaps_32
                    };
                RegisterCommand(_fixtureUpdateXPositionsWithoutGapsCommand);
                return _fixtureUpdateXPositionsWithoutGapsCommand;
            }
        }

        /// <summary>
        ///     Invoked when checking whether <see cref="FixtureUpdateXPositionsWithoutGapsCommand"/> can be executed.
        /// </summary>
        /// <returns><c>True</c> if the command can be executed, <c>false</c> otherwise.</returns>
        private Boolean FixtureUpdateXPositionsWithoutGapsCommand_CanExecute()
        {
            //Do we have an active planogram
            if (ActivePlanController == null)
            {
                FixtureUpdateXPositionsWithoutGapsCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //Do we have a selected plan
            if (ActivePlanController.SelectedPlanDocument == null)
            {
                FixtureUpdateXPositionsWithoutGapsCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //Can the selected plan recalculate the fixture x positions
            if (!ActivePlanController.SelectedPlanDocument.Planogram.Model.CanReCalculateFixtureXPositions())
            {
                FixtureUpdateXPositionsWithoutGapsCommand.DisabledReason = Message.DisabledReason_FixtureUpdateComplex;
                return false;
            }

            FixtureUpdateXPositionsWithoutGapsCommand.DisabledReason = String.Empty;
            return true;
        }

        /// <summary>
        ///     Invoked whenever the <see cref="FixtureUpdateXPositionsWithoutGapsCommand"/> is executed.
        /// </summary>
        private void FixtureUpdateXPositionsWithoutGapsCommand_Executed()
        {
            MainViewModel.FixtureUpdateXPositionsWithoutGaps();
        }

        #endregion

        #region Fixture Move Left

        /// <summary>
        /// Move fixture left
        /// </summary>
        public static RelayCommand FixtureMoveLeft
        {
            get { return Instance.FixtureMoveLeftCommand; }
        }

        private RelayCommand _fixtureMoveLeftCommand;

        /// <summary>
        /// Shows the properties window for the active planogram.
        /// </summary>
        public RelayCommand FixtureMoveLeftCommand
        {
            get
            {
                if (_fixtureMoveLeftCommand == null)
                {
                    _fixtureMoveLeftCommand = new RelayCommand(
                        p => FixtureMoveLeft_Executed(),
                        p => FixtureMoveLeft_CanExecute())
                    {
                        FriendlyName = Message.PlanogramProperties_MoveFixtureLeft,
                        FriendlyDescription = Message.PlanogramProperties_MoveFixtureLeft_Desc,
                        SmallIcon = ImageResources.PlanogramProperties_MoveFixtureLeft_16,
                    };
                    RegisterCommand(_fixtureMoveLeftCommand);
                }
                return _fixtureMoveLeftCommand;
            }
        }
        
        private Boolean FixtureMoveLeft_CanExecute()
        {
            //Do we have an active planogram
            if (ActivePlanController == null)
            {
                this.ShowPlanogramPropertiesCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //Do we have a selected plan
            if (ActivePlanController.SelectedPlanDocument == null)
            {
                this.ShowPlanogramPropertiesCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //Find any invalid selections
            bool noSelectedFixture = ActivePlanController.SelectedPlanDocument.SelectedPlanItems.Where(w => w.Fixture != null && w.PlanItemType == PlanItemType.Fixture).Count() == 0;
            bool multiSelectedFixtures = ActivePlanController.SelectedPlanDocument.SelectedPlanItems.Where(w => w.Fixture != null && w.PlanItemType == PlanItemType.Fixture).Count() > 1;
            bool differentSelectedPlanItems = ActivePlanController.SelectedPlanDocument.SelectedPlanItems.Where(w => w.Fixture != null && w.PlanItemType == PlanItemType.Fixture).Count() != ActivePlanController.SelectedPlanDocument.SelectedPlanItems.Count();

            //Do we have a valid selection?
            if (noSelectedFixture
                || multiSelectedFixtures
                || differentSelectedPlanItems)
            {
                this.ShowPlanogramPropertiesCommand.DisabledReason = Message.DisabledReason_FixtureInvalidSelection;
                return false;
            }

            //Check the selected fixture can move left
            IPlanItem fixture = ActivePlanController.SelectedPlanDocument.SelectedPlanItems.Where(w => w.Fixture != null && w.PlanItemType == PlanItemType.Fixture).FirstOrDefault();

            if (ActivePlanController.SelectedPlanDocument.Planogram != null && ActivePlanController.SelectedPlanDocument.Planogram.Fixtures != null)
            {
                if (ActivePlanController.SelectedPlanDocument.Planogram.Fixtures.Where(w => w != fixture && w.X < fixture.X).Count() == 0)
                {
                    this.ShowPlanogramPropertiesCommand.DisabledReason = Message.DisabledReason_FixtureMoveLeft;
                    return false;
                }
            }
            
            return true;
        }

        private void FixtureMoveLeft_Executed()
        {
            ActivePlanController.FixtureMoveLeft();
        }

        #endregion

        #region Fixture Move Right

        /// <summary>
        /// Move fixture Right
        /// </summary>
        public static RelayCommand FixtureMoveRight
        {
            get { return Instance.FixtureMoveRightCommand; }
        }

        private RelayCommand _fixtureMoveRightCommand;

        /// <summary>
        /// Shows the properties window for the active planogram.
        /// </summary>
        public RelayCommand FixtureMoveRightCommand
        {
            get
            {
                if (_fixtureMoveRightCommand == null)
                {
                    _fixtureMoveRightCommand = new RelayCommand(
                        p => FixtureMoveRight_Executed(),
                        p => FixtureMoveRight_CanExecute())
                    {
                        FriendlyName = Message.PlanogramProperties_MoveFixtureRight,
                        FriendlyDescription = Message.PlanogramProperties_MoveFixtureRight_Desc,
                        SmallIcon = ImageResources.PlanogramProperties_MoveFixtureRight_16,
                    };
                    RegisterCommand(_fixtureMoveRightCommand);
                }
                return _fixtureMoveRightCommand;
            }
        }

        private Boolean FixtureMoveRight_CanExecute()
        {
            //Do we have an active planogram
            if (ActivePlanController == null)
            {
                this.ShowPlanogramPropertiesCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //Do we have a selected plan
            if (ActivePlanController.SelectedPlanDocument == null)
            {
                this.ShowPlanogramPropertiesCommand.DisabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //Find any invalid selections
            bool noSelectedFixture = ActivePlanController.SelectedPlanDocument.SelectedPlanItems.Where(w => w.Fixture != null && w.PlanItemType == PlanItemType.Fixture).Count() == 0;
            bool multiSelectedFixtures = ActivePlanController.SelectedPlanDocument.SelectedPlanItems.Where(w => w.Fixture != null && w.PlanItemType == PlanItemType.Fixture).Count() > 1;
            bool differentSelectedPlanItems = ActivePlanController.SelectedPlanDocument.SelectedPlanItems.Where(w => w.Fixture != null && w.PlanItemType == PlanItemType.Fixture).Count() != ActivePlanController.SelectedPlanDocument.SelectedPlanItems.Count();

            //Do we have a valid selection?
            if (noSelectedFixture
                || multiSelectedFixtures
                || differentSelectedPlanItems)
            {
                this.ShowPlanogramPropertiesCommand.DisabledReason = Message.DisabledReason_FixtureInvalidSelection;
                return false;
            }

            //Check the selected fixture can move left
            IPlanItem fixture = ActivePlanController.SelectedPlanDocument.SelectedPlanItems.Where(w => w.Fixture != null && w.PlanItemType == PlanItemType.Fixture).FirstOrDefault();

            if (ActivePlanController.SelectedPlanDocument.Planogram != null && ActivePlanController.SelectedPlanDocument.Planogram.Fixtures != null)
            {
                if (ActivePlanController.SelectedPlanDocument.Planogram.Fixtures.Where(w => w != fixture && w.X > fixture.X).Count() == 0)
                {
                    this.ShowPlanogramPropertiesCommand.DisabledReason = Message.DisabledReason_FixtureMoveRight;
                    return false;
                }
            }

            return true;
        }

        private void FixtureMoveRight_Executed()
        {
            ActivePlanController.FixtureMoveRight();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the contents of the clipboard.
        /// </summary>
        /// <returns></returns>
        public static ReadOnlyCollection<IPlanItem> GetClipboardContent()
        {
            return new ReadOnlyCollection<IPlanItem>(Instance._clipboardList);
        }

        /// <summary>
        /// Processes the given key combination.
        /// </summary>
        /// <param name="key">The key pressed</param>
        /// <param name="modifier">Any active modifier</param>
        /// <returns>True if a command was executed.</returns>
        public static Boolean ProcessKeyDown(Key key, ModifierKeys modifier)
        {
            Boolean isProcessed = false;

            foreach (IRelayCommand cmd in Instance._viewModelCommands.ToList())
            {
                if (cmd.InputGestureKey == key
                    && cmd.InputGestureModifiers == modifier)
                {
                    Object args = null;

                    if (cmd == NewDesignView || cmd == NewPerspectiveView
                        || cmd == NewFrontView || cmd == NewBackView
                        || cmd == NewTopView || cmd == NewBottomView
                        || cmd == NewLeftView || cmd == NewRightView)
                    {
                        //check for existing doc.
                        args = true;
                    }

                    if (cmd.CanExecute(args))
                    {
                        cmd.Execute(args);
                        isProcessed = true;
                    }
                }



                if (!isProcessed && modifier == ModifierKeys.None)
                {
                    Boolean isNumeric = ((key >= Key.A && key <= Key.Z) || (key >= Key.D0 && key <= Key.D9) || (key >= Key.NumPad0 && key <= Key.NumPad9));
                    if (isNumeric)
                    {
                        isProcessed = UpdateFacingWideFromNumericKey(key);
                    }
                }
            }

            if (!isProcessed && ActivePlanController != null
                && ActivePlanController.SelectedPlanDocument != null)
            {
                isProcessed = ActivePlanController.SelectedPlanDocument.ProcessKeyboardShortcut(modifier, key);
            }

            return isProcessed;
        }

        /// <summary>
        /// Processes the given key combination.
        /// </summary>
        /// <param name="key">The key released</param>
        /// <param name="modifier">Any active modifier</param>
        /// <returns>True if a command was executed.</returns>
        public static Boolean ProcessKeyUp(Key key, ModifierKeys modifier)
        {
            Boolean isProcessed = false;

            if (!isProcessed && ActivePlanController != null
                && ActivePlanController.SelectedPlanDocument != null &&
                ActivePlanController.SelectedPlanDocument.Planogram != null)
            {
                //This should probably be moved to an IPlanDocument method
                switch (key)
                {
                    case Key.LeftCtrl:
                    case Key.RightCtrl:

                        //End undoable action for plan actions occuring in an unfocused document. e.g
                        //when moving a component with the arrow keys.
                        PlanogramView planView = ActivePlanController.SelectedPlanDocument.Planogram;
                        if (planView.IsRecordingUndoableAction && !planView.IsUpdating)
                        {
                            planView.EndUndoableAction();
                            isProcessed = true;
                        }
                        break;
                }

            }


            return isProcessed;
        }
        /// <summary>
        /// Updates the facings wide from a numeric key object. Does not apply change when autofil is on.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static Boolean UpdateFacingWideFromNumericKey(Key key)
        {
            PlanControllerViewModel activePlan = App.MainPageViewModel.ActivePlanController;

            if (activePlan == null || activePlan.SelectedPlanItems == null) return false;
            if (activePlan.SourcePlanogram.ProductPlacementX == PlanogramProductPlacementXType.FillWide) return false;

            short facingsWideToSet;

            switch (key)
            {
                case Key.D1:
                case Key.NumPad1:
                    facingsWideToSet = 1;
                    break;
                case Key.D2:
                case Key.NumPad2:
                    facingsWideToSet = 2;
                    break;
                case Key.D3:
                case Key.NumPad3:
                    facingsWideToSet = 3;
                    break;
                case Key.D4:
                case Key.NumPad4:
                    facingsWideToSet = 4;
                    break;
                case Key.D5:
                case Key.NumPad5:
                    facingsWideToSet = 5;
                    break;
                case Key.D6:
                case Key.NumPad6:
                    facingsWideToSet = 6;
                    break;
                case Key.D7:
                case Key.NumPad7:
                    facingsWideToSet = 7;
                    break;
                case Key.D8:
                case Key.NumPad8:
                    facingsWideToSet = 8;
                    break;
                case Key.D9:
                case Key.NumPad9:
                    facingsWideToSet = 9;
                    break;
                default:
                    return false;
            }


            IEnumerable<IPlanItem> positionItems = activePlan.SelectedPlanItems.Where(i => i.PlanItemType == PlanItemType.Position);

            foreach (var positionItem in positionItems)
            {
                positionItem.Position.FacingsWide = facingsWideToSet;
            }

            return true;
        }

        /// <summary>
        /// Assigns keyboard shortcuts to all commands.
        /// </summary>
        private void AssignKeyboardShortcuts()
        {
            //File commands
            SetShortcut(NewPlanogramCommand, ModifierKeys.Control, Key.N);
            SetShortcut(OpenPlanogramFromFileCommand, ModifierKeys.Control, Key.O);
            SetShortcut(SavePlanogramCommand, ModifierKeys.Control, Key.S);
            SetShortcut(SavePlanogramAsFileCommand, ModifierKeys.None, null);
            //TODO: [Ctrl + P] - Print

            //editing functions
            SetShortcut(CutCommand, ModifierKeys.Control, Key.X);
            SetShortcut(CopyCommand, ModifierKeys.Control, Key.C);
            SetShortcut(PasteCommand, ModifierKeys.Control, Key.V);
            SetShortcut(RedoCommand, ModifierKeys.Control, Key.Y);
            SetShortcut(UndoCommand, ModifierKeys.Control, Key.Z);
            SetShortcut(RemoveSelectedCommand, ModifierKeys.None, Key.Delete);
            SetShortcut(SearchCommand, ModifierKeys.Control, Key.F);

            //Plan editing
            SetShortcut(ShowPlanogramPropertiesCommand, ModifierKeys.None, null);
            SetShortcut(ShowSelectedItemPropertiesCommand, ModifierKeys.None, Key.Enter);

            SetShortcut(ToggleProductLabelVisibilityCommand, ModifierKeys.Control, Key.L);
            SetShortcut(ToggleFixtureLabelVisibilityCommand, ModifierKeys.Control, Key.K);
            SetShortcut(ToggleHighlightVisibilityCommand, ModifierKeys.Control, Key.H);

            SetShortcut(AddToFixtureLibraryCommand, ModifierKeys.None, null);
            SetShortcut(EditComponentCommand, ModifierKeys.None, null);
            SetShortcut(CreateAssemblyCommand, ModifierKeys.None, null);
            SetShortcut(SplitAssemblyCommand, ModifierKeys.None, null);

            SetShortcut(AddNewProductCommand, ModifierKeys.Shift, Key.F7);
            //SetShortcut(AddNewPositionCommand, ModifierKeys.Shift, Key.F8);
            SetShortcut(FillPositionsHighCommand, ModifierKeys.None, null);
            SetShortcut(FillPositionsWideCommand, ModifierKeys.None, null);
            SetShortcut(FillPositionsDeepCommand, ModifierKeys.None, null);
            SetShortcut(IncreasePositionFacingsCommand, ModifierKeys.Shift, Key.Add);
            SetShortcut(DecreasePositionFacingsCommand, ModifierKeys.Shift, Key.Subtract);

            //settings
            SetShortcut(ShowHighlightEditorCommand, ModifierKeys.None, null);
            SetShortcut(ShowProductLabelEditorCommand, ModifierKeys.None, null);
            SetShortcut(ShowFixtureLabelEditorCommand, ModifierKeys.None, null);


            //zooming
            SetShortcut(ToggleZoomToAreaModeCommand, ModifierKeys.None, null);
            SetShortcut(TogglePanningModeCommand, ModifierKeys.Control, Key.H); //middle mouse button
            SetShortcut(ZoomToFitCommand, ModifierKeys.Control, Key.Home);
            SetShortcut(ZoomToFitHeightCommand, ModifierKeys.None, null);
            SetShortcut(ZoomToFitWidthCommand, ModifierKeys.None, null);
            SetShortcut(ZoomInCommand, ModifierKeys.Control, Key.PageUp);
            SetShortcut(ZoomOutCommand, ModifierKeys.Control, Key.PageDown);
            SetShortcut(ZoomSelectedItemsCommand, ModifierKeys.None, null);

            //views
            SetShortcut(NewDesignViewCommand, ModifierKeys.Shift, Key.D1);
            SetShortcut(NewPerspectiveViewCommand, ModifierKeys.Shift, Key.D2);
            SetShortcut(NewFrontViewCommand, ModifierKeys.Shift, Key.D3);
            SetShortcut(NewBackViewCommand, ModifierKeys.Shift, Key.D4);
            SetShortcut(NewTopViewCommand, ModifierKeys.Shift, Key.D5);
            SetShortcut(NewBottomViewCommand, ModifierKeys.Shift, Key.D6);
            SetShortcut(NewLeftViewCommand, ModifierKeys.Shift, Key.D7);
            SetShortcut(NewRightViewCommand, ModifierKeys.Shift, Key.D8);
            SetShortcut(NewProductListViewCommand, ModifierKeys.Shift, Key.P);
            SetShortcut(NewFixtureListViewCommand, ModifierKeys.Shift, Key.F);

            SetShortcut(TileViewsHorizontalCommand, ModifierKeys.Shift, Key.F3);
            SetShortcut(TileViewsVerticalCommand, ModifierKeys.Shift, Key.F4);
            SetShortcut(ArrangeAllViewsCommand, ModifierKeys.Shift, Key.F5);
            SetShortcut(DockAllViewsCommand, ModifierKeys.Shift, Key.F6);
            SetShortcut(SwitchToViewCommand, ModifierKeys.None, null);

            //panels
            SetShortcut(ShowHideLibraryPanelCommand, ModifierKeys.Shift, Key.L);
            SetShortcut(ShowHidePropertiesPanelCommand, ModifierKeys.None, null);

            //windows
            SetShortcut(TileWindowsHorizontalCommand, ModifierKeys.None, null);
            SetShortcut(TileWindowsVerticalCommand, ModifierKeys.None, null);
        }

        /// <summary>
        /// Sets the shortcut against the given command.
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="modifier"></param>
        /// <param name="key"></param>
        private void SetShortcut(RelayCommand cmd, ModifierKeys modifier, Key? key)
        {
            cmd.InputGestureModifiers = modifier;
            cmd.InputGestureKey = key;
        }

        /// <summary>
        ///     Displays a warning showing that there is no connection to the repository database.
        /// </summary>
        private static void ShowMissingConnectionWarning()
        {
            var msg = new ModalMessage
            {
                MessageIcon = ImageResources.Error_32,
                Description =
                    String.Format(CultureInfo.CurrentCulture, Message.OpenRecent_MissingConnectionWarning_Description),
                Header = Message.OpenRecent_MissingConnectionWarning_Header,
                Button1Content = Message.Generic_OK,
                ButtonCount = 1,
                Owner = App.MainPageViewModel.AttachedControl,
                WindowStartupLocation = WindowStartupLocation.CenterScreen
            };

            msg.ShowDialog();
        }

        /// <summary>
        ///     Displays a warning showing that there is no connection to the repository database.
        /// </summary>
        private static void ShowNotSufficientPermissionWarning()
        {
            var msg = new ModalMessage
            {
                MessageIcon = ImageResources.Error_32,
                Description =
                    String.Format(CultureInfo.CurrentCulture, Message.OpenRecent_InsufficientPermissionWarning_Description),
                Header = Message.OpenRecent_InsufficientPermissionsWarning_Header,
                Button1Content = Message.Generic_OK,
                ButtonCount = 1,
                Owner = App.MainPageViewModel.AttachedControl,
                WindowStartupLocation = WindowStartupLocation.CenterScreen
            };

            msg.ShowDialog();
        }

        /// <summary>
        /// Registers the given command so that keyboard shortcuts can be handled.
        /// </summary>
        private void RegisterCommand(IRelayCommand command)
        {
            Debug.Assert(!_viewModelCommands.Contains(command),
                "Command already registered");

            _viewModelCommands.Add(command);
        }

        /// <summary>
        ///     Check whether the given <paramref name="componentView" /> is un-merchandised.
        /// </summary>
        /// <param name="componentView"></param>
        /// <returns></returns>
        /// <remarks>
        ///     A component needs to be merchandisble to even be un-merchandised.
        ///     <para />
        ///     A component must be completely without positions.
        ///     <para />
        ///     A combined component must be without positions in ANY combined component.
        /// </remarks>
        private static Boolean IsUnMerchandised(PlanogramComponentView componentView)
        {
            //  If all subcomponents are not merchandisible this cannot be "unmerchandised".
            if (componentView.SubComponents.All(s => !s.IsMerchandisable)) return false;

            //  If there is ANY position in the subcomponents is is not "unmerchandised".
            if (componentView.SubComponents.SelectMany(s => s.Positions).Any()) return false;

            //  If if can be combined and the combined components have positions it is not "unmerchandised".
            if (componentView.SubComponents.Any(s => s.CombineType != PlanogramSubComponentCombineType.None))
            {
                // Get any position in the combined components.
                if (componentView.ComponentModel.GetSubComponentPlacements()
                     .First().GetCombinedWithList().Any(placement => placement.GetPlanogramPositions().Any())) return false;
            }

            //  Must be "unmerchandised".
            return true;
        }

        #endregion
    }
}
