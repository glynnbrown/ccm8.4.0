﻿#region Header Information

// // Copyright © Galleria RTS Ltd 2015

#region Version History CCM 830

//  V8-31386 : A.Heathcote
//      Created
// V8-32652 : A.Silva
//  Refactored add and delete selected icons to use the same set.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.ViewModel;
using CommonMessage = Galleria.Ccm.Common.Wpf.Resources.Language.Message;
using CommonImageResources = Galleria.Ccm.Common.Wpf.Resources.ImageResources;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    public class ProductFixtureAdvancedSearchViewModel : WindowViewModelBase
    {
        #region Nested Classes
        /// <summary>
        /// A simple class that is used to construct
        /// the Group Selections in the fieldSelector
        /// </summary>
        public sealed class FieldGroup
        {
            public String GroupName { get; set; }
            public Boolean IsAllGroup { get; set; }

            public override string ToString()
            {
                return GroupName;
            }
        }

        /// <summary>
        /// A simple class thats constructs 
        /// the fields withing the field groups.
        /// </summary>
        public sealed class FieldGroupRows
        {
            #region Properties

            public String GroupName { get; set; }
            public ObjectFieldInfo FieldInfo { get; private set; }


            public String Header
            {
                get { return FieldInfo.FieldFriendlyName; }
            }

            public String DisplayNames
            {
                get { return FieldInfo.FieldFriendlyName; }
            }
            #endregion

            #region Constructor

            public FieldGroupRows(ObjectFieldInfo fieldInfo, String groupName)
            {
                this.FieldInfo = fieldInfo;
                this.GroupName = groupName;
            }

            #endregion
        }

        /// <summary>
        /// Wrapper object that is used by the search screen,
        /// to add the selected fields to the UserEditorSettings
        /// </summary>
        public sealed class SelectedField
        {
            #region Fields
            
            private readonly String _fieldFriendlyName;
            private readonly String _fieldPlaceHolder;            
            #endregion

            #region Properties
            public String FieldFriendlyName
            {
                get { return _fieldFriendlyName; }
            }

            public String FieldPlaceHolder
            {
                get { return _fieldPlaceHolder; }
            }

            /// <summary>
            /// Gets/Sets whether this is a default field that should be locked in place.
            /// </summary>
            public Boolean IsLocked { get; set; }
            public SearchColumnType ColumnType { get; set; }

            #endregion

            #region Constructor

            /// <summary>
            /// Constructor takes ObjectFieldInfo's and creates a new type, to aid in saving down.
            /// </summary>
            public SelectedField(ObjectFieldInfo fieldInfo)
            {       
                //Friendly Name is used to populate the second column of the grid for each row
                _fieldFriendlyName = fieldInfo.FieldFriendlyName;

                //As the fieldinfo is constructed as a Selected Field it
                //sets to locked if any of the fields match any of the defaults in the static field
                IsLocked =
                    ProductFixtureSearchViewModel.DefaultProductSearchFields
                        .Any(f =>
                            f.Equals(fieldInfo.OwnerType) &&
                            f.PropertyName.Equals(fieldInfo.PropertyName))
                    ||
                    ProductFixtureSearchViewModel.DefaultComponentSearchFields
                        .Any(f =>
                            f.OwnerType.Equals(fieldInfo.OwnerType) &&
                            f.PropertyName.Equals(fieldInfo.PropertyName));

                 _fieldPlaceHolder = fieldInfo.FieldPlaceholder;
            }

            #endregion

            //#region INotifyProperty Changed

            //public event PropertyChangedEventHandler PropertyChanged;

            //private void OnPropertyChanged(String property)
            //{
            //    if (PropertyChanged == null) return;
            //    PropertyChanged(this, new PropertyChangedEventArgs(property));
            //}

            //#endregion

        }

        #endregion

        #region Fields
        private Boolean? _dialogResult;
        private readonly ReadOnlyCollection<FieldGroupRows> _masterFields;
        private readonly ReadOnlyCollection<FieldGroup> _availableFieldGroups;
        private FieldGroup _selectedFieldGroup;
        private readonly BulkObservableCollection<FieldGroupRows> _selectedGroupOfFields = new BulkObservableCollection<FieldGroupRows>();
        private ReadOnlyBulkObservableCollection<FieldGroupRows> _fieldsInFieldSelectorGroupRO;
        private readonly ObservableCollection<FieldGroupRows> _selectedAvailableFields = new ObservableCollection<FieldGroupRows>();
        private readonly BulkObservableCollection<SelectedField> _selectedProductFields = new BulkObservableCollection<SelectedField>();
        private readonly ObservableCollection<SelectedField> _highlightedProductFields = new ObservableCollection<SelectedField>();
        private readonly BulkObservableCollection<SelectedField> _selectedComponentFields = new BulkObservableCollection<SelectedField>();
        private readonly ObservableCollection<SelectedField> _highlightedComponentFields = new ObservableCollection<SelectedField>();

        #endregion

        #region Binding Property Paths
        //Properties

        public static readonly PropertyPath AvailableFieldGroupsProperty = GetPropertyPath<ProductFixtureAdvancedSearchViewModel>(p => p.AvailableFieldGroups);
        public static readonly PropertyPath SelectedFieldGroupProperty = GetPropertyPath<ProductFixtureAdvancedSearchViewModel>(p => p.SelectedFieldGroup);
        public static readonly PropertyPath FieldsInFieldSelectorGroupProperty = GetPropertyPath<ProductFixtureAdvancedSearchViewModel>(p => p.FieldsInFieldSelectorGroup);
        public static readonly PropertyPath SelectedAvailableFieldsProperty = GetPropertyPath<ProductFixtureAdvancedSearchViewModel>(p => p.SelectedAvailableFields);
        public static readonly PropertyPath SelectedProductFieldsProperty = GetPropertyPath<ProductFixtureAdvancedSearchViewModel>(p => p.SelectedProductFields);
        public static readonly PropertyPath HighlightedProductFieldsProperty = GetPropertyPath<ProductFixtureAdvancedSearchViewModel>(p => p.HighlightedProductFields);
        public static readonly PropertyPath SelectedComponentFieldsProperty = GetPropertyPath<ProductFixtureAdvancedSearchViewModel>(p => p.SelectedComponentFields);
        public static readonly PropertyPath HighlightedComponentFieldsProperty = GetPropertyPath<ProductFixtureAdvancedSearchViewModel>(p => p.HighlightedComponentFields);

        // Commands

        //Add Selected for Product and Component tabs
        public static readonly PropertyPath AddSelectedProductFieldCommandProperty = GetPropertyPath<ProductFixtureAdvancedSearchViewModel>(c => c.AddSelectedProductFieldCommand);
        public static readonly PropertyPath AddSelectedComponentFieldsPropertyCommand = GetPropertyPath<ProductFixtureAdvancedSearchViewModel>(c => c.AddSelectedComponentFieldsCommand);
        //Remove Selected for Product and Component Tabs
        public static readonly PropertyPath RemoveSelectedColumnsCommandProperty = GetPropertyPath<ProductFixtureAdvancedSearchViewModel>(c => c.RemoveSelectedProductFieldsCommand);
        public static readonly PropertyPath RemoveSelectedCompontentColumnsCommandProperty = GetPropertyPath<ProductFixtureAdvancedSearchViewModel>(c => c.RemoveSelectedComponentFieldsCommand);
        //Move Down for Product and Component tab
        public static readonly PropertyPath MoveColumnDownCommandProperty = GetPropertyPath<ProductFixtureAdvancedSearchViewModel>(c => c.MoveProductFieldDownCommand);
        public static readonly PropertyPath MoveComponentColumnDownCommandProperty = GetPropertyPath<ProductFixtureAdvancedSearchViewModel>(c => c.MoveComponentFieldDownCommand);
        //Move Up for Product and Component tab        
        public static readonly PropertyPath MoveColumnUpCommandProperty = GetPropertyPath<ProductFixtureAdvancedSearchViewModel>(c => c.MoveProductFieldUpCommand);
        public static readonly PropertyPath MoveComponentColumnUpCommandProperty = GetPropertyPath<ProductFixtureAdvancedSearchViewModel>(c => c.MoveComponentFieldColumnUpCommand);
        //Bottom Buttons (Ok and Cancel)
        public static readonly PropertyPath OkCommandProperty = GetPropertyPath<ProductFixtureAdvancedSearchViewModel>(c => c.OkCommand);
        public static readonly PropertyPath CloseCommandProperty = GetPropertyPath<ProductFixtureAdvancedSearchViewModel>(c => c.CloseCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the result and closes the 
        /// attached dialog.
        /// </summary>
        public Boolean? DialogResult  //Boolean a Nullable Type adding an additional value of Null as well as the True/False
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;
                CloseDialog(value);
            }
        }

        /// <summary>
        /// Gets the list of available item types
        /// </summary>
        public ReadOnlyCollection<FieldGroup> AvailableFieldGroups
        {
            get { return _availableFieldGroups; }
        }

        /// <summary>
        /// Gets/Sets the selected item type
        /// </summary>
        public FieldGroup SelectedFieldGroup
        {
            get { return _selectedFieldGroup; }
            set
            {
                _selectedFieldGroup = value;
                OnPropertyChanged(SelectedFieldGroupProperty);

                OnSelectedFieldGroupChanged(value);
            }
        }

        /// <summary>
        /// Returns the collection of fields available for the selected group in the fieldselector.
        /// </summary>
        public ReadOnlyBulkObservableCollection<FieldGroupRows> FieldsInFieldSelectorGroup
        {
            get
            {
                if (_fieldsInFieldSelectorGroupRO == null)
                {
                    _fieldsInFieldSelectorGroupRO = new ReadOnlyBulkObservableCollection<FieldGroupRows>(_selectedGroupOfFields);
                }
                return _fieldsInFieldSelectorGroupRO;
            }
        }

        /// <summary>
        /// Returns the editable collection of the selected available fields (from field selector)
        /// </summary>
        public ObservableCollection<FieldGroupRows> SelectedAvailableFields
        {
            get { return _selectedAvailableFields; }
        }

        /// <summary>
        /// Returns the collection of views for columns that have been assigned.
        /// </summary>
        public BulkObservableCollection<SelectedField> SelectedProductFields
        {
            get
            {
                return _selectedProductFields;
            }
        }

        public BulkObservableCollection<SelectedField> SelectedComponentFields
        {
            get
            {
                return _selectedComponentFields;
            }
        }

        /// <summary>
        /// Returns the editable collection of column views that are selected.
        /// </summary>
        public ObservableCollection<SelectedField> HighlightedProductFields
        {
            get { return _highlightedProductFields; }
        }

        public ObservableCollection<SelectedField> HighlightedComponentFields
        {
            get { return _highlightedComponentFields; }
        }

        #endregion

        #region Constructor

        public ProductFixtureAdvancedSearchViewModel()
        {
            List<ObjectFieldInfo> fieldInfos = PlanogramFieldHelper.EnumerateAllFields().ToList();

            //Prefix all property names with the owner type name
            //and swap the group names to be the owner friendly names 
            //Needs to change, String Splicing not a good idea, unless use Regex
            foreach (ObjectFieldInfo field in fieldInfos)
            {
               field.PropertyName = field.OwnerType.Name + "." + field.PropertyName;
            }
            
                      
            List<FieldGroupRows> allFields = new List<FieldGroupRows>();

            foreach (ObjectFieldInfo fieldInfo in fieldInfos)
            {
                allFields.Add(new FieldGroupRows(fieldInfo, fieldInfo.OwnerFriendlyName));
            }

            _masterFields = allFields.AsReadOnly();

            //setting field groups
            List<FieldGroup> availableFieldGroups = new List<FieldGroup>();
            availableFieldGroups.Add(new FieldGroup() { GroupName = Message.DataSheetEditor_AllFieldGroup, IsAllGroup = true });
            availableFieldGroups.AddRange(_masterFields.GroupBy(g => g.GroupName).Select(c => new FieldGroup() { GroupName = c.Key }));
            _availableFieldGroups = availableFieldGroups.AsReadOnly();

            //select first field group 
            this.SelectedFieldGroup = this.AvailableFieldGroups.First();
            
            if (!App.ViewState.Settings.Model.SelectedColumns.Any())
            {
                foreach (ObjectFieldInfo field in ProductFixtureSearchViewModel.DefaultProductSearchFields)
                {
                    SelectedProductFields.Add(new SelectedField(field)); 
                }
                foreach (ObjectFieldInfo field in ProductFixtureSearchViewModel.DefaultComponentSearchFields)
                {
                    SelectedComponentFields.Add(new SelectedField(field));
                }
           }

            foreach (UserEditorSettingsSelectedColumn field in App.ViewState.Settings.Model.SelectedColumns)
            {
                if (field.ColumnType.Equals(SearchColumnType.Product))
                {
                    this._selectedProductFields.Add(new SelectedField
                        (_masterFields.FirstOrDefault(f => f.FieldInfo.FieldPlaceholder.Equals(field.FieldPlaceHolder)).FieldInfo));
                }
                else if (field.ColumnType.Equals(SearchColumnType.Component))
                {
                    this._selectedComponentFields.Add(new SelectedField
                        (_masterFields.FirstOrDefault(f => f.FieldInfo.FieldPlaceholder.Equals(field.FieldPlaceHolder)).FieldInfo));
                }

            }

            foreach (SelectedField field in _selectedProductFields)
            {
                field.ColumnType = SearchColumnType.Product;
                foreach (ObjectFieldInfo fieldInfo in ProductFixtureSearchViewModel.DefaultProductSearchFields)
                {
                    if (field.FieldPlaceHolder == fieldInfo.FieldPlaceholder)
                    {
                        field.IsLocked = true;
                    }
                }
            }

            foreach (SelectedField field in _selectedComponentFields)
            {
                field.ColumnType = SearchColumnType.Component;
                foreach (ObjectFieldInfo fieldInfo in ProductFixtureSearchViewModel.DefaultComponentSearchFields)
                {
                    if (field.FieldPlaceHolder == fieldInfo.FieldPlaceholder)
                    {
                        field.IsLocked = true;
                    }
                }
            }
        }

        #endregion

        #region Commands

        #region AddSelectedFieldsCommand

        private RelayCommand _addSelectedFieldsCommand;

        /// <summary>
        ///     Adds any selected available columns to the current view column list
        /// </summary>
        public RelayCommand AddSelectedProductFieldCommand
        {
            get
            {
                if (_addSelectedFieldsCommand != null) return _addSelectedFieldsCommand;

                _addSelectedFieldsCommand =
                    new RelayCommand(p => AddSelectedFields_Executed(), p => AddSelectedFields_CanExecute())
                    {
                        FriendlyDescription = Message.ColumnLayoutEditor_AddSelectedColumns_Description,
                        SmallIcon = CommonImageResources.Add_16,
                        DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnsSelected
                    };
                RegisterCommand(_addSelectedFieldsCommand);

                return _addSelectedFieldsCommand;
            }
        }
        private Boolean AddSelectedFields_CanExecute()
        {
            if (this.SelectedAvailableFields.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void AddSelectedFields_Executed()
        {
            AddProductField();
        }

        #endregion

        #region AddSelectedComponentFieldsCommand

        private RelayCommand _addSelectedComponentFieldsCommand;
        /// <summary>
        ///     Adds any selected available columns to the current view column list
        /// </summary>
        public RelayCommand AddSelectedComponentFieldsCommand
        {
          get
            {
                if (_addSelectedComponentFieldsCommand != null) return _addSelectedComponentFieldsCommand;

                _addSelectedComponentFieldsCommand =
                    new RelayCommand(p => AddSelectedComponentFields_Executed(), p => AddSelectedComponentFields_CanExecute())
                    {
                        FriendlyDescription = Message.ColumnLayoutEditor_AddSelectedColumns_Description,
                        SmallIcon = CommonImageResources.Add_16,
                        DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnsSelected
                    };
                RegisterCommand(_addSelectedComponentFieldsCommand);
                return _addSelectedComponentFieldsCommand;
            }
        }

        private Boolean AddSelectedComponentFields_CanExecute()
        {
            if (this.SelectedAvailableFields.Count > 0)
            {
                return true;
            }
            return false;
        }

        private void AddSelectedComponentFields_Executed()
        {
            AddComponentField();
        }

        #endregion

        #region RemoveSelectedFieldsCommand

        private RelayCommand _removeSelectedProductFieldsCommand;

        /// <summary>
        ///     Remove any selected available columns from the current view column list
        /// </summary>
        public RelayCommand RemoveSelectedProductFieldsCommand
        {
            get
            {
                if (_removeSelectedProductFieldsCommand != null) return _removeSelectedProductFieldsCommand;

                _removeSelectedProductFieldsCommand =
                    new RelayCommand(p => RemoveSelectedProductFields_Executed(), p => RemoveSelectedProductFields_CanExecute())
                    {
                        FriendlyDescription = Message.ColumnLayoutEditor_RemoveSelectedColumns_Description,
                        SmallIcon = CommonImageResources.Delete_16,
                        DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnsSelected
                    };
                RegisterCommand(_removeSelectedProductFieldsCommand);

                return _removeSelectedProductFieldsCommand;
            }
        }

        private Boolean RemoveSelectedProductFields_CanExecute()
        {

            if (HighlightedProductFields.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void RemoveSelectedProductFields_Executed()
        {
            RemoveProductField();
        }

        #endregion

        #region RemoveSelectedComponentFieldsCommand

        private RelayCommand _removeSelectedComponentFieldsCommand;

        /// <summary>
        ///     Remove any selected available columns from the current view column list
        /// </summary>
        public RelayCommand RemoveSelectedComponentFieldsCommand
        {
            get
            {
                if (_removeSelectedComponentFieldsCommand != null) return _removeSelectedComponentFieldsCommand;

                _removeSelectedComponentFieldsCommand =
                    new RelayCommand(p => RemoveSelectedComponentFields_Executed(), p => RemoveSelectedComponentFields_CanExecute())
                    {
                        FriendlyDescription = Message.ColumnLayoutEditor_RemoveSelectedColumns_Description,
                        SmallIcon = CommonImageResources.Delete_16,
                        DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnsSelected
                    };
                RegisterCommand(_removeSelectedComponentFieldsCommand);

                return _removeSelectedComponentFieldsCommand;
            }
        }

        private Boolean RemoveSelectedComponentFields_CanExecute()
        {
            if (HighlightedComponentFields.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void RemoveSelectedComponentFields_Executed()
        {
            RemoveComponentField();
        }

        #endregion        

        #region MoveColumnUpCommand

        private RelayCommand _moveProductFieldUpCommand;
        /// <summary>
        ///     Move column up in the column order
        /// </summary>
        public RelayCommand MoveProductFieldUpCommand
        {
            get
            {
                if (_moveProductFieldUpCommand != null) return _moveProductFieldUpCommand;

                _moveProductFieldUpCommand = new RelayCommand(
                    p => MoveProductFieldUp_Executed(),
                    p => MoveProductFieldUp_CanExecute())
                    {
                        FriendlyDescription = Message.ColumnLayoutEditor_MoveColumnUp_Description,
                        SmallIcon = CommonImageResources.ColumnLayoutEditor_MoveColumnUp,
                        DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnSelected
                    };
                RegisterCommand(_moveProductFieldUpCommand);

                return _moveProductFieldUpCommand;
            }
        }
        private Boolean MoveProductFieldUp_CanExecute()
        {
            if (HighlightedProductFields.Count == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void MoveProductFieldUp_Executed()
        {
            IEnumerable<SelectedField> selectedItems = HighlightedProductFields.ToList();
            foreach (SelectedField row in selectedItems)
            {
                Int32 currentIndex = this.SelectedProductFields.IndexOf(row);
                Int32 newIndex = currentIndex - 1;
                if (currentIndex < 0 || newIndex < 0)
                {
                    continue;
                }
                else
                {
                    SelectedProductFields.Move(currentIndex, newIndex);
                }
            }

            HighlightedProductFields.Clear();
            foreach (SelectedField row in selectedItems)
            {
                HighlightedProductFields.Add(row);
            }
        }

        #endregion

        #region MoveComponentFieldUpCommand

        private RelayCommand _moveComponentFieldUpCommand;
        /// <summary>
        /// Move column up in the column order
        /// </summary>
        public RelayCommand MoveComponentFieldColumnUpCommand
        {
            get
            {
                if (_moveComponentFieldUpCommand != null) return _moveComponentFieldUpCommand;

                _moveComponentFieldUpCommand = new RelayCommand(
                    p => MoveComponentFieldUp_Executed(),
                    p => MoveComponentFieldUp_CanExecute())
                    {
                        FriendlyDescription = Message.ColumnLayoutEditor_MoveColumnUp_Description,
                        SmallIcon = CommonImageResources.ColumnLayoutEditor_MoveColumnUp,
                        DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnSelected
                    };
                RegisterCommand(_moveComponentFieldUpCommand);

                return _moveComponentFieldUpCommand;
            }
        }
        private Boolean MoveComponentFieldUp_CanExecute()
        {
            if (HighlightedComponentFields.Count == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void MoveComponentFieldUp_Executed()
        {
            IEnumerable<SelectedField> selectedItems = HighlightedComponentFields.ToList();
            foreach (SelectedField row in selectedItems)
            {
                Int32 currentIndex = this.SelectedComponentFields.IndexOf(row);
                Int32 newIndex = currentIndex - 1;
                if (currentIndex < 0 || newIndex < 0)
                {
                    continue;
                }
                else
                {
                    SelectedComponentFields.Move(currentIndex, newIndex);
                }
            }
            HighlightedComponentFields.Clear();
            foreach (SelectedField row in selectedItems)
            {
                HighlightedComponentFields.Add(row);
            }
        }

        #endregion

        #region MoveProductFieldDownCommand

        private RelayCommand _moveProductFieldDownCommand;
        /// <summary>
        /// Move column down in the column order
        /// </summary>
        public RelayCommand MoveProductFieldDownCommand
        {
            get
            {
                if (_moveProductFieldDownCommand != null) return _moveProductFieldDownCommand;

                _moveProductFieldDownCommand = new RelayCommand(
                    p => MoveProductFieldDown_Executed(),
                    p => MoveProductFieldDown_CanExecute())
                    {
                        FriendlyDescription = Message.ColumnLayoutEditor_MoveColumnDown_Description,
                        SmallIcon = CommonImageResources.ColumnLayoutEditor_MoveColumnDown,
                        DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnSelected
                    };
                RegisterCommand(_moveProductFieldDownCommand);

                return _moveProductFieldDownCommand;
            }
        }

        private Boolean MoveProductFieldDown_CanExecute()
        {
            if (HighlightedProductFields.Count == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void MoveProductFieldDown_Executed()
        {
            IEnumerable<SelectedField> selectedItems = HighlightedProductFields.ToList();
            foreach (SelectedField row in selectedItems)
            {
                Int32 currentIndex = this.SelectedProductFields.IndexOf(row);
                Int32 newIndex = currentIndex + 1;
                if (newIndex >= SelectedProductFields.Count())
                {
                    continue;
                }
                else
                {
                    SelectedProductFields.Move(currentIndex, newIndex);
                }
            }

            HighlightedProductFields.Clear();
            foreach (SelectedField row in selectedItems)
            {
                HighlightedProductFields.Add(row);
            }
        }

        #endregion

        #region MoveComponentFieldDownCommand

        private RelayCommand _moveComponentFieldDownCommand;
        /// <summary>
        ///     Move column down in the column order
        /// </summary>
        public RelayCommand MoveComponentFieldDownCommand
        {
            get
            {
                if (_moveComponentFieldDownCommand != null) return _moveComponentFieldDownCommand;

                _moveComponentFieldDownCommand = new RelayCommand(
                    p => MoveComponentFieldDown_Executed(),
                    p => MoveComponentFieldDown_CanExecute())
                    {
                        FriendlyDescription = Message.ColumnLayoutEditor_MoveColumnDown_Description,
                        SmallIcon = CommonImageResources.ColumnLayoutEditor_MoveColumnDown,
                        DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnSelected
                    };
                RegisterCommand(_moveComponentFieldDownCommand);

                return _moveComponentFieldDownCommand;
            }
        }
        private Boolean MoveComponentFieldDown_CanExecute()
        {
            if (HighlightedComponentFields.Count == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void MoveComponentFieldDown_Executed()
        {
            IEnumerable<SelectedField> selectedItems = HighlightedComponentFields.ToList();
            foreach (SelectedField row in selectedItems)
            {
                Int32 currentIndex = this.SelectedComponentFields.IndexOf(row);
                Int32 newIndex = currentIndex + 1;
                if (newIndex >= SelectedComponentFields.Count())
                {
                    continue;
                }
                else
                {
                    SelectedComponentFields.Move(currentIndex, newIndex);
                }
            }
            HighlightedComponentFields.Clear();
            foreach (SelectedField row in selectedItems)
            {
                HighlightedComponentFields.Add(row);
            }
        }

        #endregion

        #region OkCommand

        private RelayCommand _OkCommand;
        /// <summary>
        ///  Applies and closes the window.
        /// </summary>
        public RelayCommand OkCommand
        {
            get
            {
                if (_OkCommand == null)
                {
                    _OkCommand = new RelayCommand
                    (p => Ok_Executed())
                    {
                      FriendlyName = Message.Generic_OK,
                      DisabledReason = Message.Generic_Ok_DisabledReasonNoChanges
                    };
                    RegisterCommand(_OkCommand);
                }
                return _OkCommand;
            }
        }
        private void Ok_Executed()
        {
            this.DialogResult = true;
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;
        /// <summary>
        /// Closes the window
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        p => Close_Executed())
                        {
                           FriendlyName = Message.Generic_Cancel,
                           //FriendlyDescription = CommonMessage.ColumnLayoutEditor_CloseWindow_Description,
                           Icon = CommonImageResources.Close_16,
                           SmallIcon = CommonImageResources.Close_16
                        };
                    RegisterCommand(_closeCommand);
                }
                return _closeCommand;
            }
        }
        private void Close_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #endregion

        #region Methods

        #region Remove selected Product Field
        private void RemoveProductField()
        {
            if (HighlightedProductFields != null)
            {
                foreach (SelectedField row in _highlightedProductFields.ToList())
                {
                    if (row.IsLocked == false)
                    {
                        _selectedProductFields.Remove(row);
                    }
                    else
                    {
                        continue;
                    }
                }
                _highlightedProductFields.Clear();
            }
        }

        #endregion

        #region Remove selected component field
        private void RemoveComponentField()
        {
            if (HighlightedComponentFields != null)
            {
                foreach (SelectedField field in _highlightedComponentFields.ToList())
                {
                    if (field.IsLocked == false)
                    {
                        _selectedComponentFields.Remove(field);
                    }
                    else
                    {
                        continue;
                    }
                }
                _highlightedComponentFields.Clear();
            }
        }

        #endregion

        #region Add Product Fields
        private void AddProductField()
        {
            if (SelectedAvailableFields != null)
            {
                foreach (FieldGroupRows field in SelectedAvailableFields)
                {
                    SelectedField newField = new SelectedField(field.FieldInfo);
                    if (!_selectedProductFields.Any(r => r.FieldFriendlyName.Equals(newField.FieldFriendlyName)))
                    {
                        if (newField.FieldFriendlyName.Equals("Component Name"))
                        {
                            newField.ColumnType = SearchColumnType.Product;
                            newField.IsLocked = false;
                            _selectedProductFields.Add(newField);
                        }
                        else
                        {
                            newField.ColumnType = SearchColumnType.Product;
                            _selectedProductFields.Add(newField);
                        }
                    }
                }
            }
        }
        #endregion

        #region Add Component Fields

        private void AddComponentField()
        {
            if (SelectedAvailableFields != null)
            {
                foreach (FieldGroupRows field in SelectedAvailableFields)
                {
                    SelectedField newField = new SelectedField(field.FieldInfo);
                    if (!_selectedComponentFields.Any(r => r.FieldFriendlyName.Equals(newField.FieldFriendlyName)))
                    {
                        if (newField.FieldFriendlyName.Equals("[PlanogramProduct.Name]") ||
                            newField.FieldFriendlyName.Equals("Product GTIN") ||
                            newField.FieldFriendlyName.Equals("Product Brand"))
                        {
                            newField.ColumnType = SearchColumnType.Component;
                            newField.IsLocked = false;
                            _selectedComponentFields.Add(newField);
                        }
                        else
                        {
                            newField.ColumnType = SearchColumnType.Component;
                            _selectedComponentFields.Add(newField);
                        }
                    }
                }
            }
        }
        #endregion
      
        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when the selected item type property value changes.
        /// </summary>
        private void
        OnSelectedFieldGroupChanged(FieldGroup newValue)
        {
            _selectedGroupOfFields.Clear();
            _selectedGroupOfFields.AddRange(_masterFields.Where
                (f => newValue.IsAllGroup || newValue.GroupName == f.GroupName));
        }

        #endregion

        #region IDisposable Support

        protected override void Dispose(Boolean disposing)
        {
            if (IsDisposed) return;
            if (disposing)
            {
                IsDisposed = true;
            }
        }

        #endregion

    }
}
