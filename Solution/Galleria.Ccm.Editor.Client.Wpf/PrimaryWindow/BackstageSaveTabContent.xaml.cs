﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27496 : L.Luong
//    Created
// V8-28474 : L.Ineson
//  Added IsActive property and moved save function set into the viewmodel.
#endregion

#region Version History: CCM820

// V8-30620 : A.Silva
//  Subscribed to the View Model's Property Changed event to handle changes to the Selected Group property. 
//      Now the corresponsding Product Group will be scrolled into view if possible.

#endregion

#region Version History: (CCM 8.3.0)
//V8-32185 : L.Ineson
//  Added the ability to create and edit planogram groups.
#endregion


#endregion

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for BackstageSaveTabContent.xaml
    /// </summary>
    public sealed partial class BackstageSaveTabContent
    {
        #region Fields

        private Int32? _lastProductGroupIdScrolledIntoView;

        #endregion

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(BackstageSaveTabViewModel), typeof(BackstageSaveTabContent),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            //  Validate the sender.
            var senderControl = obj as BackstageSaveTabContent;
            if (senderControl == null) return;

            //  Unsubscribe from the old model, if there was one.
            var oldModel = e.OldValue as BackstageSaveTabViewModel;
            if (oldModel != null)
            {
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModelOnPropertyChanged;
            }
            
            //  Subscribe to the new model, if there is one.
            var newModel = e.NewValue as BackstageSaveTabViewModel;
            if (newModel == null) return;

            newModel.AttachedControl = senderControl;
            newModel.PropertyChanged += senderControl.ViewModelOnPropertyChanged;
        }

        /// <summary>
        /// Gets/Sets the viewmodel context.
        /// </summary>
        public BackstageSaveTabViewModel ViewModel
        {
            get { return (BackstageSaveTabViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region IsActiveProperty

        public static readonly DependencyProperty IsActiveProperty
            = DependencyProperty.Register("IsActive", typeof(Boolean), typeof(BackstageSaveTabContent),
            new PropertyMetadata(false, OnIsActivePropertyChanged));

        private static void OnIsActivePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BackstageSaveTabContent senderControl = (BackstageSaveTabContent)obj;
            if (senderControl.ViewModel != null)
            {
                senderControl.ViewModel.IsActive = (Boolean)e.NewValue;
            }
        }

        /// <summary>
        /// Gets/Sets whether this tab is active.
        /// </summary>
        public Boolean IsActive
        {
            get { return (Boolean)GetValue(IsActiveProperty); }
            set { SetValue(IsActiveProperty, value); }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public BackstageSaveTabContent()
        {
            Console.WriteLine("BackstageSaveTabContent");
            InitializeComponent();

            this.IsVisibleChanged += BackstageSaveTabContent_IsVisibleChanged;
            //this.ViewModel = new BackstageSaveTabViewModel();
        }


        #endregion

        #region Event Handlers

        private void BackstageSaveTabContent_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.IsVisible && this.ViewModel == null)
            {
                this.IsVisibleChanged -= BackstageSaveTabContent_IsVisibleChanged;

                this.ViewModel = new BackstageSaveTabViewModel();
            }
        }


        /// <summary>
        ///     Invoked whenever a <c>Property</c> changes in the currently associated <c>ViewModel</c>.
        /// </summary>
        private void ViewModelOnPropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            //  Check the sender.
            var source = sender as BackstageSaveTabViewModel;
            if (source == null) return;

            //  Handle changes to SelectedGroup.
            if (String.Equals(e.PropertyName, BackstageSaveTabViewModel.SelectedGroupProperty.Path))
            {
                //  Check there is a selection, and that it is a new selection, before proceeding.
                if (source.SelectedGroup == null ||
                    source.SelectedGroup.ProductGroupId == _lastProductGroupIdScrolledIntoView) return;
                _lastProductGroupIdScrolledIntoView = source.SelectedGroup.ProductGroupId;

                xSaveSelector.ScrollSelectionIntoView(source.SelectedGroup);
            }
        }

        /// <summary>
        /// Called whenever the user right clicks on a repository folder
        /// </summary>
        private void xSaveSelector_PlanogramGroupContextMenuShowing(object sender, Ccm.Common.Wpf.Selectors.PlanogramHierarchyContextEventArgs e)
        {
            e.ContextMenu.Items.Add(new Separator());


            e.ContextMenu.Items.Add(new Fluent.MenuItem()
            {
                Command = this.ViewModel.NewPlanogramGroupCommand,
                Header = this.ViewModel.NewPlanogramGroupCommand.FriendlyName,
                Icon = new Image { Height = 16, Width = 16, Source=this.ViewModel.NewPlanogramGroupCommand.SmallIcon }
            });

            e.ContextMenu.Items.Add(new Fluent.MenuItem()
            {
                Command = this.ViewModel.EditPlanogramGroupCommand,
                Header = this.ViewModel.EditPlanogramGroupCommand.FriendlyName,
                Icon = new Image { Height = 16, Width = 16, Source = this.ViewModel.EditPlanogramGroupCommand.SmallIcon }
            });

        }

        #endregion

        
    }
}