﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24265 : J.Pickup
//  Label Settings fetchall() uses new VM wrapper.
// V8-26364 : L.Ineson
//  Removed references to custom view layouts
// V8-27154 : L.Luong
//  Added ProductPlacement X,Y,Z
// V8-27239 : I.George
//  Added Manipulation to OptionsWindowTab Enum
// V8-27654 : L.Ineson
//  Amended SetLocation command to allow more values
// V8-27938 : N.Haywood
//  Added data sheets
// V8-29054 : M.Pettit
//  Added Validation tab
#endregion

#region Version History: (CCM 8.0.1)

// V8-28676 : A.Silva
//      Added ProductImages.
// V8-28103 : M.Shelley
//  Modified ".." to localised message label

#endregion

#region Version History: (CCM 8.1.1)
// V8-29571 : L.Luong
//      Changed Close button to Apply and cancel buttons
// V8-29406 : N.Haywood
//  Added file type validation for OpenFileDialog
#endregion
#region Version History: (CCM 8.2.0)
// V8-30705 : A.Probyn
//  Updated to support assortment validation warnings
// V8-30725 : M.Brumby
//  PCR01417 Import from Apollo
//V8-30775 : L.Ineson
//  Swapped over available display language to en-us.
// V8-31430 : D.Pleasance
//  Added flags to to determine property changes on the model.
// V8-31533 : A.Probyn
//  Added defensive code to field selectors SelectedField value
#endregion
#region Version History: (CCM830)
// V8-31546 : M.Pettit
//  Added Spaceman,Apollo,JDA Export Templates
//  Refactored PlanogramFileType to Framework.Plangoram.Model and renamed PlanogramExportFileType
// V8-32111 : A.Silva
//  Added PlanogramComparisonTemplateLocation.
//V8-32361 : L.Ineson
//  Added DefaultProductUniverseTemplate
// V8-32733 : A.Silva
//  Added DefaultPlanogramComparisonTemplate.

#endregion

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Windows;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using CommonImageResources = Galleria.Ccm.Common.Wpf.Resources.ImageResources;
using CommonMessage = Galleria.Ccm.Common.Wpf.Resources.Language.Message;
using Message = Galleria.Ccm.Editor.Client.Wpf.Resources.Language.Message;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
using System.IO;
using System.Windows.Media;

namespace Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// ViewModel controller for OptionsWindow
    /// </summary>
    public sealed class OptionsViewModel : ViewModelAttachedControlObject<OptionsWindow>
    {
        #region Nested Classes

        /// <summary>
        /// Denotes the available tabs in the options window
        /// </summary>
        public enum OptionsWindowTab
        {
            General = 0,
            Defaults = 1,
            Display = 2,
            PositionProperties = 3,
            ComponentProperties = 4,
            Manipulation = 5,
            ProductImages = 6,
            Validation = 7
        }

        /// <summary>
        /// A simple selection class 
        /// </summary>
        public sealed class SelectionItem
        {
            public String DisplayName { get; private set; }
            public Object Value { get; private set; }

            public SelectionItem(String name, Object val)
            {
                DisplayName = name;
                Value = val;
            }

            /// <summary>
            /// Returns a null selection item.
            /// </summary>
            public static SelectionItem None
            {
                get { return new SelectionItem(Message.Generic_None, String.Empty); }
            }
        }

        /// <summary>
        /// Selection class used to populate the position properties
        /// and component propeties values.
        /// </summary>
        public sealed class AssignedFieldItem
        {
            public String DisplayName
            {
                get { return (Field != null) ? Field.FieldFriendlyName : null; }
            }

            public ObjectFieldInfo Field { get; private set; }
            public Int32 Order { get; set; }

            public AssignedFieldItem(ObjectFieldInfo field, Int32 order)
            {
                Field = field;
                Order = order;
            }
        }

        #endregion

        #region Fields

        private readonly UserEditorSettingsViewModel _settingsView;

        private readonly ReadOnlyCollection<SelectionItem> _availableLanguages;
        private readonly ReadOnlyCollection<SelectionItem> _availableProductLabels;
        private readonly ReadOnlyCollection<SelectionItem> _availableFixtureLabels;
        private readonly ReadOnlyCollection<SelectionItem> _availableHighlights;

        private readonly BulkObservableCollection<ObjectFieldInfo> _availablePositionProperties = new BulkObservableCollection<ObjectFieldInfo>();
        private readonly ReadOnlyBulkObservableCollection<ObjectFieldInfo> _availablePositionPropertiesRO;

        private readonly BulkObservableCollection<AssignedFieldItem> _assignedPositionProperties = new BulkObservableCollection<AssignedFieldItem>();
        private readonly ReadOnlyBulkObservableCollection<AssignedFieldItem> _assignedPositionPropertiesRO;

        private readonly BulkObservableCollection<ObjectFieldInfo> _availableComponentProperties = new BulkObservableCollection<ObjectFieldInfo>();
        private readonly ReadOnlyBulkObservableCollection<ObjectFieldInfo> _availableComponentPropertiesRO;

        private readonly BulkObservableCollection<AssignedFieldItem> _assignedComponentProperties = new BulkObservableCollection<AssignedFieldItem>();
        private readonly ReadOnlyBulkObservableCollection<AssignedFieldItem> _assignedComponentPropertiesRO;

        private OptionsWindowTab _selectedTab = OptionsWindowTab.General;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath SettingsProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.Settings);
        public static readonly PropertyPath AvailableProductLabelsProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.AvailableProductLabels);
        public static readonly PropertyPath AvailableFixtureLabelsProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.AvailableFixtureLabels);
        public static readonly PropertyPath AvailableLanguagesProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.AvailableLanguages);
        public static readonly PropertyPath DisplayUomProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.DisplayUom);
        public static readonly PropertyPath AvailablePositionPropertiesProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.AvailablePositionProperties);
        public static readonly PropertyPath AssignedPositionPropertiesProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.AssignedPositionProperties);
        public static readonly PropertyPath AvailableComponentPropertiesProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.AvailableComponentProperties);
        public static readonly PropertyPath AssignedComponentPropertiesProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.AssignedComponentProperties);
        public static readonly PropertyPath SelectedTabProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.SelectedTab);

        //Commands
        public static readonly PropertyPath SetLocationCommandProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.SetLocationCommand);
        public static readonly PropertyPath SetComponentHoverStatusBarTextCommandProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.SetComponentHoverStatusBarTextCommand);
        public static readonly PropertyPath SetPositionHoverStatusBarTextCommandProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.SetPositionHoverStatusBarTextCommand);
        public static readonly PropertyPath AssignPositionPropertiesCommandProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.AssignPositionPropertiesCommand);
        public static readonly PropertyPath RemovePositionPropertiesCommandProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.RemovePositionPropertiesCommand);
        public static readonly PropertyPath AssignComponentPropertiesCommandProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.AssignComponentPropertiesCommand);
        public static readonly PropertyPath RemoveComponentPropertiesCommandProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.RemoveComponentPropertiesCommand);
        public static readonly PropertyPath MovePropertiesFieldUpCommandProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.MovePropertiesFieldUpCommand);
        public static readonly PropertyPath MovePropertiesFieldDownCommandProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.MovePropertiesFieldDownCommand);
        public static readonly PropertyPath SetImageProductAttributeFilterCommandProperty = GetPropertyPath(o => o.SetImageProductAttributeFilterCommand);
        public static readonly PropertyPath ApplyCommandProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.ApplyCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(p => p.CancelCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the system settings model.
        /// </summary>
        public UserEditorSettings Settings
        {
            get { return _settingsView.Model; }
        }

        /// <summary>
        /// Returns the collection of available languages
        /// </summary>
        public ReadOnlyCollection<SelectionItem> AvailableLanguages
        {
            get { return _availableLanguages; }
        }

        /// <summary>
        /// Returns the collection of available product labels
        /// </summary>
        public ReadOnlyCollection<SelectionItem> AvailableProductLabels
        {
            get { return _availableProductLabels; }
        }

        /// <summary>
        /// Returns the collection of available fixture labels.
        /// </summary>
        public ReadOnlyCollection<SelectionItem> AvailableFixtureLabels
        {
            get { return _availableFixtureLabels; }
        }

        /// <summary>
        /// Returns the collection of available highlights.
        /// </summary>
        public ReadOnlyCollection<SelectionItem> AvailableHighlights
        {
            get { return _availableHighlights; }
        }

        /// <summary>
        /// Returns the display unit of measure.
        /// </summary>
        public DisplayUnitOfMeasure DisplayUom
        {
            get
            {
                if (this.Settings != null)
                {
                    return
                        new DisplayUnitOfMeasure(
                        PlanogramLengthUnitOfMeasureTypeHelper.FriendlyNames[Settings.LengthUnitOfMeasure],
                        PlanogramLengthUnitOfMeasureTypeHelper.Abbreviations[Settings.LengthUnitOfMeasure],
                        false);

                }
                return null;
            }
        }

        /// <summary>
        /// Returns the collection of available position properties fields.
        /// </summary>
        public ReadOnlyBulkObservableCollection<ObjectFieldInfo> AvailablePositionProperties
        {
            get { return _availablePositionPropertiesRO; }
        }

        /// <summary>
        /// Returns the collection of assigned position properties
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssignedFieldItem> AssignedPositionProperties
        {
            get { return _assignedPositionPropertiesRO; }
        }

        /// <summary>
        /// Returns the collection of available component properties
        /// </summary>
        public ReadOnlyBulkObservableCollection<ObjectFieldInfo> AvailableComponentProperties
        {
            get { return _availableComponentPropertiesRO; }
        }

        /// <summary>
        /// Returns the collection of assigned component properties
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssignedFieldItem> AssignedComponentProperties
        {
            get { return _assignedComponentPropertiesRO; }
        }

        /// <summary>
        /// Gets/Sets the currently seleted tab
        /// </summary>
        public OptionsWindowTab SelectedTab
        {
            get { return _selectedTab; }
            set
            {
                _selectedTab = value;
                OnPropertyChanged(SelectedTabProperty);
            }
        }

        public Boolean HasHighlightLocationChanged { get; set; }
        public Boolean HasLabelLocationChanged { get; set; }
        public Boolean HasDataSheetFavouritesChanged { get; set; }
        public Boolean HasAutosaveChanged { get; set; }
        public Boolean HasWarningsSettingsChanged { get; set; }

        /// <summary>
        /// Returns a collection of available fonts.
        /// </summary>
        public ReadOnlyCollection<FontFamily> AvailableFonts {get; private set;}


        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        public OptionsViewModel()
            : this(App.ViewState.Settings)
        { }

        /// <summary>
        /// Testing Constructor
        /// </summary>
        /// <param name="settingsView"></param>
        public OptionsViewModel(UserEditorSettingsViewModel settingsView)
        {
            _availablePositionPropertiesRO = new ReadOnlyBulkObservableCollection<ObjectFieldInfo>(_availablePositionProperties);
            _assignedPositionPropertiesRO = new ReadOnlyBulkObservableCollection<AssignedFieldItem>(_assignedPositionProperties);
            _availableComponentPropertiesRO = new ReadOnlyBulkObservableCollection<ObjectFieldInfo>(_availableComponentProperties);
            _assignedComponentPropertiesRO = new ReadOnlyBulkObservableCollection<AssignedFieldItem>(_assignedComponentProperties);

            //refetch the settings view so that we are full up to date.
            settingsView.Fetch();

            //available languages
            //nb - old code was engb be we need to change this to en-us for now.
            if (settingsView.Model.DisplayLanguageCode == "en-GB") settingsView.Model.DisplayLanguageCode = "en-US";
            _availableLanguages = new ReadOnlyCollection<SelectionItem>(
                new SelectionItem[]
                {
                    new SelectionItem(CultureInfo.GetCultureInfo("en-US").DisplayName, "en-US")
                });


            //product labels 
            String labelsDirectory = settingsView.Model.LabelLocation;
            using(LabelInfoListViewModel productLabelInfoList = new LabelInfoListViewModel(labelsDirectory))
            {

                productLabelInfoList.FetchAllByType(LabelType.Product);
            
                _availableProductLabels = new ReadOnlyCollection<SelectionItem>(
                    new SelectionItem[] { SelectionItem.None }
                    .Union(productLabelInfoList.Model.OrderBy(i => i.Name).Select(l => new SelectionItem(l.Name, l.Name)))
                    .ToArray());
            }

            //fixture labels
            using(LabelInfoListViewModel fixtureLabelInfoList = new LabelInfoListViewModel(labelsDirectory))
            {
                fixtureLabelInfoList.FetchAllByType(LabelType.Fixture);

                _availableFixtureLabels = new ReadOnlyCollection<SelectionItem>(
                new SelectionItem[] { SelectionItem.None }
                .Union(fixtureLabelInfoList.Model.OrderBy(i => i.Name).Select(l => new SelectionItem(l.Name, l.Name)))
                .ToArray());
            }

            //highlights
            String highlightsDirectory = settingsView.Model.HighlightLocation;
            using(HighlightInfoListViewModel highlightInfoList = new HighlightInfoListViewModel(highlightsDirectory))
            {
                highlightInfoList.FetchAll();

                _availableHighlights = new ReadOnlyCollection<SelectionItem>(
                    new SelectionItem[] { SelectionItem.None }
                    .Union(highlightInfoList.Model.OrderBy(i=> i.Name).Select(l => new SelectionItem(l.Name, l.Name)))
                    .ToArray());
            }
            

            //available position properties
            _availablePositionProperties.AddRange(
                PlanogramPositionView.EnumerateDisplayableFields()
                .Union(PlanogramProductView.EnumerateDisplayableFields(null))
                .Union(PlanogramComponentView.EnumerateDisplayableFields())
                .Union(PlanogramFixtureView.EnumerateDisplayableFields())
                );
            _assignedPositionProperties.BulkCollectionChanged += AssignedPositionProperties_BulkCollectionChanged;

            //available component properties
            _availableComponentProperties.AddRange(
                PlanogramComponentView.EnumerateDisplayableFields()
                .Union(PlanogramSubComponentView.EnumerateDisplayableFields())
                .Union(PlanogramFixtureView.EnumerateDisplayableFields())
                );
            _assignedComponentProperties.BulkCollectionChanged += AssignedComponentProperties_BulkCollectionChanged;

            //available fonts
            this.AvailableFonts = Fonts.SystemFontFamilies.OrderBy(f => f.ToString()).ToList().AsReadOnly();
            

            _settingsView = settingsView;
            OnSettingsChanged(null, _settingsView.Model);
            _settingsView.ModelChanged += SettingsView_ModelChanged;
        }

        #endregion

        #region Commands

        #region SetLocation

        private RelayCommand _setLocationCommand;

        /// <summary>
        /// Sets the ocation value
        /// </summary>
        public RelayCommand SetLocationCommand
        {
            get
            {
                if (_setLocationCommand == null)
                {
                    _setLocationCommand = new RelayCommand(
                        p => SetLocation_Executed(p as String))
                        {
                            FriendlyName = Message.Generic_Ellipsis
                        };
                    base.ViewModelCommands.Add(_setLocationCommand);
                }
                return _setLocationCommand;
            }
        }

        private void SetLocation_Executed(String propertyName)
        {
            if (String.IsNullOrEmpty(propertyName)) return;

            String currentLocation = GetLocation(propertyName);

            if (this.AttachedControl != null)
            {
                var dia = new System.Windows.Forms.FolderBrowserDialog();
                dia.SelectedPath = currentLocation;
                dia.ShowNewFolderButton = true;

                if (dia.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    SetLocation(propertyName, dia.SelectedPath);
                }
            }
        }

        private String GetLocation(String propertyName)
        {
            switch (propertyName)
            {
                case "AutosaveLocation": return this.Settings.AutosaveLocation;
                case "LabelLocation": return this.Settings.LabelLocation;
                case "HighlightLocation": return this.Settings.HighlightLocation;
                case "ValidationTemplateLocation": return this.Settings.ValidationTemplateLocation;
                case "PlanogramLocation": return this.Settings.PlanogramLocation;
                case "FixtureLibraryLocation": return this.Settings.FixtureLibraryLocation;
                case "ImageLocation": return this.Settings.ImageLocation;
                case "ColumnLayoutLocation": return this.Settings.ColumnLayoutLocation;
                case "DataSheetLayoutLocation": return this.Settings.DataSheetLayoutLocation;
                case "PlanogramFileTemplateLocation": return this.Settings.PlanogramFileTemplateLocation;
                case "PlanogramExportFileTemplateLocation": return this.Settings.PlanogramExportFileTemplateLocation;
                case "ProductImageLocation": return this.Settings.ProductImageLocation;
                case "PrintTemplateLocation": return this.Settings.PrintTemplateLocation;
                case "PlanogramComparisonTemplate" : return this.Settings.PlanogramComparisonTemplateLocation;

                default:
                    Debug.Fail("Not handled");
                    return null;
            }
        }

        private void SetLocation(String propertyName, String newValue)
        {
            switch (propertyName)
            {
                case "AutosaveLocation":
                    this.Settings.AutosaveLocation = newValue;
                    break;

                case "LabelLocation":
                    this.Settings.LabelLocation = newValue;
                    break;

                case "HighlightLocation":
                    this.Settings.HighlightLocation = newValue;
                    break;

                case "ValidationTemplateLocation":
                    this.Settings.ValidationTemplateLocation = newValue;
                    break;

                case "PlanogramLocation":
                    this.Settings.PlanogramLocation = newValue;
                    break;

                case "FixtureLibraryLocation":
                    this.Settings.FixtureLibraryLocation = newValue;
                    break;

                case "ImageLocation":
                    this.Settings.ImageLocation = newValue;
                    break;

                case "ColumnLayoutLocation":
                    this.Settings.ColumnLayoutLocation = newValue;
                    break;

                case "DataSheetLayoutLocation":
                    this.Settings.DataSheetLayoutLocation = newValue;
                    break;

                case "PlanogramFileTemplateLocation":
                    this.Settings.PlanogramFileTemplateLocation = newValue;
                    break;

                case "PlanogramExportFileTemplateLocation":
                    this.Settings.PlanogramExportFileTemplateLocation = newValue;
                    break;

                case "ProductImageLocation":
                    this.Settings.ProductImageLocation = newValue;
                    break;

                case "PrintTemplateLocation":
                    this.Settings.PrintTemplateLocation = newValue;
                    break;

                case "PlanogramComparisonTemplateLocation":
                    this.Settings.PlanogramComparisonTemplateLocation = newValue;
                    break;

                default:
                    Debug.Fail("Not handled");
                    break;
            }
        }


        #endregion

        #region SetProductLibraryLocation

        private RelayCommand _setProductLibraryLocationCommand;

        /// <summary>
        /// Sets the ProductLibraryLocation value
        /// </summary>
        public RelayCommand SetProductLibraryLocationCommand
        {
            get
            {
                if (_setProductLibraryLocationCommand == null)
                {
                    _setProductLibraryLocationCommand = new RelayCommand(
                        p => SetProductLibraryLocation_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                    base.ViewModelCommands.Add(_setProductLibraryLocationCommand);
                }
                return _setProductLibraryLocationCommand;
            }
        }


        private void SetProductLibraryLocation_Executed()
        {
            if (this.AttachedControl != null)
            {
                //show the file open dialog
                OpenFileDialog dia = new OpenFileDialog();
                dia.Multiselect = false;
                dia.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                dia.Filter = Message.Options_ProductLibraryLocationFilter;

                if (dia.ShowDialog() == true)
                {
                    List<String> extensions = new List<String>(new String[]{".pogpl"});

                    if (!extensions.Any(s => String.Equals(Path.GetExtension(dia.FileName), s, StringComparison.OrdinalIgnoreCase)))
                    {
                        ModalMessage msg = new ModalMessage
                            {
                                Title = Message.General_WrongFileType_Title,
                                Header = Message.General_WrongFileType_Title,
                                MessageIcon = ImageResources.Warning_32,
                                Description = Message.General_WrongFileType_Description,
                                ButtonCount = 1,
                                Button1Content = Message.Generic_OK,
                                DefaultButton = ModalMessageButton.Button1
                            };
                        App.ShowWindow(msg, true);
                    }
                    else
                    {
                        this.Settings.DefaultProductLibrarySource = dia.FileName;
                    }
                }
            }
        }

        #endregion

        #region SetProductUniverseTemplateCommand

        private RelayCommand _setProductUniverseTemplateCommand;

        /// <summary>
        /// Show the dialog to select a print template from the repository.
        /// </summary>
        public RelayCommand SetProductUniverseTemplateCommand
        {
            get
            {
                if (_setProductUniverseTemplateCommand == null)
                {
                    _setProductUniverseTemplateCommand = new RelayCommand(
                        p => SetProductUniverseTemplate_Executed())
                    {
                        Icon = CommonImageResources.Open_16,
                        SmallIcon = CommonImageResources.Open_16,
                    };
                    base.ViewModelCommands.Add(_setProductUniverseTemplateCommand);
                }

                return _setProductUniverseTemplateCommand;
            }
        }

        private void SetProductUniverseTemplate_Executed()
        {
            if (App.ViewState.IsConnectedToRepository)
            {
                this.SetProductUniverseTemplateFromRepositoryCommand.Execute();
            }
            else
            {
                this.SetProductUniverseTemplateFromFileCommand.Execute();
            }
        }

        #endregion

        #region SetProductUniverseTemplateFromFileCommand

        private RelayCommand _setProductUniverseTemplateFromFileCommand;

        /// <summary>
        /// Show the dialog to select a print template from the repository.
        /// </summary>
        public RelayCommand SetProductUniverseTemplateFromFileCommand
        {
            get
            {
                if (_setProductUniverseTemplateFromFileCommand == null)
                {
                    _setProductUniverseTemplateFromFileCommand = new RelayCommand(
                        p => SetProductUniverseTemplateFromFile_Executed())
                    {
                        FriendlyName = Message.BackstagePrintTab_SetPrintingTemplateFromFile,
                        Icon = CommonImageResources.Open_16,
                        SmallIcon = CommonImageResources.Open_16,
                    };
                    base.ViewModelCommands.Add(_setProductUniverseTemplateFromFileCommand);
                }

                return _setProductUniverseTemplateFromFileCommand;
            }
        }

        private void SetProductUniverseTemplateFromFile_Executed()
        {
            //show the open file dialog.
            String file = null;
            if (String.IsNullOrEmpty(file))
            {
                Boolean result =
                CommonHelper.GetWindowService().ShowOpenFileDialog(
                    /*initialDir*/null,
                    String.Format(CultureInfo.InvariantCulture, 
                    Message.ProductLibraryMaintenance_ImportFilter, Model.ProductLibrary.FileExtension),
                    out file);

                if (!result) return;
            }


            this.Settings.DefaultProductUniverseTemplateId = file;
        }

        #endregion

        #region SetProductUniverseTemplateFromRepositoryCommand

        private RelayCommand _setProductUniverseTemplateFromRepositoryCommand;

        /// <summary>
        /// Show the dialog to select a print template from the repository.
        /// </summary>
        public RelayCommand SetProductUniverseTemplateFromRepositoryCommand
        {
            get
            {
                if (_setProductUniverseTemplateFromRepositoryCommand == null)
                {
                    _setProductUniverseTemplateFromRepositoryCommand = new RelayCommand(
                        p => SetProductUniverseTemplateFromRepository_Executed(),
                        p => SetProductUniverseTemplateFromRepository_CanExecute())
                    {
                        FriendlyName = Message.BackstagePrintTab_SetPrintingTemplateFromRepository,
                        Icon = CommonImageResources.Open_16,
                        SmallIcon = CommonImageResources.Open_16,
                    };
                    base.ViewModelCommands.Add(_setProductUniverseTemplateFromRepositoryCommand);
                }

                return _setProductUniverseTemplateFromRepositoryCommand;
            }
        }

        private Boolean SetProductUniverseTemplateFromRepository_CanExecute()
        {
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.SetProductUniverseTemplateFromRepositoryCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }
            return true;
        }

        private void SetProductUniverseTemplateFromRepository_Executed()
        {
            ProductLibraryInfoList productUniverseTemplateInfos;
            try
            {
                productUniverseTemplateInfos = ProductLibraryInfoList.FetchByEntityId(App.ViewState.EntityId);
            }
            catch (DataPortalException ex)
            {
                CommonHelper.RecordException(ex);
                return;
            }


            GenericSingleItemSelectorWindow win = new GenericSingleItemSelectorWindow();
            win.SelectionMode = System.Windows.Controls.DataGridSelectionMode.Single;
            win.ItemSource = productUniverseTemplateInfos;

            CommonHelper.GetWindowService().ShowDialog<GenericSingleItemSelectorWindow>(win);
            if (win.DialogResult != true) return;

            ProductLibraryInfo selectedInfo = win.SelectedItems.Cast<ProductLibraryInfo>().FirstOrDefault();
            if (selectedInfo == null) return;

            //save the name instead of the id so we can just look for it in the repository.
            this.Settings.DefaultProductUniverseTemplateId = selectedInfo.Name;
        }

        #endregion

        #region SetPlanogramComparisonTemplateFromFileCommand

        private RelayCommand _setPlanogramComparisonTemplateFromFileCommand;

        /// <summary>
        /// Show the dialog to select a planogram comparison template from the repository.
        /// </summary>
        public RelayCommand SetPlanogramComparisonTemplateFromFileCommand
        {
            get
            {
                if (_setPlanogramComparisonTemplateFromFileCommand != null) return _setPlanogramComparisonTemplateFromFileCommand;

                _setPlanogramComparisonTemplateFromFileCommand =
                    new RelayCommand(p => SetPlanogramComparisonTemplateFromFile_Executed())
                    {
                        FriendlyName = Message.BackstagePrintTab_SetPrintingTemplateFromFile,
                        Icon = CommonImageResources.Open_16,
                        SmallIcon = CommonImageResources.Open_16,
                    };
                ViewModelCommands.Add(_setPlanogramComparisonTemplateFromFileCommand);

                return _setPlanogramComparisonTemplateFromFileCommand;
            }
        }

        private void SetPlanogramComparisonTemplateFromFile_Executed()
        {
            String file;
            if (!CommonHelper.GetWindowService()
                             .ShowOpenFileDialog(
                                 App.ViewState.GetSessionDirectory(SessionDirectory.PlanogramComparisonTemplate),
                                 String.Format(CommonMessage.PlanogramComparisonFilePromptMask, PlanogramComparisonTemplate.FileExtension),
                                 out file))
                return;

            Settings.DefaultPlanogramComparisonTemplateId = file;
        }

        #endregion

        #region SetPlanogramComparisonTemplateFromRepositoryCommand

        private RelayCommand _setPlanogramComparisonTemplateFromRepositoryCommand;

        /// <summary>
        /// Show the dialog to select a planogram comparison template from the repository.
        /// </summary>
        public RelayCommand SetPlanogramComparisonTemplateFromRepositoryCommand
        {
            get
            {
                if (_setPlanogramComparisonTemplateFromRepositoryCommand != null) return _setPlanogramComparisonTemplateFromRepositoryCommand;

                _setPlanogramComparisonTemplateFromRepositoryCommand =
                    new RelayCommand(p => SetPlanogramComparisonTemplateFromRepository_Executed(),
                                     p => SetPlanogramComparisonTemplateFromRepository_CanExecute())
                    {
                        FriendlyName = Message.BackstagePrintTab_SetPrintingTemplateFromRepository,
                        Icon = CommonImageResources.Open_16,
                        SmallIcon = CommonImageResources.Open_16,
                    };
                ViewModelCommands.Add(_setPlanogramComparisonTemplateFromRepositoryCommand);

                return _setPlanogramComparisonTemplateFromRepositoryCommand;
            }
        }

        private Boolean SetPlanogramComparisonTemplateFromRepository_CanExecute()
        {
            if (!App.ViewState.IsConnectedToRepository)
            {
                SetPlanogramComparisonTemplateFromRepositoryCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }

            return true;
        }

        private void SetPlanogramComparisonTemplateFromRepository_Executed()
        {
            PlanogramComparisonTemplateInfoList infos;
            try
            {
                infos = PlanogramComparisonTemplateInfoList.FetchByEntityId(App.ViewState.EntityId);
            }
            catch (DataPortalException ex)
            {
                CommonHelper.RecordException(ex);
                return;
            }

            var win = new GenericSingleItemSelectorWindow
                      {
                          SelectionMode = System.Windows.Controls.DataGridSelectionMode.Single,
                          ItemSource = infos
                      };

            CommonHelper.GetWindowService().ShowDialog<GenericSingleItemSelectorWindow>(win);
            if (win.DialogResult != true) return;

            PlanogramComparisonTemplateInfo selectedInfo = win.SelectedItems.Cast<PlanogramComparisonTemplateInfo>().FirstOrDefault();
            if (selectedInfo == null) return;

            //  save the name instead of the id so we can just look for it in the repository.
            Settings.DefaultPlanogramComparisonTemplateId = selectedInfo.Name;
        }

        #endregion

        #region Set Default Import Templates
        #region SetDefaultSpacemanTemplateCommand

        private RelayCommand _setDefaultSpacemanTemplateCommand;

        /// <summary>
        /// Sets the DefaultSpacemanTemplate value
        /// </summary>
        public RelayCommand SetDefaultSpacemanTemplateCommand
        {
            get
            {
                if (_setDefaultSpacemanTemplateCommand == null)
                {
                    _setDefaultSpacemanTemplateCommand = new RelayCommand(
                        p => SetDefaultSpacemanTemplate_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                    base.ViewModelCommands.Add(_setDefaultSpacemanTemplateCommand);
                }
                return _setDefaultSpacemanTemplateCommand;
            }
        }


        private void SetDefaultSpacemanTemplate_Executed()
        {
            if (this.AttachedControl != null)
            {
                //show the file open dialog
                OpenFileDialog dia = new OpenFileDialog();
                dia.Multiselect = false;
                dia.CheckFileExists = true;
                dia.InitialDirectory = App.ViewState.Settings.Model.PlanogramFileTemplateLocation;
                dia.Filter = String.Format(CultureInfo.InvariantCulture, Message.PlanogramFileTemplateEditor_ImportFilter, PlanogramImportTemplate.FileExtension);

                if (dia.ShowDialog() == true)
                {
                    ModalMessage msg = new ModalMessage
                    {
                        Title = Message.General_WrongFileType_Title,
                        Header = Message.General_WrongFileType_Title,
                        MessageIcon = ImageResources.Warning_32,
                        Description = Message.General_WrongFileType_Description,
                        ButtonCount = 1,
                        Button1Content = Message.Generic_OK,
                        DefaultButton = ModalMessageButton.Button1
                    };

                    if(!String.Equals(Path.GetExtension(dia.FileName), PlanogramImportTemplate.FileExtension, StringComparison.OrdinalIgnoreCase))
                    {                        
                        App.ShowWindow(msg, true);
                    }
                    else
                    {
                        Boolean valid = false;
                        try
                        {
                            PlanogramImportTemplate template = PlanogramImportTemplate.FetchByFilename(dia.FileName);

                            if (template.FileType == PlanogramImportFileType.SpacemanV9)
                            {                                
                                valid = true;
                            }
                        }
                        finally
                        {

                            if (valid)
                            {
                                this.Settings.DefaultSpacemanFileTemplate = dia.FileName;
                            }
                            else
                            {
                                App.ShowWindow(msg, true);
                            }
                        }
                        
                    }
                }
            }
        }

        #endregion

        #region SetDefaultApolloTemplateCommand

        private RelayCommand _setDefaultApolloTemplateCommand;

        /// <summary>
        /// Sets the DefaultApolloTemplate value
        /// </summary>
        public RelayCommand SetDefaultApolloTemplateCommand
        {
            get
            {
                if (_setDefaultApolloTemplateCommand == null)
                {
                    _setDefaultApolloTemplateCommand = new RelayCommand(
                        p => SetDefaultApolloTemplate_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                    base.ViewModelCommands.Add(_setDefaultApolloTemplateCommand);
                }
                return _setDefaultApolloTemplateCommand;
            }
        }


        private void SetDefaultApolloTemplate_Executed()
        {
            if (this.AttachedControl != null)
            {
                //show the file open dialog
                OpenFileDialog dia = new OpenFileDialog();
                dia.Multiselect = false;
                dia.CheckFileExists = true;
                dia.InitialDirectory = App.ViewState.Settings.Model.PlanogramFileTemplateLocation;
                dia.Filter = String.Format(CultureInfo.InvariantCulture, Message.PlanogramFileTemplateEditor_ImportFilter, PlanogramImportTemplate.FileExtension);

                if (dia.ShowDialog() == true)
                {
                    ModalMessage msg = new ModalMessage
                    {
                        Title = Message.General_WrongFileType_Title,
                        Header = Message.General_WrongFileType_Title,
                        MessageIcon = ImageResources.Warning_32,
                        Description = Message.General_WrongFileType_Description,
                        ButtonCount = 1,
                        Button1Content = Message.Generic_OK,
                        DefaultButton = ModalMessageButton.Button1
                    };

                    if (!String.Equals(Path.GetExtension(dia.FileName), PlanogramImportTemplate.FileExtension, StringComparison.OrdinalIgnoreCase))
                    {
                        App.ShowWindow(msg, true);
                    }
                    else
                    {
                        Boolean valid = false;
                        try
                        {
                            PlanogramImportTemplate template = PlanogramImportTemplate.FetchByFilename(dia.FileName);

                            if (template.FileType == PlanogramImportFileType.Apollo)
                            {
                                valid = true;
                            }
                        }
                        finally
                        {

                            if (valid)
                            {
                                this.Settings.DefaultApolloFileTemplate = dia.FileName;
                            }
                            else
                            {
                                App.ShowWindow(msg, true);
                            }
                        }

                    }
                }
            }
        }

        #endregion

        #region SetDefaultProspaceTemplateCommand

        private RelayCommand _setDefaultProspaceTemplateCommand;

        /// <summary>
        /// Sets the DefaultProspaceTemplate value
        /// </summary>
        public RelayCommand SetDefaultProspaceTemplateCommand
        {
            get
            {
                if (_setDefaultProspaceTemplateCommand == null)
                {
                    _setDefaultProspaceTemplateCommand = new RelayCommand(
                        p => SetDefaultProspaceTemplate_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                    base.ViewModelCommands.Add(_setDefaultProspaceTemplateCommand);
                }
                return _setDefaultProspaceTemplateCommand;
            }
        }


        private void SetDefaultProspaceTemplate_Executed()
        {
            if (this.AttachedControl != null)
            {
                //show the file open dialog
                OpenFileDialog dia = new OpenFileDialog();
                dia.Multiselect = false;
                dia.CheckFileExists = true;
                dia.InitialDirectory = App.ViewState.Settings.Model.PlanogramFileTemplateLocation;
                dia.Filter = String.Format(CultureInfo.InvariantCulture, Message.PlanogramFileTemplateEditor_ImportFilter, PlanogramImportTemplate.FileExtension);

                if (dia.ShowDialog() == true)
                {
                    ModalMessage msg = new ModalMessage
                    {
                        Title = Message.General_WrongFileType_Title,
                        Header = Message.General_WrongFileType_Title,
                        MessageIcon = ImageResources.Warning_32,
                        Description = Message.General_WrongFileType_Description,
                        ButtonCount = 1,
                        Button1Content = Message.Generic_OK,
                        DefaultButton = ModalMessageButton.Button1
                    };

                    if (!String.Equals(Path.GetExtension(dia.FileName), PlanogramImportTemplate.FileExtension, StringComparison.OrdinalIgnoreCase))
                    {
                        App.ShowWindow(msg, true);
                    }
                    else
                    {
                        Boolean valid = false;
                        try
                        {
                            PlanogramImportTemplate template = PlanogramImportTemplate.FetchByFilename(dia.FileName);

                            if (template.FileType == PlanogramImportFileType.ProSpace)
                            {
                                valid = true;
                            }
                        }
                        finally
                        {

                            if (valid)
                            {
                                this.Settings.DefaultProspaceFileTemplate = dia.FileName;
                            }
                            else
                            {
                                App.ShowWindow(msg, true);
                            }
                        }

                    }
                }
            }
        }

        #endregion

        #endregion

        #region Set Default Export Templates
        #region SetDefaultExportSpacemanTemplateCommand

        private RelayCommand _setDefaultExportSpacemanTemplateCommand;

        /// <summary>
        /// Sets the DefaultExportSpacemanTemplate value
        /// </summary>
        public RelayCommand SetDefaultExportSpacemanTemplateCommand
        {
            get
            {
                if (_setDefaultExportSpacemanTemplateCommand == null)
                {
                    _setDefaultExportSpacemanTemplateCommand = new RelayCommand(
                        p => SetDefaultExportSpacemanTemplate_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                    base.ViewModelCommands.Add(_setDefaultExportSpacemanTemplateCommand);
                }
                return _setDefaultExportSpacemanTemplateCommand;
            }
        }

        private void SetDefaultExportSpacemanTemplate_Executed()
        {
            if (this.AttachedControl != null)
            {
                //show the file open dialog
                OpenFileDialog dia = new OpenFileDialog();
                dia.Multiselect = false;
                dia.CheckFileExists = true;
                dia.InitialDirectory = App.ViewState.Settings.Model.PlanogramExportFileTemplateLocation;
                dia.Filter = String.Format(CultureInfo.InvariantCulture, Message.PlanogramFileTemplateEditor_ExportFilter, PlanogramExportTemplate.FileExtension);

                if (dia.ShowDialog() == true)
                {
                    ModalMessage msg = new ModalMessage
                    {
                        Title = Message.General_WrongFileType_Title,
                        Header = Message.General_WrongFileType_Title,
                        MessageIcon = ImageResources.Warning_32,
                        Description = Message.General_WrongFileType_Description,
                        ButtonCount = 1,
                        Button1Content = Message.Generic_OK,
                        DefaultButton = ModalMessageButton.Button1
                    };

                    if (!String.Equals(Path.GetExtension(dia.FileName), PlanogramExportTemplate.FileExtension, StringComparison.OrdinalIgnoreCase))
                    {
                        App.ShowWindow(msg, true);
                    }
                    else
                    {
                        Boolean valid = false;
                        try
                        {
                            PlanogramExportTemplate template = PlanogramExportTemplate.FetchByFilename(dia.FileName);

                            if (template.FileType == PlanogramExportFileType.Spaceman)
                            {
                                valid = true;
                            }
                        }
                        finally
                        {

                            if (valid)
                            {
                                this.Settings.DefaultExportSpacemanFileTemplate = dia.FileName;
                            }
                            else
                            {
                                App.ShowWindow(msg, true);
                            }
                        }

                    }
                }
            }
        }

        #endregion

        #region SetDefaultExportApolloTemplateCommand

        private RelayCommand _setDefaultExportApolloTemplateCommand;

        /// <summary>
        /// Sets the DefaultExportApolloTemplate value
        /// </summary>
        public RelayCommand SetDefaultExportApolloTemplateCommand
        {
            get
            {
                if (_setDefaultExportApolloTemplateCommand == null)
                {
                    _setDefaultExportApolloTemplateCommand = new RelayCommand(
                        p => SetDefaultExportApolloTemplate_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                    base.ViewModelCommands.Add(_setDefaultExportApolloTemplateCommand);
                }
                return _setDefaultExportApolloTemplateCommand;
            }
        }

        private void SetDefaultExportApolloTemplate_Executed()
        {
            if (this.AttachedControl != null)
            {
                //show the file open dialog
                OpenFileDialog dia = new OpenFileDialog();
                dia.Multiselect = false;
                dia.CheckFileExists = true;
                dia.InitialDirectory = App.ViewState.Settings.Model.PlanogramExportFileTemplateLocation;
                dia.Filter = String.Format(CultureInfo.InvariantCulture, Message.PlanogramFileTemplateEditor_ExportFilter, PlanogramExportTemplate.FileExtension);

                if (dia.ShowDialog() == true)
                {
                    ModalMessage msg = new ModalMessage
                    {
                        Title = Message.General_WrongFileType_Title,
                        Header = Message.General_WrongFileType_Title,
                        MessageIcon = ImageResources.Warning_32,
                        Description = Message.General_WrongFileType_Description,
                        ButtonCount = 1,
                        Button1Content = Message.Generic_OK,
                        DefaultButton = ModalMessageButton.Button1
                    };

                    if (!String.Equals(Path.GetExtension(dia.FileName), PlanogramExportTemplate.FileExtension, StringComparison.OrdinalIgnoreCase))
                    {
                        App.ShowWindow(msg, true);
                    }
                    else
                    {
                        Boolean valid = false;
                        try
                        {
                            PlanogramExportTemplate template = PlanogramExportTemplate.FetchByFilename(dia.FileName);

                            if (template.FileType == PlanogramExportFileType.Apollo)
                            {
                                valid = true;
                            }
                        }
                        finally
                        {

                            if (valid)
                            {
                                this.Settings.DefaultExportApolloFileTemplate = dia.FileName;
                            }
                            else
                            {
                                App.ShowWindow(msg, true);
                            }
                        }
                    }
                }
            }
        }

        #endregion


        #region SetDefaultExportJDATemplateCommand

        private RelayCommand _setDefaultExportJDATemplateCommand;

        /// <summary>
        /// Sets the DefaultExportJDATemplate value
        /// </summary>
        public RelayCommand SetDefaultExportJDATemplateCommand
        {
            get
            {
                if (_setDefaultExportJDATemplateCommand == null)
                {
                    _setDefaultExportJDATemplateCommand = new RelayCommand(
                        p => SetDefaultExportJDATemplate_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                    base.ViewModelCommands.Add(_setDefaultExportJDATemplateCommand);
                }
                return _setDefaultExportJDATemplateCommand;
            }
        }


        private void SetDefaultExportJDATemplate_Executed()
        {
            if (this.AttachedControl != null)
            {
                //show the file open dialog
                OpenFileDialog dia = new OpenFileDialog();
                dia.Multiselect = false;
                dia.CheckFileExists = true;
                dia.InitialDirectory = App.ViewState.Settings.Model.PlanogramExportFileTemplateLocation;
                dia.Filter = 
                    String.Format(CultureInfo.InvariantCulture, 
                    Message.PlanogramFileTemplateEditor_ExportFilter, 
                    PlanogramExportTemplate.FileExtension);

                if (dia.ShowDialog() == true)
                {
                    ModalMessage msg = new ModalMessage
                    {
                        Title = Message.General_WrongFileType_Title,
                        Header = Message.General_WrongFileType_Title,
                        MessageIcon = ImageResources.Warning_32,
                        Description = Message.General_WrongFileType_Description,
                        ButtonCount = 1,
                        Button1Content = Message.Generic_OK,
                        DefaultButton = ModalMessageButton.Button1
                    };

                    if (!String.Equals(Path.GetExtension(dia.FileName), PlanogramExportTemplate.FileExtension, StringComparison.OrdinalIgnoreCase))
                    {
                        App.ShowWindow(msg, true);
                    }
                    else
                    {
                        Boolean valid = false;
                        try
                        {
                            PlanogramExportTemplate template = PlanogramExportTemplate.FetchByFilename(dia.FileName);

                            if (template.FileType == PlanogramExportFileType.JDA)
                            {
                                valid = true;
                            }
                        }
                        finally
                        {

                            if (valid)
                            {
                                this.Settings.DefaultExportJDAFileTemplate = dia.FileName;
                            }
                            else
                            {
                                App.ShowWindow(msg, true);
                            }
                        }

                    }
                }
            }
        }

        #endregion

        #endregion

        #region SetComponentHoverStatusBarText

        private RelayCommand _setComponentHoverStatusBarTextCommand;

        /// <summary>
        /// Sets the ComponentHoverStatusBarText command
        /// </summary>
        public RelayCommand SetComponentHoverStatusBarTextCommand
        {
            get
            {
                if (_setComponentHoverStatusBarTextCommand == null)
                {
                    _setComponentHoverStatusBarTextCommand = new RelayCommand(
                        p => SetComponentHoverStatusBarText_Executed())
                        {
                            FriendlyName = Message.Generic_Ellipsis
                        };
                    base.ViewModelCommands.Add(_setComponentHoverStatusBarTextCommand);
                }
                return _setComponentHoverStatusBarTextCommand;
            }
        }

        private void SetComponentHoverStatusBarText_Executed()
        {
            FieldSelectorViewModel fieldSelectorView =
                new FieldSelectorViewModel(
                    FieldSelectorInputType.Formula, FieldSelectorResolveType.Text,
                    this.Settings.ComponentHoverStatusBarText,
                    PlanItemHelper.GetPlanFixtureFieldSelectorGroups());

            fieldSelectorView.ShowDialog(this.AttachedControl);

            if (fieldSelectorView.DialogResult == true)
            {
                this.Settings.ComponentHoverStatusBarText = fieldSelectorView.FieldText;
            }
        }

        #endregion

        #region SetPositionHoverStatusBarText

        private RelayCommand _setPositionHoverStatusBarTextCommand;

        /// <summary>
        /// Sets the PositionHoverStatusBarText command
        /// </summary>
        public RelayCommand SetPositionHoverStatusBarTextCommand
        {
            get
            {
                if (_setPositionHoverStatusBarTextCommand == null)
                {
                    _setPositionHoverStatusBarTextCommand = new RelayCommand(
                        p => SetPositionHoverStatusBarText_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                    base.ViewModelCommands.Add(_setPositionHoverStatusBarTextCommand);
                }
                return _setPositionHoverStatusBarTextCommand;
            }
        }

        private void SetPositionHoverStatusBarText_Executed()
        {
            FieldSelectorViewModel fieldSelectorView =
                new FieldSelectorViewModel(FieldSelectorInputType.Formula,
                    FieldSelectorResolveType.Text, this.Settings.PositionHoverStatusBarText,
                    PlanItemHelper.GetPlanogramFieldSelectorGroups());


            fieldSelectorView.ShowDialog(this.AttachedControl);

            if (fieldSelectorView.DialogResult == true)
            {
                this.Settings.PositionHoverStatusBarText = fieldSelectorView.FieldText;
            }
        }

        #endregion

        #region AssignPositionPropertiesCommand

        private RelayCommand _assignPositionPropertiesCommand;

        /// <summary>
        /// Assigns the given fields to the position properties list.
        /// </summary>
        public RelayCommand AssignPositionPropertiesCommand
        {
            get
            {
                if (_assignPositionPropertiesCommand == null)
                {
                    _assignPositionPropertiesCommand = new RelayCommand(
                        p => AssignPositionProperties_Executed(p),
                        p => AssignPositionProperties_CanExecute(p))
                        {
                            FriendlyName = Message.Options_AssignPropertiesFields,
                            SmallIcon = ImageResources.Options_AssignProperties,
                        };
                    base.ViewModelCommands.Add(_assignPositionPropertiesCommand);
                }
                return _assignPositionPropertiesCommand;
            }
        }

        private Boolean AssignPositionProperties_CanExecute(Object args)
        {
            IList fields = args as IList;

            if (fields == null)
            {
                return false;
            }

            if (fields.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void AssignPositionProperties_Executed(Object args)
        {
            Int32 order = (this.AssignedPositionProperties.Count > 0) ? this.AssignedPositionProperties.Max(a => a.Order) : 0;
            order++;

            var fields = (IList)args;

            List<AssignedFieldItem> fieldItems = new List<AssignedFieldItem>(fields.Count);
            foreach (ObjectFieldInfo field in fields)
            {
                fieldItems.Add(new AssignedFieldItem(field, order));
                order++;
            }
            _assignedPositionProperties.AddRange(fieldItems);
        }

        #endregion

        #region RemovePositionPropertiesCommand

        private RelayCommand _removePositionPropertiesCommand;

        /// <summary>
        /// Removes the given fields to the position properties list.
        /// </summary>
        public RelayCommand RemovePositionPropertiesCommand
        {
            get
            {
                if (_removePositionPropertiesCommand == null)
                {
                    _removePositionPropertiesCommand = new RelayCommand(
                        p => RemovePositionProperties_Executed(p),
                        p => RemovePositionProperties_CanExecute(p))
                    {
                        FriendlyName = Message.Options_RemovePropertiesFields,
                        SmallIcon = ImageResources.Options_RemoveProperties,
                    };
                    base.ViewModelCommands.Add(_removePositionPropertiesCommand);
                }
                return _removePositionPropertiesCommand;
            }
        }

        private Boolean RemovePositionProperties_CanExecute(Object args)
        {
            IList fields = args as IList;

            if (fields == null)
            {
                return false;
            }

            if (fields.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void RemovePositionProperties_Executed(Object args)
        {
            _assignedPositionProperties.RemoveRange(((IList)args).Cast<AssignedFieldItem>());
        }

        #endregion

        #region AssignComponentPropertiesCommand

        private RelayCommand _assignComponentPropertiesCommand;

        /// <summary>
        /// Assigns the given fields to the position properties list.
        /// </summary>
        public RelayCommand AssignComponentPropertiesCommand
        {
            get
            {
                if (_assignComponentPropertiesCommand == null)
                {
                    _assignComponentPropertiesCommand = new RelayCommand(
                        p => AssignComponentProperties_Executed(p),
                        p => AssignComponentProperties_CanExecute(p))
                    {
                        FriendlyName = Message.Options_AssignPropertiesFields,
                        SmallIcon = ImageResources.Options_AssignProperties,
                    };
                    base.ViewModelCommands.Add(_assignComponentPropertiesCommand);
                }
                return _assignComponentPropertiesCommand;
            }
        }

        private Boolean AssignComponentProperties_CanExecute(Object args)
        {
            IList fields = args as IList;

            if (fields == null)
            {
                return false;
            }

            if (fields.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void AssignComponentProperties_Executed(Object args)
        {
            Int32 order = (this.AssignedComponentProperties.Count > 0) ? this.AssignedComponentProperties.Max(a => a.Order) : 0;
            order++;

            var fields = (IList)args;

            List<AssignedFieldItem> fieldItems = new List<AssignedFieldItem>(fields.Count);
            foreach (ObjectFieldInfo field in fields)
            {
                fieldItems.Add(new AssignedFieldItem(field, order));
                order++;
            }
            _assignedComponentProperties.AddRange(fieldItems);
        }

        #endregion

        #region RemoveComponentPropertiesCommand

        private RelayCommand _removeComponentPropertiesCommand;

        /// <summary>
        /// Removes the given fields to the position properties list.
        /// </summary>
        public RelayCommand RemoveComponentPropertiesCommand
        {
            get
            {
                if (_removeComponentPropertiesCommand == null)
                {
                    _removeComponentPropertiesCommand = new RelayCommand(
                        p => RemoveComponentProperties_Executed(p),
                        p => RemoveComponentProperties_CanExecute(p))
                    {
                        FriendlyName = Message.Options_RemovePropertiesFields,
                        SmallIcon = ImageResources.Options_RemoveProperties,
                    };
                    base.ViewModelCommands.Add(_removeComponentPropertiesCommand);
                }
                return _removeComponentPropertiesCommand;
            }
        }

        private Boolean RemoveComponentProperties_CanExecute(Object args)
        {
            IList fields = args as IList;

            if (fields == null)
            {
                return false;
            }

            if (fields.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void RemoveComponentProperties_Executed(Object args)
        {
            _assignedComponentProperties.RemoveRange(((IList)args).Cast<AssignedFieldItem>());
        }

        #endregion

        #region MovePropertiesFieldUpCommand

        private RelayCommand _movePropertiesFieldUpCommand;

        /// <summary>
        /// Move the selected properties field up
        /// </summary>
        public RelayCommand MovePropertiesFieldUpCommand
        {
            get
            {
                if (_movePropertiesFieldUpCommand == null)
                {
                    _movePropertiesFieldUpCommand = new RelayCommand(
                        p => MovePropertiesFieldUp_Executed(p),
                        p => MovePropertiesFieldUp_CanExecute(p))
                        {
                            FriendlyName = Message.Options_MovePropertiesFieldUp,
                            SmallIcon = ImageResources.Options_MovePropertiesFieldUp,
                        };
                    base.ViewModelCommands.Add(_movePropertiesFieldUpCommand);
                }
                return _movePropertiesFieldUpCommand;
            }
        }

        private Boolean MovePropertiesFieldUp_CanExecute(Object args)
        {
            IList fields = args as IList;

            if (fields == null)
            {
                return false;
            }

            if (fields.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void MovePropertiesFieldUp_Executed(Object args)
        {
            List<AssignedFieldItem> fieldToMove = ((IList)args).Cast<AssignedFieldItem>().ToList();

            List<AssignedFieldItem> newOrder;
            if (this.AssignedComponentProperties.Contains(fieldToMove.First()))
            {
                newOrder = this.AssignedComponentProperties.OrderBy(a => a.Order).ToList();

            }
            else
            {
                newOrder = this.AssignedPositionProperties.OrderBy(a => a.Order).ToList();
            }

            foreach (AssignedFieldItem moveItem in fieldToMove)
            {
                Int32 curIdx = newOrder.IndexOf(moveItem);
                if (curIdx != 0)
                {
                    newOrder.Remove(moveItem);
                    newOrder.Insert(curIdx - 1, moveItem);
                }
            }

            Int32 order = 1;
            foreach (AssignedFieldItem field in newOrder)
            {
                field.Order = order;
                order++;
            }

        }

        #endregion

        #region MovePropertiesFieldDownCommand

        private RelayCommand _movePropertiesFieldDownCommand;

        /// <summary>
        /// Move the selected properties field up
        /// </summary>
        public RelayCommand MovePropertiesFieldDownCommand
        {
            get
            {
                if (_movePropertiesFieldDownCommand == null)
                {
                    _movePropertiesFieldDownCommand = new RelayCommand(
                        p => MovePropertiesFieldDown_Executed(p),
                        p => MovePropertiesFieldDown_CanExecute(p))
                    {
                        FriendlyName = Message.Options_MovePropertiesFieldDown,
                        SmallIcon = ImageResources.Options_MovePropertiesFieldDown,
                    };
                    base.ViewModelCommands.Add(_movePropertiesFieldDownCommand);
                }
                return _movePropertiesFieldDownCommand;
            }
        }

        private Boolean MovePropertiesFieldDown_CanExecute(Object args)
        {
            IList fields = args as IList;

            if (fields == null)
            {
                return false;
            }

            if (fields.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void MovePropertiesFieldDown_Executed(Object args)
        {
            List<AssignedFieldItem> fieldToMove = ((IList)args).Cast<AssignedFieldItem>().ToList();

            List<AssignedFieldItem> newOrder;
            if (this.AssignedComponentProperties.Contains(fieldToMove.First()))
            {
                newOrder = this.AssignedComponentProperties.OrderBy(a => a.Order).ToList();

            }
            else
            {
                newOrder = this.AssignedPositionProperties.OrderBy(a => a.Order).ToList();
            }

            foreach (AssignedFieldItem moveItem in fieldToMove)
            {
                Int32 curIdx = newOrder.IndexOf(moveItem);
                if (curIdx != newOrder.Count - 1)
                {
                    newOrder.Remove(moveItem);
                    newOrder.Insert(curIdx + 1, moveItem);
                }
            }

            Int32 order = 1;
            foreach (AssignedFieldItem field in newOrder)
            {
                field.Order = order;
                order++;
            }

        }

        #endregion

        #region SetImageProductAttributeFilter
        
        private RelayCommand _setImageProductAttributeFilterCommand;

        /// <summary>
        /// Shows the field selector for the images product attribute
        /// </summary>
        public RelayCommand SetImageProductAttributeFilterCommand
        {
            get
            {
                if (_setImageProductAttributeFilterCommand != null) return _setImageProductAttributeFilterCommand;

                _setImageProductAttributeFilterCommand = new RelayCommand(o => SetImageProductAttributeFilter_Executed(), o => SetImageProductAttributeFilter_CanExecute())
                {
                    FriendlyName = Message.Generic_Ellipsis
                };
                ViewModelCommands.Add(_setImageProductAttributeFilterCommand);
                return _setImageProductAttributeFilterCommand;
            }
        }

        private Boolean SetImageProductAttributeFilter_CanExecute()
        {
            return this.Settings.ProductImageSource == RealImageProviderType.Folder;
        }

        private void SetImageProductAttributeFilter_Executed()
        {
            // Prompt the user for the product attribute to fetch images with.
            String fieldValue = String.Empty;

            //  Add the product attributes available when creating the field selector view model.
            FieldSelectorGroup[] availableFieldGroups =
            {
                new FieldSelectorGroup(Message.FieldSelectorGroup_ProductAttributes, 
                    PlanogramProduct.EnumerateDisplayableFieldInfos(/*incMetadata*/false, /*incCustom*/true, /*incPerf*/false)),
            };

            FieldSelectorViewModel viewModel = new FieldSelectorViewModel(
                       FieldSelectorInputType.SingleField, FieldSelectorResolveType.SingleValue,
                       fieldValue, availableFieldGroups);
            
            CommonHelper.GetWindowService().ShowDialog<FieldSelectorWindow>(viewModel);
            
            if (viewModel.DialogResult != true) return;
            if (viewModel.SelectedField != null)
            {
                this.Settings.ProductImageAttribute = viewModel.SelectedField.FieldPlaceholder;
            }
        }

        #endregion

        #region ApplyCommand

        private RelayCommand _applyCommand;

        /// <summary>
        /// Applies the current changes and closes the window
        /// </summary>
        public RelayCommand ApplyCommand
        {
            get
            {
                if (_applyCommand == null)
                {
                    _applyCommand = new RelayCommand
                        (p => Apply_Executed(),
                        p => Apply_CanExecute())
                    {
                        FriendlyName = Message.Generic_OK_AccessText

                    };
                    base.ViewModelCommands.Add(_applyCommand);
                }
                return _applyCommand;
            }
        }

        private Boolean Apply_CanExecute()
        {
            //If settings list or settings state hasn't changed
            if (!this.Settings.IsDirty &&
            this.Settings.PositionProperties == GetFieldString(this.AssignedPositionProperties) &&
            this.Settings.ComponentProperties == GetFieldString(this.AssignedComponentProperties))
            {
                ApplyCommand.DisabledReason = Message.Generic_Ok_DisabledReasonNoChanges;
                return false;
            }
            return true;
        }

        private void Apply_Executed()
        {
            Save();

            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Closes the attached window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                        {
                            FriendlyName = Message.Generic_Cancel_AccessText
                        };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            App.ViewState.Settings.Fetch();

            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the settings model changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingsView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<UserEditorSettings> e)
        {
            OnSettingsChanged(e.OldModel, e.NewModel);
        }

        /// <summary>
        /// Called whenever the settings property value changes
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        private void OnSettingsChanged(UserEditorSettings oldModel, UserEditorSettings newModel)
        {
            _assignedPositionProperties.Clear();
            _assignedComponentProperties.Clear();

            if (oldModel != null)
            {
                oldModel.PropertyChanged -= Settings_PropertyChanged;
            }

            if (newModel != null)
            {
                newModel.PropertyChanged += Settings_PropertyChanged;

                //Load assigned position fields
                Int32 order = 1;
                if (!String.IsNullOrEmpty(newModel.PositionProperties))
                {
                    String[] fields = newModel.PositionProperties.Split(',');
                    List<AssignedFieldItem> positionFields = new List<AssignedFieldItem>(fields.Count());

                    Dictionary<String, ObjectFieldInfo> avFields = this.AvailablePositionProperties.ToDictionary(a => a.FieldPlaceholder);
                    foreach (String field in fields)
                    {
                        ObjectFieldInfo avField;
                        if (avFields.TryGetValue(field, out avField))
                        {
                            positionFields.Add(new AssignedFieldItem(avField, order));
                            order++;
                        }
                    }
                    _assignedPositionProperties.AddRange(positionFields);
                }

                //load assigned component fields
                order = 1;
                if (!String.IsNullOrEmpty(newModel.ComponentProperties))
                {
                    String[] fields = newModel.ComponentProperties.Split(',');
                    List<AssignedFieldItem> componentFields = new List<AssignedFieldItem>(fields.Count());

                    Dictionary<String, ObjectFieldInfo> avFields = this.AvailableComponentProperties.ToDictionary(a => a.FieldPlaceholder);
                    foreach (String field in fields)
                    {
                        ObjectFieldInfo avField;
                        if (avFields.TryGetValue(field, out avField))
                        {
                            componentFields.Add(new AssignedFieldItem(avField, order));
                            order++;
                        }
                    }
                    _assignedComponentProperties.AddRange(componentFields);
                }
            }
        }

        /// <summary>
        /// Called whenever a property changes on the SystemSettings model.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Settings_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == UserEditorSettings.LengthUnitOfMeasureProperty.Name)
            {
                OnPropertyChanged(DisplayUomProperty);
            }

            if (e.PropertyName == UserEditorSettings.HighlightLocationProperty.Name)
            {
                HasHighlightLocationChanged = true;
            }
            else if (e.PropertyName == UserEditorSettings.LabelLocationProperty.Name)
            {
                HasLabelLocationChanged = true;
            }
            else if (e.PropertyName == UserEditorSettings.DataSheetFavourite1Property.Name ||
                e.PropertyName == UserEditorSettings.DataSheetFavourite2Property.Name ||
                e.PropertyName == UserEditorSettings.DataSheetFavourite3Property.Name ||
                e.PropertyName == UserEditorSettings.DataSheetFavourite4Property.Name ||
                e.PropertyName == UserEditorSettings.DataSheetFavourite5Property.Name)
            {
                HasLabelLocationChanged = true;
            }
            else if (e.PropertyName == UserEditorSettings.IsAutosaveEnabledProperty.Name ||
                e.PropertyName == UserEditorSettings.AutosaveIntervalProperty.Name)
            {
                HasAutosaveChanged = true;
            }
            else if (e.PropertyName.Contains("Warning"))
            {
                HasWarningsSettingsChanged = true;
            }
        }

        /// <summary>
        /// Called whenever the AssignedPositionProperties collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssignedPositionProperties_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    _availablePositionProperties.RemoveRange(e.ChangedItems.Cast<AssignedFieldItem>().Select(a => a.Field));
                    break;

                case NotifyCollectionChangedAction.Remove:
                case NotifyCollectionChangedAction.Reset:
                    if (e.ChangedItems != null)
                    {
                        _availablePositionProperties.AddRange(e.ChangedItems.Cast<AssignedFieldItem>().Select(a => a.Field));
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever the AssignedPositionProperties collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssignedComponentProperties_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    _availableComponentProperties.RemoveRange(e.ChangedItems.Cast<AssignedFieldItem>().Select(a => a.Field));
                    break;

                case NotifyCollectionChangedAction.Remove:
                case NotifyCollectionChangedAction.Reset:
                    if (e.ChangedItems != null)
                    {
                        _availableComponentProperties.AddRange(e.ChangedItems.Cast<AssignedFieldItem>().Select(a => a.Field));
                    }
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Saves down changes to the model
        /// </summary>
        private void Save()
        {
            base.ShowWaitCursor(true);

            //set the field list properties
            this.Settings.PositionProperties = GetFieldString(this.AssignedPositionProperties);
            this.Settings.ComponentProperties = GetFieldString(this.AssignedComponentProperties);

            try
            {
                //save changes
                _settingsView.Save();
            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);

                Exception baseException = ex.GetBaseException();

                LocalHelper.RecordException(baseException);

                Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(null, OperationType.Save);
            }

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Returns the comma separated string to save down the given fields
        /// </summary>
        /// <param name="fields"></param>
        /// <returns></returns>
        private String GetFieldString(IEnumerable<AssignedFieldItem> fields)
        {
            StringBuilder sb = new StringBuilder();

            Boolean isFirst = true;
            foreach (AssignedFieldItem field in fields.OrderBy(a => a.Order))
            {
                if (!isFirst)
                {
                    sb.Append(",");
                }

                sb.Append(field.Field.FieldPlaceholder);

                isFirst = false;
            }

            return sb.ToString();
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _settingsView.ModelChanged -= SettingsView_ModelChanged;
                    OnSettingsChanged(this.Settings, null);

                    base.IsDisposed = true;
                }
            }
        }

        #endregion

        #region Private Static Helpers

        /// <summary>
        ///     Helper method to get the property path for the <see cref="OptionsViewModel" /> property indicated by the
        ///     <paramref name="expression" />.
        /// </summary>
        /// <param name="expression">Lambda expression to access the property.</param>
        /// <returns>
        ///     Returns a <see cref="PropertyPath" /> Object with the path to the property in the
        ///     <paramref name="expression" />.
        /// </returns>
        private static PropertyPath GetPropertyPath(Expression<Func<OptionsViewModel, Object>> expression)
        {
            return WpfHelper.GetPropertyPath(expression);
        }

        #endregion
    }
}