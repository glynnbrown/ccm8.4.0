﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-XXXXX : X.XXXX
//  Created
#endregion

#region Version History: (CCM 8.1.0)
// V8-30196 : M.Pettit
//  Added Help links for keys 28 - 34
#endregion

#region Version History: (CCM 8.2.0)
// V8-31403 : A.Probyn
//  Updated help keys for new file.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf
{
    public static class HelpFileKeys
    {
        #region Helper Method
        public static void SetHelpFile(DependencyObject obj, String screenKey)
        {
            Help.SetFilename(obj, App.ViewState.HelpFilePath);

            if (!String.IsNullOrEmpty(screenKey))
            {
                Help.SetKeyword(obj, screenKey);
            }
        }
        #endregion

        //Ribbon tabs
        public const String RibbonFileTab = "1";
        public const String RibbonHomeTab = "2";
        public const String RibbonViewTab = "3";
        public const String RibbonInsertTab = "4";
        public const String RibbonContentTab = "5";
        public const String RibbonFixtureComponentTab = "6";
        public const String RibbonPositionTab = "7";
        public const String RibbonProductUniverseTab = "8";
        public const String RibbonAssortmentTab = "9";
        public const String RibbonConsumerDecisionTree = "10";
        public const String RibbonBlockingTab = "11";
        public const String RibbonDesignTab = "35";
        
        //Documents
        //
        //public const String FixtureListDocument = "";
        //public const String ProductListDocument = "";
        //public const String PlanVisualDocument = "";

        //Backstage
        public const String BackstagePrint = "12";
        public const String OptionsWindow = "14";
        public const String DatabaseConnectionWindow = "15";
        public const String BackstageHelp = "34";

        //Properties Windows
        public const String PlanogramInfo = "18";
        public const String PositionProperties = "16";
        public const String ProductProperties = "17";
        public const String ComponentProperties = "21";

        //Other windows
        public const String ComponentEditor ="13";
        public const String Highlights = "27";
        public const String ProductLabels = "26";
        public const String FixtureLabels = "25";
        public const String PlanogramValidationTemplate = "19";
        public const String PlanogramEventLog = "20";
        public const String DataSheets = "22";
        public const String CustomColumnsEditor = "23";
        public const String ImportPlanogram = "28";
        public const String AssortmentSelection = "29";
        public const String ProductRules = "30";
        public const String FamilyRules = "31";
        public const String RegionSetup = "32";
        public const String AssortmentLocalProductSetup = "33";
        public const String PrintTemplateSetup = "36";
    }
}
