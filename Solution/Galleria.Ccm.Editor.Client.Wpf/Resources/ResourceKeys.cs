﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
//    V8-25395 : A.Probyn
//        ~ Added resources for data management
// V8-24124 : A.Silva ~ Added ConverterIsTypeToVisibility, ConverterImplementsInterfaceToVisibility, ConverterImplementsInterfaceToBool
// V8-27153 : A.Silva   ~ Added ConverterEnumToBool.

#endregion

#region Version History: (CCM 8.1)

// V8-29434 : Added ConverterToPercentage

#endregion

#region Version History: CCM811

// V8-30381 : A.Silva
//  Added ConverterConditionalString

#endregion

#region Version History: CCM820
// V8-30791 : D.Pleasance
//  Added ConverterColorPickerRecentColors
#endregion
#endregion

using System;

namespace Galleria.Ccm.Editor.Client.Wpf
{
    /// <summary>
    /// Holds the resource keys for all resources 
    /// defined in the application resource dictionaries.
    /// </summary>
    public static class ResourceKeys
    {
        #region Converters

        public static readonly String ConverterAreEqual = "ConverterAreEqual";
        public static readonly String ConverterFieldTextToDisplay = "ConverterFieldTextToDisplay";
        public static readonly String ConverterRadiansToDegrees = "ConverterRadiansToDegrees";
        public static readonly String ConverterBytesToImageSource = "ConverterBytesToImageSource";
        public static readonly String ConverterUnitDisplay = "ConverterUnitDisplay";
        public static readonly String ConverterSourceToDisplayUnitOfMeasure = "ConverterSourceToDisplayUnitOfMeasure";
        public static readonly String ConverterIsTypeToVisiblity = "ConverterIsTypeToVisibility";
        public static readonly String ConverterIsType = "ConverterIsType";
        public static readonly String ConverterImplementsInterfaceToVisiblity = "ConverterImplementsInterfaceToVisibility";
        public static readonly String ConverterImplementsInterfaceToBool = "ConverterImplementsInterfaceToBool";
        public static readonly String ConverterToPercentage = "ConverterToPercentage";
        public static readonly String ConverterConditionalString = "ConverterConditionalString";
        public static readonly String ConverterColorPickerRecentColors = "ConverterColorPickerRecentColors";
        
        #endregion

        #region Res_Main

        /// <summary>
        /// ContentControl control template -  Bullet decorator
        /// </summary>
        public static readonly String Main_tmpBulletContent = "Main_tmpBulletContent";

        /// <summary>
        /// Border style - border and size definition for maintenance page link items displayed in the root window content.
        /// </summary>
        public static readonly String Main_StyMaintenanceWindowButton = "Main_StyMaintenanceWindowButton";

        public static readonly String Main_TmpTransparentButton = "Main_TmpTransparentButton";

        /// <summary>
        /// TabControl style - tab strip placement is top right aligned.
        /// </summary>
        public static readonly String Main_StyTopRightTabControl = "Main_StyTopRightTabControl";
        public static readonly String Main_StyTopRightTabItem = "Main_StyTopRightTabItem";
        public static readonly String Main_StyTopLeftTabControl = "Main_StyTopLeftTabControl";

        /// <summary>
        /// Fluent:SplitButton styling for when it is used outside of ribbons.
        /// </summary>
        public static readonly String Main_StySplitButton = "Main_StySplitButton";

        /// <summary>
        /// Button style to standardise buttons to set a field value.
        /// </summary>
        public static readonly String Main_StySetFieldButton = "Main_StySetFieldButton";

        public static readonly String Text_StyDataManagementErrorCommands = "Text_StyDataManagementErrorCommands";
        public static readonly String Buttons_TmpHyperlinkButton = "Buttons_TmpHyperlinkButton";
        public static readonly String Buttons_StyHyperlinkButton = "Buttons_StyHyperlinkButton";

        /// <summary>
        /// Simple border style to draw a horizontal line
        /// </summary>
        public static readonly String Main_StyHorizontalSeparator = "Main_StyHorizontalSeparator";

        #endregion
    }
}