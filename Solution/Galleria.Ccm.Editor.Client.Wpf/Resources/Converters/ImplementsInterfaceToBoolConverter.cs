#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24124 : A.Silva ~ Created

#endregion

#endregion

using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace Galleria.Ccm.Editor.Client.Wpf.Resources.Converters
{
    /// <summary>
    ///     Returns <c>true</c> if the type implements an given interface, otherwise <c>false</c>.
    /// </summary>
    public sealed class ImplementsInterfaceToBoolConverter : IValueConverter
    {
        #region IValueConverter Members

        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            if (value == null)
                return false; // No value to check.

            var interfaceName = parameter as String;

            if (String.IsNullOrEmpty(interfaceName))
                throw new ArgumentNullException("parameter", @"No interface name was provided, provide a type to check.");

            var implementedInterfaces = value.GetType().GetInterfaces()
                .Select(type => type.Name);
            var implementsInterface = implementedInterfaces
                .Any(s1 => s1.Equals(interfaceName, StringComparison.InvariantCultureIgnoreCase));

            return implementsInterface;
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            return null; // Nothing to return as we do not convert back.
        }

        #endregion
    }
}