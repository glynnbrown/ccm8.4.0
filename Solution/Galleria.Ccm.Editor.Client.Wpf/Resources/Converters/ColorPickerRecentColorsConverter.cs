﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.2.0)
// V8-30791 : D.Pleasance
//  Created.
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Data;

namespace Galleria.Ccm.Editor.Client.Wpf.Resources.Converters
{
    public sealed class ColorPickerRecentColorsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return App.ViewState.RecentColors;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}