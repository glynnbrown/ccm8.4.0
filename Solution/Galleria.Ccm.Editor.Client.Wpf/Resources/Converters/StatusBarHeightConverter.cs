﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-25042: A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Data;

namespace Galleria.Ccm.Editor.Client.Wpf.Resources.Converters
{
    public sealed class StatusBarHeightConverter : IValueConverter
    {
        public Object Convert(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return Binding.DoNothing;
            }

            var height = (Int32)value;
            return new GridLength(height);
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return Binding.DoNothing;
            }

            var length = (GridLength)value;
            return (Int32)length.Value;
        }
    }
}
