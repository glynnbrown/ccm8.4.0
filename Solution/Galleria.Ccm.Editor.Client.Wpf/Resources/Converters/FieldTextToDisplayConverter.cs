﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion
#endregion

using System;
using System.Windows.Data;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.Resources.Converters
{
    /// <summary>
    /// Converters the given field text to a display friendly version
    /// </summary>
    public sealed class FieldTextToDisplayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ObjectFieldInfo.ReplaceFieldsWithFriendlyPlaceholders(value as String, PlanItemHelper.EnumerateAllFields());
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ObjectFieldInfo.ReplaceFriendlyPlaceholdersWithFields(value as String, PlanItemHelper.EnumerateAllFields());
        }
    }
}
