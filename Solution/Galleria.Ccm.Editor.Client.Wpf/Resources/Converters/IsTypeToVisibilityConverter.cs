﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24124 : A.Silva ~ Created

#endregion

#endregion

using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Diagnostics;

namespace Galleria.Ccm.Editor.Client.Wpf.Resources.Converters
{
    /// <summary>
    ///     Returns Visibility.Visible if the types are equal, otherwise Visibility.Collapsed.
    /// </summary>
    public sealed class IsTypeTovisibilityConverter : IValueConverter
    {
        #region IValueConverter Members

        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            if (value == null) return null; // No value to check.
            if (targetType != typeof(Visibility)) return null;

            Visibility returnValue = Visibility.Visible; //always return visible by default.

            if (parameter is Type)
            {
                returnValue = ((Type)parameter == value.GetType())? Visibility.Visible : Visibility.Collapsed;
            }
            else
            {
                var typeName = parameter as String;
                if (String.IsNullOrEmpty(typeName)) Debug.Fail("parameter", @"No type was provided, provide a type to compare.");

                var s = value.GetType().Name;
                returnValue = (typeName == s)? Visibility.Visible : Visibility.Collapsed;
            }

            return returnValue;
        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            return null; // Nothing to return as we do not convert back.
        }

        #endregion
    }

    public sealed class IsTypeConverter : IValueConverter
    {
        #region IValueConverter Members

        public Object Convert(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            if (value == null) return false; // No value to check.

            if (parameter is Type)
            {
                return (Type)parameter == value.GetType();
            }
            else
            {
                var typeName = parameter as String;
                if (String.IsNullOrEmpty(typeName)) Debug.Fail("parameter", @"No type was provided, provide a type to compare.");

                var s = value.GetType().Name;
                return (typeName == s) ? Visibility.Visible : Visibility.Collapsed;
            }

        }

        public Object ConvertBack(Object value, Type targetType, Object parameter, CultureInfo culture)
        {
            return null; // Nothing to return as we do not convert back.
        }

        #endregion
    }
}