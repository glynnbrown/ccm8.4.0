﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
// V8-24265 J.Pickup
//  Added LabelSettings Import & Export
// V8-25395 : A.Probyn
//  Added ProductLibrary related references
// V8-25042 : A.Silva ~ Added PlanRepository related references
// V8-24700 : A.Silva ~ Added GridViewSettings related references
// V8-25724 : A.Silva ~ Added Planogram Group Hierarchy side panel related references.
// V8-24700 : A.Silva ~ Added image resources for Column Layout Editor.
// V8-25871 : A.Kuszyk ~ Added PlanogramRepository_PlanogramDragged
// V8-25664 : A.Kuszyk ~ Added Favourites icons.
// V8-26076 : A.Silva ~ Added ColumnLayoutEditor_DraggedColumn icon.
// V8-26337 : A.Kuszyk ~ Added PlanogramProperties PerformanceMetric related icons.
// V8-26338 : A.Kuszyk ~ Added PlanogramProperties_Metric and _performanceMetric_32.
// V8-26491 : A.Kuszyk ~ Added Assortment images.
// V8-26685 : A.Silva ~ Added image resources for Validation Template.
// V8-26860 : L.Luong ~ Added image resources for Event Log
// V8-26956 : L.Luong ~ Added image resources for Consumer Decision Tree
// V8-27132 : A.Kuszyk ~ Added CDT LoadFromRepositorty.
// V8-27569 : A.Probyn ~ Added Backstage Print related.
// V8-27589 : A.Silva   ~ Added StatusBar_EditorValidation.
// V8-27912 : M.Pettit
//  Updated most icons to flat modern look
// V8-27966 : A.Silva
//      Added icons for the plan hierarchy selector when shown in the planogram repository.
//V8-28786 : M.Pettit
//  Added 16x16 icons for Quick Access Toolbar

#endregion

#region Version History: CCM801

// V8-28676 : A.Silva
//      Added RealImage related icons.

#endregion

#region Version History: CCM802
// V8-25378 : M.Pettit
//  Added View_DockAllWindows_16 icon.
// V8-29004 : L.Luong
//  Added LeftArrow-16 and RightArrow-16 icon.
#endregion

#region Version History: CCM803

// V8-29518 : A.Silva
//      Added image for Product Sequence.
// V8-29046 : L.Luong
//      Added image for Product Library Context 

#endregion

#region Version History : CCM 820
// V8-30792 : A.Kuszyk
//  Added BlockingPlanDocument_ShowPositions.
// V8-30936 : M.Brumby
//  Added Ribbon_ToolsCreateSlotWall
// V8-29439 : J.Pickup
//  Added squareWarning48.
// V8-31077 : A.Kuszyk
//  Added additional sequence icons.
#endregion

#region Version History : CCM 820
#endregion

#region Version History: CCM830

// V8-31557 : M.Shelley
//  Added the Planogram Append icon for the design tab planogram append button.
// V8-31742 : A.Silva
//  Added Planogram Compare icon for the Content Tab.
// V8-31747 : A.Silva
//  Added some more Planogram Compare icons for the context tab.
// V8-32317 : M.Pettit
//  Updated Plan Compare icons to V8 flat style
//  Updated ImagesAdd/Remove icons
// V8-32088 : Added specific validation Invalid_XXX icons
// V8-32396 : A.Probyn ~ Corrected _invalidRule_16
// V8-32560 : A.Silva
//  Removed AddAll and RemoveAll commands related icons.
// V8-32700 : A.Silva
//  Added new icons for Update Planogram Product Attributes.
// V8-32661 : A.Kuszyk
//  Added icons for sequence template context ribbon.
// V8-32698 : N.Haywood
//  Added assortment update from products and save to repository
// V8-32564 : A.Heathcote
//  Added SearchScreen ImageSource and LockedByCurrentUser const string
// V8-32816 : A.Kuszyk
//  Added _selectionReverseSequence_32
// V8-32848 : M.Pettit
//  Added _saveAsNewAssortment_32
// CCM-18316 : A.Heathcote
//  Added ProductBuddy Image resource, for the AdvancedAddManualProductBuddyWindow.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Reflection;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using CommonImageResources = Galleria.Ccm.Common.Wpf.Resources.ImageResources;

namespace Galleria.Ccm.Editor.Client.Wpf
{
    /// <summary>
    /// Holds static references to all image sources required for the solution.
    /// </summary>
    /// <remarks>Need to lazy load all the main resources</remarks>
    [SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores", Justification = "NamingConvention ok here.")]
    public static class ImageResources
    {
        #region Helper Methods

        private static readonly Dictionary<String, ImageSource> _sourceDict = new Dictionary<String, ImageSource>();

        private static ImageSource GetSource(String key)
        {
            ImageSource src;
            if (!_sourceDict.TryGetValue(key, out src))
            {
                src = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _iconFolderPath, key));
                _sourceDict[key] = src;
            }
            return src;
        }

        /// <summary>
        /// Helper method to create a frozen bitmap image source
        /// This prevents issue where static sources can memory leak
        /// </summary>
        /// <param name="imagePath"></param>
        /// <returns></returns>
        private static BitmapImage CreateAsFrozen(String imagePath)
        {
            BitmapImage src = new BitmapImage(new Uri(imagePath));
            src.Freeze();
            return src;
        }

        #endregion

        #region Constant Image Paths

        private static readonly String _graphicsFolderPath = String.Format(
            CultureInfo.InvariantCulture, "pack://application:,,,/{0};component/Resources/Graphics/{{0}}", Assembly.GetExecutingAssembly().GetName().Name);
        private static readonly String _iconFolderPath = String.Format(CultureInfo.InvariantCulture, _graphicsFolderPath, "{0}");

        //NB - these private fields are now just strings so that image sources are lazy loaded in as required.

        //A
        const String _add_16 = "Add-16-Database.png";
        const String _add_32 = "Add-32-Database.png";
        const String _addAll_24 = "Forward-24-Multimedia.png";
        const String _addAfter_16 = "AddAfter-16.png";
        const String _addBefore_16 = "AddBefore-16.png";
        const String _addSelected_24 = "Forward-24-Toolbar.png";
        const String _alignLeft_16 = "AlignLeft-16.png";
        const String _alignCentre_16 = "AlignCentre-16.png";
        const String _alignRight_16 = "AlignRight-16.png";
        const String _apply_16 = "Apply-16-Toolbar.png";
        const String _apply_32 = "Apply-32-Business.png";
        const String _applyAndClose_16 = "ApplyAndClose-16.png";
        const String _assemblyGroup_16 = "AssemblyGroup-16.png";
        const String _assemblyUngroup_16 = "AssemblyUnGroup-16.png";
        const String _assortment_16 = "Assortment-16.png";
        const String _assortment_32 = "Assortment-32.png";
        const String _assortmentProductRule_16 = "AssortmentProductRule-16.png";
        const String _assortmentProductRule_32 = "AssortmentProductRule-32.png";
        const String _assortmentRegionalProduct_16 = "RegionalProductSetup-16.png";
        const String _assortmentRegionalProduct_32 = "RegionalProductSetup-32.png";
        const String _assortmentLocalProduct_16 = "AssortmentLocalProducts-16.png";
        const String _assortmentLocalProduct_32 = "AssortmentLocalProducts-32.png";
        const String _assortmentRulePriorities_32 = "RulePriorities-32.png";
        const String _assortmentInventoryRules_32 = "AssortmentInventoryRules-32.png";
        const String _assortmentInventoryRules_16 = "AssortmentInventoryRules-16.png";
        const String _autoMatch_32 = "AutoMatch-32.png";

        //B
        const String _back_16 = "Back-16-Toolbar.png";
        const String _back_24 = "Back-24-Toolbar.png";
        const String _bar1_16 = "Bar1-16.png";
        const String _bar2_16 = "Bar2-16.png";
        const String _bar1_32 = "Bar1-32.png";
        const String _bar2_32 = "Bar2-32.png";
        const String _bay_32 = "Bay-32.png";
        const String _bookLibrary_32 = "BookLibrary-32-Office.png";
        const String _blockMerchandiser_16 = "BlockMerchandiser-16.png";
        const String _blockMerchandiser_32 = "BlockMerchandiser-32.png";
        const String _blockMerchandiserClear_32 = "BlockMerchandiserClear-32.png";

        //C
        const String _ccmAboutScreen = "CCM-AboutScreen.png";
        const String _ccmIcon = "CCMIcon.ico";
        const String _ccmSplashScreen = "CCM-Splashscreen.png";
        const String _cdt_16 = "CDT-16.png";
        const String _cdt_32 = "CDT-32.png";
        const String _checkbox_32_checked = "Checkbox-32-Checked.png";
        const String _checkbox_32_unchecked = "Checkbox-32-Unchecked.png";
        const String _colorPicker_16 = "ColorPicker-16.png";
        const String _colorPicker_32 = "ColorPicker-32.png";
        const String _componentEdit_16 = "ComponentEdit-16.png";
        const String _clear_32 = "Clear-32-Database.png";
        const String _clearDocument_32 = "ClearDocument-32-Toolbar.png";
        const String _clearSelection_32 = "ClearSelection-32.png";
        const String _clipstrip_16 = "ClipStrip-16.png";
        const String _clipstrip_32 = "ClipStrip-32.png";
        const String _close_16 = "ClosedFolder-16-File.png";
        const String _connectDatabase_32 = "ConnectDatabase-32-Database.png";
        const String _copy_16 = "Copy-16-Toolbar.png";
        const String _cut_16 = "Cut-16-Toolbar.png";
        const String _cut_32 = "Cut-32-Design.png";
        const String _connect_32 = "Connect-32.png";
        const String _cursor_32 = "Cursor-32-Design.png";
        const String _combinedShelf_16 = "CombinedShelf-16.png";
        const String _categoryGoals32 = "CategoryGoals-32.png";
        const String _categoryRole32 = "CategoryRole-32.png";
        const String _categoryTactics32 = "CategoryTactics-32.png";

        //D
        const String _danger_32 = "Danger-32-Office.png";
        const String _database_32 = "Database-32-Database.png";
        const String _datasheetAdd_32 = "DatasheetAdd-32.png";
        const String _datasheetSave_32 = "DatasheetSave-32.png";
        const String _delete_14 = "Delete-14.png";
        const String _delete_16 = "Delete-16-Database.png";
        const String _delete_32 = "Delete-32-Database.png";
        const String _downArrow_16 = "DownArrow-16.png";
        //const String _downloadFolder_16 = "DownloadFolder-16-Download.png";
        const String _deleteDivider_32 = "DeleteDivider-32.png";

        //E
        const String _edit_14 = "Edit-14.png";
        const String _edit_16 = "Edit-16-Computer.png";
        const String _entity_32 = "Entity-32.png";
        const String _errorFilter_16 = "ErrorFilter-16.png";
        const String _eventLog_16 = "EventLog-16.png";
        const String _eventLog_32 = "EventLog-32.png";
        const String _exit_16 = "Exit-16-Toolbar.png";
        const String _exportExcelFile_32 = "ExportExcelFile_32.png";

        //F
        const String _facingsAdd_16 = "FacingsAdd-16.png";
        const String _facingsAdd_32 = "FacingsAdd-32.png";
        const String _facingsRemove_16 = "FacingsRemove-16.png";
        const String _facingsRemove_32 = "FacingsRemove-32.png";
        const String _familyRules_32 = "FamilyRules-32.png";
        const String _familyRules_16 = "FamilyRules-16.png";
        const String _favouritesAdd_32 = "FavouritesAdd-32.png";
        const String _favouritesAdd_16 = "FavouritesAdd-16-Toolbar.png";
        const String _favouritesDelete_32 = "FavouritesDelete-32.png";
        const String _favouritesDelete_16 = "FavouritesDelete-16-Toolbar.png";
        const String _filter_16 = "Filter-16.png";
        const String _filter_32 = "Filter-32.png";
        const String _find_32 = "Find-32-Design.png";
        const String _fixtureBack_16 = "FixtureBack-16.png";
        const String _fixtureBottom_16 = "FixtureBottom-16.png";
        const String _fixtureDataView_32 = "FixtureDataView-32.png";
        const String _fixtureLabel_32 = "FixtureLabel-32.png";
        const String _fixtureLibrary_32 = "FixtureLibrary-32.png";
        const String _fixtureFront_16 = "FixtureFront-16.png";
        const String _fixtureSideLeft_16 = "FixtureSideLeft-16.png";
        const String _fixtureSideRight_16 = "FixtureSideRight-16.png";
        const String _fixtureTop_16 = "FixtureTop-16.png";
        const String _fixtureProperties_16 = "FixtureProperties-16.png";
        const String _fixtureProperties_32 = "FixtureProperties-32.png";
        const String _fixtureUpdateXPositionsWithGaps_32 = "FixtureUpdateXPositionsWithGaps-32.png";
        const String _fixtureUpdateXPositionsWithoutGaps_32 = "FixtureUpdateXPositionsWithoutGaps-32.png";
        const String _floodFill_16 = "FloodFill-16-Design.png";
        const String _fontBold_16 = "FontBold-16.png";
        const String _fontItalic_16 = "FontItalic-16.png";
        const String _fontUnderline_16 = "FontUnderline-16.png";
        const String _form_32 = "Form-32-Design.png";
        const String _forward_16 = "Forward-16-Toolbar.png";
        const String _forward_24 = "Forward-24-Toolbar.png";
        const String _freezer_16 = "Freezer-16.png";
        const String _freezer_32 = "Freezer-32.png";
        const String _freezerAdd_32 = "FreezerAdd-32.png";
        const String _fitToHeight_32 = "FitToHeight-32.png";

        //G
        const String _grid_16 = "Grid-16-Database.png";
        
        //H
        const String _hierarchyAddLevel_32 = "HierarchyAddLevel-32.png";
        const String _hierarchyAddLevel_16 = "HierarchyAddLevel-16.png";
        const String _hierarchyAddNode_16 = "HierarchyAddNode-16.png";
        const String _hierarchyAddNode_32 = "HierarchyAddNode-32.png";
        const String _hierarchyEditLevel_16 = "HierarchyEditLevel-16.png";
        const String _hierarchyEditLevel_32 = "HierarchyEditLevel-32.png";
        const String _hierarchyEditNode_16 = "HierarchyEditNode-16.png";
        const String _hierarchyEditNode_32 = "HierarchyEditNode-32.png";
        const String _hierarchyRemoveLevel_32 = "HierarchyRemoveLevel-32.png";
        const String _hierarchyRemoveLevel_16 = "HierarchyRemoveLevel-16.png";
        const String _hierarchyRemoveNode_32 = "HierarchyRemoveNode-32.png";
        const String _hierarchyRemoveNode_16 = "HierarchyRemoveNode-16.png";
        const String _horizontalDivider_32 = "HorizontalDivider-32.png";

        //I
        const String _addImages_16 = "ImagesAdd-16.png";
        const String _addImages_32 = "ImagesAdd-32.png";
        const String _images_16 = "Images-16.png";
        const String _removeImages_16 = "ImagesRemove-16.png";
        const String _removeImages_32 = "ImagesRemove-32.png";
        const String _info_32 = "Info-32-Toolbar.png";

        const String _invalidComponent_16 = "InvalidComponent-16.png";
        const String _invalidInventory_16 = "InvalidInventory-16.png";
        const String _invalidPosition_16 = "InvalidPosition-16.png";
        const String _invalidProduct_16 = "InvalidProduct-16.png";
        const String _invalidRule_16 = "InvalidRule-16.png";

        const String _itemsFind_16 = "Items-Find-16.png";
        const String _itemsOk_16 = "Items-Ok-16.png";
        const String _itemsWarning_16 = "Items-Warning-16.png";

        //L
        const String _leftArrow_16 = "LeftArrow-16.png";
        const String _localPrinter_32 = "Local_Printer.png";
        const String _locked_16 = "Locked-16.png";
        const String _lockedByCurrentUser_16 = "LockedByCurrentUser-16.png";
        const String _locationBuddy_32 = "LocationBuddy-32.png";
        const String _locationBuddyAdd_32 = "LocationBuddyAdd-32.png";

        //M
        const String _meeting_32 = "Meeting-32-People.png";
        const String _move_16 = "Move-16-Design.png";
        const String _moveToOtherWindow_16 = "MoveToOtherWindow-16-Office.png";
        const String _multipleDialogs_32 = "MultipleDialogs-32-Design.png";

        //N
        const String _networkPrinter_32 = "Network_Printer.png";
        const String _newDocument_16 = "NewDocument-16-Toolbar.png";
        const String _newDocument_32 = "NewDocument-32-Toolbar.png";
        const String _newExcelFile_32 = "NewExcelFile-32.png";
        const String _newFolder_16 = "NewFolder-16-Design.png";
        const String _newWindow_16 = "NewWindow-16-Office.png";
        const String _no_48 = "No-48-Toolbar.png";
        const String _normalizeWindows_16 = "NormalizeWindows-16-Office.png";

        //O
        const String _open_16 = "Open-16-Toolbar.png";
        const String _open_32 = "Open-32-Toolbar.png";
        const String _options_16 = "Options-16-Toolbar.png";

        //P
        const String _pallet_16 = "Pallet-16.png";
        const String _pallet_32 = "Pallet-32.png";
        const String _page_32 = "Page-32-Design.png";
        const String _pageOrientationLandscape_32 = "PageOrientation_Landscape.png";
        const String _pageOrientationPortrait_32 = "PageOrientation_Portrait.png";
        const String _pan_16 = "Pan-16-Design.png";
        const String _panel_16 = "Panel-16.png";
        const String _panel_32 = "Panel-32.png";
        const String _pasteDocument_16 = "PasteDocument-16-Toolbar.png";
        const String _pegboard_16 = "Pegboard-16.png";
        const String _pegboard_32 = "Pegboard-32.png";
        const String _performanceMetric_32 = "PerformanceMetric-32.png";
        const String _pin_16 = "Pin-16-Design.png";
        const String _pinGray_16 = "PinGray-16.png";
        const String _planInfo_16 = "PlanInfo-16.png";
        const String _planInfo_32 = "PlanInfo-32.png";
        const String _planogram_32 = "Planogram_32.png";
        const String _planogram3D_16 = "Planogram3D-16.png";
        const String _planogram3D_32 = "Planogram3D-32.png";
        const String _planogramAppend_32 = "PlanogramAppend-32.png";
        const String _planogramCompare_16 = "PlanogramCompare-16.png";
        const String _planogramCompare_32 = "PlanogramCompare-32.png";
        const String _planogramCompareRefresh_32 = "PlanogramCompareRefresh-32.png";
        const String _planogramNoColour_16 = "PlanogramNoColour_16.png";
        const String _planogramNoColour_32 = "PlanogramNoColour_32.png";
        const String _planogramRealImages_16 = "PlanogramRealImages_16.png";
        const String _planogramRealImages_32 = "PlanogramRealImages_32.png";
        const String _planogramSequenceGroups_32 = "PlanogramSequenceGroups-32.png";
        const String _planogramSequenceSubGroupAdd_32 = "PlanogramSequenceSubGroupAdd-32.png";
        const String _planogramSequenceSubGroupRemove_32 = "PlanogramSequenceSubGroupRemove-32.png";
        const String _pogDatabaseFile_24 = "PogDatabaseFile-24.png";
        const String _pogFile_24 = "PogFile-24.png";
        const String _pointer_16 = "Pointer-16-Toolbar.png";
        const String _position_16 = "Position-16.png";
        const String _position_32 = "Position-32.png";
        const String _positionProperties_32 = "PositionProperties-32.png";
        const String _positionFillDeep_16 = "PositionFillDeep-16.png";
        const String _positionFillWide_16 = "PositionFillWide-16.png";
        const String _positionFillHigh_16 = "PositionFillHigh-16.png";
        const String _print_32 = "Print-32-Website.png";
        const String _productBuddy_32 = "ProductBuddy-32.png";
        const String _productBuddyAdd_32 = "ProductBuddyAdd-32.png";
        const String _productLabel_16 = "ProductLabel-16.png";
        const String _productLabel_32 = "ProductLabel-32.png";
        const String _products_16 = "Products-16-Warehouse.png";
        const String _products_32 = "Products-32-Warehouse.png";
        const String _productProperties_16 = "ProductProperties-16.png";
        const String _productProperties_32 = "ProductProperties-32.png";
        const String _productUnits_16 = "ProductUnits-16.png";
        const String _productUnits_32 = "ProductUnits-32.png";
        const String _productFillPatterns_16 = "ProductFillPatterns-16.png";
        const String _productShapes_16 = "ProductShapes-16.png";
        const String _productsDataView_16 = "ProductsDataView-16.png";
        const String _productsDataView_32 = "ProductsDataView-32.png";
        const String _properties_14 = "Properties-14.png";
        const String _propertiesSettings_16 = "PropertiesSettings-16.png";
        const String _propertiesSettings_32 = "PropertiesSettings-32.png";
        const String _productUniverse_16 = "ProductUniverse-16.png";
        const String _productUniverse_32 = "ProductUniverse-32.png";
        const String _productUniverseEdit_32 = "ProductUniverseEdit-32.png";
        const String _productUniverseMappingTemplate_32 = "ProductUniverseMappingTemplate-32.png";
        const String _planogramSequenceLoad_32 = "PlanogramSequenceLoad-32.png";
        const String _planogramSequenceGenerate_32 = "PlanogramSequenceGenerate-32.png";
        const String _planogramSequenceGenerate_16 = "PlanogramSequenceGenerate-16.png";
        const String _priorityCritical16 = "PriorityCritical-16.png";
        const String _priorityHigh16 = "PriorityHigh-16.png";
        const String _priorityMedium16 = "PriorityMedium-16.png";
        const String _priorityLow16 = "PriorityLow-16.png";

        //R
        const String _refresh_16 = "Refresh-16-Toolbar.png";
        const String _redo_16 = "Redo-16-Design.png";
        const String _reportGridEdit_32 = "ReportGridEdit-32.png";
        const String _removeAll_24 = "Rewind-24-Multimedia.png";
        const String _removeSelected_24 = "Back-24-Toolbar.png";
        const String _repositoryPlanogram_24 = "RepositoryPlanogram-24.png";
        const String _rightArrow_16 = "RightArrow-16.png";
        const String _rotate_16 = "Rotate-16-Design.png";

        //S
        const String _save_16 = "Save-16-Toolbar.png";
        const String _save_32 = "Save-32-Toolbar.png";
        const String _save_48 = "Save-48.png";
        const String _saveAs_16 = "SaveAs-16-Toolbar.png";
        const String _saveAs_32 = "SaveAs-32-Toolbar.png";
        const String _saveAsNewAssortment_16 = "SaveAsNewAssortment-16.png";
        const String _saveAsNewAssortment_32 = "SaveAsNewAssortment-32.png";
        const String _saveAndClose_16 = "SaveAndClose-16.png";
        const String _saveAndNew_16 = "SaveAndNew-16.png";
        const String _saveAndClose_32 = "SaveAndClose-32.png";
        const String _saveToDatabase_16 = "SaveToDatabase-16.png";
        const String _saveToDatabase_32 = "SaveToDatabase-32.png";
        const String _saveToDatabase_48 = "SaveToDatabase-48.png";
        const String _saveToFile_16 = "SaveToFile-16.png";
        const String _saveToFile_32 = "SaveToFile-32.png";
        const String _saveToFile_48 = "SaveToFile-48.png";
        const String _saveToLinkedAssortment_16 = "SaveToLinkedAssortment-16.png";
        const String _saveToLinkedAssortment_32 = "SaveToLinkedAssortment-32.png";
        const String _screenshot_16 = "Screenshot-16.png";
        const String _search_16 = "Search-16-Toolbar.png";
        const String _shelf_16 = "Shelf-16.png";
        const String _shelf_32 = "Shelf-32.png";
        const String _slotWall_16 = "Slotwall-16.png";
        const String _snapToNotch_32 = "SnapToNotch-32.png";
        const String _splitWindow_16 = "SplitWindow-16-Office.png";
        const String _splitFixture_16 = "FixtureSplit-16.png";
        const String _saveAll_16 = "SaveAll-16.png";
        const String _squareTick_16 = "SquareTick-16.png";
        const String _squareWarning_16 = "SquareWarning-16.png";
        const String _squareWarning_48 = "SquareWarning-48.png";
        const String _support_16 = "Support-16-Office.png";
        const String _support_32 = "Support-32-Office.png";
        const String _select32 = "Select-32.png";
        const String _selectionReverseSequence_32 = "SelectionReverseSequence-32.png";

        //T
        const String _text_16 = "Text-16-Toolbar.png";
        const String _toggleShortLong_16 = "ToggleShortLong-16.png";
        const String _undo_16 = "Undo-16-Design.png";
        const String _turnOff_16 = "TurnOff-16-Toolbar.png";

        //U
        const String _upArrow_16 = "UpArrow-16.png";
        //const String _uploadFolder_16 = "UploadFolder-16-Download.png";
        const String _users_16 = "Users-16-Toolbar.png";
        private const String _updateAllPlanogramsFromProductAttribute_32 = "UpdateAllPlanogramsFromProductAttribute-32.png";
        private const String _updatePlanogramFromProductAttribute_32 = "UpdatePlanogramFromProductAttribute-32.png";
        
        const String _updatePlanogramFromUniverse_32 = "UpdatePlanogramFromUniverse-32.png";
        const String _updateAllPlanogramsFromUniverse_32 = "UpdateAllPlanogramsFromUniverse-32.png";
        const String _updateAssortmentFromProduct_32 = "UpdateAssortmentFromProduct-32.png";
        const String _unlock_32 = "Unlocked-32.png";
        const String _unmerchandisingComponentRemove_32 = "UnmerchandisingComponentRemove-32.png";
        const String _unplacedProductRemove_32 = "UnplacedProductRemove-32.png";

        //V
        const String _validationTemplateMaintenance_32 = "ValidationTemplate-32.png";
        const String _viewsArrangeAll_32 = "ViewsArrangeAll-32.png";
        const String _verticalDivider_32 = "VerticalDivider-32.png";

        // W
        const String _warning_20 = "Warning-20-Toolbar.png";
        const String _warning_32 = "Warning-32-Toolbar.png";
        const String _window_16 = "Window-16-Office.png";
        const String _windowDockAll_16 = "WindowDockAll-16.png";
        const String _windows_32 = "Windows-32-Office.png";
        const String _warehouse32 = "Warehouse-32.png";

        //Z
        const String _zoomArea_16 = "ZoomArea-16.png";
        const String _zoomArea_32 = "ZoomArea-32.png";
        const String _zoomIn_16 = "ZoomIn-16-Toolbar.png";
        const String _zoomIn_32 = "ZoomIn-32-Toolbar.png";
        const String _zoomOut_16 = "ZoomOut-16-Toolbar.png";
        const String _zoomOut_32 = "ZoomOut-32-Toolbar.png";
        const String _zoomSelection_32 = "ZoomSelection-32.png";
        const String _zoomToFit_16 = "ZoomToFit-16.png";


        #endregion

        #region Icon, Splash and About page backgrounds
        public static ImageSource CCMSplashscreen { get { return GetSource(_ccmSplashScreen); } }
        public static ImageSource CCMAboutScreen { get { return GetSource(_ccmAboutScreen); } }
        public static ImageSource CCMIcon { get { return GetSource(_ccmIcon); } }
        #endregion

        #region General

        public static ImageSource Add_16 { get { return GetSource(_add_16); } }
        public static ImageSource Add_32 { get { return GetSource(_add_32); } }
        public static ImageSource Copy_16 { get { return GetSource(_copy_16); } }
        public static ImageSource Cut_16 { get { return GetSource(_cut_16); } }
        public static ImageSource Close_16 { get { return GetSource(_close_16); } }
        public static ImageSource Delete_14 { get { return GetSource(_delete_14); } }
        public static ImageSource Delete_16 { get { return GetSource(_delete_16); } }
        public static ImageSource Delete_32 { get { return GetSource(_delete_32); } }
        public static ImageSource Dialog_Information { get { return GetSource(_info_32); } }
        public static ImageSource Dialog_Warning { get { return GetSource(_warning_32); } }
        public static ImageSource Edit_14 { get { return GetSource(_edit_14); } }
        public static ImageSource Filter_32 { get { return GetSource(_filter_32); } }
        public static ImageSource NewDocument_32 { get { return GetSource(_newDocument_32); } }
        public static ImageSource NewDocument_16 { get { return GetSource(_newDocument_16); } }
        public static ImageSource Open_16 { get { return GetSource(_open_16); } }
        public static ImageSource Properties_14 { get { return GetSource(_properties_14); } }

        public static ImageSource Paste_16 { get { return GetSource(_pasteDocument_16); } }
        public static ImageSource Redo_16 { get { return GetSource(_redo_16); } }
        public static ImageSource Save_16 { get { return GetSource(_save_16); } }
        public static ImageSource Save_32 { get { return GetSource(_save_32); } }
        public static ImageSource Save_48 { get { return GetSource(_save_48); } }
        public static ImageSource SaveAs_16 { get { return GetSource(_saveAs_16); } }
        public static ImageSource SaveAs_32 { get { return GetSource(_saveAs_32); } }
        public static ImageSource SaveAll_16 { get { return GetSource(_saveAll_16); } }
        public static ImageSource SaveAndNew_16 { get { return GetSource(_saveAndNew_16); } }
        public static ImageSource SaveAndClose_16 { get { return GetSource(_saveAndClose_16); } }
        public static ImageSource SaveAndClose_32 { get { return GetSource(_saveAndClose_32); } }

        public static ImageSource SaveToDatabase_16 { get { return GetSource(_saveToDatabase_16); } }
        public static ImageSource SaveToDatabase_32 { get { return GetSource(_saveToDatabase_32); } }
        public static ImageSource SaveToDatabase_48 { get { return GetSource(_saveToDatabase_48); } }
        public static ImageSource SaveToFile_16 { get { return GetSource(_saveToFile_16); } }
        public static ImageSource SaveToFile_32 { get { return GetSource(_saveToFile_32); } }
        public static ImageSource SaveToFile_48 { get { return GetSource(_saveToFile_48); } }
        public static ImageSource Screenshot_16 { get { return GetSource(_screenshot_16); } }
        
        public static ImageSource Tick {get { return GetSource(_checkbox_32_checked); } }
        public static ImageSource TreeViewItem_ClosedFolder { get { return GetSource(_close_16); } }
        public static ImageSource TreeViewItem_OpenFolder { get { return GetSource(_open_16); } }

        public static ImageSource Undo_16 { get { return GetSource(_undo_16); } }
        public static ImageSource UnTick {get { return GetSource(_checkbox_32_unchecked); } }
        public static ImageSource ZoomToFit_32 { get { return GetSource(_zoomToFit_16); } }
        public static ImageSource ZoomIn_16 { get { return GetSource(_zoomIn_16); } }
        public static ImageSource ZoomIn_32 { get { return GetSource(_zoomIn_32); } }
        public static ImageSource ZoomOut_16 { get { return GetSource(_zoomOut_16); } }
        public static ImageSource ZoomOut_32 { get { return GetSource(_zoomOut_32); } }
        public static ImageSource Info_16 { get { return GetSource(_support_16); } }
        public static ImageSource Warning_32 { get { return GetSource(_warning_32); } }
        public static ImageSource Error_32 { get { return GetSource(_danger_32); } }
        public static ImageSource Apply_16 { get { return GetSource(_apply_16); } }
        public static ImageSource Apply_32 { get { return GetSource(_apply_32); } }
        public static ImageSource ApplyAndClose_16 { get { return GetSource(_applyAndClose_16); } }
        public static ImageSource TurnOff_16 { get { return GetSource(_turnOff_16); } }
        public static ImageSource Warning_16 { get { return GetSource(_warning_20); } }

        public static ImageSource ImageSelector_NoImage { get { return GetSource(_no_48); } }

        #endregion

        #region Backstage Help
        public static ImageSource BackstageHelp_Help_32 { get { return GetSource(_support_32); } }
        //public static ImageSource BackstageHelp_Email_48 { get { return GetSource(_email_48); } }
        public static ImageSource BackstageHelp_ContactUs_32 { get { return GetSource(_meeting_32); } }
        //public static ImageSource BackstageHelp_GalleriaAddress_48 { get { return GetSource(_office_48); } }
        //public static ImageSource BackstageHelp_Web_48 { get { return GetSource(_web_48); } }
        //public static ImageSource BackstageHelp_Telephone_48 { get { return GetSource(_telephone_48); } }
        #endregion

        #region Backstage Recent

        public static ImageSource BackstageRecent_RepositoryFileType { get { return GetSource(_pogDatabaseFile_24); } }
        public static ImageSource BackstageRecent_PogFileType { get { return GetSource(_pogFile_24); } }
        public static ImageSource BackstageRecent_Pin { get { return GetSource(_pinGray_16); } }
        public static ImageSource BackstageRecent_Unpin { get { return GetSource(_pin_16); } }

        #endregion

        #region Print

        public static ImageSource BackstagePrint_Print { get { return GetSource(_print_32); } }
        public static ImageSource BackstagePrint_LocalPrinter { get { return GetSource(_localPrinter_32); } }
        public static ImageSource BackstagePrint_NetworkPrinter { get { return GetSource(_networkPrinter_32); } }
        public static ImageSource BackstagePrint_Orientation_Landscape { get { return GetSource(_pageOrientationLandscape_32); } }
        public static ImageSource BackstagePrint_Orientation_Portrait { get { return GetSource(_pageOrientationPortrait_32); } }
        public static ImageSource BackstagePrint_PaperType { get { return GetSource(_page_32); } }

        #endregion

        #region Backstage Setup

        public static ImageSource BackstageSetup_Repositories { get { return GetSource(_database_32); } }

        #endregion

        #region ViewSettings

        public static ImageSource ViewSettings_RemoveFilter_16 { get { return GetSource(_delete_16); } }
        public static ImageSource ViewSettings_Properties_32 { get { return TODO; } }
        public static ImageSource ViewSettings_Columns_16 { get { return TODO; } }

        #endregion

        #region Ribbons

        public static ImageSource PlanogramProperties_16 { get { return GetSource(_planInfo_16); } }
        public static ImageSource PlanogramProperties_32 { get { return GetSource(_planInfo_32); } }

        public static ImageSource PlanItemProperties_16 { get { return GetSource(_propertiesSettings_16); } }
        public static ImageSource PlanItemProperties_32 { get { return GetSource(_form_32); } }

        public static ImageSource PlanogramAppend_32 { get { return GetSource(_planogramAppend_32); } }

        public static ImageSource Ribbon_Search_16 { get { return GetSource(_search_16); } }

        public static ImageSource Ribbon_OtherViews_16 { get { return GetSource(_planogram3D_16); } }
        public static ImageSource Ribbon_DuplicateCurrentView_16 { get { return GetSource(_newWindow_16); } }

        public static ImageSource TogglePanningMode_16 { get { return GetSource(_pan_16); } }
        public static ImageSource ZoomSelectedPlanItems_32 { get { return GetSource(_zoomSelection_32); } }
        public static ImageSource ZoomToArea_16 { get { return GetSource(_zoomArea_16); } }
        public static ImageSource ZoomToArea_32 { get { return GetSource(_zoomArea_32); } }

        public static ImageSource Ribbon_IncreasePositionFacings_16 { get { return GetSource(_facingsAdd_16); } }
        public static ImageSource Ribbon_IncreasePositionFacings_32 { get { return GetSource(_facingsAdd_32); } }
        public static ImageSource Ribbon_DecreasePositionFacings_16 { get { return GetSource(_facingsRemove_16); } }
        public static ImageSource Ribbon_DecreasePositionFacings_32 { get { return GetSource(_facingsRemove_32); } }

        public static ImageSource Ribbon_ToolsPointer { get { return GetSource(_pointer_16); } }
        public static ImageSource Ribbon_ToolsMove { get { return GetSource(_move_16); } }
        public static ImageSource Ribbon_ToolsRotate { get { return GetSource(_rotate_16); } }
        public static ImageSource Ribbon_ToolsNewTextPart { get { return GetSource(_text_16); } }
        //public static ImageSource Ribbon_ToolsCreateBackboard { get { return GetSource(_bay_32); } }

        public static ImageSource Ribbon_ToolsCreateShelf { get { return GetSource(_shelf_16); } }
        public static ImageSource Ribbon_ToolsCreateChest { get { return GetSource(_freezer_16); } }
        public static ImageSource Ribbon_ToolsCreatePegboard { get { return GetSource(_pegboard_16); } }
        public static ImageSource Ribbon_ToolsCreatePallet { get { return GetSource(_pallet_16); } }
        public static ImageSource Ribbon_ToolsCreateBar { get { return GetSource(_bar2_16); } }
        public static ImageSource Ribbon_ToolsCreateRod { get { return GetSource(_bar1_16); } }
        public static ImageSource Ribbon_ToolsCreateClipstrip { get { return GetSource(_clipstrip_16); } }
        public static ImageSource Ribbon_ToolsCreatePanel { get { return GetSource(_panel_16); } }
        public static ImageSource Ribbon_ToolsCreateSlotWall { get { return GetSource(_slotWall_16); } }


        //public static ImageSource Ribbon_ToolsCreateShelf { get { return GetSource(_shelf_32); } }
        //public static ImageSource Ribbon_ToolsCreateChest { get { return GetSource(_freezer_32); } }
        //public static ImageSource Ribbon_ToolsCreatePegboard { get { return GetSource(_pegboard_32); } }
        //public static ImageSource Ribbon_ToolsCreatePallet { get { return GetSource(_pallet_32); } }
        //public static ImageSource Ribbon_ToolsCreateBar { get { return GetSource(_bar2_32); } }
        //public static ImageSource Ribbon_ToolsCreateRod { get { return GetSource(_bar1_32); } }
        //public static ImageSource Ribbon_ToolsCreateClipstrip { get { return GetSource(_clipstrip_32); } }
        //public static ImageSource Ribbon_ToolsCreatePanel { get { return GetSource(_panel_32); } }
        public static ImageSource Ribbon_ToolsCreateCustom { get { return GetSource(_componentEdit_16); } }

        public static ImageSource Ribbon_ToolsNewBoxPart { get { return GetSource(_freezerAdd_32); } }
        public static ImageSource Ribbon_EditComponent_16 { get { return GetSource(_componentEdit_16); } }
        public static ImageSource Ribbon_CreateAssembly_16 { get { return GetSource(_assemblyGroup_16); } }
        public static ImageSource Ribbon_SplitAssembly_16 { get { return GetSource(_assemblyUngroup_16); } }
        public static ImageSource Ribbon_LinkAnnotation { get { return GetSource(_assemblyGroup_16); } }
        public static ImageSource Ribbon_UnlinkAnnotation { get { return GetSource(_assemblyUngroup_16); } }

        public static ImageSource Ribbon_AddPosition { get { return GetSource(_position_16); } }
        public static ImageSource Ribbon_AddProduct { get { return GetSource(_products_16); } }
        public static ImageSource Ribbon_AddToFixtureLibrary_16 { get { return GetSource(_fixtureLibrary_32); } }
        public static ImageSource Ribbon_AddToFixtureLibrary_32 { get { return GetSource(_fixtureLibrary_32); } }

        public static ImageSource Ribbon_DesignSelect_32 { get { return GetSource(_select32); } }

        public static ImageSource View_Positions_32 { get { return GetSource(_planogram_32); } }
        public static ImageSource View_ProductUnits_16 { get { return GetSource(_productUnits_16); } }
        public static ImageSource View_ProductUnits_32 { get { return GetSource(_productUnits_32); } }
        public static ImageSource View_ProductFillPatterns_16 { get { return GetSource(_productFillPatterns_16); } }
        public static ImageSource View_ProductShapes_16 { get { return GetSource(_productShapes_16); } }
        public static ImageSource View_ProductLabel_16 { get { return GetSource(_productLabel_16); } }
        public static ImageSource View_ProductLabel_32 { get { return GetSource(_productLabel_32); } }
        public static ImageSource View_RealImages_16 { get { return GetSource(_planogramRealImages_16); } }
        public static ImageSource View_RealImages_32 { get { return GetSource(_planogramRealImages_32); } }
        public static ImageSource View_FixtureLabel_32 { get { return GetSource(_fixtureLabel_32); } }
        public static ImageSource View_ManageHighlights_16 { get { return GetSource(_colorPicker_16); } }
        public static ImageSource View_ManageHighlights_32 { get { return GetSource(_colorPicker_32); } }

        public static ImageSource View_PositionProperties_32 { get { return GetSource(_positionProperties_32); } }
        public static ImageSource View_FillPositionsDeep_16 { get { return GetSource(_positionFillDeep_16); } }
        public static ImageSource View_FillPositionsHigh_16 { get { return GetSource(_positionFillHigh_16); } }
        public static ImageSource View_FillPositionsWide_16 { get { return GetSource(_positionFillWide_16); } }

        public static ImageSource Ribbon_ReportEditor_32 { get { return GetSource(_reportGridEdit_32); } }

        public static ImageSource Ribbon_RemoveImages_16 { get { return GetSource(_removeImages_16); } }
        public static ImageSource Ribbon_RemoveImages_32 { get { return GetSource(_removeImages_32); } }
        public static ImageSource Ribbon_AddImages_16 { get { return GetSource(_addImages_16); } }
        public static ImageSource Ribbon_AddImages_32 { get { return GetSource(_addImages_32); } }

        public static ImageSource Ribbon_ShowHideLibraryPanel_32 { get { return GetSource(_bookLibrary_32); } }

        public static ImageSource View_ArrangeWindows_32 { get { return GetSource(_windows_32); } }
        public static ImageSource View_WinTileHoriz_16 { get { return GetSource(_normalizeWindows_16); } }
        public static ImageSource View_WinTileVert_16 { get { return GetSource(_splitWindow_16); } }
        public static ImageSource View_WinSwitch_16 { get { return GetSource(_moveToOtherWindow_16); } }
        public static ImageSource View_WinDockAll_16 { get { return GetSource(_windowDockAll_16); } }
        public static ImageSource View_ViewsTileHoriz_16 { get { return GetSource(_normalizeWindows_16); } }
        public static ImageSource View_ViewsTileVert_16 { get { return GetSource(_splitWindow_16); } }
        public static ImageSource View_ViewsDockAll_16 { get { return GetSource(_window_16); } }
        public static ImageSource View_ViewsArrangeAll_32 { get { return GetSource(_viewsArrangeAll_32); } }
        public static ImageSource View_ViewsArrangeAll_16 { get { return GetSource(_window_16); } }

        public static ImageSource Ribbon_PreviewPlanItem_32 { get { return GetSource(_find_32); } }

        public static ImageSource PositionContext_EditProduct_16 { get { return GetSource(_productProperties_16); } }
        public static ImageSource PositionContext_EditProduct_32 { get { return GetSource(_productProperties_32); } }
        public static ImageSource PositionContext_EditPosition_32 { get { return GetSource(_position_32); } }

        public static ImageSource PositionContext_ProductFill_16 { get { return GetSource(_floodFill_16); } }

        public static ImageSource FixtureContext_EditFixture_16 { get { return GetSource(_fixtureProperties_16); } }
        public static ImageSource FixtureContext_EditFixture_32 { get { return GetSource(_fixtureProperties_32); } }

        public static ImageSource MainPage_Options { get { return GetSource(_options_16); } }
        public static ImageSource MainPage_ExitApplication { get { return GetSource(_exit_16); } }

        public static ImageSource Ribbon_SelectAllComponents_16 { get { return  GetSource(_shelf_16); } }
        public static ImageSource Ribbon_RemoveUnmerchandisedComponents_32 { get { return GetSource(_unmerchandisingComponentRemove_32); } }
        public static ImageSource Ribbon_RemoveUnplacedProducts_32 { get { return GetSource(_unplacedProductRemove_32); } }
        public static ImageSource Ribbon_SelectSameProduct_16 { get { return  GetSource(_products_16); } }
        public static ImageSource Ribbon_SelectAllPositions_16 { get { return  GetSource(_position_16); } }
        public static ImageSource Ribbon_SelectCombinedComponents_16 { get { return GetSource(_combinedShelf_16); } }

        public static ImageSource DesignContext_UpdatePlanogramFromProductAttribute_32
        {
            get { return GetSource(_updatePlanogramFromProductAttribute_32); }
        }
        public static ImageSource DesignContext_UpdateAllPlanogramsFromProductAttribute_32
        {
            get { return GetSource(_updateAllPlanogramsFromProductAttribute_32); }
        }

        public static ImageSource DesignContext_FixtureUpdateXPositionsWithGaps_32
        {
            get { return GetSource(_fixtureUpdateXPositionsWithGaps_32); }
        }
        public static ImageSource DesignContext_FixtureUpdateXPositionsWithoutGaps_32
        {
            get { return GetSource(_fixtureUpdateXPositionsWithoutGaps_32); }
        }
        #endregion

        #region FixtureLibrary

        public static ImageSource FixtureLibrary_ValidDragOverAdorner { get { return GetSource(_fixtureLibrary_32); } }
        public static ImageSource FixtureLibrary_InvalidDragOverAdorner { get { return GetSource(_delete_16); } }

        public static ImageSource FixtureLibrary_MultiFixtureTypeIcon { get { return GetSource(_fixtureLibrary_32); } }
        public static ImageSource FixtureLibrary_FixtureTypeIcon { get { return GetSource(_fixtureSideLeft_16); } }
        public static ImageSource FixtureLibrary_AssemblyTypeIcon { get { return GetSource(_assemblyGroup_16); } }
        public static ImageSource FixtureLibrary_ComponentTypeIcon { get { return GetSource(_shelf_32); } }

        public static ImageSource FixtureLibrary_ClosedTreeItem { get { return GetSource(_close_16); } }
        public static ImageSource FixtureLibrary_OpenTreeItem { get { return GetSource(_open_16); } }

        public static ImageSource FixtureLibrary_NewFolder_16 { get { return GetSource(_newFolder_16); } }
        public static ImageSource FixtureLibrary_RenameFolder { get { return GetSource(_edit_16); } }
        public static ImageSource FixtureLibrary_DeleteFolder_16 { get { return GetSource(_delete_16); } }
        public static ImageSource FixtureLibrary_EditFixturePackage { get { return GetSource(_edit_16); } }

        public static ImageSource FixtureLibraryAddEdit_SelectFolder_16 { get { return GetSource(_open_16); } }

        #endregion

        #region Import Definitions

        public static ImageSource ImportDefinition_Duplicate { get { return GetSource(_warning_20); } }

        #endregion

        #region PlanDocumentTypes

        public static ImageSource PlanDocumentType_OrthFront16 { get { return GetSource(_planogramNoColour_16); } }
        public static ImageSource PlanDocumentType_OrthFront32 { get { return GetSource(_planogramNoColour_32); } }
        public static ImageSource PlanDocumentType_OrthLeft16 { get { return GetSource(_fixtureSideLeft_16); } }
        public static ImageSource PlanDocumentType_OrthRight16 { get { return GetSource(_fixtureSideRight_16); } }
        public static ImageSource PlanDocumentType_OrthTop16 { get { return GetSource(_fixtureTop_16); } }
        public static ImageSource PlanDocumentType_OrthBottom16 { get { return GetSource(_fixtureBottom_16); } }
        public static ImageSource PlanDocumentType_OrthBack16 { get { return GetSource(_fixtureBack_16); } }
        public static ImageSource PlanDocumentType_ThreeDimesional16 { get { return GetSource(_planogram3D_16); } }
        public static ImageSource PlanDocumentType_ThreeDimesional32 { get { return GetSource(_planogram3D_32); } }
        public static ImageSource PlanDocumentType_ProductList16 { get { return GetSource(_productsDataView_16); } }
        public static ImageSource PlanDocumentType_ProductList32 { get { return GetSource(_productsDataView_32); } }
        public static ImageSource PlanDocumentType_FixtureList32 { get { return GetSource(_fixtureDataView_32); } }
        public static ImageSource PlanDocumentType_Assortment16 { get { return GetSource(_assortment_16); } }
        public static ImageSource PlanDocumentType_Assortment32 { get { return GetSource(_assortment_32); } }
        public static ImageSource PlanDocumentType_CDT16 { get { return GetSource(_cdt_16); } }
        public static ImageSource PlanDocumentType_CDT32 { get { return GetSource(_cdt_32); } }
        public static ImageSource PlanDocumentType_Blocking16 { get { return GetSource(_blockMerchandiser_16); } }
        public static ImageSource PlanDocumentType_Blocking32 { get { return GetSource(_blockMerchandiser_32); } }
        public static ImageSource PlanDocumentType_PlanogramComparison16 { get { return GetSource(_planogramCompare_16); } }
        public static ImageSource PlanDocumentType_PlanogramComparison32 { get { return GetSource(_planogramCompare_32); } }
        public static ImageSource PlanDocumentType_CategoryInsight16 { get { return GetSource(_warehouse32); } }
        public static ImageSource PlanDocumentType_CategoryInsight32 { get { return GetSource(_warehouse32); } }

        #endregion

        #region PlanogramProperties

        public static ImageSource PlanogramProperties_AddFixtureAfter_16 { get { return GetSource(_addAfter_16); } }
        public static ImageSource PlanogramProperties_AddFixtureBefore_16 { get { return GetSource(_addBefore_16); } }
        public static ImageSource PlanogramProperties_MoveFixtureLeft_16 { get { return GetSource(_leftArrow_16); } }
        public static ImageSource PlanogramProperties_MoveFixtureRight_16 { get { return GetSource(_rightArrow_16); } }
        public static ImageSource PlanogramProperties_AddPerformanceMetric { get { return GetSource(_add_16); } }
        public static ImageSource PlanogramProperties_ViewPerformanceMetric { get { return GetSource(_edit_16); } }
        public static ImageSource PlanogramProperties_RemovePerformanceMetric { get { return GetSource(_delete_16); } }
        public static ImageSource PlanogramProperties_Metric { get { return GetSource(_performanceMetric_32); } }
        public static ImageSource PlanogramProperties_SnapToNotch { get { return GetSource(_snapToNotch_32); } }
        public static ImageSource PlanogramProperties_SplitFixture { get { return GetSource(_splitFixture_16); } }
        #endregion

        #region PlanRepository

        public static ImageSource PlanogramRepository_PlanogramDragging_32 { get { return GetSource(_planogramNoColour_32); } }
        public static ImageSource PlanRepository_Ribbon_ConnectRepository_32 { get { return GetSource(_connectDatabase_32); } }
        public static ImageSource PlanRepository_Ribbon_ImportFile_32 { get { return GetSource(_newExcelFile_32); } }
        public static ImageSource PlanRepository_Ribbon_ExportFile_32 { get { return GetSource(_exportExcelFile_32); } }
        public static ImageSource PlanRepository_Ribbon_AddNewPlanogram_32 { get { return GetSource(_newDocument_32); } }
        public static ImageSource PlanRepository_Ribbon_OpenPlanogram_32 { get { return GetSource(_open_32); } }
        public static ImageSource PlanRepository_Ribbon_FindPlanogram_16 { get { return GetSource(_search_16); } }
        public static ImageSource PlanRepository_Ribbon_CopyPlanogram_16 { get { return GetSource(_copy_16); } }
        public static ImageSource PlanRepository_Ribbon_DuplicatePlanogram_16 { get { return GetSource(_pasteDocument_16); } }
        public static ImageSource PlanRepository_Ribbon_FilterByUser_16 { get { return GetSource(_users_16); } }
        public static ImageSource Ribbon_NewView_32 { get { return GetSource(_datasheetAdd_32); } }
        public static ImageSource Ribbon_EditView_16 { get { return GetSource(_edit_16); } }
        public static ImageSource Ribbon_RemoveView_16 { get { return GetSource(_delete_16); } }
        public static ImageSource Ribbon_SaveViewSettings_32 { get { return GetSource(_datasheetSave_32); } }
        public static ImageSource Ribbon_SaveViewAs_16 { get { return GetSource(_saveAs_16); } }
        public static ImageSource PlanRepository_AddToFavourites32 { get { return GetSource(_favouritesAdd_32); } }
        public static ImageSource PlanRepository_AddToFavourites16 { get { return GetSource(_favouritesAdd_16); } }
        public static ImageSource PlanRepository_DeleteFromFavourites32 { get { return GetSource(_favouritesDelete_32); } }
        public static ImageSource PlanRepository_DeleteFromFavourites16 { get { return GetSource(_favouritesDelete_16); } }
        public static ImageSource PlanRepository_Refresh16 { get { return GetSource(_refresh_16); } }
        public static ImageSource Entity_32 { get { return GetSource(_entity_32); } }
        public static ImageSource PlanogramRepository_NewGroup_32 { get { return GetSource(_hierarchyAddLevel_32); } }
        public static ImageSource PlanogramRepository_NewGroup_16 { get { return GetSource(_hierarchyAddLevel_16); } }
        public static ImageSource PlanogramRepository_EditGroup_32 { get { return GetSource(_hierarchyEditLevel_32); } }
        public static ImageSource PlanogramRepository_EditGroup_16 { get { return GetSource(_hierarchyEditLevel_16); } }
        public static ImageSource PlanogramRepository_DeleteGroup_32 { get { return GetSource(_hierarchyRemoveLevel_32); } }
        public static ImageSource PlanogramRepository_DeleteGroup_16 { get { return GetSource(_hierarchyRemoveLevel_16); } }
        public static ImageSource PlanogramRepository_UnlockPlanogram { get { return GetSource(_unlock_32); } }

        #endregion

        #region PlanItemProperties

        public static ImageSource PlanItemProperties_EditProductImage_16 { get { return GetSource(_edit_16); } }
        public static ImageSource PlanItemProperties_ClearProductImage_16 { get { return GetSource(_delete_16); } }
        public static ImageSource PlanItemProperties_StoredProductImage_16 { get { return GetSource(_images_16); } }

        #endregion

        #region ProductLibrary

        public static ImageSource ProductLibrary_ValidDragOverAdorner { get { return GetSource(_products_16); } }
        public static ImageSource ProductLibrary_InvalidDragOverAdorner { get { return GetSource(_delete_16); } }

        public static ImageSource ProductLibraryContext_ProductLibraryMaintenance_32 { get { return GetSource(_productUniverseMappingTemplate_32); } }
        public static ImageSource ProductLibraryContext_SelectProductLibrary_16 { get { return GetSource(_productUniverse_16); } }
        public static ImageSource ProductLibraryContext_SelectProductLibrary_32 { get { return GetSource(_productUniverse_32); } }
        public static ImageSource ProductLibraryContext_SelectProductLibraryFromDatabase_32 { get { return GetSource(_database_32); } }
        public static ImageSource ProductLibraryContext_SelectProductLibraryFromFile_32 { get { return GetSource(_newDocument_32); } }
        public static ImageSource ProductLibraryContext_UpdatePlanogramFromUniverse_32 { get { return GetSource(_updatePlanogramFromUniverse_32); } }
        public static ImageSource ProductLibraryContext_UpdateAllPlanogramsFromUniverse_32 { get { return GetSource(_updateAllPlanogramsFromUniverse_32); } }

        #endregion

        #region ProductLibraryMaintenance

        public static ImageSource ProductLibraryMaintenance { get { return GetSource(_productUniverse_32); } }
        public static ImageSource ProductLibraryMaintenance_New { get { return GetSource(_newDocument_32); } }
        public static ImageSource ProductLibraryMaintenance_Clear { get { return GetSource(_delete_32); } }
        public static ImageSource ProductLibraryMaintenance_ClearSelection { get { return GetSource(_clearSelection_32); } }
        public static ImageSource ProductLibraryMaintenance_ClearAll { get { return GetSource(_clear_32); } }
        public static ImageSource ProductLibraryMaintenance_AutoMatch { get { return GetSource(_autoMatch_32); } }
        public static ImageSource ProductLibraryMaintenance_NoFilePathSet { get { return GetSource(_warning_20); } }

        #endregion

        #region ProductList

        public static ImageSource ProductList_AddNewProduct_16 { get { return GetSource(_add_16); } }

        public static ImageSource CreatePositions_AddProducts_24 { get { return GetSource(_forward_24); } }
        public static ImageSource CreatePositions_RemoveProducts_24 { get { return GetSource(_back_24); } }


        #endregion

        #region FieldSelector

        public static ImageSource FieldSelector_RemoveLastFormulaItem { get { return GetSource(_undo_16); } }
        public static ImageSource FieldSelector_ClearFormula { get { return GetSource(_delete_16); } }

        #endregion

        #region HighlightEditor

        public static ImageSource HighlightEditor_ApplyToAll_16 { get { return GetSource(_apply_16); } }

        #endregion

        #region LabelSettings
        public static ImageSource LabelSettings_AlignLeft { get { return GetSource(_alignLeft_16); } }
        public static ImageSource LabelSettings_AlignCentre { get { return GetSource(_alignCentre_16); } }
        public static ImageSource LabelSettings_AlignRight { get { return GetSource(_alignRight_16); } }
        public static ImageSource LabelSettings_FontBold { get { return GetSource(_fontBold_16); } }
        public static ImageSource LabelSettings_FontItalic { get { return GetSource(_fontItalic_16); } }
        public static ImageSource LabelSettings_FontUnderline { get { return GetSource(_fontUnderline_16); } }
        public static ImageSource LabelSettings_ApplyToAll_16 { get { return GetSource(_apply_16); } }
        public static ImageSource LabelSettings_Add_32 { get { return GetSource(_add_32); } }

        #endregion

        #region ColumnLayoutEditor
        public static ImageSource ColumnLayoutEditor_DraggedColumn { get { return GetSource(_grid_16); } }
        public static ImageSource ColumnLayoutEditor_AddAll { get { return GetSource(_addAll_24); } }
        public static ImageSource ColumnLayoutEditor_AddSelected { get { return GetSource(_addSelected_24); } }
        public static ImageSource ColumnLayoutEditor_RemoveAll { get { return GetSource(_removeAll_24); } }
        public static ImageSource ColumnLayoutEditor_RemoveSelected { get { return GetSource(_removeSelected_24); } }
        public static ImageSource ColumnLayoutEditor_MoveColumnUp { get { return GetSource(_upArrow_16); } }
        public static ImageSource ColumnLayoutEditor_MoveColumnDown { get { return GetSource(_downArrow_16); } }
        public static ImageSource ReportColumnLayoutEditor_ColumnLock { get { return CommonImageResources.Locked_16; } }
        #endregion

        #region Options

        public static ImageSource Options_AssignProperties { get { return GetSource(_forward_16); } }
        public static ImageSource Options_RemoveProperties { get { return GetSource(_back_16); } }
        public static ImageSource Options_MovePropertiesFieldUp { get { return GetSource(_upArrow_16); } }
        public static ImageSource Options_MovePropertiesFieldDown { get { return GetSource(_downArrow_16); } }

        #endregion

        #region Component Editor

        public static ImageSource ComponentEditor_NewFrontView32 { get { return GetSource(_planogramNoColour_32); } }
        public static ImageSource ComponentEditor_NewFrontView16 { get { return GetSource(_planogramNoColour_32); } }
        public static ImageSource ComponentEditor_NewBackView32 { get { return GetSource(_fixtureBack_16); } }
        public static ImageSource ComponentEditor_NewBackView16 { get { return GetSource(_fixtureBack_16); } }
        public static ImageSource ComponentEditor_NewTopView32 { get { return GetSource(_fixtureTop_16); } }
        public static ImageSource ComponentEditor_NewTopView16 { get { return GetSource(_fixtureTop_16); } }
        public static ImageSource ComponentEditor_NewBottomView32 { get { return GetSource(_fixtureBottom_16); } }
        public static ImageSource ComponentEditor_NewBottomView16 { get { return GetSource(_fixtureBottom_16); } }
        public static ImageSource ComponentEditor_NewLeftView32 { get { return GetSource(_fixtureSideLeft_16); } }
        public static ImageSource ComponentEditor_NewLeftView16 { get { return GetSource(_fixtureSideLeft_16); } }
        public static ImageSource ComponentEditor_NewRightView32 { get { return GetSource(_fixtureSideRight_16); } }
        public static ImageSource ComponentEditor_NewRightView16 { get { return GetSource(_fixtureSideRight_16); } }
        public static ImageSource ComponentEditor_NewPerspectiveView32 { get { return GetSource(_planogram3D_32); } }
        public static ImageSource ComponentEditor_NewPerspectiveView16 { get { return GetSource(_planogram3D_32); } }
        public static ImageSource ComponentEditor_NewSubComponentListView32 { get { return GetSource(_fixtureDataView_32); } }
        public static ImageSource ComponentEditor_NewSubComponentListView16 { get { return GetSource(_fixtureDataView_32); } }

        #endregion

        #region Assortment Document

        public static ImageSource AssortmentDocument_RegionalProducts16 { get { return GetSource(_assortmentRegionalProduct_16); } }
        public static ImageSource AssortmentDocument_RegionalProducts { get { return GetSource(_assortmentRegionalProduct_32); } }
        public static ImageSource AssortmentDocument_RemoveSelectedProducts16 { get { return GetSource(_delete_16); } }
        public static ImageSource AssortmentDocument_RemoveSelectedProducts { get { return GetSource(_delete_32); } }
        public static ImageSource Assortment_SaveAsNewAssortment_16 { get { return GetSource(_saveAsNewAssortment_16); } }
        public static ImageSource Assortment_SaveAsNewAssortment_32 { get { return GetSource(_saveAsNewAssortment_32); } }
        public static ImageSource Assortment_SaveToLinkedAssortment_16 { get { return GetSource(_saveToLinkedAssortment_16); } }
        public static ImageSource Assortment_SaveToLinkedAssortment_32 { get { return GetSource(_saveToLinkedAssortment_32); } }
        public static ImageSource Assortment_UpdateAssortmentFromProduct_32 { get { return GetSource(_updateAssortmentFromProduct_32); } }

        public static ImageSource Assortment_FamilyRules { get { return GetSource(_familyRules_32); } }
        public static ImageSource Assortment_FamilyRules16 { get { return GetSource(_familyRules_16); } }
        public static ImageSource Assortment_ProductRules16 { get { return GetSource(_assortmentProductRule_16); } }
        public static ImageSource Assortment_ProductRules { get { return GetSource(_assortmentProductRule_32); } }
        public static ImageSource Assortment_RegionalProducts16 { get { return GetSource(_assortmentRegionalProduct_16); } }
        public static ImageSource Assortment_RegionalProducts { get { return GetSource(_assortmentRegionalProduct_32); } }
        public static ImageSource Assortment_LocalProducts16 { get { return GetSource(_assortmentLocalProduct_16); } }
        public static ImageSource Assortment_LocalProducts { get { return GetSource(_assortmentLocalProduct_32); } }
        public static ImageSource Assortment_RulePriorities { get { return GetSource(_assortmentRulePriorities_32); } }
        public static ImageSource Assortment_RulePriorities_StaticPriority { get { return GetSource(_locked_16); } }
        public static ImageSource Assortment_InventoryRules { get { return GetSource(_assortmentInventoryRules_32); } }
        public static ImageSource Assortment_LocationBuddys_AddNew { get { return GetSource(_locationBuddyAdd_32); } }
        public static ImageSource Assortment_LocationBuddys_Review { get { return GetSource(_locationBuddy_32); } }
        public static ImageSource Assortment_ProductBuddys { get { return GetSource(_productBuddy_32); } }
        public static ImageSource Assortment_ProductBuddys_AddNew { get { return GetSource(_productBuddyAdd_32); } }
        public static ImageSource Assortment_ProductBuddys_Wizard { get { return GetSource(_productBuddyAdd_32); } }
        public static ImageSource Assortment_ProductBuddys_Advanced { get { return GetSource(_productBuddyAdd_32); } }
        public static ImageSource AssortmentProductBuddyWizard_RemoveSourceProduct { get { return GetSource(_delete_16); } }
        public static ImageSource AssortmentProductBuddyWizard_AddSourceProduct { get { return GetSource(_add_16); }        }
        public static ImageSource AssortmentProductBuddyAdvancedAdd_RowStatusInvalid { get { return GetSource(_warning_20); }  }
        public static ImageSource AssortmentProductBuddyAdvancedAdd_RowStatusNew {  get { return GetSource(_add_16); }  }

        #endregion

        #region Validation Template

        public static ImageSource ValidationTemplateMaintenance_32
        {
            get { return GetSource(_validationTemplateMaintenance_32); }
        }

        #endregion

        #region Event Log

        public static ImageSource ErrorFilter_16 { get { return GetSource(_errorFilter_16); } }
        public static ImageSource EventLog_16 { get { return GetSource(_eventLog_16); } }
        public static ImageSource EventLog_32 { get { return GetSource(_eventLog_32); } }
        public static ImageSource EventLogFilter_16 { get { return GetSource(_filter_16); } }

        #endregion

        #region Consumer Decision Tree Document

        public static ImageSource CdtMaintenance_AddLevel16 { get { return GetSource(_hierarchyAddLevel_16); } }
        public static ImageSource CdtMaintenance_AddLevel { get { return GetSource(_hierarchyAddLevel_32); } }
        public static ImageSource CdtMaintenance_AddNode16 { get { return GetSource(_hierarchyAddNode_16); } }
        public static ImageSource CdtMaintenance_AddNode { get { return GetSource(_hierarchyAddNode_32); } }
        public static ImageSource CdtMaintenance_RemoveLevel16 { get { return GetSource(_hierarchyRemoveLevel_16); } }
        public static ImageSource CdtMaintenance_RemoveLevel { get { return GetSource(_hierarchyRemoveLevel_32); } }
        public static ImageSource CdtMaintenance_RemoveNode { get { return GetSource(_hierarchyRemoveNode_32); } }
        public static ImageSource CdtMaintenance_RemoveNode_16 { get { return GetSource(_hierarchyRemoveNode_16); } }
        public static ImageSource CdtMaintenance_ShowUnitEditWindow16 { get { return GetSource(_hierarchyEditNode_16); } }
        public static ImageSource CdtMaintenance_ShowUnitEditWindow { get { return GetSource(_hierarchyEditNode_32); } }
        public static ImageSource CdtMaintenance_SplitNode_16 { get { return GetSource(_cut_16); } }
        public static ImageSource CdtMaintenance_SplitNode_32 { get { return GetSource(_cut_32); } }
        public static ImageSource ConsumerDecisionTreeMaintenance { get { return GetSource(_cdt_32); } }

        public static ImageSource CdtMaintenanceEditNode_ProductsTab { get { return GetSource(_products_32); } }

        public static ImageSource ConsumerDecisionTreeDocument_LoadFromRepository16 { get { return GetSource(_open_16); } }
        public static ImageSource ConsumerDecisionTreeDocument_LoadFromRepository32 { get { return GetSource(_open_32); } }
        public static ImageSource ConsumerDecisionTreeDocument_AddSelectedProducts { get { return GetSource(_forward_24); } }
        public static ImageSource ConsumerDecisionTreeDocument_AddAllProducts { get { return GetSource(_addAll_24); } }
        public static ImageSource ConsumerDecisionTreeDocument_RemoveSelectedProducts { get { return GetSource(_back_24); } }
        public static ImageSource ConsumerDecisionTreeDocument_RemoveAllProducts { get { return GetSource(_removeAll_24); } }

        public static ImageSource LocationModel_AddNode { get { return GetSource(_hierarchyAddNode_32); } }

        #endregion

        #region Blocking Document

        public static ImageSource BlockingPlanDocument_LoadFromRepository16 { get { return GetSource(_open_16); } }
        public static ImageSource BlockingPlanDocument_LoadFromRepository32 { get { return GetSource(_open_32); } }
        public static ImageSource BlockingPlanDocument_SetCurrent16 { get { return GetSource(_blockMerchandiser_16); } }
        public static ImageSource BlockingPlanDocument_SetCurrent32 { get { return GetSource(_blockMerchandiser_32); } }
        public static ImageSource BlockingPlanDocument_VerticalTool32 { get { return GetSource(_verticalDivider_32); } }
        public static ImageSource BlockingPlanDocument_HorizontalTool32 { get { return GetSource(_horizontalDivider_32); } }
        public static ImageSource BlockingPlanDocument_ConnectTool32 { get { return GetSource(_connect_32); } }
        public static ImageSource BlockingPlanDocument_PointerTool32 { get { return GetSource(_cursor_32); } }
        public static ImageSource BlockingPlanDocument_DeleteDividerTool32 { get { return GetSource(_deleteDivider_32); } }
        public static ImageSource BlockingPlanDocument_ZoomToFitHeight32 { get { return GetSource(_fitToHeight_32); } }
        public static ImageSource BlockingPlanDocument_ZoomToFitHeight16 { get { return GetSource(_fitToHeight_32); } }
        public static ImageSource BlockingPlanDocument_ZoomToFit16 { get { return GetSource(_zoomToFit_16); } }
        public static ImageSource BlockingPlanDocument_ZoomIn16 { get { return GetSource(_zoomIn_16); } }
        public static ImageSource BlockingPlanDocument_ZoomOut16 { get { return GetSource(_zoomOut_16); } }
        public static ImageSource BlockingPlanDocument_GenerateProductSequence16 { get { return GetSource(_planogramSequenceGenerate_16); } }
        public static ImageSource BlockingPlanDocument_GenerateProductSequence32 { get { return GetSource(_planogramSequenceGenerate_32); } }
        public static ImageSource BlockingPlanDocument_LoadProductSequence32 { get { return GetSource(_planogramSequenceLoad_32); } }
        public static ImageSource BlockingPlanDocument_ShowPositions { get { return GetSource(_planogram_32); } }
        public static ImageSource BlockingPlanDocument_ClearBlocking32 { get { return GetSource(_blockMerchandiserClear_32); } }
        #endregion

        #region Editor Validation

        public static ImageSource EditorValidation_ItemsOk { get { return GetSource(_squareTick_16); } }
        public static ImageSource EditorValidation_ItemsWarning { get { return GetSource(_squareWarning_16); } }
        public static ImageSource EditorValidation_ItemsWarning48 { get { return GetSource(_squareWarning_48); } }
        public static ImageSource EditorValidation_ItemsRefresh { get { return GetSource(_refresh_16); } }
        public static ImageSource EditorValidation_ToggleDetailedDescription { get { return GetSource(_toggleShortLong_16); } }

        public static ImageSource EditorValidation_ComponentItemWarning { get { return GetSource(_invalidComponent_16); } }
        public static ImageSource EditorValidation_PositionItemWarning { get { return GetSource(_invalidPosition_16); } }
        public static ImageSource EditorValidation_ProductItemWarning { get { return GetSource(_invalidProduct_16); } }
        public static ImageSource EditorValidation_InventoryItemWarning { get { return GetSource(_invalidInventory_16); } }
        public static ImageSource EditorValidation_RuleItemWarning { get { return GetSource(_invalidRule_16); } }
        #endregion

        #region PlanogramFileTemplateEditor

        public static ImageSource PlanogramFileTemplateEditor_IsDoubleMapped { get { return GetSource(_warning_20); } }

        #endregion

        #region Planogram Comparison Document

        public static ImageSource PlanogramComparisonPlanDocument_ShowSettings16 { get { return GetSource(_propertiesSettings_16); } }
        public static ImageSource PlanogramComparisonPlanDocument_ShowSettings32 { get { return GetSource(_propertiesSettings_32); } }
        public static ImageSource PlanogramComparisonPlanDocument_Compare16 { get { return GetSource(_refresh_16); } }
        public static ImageSource PlanogramComparisonPlanDocument_Compare32 { get { return GetSource(_planogramCompareRefresh_32); } }

        #endregion

        #region Category Insight Document

        public static ImageSource CategoryInsightPlanDocument_NextDocument {get{return GetSource(_forward_16);}}
        public static ImageSource CategoryInsightPlanDocument_PreviousDocument { get { return GetSource(_back_16); } }
        public static ImageSource CategoryInsightPlanDocument_CategoryRole { get { return GetSource(_categoryRole32); } }
        public static ImageSource CategoryInsightPlanDocument_CategoryGoals { get { return GetSource(_categoryGoals32); } }
        public static ImageSource CategoryInsightPlanDocument_CategoryTactics { get { return GetSource(_categoryTactics32); } }

        public static ImageSource CategoryInsightPlanDocument_CriticalPriority { get { return GetSource(_priorityCritical16); } }
        public static ImageSource CategoryInsightPlanDocument_HighPriority { get { return GetSource(_priorityHigh16); } }
        public static ImageSource CategoryInsightPlanDocument_MediumPriority { get { return GetSource(_priorityMedium16); } }
        public static ImageSource CategoryInsightPlanDocument_LowPriority { get { return GetSource(_priorityLow16); } }
        public static ImageSource CategoryInsightPlanDocument_ProgressComplete { get { return GetSource(_apply_16); } }
           
        #endregion

        #region Sequence Template Ribbon

        public static ImageSource SequenceTemplateRibbon_ShowSequenceGroups { get { return GetSource(_planogramSequenceGroups_32); } }
        public static ImageSource SequenceTemplateRibbon_AddSequenceSubGroup { get { return GetSource(_planogramSequenceSubGroupAdd_32); } }
        public static ImageSource SequenceTemplateRibbon_RemoveSequenceSubGroup { get { return GetSource(_planogramSequenceSubGroupRemove_32); } }
        public static ImageSource SequenceTemplateRibbon_ReverseSequenceOrder { get { return GetSource(_selectionReverseSequence_32); } }

        #endregion

        #region Search Screen 

        public static ImageSource SearchScreen_LockedField { get { return GetSource(_lockedByCurrentUser_16); } }

        #endregion

        #region Product Buddy
        public static ImageSource ProductBuddy { get { return GetSource(_productBuddy_32); } }
       
        #endregion

        public static ImageSource TODO { get { return GetSource(_support_32); } }
    }
}
