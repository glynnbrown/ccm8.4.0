﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// L.Ineson
//  Created.
// V8-25767 : A.Silva 
//  Added ISingleInstanceApp and a custom Main to ensure only one instance runs, 
//              and that arguments are passed to the running instance.
// V8-26159 : L.Ineson
//  Added gfs service types registration
// V8-26396 : L.Ineson
//  Moved authenticate into loading window.
// V8-27528 : M.Shelley
//  Added Desaware product licensing
// V8-27683 : J.Pickup
//  Added .pog file association.
// V8-27826 : L.Ineson
//  App now attempts to unlock all plans before shutting down after an unexpected error.
// V8-27923 : J.Pickup
//  Aspose licensing added.
// V8-27732 : L.Ineson
//  Added ceip window and moved Desaware product licensing call to loading window.
// V8-28000 : L.ineson
//  Added wrap around process args to ensure it does not fall over.
// V8-28001 : A.Kuszyk
//  Added event to indicate when ViewState has been instantiated.
// V8-26763 : A.Silva
//      Amended App_DispatcherUnhandledException so that we hold a reference to the 
//          initial reference until the end, even if the user restarts.
//      Amended App_DispatcherUnhandledException so that the the single instance is 
//          cleaned up (freeing the mutex that keeps it unique, and allowing a new instance to be the new unique instance).
#endregion
#region Version History: (CCM 8.01)
// CCM-28755 : L.Ineson
//  Amended to use new window service
// V8-28676 : A.Silva
//      Amended to initialize the real image provider.
// V8-28951 : A.Silva
//      Amended RegisterRealImageProvider so that it can retrieve the current entity system settings if necessary.

#endregion
#region Version History: CCM802
// V8-28964 : A.Silva
//      Enabled Gfs option to RegisterRealImageProvider.
// V8-28984 : A.Silva
//      Added a handler to retry registering the real image provider if it should have used the repository, but there was no connection yet.
//      Amended GetImageSettings so that it will now return the local settings if set to return from repository, but no connection exists.
#endregion
#region Version History: CCM810
// V8-30243 : L.Ineson
// Added try catch around the check for existing process.
#endregion
#region Version History: CCM811
// V8-30473 : L.Ineson
//  Removed overriding line which meant that repository 
//  settings could never be used for real images.
#endregion

#region Version History: CCM820

// V8-31087 : M.Brumby
//  Enable silent opening of external file types
// V8-31325 : A.Silva
//  Amended ProcessArgs so that it can determine better when a plan comes from the repository or the file system.

#endregion

#region Version History: CCM 8.3.0
// V8-32191 : A.Probyn
//  Added handling in the App_DispatcherUnhandledException for clipboard exception
// V8-32675 : L.Ineson
//  Added UITaskScheduler
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Security;
using System.Threading;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Licensing;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Editor.Client.Wpf.Startup;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Ccm.Services;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Gibraltar.Agent;
using Microsoft.Shell;
using ServiceContainer = Galleria.Ccm.Services.ServiceContainer;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Ccm.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Controls.Wpf.Converters;
using Galleria.Ccm.Common.Wpf.Converters;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Converters;
using Galleria.Framework.Controls.Wpf.Themes.Colours;

namespace Galleria.Ccm.Editor.Client.Wpf
{
    /// <summary>
    ///  Interaction logic for App.xaml
    /// </summary>
    public partial class App : ISingleInstanceApp
    {
        #region Nested Classes

        /// <summary>
        ///     The SessionException class allows an unhandled exception to be wrapped in such a way that
        ///     its message includes the unique Session ID for this ISO session, a property that can be used to
        ///     find this specific session in a Gibraltar repository.
        /// </summary>
        private class SessionException : Exception
        {
            public SessionException(Exception innerException) :
                base(String.Format(
                    CultureInfo.InvariantCulture,
                    Wpf.Resources.Language.Message.UnhandledException_Message,
                    Log.SessionSummary.Properties[_sessionIdName],
                    Environment.NewLine,
                    innerException.Message),
                    innerException)
            {
                SessionId = Log.SessionSummary.Properties[_sessionIdName];
            }

            public String SessionId { get; set; }
        }

        #endregion

        #region Constants

        /// <summary>
        ///     Unique String Id for ISingleInstance to detect other instances running.
        /// </summary>
        private const String Unique = "CCM Editor";

        private const String _sessionIdName = "Session ID";

        #endregion

        #region Fields

        /// <summary>
        ///     Array of String passed as arguments to the application on startup.
        /// </summary>
        private static String[] _args;
        private static MainPageViewModel _unitTestingViewModel; // the mainpage viewmodel to return when unit testing.
        private static ViewState _viewState;
        private static LicenseState _licenseState;
        private static IRealImageProvider _currentImageProvider;

        #endregion

        #region Events

        /// <summary>
        /// Fired when the ViewState static property will definitely have been instantiated.
        /// </summary>
        public static event EventHandler ViewStateLoaded;

        #endregion

        #region Constructors

        /// <summary>
        ///     Creates a new instance of this type
        /// </summary>
        public App()
        {
            Startup += App_Startup;
            Exit += App_Exit;

            // Attach to the event to globally hand exceptions before they hit the os
            DispatcherUnhandledException += App_DispatcherUnhandledException;

            //Register the viewstate.
            CCMClient.Register(ViewState);
        }

        static App()
        {
            _viewState = new ViewState();
            ViewStateLoaded?.Invoke(null, EventArgs.Empty);
        }

        #endregion

        #region App Event Handlers

        /// <summary>
        ///     Called once the application has been started up
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">The event arguments</param>
        private void App_Startup(Object sender, StartupEventArgs e)
        {
            // unsubscribe from the startup event
            Startup -= App_Startup;

            // Store any arguments passed to the application.
            _args = e.Args;

            UserSystemSetting systemSettings = GetSystemSettings();
            
            //Override the current culture info
            // This determines the numberformats/currency etc that will be used by the app.
            // The following line ensures that it will be picked up from the pc ControlPanel-> Region and Language -> Format setting.
            FrameworkElement.LanguageProperty.OverrideMetadata(
                typeof(FrameworkElement),
                new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

            //set the display language
            //This is the language that the application will be displayed in.
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(systemSettings.DisplayLanguage);

            // if we are unit testing, then we do not perform
            // authentication or load the main page organiser
            if (IsUnitTesting) return;

            //Load the required dictionaries for themes and styles.
            LoadThemeResources();

            ApplyAsposeLicenses();

            //Have to set the below because of the dialog win
            this.ShutdownMode = ShutdownMode.OnExplicitShutdown;

            //CEIP gilbraltar integration
            CEIPViewModel ceipViewModel = new CEIPViewModel(systemSettings);
            if (ceipViewModel.CEIPWindowRequired())
            {
                CEIPWindow win = new CEIPWindow(ceipViewModel);
                win.ShowDialog();
            }
            
            // initialize and start the logging session
            Log.Initializing += Log_Initializing;
            Log.StartSession();

            //register gfs service types
            FoundationServiceClient.RegisterGFSServiceClientTypes();

            //register the image renderer
            PlanogramImagesHelper.RegisterPlanogramImageRenderer<Galleria.Framework.Planograms.Controls.Wpf.Helpers.PlanogramImageRenderer>();

            // Initialize a the real image provider with the user settings.
            RegisterRealImageProvider();
            ViewState.Settings.ModelChanged += SettingsOnModelChanged;

            //register the window and modal busy worker services
            ServiceContainer.RegisterServiceObject<IWindowService>(new WpfWindowService());
            ServiceContainer.RegisterServiceObject<IModalBusyWorkerService>(new WpfModalBusyWorkerService());

            //nb - db setup is not to be shown on startup.

            // Logon
            BeginSession();
        }

        /// <summary>
        ///     Called when the application is exiting
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">The event arguments</param>
        private void App_Exit(Object sender, ExitEventArgs e)
        {
            // unsubscribe from the exit event
            Exit -= App_Exit;

            // end the logging session
            Log.EndSession();
        }

        /// <summary>
        ///     Handler to catch global exceptions.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void App_DispatcherUnhandledException(Object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            // indicate that this exception has been handled
            e.Handled = true;

            //  V8-26763 Store the reference to the first app, so that any cleanup after a possible restart references it, and not the new instance.
            Application current = App.Current;

            // get the base exception
            Exception baseException = e.Exception.GetBaseException();

            if (current.MainWindow is MainPageOrganiser && baseException is Csla.PropertyLoadException)
            {
                //just ignore.
                Debug.Fail(baseException.Message);
                return;
            }

            //V8-32191 - Clipboard exception
            var comException = baseException as System.Runtime.InteropServices.COMException;
            if (comException != null && comException.ErrorCode == -2147221040)
            {
                //just ignore.
                Debug.Fail(baseException.Message);
                return;
            }

            // if a security exception has been thrown
            // then display a standard dialog box to the
            // user and allow them to continue
            // otherwise log the exception using gibralter
            // and shut down the application
            // we only show the dialog if the main window has
            // been loaded, as any security issue before that
            // point means that the application cannot start
            if (current.MainWindow is MainPageOrganiser && (baseException is SecurityException))
            {
                //turn off busy cursor
                Dispatcher.BeginInvoke((Action)(() => { System.Windows.Input.Mouse.OverrideCursor = null; }));

                ModalMessage message = new ModalMessage
                {
                    Title = current.MainWindow.GetType().Assembly.GetName().Name,
                    Header = Message.SecurityDialog_Exception,
                    Description = baseException.Message,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner,
                    ButtonCount = 1,
                    Button1Content = Message.Generic_OK,
                    DefaultButton = ModalMessageButton.Button1,
                    MessageIcon = ImageResources.Error_32
                };
                App.ShowWindow(message, true);
                return;
            }

            //  V8-26763 Release the one instance mutex. After this point the app will either be shut down or restarted as a new instance.
            SingleInstance<App>.Cleanup();

            // log the exception to gibralter
            Log.ReportException(new SessionException(e.Exception), "Unhandled Exception", false, true); // GFS-15573

            //try to unlock all plans held by the main page viewmodel
            // so that they don't get let locked as we crash out.
            MainPageViewModel mainPage = App.MainPageViewModel;
            if (mainPage != null)
            {
                foreach (var planController in mainPage.PlanControllers)
                {
                    Object planId = planController.SourcePlanogram.ParentPackageView.Model.Id;
                    Package.TryUnlockPackageById(planId, DomainPrincipal.CurrentUserId, PackageLockType.User);
                }
            }

            //shutdown the app.
            current.Shutdown();
        }

        /// <summary>
        ///     Invoked when the model for the user settings changes.
        ///     TODO: Move this into viewstate.
        /// </summary>
        private static void SettingsOnModelChanged(Object sender, ViewStateObjectModelChangedEventArgs<UserEditorSettings> e)
        {
            RegisterRealImageProvider();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Entry point for the application. Makes sure that there is only one instance running. It will pass any arguments to
        ///     the running instance if a new one is started.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            if (!SingleInstance<App>.InitializeAsFirstInstance(Unique)) return;

            var app = new App();
            app.InitializeComponent();
            app.Run();

            // Allow single instance code to perform cleanup opearions.
            SingleInstance<App>.Cleanup();
        }

        public static void ProcessArgs()
        {
            if (MainPageViewModel == null) return;

            try
            {
                //Args may either come from repository for from .pog file (extension)
                List<String> argsList = _args.ToList();

                //  Check whether we have at least one argument.
                String filePath = argsList.FirstOrDefault();
                if (filePath == null) return;

                var extensions =
                        new List<String>(new[]
                                         {
                                             Constants.PlanogramFileExtension,
                                             PlanogramImportFileTypeHelper.Extension[PlanogramImportFileType.SpacemanV9],
                                             PlanogramImportFileTypeHelper.Extension[PlanogramImportFileType.ProSpace],
                                             PlanogramImportFileTypeHelper.Extension[PlanogramImportFileType.Apollo]
                                         });

                //  Check whether a valid file is being requested.
                if (argsList.Count == 1 && extensions.Any(s => filePath.EndsWith(s, StringComparison.OrdinalIgnoreCase)) && System.IO.File.Exists(filePath))
                {
                    MainPageViewModel.OpenPlanogramsFromFileSystem(filePath);
                }
                //  Check whether it is a repository request.
                else if (argsList.TrueForAll(s => s.Contains("|")))
                {
                    MainPageViewModel.OpenPlanogramsFromRepository(argsList);
                }

                _args = new List<String>().ToArray();
            }
            catch (Exception)
            {
                //just carry on.
            }
        }

        /// <summary>
        /// Checks that the application is licensed.
        /// </summary>
        public Boolean CheckLicense()
        {
            // Setup / verify licensing
            var licenceObject = new LicenceSetup(/*runAsync*/false);

            if (licenceObject.ApplicationShutdown)
            {
                this.Shutdown();
                return false;
            }

            _licenseState = licenceObject.LicenseState;

            // Check if the license setup has set the Application LicenseState property
            if (this.Properties["LicenseState"] == null)
            {
                // There is no current App LicenseState property, so set a reference in the App properties for use by the Help page
                this.Properties["LicenseState"] = _licenseState;
            }
            return true;
        }

        /// <summary>
        /// Returns the system settings model for this app.
        /// </summary>
        public static UserSystemSetting GetSystemSettings()
        {
            UserSystemSetting settings;

            try
            {
                settings = UserSystemSetting.FetchUserSettings(IsUnitTesting);
            }
            catch (Exception)
            {
                settings = UserSystemSetting.NewUserSystemSetting();
            }
            return settings;
        }


        /// <summary>
        /// Loads resource dictionaries required for themes and styling.
        /// </summary>
        private void LoadThemeResources()
        {
            //load converters
            ResourceDictionary convertersDictionary = new ResourceDictionary();
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterStringFormat, new StringFormatConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterUTCToDate, new UTCToLocalDateConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterUTCToDateTime, new UTCToLocalDateTimeConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterUTCToLocalTime, new UTCToLocalTimeConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterConditionToBool, new ConditionToBoolConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterConditionToVisibility, new ConditionToVisibilityConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterDictionaryKeyToValue, new DictionaryKeyToValueConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterCollectionToView, new CollectionToViewConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterMultiConditionToBool, new MultiConditionToBoolConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterValueToPercentage, new ValueToPercentageConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterMultiConditionToVisibility, new MultiConditionToVisibilityConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterTrueFalseValueProvider, new TrueFalseValueProvider());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterIntToColor, new IntToColorConverter());
            convertersDictionary.Add(ResourceKeys.ConverterAreEqual, new AreEqualConverter());
            convertersDictionary.Add(ResourceKeys.ConverterUnitDisplay, new UnitDisplayConverter());
            convertersDictionary.Add(ResourceKeys.ConverterBytesToImageSource, new BytesToImageSourceConverter());
            convertersDictionary.Add(ResourceKeys.ConverterRadiansToDegrees, new RadiansToDegreesConverter());
            convertersDictionary.Add(ResourceKeys.ConverterToPercentage, new ToPercentageConverter());
            convertersDictionary.Add(ResourceKeys.ConverterConditionalString, new ConditionalStringConverter());
            convertersDictionary.Add(ResourceKeys.ConverterFieldTextToDisplay, new FieldTextToDisplayConverter());
            convertersDictionary.Add(ResourceKeys.ConverterIsTypeToVisiblity, new IsTypeTovisibilityConverter());
            convertersDictionary.Add(ResourceKeys.ConverterIsType, new IsTypeConverter());
            convertersDictionary.Add(ResourceKeys.ConverterImplementsInterfaceToVisiblity, new ImplementsInterfaceToVisibilityConverter());
            convertersDictionary.Add(ResourceKeys.ConverterImplementsInterfaceToBool, new ImplementsInterfaceToBoolConverter());
            convertersDictionary.Add(ResourceKeys.ConverterColorPickerRecentColors, new ColorPickerRecentColorsConverter());
            Application.Current.Resources.MergedDictionaries.Add(convertersDictionary);

            //Colours
            Application.Current.Resources.MergedDictionaries.Add((ResourceDictionary)Application.LoadComponent(new Uri("/Fluent;Component/Themes/Office2010/Blue.xaml", UriKind.Relative)));
            
            if (SystemParameters.HighContrast) Application.Current.Resources.MergedDictionaries.Add(HighContrastHelper.GetFluentHighContrastResourceDictionary());

            Application.Current.Resources.MergedDictionaries.Add((ResourceDictionary)Application.LoadComponent(new Uri("/Galleria.Framework.Controls.Wpf;component/Themes/Colours/ColorsBlue.xaml", UriKind.Relative)));
            
            if (SystemParameters.HighContrast) Application.Current.Resources.MergedDictionaries.Add(HighContrastHelper.GetGalleriaHighContrastResourceDictionary());
            //framework resources
            Application.Current.Resources.MergedDictionaries.Add((ResourceDictionary)Application.LoadComponent(new Uri("/Galleria.Framework.Controls.Wpf;component/Themes/Theme_Common.xaml", UriKind.Relative)));
            Application.Current.Resources.MergedDictionaries.Add((ResourceDictionary)Application.LoadComponent(new Uri("/Galleria.Framework.Controls.Wpf;component/Themes/Colours_Common.xaml", UriKind.Relative)));

            //planogram framework resources
            Application.Current.Resources.MergedDictionaries.Add((ResourceDictionary)Application.LoadComponent(new Uri("/Galleria.Framework.Planograms.Controls.Wpf;component/Themes/Theme_Common.xaml", UriKind.Relative)));

            //local resources
            Application.Current.Resources.MergedDictionaries.Add((ResourceDictionary)Application.LoadComponent(new Uri("/Galleria.Ccm.Common.Wpf;component/Themes/Res_Main.xaml", UriKind.Relative)));
            Application.Current.Resources.MergedDictionaries.Add((ResourceDictionary)Application.LoadComponent(new Uri("Resources/Theme/Res_Main.xaml", UriKind.Relative)));

            //Set the application colour brush
            Application.Current.Resources.Add("ApplicationColourBrush",
                //(SystemParameters.HighContrast) ? SystemColors.HighlightBrush :
                new System.Windows.Media.SolidColorBrush((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FF027EB3")));

            SetupColourPicker();

        }

        /// <summary>
        /// this is to added the envent selected color to color picker
        /// </summary>
        private static void SetupColourPicker()
        {
            Style currentColorPickerStyle = (Application.Current.Resources[typeof(ColorPickerBox)] as Style);
            if (currentColorPickerStyle == null) return;

            Style colorPickerBoxStyle = new Style(typeof(ColorPickerBox), currentColorPickerStyle);
            colorPickerBoxStyle.Setters.Add(new EventSetter(ColorPickerBox.SelectedColorChangedEvent, (RoutedEventHandler)(OnSelectedColorChanged)));
            colorPickerBoxStyle.Seal();
            Application.Current.Resources[typeof(ColorPickerBox)] = colorPickerBoxStyle;
        }

        public static void OnSelectedColorChanged(object sender, RoutedEventArgs e)
        {
            var colorPicker = e.OriginalSource as ColorPickerBox;
            if (colorPicker != null)
            {
                App.ViewState.ApplyRecentColor(colorPicker.SelectedColor);
            }
        }

        #endregion

        #region Authentication

        private void BeginSession()
        {
            UITaskScheduler = System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext();

            //start the session - the loading window will take care of the rest. (inc. authentication)
            var loadingWin = new LoadingWindow { WindowStartupLocation = WindowStartupLocation.CenterScreen };
            loadingWin.Show();
        }

        #endregion

        #region Logging

        /// <summary>
        ///     Called when the log is initialized
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">The event arguments</param>
        private void Log_Initializing(Object sender, LogInitializingEventArgs e)
        {
            // configure the application to log to the service
            e.Configuration.Server.UseGibraltarService = true;
            e.Configuration.Server.CustomerName = "galleria";
            e.Configuration.Server.SendAllApplications = true;
            e.Configuration.Server.PurgeSentSessions = true;
            e.Configuration.Server.UseSsl = true;
            e.Configuration.Properties[_sessionIdName] = Guid.NewGuid().ToString();
            e.Configuration.SessionFile.EnableFilePruning = true; // ISO-13436
            e.Configuration.SessionFile.MaxLocalDiskUsage = 200; // ISO-13436
            e.Configuration.Publisher.EnableAnonymousMode = false; // GFS-14882
#if !DEBUG
            // now determine if we are running within the IDE.
            // normally, you shouldn't change the behaviour of
            // an application depending on whether a debugger
            // is attached or not, however, we want to be able
            // to still use the logging facilities in debug
            // builds when they are distrubuted to people such
            // as qa and consultancy. Therefore we have used
            // this method instead of conditional compilation
            //if (!Debugger.IsAttached) // GFS-14882
            //{
            //    // set auto-sending of debug information to what the user has selected
            //    e.Configuration.Server.AutoSendSessions = _CEIPViewModel.IsParticipate();
            //}
#endif
        }

        #endregion

        #region Real Image Handling

        /// <summary>
        ///     Determines which real image provider to register based on the current user settings.
        /// </summary>
        private static void RegisterRealImageProvider()
        {
            IRealImageProviderSettings settings = GetImageSettings();
            if (settings == null) return;

            //  We have settings, either reload the current provider or create a new one, depending on the source setting.
            switch (settings.ProductImageSource)
            {
                case RealImageProviderType.Gfs:
                    if (_currentImageProvider is RealImageProviderForGfs)
                    {
                        _currentImageProvider.Reload(settings);
                    }
                    else
                    {
                        if (_currentImageProvider != null) _currentImageProvider.Dispose();
                        _currentImageProvider = new RealImageProviderForGfs(ViewState.EntityId, settings);
                    }
                    break;
                case RealImageProviderType.Repository:
                // Commented out so that it is not used for now, use the folder provider for now.
                //if (_currentImageProvider is RealImageProviderForRepository)
                //    _currentImageProvider.Reload(settings);
                //else
                //    if (_currentImageProvider != null) _currentImageProvider.Dispose();
                //    _currentImageProvider = new RealImageProviderForRepository(ViewState.EntityId, settings);
                //break;
                case RealImageProviderType.Folder:
                    if (_currentImageProvider is RealImageProviderForFolder)
                    {
                        _currentImageProvider.Reload(settings);
                    }
                    else
                    {
                        if (_currentImageProvider != null) _currentImageProvider.Dispose();
                        _currentImageProvider = new RealImageProviderForFolder(settings);
                    }
                    break;
            }

            PlanogramImagesHelper.RegisterRealImageProvider(_currentImageProvider);
        }

        /// <summary>
        ///     Gets the current Image Settings to be used.
        /// </summary>
        /// <returns></returns>
        /// <remarks>TODO move into the view state class.</remarks>
        private static IRealImageProviderSettings GetImageSettings()
        {
            IRealImageProviderSettings imageSettings = ViewState.Settings.Model;
            if (imageSettings == null) return null;

            //if the local setting is not pointing at the repository one then
            // just return local
            if (imageSettings.ProductImageSource != RealImageProviderType.Repository)
                return imageSettings;

            //if we are not connected to a repository then just wait until we are.
            if (!ViewState.IsConnectedToRepository)
            {
                ViewState.EntityIdChanged += ViewStateOnEntityIdChanged;
                return imageSettings;
            }

            //  Get the system settings if we need to use the repository ones and there is a connection.
            Entity entity = Entity.FetchById(ViewState.EntityId);
            if (entity != null) imageSettings = entity.SystemSettings;


            return imageSettings;
        }

        /// <summary>
        ///     Invoked when the Entity Id has changed in the ViewState.
        ///     TODO: Move this into viewstate.
        /// </summary>
        /// <remarks>This handler will unsubscribe itself every time it is fired.</remarks>
        private static void ViewStateOnEntityIdChanged(Object sender, EventArgs eventArgs)
        {
            ViewState.EntityIdChanged -= ViewStateOnEntityIdChanged;
            RegisterRealImageProvider();
        }

        #endregion

        #region ISingleInstanceApp Members

        public Boolean SignalExternalCommandLineArgs(IList<String> args)
        {
            // handle command line arguments of second instance;
            args.Remove(args[0]);
            _args = args.ToArray();
            ProcessArgs();
            return true;
        }

        #endregion

        #region Static Properties

        /// <summary>
        ///     Gets/Sets whether we are currently unit testing.
        /// </summary>
        public static Boolean IsUnitTesting
        {
            get { return CCMClient.IsUnitTesting; }
        }

        /// <summary>
        ///     Returns the mainpage viewmodel for this app.
        /// </summary>
        public static MainPageViewModel MainPageViewModel
        {
            get
            {
                if (IsUnitTesting)
                {
                    if (_unitTestingViewModel == null)
                    {
                        _unitTestingViewModel = new MainPageViewModel();
                    }
                    return _unitTestingViewModel;
                }
                var org = Current.MainWindow as MainPageOrganiser;
                if (org != null)
                {
                    return org.ViewModel;
                }
                return null;
            }
        }

        /// <summary>
        ///     The application view state
        /// </summary>
        public static ViewState ViewState
        {
            get { return _viewState; }
        }

        /// <summary>
        /// The Application LicenseState
        /// </summary>
        public static LicenseState LicenseState
        {
            get
            {
                if (_licenseState == null)
                {
                    _licenseState = new LicenseState();
                }
                return _licenseState;
            }
        }

        /// <summary>
        /// Provides a reference back to the ui scheduler.
        /// </summary>
        internal static System.Threading.Tasks.TaskScheduler UITaskScheduler 
        { 
            get; 
            private set;
        }

        #endregion

        #region Static Helpers

        /// <summary>
        /// License doesn't exist for developer, GABS will swap in on release build.
        /// </summary>
        [Conditional("RELEASE")]
        private static void ApplyAsposeLicenses()
        {
            try
            {
                // set aspose cells license
                var cellsLicense = new Aspose.Cells.License();
                cellsLicense.SetLicense("Aspose.Total.10.lic");

                // set aspose pdf license
                var pdfLicense = new Aspose.Pdf.License();
                pdfLicense.SetLicense("Aspose.Total.10.lic");
            }
            catch { /*Unexpected exception, proceed without license.*/ }
        }

        /// <summary>
        ///     Initializes for a new unit test.
        /// </summary>
        public static void InitialiseForUnitTest()
        {
            _viewState = new ViewState();

            _unitTestingViewModel = null;

            CCMClient.Register(ViewState);
            CCMClient.MarkAsUnitTesting();
        }

       [Obsolete("Use Window service instead")]
       [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static void ShowWindow(Window window, Boolean isModal)
        {
            WindowHelper.ShowWindow(window, isModal);
        }

        [Obsolete("Use Window service instead")]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static void ShowWindow(Window window, Window parentWindow, Boolean isModal)
        {
            WindowHelper.ShowWindow(window, parentWindow, isModal);
        }

        #endregion
    }
}