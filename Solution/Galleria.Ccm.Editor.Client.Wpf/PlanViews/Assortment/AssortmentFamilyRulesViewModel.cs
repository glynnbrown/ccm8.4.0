﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014
#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//  Adapted from Workflow client.
#endregion

#region Version History : CCM830
// V8-32560 : A.Silva
//  Removed AddAll and RemoveAll commands.
// V8-32718 : A.Probyn ~ Updated so full product attributes can be displayed.
// V8-32718 : D.Pleasance
//  Added planItems to constructor. These are passed through to enable displaying of product / position attributes.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using System.ComponentModel;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Collections;
using System.Collections.ObjectModel;
using System.Windows;
using Galleria.Framework.Helpers;
using System.Globalization;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using System.Diagnostics;
using Galleria.Ccm.Editor.Client.Wpf.Common;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    public class AssortmentFamilyRulesViewModel
        : ViewModelAttachedControlObject<AssortmentFamilyRulesWindow>, IDataErrorInfo
    {
        #region Fields

        private IEnumerable<IPlanItem> _planItems;

        // The selected assortment
        private PlanogramAssortment _selectedAssortment;

        //The list of current family rules for the assortment
        private BulkObservableCollection<AssortmentFamilyRuleRow> _currentAssortmentFamilyRules = new BulkObservableCollection<AssortmentFamilyRuleRow>();
        private ReadOnlyBulkObservableCollection<AssortmentFamilyRuleRow> _currentAssortmentFamilyRulesRO;

        //The current Assortment Region being edited
        private AssortmentFamilyRuleRow _currentAssortmentFamilyRule;

        //The list of available products for the family rule to choose from 
        private BulkObservableCollection<PlanogramAssortmentProductView> _availableProducts = new BulkObservableCollection<PlanogramAssortmentProductView>();
        private ReadOnlyBulkObservableCollection<PlanogramAssortmentProductView> _availableProductsRO;
        //The currently selected product(s) in the available products list
        private ObservableCollection<PlanogramAssortmentProductView> _selectedAvailableProducts = new ObservableCollection<PlanogramAssortmentProductView>();

        //The list of taken assortment products already part of the family
        private BulkObservableCollection<PlanogramAssortmentProductView> _assignedProducts = new BulkObservableCollection<PlanogramAssortmentProductView>();
        private ReadOnlyBulkObservableCollection<PlanogramAssortmentProductView> _assignedProductsRO;
        //The currently selected product(s) in the taken products list
        private ObservableCollection<PlanogramAssortmentProductView> _selectedAssignedProducts = new ObservableCollection<PlanogramAssortmentProductView>();

        private String _selectedFamilyRuleName = String.Empty; // the family rule name
        private PlanogramAssortmentProductFamilyRuleType _selectedFamilyRuleType = PlanogramAssortmentProductFamilyRuleType.None; // the family rule type
        private Byte? _selectedFamilyRuleValue = null; // the family rule value

        private List<AssortmentFamilyRuleRow> _deletedFamilyRuleRows = new List<AssortmentFamilyRuleRow>();

        #endregion

        #region Binding properties

        public static readonly PropertyPath SelectedAssortmentProperty =
            WpfHelper.GetPropertyPath<AssortmentFamilyRulesViewModel>(p => p.SelectedAssortment);
        public static readonly PropertyPath CurrentAssortmentFamilyRulesProperty =
            WpfHelper.GetPropertyPath<AssortmentFamilyRulesViewModel>(p => p.CurrentAssortmentFamilyRules);
        public static readonly PropertyPath CurrentAssortmentFamilyRuleProperty =
            WpfHelper.GetPropertyPath<AssortmentFamilyRulesViewModel>(p => p.CurrentAssortmentFamilyRule);
        public static readonly PropertyPath AvailableProductsProperty =
            WpfHelper.GetPropertyPath<AssortmentFamilyRulesViewModel>(p => p.AvailableProducts);
        public static readonly PropertyPath SelectedAvailableProductsProperty =
            WpfHelper.GetPropertyPath<AssortmentFamilyRulesViewModel>(p => p.SelectedAvailableProducts);
        public static readonly PropertyPath TakenProductsProperty =
            WpfHelper.GetPropertyPath<AssortmentFamilyRulesViewModel>(p => p.AssignedProducts);
        public static readonly PropertyPath SelectedTakenProductsProperty =
            WpfHelper.GetPropertyPath<AssortmentFamilyRulesViewModel>(p => p.SelectedAssignedProducts);
        public static readonly PropertyPath SelectedFamilyRuleNameProperty =
            WpfHelper.GetPropertyPath<AssortmentFamilyRulesViewModel>(p => p.SelectedFamilyRuleName);
        public static readonly PropertyPath SelectedFamilyRuleTypeProperty =
            WpfHelper.GetPropertyPath<AssortmentFamilyRulesViewModel>(p => p.SelectedFamilyRuleType);
        public static readonly PropertyPath SelectedFamilyRuleValueProperty =
            WpfHelper.GetPropertyPath<AssortmentFamilyRulesViewModel>(p => p.SelectedFamilyRuleValue);
        public static readonly PropertyPath ValidFamilyRuleValueProperty =
            WpfHelper.GetPropertyPath<AssortmentFamilyRulesViewModel>(p => p.ValidFamilyRuleValue);

        //commands
        public static readonly PropertyPath AddFamilyRuleCommandProperty =
            WpfHelper.GetPropertyPath<AssortmentFamilyRulesViewModel>(p => p.AddFamilyRuleCommand);
        public static readonly PropertyPath RemoveCommandProperty =
            WpfHelper.GetPropertyPath<AssortmentFamilyRulesViewModel>(p => p.RemoveCommand);
        public static readonly PropertyPath CancelCommandProperty =
            WpfHelper.GetPropertyPath<AssortmentFamilyRulesViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath ApplyCommandProperty =
            WpfHelper.GetPropertyPath<AssortmentFamilyRulesViewModel>(p => p.ApplyCommand);
        public static readonly PropertyPath ApplyAndCloseCommandProperty =
            WpfHelper.GetPropertyPath<AssortmentFamilyRulesViewModel>(p => p.ApplyAndCloseCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Property for the selected Assortment
        /// </summary>
        public PlanogramAssortment SelectedAssortment
        {
            get { return _selectedAssortment; }
            set
            {
                _selectedAssortment = value;
                OnPropertyChanged(SelectedAssortmentProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the family rule name
        /// </summary>
        public String SelectedFamilyRuleName
        {
            get { return _selectedFamilyRuleName; }
            set
            {
                _selectedFamilyRuleName = value;
                if (this.CurrentAssortmentFamilyRule != null)
                {
                    this.CurrentAssortmentFamilyRule.FamilyRuleName = _selectedFamilyRuleName;
                }
                OnPropertyChanged(SelectedFamilyRuleNameProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the family rule Type
        /// </summary>
        public PlanogramAssortmentProductFamilyRuleType SelectedFamilyRuleType
        {
            get { return _selectedFamilyRuleType; }
            set
            {
                _selectedFamilyRuleType = value;
                if (this.CurrentAssortmentFamilyRule != null)
                {
                    this.CurrentAssortmentFamilyRule.FamilyRuleType = _selectedFamilyRuleType;
                }
                OnPropertyChanged(SelectedFamilyRuleTypeProperty);
                OnPropertyChanged(SelectedFamilyRuleValueProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the family rule value
        /// </summary>
        public Byte? SelectedFamilyRuleValue
        {
            get { return _selectedFamilyRuleValue; }
            set
            {
                _selectedFamilyRuleValue = value;
                if (this.CurrentAssortmentFamilyRule != null)
                {
                    this.CurrentAssortmentFamilyRule.FamilyRuleValue = _selectedFamilyRuleValue;
                }
                OnPropertyChanged(SelectedFamilyRuleValueProperty);
            }
        }

        /// <summary>
        /// Returns the readonly collection of familys applied within the assortment
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssortmentFamilyRuleRow> CurrentAssortmentFamilyRules
        {
            get
            {
                if (_currentAssortmentFamilyRulesRO == null)
                {
                    _currentAssortmentFamilyRulesRO = new ReadOnlyBulkObservableCollection<AssortmentFamilyRuleRow>(_currentAssortmentFamilyRules);
                }
                return _currentAssortmentFamilyRulesRO;
            }
        }

        /// <summary>
        /// The currently selected family rule
        /// </summary>
        public AssortmentFamilyRuleRow CurrentAssortmentFamilyRule
        {
            get { return _currentAssortmentFamilyRule; }
            set
            {
                base.ShowWaitCursor(true);

                _currentAssortmentFamilyRule = value;
                LoadAvailableAndAssignedProducts();
                _selectedFamilyRuleType = _currentAssortmentFamilyRule == null ? PlanogramAssortmentProductFamilyRuleType.None : _currentAssortmentFamilyRule.FamilyRuleType;
                _selectedFamilyRuleName = _currentAssortmentFamilyRule == null ? String.Empty : _currentAssortmentFamilyRule.FamilyRuleName;
                _selectedFamilyRuleValue = _currentAssortmentFamilyRule == null ? null : _currentAssortmentFamilyRule.FamilyRuleValue;
                OnPropertyChanged(CurrentAssortmentFamilyRuleProperty);
                OnPropertyChanged(SelectedFamilyRuleTypeProperty);
                OnPropertyChanged(SelectedFamilyRuleValueProperty);

                base.ShowWaitCursor(false);
            }
        }

        /// <summary>
        /// Returns the readonly collection of products still available for the family
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramAssortmentProductView> AvailableProducts
        {
            get
            {
                if (_availableProductsRO == null)
                {
                    _availableProductsRO = new ReadOnlyBulkObservableCollection<PlanogramAssortmentProductView>(_availableProducts);
                }
                return _availableProductsRO;
            }
        }


        /// <summary>
        /// Returns the readonly collection of products already added to the family
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramAssortmentProductView> AssignedProducts
        {
            get
            {
                if (_assignedProductsRO == null)
                {
                    _assignedProductsRO = new ReadOnlyBulkObservableCollection<PlanogramAssortmentProductView>(_assignedProducts);
                }
                return _assignedProductsRO;
            }
        }

        /// <summary>
        /// Returns the collection of selected available products
        /// </summary>
        public ObservableCollection<PlanogramAssortmentProductView> SelectedAvailableProducts
        {
            get { return _selectedAvailableProducts; }
        }

        /// <summary>
        /// Returns the collection of selected available products
        /// </summary>
        public ObservableCollection<PlanogramAssortmentProductView> SelectedAssignedProducts
        {
            get { return _selectedAssignedProducts; }
        }

        /// <summary>
        /// Determines if the current family rule value is valid
        /// </summary>
        private Boolean ValidFamilyRuleValue
        {
            get
            {
                if (SelectedFamilyRuleType == PlanogramAssortmentProductFamilyRuleType.MinimumProductCount ||
                        SelectedFamilyRuleType == PlanogramAssortmentProductFamilyRuleType.MaximumProductCount)
                {
                    if (SelectedFamilyRuleValue == null || SelectedFamilyRuleValue <= 0 || SelectedFamilyRuleValue > this._assignedProducts.Count)
                    {
                        return false;
                    }
                }

                return true;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="assortment">The assortment product</param>
        /// <param name="assortmentProduct">The assortment product containing family rule</param>
        public AssortmentFamilyRulesViewModel(PlanogramAssortment assortment, PlanogramAssortmentProduct assortmentProduct, IEnumerable<IPlanItem> planItems)
        {
            //Load assortment selected in previous screen
            this.SelectedAssortment = assortment;

            _planItems = planItems;

            // Load assortments current family products
            LoadCurrentFamilyRules();
        }

        #endregion

        #region Commands

        #region AddFamilyRuleCommand

        private RelayCommand _addFamilyRuleCommand;

        /// <summary>
        /// Adds a new region to the list
        /// </summary>
        public RelayCommand AddFamilyRuleCommand
        {
            get
            {
                if (_addFamilyRuleCommand == null)
                {
                    _addFamilyRuleCommand = new RelayCommand(
                        p => AddRegion_Executed())
                    {
                        FriendlyName = Message.AssortmentDocument_FamilyRules_AddFamilyRule,
                        SmallIcon = ImageResources.Add_16,
                    };
                    base.ViewModelCommands.Add(_addFamilyRuleCommand);
                }
                return _addFamilyRuleCommand;
            }
        }

        private void AddRegion_Executed()
        {
            var newFamily = new AssortmentFamilyRuleRow(
                String.Empty, 
                PlanogramAssortmentProductFamilyRuleType.None, 
                null, 
                new List<PlanogramAssortmentProduct>());
            _currentAssortmentFamilyRules.Add(newFamily);
            this.CurrentAssortmentFamilyRule = newFamily;
        }

        #endregion

        #region RemoveCommand

        private RelayCommand _removeCommand;

        /// <summary>
        /// Removes the current item
        /// </summary>
        public RelayCommand RemoveCommand
        {
            get
            {
                if (_removeCommand == null)
                {
                    _removeCommand = new RelayCommand(
                        p => Remove_Executed(p),
                        p => Remove_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Remove,
                        FriendlyDescription = Message.Generic_Remove_Tooltip,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeCommand);
                }
                return _removeCommand;
            }
        }

        private Boolean Remove_CanExecute(Object arg)
        {
            var row = arg as AssortmentFamilyRuleRow;

            if (row == null)
            {
                ApplyCommand.DisabledReason = String.Empty;
                return false;
            }

            if (row.FamilyRuleType == PlanogramAssortmentProductFamilyRuleType.None)
            {
                ApplyCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Remove_Executed(Object arg)
        {
            var row = arg as AssortmentFamilyRuleRow;
            if (row != null)
            {
                row.ClearRule();
                _currentAssortmentFamilyRules.Remove(row);
                _deletedFamilyRuleRows.Add(row);
            }
        }

        #endregion

        #region ApplyCommand

        private RelayCommand _applyCommand;

        /// <summary>
        /// Applies any changes made in the current item to the parent asssortment
        /// </summary>
        public RelayCommand ApplyCommand
        {
            get
            {
                if (_applyCommand == null)
                {
                    _applyCommand = new RelayCommand(
                        p => Apply_Executed(),
                        p => Apply_CanExecute())
                    {
                        FriendlyName = Message.Generic_Apply,
                        FriendlyDescription = Message.Generic_Apply_Tooltip,
                        Icon = ImageResources.Apply_32,
                        SmallIcon = ImageResources.Apply_16
                    };
                    base.ViewModelCommands.Add(_applyCommand);
                }
                return _applyCommand;
            }
        }

        private Boolean Apply_CanExecute()
        {
            if (this.SelectedAssortment == null)
            {
                ApplyCommand.DisabledReason = String.Empty;
                return false;
            }

            //all rows must be valid
            if (!this.CurrentAssortmentFamilyRules.All(r => r.IsValid))
            {
                return false;
            }

            //all regions must have a unique name
            if (this.CurrentAssortmentFamilyRules.Select(r => r.FamilyRuleName).Distinct().Count() != this.CurrentAssortmentFamilyRules.Count)
            {
                return false;
            }

            //at least one row must be dirty
            if (!this.CurrentAssortmentFamilyRules.Any(f => f.IsDirty))
            {
                //if no rows are dirty, check if any rows have been deleted
                if (this._deletedFamilyRuleRows.Count == 0)
                {
                    return false;
                }
                return true;
            }

            return true;
        }

        private void Apply_Executed()
        {
            ApplyCurrentItem();
        }

        private void ApplyCurrentItem()
        {
            base.ShowWaitCursor(true);

            //delete all removed rows
            foreach (var deletedRow in _deletedFamilyRuleRows)
            {
                deletedRow.DeleteFamilyRule();
                deletedRow.Dispose();
            }
            _deletedFamilyRuleRows.Clear();

            //cycle through all rows applying changes made
            foreach (var row in this.CurrentAssortmentFamilyRules)
            {
                row.CommitChanges();
            }

            //recreate all rows
            LoadCurrentFamilyRules();
            LoadAvailableAndAssignedProducts();

            base.ShowWaitCursor(false);
        }

        #endregion

        #region ApplyAndCloseCommand

        private RelayCommand _applyAndCloseCommand;
        /// <summary>
        /// Executes the save command then the close command
        /// The organiser should handle this command to peform the close action
        /// </summary>
        public RelayCommand ApplyAndCloseCommand
        {
            get
            {
                if (_applyAndCloseCommand == null)
                {
                    _applyAndCloseCommand = new RelayCommand(p => ApplyAndClose_Executed(), p => Apply_CanExecute())
                    {
                        FriendlyName = Message.Generic_ApplyAndClose,
                        FriendlyDescription = Message.Generic_ApplyAndClose_Tooltip,
                        SmallIcon = ImageResources.ApplyAndClose_16
                    };
                    base.ViewModelCommands.Add(_applyAndCloseCommand);
                }
                return _applyAndCloseCommand;
            }
        }

        [DebuggerStepThrough]
        private void ApplyAndClose_Executed()
        {
            this.Apply_Executed();

            //close the attached window
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels changes and closes the window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region AddSelectedProductsCommand

        private RelayCommand _addSelectedProductsCommand;

        /// <summary>
        /// Adds any selected available products for the family
        /// </summary>
        public RelayCommand AddSelectedProductsCommand
        {
            get
            {
                if (_addSelectedProductsCommand == null)
                {
                    _addSelectedProductsCommand = new RelayCommand(
                        p => AddSelectedProducts_Executed(),
                        p => AddSelectedProducts_CanExecute())
                    {
                        FriendlyDescription = Message.Generic_AddToAssigned,
                        SmallIcon = ImageResources.Add_16,
                        DisabledReason = Message.AssortmentDocument_NoLocationsSelected
                    };
                    base.ViewModelCommands.Add(_addSelectedProductsCommand);
                }
                return _addSelectedProductsCommand;
            }
        }

        private Boolean AddSelectedProducts_CanExecute()
        {
            //must have selected locations available
            if (this.SelectedAvailableProducts.Count == 0)
            {
                AddSelectedProductsCommand.DisabledReason = Message.AssortmentDocument_NoLocationsSelected;
                return false;
            }

            return AddProducts_CanExecute(AddSelectedProductsCommand);
        }

        private bool AddProducts_CanExecute(RelayCommand command)
        {
            if (_currentAssortmentFamilyRule == null)
            {
                command.DisabledReason = Message.AssortmentDocument_FamilyRules_NoFamilyRuleSelected;
                return false;
            }

            //must have a region
            if (_currentAssortmentFamilyRule.FamilyRuleName.Length == 0)
            {
                command.DisabledReason = Message.AssortmentDocument_FamilyRules_FamilyRuleNameValidation;
                return false;
            }

            if (_currentAssortmentFamilyRule.FamilyRuleType == PlanogramAssortmentProductFamilyRuleType.None)
            {
                command.DisabledReason = Message.AssortmentDocument_FamilyRules_FamilyRuleTypeValidation;
                return false;
            }

            //must have locations available
            if (this.AvailableProducts.Count == 0)
            {
                command.DisabledReason = Message.AssortmentDocument_NoAssortmentLocations;
                return false;
            }

            return true;
        }

        private void AddSelectedProducts_Executed()
        {
            //Add to the assigned Products list so that grid updated
            _assignedProducts.AddRange(_selectedAvailableProducts);
            this._currentAssortmentFamilyRule.AssignedProducts.AddRange(_selectedAvailableProducts.Select(p => p.Product));
            _availableProducts.RemoveRange(_selectedAvailableProducts);
            _selectedAvailableProducts.Clear();
            OnPropertyChanged(SelectedFamilyRuleValueProperty);
        }

        #endregion

        #region RemoveSelectedProductsCommand

        private RelayCommand _removeSelectedProductsCommand;

        /// <summary>
        /// Removes the selected products for the family rule
        /// </summary>
        public RelayCommand RemoveSelectedProductsCommand
        {
            get
            {
                if (_removeSelectedProductsCommand == null)
                {
                    _removeSelectedProductsCommand = new RelayCommand(
                        p => RemoveSelectedProducts_Executed(),
                        p => RemoveSelectedProducts_CanExecute())
                    {
                        FriendlyDescription = Message.Generic_RemoveFromAssigned,
                        SmallIcon = ImageResources.Delete_16,
                        DisabledReason = Message.AssortmentDocument_NoLocationsSelected
                    };
                    base.ViewModelCommands.Add(_removeSelectedProductsCommand);
                }
                return _removeSelectedProductsCommand;
            }
        }

        [DebuggerStepThrough]
        private bool RemoveSelectedProducts_CanExecute()
        {
            if (_currentAssortmentFamilyRule == null)
            {
                RemoveSelectedProductsCommand.DisabledReason = Message.AssortmentDocument_FamilyRules_NoFamilyRuleSelected;
                return false;
            }

            //must have a region
            if (_currentAssortmentFamilyRule.FamilyRuleName.Length == 0)
            {
                RemoveSelectedProductsCommand.DisabledReason = Message.AssortmentDocument_FamilyRules_FamilyRuleNameValidation;
                return false;
            }

            if (_selectedAssignedProducts.Count == 0)
            {
                RemoveSelectedProductsCommand.DisabledReason = Message.AssortmentDocument_NoAssortmentLocations;
                return false;
            }
            else
            {
                this.RemoveSelectedProductsCommand.DisabledReason = String.Empty;
            }

            return true;
        }

        private void RemoveSelectedProducts_Executed()
        {
            //Remove from taken Locations list so that grid updated
            _availableProducts.AddRange(this.SelectedAssignedProducts);
            _currentAssortmentFamilyRule.AssignedProducts.RemoveRange(this.SelectedAssignedProducts.Select(p => p.Product));
            _assignedProducts.RemoveRange(this.SelectedAssignedProducts);
            this.SelectedAssignedProducts.Clear();
            OnPropertyChanged(SelectedFamilyRuleValueProperty);
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                if (this.CurrentAssortmentFamilyRules.Any(p => p.IsDirty))
                {
                    ChildItemChangeResult result = Galleria.Framework.Controls.Wpf.Helpers.ShowContinueWithChildItemChangeDialog(this.ApplyCommand.CanExecute());

                    if (result == ChildItemChangeResult.Apply)
                    {
                        this.ApplyCommand.Execute();
                    }
                    else if (result == ChildItemChangeResult.DoNotApply)
                    {
                        //do nothing
                    }
                    else
                    {
                        continueExecute = false;
                    }

                }
            }
            return continueExecute;

        }

        /// <summary>
        /// Loads the current family products within the selected assortment
        /// </summary>
        private void LoadCurrentFamilyRules()
        {
            this._currentAssortmentFamilyRules.Clear();

            List<String> currentFamilyRule = new List<string>();

            foreach (var product in _selectedAssortment.Products.Where(p => p.FamilyRuleType != PlanogramAssortmentProductFamilyRuleType.None).ToList())
            {
                if (!currentFamilyRule.Contains(product.FamilyRuleName))
                {
                    var assortmentFamilyRuleRowViewModel = new AssortmentFamilyRuleRow(
                        product.FamilyRuleName, 
                        product.FamilyRuleType, 
                        product.FamilyRuleValue,
                        _selectedAssortment.Products.Where(p => p.FamilyRuleName == product.FamilyRuleName).ToList());
                    _currentAssortmentFamilyRules.Add(assortmentFamilyRuleRowViewModel);
                    currentFamilyRule.Add(product.FamilyRuleName);
                }
            }
        }

        /// <summary>
        /// Load family product available \ assigned family products
        /// </summary>
        private void LoadAvailableAndAssignedProducts()
        {
            _availableProducts.Clear();
            _assignedProducts.Clear();

            foreach (var availableProduct in this.SelectedAssortment.Products.Where(
                                                        p => p.FamilyRuleType == PlanogramAssortmentProductFamilyRuleType.None).ToList())
            {
                _availableProducts.Add(new PlanogramAssortmentProductView(availableProduct, _planItems.FirstOrDefault(p => p.Product.Gtin == availableProduct.Gtin)));
            }

            if (_currentAssortmentFamilyRule != null)
            {
                _assignedProducts.AddRange(this._currentAssortmentFamilyRule.AssignedProducts.Select(p => new PlanogramAssortmentProductView(p, _planItems.FirstOrDefault(planItems => planItems.Product.Gtin == p.Gtin))));
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.SelectedFamilyRuleName = String.Empty;
                    _selectedAssortment = null;
                    _currentAssortmentFamilyRules.Clear();
                    _availableProducts.Clear();
                    _selectedAvailableProducts.Clear();
                    _assignedProducts.Clear();
                    _selectedAssignedProducts.Clear();
                }
                base.IsDisposed = true;
            }
        }

        #endregion

        #region IDataErrorInfo Members

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                String result = null;

                //Family Rule Name
                if (columnName == SelectedFamilyRuleNameProperty.Path)
                {
                    if (String.IsNullOrWhiteSpace(SelectedFamilyRuleName))
                    {
                        result = String.Format(CultureInfo.CurrentCulture, Message.AssortmentDocument_FamilyRules_FamilyRuleNameValidation);
                    }
                }

                //Family Rule Tpe
                if (columnName == SelectedFamilyRuleTypeProperty.Path)
                {
                    if (SelectedFamilyRuleType == PlanogramAssortmentProductFamilyRuleType.None)
                    {
                        result = String.Format(CultureInfo.CurrentCulture, Message.AssortmentDocument_FamilyRules_FamilyRuleTypeValidation);
                    }
                }

                //Family Rule Value
                if (columnName == SelectedFamilyRuleValueProperty.Path)
                {
                    if (!ValidFamilyRuleValue)
                    {
                        result = String.Format(CultureInfo.CurrentCulture, Message.AssortmentDocument_FamilyRules_FamilyRuleValueValidation);
                    }
                }

                return result; // return result
            }
        }

        #endregion
    }
}
