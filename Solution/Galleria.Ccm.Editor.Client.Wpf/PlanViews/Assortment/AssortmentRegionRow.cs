﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//  Adapted from Workflow client.
#endregion

#region Version History : CCM830
// V8-32987 : R.Cooper
//  Dispose of _regionLocations collection.
// CCM-18480 : D.Pleasance
//  Updated RegionLocations to Location model rather than LocationInfo so all attributes can be selected from grids.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Collections;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Represents a row on the region screen
    /// </summary>
    public sealed class AssortmentRegionRow : ViewModelObject
    {
        #region Fields

        private PlanogramAssortmentRegion _existingRegion;
        private PlanogramAssortmentRegion _region;
        private Boolean _isDirty;

        private BulkObservableCollection<Location> _regionLocations = new BulkObservableCollection<Location>();

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath RegionProperty = WpfHelper.GetPropertyPath<AssortmentRegionRow>(p => p.Region);
        public static readonly PropertyPath IsDirtyProperty = WpfHelper.GetPropertyPath<AssortmentRegionRow>(p => p.IsDirty);
        public static readonly PropertyPath RegionLocationsProperty = WpfHelper.GetPropertyPath<AssortmentRegionRow>(p => p.RegionLocations);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the region represented by this row.
        /// </summary>
        public PlanogramAssortmentRegion Region
        {
            get { return _region; }
        }


        /// <summary>
        /// Returns the collection of products allocated to the family.
        /// </summary>
        public BulkObservableCollection<Location> RegionLocations
        {
            get { return _regionLocations; }
        }

        /// <summary>
        /// Returns true if this row is dirty
        /// </summary>
        public Boolean IsDirty
        {
            get { return _isDirty; }
            private set
            {
                _isDirty = value;
                OnPropertyChanged(IsDirtyProperty);
            }
        }

        #endregion

        #region Constructor

        public AssortmentRegionRow()
        {
            _region = PlanogramAssortmentRegion.NewPlanogramAssortmentRegion();
            _isDirty = true;

            _region.PropertyChanged += Region_PropertyChanged;
            _regionLocations.BulkCollectionChanged += RegionLocations_BulkCollectionChanged;
        }


        public AssortmentRegionRow(PlanogramAssortmentRegion existingRegion, List<Location> regionLocations)
        {
            _existingRegion = existingRegion;
            _region = existingRegion.Clone();
            _regionLocations.AddRange(regionLocations);

            _isDirty = false;

            _region.PropertyChanged += Region_PropertyChanged;
            _regionLocations.BulkCollectionChanged += RegionLocations_BulkCollectionChanged;
        }

        #endregion

        #region Event Handlers

        private void Region_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            this.IsDirty = true;
        }


        private void RegionLocations_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            this.IsDirty = true;
        }

        #endregion

        #region Methods

        public void CommitChanges(PlanogramAssortment assort)
        {
            if (this.IsDirty)
            {
                PlanogramAssortmentRegion commitRegion = null;

                if (_existingRegion == null)
                {
                    commitRegion = _region;

                    //just add the rule
                    assort.Regions.Add(_region);
                }
                else
                {
                    commitRegion = _existingRegion;

                    //copy values
                    _existingRegion.Name = _region.Name;
                }

                //clear existing locs and add all new
                commitRegion.Locations.Clear();

                var addLocations = new List<PlanogramAssortmentRegionLocation>();
                foreach (var loc in this.RegionLocations)
                {
                    var locationToAdd = PlanogramAssortmentRegionLocation.NewPlanogramAssortmentRegionLocation();
                    locationToAdd.LocationCode = loc.Code;
                    addLocations.Add(locationToAdd);
                }
                commitRegion.Locations.AddRange(addLocations);

                this.IsDirty = false;
            }
        }

        public void DeleteExistingRegion()
        {
            if (_existingRegion != null)
            {
                _existingRegion.Parent.Regions.Remove(_existingRegion);
            }
            this.IsDirty = false;
        }

        #endregion
        
        #region IDisposable
        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {

                _regionLocations.Clear();
                _region.PropertyChanged -= Region_PropertyChanged;
                _regionLocations.BulkCollectionChanged -= RegionLocations_BulkCollectionChanged;

                if (disposing)
                {
                }

                base.IsDisposed = true;
            }
        }
        #endregion
    }
}
