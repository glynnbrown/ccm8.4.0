﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//      Created (adapted from Workflow client).
#endregion
#region Version History: (CCM 8.3.0)
// V8-30095 : A.Probyn
//  Corrected cast on SetProductRule from Byte to Int16
//  Added validation to byte and int16 edit values (maximum included too)
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32718 : D.Pleasance
//  Added planItems to constructor. These are passed through to enable displaying of product / position attributes.
// CCM-18445 : G.Richards
//  Apply specific validation and messaging for a rule value against a selected rule type.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Collections;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using System.ComponentModel;
using System.Globalization;
using Galleria.Ccm.Editor.Client.Wpf.Common;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    public sealed class AssortmentProductRuleViewModel : ViewModelAttachedControlObject<AssortmentProductRuleWindow>, IDataErrorInfo
    {
        #region Fields
        private IEnumerable<IPlanItem> _planItems;

        private PlanogramAssortment _selectedAssortment;
        private BulkObservableCollection<AssortmentProductRuleRow> _productRows =
            new BulkObservableCollection<AssortmentProductRuleRow>();
        private ReadOnlyBulkObservableCollection<AssortmentProductRuleRow> _productRowsRO;

        private BulkObservableCollection<AssortmentProductRuleRow> _selectedProductRows =
            new BulkObservableCollection<AssortmentProductRuleRow>();

        private BulkObservableCollection<Tuple<PlanogramAssortmentProductRuleType, String>> _availableProductRuleTypes = new BulkObservableCollection<Tuple<PlanogramAssortmentProductRuleType, String>>();
        private ReadOnlyBulkObservableCollection<Tuple<PlanogramAssortmentProductRuleType, String>> _availableProductRuleTypesRO;

        private PlanogramAssortmentProductRuleType? _editRuleType;
        private PlanogramAssortmentProductRuleValueType? _editValueType;
        private Int16? _editValue;
        private Boolean? _editApplyMaximum;
        private PlanogramAssortmentProductRuleValueType? _editMaximumValueType;
        private Int16? _editMaximumValue;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath SelectedAssortmentProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleViewModel>(p => p.SelectedAssortment);
        public static readonly PropertyPath ProductRowsProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleViewModel>(p => p.ProductRows);
        public static readonly PropertyPath SelectedProductRowsProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleViewModel>(p => p.SelectedProductRows);
        public static readonly PropertyPath IsMaximumAvailableProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleViewModel>(p => p.IsMaximumAvailable);
        public static readonly PropertyPath AvailableProductRuleTypesProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleViewModel>(p => p.AvailableProductRuleTypes);
        public static readonly PropertyPath EditRuleTypeProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleViewModel>(p => p.EditRuleType);
        public static readonly PropertyPath EditValueTypeProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleViewModel>(p => p.EditValueType);
        public static readonly PropertyPath EditValueProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleViewModel>(p => p.EditValue);
        public static readonly PropertyPath EditApplyMaximumProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleViewModel>(p => p.EditApplyMaximum);
        public static readonly PropertyPath EditMaximumValueTypeProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleViewModel>(p => p.EditMaximumValueType);
        public static readonly PropertyPath EditMaximumValueProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleViewModel>(p => p.EditMaximumValue);
        //commands
        public static readonly PropertyPath ApplyCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleViewModel>(p => p.ApplyCommand);
        public static readonly PropertyPath ApplyAndCloseCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleViewModel>(p => p.ApplyAndCloseCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath RemoveRuleCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleViewModel>(p => p.RemoveRuleCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the current Assortment
        /// </summary>
        public PlanogramAssortment SelectedAssortment
        {
            get { return _selectedAssortment; }
            set
            {
                _selectedAssortment = value;
                OnPropertyChanged(SelectedAssortmentProperty);

                OnSelectedAssortmentChanged(value);
            }
        }

        /// <summary>
        /// Returns the collection product rows
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssortmentProductRuleRow> ProductRows
        {
            get
            {
                if (_productRowsRO == null)
                {
                    _productRowsRO = new ReadOnlyBulkObservableCollection<AssortmentProductRuleRow>(_productRows);
                }
                return _productRowsRO;
            }
        }

        /// <summary>
        /// Returns the editable collection of selected product rows
        /// </summary>
        public BulkObservableCollection<AssortmentProductRuleRow> SelectedProductRows
        {
            get { return _selectedProductRows; }
        }

        /// <summary>
        /// Gets/Sets the rule type of the currently selected rows (None, Force,Preserve or Min Hurdle)
        /// </summary>
        public PlanogramAssortmentProductRuleType? EditRuleType
        {
            get { return _editRuleType; }
            set
            {
                _editRuleType = value;

                if (value.HasValue)
                {
                    foreach (AssortmentProductRuleRow row in this.SelectedProductRows)
                    {
                        SetProductRule(row.Product);
                    }
                }

                OnPropertyChanged(EditRuleTypeProperty);
                OnPropertyChanged(IsMaximumAvailableProperty);
            }

        }

        /// <summary>
        /// Gets/Sets the value type of the currently selected rows (Units or Facings)
        /// </summary>
        public PlanogramAssortmentProductRuleValueType? EditValueType
        {
            get { return _editValueType; }
            set
            {
                _editValueType = value;

                if (value.HasValue)
                {
                    foreach (AssortmentProductRuleRow row in this.SelectedProductRows)
                    {
                        SetProductRule(row.Product);
                    }
                }

                OnPropertyChanged(EditValueTypeProperty);
                OnPropertyChanged(EditValueProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the value of the currently selected rows (Number facings to apply)
        /// </summary>
        public Int16? EditValue
        {
            get { return _editValue; }
            set
            {
                //Ensure 0 is not entered
                Int16? oldValue = _editValue == 0 ? null : _editValue;

                if (value.HasValue)
                {
                    _editValue = value;
                    if (IsEditValueValid(this.EditValueType, _editValue))
                    {
                        foreach (AssortmentProductRuleRow row in this.SelectedProductRows)
                        {
                            SetProductRule(row.Product);

                            if (!row.Product.IsValid)
                            {
                                //reset to previous value
                                _editValue = oldValue;
                                OnPropertyChanged(EditValueProperty);
                                SetProductRule(row.Product);
                            }
                        }
                        //had to add this to force the EditValue's textbox to refresh
                        RefreshSelectedRows();
                    }
                }

                OnPropertyChanged(EditValueProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the apply max value of the currently selected rows
        /// </summary>
        public Boolean? EditApplyMaximum
        {
            get { return _editApplyMaximum; }
            set
            {
                _editApplyMaximum = value;

                if (value.HasValue)
                {
                    foreach (AssortmentProductRuleRow row in this.SelectedProductRows)
                    {
                        SetProductRule(row.Product);
                    }
                }

                OnPropertyChanged(EditApplyMaximumProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the max value type of the currently selected rows
        /// </summary>
        public PlanogramAssortmentProductRuleValueType? EditMaximumValueType
        {
            get { return _editMaximumValueType; }
            set
            {
                _editMaximumValueType = value;

                if (value.HasValue)
                {
                    foreach (AssortmentProductRuleRow row in this.SelectedProductRows)
                    {
                        SetProductRule(row.Product);
                    }
                }

                OnPropertyChanged(EditMaximumValueTypeProperty);
                OnPropertyChanged(EditMaximumValueProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the max value of the currently selected rows
        /// </summary>
        public Int16? EditMaximumValue
        {
            get { return _editMaximumValue; }
            set
            {

                Int16? oldValue = _editMaximumValue == 0 ? null : _editMaximumValue;

                if (value.HasValue)
                {
                    _editMaximumValue = value;
                    if (IsEditValueValid(this.EditMaximumValueType, _editMaximumValue))
                    {
                        foreach (AssortmentProductRuleRow row in this.SelectedProductRows)
                        {
                            SetProductRule(row.Product);

                            if (!row.Product.IsValid)
                            {
                                //reset to previous value
                                _editMaximumValue = oldValue;
                                OnPropertyChanged(EditMaximumValueProperty);
                                SetProductRule(row.Product);
                            }
                        }
                        //had to add this to force the EditValue's textbox to refresh
                        RefreshSelectedRows();
                    }
                }


                OnPropertyChanged(EditMaximumValueProperty);
            }
        }

        /// <summary>
        /// Returns true if the options for maximum list should
        /// be shown.
        /// </summary>
        public Boolean IsMaximumAvailable
        {
            get
            {
                return (this.EditRuleType == PlanogramAssortmentProductRuleType.Preserve ||
                        this.EditRuleType == PlanogramAssortmentProductRuleType.None ||
                        this.EditRuleType == PlanogramAssortmentProductRuleType.MinimumHurdle);
            }
        }

        /// <summary>
        /// Gets the available product rule types for the current scenario product
        /// </summary>
        public ReadOnlyBulkObservableCollection<Tuple<PlanogramAssortmentProductRuleType, String>> AvailableProductRuleTypes
        {
            get
            {
                if (_availableProductRuleTypesRO == null)
                {
                    _availableProductRuleTypesRO = new ReadOnlyBulkObservableCollection<Tuple<PlanogramAssortmentProductRuleType, String>>(_availableProductRuleTypes);
                }
                return _availableProductRuleTypesRO;
            }
        }

        #endregion

        #region Constructor
        public AssortmentProductRuleViewModel(PlanogramAssortment planogramAssortment, IEnumerable<IPlanItem> planItems)
        {
            _planItems = planItems;
            _productRows.BulkCollectionChanged += ProductRows_BulkCollectionChanged;
            _selectedProductRows.CollectionChanged += SelectedProductRows_CollectionChanged;
            
            //Get the available Rule types
            RefreshAvailableProductRuleTypes();

            //select the Assortment
            this.SelectedAssortment = planogramAssortment;
        } 
        #endregion

        #region Commands

        #region Apply

        private RelayCommand _applyCommand;

        /// <summary>
        /// Applies the changes to the current item
        /// </summary>
        public RelayCommand ApplyCommand
        {
            get
            {
                if (_applyCommand == null)
                {
                    _applyCommand = new RelayCommand(
                        p => Apply_Executed(),
                        p => Apply_CanExecute())
                    {
                        FriendlyName = Message.Generic_Apply,
                        FriendlyDescription = Message.Generic_Apply_Tooltip,
                        Icon = ImageResources.Apply_32,
                        SmallIcon = ImageResources.Apply_16
                    };
                    base.ViewModelCommands.Add(_applyCommand);
                }
                return _applyCommand;
            }
        }

        private Boolean Apply_CanExecute()
        {
            //must have a range selected
            if (this.SelectedAssortment == null)
            {
                ApplyCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be dirty
            if (!this.ProductRows.Any(p => p.IsDirty))
            {
                ApplyCommand.DisabledReason = Message.AssortmentDocument_NoChangesHaveBeenMade;
                return false;
            }

            //must all be valid
            if (!this.ProductRows.All(p => p.Product.IsValid))
            {
                ApplyCommand.DisabledReason = Message.AssortmentDocument_OneOrMoreRulesAreInvalid;
                return false;
            }

            return true;
        }

        private void Apply_Executed()
        {
            ApplyCurrentItem();
        }

        /// <summary>
        /// Applies changes made to the current item.
        /// </summary>
        private void ApplyCurrentItem()
        {
            base.ShowWaitCursor(true);

            //cycle through all product rows applying changes made
            foreach (var row in this.ProductRows)
            {
                row.CommitChanges();
            }

            //take a snap of the currently selected ids
            var selectedProductGtins = SelectedProductRows.Select(r => r.Product.Gtin).ToList();

            //recreate all product rows
            ReloadProductRows();

            //Unsubscribe from collection changed to stop spamming
            this.SelectedProductRows.CollectionChanged -= SelectedProductRows_CollectionChanged;

            //reselect
            foreach (var row in this.ProductRows)
            {
                if (selectedProductGtins.Contains(row.Product.Gtin))
                {
                    this.SelectedProductRows.Add(row);
                }
            }

            //Resubscribe from collection changed to stop spamming
            this.SelectedProductRows.CollectionChanged += SelectedProductRows_CollectionChanged;

            //Trigger change
            SelectedProductRows_CollectionChanged(this, null);

            base.ShowWaitCursor(false);
        }

        #endregion

        #region ApplyAndClose

        private RelayCommand _applyAndCloseCommand;

        /// <summary>
        /// Applies changes to the current item and closes the window
        /// </summary>
        public RelayCommand ApplyAndCloseCommand
        {
            get
            {
                if (_applyAndCloseCommand == null)
                {
                    _applyAndCloseCommand = new RelayCommand(
                        p => ApplyAndClose_Executed(),
                        p => ApplyAndClose_CanExecute())
                    {
                        FriendlyName = Message.Generic_ApplyAndClose,
                        FriendlyDescription = Message.Generic_ApplyAndClose_Tooltip,
                        SmallIcon = ImageResources.ApplyAndClose_16
                    };
                    base.ViewModelCommands.Add(_applyAndCloseCommand);
                }
                return _applyAndCloseCommand;
            }
        }

        private Boolean ApplyAndClose_CanExecute()
        {
            //must have a range selected
            if (this.SelectedAssortment == null)
            {
                ApplyAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be dirty
            if (!this.ProductRows.Any(p => p.IsDirty))
            {
                ApplyAndCloseCommand.DisabledReason = Message.AssortmentDocument_NoChangesHaveBeenMade;
                return false;
            }

            //must all be valid
            if (!this.ProductRows.All(p => p.Product.IsValid))
            {
                ApplyAndCloseCommand.DisabledReason = Message.AssortmentDocument_OneOrMoreRulesAreInvalid;
                return false;
            }

            return true;
        }

        private void ApplyAndClose_Executed()
        {
            ApplyCurrentItem();

            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels changes and closes the window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region RemoveRule

        private RelayCommand _removeRuleCommand;

        /// <summary>
        /// Nulls off the current product rule
        /// </summary>
        public RelayCommand RemoveRuleCommand
        {
            get
            {
                if (_removeRuleCommand == null)
                {
                    _removeRuleCommand = new RelayCommand(
                        p => RemoveRule_Executed(p),
                        p => RemoveRule_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Remove,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeRuleCommand);

                }
                return _removeRuleCommand;
            }
        }

        private Boolean RemoveRule_CanExecute(Object arg)
        {
            var row = arg as AssortmentProductRuleRow;

            //must have a valid row
            if (row == null)
            {
                return false;
            }

            //must not be a null row
            if (row.IsNullRule)
            {
                return false;
            }

            return true;
        }

        private void RemoveRule_Executed(Object arg)
        {
            var row = arg as AssortmentProductRuleRow;
            if (row != null)
            {
                row.ClearRule();

                //if the row is selected then refresh
                if (this.SelectedProductRows.Contains(row))
                {
                    var selectedRows = this.SelectedProductRows.ToList();
                    this.SelectedProductRows.Clear();
                    selectedRows.ForEach(r => this.SelectedProductRows.Add(r));
                }
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Reselects the selected rows as this is the only way i can find to refresh the contents
        /// of the Editvalue and EditmaximumValue textboxes - the onPropertyChanged didnt work
        /// </summary>
        public void RefreshSelectedRows()
        {
            //take a snap of the currently selected ids
            var selectedProductGtins = SelectedProductRows.Select(r => r.Product.Gtin).ToList();

            //reselect
            this.SelectedProductRows.Clear();
            foreach (var row in this.ProductRows)
            {
                if (selectedProductGtins.Contains(row.Product.Gtin))
                {
                    this.SelectedProductRows.Add(row);
                }
            }
        }

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                if (this.ProductRows.Any(p => p.IsDirty))
                {
                    var result = Galleria.Framework.Controls.Wpf.Helpers.ShowContinueWithChildItemChangeDialog(this.ApplyCommand.CanExecute());

                    if (result == ChildItemChangeResult.Apply)
                    {
                        ApplyCurrentItem();
                    }
                    else if (result == ChildItemChangeResult.DoNotApply)
                    {
                        //do nothing
                    }
                    else
                    {
                        continueExecute = false;
                    }

                }
            }
            return continueExecute;
        }

        /// <summary>
        /// Recreates the product rows collection.
        /// </summary>
        private void ReloadProductRows()
        {
            //clear all existing product rows
            if (_productRows.Count > 0)
            {
                _productRows.Clear();
            }
            if (this.SelectedProductRows.Count > 0)
            {
                this.SelectedProductRows.Clear();
            }

            //reload for the selected assortment
            var rows = new List<AssortmentProductRuleRow>();
            foreach (var asstProduct in _selectedAssortment.Products)
            {
                rows.Add(new AssortmentProductRuleRow(_selectedAssortment, asstProduct, _planItems.FirstOrDefault(p => p.Product.Gtin == asstProduct.Gtin)));
            }
            _productRows.AddRange(rows);
        }

        /// <summary>
        /// Sets the rule applied to the UI variables down to the underlying product object
        /// </summary>
        /// <param name="product"></param>
        private void SetProductRule(PlanogramAssortmentProduct product)
        {
            switch (_editRuleType)
            {
                case PlanogramAssortmentProductRuleType.Core:
                    product.ExactListFacings = null;
                    product.ExactListUnits = null;
                    product.PreserveListFacings = null;
                    product.PreserveListUnits = null;
                    product.MinListFacings = null;
                    product.MinListUnits = null;
                    product.ProductTreatmentType = PlanogramAssortmentProductTreatmentType.Core;
                    product.IsRanged = true;
                    break;

                case PlanogramAssortmentProductRuleType.None:
                    product.ExactListFacings = null;
                    product.ExactListUnits = null;
                    product.PreserveListFacings = null;
                    product.PreserveListUnits = null;
                    product.MinListFacings = null;
                    product.MinListUnits = null;
                    product.ProductTreatmentType = PlanogramAssortmentProductTreatmentType.Normal;
                    break;

                case PlanogramAssortmentProductRuleType.Force:
                    _editApplyMaximum = null;
                    if (_editValue.HasValue)
                    {
                        switch (_editValueType.Value)
                        {
                            case PlanogramAssortmentProductRuleValueType.Facings:
                                product.ExactListUnits = null;
                                product.ExactListFacings = _editValue.Value <= 255 ? (Byte?)_editValue.Value : null;
                                break;

                            case PlanogramAssortmentProductRuleValueType.Units:
                                product.ExactListFacings = null;
                                product.ExactListUnits = (Int16?)_editValue.Value;
                                break;
                        }
                        product.PreserveListFacings = null;
                        product.PreserveListUnits = null;
                        product.MinListFacings = null;
                        product.MinListUnits = null;
                        product.MaxListFacings = null;
                        product.MaxListUnits = null;
                        product.ProductTreatmentType = PlanogramAssortmentProductTreatmentType.Normal;
                        product.IsRanged = (_editValue != 0);
                    }
                    break;

                case PlanogramAssortmentProductRuleType.MinimumHurdle:
                    if (_editValue.HasValue)
                    {
                        product.ExactListFacings = null;
                        product.ExactListUnits = null;
                        product.PreserveListFacings = null;
                        product.PreserveListUnits = null;
                        switch (_editValueType.Value)
                        {
                            case PlanogramAssortmentProductRuleValueType.Facings:
                                product.MinListUnits = null;
                                product.MinListFacings = _editValue.Value <= 255 ? (Byte?)_editValue.Value : null;
                                break;

                            case PlanogramAssortmentProductRuleValueType.Units:
                                product.MinListFacings = null;
                                product.MinListUnits = (Int16?)_editValue.Value;
                                break;
                        }
                        product.ProductTreatmentType = PlanogramAssortmentProductTreatmentType.Normal;
                    }
                    break;

                case PlanogramAssortmentProductRuleType.Preserve:
                    if (_editValue.HasValue)
                    {
                        product.ExactListFacings = null;
                        product.ExactListUnits = null;
                        switch (_editValueType.Value)
                        {
                            case PlanogramAssortmentProductRuleValueType.Facings:
                                product.PreserveListUnits = null;
                                product.PreserveListFacings = _editValue.Value <= 255 ? (Byte?)_editValue.Value : null;
                                break;

                            case PlanogramAssortmentProductRuleValueType.Units:
                                product.PreserveListFacings = null;
                                product.PreserveListUnits = (Int16?)_editValue.Value;
                                break;
                        }
                        product.MinListFacings = null;
                        product.MinListUnits = null;
                        product.ProductTreatmentType = PlanogramAssortmentProductTreatmentType.Normal;
                        product.IsRanged = (_editValue != 0);
                    }
                    break;
            }

            if (this.EditApplyMaximum.HasValue)
            {
                if ((Boolean)this.EditApplyMaximum)
                {
                    if ((_editMaximumValue.HasValue) && (_editMaximumValueType.HasValue))
                    {
                        switch (_editMaximumValueType.Value)
                        {
                            case PlanogramAssortmentProductRuleValueType.Facings:
                                product.MaxListUnits = null;
                                product.MaxListFacings = _editMaximumValue.Value <= 255 ? (Byte?)_editMaximumValue.Value : null;
                                break;

                            case PlanogramAssortmentProductRuleValueType.Units:
                                product.MaxListFacings = null;
                                product.MaxListUnits = (Int16?)_editMaximumValue.Value;
                                break;
                        }
                    }
                    else
                    {
                        product.MaxListFacings = null;
                        product.MaxListUnits = null;
                    }
                }
                else
                {
                    product.MaxListFacings = null;
                    product.MaxListUnits = null;
                }
            }
            else
            {
                product.MaxListFacings = null;
                product.MaxListUnits = null;
            }
        }

        /// <summary>
        /// Refreshes the available product rule types collection
        /// </summary>
        private void RefreshAvailableProductRuleTypes()
        {
            if (_availableProductRuleTypes.Count > 0)
            {
                _availableProductRuleTypes.Clear();
            }

            //add initial types
            _availableProductRuleTypes.Add(new Tuple<PlanogramAssortmentProductRuleType, String>(PlanogramAssortmentProductRuleType.None, PlanogramAssortmentProductRuleTypeHelper.FriendlyNames[PlanogramAssortmentProductRuleType.None]));
            _availableProductRuleTypes.Add(new Tuple<PlanogramAssortmentProductRuleType, String>(PlanogramAssortmentProductRuleType.Force, PlanogramAssortmentProductRuleTypeHelper.FriendlyNames[PlanogramAssortmentProductRuleType.Force]));
            _availableProductRuleTypes.Add(new Tuple<PlanogramAssortmentProductRuleType, String>(PlanogramAssortmentProductRuleType.Preserve, PlanogramAssortmentProductRuleTypeHelper.FriendlyNames[PlanogramAssortmentProductRuleType.Preserve]));
            _availableProductRuleTypes.Add(new Tuple<PlanogramAssortmentProductRuleType, String>(PlanogramAssortmentProductRuleType.MinimumHurdle, PlanogramAssortmentProductRuleTypeHelper.FriendlyNames[PlanogramAssortmentProductRuleType.MinimumHurdle]));

            //check if a local product exists for any of the selected products
            Boolean localExists = false;
            foreach (var row in this.SelectedProductRows)
            {
                var localProduct = _selectedAssortment.LocalProducts.FirstOrDefault(p => p.ProductGtin == row.Product.Gtin);
                if (localProduct != null)
                {
                    localExists = true;
                }
            }

            if (!localExists)
            {
                _availableProductRuleTypes.Add(
                    new Tuple<PlanogramAssortmentProductRuleType, String>(
                        PlanogramAssortmentProductRuleType.Core, 
                        PlanogramAssortmentProductRuleTypeHelper.FriendlyNames[PlanogramAssortmentProductRuleType.Core]));
            }
        }

        private Boolean IsEditValueValid(PlanogramAssortmentProductRuleValueType? valueType, Int16? value)
        {
            //If no values - invalid.
            if (!value.HasValue || !valueType.HasValue)
            {
                return false;
            }

            //Apply specific validation for a rule value against the selected rule type.
            if (_editRuleType.HasValue)
            {
                switch (_editRuleType.Value)
                {
                    case PlanogramAssortmentProductRuleType.Force:
                        //Force can have a value of zero
                        switch (valueType.Value)
                        {
                            case PlanogramAssortmentProductRuleValueType.Units:
                                return value.Value >= 0 && value.Value < 1000;

                            case PlanogramAssortmentProductRuleValueType.Facings:
                                return value.Value >= 0 && value.Value <= 255;
                        }
                        break;
                    default:
                        break;
                }
            }

            //Check max value based on value type
            switch (valueType.Value)
            {
                case PlanogramAssortmentProductRuleValueType.Units:
                    return value.Value > 0 && value.Value < 1000;

                case PlanogramAssortmentProductRuleValueType.Facings:
                    return value.Value > 0 && value.Value <= 255;
            }

            return false;
        }
        
        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change of selected range
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedAssortmentChanged(PlanogramAssortment newValue)
        {
            this.SelectedProductRows.Clear();

            ReloadProductRows();

            //select the first available row
            if (this.ProductRows.Count > 0)
            {
                if (this.SelectedProductRows.Count > 0)
                {
                    this.SelectedProductRows.Clear();
                }
                this.SelectedProductRows.Add(this.ProductRows.OrderBy(p => p.ProductGtin).First());
            }
        }

        /// <summary>
        /// Responds to changes to the product rows collection
        /// </summary>
        private void ProductRows_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove ||
                e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Reset)
            {
                foreach (AssortmentProductRuleRow row in e.ChangedItems)
                {
                    row.Dispose();
                }
            }
        }

        /// <summary>
        /// Responds to changes to the selected product rows collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedProductRows_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //reset all vals to null
            _editRuleType = null;
            _editValueType = null;
            _editValue = null;
            _editMaximumValueType = null;
            _editMaximumValue = null;
            _editApplyMaximum = null;

            //reset the edit values
            if (this.SelectedProductRows.Count > 0)
            {
                var firstProduct = this.SelectedProductRows.First().Product;

                //rule type
                var firstType = firstProduct.RuleType;
                if (this.SelectedProductRows.All(r => r.Product.RuleType == firstType))
                {
                    _editRuleType = firstType;
                }

                var firstValueType = firstProduct.RuleValueType;
                if (this.SelectedProductRows.All(r => r.Product.RuleValueType == firstValueType))
                {
                    _editValueType = firstValueType;
                }

                //value
                Int16? firstValue = firstProduct.RuleValue;
                if (this.SelectedProductRows.All(r => r.Product.RuleValue == firstValue))
                {
                    _editValue = firstValue;
                }

                //apply max
                Boolean firstHasMax = firstProduct.HasMaximum;
                if (this.SelectedProductRows.All(r => r.Product.HasMaximum == firstHasMax))
                {
                    _editApplyMaximum = firstHasMax;
                }

                //max value type
                var firstMaxValueType = firstProduct.RuleMaximumValueType;
                if (this.SelectedProductRows.All(r => r.Product.RuleMaximumValueType == firstMaxValueType))
                {
                    _editMaximumValueType = firstMaxValueType;
                }

                //max value
                Int16? firstMaxValue = firstProduct.RuleMaximumValue;
                if (this.SelectedProductRows.All(r => r.Product.RuleMaximumValue == firstMaxValue))
                {
                    _editMaximumValue = firstMaxValue;
                }
            }

            RefreshAvailableProductRuleTypes();

            //fire off all property changed events
            OnPropertyChanged(EditRuleTypeProperty);
            OnPropertyChanged(EditValueTypeProperty);
            OnPropertyChanged(EditValueProperty);
            OnPropertyChanged(EditApplyMaximumProperty);
            OnPropertyChanged(EditMaximumValueTypeProperty);
            OnPropertyChanged(EditMaximumValueProperty);
            OnPropertyChanged(IsMaximumAvailableProperty);
        }

        #endregion

        #region IDataErrorInfo

        String IDataErrorInfo.Error
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Implementation of the IDataErrorInfo interface
        /// </summary>
        /// <param name="columnName">Property name</param>
        /// <returns>String containing error</returns>
        String IDataErrorInfo.this[String columnName]
        {
            get
            {
                String result = null;

                //Edit maximum value
                if (columnName == EditMaximumValueProperty.Path)
                {
                    if (this.EditMaximumValueType.HasValue && this.EditMaximumValueType.Value == PlanogramAssortmentProductRuleValueType.Units)
                    {
                        if (this.EditMaximumValue.HasValue && !IsEditValueValid(this.EditMaximumValueType, this.EditMaximumValue))
                        {
                            result = String.Format(CultureInfo.CurrentCulture, Message.AssortmentSetupProductRules_InvalidUnits);
                        }
                    }
                    else if (this.EditMaximumValueType.HasValue && this.EditMaximumValueType.Value == PlanogramAssortmentProductRuleValueType.Facings)
                    {
                        if (this.EditMaximumValue.HasValue && !IsEditValueValid(this.EditMaximumValueType, this.EditMaximumValue))
                        {
                            result = String.Format(CultureInfo.CurrentCulture, Message.AssortmentSetupProductRules_InvalidFacings);
                        }
                    }

                    //Apply specific validation messaging for a rule value against the selected rule type.
                    if (_editRuleType.HasValue)
                    {
                        switch (_editRuleType.Value)
                        {
                            case PlanogramAssortmentProductRuleType.Force:
                                if (this.EditMaximumValueType.HasValue && this.EditMaximumValueType.Value == PlanogramAssortmentProductRuleValueType.Units)
                                {
                                    if (this.EditMaximumValue.HasValue && !IsEditValueValid(this.EditMaximumValueType, this.EditMaximumValue))
                                    {
                                        result = String.Format(CultureInfo.CurrentCulture, Message.AssortmentSetupProductRules_InvalidUnits_Force);
                                    }
                                }
                                else if (this.EditMaximumValueType.HasValue && this.EditMaximumValueType.Value == PlanogramAssortmentProductRuleValueType.Facings)
                                {
                                    if (this.EditMaximumValue.HasValue && !IsEditValueValid(this.EditMaximumValueType, this.EditMaximumValue))
                                    {
                                        result = String.Format(CultureInfo.CurrentCulture, Message.AssortmentSetupProductRules_InvalidFacings_Force);
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }

                //Edit value
                if (columnName == EditValueProperty.Path)
                {
                    if (this.EditValueType.HasValue && this.EditValueType.Value == PlanogramAssortmentProductRuleValueType.Units)
                    {
                        if (this.EditValue.HasValue && !IsEditValueValid(this.EditValueType, this.EditValue))
                        {
                            result = String.Format(CultureInfo.CurrentCulture, Message.AssortmentSetupProductRules_InvalidUnits);
                        }
                    }
                    else if (this.EditValueType.HasValue && this.EditValueType.Value == PlanogramAssortmentProductRuleValueType.Facings)
                    {
                        if (this.EditValue.HasValue && !IsEditValueValid(this.EditValueType, this.EditValue))
                        {
                            result = String.Format(CultureInfo.CurrentCulture, Message.AssortmentSetupProductRules_InvalidFacings);
                        }
                    }

                    //Apply specific validation messaging for a rule value against the selected rule type.
                    if (_editRuleType.HasValue)
                    {
                        switch (_editRuleType.Value)
                        {
                            case PlanogramAssortmentProductRuleType.Force:
                                if (this.EditValueType.HasValue && this.EditValueType.Value == PlanogramAssortmentProductRuleValueType.Units)
                                {
                                    if (this.EditValue.HasValue && !IsEditValueValid(this.EditValueType, this.EditValue))
                                    {
                                        result = String.Format(CultureInfo.CurrentCulture, Message.AssortmentSetupProductRules_InvalidUnits_Force);
                                    }
                                }
                                else if (this.EditValueType.HasValue && this.EditValueType.Value == PlanogramAssortmentProductRuleValueType.Facings)
                                {
                                    if (this.EditValue.HasValue && !IsEditValueValid(this.EditValueType, this.EditValue))
                                    {
                                        result = String.Format(CultureInfo.CurrentCulture, Message.AssortmentSetupProductRules_InvalidFacings_Force);
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }

                return result; // return result
            }
        }
        #endregion
        
        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _productRows.Clear();
                    _productRows.BulkCollectionChanged -= ProductRows_BulkCollectionChanged;

                    _selectedProductRows.CollectionChanged -= SelectedProductRows_CollectionChanged;
                    _selectedProductRows.Clear();
                    _selectedProductRows = null;
                }

                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
