﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014
#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//  Adapted from Workflow client.
#endregion

#region Version History: (CCM 830)
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    public sealed class AssortmentRegionProductRow : INotifyPropertyChanged
    {
        #region Fields
        private PlanogramAssortmentRegion _region;
        private PlanogramAssortmentRegionProduct _regionProduct;
        private PlanogramAssortmentProduct _selectedProduct;
        private Boolean _isDirty;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath RegionProperty = WpfHelper.GetPropertyPath<AssortmentRegionProductRow>(p => p.Region);
        public static readonly PropertyPath SelectedProductProperty = WpfHelper.GetPropertyPath<AssortmentRegionProductRow>(p => p.SelectedProduct);
        public static readonly PropertyPath IsDirtyProperty = WpfHelper.GetPropertyPath<AssortmentRegionProductRow>(p => p.IsDirty);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the region this row represents
        /// </summary>
        public PlanogramAssortmentRegion Region
        {
            get { return _region; }
        }

        /// <summary>
        /// Returns the selected product for this row.
        /// </summary>
        public PlanogramAssortmentProduct SelectedProduct
        {
            get { return _selectedProduct; }
            set
            {
                _selectedProduct = value;
                OnPropertyChanged(SelectedProductProperty);

                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Returns true if this row is dirty
        /// </summary>
        public Boolean IsDirty
        {
            get { return _isDirty; }
            private set
            {
                _isDirty = value;
                OnPropertyChanged(IsDirtyProperty);
            }
        }

        #endregion

        #region Constructor

        public AssortmentRegionProductRow(PlanogramAssortmentRegion region, PlanogramAssortmentProduct primaryProduct)
        {
            _region = region;

            //check if a region product exists
            _regionProduct = region.Products.Where(p => p.PrimaryProductGtin == primaryProduct.Gtin).FirstOrDefault();

            if (_regionProduct != null)
            {
                //set the selected product
                if (_regionProduct.RegionalProductGtin != null)
                {
                    var parentAssortment = _region.Parent;
                    if (parentAssortment.Products != null)
                    {
                        _selectedProduct = parentAssortment.Products.Where(p => p.Gtin == _regionProduct.RegionalProductGtin).FirstOrDefault();
                    }
                }
            }
            else
            {
                _regionProduct = PlanogramAssortmentRegionProduct.NewPlanogramAssortmentRegionProduct();
                _regionProduct.PrimaryProductGtin = primaryProduct.Gtin;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Commits changes made to the row
        /// </summary>
        public void CommitChanges()
        {
            if (this.IsDirty)
            {
                Boolean isExistingRegion = _regionProduct.Parent != null;

                if (this.SelectedProduct != null)
                {
                    if (_regionProduct.RegionalProductGtin != this.SelectedProduct.Gtin)
                    {
                        //reset previous
                        var parentAssortment = _region.Parent;
                        if (parentAssortment.Products != null)
                        {
                            var currentregionalProduct = parentAssortment.Products
                                .Where(p => p.Gtin == _regionProduct.RegionalProductGtin)
                                .FirstOrDefault();
                            if (currentregionalProduct != null)
                            {
                                //re assign regional product
                                currentregionalProduct.ProductLocalizationType = PlanogramAssortmentProductLocalizationType.None;
                            }
                        }
                    }

                    _regionProduct.RegionalProductGtin = this.SelectedProduct.Gtin;
                    // Set the assortment product to be a regional swap product
                    this.SelectedProduct.ProductLocalizationType = PlanogramAssortmentProductLocalizationType.Regional;
                }
                else
                {
                    var parentAssortment = _region.Parent;
                    if (parentAssortment.Products != null)
                    {
                        var currentregionalProduct = parentAssortment.Products
                            .Where(p => p.Gtin == _regionProduct.RegionalProductGtin)
                            .FirstOrDefault();
                        if (currentregionalProduct != null)
                        {
                            //re assign regional product
                            currentregionalProduct.ProductLocalizationType = PlanogramAssortmentProductLocalizationType.None;
                        }
                    }

                    _regionProduct.RegionalProductGtin = null;
                }

                if (isExistingRegion && this.SelectedProduct == null)
                {
                    //remove the row
                    _region.Products.Remove(_regionProduct);
                }
                else if (!isExistingRegion && this.SelectedProduct != null)
                {
                    //add the row
                    _region.Products.Add(_regionProduct);
                }

                this.IsDirty = false;
            }
        }

        #endregion

        #region INotifyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(PropertyPath property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }
}
