﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26491 : A.Kuszyk
//  Adapted from Workflow client.
// V8-28444 : M.Shelley
//  Fix failed unit tests
// V8-28460 : J.Pickup
//  Introduced cancel logic. Window didn't previously discard changes.

#endregion

#region Version History: (CCM 830)

// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32560 : A.Silva
//  Removed AddAll and RemoveAll commands, ammended Icons for AddSelected and RemoveSelected commands.
//  V8-32718 : A.Probyn
//  Updated to have full fat locations available
// V8-32718 : D.Pleasance
//  Added planItems to constructor. These are passed through to enable displaying of product / position attributes.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Collections;
using Galleria.Ccm.Model;
using System.Collections.ObjectModel;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using System.Collections.Specialized;
using System.Diagnostics;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Common;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    public class AssortmentLocalProductViewModel : ViewModelAttachedControlObject<AssortmentLocalProductWindow>
    {
        #region Fields

        private IEnumerable<IPlanItem> _planItems;

        private Boolean _isEditing = false; //Is the model object being edited?
        private Boolean _isDirty = false;  //Keeps track of whether any changes made to the current region

        private PlanogramAssortment _selectedAssortment;
        private PlanogramAssortmentProduct _selectedLocalAssortmentProduct; //The product in the assortment the user has selected
        private Dictionary<String, Location> locationLookup = null;

        //The list of current local products for the assortment
        private BulkObservableCollection<PlanogramAssortmentProduct> _currentAssortmentLocalProducts = new BulkObservableCollection<PlanogramAssortmentProduct>();
        private ReadOnlyBulkObservableCollection<PlanogramAssortmentProduct> _currentAssortmentLocalProductsRO;

        //The list of available locations for the local product to choose from (= assortment locations - ones already added)
        private BulkObservableCollection<Location> _availableLocations = new BulkObservableCollection<Location>();
        private ReadOnlyBulkObservableCollection<Location> _availableLocationsRO;
        //The currently selected location(s) in the available locations list
        private ObservableCollection<Location> _selectedAvailableLocations = new ObservableCollection<Location>();

        //The list of taken assortment locations already part of the local products
        private BulkObservableCollection<Location> _takenLocations = new BulkObservableCollection<Location>();
        private ReadOnlyBulkObservableCollection<Location> _takenLocationsRO;
        //The currently selected location(s) in the taken locations list
        private ObservableCollection<Location> _selectedTakenLocations = new ObservableCollection<Location>();

        private BulkObservableCollection<PlanogramAssortmentLocalProduct> _assignedAssortmentLocalProducts = new BulkObservableCollection<PlanogramAssortmentLocalProduct>();

        private String _selectedLocalProductDescription = String.Empty;

        //The field to track changes for cancel command
        private List<PlanogramAssortmentLocalProduct> _localProductsCopy = new List<PlanogramAssortmentLocalProduct>();

        #endregion

        #region Binding properties

        public static readonly PropertyPath SelectedAssortmentProperty =
            WpfHelper.GetPropertyPath<AssortmentLocalProductViewModel>(p => p.SelectedAssortment);
        public static readonly PropertyPath SelectedLocalAssortmentProductProperty =
            WpfHelper.GetPropertyPath<AssortmentLocalProductViewModel>(p => p.SelectedLocalAssortmentProduct);
        public static readonly PropertyPath SelectedLocalProductDescriptionProperty =
            WpfHelper.GetPropertyPath<AssortmentLocalProductViewModel>(p => p.SelectedLocalProductDescription);
        public static readonly PropertyPath AvailableAssortmentLocalProductsProperty =
            WpfHelper.GetPropertyPath<AssortmentLocalProductViewModel>(p => p.AvailableAssortmentLocalProducts);
        public static readonly PropertyPath CurrentAssortmentLocalProductsProperty =
            WpfHelper.GetPropertyPath<AssortmentLocalProductViewModel>(p => p.CurrentAssortmentLocalProducts);
        public static readonly PropertyPath AvailableLocationsProperty =
            WpfHelper.GetPropertyPath<AssortmentLocalProductViewModel>(p => p.AvailableLocations);
        public static readonly PropertyPath SelectedAvailableLocationsProperty =
            WpfHelper.GetPropertyPath<AssortmentLocalProductViewModel>(p => p.SelectedAvailableLocations);
        public static readonly PropertyPath TakenLocationsProperty =
            WpfHelper.GetPropertyPath<AssortmentLocalProductViewModel>(p => p.TakenLocations);
        public static readonly PropertyPath SelectedTakenLocationsProperty =
            WpfHelper.GetPropertyPath<AssortmentLocalProductViewModel>(p => p.SelectedTakenLocations);

        //commands
        public static readonly PropertyPath ApplyCommandProperty =
            WpfHelper.GetPropertyPath<AssortmentLocalProductViewModel>(p => p.ApplyCommand);
        public static readonly PropertyPath CancelCommandProperty =
            WpfHelper.GetPropertyPath<AssortmentLocalProductViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath ApplyAndCloseCommandProperty =
            WpfHelper.GetPropertyPath<AssortmentLocalProductViewModel>(p => p.ApplyAndCloseCommand);
        public static readonly PropertyPath GetLocalProductCommandProperty =
            WpfHelper.GetPropertyPath<AssortmentLocalProductViewModel>(p => p.GetLocalProductCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Property for the selected Assortment
        /// </summary>
        public PlanogramAssortment SelectedAssortment
        {
            get { return _selectedAssortment; }
            set
            {
                _selectedAssortment = value;
                OnPropertyChanged(SelectedAssortmentProperty);
                OnPropertyChanged(AvailableAssortmentLocalProductsProperty);
            }
        }

        /// <summary>
        /// Property for the products assigned to the selected Assortment
        /// </summary>
        public ObservableCollection<PlanogramAssortmentProduct> AvailableAssortmentLocalProducts
        {
            get { return _selectedAssortment.Products.ToObservableCollection(); }
        }

        /// <summary>
        /// Gets/Sets the product in the assortment the user has selected
        /// </summary>
        public PlanogramAssortmentProduct SelectedLocalAssortmentProduct
        {
            get { return _selectedLocalAssortmentProduct; }
            set
            {
                var oldModel = _selectedLocalAssortmentProduct;
                _selectedLocalAssortmentProduct = value;
                OnPropertyChanged(SelectedLocalAssortmentProductProperty);
                OnPropertyChanged(SelectedLocalProductDescriptionProperty);
                OnCurrentAssortmentLocalProductChanged(oldModel, value);
            }
        }

        /// <summary>
        /// The selected local product description
        /// </summary>
        public String SelectedLocalProductDescription
        {
            get
            {
                if (this.SelectedLocalAssortmentProduct != null)
                {
                    return String.Format(Message.AssortmentDocument_LocalProduct_LocalProductDescription, SelectedLocalAssortmentProduct.Gtin, SelectedLocalAssortmentProduct.Name);
                }
                return String.Empty;
            }
        }

        /// <summary>
        /// Returns the readonly collection of local products applied within the assortment
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramAssortmentProduct> CurrentAssortmentLocalProducts
        {
            get
            {
                if (_currentAssortmentLocalProductsRO == null)
                {
                    _currentAssortmentLocalProductsRO = new ReadOnlyBulkObservableCollection<PlanogramAssortmentProduct>(_currentAssortmentLocalProducts);
                }
                return _currentAssortmentLocalProductsRO;
            }
        }

        /// <summary>
        /// Returns the readonly collection of locations still available for adding to the region
        /// </summary>
        public ReadOnlyBulkObservableCollection<Location> AvailableLocations
        {
            get
            {
                if (_availableLocationsRO == null)
                {
                    _availableLocationsRO = new ReadOnlyBulkObservableCollection<Location>(_availableLocations);
                }
                return _availableLocationsRO;
            }
        }


        /// <summary>
        /// Returns the readonly collection of locations already added to the region
        /// </summary>
        public ReadOnlyBulkObservableCollection<Location> TakenLocations
        {
            get
            {
                if (_takenLocationsRO == null)
                {
                    _takenLocationsRO = new ReadOnlyBulkObservableCollection<Location>(_takenLocations);
                }
                return _takenLocationsRO;
            }
        }

        /// <summary>
        /// Returns the collection of selected available locations
        /// </summary>
        public ObservableCollection<Location> SelectedAvailableLocations
        {
            get { return _selectedAvailableLocations; }
        }

        /// <summary>
        /// Returns the collection of selected available locations
        /// </summary>
        public ObservableCollection<Location> SelectedTakenLocations
        {
            get { return _selectedTakenLocations; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="assortment"></param>
        /// <param name="selectedProduct"></param>
        public AssortmentLocalProductViewModel(PlanogramAssortment assortment, IEnumerable<IPlanItem> planItems)
        {
            //Load locations once
            locationLookup = LocationList.FetchByEntityId(App.ViewState.EntityId).ToDictionary(p => p.Code);

            _planItems = planItems;

            //Load assortment selected in previous screen
            this.SelectedAssortment = assortment;

            // Load assortments current local products
            LoadCurrentLocalProducts();

            //Update the trackable changes
            _localProductsCopy.AddRange(this.SelectedAssortment.LocalProducts.Copy());
        }

        #endregion

        #region Commands

        #region ApplyCommand

        private RelayCommand _applyCommand;

        /// <summary>
        /// Applies any changes made in the current item to the parent asssortment
        /// </summary>
        public RelayCommand ApplyCommand
        {
            get
            {
                if (_applyCommand == null)
                {
                    _applyCommand = new RelayCommand(
                        p => Apply_Executed(),
                        p => Apply_CanExecute())
                    {
                        FriendlyName = Message.Generic_Apply,
                        FriendlyDescription = Message.Generic_Apply_Tooltip, 
                        Icon = ImageResources.Apply_32,
                        SmallIcon = ImageResources.Apply_16
                    };
                    base.ViewModelCommands.Add(_applyCommand);
                }
                return _applyCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Apply_CanExecute()
        {
            if (this.SelectedAssortment == null)
            {
                ApplyCommand.DisabledReason = String.Empty;
                return false;
            }

            if (!this._isDirty)
            {
                ApplyCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Apply_Executed()
        {
            if (this.SelectedLocalAssortmentProduct != null)
            {
                if (this.TakenLocations.Count > 0)
                {
                    this.SelectedLocalAssortmentProduct.ProductLocalizationType = PlanogramAssortmentProductLocalizationType.Local;
                }
                else
                {
                    this.SelectedLocalAssortmentProduct.ProductLocalizationType = PlanogramAssortmentProductLocalizationType.None;
                }
            }

            //Update tracked changes
            _localProductsCopy.Clear();
            _localProductsCopy.AddRange(this.SelectedAssortment.LocalProducts);
        }

        #endregion

        #region ApplyAndCloseCommand

        private RelayCommand _applyAndCloseCommand;
        /// <summary>
        /// Executes the save command then the close command
        /// The organiser should handle this command to peform the close action
        /// </summary>
        public RelayCommand ApplyAndCloseCommand
        {
            get
            {
                if (_applyAndCloseCommand == null)
                {
                    _applyAndCloseCommand = new RelayCommand(p => ApplyAndClose_Executed(), p => Apply_CanExecute())
                    {
                        FriendlyName = Message.Generic_ApplyAndClose,
                        FriendlyDescription = Message.Generic_ApplyAndClose_Tooltip,
                        SmallIcon = ImageResources.ApplyAndClose_16
                    };
                    base.ViewModelCommands.Add(_applyAndCloseCommand);
                }
                return _applyAndCloseCommand;
            }
        }

        [DebuggerStepThrough]
        private void ApplyAndClose_Executed()
        {
            this.Apply_Executed();

            //close the attached window
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels changes and closes the window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            //Undo tracked changes
            List<PlanogramAssortmentLocalProduct> toRemove = new List<PlanogramAssortmentLocalProduct>();
            foreach (PlanogramAssortmentLocalProduct currentProduct in this.SelectedAssortment.LocalProducts)
            {
                if (_localProductsCopy.Where(cpy => cpy.ProductGtin == currentProduct.ProductGtin && cpy.LocationCode == currentProduct.LocationCode).FirstOrDefault() == null)
                {
                    //needs removing
                    toRemove.Add(currentProduct);
                }
            }

            SelectedAssortment.LocalProducts.RemoveList(toRemove);
            _localProductsCopy.Clear();

            //Close
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region GetLocalProductCommand

        private RelayCommand _getLocalProductCommand;

        /// <summary>
        /// Allows selection of available assortment products to be the local product
        /// </summary>
        public RelayCommand GetLocalProductCommand
        {
            get
            {
                if (_getLocalProductCommand == null)
                {
                    _getLocalProductCommand = new RelayCommand(
                        p => GetLocalProduct_Executed())
                    {
                        FriendlyName = Message.AssortmentDocument_More,
                        FriendlyDescription = Message.AssortmentDocument_LocalProduct_GetLocalProductDescription
                    };
                    base.ViewModelCommands.Add(_getLocalProductCommand);
                }
                return _getLocalProductCommand;
            }
        }

        private void GetLocalProduct_Executed()
        {
            if (this.AttachedControl != null)
            {
                // Get list of products that can be the local products
                var availableAssortmentProducts = this.SelectedAssortment.Products.
                    Where(p => 
                        p.ProductTreatmentType == PlanogramAssortmentProductTreatmentType.Normal &&
                        p.ProductLocalizationType == PlanogramAssortmentProductLocalizationType.None ||
                        p.ProductLocalizationType == PlanogramAssortmentProductLocalizationType.Local).
                    ToList();

                //Show local product selection window
                var assortmentLocalProductSelectionWindow = new AssortmentLocalProductSelectionWindow(
                        availableAssortmentProducts.Select(p => new PlanogramAssortmentProductView(p, _planItems.FirstOrDefault(planItems => planItems.Product.Gtin == p.Gtin))));
                Galleria.Ccm.Common.Wpf.Helpers.CommonHelper.GetWindowService().ShowDialog<AssortmentLocalProductSelectionWindow>(assortmentLocalProductSelectionWindow);

                //If result returned
                if (assortmentLocalProductSelectionWindow.DialogResult == true)
                {
                    //Set selected product
                    this.SelectedLocalAssortmentProduct = assortmentLocalProductSelectionWindow.SelectionResult.Product;
                }
            }
        }

        #endregion

        #region AddSelectedLocationsCommand

        private RelayCommand _addSelectedLocationsCommand;

        /// <summary>
        /// Adds any selected available locations for the local product
        /// </summary>
        public RelayCommand AddSelectedLocationsCommand
        {
            get
            {
                if (_addSelectedLocationsCommand == null)
                {
                    _addSelectedLocationsCommand = new RelayCommand(
                        p => AddSelectedLocations_Executed(),
                        p => AddSelectedLocations_CanExecute())
                    {
                        FriendlyDescription = Message.Generic_AddToAssigned,
                        SmallIcon = ImageResources.Add_16,
                        DisabledReason = Message.AssortmentDocument_NoLocationsSelected
                    };
                    base.ViewModelCommands.Add(_addSelectedLocationsCommand);
                }
                return _addSelectedLocationsCommand;
            }
        }

        private Boolean AddSelectedLocations_CanExecute()
        {
            //must have a region
            if (this.SelectedLocalAssortmentProduct == null)
            {
                this.AddSelectedLocationsCommand.DisabledReason = Message.AssortmentDocument_NoAssortmentLocalProductSelected;
                return false;
            }

            //must have locations available
            if (this.AvailableLocations.Count == 0)
            {
                this.AddSelectedLocationsCommand.DisabledReason = Message.AssortmentDocument_NoAssortmentLocations;
                return false;
            }

            //must have selected locations available
            if (this.SelectedAvailableLocations.Count == 0)
            {
                this.AddSelectedLocationsCommand.DisabledReason = Message.AssortmentDocument_NoLocationsSelected;
                return false;
            }

            return true;
        }

        private void AddSelectedLocations_Executed()
        {
            //Add to the taken Locations list so that grid updated
            _takenLocations.AddRange(_selectedAvailableLocations);

            //Add to model region's locations list
            var newAssortmentLocalProducts = new List<PlanogramAssortmentLocalProduct>();
            foreach (var loc in this.SelectedAvailableLocations)
            {
                var newAssortmentLocalProduct =
                    PlanogramAssortmentLocalProduct.NewPlanogramAssortmentLocalProduct();
                newAssortmentLocalProduct.LocationCode = loc.Code;
                newAssortmentLocalProduct.ProductGtin = SelectedLocalAssortmentProduct.Gtin;
                newAssortmentLocalProducts.Add(newAssortmentLocalProduct);
            }
            _assignedAssortmentLocalProducts.AddRange(newAssortmentLocalProducts);
            this.SelectedAssortment.LocalProducts.AddList(newAssortmentLocalProducts);
            _selectedAvailableLocations.Clear();
        }

        #endregion

        #region RemoveSelectedLocationsCommand

        private RelayCommand _removeSelectedLocationsCommand;

        /// <summary>
        /// Removes the selected locations for the selected local product
        /// </summary>
        public RelayCommand RemoveSelectedLocationsCommand
        {
            get
            {
                if (_removeSelectedLocationsCommand == null)
                {
                    _removeSelectedLocationsCommand = new RelayCommand(
                        p => RemoveSelectedLocations_Executed(),
                        p => RemoveSelectedLocations_CanExecute())
                    {
                        FriendlyDescription = Message.Generic_RemoveFromAssigned,
                        SmallIcon = ImageResources.Delete_16,
                        DisabledReason = Message.AssortmentDocument_NoLocationsSelected
                    };
                    base.ViewModelCommands.Add(_removeSelectedLocationsCommand);
                }
                return _removeSelectedLocationsCommand;
            }
        }

        [DebuggerStepThrough]
        private bool RemoveSelectedLocations_CanExecute()
        {
            //must have an assortment selected
            if (this.SelectedLocalAssortmentProduct == null)
            {
                this.RemoveSelectedLocationsCommand.DisabledReason = Message.AssortmentDocument_NoAssortmentLocalProductSelected;
                return false;
            }

            if (_selectedTakenLocations.Count == 0)
            {
                this.RemoveSelectedLocationsCommand.DisabledReason = Message.AssortmentDocument_NoLocationsSelected;
                return false;
            }
            else
            {
                this.RemoveSelectedLocationsCommand.DisabledReason = String.Empty;
            }

            return true;
        }

        private void RemoveSelectedLocations_Executed()
        {
            var locationCodes = this.SelectedTakenLocations.Select(l => l.Code).ToList();

            //Remove from taken Locations list so that grid updated
            var locationsToRemove = this.SelectedTakenLocations.ToList();
            _takenLocations.RemoveRange(locationsToRemove);

            //Remove from model region's locations list
            var removedLocations = this._assignedAssortmentLocalProducts.
                Where(p =>
                    locationCodes.Contains(p.LocationCode) && p.ProductGtin == this.SelectedLocalAssortmentProduct.Gtin).
                ToList();

            this._assignedAssortmentLocalProducts.RemoveRange(removedLocations);
            this.SelectedAssortment.LocalProducts.RemoveList(removedLocations);

            this.SelectedTakenLocations.Clear();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Prompt user to continue opening new or existing after changes made
        /// </summary>
        /// <returns></returns>
        public Boolean ContinueWithItemChange()
        {
            if (_isEditing)
            {
                if (_isDirty)
                {
                    //ask user if they want to cancel, save or discard
                    ModalMessageResult messageResult = ModalMessageResult.Button3;
                    ModalMessage win = new ModalMessage();
                    win.Title = System.Windows.Application.Current.MainWindow.GetType().Assembly.GetName().Name;
                    win.Header = Message.Generic_ApplyChangesBeforeContinuingWarning;
                    win.Description = Message.AssortmentDocument_LocalProduct_ApplyDiscardDescription;
                    win.MessageIcon = ImageResources.Dialog_Information;
                    win.ButtonCount = 3;
                    win.Button1Content = Message.Generic_Apply;
                    win.Button2Content = Message.Generic_DontApply;
                    win.Button3Content = Message.Generic_Cancel;
                    win.DefaultButton = ModalMessageButton.Button1;
                    win.CancelButton = ModalMessageButton.Button3;
                    App.ShowWindow(win, this.AttachedControl, true);

                    messageResult = win.Result;

                    if (messageResult == ModalMessageResult.Button1)
                    {
                        //save changes
                        this.Apply_Executed();
                        return true;
                    }
                    else if (messageResult == ModalMessageResult.Button2)
                    {
                        //discard any changes
                        return true;
                    }
                    else if (messageResult == ModalMessageResult.Button3)
                    {
                        //user has cancelled 
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Loads the current local products within the selected assortment
        /// </summary>
        private void LoadCurrentLocalProducts()
        {
            this._currentAssortmentLocalProducts.Clear();

            this._currentAssortmentLocalProducts.AddRange(
                this._selectedAssortment.Products.Where(
                    p => p.ProductLocalizationType == PlanogramAssortmentProductLocalizationType.Local));
        }

        /// <summary>
        /// Load local product available \ assigned locations
        /// </summary>
        private void LoadAvailableAndTakenLocations()
        {
            if (this.SelectedLocalAssortmentProduct != null)
            {
                _availableLocations.Clear();
                _takenLocations.Clear();

                var localProducts = this.SelectedAssortment.LocalProducts.
                    Where(p => p.ProductGtin == this.SelectedLocalAssortmentProduct.Gtin).
                    ToList();

                foreach (var location in locationLookup.Values)
                {
                    var localProduct = localProducts.
                        Where(p => p.LocationCode == location.Code).
                        FirstOrDefault();

                    if (localProduct != null)
                    {
                        _takenLocations.Add(location);
                        _assignedAssortmentLocalProducts.Add(localProduct);
                        _isDirty = false;
                    }
                    else
                    {
                        _availableLocations.Add(location);
                    }
                }
            }
        }

        /// <summary>
        /// Returns a list of local products that match the given criteria
        /// </summary>
        /// <param name="NameCriteria"></param>
        /// <returns>A list of regions matching the criteria</returns>
        public IEnumerable<PlanogramAssortmentProduct> GetMatchingLocalProducts(String NameCriteria)
        {
            return this.CurrentAssortmentLocalProducts.Where(
                p => p.Name.ToUpperInvariant().Contains(NameCriteria.ToUpperInvariant()));
        }

        #endregion

        #region Event Handlers

        private void OnCurrentAssortmentLocalProductChanged(PlanogramAssortmentProduct oldModel, PlanogramAssortmentProduct newModel)
        {
            if (oldModel != null)
            {
                this._assignedAssortmentLocalProducts.BulkCollectionChanged -= new EventHandler<Galleria.Framework.Model.BulkCollectionChangedEventArgs>(AssignedAssortmentLocalProducts_BulkCollectionChanged);
            }

            if (newModel != null)
            {
                this._assignedAssortmentLocalProducts.BulkCollectionChanged += new EventHandler<Galleria.Framework.Model.BulkCollectionChangedEventArgs>(AssignedAssortmentLocalProducts_BulkCollectionChanged);
            }

            LoadAvailableAndTakenLocations();

            _isDirty = false;
        }

        /// <summary>
        /// Respond to assigned local product locations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssignedAssortmentLocalProducts_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (PlanogramAssortmentLocalProduct item in e.ChangedItems)
                    {
                        //remove from the available list
                        var code = item.LocationCode;
                        var availableLoc = _availableLocations.FirstOrDefault(p => p.Code == code);
                        _availableLocations.Remove(availableLoc);
                    }
                    _isDirty = true;
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (PlanogramAssortmentLocalProduct item in e.ChangedItems)
                    {
                        //add back to the available list
                        var code = item.LocationCode;
                        Location availableLoc = null;                        
                        if (locationLookup.TryGetValue(code, out availableLoc))
                        {
                            _availableLocations.Add(availableLoc);
                        }
                    }
                    _isDirty = true;
                    break;

                case NotifyCollectionChangedAction.Reset:
                    break;
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.SelectedLocalAssortmentProduct = null;
                    _selectedAssortment = null;

                    _currentAssortmentLocalProducts.Clear();
                    _availableLocations.Clear();
                    _selectedAvailableLocations.Clear();
                    _takenLocations.Clear();
                    _selectedTakenLocations.Clear();
                    _assignedAssortmentLocalProducts.Clear();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}