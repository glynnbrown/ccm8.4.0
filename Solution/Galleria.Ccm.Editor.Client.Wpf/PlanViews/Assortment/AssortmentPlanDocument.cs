﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//  Created.
// V8-26704 : A.Kuszyk
//  Added methods for Merging/Replacing assortments.
// V8-77177 : L.Ineson
//  Moved the commands from the mainpage commands to here.
// V8-27254 : A.Kuszyk
//  Fixed AddAssortmentProductFromRepository_CanExecute to return false if not connected.
// V8-27247 : A.Kuszyk
//  Prevented multiples of the same product from being added.
// V8-27251 : A.Kuszyk
//  Added friendly description to RemoveSelectedProductsCommand.
// V8-27825 : L.Ineson
//  Added document freezing
// V8-27648 : A.Probyn ~ Updated to support ProductLibraryProduct
// V8-28444 : M.Shelley
//  Modified the ShowLocalProducts_CanExecute method so that it can run correctly under 
//  unit test conditions
// V8-28193 : A.Probyn
//  Extended in same way as product list view so it responds to product selection.
#endregion

#region Version History: (CCM 8.0.1)
// V8-27554 : I.George
//  Set Added Small Icon property to commands where they are missing
// V8-28786 : I.George
//  Updated SmallIcon property to use 16 x 16 icon size
#endregion

#region Version History: (CCM 8.1.0)
// V8-29525 : M.Pettit
//  When adding a product to an assortment, the product is also added to the parent planogram's product list
#endregion

#region Version History : (CCM 8.2.0)
// V8-30762 : I.George
//  Added CustomColumn layout type
// V8-30777 : L.Ineson
//  Changed type of SelectedProducts collection to speed things up
// V8-31116 : L.Ineson
//  Changed row type to planogram product view to support the need to display product fields.
// V8-31366 : A.Probyn
//  Brought adding product library products into line with selecting an assortment, so that product library products
//  are also added to the planogram if missing.
// V8-31393 : L.Ineson
//  Wrapped SelectAssortment command action in a begin and end undoable action call.
#endregion

#region Version Hisitory : (CCM 8.3.0)
// V8-30393 : A.Probyn
//  Added new Ranged/Non Ranged Product Counts
// V8-31558 : Allow drag / drop between planogram assortment views.
// V8-31550 : A.Probyn ~ Added Product and Location buddys commands
// V8-31551 : A.Probyn ~ Added inventory rules commands
// V8-31224 : A.Probyn
//  Updated confusing product buddy commands.
// V8-32396 : A.Probyn 
//      ~ Updated GTIN references to Gtin
//      ~ Disabled location buddies if no repository connected
// V8-32698 : N.Haywood
//  Added update from products and save to repository
// V8-32848 : M.Pettit
//  Added SelectAssortmentCommand which behaves like the Save or SaveAs depending on the state of the Assortment
// V8-32847 : N.Haywood
//  Fixed issue where clicking cancel on save as cleared the assortment name
// V8-32718 : D.Pleasance
//  Amended Rows collection to be IPlanItem so that product and position attributes can be filtered in the datagrid.
// CCM-13988 : R.Cooper
//  Amended Rows collection to be IPlanItem so that product and position attributes can be filtered in the datagrid.
// CCM-14012 : A.Heathcote 
//  Made changes to the product buddies region, to protect against null reference exception on the S1 - S5ProductGtin's
// CCM-18489 : M.Pettit
//  Auto-add selected assortment products to Advanced Add grid
// CCM-18495 : M.Pettit
//  AddAssortmentProductFromRepository selector now defaults to planogram's category products initally
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Common.Selectors;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews.ProductBuddy;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews.LocationBuddy;
using System.ComponentModel;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Controls the ui view of the Planogram Assortment.
    /// </summary>
    public sealed class AssortmentPlanDocument : PlanDocument<AssortmentPlanDocumentView>
    {
        #region Constants

        /// <summary>
        ///     Type of column layout used by the <see cref="ColumnLayoutManager" /> in this instance.
        /// </summary>
        public const CustomColumnLayoutType ColumnLayoutType = CustomColumnLayoutType.Assortment;

        /// <summary>
        ///     Name of the column layout preferred by the user to be used by the <see cref="ColumnLayoutManager" /> in this
        ///     instance.
        /// </summary>
        public const String ScreenKey = "AssortmentPlanDocument";

        #endregion

        #region Fields

        private Boolean _isSynchingSelection;

        private readonly BulkObservableCollection<IPlanItem> _rows = new BulkObservableCollection<IPlanItem>();
        private readonly ReadOnlyBulkObservableCollection<IPlanItem> _rowsRO;
        private BulkObservableCollection<IPlanItem> _selectedRows = new BulkObservableCollection<IPlanItem>();
        private ProductHierarchy _productHierarchy;
        private IEnumerable<ProductGroup> _productGroupList;
        private AssortmentInfoList _assortmentInfos;
        #endregion

        #region Binding Property Paths

        //Properties
        public static PropertyPath RowsProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.Rows);
        public static PropertyPath SelectedProductsProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.SelectedProducts);
        public static PropertyPath NotRangedProductsCountProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.NotRangedProductsCount);
        public static PropertyPath RangedProductsCountProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.RangedProductsCount);

        //Commands
        public static PropertyPath AddAssortmentProductCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.AddAssortmentProductCommand);
        public static PropertyPath AddAssortmentProductFromRepositoryCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.AddAssortmentProductFromRepositoryCommand);
        public static PropertyPath AddAssortmentProductFromLibraryCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.AddAssortmentProductFromLibraryCommand);
        public static PropertyPath ShowProductRulesCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.ShowProductRulesCommand);
        public static PropertyPath ShowFamilyRulesCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.ShowFamilyRulesCommand);
        public static PropertyPath ShowLocalProductsCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.ShowLocalProductsCommand);
        public static PropertyPath RegionalProductSetupCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.RegionalProductSetupCommand);
        public static PropertyPath ShowInventoryRulesCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.ShowInventoryRulesCommand);
        public static PropertyPath ProductBuddyCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.ProductBuddyCommand);
        public static PropertyPath ShowProductBuddyCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.ShowProductBuddyCommand);
        public static PropertyPath ShowProductBuddyAddNewWizardCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.ShowProductBuddyAddNewWizardCommand);
        public static PropertyPath ShowProductBuddyAddNewAdvancedCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.ShowProductBuddyAddNewAdvancedCommand);
        public static PropertyPath ShowLocationBuddyCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.ShowLocationBuddyCommand);
        public static PropertyPath ShowLocationBuddyReviewCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.ShowLocationBuddyReviewCommand);
        public static PropertyPath ShowLocationBuddyAddNewCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.ShowLocationBuddyAddNewCommand);
        public static PropertyPath ShowRulePrioritiesCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.ShowRulePrioritiesCommand);
        public static PropertyPath RemoveSelectedProductsCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.RemoveSelectedProductsCommand);
        public static PropertyPath SelectAssortmentCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.SelectAssortmentCommand);
        public static PropertyPath UpdateAssortmentFromProductsCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.UpdateAssortmentFromProductsCommand);
        public static PropertyPath SaveAssortmentCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.SaveAssortmentCommand);
        public static PropertyPath SaveAssortmentToRepositoryCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.SaveAssortmentToRepositoryCommand);
        public static PropertyPath SaveAsAssortmentToRepositoryCommandProperty = WpfHelper.GetPropertyPath<AssortmentPlanDocument>(p => p.SaveAsAssortmentToRepositoryCommand);

        #endregion

        #region Properties

        /// <summary>
        /// The document type.
        /// </summary>
        public override DocumentType DocumentType
        {
            get { return DocumentType.Assortment; }
        }

        /// <summary>
        /// The document title
        /// </summary>
        public override String Title
        {
            get { return DocumentTypeHelper.FriendlyNames[DocumentType.Assortment]; }
        }

        /// <summary>
        /// Returns the collection of rows to display in the grid.
        /// </summary>
        public ReadOnlyBulkObservableCollection<IPlanItem> Rows
        {
            get { return _rowsRO; }
        }

        /// <summary>
        /// Returns the collection of selected products
        /// </summary>
        public BulkObservableCollection<IPlanItem> SelectedProducts
        {
            get { return _selectedRows; }
        }

        /// <summary>
        /// The Assortment model this document represents.
        /// </summary>
        public PlanogramAssortment AssortmentModel
        {
            get
            {
                if (Planogram == null || Planogram.Model == null) return null;
                return Planogram.Model.Assortment;
            }
        }

        /// <summary>
        /// Gets a collection of Gtins from the products currently in the assortment.
        /// </summary>
        public IEnumerable<String> TakenGtins
        {
            get
            {
                return AssortmentModel.Products.Select(p => p.Gtin);
            }
        }

        /// <summary>
        /// Indicates whether or not this Assortment has any "details", or if it is blank.
        /// </summary>
        public Boolean HasAssortmentDetails
        {
            get
            {
                return AssortmentModel.LocalProducts.Count > 0 ||
                    AssortmentModel.Products.Count > 0 ||
                    AssortmentModel.Regions.Count > 0;
            }
        }

        /// <summary>
        /// Returns the ranged product count
        /// </summary>
        public Int32 RangedProductsCount
        {
            get { return AssortmentModel != null ? AssortmentModel.Products.Where(p => p.IsRanged).Count() : 0; }
        }

        /// <summary>
        /// Returns the not ranged product count
        /// </summary>
        public Int32 NotRangedProductsCount
        {
            get { return AssortmentModel != null ? AssortmentModel.Products.Where(p => !p.IsRanged).Count() : 0; }
        }

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="parentController">the parent controller of this document.</param>
        public AssortmentPlanDocument(PlanControllerViewModel parentController)
            : base(parentController)
        {
            //initialize collections.
            _rowsRO = new ReadOnlyBulkObservableCollection<IPlanItem>(_rows);

            if(App.ViewState.IsConnectedToRepository)
            {
                _productHierarchy = ProductHierarchy.FetchByEntityId(App.ViewState.EntityId);

                if(_productHierarchy != null)
                {
                    _productGroupList = _productHierarchy.FetchAllGroups();
                }

                _assortmentInfos = AssortmentInfoList.FetchByEntityId(App.ViewState.EntityId);
            }

            //attach events
            SetPlanEventHandlers(true);

            //load rows
            ReloadRows();


            //if the counts don't match up it may be that somehow we
            // havent added all assortment products to the product list
            Debug.Assert(this.Rows.Count == this.Planogram.Model.Assortment.Products.Count,
                "Mismatch between number of rows and number of assortment products..");
        }
        #endregion

        #region Commands

        #region ShowProductRules

        private RelayCommand _showProductRulesCommand;

        /// <summary>
        /// Show the Product Rules dialog
        /// </summary>
        public RelayCommand ShowProductRulesCommand
        {
            get
            {
                if (_showProductRulesCommand == null)
                {
                    _showProductRulesCommand =
                        new RelayCommand(p => ShowProductRules_Executed(), p => ShowProductRules_CanExecute())
                        {
                            Icon = ImageResources.Assortment_ProductRules,
                            FriendlyName = Message.AssortmentDocument_ProductRulesButton_Caption,
                            FriendlyDescription = Message.AssortmentDocument_ProductRulesButton_Description,
                            SmallIcon = ImageResources.Assortment_ProductRules16
                        };
                }
                return _showProductRulesCommand;
            }
        }

        private Boolean ShowProductRules_CanExecute()
        {
            if (AssortmentModel == null)
            {
                this.ShowProductRulesCommand.DisabledReason = Message.AssortmentDocument_NoAssortmentSelected;
                return false;
            }

            if (AssortmentModel.Products != null && AssortmentModel.Products.Count == 0)
            {
                this.ShowProductRulesCommand.DisabledReason = Message.AssortmentDocument_NoAssortmentProducts;
                return false;
            }

            return true;
        }

        private void ShowProductRules_Executed()
        {
            AssortmentProductRuleWindow productRulesWindow = new AssortmentProductRuleWindow(AssortmentModel, Rows);
            GetWindowService().ShowDialog<AssortmentProductRuleWindow>(productRulesWindow);
        }

        #endregion

        #region ShowFamilyRulesCommand

        private RelayCommand _showFamilyRulesCommand;

        /// <summary>
        /// Add related Location 
        /// </summary>
        public RelayCommand ShowFamilyRulesCommand
        {
            get
            {
                if (_showFamilyRulesCommand == null)
                {
                    _showFamilyRulesCommand =
                        new RelayCommand(p => ShowFamilyRules_Executed(), p => ShowFamilyRules_CanExecute())
                        {
                            Icon = ImageResources.Assortment_FamilyRules,
                            FriendlyName = Message.AssortmentDocument_FamilyRules,
                            FriendlyDescription = Message.AssortmentDocument_FamilyRules_Description,
                            SmallIcon = ImageResources.Assortment_FamilyRules16
                        };
                }
                return _showFamilyRulesCommand;
            }
        }

        private Boolean ShowFamilyRules_CanExecute()
        {
            if (this.AssortmentModel == null)
            {
                this.ShowFamilyRulesCommand.DisabledReason = Message.AssortmentDocument_NoAssortmentSelected;
                return false;
            }

            if (this.AssortmentModel.Products != null && this.AssortmentModel.Products.Count == 0)
            {
                this.ShowFamilyRulesCommand.DisabledReason = Message.AssortmentDocument_NoAssortmentProducts;
                return false;
            }

            return true;
        }

        private void ShowFamilyRules_Executed()
        {
            PlanogramAssortmentProduct selectedProduct = null;
            if (this.SelectedProducts != null && this.SelectedProducts.Count == 1)
            {
                if (this.SelectedProducts[0].Product.AssortmentProduct.FamilyRuleType != PlanogramAssortmentProductFamilyRuleType.None)
                {
                    selectedProduct = this.SelectedProducts[0].Product.AssortmentProduct;
                }
            }

            var viewModel = new AssortmentFamilyRulesViewModel(this.AssortmentModel, selectedProduct, Rows);
            GetWindowService().ShowDialog<AssortmentFamilyRulesWindow>(viewModel);

        }

        #endregion

        #region ShowLocalProductsCommand

        private RelayCommand _showLocalProductsCommand;

        /// <summary>
        /// Show the Local Products dialog
        /// </summary>
        public RelayCommand ShowLocalProductsCommand
        {
            get
            {
                if (_showLocalProductsCommand == null)
                {
                    _showLocalProductsCommand =
                        new RelayCommand(p => ShowLocalProducts_Executed(), p => ShowLocalProducts_CanExecute())
                        {
                            Icon = ImageResources.Assortment_LocalProducts,
                            FriendlyName = Message.AssortmentDocument_LocalProducts,
                            FriendlyDescription = Message.AssortmentDocument_LocalProducts_Description,
                            SmallIcon = ImageResources.Assortment_LocalProducts16
                        };
                }
                return _showLocalProductsCommand;
            }
        }

        private Boolean ShowLocalProducts_CanExecute()
        {
            // Check if the application is connected to a repository, but not when running as a unit test...
            if (!App.ViewState.IsConnectedToRepository && Application.Current != null)
            {
                this.ShowLocalProductsCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }

            if (AssortmentModel == null)
            {
                this.ShowLocalProductsCommand.DisabledReason = Message.AssortmentDocument_NoAssortmentSelected;
                return false;
            }

            if (AssortmentModel.Products != null && AssortmentModel.Products.Count == 0)
            {
                this.ShowLocalProductsCommand.DisabledReason = Message.AssortmentDocument_NoAssortmentProducts;
                return false;
            }

            return true;
        }

        private void ShowLocalProducts_Executed()
        {
            AssortmentLocalProductViewModel viewModel = new AssortmentLocalProductViewModel(AssortmentModel, Rows);
            GetWindowService().ShowDialog<AssortmentLocalProductWindow>(viewModel);
        }

        #endregion

        #region RegionalProductSetupCommand

        private RelayCommand _regionalProductSetupCommand;

        /// <summary>
        /// Add related Location 
        /// </summary>
        public RelayCommand RegionalProductSetupCommand
        {
            get
            {
                if (_regionalProductSetupCommand == null)
                {
                    _regionalProductSetupCommand =
                        new RelayCommand(
                            p => RegionalProductSetup_Executed(),
                            p => RegionalProductSetup_CanExecute())
                        {
                            Icon = ImageResources.Assortment_RegionalProducts,
                            FriendlyName = Message.AssortmentDocument_RegionalProductSetup,
                            FriendlyDescription = Message.AssortmentDocument_RegionalProductSetup_Description,
                            SmallIcon = ImageResources.Assortment_RegionalProducts16
                        };
                }
                return _regionalProductSetupCommand;
            }
        }

        private Boolean RegionalProductSetup_CanExecute()
        {
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.RegionalProductSetupCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }

            if (this.AssortmentModel == null)
            {
                this.RegionalProductSetupCommand.DisabledReason = Message.AssortmentDocument_NoAssortmentSelected;
                return false;
            }

            if (this.AssortmentModel.Products != null && this.AssortmentModel.Products.Count == 0)
            {
                this.RegionalProductSetupCommand.DisabledReason = Message.AssortmentDocument_NoAssortmentProducts;
                return false;
            }

            return true;
        }

        private void RegionalProductSetup_Executed()
        {
            AssortmentAddRegionalProductsViewModel viewModel = new AssortmentAddRegionalProductsViewModel(this.AssortmentModel, Rows);
            GetWindowService().ShowDialog<AssortmentAddRegionalProductsWindow>(viewModel);
        }

        #endregion

        #region ShowRulePriorities

        private RelayCommand _showRulePrioritiesCommand;

        /// <summary>
        /// Show the Rule Priorities dialog
        /// </summary>
        public RelayCommand ShowRulePrioritiesCommand
        {
            get
            {
                if (_showRulePrioritiesCommand == null)
                {
                    _showRulePrioritiesCommand =
                        new RelayCommand(p => ShowRulePriorities_Executed())
                        {
                            Icon = ImageResources.Assortment_RulePriorities,
                            FriendlyName = Message.AssortmentDocument_RulePrioritiesButton_Caption,
                            FriendlyDescription = Message.AssortmentDocument_RulePrioritiesButton_Description,
                            SmallIcon = ImageResources.Assortment_ProductRules16
                        };
                }
                return _showRulePrioritiesCommand;
            }
        }

        private void ShowRulePriorities_Executed()
        {
            GetWindowService().ShowWindow<AssortmentRulePrioritiesWindow>(AssortmentModel);
        }

        #endregion

        #region ShowInventoryRules

        private RelayCommand _showInventoryRulesCommand;

        /// <summary>
        /// Show the Inventory Rules dialog
        /// </summary>
        public RelayCommand ShowInventoryRulesCommand
        {
            get
            {
                if (_showInventoryRulesCommand == null)
                {
                    _showInventoryRulesCommand =
                        new RelayCommand(p => ShowInventoryRules_Executed(), p => ShowInventoryRules_CanExecute())
                        {
                            Icon = ImageResources.Assortment_InventoryRules,
                            FriendlyName = Message.AssortmentInventoryRules,
                            FriendlyDescription = Message.AssortmentInventoryRules_Description,
                            SmallIcon = ImageResources.Assortment_InventoryRules,
                            DisabledReason = Message.AssortmentDocument_NoAssortmentProducts
                        };
                }
                return _showInventoryRulesCommand;
            }
        }

        private Boolean ShowInventoryRules_CanExecute()
        {
            if (AssortmentModel == null)
            {
                this.ShowInventoryRulesCommand.DisabledReason = Message.AssortmentDocument_NoAssortmentSelected;
                return false;
            }

            if (this.AssortmentModel.Products.Count <= 0)
            {
                return false;
            }

            return true;
        }

        private void ShowInventoryRules_Executed()
        {
            AssortmentInventoryRuleWindow productRulesWindow = new AssortmentInventoryRuleWindow(AssortmentModel, Rows);
            GetWindowService().ShowDialog<AssortmentInventoryRuleWindow>(productRulesWindow);
        }

        #endregion

        #region ShowProductBuddyCommands

        private RelayCommand _productBuddyCommand;

        /// <summary>
        /// Show the product buddy screen
        /// </summary>
        public RelayCommand ProductBuddyCommand
        {
            get
            {
                if (_productBuddyCommand == null)
                {
                    _productBuddyCommand = new RelayCommand(
                            p => ShowProductBuddy_Executed(),
                            p => ProductBuddy_CanExecute())
                    {
                        FriendlyName = Message.AssortmentProductBuddy,
                        FriendlyDescription = Message.AssortmentProductBuddy_Desc,
                        Icon = ImageResources.Assortment_ProductBuddys,
                        SmallIcon = ImageResources.Assortment_ProductBuddys,
                        DisabledReason = Message.AssortmentDocument_NoAssortmentProducts
                    };
                    this.ViewModelCommands.Add(_productBuddyCommand);
                }
                return _productBuddyCommand;
            }
        }

        private Boolean ProductBuddy_CanExecute()
        {
            if (this.AssortmentModel == null)
            {
                return false;
            }

            if (this.AssortmentModel.Products.Count <= 0)
            {
                return false;
            }

            return true;
        }

        #region ShowProductBuddyCommand

        private RelayCommand _showProductBuddyCommand;

        /// <summary>
        /// Launches a window showing existing product buddies
        /// </summary>
        public RelayCommand ShowProductBuddyCommand
        {
            get
            {
                if (_showProductBuddyCommand == null)
                {
                    _showProductBuddyCommand = new RelayCommand(
                        p => ShowProductBuddy_Executed(),
                        p => ProductBuddy_CanExecute())
                    {
                        FriendlyName = Message.AssortmentProductBuddy_Show,
                        FriendlyDescription = Message.AssortmentProductBuddy_Show_Desc,
                        Icon = ImageResources.Assortment_ProductBuddys,
                        SmallIcon= ImageResources.Assortment_ProductBuddys,
                        DisabledReason = Message.AssortmentDocument_NoAssortmentProducts
                    };
                    base.ViewModelCommands.Add(_showProductBuddyCommand);
                }
                return _showProductBuddyCommand;
            }
        }



        private void ShowProductBuddy_Executed()
        {
            if (this.AttachedControl != null)
            {
                AssortmentProductBuddyWindow win = new AssortmentProductBuddyWindow(this.AssortmentModel, this.Rows);
                GetWindowService().ShowDialog<AssortmentProductBuddyWindow>(win);
            }
        }

        #endregion

        #region ShowProductBuddyAddNewWizardCommand

        private RelayCommand _showProductBuddyAddNewWizardCommand;

        /// <summary>
        /// Launches the add new product buddy wizard
        /// </summary>
        public RelayCommand ShowProductBuddyAddNewWizardCommand
        {
            get
            {
                if (_showProductBuddyAddNewWizardCommand == null)
                {
                    _showProductBuddyAddNewWizardCommand = new RelayCommand(
                        p => ProductBuddyAddNewWizard_Executed(),
                        p => ProductBuddy_CanExecute())
                    {
                        FriendlyName = Message.AssortmentProductBuddyWizard,
                        FriendlyDescription = Message.AssortmentProductBuddyWizard_Desc,
                        Icon = ImageResources.Assortment_ProductBuddys_Wizard,
                        SmallIcon = ImageResources.Assortment_ProductBuddys_Wizard,
                        DisabledReason = Message.AssortmentDocument_NoAssortmentProducts
                    };
                    base.ViewModelCommands.Add(_showProductBuddyAddNewWizardCommand);
                }
                return _showProductBuddyAddNewWizardCommand;
            }
        }

        private void ProductBuddyAddNewWizard_Executed()
        {
            if (this.AttachedControl != null)
            {
                IPlanItem selectedProduct = null;

                if (this.SelectedProducts.Count > 0)
                {
                    selectedProduct = this.SelectedProducts.FirstOrDefault();
                }

                AssortmentProductBuddyWizardWindow win = new AssortmentProductBuddyWizardWindow(this.AssortmentModel, selectedProduct != null ? selectedProduct.Product : null, this.Rows);
                GetWindowService().ShowDialog<AssortmentProductBuddyWizardWindow>(win);
            }
        }

        #endregion

        #region ShowProductBuddyAddNewAdvancedCommand

        private RelayCommand _showProductBuddyAddNewAdvancedCommand;

        /// <summary>
        /// Launches the add new product buddy advanced window.
        /// </summary>
        public RelayCommand ShowProductBuddyAddNewAdvancedCommand
        {
            get
            {
                if (_showProductBuddyAddNewAdvancedCommand == null)
                {
                    _showProductBuddyAddNewAdvancedCommand = new RelayCommand(
                        p => ProductBuddyAddNewAdvanced_Executed(),
                        p => ProductBuddy_CanExecute())
                    {
                        FriendlyName = Message.AssortmentProductBuddyAdvancedAdd,
                        FriendlyDescription = Message.AssortmentProductBuddyAdvancedAdd_Desc,
                        Icon = ImageResources.Assortment_ProductBuddys_Advanced,
                        SmallIcon = ImageResources.Assortment_ProductBuddys_Advanced,
                        DisabledReason = Message.AssortmentDocument_NoAssortmentProducts
                    };
                    base.ViewModelCommands.Add(_showProductBuddyAddNewAdvancedCommand);
                }
                return _showProductBuddyAddNewAdvancedCommand;
            }
        }

        private void ProductBuddyAddNewAdvanced_Executed()
        {
            if (this.AttachedControl != null)
            {
                AssortmentProductBuddyAdvancedAddWindow win = new AssortmentProductBuddyAdvancedAddWindow(this.AssortmentModel, this.Rows, _selectedRows);
                GetWindowService().ShowDialog<AssortmentProductBuddyAdvancedAddWindow>(win);
            }
        }

        #endregion

        #endregion

        #region ShowLocationBuddyCommand

        private RelayCommand _showLocationBuddyCommand;

        /// <summary>
        /// Show the location buddy screen
        /// </summary>
        public RelayCommand ShowLocationBuddyCommand
        {
            get
            {
                if (_showLocationBuddyCommand == null)
                {
                    _showLocationBuddyCommand =
                        new RelayCommand(
                            p => LocationBuddyReview_Executed(),
                            p => LocationBuddy_CanExecute())
                        {
                            FriendlyName = Message.AssortmentLocationBuddy,
                            FriendlyDescription = Message.AssortmentLocationBuddy_Desc,
                            Icon = ImageResources.Assortment_LocationBuddys_Review,
                            SmallIcon = ImageResources.Assortment_LocationBuddys_Review,
                        };
                    this.ViewModelCommands.Add(_showLocationBuddyCommand);
                }
                return _showLocationBuddyCommand;
            }
        }

        private Boolean LocationBuddy_CanExecute()
        {
            // Check if the application is connected to a repository, but not when running as a unit test...
            if (!App.ViewState.IsConnectedToRepository && Application.Current != null)
            {
                this.ShowLocationBuddyCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                this.ShowLocationBuddyReviewCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                this.ShowLocationBuddyAddNewCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }

            if (this.AssortmentModel == null)
            {
                return false;
            }

            return true;
        }

        #region ShowLocationBuddyReviewCommand

        private RelayCommand _showLocationBuddyReviewCommand;

        /// <summary>
        /// Launches a window Reviewing existing Location buddies
        /// </summary>
        public RelayCommand ShowLocationBuddyReviewCommand
        {
            get
            {
                if (_showLocationBuddyReviewCommand == null)
                {
                    _showLocationBuddyReviewCommand = new RelayCommand(
                        p => LocationBuddyReview_Executed(),
                        p => LocationBuddy_CanExecute()
                        )
                    {
                        FriendlyName = Message.AssortmentLocationBuddy_Review,
                        FriendlyDescription = Message.AssortmentLocationBuddy_Review_Desc,
                        Icon = ImageResources.Assortment_LocationBuddys_Review,
                        SmallIcon = ImageResources.Assortment_LocationBuddys_Review,
                    };
                    base.ViewModelCommands.Add(_showLocationBuddyReviewCommand);
                }
                return _showLocationBuddyReviewCommand;
            }
        }

        private void LocationBuddyReview_Executed()
        {
            if (this.AttachedControl != null)
            {
                AssortmentLocationBuddyReviewWindow win = new AssortmentLocationBuddyReviewWindow(this.AssortmentModel);
                GetWindowService().ShowDialog<AssortmentLocationBuddyReviewWindow>(win);
            }
        }

        #endregion

        #region ShowLocationBuddyAddNewCommand

        private RelayCommand _showLocationBuddyAddNewCommand;

        /// <summary>
        /// Launches the add new Location buddy 
        /// </summary>
        public RelayCommand ShowLocationBuddyAddNewCommand
        {
            get
            {
                if (_showLocationBuddyAddNewCommand == null)
                {
                    _showLocationBuddyAddNewCommand = new RelayCommand(
                        p => ShowLocationBuddyAddNew_Executed(),
                        p => LocationBuddy_CanExecute())
                    {
                        FriendlyName = Message.AssortmentLocationBuddy_AddNew,
                        FriendlyDescription = Message.AssortmentLocationBuddy_AddNew_Desc,
                        Icon = ImageResources.Assortment_LocationBuddys_AddNew,
                        SmallIcon = ImageResources.Assortment_LocationBuddys_AddNew
                    };
                    base.ViewModelCommands.Add(_showLocationBuddyAddNewCommand);
                }
                return _showLocationBuddyAddNewCommand;
            }
        }

        private void ShowLocationBuddyAddNew_Executed()
        {
            if (this.AttachedControl != null)
            {
                AssortmentLocationBuddyAddNewWindow win = new AssortmentLocationBuddyAddNewWindow(this.AssortmentModel);
                GetWindowService().ShowDialog<AssortmentLocationBuddyAddNewWindow>(win);
            }
        }

        #endregion

        #endregion

        #region RemoveSelectedProductsCommand

        private RelayCommand _removeSelectedProductsCommand;

        /// <summary>
        /// Removes the selected products from the current assortment
        /// </summary>
        public RelayCommand RemoveSelectedProductsCommand
        {
            get
            {
                if (_removeSelectedProductsCommand == null)
                {
                    _removeSelectedProductsCommand = new RelayCommand(
                        p => RemoveSelectedProducts_Executed(),
                        p => RemoveSelectedProducts_CanExecute())
                    {
                        FriendlyName = Message.AssortmentDocument_RemoveProduct,
                        FriendlyDescription = Message.AssortmentDocument_RemoveProduct_Description,
                        Icon = ImageResources.AssortmentDocument_RemoveSelectedProducts,
                        DisabledReason = Message.AssortmentDocument_RemoveProducts_Description,
                        SmallIcon = ImageResources.AssortmentDocument_RemoveSelectedProducts16
                    };
                    base.ViewModelCommands.Add(_removeSelectedProductsCommand);
                }
                return _removeSelectedProductsCommand;
            }
        }

        private bool RemoveSelectedProducts_CanExecute()
        {
            return (this.SelectedProducts.Count > 0);
        }

        private void RemoveSelectedProducts_Executed()
        {
            this.AssortmentModel.Products.RemoveList(this.SelectedProducts.Select(p => p.Product.AssortmentProduct).ToList());
            this.SelectedProducts.Clear();
        }

        #endregion

        #region UnrangeSelectedProductsCommand

        private RelayCommand _unrangeSelectedProductsCommand;

        /// <summary>
        /// Sets the IsRanged flag to false for the selected products
        /// </summary>
        public RelayCommand UnrangeSelectedProductsCommand
        {
            get
            {
                if (_unrangeSelectedProductsCommand == null)
                {
                    _unrangeSelectedProductsCommand = new RelayCommand(
                        p => UnrangeSelectedProducts_Executed(),
                        p => UnrangeSelectedProducts_CanExecute())
                    {
                        FriendlyName = Message.AssortmentDocument_UnrangeSelectedProducts,
                        DisabledReason = Message.AssortmentDocument_UnrangeSelectedProducts_Description
                    };
                    base.ViewModelCommands.Add(_unrangeSelectedProductsCommand);
                }
                return _unrangeSelectedProductsCommand;
            }
        }

        private Boolean UnrangeSelectedProducts_CanExecute()
        {
            return (this.SelectedProducts.Count > 0);
        }

        private void UnrangeSelectedProducts_Executed()
        {
            this.Planogram.Model.Assortment.Products.ChildChanged -= AssortmentProducts_ChildChanged;

            foreach (var selectedProduct in this.SelectedProducts.ToList())
            {
                selectedProduct.Product.AssortmentProduct.IsRanged = false;
            }

            this.Planogram.Model.Assortment.Products.ChildChanged += AssortmentProducts_ChildChanged;
            RefreshRangedProductCounts();
        }

        #endregion

        #region RangeSelectedProductsCommand

        private RelayCommand _rangeSelectedProductsCommand;

        /// <summary>
        /// Sets the IsRanged flag to false for the selected products
        /// </summary>
        public RelayCommand RangeSelectedProductsCommand
        {
            get
            {
                if (_rangeSelectedProductsCommand == null)
                {
                    _rangeSelectedProductsCommand = new RelayCommand(
                        p => RangeSelectedProducts_Executed(),
                        p => RangeSelectedProducts_CanExecute())
                    {
                        FriendlyName = Message.AssortmentDocument_RangeSelectedProducts,
                        DisabledReason = Message.AssortmentDocument_RangeSelectedProducts_Description
                    };
                    base.ViewModelCommands.Add(_rangeSelectedProductsCommand);
                }
                return _rangeSelectedProductsCommand;
            }
        }

        private Boolean RangeSelectedProducts_CanExecute()
        {
            return (this.SelectedProducts.Count > 0);
        }

        private void RangeSelectedProducts_Executed()
        {
            this.Planogram.Model.Assortment.Products.ChildChanged -= AssortmentProducts_ChildChanged;

            foreach (var selectedProduct in this.SelectedProducts.ToList())
            {
                selectedProduct.Product.AssortmentProduct.IsRanged = true;
            }

            this.Planogram.Model.Assortment.Products.ChildChanged += AssortmentProducts_ChildChanged;
            RefreshRangedProductCounts();
        }

        #endregion

        #region UnrangeAllProductsCommand

        private RelayCommand _unrangeAllProductsCommand;

        /// <summary>
        /// Sets the IsRanged flag to false for the selected products
        /// </summary>
        public RelayCommand UnrangeAllProductsCommand
        {
            get
            {
                if (_unrangeAllProductsCommand == null)
                {
                    _unrangeAllProductsCommand = new RelayCommand(
                        p => UnrangeAllProducts_Executed(),
                        p => UnrangeAllProducts_CanExecute())
                    {
                        FriendlyName = Message.AssortmentDocument_UnrangeAll,
                        DisabledReason = Message.AssortmentDocument_UnrangeAll_Description
                    };
                    base.ViewModelCommands.Add(_unrangeAllProductsCommand);
                }
                return _unrangeAllProductsCommand;
            }
        }

        [DebuggerStepThrough]
        private bool UnrangeAllProducts_CanExecute()
        {
            if (AssortmentModel != null)
            {
                return (AssortmentModel.Products != null && AssortmentModel.Products.Count > 0);
            }
            else
            {
                return false;
            }
        }

        private void UnrangeAllProducts_Executed()
        {
            this.Planogram.Model.Assortment.Products.ChildChanged -= AssortmentProducts_ChildChanged;

            foreach (var selectedProduct in AssortmentModel.Products.ToList())
            {
                selectedProduct.IsRanged = false;
            }

            this.Planogram.Model.Assortment.Products.ChildChanged += AssortmentProducts_ChildChanged;
            RefreshRangedProductCounts();
        }

        #endregion

        #region RangeAllProductsCommand

        private RelayCommand _rangeAllProductsCommand;

        /// <summary>
        /// Sets the IsRanged flag to false for the selected products
        /// </summary>
        public RelayCommand RangeAllProductsCommand
        {
            get
            {
                if (_rangeAllProductsCommand == null)
                {
                    _rangeAllProductsCommand = new RelayCommand(
                        p => RangeAllProducts_Executed(),
                        p => RangeAllProducts_CanExecute())
                    {
                        FriendlyName = Message.AssortmentDocument_RangeAll,
                        DisabledReason = Message.AssortmentDocument_RangeAll_Description
                    };
                    base.ViewModelCommands.Add(_rangeAllProductsCommand);
                }
                return _rangeAllProductsCommand;
            }
        }

        [DebuggerStepThrough]
        private bool RangeAllProducts_CanExecute()
        {
            if (AssortmentModel != null)
            {
                return (AssortmentModel.Products.Count > 0);
            }
            else return false;
        }

        private void RangeAllProducts_Executed()
        {
            this.Planogram.Model.Assortment.Products.ChildChanged -= AssortmentProducts_ChildChanged;

            foreach (var selectedProduct in AssortmentModel.Products.ToList())
            {
                selectedProduct.IsRanged = true;
            }

            this.Planogram.Model.Assortment.Products.ChildChanged += AssortmentProducts_ChildChanged;
            RefreshRangedProductCounts();
        }

        #endregion

        #region AddAssortmentProduct

        private RelayCommand _addAssortmentProductCommand;

        /// <summary>
        /// Show the Product Rules dialog
        /// </summary>
        public RelayCommand AddAssortmentProductCommand
        {
            get
            {
                if (_addAssortmentProductCommand == null)
                {
                    _addAssortmentProductCommand =
                        new RelayCommand(p => AddAssortmentProduct_Executed(), p => AddAssortmentProduct_CanExecute())
                        {
                            Icon = ImageResources.Add_32,
                            FriendlyName = Message.AssortmentDocument_AddProduct,
                            FriendlyDescription = Message.AssortmentDocument_AddProduct_Description,
                            SmallIcon = ImageResources.Add_16
                        };
                }
                return _addAssortmentProductCommand;
            }
        }

        private Boolean AddAssortmentProduct_CanExecute()
        {
            return AddAssortmentProductFromRepositoryCommand.CanExecute() || AddAssortmentProductFromLibraryCommand.CanExecute();
        }

        private void AddAssortmentProduct_Executed()
        {
            if (App.ViewState.IsConnectedToRepository)
            {
                AddAssortmentProductFromRepositoryCommand.Execute();
            }
            else
            {
                AddAssortmentProductFromLibraryCommand.Execute();
            }
        }

        #endregion

        #region AddAssortmentProductFromRepository

        private RelayCommand _addAssortmentProductFromRepositoryCommand;

        /// <summary>
        /// Show the Product Rules dialog
        /// </summary>
        public RelayCommand AddAssortmentProductFromRepositoryCommand
        {
            get
            {
                if (_addAssortmentProductFromRepositoryCommand == null)
                {
                    _addAssortmentProductFromRepositoryCommand =
                        new RelayCommand(
                            p => AddAssortmentProductFromRepository_Executed(),
                            p => AddAssortmentProductFromRepository_CanExecute())
                        {
                            SmallIcon = ImageResources.Add_16,
                            FriendlyName = Message.AssortmentDocument_AddProductFromRepository,
                            FriendlyDescription = Message.AssortmentDocument_AddProductFromRepository_Description
                        };
                }
                return _addAssortmentProductFromRepositoryCommand;
            }
        }

        private Boolean AddAssortmentProductFromRepository_CanExecute()
        {
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.AddAssortmentProductFromRepositoryCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }

            return true;
        }

        private void AddAssortmentProductFromRepository_Executed()
        {
            PlanogramAssortment assortment = this.AssortmentModel;
            List<String> takenProductGtins = assortment.Products.Select(p => p.Gtin).ToList();

            var parentPlanogram = assortment.Parent as Planogram;
            ProductSelectorViewModel winView = null;
            if (parentPlanogram != null)
            {
                winView = new ProductSelectorViewModel(takenProductGtins, parentPlanogram.CategoryCode);
            }
            else
            {
                winView = new ProductSelectorViewModel(takenProductGtins);
            }
            GetWindowService().ShowDialog<ProductSelectorWindow>(winView);
            if (winView.DialogResult != true) return;

            ShowWaitCursor(true);

            List<Product> assignedProducts = winView.AssignedProducts.ToList();

            //remove any old products
            List<PlanogramAssortmentProduct> productsToRemove =
                assortment.Products.Where(p => !assignedProducts.Any(a => a.Gtin == p.Gtin)).ToList();
            assortment.Products.RemoveList(productsToRemove);

            //add any new products
            List<PlanogramAssortmentProduct> productsToAdd = new List<PlanogramAssortmentProduct>();
            foreach (Product product in assignedProducts)
            {
                if (!takenProductGtins.Contains(product.Gtin))
                {
                    productsToAdd.Add(PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(product));
                }
            }
            assortment.Products.AddList(productsToAdd);

            //and do the same for the current planogram
            AddProductsToParentPlanogram(assignedProducts);

            ShowWaitCursor(false);

        }

        #endregion

        #region AddAssortmentProductFromLibrary

        private RelayCommand _addAssortmentProductFromLibraryCommand;

        /// <summary>
        /// Show the Product Rules dialog
        /// </summary>
        public RelayCommand AddAssortmentProductFromLibraryCommand
        {
            get
            {
                if (_addAssortmentProductFromLibraryCommand == null)
                {
                    _addAssortmentProductFromLibraryCommand =
                        new RelayCommand(p => AddAssortmentProductFromLibrary_Executed(), p => AddAssortmentProductFromLibrary_CanExecute())
                        {
                            Icon = ImageResources.Add_32,
                            SmallIcon = ImageResources.Add_16,
                            FriendlyName = Message.AssortmentDocument_AddProductFromLibrary,
                            FriendlyDescription = Message.AssortmentDocument_AddProductFromLibrary_Description
                        };
                }
                return _addAssortmentProductFromLibraryCommand;
            }
        }

        private Boolean AddAssortmentProductFromLibrary_CanExecute()
        {
            if (!(App.ViewState.ProductLibraryView != null && App.ViewState.ProductLibraryView.Products.Count > 0))
            {
                AddAssortmentProductFromLibraryCommand.DisabledReason = Message.AssortmentDocument_NoProductLibraryLoaded;
                return false;
            }
            return true;
        }

        private void AddAssortmentProductFromLibrary_Executed()
        {
            var libraryProducts = App.ViewState.ProductLibraryView.Products;

            var win = new ProductLibraryProductSelectionWindow(
                libraryProducts.Where(p => !this.TakenGtins.Contains(p.Product.Gtin)).Select(l => l.Product), true, true);
            win.ViewModel.AddToSelectionRequested += OnAddProducts;

            GetWindowService().ShowDialog<ProductLibraryProductSelectionWindow>(win);

            win.ViewModel.AddToSelectionRequested -= OnAddProducts;
        }

        /// <summary>
        /// Called when a collection of Products are added to the assortment.
        /// </summary>
        private void OnAddProducts(Object sender, EventArgs<IEnumerable<Product>> e)
        {
            AddProducts(e.ReturnValue);
        }


        #endregion

        #region SelectAssortment

        private RelayCommand _selectAssortmentCommand;

        /// <summary>
        /// Show the Select Assortment dialog.
        /// </summary>
        public RelayCommand SelectAssortmentCommand
        {
            get
            {
                if (_selectAssortmentCommand == null)
                {
                    _selectAssortmentCommand =
                        new RelayCommand(
                            p => SelectAssortment_Executed(p),
                            p => SelectAssortment_CanExecute())
                        {
                            Icon = ImageResources.PlanDocumentType_Assortment32,
                            FriendlyName = Message.AssortmentDocument_SelectAssortment,
                            FriendlyDescription = Message.AssortmentDocument_SelectAssortment_Description,
                            SmallIcon = ImageResources.PlanDocumentType_Assortment16
                        };
                }
                return _selectAssortmentCommand;
            }
        }

        private Boolean SelectAssortment_CanExecute()
        {
            if (!App.ViewState.IsConnectedToRepository)
            {
                SelectAssortmentCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }

            return true;
        }

        private void SelectAssortment_Executed(Object args)
        {
            //show the selection window.
            AssortmentInfo selectedInfo = args as AssortmentInfo;
            if (selectedInfo == null && this.AttachedControl != null)
            {
                AssortmentSelectionWindow win = new AssortmentSelectionWindow();
                GetWindowService().ShowDialog<AssortmentSelectionWindow>(win);
                if (win.DialogResult != true) return;
                selectedInfo = win.SelectedAssortment;
            }
            if (selectedInfo == null) return;

            ShowWaitCursor(true);

            this.Planogram.BeginUndoableAction();

            //fetch the assortment to be loaded.
            Assortment assortment;
            try
            {
                assortment = Assortment.GetById(selectedInfo.Id);
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);
                CommonHelper.RecordException(ex);
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(selectedInfo.Name, OperationType.Open);
                return;
            }

            //check if we are merging or replacing.
            if (this.HasAssortmentDetails)
            {
                //ask user if they want to merge or replace.
                ShowWaitCursor(false);
                Boolean? mergeAssortments = DoesUserWantToMergeAssortments();
                if (mergeAssortments == null) return;
                ShowWaitCursor(true);


                if (mergeAssortments == true)
                {
                    this.AssortmentModel.AddAssortment(assortment);
                }
                else
                {
                    this.AssortmentModel.ReplaceAssortment(assortment);
                }
            }
            else
            {
                this.AssortmentModel.ReplaceAssortment(assortment);
            }


            //Now add any new products from the selected assortment that are
            //not already in the parent planogram's product list
            List<Product> productsToAdd;
            try
            {
                //check master data produc tis not deleted
                //(if assortmentProduct's productId is null, the product has been deleted)
                productsToAdd = ProductList.FetchByProductIds(assortment.Products.Select(a => a.ProductId))
                    .Where(p => !p.DateDeleted.HasValue)//do not add deleted products
                    .ToList();
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);
                CommonHelper.RecordException(ex);
                return;
            }

            if (productsToAdd.Any())
            {
                AddProductsToParentPlanogram(productsToAdd);
            }

            this.Planogram.EndUndoableAction();

            RefreshRangedProductCounts();

            ShowWaitCursor(false);

        }

        /// <summary>
        /// Presents the user with a message box asking if they want to merge or overwrite the existing assortment, or cancel.
        /// </summary>
        /// <returns>True for merge, false for overwrite, null for cancel.</returns>
        private Boolean? DoesUserWantToMergeAssortments()
        {
            ModalMessageResult result =
            GetWindowService().ShowMessage(
                Ccm.Common.Wpf.Services.MessageWindowType.Question,
                Message.AssortmentDocument_SelectAssortment_Header,
                String.Format(Message.AssortmentDocument_SelectAssortmentMessage_Description, this.Planogram.Name),
                3,
                Message.AssortmentDocument_SelectAssortment_Merge,
                Message.AssortmentDocument_SelectAssortment_Overwrite,
                Message.Generic_Cancel,
                ModalMessageButton.Button1, ModalMessageButton.Button3);


            switch (result)
            {
                case ModalMessageResult.Button1:
                    return true;
                case ModalMessageResult.Button2:
                    return false;
                default:
                    return null;
            }
        }

        #endregion

        #region UpdateAssortmentFromProducts

        private RelayCommand _updateAssortmentFromProductsCommand;

        /// <summary>
        /// Show the Select Assortment dialog.
        /// </summary>
        public RelayCommand UpdateAssortmentFromProductsCommand
        {
            get
            {
                if (_updateAssortmentFromProductsCommand == null)
                {
                    _updateAssortmentFromProductsCommand =
                        new RelayCommand(
                            p => UpdateAssortmentFromProducts_Executed(),
                            p => UpdateAssortmentFromProducts_CanExecute())
                        {
                            Icon = ImageResources.Assortment_UpdateAssortmentFromProduct_32,
                            SmallIcon = ImageResources.Assortment_UpdateAssortmentFromProduct_32,
                            FriendlyName = Message.AssortmentDocument_UpdateAssortmentFromProducts,
                            FriendlyDescription = Message.AssortmentDocument_UpdateAssortmentFromProducts_Description
                        };
                }
                return _updateAssortmentFromProductsCommand;
            }
        }

        private Boolean UpdateAssortmentFromProducts_CanExecute()
        {
            return true;
        }

        private void UpdateAssortmentFromProducts_Executed()
        {
            base.ShowWaitCursor(true);
            //store list of gtins we've already processed
            List<String> processedGtins = new List<String>();

            //go through the products already in the assortment and update if they're ranged
            Planogram planogram = Planogram.Model;
            var planogramAssortmentProductVisitor = new PlanogramAssortmentProductVisitor(new PlanogramAssortmentProductVisitor.PlanAssortmentData(planogram, AssortmentModel));
            foreach (PlanogramAssortmentProduct prod in this.AssortmentModel.Products)
            {
                //check to see if we've processed this before
                if (processedGtins.Any(p => p == prod.Gtin)) continue;
                planogramAssortmentProductVisitor.UpdateRangedAssortmentProduct(prod, processedGtins, p => p.GetPlanogramProduct().Gtin == prod.Gtin);
            }

            //go through the products already in the planogram and update if they're ranged
            foreach (PlanogramProduct planProd in this.Planogram.Model.Positions.Select(p => p.GetPlanogramProduct()))
            {
                if (processedGtins.Any(p => p == planProd.Gtin)) continue;
                List<PlanogramPosition> positionsInPlan = this.Planogram.Model.Positions.Where(p => p.GetPlanogramProduct().Gtin == planProd.Gtin).ToList();

                //on the planogram but not in the assortment - add it and range it
                PlanogramAssortmentProduct planAssortmentProduct = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(planProd.Gtin, planProd.Name);
                planAssortmentProduct.IsRanged = true;
                planAssortmentProduct.Facings = PlanogramAssortmentProductVisitor.SafeConvertToByte(positionsInPlan.Sum(p => p.FacingsWide));
                planAssortmentProduct.Units = PlanogramAssortmentProductVisitor.SafeConvertToInt16(positionsInPlan.Sum(p => p.TotalUnits));
                this.AssortmentModel.Products.Add(planAssortmentProduct);

                processedGtins.Add(planProd.Gtin);
                
            }
            base.ShowWaitCursor(false);
        }

        #endregion


        #region SaveAssortment

        private RelayCommand _saveAssortmentCommand;

        /// <summary>
        /// Show the Save Assortment dialog.
        /// </summary>
        public RelayCommand SaveAssortmentCommand
        {
            get
            {
                if (_saveAssortmentCommand == null)
                {
                    _saveAssortmentCommand =
                        new RelayCommand(
                            p => SaveAssortment_Executed(),
                            p => SaveAssortment_CanExecute())
                        {
                            Icon = ImageResources.Assortment_SaveToLinkedAssortment_32,
                            FriendlyName = Message.AssortmentDocument_SaveAssortmentToRepository,
                            FriendlyDescription = Message.AssortmentDocument_SaveAssortment_FriendlyDescription,
                            SmallIcon = ImageResources.Assortment_SaveToLinkedAssortment_16
                        };
                }
                return _saveAssortmentCommand;
            }
        }

        private Boolean SaveAssortment_CanExecute()
        {
            this.SaveAssortmentCommand.DisabledReason = String.Empty;

            //See if we can save the current assortment
            if (this.SaveAssortmentToRepositoryCommand != null)
            {
                if (!this.SaveAssortmentToRepository_CanExecute())
                {
                    //we can't so see if we can save as a new assortment
                    if (this.SaveAsAssortmentToRepositoryCommand != null)
                    {
                        if (!this.SaveAsAssortmentToRepository_CanExecute())
                        {
                            //we can't save as that either so set disabled reason
                            this.SaveAssortmentCommand.DisabledReason = this.SaveAsAssortmentToRepositoryCommand.DisabledReason;
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Saves the Assortment either to Repository if possible or as a new Assortment if not already linked
        /// </summary>
        private void SaveAssortment_Executed()
        {
            //try to execute the Save to Repository command first
            if (this.SaveAssortmentToRepositoryCommand != null)
            {
                if (this.SaveAssortmentToRepository_CanExecute())
                {
                    this.SaveAssortmentToRepositoryCommand.Execute();
                    return;
                }
            }

            //we can't, so try to execute the Save As to Repository command
            if (this.SaveAsAssortmentToRepositoryCommand != null)
            {
                if (this.SaveAsAssortmentToRepository_CanExecute())
                {
                    this.SaveAsAssortmentToRepositoryCommand.Execute();
                    return;
                }
            }
        }

        #endregion



        #region SaveAssortmentToRepository

        private RelayCommand _saveAssortmentToRepositoryCommand;

        /// <summary>
        /// Show the Select Assortment dialog.
        /// </summary>
        public RelayCommand SaveAssortmentToRepositoryCommand
        {
            get
            {
                if (_saveAssortmentToRepositoryCommand == null)
                {
                    _saveAssortmentToRepositoryCommand =
                        new RelayCommand(
                            p => SaveAssortmentToRepository_Executed(),
                            p => SaveAssortmentToRepository_CanExecute())
                        {
                            Icon = ImageResources.Assortment_SaveToLinkedAssortment_32,
                            FriendlyName = Message.AssortmentDocument_SaveAssortmentToRepository,
                            FriendlyDescription = Message.AssortmentDocument_SaveAssortmentToRepository_Description,
                            SmallIcon = ImageResources.Assortment_SaveToLinkedAssortment_16
                        };
                }
                return _saveAssortmentToRepositoryCommand;
            }
        }

        private Boolean SaveAssortmentToRepository_CanExecute()
        {
            //  Check wether there is a connection to a repository.
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.SaveAssortmentToRepositoryCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }

            if(_assortmentInfos != null && !_assortmentInfos.Any(a => a.Name == this.AssortmentModel.Name))
            {
                this.SaveAssortmentToRepositoryCommand.DisabledReason = Message.AssortmentDocument_SaveAssortmentToRepository_NotLinked_DisabledReason;
                return false;
            }

            return true;
        }

        private void SaveAssortmentToRepository_Executed()
        {
            base.ShowWaitCursor(true);

            String assortmentName = this.AssortmentModel.Name;

            Boolean nameAccepted = true;

            // if it's a totally new assortment, get a new name
            if (assortmentName == Galleria.Framework.Planograms.Resources.Language.Message.PlanogramAssortment_Name_Default)
            {
                //get new name
                
                Predicate<String> isUniqueCheck =
                   (s) =>
                   {
                       Boolean returnValue = true;

                       base.ShowWaitCursor(true);

                       foreach (AssortmentInfo locInfo in
                           AssortmentInfoList.FetchByEntityId(App.ViewState.EntityId))
                       {
                           if (locInfo.Name.ToLowerInvariant() == s.ToLowerInvariant())
                           {
                               returnValue = false;
                               break;
                           }
                       }
                       return returnValue;
                   };
                base.ShowWaitCursor(false);

                nameAccepted = Galleria.Framework.Controls.Wpf.Helpers.PromptUserForValue(Message.Generic_SaveAs, Message.Generic_SaveAs_NameDescription,
                Message.Generic_NameIsNotUnique, Assortment.NameProperty.FriendlyName, 50,
                    /*forceFirstShow*/true, isUniqueCheck, String.Empty, out assortmentName);

                
                base.ShowWaitCursor(true);
            }

            if (nameAccepted)
            {
                this.AssortmentModel.Name = assortmentName;

                SaveAssortmentByName(assortmentName);
            }

            base.ShowWaitCursor(false);
        }

        private void SaveAssortmentByName(String assortmentName)
        {            
            //try to get the assortment previously assigned
            Assortment assortment = null;
            try
            {
                assortment = Assortment.FetchByEntityIdName(App.ViewState.EntityId, this.AssortmentModel.Name);
            }
            catch (Exception e) { }

            if (assortment == null)
            {
                assortment = Assortment.NewAssortment(App.ViewState.EntityId);
            }

            assortment.Name = assortmentName;

            #region set properties
            assortment.Products.Clear();
            assortment.LocalProducts.Clear();
            assortment.Regions.Clear();
            assortment.InventoryRules.Clear();
            assortment.ProductBuddies.Clear();
            assortment.LocationBuddies.Clear();

            Dictionary<String, ProductInfo> gtinToIdDictionary = ProductInfoList.FetchByEntityId(App.ViewState.EntityId).ToDictionary(p => p.Gtin);
            Dictionary<String, LocationInfo> codeToIdDictionary = LocationInfoList.FetchByEntityId(App.ViewState.EntityId).ToDictionary(p => p.Code);
            List<String> unselectedGtins = new List<String>();            

            ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(App.ViewState.EntityId);

            assortment.ProductGroupId = hierarchy.RootGroup.Id;

            IEnumerable<ProductGroup> groups = hierarchy.FetchAllGroups();

            ProductGroup productGroup = groups.FirstOrDefault(g => g.Code == this.Planogram.CategoryCode);
            if (productGroup != null)
            {
                assortment.ProductGroupId = productGroup.Id;
            }

            #region products
            foreach (PlanogramAssortmentProduct preAssortmentProduct in this.AssortmentModel.Products)
            {
                AssortmentProduct product = AssortmentProduct.NewAssortmentProduct();

                product.Comments = preAssortmentProduct.Comments;
                product.ExactListFacings = preAssortmentProduct.ExactListFacings;
                product.ExactListUnits = preAssortmentProduct.ExactListUnits;
                product.Facings = preAssortmentProduct.Facings;
                product.FamilyRuleName = preAssortmentProduct.FamilyRuleName;
                product.FamilyRuleType = preAssortmentProduct.FamilyRuleType;
                product.FamilyRuleValue = preAssortmentProduct.FamilyRuleValue;
                product.Gtin = preAssortmentProduct.Gtin;
                product.IsRanged = preAssortmentProduct.IsRanged;
                product.IsPrimaryRegionalProduct = preAssortmentProduct.IsPrimaryRegionalProduct;
                product.MaxListFacings = preAssortmentProduct.MaxListFacings;
                product.MaxListUnits = preAssortmentProduct.MaxListUnits;
                product.MinListFacings = preAssortmentProduct.MinListFacings;
                product.MinListUnits = preAssortmentProduct.MinListUnits;
                product.Name = preAssortmentProduct.Name;
                product.PreserveListFacings = preAssortmentProduct.PreserveListFacings;
                product.PreserveListUnits = preAssortmentProduct.PreserveListUnits;
                product.ProductLocalizationType = preAssortmentProduct.ProductLocalizationType;
                product.ProductTreatmentType = preAssortmentProduct.ProductTreatmentType;
                product.Rank = preAssortmentProduct.Rank;
                product.Segmentation = preAssortmentProduct.Segmentation;
                product.Units = preAssortmentProduct.Units;

                //only add if we can find the product
                ProductInfo info = null;
                if (gtinToIdDictionary.TryGetValue(preAssortmentProduct.Gtin, out info))
                {
                    product.ProductId = info.Id;
                    assortment.Products.Add(product);
                }
                else
                {
                    unselectedGtins.Add(preAssortmentProduct.Gtin);
                }
            }
            #endregion
            #region local products
            foreach (PlanogramAssortmentLocalProduct planLocalProduct in this.AssortmentModel.LocalProducts)
            {
                AssortmentLocalProduct localProduct = AssortmentLocalProduct.NewAssortmentLocalProduct();

                LocationInfo info = null;
                if (codeToIdDictionary.TryGetValue(planLocalProduct.LocationCode, out info))
                {
                    localProduct.LocationId = info.Id;

                    ProductInfo prodInfo = null;
                    if (gtinToIdDictionary.TryGetValue(planLocalProduct.ProductGtin, out prodInfo))
                    {
                        localProduct.ProductId = info.Id;

                        assortment.LocalProducts.Add(localProduct);
                    }
                }
            }
            #endregion
            #region regions
            foreach (PlanogramAssortmentRegion planRegion in this.AssortmentModel.Regions)
            {
                AssortmentRegion region = AssortmentRegion.NewAssortmentRegion();
                region.Name = planRegion.Name;

                Boolean addRegion = false;

                foreach (PlanogramAssortmentRegionLocation planRegionLocation in planRegion.Locations)
                {
                    AssortmentRegionLocation regionLocation = AssortmentRegionLocation.NewAssortmentRegionLocation();

                    LocationInfo info = null;
                    if (codeToIdDictionary.TryGetValue(planRegionLocation.LocationCode, out info))
                    {
                        regionLocation.LocationId = info.Id;

                        region.Locations.Add(regionLocation);

                        addRegion = true;
                    }
                }

                foreach (PlanogramAssortmentRegionProduct planRegionProduct in planRegion.Products)
                {
                    AssortmentRegionProduct regionProduct = AssortmentRegionProduct.NewAssortmentRegionProduct();

                    ProductInfo info = null;
                    if (gtinToIdDictionary.TryGetValue(planRegionProduct.PrimaryProductGtin, out info))
                    {
                        regionProduct.PrimaryProductId = info.Id;


                        info = null;
                        if (gtinToIdDictionary.TryGetValue(planRegionProduct.RegionalProductGtin, out info))
                        {
                            regionProduct.RegionalProductId = info.Id;

                            region.Products.Add(regionProduct);

                            addRegion = true;
                        }
                    }
                }

                if (addRegion)
                {
                    assortment.Regions.Add(region);
                }

            }
            #endregion
            #region inventory rules
            foreach (PlanogramAssortmentInventoryRule planInventoryRule in this.AssortmentModel.InventoryRules)
            {
                AssortmentInventoryRule inventoryRule = AssortmentInventoryRule.NewAssortmentInventoryRule();

                ProductInfo info = null;
                if (gtinToIdDictionary.TryGetValue(planInventoryRule.ProductGtin, out info))
                {
                    inventoryRule.ProductId = info.Id;

                    inventoryRule.CasePack = planInventoryRule.CasePack;
                    inventoryRule.DaysOfSupply = planInventoryRule.DaysOfSupply;
                    inventoryRule.MinFacings = planInventoryRule.MinFacings;
                    inventoryRule.MinUnits = planInventoryRule.MinUnits;

                    inventoryRule.ReplenishmentDays = planInventoryRule.ReplenishmentDays;
                    inventoryRule.ShelfLife = planInventoryRule.ShelfLife;
                    inventoryRule.WasteHurdleCasePack = planInventoryRule.WasteHurdleCasePack;
                    inventoryRule.WasteHurdleUnits = planInventoryRule.WasteHurdleUnits;

                    assortment.InventoryRules.Add(inventoryRule);
                }
            }
            #endregion
            #region product buddies
            foreach (PlanogramAssortmentProductBuddy planProductBuddy in this.AssortmentModel.ProductBuddies)
            {
                AssortmentProductBuddy productBuddy = AssortmentProductBuddy.NewAssortmentProductBuddy();

                ProductInfo info = null;
                if (gtinToIdDictionary.TryGetValue(planProductBuddy.ProductGtin, out info))
                {
                    productBuddy.ProductId = info.Id;
                    
                    info = null;
                    if (planProductBuddy.S1ProductGtin != null && gtinToIdDictionary.TryGetValue(planProductBuddy.S1ProductGtin, out info))
                    {
                        productBuddy.S1ProductId = info.Id;
                    }
                    info = null;
                    if (planProductBuddy.S2ProductGtin != null && gtinToIdDictionary.TryGetValue(planProductBuddy.S2ProductGtin, out info))
                    {
                        productBuddy.S2ProductId = info.Id;
                    }
                    info = null;
                    if (planProductBuddy.S3ProductGtin != null && gtinToIdDictionary.TryGetValue(planProductBuddy.S3ProductGtin, out info))
                    {
                        productBuddy.S3ProductId = info.Id;
                    }
                    info = null;
                    if (planProductBuddy.S4ProductGtin != null && gtinToIdDictionary.TryGetValue(planProductBuddy.S4ProductGtin, out info))
                    {
                        productBuddy.S4ProductId = info.Id;
                    }
                    info = null;
                    if (planProductBuddy.S5ProductGtin != null && gtinToIdDictionary.TryGetValue(planProductBuddy.S5ProductGtin, out info))
                    {
                        productBuddy.S5ProductId = info.Id;
                    }

                    productBuddy.ProductAttributeType = planProductBuddy.ProductAttributeType;
                    productBuddy.S1Percentage = planProductBuddy.S1Percentage;
                    productBuddy.S2Percentage = planProductBuddy.S2Percentage;
                    productBuddy.S3Percentage = planProductBuddy.S3Percentage;
                    productBuddy.S4Percentage = planProductBuddy.S4Percentage;
                    productBuddy.S5Percentage = planProductBuddy.S5Percentage;
                    productBuddy.SourceType = planProductBuddy.SourceType;
                    productBuddy.TreatmentType = planProductBuddy.TreatmentType;
                    productBuddy.TreatmentTypePercentage = planProductBuddy.TreatmentTypePercentage;

                    assortment.ProductBuddies.Add(productBuddy);
                }
            }
            #endregion
            #region location buddies
            foreach (PlanogramAssortmentLocationBuddy planLocationBuddy in this.AssortmentModel.LocationBuddies)
            {
                AssortmentLocationBuddy locationBuddy = AssortmentLocationBuddy.NewAssortmentLocationBuddy();

                LocationInfo info = null;
                if (codeToIdDictionary.TryGetValue(planLocationBuddy.LocationCode, out info))
                {
                    locationBuddy.LocationId = info.Id;

                    info = null;
                    if (codeToIdDictionary.TryGetValue(planLocationBuddy.S1LocationCode, out info))
                    {
                        locationBuddy.S1LocationId = info.Id;
                    }
                    info = null;
                    if (codeToIdDictionary.TryGetValue(planLocationBuddy.S2LocationCode, out info))
                    {
                        locationBuddy.S2LocationId = info.Id;
                    }
                    info = null;
                    if (codeToIdDictionary.TryGetValue(planLocationBuddy.S3LocationCode, out info))
                    {
                        locationBuddy.S3LocationId = info.Id;
                    }
                    info = null;
                    if (codeToIdDictionary.TryGetValue(planLocationBuddy.S4LocationCode, out info))
                    {
                        locationBuddy.S4LocationId = info.Id;
                    }
                    info = null;
                    if (codeToIdDictionary.TryGetValue(planLocationBuddy.S5LocationCode, out info))
                    {
                        locationBuddy.S5LocationId = info.Id;
                    }
                    locationBuddy.S1Percentage = planLocationBuddy.S1Percentage;
                    locationBuddy.S2Percentage = planLocationBuddy.S2Percentage;
                    locationBuddy.S3Percentage = planLocationBuddy.S3Percentage;
                    locationBuddy.S4Percentage = planLocationBuddy.S4Percentage;
                    locationBuddy.S5Percentage = planLocationBuddy.S5Percentage;
                    locationBuddy.TreatmentType = planLocationBuddy.TreatmentType;

                    assortment.LocationBuddies.Add(locationBuddy);
                }
            }
            #endregion
            #endregion

            assortment.Save();

            if (unselectedGtins.Any())
            {
                base.ShowWaitCursor(false);
                //show message saying x number were not selected
                Galleria.Framework.Controls.Wpf.ModalMessage win = new Galleria.Framework.Controls.Wpf.ModalMessage();
                win.Header = Message.AssortmentDocument_ProductsNotInRepository_Header;
                win.Description = String.Format(Message.AssortmentDocument_ProductsNotInRepository, unselectedGtins.Count());
                win.MessageIcon = ImageResources.Dialog_Warning;
                win.ButtonCount = 2;
                win.Button1Content = Message.Generic_OK;
                win.Button2Content = Message.Generic_CopyToClipboard;
                win.DefaultButton = Galleria.Framework.Controls.Wpf.ModalMessageButton.Button1;
                App.ShowWindow(win, true);

                if (win.Result == Framework.Controls.Wpf.ModalMessageResult.Button2)
                {
                    String clip = "";

                    foreach (String gtin in unselectedGtins)
                    {
                        clip += gtin + "\r\n";
                    }

                    Clipboard.SetText(clip);
                }
                base.ShowWaitCursor(true);
            }

            _productHierarchy = ProductHierarchy.FetchByEntityId(App.ViewState.EntityId);

            if (_productHierarchy != null)
            {
                _productGroupList = _productHierarchy.FetchAllGroups();
            }

            _assortmentInfos = AssortmentInfoList.FetchByEntityId(App.ViewState.EntityId);
        }

        #endregion

        #region SaveAsAssortmentToRepository

        private RelayCommand _saveAsAssortmentToRepositoryCommand;

        /// <summary>
        /// Show the Select Assortment dialog.
        /// </summary>
        public RelayCommand SaveAsAssortmentToRepositoryCommand
        {
            get
            {
                if (_saveAsAssortmentToRepositoryCommand == null)
                {
                    _saveAsAssortmentToRepositoryCommand =
                        new RelayCommand(
                            p => SaveAsAssortmentToRepository_Executed(),
                            p => SaveAsAssortmentToRepository_CanExecute())
                        {
                            Icon =ImageResources.Assortment_SaveAsNewAssortment_32,
                            SmallIcon = ImageResources.Assortment_SaveAsNewAssortment_16,
                            FriendlyName = Message.AssortmentDocument_SaveAsAssortmentToRepository,
                            FriendlyDescription = Message.AssortmentDocument_SaveAsAssortmentToRepository_Description
                        };
                }
                return _saveAsAssortmentToRepositoryCommand;
            }
        }

        private Boolean SaveAsAssortmentToRepository_CanExecute()
        {
            //  Check wether there is a connection to a repository.
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.SaveAsAssortmentToRepositoryCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }

            if (this.AssortmentModel.Products.Count == 0)
            {
                this.SaveAsAssortmentToRepositoryCommand.DisabledReason = Message.AssortmentDocument_SaveAsAssortmentToRepository_NoProducts_DisabledReason;
                return false;
            }

            return true;
        }

        private void SaveAsAssortmentToRepository_Executed()
        {
            String assortmentName = this.AssortmentModel.Name;

            //get new name
            Predicate<String> isUniqueCheck =
                (s) =>
                {
                    Boolean returnValue = true;

                    base.ShowWaitCursor(true);

                    foreach (AssortmentInfo locInfo in
                        AssortmentInfoList.FetchByEntityId(App.ViewState.EntityId))
                    {
                        if (locInfo.Name.ToLowerInvariant() == s.ToLowerInvariant())
                        {
                            returnValue = false;
                            break;
                        }
                    }

                    return returnValue;
                };
            base.ShowWaitCursor(false);

            Boolean nameAccepted = Galleria.Framework.Controls.Wpf.Helpers.PromptUserForValue(Message.Generic_SaveAs, Message.Generic_SaveAs_NameDescription,
            Message.Generic_NameIsNotUnique, Assortment.NameProperty.FriendlyName, /*inputMaxLength*/50, /*inputRequired*/true,
                /*forceFirstShow*/true, isUniqueCheck, String.Empty, out assortmentName);


            base.ShowWaitCursor(true);

            if (nameAccepted)
            {
                this.AssortmentModel.Name = assortmentName;

                SaveAssortmentByName(assortmentName);
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the planogram products collection changes.
        /// </summary>
        private void Planogram_ProductsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            ReloadRows();
        }

        /// <summary>
        /// Called whenever the planogram products collection changes.
        /// </summary>
        private void Planogram_AssortmentProductsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            ReloadRows();
        }

        /// <summary>
        ///     Called when the items held by the selected plan items changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedPlanItems_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            if (_isSynchingSelection) return;

            _isSynchingSelection = true;

            try
            {
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        {
                            foreach (IPlanItem item in e.ChangedItems)
                            {
                                if (item.Product != null)
                                {
                                    IPlanItem row = this.Rows.FirstOrDefault(r => r.Product.Gtin.Equals(item.Product.Gtin));
                                    if (row != null)
                                    {
                                        if (!this.SelectedProducts.Contains(row))
                                        {
                                            this.SelectedProducts.Add(row);
                                        }
                                    }
                                }
                            }
                        }
                        break;

                    case NotifyCollectionChangedAction.Remove:
                        {
                            foreach (IPlanItem item in e.ChangedItems)
                            {
                                if (item.Product != null)
                                {
                                    IPlanItem row = this.Rows.FirstOrDefault(r => r.Product.Gtin.Equals(item.Product.Gtin));
                                    this.SelectedProducts.Remove(row);
                                }
                            }
                        }
                        break;

                    case NotifyCollectionChangedAction.Reset:
                        {

                            this.SelectedProducts.Clear();

                            foreach (var item in SelectedPlanItems)
                            {
                                if (item.Product != null)
                                {
                                    IPlanItem row = Rows.FirstOrDefault(r => r.Product.Gtin.Equals(item.Product.Gtin));
                                    if (row != null)
                                    {
                                        if (!SelectedProducts.Contains(row))
                                        {
                                            SelectedProducts.Add(row);
                                        }
                                    }
                                }
                            }
                        }
                        break;
                }
            }
            finally
            {
                _isSynchingSelection = false;
            }

        }

        /// <summary>
        /// Called when the items in the SelectedProducts collection change.
        /// </summary>
        private void SelectedProducts_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            if (_isSynchingSelection) return;


            _isSynchingSelection = true;

            try
            {
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        {
                            //  Get all products selected by the user (including the new ones).
                            List<String> productGtinsToAdd = e.ChangedItems.Cast<IPlanItem>().Select(row => row.Product.Gtin).ToList();

                            //  Get all the positions for the selected products.
                            List<PlanogramPositionView> positionsToAdd = this.Planogram.EnumerateAllPositions().Where(p => productGtinsToAdd.Contains(p.Product.Gtin)).ToList();

                            //  Add all the selected items, replacing previous selections.
                            this.SelectedPlanItems.AddRange(positionsToAdd.Except(this.SelectedPlanItems.ToList()));
                        }
                        break;

                    case NotifyCollectionChangedAction.Remove:
                        {
                            List<String> productGtinsToRemove = e.ChangedItems.Cast<IPlanItem>().Select(r => r.Product.Gtin).ToList();

                            foreach (IPlanItem item in this.SelectedPlanItems.ToList())
                            {
                                if (item.Product != null && productGtinsToRemove.Contains(item.Product.Gtin))
                                {
                                    this.SelectedPlanItems.Remove(item);
                                }
                            }
                        }
                        break;

                    case NotifyCollectionChangedAction.Reset:
                        {
                            //clear and reselect

                            //  Get all products selected by the user (including the new ones).
                            List<String> productGtinsToAdd = this.SelectedProducts.Select(row => row.Product.Gtin).ToList();

                            //  Get all the positions for the selected products.
                            List<PlanogramPositionView> positionsToAdd = this.Planogram.EnumerateAllPositions().Where(p => productGtinsToAdd.Contains(p.Product.Gtin)).ToList();

                            //  Add all the selected items, replacing previous selections.
                            this.SelectedPlanItems.SetSelection(positionsToAdd);

                        }
                        break;
                }
            }
            finally
            {
                _isSynchingSelection = false;
            }

        }

        #endregion

        #region Methods

        /// <summary>
        /// Subscribes and unsubscribes event handlers
        /// </summary>
        /// <param name="isAttach"></param>
        private void SetPlanEventHandlers(Boolean isAttach)
        {
            if (isAttach)
            {
                this.Planogram.Model.Products.BulkCollectionChanged += Planogram_ProductsBulkCollectionChanged;
                this.Planogram.Model.Assortment.Products.BulkCollectionChanged += Planogram_AssortmentProductsBulkCollectionChanged;
                this.Planogram.Model.Assortment.Products.ChildChanged += AssortmentProducts_ChildChanged;

                this.SelectedPlanItems.BulkCollectionChanged += SelectedPlanItems_BulkCollectionChanged;
                this.SelectedProducts.BulkCollectionChanged += SelectedProducts_BulkCollectionChanged;
            }
            else
            {
                this.Planogram.Model.Products.BulkCollectionChanged -= Planogram_ProductsBulkCollectionChanged;
                this.Planogram.Model.Assortment.Products.BulkCollectionChanged -= Planogram_AssortmentProductsBulkCollectionChanged;
                this.Planogram.Model.Assortment.Products.ChildChanged -= AssortmentProducts_ChildChanged;

                this.SelectedPlanItems.BulkCollectionChanged -= SelectedPlanItems_BulkCollectionChanged;
                this.SelectedProducts.BulkCollectionChanged -= SelectedProducts_BulkCollectionChanged;
            }
        }

        /// <summary>
        /// Assortment product list child changed event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssortmentProducts_ChildChanged(object sender, Csla.Core.ChildChangedEventArgs e)
        {
            if (e.PropertyChangedArgs.PropertyName == PlanogramAssortmentProduct.IsRangedProperty.Name)
            {
                RefreshRangedProductCounts();
            }
        }

        /// <summary>
        /// Method to handle the fresh of product counts
        /// </summary>
        private void RefreshRangedProductCounts()
        {
            OnPropertyChanged(NotRangedProductsCountProperty);
            OnPropertyChanged(RangedProductsCountProperty);
        }

        /// <summary>
        /// Called whenever this document should
        /// stop updating
        /// </summary>
        protected override void OnFreeze()
        {
            SetPlanEventHandlers(false);
        }

        /// <summary>
        /// Called whenever this document should resume
        /// updating.
        /// </summary>
        protected override void OnUnfreeze()
        {
            SetPlanEventHandlers(true);

            ReloadRows();
        }

        /// <summary>
        /// Adds the given masterdata products to the plangoram.
        /// </summary>
        private void AddProductsToParentPlanogram(IEnumerable<Product> productsToAdd)
        {
            //check if they are already on the plan
            List<String> takenPlanProductGtins = this.Planogram.Products.Select(p => p.Gtin).ToList();

            List<Product> productsToAddToPlan = new List<Product>();
            foreach (Product product in productsToAdd)
            {
                if (!takenPlanProductGtins.Contains(product.Gtin))
                {
                    productsToAddToPlan.Add(product);
                }
            }
            if (!productsToAddToPlan.Any()) return;

            //and do the same for the current planogram
            this.Planogram.BeginUndoableAction();

            //add any new products
            this.Planogram.AddProducts(productsToAddToPlan);

            this.Planogram.EndUndoableAction();
        }

        /// <summary>
        /// Adds the given enumeration of Product Library Products to the plan, 
        /// updating their Performance Data in the plan.
        /// </summary>
        /// <param name="productLibraryProducts">The Product Library Products to add.</param>
        public void AddProductsToParentPlanogram(IEnumerable<ProductLibraryProduct> productLibraryProducts)
        {
            // Add the products to the plan.
            AddProductsToParentPlanogram(productLibraryProducts.Select(p => p.Product));

            // Build a dictionary of planogram products.
            if (this.Planogram == null || this.Planogram.Model == null || this.Planogram.Model.Products == null) return;
            Dictionary<String, PlanogramProduct> productsByGtin = this.Planogram.Model.Products.
                ToDictionary(p => p.Gtin, p => p, StringComparer.InvariantCultureIgnoreCase);

            // Ensure any performance data is copied over as well.
            foreach (ProductLibraryProduct productLibraryProduct in productLibraryProducts)
            {
                PlanogramProduct planogramProduct;
                if (productsByGtin.TryGetValue(productLibraryProduct.Gtin, out planogramProduct))
                {
                    ParentController.CopyPerformanceData(productLibraryProduct, planogramProduct);
                }
            }
        }

        /// <summary>
        /// Adds a collection of Products to the assortment.
        /// </summary>
        /// <param name="products"></param>
        public void AddProducts(IEnumerable<Product> products)
        {
            List<String> newPlanProductGtins = new List<String>();
            List<IPlanogramProductInfo> existingProducts = new List<IPlanogramProductInfo>();
            PlanogramAssortment assortment = this.Planogram.Model.Assortment;

            //before we add the products to the assortment, they need to be added to the planogram,
            //otherwise they will not be included correctly in the assortment
            if (products.Any())
            {
                // Add any products not already in the plan to the plan.
                AddProductsToParentPlanogram(products);
            }

            foreach (IPlanogramProductInfo product in products)
            {
                if (assortment.Products.Any(p => p.Gtin == product.Gtin))
                {
                    existingProducts.Add(product);
                }
                else
                {
                    assortment.Products.Add(PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(product));
                }
            }

            if (existingProducts.Count > 0)
            {
                GetWindowService()
                    .ShowOkMessage(Ccm.Common.Wpf.Services.MessageWindowType.Warning,
                    Message.AssortmentPlanDocument_ProductsAlreadyAdded,
                     Message.AssortmentPlanDocument_ProductsAlreadyAdded_Description);
            }
        }

        /// <summary>
        /// Adds a collection of Products to the assortment.
        /// </summary>
        /// <param name="products"></param>
        public void AddProducts(IEnumerable<IPlanogramProductInfo> products)
        {
            List<IPlanogramProductInfo> existingProducts = new List<IPlanogramProductInfo>();
            PlanogramAssortment assortment = this.Planogram.Model.Assortment;

            foreach (IPlanogramProductInfo product in products)
            {
                if (assortment.Products.Any(p => p.Gtin == product.Gtin))
                {
                    existingProducts.Add(product);
                }
                else
                {
                    assortment.Products.Add(PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(product));
                }
            }

            if (existingProducts.Count > 0)
            {
                GetWindowService()
                    .ShowOkMessage(Ccm.Common.Wpf.Services.MessageWindowType.Warning,
                    Message.AssortmentPlanDocument_ProductsAlreadyAdded,
                     Message.AssortmentPlanDocument_ProductsAlreadyAdded_Description);
            }
        }

        /// <summary>
        /// Adds a collection of Products to the assortment (when dropped from another assortment view).
        /// </summary>
        /// <param name="productViews"></param>
        public void AddProducts(IEnumerable<PlanogramProductView> productViews)
        {
            List<String> newPlanProductGtins = new List<String>();
            List<IPlanogramProductInfo> existingProducts = new List<IPlanogramProductInfo>();
            PlanogramAssortment assortment = this.Planogram.Model.Assortment;

            var products = productViews.Select(x => x.Product.Model).ToList();
            foreach (var productItem in products)
            {
                if (assortment.Products.Any(p => p.Gtin == productItem.Gtin))
                {
                    existingProducts.Add(productItem);
                }
                else
                {
                    assortment.Products.Add(PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(productItem));
                }

                // Check if the product is in the planogram product list
                var matchingProds = this.Planogram.Model.Products.Any(x => x.Gtin == productItem.Gtin);
                if (!matchingProds)
                {
                    // Get the product details from the source
                    newPlanProductGtins.Add(productItem.Gtin);
                }
            }

            // If there are any new products, add these to the planogram
            if (newPlanProductGtins.Any())
            {
                var productsList = ProductList.FetchByEntityIdProductGtins(App.ViewState.EntityId, newPlanProductGtins);

                // Add the products to the plan.
                AddProductsToParentPlanogram(productsList);
            }

            if (existingProducts.Count > 0)
            {
                GetWindowService()
                    .ShowOkMessage(Ccm.Common.Wpf.Services.MessageWindowType.Warning,
                        Message.AssortmentPlanDocument_ProductsAlreadyAdded,
                        Message.AssortmentPlanDocument_ProductsAlreadyAdded_Description);
            }
        }

        /// <summary>
        /// Reloads the rows collection
        /// </summary>
        private void ReloadRows()
        {
            //clear the current collection
            _rows.Clear();

            //get the list of assortment product gtins
            PlanogramAssortment assortment = this.Planogram.Model.Assortment;
            String[] gtins = assortment.Products.Select(p => p.Gtin).ToArray();

            List<PlanogramPositionView> positons =
                this.Planogram.EnumerateAllPositions().Where(p => gtins.Contains(p.Product.Gtin)).ToList();

            _rows.AddRange(positons.GroupBy(p => p.Product.Gtin).Select(g => g.First()));

            var unplacedGtins = gtins.Where(p => positons.All(pos => pos.Product.Gtin != p)).ToList();
            
            if (unplacedGtins.Any())
            {
                List<PlanogramProductView> products =
                    this.Planogram.Products.Where(p => unplacedGtins.Contains(p.Gtin)).ToList();

                _rows.AddRange(products.Select(p => p));
            }

            RefreshRangedProductCounts();
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!IsDisposed)
            {
                SetPlanEventHandlers(false);
                base.Dispose(disposing);

                IsDisposed = true;
            }
        }

        #endregion
    }

    #region Supporting Classes

    /// <summary>
    /// Binding validation rule to ensure assortment product rank is unique:
    /// </summary>
    public sealed class AssortmentProductRankRule : ValidationRule
    {
        #region Constructor
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public AssortmentProductRankRule() : base(ValidationStep.CommittedValue, true) { }
        #endregion

        #region Methods
        /// <summary>
        /// Performs the validation of our business rules
        /// </summary>
        /// <param name="value">The value to validate</param>
        /// <param name="cultureInfo">The current culture</param>
        /// <returns>The validation results</returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // assume success
            ValidationResult validationResult = ValidationResult.ValidResult;

            BindingExpression expression = value as BindingExpression;
            if (expression != null)
            {
                var sourceItem = expression.DataItem as PlanogramAssortmentProduct;
                if (sourceItem != null)
                {
                    var assortProductList = sourceItem.Parent.Products;

                    //validate non-zero, set rank values for uniquness
                    var rankedProducts = assortProductList.Where(a => a.Rank > 0);

                    // if user entered at least one rank value -> validate uniqueness
                    if (rankedProducts.Any())
                    {
                        foreach (var assortmentProduct in rankedProducts.ToList())
                        {
                            if (assortmentProduct != sourceItem)
                            {
                                if (assortmentProduct.Rank == sourceItem.Rank)
                                {
                                    validationResult = new ValidationResult(false, Message.AssortmentDocument_ProductRankValidation);
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            // and return the validation result
            return validationResult;
        }
        #endregion

    }
    
    #endregion
}
