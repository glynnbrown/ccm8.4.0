﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014
#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//  Adapted from Workflow client.
#endregion
#region Version History: (CCM 8.3.0)
// V8-32396 : A.Probyn
//  Updated for delist family rule
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Collections;
using System.Windows;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    public class AssortmentFamilyRuleRow : ViewModelObject
    {
        #region Field Names

        private String _familyRuleName;
        private PlanogramAssortmentProductFamilyRuleType _familyRuleType;
        private Byte? _familyRuleValue;
        private BulkObservableCollection<PlanogramAssortmentProduct> _assignedProducts = new BulkObservableCollection<PlanogramAssortmentProduct>();
        private List<PlanogramAssortmentProduct> _originalProducts = new List<PlanogramAssortmentProduct>();
        private Boolean _isDirty;

        #endregion

        #region Property Paths

        public static readonly PropertyPath FamilyRuleNameProperty = WpfHelper.GetPropertyPath<AssortmentFamilyRuleRow>(p => p.FamilyRuleName);
        public static readonly PropertyPath FamilyRuleTypeProperty = WpfHelper.GetPropertyPath<AssortmentFamilyRuleRow>(p => p.FamilyRuleType);
        public static readonly PropertyPath FamilyRuleValueProperty = WpfHelper.GetPropertyPath<AssortmentFamilyRuleRow>(p => p.FamilyRuleValue);
        public static readonly PropertyPath IsDirtyProperty = WpfHelper.GetPropertyPath<AssortmentFamilyRuleRow>(p => p.IsDirty);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the Family Rule Name
        /// </summary>
        public String FamilyRuleName
        {
            get { return _familyRuleName; }
            set
            {
                _familyRuleName = value;
                _isDirty = true;
            }
        }

        /// <summary>
        /// Gets the family Rule Type
        /// </summary>
        public PlanogramAssortmentProductFamilyRuleType FamilyRuleType
        {
            get { return _familyRuleType; }
            set
            {
                _familyRuleType = value;
                _isDirty = true;
            }
        }

        /// <summary>
        /// Gets the family Rule Value
        /// </summary>
        public Byte? FamilyRuleValue
        {
            get { return _familyRuleValue; }
            set
            {
                _familyRuleValue = value;
                _isDirty = true;
            }
        }

        public String FamilyRuleDetail
        {
            get
            {
                String value = PlanogramAssortmentProductFamilyRuleTypeHelper.FriendlyNames[_familyRuleType];

                if (_familyRuleType != PlanogramAssortmentProductFamilyRuleType.DependencyList &&
                    _familyRuleType != PlanogramAssortmentProductFamilyRuleType.Delist)
                {
                    value = String.Format("{0} {1}", value, _familyRuleValue);
                }
                return value;
            }
        }

        /// <summary>
        /// Returns the collection family rows
        /// </summary>
        public BulkObservableCollection<PlanogramAssortmentProduct> AssignedProducts
        {
            get { return _assignedProducts; }
        }

        /// <summary>
        /// Returns true if this row is dirty.
        /// </summary>
        public Boolean IsDirty
        {
            get { return _isDirty; }
            private set
            {
                _isDirty = value;
                OnPropertyChanged(IsDirtyProperty);
            }
        }

        /// <summary>
        /// Determines if the current family rule value is valid
        /// </summary>
        public Boolean IsValid
        {
            get
            {
                if (this._familyRuleName.Length == 0)
                {
                    return false;
                }

                if (_assignedProducts.Count == 0)
                {
                    return false;
                }

                if (_familyRuleType == PlanogramAssortmentProductFamilyRuleType.MinimumProductCount ||
                        _familyRuleType == PlanogramAssortmentProductFamilyRuleType.MaximumProductCount)
                {
                    if (_familyRuleValue == null || _familyRuleValue <= 0 || _familyRuleValue > this._assignedProducts.Count)
                    {
                        return false;
                    }
                }
                else if (_familyRuleType == PlanogramAssortmentProductFamilyRuleType.None)
                {
                    return false;
                }

                return true;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="locationSpace"></param>
        public AssortmentFamilyRuleRow(String familyRuleName, PlanogramAssortmentProductFamilyRuleType familyRuleType, Byte? familyRuleValue,
                                                List<PlanogramAssortmentProduct> assignedProducts)
        {
            //Load properties
            this._familyRuleName = familyRuleName;
            this._familyRuleType = familyRuleType;
            this._familyRuleValue = familyRuleValue;
            this._originalProducts = assignedProducts;
            this._assignedProducts.AddRange(assignedProducts);
            this._assignedProducts.BulkCollectionChanged += new EventHandler<Galleria.Framework.Model.BulkCollectionChangedEventArgs>(assignedProducts_BulkCollectionChanged);

            _isDirty = false;
        }

        #endregion

        #region Event Handlers
        
        private void assignedProducts_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            _isDirty = true;
        }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return this._familyRuleName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Commits any changes made back to the given node
        /// </summary>
        /// <param name="applyToRange"></param>
        public void CommitChanges()
        {
            if (this.IsDirty)
            {
                foreach (PlanogramAssortmentProduct assortmentProduct in _assignedProducts)
                {
                    if (_originalProducts.Contains(assortmentProduct))
                    {
                        _originalProducts.Remove(assortmentProduct);
                    }

                    assortmentProduct.FamilyRuleName = this._familyRuleName;
                    assortmentProduct.FamilyRuleType = this._familyRuleType;
                    assortmentProduct.FamilyRuleValue = this._familyRuleValue;
                }

                foreach (PlanogramAssortmentProduct assortmentProduct in _originalProducts)
                {
                    assortmentProduct.FamilyRuleName = String.Empty;
                    assortmentProduct.FamilyRuleType = PlanogramAssortmentProductFamilyRuleType.None;
                    assortmentProduct.FamilyRuleValue = null;
                }

                this.IsDirty = false;
            }
        }

        /// <summary>
        /// Nulls off this rule
        /// </summary>
        public void ClearRule()
        {
            this.FamilyRuleName = String.Empty;
            this.FamilyRuleType = PlanogramAssortmentProductFamilyRuleType.None;
            this.FamilyRuleValue = null;
            this.IsDirty = true;
        }

        public void DeleteFamilyRule()
        {
            foreach (var assortmentProduct in _originalProducts)
            {
                assortmentProduct.FamilyRuleName = String.Empty;
                assortmentProduct.FamilyRuleType = PlanogramAssortmentProductFamilyRuleType.None;
                assortmentProduct.FamilyRuleValue = null;
            }
            this.IsDirty = false;
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this._assignedProducts.BulkCollectionChanged -= assignedProducts_BulkCollectionChanged;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
