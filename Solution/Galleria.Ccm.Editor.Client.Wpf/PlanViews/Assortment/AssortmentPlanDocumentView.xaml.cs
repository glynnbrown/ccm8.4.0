﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//  Created.
// V8-26702 : A.Kuszyk
//  Added event handlers for OnDragOver and OnDragEnter.
// V8-27900 : L.Ineson
//  Added column context menu option for copy to clipboard.
#endregion

#region Version History: (CCM 8.1.1)
// V8-30360 : N.Haywood
//  Added OnViewModelSelectedRowsChanged
#endregion

#region Version History : (8.2.0)
// V8-30762 : I.George
//  Grid columns now uses CustomColumnlayout 
// V8-30705 : A.Probyn
//  Fixed implementation of CustomColumnLayout so that the grid is editable except for Gtin and name.
// V8-30777 : L.Ineson
//  Scroll row into view is now on a delay.
//  Changed how selection sync works to speed things up.
//V8-30958 : L.Ineson
//  Updated after column manager changes.
// V8-31366 : A.Probyn
//  Brought adding product library products into line with selecting an assortment, so that product library products
//  are also added to the planogram if missing.
#endregion

#region Version History : (8.3.0)
// V8-31558 : M.Shelley
//  Allow drag / drop of assortment items between 2 different plans
// V8-32869 : M.Brumby
//  Prevent drag / drop product adds when within the same grid
// V8-32718 : D.Pleasance
//  Added due to grid items now being of type IPlanItem instead of PlanogramProductView.
// CCM-18480 : D.Pleasance
//  Amended Column layout factory to use AssortmentProductCustomColumnLayoutFactory so that Product and position attributes can be selected.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using System.Windows.Controls;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews.ProductsList;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Interaction logic for AssortmentPlanDocumentView.xaml
    /// </summary>
    public sealed partial class AssortmentPlanDocumentView : IPlanDocumentView
    {
        #region Fields
        const String productPrefix = "Product.";
        private ColumnLayoutManager _columnLayoutManager;
        private Boolean _isScrollIntoViewPending;
        private Boolean _isSelectionChangeProcessing;        
        #endregion

        #region Properties

        #region ViewModel Property

        /// <summary>
        /// ViewModel dependency property definition.
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentPlanDocument),
                typeof(AssortmentPlanDocumentView),
                new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        ///     Handles the change of viewmodels for the view.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            AssortmentPlanDocumentView senderControl = (AssortmentPlanDocumentView)sender;

            if (e.OldValue != null)
            {
                AssortmentPlanDocument oldModel = (AssortmentPlanDocument)e.OldValue;
                oldModel.SelectedProducts.CollectionChanged -= senderControl.OnViewModelSelectedRowsChanged;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                AssortmentPlanDocument newModel = (AssortmentPlanDocument)e.NewValue;
                newModel.SelectedProducts.CollectionChanged += senderControl.OnViewModelSelectedRowsChanged;
                newModel.AttachedControl = senderControl;
            }
        }


        /// <summary>
        ///     Gets the viewmodel controller for this view.
        /// </summary>
        public AssortmentPlanDocument ViewModel
        {
            get { return (AssortmentPlanDocument)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="planDocument">the plan document to be visualized.</param>
        public AssortmentPlanDocumentView(AssortmentPlanDocument planDocument)
        {
            InitializeComponent();

            this.ViewModel = planDocument;

            this.Loaded += AssortmentPlanDocumentView_Loaded;
        }

        #endregion

        #region Event Handlers

        private void AssortmentPlanDocumentView_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= AssortmentPlanDocumentView_Loaded;
            
            var factory =
            new PlanogramAssortmentProductColumnLayoutFactory(typeof(PlanogramProductView),
                    this.ViewModel.Planogram.Model, null, true,
                    new List<String>
                    {
                        productPrefix + PlanogramProduct.NameProperty.Name,
                        productPrefix + PlanogramProduct.GtinProperty.Name,
                        productPrefix + PlanogramProduct.AssortmentFieldPrefix + PlanogramAssortmentProduct.IsRangedProperty.Name,
                        productPrefix + PlanogramProduct.AssortmentFieldPrefix + PlanogramAssortmentProduct.RankProperty.Name,
                        productPrefix + PlanogramProduct.AssortmentFieldPrefix + PlanogramAssortmentProduct.SegmentationProperty.Name,
                        productPrefix + PlanogramProduct.AssortmentFieldPrefix + PlanogramAssortmentProduct.FacingsProperty.Name,
                        productPrefix + PlanogramProduct.AssortmentFieldPrefix + PlanogramAssortmentProduct.UnitsProperty.Name,
                        productPrefix + PlanogramProduct.AssortmentFieldPrefix + PlanogramAssortmentProduct.ProductTreatmentTypeProperty.Name,
                        productPrefix + PlanogramProduct.AssortmentFieldPrefix + PlanogramAssortmentProduct.ProductLocalizationTypeProperty.Name,
                        productPrefix + PlanogramProduct.AssortmentFieldPrefix + PlanogramAssortmentProduct.IsPrimaryRegionalProductProperty.Name,
                        productPrefix + PlanogramProduct.AssortmentFieldPrefix + PlanogramAssortmentProduct.CommentsProperty.Name,
                        productPrefix + PlanogramProduct.AssortmentFieldPrefix + PlanogramAssortmentProduct.MetaIsProductPlacedOnPlanogramProperty.Name
                    });

            // Initialize the custom column manager.
            _columnLayoutManager = new ColumnLayoutManager(
                factory,
                this.ViewModel.Planogram.DisplayUnits,
                AssortmentPlanDocument.ScreenKey, /*PathMask*/"{0}");

            _columnLayoutManager.CalculatedColumnPath = "CalculatedValueResolver";            
            _columnLayoutManager.AttachDataGrid(xAssignedProducts);
            
            this.xAssignedProducts.DragScope = Application.Current.MainWindow.Content as FrameworkElement;
            
            //process the viewmodel selection.
            SyncViewModelSelectionToGrid();
        }


        /// <summary>
        /// Called when a drag drop action is moved over this.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnDragOver(DragEventArgs e)
        {
            base.OnDragOver(e);

            if (DragDropBehaviour.IsUnpackedType<ExtendedDataGridRowDropEventArgs>(e.Data))
            {
                ExtendedDataGridRowDropEventArgs rowArgs = DragDropBehaviour.UnpackData<ExtendedDataGridRowDropEventArgs>(e.Data);
                if (rowArgs.Items.Count > 0)
                {
                    if (rowArgs.Items.First() is Product)
                    {
                        PlanDocumentHelper.SetDragAdorner(ImageResources.ProductLibrary_ValidDragOverAdorner);
                    }
                }
            }
        }

        protected override void OnDragEnter(DragEventArgs e)
        {
            base.OnDragEnter(e);

            if (!DragDropBehaviour.IsUnpackedType<ExtendedDataGridRowDropEventArgs>(e.Data)) return;

            var rowArgs = DragDropBehaviour.UnpackData<ExtendedDataGridRowDropEventArgs>(e.Data);
            if (rowArgs.Items.Count == 0) return;

            if (rowArgs.Items.First() is PlanogramAssortmentProduct)
            {
                PlanDocumentHelper.SetDragAdorner(ImageResources.ProductLibrary_ValidDragOverAdorner);
            }
        }

        protected override void OnDrop(DragEventArgs e)
        {
            base.OnDrop(e);

            if (!DragDropBehaviour.IsUnpackedType<ExtendedDataGridRowDropEventArgs>(e.Data)) return;

            var rowArgs = DragDropBehaviour.UnpackData<ExtendedDataGridRowDropEventArgs>(e.Data);
            if (rowArgs.Items.Count <= 0) return;

            //don't perform product adds if we are dragging within the same grid
            if(Equals(rowArgs.SourceGrid, this.xAssignedProducts)) return;

            if (rowArgs.Items.First() is ProductListRow)
            {
                ViewModel.AddProducts(rowArgs.Items.Cast<ProductListRow>().Select(r => r.PlanItem.Product.Model));
            }
            else if (rowArgs.Items.First() is Product)
            {
                ViewModel.AddProducts(rowArgs.Items.Cast<Product>());
            }
            else if (rowArgs.Items.First() is PlanogramProductView)
            {
                var castItems = rowArgs.Items.Cast<PlanogramProductView>();
                ViewModel.AddProducts(castItems);
            }
            else if (rowArgs.Items.First() is ProductLibraryProduct)
            {
                // Get the library products from the row args.
                IEnumerable<ProductLibraryProduct> productLibraryProducts = rowArgs.Items.Cast<ProductLibraryProduct>();

                // Add the products to the plan.
                ViewModel.AddProductsToParentPlanogram(productLibraryProducts);

                //Add product to the assortment now its been added to the plan
                ViewModel.AddProducts(rowArgs.Items.Cast<ProductLibraryProduct>().Select(p => p.Product));
            }
        }

        /// <summary>
        /// Called when the View Model's Selected Rows collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnViewModelSelectedRowsChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            if (_isSelectionChangeProcessing) return;
            if (e.NewItems == null) return;

            IEnumerable<IPlanItem> newRows = e.NewItems.Cast<IPlanItem>();
            if (!newRows.Any()) return;

            //process the new viewmodel selection.
            _isSelectionChangeProcessing = true;
            SyncViewModelSelectionToGrid();
            _isSelectionChangeProcessing = false;

            if (_isScrollIntoViewPending) return;
            _isScrollIntoViewPending = true;

            // Ensure that the selected row is visible.
            // but do so on a delay.
            this.Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    if (this.xAssignedProducts != null && !this.IsDisposed
                        && this.xAssignedProducts.SelectedItem != null)
                    {
                        this.xAssignedProducts.ScrollIntoView(this.xAssignedProducts.SelectedItem);
                    }
                    _isScrollIntoViewPending = false;

                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        /// <summary>
        /// Called whenever the selection changes on the assigned products grid.
        /// </summary>
        private void xAssignedProducts_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (_isSelectionChangeProcessing || IsDisposed) return;

            _isSelectionChangeProcessing = true;

            // Ensure that the selected row is visible.
            // but do so on a delay.
            this.Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    if (!IsDisposed)
                    {
                        SyncGridSelectionToViewModel();
                    }
                    _isSelectionChangeProcessing = false;

                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        /// <summary>
        /// Called whenever a grid row is right clicked.
        /// </summary>
        private void xAssignedProducts_RowItemMouseRightClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if(this.ViewModel == null) return;

            ContextMenu contextMenu = new ContextMenu();

            var vm = this.ViewModel;

            //RemoveSelectedProductsCommand
            contextMenu.Items.Add(new MenuItem()
            {
                Header = vm.RemoveSelectedProductsCommand.FriendlyName,
                Command = vm.RemoveSelectedProductsCommand
            });

            //SelectAllCommand
             contextMenu.Items.Add(new MenuItem()
            {
                Header = Message.DataGridDisplay_SelectAllRows,
                Command = DataGrid.SelectAllCommand,
                CommandTarget = xAssignedProducts
            });

            //ShowProductRulesCommand
            contextMenu.Items.Add(new MenuItem()
            {
                Header = vm.ShowProductRulesCommand.FriendlyName,
                Command = vm.ShowProductRulesCommand
            });

            //ShowLocalProductsCommand
            contextMenu.Items.Add(new MenuItem()
            {
                Header = vm.ShowLocalProductsCommand.FriendlyName,
                Command = vm.ShowLocalProductsCommand
            });

            contextMenu.Items.Add(new Separator());

            //RangeSelectedProductsCommand
            contextMenu.Items.Add(new MenuItem()
            {
                Header = vm.RangeSelectedProductsCommand.FriendlyName,
                Command = vm.RangeSelectedProductsCommand
            });

            //UnrangeSelectedProductsCommand
            contextMenu.Items.Add(new MenuItem()
            {
                Header = vm.UnrangeSelectedProductsCommand.FriendlyName,
                Command = vm.UnrangeSelectedProductsCommand
            });

            //RangeAllProductsCommand
            contextMenu.Items.Add(new MenuItem()
            {
                Header = vm.RangeAllProductsCommand.FriendlyName,
                Command = vm.RangeAllProductsCommand
            });

            //UnrangeAllProductsCommand
            contextMenu.Items.Add(new MenuItem()
            {
                Header = vm.UnrangeAllProductsCommand.FriendlyName,
                Command = vm.UnrangeAllProductsCommand
            });


            contextMenu.IsOpen = true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Copies the current grid selection to the viewmodel.
        /// </summary>
        private void SyncGridSelectionToViewModel()
        {
            if (this.ViewModel == null || this.xAssignedProducts == null) return;

            //copy the selected items to the viewmodel
            List<IPlanItem> gridSelection = this.xAssignedProducts.SelectedItems.Cast<IPlanItem>().ToList();

            //remove old
            this.ViewModel.SelectedProducts.RemoveRange(this.ViewModel.SelectedProducts.Except(gridSelection).ToList());

            //add new
            this.ViewModel.SelectedProducts.AddRange(gridSelection.Except(this.ViewModel.SelectedProducts).ToList());

        }

        /// <summary>
        /// Copies the current viewmodel selection to the grid.
        /// </summary>
        private void SyncViewModelSelectionToGrid()
        {
            if (this.ViewModel == null || this.xAssignedProducts == null) return;

            List<IPlanItem> vmSelection = this.ViewModel.SelectedProducts.ToList();
            List<IPlanItem> gridSelection = this.xAssignedProducts.SelectedItems.Cast<IPlanItem>().ToList();

            //remove old.
            gridSelection.Except(vmSelection).ToList().ForEach(i => this.xAssignedProducts.SelectedItems.Remove(i));

            //add new
            vmSelection.Except(gridSelection).ToList().ForEach(i => this.xAssignedProducts.SelectedItems.Add(i));
        }

        #endregion

        #region IPlanDocumentView Members

        public Boolean IsDisposed { get; private set; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean isDisposing)
        {
            if (IsDisposed) return;

            if (isDisposing)
            {
                if (_columnLayoutManager != null)
                {
                    _columnLayoutManager.Dispose();
                    _columnLayoutManager = null;
                }
                this.ViewModel = null;
            }

            IsDisposed = true;

        }

        ~AssortmentPlanDocumentView()
        {
            try
            {
                Dispose(false);
            }
            catch { }
        }

        public IPlanDocument GetIPlanDocument()
        {
            return ViewModel;
        }

        #endregion

        

    }
}
