﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created
// V8-31224 : A.Probyn
//  Fixed missing clear button, and close button being disabled
// V8-32718 : A.Probyn ~ Updated so full product attributes can be displayed.
// V8-32718 : D.Pleasance
//  Amended to enable displaying of product / position attributes.
// V8-32916 : A.Probyn ~ Updated so remove rule column cannot be hidden or frozen
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using System.Collections.ObjectModel;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Interaction logic for AssortmentAssortmentInventoryRuleWindow.xaml
    /// </summary>
    public sealed partial class AssortmentInventoryRuleWindow : ExtendedRibbonWindow
    {
        #region Fields

        private ColumnLayoutManager _columnLayoutManager;

        #endregion

        const String _removeRuleCommandKey = "InventoryRulesRemoveRuleCommand";

        #region Properties

        #region ViewModel property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentInventoryRuleViewModel), typeof(AssortmentInventoryRuleWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public AssortmentInventoryRuleViewModel ViewModel
        {
            get { return (AssortmentInventoryRuleViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentInventoryRuleWindow senderControl = (AssortmentInventoryRuleWindow)obj;

            if (e.OldValue != null)
            {
                AssortmentInventoryRuleViewModel oldModel = (AssortmentInventoryRuleViewModel)e.OldValue;
                oldModel.AttachedControl = null;

                senderControl.Resources.Remove(_removeRuleCommandKey);
            }

            if (e.NewValue != null)
            {
                AssortmentInventoryRuleViewModel newModel = (AssortmentInventoryRuleViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

                senderControl.Resources.Add(_removeRuleCommandKey, newModel.RemoveRuleCommand);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public AssortmentInventoryRuleWindow(PlanogramAssortment selectedAssortment, IEnumerable<IPlanItem> planItems)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add link to SA.chm
            //Help.SetFilename((DependencyObject)this, App.ViewState.HelpFilePath);
            //Help.SetKeyword((DependencyObject)this, "**");

            this.ViewModel = new AssortmentInventoryRuleViewModel(selectedAssortment, planItems);

            this.Loaded += AssortmentInventoryRuleWindow_Loaded;
        }

        /// <summary>
        /// Carries out initial first load actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssortmentInventoryRuleWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= AssortmentInventoryRuleWindow_Loaded;

            var factory =
            new PlanogramAssortmentProductColumnLayoutFactory(typeof(AssortmentProductRuleRow),
                    this.ViewModel.CurrentAssortment.Parent,
                    new List<Framework.Model.IModelPropertyInfo>() 
                    { 
                        PlanogramProduct.GtinProperty,
                        PlanogramProduct.NameProperty
                    }, true);

            // Initialize the custom column manager.
            _columnLayoutManager = new ColumnLayoutManager(
                factory,
                DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.EntityId),
                CCMClient.ColumnScreenKeyAssortmentProducts, /*PathMask*/"PlanItem.{0}");

            _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            _columnLayoutManager.AttachDataGrid(xProductDataGrid);

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers


        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //Prefix on addition columns:
            Int32 curIdx = 0;

            //Add default product columns
            foreach (DataGridColumn column in BuildAssortmentProductColumnList())
            {
                columnSet.Insert(curIdx, column);
                curIdx++;
            }

            //add the delete row col
            DataGridExtendedTemplateColumn deleteRowCol = new DataGridExtendedTemplateColumn()
            {
                Width = 40,
                CanUserReorder = false,
                CanUserResize = false,
                CanUserHide = false,
                CanUserFreeze = false,
                CellTemplate = (DataTemplate)this.Resources["InventoryRulesWindow_DTRemoveProductRuleColumn"]
            };
            ExtendedDataGrid.SetColumnCellAlignment(deleteRowCol, System.Windows.HorizontalAlignment.Center);
            deleteRowCol.SetValue(ExtendedDataGrid.CanUserFilterProperty, false);
            columnSet.Insert(curIdx, deleteRowCol);
        }

        #endregion

        #region Methods

        private ObservableCollection<DataGridColumn> BuildAssortmentProductColumnList()
        {
            //load the columnset
            ObservableCollection<DataGridColumn> columnList = new ObservableCollection<DataGridColumn>();

            DataGridTextColumn col0 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    AssortmentProduct.GtinProperty.FriendlyName,
                    AssortmentInventoryRuleRow.ProductGTINProperty.Path,
                    System.Windows.HorizontalAlignment.Left);
            col0.MinWidth = 50;
            col0.Width = 80;
            columnList.Add(col0);

            DataGridTextColumn col1 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    AssortmentProduct.NameProperty.FriendlyName,
                    AssortmentInventoryRuleRow.ProductNameProperty.Path,
                    System.Windows.HorizontalAlignment.Left);
            col1.Width = 250;
            col1.MinWidth = 50;
            columnList.Add(col1);

            DataGridTextColumn col2 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    Message.AssortmentInventoryRules_ColumnRule,
                    AssortmentInventoryRuleRow.RuleSummaryProperty.Path,
                    System.Windows.HorizontalAlignment.Left);
            col2.MinWidth = 50;
            col2.Width = 185;
            columnList.Add(col2);

            return columnList;
        }

        #endregion

        #region Window Close


        protected override void OnCrossCloseRequested(CancelEventArgs e)
        {
            if (!this.ViewModel.ContinueWithItemChange())
            {
                e.Cancel = true;
            }

            base.OnCrossCloseRequested(e);
        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {
                   if (_columnLayoutManager != null)
                   {
                       _columnLayoutManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                       _columnLayoutManager.Dispose();
                   }

                   IDisposable disposableViewModel = this.ViewModel;
                   this.ViewModel = null;

                   if (disposableViewModel != null)
                   {
                       disposableViewModel.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion
    }
}
