﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//  Adapted from Workflow client.
#endregion

#region Version History: (CCM 8.1.0)
// V8-30196 : M.Pettit
//  Added Help link
#endregion

#region Version History: (CCM 8.3.0)
// V8-32718 : A.Probyn ~ Updated so full product attributes can be displayed.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Interaction logic for AssortmentLocalProductWindow.xaml
    /// </summary>
    public partial class AssortmentLocalProductWindow : ExtendedRibbonWindow
    {
        #region Fields

        private ColumnLayoutManager _columnLayoutManager;

        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentLocalProductViewModel),
            typeof(AssortmentLocalProductWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets/Sets the window viewmodel context
        /// </summary>
        public AssortmentLocalProductViewModel ViewModel
        {
            get { return (AssortmentLocalProductViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentLocalProductWindow senderControl = (AssortmentLocalProductWindow)obj;

            if (e.OldValue != null)
            {
                AssortmentLocalProductViewModel oldModel = (AssortmentLocalProductViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                AssortmentLocalProductViewModel newModel = (AssortmentLocalProductViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public AssortmentLocalProductWindow(AssortmentLocalProductViewModel viewModel)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //set the help file key for this window.
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.AssortmentLocalProductSetup);

            this.ViewModel = viewModel;

            this.Loaded += new RoutedEventHandler(AssortmentLocalProductWindow_Loaded);
        }

        /// <summary>
        /// Event handler for the on loaded event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssortmentLocalProductWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= new RoutedEventHandler(AssortmentLocalProductWindow_Loaded);

            var factory =
            new AssortmentLocationCustomColumnLayoutFactory(typeof(Location),
                    new List<Framework.Model.IModelPropertyInfo>() 
                    { 
                        Location.CodeProperty,
                        Location.NameProperty
                    });

            // Initialize the custom column manager.
            _columnLayoutManager = new ColumnLayoutManager(
                factory,
                DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.EntityId),
                CCMClient.ColumnScreenKeyAssortmentLocations);

            _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            _columnLayoutManager.AttachDataGrid(this.assignedGrid);
            _columnLayoutManager.AttachDataGrid(this.unassignedGrid);

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event handlers
        
        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //Prefix on addition columns:
            Int32 curIdx = 0;

            //Add default product columns
            foreach (DataGridColumn column in BuildAssortmentLocationColumnList())
            {
                columnSet.Insert(curIdx, column);
                curIdx++;
            }
        }     

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
        }

        private void AssignedGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid == this.unassignedGrid)
            {
                if (this.ViewModel != null)
                {
                    this.ViewModel.AddSelectedLocationsCommand.Execute();
                }
            }
        }

        private void UnassignedGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid == this.assignedGrid)
            {
                if (this.ViewModel != null)
                {
                    this.ViewModel.RemoveSelectedLocationsCommand.Execute();
                }
            }
        }

        private void UnassignedGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.AddSelectedLocationsCommand.Execute();
            }
        }

        private void AssignedGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.RemoveSelectedLocationsCommand.Execute();
            }
        }

        #endregion


        #region Methods

        private List<DataGridColumn> BuildAssortmentLocationColumnList()
        {
            //load the columnset
            List<DataGridColumn> columnList = new List<DataGridColumn>();

            DataGridTextColumn col0 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    Location.CodeProperty.FriendlyName,
                    Location.CodeProperty.Name,
                    System.Windows.HorizontalAlignment.Left);
            col0.MinWidth = 50;
            col0.Width = 50;
            columnList.Add(col0);

            DataGridTextColumn col1 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    Location.NameProperty.FriendlyName,
                    Location.NameProperty.Name,
                    System.Windows.HorizontalAlignment.Left);
            col1.Width = 250;
            col1.MinWidth = 50;
            columnList.Add(col1);

            return columnList;
        }

        #endregion

        #region OnClose

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (_columnLayoutManager != null)
                {
                    _columnLayoutManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                    _columnLayoutManager.Dispose();
                }

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
    }
}
