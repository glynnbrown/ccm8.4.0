﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//      Created (adapted from Workflow client).
#endregion

#region Version History: (CCM 8.1.0)
// V8-30196 : M.Pettit
//  Added Help link
#endregion
#region Version History: (CCM 8.3.0)
// V8-32718 : A.Probyn ~ Updated so full product attributes can be displayed.
// V8-32718 : D.Pleasance
//  Amended to enable displaying of product / position attributes.
// V8-32916 : A.Probyn ~ Updated so remove rule column cannot be hidden or frozen
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Planograms.Model;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Common;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Interaction logic for AssortmentProductRuleWindow.xaml
    /// </summary>
    public partial class AssortmentProductRuleWindow : ExtendedRibbonWindow
    {
        #region Fields

        private ColumnLayoutManager _columnLayoutManager;

        #endregion

        #region Constants

        const String _removeRuleCommandKey = "ProductsRulesRemoveRuleCommand";

        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentProductRuleViewModel),
            typeof(AssortmentProductRuleWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets/Sets the window viewmodel context
        /// </summary>
        public AssortmentProductRuleViewModel ViewModel
        {
            get { return (AssortmentProductRuleViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentProductRuleWindow senderControl =
                (AssortmentProductRuleWindow)obj;

            if (e.OldValue != null)
            {
                AssortmentProductRuleViewModel oldModel =
                    (AssortmentProductRuleViewModel)e.OldValue;
                oldModel.AttachedControl = null;

                senderControl.Resources.Remove(_removeRuleCommandKey);
            }

            if (e.NewValue != null)
            {
                AssortmentProductRuleViewModel newModel =
                    (AssortmentProductRuleViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

                senderControl.Resources.Add(_removeRuleCommandKey, newModel.RemoveRuleCommand);
            }
        }

        #endregion

        #endregion

        #region Constructors
        public AssortmentProductRuleWindow(PlanogramAssortment selectedAssortment, IEnumerable<IPlanItem> planItems)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            InitializeComponent();

            //set the help file key for this window.
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.ProductRules);

            this.ViewModel = new AssortmentProductRuleViewModel(selectedAssortment, planItems);
            this.Loaded += new RoutedEventHandler(AssortmentSetupProductRuleWindow_Loaded);
        }
        
        /// <summary>
        /// Event handler for the on loaded event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssortmentSetupProductRuleWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= new RoutedEventHandler(AssortmentSetupProductRuleWindow_Loaded);

            var factory =
            new PlanogramAssortmentProductColumnLayoutFactory(typeof(AssortmentProductRuleRow),
                    this.ViewModel.SelectedAssortment.Parent,
                    new List<Framework.Model.IModelPropertyInfo>() 
                    { 
                        PlanogramProduct.GtinProperty,
                        PlanogramProduct.NameProperty
                    }, true);

            // Initialize the custom column manager.
            _columnLayoutManager = new ColumnLayoutManager(
                factory,
                DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.EntityId),
                CCMClient.ColumnScreenKeyAssortmentProducts, /*PathMask*/"PlanItem.{0}");

            _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            _columnLayoutManager.AttachDataGrid(xProductDataGrid);

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }
        #endregion

        #region Event handlers


        #region Event handlers

        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //Prefix on addition columns:
            Int32 curIdx = 0;

            //Add default product columns
            foreach (DataGridColumn column in BuildAssortmentProductColumnList())
            {
                columnSet.Insert(curIdx, column);
                curIdx++;
            }

            //add the delete row col
            columnSet.Insert(curIdx, new DataGridExtendedTemplateColumn
            {
                Header = String.Empty,
                CellTemplate = Resources["ProductRulesWindow_DTRemoveProductRuleColumn"] as DataTemplate,
                CanUserFilter = false,
                CanUserSort = false,
                CanUserReorder = false,
                CanUserResize = false,
                CanUserHide = false,
                CanUserFreeze = false,
                ColumnGroupName = "",
                Width = 40,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            });
        }

        #endregion

        /// <summary>
        /// Method to force the EditValue and EditMaximum value textboxes to refresh if
        /// and invalid value was entered.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExtendedTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.RefreshSelectedRows();
            }
        }

        #endregion

        #region Methods

        private ObservableCollection<DataGridColumn> BuildAssortmentProductColumnList()
        {
            //load the columnset
            ObservableCollection<DataGridColumn> columnList = new ObservableCollection<DataGridColumn>();

            DataGridTextColumn col0 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    PlanogramAssortmentProduct.GtinProperty.FriendlyName,
                    AssortmentProductRuleRow.ProductGtinProperty.Path,
                    System.Windows.HorizontalAlignment.Left);
            col0.MinWidth = 50;
            col0.Width = 80;
            columnList.Add(col0);

            DataGridTextColumn col1 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    PlanogramAssortmentProduct.NameProperty.FriendlyName,
                    AssortmentProductRuleRow.ProductNameProperty.Path,
                    System.Windows.HorizontalAlignment.Left);
            col1.Width = 250;
            col1.MinWidth = 50;
            columnList.Add(col1);

            DataGridTextColumn col2 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    Message.AssortmentDocument_CommonRule,
                    AssortmentProductRuleRow.RuleSummaryProperty.Path,
                    System.Windows.HorizontalAlignment.Left);
            col2.MinWidth = 50;
            col2.Width = 185;
            columnList.Add(col2);

            return columnList;
        }

        #endregion

        #region Window Close

        protected override void OnCrossCloseRequested(CancelEventArgs e)
        {
            if (!this.ViewModel.ContinueWithItemChange())
            {
                e.Cancel = true;
            }

            base.OnCrossCloseRequested(e);
        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {
                   if (_columnLayoutManager != null)
                   {
                       _columnLayoutManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                       _columnLayoutManager.Dispose();
                   }

                   IDisposable disposableViewModel = this.ViewModel;
                   this.ViewModel = null;

                   if (disposableViewModel != null)
                   {
                       disposableViewModel.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion
    }
}
