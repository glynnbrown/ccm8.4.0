﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014
#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//  Adapted from Workflow client.
// V8-27259 : A.Kuszyk
//  Amended command binding of remove button.
#endregion

#region Version History: (CCM 8.1.0)
// V8-30196 : M.Pettit
//  Added Help link
#endregion
#region Version History: (CCM 8.3.0)
// V8-32718 : A.Probyn ~ Updated so full product attributes can be displayed.
// V8-32718 : D.Pleasance
//  Amended to enable displaying of product / position attributes.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf;
using Galleria.Framework.Planograms.Model;
using System.Collections.ObjectModel;
using Galleria.Ccm.Editor.Client.Wpf.Common;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Interaction logic for AssortmentFamilyRulesWindow.xaml
    /// </summary>
    public partial class AssortmentFamilyRulesWindow : ExtendedRibbonWindow
    {
        #region Constants

        public static readonly String RemoveCommandKey = "RemoveCommand";

        #endregion

        #region Fields

        private ColumnLayoutManager _columnLayoutManager;

        #endregion

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel",
            typeof(AssortmentFamilyRulesViewModel),
            typeof(AssortmentFamilyRulesWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public AssortmentFamilyRulesViewModel ViewModel
        {
            get { return (AssortmentFamilyRulesViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentFamilyRulesWindow senderControl = (AssortmentFamilyRulesWindow)obj;

            if (e.OldValue != null)
            {
                AssortmentFamilyRulesViewModel oldModel = (AssortmentFamilyRulesViewModel)e.OldValue;
                oldModel.AttachedControl = null;

                senderControl.Resources.Remove(RemoveCommandKey);
            }

            if (e.NewValue != null)
            {
                AssortmentFamilyRulesViewModel newModel = (AssortmentFamilyRulesViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

                senderControl.Resources.Add(RemoveCommandKey, newModel.RemoveCommand);
            }
        }

        #endregion

        #endregion

        #region Constructor
        public AssortmentFamilyRulesWindow(AssortmentFamilyRulesViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            InitializeComponent();

            //set the help file key for this window.
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.FamilyRules);

            this.ViewModel = viewModel;
            this.Loaded += new RoutedEventHandler(AssortmentSetupFamilyRules_Loaded);
        }

        /// <summary>
        /// Event handler for the on loaded event
        /// </summary>
        private void AssortmentSetupFamilyRules_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= new RoutedEventHandler(AssortmentSetupFamilyRules_Loaded);

            var factory =
            new PlanogramAssortmentProductColumnLayoutFactory(typeof(AssortmentProductRuleRow),
                    this.ViewModel.SelectedAssortment.Parent,
                    new List<Framework.Model.IModelPropertyInfo>() 
                    { 
                        PlanogramProduct.GtinProperty,
                        PlanogramProduct.NameProperty
                    }, true);

            // Initialize the custom column manager.
            _columnLayoutManager = new ColumnLayoutManager(
                factory,
                DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.EntityId),
                CCMClient.ColumnScreenKeyAssortmentProducts, /*PathMask*/"PlanItem.{0}");

            _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            _columnLayoutManager.AttachDataGrid(assignedGrid);
            _columnLayoutManager.AttachDataGrid(unassignedGrid);

            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }
        #endregion

        #region Event Handlers
        
        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //Prefix on addition columns:
            Int32 curIdx = 0;

            //Add default product columns
            foreach (DataGridColumn column in BuildAssortmentProductColumnList())
            {
                columnSet.Insert(curIdx, column);
                curIdx++;
            }
        }        

        private void AssignedGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid == this.unassignedGrid)
            {
                if (this.ViewModel != null && this.ViewModel.AddSelectedProductsCommand.CanExecute())
                {
                    this.ViewModel.AddSelectedProductsCommand.Execute();
                }
            }
        }

        private void UnassignedGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid == this.assignedGrid)
            {
                if (this.ViewModel != null)
                {
                    this.ViewModel.RemoveSelectedProductsCommand.Execute();
                }
            }
        }

        private void UnassignedGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.AddSelectedProductsCommand.Execute();
            }
        }

        private void AssignedGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.RemoveSelectedProductsCommand.Execute();
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            this.Close();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (_columnLayoutManager != null)
                {
                    _columnLayoutManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                    _columnLayoutManager.Dispose();
                }

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion

        #region Methods

        private ObservableCollection<DataGridColumn> BuildAssortmentProductColumnList()
        {
            //load the columnset
            ObservableCollection<DataGridColumn> columnList = new ObservableCollection<DataGridColumn>();

            DataGridTextColumn col0 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    PlanogramProduct.GtinProperty.FriendlyName,
                    PlanogramAssortmentProductView.ProductGtinProperty.Path,
                    System.Windows.HorizontalAlignment.Left);
            col0.MinWidth = 50;
            col0.Width = 80;
            columnList.Add(col0);

            DataGridTextColumn col1 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    PlanogramProduct.NameProperty.FriendlyName,
                    PlanogramAssortmentProductView.ProductNameProperty.Path,
                    System.Windows.HorizontalAlignment.Left);
            col1.Width = 250;
            col1.MinWidth = 50;
            columnList.Add(col1);

            return columnList;
        }

        #endregion
    }
}
