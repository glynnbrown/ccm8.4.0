﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created
// V8-32718 : A.Probyn ~ Updated so full product attributes can be displayed.
// V8-32718 : D.Pleasance
//  Added planItems to constructor. These are passed through to enable displaying of product / position attributes.
#endregion

#endregion

using System;
using System.Linq;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using System.Globalization;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Editor.Client.Wpf.Common;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Represents a row on the PlanogramAssortment inventory rules screen
    /// </summary>
    public sealed class AssortmentInventoryRuleRow : ViewModelObject
    {
        #region Fields
        private PlanogramAssortment _assortment;
        private PlanogramAssortmentProduct _product;
        private PlanogramProduct _planogramProduct;
        private IPlanItem _planItem;
        private PlanogramAssortmentInventoryRule _existingRule;
        private PlanogramAssortmentInventoryRule _rule;
        private Boolean _isDirty;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ProductProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleRow>(p => p.Product);
        public static readonly PropertyPath PlanogramProductProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleRow>(p => p.PlanogramProduct);
        public static readonly PropertyPath ProductGTINProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleRow>(p => p.ProductGTIN);
        public static readonly PropertyPath ProductNameProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleRow>(p => p.ProductName);
        public static readonly PropertyPath RuleProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleRow>(p => p.Rule);
        public static readonly PropertyPath RuleSummaryProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleRow>(p => p.RuleSummary);
        public static readonly PropertyPath IsDirtyProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleRow>(p => p.IsDirty);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the PlanogramAssortment product this row relates to
        /// </summary>
        public PlanogramAssortmentProduct Product
        {
            get { return _product; }
        }

        /// <summary>
        /// Gets the editing rule for this row.
        /// </summary>
        public PlanogramAssortmentInventoryRule Rule
        {
            get { return _rule; }
        }

        public PlanogramProduct PlanogramProduct
        {
            get
            {
                if (_planogramProduct == null)
                {
                    _planogramProduct = _product.GetPlanogramProduct();
                }
                return _planogramProduct;
            }
        }

        public IPlanItem PlanItem
        {
            get
            {
                return _planItem;
            }
        }

        /// <summary>
        /// Gets the Gtin of the product this row relates to
        /// </summary>
        public String ProductGTIN
        {
            get { return _product.Gtin; }
        }

        /// <summary>
        /// Gets the Name of the product this row relates to
        /// </summary>
        public String ProductName
        {
            get { return _product.Name; }
        }

        /// <summary>
        /// Gets a summary of the current rule
        /// </summary>
        public String RuleSummary
        {
            get
            {
                if (this.IsNullRule)
                {
                    return String.Empty;
                }
                else
                {
                    String value = String.Empty;

                    if (_rule.CasePack != 0)
                    {
                        value = String.Format(CultureInfo.CurrentCulture,
                            "{0} {1}={2}", value, Message.AssortmentInventoryRules_RuleSummary_CasePacks, _rule.CasePack);
                    }

                    if (_rule.DaysOfSupply != 0)
                    {
                        value = String.Format(CultureInfo.CurrentCulture,
                            "{0}, {1}={2}", value, Message.AssortmentInventoryRules_RuleSummary_Dos, _rule.DaysOfSupply);
                    }

                    if (_rule.ShelfLife != 0)
                    {
                        value = String.Format(CultureInfo.CurrentCulture,
                            "{0}, {1}={2}", value, Message.AssortmentInventoryRules_RuleSummary_ShelfLife, _rule.ShelfLife.ToString("P0", CultureInfo.CurrentCulture));
                    }

                    if (_rule.ReplenishmentDays != 0)
                    {
                        value = String.Format(CultureInfo.CurrentCulture,
                            "{0}, {1}={2}", value, Message.AssortmentInventoryRules_RuleSummary_Delivery, _rule.ReplenishmentDays);
                    }

                    if (_rule.WasteHurdleUnits != 0)
                    {
                        value = String.Format(CultureInfo.CurrentCulture,
                            "{0}, {1}={2}", value, Message.AssortmentInventoryRules_RuleSummary_WasteHurdleUnits, _rule.WasteHurdleUnits);
                    }
                    if (_rule.WasteHurdleCasePack != 0)
                    {
                        value = String.Format(CultureInfo.CurrentCulture,
                            "{0}, {1}={2}", value, Message.AssortmentInventoryRules_RuleSummary_WasteHurdleCasePacks, _rule.WasteHurdleCasePack);
                    }

                    if (_rule.MinUnits != 0)
                    {
                        value = String.Format(CultureInfo.CurrentCulture,
                           "{0}, {1}={2}", value, Message.AssortmentInventoryRules_RuleSummary_MinUnits, _rule.MinUnits);
                    }
                    if (_rule.MinFacings != 0)
                    {
                        value = String.Format(CultureInfo.CurrentCulture,
                           "{0}, {1}={2}", value, Message.AssortmentInventoryRules_RuleSummary_MinFacings, _rule.MinFacings);
                    }


                    return value.TrimStart(", ".ToCharArray());
                }
            }
        }

        /// <summary>
        /// Returns true if the rule has no values.
        /// </summary>
        public Boolean IsNullRule
        {
            get
            {
                if (_rule == null) { return true; }
                if (_rule.CasePack != 0) { return false; }
                if (_rule.DaysOfSupply != 0) { return false; }
                if (_rule.ShelfLife != 0) { return false; }
                if (_rule.ReplenishmentDays != 0) { return false; }
                if (_rule.WasteHurdleUnits != 0) { return false; }
                if (_rule.WasteHurdleCasePack != 0) { return false; }
                if (_rule.MinUnits != 0) { return false; }
                if (_rule.MinFacings != 0) { return false; }

                return true;
            }
        }

        /// <summary>
        /// Returns true if the row is dirty
        /// </summary>
        public Boolean IsDirty
        {
            get { return _isDirty; }
            private set
            {
                _isDirty = value;
                OnPropertyChanged(IsDirtyProperty);
            }
        }

        #endregion

        #region Constructor

        public AssortmentInventoryRuleRow(PlanogramAssortment assortment, PlanogramAssortmentProduct product, IPlanItem planItem)
        {
            _product = product;
            _assortment = assortment;
            _planItem = planItem;

            //check if a rule exists on the range
            _existingRule = _assortment.InventoryRules.FirstOrDefault(p => p.ProductGtin.Equals(product.Gtin));

            //set the rule to actually edit
            if (_existingRule != null)
            {
                _rule = _existingRule.Clone();
            }
            else
            {
                //create a new null rule
                _rule = PlanogramAssortmentInventoryRule.NewPlanogramAssortmentInventoryRule(product);
                ClearRule();
                //nb - dont mark dirty as this is just a blank rule
            }
            _rule.PropertyChanged += Rule_PropertyChanged;
        }


        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to changes to the current rule
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Rule_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            OnPropertyChanged(RuleSummaryProperty);

            this.IsDirty = true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Commits any changes made back to the given node
        /// </summary>
        /// <param name="applyToRange"></param>
        public void CommitChanges()
        {
            if (this.IsDirty)
            {
                PlanogramAssortment applyToPlanogramAssortment = _assortment;
                PlanogramAssortmentInventoryRule existingRule = _existingRule;
                PlanogramAssortmentInventoryRule editRule = _rule;

                //if this had an existing rule but is now null - remove it
                if (this.IsNullRule && existingRule != null)
                {
                    applyToPlanogramAssortment.InventoryRules.Remove(existingRule);
                }
                else if (!this.IsNullRule && existingRule == null)
                {
                    //just add the rule
                    applyToPlanogramAssortment.InventoryRules.Add(editRule);
                }
                else if (!this.IsNullRule && existingRule != null)
                {
                    //copy values
                    _existingRule.CopyValues(editRule);
                }

                this.IsDirty = false;
            }
        }

        /// <summary>
        /// Clears off all of the rule values
        /// </summary>
        public void ClearRule()
        {
            _rule.CasePack = 0;
            _rule.DaysOfSupply = 0;
            _rule.ShelfLife = 0;
            _rule.ReplenishmentDays = 0;
            _rule.WasteHurdleUnits = 0;
            _rule.WasteHurdleCasePack = 0;
            _rule.WasteHurdleUnits = 0;
            _rule.MinUnits = 0;
            _rule.MinFacings = 0;
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                _rule.PropertyChanged -= Rule_PropertyChanged;

                if (disposing)
                {
                    _assortment = null;
                    _existingRule = null;
                    _rule = null;
                    _product = null;
                }
            }

        }

        #endregion
    }
}

