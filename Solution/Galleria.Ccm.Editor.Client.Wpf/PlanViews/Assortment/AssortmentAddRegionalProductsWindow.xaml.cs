﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//  Adapted from Workflow client.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using System.ComponentModel;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Interaction logic for AssortmentAddRegionalProductsWindow.xaml
    /// </summary>
    public partial class AssortmentAddRegionalProductsWindow : ExtendedRibbonWindow
    {
        #region Constants

        const String _removeRuleCommandKey = "RegionProductRemoveRegionalProductCommand";
        const String _selectRegionProductCommandKey = "RegionProductSelectRegionProductCommand";

        #endregion

        #region Properties

        #region ViewModel property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentAddRegionalProductsViewModel), typeof(AssortmentAddRegionalProductsWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public AssortmentAddRegionalProductsViewModel ViewModel
        {
            get { return (AssortmentAddRegionalProductsViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentAddRegionalProductsWindow senderControl = (AssortmentAddRegionalProductsWindow)obj;

            if (e.OldValue != null)
            {
                AssortmentAddRegionalProductsViewModel oldModel = (AssortmentAddRegionalProductsViewModel)e.OldValue;
                oldModel.AttachedControl = null;

                senderControl.Resources.Remove(_selectRegionProductCommandKey);
                senderControl.Resources.Remove(_removeRuleCommandKey);
            }

            if (e.NewValue != null)
            {
                AssortmentAddRegionalProductsViewModel newModel = (AssortmentAddRegionalProductsViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

                senderControl.Resources.Add(_selectRegionProductCommandKey, newModel.SelectRegionalProductCommand);
                senderControl.Resources.Add(_removeRuleCommandKey, newModel.RemoveRegionalProductCommand);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public AssortmentAddRegionalProductsWindow(AssortmentAddRegionalProductsViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.ViewModel = viewModel;
            this.Loaded += RegionProductWindow_Loaded;
        }

        /// <summary>
        /// Carries out initial first load actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RegionProductWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= RegionProductWindow_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Window Close

        protected override void OnCrossCloseRequested(CancelEventArgs e)
        {
            if (!this.ViewModel.ContinueWithItemChange())
            {
                e.Cancel = true;
            }

            base.OnCrossCloseRequested(e);
        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {
                   IDisposable disposableViewModel = this.ViewModel;
                   this.ViewModel = null;

                   if (disposableViewModel != null)
                   {
                       disposableViewModel.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion
    }
}
