﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//  Adapted from Workflow client.
#endregion
#region Version History: (CCM 8.3.0)
// V8-32718 : A.Probyn ~ Updated so full product attributes can be displayed.
// V8-32718 : D.Pleasance
//  Amended to enable displaying of product / position attributes.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf;
using System.Collections.ObjectModel;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Interaction logic for AssortmentRegionalProductSelectionWindow.xaml
    /// </summary>
    public partial class AssortmentRegionalProductSelectionWindow : ExtendedRibbonWindow
    {
        #region Fields

        private PlanogramAssortmentProductView _selectionResult;
        private IEnumerable<PlanogramAssortmentProductView> _availableAssortmentProducts;
        private ColumnLayoutManager _columnLayoutManager;

        #endregion

        #region Properties

        public PlanogramAssortmentProductView SelectionResult
        {
            get { return _selectionResult; }
            private set
            {
                _selectionResult = value;
            }
        }

        #endregion

        #region Constructor

        public AssortmentRegionalProductSelectionWindow(IEnumerable<PlanogramAssortmentProductView> availableAssortmentProducts)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.Loaded += AssortmentRegionalProductSelectionWindow_Loaded;

            this._availableAssortmentProducts = availableAssortmentProducts;
        }

        private void AssortmentRegionalProductSelectionWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= AssortmentRegionalProductSelectionWindow_Loaded;

            var factory =
            new PlanogramAssortmentProductColumnLayoutFactory(typeof(AssortmentProductRuleRow),
                    null,
                    new List<Framework.Model.IModelPropertyInfo>() 
                    { 
                        PlanogramProduct.GtinProperty,
                        PlanogramProduct.NameProperty
                    }, true);

            // Initialize the custom column manager.
            _columnLayoutManager = new ColumnLayoutManager(
                factory,
                DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.EntityId),
                CCMClient.ColumnScreenKeyAssortmentProducts, /*PathMask*/"PlanItem.{0}");

            _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            _columnLayoutManager.AttachDataGrid(this.assortmentProductGrid);

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers
        
        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //Prefix on addition columns:
            Int32 curIdx = 0;

            //Add default product columns
            foreach (DataGridColumn column in BuildAssortmentProductColumnList())
            {
                columnSet.Insert(curIdx, column);
                curIdx++;
            }
        }     

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            //set data
            assortmentProductGrid.ItemsSourceExtended = _availableAssortmentProducts.ToList();
        }

        private void assortmentProductGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            this.DialogResult = true;
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            //set the result (will close the window)
            this.DialogResult = true;
        }

        /// <summary>
        /// When enter is pressed return selected items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void assortmentProductGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    if (assortmentProductGrid.SelectedItems.Count > 0)
                    {
                        e.Handled = true;
                        this.DialogResult = true;
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region Methods

        private ObservableCollection<DataGridColumn> BuildAssortmentProductColumnList()
        {
            //load the columnset
            ObservableCollection<DataGridColumn> columnList = new ObservableCollection<DataGridColumn>();

            DataGridTextColumn col0 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    PlanogramProduct.GtinProperty.FriendlyName,
                    PlanogramAssortmentProductView.ProductGtinProperty.Path,
                    System.Windows.HorizontalAlignment.Left);
            col0.MinWidth = 50;
            col0.Width = 80;
            columnList.Add(col0);

            DataGridTextColumn col1 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    PlanogramProduct.NameProperty.FriendlyName,
                    PlanogramAssortmentProductView.ProductNameProperty.Path,
                    System.Windows.HorizontalAlignment.Left);
            col1.Width = 250;
            col1.MinWidth = 50;
            columnList.Add(col1);

            return columnList;
        }

        #endregion
        #region window close

        /// <summary>
        /// Cancel view model changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            //set the result (will close the window)
            this.DialogResult = false;
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (this.DialogResult == true)
            {
                this.SelectionResult = (PlanogramAssortmentProductView)assortmentProductGrid.SelectedItem;
            }
        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {
                   if (_columnLayoutManager != null)
                   {
                       _columnLayoutManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                       _columnLayoutManager.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion
    }
}
