﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//  Adapted from Workflow client.
#endregion

#region Version History: (CCM 8.1.0)
// V8-30196 : M.Pettit
//  Added Help link
#endregion

#region Version History: (CCM 8.3.0)
// CCM-18480 : D.Pleasance
//  Updated so full location attributes can be displayed.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using System.ComponentModel;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Interaction logic for AssortmentAddRegionsWindow.xaml
    /// </summary>
    public partial class AssortmentAddRegionsWindow : ExtendedRibbonWindow
    {
        #region Constants

        const String RemoveSelectedRegionCommandKey = "RegionRemoveSelectedRegionCommand";

        #endregion

        #region Fields

        private ColumnLayoutManager _columnLayoutManager;

        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentAddRegionsViewModel),
            typeof(AssortmentAddRegionsWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets/Sets the window viewmodel context
        /// </summary>
        public AssortmentAddRegionsViewModel ViewModel
        {
            get { return (AssortmentAddRegionsViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentAddRegionsWindow senderControl =
                (AssortmentAddRegionsWindow)obj;

            if (e.OldValue != null)
            {
                AssortmentAddRegionsViewModel oldModel = (AssortmentAddRegionsViewModel)e.OldValue;
                oldModel.AttachedControl = null;

                senderControl.Resources.Remove(RemoveSelectedRegionCommandKey);
            }

            if (e.NewValue != null)
            {
                AssortmentAddRegionsViewModel newModel = (AssortmentAddRegionsViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

                senderControl.Resources.Add(RemoveSelectedRegionCommandKey, newModel.RemoveSelectedRegionCommand);
            }
        }

        #endregion

        #endregion

        #region Constructor

        public AssortmentAddRegionsWindow(AssortmentAddRegionsViewModel viewModel)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //set the help file key for this window.
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.RegionSetup);

            this.ViewModel = viewModel;

            this.Loaded += RegionWindow_Loaded;
        }

        /// <summary>
        /// Event handler for the on loaded event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RegionWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= new RoutedEventHandler(RegionWindow_Loaded);

            var factory =
            new AssortmentLocationCustomColumnLayoutFactory(typeof(Location),
                    new List<Framework.Model.IModelPropertyInfo>()
                    {
                        Location.CodeProperty,
                        Location.NameProperty
                    });

            // Initialize the custom column manager.
            _columnLayoutManager = new ColumnLayoutManager(
                factory,
                DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.EntityId),
                CCMClient.ColumnScreenKeyAssortmentLocations);

            _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            _columnLayoutManager.AttachDataGrid(this.xTakenLocationsGrid);
            _columnLayoutManager.AttachDataGrid(this.xAvailableLocationsGrid);

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //Prefix on addition columns:
            Int32 curIdx = 0;

            //Add default product columns
            foreach (DataGridColumn column in BuildAssortmentLocationColumnList())
            {
                columnSet.Insert(curIdx, column);
                curIdx++;
            }
        }

        #endregion

        #region Methods

        private List<DataGridColumn> BuildAssortmentLocationColumnList()
        {
            //load the columnset
            List<DataGridColumn> columnList = new List<DataGridColumn>();

            DataGridTextColumn col0 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    Location.CodeProperty.FriendlyName,
                    Location.CodeProperty.Name,
                    System.Windows.HorizontalAlignment.Left);
            col0.MinWidth = 50;
            col0.Width = 50;
            columnList.Add(col0);

            DataGridTextColumn col1 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    Location.NameProperty.FriendlyName,
                    Location.NameProperty.Name,
                    System.Windows.HorizontalAlignment.Left);
            col1.Width = 250;
            col1.MinWidth = 50;
            columnList.Add(col1);

            return columnList;
        }

        #endregion

        #region Window Close

        protected override void OnCrossCloseRequested(CancelEventArgs e)
        {
            if (!this.ViewModel.ContinueWithItemChange())
            {
                e.Cancel = true;
            }

            base.OnCrossCloseRequested(e);
        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {
                   IDisposable disposableViewModel = this.ViewModel;
                   this.ViewModel = null;

                   if (disposableViewModel != null)
                   {
                       disposableViewModel.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion
    }
}
