﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//  Adapted from Workflow client.
#endregion

#region Version History: (CCM 801)
// V8-28786 : I.George
//  Updated SmallIcon property to use 16 x 16 icon size
#endregion

#region Version History: (CCM 830)
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32718 : D.Pleasance
//  Added planItems to constructor. These are passed through to enable displaying of product / position attributes.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Collections;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Editor.Client.Wpf.Common;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    public sealed class AssortmentAddRegionalProductsViewModel : ViewModelAttachedControlObject<AssortmentAddRegionalProductsWindow>
    {
        #region Fields

        private IEnumerable<IPlanItem> _planItems;
        private PlanogramAssortment _currentAssortment;

        private PlanogramAssortmentProduct _selectedPrimaryProduct;

        private BulkObservableCollection<AssortmentRegionProductRow> _regionProductRows = new BulkObservableCollection<AssortmentRegionProductRow>();
        private ReadOnlyBulkObservableCollection<AssortmentRegionProductRow> _regionProductRowsRO;

        private AssortmentRegionProductRow _selectedRegionProductRow;

        #endregion

        #region Binding properties

        //properties
        public static readonly PropertyPath SelectedPrimaryProductProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionalProductsViewModel>(p => p.SelectedPrimaryProduct);
        public static readonly PropertyPath RegionProductRowsProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionalProductsViewModel>(p => p.RegionProductRows);
        public static readonly PropertyPath SelectedRegionProductRowProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionalProductsViewModel>(p => p.SelectedRegionProductRow);

        //commands
        public static readonly PropertyPath ApplyCommandProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionalProductsViewModel>(p => p.ApplyCommand);
        public static readonly PropertyPath ApplyAndCloseCommandProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionalProductsViewModel>(p => p.ApplyAndCloseCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionalProductsViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath SelectPrimaryProductCommandProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionalProductsViewModel>(p => p.SelectPrimaryProductCommand);
        public static readonly PropertyPath EditRegionsCommandProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionalProductsViewModel>(p => p.EditRegionsCommand);
        public static readonly PropertyPath SelectRegionalProductCommandProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionalProductsViewModel>(p => p.SelectRegionalProductCommand);
        public static readonly PropertyPath RemoveRegionalProductCommandProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionalProductsViewModel>(p => p.RemoveRegionalProductCommand);

        #endregion

        #region Properties

        /// <summary>
        /// The currently selected AssortmentProduct (primary product)
        /// </summary>
        public PlanogramAssortmentProduct SelectedPrimaryProduct
        {
            get { return _selectedPrimaryProduct; }
            set
            {
                _selectedPrimaryProduct = value;
                OnPropertyChanged(SelectedPrimaryProductProperty);

                OnSelectedPrimaryProductChanged(value);
            }
        }

        /// <summary>
        /// Returns the readonly collection of regional products
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssortmentRegionProductRow> RegionProductRows
        {
            get
            {
                if (_regionProductRowsRO == null)
                {
                    _regionProductRowsRO = new ReadOnlyBulkObservableCollection<AssortmentRegionProductRow>(_regionProductRows);
                }
                return _regionProductRowsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the current selected regional product row
        /// </summary>
        public AssortmentRegionProductRow SelectedRegionProductRow
        {
            get { return _selectedRegionProductRow; }
            set
            {
                _selectedRegionProductRow = value;
                OnPropertyChanged(SelectedRegionProductRowProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        public AssortmentAddRegionalProductsViewModel(PlanogramAssortment assortment, IEnumerable<IPlanItem> planItems)
        {
            _currentAssortment = assortment;
            _planItems = planItems;
        }

        #endregion

        #region Commands

        #region Apply

        private RelayCommand _applyCommand;

        /// <summary>
        /// Applies any changes made in the current item to the parent scenario
        /// </summary>
        public RelayCommand ApplyCommand
        {
            get
            {
                if (_applyCommand == null)
                {
                    _applyCommand = new RelayCommand(
                        p => Apply_Executed(),
                        p => Apply_CanExecute())
                    {
                        FriendlyName = Message.Generic_Apply,
                        FriendlyDescription = Message.Generic_Apply_Tooltip,
                        Icon = ImageResources.Apply_32,
                        SmallIcon = ImageResources.Apply_16
                    };
                    base.ViewModelCommands.Add(_applyCommand);
                }
                return _applyCommand;
            }
        }

        private Boolean Apply_CanExecute()
        {
            //must have primary product selected
            if (this.SelectedPrimaryProduct == null)
            {
                return false;
            }

            //must be dirty
            if (!this.RegionProductRows.Any(p => p.IsDirty))
            {
                return false;
            }

            return true;
        }

        private void Apply_Executed()
        {
            ApplyCurrentItem();
        }

        /// <summary>
        /// Applies changes made to the current item.
        /// </summary>
        private void ApplyCurrentItem()
        {
            base.ShowWaitCursor(true);

            //cycle through all rows applying changes made
            foreach (var row in this.RegionProductRows)
            {
                row.CommitChanges();
            }

            //Assign as primary product if contain the primary product, else reset as normal product
            var primaryProduct = this.SelectedPrimaryProduct;
            Boolean containsRegionalSwap = false;
            if (primaryProduct != null)
            {
                //create a row per region
                foreach (var region in _currentAssortment.Regions)
                {
                    if (region.Products.Any(p => p.PrimaryProductGtin == primaryProduct.Gtin))
                    {
                        containsRegionalSwap = true;
                        break;
                    }
                }

                if (containsRegionalSwap)
                {
                    this.SelectedPrimaryProduct.ProductLocalizationType = PlanogramAssortmentProductLocalizationType.Regional;
                    this.SelectedPrimaryProduct.IsPrimaryRegionalProduct = true;
                }
                else
                {
                    this.SelectedPrimaryProduct.ProductLocalizationType = PlanogramAssortmentProductLocalizationType.None;
                    this.SelectedPrimaryProduct.IsPrimaryRegionalProduct = false;
                }
            }

            //take a snap of the currently selected item
            var selectedRegion = (this.SelectedRegionProductRow != null) ? this.SelectedRegionProductRow.Region : null;

            //recreate all rows
            ReloadRegionRows();

            //reselect
            if (selectedRegion != null)
            {
                foreach (var row in this.RegionProductRows)
                {
                    if (row.Region == selectedRegion)
                    {
                        this.SelectedRegionProductRow = row;
                        break;
                    }
                }
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region ApplyAndClose

        private RelayCommand _applyAndCloseCommand;
        /// <summary>
        /// Executes the save command then the close command
        /// The organiser should handle this command to peform the close action
        /// </summary>
        public RelayCommand ApplyAndCloseCommand
        {
            get
            {
                if (_applyAndCloseCommand == null)
                {
                    _applyAndCloseCommand = new RelayCommand(
                        p => ApplyAndClose_Executed(),
                        p => ApplyAndClose_CanExecute())
                    {
                        FriendlyName = Message.Generic_ApplyAndClose,
                        FriendlyDescription = Message.Generic_ApplyAndClose_Tooltip,
                        SmallIcon = ImageResources.ApplyAndClose_16
                    };
                    base.ViewModelCommands.Add(_applyAndCloseCommand);
                }
                return _applyAndCloseCommand;
            }
        }

        private Boolean ApplyAndClose_CanExecute()
        {
            //must have primary product selected
            if (this.SelectedPrimaryProduct == null)
            {
                return false;
            }

            //must be dirty
            if (!this.RegionProductRows.Any(p => p.IsDirty))
            {
                return false;
            }

            return true;
        }

        private void ApplyAndClose_Executed()
        {
            ApplyCurrentItem();

            //close the attached window
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels changes and closes the window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region EditRegions

        private RelayCommand _editRegionsCommand;

        /// <summary>
        /// Launches the edit regions window
        /// </summary>
        public RelayCommand EditRegionsCommand
        {
            get
            {
                if (_editRegionsCommand == null)
                {
                    _editRegionsCommand = new RelayCommand(
                        p => EditRegions_Executed())
                    {
                        FriendlyName = Message.AssortmentDocument_EditRegions,
                        SmallIcon = ImageResources.AssortmentDocument_RegionalProducts16
                    };
                    base.ViewModelCommands.Add(_editRegionsCommand);
                }
                return _editRegionsCommand;
            }
        }

        private void EditRegions_Executed()
        {
            if (this.AttachedControl != null)
            {
                var win = new AssortmentAddRegionsWindow(new AssortmentAddRegionsViewModel(this._currentAssortment));
                App.ShowWindow(win, this.AttachedControl, /*isModal*/true);

                if (this.SelectedPrimaryProduct != null)
                {
                    //check if a row has been added or removed
                    var existingRows = this.RegionProductRows.ToList();
                    var newRows = new List<AssortmentRegionProductRow>();

                    //iterate through all regions in the scenario
                    foreach (var region in _currentAssortment.Regions)
                    {
                        var row = existingRows.FirstOrDefault(r => r.Region == region);
                        if (row == null)
                        {
                            //add a row for the region - there wont be a rule yet so don't bother looking.
                            newRows.Add(new AssortmentRegionProductRow(region, SelectedPrimaryProduct));
                        }
                        else
                        {
                            existingRows.Remove(row);
                        }
                    }

                    //remove old rows
                    _regionProductRows.RemoveRange(existingRows);
                    _regionProductRows.AddRange(newRows);
                }
            }
        }

        #endregion

        #region SelectPrimaryProduct

        private RelayCommand _selectPrimaryProductCommand;

        /// <summary>
        /// Allows selection of available products to be the regional primary product
        /// </summary>
        public RelayCommand SelectPrimaryProductCommand
        {
            get
            {
                if (_selectPrimaryProductCommand == null)
                {
                    _selectPrimaryProductCommand =
                        new RelayCommand(p => SelectPrimaryProduct_Executed())
                        {
                            FriendlyName = Message.AssortmentDocument_More,
                            FriendlyDescription = Message.AssortmentDocument_SelectPrimaryProduct_Tooltip
                        };
                    base.ViewModelCommands.Add(_selectPrimaryProductCommand);
                }
                return _selectPrimaryProductCommand;
            }
        }

        private void SelectPrimaryProduct_Executed()
        {
            if (ContinueWithItemChange())
            {
                if (this.AttachedControl != null)
                {
                    // Get list of products that can be the primary regional product
                    var availableAssortmentProducts = _currentAssortment.Products
                        .Where(
                            p => p.ProductLocalizationType == PlanogramAssortmentProductLocalizationType.None || p.IsPrimaryRegionalProduct)
                        .ToList();

                    var assortmentRegionalPrimaryProductSelectionWindow = new AssortmentRegionalPrimaryProductSelectionWindow(availableAssortmentProducts.Select(
                        p => new PlanogramAssortmentProductView(p, _planItems.FirstOrDefault(planItems => planItems.Product.Gtin == p.Gtin))));

                    App.ShowWindow(assortmentRegionalPrimaryProductSelectionWindow, this.AttachedControl, true);

                    if (assortmentRegionalPrimaryProductSelectionWindow.DialogResult == true)
                    {
                        this.SelectedPrimaryProduct = assortmentRegionalPrimaryProductSelectionWindow.SelectionResult.Product;
                    }
                }
            }
        }

        #endregion

        #region SelectRegionalProduct

        private RelayCommand _selectRegionalProductCommand;

        /// <summary>
        /// Allows selection of available products that can be a regional swap
        /// </summary>
        public RelayCommand SelectRegionalProductCommand
        {
            get
            {
                if (_selectRegionalProductCommand == null)
                {
                    _selectRegionalProductCommand = new RelayCommand(
                        p => SelectRegionalProduct_Executed(p),
                        p => SelectRegionalProduct_CanExecute(p))
                    {
                        FriendlyName = Message.AssortmentDocument_More
                    };
                    base.ViewModelCommands.Add(_selectRegionalProductCommand);
                }
                return _selectRegionalProductCommand;
            }
        }

        private Boolean SelectRegionalProduct_CanExecute(Object arg)
        {
            var row = arg as AssortmentRegionProductRow;

            //must have a region row selected
            if (row == null)
            {
                return false;
            }

            return true;
        }

        private void SelectRegionalProduct_Executed(Object arg)
        {
            var row = arg as AssortmentRegionProductRow;
            if (row != null)
            {

                if (this.AttachedControl != null)
                {
                    // Load the available assortment products that can be regional swaps
                    var availableAssortmentProducts = new List<PlanogramAssortmentProduct>();

                    foreach (var assortmentProduct in this._currentAssortment.Products)
                    {
                        if (assortmentProduct.Gtin == this.SelectedPrimaryProduct.Gtin)
                        {
                            availableAssortmentProducts.Add(assortmentProduct);
                        }
                        else
                        {
                            if (assortmentProduct.ProductLocalizationType == PlanogramAssortmentProductLocalizationType.None)
                            {
                                //check that the product hasnt already been assigned a region
                                Boolean alreadyAssigned = false;

                                foreach (var regionProductRow in RegionProductRows)
                                {
                                    if (regionProductRow.SelectedProduct != null && regionProductRow.SelectedProduct.Gtin == assortmentProduct.Gtin)
                                    {
                                        alreadyAssigned = true;
                                    }
                                }

                                if (!alreadyAssigned) { availableAssortmentProducts.Add(assortmentProduct); }
                            }
                        }
                    }

                    var assortmentRegionalProductSelectionWindow = new AssortmentRegionalProductSelectionWindow(
                        availableAssortmentProducts.Select(p => new PlanogramAssortmentProductView(p, _planItems.FirstOrDefault(planItems => planItems.Product.Gtin == p.Gtin))));

                    App.ShowWindow(assortmentRegionalProductSelectionWindow, this.AttachedControl, true);

                    if (assortmentRegionalProductSelectionWindow.DialogResult == true)
                    {
                        this.SelectedRegionProductRow.SelectedProduct = assortmentRegionalProductSelectionWindow.SelectionResult.Product;
                    }
                }
            }
        }

        #endregion

        #region RemoveRegionalProduct

        private RelayCommand _removeRegionalProductCommand;

        /// <summary>
        /// Allows selection of available products that can be a regional swap
        /// </summary>
        public RelayCommand RemoveRegionalProductCommand
        {
            get
            {
                if (_removeRegionalProductCommand == null)
                {
                    _removeRegionalProductCommand = new RelayCommand(
                        p => RemoveRegionalProduct_Executed(p),
                        p => RemoveRegionalProduct_CanExecute(p))
                    {
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeRegionalProductCommand);
                }
                return _removeRegionalProductCommand;
            }
        }

        private Boolean RemoveRegionalProduct_CanExecute(Object arg)
        {
            var row = arg as AssortmentRegionProductRow;

            //must have a region row selected
            if (row == null)
            {
                return false;
            }

            return true;
        }

        private void RemoveRegionalProduct_Executed(Object arg)
        {
            var row = arg as AssortmentRegionProductRow;
            if (row != null)
            {
                if (this.AttachedControl != null)
                {
                    this.SelectedRegionProductRow.SelectedProduct = null;
                }
            }
        }

        #endregion

        #endregion

        #region EventHandlers

        /// <summary>
        /// Responds to a change of selected primary product
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedPrimaryProductChanged(PlanogramAssortmentProduct newValue)
        {
            ReloadRegionRows();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                if (this.RegionProductRows.Any(r => r.IsDirty))
                {
                    var result = Galleria.Framework.Controls.Wpf.Helpers.ShowContinueWithChildItemChangeDialog(this.ApplyCommand.CanExecute());

                    if (result == ChildItemChangeResult.Apply)
                    {
                        this.ApplyCommand.Execute();
                    }
                    else if (result == ChildItemChangeResult.DoNotApply)
                    {
                        //Do nothing
                    }
                    else
                    {
                        continueExecute = false;
                    }
                }
            }
            return continueExecute;
        }

        /// <summary>
        /// Reloads the region rows collection
        /// </summary>
        private void ReloadRegionRows()
        {
            _regionProductRows.Clear();

            var primaryProduct = this.SelectedPrimaryProduct;
            if (primaryProduct != null)
            {
                //create a row per region
                foreach (var region in _currentAssortment.Regions)
                {
                    _regionProductRows.Add(new AssortmentRegionProductRow(region, primaryProduct));
                }
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    if (_regionProductRows != null)
                    {
                        _regionProductRows.Clear();
                    }

                    _selectedPrimaryProduct = null;
                    _currentAssortment = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
