﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26491 : A.Kuszyk
//  Adapted from Workflow client.

#endregion

#region Version History : CCM830

// V8-32560 : A.Silva
//  Removed AddAll and RemoveAll commands, ammended Icons for AddSelected and RemoveSelected commands.
// V8-32987 : R.Cooper
//  Clear RegionLocations collection to ensure assigned locations grid is cleared out correctly.
// V8-32990 : R.Cooper
//  Ensure any assigned locations are removed from the available locations grid.
// CCM-18480 : D.Pleasance
//  Updated Region Locations to Location model rather than LocationInfo so all attributes can be selected from grids.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Collections;
using Galleria.Ccm.Model;
using System.Collections.ObjectModel;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using System.Diagnostics;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    public sealed class AssortmentAddRegionsViewModel : ViewModelAttachedControlObject<AssortmentAddRegionsWindow>
    {
        #region Fields

        private PlanogramAssortment _currentAssortment;
        private LocationList _locations = null;
        private Dictionary<String, Location> _locationLookup = null;

        private BulkObservableCollection<AssortmentRegionRow> _regionRows = new BulkObservableCollection<AssortmentRegionRow>();
        private ReadOnlyBulkObservableCollection<AssortmentRegionRow> _regionRowsRO;

        private AssortmentRegionRow _selectedRegionRow;

        private BulkObservableCollection<Location> _availableLocations = new BulkObservableCollection<Location>();
        private ReadOnlyBulkObservableCollection<Location> _availableLocationsRO;

        private ObservableCollection<Location> _selectedAvailableLocations = new ObservableCollection<Location>();
        private ObservableCollection<Location> _selectedRegionLocations = new ObservableCollection<Location>();

        private List<AssortmentRegionRow> _deletedRegionRows = new List<AssortmentRegionRow>();
        
        #endregion

        #region Binding properties

        //properties
        public static readonly PropertyPath CurrentAssortmentProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionsViewModel>(p => p.CurrentAssortment);
        public static readonly PropertyPath RegionRowsProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionsViewModel>(p => p.RegionRows);
        public static readonly PropertyPath SelectedRegionRowProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionsViewModel>(p => p.SelectedRegionRow);
        public static readonly PropertyPath AvailableLocationsProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionsViewModel>(p => p.AvailableLocations);
        public static readonly PropertyPath SelectedAvailableLocationsProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionsViewModel>(p => p.SelectedAvailableLocations);
        public static readonly PropertyPath SelectedRegionLocationsProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionsViewModel>(p => p.SelectedRegionLocations);

        //commands
        public static readonly PropertyPath ApplyCommandProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionsViewModel>(p => p.ApplyCommand);
        public static readonly PropertyPath ApplyAndCloseCommandProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionsViewModel>(p => p.ApplyAndCloseCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionsViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath AddRegionCommandProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionsViewModel>(p => p.AddRegionCommand);
        public static readonly PropertyPath RemoveSelectedRegionCommandProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionsViewModel>(p => p.RemoveSelectedRegionCommand);
        public static readonly PropertyPath AddSelectedLocationsCommandProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionsViewModel>(p => p.AddSelectedLocationsCommand);
        public static readonly PropertyPath RemoveSelectedLocationsCommandProperty = WpfHelper.GetPropertyPath<AssortmentAddRegionsViewModel>(p => p.RemoveSelectedLocationsCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Property for the selected Assortment
        /// </summary>
        public PlanogramAssortment CurrentAssortment
        {
            get { return _currentAssortment; }
            set
            {
                _currentAssortment = value;
                OnPropertyChanged(CurrentAssortmentProperty);
            }
        }

        /// <summary>
        /// Returns the collection of family rows.
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssortmentRegionRow> RegionRows
        {
            get
            {
                if (_regionRowsRO == null)
                {
                    _regionRowsRO = new ReadOnlyBulkObservableCollection<AssortmentRegionRow>(_regionRows);
                }
                return _regionRowsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected region row
        /// </summary>
        public AssortmentRegionRow SelectedRegionRow
        {
            get { return _selectedRegionRow; }
            set
            {
                _selectedRegionRow = value;
                OnPropertyChanged(SelectedRegionRowProperty);

                OnSelectedRegionRowChanged(value);
            }
        }

        /// <summary>
        /// Returns the readonly collection of locations still available for adding to the region
        /// </summary>
        public ReadOnlyBulkObservableCollection<Location> AvailableLocations
        {
            get
            {
                if (_availableLocationsRO == null)
                {
                    _availableLocationsRO = new ReadOnlyBulkObservableCollection<Location>(_availableLocations);
                }
                return _availableLocationsRO;
            }
        }

        /// <summary>
        /// Returns the collection of selected available locations
        /// </summary>
        public ObservableCollection<Location> SelectedAvailableLocations
        {
            get { return _selectedAvailableLocations; }
        }

        /// <summary>
        /// Returns the collection of selected region locations
        /// </summary>
        public ObservableCollection<Location> SelectedRegionLocations
        {
            get { return _selectedRegionLocations; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="assortment">The given scenario</param>
        public AssortmentAddRegionsViewModel(PlanogramAssortment assortment)
        {
            //Load locations once
            _locations = LocationList.FetchByEntityId(App.ViewState.EntityId);
            _locationLookup = LocationList.FetchByEntityId(App.ViewState.EntityId).ToDictionary(p => p.Code);

            _regionRows.BulkCollectionChanged += RegionRows_BulkCollectionChanged;
            _selectedAvailableLocations.CollectionChanged += SelectedAvailableLocations_CollectionChanged;
            _selectedRegionLocations.CollectionChanged += SelectedRegionLocations_CollectionChanged;

            _currentAssortment = assortment;

            ReloadRegionRows();

            //select the first available row.
            if (this.RegionRows.Count > 0)
            {
                this.SelectedRegionRow = this.RegionRows.First();
            }
        }

        #endregion

        #region Commands

        #region ApplyCommand

        private RelayCommand _applyCommand;

        /// <summary>
        /// Applies any changes made in the current item to the parent scenario
        /// </summary>
        public RelayCommand ApplyCommand
        {
            get
            {
                if (_applyCommand == null)
                {
                    _applyCommand = new RelayCommand(
                        p => Apply_Executed(),
                        p => Apply_CanExecute())
                    {
                        FriendlyName = Message.Generic_Apply,
                        FriendlyDescription = Message.Generic_Apply_Tooltip,
                        Icon = ImageResources.Apply_32,
                        SmallIcon = ImageResources.Apply_16
                    };
                    base.ViewModelCommands.Add(_applyCommand);
                }
                return _applyCommand;
            }
        }

        private Boolean Apply_CanExecute()
        {
            //at least one row must be dirty
            if (!this.RegionRows.Any(f => f.IsDirty))
            {
                //if no rows are dirty, check if any rows have been deleted
                if (this._deletedRegionRows.Count == 0)
                {
                    return false;
                }
                return true;
            }

            //all rows must be valid
            if (!this.RegionRows.All(r => r.Region.IsValid))
            {
                return false;
            }

            //all regions must have a unique name
            if (this.RegionRows.Select(r => r.Region.Name).Distinct().Count() != this.RegionRows.Count)
            {
                return false;
            }

            return true;
        }

        private void Apply_Executed()
        {
            ApplyCurrentItem();
        }

        private void ApplyCurrentItem()
        {
            base.ShowWaitCursor(true);

            //delete all removed rows
            foreach (var deletedRow in _deletedRegionRows)
            {
                deletedRow.DeleteExistingRegion();
                deletedRow.Dispose();
            }
            _deletedRegionRows.Clear();

            //cycle through all rows applying changes made
            foreach (var row in this.RegionRows)
            {
                row.CommitChanges(_currentAssortment);
            }
            
            //recreate all rows
            ReloadRegionRows();

            //Ensure any assigned locations are removed from the available locations grid.
            ReloadAvailableLocations();

            base.ShowWaitCursor(false);
        }

        #endregion

        #region ApplyAndCloseCommand

        private RelayCommand _applyAndCloseCommand;
        /// <summary>
        /// Executes the save command then the close command
        /// The organiser should handle this command to peform the close action
        /// </summary>
        public RelayCommand ApplyAndCloseCommand
        {
            get
            {
                if (_applyAndCloseCommand == null)
                {
                    _applyAndCloseCommand = new RelayCommand(
                        p => ApplyAndClose_Executed(),
                        p => ApplyAndClose_CanExecute())
                    {
                        FriendlyName = Message.Generic_ApplyAndClose,
                        FriendlyDescription = Message.Generic_ApplyAndClose_Tooltip,
                        SmallIcon = ImageResources.ApplyAndClose_16
                    };
                    base.ViewModelCommands.Add(_applyAndCloseCommand);
                }
                return _applyAndCloseCommand;
            }
        }

        private Boolean ApplyAndClose_CanExecute()
        {
            //all rows must be valid
            if (!this.RegionRows.All(r => r.Region.IsValid))
            {
                return false;
            }

            //all regions must have a unique name
            if (this.RegionRows.Select(r => r.Region.Name).Distinct().Count() != this.RegionRows.Count)
            {
                return false;
            }

            return true;
        }

        private void ApplyAndClose_Executed()
        {
            ApplyCurrentItem();

            //close the attached window
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels changes and closes the window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region AddRegionCommand

        private RelayCommand _addRegionCommand;

        /// <summary>
        /// Adds a new region to the list
        /// </summary>
        public RelayCommand AddRegionCommand
        {
            get
            {
                if (_addRegionCommand == null)
                {
                    _addRegionCommand = new RelayCommand(
                        p => AddRegion_Executed())
                    {
                        FriendlyName = Message.AssortmentDocument_Region_AddRegion,
                        SmallIcon = ImageResources.Add_16,
                    };
                    base.ViewModelCommands.Add(_addRegionCommand);
                }
                return _addRegionCommand;
            }
        }

        private void AddRegion_Executed()
        {
            var newRow = new AssortmentRegionRow();

            _regionRows.Add(newRow);

            this.SelectedRegionRow = newRow;
        }

        #endregion

        #region RemoveSelectedRegionCommand

        private RelayCommand _removeSelectedRegionCommand;

        /// <summary>
        /// Removes the currently selected region row
        /// </summary>
        public RelayCommand RemoveSelectedRegionCommand
        {
            get
            {
                if (_removeSelectedRegionCommand == null)
                {
                    _removeSelectedRegionCommand = new RelayCommand(
                        p => RemoveSelectedRegion_Executed(),
                        p => RemoveSelectedRegion_CanExecute())
                    {
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeSelectedRegionCommand);
                }
                return _removeSelectedRegionCommand;
            }
        }

        private Boolean RemoveSelectedRegion_CanExecute()
        {
            if (this.SelectedRegionRow == null)
            {
                return false;
            }

            return true;
        }

        private void RemoveSelectedRegion_Executed()
        {
            var rowToRemove = this.SelectedRegionRow;

            //warn the user if the region to be deleted has any associated products
            Boolean continueWithRemove = true;
            if (rowToRemove.Region.Products.Count > 0)
            {
                //warn the user
                ModalMessage msg = new ModalMessage();
                msg.Header = rowToRemove.Region.Name;
                msg.Description = Message.AssortmentDocument_Region_DeleteHasRulesWarning_Desc;
                msg.ButtonCount = 2;
                msg.Button1Content = Message.Generic_OK;
                msg.Button2Content = Message.Generic_Cancel;
                msg.CancelButton = ModalMessageButton.Button2;
                msg.DefaultButton = ModalMessageButton.Button1;
                msg.MessageIcon = ImageResources.Warning_32;

                App.ShowWindow(msg, this.AttachedControl, /*isModal*/true);

                if (msg.Result != ModalMessageResult.Button1)
                {
                    continueWithRemove = false;
                }
            }

            if (continueWithRemove)
            {
                //select the next available row
                if (this.RegionRows.Count > 1)
                {
                    this.RegionRows.First(r => r != rowToRemove);
                }
                rowToRemove.RegionLocations.Clear();

                //remove the row from the main collection and add to to the deleted list
                _regionRows.Remove(rowToRemove);
                _deletedRegionRows.Add(rowToRemove);
            }
        }

        #endregion

        #region AddSelectedLocationsCommand

        private RelayCommand _addSelectedLocationsCommand;

        /// <summary>
        /// Adds any selected available locations to taken list
        /// </summary>
        public RelayCommand AddSelectedLocationsCommand
        {
            get
            {
                if (_addSelectedLocationsCommand == null)
                {
                    _addSelectedLocationsCommand = new RelayCommand(
                        p => AddSelectedLocations_Executed(),
                        p => AddSelectedLocations_CanExecute())
                    {
                        FriendlyDescription = Message.Generic_AddToAssigned,
                        SmallIcon = ImageResources.Add_16
                    };
                    base.ViewModelCommands.Add(_addSelectedLocationsCommand);
                }
                return _addSelectedLocationsCommand;
            }
        }

        private Boolean AddSelectedLocations_CanExecute()
        {
            //must have a region
            if (this.SelectedRegionRow == null)
            {
                this.AddSelectedLocationsCommand.DisabledReason = Message.AssortmentDocument_NoRegionSelected;
                return false;
            }

            //must have selected locations available
            if (this.SelectedAvailableLocations.Count == 0)
            {
                this.AddSelectedLocationsCommand.DisabledReason = Message.AssortmentDocument_NoLocationsSelected;
                return false;
            }

            return true;
        }

        private void AddSelectedLocations_Executed()
        {
            var addList = this.SelectedAvailableLocations.ToList();

            //clear off the selection first to make loading quicker.
            this.SelectedAvailableLocations.Clear();

            //remove from the available
            _availableLocations.RemoveRange(addList);

            //add the items
            this.SelectedRegionRow.RegionLocations.AddRange(addList);
        }

        #endregion

        #region RemoveSelectedLocationsCommand

        private RelayCommand _removeSelectedLocationsCommand;

        /// <summary>
        /// Removes the selected locations from the taken list
        /// </summary>
        public RelayCommand RemoveSelectedLocationsCommand
        {
            get
            {
                if (_removeSelectedLocationsCommand == null)
                {
                    _removeSelectedLocationsCommand = new RelayCommand(
                        p => RemoveSelectedLocations_Executed(),
                        p => RemoveSelectedLocations_CanExecute())
                    {
                        FriendlyDescription = Message.Generic_RemoveFromAssigned,
                        SmallIcon = ImageResources.Delete_16,
                        DisabledReason = Message.AssortmentDocument_NoLocationsSelected
                    };
                    base.ViewModelCommands.Add(_removeSelectedLocationsCommand);
                }
                return _removeSelectedLocationsCommand;
            }
        }

        private Boolean RemoveSelectedLocations_CanExecute()
        {
            if (this.SelectedRegionRow == null)
            {
                return false;
            }

            if (this.SelectedRegionLocations.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void RemoveSelectedLocations_Executed()
        {
            var removeList = SelectedRegionLocations.ToList();

            //clear off the selection first to make loading quicker.
            this.SelectedRegionLocations.Clear();

            //remove the items
            this.SelectedRegionRow.RegionLocations.RemoveRange(removeList);

            //add to the available
            this.ReloadAvailableLocations();
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change of selected family
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedRegionRowChanged(AssortmentRegionRow newValue)
        {
            ReloadAvailableLocations();
        }

        /// <summary>
        /// Responds to changes to the family rows collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RegionRows_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove ||
                e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Reset)
            {
                foreach (AssortmentRegionRow row in e.ChangedItems)
                {
                    row.Dispose();
                }
            }
        }

        /// <summary>
        /// Responds to selected item changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedAvailableLocations_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                this.SelectedRegionLocations.Clear();
            }
        }

        /// <summary>
        /// Responds to selected item changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedRegionLocations_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                this.SelectedAvailableLocations.Clear();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                if (this.RegionRows.Any(p => p.IsDirty))
                {
                    ChildItemChangeResult result = Galleria.Framework.Controls.Wpf.Helpers.ShowContinueWithChildItemChangeDialog(this.ApplyCommand.CanExecute());

                    if (result == ChildItemChangeResult.Apply)
                    {
                        this.ApplyCommand.Execute();
                    }
                    else if (result == ChildItemChangeResult.DoNotApply)
                    {
                        //do nothing
                    }
                    else
                    {
                        continueExecute = false;
                    }

                }
            }
            return continueExecute;
        }

        /// <summary>
        /// Recreates the family rows collection
        /// </summary>
        private void ReloadRegionRows()
        {
            if (_regionRows.Count > 0)
            {
                if (this.SelectedRegionRow != null)
                {
                    this.SelectedRegionRow.RegionLocations.Clear();
                }
                _regionRows.Clear();
            }

            var newRows = new List<AssortmentRegionRow>();
            foreach (var region in _currentAssortment.Regions)
            {
                List<Location> regionLocations = new List<Location>();
                foreach (String locationCode in region.Locations.Select(l => l.LocationCode))
                {
                    Location location = null;
                    if(_locationLookup.TryGetValue(locationCode, out location))
                    {
                        regionLocations.Add(location);
                    }
                }

                newRows.Add(new AssortmentRegionRow(region, regionLocations));
            }
            _regionRows.AddRange(newRows);
        }

        /// <summary>
        /// Reloads the list of available products
        /// </summary>
        private void ReloadAvailableLocations()
        {
            if (_availableLocations.Count > 0)
            {
                _availableLocations.Clear();
            }
            
            _availableLocations.AddRange(_locations.
                Where(location => 
                    !RegionRows.
                    SelectMany(row => row.RegionLocations).
                    Any(regionLocation => regionLocation.Code == location.Code)
                    ));
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                _regionRows.BulkCollectionChanged -= RegionRows_BulkCollectionChanged;
                _selectedAvailableLocations.CollectionChanged -= SelectedAvailableLocations_CollectionChanged;
                _selectedRegionLocations.CollectionChanged -= SelectedRegionLocations_CollectionChanged;
                _locationLookup = null;
                _locations = null;

                if (disposing)
                {
                    _regionRows.Clear();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}