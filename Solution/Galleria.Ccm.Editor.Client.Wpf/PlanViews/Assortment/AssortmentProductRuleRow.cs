﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26491 : A.Kuszyk
//      Created (adapted from Workflow client).
#endregion

#region Version History: (CCM 830)
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32718 : A.Probyn ~ Updated so full product attributes can be displayed.
// V8-32718 : D.Pleasance
//  Added planItems to constructor. These are passed through to enable displaying of product / position attributes.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.Model;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Common;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Represents a row on the product rules screen
    /// </summary>
    public sealed class AssortmentProductRuleRow : ViewModelObject
    {
        #region Fields
        private PlanogramAssortment _assortment;
        private PlanogramAssortmentProduct _product;
        private PlanogramProduct _planogramProduct;
        private PlanogramAssortmentProduct _existingProduct;
        private Boolean _isDirty;
        private IPlanItem _planItem;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ProductProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleRow>(p => p.Product);
        public static readonly PropertyPath PlanogramProductProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleRow>(p => p.PlanogramProduct);
        public static readonly PropertyPath ProductGtinProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleRow>(p => p.ProductGtin);
        public static readonly PropertyPath ProductNameProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleRow>(p => p.ProductName);
        public static readonly PropertyPath RuleSummaryProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleRow>(p => p.RuleSummary);
        public static readonly PropertyPath IsDirtyProperty = WpfHelper.GetPropertyPath<AssortmentProductRuleRow>(p => p.IsDirty);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the product this row relates to
        /// </summary>
        public PlanogramAssortment SelectedAssortment
        {
            get { return _assortment; }
        }

        /// <summary>
        /// Gets the product this row relates to
        /// </summary>
        public PlanogramAssortmentProduct Product
        {
            get { return _product; }
        }

        public PlanogramProduct PlanogramProduct
        {
            get
            {
                if (_planogramProduct == null)
                {
                    _planogramProduct = _existingProduct.GetPlanogramProduct();
                }
                return _planogramProduct;
            }
        }


        public IPlanItem PlanItem
        {
            get
            {
                return _planItem;
            }
        }
        
        
        /// <summary>
        /// Gets the Gtin of the product this row relates to
        /// </summary>
        public String ProductGtin
        {
            get { return _product.Gtin; }
        }

        /// <summary>
        /// Gets the Name of the product this row relates to
        /// </summary>
        public String ProductName
        {
            get { return _product.Name; }
        }

        /// <summary>
        /// Gets a summary of the current rule
        /// </summary>
        public String RuleSummary
        {
            get
            {
                if (this.IsNullRule)
                {
                    return String.Empty;
                }
                else
                {
                    return _product.RuleSummary;
                }
            }
        }

        /// <summary>
        /// Returns true if the rule has no values.
        /// </summary>
        public Boolean IsNullRule
        {
            get
            {
                if (_product == null) { return true; }
                return (_product.RuleType == PlanogramAssortmentProductRuleType.None && _product.HasMaximum == false);
            }
        }

        /// <summary>
        /// Returns true if the row is dirty
        /// </summary>
        public Boolean IsDirty
        {
            get { return _isDirty; }
            private set
            {
                _isDirty = value;
                OnPropertyChanged(IsDirtyProperty);
            }
        }

        public Boolean HasMaximum
        {
            get { return _product.HasMaximum; }
        }
        #endregion

        #region Constructor
        public AssortmentProductRuleRow(PlanogramAssortment assortment, PlanogramAssortmentProduct product, IPlanItem planItem)
        {
            _assortment = assortment;
            _existingProduct = product;
            _planItem = planItem;

            //Take copy for custom begin edit
            _product = _existingProduct.Clone();
            _product.PropertyChanged += Product_PropertyChanged;

            _isDirty = false;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to changes to the current rule
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Product_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            OnPropertyChanged(RuleSummaryProperty);
            this.IsDirty = true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Commits any changes made back to the given node
        /// </summary>
        /// <param name="applyToRange"></param>
        public void CommitChanges()
        {
            if (this.IsDirty)
            {
                var existingRule = _existingProduct;
                var editRule = _product;

                //copy values
                existingRule.ExactListFacings = editRule.ExactListFacings;
                existingRule.ExactListUnits = editRule.ExactListUnits;
                existingRule.PreserveListFacings = editRule.PreserveListFacings;
                existingRule.PreserveListUnits = editRule.PreserveListUnits;
                existingRule.MinListFacings = editRule.MinListFacings;
                existingRule.MinListUnits = editRule.MinListUnits;
                existingRule.MaxListFacings = editRule.MaxListFacings;
                existingRule.MaxListUnits = editRule.MaxListUnits;
                existingRule.ProductTreatmentType = editRule.ProductTreatmentType;

                this.IsDirty = false;
            }
        }

        /// <summary>
        /// Clears off this rule
        /// </summary>
        public void ClearRule()
        {
            _product.ClearRule();
            this.IsDirty = true;
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                _product.PropertyChanged -= Product_PropertyChanged;

                if (disposing)
                {
                    _assortment = null;
                    _existingProduct = null;
                    _product = null;
                }
            }

        }

        #endregion
    }
}
