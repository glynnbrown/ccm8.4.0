﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.2.0)
// V8-30705 : A.Probyn
//  Adapted from SA
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using System.ComponentModel;
using System.Windows.Controls;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Interaction logic for AssortmentRulePrioritiesWindow.xaml
    /// </summary>
    public sealed partial class AssortmentRulePrioritiesWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentRulePrioritiesViewModel),
            typeof(AssortmentRulePrioritiesWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public AssortmentRulePrioritiesViewModel ViewModel
        {
            get { return (AssortmentRulePrioritiesViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentRulePrioritiesWindow senderControl =
                (AssortmentRulePrioritiesWindow)obj;

            if (e.OldValue != null)
            {
                AssortmentRulePrioritiesViewModel oldModel =
                    (AssortmentRulePrioritiesViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                AssortmentRulePrioritiesViewModel newModel =
                    (AssortmentRulePrioritiesViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public AssortmentRulePrioritiesWindow(PlanogramAssortment selectedAssortment)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add link to SA.chm
            Help.SetFilename((DependencyObject)this, App.ViewState.HelpFilePath);
            Help.SetKeyword((DependencyObject)this, "24");

            this.ViewModel = new AssortmentRulePrioritiesViewModel(selectedAssortment);

            this.Loaded += RulePrioritiesWindow_Loaded;

            //add events for the drag and drop to the style
            Style itemContainerStyle = new Style(typeof(ListBoxItem));
            itemContainerStyle.BasedOn = (Style)App.Current.FindResource(Framework.Controls.Wpf.ResourceKeys.ListBox_StyDefaultListBoxItem);
            //itemContainerStyle.Setters.Add(new Setter(ListBoxItem.AllowDropProperty, true));
            //itemContainerStyle.Setters.Add(new EventSetter(ListBoxItem.PreviewMouseLeftButtonDownEvent, new MouseButtonEventHandler(s_PreviewMouseLeftButtonDown)));
            //itemContainerStyle.Setters.Add(new EventSetter(ListBoxItem.DropEvent, new DragEventHandler(RulesList_Drop)));
            //add the style back to the list
            rulesList.ItemContainerStyle = itemContainerStyle;
        }

        /// <summary>
        /// Event handler for the on loaded event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RulePrioritiesWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= new RoutedEventHandler(RulePrioritiesWindow_Loaded);

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event handlers

        //protected override void OnKeyDown(KeyEventArgs e)
        //{
        //    base.OnKeyDown(e);
        //}

        ////custom handler for beginning dragging
        //private void s_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        //{
        //    if (sender is ListBoxItem)
        //    {
        //        //get dragged item
        //        ListBoxItem draggedItem = sender as ListBoxItem;
        //        PriorityRule target = ((ListBoxItem)(sender)).DataContext as PriorityRule;
        //        if (!target.IsStaticPriority)
        //        {
        //            //do drag/drop
        //            DragDrop.DoDragDrop(draggedItem, draggedItem.DataContext, DragDropEffects.Move);
        //        }
        //        //reselect the item
        //        draggedItem.IsSelected = true;
        //        //do the base click event
        //        base.OnPreviewMouseLeftButtonDown(e);
        //    }
        //}

        ////method for drop
        //private void RulesList_Drop(object sender, DragEventArgs e)
        //{
        //    //get both the moved item and the target item
        //    PriorityRule droppedData = e.Data.GetData(typeof(PriorityRule)) as PriorityRule;
        //    PriorityRule target = ((ListBoxItem)(sender)).DataContext as PriorityRule;
        //    if (!droppedData.IsStaticPriority && !target.IsStaticPriority)
        //    {
        //        //move from dragged data position to target
        //        this.ViewModel.MoveRanks(droppedData, target);
        //    }
        //}

        #endregion

        #region Window Close

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {
                   IDisposable disposableViewModel = this.ViewModel;
                   this.ViewModel = null;

                   if (disposableViewModel != null)
                   {
                       disposableViewModel.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion
    }
}

