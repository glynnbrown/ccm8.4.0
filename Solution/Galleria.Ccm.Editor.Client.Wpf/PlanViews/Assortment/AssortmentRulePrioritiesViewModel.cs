﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.2.0)
// V8-30705 : A.Probyn
//  Adapted from SA. This is a read only version where the priorities cannot be changed.
//  All code to mode priorities is commented out until implemented later on based on the priorities object
//  that will come from the assortment itself.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using System.ComponentModel;
using System.Diagnostics;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    public sealed class AssortmentRulePrioritiesViewModel : ViewModelAttachedControlObject<AssortmentRulePrioritiesWindow>
    {
        #region Constants

        //private const Byte rulePriorityAdjustmentFactor = 3;

        #endregion

        #region Fields
        private Entity _selectedEntity;
        private BulkObservableCollection<PriorityRule> _rulesList = new BulkObservableCollection<PriorityRule>();
        private PriorityRule _selectedRule;

        #endregion

        #region Binding Properties

        //properties
        //public static readonly PropertyPath SelectedEntityProperty = WpfHelper.GetPropertyPath<AssortmentRulePrioritiesViewModel>(p => p.SelectedEntity);
        public static readonly PropertyPath RulesListProperty = WpfHelper.GetPropertyPath<AssortmentRulePrioritiesViewModel>(p => p.RulesList);
        public static readonly PropertyPath SelectedRuleProperty = WpfHelper.GetPropertyPath<AssortmentRulePrioritiesViewModel>(p => p.SelectedRule);

        //commands
        public static readonly PropertyPath OkCommandProperty = WpfHelper.GetPropertyPath<AssortmentRulePrioritiesViewModel>(p => p.OkCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<AssortmentRulePrioritiesViewModel>(p => p.CancelCommand);
        //public static readonly PropertyPath IncreaseRankCommandProperty = WpfHelper.GetPropertyPath<AssortmentRulePrioritiesViewModel>(p => p.IncreaseRankCommand);
        //public static readonly PropertyPath DecreaseRankCommandProperty = WpfHelper.GetPropertyPath<AssortmentRulePrioritiesViewModel>(p => p.DecreaseRankCommand);

        #endregion

        #region Properties
        
        /// <summary>
        /// Gets/Sets the available rules
        /// </summary>
        public BulkObservableCollection<PriorityRule> RulesList
        {
            get { return _rulesList; }
        }

        /// <summary>
        /// Property for the selected Entity
        /// </summary>
        public PriorityRule SelectedRule
        {
            get { return _selectedRule; }
            set
            {
                _selectedRule = value;
                OnPropertyChanged(SelectedRuleProperty);
            }
        }

        #endregion

        #region Constructor
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="entity"></param>
        public AssortmentRulePrioritiesViewModel(PlanogramAssortment planogramAssortment)
        {
            //Add 3 static priority rules
            //Nb - Increase rulePriorityAdjustmentFactor if number changes
            RulesList.Add(new PriorityRule(Message.AssortmentRulePriorities_LocalProductRules, 1, PriorityType.LocalProductRules));
            RulesList.Add(new PriorityRule(Message.AssortmentRulePriorities_DistributionRules, 2, PriorityType.DistributionRules));

            //add rules to list in order from assortment eventually, for now - hardcode
            RulesList.Add(new PriorityRule(Message.AssortmentRulePriorities_ProductRules, 3, PriorityType.ProductRules));
            RulesList.Add(new PriorityRule(Message.AssortmentRulePriorities_FamilyRules, 4, PriorityType.FamilyRules));
            //for (Byte i = 1; i < 5; i++)
            //{
            //    if (i == SelectedEntity.RulePriorities.CdtNodeRulePriority)
            //    {
            //        RulesList.Add(new PriorityRule(Message.RulePriorities_CdtNodeRules, (Byte)(i + rulePriorityAdjustmentFactor), PriorityType.CdtNodeRules));
            //    }
            //    if (i == SelectedEntity.RulePriorities.FamilyRulePriority)
            //    {
            //        RulesList.Add(new PriorityRule(Message.RulePriorities_FamilyRules, (Byte)(i + rulePriorityAdjustmentFactor), PriorityType.FamilyRules));
            //    }
            //    if (i == SelectedEntity.RulePriorities.ImplicitCdtNodeRulePriority)
            //    {
            //        RulesList.Add(new PriorityRule(Message.RulePriorities_ImplicitCdtNodeRules, (Byte)(i + rulePriorityAdjustmentFactor), PriorityType.ImplicitCdtNodeRules));
            //    }
            //    if (i == SelectedEntity.RulePriorities.ProductRulePriority)
            //    {
            //        RulesList.Add(new PriorityRule(Message.RulePriorities_ProductRules, (Byte)(i + rulePriorityAdjustmentFactor), PriorityType.ProductRules));
            //    }
            //}

            //Add final static property - lowest
            RulesList.Add(new PriorityRule(Message.AssortmentRulePriorities_InheritanceRules, 5, PriorityType.InheritanceRules));
            
        }

        #endregion

        #region Commands

        #region Ok

        private RelayCommand _okCommand;

        /// <summary>
        /// applys the current item
        /// </summary>
        public RelayCommand OkCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => Ok_Executed())
                    {
                        FriendlyName = Message.Generic_OK
                    };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        private void Ok_Executed()
        {
            //modify the RulePriorities in the entity
            //Decrease to remove the adjustment required for the static rules.
            //SelectedEntity.RulePriorities.CdtNodeRulePriority = (Byte)(RulesList.First<PriorityRule>(p => p.RuleType == PriorityType.CdtNodeRules).Rank - rulePriorityAdjustmentFactor);
            //SelectedEntity.RulePriorities.FamilyRulePriority = (Byte)(RulesList.First<PriorityRule>(p => p.RuleType == PriorityType.FamilyRules).Rank - rulePriorityAdjustmentFactor);
            //SelectedEntity.RulePriorities.ImplicitCdtNodeRulePriority = (Byte)(RulesList.First<PriorityRule>(p => p.RuleType == PriorityType.ImplicitCdtNodeRules).Rank - rulePriorityAdjustmentFactor);
            //SelectedEntity.RulePriorities.ProductRulePriority = (Byte)(RulesList.First<PriorityRule>(p => p.RuleType == PriorityType.ProductRules).Rank - rulePriorityAdjustmentFactor);

            //close
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels changes and closes the window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            //close
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        //#region IncreaseRank

        //private RelayCommand _increaseRankCommand;

        ///// <summary>
        ///// Increases the rank of the selected rule
        ///// </summary>
        //public RelayCommand IncreaseRankCommand
        //{
        //    get
        //    {
        //        if (_increaseRankCommand == null)
        //        {
        //            _increaseRankCommand = new RelayCommand(
        //                p => IncreaseRank_Executed(),
        //                p => IncreaseRank_CanExecute())
        //            {
        //                SmallIcon = ImageResources.RulePriorities_IncreaseRank,
        //                FriendlyName = Message.RulePriorities_IncreaseRank,
        //                FriendlyDescription = Message.RulePriorities_IncreaseRank_Description,
        //                DisabledReason = Message.RulePriorities_DisabledReason
        //            };
        //            base.ViewModelCommands.Add(_increaseRankCommand);
        //        }
        //        return _increaseRankCommand;
        //    }
        //}

        ////can't increase rank if no rank selected or is top of the list
        //private Boolean IncreaseRank_CanExecute()
        //{
        //    if (SelectedRule != null)
        //    {
        //        //Check rule above isn't static
        //        Int32 currentIndex = RulesList.IndexOf(SelectedRule);
        //        if ((currentIndex - 1) >= 0 &&
        //            (RulesList[currentIndex - 1] == null || RulesList[currentIndex - 1].IsStaticPriority))
        //        {
        //            return false;
        //        }
        //        return (SelectedRule.Rank != 1 && !SelectedRule.IsStaticPriority);
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        //private void IncreaseRank_Executed()
        //{
        //    //get previous rule
        //    PriorityRule previousSelectedRule = SelectedRule;
        //    Int32 currentIndex = RulesList.IndexOf(SelectedRule);
        //    //switch selected rule with the one above
        //    MoveRanks(RulesList[currentIndex], RulesList[currentIndex - 1]);
        //    //reselect the rule
        //    this.SelectedRule = previousSelectedRule;
        //}

        //#endregion

        //#region DecreaseRank

        //private RelayCommand _decreaseRankCommand;

        ///// <summary>
        ///// Decreases the rank of the selected rule
        ///// </summary>
        //public RelayCommand DecreaseRankCommand
        //{
        //    get
        //    {
        //        if (_decreaseRankCommand == null)
        //        {
        //            _decreaseRankCommand = new RelayCommand(
        //                p => DecreaseRank_Executed(),
        //                p => DecreaseRank_CanExecute())
        //            {
        //                SmallIcon = ImageResources.RulePriorities_DecreaseRank,
        //                FriendlyName = Message.RulePriorities_DecreaseRank,
        //                FriendlyDescription = Message.RulePriorities_DecreaseRank_Description,
        //                DisabledReason = Message.RulePriorities_DisabledReason
        //            };
        //            base.ViewModelCommands.Add(_decreaseRankCommand);
        //        }
        //        return _decreaseRankCommand;
        //    }
        //}

        ////can't decrease rank if no rank selected or is bottom of the list
        //private Boolean DecreaseRank_CanExecute()
        //{
        //    if (SelectedRule != null)
        //    {
        //        Int32 currentIndex = RulesList.IndexOf(SelectedRule);
        //        //Check rule belove isn't static
        //        if ((currentIndex + 1) < RulesList.Count
        //            && RulesList[currentIndex + 1].IsStaticPriority)
        //        {
        //            return false;
        //        }
        //        return (SelectedRule.Rank != RulesList.Count && !SelectedRule.IsStaticPriority);
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        //private void DecreaseRank_Executed()
        //{
        //    //get previous rule
        //    PriorityRule previousSelectedRule = SelectedRule;
        //    Int32 currentIndex = RulesList.IndexOf(SelectedRule);
        //    //switch selected rule with the one below
        //    MoveRanks(RulesList[currentIndex], RulesList[currentIndex + 1]);
        //    //reselect the rule
        //    this.SelectedRule = previousSelectedRule;
        //}



        //#endregion

        #endregion

        #region Methods

        //public void UpdateRanks()
        //{
        //    //reset the ranks to how they are in the list
        //    RulesList[3].Rank = 4;
        //    RulesList[4].Rank = 5;
        //    RulesList[5].Rank = 6;
        //    RulesList[6].Rank = 7;
        //}

        //public void MoveRanks(PriorityRule droppedData, PriorityRule target)
        //{
        //    Int32 removedIdx = RulesList.IndexOf(droppedData);
        //    Int32 targetIdx = RulesList.IndexOf(target);

        //    //if moved item comes before the target move it up else move it down if not equal
        //    if (removedIdx < targetIdx)
        //    {
        //        RulesList.Insert(targetIdx + 1, droppedData);
        //        RulesList.RemoveAt(removedIdx);
        //    }
        //    else if (removedIdx > targetIdx)
        //    {
        //        Int32 remIdx = removedIdx + 1;
        //        if (RulesList.Count + 1 > remIdx)
        //        {
        //            RulesList.Insert(targetIdx, droppedData);
        //            RulesList.RemoveAt(remIdx);
        //        }
        //    }
        //    //if equal no changes happen and the rule is just selected
        //    //update ranks
        //    UpdateRanks();
        //}

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.SelectedRule = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    #region PriorityRule
    //class to store the details about the rules in the RulesList
    public class PriorityRule : INotifyPropertyChanged
    {
        #region Fields

        String _ruleName;
        Byte _rank;
        PriorityType _ruleType;

        #endregion

        #region Binding Properties

        public static readonly PropertyPath RankProperty = WpfHelper.GetPropertyPath<PriorityRule>(p => p.Rank);
        public static readonly PropertyPath RuleNameProperty = WpfHelper.GetPropertyPath<PriorityRule>(p => p.RuleName);
        public static readonly PropertyPath IsStaticPriorityProperty = WpfHelper.GetPropertyPath<PriorityRule>(p => p.IsStaticPriority);

        #endregion

        #region Properties

        /// <summary>
        /// Property for the Rule's Name
        /// </summary>
        public String RuleName
        {
            get { return _ruleName; }
            set
            {
                _ruleName = value;
                OnPropertyChanged(RuleNameProperty);
            }
        }

        /// <summary>
        /// Property for the Rule's Rank
        /// </summary>
        public Byte Rank
        {
            get { return _rank; }
            set
            {
                _rank = value;
                OnPropertyChanged(RankProperty);
            }
        }

        /// <summary>
        /// Property for the Rule's Type
        /// </summary>
        public PriorityType RuleType
        {
            get { return _ruleType; }
            set
            {
                _ruleType = value;
            }
        }

        /// <summary>
        /// Returns whether the rule is a static priority and so can't be changed
        /// </summary>
        public Boolean IsStaticPriority
        {
            get
            {
                return true;
                //return (this.RuleType == PriorityType.DistributionRules ||
                //    this.RuleType == PriorityType.LocalProductRules ||
                //    this.RuleType == PriorityType.InheritanceRules);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="rank"></param>
        /// <param name="type"></param>
        public PriorityRule(String name, Byte rank, PriorityType type)
        {
            RuleName = name;
            Rank = rank;
            RuleType = type;
        }

        #endregion

        #region Methods

        internal PriorityRule Clone()
        {
            PriorityRule ret = new PriorityRule(this.RuleName, this.Rank, this._ruleType);
            return ret;
        }

        public override string ToString()
        {
            return this.RuleName;
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(PropertyPath property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }
    #endregion

    #region Enum

    //Enum for the type of rule
    public enum PriorityType
    {
        InheritanceRules,
        LocalProductRules,
        FamilyRules,
        ProductRules,
        DistributionRules
    };

    #endregion
}

