﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0)
// L.Ineson 
//  Created.
// V8-26408 : L.Ineson
//  Implemented properly.
// V8-26538 : L.Ineson
//  Corrected assembly height width and depth to readonly
// V8-26472 : A.Probyn ~ Updated reference to ColumnLayoutManager Constructor
// V8-27515 : L.Ineson
//  Double clicking a fixture now opens the planogram info screen.
// V8-28040 : A.Probyn
//  ~ Added code to call ApplyFiltersIntoGrid after column set is applied to the grid.
// V8-26597 : A.Probyn
//  ~ Corrected the implementation code that has been added which was stopping the grid columns from being loaded via the custom columns.
// V8-27900 : L.ineson
//  Added export to excel to column context menu.
// V8-26646 : A.Probyn
//  Corrected UpdateColumnSet to ensure a sort is applied.
#endregion
#region Version History: (CCM 8.2.0)
//V8-30958 : L.Ineson
//  Updated after column manager changes.
// V8-30991 : L.Ineson
//  Added layout factories for bay and assembly view types.
#endregion
#region Version History: (CCM 8.3.0)
// V8-32081 : N.Haywood
//  Added dragging from one fixture list to another plan
// V8-32331 : L.Ineson
//  Removed dragging preview handler to use datagrid version instead.
#endregion
#endregion

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Interaction logic for FixtureListPlanDocumentView.xaml
    /// </summary>
    public sealed partial class FixtureListPlanDocumentView : UserControl, IPlanDocumentView
    {
        #region Fields

        const String _screenKey = "FixtureListPlanDocument";
        public static readonly String RemoveRowCommandKey = "RemoveRowCommand";
        public static readonly String EditRowCommandKey = "EditRowCommand";

        private ColumnLayoutManager _columnManager;

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(FixtureListPlanDocument), typeof(FixtureListPlanDocumentView),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Called whenever the viewmodel context changes
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            FixtureListPlanDocumentView senderControl = (FixtureListPlanDocumentView)obj;

            if (e.OldValue != null)
            {
                FixtureListPlanDocument oldModel = (FixtureListPlanDocument)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;

                senderControl.Resources.Remove(RemoveRowCommandKey);
                senderControl.Resources.Remove(EditRowCommandKey);
            }

            if (e.NewValue != null)
            {
                FixtureListPlanDocument newModel = (FixtureListPlanDocument)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;

                senderControl.Resources.Add(RemoveRowCommandKey, newModel.RemoveRowCommand);
                senderControl.Resources.Add(EditRowCommandKey, newModel.EditRowCommand);
            }
        }

        /// <summary>
        /// Gets the viewmodel controller for this view.
        /// </summary>
        public FixtureListPlanDocument ViewModel
        {
            get { return (FixtureListPlanDocument)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }


        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public FixtureListPlanDocumentView(FixtureListPlanDocument document)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = document;

            this.Loaded += FixtureListPlanDocumentView_Loaded;
           

        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///Carries out intial load actions.
        /// </summary>
        private void FixtureListPlanDocumentView_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= FixtureListPlanDocumentView_Loaded;

            //call update columnset to create the right manager for the right view.
            UpdateColumnSet();

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }
        
        /// <summary>
        /// Called whenever a property changes on the viewmodel.
        /// </summary>
        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == FixtureListPlanDocument.ViewTypeProperty.Path)
            {
                UpdateColumnSet();
            }
        }

        /// <summary>
        /// Called whenever a grid row is double clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xFixtureListGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            IPlanItem item = e.RowItem as IPlanItem;
            if (item != null)
            {
                if (item.PlanItemType == PlanItemType.Fixture)
                {
                    MainPageCommands.ShowPlanogramProperties.Execute();
                }
                else
                {
                    MainPageCommands.ShowSelectedItemProperties.Execute();
                }
            }
        }

        /// <summary>
        /// Called whenever the column layout manager columns are about to change.
        /// </summary>
        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //prefix the delete column.
            columnSet.Insert(0, new DataGridExtendedTemplateColumn
                    {
                        CellTemplate = this.Resources["FixtureList_DTButtonsCol"] as DataTemplate,
                        CanUserFilter = false,
                        CanUserSort = false,
                        CanUserReorder = false,
                        CanUserHide = false,
                        CanUserResize = false,
                        CanUserFreeze = false,
                        ColumnCellAlignment = HorizontalAlignment.Stretch
                    });

            this.xFixtureListGrid.FrozenColumnCount = 1;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Recreates the column set.
        /// </summary>
        private void UpdateColumnSet()
        {
            //if we have a column manager already then clear it off
            ClearColumnManager();

            if (this.ViewModel == null) return;

            //create a new one for the view type
            IColumnLayoutFactory layoutFactory = null;
            switch (this.ViewModel.ViewType)
            {
                case FixtureListViewType.Components:
                    layoutFactory = new PlanogramComponentColumnLayoutFactory(typeof(PlanogramComponentView), this.ViewModel.Planogram.Model);
                    break;

                case FixtureListViewType.Assemblies:
                    layoutFactory = new PlanogramAssemblyColumnLayoutFactory(typeof(PlanogramAssemblyView), this.ViewModel.Planogram.Model);
                    break;

                case FixtureListViewType.Bays:
                    layoutFactory = new PlanogramFixtureColumnLayoutFactory(typeof(PlanogramFixtureView), this.ViewModel.Planogram.Model);
                    break;
            }


            _columnManager = new ColumnLayoutManager(layoutFactory, this.ViewModel.Planogram.DisplayUnits, _screenKey);
            _columnManager.CalculatedColumnPath = "CalculatedValueResolver";
            _columnManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            _columnManager.AttachDataGrid(this.xFixtureListGrid);
        }

        /// <summary>
        /// Clears the existing column manager
        /// </summary>
        private void ClearColumnManager()
        {
            if (_columnManager == null) return;

            _columnManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
            _columnManager.ClearGrid();
            _columnManager.Dispose();
            _columnManager = null;
        }

        #endregion

        #region IPlanDocumentView Members

        public IPlanDocument GetIPlanDocument()
        {
            return this.ViewModel as IPlanDocument;
        }

        public void Dispose()
        {
            ClearColumnManager();

            this.ViewModel = null;
        }

        #endregion

        

    }
}
