﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26956 : L.Luong
//      Created

#endregion

#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Windows;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Controls.Wpf.TreeMap;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.ConsumerDecisionTree
{
    /// <summary>
    /// Diagram node content depicting a product hierarchy unit
    /// </summary>
    public sealed class ConsumerDecisionTreePlanNode : TreeMapBlock, IDisposable
    {
        #region Events

        #region NodeChildrenChanged

        public static readonly RoutedEvent NodeChildrenChangedEvent =
         EventManager.RegisterRoutedEvent("NodeChildrenChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ConsumerDecisionTreePlanNode));

        public event RoutedEventHandler NodeChildrenChanged
        {
            add { AddHandler(NodeChildrenChangedEvent, value); }
            remove { RemoveHandler(NodeChildrenChangedEvent, value); }
        }

        private void RaiseNodeChildrenChangedEvent()
        {
            RoutedEventArgs newEventArgs = new RoutedEventArgs(ConsumerDecisionTreePlanNode.NodeChildrenChangedEvent);
            RaiseEvent(newEventArgs);
        }

        #endregion

        #endregion

        #region Properties

        #region Node Context Property

        public static readonly DependencyProperty SourceNodeProperty =
            DependencyProperty.Register("SourceNode", typeof(PlanogramConsumerDecisionTreeNode), typeof(ConsumerDecisionTreePlanNode),
            new PropertyMetadata(OnSourceNodePropertyChanged));
        /// <summary>
        /// Gets/ Sets the node content context
        /// </summary>
        public PlanogramConsumerDecisionTreeNode SourceNode
        {
            get { return (PlanogramConsumerDecisionTreeNode)GetValue(SourceNodeProperty); }
            set { SetValue(SourceNodeProperty, value); }
        }

        public static void OnSourceNodePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ConsumerDecisionTreePlanNode senderControl = sender as ConsumerDecisionTreePlanNode;
            if (e.OldValue != null)
            {
                PlanogramConsumerDecisionTreeNode oldModel = e.OldValue as PlanogramConsumerDecisionTreeNode;
                oldModel.PropertyChanged -= senderControl.NodeContext_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                PlanogramConsumerDecisionTreeNode newModel = e.NewValue as PlanogramConsumerDecisionTreeNode;
                newModel.PropertyChanged += senderControl.NodeContext_PropertyChanged;
            }
        }

        #endregion

        /// <summary>
        /// Percentage Node Contribution Property
        /// </summary>
        public static readonly DependencyProperty PercentageNodeContributionProperty =
            DependencyProperty.Register("PercentageNodeContribution", typeof(Double), typeof(ConsumerDecisionTreePlanNode));
        public Double PercentageNodeContribution
        {
            get { return (Double)GetValue(PercentageNodeContributionProperty); }
            set { SetValue(PercentageNodeContributionProperty, value); }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Static constructor
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Style key override")]
        static ConsumerDecisionTreePlanNode()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(ConsumerDecisionTreePlanNode), new FrameworkPropertyMetadata(typeof(ConsumerDecisionTreePlanNode)));
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitContext"></param>
        public ConsumerDecisionTreePlanNode(PlanogramConsumerDecisionTreeNode sourceNode)
            : base()
        {
            //set source node
            this.SourceNode = sourceNode;

            //Subscribe to size changed
            this.SizeChanged += new SizeChangedEventHandler(CdtMaintenanceNode_SizeChanged);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to property changes on the node context
        /// </summary>
        private void NodeContext_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == PlanogramConsumerDecisionTreeNode.NameProperty.Name)
            {
                CdtMaintenanceNode_SizeChanged(null, null);
            }
        }

        private void CdtMaintenanceNode_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (this.SourceNode != null)
            {
                this.ToolTip = String.Format(CultureInfo.CurrentCulture,
                    "{0}{1}Space %: {2}",
                   this.SourceNode.Name, Environment.NewLine,
                   this.SpacePercentage);
            }
        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean isDisposing)
        {
            if (!_isDisposed)
            {
                if (isDisposing)
                {
                    this.SourceNode = null;
                }

                _isDisposed = true;
            }
        }

        #endregion
    }
}
