﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26956 : L.Luong
//      Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using System.Collections.Specialized;
using Galleria.Framework.Planograms.Model;
using System.Collections.ObjectModel;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Collections;
using System.Diagnostics;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.ConsumerDecisionTree
{
    /// <summary>
    /// Represents a view of a PlanogramConsumerDecisionTreeNode for the ui
    /// </summary>
    public sealed class ConsumerDecisionTreeNodeViewModel : ViewModelObject
    {
        #region Event

        /// <summary>
        /// Notifies that a child has had its collection changed
        /// </summary>
        public event EventHandler<BulkCollectionChangedEventArgs> ChildCollectionChanged;

        /// <summary>
        /// Raises the child collection changed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NotifyChildCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            if (this.ChildCollectionChanged != null)
            {
                this.ChildCollectionChanged(sender, e);
            }
        }

        #endregion

        #region Fields

        private PlanogramConsumerDecisionTreeNode _node;
        private BulkObservableCollection<ConsumerDecisionTreeNodeViewModel> _childViews = new BulkObservableCollection<ConsumerDecisionTreeNodeViewModel>();
        private ReadOnlyBulkObservableCollection<ConsumerDecisionTreeNodeViewModel> _childViewsRO;

        private Dictionary<Int32, PlanogramConsumerDecisionTreeNode> _levelPathValues;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath NodeProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreeNodeViewModel>(p => p.Node);
        public static readonly PropertyPath ChildViewsProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreeNodeViewModel>(p => p.ChildViews);
        public static readonly PropertyPath LevelPathValuesProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreeNodeViewModel>(p => p.LevelPathValues);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the the source product group for this view
        /// </summary>
        public PlanogramConsumerDecisionTreeNode Node
        {
            get { return _node; }
        }

        /// <summary>
        /// Returns the collection of child views
        /// </summary>
        public ReadOnlyBulkObservableCollection<ConsumerDecisionTreeNodeViewModel> ChildViews
        {
            get
            {
                if (_childViewsRO == null)
                {
                    _childViewsRO = new ReadOnlyBulkObservableCollection<ConsumerDecisionTreeNodeViewModel>(_childViews);
                }
                return _childViewsRO;
            }
        }

        /// <summary>
        /// Returns the dictionary collection of level path values
        /// </summary>
        public Dictionary<Int32, PlanogramConsumerDecisionTreeNode> LevelPathValues
        {
            get
            {
                if (_levelPathValues == null)
                {
                    _levelPathValues = ConstructLevelPath();
                }
                return _levelPathValues;
            }
            private set
            {
                _levelPathValues = value;
                OnPropertyChanged(LevelPathValuesProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ConsumerDecisionTreeNodeViewModel(PlanogramConsumerDecisionTreeNode sourceNode)
        {
            _childViews.BulkCollectionChanged += ChildViews_BulkCollectionChanged;

            _node = sourceNode;

            RecreateChildViews();

            _node.ChildList.BulkCollectionChanged += Node_ChildListBulkCollectionChanged;
        }


        #endregion

        #region Methods

        /// <summary>
        /// Constructs the level path dictionary for the source group
        /// </summary>
        /// <returns></returns>
        private Dictionary<Int32, PlanogramConsumerDecisionTreeNode> ConstructLevelPath()
        {
            Dictionary<Int32, PlanogramConsumerDecisionTreeNode> levelPathDict = new Dictionary<Int32, PlanogramConsumerDecisionTreeNode>();

            PlanogramConsumerDecisionTreeNode node = _node;
            PlanogramConsumerDecisionTree structure = node.ParentPlanogramConsumerDecisionTree;

            List<PlanogramConsumerDecisionTreeNode> pathPartList = node.FetchParentPath();

            //construct the dictionary
            Int32 currentLevelNo = 0;
            foreach (PlanogramConsumerDecisionTreeNode parentNode in pathPartList)
            {
                levelPathDict.Add(currentLevelNo, parentNode);
                currentLevelNo++;
            }

            //add this unit to the dictionary
            levelPathDict.Add(currentLevelNo, _node);

            return levelPathDict;
        }

        /// <summary>
        /// Recreates all the child unit views
        /// </summary>
        private void RecreateChildViews()
        {
            //_childViews.RemoveAllItems();
            if (_childViews.Count > 0)
            {
                _childViews.Clear();
            }

            foreach (PlanogramConsumerDecisionTreeNode childUnit in this.Node.ChildList)
            {
                ConsumerDecisionTreeNodeViewModel childView = new ConsumerDecisionTreeNodeViewModel(childUnit);
                _childViews.Add(childView);
            }
        }

        /// <summary>
        /// Returns a list  of this unit and  all child units below this one
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ConsumerDecisionTreeNodeViewModel> GetAllChildUnits()
        {
            IEnumerable<ConsumerDecisionTreeNodeViewModel> returnList =
                new List<ConsumerDecisionTreeNodeViewModel>() { this };

            foreach (ConsumerDecisionTreeNodeViewModel child in this.ChildViews)
            {
                returnList = returnList.Union(child.GetAllChildUnits());
            }

            return returnList;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to changes in the child views collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChildViews_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            Debug.WriteLine(this.Node.ToString() + ": ChildViews_BulkCollectionChanged " + e.Action.ToString());

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (ConsumerDecisionTreeNodeViewModel view in e.ChangedItems)
                    {
                        view.ChildCollectionChanged += ChildView_ChildCollectionChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (ConsumerDecisionTreeNodeViewModel view in e.ChangedItems)
                    {
                        view.ChildCollectionChanged -= ChildView_ChildCollectionChanged;
                        view.Dispose();
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    if (e.ChangedItems != null)
                    {
                        foreach (ConsumerDecisionTreeNodeViewModel view in e.ChangedItems)
                        {
                            view.ChildCollectionChanged -= ChildView_ChildCollectionChanged;
                            view.Dispose();
                        }

                        IEnumerable<ConsumerDecisionTreeNodeViewModel> senderCollection = (IEnumerable<ConsumerDecisionTreeNodeViewModel>)sender;
                        foreach (ConsumerDecisionTreeNodeViewModel view in senderCollection)
                        {
                            view.ChildCollectionChanged += ChildView_ChildCollectionChanged;
                        }
                    }
                    else
                    {
                        throw new NotSupportedException("Would cause handler memory leak");
                    }
                    break;
            }

            NotifyChildCollectionChanged(this, e);
        }

        /// <summary>
        /// Responds to a collection change of a child unit view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChildView_ChildCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            //pass the notification up
            NotifyChildCollectionChanged(sender, e);
        }

        /// <summary>
        /// Responds to changes in the source group's child list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Node_ChildListBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            Debug.WriteLine(this.Node.ToString() + ": Node_ChildListBulkCollectionChanged " + e.Action.ToString());

            //update the child views collection to be in synch
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (PlanogramConsumerDecisionTreeNode childUnit in e.ChangedItems)
                    {
                        //add in a new child view
                        ConsumerDecisionTreeNodeViewModel childView = new ConsumerDecisionTreeNodeViewModel(childUnit);
                        _childViews.Add(childView);
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (PlanogramConsumerDecisionTreeNode childUnit in e.ChangedItems)
                    {
                        //remove the view
                        ConsumerDecisionTreeNodeViewModel removeView =
                            this.ChildViews.FirstOrDefault(v => v.Node == childUnit);

                        if (removeView != null)
                        {
                            _childViews.Remove(removeView);
                        }

                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    RecreateChildViews();
                    break;
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //remove all child unit views so that they get disposed of correctly.
                    //_childViews.RemoveAllItems();

                    //dettach from the current unit
                    _node.ChildList.BulkCollectionChanged -= Node_ChildListBulkCollectionChanged;
                    _childViews.BulkCollectionChanged -= ChildViews_BulkCollectionChanged;
                    _node = null;

                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
