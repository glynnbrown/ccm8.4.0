﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26956 : L.Luong
//      Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Windows;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.ConsumerDecisionTree
{
    public sealed class ConsumerDecisionTreePlanNodeEditViewModel : ViewModelAttachedControlObject<ConsumerDecisionTreePlanNodeEditWindow>, IDataErrorInfo
    {
        #region Fields
        private PlanogramConsumerDecisionTree _structure;
        private PlanogramConsumerDecisionTreeNode _selectedNode;

        private BulkObservableCollection<PlanogramProduct> _products = new BulkObservableCollection<PlanogramProduct>();
        private ReadOnlyBulkObservableCollection<PlanogramProduct> _productsRO;
        private PlanogramProductList _productList;
        private List<String> _siblingNames = new List<String>();
        private String _editNodeName;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath SelectedNodeProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanNodeEditViewModel>(p => p.SelectedNode);
        public static readonly PropertyPath ProductsProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanNodeEditViewModel>(p => p.Products);

        public static readonly PropertyPath EditNodeNameProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanNodeEditViewModel>(p => p.EditNodeName);
        public static readonly PropertyPath ApplyCommandProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanNodeEditViewModel>(p => p.ApplyCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the selected unit context to edit.
        /// </summary>
        public PlanogramConsumerDecisionTreeNode SelectedNode
        {
            get { return _selectedNode; }
            set
            {
                _selectedNode = value;
                OnPropertyChanged(SelectedNodeProperty);
                OnSelectedNodeChanged(value);
            }
        }

        /// <summary>
        /// Returns the collection of products assigned to the node
        /// or its children
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramProduct> Products
        {
            get
            {
                if (_productsRO == null)
                {
                    _productsRO = new ReadOnlyBulkObservableCollection<PlanogramProduct>(_products);
                }
                return _productsRO;
            }
        }

        public String EditNodeName
        {
            get
            {
                return _editNodeName;
            }
            set
            {
                _editNodeName = value;
                OnPropertyChanged(EditNodeNameProperty);
            }
        }

        #endregion

        #region Commands

        private RelayCommand _applyCommand;

        /// <summary>
        /// Shows the edit window for the selected node
        /// </summary>
        public RelayCommand ApplyCommand
        {
            get
            {
                if (_applyCommand == null)
                {
                    _applyCommand = new RelayCommand(
                        p => ApplyCommand_Executed(),
                        p => ApplyCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_ApplyAndClose,
                    };
                    base.ViewModelCommands.Add(_applyCommand);
                }
                return _applyCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ApplyCommand_CanExecute()
        {
            if (_siblingNames.Contains(_editNodeName.TrimEnd(' ').ToUpperInvariant()))
            {
                _applyCommand.DisabledReason = Message.ConsumerDecisionTreeDocument_DuplicateName;
                return false;
            }

            return true;
        }

        private void ApplyCommand_Executed()
        {
            this.SelectedNode.Name = _editNodeName;

            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ConsumerDecisionTreePlanNodeEditViewModel(PlanogramConsumerDecisionTreeNode selectedNode, PlanogramProductList planogramProductList)
        {
            _structure = selectedNode.ParentPlanogramConsumerDecisionTree;
            _productList = planogramProductList;
            this.SelectedNode = selectedNode;
            _editNodeName = this.SelectedNode.Name;
            if (this.SelectedNode.ParentNode != null)
            {
                foreach (PlanogramConsumerDecisionTreeNode node in this.SelectedNode.ParentNode.ChildList)
                {
                    if (this.SelectedNode != node)
                    {
                        _siblingNames.Add(node.Name.ToUpperInvariant());
                    }
                }
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change of selected node
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedNodeChanged(PlanogramConsumerDecisionTreeNode newValue)
        {
            _products.Clear();

            if (newValue != null)
            {

                IEnumerable<PlanogramConsumerDecisionTreeNodeProduct> nodeProducts =
                this.SelectedNode.FetchAllChildPlanogramConsumerDecisionTreeNodeProducts();

                //[TODO] ProductList.FetchByIds
                foreach (PlanogramConsumerDecisionTreeNodeProduct nodeProduct in nodeProducts)
                {
                    _products.Add(_productList.FindById(nodeProduct.PlanogramProductId));
                }
            }

        }

        #endregion

        #region IDataErrorInfo

        String IDataErrorInfo.Error
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Implementation of the IDataErrorInfo interface
        /// </summary>
        /// <param name="columnName">Property name</param>
        /// <returns>String containing error</returns>
        String IDataErrorInfo.this[String columnName]
        {
            get
            {
                String result = null;
                List<String> siblingNames = new List<String>();
                if (columnName == EditNodeNameProperty.Path)
                {
                    if (_siblingNames.Contains(_editNodeName.TrimEnd(' ').ToUpperInvariant()))
                    {
                        result = String.Format(CultureInfo.CurrentCulture, Message.ConsumerDecisionTreeModel_DuplicateNodeName, SelectedNode.ParentNode.Name);
                    }
                }

                return result; // return result
            }
        }
        #endregion

        #region IDisposable

        /// <summary>
        /// IDisposable implementation
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
