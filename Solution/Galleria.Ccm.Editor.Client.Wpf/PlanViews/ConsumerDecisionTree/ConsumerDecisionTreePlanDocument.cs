﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// V8-26956 : L.Luong
//      Created
// V8-27132 : A.Kuszyk
//  Added LoadFromRepository command.
// V8-27825 : L.Ineson
//  Added document freezing
// V8-27500 : A.Kuszyk
//  Added warning prompt to remove level command.
// V8-28409 : A.Kuszyk
//  Added warning prompt for remove nodes and removing levels with child nodes.
// V8-28444 : M.Shelley
//  Fix failed unit test - added code to the RemoveNode_Executed method to check
//  if it is running under a unit test environment
#endregion

#region Version History: (CCM 8.0.1)
// V8-27554 : I.George
//  Added SmallIcon property to commands where they are missing.
// V8-28786 : I.George
//  Updated SmallIcon property to use 16 x 16 icon size
#endregion

#region Version History: (CCM 8.2.0)
// V8-30737 : M.Shelley
//  Added new metadata fields for selected CDT nodes
#endregion

#region Version History: (CCM 8.3.0)
// V8-31832 : L.Ineson
//  Amended to use plangoramproductviews so support for calculated columns could be added.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Csla;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.ConsumerDecisionTree
{
    public sealed class ConsumerDecisionTreePlanDocument : PlanDocument<ConsumerDecisionTreePlanDocumentView>
    {
        #region Fields

        private Object _selectedLevelId;
        private BulkObservableCollection<PlanogramConsumerDecisionTreeNode> _selectedNodes = new BulkObservableCollection<PlanogramConsumerDecisionTreeNode>();

        //Products
        private ObservableCollection<PlanogramProductView> _selectedUnassignedProducts = new ObservableCollection<PlanogramProductView>();
        private BulkObservableCollection<PlanogramProductView> _unassignedProducts = new BulkObservableCollection<PlanogramProductView>();
        private ReadOnlyBulkObservableCollection<PlanogramProductView> _unassignedProductsRO;

        private ObservableCollection<PlanogramProductView> _selectedAssignedProducts = new ObservableCollection<PlanogramProductView>();
        private BulkObservableCollection<PlanogramProductView> _selectedNodeProducts = new BulkObservableCollection<PlanogramProductView>();
        private ReadOnlyBulkObservableCollection<PlanogramProductView> _selectedNodeProductsRO;
        private DisplayUnitOfMeasureCollection _displayUnits;
        BulkObservableCollection<ConsumerDecisionTreePlanDocumentPerformanceItem> _metaPropertyItemsValues = 
            new BulkObservableCollection<ConsumerDecisionTreePlanDocumentPerformanceItem>();

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath RootNodeProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.RootNode);
        public static readonly PropertyPath ConsumerDecisionTreeModelProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.ConsumerDecisionTreeModel);
        public static readonly PropertyPath SelectedLevelIdProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.SelectedLevelId);
        public static readonly PropertyPath SelectedNodesProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.SelectedNodes);
        public static readonly PropertyPath SelectedNodeProductsProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.SelectedNodeProducts);
        public static readonly PropertyPath SelectedUnassignedProductsProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.SelectedUnassignedProducts);
        public static readonly PropertyPath UnassignedProductsProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.UnassignedProducts);
        public static readonly PropertyPath SelectedAssignedProductsProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.SelectedAssignedProducts);
        public static readonly PropertyPath ProductsProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.Products);
        public static readonly PropertyPath MetaCountOfProductsProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.MetaCountOfProducts);
        public static readonly PropertyPath MetaCountOfRecommendedProductsProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.MetaCountOfRecommendedProducts);
        public static readonly PropertyPath MetaCountOfPlacedProductsProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.MetaCountOfPlacedProducts);
        public static readonly PropertyPath MetaLinearSpaceAllocatedToProductsOnPlanogramProperty =
                    WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.MetaLinearSpaceAllocatedToProductsOnPlanogram);
        public static readonly PropertyPath MetaAreaSpaceAllocatedToProductsOnPlanogramProperty =
                    WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.MetaAreaSpaceAllocatedToProductsOnPlanogram);
        public static readonly PropertyPath MetaVolumetricSpaceAllocatedToProductsOnPlanogramProperty =
                    WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.MetaVolumetricSpaceAllocatedToProductsOnPlanogram);
        public static readonly PropertyPath DisplayUnitsProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.DisplayUnits);
        public static readonly PropertyPath MetaPropertyItemValuesProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.MetaPropertyItemValues);

        //commands
        public static readonly PropertyPath LoadFromRepositoryCommandProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.LoadFromRepositoryCommand);
        public static readonly PropertyPath AddSelectedProductsCommandProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.AddSelectedProductsCommand);
        public static readonly PropertyPath RemoveSelectedProductsCommandProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.RemoveSelectedProductsCommand);
        public static readonly PropertyPath AddAllProductsCommandProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.AddAllProductsCommand);
        public static readonly PropertyPath RemoveAllProductsCommandProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.RemoveAllProductsCommand);
        public static PropertyPath EditNodeCommandProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.EditNodeCommand);
        public static PropertyPath AddNodeCommandProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.AddNodeCommand);
        public static PropertyPath RemoveNodeCommandProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.RemoveNodeCommand);
        public static PropertyPath SplitNodeCommandProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.SplitNodeCommand);
        public static PropertyPath RemoveLevelCommandProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.RemoveLevelCommand);
        public static PropertyPath AddLevelCommandProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.AddLevelCommand);
        public static PropertyPath MoreSplitPropertiesCommandProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePlanDocument>(p => p.MoreSplitPropertiesCommand);

        #endregion

        #region Properties

        /// <summary>
        /// The document type.
        /// </summary>
        public override DocumentType DocumentType
        {
            get { return DocumentType.ConsumerDecisionTree; }
        }

        /// <summary>
        /// The document title
        /// </summary>
        public override String Title
        {
            get { return DocumentTypeHelper.FriendlyNames[DocumentType.ConsumerDecisionTree]; }
        }

        /// <summary>
        /// The ConsumerDecisionTree model this document represents.
        /// </summary>
        //TODO: Get rid
        public PlanogramConsumerDecisionTree ConsumerDecisionTreeModel
        {
            get
            {
                if (Planogram == null || Planogram.Model == null) return null;
                return this.Planogram.Model.ConsumerDecisionTree;
            }
        }

        //TODO: Get rid
        public PlanogramProductList Products
        {
            get { return this.Planogram.Model.Products; }
        }

        public PlanogramConsumerDecisionTreeNode RootNode
        {
            get
            {
                PlanogramConsumerDecisionTree cdt = this.ConsumerDecisionTreeModel;
                if (cdt == null) return null;

                return cdt.RootNode;
            }
            //private set
            //{
            //    PlanogramConsumerDecisionTreeNode oldValue = _rootNode;

            //    _rootNode = value;
            //    OnPropertyChanged(RootNodeProperty);
            //    OnRootNodeChanged(value);
            //}
        }

        /// <summary>
        /// Returns the collection of selected products
        /// </summary>
        public BulkObservableCollection<PlanogramConsumerDecisionTreeNode> SelectedNodes
        {
            get { return _selectedNodes; }
        }

        /// <summary>
        /// Gets/Sets the id of the selected level
        /// </summary>
        public Object SelectedLevelId
        {
            get { return _selectedLevelId; }
            set { _selectedLevelId = value; }
        }

        /// <summary>
        /// Returns the collection of selected unassigned products
        /// </summary>
        public ObservableCollection<PlanogramProductView> SelectedUnassignedProducts
        {
            get { return _selectedUnassignedProducts; }
        }

        /// <summary>
        /// Returns the collection of unassigned products
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramProductView> UnassignedProducts
        {
            get
            {
                if (_unassignedProductsRO == null)
                {
                    _unassignedProductsRO = new ReadOnlyBulkObservableCollection<PlanogramProductView>(_unassignedProducts);
                }
                return _unassignedProductsRO;
            }
        }

        /// <summary>
        /// Returns the collection of selected assigned products
        /// </summary>
        public ObservableCollection<PlanogramProductView> SelectedAssignedProducts
        {
            get { return _selectedAssignedProducts; }
        }

        /// <summary>
        /// Returns the collection of products linked to the selected nodes
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramProductView> SelectedNodeProducts
        {
            get
            {
                if (_selectedNodeProductsRO == null)
                {
                    _selectedNodeProductsRO = new ReadOnlyBulkObservableCollection<PlanogramProductView>(_selectedNodeProducts);
                }

                return _selectedNodeProductsRO;
            }
        }

        public Int32? MetaCountOfProducts
        {
            get
            {
                return (SelectedNodes != null && SelectedNodes.Count > 0) ? SelectedNodes.FirstOrDefault().MetaCountOfProducts : 0;
            }
        }

        public Int32? MetaCountOfPlacedProducts
        {
            get
            {
                return (SelectedNodes != null && SelectedNodes.Count > 0) ? SelectedNodes.FirstOrDefault().MetaCountOfProductPlacedOnPlanogram : 0;
            }
        }

        public Int32? MetaCountOfRecommendedProducts
        {
            get
            {
                return (SelectedNodes != null && SelectedNodes.Count > 0) ? SelectedNodes.FirstOrDefault().MetaCountOfProductRecommendedInAssortment : 0;
            }
        }

        public Single? MetaLinearSpaceAllocatedToProductsOnPlanogram
        {
            get 
            { 
                return (SelectedNodes != null && SelectedNodes.Count > 0) ? SelectedNodes.FirstOrDefault().MetaLinearSpaceAllocatedToProductsOnPlanogram : 0F;
            }
        }

        public Single? MetaAreaSpaceAllocatedToProductsOnPlanogram
        {
            get
            {
                return (SelectedNodes != null && SelectedNodes.Count > 0) ? SelectedNodes.FirstOrDefault().MetaAreaSpaceAllocatedToProductsOnPlanogram : 0F;
            }
        }

        public Single? MetaVolumetricSpaceAllocatedToProductsOnPlanogram
        {
            get
            {
                return (SelectedNodes != null && SelectedNodes.Count > 0) ? SelectedNodes.FirstOrDefault().MetaVolumetricSpaceAllocatedToProductsOnPlanogram : 0F;
            }
        }

        public ObservableCollection<ConsumerDecisionTreePlanDocumentPerformanceItem> MetaPropertyItemValues
        {
            get
            {
                FetchPerformanceValues();

                return _metaPropertyItemsValues;
            }
        }

        /// <summary>
        /// Returns the display unit of measure to use
        /// </summary>
        public DisplayUnitOfMeasureCollection DisplayUnits
        {
            get { return _displayUnits; }
            private set
            {
                _displayUnits = value;
                OnPropertyChanged(DisplayUnitsProperty);
            }
        }

        #endregion

        #region Events

        public event EventHandler MajorNodeChangeStarting;
        private void OnMajorNodeChangeStarting()
        {
            if (this.MajorNodeChangeStarting != null)
            {
                this.MajorNodeChangeStarting(this, EventArgs.Empty);
            }
        }

        public event EventHandler MajorNodeChangeComplete;
        private void OnMajorNodeChangeComplete()
        {
            if (this.MajorNodeChangeComplete != null)
            {
                this.MajorNodeChangeComplete(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public ConsumerDecisionTreePlanDocument(PlanControllerViewModel parentController)
            : base(parentController)
        {
            SetPlanEventHandlers(true);

            _displayUnits = DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(Planogram.Model);

            var rootNode = Planogram.Model.ConsumerDecisionTree.RootNode;
            if (rootNode.TotalProductCount == 0)
            {
                List<PlanogramConsumerDecisionTreeNodeProduct> newProducts = new List<PlanogramConsumerDecisionTreeNodeProduct>();
                foreach (PlanogramProduct product in this.Products)
                {
                    newProducts.Add(PlanogramConsumerDecisionTreeNodeProduct.NewPlanogramConsumerDecisionTreeNodeProduct(product.Id));
                }
                rootNode.Products.AddRange(newProducts);
            }
            ResetAvailableProducts();

            // Configure the Meta performance details

        }

        #endregion

        #region Commands

        #region LoadFromRepositoryCommand

        private RelayCommand _loadFromRepositoryCommand;

        /// <summary>
        /// LoadFromRepositorys a CDT from the repository into this Planogram.
        /// </summary>
        public RelayCommand LoadFromRepositoryCommand
        {
            get
            {
                if (_loadFromRepositoryCommand == null)
                {
                    _loadFromRepositoryCommand = new RelayCommand(
                        p => LoadFromRepository_Executed(),
                        p => LoadFromRepository_CanExecute())
                    {
                        FriendlyName = Message.ConsumerDecisionTreeDocument_LoadFromRepository,
                        FriendlyDescription = Message.ConsumerDecisionTreeDocument_LoadFromRepository_Description,
                        Icon = ImageResources.ConsumerDecisionTreeDocument_LoadFromRepository32,
                        SmallIcon = ImageResources.ConsumerDecisionTreeDocument_LoadFromRepository16
                    };
                    base.ViewModelCommands.Add(_loadFromRepositoryCommand);
                }
                return _loadFromRepositoryCommand;
            }
        }

        private Boolean LoadFromRepository_CanExecute()
        {
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.LoadFromRepositoryCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }

            return true;
        }

        private void LoadFromRepository_Executed()
        {
            var win = new GenericSingleItemSelectorWindow();
            win.ItemSource = ConsumerDecisionTreeInfoList.FetchByEntityId(App.ViewState.EntityId);
            win.SelectionMode = DataGridSelectionMode.Single;
            App.ShowWindow(win, true);
            Model.ConsumerDecisionTree cdt = null;

            if (win.DialogResult == true)
            {
                if (win.SelectedItems != null)
                {
                    ConsumerDecisionTreeInfo info = win.SelectedItems.Cast<ConsumerDecisionTreeInfo>().First();

                    try
                    {
                        cdt = Model.ConsumerDecisionTree.FetchById(info.Id);
                    }
                    catch (DataPortalException ex)
                    {
                        LocalHelper.RecordException(ex);

                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                            info.Name, OperationType.Open);

                        return;
                    }

                }
            }

            if (cdt == null) return;

            this.Planogram.Model.ConsumerDecisionTree.ReplaceConsumerDecisionTree(cdt);
            OnRootNodeChanged(this.RootNode);
        }

        #endregion

        #region AddNodeCommand

        private RelayCommand _addNodeCommand;

        /// <summary>
        /// Adds a new node underneath the selected nodes
        /// </summary>
        public RelayCommand AddNodeCommand
        {
            get
            {
                if (_addNodeCommand == null)
                {
                    _addNodeCommand = new RelayCommand(
                        p => AddNode_Executed(),
                        p => AddNode_CanExecute())
                    {
                        FriendlyName = Message.ConsumerDecisionTreeDocument_AddNode,
                        FriendlyDescription = Message.ConsumerDecisionTreeDocument_AddNode_Description,
                        Icon = ImageResources.CdtMaintenance_AddNode,
                        SmallIcon = ImageResources.CdtMaintenance_AddNode16
                    };
                    base.ViewModelCommands.Add(_addNodeCommand);
                }
                return _addNodeCommand;
            }
        }

        private Boolean AddNode_CanExecute()
        {
            //must have a node selected
            if (this.SelectedNodes.Count == 0)
            {
                return false;
            }
            return true;
        }

        private void AddNode_Executed()
        {
            base.ShowWaitCursor(true);

            List<PlanogramConsumerDecisionTreeNode> addToNodeList = this.SelectedNodes.ToList();

            //clear selected items
            this.SelectedNodes.Clear();

            List<PlanogramConsumerDecisionTreeNode> addedNodes = new List<PlanogramConsumerDecisionTreeNode>();

            foreach (PlanogramConsumerDecisionTreeNode parentNode in addToNodeList)
            {
                //add the node
                PlanogramConsumerDecisionTreeNode newNode = parentNode.AddNewChildNode();

                if (newNode != null)
                {
                    addedNodes.Add(newNode);
                }
            }

            this.SelectedNodes.AddRange(addedNodes);

            base.ShowWaitCursor(false);
        }

        #endregion

        #region RemoveNodeCommand

        private RelayCommand _removeNodeCommand;

        /// <summary>
        /// Removes the selected nodes
        /// </summary>
        public RelayCommand RemoveNodeCommand
        {
            get
            {
                if (_removeNodeCommand == null)
                {
                    _removeNodeCommand = new RelayCommand(
                        p => RemoveNode_Executed(),
                        p => RemoveNode_CanExecute())
                    {
                        FriendlyName = Message.ConsumerDecisionTreeDocument_RemoveNode,
                        FriendlyDescription = Message.ConsumerDecisionTreeDocument_RemoveNode_Description,
                        Icon = ImageResources.CdtMaintenance_RemoveNode,
                        SmallIcon = ImageResources.CdtMaintenance_RemoveNode_16
                    };
                    base.ViewModelCommands.Add(_removeNodeCommand);
                }
                return _removeNodeCommand;
            }
        }

        private Boolean RemoveNode_CanExecute()
        {
            //at least one node must be selected
            if (this.SelectedNodes.Count == 0)
            {
                this.RemoveNodeCommand.DisabledReason = Message.ConsumerDecisionTreeDocument_RemoveNode_DisabledNoItems;
                return false;
            }

            //if only a single node is selected it must not be the root
            if (this.SelectedNodes.Count == 1)
            {
                if (this.SelectedNodes[0] != null && this.SelectedNodes[0].IsRoot)
                {
                    this.RemoveNodeCommand.DisabledReason = Message.ConsumerDecisionTreeDocument_RemoveNode_DisabledRootNode;
                    return false;
                }
            }

            return true;
        }

        private void RemoveNode_Executed()
        {
            // Warn user that child levels could be removed.
            ModalMessage msg = new ModalMessage()
            {
                Header = Message.ConsumerDecisionTree_RemoveNodes,
                Description = Message.ConsumerDecisionTree_RemoveNodes_Description,
                Button1Content = Message.Generic_Continue,
                Button2Content = Message.Generic_Cancel,
                ButtonCount = 2,
                MessageIcon = ImageResources.Dialog_Warning
            };

            // Check if the code is running under a unit test environment
            if (Application.Current != null)
            {
                App.ShowWindow(msg, true);

                if (msg.Result != ModalMessageResult.Button1) return;
            }

            base.ShowWaitCursor(true);

            List<PlanogramConsumerDecisionTreeNode> removeNodeList = this.SelectedNodes.ToList();

            //clear selected items
            this.SelectedNodes.Clear();

            foreach (PlanogramConsumerDecisionTreeNode removeNode in removeNodeList)
            {
                if (removeNode != null)
                {
                    //Fetch levels
                    IEnumerable<PlanogramConsumerDecisionTreeLevel> levels = this.ConsumerDecisionTreeModel.FetchAllLevels();

                    //Find node level
                    PlanogramConsumerDecisionTreeLevel level = levels.FirstOrDefault(p => p.Id.Equals(removeNode.PlanogramConsumerDecisionTreeLevelId));

                    //Get all nodes at same level
                    IEnumerable<PlanogramConsumerDecisionTreeNode> levelNodes = this.RootNode.FetchAllChildNodes()
                            .Where(p => p.PlanogramConsumerDecisionTreeLevelId.Equals(removeNode.PlanogramConsumerDecisionTreeLevelId) && p != removeNode);

                    //Flag to update levels
                    Boolean isLevelsRemoved = false;

                    //Get assigned products
                    IEnumerable<Object> productIds = removeNode.Products.Select(p => p.PlanogramProductId);
                    List<PlanogramConsumerDecisionTreeNodeProduct> products = removeNode.Products.ToList();

                    ////Add locations to unassigned list
                    //foreach (Object product in productIds)
                    //{
                    //    this._unassignedProducts.Add(ConsumerDecisionTreeModel.Parent.Products.FindById(product));
                    //}

                    //Check if this is the last node at level, if so, remove level
                    if (!levelNodes.Any())
                    {
                        //Remove level
                        this.ConsumerDecisionTreeModel.RemoveLevel(level, true);

                        //Set flag to update levels
                        isLevelsRemoved = true;
                    }
                    else
                    {
                        //remove the node
                        removeNode.Products.RemoveList(products);
                        removeNode.ParentNode.ChildList.Remove(removeNode);
                    }

                    if (this.AttachedControl != null)
                    {
                        if (isLevelsRemoved)
                        {
                            //Manually call UpdateLevels if the selected nodes collection was not changed
                            this.AttachedControl.UpdateLevels();
                        }
                    }
                }
            }

            //Reset available products
            ResetAvailableProducts();

            //Update node product properties
            UpdateNodeProductProperties();

            //reselect root node
            if (!this.SelectedNodes.Any())
            {
                this.SelectedNodes.Add(RootNode);
            }

            base.ShowWaitCursor(false);
        }


        #endregion

        #region EditNodeCommand

        private RelayCommand _editNodeCommand;

        /// <summary>
        /// Shows the edit window for the selected node
        /// </summary>
        public RelayCommand EditNodeCommand
        {
            get
            {
                if (_editNodeCommand == null)
                {
                    _editNodeCommand = new RelayCommand(
                        p => EditNodeCommand_Executed(),
                        p => EditNodeCommand_CanExecute())
                    {
                        FriendlyName = Message.ConsumerDecisionTreeDocument_EditNode,
                        FriendlyDescription = Message.ConsumerDecisionTreeDocument_EditNode_Description,
                        Icon = ImageResources.CdtMaintenance_ShowUnitEditWindow,
                        SmallIcon = ImageResources.CdtMaintenance_ShowUnitEditWindow16
                    };
                    base.ViewModelCommands.Add(_editNodeCommand);
                }
                return _editNodeCommand;
            }
        }

        private Boolean EditNodeCommand_CanExecute()
        {
            //must contain 1 node and only 1 node
            if (this.SelectedNodes.Count != 1)
            {
                this.EditNodeCommand.DisabledReason = Message.ConsumerDecisionTreeDocument_EditNode_DisabledMultipleItems;
                return false;
            }

            return true;
        }

        private void EditNodeCommand_Executed()
        {

            ConsumerDecisionTreePlanNodeEditWindow win =
                new ConsumerDecisionTreePlanNodeEditWindow(this.SelectedNodes[0], this.Planogram.Model.Products);

            App.ShowWindow(win, true);

        }

        #endregion

        #region SplitNodeCommand

        private RelayCommand<String> _splitNodeCommand;

        /// <summary>
        /// Splits the selected node by the given property
        /// </summary>
        public RelayCommand<String> SplitNodeCommand
        {
            get
            {
                if (_splitNodeCommand == null)
                {
                    _splitNodeCommand = new RelayCommand<String>(
                        p => SplitNode_Executed(p),
                        p => SplitNode_CanExecute(p))
                    {
                        FriendlyName = Message.ConsumerDecisionTreeDocument_SplitNode,
                        FriendlyDescription = Message.ConsumerDecisionTreeDocument_SplitNode_Description,
                        Icon = ImageResources.CdtMaintenance_SplitNode_32,
                        SmallIcon = ImageResources.CdtMaintenance_SplitNode_16
                    };
                    base.ViewModelCommands.Add(_splitNodeCommand);
                }
                return _splitNodeCommand;
            }
        }

        private Boolean SplitNode_CanExecute(String property)
        {
            //property must not be null
            if (property == null)
            {
                SplitNodeCommand.DisabledReason = String.Empty;
                return false;
            }

            //selected node must not be null
            if (this.SelectedNodes.Count == 0)
            {
                SplitNodeCommand.DisabledReason = String.Empty;
                return false;
            }

            //selected node must be leaf
            if (this.SelectedNodes.Any(n =>
            {
                if (n != null)
                {
                    return (n.ChildList.Count != 0);
                }
                return false;
            }))
            {
                SplitNodeCommand.DisabledReason = String.Empty;
                return false;
            }

            //selected node must have products
            if (!this.SelectedNodes.Any(n =>
            {
                if (n != null)
                {
                    return (n.TotalProductCount > 0);
                }
                return false;
            }
                ))
            {
                SplitNodeCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void SplitNode_Executed(String property)
        {
            OnMajorNodeChangeStarting();

            base.ShowWaitCursor(true);

            List<PlanogramConsumerDecisionTreeNode> splitNodeList = this.SelectedNodes.ToList();
            List<PlanogramConsumerDecisionTreeNode> createdNodes = new List<PlanogramConsumerDecisionTreeNode>();

            this.SelectedNodes.Clear();

            List<PlanogramConsumerDecisionTreeNode> nullValueNodes = new List<PlanogramConsumerDecisionTreeNode>();
            List<PlanogramProduct> products = new List<PlanogramProduct>();

            foreach (PlanogramConsumerDecisionTreeNode splitNode in splitNodeList)
            {
                //get the products for the node
                List<PlanogramProduct> nodeProducts = new List<PlanogramProduct>();
                IEnumerable<Object> attachedProductIds =
                    splitNode.FetchAllChildPlanogramConsumerDecisionTreeNodeProducts().Select(p => p.PlanogramProductId);

                List<PlanogramProduct> assignedProducts = new List<PlanogramProduct>();
                foreach (Object product in attachedProductIds)
                {
                    assignedProducts.Add(ConsumerDecisionTreeModel.Parent.Products.FindById(product));

                }

                nodeProducts.AddRange(assignedProducts);
                products.AddRange(assignedProducts);

                //split
                PlanogramConsumerDecisionTreeNode nullValueNode = splitNode.SplitByProductProperties
                    (new List<String>() { property }, nodeProducts);

                //If theere are null values represented by a node, add to the list
                if (nullValueNode != null)
                {
                    nullValueNodes.Add(nullValueNode);
                }

                createdNodes.AddRange(splitNode.ChildList);
            }

            //If null nodes exist
            if (nullValueNodes.Count > 0)
            {
                var fields = PlanogramProductView.EnumerateDisplayableFields();
                List<String> properties = new List<String>();
                //Get properties 
                foreach (ObjectFieldInfo field in fields)
                {
                    properties.Add(field.PropertyName);
                }

                //Show dialog
                ConsumerDecisionTreePlanSplitByResultsWindow win = new ConsumerDecisionTreePlanSplitByResultsWindow(this.ConsumerDecisionTreeModel, products, properties, nullValueNodes);
                App.ShowWindow(win, true);
            }

            //Fetch all levels in descending order
            IEnumerable<PlanogramConsumerDecisionTreeLevel> currentLevels = this.ConsumerDecisionTreeModel.FetchAllLevels().OrderByDescending(p => p.LevelNumber);
            //Enumerate through levels
            foreach (PlanogramConsumerDecisionTreeLevel level in currentLevels)
            {
                //Ensure name is unique
                level.Name = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(level.Name, currentLevels.Where(p => p != level).Select(p => p.Name));
            }

            OnMajorNodeChangeComplete();

            this.UpdateNodeProductProperties();
            this.SelectedNodes.AddRange(createdNodes);

            base.ShowWaitCursor(false);
        }

        #endregion

        #region AddLevelCommand

        private RelayCommand _addLevelCommand;

        public RelayCommand AddLevelCommand
        {
            get
            {
                if (_addLevelCommand == null)
                {
                    _addLevelCommand = new RelayCommand(
                        p => AddLevel_Executed(),
                        p => AddLevel_CanExecute())
                    {
                        FriendlyName = Message.ConsumerDecisionTreeDocument_AddLevel,
                        FriendlyDescription = Message.ConsumerDecisionTreeDocument_AddLevel_Description,
                        Icon = ImageResources.CdtMaintenance_AddLevel,
                        SmallIcon = ImageResources.CdtMaintenance_AddLevel16
                    };
                    base.ViewModelCommands.Add(_addLevelCommand);
                }
                return _addLevelCommand;
            }
        }

        private Boolean AddLevel_CanExecute()
        {
            //current cdt must not be null
            if (ConsumerDecisionTreeModel == null)
            {
                this.AddLevelCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void AddLevel_Executed()
        {
            base.ShowWaitCursor(true);

            PlanogramConsumerDecisionTreeLevel newLevel = null;

            if (this.SelectedLevelId != null)
            {
                //If there is a level selected add the new level as a child
                PlanogramConsumerDecisionTreeLevel level = this.ConsumerDecisionTreeModel.FetchAllLevels().FirstOrDefault(p => p.Id.Equals(this.SelectedLevelId));
                if (level != null)
                {
                    newLevel = this.ConsumerDecisionTreeModel.InsertNewChildLevel(level);
                }
            }
            else
            {
                //There is no level selected, so add the child to the last level
                PlanogramConsumerDecisionTreeLevel level = this.ConsumerDecisionTreeModel.FetchAllLevels().Last();
                newLevel = this.ConsumerDecisionTreeModel.InsertNewChildLevel(level);
            }

            if (newLevel != null &&
                this.AttachedControl != null)
            {
                //Manually call UpdateLevels
                this.AttachedControl.UpdateLevels();
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region RemoveLevelCommand

        private RelayCommand _removeLevelCommand;

        public RelayCommand RemoveLevelCommand
        {
            get
            {
                if (_removeLevelCommand == null)
                {
                    _removeLevelCommand = new RelayCommand(
                        p => RemoveLevel_Executed(),
                        p => RemoveLevel_CanExecute())
                    {
                        FriendlyName = Message.ConsumerDecisionTreeDocument_RemoveLevel,
                        FriendlyDescription = Message.ConsumerDecisionTreeDocument_RemoveLevel_Description,
                        Icon = ImageResources.CdtMaintenance_RemoveLevel,
                        SmallIcon = ImageResources.CdtMaintenance_RemoveLevel16
                    };
                    base.ViewModelCommands.Add(_removeLevelCommand);
                }
                return _removeLevelCommand;
            }
        }

        private Boolean RemoveLevel_CanExecute()
        {
            //current cdt must not be null
            if (this.ConsumerDecisionTreeModel == null)
            {
                this.RemoveLevelCommand.DisabledReason = String.Empty;
                return false;
            }

            //items must be selected
            if (this.SelectedLevelId == null)
            {
                this.RemoveLevelCommand.DisabledReason = Message.ConsumerDecisionTreeDocument_RemoveLevel_DisabledNoLevel;
                return false;
            }

            //must not be the root level
            if (this.SelectedLevelId.Equals(this.ConsumerDecisionTreeModel.RootLevel.Id))
            {
                this.RemoveLevelCommand.DisabledReason = Message.ConsumerDecisionTreeDocument_RemoveLevel_DisabledRootLevel;
                return false;
            }

            return true;
        }

        private void RemoveLevel_Executed()
        {
            base.ShowWaitCursor(true);

            PlanogramConsumerDecisionTreeLevel level = this.ConsumerDecisionTreeModel.FetchAllLevels().FirstOrDefault(p => p.Id.Equals(this.SelectedLevelId));
            if (level == null)
            {
                base.ShowWaitCursor(false);
                return;
            }

            // Check for child levels and confirm with user.
            Boolean continueWithRemove = true;
            Boolean hasChildLevel = level.ChildLevel != null;
            Boolean hasChildNodes = ConsumerDecisionTreeModel.FetchAllNodes().Any(n => n.PlanogramConsumerDecisionTreeLevelId == level.Id);
            if (hasChildLevel || hasChildNodes)
            {
                ModalMessage msg = new ModalMessage()
                {
                    Header = level.Name,
                    Description = Message.ConsumerDecisionTree_RemoveLevel_Description,
                    Button1Content = Message.Generic_Continue,
                    Button2Content = Message.Generic_Cancel,
                    ButtonCount = 2,
                    MessageIcon = ImageResources.Dialog_Warning
                };
                App.ShowWindow(msg, true);
                continueWithRemove = (msg.Result == ModalMessageResult.Button1);
            }
            if (!continueWithRemove)
            {
                base.ShowWaitCursor(false);
                return;
            }

            // Remove level.
            this.ConsumerDecisionTreeModel.RemoveLevel(level, true);

            Boolean selectedNodesCleared = false;
            if (this.SelectedNodes.Count > 0)
            {
                //ensure the selected list is clear
                this.SelectedNodes.Clear();
                selectedNodesCleared = true;
            }

            if (this.AttachedControl != null)
            {
                if (!selectedNodesCleared)
                {
                    //Manually call UpdateLevels if the selected nodes collection was not changed
                    this.AttachedControl.UpdateLevels();
                }
                this.AttachedControl.OnLevelBlockSelectionChanged();
            }

            //Reset available products
            ResetAvailableProducts();

            //Update node product properties
            UpdateNodeProductProperties();

            //reselect root node
            if (!this.SelectedNodes.Any())
            {
                this.SelectedNodes.Add(RootNode);
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region AddSelectedProductsCommand

        private RelayCommand _addSelectedProductsCommand;

        public RelayCommand AddSelectedProductsCommand
        {
            get
            {
                if (_addSelectedProductsCommand == null)
                {
                    _addSelectedProductsCommand = new RelayCommand(
                        p => AddSelectedProducts_Executed(),
                        p => AddSelectedProducts_CanExecute())
                    {
                        FriendlyName = Message.ConsumerDecisionTreeDocument_AddSelectedProducts,
                        Icon = ImageResources.ConsumerDecisionTreeDocument_AddSelectedProducts
                    };
                    base.ViewModelCommands.Add(_addSelectedProductsCommand);
                }
                return _addSelectedProductsCommand;
            }
        }

        private Boolean AddSelectedProducts_CanExecute()
        {
            //must have a node selected
            if (this.SelectedNodes == null)
            {
                this.AddSelectedProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //must have one node selected
            if (this.SelectedNodes.Count != 1)
            {
                this.AddSelectedProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //Selected node must be a leaf
            if (this.SelectedNodes.FirstOrDefault().ChildList.Count > 0)
            {
                this.AddSelectedProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //Must have products selected
            if (this.SelectedUnassignedProducts.Count == 0)
            {
                this.AddSelectedProductsCommand.DisabledReason = Message.ConsumerDecisionTreeDocument_AddSelectedProducts_DisabledReason;
                return false;
            }

            return true;
        }

        private void AddSelectedProducts_Executed()
        {
            List<PlanogramConsumerDecisionTreeNodeProduct> newProducts = new List<PlanogramConsumerDecisionTreeNodeProduct>();
            foreach (PlanogramProductView product in this.SelectedUnassignedProducts)
            {
                newProducts.Add(PlanogramConsumerDecisionTreeNodeProduct.NewPlanogramConsumerDecisionTreeNodeProduct(product.Model.Id));
            }

            this.SelectedNodes.First().Products.AddRange(newProducts);
            UpdateSelectedNodeProducts();

            //Remove the products from the unassigned range
            _unassignedProducts.RemoveRange(this.SelectedUnassignedProducts);
            this.SelectedUnassignedProducts.Clear();
        }

        #endregion

        #region RemoveSelectedProductsCommand

        private RelayCommand _removeSelectedProductsCommand;

        public RelayCommand RemoveSelectedProductsCommand
        {
            get
            {
                if (_removeSelectedProductsCommand == null)
                {
                    _removeSelectedProductsCommand = new RelayCommand(
                        p => RemoveSelectedProducts_Executed(),
                        p => RemoveSelectedProducts_CanExecute())
                    {
                        FriendlyName = Message.ConsumerDecisionTreeDocument_RemoveSelectedProducts,
                        Icon = ImageResources.ConsumerDecisionTreeDocument_RemoveSelectedProducts
                    };
                    base.ViewModelCommands.Add(_removeSelectedProductsCommand);
                }
                return _removeSelectedProductsCommand;
            }
        }

        private Boolean RemoveSelectedProducts_CanExecute()
        {
            //must have a node selected
            if (this.SelectedNodes == null)
            {
                this.RemoveSelectedProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //must have one node selected
            if (this.SelectedNodes.Count != 1)
            {
                this.RemoveSelectedProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //Selected node must be a leaf
            if (this.SelectedNodes.FirstOrDefault().ChildList.Count > 0)
            {
                this.RemoveSelectedProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //Must have products selected
            if (this.SelectedAssignedProducts.Count == 0)
            {
                this.RemoveSelectedProductsCommand.DisabledReason = Message.ConsumerDecisionTreeDocument_RemoveSelectedProducts_DisabledReason;
                return false;
            }

            return true;
        }

        private void RemoveSelectedProducts_Executed()
        {
            List<PlanogramConsumerDecisionTreeNodeProduct> removedProducts = new List<PlanogramConsumerDecisionTreeNodeProduct>();
            foreach (PlanogramProductView product in this.SelectedAssignedProducts)
            {
                PlanogramConsumerDecisionTreeNodeProduct foundProduct = this.SelectedNodes.First().Products.FirstOrDefault(p => p.PlanogramProductId.Equals(product.Model.Id));
                if (foundProduct != null)
                {
                    removedProducts.Add(foundProduct);
                }
            }

            //Add the products to the unassigned range
            _unassignedProducts.AddRange(this.SelectedAssignedProducts);

            //Remove the products from the node
            this.SelectedNodes.First().Products.RemoveList(removedProducts);
            UpdateSelectedNodeProducts();
        }

        #endregion

        #region AddAllProductsCommand

        private RelayCommand _addAllProductsCommand;

        public RelayCommand AddAllProductsCommand
        {
            get
            {
                if (_addAllProductsCommand == null)
                {
                    _addAllProductsCommand = new RelayCommand(
                        p => AddAllProducts_Executed(),
                        p => AddAllProducts_CanExecute())
                    {
                        FriendlyName = Message.ConsumerDecisionTreeDocument_AddAllProducts,
                        Icon = ImageResources.ConsumerDecisionTreeDocument_AddAllProducts
                    };
                    base.ViewModelCommands.Add(_addAllProductsCommand);
                }
                return _addAllProductsCommand;
            }
        }

        private Boolean AddAllProducts_CanExecute()
        {
            //must have a node selected
            if (this.SelectedNodes == null)
            {
                this.AddAllProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //must have one node selected
            if (this.SelectedNodes.Count != 1)
            {
                this.AddAllProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //Selected node must be a leaf
            if (this.SelectedNodes.FirstOrDefault().ChildList.Count > 0)
            {
                this.AddAllProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //Must be products available to add
            if (this.UnassignedProducts.Count == 0)
            {
                this.AddAllProductsCommand.DisabledReason = Message.ConsumerDecisionTreeDocument_AddAllProducts_DisabledReason;
                return false;
            }

            return true;
        }

        private void AddAllProducts_Executed()
        {

            List<PlanogramConsumerDecisionTreeNodeProduct> newProducts = new List<PlanogramConsumerDecisionTreeNodeProduct>();
            foreach (PlanogramProductView product in this.UnassignedProducts)
            {
                newProducts.Add(PlanogramConsumerDecisionTreeNodeProduct.NewPlanogramConsumerDecisionTreeNodeProduct(product.Model.Id));
            }

            this.SelectedNodes.First().Products.AddRange(newProducts);
            UpdateSelectedNodeProducts();

            //Remove the products from the unassigned range
            _unassignedProducts.Clear();
            this.SelectedUnassignedProducts.Clear();

        }

        #endregion

        #region RemoveAllProductsCommand

        private RelayCommand _removeAllProductsCommand;

        public RelayCommand RemoveAllProductsCommand
        {
            get
            {
                if (_removeAllProductsCommand == null)
                {
                    _removeAllProductsCommand = new RelayCommand(
                        p => RemoveAllProducts_Executed(),
                        p => RemoveAllProducts_CanExecute())
                    {
                        FriendlyName = Message.ConsumerDecisionTreeDocument_RemoveAllProducts,
                        Icon = ImageResources.ConsumerDecisionTreeDocument_RemoveAllProducts
                    };
                    base.ViewModelCommands.Add(_removeAllProductsCommand);
                }
                return _removeAllProductsCommand;
            }
        }

        private Boolean RemoveAllProducts_CanExecute()
        {
            //must have a node selected
            if (this.SelectedNodes == null)
            {
                this.RemoveAllProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //must have one node selected
            if (this.SelectedNodes.Count != 1)
            {
                this.RemoveAllProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //Selected node must be a leaf
            if (this.SelectedNodes.FirstOrDefault().ChildList.Count > 0)
            {
                this.RemoveAllProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //Must be products available to remove
            if (this.SelectedNodeProducts.Count == 0)
            {
                this.RemoveAllProductsCommand.DisabledReason = Message.ConsumerDecisionTreeDocument_RemoveAllProducts_DisabledReason;
                return false;
            }

            return true;
        }

        private void RemoveAllProducts_Executed()
        {
            List<PlanogramConsumerDecisionTreeNodeProduct> removedProducts = new List<PlanogramConsumerDecisionTreeNodeProduct>();
            foreach (PlanogramProductView product in this.SelectedNodeProducts)
            {
                PlanogramConsumerDecisionTreeNodeProduct foundProduct = 
                    this.SelectedNodes.First().Products.FirstOrDefault(p => p.PlanogramProductId.Equals(product.Model.Id));
                if (foundProduct != null)
                {
                    removedProducts.Add(foundProduct);
                }
            }

            //Add the products to the unassigned range
            _unassignedProducts.AddRange(this.SelectedNodeProducts);

            //Remove the products from the node
            this.SelectedNodes.First().Products.Clear();
            UpdateSelectedNodeProducts();
        }

        #endregion

        #region MoreSplitPropertiesCommand

        private RelayCommand _moreSplitPropertiesCommand;

        public RelayCommand MoreSplitPropertiesCommand
        {
            get
            {
                if (_moreSplitPropertiesCommand == null)
                {
                    _moreSplitPropertiesCommand = new RelayCommand(
                        p => MoreSplitProperties_Executed(),
                        p => MoreSplitProperties_CanExecute())
                    {
                        FriendlyName = Message.Modelling_SidePanelMore
                    };
                    base.ViewModelCommands.Add(_moreSplitPropertiesCommand);
                }
                return _moreSplitPropertiesCommand;
            }
        }

        private Boolean MoreSplitProperties_CanExecute()
        {
            //selected node must not be null
            if (this.SelectedNodes.Count == 0)
            {
                MoreSplitPropertiesCommand.DisabledReason = String.Empty;
                return false;
            }

            //selected node must be leaf
            if (this.SelectedNodes.Any(n =>
            {
                if (n != null)
                {
                    return (n.ChildList.Count != 0);
                }
                return false;
            }))
            {
                MoreSplitPropertiesCommand.DisabledReason = String.Empty;
                return false;
            }

            //selected node must have products
            if (!this.SelectedNodes.Any(n =>
            {
                if (n != null)
                {
                    return (n.TotalProductCount > 0);
                }
                return false;
            }))
            {
                MoreSplitPropertiesCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void MoreSplitProperties_Executed()
        {
            //Display window with more split properties
            QuickItemsSelectDialog dialog = new QuickItemsSelectDialog();
            List<ObjectFieldInfo> fields = PlanogramProductView.EnumerateDisplayableFields().ToList();

            //create the columnset
            DataGridColumnCollection columnSet = new DataGridColumnCollection();

            dialog.ItemSource = fields;
            dialog.SelectionMode = DataGridSelectionMode.Single;

            columnSet.Add(
                ExtendedDataGrid.CreateReadOnlyTextColumn("Properties", "PropertyFriendlyName", HorizontalAlignment.Left));

            dialog.ColumnSet = columnSet;

            //show the dialog
            App.ShowWindow(dialog, true);

            if (dialog.DialogResult == true)
            {
                ObjectFieldInfo property = dialog.SelectedItems.Cast<ObjectFieldInfo>().FirstOrDefault();
                if (this.SplitNodeCommand.CanExecute(property.PropertyName))
                {
                    this.SplitNodeCommand.Execute(property.PropertyName);
                }
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Method to handle the root node changing
        /// </summary>
        /// <param name="newValue"></param>
        private void OnRootNodeChanged(PlanogramConsumerDecisionTreeNode newValue)
        {
            OnPropertyChanged(RootNodeProperty);

            ResetAvailableProducts();

            if (newValue != null)
            {
                this.SelectedNodes.Add(newValue);
            }
        }

        /// <summary>
        /// Responds to changes in the selected nodes collection
        /// </summary>
        private void SelectedNodes_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            UpdateSelectedNodeProducts();

            OnPropertyChanged(MetaCountOfProductsProperty);
            OnPropertyChanged(MetaCountOfRecommendedProductsProperty);
            OnPropertyChanged(MetaCountOfPlacedProductsProperty);
            OnPropertyChanged(MetaLinearSpaceAllocatedToProductsOnPlanogramProperty);
            OnPropertyChanged(MetaAreaSpaceAllocatedToProductsOnPlanogramProperty);
            OnPropertyChanged(MetaVolumetricSpaceAllocatedToProductsOnPlanogramProperty);
            OnPropertyChanged(MetaPropertyItemValuesProperty);
        }

        /// <summary>
        /// Corrects the list of products in the planogram
        /// </summary>
        private void Products_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            IEnumerable<PlanogramConsumerDecisionTreeNodeProduct> nodeProductList =
                 this.RootNode.FetchAllChildPlanogramConsumerDecisionTreeNodeProducts();
            List<PlanogramConsumerDecisionTreeNodeProduct> deletedProductList = new List<PlanogramConsumerDecisionTreeNodeProduct>();
            List<Int32> products = new List<Int32>();

            // get list of current products in planogram
            foreach (PlanogramProduct product in this.Planogram.Model.Products)
            {
                products.Add((Int32)product.Id);
            }

            // find if any products are missing from the list
            foreach (PlanogramConsumerDecisionTreeNodeProduct nodeProduct in nodeProductList)
            {
                if (!products.Contains((Int32)nodeProduct.PlanogramProductId))
                {
                    deletedProductList.Add(nodeProduct);
                }
            }

            // remove deleted products if any
            foreach (PlanogramConsumerDecisionTreeNodeProduct deletedProduct in deletedProductList)
            {
                deletedProduct.Parent.Products.Remove(deletedProduct);
            }

            ResetAvailableProducts();

            UpdateNodeProductProperties();

            UpdateSelectedNodeProducts();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Attaches and detaches events to the source planogramview
        /// </summary>
        /// <param name="isAttach"></param>
        private void SetPlanEventHandlers(Boolean isAttach)
        {
            if (isAttach)
            {
                _selectedNodes.BulkCollectionChanged += SelectedNodes_BulkCollectionChanged;
                this.Planogram.Model.Products.BulkCollectionChanged += Products_BulkCollectionChanged;
            }
            else
            {
                _selectedNodes.BulkCollectionChanged -= SelectedNodes_BulkCollectionChanged;
                this.Planogram.Model.Products.BulkCollectionChanged -= Products_BulkCollectionChanged;
            }
        }

        /// <summary>
        /// Called whenever this document should
        /// stop updating
        /// </summary>
        protected override void OnFreeze()
        {
            //dettach events
            SetPlanEventHandlers(false);
        }

        /// <summary>
        /// Called whenever this document should resume
        /// updating.
        /// </summary>
        protected override void OnUnfreeze()
        {
            //reattach events
            SetPlanEventHandlers(true);

            //update data
            OnPropertyChanged(ConsumerDecisionTreeModelProperty);
            OnRootNodeChanged(this.RootNode);
            OnPropertyChanged(ProductsProperty);
        }

        /// <summary>
        /// Corrects the list of available products
        /// </summary>
        private void ResetAvailableProducts()
        {
            if (this.ConsumerDecisionTreeModel == null) return;


            //clear any existing items
            if (_unassignedProducts.Count > 0)
            {
                _unassignedProducts.Clear();
            }

            List<object> rootProducts = new List<object>();
            IEnumerable<PlanogramConsumerDecisionTreeNodeProduct> nodeProductList =
                this.ConsumerDecisionTreeModel.RootNode.FetchAllChildPlanogramConsumerDecisionTreeNodeProducts();

            foreach (PlanogramConsumerDecisionTreeNodeProduct nodeProduct in nodeProductList)
            {
                rootProducts.Add(nodeProduct.PlanogramProductId);
            }

            foreach (PlanogramProductView product in this.Planogram.Products)
            {
                if (!rootProducts.Contains(product.Model.Id) &&
                    !_unassignedProducts.Contains(product))
                {
                    _unassignedProducts.Add(product);
                }
            }
        }

        /// <summary>
        /// Updates the SelectedNodeProducts collection
        /// </summary>
        private void UpdateSelectedNodeProducts()
        {
            //clear any existing items
            if (_selectedNodeProducts.Any())
            {
                _selectedNodeProducts.Clear();
                _selectedAssignedProducts.Clear();
            }

            if (this.SelectedNodes.Any())
            {
                List<PlanogramConsumerDecisionTreeNodeProduct> prodsToAdd = new List<PlanogramConsumerDecisionTreeNodeProduct>();
                foreach (PlanogramConsumerDecisionTreeNode node in this.SelectedNodes)
                {
                    if (node != null)
                    {
                        prodsToAdd.AddRange(node.FetchAllChildPlanogramConsumerDecisionTreeNodeProducts());
                    }
                }

                foreach (PlanogramConsumerDecisionTreeNodeProduct product in prodsToAdd)
                {
                    PlanogramProductView productView =
                        this.Planogram.Products.FirstOrDefault(p => Object.Equals(p.Model.Id, product.PlanogramProductId));

                    if (productView != null && !_selectedNodeProducts.Contains(productView))
                    {
                        _selectedNodeProducts.Add(productView);
                    }
                }
            }
        }

        /// <summary>
        /// Triggers an update of the range location properties
        /// </summary>
        private void UpdateNodeProductProperties()
        {
            //Enumerate through nodes
            foreach (PlanogramConsumerDecisionTreeNode node in this.ConsumerDecisionTreeModel.FetchAllNodes())
            {
                node.RaiseTotalProductCountChanged();
            }
        }

        /// <summary>
        /// For the currently selected node, retrieve the performance data metrics and associated
        /// values to display in a list box
        /// </summary>
        private void FetchPerformanceValues()
        {
            _metaPropertyItemsValues.Clear();

            if (SelectedNodes == null || SelectedNodes.Count == 0)
            {
                return;
            }

            // Find the plan performance metrics details
            var cdtNodeItem = SelectedNodes.FirstOrDefault();
            var metaPropertyItems = new List<ConsumerDecisionTreePlanDocumentPerformanceItem>();

            if (Planogram.PerformanceMetrics != null)
            {
                for (Byte metricCount = 0; metricCount < Planogram.PerformanceMetrics.Count(); metricCount++)
                {
                    var newMetricItem = new ConsumerDecisionTreePlanDocumentPerformanceItem()
                    {
                        MetaPropertyName = Planogram.PerformanceMetrics[metricCount].Name,

                        MetaPropertyValue = GetPerformanceMetricValue(cdtNodeItem, metricCount),
                        MetaPropertyPercent = GetPerformanceMetricPercent(cdtNodeItem, metricCount),
                    };

                    metaPropertyItems.Add(newMetricItem);
                }
            }

            _metaPropertyItemsValues.AddRange(metaPropertyItems);
        }

        /// <summary>
        /// Return the meta performance data property given the approrpiate metric number
        /// </summary>
        /// <param name="cdtNode">The CDT node to retrieve the data from</param>
        /// <param name="metricCount">The metric number</param>
        /// <returns>The matching performance data value</returns>
        private static Single? GetPerformanceMetricValue(PlanogramConsumerDecisionTreeNode cdtNode, Byte metricCount)
        {
            if (cdtNode == null) return null;

            switch (metricCount)
            {
                case 0: return cdtNode.MetaP1;
                case 1: return cdtNode.MetaP2;
                case 2: return cdtNode.MetaP3;
                case 3: return cdtNode.MetaP4;
                case 4: return cdtNode.MetaP5;
                case 5: return cdtNode.MetaP6;
                case 6: return cdtNode.MetaP7;
                case 7: return cdtNode.MetaP8;
                case 8: return cdtNode.MetaP9;
                case 9: return cdtNode.MetaP10;
                case 10: return cdtNode.MetaP11;
                case 11: return cdtNode.MetaP12;
                case 12: return cdtNode.MetaP13;
                case 13: return cdtNode.MetaP14;
                case 14: return cdtNode.MetaP15;
                case 15: return cdtNode.MetaP16;
                case 16: return cdtNode.MetaP17;
                case 17: return cdtNode.MetaP18;
                case 18: return cdtNode.MetaP19;
                case 19: return cdtNode.MetaP20;

                default:
                    return null;
            }
        }

        /// <summary>
        /// Return the meta percentage performance data property given the approrpiate metric number
        /// </summary>
        /// <param name="cdtNode">The CDT node to retrieve the data from</param>
        /// <param name="metricCount">The metric number</param>
        /// <returns>The matching meta percentage performance data value</returns>
        private static Single? GetPerformanceMetricPercent(PlanogramConsumerDecisionTreeNode cdtNode, Byte metricCount)
        {
            if (cdtNode == null) return null;

            switch (metricCount)
            {
                case 0: return cdtNode.MetaP1Percentage;
                case 1: return cdtNode.MetaP2Percentage;
                case 2: return cdtNode.MetaP3Percentage;
                case 3: return cdtNode.MetaP4Percentage;
                case 4: return cdtNode.MetaP5Percentage;
                case 5: return cdtNode.MetaP6Percentage;
                case 6: return cdtNode.MetaP7Percentage;
                case 7: return cdtNode.MetaP8Percentage;
                case 8: return cdtNode.MetaP9Percentage;
                case 9: return cdtNode.MetaP10Percentage;
                case 10: return cdtNode.MetaP11Percentage;
                case 11: return cdtNode.MetaP12Percentage;
                case 12: return cdtNode.MetaP13Percentage;
                case 13: return cdtNode.MetaP14Percentage;
                case 14: return cdtNode.MetaP15Percentage;
                case 15: return cdtNode.MetaP16Percentage;
                case 16: return cdtNode.MetaP17Percentage;
                case 17: return cdtNode.MetaP18Percentage;
                case 18: return cdtNode.MetaP19Percentage;
                case 19: return cdtNode.MetaP20Percentage;

                default:
                    return null;
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                base.Dispose(disposing);

                SetPlanEventHandlers(false);

                base.IsDisposed = true;
            }
        }
        #endregion
    }

    public class ConsumerDecisionTreePlanDocumentPerformanceItem
    {
        public String MetaPropertyName { get; set; }
        public Single? MetaPropertyValue { get; set; }
        public Single? MetaPropertyPercent { get; set; }
    }
}
