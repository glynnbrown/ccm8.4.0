﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26956 : L.Luong
//      Created

#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.ConsumerDecisionTree
{
    /// <summary>
    /// Interaction logic for ConsumerDecisionTreePlanNodeEditWindow.xaml
    /// </summary>
    public partial class ConsumerDecisionTreePlanNodeEditWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ConsumerDecisionTreePlanNodeEditViewModel), typeof(ConsumerDecisionTreePlanNodeEditWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public ConsumerDecisionTreePlanNodeEditViewModel ViewModel
        {
            get { return (ConsumerDecisionTreePlanNodeEditViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ConsumerDecisionTreePlanNodeEditWindow senderControl = (ConsumerDecisionTreePlanNodeEditWindow)obj;

            if (e.OldValue != null)
            {
                ConsumerDecisionTreePlanNodeEditViewModel oldModel = (ConsumerDecisionTreePlanNodeEditViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                ConsumerDecisionTreePlanNodeEditViewModel newModel = (ConsumerDecisionTreePlanNodeEditViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }

        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ConsumerDecisionTreePlanNodeEditWindow(PlanogramConsumerDecisionTreeNode nodeContext, PlanogramProductList planogramProductList)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = new ConsumerDecisionTreePlanNodeEditViewModel(nodeContext, planogramProductList);

            this.Loaded += new RoutedEventHandler(ConsumerDecisionTreePlanNodeEditWindow_Loaded);
        }

        private void ConsumerDecisionTreePlanNodeEditWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ConsumerDecisionTreePlanNodeEditWindow_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Window Close

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    IDisposable disposableViewModel = this.ViewModel as IDisposable;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }
                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion
    }
}
