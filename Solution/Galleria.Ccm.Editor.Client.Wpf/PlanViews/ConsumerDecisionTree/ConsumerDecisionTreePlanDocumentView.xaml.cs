﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26956 : L.Luong
//      Created
// V8-28040 : A.Probyn
//  ~ Added code to call ApplyFiltersIntoGrid after column set is applied to the grid.
#endregion

#region Version History: (CCM 8.2.0)
// V8-30958 : L.Ineson
//  Updated after column manager changes.
// V8-31423 : M.Shelley
//  Tidied up the styling of the performance expnder / panel
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.ConsumerDecisionTree
{
    /// <summary>
    /// Interaction logic for ConsumerDecisionTreePlanDocumentView.xaml
    /// </summary>
    public sealed partial class ConsumerDecisionTreePlanDocumentView : IPlanDocumentView
    {
        #region Fields

        private NodeTreeController _treeController = new NodeTreeController();
        private Boolean _suppressSelectionChangedHandler;
        private Boolean _suppressScrollChangedEvents;
        private ConsumerDecisionTreePlanNode _marginNode;
        private ConsumerDecisionTreePlanNode _rootNode;

        private ColumnLayoutManager _assignedColumnManager;
        private ColumnLayoutManager _unassignedColumnManager;
        private Boolean _isSynchingLayout;
        const String _screenKey = "ConsumerDecisionTreePlanDocument";

        private GridLength _performancePanelWidth = new GridLength(310.0);

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ConsumerDecisionTreePlanDocument),
                typeof(ConsumerDecisionTreePlanDocumentView),
                new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        ///     Handles the change of viewmodels for the view.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = (ConsumerDecisionTreePlanDocumentView)sender;

            if (e.OldValue != null)
            {
                ConsumerDecisionTreePlanDocument oldModel = (ConsumerDecisionTreePlanDocument)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                oldModel.SelectedNodes.BulkCollectionChanged -= senderControl.ViewModel_SelectedNodesBulkCollectionChanged;
                oldModel.MajorNodeChangeStarting -= senderControl.ViewModel_MajorNodeChangeStarting;
                oldModel.MajorNodeChangeComplete -= senderControl.ViewModel_MajorNodeChangeComplete;
            }

            if (e.NewValue != null)
            {
                ConsumerDecisionTreePlanDocument newModel = (ConsumerDecisionTreePlanDocument)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
                newModel.SelectedNodes.BulkCollectionChanged += senderControl.ViewModel_SelectedNodesBulkCollectionChanged;
                newModel.MajorNodeChangeStarting += senderControl.ViewModel_MajorNodeChangeStarting;
                newModel.MajorNodeChangeComplete += senderControl.ViewModel_MajorNodeChangeComplete;
            }

            senderControl.OnRootBlockChanged();
        }


        /// <summary>
        ///     Gets the viewmodel controller for this view.
        /// </summary>
        public ConsumerDecisionTreePlanDocument ViewModel
        {
            get { return (ConsumerDecisionTreePlanDocument)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor
        public ConsumerDecisionTreePlanDocumentView(ConsumerDecisionTreePlanDocument planDocument)
        {
            this.AddHandler(ConsumerDecisionTreePlanNode.MouseDoubleClickEvent, new MouseButtonEventHandler(CdtMaintenanceNode_MouseDoubleClicked));

            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            _treeController.Nodes.BulkCollectionChanged += TreeController_NodesBulkCollectionChanged;

            ViewModel = planDocument;

            this.Loaded += new RoutedEventHandler(ModellingCDTProductFamiliesWindow_Loaded);
        }

        /// <summary>
        /// Carries out first load complete actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ModellingCDTProductFamiliesWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ModellingCDTProductFamiliesWindow_Loaded;

            treeMapControl.SelectedItems.BulkCollectionChanged += TreeMapControl_SelectedItemsBulkCollectionChanged;

            _rootNode.IsSelected = true;

            OnLevelBlockSelectionChanged();

            //setup the column managers
            //nb - dont need to hook into columnset changing as we do not need to make any amendments.

            //we use two managers with the same layout applied as both product grids should be kept mirrored.
            IColumnLayoutFactory layoutFactory = new ProductColumnLayoutFactory(typeof(Product), this.ViewModel.Planogram.Model);

            _assignedColumnManager = new ColumnLayoutManager(
                layoutFactory,
                this.ViewModel.Planogram.DisplayUnits,
               _screenKey);
            _assignedColumnManager.CalculatedColumnPath = "CalculatedValueResolver";
            _assignedColumnManager.IncludeUnseenColumns = false;
            _assignedColumnManager.CurrentLayoutChanged += ColumnManager_CurrentLayoutChanged;
            _assignedColumnManager.AttachDataGrid(this.xNodeAssignedProductsGrid);


            _unassignedColumnManager = new ColumnLayoutManager(
                layoutFactory,
                this.ViewModel.Planogram.DisplayUnits,
                _screenKey);
            _unassignedColumnManager.CalculatedColumnPath = "CalculatedValueResolver";
            _unassignedColumnManager.IncludeUnseenColumns = false;
            _unassignedColumnManager.CurrentLayoutChanged += ColumnManager_CurrentLayoutChanged;
            _unassignedColumnManager.AttachDataGrid(this.xNodeUnassignedProductsGrid);


            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        
        #endregion

        #region Event Handlers

        private void ColumnManager_CurrentLayoutChanged(object sender, EventArgs e)
        {
            if (_isSynchingLayout) return;

            _isSynchingLayout = true;

            //mirror to the other manager
            if (sender == _unassignedColumnManager)
            {
                _assignedColumnManager.CurrentColumnLayout = _unassignedColumnManager.CurrentColumnLayout;
            }
            else
            {
                _unassignedColumnManager.CurrentColumnLayout = _assignedColumnManager.CurrentColumnLayout;
            }

            _isSynchingLayout = false;
        }

        private void xNodeAssignedProductsGrid_ColumnContextMenuOpening(Object sender, ExtendedDataGridContextMenuOpeningEventArgs e)
        {
            //TODO: Remove.
        }


        /// <summary>
        /// Responds to property changes on the viewmodel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ConsumerDecisionTreePlanDocument.RootNodeProperty.Path)
            {
                OnRootBlockChanged();
            }
        }

        /// <summary>
        /// Responds to changes in the viewmodel selected nodes collection
        /// </summary>
        private void ViewModel_SelectedNodesBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            if (!_suppressSelectionChangedHandler)
            {
                _suppressSelectionChangedHandler = true;

                IEnumerable<ConsumerDecisionTreePlanNode> blockList = treeMapControl.Blocks.Cast<ConsumerDecisionTreePlanNode>();

                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        this.treeMapControl.SelectedItems.Clear();

                        IEnumerable<ConsumerDecisionTreePlanNode> availableBlocks =
                            this.treeMapControl.Blocks.Cast<ConsumerDecisionTreePlanNode>().Where(b => e.ChangedItems.Contains(b.SourceNode));

                        //Set the selected level id
                        if (availableBlocks.Any())
                        {
                            this.ViewModel.SelectedLevelId = availableBlocks.First().SourceNode.PlanogramConsumerDecisionTreeLevelId;
                        }

                        this.treeMapControl.SelectedItems.AddRange(availableBlocks);
                        break;

                    case NotifyCollectionChangedAction.Remove:
                        foreach (PlanogramConsumerDecisionTreeNode node in e.ChangedItems)
                        {
                            ConsumerDecisionTreePlanNode block = blockList.FirstOrDefault(b => b.SourceNode == node);
                            if (block != null)
                            {
                                block.IsSelected = false;
                            }
                        }
                        break;

                    case NotifyCollectionChangedAction.Reset:
                        foreach (ConsumerDecisionTreePlanNode block in blockList)
                        {
                            PlanogramConsumerDecisionTreeNode content = block.SourceNode;
                            block.IsSelected = this.ViewModel.SelectedNodes.Contains(content);
                            if (block.IsSelected)
                            {
                                this.ViewModel.SelectedLevelId = block.SourceNode.PlanogramConsumerDecisionTreeLevelId;
                            }
                        }
                        break;
                }
                UpdateLevelSelection();
                _suppressSelectionChangedHandler = false;
            }
        }

        /// <summary>
        /// Responds to changes in the treemapcontrol selected items collection
        /// </summary>
        private void TreeMapControl_SelectedItemsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            if (!_suppressSelectionChangedHandler)
            {
                _suppressSelectionChangedHandler = true;

                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        List<PlanogramConsumerDecisionTreeNode> selectedBlockAdd = new List<PlanogramConsumerDecisionTreeNode>();
                        foreach (ConsumerDecisionTreePlanNode block in e.ChangedItems)
                        {
                            selectedBlockAdd.Add(block.SourceNode);
                            this.ViewModel.SelectedLevelId = block.SourceNode.PlanogramConsumerDecisionTreeLevelId;
                        }

                        this.ViewModel.SelectedNodes.AddRange(selectedBlockAdd);
                        break;

                    case NotifyCollectionChangedAction.Remove:
                        List<PlanogramConsumerDecisionTreeNode> selectedBlockRemove = new List<PlanogramConsumerDecisionTreeNode>();
                        foreach (ConsumerDecisionTreePlanNode block in e.ChangedItems)
                        {
                            selectedBlockRemove.Add(block.SourceNode);
                        }

                        this.ViewModel.SelectedNodes.RemoveRange(selectedBlockRemove);
                        break;

                    case NotifyCollectionChangedAction.Reset:
                        foreach (ConsumerDecisionTreePlanNode block in treeMapControl.Blocks)
                        {
                            PlanogramConsumerDecisionTreeNode content = block.SourceNode;
                            if (block.IsSelected)
                            {
                                if (!this.ViewModel.SelectedNodes.Contains(content))
                                {
                                    this.ViewModel.SelectedNodes.Add(content);
                                    this.ViewModel.SelectedLevelId = block.SourceNode.PlanogramConsumerDecisionTreeLevelId;
                                }
                            }
                            else
                            {
                                this.ViewModel.SelectedNodes.Remove(content);
                            }
                        }
                        break;
                }
                UpdateLevelSelection();
                _suppressSelectionChangedHandler = false;
            }
        }

        /// <summary>
        /// Responds to changes in the collection of nodes held by the tree controller
        /// </summary>
        private void TreeController_NodesBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (PlanogramConsumerDecisionTreeNode addedNode in e.ChangedItems)
                    {
                        PlanogramConsumerDecisionTreeNode parentNode = _treeController.GetNodeHandledParent(addedNode);
                        ConsumerDecisionTreePlanNode nodeBlock = null;

                        if (parentNode != null)
                        {
                            ConsumerDecisionTreePlanNode parentVisual =
                                treeMapControl.Blocks.Cast<ConsumerDecisionTreePlanNode>()
                                .FirstOrDefault(n => n.SourceNode == parentNode);

                            nodeBlock = DrawNode(addedNode, parentVisual);
                        }
                        else
                        {
                            nodeBlock = DrawNode(addedNode, null);
                            _rootNode = nodeBlock;
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (PlanogramConsumerDecisionTreeNode removedNode in e.ChangedItems)
                    {
                        IEnumerable<ConsumerDecisionTreePlanNode> treeNodes =
                            treeMapControl.Blocks.Cast<ConsumerDecisionTreePlanNode>();

                        //get the node visual
                        ConsumerDecisionTreePlanNode nodeVisual =
                            treeNodes.FirstOrDefault(n => n.SourceNode == removedNode);

                        if (nodeVisual != null)
                        {
                            treeMapControl.Blocks.Remove(nodeVisual);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    treeMapControl.Blocks.Clear();
                    break;
            }

            UpdateLevels();
        }

        /// <summary>
        /// Calls the edit node command on node double click
        /// </summary>
        private void CdtMaintenanceNode_MouseDoubleClicked(object sender, MouseButtonEventArgs e)
        {
            if (((DependencyObject)e.OriginalSource).FindVisualAncestor<ConsumerDecisionTreePlanNode>() != null)
            {
                this.ViewModel.EditNodeCommand.Execute();
            }
        }

        /// <summary>
        /// Responds to block right click
        /// </summary>
        private void TreeMapControl_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            ConsumerDecisionTreePlanNode clickedNode = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ConsumerDecisionTreePlanNode>();

            if (clickedNode != null)
            {
                //ensure the block is selected
                clickedNode.IsSelected = true;

                //display the context menu
                ShowNodeContextMenu(clickedNode);
            }
        }

        /// <summary>
        /// Responds to changes to the treemapcontrol scrollviewer
        /// Scrolls the levels scrollviewer to the same vertical offset
        /// </summary>
        private void TreeMapControl_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (!_suppressScrollChangedEvents)
            {
                _suppressScrollChangedEvents = true;

                //scroll the levels stack scrollviewer to the same offset.
                levelsStackScrollviewer.ScrollToVerticalOffset(e.VerticalOffset);

                //Get the scrollableheight without the spacer row's height
                Double realScrollableHeight = levelsStackScrollviewer.ScrollableHeight - Math.Round(xSpacerRow_Levels.Height.Value);

                //Increase the height of the space as the tree map's horizontal scroll bar adds to it's scrollable height
                if (e.VerticalOffset > realScrollableHeight)
                {
                    xSpacerRow_Levels.Height = new GridLength(e.VerticalOffset - realScrollableHeight);
                }
                else if (xSpacerRow_Levels.Height.Value > 0)
                {
                    xSpacerRow_Levels.Height = new GridLength(0);
                }
            }
            else
            {
                _suppressScrollChangedEvents = false;
            }
        }


        /// <summary>
        /// Responds to changes to the levels scrollviewer
        /// Scrolls the node tree map to the same vertical offset
        /// </summary>
        private void LevelsStackScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (!_suppressScrollChangedEvents)
            {
                _suppressScrollChangedEvents = true;

                //scroll the node scrollviewer to the same offset.
                ScrollViewer scrollView = treeMapControl.FindVisualDescendent<ScrollViewer>();

                if (scrollView != null)
                {
                    scrollView.ScrollToVerticalOffset(e.VerticalOffset);
                }
            }
            else
            {
                _suppressScrollChangedEvents = false;
            }
        }


        /// <summary>
        /// Selects the nodes on the selected level
        /// </summary>
        public void OnLevelBlockSelectionChanged()
        {
            UpdateLevelSelection();

            this.treeMapControl.SelectedItems.Clear();
            IEnumerable<ConsumerDecisionTreePlanNode> availableBlocks =
                this.treeMapControl.Blocks.Cast<ConsumerDecisionTreePlanNode>().Where(b => b.SourceNode.PlanogramConsumerDecisionTreeLevelId.Equals(this.ViewModel.SelectedLevelId));
            this.treeMapControl.SelectedItems.AddRange(availableBlocks);
        }

        /// <summary>
        /// Resets the row height when the expander collapses
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAssignedProducts_Collapsed(object sender, RoutedEventArgs e)
        {
            xExpanderColumn.Height = GridLength.Auto;
            xExpanderColumn.MinHeight = 18;
        }

        /// <summary>
        /// Set the row height of the assigned products expander to * when it expands
        /// </summary>
        private void xAssignedProducts_Expanded(object sender, RoutedEventArgs e)
        {
            xExpanderColumn.Height = new GridLength(1, GridUnitType.Star);
            xExpanderColumn.MinHeight = 200;
        }

        /// <summary>
        /// Responds to the viewmodel notification that a major node change is starting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_MajorNodeChangeStarting(object sender, EventArgs e)
        {
            //supress handlers for speed
            _treeController.Nodes.BulkCollectionChanged -= TreeController_NodesBulkCollectionChanged;
        }

        /// <summary>
        /// Reponds to the viewmodel notification that a major node change has completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_MajorNodeChangeComplete(object sender, EventArgs e)
        {
            //redraw and recalculate all.
            _treeController.Nodes.BulkCollectionChanged += TreeController_NodesBulkCollectionChanged;
            OnRootBlockChanged();
        }

        /// <summary>
        /// Selects all range breaks on the clicked level
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LevelBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ConsumerDecisionTreePlanLevelNode senderControl = (ConsumerDecisionTreePlanLevelNode)sender;
            this.ViewModel.SelectedLevelId = senderControl.LevelContext.Id;

            OnLevelBlockSelectionChanged();
        }

        /// <summary>
        /// Selects all nodes on the selected level
        /// </summary>
        private void LevelBlockText_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox senderControl = (TextBox)sender;

            ConsumerDecisionTreePlanLevelNode levelNode = senderControl.FindVisualAncestor<ConsumerDecisionTreePlanLevelNode>();
            if (levelNode != null)
            {
                this.ViewModel.SelectedLevelId = levelNode.LevelContext.Id;
                OnLevelBlockSelectionChanged();
            }
        }

        private void PerformancePanel_OnCollapsed(object sender, RoutedEventArgs e)
        {
            var index = Grid.GetColumn((Expander)sender);
            var column = TopGrid.ColumnDefinitions[index];

            // Store the width of the the panel before it was collapsed
            _performancePanelWidth = column.Width;

            // Collapse the panel
            column.Width = GridLength.Auto;
        }

        private void PerformancePanel_OnExpanded(object sender, RoutedEventArgs e)
        {
            var index = Grid.GetColumn((Expander)sender);
            var column = TopGrid.ColumnDefinitions[index];

            // Restore the panel to the size it had before being collapsed.
            column.Width = _performancePanelWidth;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Responds to a change of root block
        /// </summary>
        private void OnRootBlockChanged()
        {
            //clear out the old items
            if (treeMapControl.Blocks.Count > 0)
            {
                List<ConsumerDecisionTreePlanNode> oldBlocks = treeMapControl.Blocks.Cast<ConsumerDecisionTreePlanNode>().ToList();
                treeMapControl.Blocks.Clear();
                foreach (ConsumerDecisionTreePlanNode oldBlock in oldBlocks)
                {
                    oldBlock.Dispose();
                }
            }
            _marginNode = null;

            if (this.ViewModel != null)
            {
                //load the tree
                _treeController.RootNode = this.ViewModel.ConsumerDecisionTreeModel.RootNode;
            }
            else
            {
                _treeController.RootNode = null;
            }
        }

        /// <summary>
        /// Draws the visual for the given node
        /// </summary>
        /// <param name="nodeViewModel"></param>
        /// <param name="parentVisual"></param>
        /// <returns></returns>
        private ConsumerDecisionTreePlanNode DrawNode(PlanogramConsumerDecisionTreeNode node, ConsumerDecisionTreePlanNode parentVisual)
        {
            // Create the node
            ConsumerDecisionTreePlanNode nodeVisual = new ConsumerDecisionTreePlanNode(node);
            if (parentVisual != null)
            {
                nodeVisual.ParentBlock = parentVisual;
            }

            nodeVisual.SpacePercentage = node.GetPercentOfParent();

            treeMapControl.Blocks.Add(nodeVisual);

            return nodeVisual;
        }

        /// <summary>
        /// Updates the collection of level blocks
        /// </summary>
        public void UpdateLevels()
        {
            if (this.ViewModel != null)
            {
                //check if the levels display has changed
                Boolean reloadRequired = false;

                Int32 currentStackCount = this.cdtLevelsStack.Children.Count;

                List<PlanogramConsumerDecisionTreeLevel> levels = this.ViewModel.ConsumerDecisionTreeModel.FetchAllLevels().ToList();
                Int32 reqLevelCount = levels.Count();

                if (currentStackCount != reqLevelCount)
                {
                    reloadRequired = true;
                }
                else
                {
                    for (Int32 i = 0; i < reqLevelCount; i++)
                    {
                        ConsumerDecisionTreePlanLevelNode levelBlock = (ConsumerDecisionTreePlanLevelNode)this.cdtLevelsStack.Children[i];
                        PlanogramConsumerDecisionTreeLevel level = levels[i];
                        if (levelBlock.LevelContext != level)
                        {
                            reloadRequired = true;
                            break;
                        }
                    }
                }

                if (reloadRequired)
                {
                    //clear existing blocks
                    if (this.cdtLevelsStack.Children.Count > 0)
                    {
                        foreach (ConsumerDecisionTreePlanLevelNode levelNode in this.cdtLevelsStack.Children)
                        {
                            levelNode.Dispose();
                        }

                        this.cdtLevelsStack.Children.Clear();
                    }


                    PlanogramConsumerDecisionTreeLevel level = this.ViewModel.ConsumerDecisionTreeModel.RootLevel;
                    while (level != null)
                    {
                        ConsumerDecisionTreePlanLevelNode block = new ConsumerDecisionTreePlanLevelNode(level);
                        block.Height = treeMapControl.BlockHeight;
                        block.MouseDown += new MouseButtonEventHandler(LevelBlock_MouseDown);

                        //if not the root level then add context menu options
                        if (!level.IsRoot)
                        {
                            System.Windows.Controls.ContextMenu blockContext = new System.Windows.Controls.ContextMenu();

                            //remove level ranges command
                            System.Windows.Controls.MenuItem removeLevel = new System.Windows.Controls.MenuItem();
                            removeLevel.Command = this.ViewModel.RemoveLevelCommand;
                            removeLevel.Header = this.ViewModel.RemoveLevelCommand.FriendlyName;
                            removeLevel.Icon = new System.Windows.Controls.Image() { Source = this.ViewModel.RemoveLevelCommand.SmallIcon, Margin = new Thickness(1) };
                            blockContext.Items.Add(removeLevel);

                            block.ContextMenu = blockContext;
                        }
                        else
                        {
                            block.IsSelected = true;
                        }

                        this.cdtLevelsStack.Children.Add(block);
                        level = level.ChildLevel;
                    }

                    UpdateNodeBlockMargin();
                    UpdateLevelSelection();
                }
            }
            else
            {
                //clear existing blocks
                if (this.cdtLevelsStack.Children.Count > 0)
                {
                    foreach (ConsumerDecisionTreePlanLevelNode levelNode in this.cdtLevelsStack.Children)
                    {
                        levelNode.Dispose();
                    }

                    this.cdtLevelsStack.Children.Clear();
                }

            }
        }

        /// <summary>
        /// Updates the levels to highlight the selected level
        /// </summary>
        private void UpdateLevelSelection()
        {
            IEnumerable<ConsumerDecisionTreePlanLevelNode> levelBlocks = this.cdtLevelsStack.Children.Cast<ConsumerDecisionTreePlanLevelNode>();

            //Set the IsSelected property of the selected level to true
            ConsumerDecisionTreePlanLevelNode selectedLevel = levelBlocks.FirstOrDefault(p => p.LevelContext.Id.Equals(this.ViewModel.SelectedLevelId));
            if (selectedLevel != null)
            {
                if (!selectedLevel.IsSelected)
                {
                    selectedLevel.IsSelected = true;
                }
            }
            else
            {
                selectedLevel = levelBlocks.First();
            }

            //Update all the other level's IsSelected property to false
            IEnumerable<ConsumerDecisionTreePlanLevelNode> availableLevels = levelBlocks.Where(b => !b.Equals(selectedLevel) && b.IsSelected);
            foreach (ConsumerDecisionTreePlanLevelNode level in availableLevels)
            {
                level.IsSelected = false;
            }
        }

        /// <summary>
        /// Updates the margin on the last node block in the tree map so that the levels and nodes have the same scrollable area
        /// </summary>
        private void UpdateNodeBlockMargin()
        {
            //Get the last block in the tree map control to set a margin on if required
            ConsumerDecisionTreePlanNode lastBlock = this.treeMapControl.Blocks.LastOrDefault() as ConsumerDecisionTreePlanNode;
            if (lastBlock != null)
            {
                //reset the margin on the previous last block if required
                if (_marginNode != null &&
                    _marginNode != lastBlock)
                {
                    _marginNode.Margin = new Thickness(0);
                }

                Double levelScrollableHeight = Math.Min(0, levelsStackScrollviewer.ActualHeight - (cdtLevelsStack.Children.Count * lastBlock.Height)) * -1;
                Double nodeScrollableHeight = Math.Min(0, treeMapControl.ActualHeight - (treeMapControl.Blocks.Count * lastBlock.Height)) * -1;

                if (levelScrollableHeight > 0 ||
                    nodeScrollableHeight > 0)
                {
                    Int32 blockLevels = treeMapControl.Blocks.Select(p => p.BlockLevel).Distinct().Count();
                    lastBlock.Margin = new Thickness(0, 0, 0, Math.Max(0, (cdtLevelsStack.Children.Count - blockLevels) * lastBlock.Height));
                    _marginNode = lastBlock;
                }
                else if (lastBlock.Margin.Bottom > 0)
                {
                    lastBlock.Margin = new Thickness(0);
                }
            }
        }

        /// <summary>
        /// Displays the context menu for the clicked CdtMaintenanceNode
        /// </summary>
        private void ShowNodeContextMenu(ConsumerDecisionTreePlanNode clickedNode)
        {
            System.Windows.Controls.ContextMenu nodeMenu = new System.Windows.Controls.ContextMenu();

            //Remove item
            MenuItem removeItem =
                new MenuItem()
                {
                    Header = this.ViewModel.RemoveNodeCommand.FriendlyName,
                    Command = this.ViewModel.RemoveNodeCommand,
                    IsEnabled = this.ViewModel.RemoveNodeCommand.CanExecute(),
                    Icon = new System.Windows.Controls.Image() { Height = 16, Width = 16, Margin = new Thickness(3, 1, 1, 1), Source = this.ViewModel.RemoveNodeCommand.SmallIcon }
                };
            if (!removeItem.IsEnabled)
            {
                ((System.Windows.Controls.Image)removeItem.Icon).Effect = new Fluent.GrayscaleEffect();
            }
            nodeMenu.Items.Add(removeItem);

            MenuItem editItem =
                new MenuItem()
                {
                    Header = this.ViewModel.EditNodeCommand.FriendlyName,
                    Command = this.ViewModel.EditNodeCommand,
                    IsEnabled = this.ViewModel.EditNodeCommand.CanExecute(),
                    Icon = new System.Windows.Controls.Image() { Height = 16, Width = 16, Margin = new Thickness(3, 1, 1, 1), Source = this.ViewModel.EditNodeCommand.SmallIcon }
                };
            if (!editItem.IsEnabled)
            {
                ((System.Windows.Controls.Image)editItem.Icon).Effect = new Fluent.GrayscaleEffect();
            }
            nodeMenu.Items.Add(editItem);

            nodeMenu.PlacementTarget = clickedNode;
            nodeMenu.StaysOpen = false;
            nodeMenu.IsOpen = true;
        }

        #endregion

        #region IPlanDocumentView Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                _treeController.Nodes.BulkCollectionChanged -= TreeController_NodesBulkCollectionChanged;
                treeMapControl.SelectedItems.BulkCollectionChanged -= TreeMapControl_SelectedItemsBulkCollectionChanged;
                _assignedColumnManager.CurrentLayoutChanged -= ColumnManager_CurrentLayoutChanged;
                _unassignedColumnManager.CurrentLayoutChanged -= ColumnManager_CurrentLayoutChanged;

                if (disposing)
                {
                    //clean up level nodes.
                    if (this.cdtLevelsStack != null)
                    {
                        List<ConsumerDecisionTreePlanLevelNode> nodes = this.cdtLevelsStack.Children.OfType<ConsumerDecisionTreePlanLevelNode>().ToList();
                        this.cdtLevelsStack.Children.Clear();
                        nodes.ForEach(n => n.Dispose());
                    }

                    //clean up nodes
                    if (treeMapControl != null)
                    {
                        List<ConsumerDecisionTreePlanNode> oldBlocks = treeMapControl.Blocks.Cast<ConsumerDecisionTreePlanNode>().ToList();
                        treeMapControl.Blocks.Clear();
                        oldBlocks.ForEach(b => b.Dispose());
                    }

                    _assignedColumnManager.Dispose();
                    _unassignedColumnManager.Dispose();

                    this.ViewModel = null;
                }


                _isDisposed = true;
            }
        }

        public IPlanDocument GetIPlanDocument()
        {
            return ViewModel;
        }

        #endregion
    }
}
