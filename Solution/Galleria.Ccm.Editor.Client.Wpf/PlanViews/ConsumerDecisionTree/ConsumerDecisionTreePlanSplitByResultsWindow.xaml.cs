﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26956 : L.Luong
//      Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Collections;
using System.Reflection;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.Collections.ObjectModel;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.ConsumerDecisionTree
{
    /// <summary>
    /// Interaction logic for ConsumerDecisionTreePlanSplitByResultsWindow.xaml
    /// </summary>
    public partial class ConsumerDecisionTreePlanSplitByResultsWindow : ExtendedRibbonWindow
    {
         #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ConsumerDecisionTreePlanSplitByResultsViewModel), typeof(ConsumerDecisionTreePlanSplitByResultsWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets/Sets the viewmodel controller for this screen
        /// </summary>
        public ConsumerDecisionTreePlanSplitByResultsViewModel ViewModel
        {
            get { return (ConsumerDecisionTreePlanSplitByResultsViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ConsumerDecisionTreePlanSplitByResultsWindow senderControl = (ConsumerDecisionTreePlanSplitByResultsWindow)obj;

            if (e.OldValue != null)
            {
                ConsumerDecisionTreePlanSplitByResultsViewModel oldModel = (ConsumerDecisionTreePlanSplitByResultsViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                ConsumerDecisionTreePlanSplitByResultsViewModel newModel = (ConsumerDecisionTreePlanSplitByResultsViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public ConsumerDecisionTreePlanSplitByResultsWindow(PlanogramConsumerDecisionTree currentCdt, IEnumerable<PlanogramProduct> availableProducts, List<String> splitByProperties, List<PlanogramConsumerDecisionTreeNode> nullValueNodes)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            //Construct view model
            this.ViewModel = new ConsumerDecisionTreePlanSplitByResultsViewModel(currentCdt, availableProducts, splitByProperties, nullValueNodes);

            InitializeComponent();

            this.Loaded += new RoutedEventHandler(ConsumerDecisionTreePlanSplitByResultsWindow_Loaded);
        }

        #endregion

        #region Event Handlers

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            //load the columnset
            LoadColumnSet();
        }

        /// <summary>
        /// On loaded event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConsumerDecisionTreePlanSplitByResultsWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ConsumerDecisionTreePlanSplitByResultsWindow_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Loads the columns set from the viewmodel column definitions
        /// </summary>
        private void LoadColumnSet()
        {
            //Clear columns
            this.xScenarioLocationGrid.Columns.Clear();

            DataGridExtendedTextColumn codeCol = ExtendedDataGrid.CreateReadOnlyTextColumn(PlanogramProduct.GtinProperty.FriendlyName, String.Format("{0}.{1}", SplitByProductRow.ProductProperty.Path, PlanogramProduct.GtinProperty.Name), System.Windows.HorizontalAlignment.Left);
            codeCol.Visibility = Visibility.Visible;
            xScenarioLocationGrid.Columns.Add(codeCol);

            DataGridExtendedTextColumn nameCol = ExtendedDataGrid.CreateReadOnlyTextColumn(PlanogramProduct.NameProperty.FriendlyName, String.Format("{0}.{1}", SplitByProductRow.ProductProperty.Path, PlanogramProduct.NameProperty.Name), System.Windows.HorizontalAlignment.Left);
            nameCol.Visibility = Visibility.Visible;
            xScenarioLocationGrid.Columns.Add(nameCol);

            DataGridExtendedTextColumn rangeCol = ExtendedDataGrid.CreateReadOnlyTextColumn(Message.ConsumerDecisionTreeDocumentSplitByResultsWindow_SelectedNode, SplitByProductRow.SelectedNodeProperty.Path, System.Windows.HorizontalAlignment.Left);
            rangeCol.Visibility = Visibility.Visible;
            xScenarioLocationGrid.Columns.Add(rangeCol);

            DataGridExtendedTextColumn levelCol = ExtendedDataGrid.CreateReadOnlyTextColumn(Message.ConsumerDecisionTreeDocumentSplitByResultsWindow_NodeLevel, SplitByProductRow.ParentLevelProperty.Path, System.Windows.HorizontalAlignment.Left);
            levelCol.Visibility = Visibility.Visible;
            xScenarioLocationGrid.Columns.Add(levelCol);

            //Create column for each split by property
            foreach (String property in this.ViewModel.SplitByProperties)
            {
                DataGridExtendedTextColumn col = ExtendedDataGrid.CreateReadOnlyTextColumn(property, String.Format("{0}.{1}", SplitByProductRow.ProductProperty.Path, property), System.Windows.HorizontalAlignment.Left);
                col.Visibility = Visibility.Collapsed;
                xScenarioLocationGrid.Columns.Add(col);
            }
        }

        #endregion

        #region Window Close

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    IDisposable disposableViewModel = this.ViewModel as IDisposable;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }
                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion
    }
}
