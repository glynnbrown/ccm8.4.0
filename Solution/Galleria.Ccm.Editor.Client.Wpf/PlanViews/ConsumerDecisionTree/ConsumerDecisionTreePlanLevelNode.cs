﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26956 : L.Luong
//      Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Galleria.Framework.Controls.Wpf.TreeMap;

using Galleria.Framework.Planograms.Model;
using System.ComponentModel;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.ConsumerDecisionTree
{
    /// <summary>
    /// TreeMapBlock for a Cdt maintenance level node
    /// </summary>
    public sealed class ConsumerDecisionTreePlanLevelNode : TreeMapBlock, IDataErrorInfo, IDisposable
    {
        #region Fields

        private Boolean _isLevelNameValid = false;

        #endregion

        #region Events

        #region LevelChildChanged

        public static readonly RoutedEvent LevelChildChangedEvent =
         EventManager.RegisterRoutedEvent("LevelChildChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ConsumerDecisionTreePlanLevelNode));

        public event RoutedEventHandler LevelChildChanged
        {
            add { AddHandler(LevelChildChangedEvent, value); }
            remove { RemoveHandler(LevelChildChangedEvent, value); }
        }

        private void RaiseLevelChildChangedEvent()
        {
            RoutedEventArgs newEventArgs = new RoutedEventArgs(ConsumerDecisionTreePlanLevelNode.LevelChildChangedEvent);
            RaiseEvent(newEventArgs);
        }

        #endregion

        #endregion

        #region Properties

        #region Level Context Property
        private PlanogramConsumerDecisionTreeLevel _levelContext;

        public static readonly DependencyProperty LevelContextProperty =
            DependencyProperty.Register("LevelContext", typeof(PlanogramConsumerDecisionTreeLevel), typeof(ConsumerDecisionTreePlanLevelNode),
            new PropertyMetadata(null, OnLevelContextPropertyChanged));

        private static void OnLevelContextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ConsumerDecisionTreePlanLevelNode senderControl = (ConsumerDecisionTreePlanLevelNode)obj;
            senderControl._levelContext = e.NewValue as PlanogramConsumerDecisionTreeLevel;
               
            if (e.OldValue != null)
            {
                PlanogramConsumerDecisionTreeLevel oldLevel = (PlanogramConsumerDecisionTreeLevel)e.OldValue;
                oldLevel.PropertyChanged -= senderControl.LevelContext_PropertyChanged;
                oldLevel.ChildChanged -= senderControl.LevelContext_ChildChanged;
            }


            if (e.NewValue != null)
            {
                PlanogramConsumerDecisionTreeLevel newLevel = (PlanogramConsumerDecisionTreeLevel)e.NewValue;

                //Attach to property changed
                newLevel.PropertyChanged += senderControl.LevelContext_PropertyChanged;

                //Subscribe to child list collection changed
                newLevel.ChildChanged += senderControl.LevelContext_ChildChanged;
            }
        }
        
        /// <summary>
        /// Gets/ Sets the node content context
        /// </summary>
        public PlanogramConsumerDecisionTreeLevel LevelContext
        {
            get { return (PlanogramConsumerDecisionTreeLevel)GetValue(LevelContextProperty); }
            set { SetValue(LevelContextProperty, value); }
        }

        #endregion

        /// <summary>
        /// Gets/ Sets the node content context
        /// </summary>
        public Boolean IsLevelNameValid
        {
            get { return _isLevelNameValid; }
            set { _isLevelNameValid = value; }
        }

        /// <summary>
        /// Gets/ Sets the node content context
        /// </summary>
        public String LevelName
        {
            get { return this.LevelContext.Name; }
            set { this.LevelContext.Name = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Static constructor
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Style key override")]
        static ConsumerDecisionTreePlanLevelNode()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(ConsumerDecisionTreePlanLevelNode), new FrameworkPropertyMetadata(typeof(ConsumerDecisionTreePlanLevelNode)));
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitContext"></param>
        public ConsumerDecisionTreePlanLevelNode(PlanogramConsumerDecisionTreeLevel levelContext)
            : base()
        {
            //Validate 1st time
            this.IsLevelNameValid = ConsumerDecisionTreePlanLevelNode.ValidateLevelName(levelContext);

            //Set the level context
            this.LevelContext = levelContext;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Event handler for the child level changing
        /// </summary>
        private void LevelContext_ChildChanged(object sender, Csla.Core.ChildChangedEventArgs e)
        {
            RaiseLevelChildChangedEvent();
        }

        /// <summary>
        /// Property changed handler for the cdt level
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LevelContext_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == PlanogramConsumerDecisionTreeLevel.NameProperty.Name)
            {
                PlanogramConsumerDecisionTreeLevel level = sender as PlanogramConsumerDecisionTreeLevel;
                if (level != null)
                {
                    this.IsLevelNameValid = ConsumerDecisionTreePlanLevelNode.ValidateLevelName(level);
                }
            }
        }

        /// <summary>
        /// Static method to validate the level name
        /// </summary>
        /// <param name="level"></param>
        private static Boolean ValidateLevelName(PlanogramConsumerDecisionTreeLevel level)
        {
            if (level != null)
            {
                //Fetch all levels in descending order
                IEnumerable<PlanogramConsumerDecisionTreeLevel> currentRangeModelLevels = level.ParentPlanogramConsumerDecisionTree.FetchAllLevels();

                //Ensure name is unique
                return !(currentRangeModelLevels.Where(p => p.Name.Equals(level.Name)).Count() > 1);
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region IDataErrorInfo

        String IDataErrorInfo.Error
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Implementation of the IDataErrorInfo interface
        /// </summary>
        /// <param name="columnName">Property name</param>
        /// <returns>String containing error</returns>
        String IDataErrorInfo.this[String columnName]
        {
            get
            {
                String result = null;
                List<String> siblingNames = new List<String>();
                if (columnName == "LevelName")
                {
                    if (!this.IsLevelNameValid)
                    {
                        result = Message.ConsumerDecisionTreeDocument_DuplicateLevelName;
                    }
                }

                return result; // return result
            }
        }
        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean isDisposing)
        {
            if (!_isDisposed)
            {
                if (isDisposing)
                {
                    this.LevelContext = null;
                }
               
                _isDisposed = true;
            }
        }

        #endregion
    }
}
