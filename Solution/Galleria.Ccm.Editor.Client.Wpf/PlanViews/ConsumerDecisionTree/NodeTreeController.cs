﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26956 : L.Luong
//      Created

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Model;
using Galleria.Framework.Model;
using Galleria.Framework.Collections;
using System.Collections.Specialized;
using System.Collections;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.ConsumerDecisionTree
{
    /// <summary>
    /// Handles the notification of node changes for a tree node hierarchy
    /// </summary> 
    public sealed class NodeTreeController
       {
        #region Fields
        
        private PlanogramConsumerDecisionTreeNode _rootNode;
        private BulkObservableCollection<PlanogramConsumerDecisionTreeNode> _handledNodes = new BulkObservableCollection<PlanogramConsumerDecisionTreeNode>();
        private ReadOnlyBulkObservableCollection<PlanogramConsumerDecisionTreeNode> _handledNodesRO;

        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of nodes that make up the joined tree
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramConsumerDecisionTreeNode> Nodes
        {
            get
            {
                if (_handledNodesRO == null)
                {
                    _handledNodesRO = new ReadOnlyBulkObservableCollection<PlanogramConsumerDecisionTreeNode>(_handledNodes);
                }
                return _handledNodesRO;
            }
        }

        /// <summary>
        /// Gets/Sets the root node to process from
        /// </summary>
        public PlanogramConsumerDecisionTreeNode RootNode
        {
            get { return _rootNode; }
            set
            {
                _rootNode = value;
                OnRootNodeChanged();
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public NodeTreeController()
        {
            _handledNodes.BulkCollectionChanged += HandledNodes_BulkCollectionChanged;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to changes in the handled nodes collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandledNodes_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (PlanogramConsumerDecisionTreeNode n in e.ChangedItems)
                    {
                        n.ChildList.CollectionChanged += Node_ChildCollectionChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (PlanogramConsumerDecisionTreeNode n in e.ChangedItems)
                    {
                        n.ChildList.CollectionChanged -= Node_ChildCollectionChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    if (e.ChangedItems != null)
                    {
                        foreach (PlanogramConsumerDecisionTreeNode n in e.ChangedItems)
                        {
                            n.ChildList.CollectionChanged -= Node_ChildCollectionChanged;
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Responds to changes in a node's child collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Node_ChildCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (PlanogramConsumerDecisionTreeNode n in e.NewItems)
                    {
                        AddNodeToHandled(n);
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (PlanogramConsumerDecisionTreeNode n in e.OldItems)
                    {
                        RemoveNodeFromHandled(n);
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:

                    //remove all handled children
                    PlanogramConsumerDecisionTreeNode parentNode = this.Nodes.First(n => n.ChildList == sender);
                    foreach (PlanogramConsumerDecisionTreeNode child in GetNodeHandledChildNodes(parentNode))
                    {
                        RemoveNodeFromHandled(child);
                    }

                    //add in all required children
                    foreach (PlanogramConsumerDecisionTreeNode child in (IEnumerable)sender)
                    {
                        AddNodeToHandled(child);
                    }
                    break;
            }
        }

        /// <summary>
        /// Responds to a change of the root node
        /// </summary>
        private void OnRootNodeChanged()
        {
            //clear the current handled nodes
            _handledNodes.RemoveRange(_handledNodes.ToList());

            if (_rootNode != null)
            {
                //add the root model node - the rest is recursive
                AddNodeToHandled(_rootNode);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the parent node of the given node
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public PlanogramConsumerDecisionTreeNode GetNodeHandledParent(PlanogramConsumerDecisionTreeNode node)
        {
            PlanogramConsumerDecisionTreeNode nodeToGetParentFor = node;

            if (nodeToGetParentFor != null)
            {
                return nodeToGetParentFor.ParentNode;
            }
            return default(PlanogramConsumerDecisionTreeNode);
        }

        /// <summary>
        /// Returns a list of the joined tree children of the given node
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public List<PlanogramConsumerDecisionTreeNode> GetNodeHandledChildNodes(PlanogramConsumerDecisionTreeNode node)
        {
            List<PlanogramConsumerDecisionTreeNode> returnList = new List<PlanogramConsumerDecisionTreeNode>();

            PlanogramConsumerDecisionTreeNode actualHandledNode = node;

            //add child nodes to the return list
            foreach (PlanogramConsumerDecisionTreeNode child in _handledNodes)
            {
                if (child.ParentNode != null)
                {
                    if (child.ParentNode.Equals(actualHandledNode))
                    {
                        returnList.Add(child);
                    }
                }
            }

            return returnList;
        }

        /// <summary>
        /// Adds the node to the handled 
        /// so that the event subscription can be tracked.
        /// </summary>
        /// <param name="n"></param>
        private void AddNodeToHandled(PlanogramConsumerDecisionTreeNode n)
        {
            PlanogramConsumerDecisionTreeNode actualNodeToAdd = n;

            if (actualNodeToAdd != null)
            {
                if (!_handledNodes.Contains(actualNodeToAdd))
                {
                    //add the node itself
                    _handledNodes.AddRange(GetNodeAndChildren(actualNodeToAdd));
                }
            }
        }

        /// <summary>
        /// Returns a node and all its children as an IEnumerable, so that they can be added to _handledNodes all at
        /// once rather than one at a time.
        /// </summary>
        private IEnumerable<PlanogramConsumerDecisionTreeNode> GetNodeAndChildren(PlanogramConsumerDecisionTreeNode n)
        {
            PlanogramConsumerDecisionTreeNode actualNodeToAdd = n;
            List<PlanogramConsumerDecisionTreeNode> returnValue = new List<PlanogramConsumerDecisionTreeNode> { n };
            //add all children recursively
            foreach (PlanogramConsumerDecisionTreeNode child in (IEnumerable)n.ChildList)
            {
                returnValue.AddRange(GetNodeAndChildren(child));
            }
            return returnValue;
        }

        /// <summary>
        /// Removes the node to the handled 
        /// so that the event unsubscription can be tracked.
        /// </summary>
        /// <param name="n"></param>
        private void RemoveNodeFromHandled(PlanogramConsumerDecisionTreeNode n)
        {
            PlanogramConsumerDecisionTreeNode actualRemoveNode = n;


            if (actualRemoveNode != null)
            {
                //remove the node itself
                _handledNodes.Remove(actualRemoveNode);

                //get and remove any child nodes recursively
                foreach (PlanogramConsumerDecisionTreeNode childNode in (IEnumerable)actualRemoveNode.ChildList)
                {
                    RemoveNodeFromHandled(childNode);
                }

            }
        }

        #endregion
    }
}
