﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// L.Ineson 
//  Created.
// V8-25395  : A.Probyn
//   Changed over from ProductInfo to Product
// V8-24124 : A.Silva
//  Added support for Custom Column Layouts.
//  Added a delete column per row. Removed the icon on top.
//  Added support to add products from other plans.
//  Extracted interface ICustomColumnLayoutView to support Custom Column Layouts.
//  Added restoring filters from the custom column layout.
// V8-25797 : A.Silva 
//  Modified GetColumnSet to make sure the path is preappended with "Product."
// V8-26143 : A.Silva 
//  Changed to use the Extended DataGrid context emnu instead of the UI ribbon button.
// V8-26155 : A.Silva 
//  Made sure the type of custom column layout is Product.
// V8-26171 : A.Silva 
//  Added CustomColumnManager and removed ICustomColumnLayoutView.
// V8-26281 : L.Ineson
//  Tidied up.
// V8-26329 : A.Silva ~ Added _columnLayoutManager_CurrentColumnLayoutEdited eventhandler.
// V8-26370 : A.Silva ~ Corrected disposing of _columnLayoutManager.
// V8-26368 : A.Silva ~ Added code to filter shown products by placement count.
// V8-26633 : A.Silva ~ Moved out PlacementFilterType to its own file.
// V8-26472 : A.Probyn ~ Updated reference to ColumnLayoutManager Constructor
// V8-27149 : A.Silva ~ Moved ColumnLayoutManager to the View.
// V8-27140 : A.Silva ~ Amended SelectedRows_CollectionChanged() to make sure it keeps products selected when multiselecting.
// V8-25209 : L.Ineson
//  Highlights are no longer lost when saving.
// V8-27825 : L.Ineson
//  Added document freezing
// V8-27900 : L.Ineson
//  Moved highlight column ui code into the code behind class.
// V8-28345 : L.Ineson
//  Reworked how highlights are processed and applied to improve performance.
// V8-28356 : A.Kuszyk
//  Added overload to AddProductsToPlanogram for ProductLibraryProducts.

#endregion

#region Version History: (CCM 8.01)

// V8-28770 : L.Ineson
//  Rows get reloaded when a filter is in place and positions change.

#endregion

#region Version History : CCM 8.02

// V8-25955 : D.Pleasance
//  Removed PositionProductRowCommand
// V8-27491 : I.George
//  Changed the small icon image in EditProductRowCommand so that it matches the product Properties icon

#endregion

#region Version History : CCM 8.10

// V8-29916 : L.Ineson
//  Stopped highlights crash.

#endregion

#region Version History : CCM811

// V8-30328 : A.Silva
//  Amended SelectedRows_CollectionChanged so that when adding new selection rows only the new are synchronized on PlanDocument<T>.SelectedPlanItems
// V8-30358 : J.Pickup
//  Small performance improvement to SelectedRows_CollectionChanged when enumerating all positions
// V8-30346 : A.Silva
//  Amended a selected row syncing issue when reselecting a selected row would cause the whole selection to be persisted.
// V8-28688 : A.Probyn
//  Updated reference to PositionPropertiesViewModel to take a new constructor depending on where it is called from.

#endregion

#region Version History : CCM820
// V8-31432 : J.Pickup
//  Changes to the SyncAddHandler etc in order to account for modifier keys. 
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf.HelperClasses;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.ProductsList
{
    /// <summary>
    ///     Viewmodel for a planogram product row.
    /// </summary>
    public sealed class ProductListRow : INotifyPropertyChanged
    {
        #region Fields

        private readonly IPlanItem _planItem;
        private Int32 _highlightColour;
        private String _highlightTooltip;
        private Int32 _productPositionsCount;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath HighlightColourProperty = WpfHelper.GetPropertyPath<ProductListRow>(p => p.HighlightColour);
        public static readonly PropertyPath HighlightTooltipProperty = WpfHelper.GetPropertyPath<ProductListRow>(p => p.HighlightTooltip);
        public static readonly PropertyPath ProductPositionsCountProperty = WpfHelper.GetPropertyPath<ProductListRow>(p => p.ProductPositionsCount);
        public static readonly PropertyPath PlanItemProperty = WpfHelper.GetPropertyPath<ProductListRow>(p => p.PlanItem);

        #endregion

        #region Properties

        /// <summary>
        ///     Gets/Sets the highlight colour.
        /// </summary>
        public Int32 HighlightColour
        {
            get { return _highlightColour; }
            set
            {
                _highlightColour = value;
                OnPropertyChanged(HighlightColourProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets the highlight tooltip.
        /// </summary>
        public String HighlightTooltip
        {
            get { return _highlightTooltip; }
            set
            {
                _highlightTooltip = value;
                OnPropertyChanged(HighlightTooltipProperty);
            }
        }

        /// <summary>
        /// Returns the plan item source for this row.
        /// </summary>
        public IPlanItem PlanItem
        {
            get { return _planItem; }
        }

        /// <summary>
        ///     Returns the number of positions which exist in the planogram for this product.
        /// </summary>
        public Int32 ProductPositionsCount
        {
            get { return _productPositionsCount; }
            set
            {
                _productPositionsCount = value;
                OnPropertyChanged(ProductPositionsCountProperty);
            }
        }

        #endregion

        #region Constructor

        public ProductListRow(IPlanItem planItem)
        {
            _planItem = planItem;
            _highlightColour = ColorHelper.White;
            _productPositionsCount = GetPositionsCount();
        }

        #endregion

        #region Methods

        private Int32 GetPositionsCount()
        {
            if (_planItem.Planogram != null)
            {
                if (this.PlanItem.PlanItemType == PlanItemType.Position) return 1;
                else return this.PlanItem.Product.Model.GetPlanogramPositions().Count();
            }
            return 0;
        }

        public void Refresh()
        {
            this.ProductPositionsCount = GetPositionsCount();
            OnPropertyChanged(String.Empty);
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        private void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion


    }
}
