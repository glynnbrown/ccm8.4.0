﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// L.Hodson 
//  Created.
// V8-25373   : A.Probyn
//  Changed over from ProductInfo to Product
// V8-24124 : A.Silva 
//  Added suport for Custom Column Layouts.
//  Added support for drag drop between product list rows (between different plans).
//  Added support for buttons ON the datagrid.
// V8-26143 : A.Silva 
//  Removed the custom column layout button code, and added the context menu generation.
// V8-26281 : L.Ineson
//  No longer processes row drops from its own grid.
// V8-27149 : A.Silva ~ Moved ColumnLayoutManager from the View Model.
// V8-27900 : L.Ineson
//  Stored highlight column here instead of against viewmodel.
// V8-27900 : L.Ineson
//  Added column context menu option for copy to clipboard.
// V8-28040 : A.Probyn
//  ~ Added code to call ApplyFiltersIntoGrid after column set is applied to the grid.

#endregion

#region Version History: (CCM 8.01)

// V8-28674 : L.Ineson
//  UOM now comes from the current plan rather than the repository entity.
//  Columns now reload when plan uom changes.

#endregion

#region Version History : CCM 802

// V8-28906 : A.Kuszyk
//  Added event handler for View Model's SelectedRows collection change to make sure 
//  that the selected row is always visible.
// V8-25955 : D.Pleasance
//  Removed redundant key PositionProductRowCommandKey

#endregion

#region Version History: (CCM 8.1.0)

// V8-29681 : A.Probyn
// Implemented new RegisterPlanogram when using product column layout factory

#endregion

#region Version History: (CCM 8.1.1)

// V8-30358 : J.Pickup
// Introduced ScrollToLastSelectedRowWithDelay to introduce a big performance improvement.
// V8-30468 : A.Silva
//  Changed the ScrollToLastSelectedRowWithDelay into an auto delaying timer 
//  (if another request is done, just restart the time, scroll only when there have not been requests for the delay time).

#endregion

#region Version History: (CCM 8.2.0)
//V8-30958 : L.Ineson
//  Updated after column manager changes.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.ProductsList
{
    /// <summary>
    ///     Interaction logic for ProductListPlanDocumentView.xaml
    /// </summary>
    public sealed partial class ProductListPlanDocumentView : IPlanDocumentView
    {
        #region Fields

        public static readonly String RemoveProductRowCommandKey = "RemoveProductCommand";
        public static readonly String EditProductRowCommandKey = "EditProductCommand";
        private ColumnLayoutManager _columnLayoutManager;
        private DataGridColumn _highlightColumn;
        private DataGridColumn _buttonColumn;

        /// <summary>
        ///     <see cref="DispatcherTimer"/> instance to control delayed scroll into view of newly selected rows.
        /// </summary>
        private readonly DispatcherTimer _scrollIntoViewTimer;

        /// <summary>
        ///     The last selected row to scroll into view.
        /// </summary>
        private Object _scrollIntoViewRow;

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ProductListPlanDocument),
                typeof(ProductListPlanDocumentView), new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ProductListPlanDocumentView senderControl = (ProductListPlanDocumentView)sender;

            ProductListPlanDocument oldModel = e.OldValue as ProductListPlanDocument;
            if (oldModel != null)
            {
                senderControl.Resources.Remove(RemoveProductRowCommandKey);
                senderControl.Resources.Remove(EditProductRowCommandKey);
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                oldModel.Planogram.PropertyChanged -= senderControl.PlanogramView_PropertyChanged;
                oldModel.SelectedRows.CollectionChanged -= senderControl.OnViewModelSelectedRowsChanged;
                oldModel.AttachedControl = null;
            }

            ProductListPlanDocument newModel = e.NewValue as ProductListPlanDocument;
            if (newModel != null)
            {
                senderControl.Resources.Add(RemoveProductRowCommandKey, newModel.RemoveProductRowCommand);
                senderControl.Resources.Add(EditProductRowCommandKey, newModel.EditProductRowCommand);
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
                newModel.SelectedRows.CollectionChanged += senderControl.OnViewModelSelectedRowsChanged;
                newModel.Planogram.PropertyChanged += senderControl.PlanogramView_PropertyChanged;
            }
        }

        /// <summary>
        ///     Gets the viewmodel controller for this view.
        /// </summary>
        public ProductListPlanDocument ViewModel
        {
            get { return (ProductListPlanDocument)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="planDocument"></param>
        public ProductListPlanDocumentView(ProductListPlanDocument planDocument)
        {
            // Show the busy cursor while loading the screen.
            Mouse.OverrideCursor = Cursors.Wait;

            //  Initialize the scroll into view timer for the product list.
            _scrollIntoViewTimer = new DispatcherTimer(
                TimeSpan.FromMilliseconds(10),
                DispatcherPriority.Background,
                ScrollIntoViewHandler,
                Dispatcher.CurrentDispatcher);

            InitializeComponent();
            this.ViewModel = planDocument;

            Loaded += OnLoaded;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called on initial load of this control.
        /// </summary>
        private void OnLoaded(Object sender, RoutedEventArgs routedEventArgs)
        {
            Loaded -= OnLoaded;

            this.xMainGrid.DragScope = Application.Current.MainWindow.Content as FrameworkElement;

            UpdateColumns();
        }

        /// <summary>
        /// Called whenever the column layout manager columns are about to change.
        /// </summary>
        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            // Add extra columns.
            this.xMainGrid.FrozenColumnCount = 1;

            Int32 curIdx = 0;

            //delete row column
            _buttonColumn =
                new DataGridExtendedTemplateColumn
                {
                    CellTemplate = Resources["ProductList_DTButtonsCol"] as DataTemplate,
                    CanUserFilter = false,
                    CanUserSort = false,
                    CanUserReorder = false,
                    CanUserHide = false,
                    CanUserResize = false,
                    ColumnCellAlignment = HorizontalAlignment.Stretch,
                };
            columnSet.Insert(curIdx, _buttonColumn);
            curIdx++;

            if (ViewModel.Highlight != null)
            {
                //highlight column
                _highlightColumn =
                    new DataGridExtendedTemplateColumn
                    {
                        Header = this.ViewModel.Highlight.Name,
                        CellTemplate = Resources["ProductList_DTHighlightCol"] as DataTemplate,
                        CanUserFilter = false,
                        SortMemberPath = ProductListRow.HighlightColourProperty.Path,
                        ColumnCellAlignment = HorizontalAlignment.Stretch
                    };
                columnSet.Insert(curIdx, _highlightColumn);
                curIdx++;
            }
            else
            {
                _highlightColumn = null;
            }

            if (this.ViewModel.PlacementFilterOption != PlacementFilterType.Placed)
            {
                //Position column
                columnSet.Insert(curIdx,
                    ExtendedDataGrid.CreateReadOnlyTextColumn(Message.ProductList_PositionsColHeader,
                ProductListRow.ProductPositionsCountProperty.Path, HorizontalAlignment.Left));
                curIdx++;
            }

        }

        private void ViewModel_PropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ProductListPlanDocument.HighlightProperty.Path)
            {
                if (_columnLayoutManager == null) return;

                _columnLayoutManager.RefreshGrid(/*keepExisting*/true);
            }
            else if (e.PropertyName == ProductListPlanDocument.PlacementFilterOptionProperty.Path)
            {
                UpdateColumns();
            }

        }

        /// <summary>
        /// Called whenever a property changes on the current planogram.
        /// </summary>
        private void PlanogramView_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.ViewModel == null) return;

            if (e.PropertyName == PlanogramView.DisplayUnitsProperty.Path)
            {
                if (_columnLayoutManager == null) return;

                //display units have changed so update the columns to pick up the new settings.
                _columnLayoutManager.DisplayUomCollection = this.ViewModel.Planogram.DisplayUnits;
            }
        }

        /// <summary>
        /// Called when a drag action enters the bounds of this control
        /// </summary>
        protected override void OnDragEnter(DragEventArgs e)
        {
            base.OnDragEnter(e);

            if (DragDropBehaviour.IsUnpackedType<ExtendedDataGridRowDropEventArgs>(e.Data))
            {
                var rowArgs = DragDropBehaviour.UnpackData<ExtendedDataGridRowDropEventArgs>(e.Data);
                if (rowArgs.Items.Count == 0) return;

                if (rowArgs.Items.First() is Product ||
                    rowArgs.Items.First() is ProductListRow)
                    OnProductDragEnter();
            }
            else if (DragDropBehaviour.IsUnpackedType<PlanItemDragDropArgs>(e.Data))
            {
                var args = DragDropBehaviour.UnpackData<PlanItemDragDropArgs>(e.Data);
                Boolean isDraggingProducts = args.DragItems.Any(item => item.PlanItem.Product != null);
                if (isDraggingProducts)
                    OnProductDragEnter();
            }
        }

        /// <summary>
        /// Called when a drop action is performed on this control
        /// </summary>
        protected override void OnDrop(DragEventArgs e)
        {
            base.OnDrop(e);

            if (DragDropBehaviour.IsUnpackedType<ExtendedDataGridRowDropEventArgs>(e.Data))
            {
                var rowArgs = DragDropBehaviour.UnpackData<ExtendedDataGridRowDropEventArgs>(e.Data);
                if (rowArgs.Items.Count <= 0) return;

                if (rowArgs.Items.First() is Product)
                    OnProductLibraryDropped(rowArgs);
                else if (rowArgs.Items.First() is ProductListRow)
                {
                    //if the product row did not come from this grid.
                    if (rowArgs.SourceGrid != this.xMainGrid)
                        OnProductListRowDropped(rowArgs);
                }
                else if (rowArgs.Items.First() is ProductLibraryProduct)
                {
                    // Get the library products from the row args.
                    IEnumerable<ProductLibraryProduct> productLibraryProducts = rowArgs.Items.Cast<ProductLibraryProduct>();

                    // Add the products to the plan.
                    ViewModel.AddProductsToPlanogram(productLibraryProducts);
                }
            }
            else if (DragDropBehaviour.IsUnpackedType<PlanItemDragDropArgs>(e.Data))
            {
                var args = DragDropBehaviour.UnpackData<PlanItemDragDropArgs>(e.Data);
                if (args.DragItems.Count <= 0) return;
                if (args.DragItems.Any(item => item.PlanItem.Product != null))
                {
                    OnPlanItemDropped(args);
                }
            }
        }

        /// <summary>
        /// Called when the user double clicks on a grid row.
        /// </summary>
        private void ExtendedDataGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            this.ViewModel.ShowRowProperties(ViewModel.SelectedRows);
        }

        /// <summary>
        ///     Whenever the <c>View Model</c>'s <see cref="ProductListPlanDocument.SelectedRows" /> collection changes, bring the
        ///     first newly selected row into view, if there is one.
        /// </summary>
        private void OnViewModelSelectedRowsChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems == null) return;
            if (e.Action == NotifyCollectionChangedAction.Add) ScrollIntoView(e.NewItems.Cast<ProductListRow>().First());
        }

        /// <summary>
        ///     Handle the delayed Scroll Into View.
        /// </summary>
        private void ScrollIntoViewHandler(Object sender, EventArgs eventArgs)
        {
            var timer = sender as DispatcherTimer;
            if (timer == null) return;
            timer.Stop();
            lock (timer)
            {
                if (!xMainGrid.IsVisible)
                {
                    timer.Start();
                    return;
                }
                if (_scrollIntoViewRow == null) return;

                xMainGrid.ScrollIntoView(_scrollIntoViewRow);
                _scrollIntoViewRow = null;
            }
        }

        #endregion

        #region Methods

        private void UpdateColumns()
        {
            if (this.ViewModel == null) return;

            CustomColumnLayoutType expectedType =
                (this.ViewModel.PlacementFilterOption == PlacementFilterType.Placed) ?
                CustomColumnLayoutType.PlanogramPosition : CustomColumnLayoutType.Product;

            if (_columnLayoutManager == null
                || _columnLayoutManager.LayoutFactory.LayoutType != expectedType)
            {
                DropCurrentColumnManager(/*update from Grid*/true);

                //load the new manager
                if (expectedType == CustomColumnLayoutType.Product)
                {
                    _columnLayoutManager = new ColumnLayoutManager(
                     new ProductColumnLayoutFactory(typeof(PlanogramProductView), this.ViewModel.Planogram.Model),
                     this.ViewModel.Planogram.DisplayUnits,
                     ProductListPlanDocument.ScreenKey, /*PathMask*/"PlanItem.Product.{0}");
                }
                else
                {
                    _columnLayoutManager = new ColumnLayoutManager(
                     new PlanogramPositionColumnLayoutFactory(typeof(PlanogramPositionView), this.ViewModel.Planogram.Model),
                     this.ViewModel.Planogram.DisplayUnits, ProductListPlanDocument.ScreenKey, /*PathMask*/"PlanItem.{0}");
                    _columnLayoutManager.IncludeUnseenColumns = false; //off for performance
                }

                _columnLayoutManager.CalculatedColumnPath = "PlanItem.CalculatedValueResolver";
                _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
                _columnLayoutManager.AttachDataGrid(this.xMainGrid);
            }
        }

        private void DropCurrentColumnManager(Boolean updateFromGrid)
        {
            if (_columnLayoutManager != null)
            {
                if (updateFromGrid)
                {
                    _columnLayoutManager.UpdateColumnLayoutFromGrid();
                }


                _columnLayoutManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                _columnLayoutManager.Dispose();
                _columnLayoutManager = null;
            }
        }

        /// <summary>
        ///     Add the given <paramref name="item"/> to the be scrolled into view with a delay.
        /// </summary>
        /// <param name="item">The <see cref="ProductListRow"/> to be scrolled to when the delay is up.</param>
        private void ScrollIntoView(ProductListRow item)
        {
            //  Temporarily stop the timer and add the new item for delayed scrolling into view.
            _scrollIntoViewTimer.Stop();
            lock (_scrollIntoViewTimer)
            {
                _scrollIntoViewRow = item;
            }
            _scrollIntoViewTimer.Start();
        }

        #region ProductLibrary Drag/Drop

        private static void OnProductDragEnter()
        {
            //update the drag adorner to show valid drop
            PlanDocumentHelper.SetDragAdorner(ImageResources.ProductLibrary_ValidDragOverAdorner);
        }

        private void OnProductLibraryDropped(ExtendedDataGridRowDropEventArgs args)
        {
            ViewModel.AddProductsToPlanogram(args.Items.Cast<Product>());
        }

        /// <summary>
        ///     Whenever a a drag drop operation originating in a Product List ends.
        /// </summary>
        /// <param name="args"><see cref="ProductListRow" /> items being dragged and dropped.</param>
        private void OnProductListRowDropped(ExtendedDataGridRowDropEventArgs args)
        {
            ViewModel.CopyProductsToPlanogram(args.Items.Cast<ProductListRow>());
        }

        private void OnPlanItemDropped(PlanItemDragDropArgs args)
        {
            ViewModel.CopyPlanogramProductsToPlanogram(args.DragItems.Where(item => item.PlanItem.Product != null).Select(item => item.PlanItem.Product.Model));
        }
        #endregion

        #endregion

        #region IPlanDocumentView Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean isDisposing)
        {
            if (_isDisposed) return;

            _isDisposed = true;

            this.ViewModel = null;

            DropCurrentColumnManager(/*updateFromGrid*/false);

            if (_scrollIntoViewTimer != null) _scrollIntoViewTimer.Stop();
        }

        public IPlanDocument GetIPlanDocument()
        {
            return ViewModel;
        }

        #endregion
    }
}