﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// L.Ineson 
//  Created.
// V8-25395  : A.Probyn
//   Changed over from ProductInfo to Product
// V8-24124 : A.Silva
//  Added support for Custom Column Layouts.
//  Added a delete column per row. Removed the icon on top.
//  Added support to add products from other plans.
//  Extracted interface ICustomColumnLayoutView to support Custom Column Layouts.
//  Added restoring filters from the custom column layout.
// V8-25797 : A.Silva 
//  Modified GetColumnSet to make sure the path is preappended with "Product."
// V8-26143 : A.Silva 
//  Changed to use the Extended DataGrid context emnu instead of the UI ribbon button.
// V8-26155 : A.Silva 
//  Made sure the type of custom column layout is Product.
// V8-26171 : A.Silva 
//  Added CustomColumnManager and removed ICustomColumnLayoutView.
// V8-26281 : L.Ineson
//  Tidied up.
// V8-26329 : A.Silva ~ Added _columnLayoutManager_CurrentColumnLayoutEdited eventhandler.
// V8-26370 : A.Silva ~ Corrected disposing of _columnLayoutManager.
// V8-26368 : A.Silva ~ Added code to filter shown products by placement count.
// V8-26633 : A.Silva ~ Moved out PlacementFilterType to its own file.
// V8-26472 : A.Probyn ~ Updated reference to ColumnLayoutManager Constructor
// V8-27149 : A.Silva ~ Moved ColumnLayoutManager to the View.
// V8-27140 : A.Silva ~ Amended SelectedRows_CollectionChanged() to make sure it keeps products selected when multiselecting.
// V8-25209 : L.Ineson
//  Highlights are no longer lost when saving.
// V8-27825 : L.Ineson
//  Added document freezing
// V8-27900 : L.Ineson
//  Moved highlight column ui code into the code behind class.
// V8-28345 : L.Ineson
//  Reworked how highlights are processed and applied to improve performance.
// V8-28356 : A.Kuszyk
//  Added overload to AddProductsToPlanogram for ProductLibraryProducts.

#endregion

#region Version History: (CCM 8.01)

// V8-28770 : L.Ineson
//  Rows get reloaded when a filter is in place and positions change.

#endregion

#region Version History : CCM 8.02

// V8-25955 : D.Pleasance
//  Removed PositionProductRowCommand
// V8-27491 : I.George
//  Changed the small icon image in EditProductRowCommand so that it matches the product Properties icon

#endregion

#region Version History : CCM 8.10

// V8-29916 : L.Ineson
//  Stopped highlights crash.

#endregion

#region Version History : CCM811

// V8-30328 : A.Silva
//  Amended SelectedRows_CollectionChanged so that when adding new selection rows only the new are synchronized on PlanDocument<T>.SelectedPlanItems
// V8-30358 : J.Pickup
//  Small performance improvement to SelectedRows_CollectionChanged when enumerating all positions
// V8-30346 : A.Silva
//  Amended a selected row syncing issue when reselecting a selected row would cause the whole selection to be persisted.
// V8-28688 : A.Probyn
//  Updated reference to PositionPropertiesViewModel to take a new constructor depending on where it is called from.

#endregion

#region Version History : CCM820
// V8-31432 : J.Pickup
//  Changes to the SyncAddHandler etc in order to account for modifier keys. 
#endregion

#region Version History : CCM830
// V8-32121 : L.Ineson
//  Placed products filter option now shows position rows.
//V8-32248 : L.Ineson
//  Stopped this document from processing updates when not in view.
//V8-32293 : L.Ineson
//  Corrected issues with reload when switching between row types.
// V8-32322 : A.Silva
//  Added CopyPlanogramProductsToPlanogram to copy PlanogramProducts to the products in the planogram.
//  Refactored CopyProductsToPlanogram to use CopyPlanogramProductsToPlanogram to avoid code duplication.

#endregion

#endregion

using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf.HelperClasses;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.ProductsList
{
    /// <summary>
    ///     Plan document controller for providing a view of products on a planogram
    /// </summary>
    public sealed class ProductListPlanDocument : PlanDocument<ProductListPlanDocumentView>
    {
        #region Constants

        /// <summary>
        ///     Type of column layout used by the <see cref="ColumnLayoutManager" /> in this instance.
        /// </summary>
        public const CustomColumnLayoutType ColumnLayoutType = CustomColumnLayoutType.Product;

        /// <summary>
        ///     Name of the column layout preferred by the user to be used by the <see cref="ColumnLayoutManager" /> in this
        ///     instance.
        /// </summary>
        public const String ScreenKey = "ProductListPlanDocument";

        #endregion

        #region Fields

        private readonly BulkObservableCollection<PlanogramHighlightResultGroup> _highlightLegendItems = new BulkObservableCollection<PlanogramHighlightResultGroup>();

        private readonly List<ProductListRow> _masterRows = new List<ProductListRow>();
        private readonly BulkObservableCollection<ProductListRow> _rows = new BulkObservableCollection<ProductListRow>();
        private ReadOnlyBulkObservableCollection<ProductListRow> _rowsRo;
        private readonly ObservableCollection<ProductListRow> _selectedRows = new ObservableCollection<ProductListRow>();

        private ReadOnlyBulkObservableCollection<PlanogramHighlightResultGroup> _highlightLegendItemsRo;
        private Boolean _isSyncingSelection;

        private PlacementFilterType _placementFilterOption = PlacementFilterType.All;
        private readonly DispatcherTimer _syncAddTimer; //instance to control delayed syncing of newly selected rows.
        private readonly List<IPlanItem> _syncAddQueue = new List<IPlanItem>();//The queue of newly selected plan items to be synced.
        private readonly DispatcherTimer _syncRemoveTimer; //instance to control delayed syncing of newly deselected rows.
        private readonly List<IPlanItem> _syncRemoveQueue = new List<IPlanItem>();//The queue of newly deselected plan items to be synced.

        private Boolean _isRowReloadRequired;
        private Boolean _isHighlightUpdateRequired;
        private Boolean _isSelectionResyncRequired;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath DeleteProductCommandProperty = WpfHelper.GetPropertyPath<ProductListPlanDocument>(p => p.DeleteProductCommand);
        public static readonly PropertyPath PlacementFilterOptionProperty = WpfHelper.GetPropertyPath<ProductListPlanDocument>(o => o.PlacementFilterOption);
        public static readonly PropertyPath RowsProperty = WpfHelper.GetPropertyPath<ProductListPlanDocument>(p => p.Rows);
        public static readonly PropertyPath SelectedRowsProperty = WpfHelper.GetPropertyPath<ProductListPlanDocument>(p => p.SelectedRows);


        #endregion

        #region Properties

        public override DocumentType DocumentType
        {
            get { return DocumentType.ProductList; }
        }

        /// <summary>
        ///     Returns the collection of legend items processed for the current highlight.
        /// </summary>
        public override ReadOnlyBulkObservableCollection<PlanogramHighlightResultGroup> HighlightLegendItems
        {
            get
            {
                if (_highlightLegendItemsRo == null)
                {
                    _highlightLegendItemsRo =
                        new ReadOnlyBulkObservableCollection<PlanogramHighlightResultGroup>(_highlightLegendItems);
                }
                return _highlightLegendItemsRo;
            }
        }

        /// <summary>
        ///     Gets or sets what type of placement filter option the user has selected for the product list.
        /// </summary>
        public PlacementFilterType PlacementFilterOption
        {
            get { return _placementFilterOption; }
            set
            {
                if (value == _placementFilterOption) return;

                _placementFilterOption = value;
                OnPropertyChanged(PlacementFilterOptionProperty);

                this.RemoveProductRowCommand.FriendlyName =
                    (this.PlacementFilterOption == PlacementFilterType.Placed) ?
                      Message.ProductList_RemovePositionRow
                    : Message.ProductList_RemoveProductRow;


                OnReloadRowsRequired();
            }
        }

        /// <summary>
        ///     Returns the readonly collection of rows to display
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductListRow> Rows
        {
            get
            {
                if (_rowsRo == null)
                {
                    _rowsRo = new ReadOnlyBulkObservableCollection<ProductListRow>(_rows);
                }
                return _rowsRo;
            }
        }

        /// <summary>
        ///     Returns the editable collection of selected items.
        /// </summary>
        /// <remarks>This is essentially a filtered version of the selected plan items collection.</remarks>
        public ObservableCollection<ProductListRow> SelectedRows
        {
            get { return _selectedRows; }
        }

        /// <summary>
        ///     Returns the document title.
        /// </summary>
        public override String Title
        {
            get { return DocumentTypeHelper.FriendlyNames[DocumentType.ProductList]; }
        }

        #endregion

        #region Constructor

        /// <summary>
        ///     Creates a new instance of this type.
        /// </summary>
        /// <param name="parentController"></param>
        public ProductListPlanDocument(PlanControllerViewModel parentController)
            : base(parentController)
        {
            //  Initialize the sync timers for the product list.
            _syncAddTimer = new DispatcherTimer(
                TimeSpan.FromMilliseconds(10),
                DispatcherPriority.Background,
                SyncAddHandler,
                Dispatcher.CurrentDispatcher);
            _syncRemoveTimer = new DispatcherTimer(
                TimeSpan.FromMilliseconds(10),
                DispatcherPriority.Background,
                SyncRemoveHandler,
                Dispatcher.CurrentDispatcher);

            //initialise document settings.
            IsHighlightingSupported = true;

            //add rows
            OnReloadRowsRequired();

            this.SelectedPlanItems_BulkCollectionChanged(SelectedPlanItems,
                new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));

            //attach events
            SetPlanEventHandlers(true);
        }

        #endregion

        #region Commands

        #region DeleteProduct Command

        private RelayCommand _deleteProductCommand;

        /// <summary>
        ///     Deletes the selected products and all releated positions from the planogram
        /// </summary>
        public RelayCommand DeleteProductCommand
        {
            get
            {
                if (_deleteProductCommand == null)
                {
                    _deleteProductCommand = new RelayCommand(
                        p => DeleteProduct_Executed(),
                        p => DeleteProduct_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        InputGestureKey = Key.Delete,
                        SmallIcon = ImageResources.Delete_14
                    };
                    ViewModelCommands.Add(_deleteProductCommand);
                }
                return _deleteProductCommand;
            }
        }

        private Boolean DeleteProduct_CanExecute()
        {
            return SelectedRows.Count != 0;
        }

        private void DeleteProduct_Executed()
        {
            Planogram.BeginUndoableAction();

            foreach (var row in SelectedRows.ToList())
            {
                Planogram.RemovePlanItem(row.PlanItem);
            }

            Planogram.EndUndoableAction();
        }

        #endregion

        #region RemoveProductRow Command

        private RelayCommand<ProductListRow> _removeProductRowCommand;

        public RelayCommand<ProductListRow> RemoveProductRowCommand
        {
            get
            {
                if (_removeProductRowCommand != null) return _removeProductRowCommand;

                _removeProductRowCommand = new RelayCommand<ProductListRow>(RemoveProductRowExecute,
                    RemoveProductRowCanExecute)
                {
                    FriendlyName = Message.ProductList_RemoveProductRow,
                    SmallIcon = ImageResources.Delete_14
                };
                ViewModelCommands.Add(_removeProductRowCommand);
                return _removeProductRowCommand;
            }
        }

        private bool RemoveProductRowCanExecute(ProductListRow productListRow)
        {
            return true;
        }

        private void RemoveProductRowExecute(ProductListRow productListRow)
        {
            Planogram.BeginUndoableAction();

            Planogram.RemovePlanItem(productListRow.PlanItem);

            Planogram.EndUndoableAction();
        }

        #endregion

        #region EditProductRow Command

        private RelayCommand<ProductListRow> _editProductRowCommand;

        public RelayCommand<ProductListRow> EditProductRowCommand
        {
            get
            {
                if (_editProductRowCommand != null) return _editProductRowCommand;

                _editProductRowCommand = new RelayCommand<ProductListRow>(EditProductRowExecute,
                    EditProductRowCanExecute)
                {
                    FriendlyName = MainPageCommands.ShowSelectedItemProperties.FriendlyName,
                    SmallIcon = ImageResources.PositionContext_EditProduct_16
                };
                ViewModelCommands.Add(_editProductRowCommand);
                return _editProductRowCommand;
            }
        }

        private Boolean EditProductRowCanExecute(ProductListRow productListRow)
        {
            return true;
        }

        private void EditProductRowExecute(ProductListRow productListRow)
        {
            ShowRowProperties(new List<ProductListRow> { productListRow });
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Called whenever a property of this viewmodel changes value.
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void OnPropertyChanged(String propertyName)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName == HighlightProperty.Path)
            {
                OnUpdateHighlight();
            }
        }

        /// <summary>
        ///     Called whenever a child changes on the planogram
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Planogram_ChildChanged(object sender, ViewModelChildChangedEventArgs e)
        {
            if (e.CollectionChangedArgs == null) return; // There are no changes to the collection.

            OnReloadRowsRequired();
        }

        /// <summary>
        ///     Called when the items held by the selected plan items changes.
        /// </summary>
        private void SelectedPlanItems_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            //return out if already syncing.
            if (_isSyncingSelection) return;

            //just flag a refresh if this document is not visible.
            if (!this.IsVisible)
            {
                _isSelectionResyncRequired = true;
                return;
            }



            _isSyncingSelection = true;

            Boolean isProduct = this.PlacementFilterOption != PlacementFilterType.Placed;

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (IPlanItem item in e.ChangedItems)
                        {
                            ProductListRow row = null;
                            if (isProduct && item.Product != null)
                            {
                                row = Rows.FirstOrDefault(r => r.PlanItem == item.Product);
                            }
                            else if (!isProduct && item.Position != null)
                            {
                                row = Rows.FirstOrDefault(r => r.PlanItem == item.Position);
                            }

                            if (row == null || SelectedRows.Contains(row)) continue;
                            this.SelectedRows.Add(row);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        foreach (IPlanItem item in e.ChangedItems)
                        {
                            ProductListRow row = null;
                            if (isProduct && item.Product != null)
                            {
                                row = this.Rows.FirstOrDefault(r => r.PlanItem == item.Product);
                            }
                            else if (!isProduct && item.Position != null)
                            {
                                row = this.Rows.FirstOrDefault(r => r.PlanItem == item.Position);
                            }
                            this.SelectedRows.Remove(row);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        if (this.SelectedRows.Any()) this.SelectedRows.Clear();

                        foreach (IPlanItem item in this.SelectedPlanItems)
                        {
                            ProductListRow row = null;
                            if (isProduct && item.Product != null)
                            {
                                row = this.Rows.FirstOrDefault(r => r.PlanItem == item.Product);
                            }
                            else if (!isProduct && item.Position != null)
                            {
                                row = this.Rows.FirstOrDefault(r => r.PlanItem == item.Product);
                            }

                            if (row == null || this.SelectedRows.Contains(row)) continue;
                            this.SelectedRows.Add(row);
                        }
                    }
                    break;
            }

            _isSyncingSelection = false;

        }

        /// <summary>
        ///     Whenever the <see cref="SelectedRows" /> collection changes, synchronize
        ///     <see cref="PlanDocument{T}.SelectedPlanItems" /> so both have the same <c>Products</c> and <c>Positions</c>
        ///     selected.
        /// </summary>
        private void SelectedRows_CollectionChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            //  Do not synchronize from the SelectedRows as a Synchornization action is already taking place.
            if (_isSyncingSelection) return;

            //just flag a refresh if this document is not visible.
            if (!this.IsVisible)
            {
                _isSelectionResyncRequired = true;
                return;
            }



            _isSyncingSelection = true;

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        //  Add the new items to the SyncAddQueue.
                        SyncAddQueue(e.NewItems.Cast<ProductListRow>().Where(row => row.PlanItem != null).Select(row => row.PlanItem));
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        //  Add the old items to the SyncRemoveQueue.
                        SyncRemoveQueue(e.OldItems.Cast<ProductListRow>().Select(row => row.PlanItem));
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        // NB We want to clear the delayed additions and removals as we are clearing the collection anyway.
                        ClearSyncAdd();
                        ClearSyncRemove();

                        // We mustn't deselect any non positional items from here when clearing...
                        List<IPlanItem> nonPositionItemsToReintroduce = SelectedPlanItems.Where(p => p.PlanItemType != PlanItemType.Position).ToList();

                        SelectedPlanItems.Clear();

                        List<IPlanItem> itemsToSelect = SelectedRows.Select(r => r.PlanItem).ToList();

                        List<PlanogramPositionView> positionsToSelect =
                            Planogram.EnumerateAllPositions().Where(i => itemsToSelect.Contains(i.Product)).ToList();

                        itemsToSelect = new List<IPlanItem>();
                        itemsToSelect.AddRange(positionsToSelect);
                        itemsToSelect.AddRange(SelectedRows.Where(p => p.ProductPositionsCount == 0).Select(p => p.PlanItem));
                        itemsToSelect.AddRange(nonPositionItemsToReintroduce);

                        SelectedPlanItems.AddRange(itemsToSelect);
                    }
                    break;
            }

            _isSyncingSelection = false;
        }

        /// <summary>
        /// Called whenever the visibility of this document changes.
        /// </summary>
        protected override void OnIsVisibleChanged()
        {
            base.OnIsVisibleChanged();

            if (this.IsVisible)
            {
                //clear any selection sync items.
                ClearSyncAdd();
                ClearSyncRemove();

                if (_isRowReloadRequired)
                {
                    OnReloadRowsRequired();
                }

                if (_isHighlightUpdateRequired)
                {
                    OnUpdateHighlight();
                }

                if (_isSelectionResyncRequired)
                {
                    SelectedPlanItems_BulkCollectionChanged(this.SelectedPlanItems, 
                        new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));
                }
            }
        }

        /// <summary>
        /// Called when the plan has finished processing.
        /// </summary>
        public override void OnPlanProcessCompleting()
        {
            base.OnPlanProcessCompleting();

            OnUpdateHighlight();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Subscribes and unsubscribes event handlers
        /// </summary>
        /// <param name="isAttach"></param>
        private void SetPlanEventHandlers(Boolean isAttach)
        {
            if (isAttach)
            {
                this.SelectedPlanItems.BulkCollectionChanged += SelectedPlanItems_BulkCollectionChanged;
                this.SelectedRows.CollectionChanged += SelectedRows_CollectionChanged;

                this.Planogram.ChildChanged += Planogram_ChildChanged;
            }
            else
            {
                this.SelectedPlanItems.BulkCollectionChanged -= SelectedPlanItems_BulkCollectionChanged;
                this.SelectedRows.CollectionChanged -= SelectedRows_CollectionChanged;

                this.Planogram.ChildChanged -= Planogram_ChildChanged;
            }
        }

        /// <summary>
        /// Called whenever this document should
        /// stop updating
        /// </summary>
        protected override void OnFreeze()
        {
            SetPlanEventHandlers(false);
        }

        /// <summary>
        /// Called whenever this document should
        /// resume updating.
        /// </summary>
        protected override void OnUnfreeze()
        {
            SetPlanEventHandlers(true);

            //update data
            OnReloadRowsRequired();
            OnUpdateHighlight();
        }

        /// <summary>
        /// Adds the given enumeration of Product Library Products to the plan, 
        /// updating their Performance Data in the plan.
        /// </summary>
        /// <param name="productLibraryProducts">The Product Library Products to add.</param>
        public void AddProductsToPlanogram(IEnumerable<ProductLibraryProduct> productLibraryProducts)
        {
            // Add the products to the plan.
            AddProductsToPlanogram(productLibraryProducts.Select(p => p.Product));

            // Build a dictionary of planogram products.
            if (this.Planogram == null || this.Planogram.Model == null || this.Planogram.Model.Products == null) return;
            Dictionary<String, PlanogramProduct> productsByGtin = this.Planogram.Model.Products.
                ToDictionary(p => p.Gtin, p => p, StringComparer.InvariantCultureIgnoreCase);

            // Ensure any performance data is copied over as well.
            foreach (ProductLibraryProduct productLibraryProduct in productLibraryProducts)
            {
                PlanogramProduct planogramProduct;
                if (productsByGtin.TryGetValue(productLibraryProduct.Gtin, out planogramProduct))
                {
                    ParentController.CopyPerformanceData(productLibraryProduct, planogramProduct);
                }
            }
        }

        /// <summary>
        ///     Adds the given products to the planogram.
        /// </summary>
        /// <param name="products"></param>
        public void AddProductsToPlanogram(IEnumerable<Product> products)
        {
            ShowWaitCursor(true);

            PlanogramView plan = Planogram;
            plan.BeginUndoableAction();

            List<PlanogramProductView> addedProducts = plan.AddProducts(products);

            plan.EndUndoableAction();

            //select the new products
            if (SelectedPlanItems.Count > 0)
            {
                SelectedPlanItems.Clear();
            }
            SelectedPlanItems.AddRange(addedProducts);

            ShowWaitCursor(false);
        }

        /// <summary>
        /// Copies the row products to this planogram
        /// </summary>
        /// <param name="rows">rows to be added.</param>
        /// <remarks>Cannot copy positions as don't know which component to put them on.</remarks>
        public void CopyProductsToPlanogram(IEnumerable<ProductListRow> rows)
        {
            if (rows == null)
            {
                Debug.Fail("A null collection of products was passed on to ProductListPlanDocument.CopyPlanogramProductsToPlanogram()");
                return;
            }
            CopyPlanogramProductsToPlanogram(rows.Select(row => row.PlanItem.Product.Model));
        }

        /// <summary>
        ///     Copies the <see cref="PlanogramProduct"/> items in the <paramref name="products"/> collection into this Product List.
        /// </summary>
        /// <param name="products">Collection of <see cref="PlanogramProduct"/> items that will be copied.</param>
        /// <remarks>
        ///     If the products already exist (determined by their GTIN) they will <c>not</c> be copied.<para />
        ///     Any newly added products will be selected on the Product List if anything but <c>Placed Products</c> is being displayed.
        /// </remarks>
        public void CopyPlanogramProductsToPlanogram(IEnumerable<PlanogramProduct> products)
        {
            if (products == null)
            {
                Debug.Fail("A null or collection of products was passed on to ProductListPlanDocument.CopyPlanogramProductsToPlanogram()");
                return;
            }

            List<PlanogramProductView> addedItems;
            ShowWaitCursor(true);
            {
                PlanogramView plan = Planogram;
                IEnumerable<String> existingGtin = plan.Products.Select(view => view.Gtin);

                plan.BeginUndoableAction(String.Empty);
                addedItems = plan.AddProducts(products.Where(product => !existingGtin.Contains(product.Gtin)));
                plan.EndUndoableAction();
            }
            ShowWaitCursor(false);

            //  Select the new products if the row type is product.
            if (PlacementFilterOption == PlacementFilterType.Placed) return;

            if (SelectedPlanItems.Any()) SelectedPlanItems.Clear();
            SelectedPlanItems.AddRange(addedItems);
        }

        /// <summary>
        /// Shows the product properties window for the given products.
        /// </summary>
        public void ShowRowProperties(IEnumerable<ProductListRow> rows)
        {
            if (!rows.Any()) return;

            PlanItemSelection selection = new PlanItemSelection(Planogram);
            Boolean isProduct = (rows.First().PlanItem.PlanItemType == PlanItemType.Product);

            selection.AddRange(
                (isProduct) ? rows.Select(p => p.PlanItem.Product) : rows.Select(p => p.PlanItem));


            GetWindowService().ShowDialog<PositionPropertiesWindow>(
                    new PositionPropertiesViewModel(Planogram, selection, /*isProductSelected*/isProduct));

            selection.Dispose();
        }

        /// <summary>
        /// Initializes and loads rows according to the selected
        /// placement filter type.
        /// </summary>
        private void OnReloadRowsRequired()
        {
            if (this.IsVisible)
            {
                _isRowReloadRequired = false;

                if (this.PlacementFilterOption != PlacementFilterType.Placed)
                {
                    ReloadProductRows();
                }
                else
                {
                    ReloadPositionRows();
                }
            }
            else
            {
                _isRowReloadRequired = true;
            }
        }

        /// <summary>
        ///     Initializes and loads the product rows from the planogram.
        /// </summary>
        private void ReloadProductRows()
        {
            //get the list of products in the plan
            if (_masterRows.Count == 0 || _masterRows.Any(p=> p.PlanItem.PlanItemType != PlanItemType.Product))
            {
                if (_masterRows.Any()) _masterRows.Clear();

                foreach (PlanogramProductView p in this.Planogram.Products.OrderBy(p => p.Gtin))
                {
                    _masterRows.Add(new ProductListRow(p));
                }
            }
            else
            {
                List<PlanogramProductView> products = this.Planogram.Products.OrderBy(p=> p.Gtin).ToList();

                //remove any old rows.
                foreach (ProductListRow row in _masterRows.ToArray())
                {
                    if (!products.Contains(row.PlanItem.Product))
                    {
                        _masterRows.Remove(row);
                    }
                    else
                    {
                        //update the existing row.
                        row.Refresh();
                        products.Remove(row.PlanItem.Product);
                    }
                }

                //add in new ones
                foreach (PlanogramProductView newProduct in products)
                {
                    _masterRows.Add(new ProductListRow(newProduct));
                }
            }

            //update the displayed list
            List<ProductListRow> rowsToDisplay;
            switch (PlacementFilterOption)
            {
                case PlacementFilterType.Placed:
                    rowsToDisplay = _masterRows.Where(row => row.ProductPositionsCount > 0).ToList();
                    break;
                case PlacementFilterType.Unplaced:
                    rowsToDisplay = _masterRows.Where(row => row.ProductPositionsCount == 0).ToList();
                    break;
                default:
                    rowsToDisplay = _masterRows.ToList();
                    break;
            }

            //remove any old rows from the display list
            foreach (ProductListRow row in _rows.ToArray())
            {
                if (!rowsToDisplay.Contains(row))
                {
                    _rows.Remove(row);
                }
                else
                {
                    rowsToDisplay.Remove(row);
                }
            }
            _rows.AddRange(rowsToDisplay);
        }

        /// <summary>
        /// Initializes and loads position rows for the planogram
        /// </summary>
        private void ReloadPositionRows()
        {
            List<IPlanItem> selectedItems = this.SelectedRows.Select(p => p.PlanItem).ToList();


            //get the list of products in the plan
            if (_masterRows.Count == 0)
            {
                foreach (PlanogramPositionView p in this.Planogram.EnumerateAllPositions().OrderBy(p=> p.Product.Gtin))
                {
                    _masterRows.Add(new ProductListRow(p));
                }
            }
            else
            {
                List<PlanogramPositionView> positions = this.Planogram.EnumerateAllPositions().OrderBy(p => p.Product.Gtin).ToList();

                //remove any old rows.
                foreach (ProductListRow row in _masterRows.ToArray())
                {
                    if (!positions.Contains(row.PlanItem.Position))
                    {
                        _masterRows.Remove(row);
                    }
                    else
                    {
                        //update the existing row.
                        row.Refresh();
                        positions.Remove(row.PlanItem.Position);
                    }
                }

                //add in new ones
                foreach (PlanogramPositionView newItem in positions)
                {
                    _masterRows.Add(new ProductListRow(newItem));
                }
            }

            //update the displayed list
            _rows.RaiseListChangedEvents = false;
            _rows.Clear();
            _rows.AddRange(_masterRows);
            _rows.RaiseListChangedEvents = true;
            _rows.Reset();


            //reupdate the selected rows.
            if (this.SelectedRows.Any()) this.SelectedRows.Clear();
            foreach (ProductListRow r in _rows)
            {
                if (selectedItems.Contains(r.PlanItem))
                    this.SelectedRows.Add(r);
            }
        }

        /// <summary>
        /// Processes the keyboard shortcut against any document 
        /// specific actions.
        /// </summary>
        /// <param name="modifiers"></param>
        /// <param name="key"></param>
        public override Boolean ProcessKeyboardShortcut(ModifierKeys modifiers, Key key)
        {
            if (key == DeleteProductCommand.InputGestureKey && DeleteProduct_CanExecute())
            {
                DeleteProductCommand.Execute();
                return true;
            }

            return base.ProcessKeyboardShortcut(modifiers, key);
        }

        /// <summary>
        /// Notifies this highlight that it should update its highlight results.
        /// </summary>
        private void OnUpdateHighlight()
        {
            if (this.IsVisible)
            {
                _isHighlightUpdateRequired = false;

                if (_highlightLegendItems.Count > 0) _highlightLegendItems.Clear();

                if (this.Highlight != null)
                {
                    PlanogramHighlightResult result = this.ParentController.GetHighlightResult(this.Highlight);
                    Debug.Assert(result.Highlight == this.Highlight, "Incorrect highlight result given to document");

                    //Update legend items.
                    if (_highlightLegendItems.Count > 0) _highlightLegendItems.Clear();
                    _highlightLegendItems.AddRange(result.Groups);

                    //reset all rows to white
                    foreach (var row in this.Rows)
                    {
                        row.HighlightColour = ColorHelper.White;
                        row.HighlightTooltip = null;
                    }


                    if (result == null || !result.Groups.Any()) return;

                    //Apply highlight colours
                    foreach (PlanogramHighlightResultGroup highlightGroup in HighlightLegendItems)
                    {
                        foreach (PlanogramPosition item in highlightGroup.Items)
                        {
                            foreach (ProductListRow row in
                                this.Rows.Where(p => Object.Equals(p.PlanItem.Product.Model.Id, item.PlanogramProductId)))
                            {
                                row.HighlightColour = highlightGroup.Colour;
                                row.HighlightTooltip = highlightGroup.Label;
                            }
                        }
                    }

                }
                else
                {
                    //reset all rows to white
                    foreach (var row in Rows)
                    {
                        row.HighlightColour = ColorHelper.White;
                        row.HighlightTooltip = null;
                    }
                }
            }
            else
            {
                _isHighlightUpdateRequired = true;
            }
        }


        #region SelectionSync

        /// <summary>
        ///     Clear the sync add queue in case it is no longer needed (when resetting the collection with pending changes, for example.)
        /// </summary>
        private void ClearSyncAdd()
        {
            if (_syncAddTimer == null) return;
            _syncAddTimer.Stop();
            lock (_syncAddTimer)
            {
                _syncAddQueue.Clear();
            }
        }

        /// <summary>
        ///     Clear the sync remove queue in case it is no longer needed (when resetting the collection with pending changes, for example)
        /// </summary>
        private void ClearSyncRemove()
        {
            if (_syncRemoveTimer == null) return;
            _syncRemoveTimer.Stop();
            lock (_syncRemoveTimer)
            {
                _syncRemoveQueue.Clear();
            }
        }

        /// <summary>
        ///     Handle the delayed Synchronize Added items.
        /// </summary>
        private void SyncAddHandler(Object sender, EventArgs eventArgs)
        {
            var timer = sender as DispatcherTimer;
            if (timer == null) return;
            timer.Stop();
            lock (timer)
            {
                //  Make sure the collection is synced only once from here... so set the is syncing flag.
                _isSyncingSelection = true;
                IEnumerable<IPlanItem> productPositions =
                    Planogram.EnumerateAllPositions().Where(view => _syncAddQueue.Contains(view.Product)).ToList();

                if (!Keyboard.IsKeyDown(Key.LeftCtrl) && !Keyboard.IsKeyDown(Key.RightCtrl) && !Keyboard.IsKeyDown(Key.LeftShift) && !Keyboard.IsKeyDown(Key.RightShift))
                {
                    SelectedPlanItems.Clear();
                }

                SelectedPlanItems.AddRange(_syncAddQueue.Concat(productPositions));


                _syncAddQueue.Clear();
                _isSyncingSelection = false;
            }
        }

        /// <summary>
        ///     Handle the delayed Synchronize Remove items.
        /// </summary>
        private void SyncRemoveHandler(Object sender, EventArgs eventArgs)
        {
            var timer = sender as DispatcherTimer;
            if (timer == null) return;
            timer.Stop();
            lock (timer)
            {
                //  Make sure the collection is synced only once from here... so set the is syncing flag.
                _isSyncingSelection = true;
                IEnumerable<IPlanItem> productPositions =
                    Planogram.EnumerateAllPositions().Where(view => _syncRemoveQueue.Contains(view.Product)).ToList();
                SelectedPlanItems.RemoveRange(_syncRemoveQueue.Concat(productPositions));
                _syncRemoveQueue.Clear();
                _isSyncingSelection = false;
            }
        }


        /// <summary>
        ///     Add the given <paramref name="items"/> to the <see cref="_syncAddQueue"/> collection and start the <see cref="_syncAddTimer"/>.
        /// </summary>
        /// <param name="items">The <see cref="PlanogramPositionView"/> items to be added with a delay.</param>
        private void SyncAddQueue(IEnumerable<IPlanItem> items)
        {
            //  Temporarily stop the timer and add the new items for delayed syncing.
            _syncAddTimer.Stop();
            lock (_syncAddTimer)
            {
                _syncAddQueue.AddRange(items);
            }
            _syncAddTimer.Start();
        }

        /// <summary>
        ///     Add the given <paramref name="items"/> to the <see cref="_syncRemoveQueue"/> collection and start the <see cref="_syncRemoveTimer"/>.
        /// </summary>
        /// <param name="items">The <see cref="PlanogramPositionView"/> items to be removed with a delay.</param>
        private void SyncRemoveQueue(IEnumerable<IPlanItem> items)
        {
            //  Temporarily stop the timer and add the old items for delayed syncing.
            _syncRemoveTimer.Stop();
            lock (_syncRemoveTimer)
            {
                _syncRemoveQueue.AddRange(items);
            }
            _syncRemoveTimer.Start();
        }


        #endregion

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!IsDisposed)
            {
                SetPlanEventHandlers(false);
                base.Dispose(disposing);
                if (_syncAddTimer != null) _syncAddTimer.Stop();
                if (_syncRemoveTimer != null) _syncRemoveTimer.Stop();

                IsDisposed = true;
            }
        }

        #endregion

    }

    
}
