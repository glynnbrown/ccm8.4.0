﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Ineson 
//  Created.
// V8-26408 : L.Ineson
//  Implemented properly
// V8-27825 : L.Ineson
//  Added document freezing
#endregion

#endregion

using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    public enum FixtureListViewType
    {
        Components,
        Assemblies,
        Bays
    }

    /// <summary>
    /// Plan document controller for providing a view of fixtures on a planogram
    /// </summary>
    public sealed class FixtureListPlanDocument : PlanDocument<FixtureListPlanDocumentView>
    {
        #region Fields

        private Boolean _isSynchingSelection;
        private FixtureListViewType _viewType = FixtureListViewType.Components;

        private readonly BulkObservableCollection<IPlanItem> _itemRows = new BulkObservableCollection<IPlanItem>();
        private ReadOnlyBulkObservableCollection<IPlanItem> _itemRowsRO;

        private readonly ObservableCollection<IPlanItem> _selectedRows = new ObservableCollection<IPlanItem>();

       
        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath ViewTypeProperty = WpfHelper.GetPropertyPath<FixtureListPlanDocument>(p => p.ViewType);
        public static readonly PropertyPath ItemRowsProperty = WpfHelper.GetPropertyPath<FixtureListPlanDocument>(p => p.ItemRows);
        public static readonly PropertyPath SelectedRowsProperty = WpfHelper.GetPropertyPath<FixtureListPlanDocument>(p => p.SelectedRows);

        #endregion

        #region Properties

        public override DocumentType DocumentType
        {
            get { return DocumentType.FixtureList; }
        }

        /// <summary>
        /// Returns the document title.
        /// </summary>
        public override String Title
        {
            get { return DocumentTypeHelper.FriendlyNames[DocumentType.FixtureList]; }
        }

        /// <summary>
        /// Gets/Sets the current view type.
        /// </summary>
        public FixtureListViewType ViewType
        {
            get { return _viewType; }
            set
            {
                _viewType = value;
                OnPropertyChanged(ViewTypeProperty);

                OnViewTypeChanged(value);
            }
        }

        /// <summary>
        /// Returns the fixture rows
        /// </summary>
        public ReadOnlyBulkObservableCollection<IPlanItem> ItemRows
        {
            get
            {
                if (_itemRowsRO == null)
                {
                    _itemRowsRO = new ReadOnlyBulkObservableCollection<IPlanItem>(_itemRows);
                }
                return _itemRowsRO;
            }
        }
        
        /// <summary>
        /// Returns the editable collection of selected items.
        /// </summary>
        /// <remarks>This is essentially a filtered version of the selected plan items collection.</remarks>
        public ObservableCollection<IPlanItem> SelectedRows
        {
            get { return _selectedRows; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="sourcePlanogram"></param>
        public FixtureListPlanDocument(PlanControllerViewModel parentController)
            : base(parentController)
        {
            UpdateRows();

            SelectedPlanItems_BulkCollectionChanged(this.SelectedPlanItems, 
                new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));


            OnViewTypeChanged(this.ViewType);

            SetPlanEventHandlers(true);
        }


        #endregion

        #region Commands

        #region RemoveRow Command

        private RelayCommand _removeRowCommand;

        public RelayCommand RemoveRowCommand
        {
            get
            {
                if (_removeRowCommand != null) return _removeRowCommand;

                _removeRowCommand = new RelayCommand(
                    p=> RemoveRow_Executed(p as IPlanItem))
                {
                    FriendlyName = Message.FixtureList_RemoveRow,
                    SmallIcon = ImageResources.Delete_14
                };
                ViewModelCommands.Add(_removeRowCommand);
                return _removeRowCommand;
            }
        }

        private void RemoveRow_Executed(IPlanItem row)
        {
            if (row != null)
            {
                Planogram.BeginUndoableAction();

                Planogram.RemovePlanItem(row);

                Planogram.EndUndoableAction();
            }
        }

        #endregion

        #region EditRow Command

        private RelayCommand _editRowCommand;

        public RelayCommand EditRowCommand
        {
            get
            {
                if (_editRowCommand == null)
                {
                    _editRowCommand = new RelayCommand(
                            p => EditRow_Executed(p))
                    {
                        FriendlyName = MainPageCommands.ShowSelectedItemProperties.FriendlyName,
                        SmallIcon = ImageResources.Properties_14
                    };
                    ViewModelCommands.Add(_editRowCommand);
                }
                return _editRowCommand;
            }
        }


        private void EditRow_Executed(Object row)
        {
            IPlanItem item = row as IPlanItem;
            if (item != null)
            {
                if (item.PlanItemType == PlanItemType.Fixture)
                {
                    MainPageCommands.ShowPlanogramProperties.Execute();
                }
                else
                {
                    MainPageCommands.ShowSelectedItemProperties.Execute();
                }
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the view type property value changes.
        /// </summary>
        /// <param name="newValue"></param>
        private void OnViewTypeChanged(FixtureListViewType newValue)
        {
            UpdateRows();

            SelectedPlanItems_BulkCollectionChanged(this.SelectedPlanItems, new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));
        }

        /// <summary>
        /// Called whenever a child of the planogram changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Planogram_ChildChanged(object sender, ViewModelChildChangedEventArgs e)
        {
            if (e.CollectionChangedArgs != null)
            {
                UpdateRows();
            }
        }

        /// <summary>
        /// Called when the items held by the selected plan items changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedPlanItems_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            if (!_isSynchingSelection)
            {
                _isSynchingSelection = true;

                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        foreach (IPlanItem item in e.ChangedItems)
                        {
                            if (item.PlanItemType == PlanItemType.Fixture
                                || item.PlanItemType == PlanItemType.Assembly
                                || item.PlanItemType == PlanItemType.Component)
                            {
                                if (this.ItemRows.Contains(item))
                                {
                                    this.SelectedRows.Add(item);
                                }
                            }
                        }
                        break;

                    case NotifyCollectionChangedAction.Remove:
                        foreach (IPlanItem item in e.ChangedItems)
                        {
                            if (this.ItemRows.Contains(item))
                            {
                                this.SelectedRows.Remove(item);
                            }
                        }
                        break;
                        
                    case NotifyCollectionChangedAction.Reset:
                        this.SelectedRows.Clear();

                        foreach (IPlanItem item in this.SelectedPlanItems)
                        {
                            if (item.PlanItemType == PlanItemType.Fixture
                                || item.PlanItemType == PlanItemType.Assembly
                                || item.PlanItemType == PlanItemType.Component)
                            {
                                if (this.ItemRows.Contains(item))
                                {
                                    this.SelectedRows.Add(item);
                                }
                            }
                        }
                        break;

                }

                _isSynchingSelection = false;
            }
        }

        /// <summary>
        /// Called when the items held by the selected rows changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedRows_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (!_isSynchingSelection)
            {
                _isSynchingSelection = true;

                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        
                        //clear out the selected plan items first
                        // if it contains any non-component items
                        if (this.SelectedPlanItems.Any(p => 
                            p.PlanItemType !=  PlanItemType.Fixture
                            || p.PlanItemType != PlanItemType.Assembly
                            || p.PlanItemType != PlanItemType.Component))
                        {
                            this.SelectedPlanItems.Clear();
                        }

                        foreach (IPlanItem item in e.NewItems)
                        {
                            this.SelectedPlanItems.Add(item);
                        }
                        break;

                    case NotifyCollectionChangedAction.Remove:
                        foreach (IPlanItem item in e.OldItems)
                        {
                            this.SelectedPlanItems.Remove(item);
                        }
                        break;

                    case NotifyCollectionChangedAction.Reset:
                        this.SelectedPlanItems.Clear();
                        foreach (IPlanItem item in this.SelectedRows)
                        {
                            this.SelectedPlanItems.Add(item);
                        }
                        break;


                }

                _isSynchingSelection = false;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Attaches and detaches events to the source planogramview
        /// </summary>
        /// <param name="isAttach"></param>
        private void SetPlanEventHandlers(Boolean isAttach)
        {
            if (isAttach)
            {
                this.SelectedPlanItems.BulkCollectionChanged += SelectedPlanItems_BulkCollectionChanged;
                this.SelectedRows.CollectionChanged += SelectedRows_CollectionChanged;
                this.Planogram.ChildChanged += Planogram_ChildChanged;
            }
            else
            {
                this.SelectedPlanItems.BulkCollectionChanged -= SelectedPlanItems_BulkCollectionChanged;
                this.SelectedRows.CollectionChanged -= SelectedRows_CollectionChanged;
                this.Planogram.ChildChanged -= Planogram_ChildChanged;
            }
        }

        /// <summary>
        /// Called whenever this document should
        /// stop updating
        /// </summary>
        protected override void OnFreeze()
        {
            //dettach events
            SetPlanEventHandlers(false);
        }

        /// <summary>
        /// Called whenever this document should resume
        /// updating.
        /// </summary>
        protected override void OnUnfreeze()
        {
            //reattach events
            SetPlanEventHandlers(true);

            //update data
            OnViewTypeChanged(this.ViewType);
        }

        /// <summary>
        /// Updates the row collections from the planogram.
        /// </summary>
        private void UpdateRows()
        {
            
            List<IPlanItem> planItems = new List<IPlanItem>();

            switch (this.ViewType)
            {
                case FixtureListViewType.Bays:
                    planItems = PlanItemHelper.GetPlanItems(this.Planogram, PlanItemType.Fixture);
                    break;

                case FixtureListViewType.Assemblies:
                    planItems = PlanItemHelper.GetPlanItems(this.Planogram, PlanItemType.Assembly);
                    break;

                case FixtureListViewType.Components:
                    //remove backboards and bases.
                    planItems = PlanItemHelper.GetPlanItems(this.Planogram, PlanItemType.Component)
                        .Where(c=> c.Component.ComponentType != PlanogramComponentType.Backboard && c.Component.ComponentType != PlanogramComponentType.Base)
                        .ToList();
                    break;
            }

            //if the list has changed
            if (!_itemRows.SequenceEqual(planItems))
            {
                _itemRows.Clear();
                _itemRows.AddRange(planItems);
            }
        }

        #endregion

        #region IDisposable Members
        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                base.Dispose(disposing);

                SetPlanEventHandlers(false);

                base.IsDisposed = true;
            }
        }
        #endregion

    }
    
}
