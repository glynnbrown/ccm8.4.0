﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
// V8-26041 : A.Kuszyk
//  Changed references to Image to System.Windows.Controls.Image due to conflict with model object.
// V8-26491 : A.Kuszyk
//  Added Assortment Document Type.
// V8-26956 : L.Luong
//  Added ConsumerDecisionTree Document Type.
// V8-27477 : L.Ineson
//  Swapped out notch method references.
// V8-27517 : A.Probyn
//  Added defensive code to GetNearestFixtureData
// V8-25832 : L.Ineson
//  Moved visual specific snapping and collision helpers into the plan visual document
// V8-27938 : N.Haywood
//  Added Data Sheets
#endregion

#region Version History: CCM830

// V8-31742 : A.Silva
//  Added PlanogramComparisonPlanDocument to CreateDocument and CreateDocumentView.
//V8-31834 : L.Ineson
//  Added CategoryInsight
// V8-32327 : A.Silva
//  Amended SetDragAdorner to account for null adorners collection.

#endregion

#endregion

using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews.Blocking;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews.CategoryInsight;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews.ConsumerDecisionTree;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews.PlanogramComparison;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews.ProductsList;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf.Rendering;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Rendering;
using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Provides helpers related to plan documents.
    /// </summary>
    public static class PlanDocumentHelper
    {
        #region General

        /// <summary>
        /// Updates the active drag adorner with the given image.
        /// </summary>
        /// <param name="newImage"></param>
        public static void SetDragAdorner(ImageSource newImage)
        {
            var element = Application.Current.MainWindow.Content as FrameworkElement;
            if (element == null) return;

            AdornerLayer layer = AdornerLayer.GetAdornerLayer(element);
            if (layer == null) return;

            Adorner[] adorners = layer.GetAdorners(element);
            if (adorners == null)
            {
                layer.Add(new CursorAdorner(element, newImage));
                adorners = layer.GetAdorners(element);
            }
            if (adorners == null ||
                !adorners.Any()) return;

            var adornerImage = adorners[0].EnumerateVisualChildren().FirstOrDefault() as System.Windows.Controls.Image;
            if (adornerImage != null)
            {
                adornerImage.Source = newImage;
            }
        }

        public static PlanogramSubComponentFaceType ToPlanogramSubComponentFaceType(this  ModelConstruct3DHelper.HitFace face)
        {
            switch (face)
            {
                default:
                case ModelConstruct3DHelper.HitFace.Front: return PlanogramSubComponentFaceType.Front;
                case ModelConstruct3DHelper.HitFace.Back: return PlanogramSubComponentFaceType.Back;
                case ModelConstruct3DHelper.HitFace.Top: return PlanogramSubComponentFaceType.Top;
                case ModelConstruct3DHelper.HitFace.Bottom: return PlanogramSubComponentFaceType.Bottom;
                case ModelConstruct3DHelper.HitFace.Left: return PlanogramSubComponentFaceType.Left;
                case ModelConstruct3DHelper.HitFace.Right: return PlanogramSubComponentFaceType.Right;
            }
        }

        #endregion

        #region Document Creation

        /// <summary>
        /// Creates a new document of the given type.
        /// </summary>
        /// <param name="docType"></param>
        /// <param name="controller"></param>
        /// <returns></returns>
        public static IPlanDocument CreateDocument(
            DocumentType docType, PlanControllerViewModel controller)
        {

            IPlanDocument document = null;

            switch (docType)
            {
                case DocumentType.Design:
                    document = new PlanVisualDocument(controller, CameraViewType.Design);
                    break;

                case DocumentType.Front:
                    document = new PlanVisualDocument(controller, CameraViewType.Front);
                    break;

                case DocumentType.Back:
                    document = new PlanVisualDocument(controller, CameraViewType.Back);
                    break;

                case DocumentType.Bottom:
                    document = new PlanVisualDocument(controller, CameraViewType.Bottom);
                    break;

                case DocumentType.Left:
                    document = new PlanVisualDocument(controller, CameraViewType.Left);
                    break;

                case DocumentType.Right:
                    document = new PlanVisualDocument(controller, CameraViewType.Right);
                    break;

                case DocumentType.Top:
                    document = new PlanVisualDocument(controller, CameraViewType.Top);
                    break;

                case DocumentType.Perspective:
                    document = new PlanVisualDocument(controller, CameraViewType.Perspective);
                    break;

                case DocumentType.FixtureList:
                    document = new FixtureListPlanDocument(controller);
                    break;

                case DocumentType.ProductList:
                    document = new ProductListPlanDocument(controller);
                    break;

                case DocumentType.Assortment:
                    document = new AssortmentPlanDocument(controller);
                    break;

                case DocumentType.ConsumerDecisionTree:
                    document = new ConsumerDecisionTreePlanDocument(controller);
                    break;

                case DocumentType.Blocking:
                    document = new BlockingPlanDocument(controller);
                    break;

                case DocumentType.DataSheet:
                    document = new ReportDocument(controller);
                    break;

                case DocumentType.PlanogramComparison:
                    document = new PlanogramComparisonPlanDocument(controller);
                    break;

                case DocumentType.CategoryInsight:
                    document = new CategoryInsightPlanDocument(controller);
                    break;

                default:
                    Debug.Fail("TODO");
                    break;
            }


            return document;
        }

        /// <summary>
        /// Creates the view control for the given document.
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static IPlanDocumentView CreateDocumentView(IPlanDocument doc)
        {
            switch(doc.DocumentType)
            {
                case DocumentType.Assortment:
                    return new AssortmentPlanDocumentView((AssortmentPlanDocument)doc);
                   
                case DocumentType.Blocking:
                    return new BlockingPlanDocumentView((BlockingPlanDocument)doc);

                case DocumentType.ConsumerDecisionTree:
                    return new ConsumerDecisionTreePlanDocumentView((ConsumerDecisionTreePlanDocument)doc);

                case DocumentType.FixtureList:
                    return new FixtureListPlanDocumentView((FixtureListPlanDocument)doc);
                    
                case DocumentType.ProductList:
                    return new ProductListPlanDocumentView((ProductListPlanDocument)doc);

                case DocumentType.DataSheet:
                    return new ReportDocumentView((ReportDocument)doc);

                case DocumentType.PlanogramComparison:
                    return new PlanogramComparisonPlanDocumentView((PlanogramComparisonPlanDocument)doc);

                case DocumentType.CategoryInsight:
                    return new CategoryInsightPlanDocumentView((CategoryInsightPlanDocument)doc);

                case DocumentType.Front:
                case DocumentType.Back:
                case DocumentType.Top:
                case DocumentType.Bottom:
                case DocumentType.Left:
                case DocumentType.Right:
                case DocumentType.Design:
                case DocumentType.Perspective:
                    return new PlanVisualDocumentView((PlanVisualDocument)doc);

                default:
                    Debug.Fail("Document type not recognised");
                    return null;

            }
        }


        #endregion

        #region Plan Item

        /// <summary>
        /// Returns the first selectable parent for the given model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static IPlanItem GetHitPlanItem(DependencyObject model, Boolean isSelectableOnly)
        {
            try
            {
                DependencyObject dep = model;

                while (dep != null)
                {
                    ModelConstruct3D depModel = dep as ModelConstruct3D;
                    if (depModel != null
                        && depModel.ModelData is IPlanPart3DData
                        && ((IPlanPart3DData)depModel.ModelData).PlanPart != null)
                    {
                        IPlanItem item = (IPlanItem)((IPlanPart3DData)depModel.ModelData).PlanPart;

                        if (!isSelectableOnly || item.IsSelectable)
                        {
                            return item;
                        }
                        else
                        {
                            return PlanItemHelper.GetSelectableItem(item);
                        }
                    }

                    //try next parent.
                    dep = VisualTreeHelper.GetParent(dep);

                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Creates a new modelcontruct3d from the given planitem.
        /// </summary>
        /// <param name="item">the item to create for</param>
        /// <returns></returns>
        public static ModelConstruct3D CreateModel(IPlanItem item)
        {
            return CreateModel(item, new PlanRenderSettings());
        }
        /// <summary>
        /// Creates a new modelcontruct3d from the given planitem.
        /// </summary>
        /// <param name="item">the item to create for</param>
        /// <param name="settings">the settings to use.</param>
        /// <returns></returns>
        public static ModelConstruct3D CreateModel(IPlanItem item, PlanRenderSettings settings)
        {
            ModelConstruct3D model = null;
            switch (item.PlanItemType)
            {
                case PlanItemType.Component:
                    model = new ModelConstruct3D(new PlanComponent3DData((IPlanComponentRenderable)item, settings));
                    break;

                case PlanItemType.Assembly:
                    model = new ModelConstruct3D(new PlanAssembly3DData((IPlanAssemblyRenderable)item, settings));
                    break;

                case PlanItemType.Fixture:
                    model = new ModelConstruct3D(new PlanFixture3DData((IPlanFixtureRenderable)item, settings));
                    break;

                case PlanItemType.Position:
                    model = new ModelConstruct3D(new PlanPosition3DData((IPlanPositionRenderable)item, settings));
                    break;

                case PlanItemType.Annotation:
                    model = new ModelConstruct3D(new PlanAnnotation3DData((IPlanAnnotationRenderable)item, settings));
                    break;
            }

            return model;
        }

        #endregion

    }

    #region OLD

    #region OLD

    //public static void Snap(PreviewComponent component, ComponentType componentType,
    //   PlanogramHitTestResult nearestSubHitResult,
    //   PointValue proposedWorldPos, RotationValue proposedWorldRotation,
    //   Boolean canChangeSize, Boolean snapToNotch)
    //{
    //    IModelConstruct3DData nearestSubData = nearestSubHitResult.NearestSubComponentData;

    //    PlanogramSubComponentView nearestSubComponent = nearestSubHitResult.NearestSubComponent;
    //    if (nearestSubComponent != null)
    //    {
    //        Point3D subHitLocalPos = nearestSubHitResult.SubComponentLocalHitPoint;

    //        RotationValue subWorldRotation = ModelConstruct3DHelper.GetWorldRotation(nearestSubData);

    //        PointValue newLocalPosition = new PointValue();
    //        RotationValue newWorldRotation = new RotationValue(
    //            subWorldRotation.Angle, subWorldRotation.Slope, subWorldRotation.Roll);

    //        switch (componentType)
    //        {
    //            #region Default
    //            default:
    //                {

    //                    newLocalPosition.X = nearestSubData.Position.X + subHitLocalPos.X;
    //                    newLocalPosition.Y = nearestSubData.Position.Y + Math.Round(subHitLocalPos.Y - component.Height / 2.0);
    //                    newLocalPosition.Z = nearestSubData.Position.Z + subHitLocalPos.Z;

    //                    switch (nearestSubHitResult.SubComponentFaceHit)
    //                    {
    //                        case ModelConstruct3DHelper.HitFace.Front:
    //                            if (canChangeSize)
    //                            {
    //                                component.Width = Convert.ToSingle(nearestSubData.Size.X);
    //                            }
    //                            newLocalPosition.X = nearestSubData.Position.X;
    //                            break;

    //                        case ModelConstruct3DHelper.HitFace.Back:
    //                            if (canChangeSize)
    //                            {
    //                                component.Width = Convert.ToSingle(nearestSubData.Size.X);
    //                            }
    //                            newLocalPosition.X = nearestSubData.Position.X + nearestSubData.Size.X;
    //                            newWorldRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(-180));
    //                            break;

    //                        case ModelConstruct3DHelper.HitFace.Top:
    //                            if (canChangeSize)
    //                            {
    //                                component.Width = Convert.ToSingle(nearestSubData.Size.X);
    //                                component.Depth = Convert.ToSingle(nearestSubData.Size.Z);
    //                            }
    //                            newLocalPosition.X = nearestSubData.Position.X;
    //                            newLocalPosition.Z = nearestSubData.Position.Z;
    //                            break;

    //                        case ModelConstruct3DHelper.HitFace.Bottom:
    //                            if (canChangeSize)
    //                            {
    //                                component.Width = Convert.ToSingle(nearestSubData.Size.X);
    //                                component.Depth = Convert.ToSingle(nearestSubData.Size.Z);
    //                            }
    //                            newLocalPosition.X = nearestSubData.Position.X;
    //                            newLocalPosition.Y = nearestSubData.Position.Y - component.Height;
    //                            newLocalPosition.Z = nearestSubData.Position.Z;
    //                            break;

    //                        case ModelConstruct3DHelper.HitFace.Left:
    //                            if (canChangeSize)
    //                            {
    //                                component.Width = Convert.ToSingle(nearestSubData.Size.Z);
    //                            }
    //                            newLocalPosition.Z = nearestSubData.Position.Z;
    //                            newWorldRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(-90));
    //                            break;

    //                        case ModelConstruct3DHelper.HitFace.Right:
    //                            if (canChangeSize)
    //                            {
    //                                component.Width = Convert.ToSingle(nearestSubData.Size.Z);
    //                            }
    //                            newLocalPosition.Z = nearestSubData.Position.Z + component.Width;
    //                            newWorldRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(90));
    //                            break;
    //                    }


    //                    if (snapToNotch)
    //                    {
    //                        Double? notchSnap =
    //                                PlanDocumentHelper.GetNotchSnap(nearestSubComponent,
    //                                component.Height, newLocalPosition.Y, nearestSubHitResult.SubComponentFaceHit);
    //                        if (notchSnap.HasValue)
    //                        {
    //                            newLocalPosition.Y = notchSnap.Value;
    //                        }
    //                    }
    //                }
    //                break;
    //            #endregion

    //            #region Rod/Clipstrip
    //            case ComponentType.Rod:
    //            case ComponentType.ClipStrip:
    //                {
    //                    newLocalPosition.X = nearestSubData.Position.X + Math.Round(subHitLocalPos.X - component.Width / 2.0);
    //                    newLocalPosition.Y = nearestSubData.Position.Y + Math.Round(subHitLocalPos.Y - component.Height / 2.0);
    //                    newLocalPosition.Z = nearestSubData.Position.Z + subHitLocalPos.Z;

    //                    switch (nearestSubHitResult.SubComponentFaceHit)
    //                    {
    //                        case ModelConstruct3DHelper.HitFace.Front:
    //                            //no change to size or position.
    //                            break;

    //                        case ModelConstruct3DHelper.HitFace.Back:
    //                            //no change to size or position.
    //                            newWorldRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(-180));
    //                            break;

    //                        case ModelConstruct3DHelper.HitFace.Top:
    //                            //no change to size or position
    //                            newWorldRotation.Slope += Convert.ToSingle(CommonHelper.ToRadians(-180));
    //                            break;

    //                        case ModelConstruct3DHelper.HitFace.Bottom:
    //                            //no change to size or position
    //                            newWorldRotation.Slope += Convert.ToSingle(CommonHelper.ToRadians(180));
    //                            break;

    //                        case ModelConstruct3DHelper.HitFace.Left:
    //                            //no change to size or position
    //                            newWorldRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(-90));
    //                            break;

    //                        case ModelConstruct3DHelper.HitFace.Right:
    //                            //no change to size or position
    //                            newWorldRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(90));
    //                            break;
    //                    }


    //                    if (snapToNotch)
    //                    {
    //                        Double? notchSnap =
    //                                PlanDocumentHelper.GetNotchSnap(nearestSubComponent,
    //                                component.Height, newLocalPosition.Y, nearestSubHitResult.SubComponentFaceHit);
    //                        if (notchSnap.HasValue)
    //                        {
    //                            newLocalPosition.Y = notchSnap.Value;
    //                        }
    //                    }

    //                }
    //                break;
    //            #endregion

    //        }

    //        //convert the position back to world space.
    //        PointValue wp = ModelConstruct3DHelper.ToWorld(newLocalPosition, nearestSubData);
    //        component.X = Convert.ToSingle(wp.X);
    //        component.Y = Convert.ToSingle(wp.Y);
    //        component.Z = Convert.ToSingle(wp.Z);

    //        component.Angle = Convert.ToSingle(newWorldRotation.Angle);
    //        component.Slope = Convert.ToSingle(newWorldRotation.Slope);
    //        component.Roll = Convert.ToSingle(newWorldRotation.Roll);
    //    }
    //    else
    //    {
    //        component.SetPosition(proposedWorldPos);
    //        component.SetRotation(proposedWorldRotation);
    //    }

    //    if (component.Y < 0) component.Y = 0;//force y above floor.

    //}

    //private static Dictionary<PlanogramFixtureView, Rect3D> GetFixtureBounds(Plan3DData planModelData, out Rect3D totalBounds)
    //{
    //    Rect3D overallBounds = Rect3D.Empty;

    //    Dictionary<PlanogramFixtureView, Rect3D> fixtureBounds = new Dictionary<PlanogramFixtureView, Rect3D>();
    //    foreach (var fixture in planModelData.GetAllFixtureModels())
    //    {
    //        Rect3D fixtureRect = new Rect3D();
    //        fixtureRect.SizeX = fixture.Size.X;
    //        fixtureRect.SizeY = fixture.Size.Y;
    //        fixtureRect.SizeZ = fixture.Size.Z;

    //        MatrixTransform3D transform = ModelConstruct3DHelper.CreateTransform(fixture.Rotation, fixture.Position);
    //        fixtureRect = transform.TransformBounds(fixtureRect);

    //        IPlanItem planItem = fixture.PlanPart as IPlanItem;
    //        if (planItem != null)
    //        {
    //            fixtureBounds.Add(planItem.Fixture, fixtureRect);
    //        }

    //        overallBounds = Rect3D.Union(overallBounds, fixtureRect);
    //    }

    //    totalBounds = overallBounds;

    //    return fixtureBounds;
    //}

    ///// <summary>
    ///// Returns the nearest fixture for the given hit point.
    ///// </summary>
    ///// <param name="vp"></param>
    ///// <param name="camera"></param>
    ///// <param name="hitPoint"></param>
    ///// <param name="planModelData"></param>
    ///// <returns></returns>
    //public static PlanogramFixtureView GetNearestFixture(
    //    Viewport3D vp, ProjectionCamera camera, Point hitPoint, Plan3DData planModelData)
    //{
    //    PlanogramFixtureView nearestFixture = null;

    //    Vector3D normal = new Vector3D(camera.LookDirection.X, camera.LookDirection.Y, camera.LookDirection.Z);
    //    normal.Negate();
    //    normal.Normalize();

    //    //get fixture bounds
    //    Rect3D overallBounds = Rect3D.Empty;
    //    Dictionary<PlanogramFixtureView, Rect3D> fixtureBounds = GetFixtureBounds(planModelData, out overallBounds);

    //    //determine possible intersection points
    //    List<Point3D> posIntersections = new List<Point3D>();
    //    posIntersections = ModelConstruct3DHelper.GetBoxPlaneIntersections(overallBounds, Viewport3DHelper.Point2DtoRay3D(vp, hitPoint));


    //    //cycle through the fixtures determining which one is intersected first.
    //    foreach (var fx in fixtureBounds.OrderBy(v => ModelConstruct3DHelper.GetCameraDistance(v.Value, camera.Position)))
    //    {
    //        Boolean found = false;

    //        foreach (Point3D point in posIntersections)
    //        {
    //            if (fx.Value.Contains(point))
    //            {
    //                nearestFixture = fx.Key;
    //                found = true;
    //                break;
    //            }
    //        }

    //        if (found) break;
    //    }


    //    return nearestFixture;
    //}


    ///// <summary>
    ///// Returns a hit test result for the subcomponent nearest to the 2D hit point 
    ///// based on an intersection ray.
    ///// </summary>
    ///// <param name="vp">The viewport.</param>
    ///// <param name="hitPoint">The 2d hit</param>
    ///// <param name="Plan3DData">The model data for the planogram we are hit testing against.</param>
    ///// <returns></returns>
    //public static PlanogramHitTestResult GetNearestSubComponent(Viewport3D vp, Point hitPoint, Plan3DData planModelData)
    //{
    //    PlanogramHitTestResult planHitResult = new PlanogramHitTestResult();
    //    planHitResult.ViewerHitPoint = ModelConstruct3DHelper.GetPoint3D(hitPoint, vp);

    //    ProjectionCamera camera = vp.Camera as ProjectionCamera;
    //    if (camera == null) throw new ArgumentException("Viewer must use a projection camera");

    //    //get the subcomponent nearest to the camera at the given hit point.
    //    Viewport3DHelper.HitResult hitResult;
    //    ModelConstruct3D nearestModel = ModelConstruct3DHelper.GetNearestModelConstruct(vp, camera, hitPoint, out hitResult);
    //    if (nearestModel != null)
    //    {

    //        PlanSubComponent3DData subData = nearestModel.ModelData as PlanSubComponent3DData;
    //        if (subData != null)
    //        {
    //            planHitResult.NearestSubComponentData = subData;
    //            planHitResult.NearestSubComponent = subData.Subcomponent as PlanogramSubComponentView;
    //            planHitResult.NearestFixture = planHitResult.NearestSubComponent.Fixture;

    //            if (hitResult != null)
    //            {
    //                planHitResult.SubComponentFaceHit = ModelConstruct3DHelper.NormalToFacing(hitResult.Normal);

    //                Point3D rayHit = hitResult.RayHit.PointHit;
    //                Point3D localHitPoint = new Point3D(rayHit.X, rayHit.Y, rayHit.Z);

    //                //corect the local hit point according to the hit face.
    //                switch (planHitResult.SubComponentFaceHit)
    //                {
    //                    case ModelConstruct3DHelper.HitFace.Front:
    //                        localHitPoint.Z = subData.Position.Z + subData.Size.Z;
    //                        break;

    //                    case ModelConstruct3DHelper.HitFace.Back:
    //                        localHitPoint.Z = subData.Position.Z;
    //                        break;

    //                    case ModelConstruct3DHelper.HitFace.Left:
    //                        localHitPoint.X = subData.Position.X;
    //                        break;

    //                    case ModelConstruct3DHelper.HitFace.Right:
    //                        localHitPoint.X = subData.Position.X + subData.Size.X;
    //                        break;

    //                    case ModelConstruct3DHelper.HitFace.Top:
    //                        localHitPoint.Y = subData.Position.Y + subData.Size.Y;
    //                        break;

    //                    case ModelConstruct3DHelper.HitFace.Bottom:
    //                        localHitPoint.Y = subData.Position.Y;
    //                        break;
    //                }



    //                planHitResult.SubComponentLocalHitPoint = localHitPoint;//hitResult.RayHit.PointHit;
    //                planHitResult.SubComponentWorldHitPoint = ModelConstruct3DHelper.ToWorld(planHitResult.SubComponentLocalHitPoint, nearestModel.ModelData);

    //            }
    //        }

    //    }

    //    //if no fixture found, then try to get one directly.
    //    if (planHitResult.NearestFixture == null)
    //    {
    //        planHitResult.NearestFixture = GetNearestFixture(vp, camera, hitPoint, planModelData);
    //    }


    //    return planHitResult;
    //}

    /// <summary>
    /// Returns the nearest merchandisable subcomponent to the hit point.
    /// </summary>
    /// <param name="vp"></param>
    /// <param name="camera"></param>
    /// <param name="hitPoint"></param>
    /// <param name="plan"></param>
    /// <returns></returns>
    //public static PlanSubComponent3DData GetNearestMerchandisableSubComponent(
    //    Viewport3D vp, ProjectionCamera camera, Point hitPoint, Plan3DData planModelData)
    //{
    //    //first check if we hit a merchandisable subcomponent directly.
    //    PlanogramHitTestResult subHitResult = GetNearestSubComponent(vp, hitPoint, planModelData);
    //    PlanSubComponent3DData nearestSubData = subHitResult.NearestSubComponentData;

    //    if (nearestSubData == null || !nearestSubData.Subcomponent.IsMerchandisable)
    //    {
    //        //no merch sub was hit directly so now lets see if we hit any when merch bounds are included.
    //        List<PlanSubComponent3DData> subsToCheck = new List<PlanSubComponent3DData>();

    //        //limit subs to hit fixture if possible.
    //        Boolean subsFound = false;
    //        PlanogramFixtureView hitFixture = subHitResult.NearestFixture;
    //        if (hitFixture != null)
    //        {
    //            PlanFixture3DData fixtureModelData = RenderControlsHelper.FindItemModelData(planModelData, hitFixture) as PlanFixture3DData;
    //            if (fixtureModelData != null)
    //            {
    //                subsToCheck.AddRange(fixtureModelData.GetAllChildModels().OfType<PlanSubComponent3DData>());
    //                subsFound = true;
    //            }
    //        }
    //        if (!subsFound)
    //        {
    //            subsToCheck.AddRange(planModelData.GetAllSubComponentModels());
    //            subsFound = true;
    //        }


    //        Ray3D hitRay = Viewport3DHelper.Point2DtoRay3D(vp, hitPoint);

    //        Dictionary<PlanSubComponent3DData, Rect3D> subToBounds = new Dictionary<PlanSubComponent3DData, Rect3D>();
    //        foreach (var sub in subsToCheck)
    //        {
    //            if (sub.Subcomponent.IsMerchandisable)
    //            {
    //                subToBounds.Add(sub, GetUnhinderedMerchandisingSpaceBounds(sub, /*toWorld*/true));
    //            }
    //        }

    //        nearestSubData = null;
    //        Double lastDist = Double.PositiveInfinity;
    //        foreach (var entry in subToBounds.OrderBy(v => ModelConstruct3DHelper.GetCameraDistance(v.Value, camera.Position)))
    //        {
    //            List<Point3D> rayPlaneIntersections = ModelConstruct3DHelper.GetBoxPlaneIntersections(entry.Value, hitRay);
    //            foreach (Point3D ri in rayPlaneIntersections.OrderBy(ri => camera.Position.DistanceTo(ri)))
    //            {
    //                if (entry.Value.Contains(ri))
    //                {
    //                    Double distance = ri.DistanceTo(entry.Value.Location);

    //                    //if the subcomponent is a hanging type then measure to its top left.
    //                    PlanogramSubComponentView subView = entry.Key.Subcomponent as PlanogramSubComponentView;
    //                    if (subView != null)
    //                    {
    //                        if (subView.MerchandisingType == SubComponentMerchandisingType.Hang)
    //                        {
    //                            //Measure to top left.
    //                            distance = ri.DistanceTo(
    //                                new Point3D(entry.Value.X, entry.Value.Y + entry.Value.SizeY, entry.Value.SizeZ));
    //                        }
    //                    }


    //                    Boolean found = (nearestSubData == null)
    //                        || (distance < lastDist);


    //                    if (found)
    //                    {
    //                        nearestSubData = entry.Key;
    //                        lastDist = distance;
    //                    }
    //                    break;
    //                }
    //            }

    //        }
    //    }

    //    if (nearestSubData != null && !nearestSubData.Subcomponent.IsMerchandisable)
    //    {
    //        //What happened??
    //        nearestSubData = null;
    //    }

    //    return nearestSubData;
    //}

    #region Snap PlanComponent3DData componentData

    //public static void Snap(PlanComponent3DData componentData, ComponentType componentType,
    //    PlanogramHitTestResult nearestSubHitResult,
    //    PointValue proposedWorldPos, RotationValue proposedWorldRotation, Boolean snapToNotch)
    //{
    //    Boolean canChangeSize = false;

    //    IModelConstruct3DData nearestSubData = nearestSubHitResult.NearestSubComponentData;

    //    PlanogramSubComponentView nearestSubComponent = nearestSubHitResult.NearestSubComponent;
    //    if (nearestSubComponent != null)
    //    {
    //        Point3D subHitLocalPos = nearestSubHitResult.SubComponentLocalHitPoint;

    //        PointValue newLocalPosition = new PointValue();
    //        RotationValue newRotation = ModelConstruct3DHelper.GetWorldRotation(nearestSubData);

    //        PointValue newSize = new PointValue(componentData.Size.X, componentData.Size.Y, componentData.Size.Z);

    //        switch (componentType)
    //        {
    //            #region Default
    //            default:
    //                {

    //                    newLocalPosition.X = nearestSubData.Position.X + subHitLocalPos.X;
    //                    newLocalPosition.Y = nearestSubData.Position.Y + Math.Round(subHitLocalPos.Y - newSize.Y / 2.0);
    //                    newLocalPosition.Z = nearestSubData.Position.Z + subHitLocalPos.Z;

    //                    switch (nearestSubHitResult.SubComponentFaceHit)
    //                    {
    //                        case ModelConstruct3DHelper.HitFace.Front:
    //                            if (canChangeSize)
    //                            {
    //                                newSize.X = Convert.ToSingle(nearestSubData.Size.X);
    //                            }
    //                            newLocalPosition.X = nearestSubData.Position.X;
    //                            break;

    //                        case ModelConstruct3DHelper.HitFace.Back:
    //                            if (canChangeSize)
    //                            {
    //                                newSize.X = Convert.ToSingle(nearestSubData.Size.X);
    //                            }
    //                            newLocalPosition.X = nearestSubData.Position.X + nearestSubData.Size.X;
    //                            newRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(-180));
    //                            break;

    //                        case ModelConstruct3DHelper.HitFace.Top:
    //                            if (canChangeSize)
    //                            {
    //                                newSize.X = Convert.ToSingle(nearestSubData.Size.X);
    //                                newSize.Z = Convert.ToSingle(nearestSubData.Size.Z);
    //                            }
    //                            newLocalPosition.X = nearestSubData.Position.X;
    //                            newLocalPosition.Z = nearestSubData.Position.Z;
    //                            break;

    //                        case ModelConstruct3DHelper.HitFace.Bottom:
    //                            if (canChangeSize)
    //                            {
    //                                newSize.X = Convert.ToSingle(nearestSubData.Size.X);
    //                                newSize.Z = Convert.ToSingle(nearestSubData.Size.Z);
    //                            }
    //                            newLocalPosition.X = nearestSubData.Position.X;
    //                            newLocalPosition.Y = nearestSubData.Position.Y - newSize.Y;
    //                            newLocalPosition.Z = nearestSubData.Position.Z;
    //                            break;

    //                        case ModelConstruct3DHelper.HitFace.Left:
    //                            if (canChangeSize)
    //                            {
    //                                newSize.X = Convert.ToSingle(nearestSubData.Size.Z);
    //                            }
    //                            newLocalPosition.Z = nearestSubData.Position.Z;
    //                            newRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(-90));
    //                            break;

    //                        case ModelConstruct3DHelper.HitFace.Right:
    //                            if (canChangeSize)
    //                            {
    //                                newSize.X = Convert.ToSingle(nearestSubData.Size.Z);
    //                            }
    //                            newLocalPosition.Z = nearestSubData.Position.Z + newSize.X;
    //                            newRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(90));
    //                            break;
    //                    }


    //                    if (snapToNotch)
    //                    {
    //                        Double? notchSnap =
    //                                PlanDocumentHelper.GetNotchSnap(nearestSubComponent,
    //                                newSize.Y, newLocalPosition.Y, nearestSubHitResult.SubComponentFaceHit);
    //                        if (notchSnap.HasValue)
    //                        {
    //                            newLocalPosition.Y = notchSnap.Value;
    //                        }
    //                    }
    //                }
    //                break;
    //            #endregion

    //            #region Rod
    //            case ComponentType.Rod:
    //                {
    //                    newLocalPosition.X = nearestSubData.Position.X + Math.Round(subHitLocalPos.X - newSize.X / 2.0);
    //                    newLocalPosition.Y = nearestSubData.Position.Y + Math.Round(subHitLocalPos.Y - newSize.Y / 2.0);
    //                    newLocalPosition.Z = nearestSubData.Position.Z + subHitLocalPos.Z;

    //                    switch (nearestSubHitResult.SubComponentFaceHit)
    //                    {
    //                        case ModelConstruct3DHelper.HitFace.Front:
    //                            //no change to size or position.
    //                            break;

    //                        case ModelConstruct3DHelper.HitFace.Back:
    //                            //no change to size or position.
    //                            newRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(-180));
    //                            break;

    //                        case ModelConstruct3DHelper.HitFace.Top:
    //                            //no change to size or position
    //                            newRotation.Slope += Convert.ToSingle(CommonHelper.ToRadians(-180));
    //                            break;

    //                        case ModelConstruct3DHelper.HitFace.Bottom:
    //                            //no change to size or position
    //                            newRotation.Slope += Convert.ToSingle(CommonHelper.ToRadians(180));
    //                            break;

    //                        case ModelConstruct3DHelper.HitFace.Left:
    //                            //no change to size or position
    //                            newRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(-90));
    //                            break;

    //                        case ModelConstruct3DHelper.HitFace.Right:
    //                            //no change to size or position
    //                            newRotation.Angle += Convert.ToSingle(CommonHelper.ToRadians(90));
    //                            break;
    //                    }


    //                    if (snapToNotch)
    //                    {
    //                        Double? notchSnap =
    //                                PlanDocumentHelper.GetNotchSnap(nearestSubComponent,
    //                                newSize.Y, newLocalPosition.Y, nearestSubHitResult.SubComponentFaceHit);
    //                        if (notchSnap.HasValue)
    //                        {
    //                            newLocalPosition.Y = notchSnap.Value;
    //                        }
    //                    }

    //                }
    //                break;
    //            #endregion

    //        }

    //        //convert the position back to world space.
    //        PointValue wp = ModelConstruct3DHelper.ToWorld(newLocalPosition, nearestSubData);
    //        if (wp.Y < 0) wp.Y = 0;//force y above floor.
    //        componentData.Position = wp;

    //        componentData.Rotation = newRotation;
    //    }
    //    else
    //    {
    //        if (proposedWorldPos.Y < 0) proposedWorldPos.Y = 0;//force y above floor.

    //        componentData.Position = proposedWorldPos;
    //        componentData.Rotation = proposedWorldRotation;
    //    }
    //}

    #endregion

    #region ShiftByCollisions




    //public static void ShiftByCollisions(IPlanItem item, Plan3DData planModelData, CameraViewType viewtype)
    //{
    //    if (viewtype == CameraViewType.Perspective) { return; }

    //    IModelConstruct3DData itemModelData = ModelConstruct3DHelper.FindItemModelData(planModelData, item);

    //    Rect3D itemBounds = ModelConstruct3DHelper.GetWorldSpaceBounds(itemModelData);
    //    if (!itemBounds.IsEmpty)
    //    {
    //        List<IPlanItem> newSubParts = PlanItemHelper.GetAllChildItemsOfType(item, PlanItemType.SubComponent);
    //        Dictionary<PlanSubComponent3DData, Rect3D> newSubBoundsDict = new Dictionary<PlanSubComponent3DData, Rect3D>();

    //        //check if this new part collides with another.
    //        Dictionary<PlanSubComponent3DData, Rect3D> subBoundsDict = new Dictionary<PlanSubComponent3DData, Rect3D>();
    //        foreach (var sub in planModelData.GetAllSubComponentModels())
    //        {
    //            Rect3D subBounds = ModelConstruct3DHelper.GetWorldSpaceBounds(sub);
    //            IPlanItem subItem = sub.Subcomponent as IPlanItem;
    //            if (subItem != null)
    //            {
    //                if (!newSubParts.Contains(subItem))
    //                {
    //                    subBoundsDict.Add(sub, subBounds);
    //                }
    //                else
    //                {
    //                    newSubBoundsDict.Add(sub, subBounds);
    //                }
    //            }
    //        }

    //        //check if the new part hits anything and if so move it.
    //        foreach (var entry in subBoundsDict)
    //        {
    //            foreach (var newEntry in newSubBoundsDict)
    //            {
    //                if (LocalHelper.CollidesWith(newEntry.Value, entry.Value))
    //                {
    //                    Rect3D collisionBounds = entry.Value;

    //                    switch (viewtype)
    //                    {
    //                        case CameraViewType.Front:
    //                        default:
    //                            item.Z = Convert.ToSingle(Math.Max(item.Z, collisionBounds.Z + collisionBounds.SizeZ));
    //                            break;

    //                        case CameraViewType.Back:
    //                            item.Z = Convert.ToSingle(Math.Min(item.Z, collisionBounds.Z - collisionBounds.SizeZ));
    //                            break;

    //                        case CameraViewType.Left:
    //                            item.X = Convert.ToSingle(Math.Min(item.X, collisionBounds.X - itemBounds.SizeX));
    //                            break;

    //                        case CameraViewType.Right:
    //                            item.X = Convert.ToSingle(Math.Max(item.X, collisionBounds.X + collisionBounds.SizeX));
    //                            break;

    //                        case CameraViewType.Top:
    //                            item.Y = Convert.ToSingle(Math.Max(item.Y, collisionBounds.Y + collisionBounds.SizeY));
    //                            break;

    //                        case CameraViewType.Bottom:
    //                            item.Y = Convert.ToSingle(Math.Min(item.Y, collisionBounds.Y - itemBounds.SizeY));
    //                            break;
    //                    }

    //                    itemBounds.X = item.X;
    //                    itemBounds.Y = item.Y;
    //                    itemBounds.Z = item.Z;
    //                }
    //            }
    //        }
    //    }
    //}

    #endregion

    #region GetSnappedDimensions

    ///// <summary>
    ///// Gets the snapped values the given dimensions to the given subcomponent
    ///// </summary>
    ///// <param name="nearestSubComponent"></param>
    ///// <param name="localSubHit"></param>
    ///// <param name="itemSize"></param>
    ///// <param name="itemWorldPos"></param>
    ///// <param name="itemWorldRotation"></param>
    ///// <param name="canSnapSize"></param>
    //public static void GetSnappedDimensions(PlanogramHitTestResult nearestSubHitResult,
    //    ref WidthHeightDepthValue itemSize, ref PointValue itemWorldPos, ref RotationValue itemWorldRotation,
    //    Boolean canSnapSize, Boolean snapToNotch)
    //{
    //    IModelConstruct3DData nearestSubData = nearestSubHitResult.NearestSubComponentData;

    //    PlanogramSubComponentView nearestSubComponent = nearestSubHitResult.NearestSubComponent;
    //    if (nearestSubComponent != null)
    //    {
    //        WidthHeightDepthValue newSize = new WidthHeightDepthValue(itemSize.X, itemSize.Y, itemSize.Z);
    //        PointValue newPos = new PointValue(itemWorldPos.X, itemWorldPos.Y, itemWorldPos.Z);
    //        RotationValue newRotation = ModelConstruct3DHelper.GetWorldRotation(nearestSubData);

    //        Point3D subHitLocalPos = nearestSubHitResult.SubComponentLocalHitPoint;

    //        PointValue newLocalPosition = new PointValue();
    //        newLocalPosition.X = nearestSubData.Position.X + subHitLocalPos.X;
    //        newLocalPosition.Y = nearestSubData.Position.Y + Math.Round(subHitLocalPos.Y - itemSize.Y / 2.0);
    //        newLocalPosition.Z = nearestSubData.Position.Z + subHitLocalPos.Z;

    //        switch (nearestSubHitResult.SubComponentFaceHit)
    //        {
    //            case ModelConstruct3DHelper.HitFace.Front:
    //                if (canSnapSize)
    //                {
    //                    newSize.X = nearestSubData.Size.X;
    //                }
    //                newLocalPosition.X = nearestSubData.Position.X;
    //                break;

    //            case ModelConstruct3DHelper.HitFace.Back:
    //                if (canSnapSize)
    //                {
    //                    newSize.X = nearestSubData.Size.X;
    //                }
    //                newLocalPosition.X = nearestSubData.Position.X + nearestSubData.Size.X;
    //                newRotation.Angle += CommonHelper.ToRadians(-180);
    //                break;

    //            case ModelConstruct3DHelper.HitFace.Top:
    //                if (canSnapSize)
    //                {
    //                    newSize.X = nearestSubData.Size.X;
    //                    newSize.Z = nearestSubData.Size.Z;
    //                }
    //                newLocalPosition.X = nearestSubData.Position.X;
    //                newLocalPosition.Z = nearestSubData.Position.Z;
    //                break;

    //            case ModelConstruct3DHelper.HitFace.Bottom:
    //                if (canSnapSize)
    //                {
    //                    newSize.X = nearestSubData.Size.X;
    //                    newSize.Z = nearestSubData.Size.Z;
    //                }
    //                newLocalPosition.X = nearestSubData.Position.X;
    //                newLocalPosition.Y = nearestSubData.Position.Y - newSize.Y;
    //                newLocalPosition.Z = nearestSubData.Position.Z;
    //                break;

    //            case ModelConstruct3DHelper.HitFace.Left:
    //                if (canSnapSize)
    //                {
    //                    newSize.X = nearestSubData.Size.Z;
    //                }
    //                newLocalPosition.Z = nearestSubData.Position.Z;
    //                newRotation.Angle += CommonHelper.ToRadians(-90);
    //                break;

    //            case ModelConstruct3DHelper.HitFace.Right:
    //                if (canSnapSize)
    //                {
    //                    newSize.X = nearestSubData.Size.Z;
    //                }
    //                newLocalPosition.Z = nearestSubData.Position.Z + newSize.X;
    //                newRotation.Angle += CommonHelper.ToRadians(90);
    //                break;
    //        }


    //        if (snapToNotch)
    //        {
    //            Double? notchSnap =
    //                    PlanDocumentHelper.GetNotchSnap(nearestSubComponent,
    //                    newSize.Y, newLocalPosition.Y, nearestSubHitResult.SubComponentFaceHit);
    //            if (notchSnap.HasValue)
    //            {
    //                newLocalPosition.Y = notchSnap.Value;
    //            }
    //        }

    //        //convert the position back to world space.
    //        newPos = ModelConstruct3DHelper.ToWorld(newLocalPosition, nearestSubData);



    //        //update the passed in values
    //        if (canSnapSize)
    //        {
    //            itemSize.X = newSize.X;
    //            itemSize.Y = newSize.Y;
    //            itemSize.Z = newSize.Z;
    //        }

    //        itemWorldPos.X = newPos.X;
    //        itemWorldPos.Y = newPos.Y;
    //        itemWorldPos.Z = newPos.Z;

    //        itemWorldRotation.Angle = newRotation.Angle;
    //        itemWorldRotation.Slope = newRotation.Slope;
    //        itemWorldRotation.Roll = newRotation.Roll;
    //    }
    //}

    #endregion

    #endregion

    //public sealed class PlanogramHitTestResult
    //{
    //    public Point3D ViewerHitPoint { get; set; }

    //    public PlanSubComponent3DData NearestSubComponentData { get; set; }
    //    public PlanogramSubComponentView NearestSubComponent { get; set; }
    //    public Point3D SubComponentLocalHitPoint { get; set; }
    //    public Point3D SubComponentWorldHitPoint { get; set; }
    //    public ModelConstruct3DHelper.HitFace SubComponentFaceHit { get; set; }

    //    public PlanogramFixtureView NearestFixture { get; set; }

    //}

    //public static PlanogramSubComponentView CreateNewBoxPart(IPlanDocument doc, Point3D modelPos, Size3D modelSize)
    //{
    //    PlanogramView plan = doc.Planogram;
    //    PlanogramFixtureView fixture = null;
    //    PlanogramAssemblyView assembly = null;

    //    if (plan.Fixtures.Count > 0)
    //    {
    //        fixture = plan.Fixtures.First();
    //        assembly = fixture.Assemblies.FirstOrDefault();
    //    }

    //    if (fixture == null)
    //    {
    //        fixture = plan.AddFixture();
    //        assembly = null;
    //    }
    //    if (assembly == null)
    //    {
    //        assembly = fixture.AddAssembly();
    //    }

    //    PlanogramComponentView component = assembly.AddComponent();
    //    component.IsSelectable = true;

    //    PlanogramSubComponentView subcomponent = component.AddSubComponent();
    //    subcomponent.IsSelectable = true;

    //    subcomponent.X = Convert.ToSingle(modelPos.X);
    //    subcomponent.Y = Convert.ToSingle(modelPos.Y);
    //    subcomponent.Z = Convert.ToSingle(modelPos.Z);
    //    subcomponent.Width = Convert.ToSingle(modelSize.X);
    //    subcomponent.Height = Convert.ToSingle(modelSize.Y);
    //    subcomponent.Depth = Convert.ToSingle(modelSize.Z);


    //    return subcomponent;
    //}


    ///// <summary>
    ///// Returns a bounding rectangle for the given subcomponent view in world space.
    ///// </summary>
    ///// <param name="subComponentView"></param>
    ///// <returns></returns>
    //public static Rect3D GetWorldSpaceBounds(PlanogramSubComponentView sub)
    //{
    //    Rect3D bounds = Rect3D.Empty;

    //    //Create the transform
    //    Matrix3D totalTransform = Matrix3D.Identity;
    //    Transform3DGroup transforms;

    //    //sub transform
    //    Point3D subCenter = new Point3D(sub.X + (sub.Width / 2), sub.Y + (sub.Height / 2), sub.Z - (sub.Depth / 2));
    //    transforms = new Transform3DGroup();
    //    transforms.Children.Add(Model3DHelper.CreateYawPitchRoll(sub.Angle, sub.Slope, sub.Roll/*, subCenter*/));
    //    transforms.Children.Add(new TranslateTransform3D(sub.X, sub.Y, sub.Z));
    //    totalTransform.Append(transforms.Value);

    //    //component transform
    //    PlanogramComponentView comp = sub.Component;
    //    if (comp != null)
    //    {
    //        Point3D compCenter = new Point3D(comp.X + (comp.Width / 2), comp.Y + (comp.Height / 2), comp.Z - (comp.Depth / 2));
    //        transforms = new Transform3DGroup();
    //        transforms.Children.Add(Model3DHelper.CreateYawPitchRoll(comp.Angle, comp.Slope, comp.Roll/*, compCenter*/));
    //        transforms.Children.Add(new TranslateTransform3D(comp.X, comp.Y, comp.Z));
    //        totalTransform.Append(transforms.Value);
    //    }

    //    //assembly transform
    //    PlanogramAssemblyView assembly = sub.Assembly;
    //    if (assembly != null)
    //    {
    //        Point3D assemblyCenter = new Point3D(assembly.X + (assembly.Width / 2), assembly.Y + (assembly.Height / 2), assembly.Z - (assembly.Depth / 2));
    //        transforms = new Transform3DGroup();
    //        transforms.Children.Add(Model3DHelper.CreateYawPitchRoll(assembly.Angle, assembly.Slope, assembly.Roll/*, assemblyCenter*/));
    //        transforms.Children.Add(new TranslateTransform3D(assembly.X, assembly.Y, assembly.Z));
    //        totalTransform.Append(transforms.Value);
    //    }

    //    //fixture transform
    //    PlanogramFixtureView fix = comp.Fixture;
    //    if (fix != null)
    //    {
    //        Point3D fixtureCenter = new Point3D(fix.X + (fix.Width / 2), fix.Y + (fix.Height / 2), fix.Z - (fix.Depth / 2));
    //        transforms = new Transform3DGroup();
    //        transforms.Children.Add(Model3DHelper.CreateYawPitchRoll(fix.Angle, fix.Slope, fix.Roll/*, fixtureCenter*/));
    //        transforms.Children.Add(new TranslateTransform3D(fix.X, fix.Y, fix.Z));
    //        totalTransform.Append(transforms.Value);
    //    }


    //    bounds = new Rect3D()
    //    {
    //        SizeX = sub.Width,
    //        SizeY = sub.Height,
    //        SizeZ = sub.Depth
    //    };
    //    bounds = new MatrixTransform3D(totalTransform).TransformBounds(bounds);

    //    return bounds;
    //}

    ///// <summary>
    ///// Returns the bounds of the given component in world space.
    ///// </summary>
    ///// <param name="comp"></param>
    ///// <returns></returns>
    //public static Rect3D GetWorldSpaceBounds(PlanogramComponentView comp)
    //{
    //    Rect3D bounds = Rect3D.Empty;

    //    foreach (PlanogramSubComponentView sub in comp.SubComponents)
    //    {
    //        Rect3D subBounds = PlanDocumentHelper.GetWorldSpaceBounds(sub);
    //        if (!subBounds.IsEmpty)
    //        {
    //            bounds.Union(subBounds);
    //        }
    //    }

    //    return bounds;
    //}

    //public static Rect3D GetWorldSpaceBounds(PlanogramAnnotationView anno)
    //{
    //    Rect3D bounds = Rect3D.Empty;

    //    //Create the transform
    //    Matrix3D totalTransform = Matrix3D.Identity;
    //    Transform3DGroup transforms;

    //    PlanogramSubComponentView sub = anno.SubComponent;
    //    if (sub != null)
    //    {
    //        //sub transform
    //        Point3D subCenter = new Point3D(sub.X + (sub.Width / 2), sub.Y + (sub.Height / 2), sub.Z - (sub.Depth / 2));
    //        transforms = new Transform3DGroup();
    //        transforms.Children.Add(Model3DHelper.CreateYawPitchRoll(sub.Angle, sub.Slope, sub.Roll/*, subCenter*/));
    //        transforms.Children.Add(new TranslateTransform3D(sub.X, sub.Y, sub.Z));
    //        totalTransform.Append(transforms.Value);
    //    }

    //    //component transform
    //    PlanogramComponentView comp = anno.Component;
    //    if (comp != null)
    //    {
    //        Point3D compCenter = new Point3D(comp.X + (comp.Width / 2), comp.Y + (comp.Height / 2), comp.Z - (comp.Depth / 2));
    //        transforms = new Transform3DGroup();
    //        transforms.Children.Add(Model3DHelper.CreateYawPitchRoll(comp.Angle, comp.Slope, comp.Roll/*, compCenter*/));
    //        transforms.Children.Add(new TranslateTransform3D(comp.X, comp.Y, comp.Z));
    //        totalTransform.Append(transforms.Value);
    //    }

    //    //assembly transform
    //    PlanogramAssemblyView assembly = anno.Assembly;
    //    if (assembly != null)
    //    {
    //        Point3D assemblyCenter = new Point3D(assembly.X + (assembly.Width / 2), assembly.Y + (assembly.Height / 2), assembly.Z - (assembly.Depth / 2));
    //        transforms = new Transform3DGroup();
    //        transforms.Children.Add(Model3DHelper.CreateYawPitchRoll(assembly.Angle, assembly.Slope, assembly.Roll/*, assemblyCenter*/));
    //        transforms.Children.Add(new TranslateTransform3D(assembly.X, assembly.Y, assembly.Z));
    //        totalTransform.Append(transforms.Value);
    //    }

    //    //fixture transform
    //    PlanogramFixtureView fix = anno.Fixture;
    //    if (fix != null)
    //    {
    //        Point3D fixtureCenter = new Point3D(fix.X + (fix.Width / 2), fix.Y + (fix.Height / 2), fix.Z - (fix.Depth / 2));
    //        transforms = new Transform3DGroup();
    //        transforms.Children.Add(Model3DHelper.CreateYawPitchRoll(fix.Angle, fix.Slope, fix.Roll/*, fixtureCenter*/));
    //        transforms.Children.Add(new TranslateTransform3D(fix.X, fix.Y, fix.Z));
    //        totalTransform.Append(transforms.Value);
    //    }

    //    bounds = new Rect3D()
    //    {
    //        SizeX = anno.Width,
    //        SizeY = anno.Height,
    //        SizeZ = anno.Depth
    //    };
    //    bounds = new MatrixTransform3D(totalTransform).TransformBounds(bounds);

    //    return bounds;
    //}

    ///// <summary>
    ///// Gets the relative transform for the given plan item.
    ///// </summary>
    ///// <param name="planItem"></param>
    ///// <returns></returns>
    //private static Matrix3D GetRelativeTransform(IPlanItem planItem)
    //{
    //    Matrix3D relativeTransform = Matrix3D.Identity;

    //    List<PlanItemType> itemOrder = PlanItemHelper.GetTypeOrderAsc();

    //    Boolean typeFound = false;
    //    foreach (PlanItemType type in itemOrder)
    //    {
    //        if (typeFound)
    //        {
    //            IPlanItem processItem = PlanItemHelper.GetItemValue(planItem, type);
    //            if (processItem != null)
    //            {
    //                Transform3DGroup transforms = new Transform3DGroup();
    //                transforms.Children.Add(Model3DHelper.CreateYawPitchRoll(processItem.Angle, processItem.Slope, processItem.Roll));
    //                transforms.Children.Add(new TranslateTransform3D(processItem.X, processItem.Y, processItem.Z));
    //                relativeTransform.Append(transforms.Value);
    //            }

    //        }
    //        else if (type == planItem.PlanItemType)
    //        {
    //            //don't include this type as we want the transform relative to its parent
    //            typeFound = true;
    //        }
    //    }

    //    return relativeTransform;
    //}

    ///// <summary>
    ///// Returns the given local point relative to world space.
    ///// </summary>
    ///// <param name="localPoint"></param>
    ///// <param name="planItem"></param>
    ///// <returns></returns>
    //public static Point3D ToWorld(Point3D localPoint, IPlanItem planItem)
    //{
    //    Matrix3D relativeTransform = GetRelativeTransform(planItem);
    //    return new MatrixTransform3D(relativeTransform).Transform(localPoint);
    //}

    ///// <summary>
    ///// Returns the given point local to the planitem.
    ///// </summary>
    ///// <param name="point"></param>
    ///// <param name="planItem"></param>
    ///// <returns></returns>
    //public static Point3D ToLocal(Point3D worldSpacePoint, IPlanItem planItem)
    //{
    //    Matrix3D relativeTransform = GetRelativeTransform(planItem);
    //    relativeTransform.Invert();
    //    return new MatrixTransform3D(relativeTransform).Transform(worldSpacePoint);
    //}


    //public static PlanogramComponentView CreateNewQuickComponent(IPlanDocument doc, Point3D modelPos, Size3D modelSize, MouseToolType toolType)
    //{
    //    PlanogramView plan = doc.Planogram;

    //    PlanogramFixtureView fixture = null;
    //    if (plan.Fixtures.Count == 0 || toolType == MouseToolType.CreateBackboard)
    //    {
    //        fixture = plan.AddFixture();
    //    }
    //    else
    //    {
    //        fixture = plan.Fixtures.First();
    //    }

    //    PlanogramAssemblyView assembly = fixture.AddAssembly();


    //    //Create the component itself
    //    PlanogramComponentView component = assembly.AddComponent();
    //    component.IsSelectable = true;
    //    component.X = Convert.ToSingle(modelPos.X);
    //    component.Y = Convert.ToSingle(modelPos.Y);
    //    component.Z = Convert.ToSingle(modelPos.Z);

    //    //add the parts for the type.
    //    switch (toolType)
    //    {
    //        default: throw new NotSupportedException();

    //        #region Backboard
    //        case MouseToolType.CreateBackboard:
    //            {
    //                component.Name = "Backboard";
    //                component.IsMoveable = false;

    //                PlanogramSubComponentView subcomponent = component.AddSubComponent();
    //                subcomponent.Name = "Backboard";
    //                subcomponent.Width = Convert.ToSingle(modelSize.X);
    //                subcomponent.Height = Convert.ToSingle(modelSize.Y);
    //                subcomponent.Depth = Convert.ToSingle(modelSize.Z);
    //                subcomponent.FillColour = IntToColorConverter.ColorToInt(Colors.Silver);
    //                subcomponent.SubComponentType = SubComponentType.Backboard;
    //            }
    //            break;
    //        #endregion

    //        #region Shelf
    //        case MouseToolType.CreateShelf:
    //            {
    //                component.Name = "Shelf";
    //                component.IsMoveable = false;

    //                PlanogramSubComponentView subcomponent = component.AddSubComponent();
    //                subcomponent.Name = "Shelf";
    //                subcomponent.Width = Convert.ToSingle(modelSize.X);
    //                subcomponent.Height = Convert.ToSingle(modelSize.Y);
    //                subcomponent.Depth = Convert.ToSingle(modelSize.Z);
    //                subcomponent.IsMerchandisable = true;
    //                subcomponent.SubComponentType = SubComponentType.Shelf;
    //                subcomponent.FillColour = IntToColorConverter.ColorToInt(Colors.SkyBlue);
    //                subcomponent.RiserThickness = 1;
    //                subcomponent.RiserHeight = 5;
    //            }
    //            break;
    //        #endregion

    //        #region Chest
    //        case MouseToolType.CreateChest:
    //            {
    //                component.Name = "Chest";
    //                component.IsMoveable = false;

    //                PlanogramSubComponentView subcomponent = component.AddSubComponent();
    //                subcomponent.Name = "Chest";
    //                subcomponent.Width = Convert.ToSingle(modelSize.X);
    //                subcomponent.Height = Convert.ToSingle(modelSize.Y);
    //                subcomponent.Depth = Convert.ToSingle(modelSize.Z);
    //                subcomponent.FillColour = IntToColorConverter.ColorToInt(Colors.Gainsboro);
    //                subcomponent.FillColourBottom = IntToColorConverter.ColorToInt(Colors.Black);
    //                subcomponent.FaceThickness = 4F;
    //                subcomponent.IsMerchandisable = true;
    //                subcomponent.SubComponentType = SubComponentType.Chest;



    //                //Single chestWallThick = 4F;

    //                //PlanogramSubComponentView basePart = (PlanogramSubComponentView)component.AddSubComponent();
    //                //basePart.Name = "Chest base";
    //                //basePart.IsMerchandisable = true;
    //                //basePart.SubComponentType = SubComponentType.Chest;
    //                //basePart.FillColour = IntToColorConverter.ColorToInt(Colors.Black);
    //                //basePart.Height = chestWallThick;
    //                //basePart.Width = Convert.ToSingle(modelSize.X);
    //                //basePart.Depth = Convert.ToSingle(modelSize.Z);
    //                //basePart.X = 0;
    //                //basePart.Y = 0;
    //                //basePart.Z = 0;

    //                //PlanogramSubComponentView frontWall = (PlanogramSubComponentView)component.AddSubComponent();
    //                //frontWall.Name = "Chest Front Wall";
    //                //frontWall.IsMerchandisable = false;
    //                //frontWall.SubComponentType = SubComponentType.Chest;
    //                //frontWall.FillColour = IntToColorConverter.ColorToInt(Colors.Gray);
    //                //frontWall.Height = Convert.ToSingle(modelSize.Y - chestWallThick);
    //                //frontWall.Width = Convert.ToSingle(modelSize.X - (chestWallThick * 2));
    //                //frontWall.Depth = chestWallThick;
    //                //frontWall.X = chestWallThick;
    //                //frontWall.Y = chestWallThick;
    //                //frontWall.Z = 0;

    //                //PlanogramSubComponentView backWall = (PlanogramSubComponentView)component.AddSubComponent();
    //                //backWall.Name = "Chest Back Wall";
    //                //backWall.IsMerchandisable = false;
    //                //backWall.SubComponentType = SubComponentType.Chest;
    //                //backWall.FillColour = frontWall.FillColour;
    //                //backWall.Height = Convert.ToSingle(modelSize.Y - chestWallThick);
    //                //backWall.Width = Convert.ToSingle(modelSize.X - (chestWallThick * 2));
    //                //backWall.Depth = chestWallThick;
    //                //backWall.X = chestWallThick;
    //                //backWall.Y = chestWallThick;
    //                //backWall.Z = Convert.ToSingle(-(modelSize.Z - chestWallThick));

    //                //PlanogramSubComponentView leftWall = (PlanogramSubComponentView)component.AddSubComponent();
    //                //leftWall.Name = "Chest Left Wall";
    //                //leftWall.IsMerchandisable = false;
    //                //leftWall.SubComponentType = SubComponentType.Chest;
    //                //leftWall.FillColour = frontWall.FillColour;
    //                //leftWall.Height = Convert.ToSingle(modelSize.Y - chestWallThick);
    //                //leftWall.Width = chestWallThick;
    //                //leftWall.Depth = Convert.ToSingle(modelSize.Z);
    //                //leftWall.X = 0;
    //                //leftWall.Y = chestWallThick;
    //                //leftWall.Z = 0;

    //                //PlanogramSubComponentView rightWall = (PlanogramSubComponentView)component.AddSubComponent();
    //                //rightWall.Name = "Chest Right Wall";
    //                //rightWall.IsMerchandisable = false;
    //                //rightWall.SubComponentType = SubComponentType.Chest;
    //                //rightWall.FillColour = frontWall.FillColour;
    //                //rightWall.Height = Convert.ToSingle(modelSize.Y - chestWallThick);
    //                //rightWall.Width = chestWallThick;
    //                //rightWall.Depth = Convert.ToSingle(modelSize.Z);
    //                //rightWall.X = Convert.ToSingle(modelSize.X - chestWallThick);
    //                //rightWall.Y = chestWallThick;
    //                //rightWall.Z = 0;

    //            }
    //            break;
    //        #endregion

    //        #region Bar
    //        case MouseToolType.CreateBar:
    //            {
    //                component.Name = "Bar";
    //                component.IsMoveable = false;

    //                PlanogramSubComponentView subcomponent = component.AddSubComponent();
    //                subcomponent.Name = "Bar";
    //                subcomponent.Width = Convert.ToSingle(modelSize.X);
    //                subcomponent.Height = Convert.ToSingle(modelSize.Y);
    //                subcomponent.Depth = Convert.ToSingle(modelSize.Z);
    //                subcomponent.FillColour = IntToColorConverter.ColorToInt(Colors.DarkGreen);
    //                subcomponent.SubComponentType = SubComponentType.Bar;
    //                subcomponent.IsMerchandisable = true;
    //            }
    //            break;
    //        #endregion

    //        #region Pegboard
    //        case MouseToolType.CreatePegboard:
    //            {
    //                component.Name = "Pegboard";
    //                component.IsMoveable = false;

    //                PlanogramSubComponentView subcomponent = component.AddSubComponent();
    //                subcomponent.Name = "Pegboard";
    //                subcomponent.Width = Convert.ToSingle(modelSize.X);
    //                subcomponent.Height = Convert.ToSingle(modelSize.Y);
    //                subcomponent.Depth = Convert.ToSingle(modelSize.Z);
    //                subcomponent.FillColour = IntToColorConverter.ColorToInt(Colors.Goldenrod);
    //                subcomponent.SubComponentType = SubComponentType.Peg;
    //                subcomponent.IsMerchandisable = true;
    //            }
    //            break;
    //        #endregion

    //        #region CreatePallet
    //        case MouseToolType.CreatePallet:
    //            {
    //                component.Name = "Pallet";
    //                component.IsMoveable = true;

    //                PlanogramSubComponentView subcomponent = component.AddSubComponent();
    //                subcomponent.Name = "Pallet";
    //                subcomponent.Width = Convert.ToSingle(modelSize.X);
    //                subcomponent.Height = Convert.ToSingle(modelSize.Y);
    //                subcomponent.Depth = Convert.ToSingle(modelSize.Z);
    //                subcomponent.FillColour = IntToColorConverter.ColorToInt(Colors.SaddleBrown);
    //                subcomponent.SubComponentType = SubComponentType.Shelf;
    //                subcomponent.IsMerchandisable = true;
    //            }
    //            break;
    //        #endregion

    //        #region CreatePanel
    //        case MouseToolType.CreatePanel:
    //            {
    //                component.Name = "Panel";
    //                component.IsMoveable = false;

    //                PlanogramSubComponentView subcomponent = component.AddSubComponent();
    //                subcomponent.Name = "Panel";
    //                subcomponent.Width = Convert.ToSingle(modelSize.X);
    //                subcomponent.Height = Convert.ToSingle(modelSize.Y);
    //                subcomponent.Depth = Convert.ToSingle(modelSize.Z);
    //                subcomponent.IsMerchandisable = false;
    //                subcomponent.SubComponentType = SubComponentType.None;
    //                subcomponent.FillColour = IntToColorConverter.ColorToInt(Colors.Gray);

    //            }
    //            break;
    //        #endregion
    //    }

    //    return component;
    //}


    //public static void ProcessPlanItemsKeyCommand(IEnumerable<IPlanItem> items, ModifierKeys modifier, Key key, ProjectionCamera camera)
    //{
    //    if (modifier == ModifierKeys.Control)
    //    {
    //        switch (key)
    //        {
    //            case Key.Up:
    //            case Key.Down:
    //            case Key.Left:
    //            case Key.Right:
    //                PlanDocumentHelper.MovePlanItems(items, key, camera);
    //                break;
    //        }

    //    }
    //    else if (modifier == ModifierKeys.Shift)
    //    {
    //        switch (key)
    //        {
    //            case Key.Up:
    //            case Key.Down:
    //            case Key.Left:
    //            case Key.Right:
    //                PlanDocumentHelper.RotatePlanItems(items, key, camera);
    //                break;
    //        }
    //    }
    //}

    ///// <summary>
    ///// Moves the given plan items based on the key and camera direction.
    ///// </summary>
    ///// <param name="items"></param>
    ///// <param name="key"></param>
    ///// <param name="upDirection"></param>
    ///// <param name="normal"></param>
    //private static void MovePlanItems(IEnumerable<IPlanItem> items, Key key, ProjectionCamera camera)
    //{
    //    const Single changeAmount = 1F;

    //    Double changeX = 0;
    //    Double changeY = 0;

    //    switch (key)
    //    {
    //        case Key.Up: changeY = changeAmount; break;
    //        case Key.Down: changeY = -changeAmount; break;
    //        case Key.Left: changeX = -changeAmount; break;
    //        case Key.Right: changeX = changeAmount; break;
    //    }

    //    if (changeX != 0 || changeY != 0)
    //    {
    //        Vector3D moveVector = Model3DHelper.FindItemMoveVector(camera, changeX, changeY);

    //        foreach (IPlanItem item in items)
    //        {
    //            if (moveVector.X != 0)
    //            {
    //                item.X += Convert.ToSingle(moveVector.X);
    //            }
    //            if (moveVector.Y != 0)
    //            {
    //                item.Y += Convert.ToSingle(moveVector.Y);
    //            }
    //            if (moveVector.Z != 0)
    //            {
    //                item.Z += Convert.ToSingle(moveVector.Z);
    //            }
    //        }
    //    }
    //}
    //private static void RotatePlanItems(IEnumerable<IPlanItem> items, Key key, ProjectionCamera camera)
    //{
    //    Single changeAmount = Convert.ToSingle(2 / (360f / (2 * Math.PI)));

    //    Double rotateX = 0;
    //    Double rotateY = 0;

    //    switch (key)
    //    {
    //        case Key.Up: rotateX = -changeAmount; break;
    //        case Key.Down: rotateX = changeAmount; break;
    //        case Key.Left: rotateY = -changeAmount; break;
    //        case Key.Right: rotateY = changeAmount; break;
    //    }

    //    if (rotateX != 0 || rotateY != 0)
    //    {
    //        Vector3D moveVector = Model3DHelper.FindItemMoveVector(camera, rotateX, rotateY);

    //        foreach (IPlanItem item in items)
    //        {
    //            if (moveVector.X != 0)
    //            {
    //                item.Slope += Convert.ToSingle(moveVector.X);
    //            }
    //            if (moveVector.Y != 0)
    //            {
    //                item.Angle += Convert.ToSingle(moveVector.Y);
    //            }
    //            if (moveVector.Z != 0)
    //            {
    //                item.Roll += Convert.ToSingle(moveVector.Z);
    //            }
    //        }
    //    }









    //    //switch (key)
    //    //{
    //    //    case Key.OemComma:
    //    //        {
    //    //            foreach (IPlanItem item in items)
    //    //            {
    //    //                item.Angle += rotateValue;
    //    //            }
    //    //        }
    //    //        break;

    //    //    case Key.OemPeriod:
    //    //        {
    //    //            foreach (IPlanItem item in items)
    //    //            {
    //    //                item.Slope += rotateValue;
    //    //            }
    //    //        }
    //    //        break;

    //    //    case Key.OemQuestion:
    //    //        {
    //    //            foreach (IPlanItem item in items)
    //    //            {
    //    //                item.Roll += rotateValue;
    //    //            }
    //    //        }
    //    //        break;
    //    //}
    //}

    #region Positions

    ///// <summary>
    ///// Adds the positions to the given subcomponent at the given position.
    ///// </summary>
    ///// <param name="positions">the positions that were dragged.</param>
    ///// <param name="subComponent">the subcomponent hit.</param>
    ///// <param name="mouseHitPoint">the point hit by the mouse</param>
    ///// <param name="dropPoints">the actual position drop points</param>
    ///// <param name="isCopy">true if the positions are to be copied.</param>
    ///// <returns></returns>
    //public static List<PlanogramPositionView> AddPositions(
    //     IEnumerable<PlanogramPositionView> positions, PlanogramSubComponentView subComponent,
    //    PointValue mouseHitPoint, List<PointValue> dropPoints, Boolean isCopy, PlanogramPositionView hitExistingPosition)
    //{
    //    List<PlanogramPositionView> newPositions = new List<PlanogramPositionView>();

    //    if (positions.Count() == 0) { return newPositions; }

    //    Debug.Assert(positions.Count() == dropPoints.Count, "Drop point and positions not matching.");

    //    //Add new positions as required.
    //    for (Int32 idx = 0; idx < positions.Count(); idx++)
    //    {
    //        PlanogramPositionView addPosition = positions.ElementAt(idx);
    //        PlanogramPositionView newPosition = addPosition;

    //        PointValue dropPoint = dropPoints[idx];

    //        if (isCopy)
    //        {
    //            newPosition = subComponent.AddPositionCopy(addPosition);
    //        }
    //        else if (addPosition.SubComponent != subComponent)
    //        {
    //            newPosition = subComponent.MovePosition(addPosition);
    //        }

    //        newPosition.X = dropPoint.X;
    //        newPosition.Y = dropPoint.Y;
    //        newPosition.Z = dropPoint.Z;

    //        newPositions.Add(newPosition);
    //    }


    //    //determine the new position order
    //    if (hitExistingPosition != null)
    //    {
    //        Single exPosCenter = hitExistingPosition.X + (hitExistingPosition.Width / 2F);

    //        Int16 dropSeqStart = hitExistingPosition.Sequence;
    //        if (exPosCenter < mouseHitPoint.X)
    //        {
    //            dropSeqStart++;
    //        }

    //        List<PlanogramPositionView> reqSeqPositions =
    //            subComponent.Positions
    //            .Where(p => !newPositions.Contains(p) && p.Sequence >= dropSeqStart)
    //            .OrderBy(p => p.Sequence).ToList();

    //        Int16 curSeq = dropSeqStart;
    //        foreach (PlanogramPositionView newPos in newPositions)
    //        {
    //            newPos.Sequence = curSeq;
    //            curSeq++;
    //        }
    //        foreach (PlanogramPositionView p in reqSeqPositions)
    //        {
    //            p.Sequence = curSeq;
    //            curSeq++;
    //        }

    //    }
    //    else
    //    {
    //        Boolean newPositionsSet = false;
    //        List<PlanogramPositionView> orderedSubPositions = new List<PlanogramPositionView>();

    //        foreach (PlanogramPositionView pos in subComponent.Positions.OrderBy(p => p.Sequence).ToList())
    //        {
    //            if (!newPositions.Contains(pos))
    //            {
    //                Double posCenter = pos.X + (pos.Width / 2);
    //                if (!newPositionsSet && posCenter > mouseHitPoint.X)
    //                {
    //                    orderedSubPositions.AddRange(newPositions);
    //                    newPositionsSet = true;
    //                }

    //                orderedSubPositions.Add(pos);
    //            }
    //        }

    //        if (!newPositionsSet)
    //        {
    //            orderedSubPositions.AddRange(newPositions);
    //            newPositionsSet = true;
    //        }

    //        //resequence
    //        Int16 curSeq = 1;
    //        foreach (PlanogramPositionView pos in orderedSubPositions)
    //        {
    //            pos.Sequence = curSeq;
    //            curSeq++;
    //        }
    //    }


    //    return newPositions;
    //}

    #endregion

    #region Model Controllers

    //public static Model3DPositionController AddTranslateModelController(IPlanItem item, ModelConstruct3D parentPlanModel, Viewport3D viewer)
    //{
    //    Model3DPositionController controller = null;

    //    ModelConstruct3D model = null;

    //    IModelConstruct3DData modelData = PlanDocumentHelper.GetItemSelectionModel(((Plan3DData)parentPlanModel.ModelData), item);
    //    if (modelData != null)
    //    {
    //        model = parentPlanModel.FindModel(modelData);
    //    }

    //    if (model != null)
    //    {
    //        model.UpdateCenterProperty();

    //        var positionController = new Model3DPositionController();
    //        controller = positionController;

    //        BindingOperations.SetBinding(positionController, Model3DPositionController.CenterProperty,
    //           new Binding(ModelConstruct3D.WorldCenterProperty.Name) { Source = model });

    //        //BindingOperations.SetBinding(positionController, Model3DPositionController.TargetPositionProperty,
    //        //    new Binding(ModelConstruct3D.PositionProperty.Name) { Source = model.ModelData, Mode = BindingMode.TwoWay });
    //        // target position change now handled by event

    //        positionController.Visibility = Visibility.Collapsed;
    //        viewer.Children.Add(positionController);
    //        positionController.Visibility = Visibility.Visible;
    //    }

    //    return controller;
    //}

    //public static Model3DSizeController AddResizeModelController(IPlanItem item, ModelConstruct3D parentPlanModel, Viewport3D viewer)
    //{
    //    Model3DSizeController controller = null;

    //    ModelConstruct3D model = null;

    //    IModelConstruct3DData modelData = PlanDocumentHelper.GetItemSelectionModel(((Plan3DData)parentPlanModel.ModelData), item);
    //    if (modelData != null)
    //    {
    //        model = parentPlanModel.FindModel(modelData);
    //    }

    //    if (model != null)
    //    {
    //        model.UpdateCenterProperty();

    //        var sizeController = new Model3DSizeController();
    //        controller = sizeController;

    //        BindingOperations.SetBinding(sizeController, Model3DSizeController.CenterProperty,
    //           new Binding(ModelConstruct3D.WorldCenterProperty.Name) { Source = model });

    //        // target size change will be dealt with by event handlers.

    //        sizeController.Visibility = Visibility.Collapsed;
    //        viewer.Children.Add(sizeController);
    //        sizeController.Visibility = Visibility.Visible;

    //    }

    //    return controller;
    //}

    //public static Rotation3DController AddRotateModelController(IPlanItem item, ModelConstruct3D parentPlanModel, Viewport3D viewer)
    //{
    //    Rotation3DController controller = null;

    //    ModelConstruct3D model = null;

    //    IModelConstruct3DData modelData = PlanDocumentHelper.GetItemSelectionModel(parentPlanModel.ModelData as ModelConstruct3DData, item);
    //    if (modelData != null)
    //    {
    //        model = parentPlanModel.FindModel(modelData);
    //    }

    //    if (model != null)
    //    {
    //        model.UpdateCenterProperty();

    //        var rotateController = new Rotation3DController()
    //        {
    //            Length = 1D,
    //            InnerDiameter = 25D,
    //            Diameter = 26D
    //        };
    //        controller = rotateController;

    //        if (viewer.Camera is OrthographicCamera)
    //        {
    //            Vector3D moveVector = Model3DHelper.FindItemMoveVector(viewer.Camera as ProjectionCamera, 2, 2);
    //            rotateController.CanRotateX = (moveVector.X == 0);
    //            rotateController.CanRotateY = (moveVector.Y == 0);
    //            rotateController.CanRotateZ = (moveVector.Z == 0);
    //        }


    //        BindingOperations.SetBinding(rotateController, Rotation3DController.CenterProperty,
    //               new Binding(ModelConstruct3D.WorldCenterProperty.Name) { Source = model });

    //        //BindingOperations.SetBinding(rotateController, Rotation3DController.TargetRotationProperty,
    //        //    new Binding(ModelConstruct3D.RotationProperty.Name) { Source = model.ModelData, Mode = BindingMode.TwoWay });

    //        rotateController.Visibility = Visibility.Collapsed;
    //        viewer.Children.Add(rotateController);
    //        rotateController.Visibility = Visibility.Visible;

    //    }

    //    return controller;
    //}

    //public static Transform3DController AddTransformModelController(IPlanItem item, ModelConstruct3D parentPlanModel, Viewport3D viewer)
    //{
    //    Transform3DController controller = null;

    //    ModelConstruct3D model = null;

    //    IModelConstruct3DData modelData = PlanDocumentHelper.GetItemSelectionModel(parentPlanModel.ModelData as ModelConstruct3DData, item);
    //    if (modelData != null)
    //    {
    //        model = parentPlanModel.FindModel(modelData);
    //    }

    //    if (model != null)
    //    {
    //        model.UpdateCenterProperty();

    //        var transformController = new Transform3DController();
    //        controller = transformController;

    //        BindingOperations.SetBinding(transformController, Transform3DController.CenterProperty,
    //           new Binding(ModelConstruct3D.WorldCenterProperty.Name) { Source = model });

    //        if (viewer.Camera is OrthographicCamera)
    //        {
    //            Vector3D moveVector = Model3DHelper.FindItemMoveVector(viewer.Camera as ProjectionCamera, 2, 2);
    //            transformController.CanTransformX = (moveVector.X != 0);
    //            transformController.CanTransformY = (moveVector.Y != 0);
    //            transformController.CanTransformZ = (moveVector.Z != 0);

    //            Rect3D modelBounds = model.GetBounds();
    //            if (!modelBounds.IsEmpty)
    //            {
    //                Double length = transformController.Length;
    //                Double minBoundsLength = new Double[] { modelBounds.SizeX, modelBounds.SizeY, modelBounds.SizeY }.Min();
    //                length = Math.Max(length, minBoundsLength * 0.1);

    //                transformController.Length = length;
    //            }

    //        }

    //        transformController.Visibility = Visibility.Collapsed;
    //        viewer.Children.Add(transformController);
    //        transformController.Visibility = Visibility.Visible;

    //    }

    //    return controller;
    //}

    //[Obsolete]
    //public static void Resize(IPlanItem item, Vector3D changeVector)
    //{
    //    if (item != null)
    //    {
    //        //we always resize the subcomponents
    //        List<IPlanItem> resizeItems = PlanItemHelper.GetAllChildItemsOfType(item, PlanItemType.SubComponent);
    //        foreach (IPlanItem ri in resizeItems)
    //        {
    //            PlanogramSubComponentView sub = ri.SubComponent;

    //            Single newHeight = Convert.ToSingle(sub.Height + changeVector.Y);
    //            if (newHeight > 0 && newHeight != sub.Height)
    //            {
    //                sub.Height = newHeight;
    //            }

    //            Single newWidth = Convert.ToSingle(sub.Width + changeVector.X);
    //            if (newWidth > 0 && newWidth != sub.Width)
    //            {
    //                sub.Width = newWidth;
    //            }

    //            Single newDepth = Convert.ToSingle(sub.Depth - changeVector.Z);
    //            if (newDepth > 0 && newDepth != sub.Depth)
    //            {
    //                sub.Depth = newDepth;
    //            }


    //        }
    //    }
    //}

    //[Obsolete]
    //public static void Reposition(IPlanItem item, Vector3D changeVector)
    //{
    //    if (item != null)
    //    {
    //        Single newY = Convert.ToSingle(item.Y + changeVector.Y);
    //        if (newY != item.Y)
    //        {
    //            item.Y = newY;
    //        }

    //        Single newX = Convert.ToSingle(item.X + changeVector.X);
    //        if (newX != item.X)
    //        {
    //            item.X = newX;
    //        }

    //        Single newZ = Convert.ToSingle(item.Z + changeVector.Z);
    //        if (newZ != item.Z)
    //        {
    //            item.Z = newZ;
    //        }

    //    }
    //}

    //[Obsolete]
    //public static void Rotate(IPlanItem item, Vector3D changeVector)
    //{
    //    if (item != null)
    //    {
    //        Single newAngle = Convert.ToSingle(Math.Round(item.Angle + changeVector.Y, 2));
    //        if (newAngle != item.Angle)
    //        {
    //            item.Angle = newAngle;
    //        }

    //        Single newSlope = Convert.ToSingle(Math.Round(item.Slope + changeVector.X, 2));
    //        if (newSlope != item.Slope)
    //        {
    //            item.Slope = newSlope;
    //        }

    //        Single newRoll = Convert.ToSingle(Math.Round(item.Roll + changeVector.Z, 2));
    //        if (newRoll != item.Roll)
    //        {
    //            item.Roll = newRoll;
    //        }

    //    }
    //}

    #endregion

    #region Snapping / Collision Detection

    ///// <summary>
    ///// Moves the given item to the new parent fixture.
    ///// </summary>
    ///// <param name="newFixtureParent"></param>
    ///// <param name="item"></param>
    ///// <returns></returns>
    //public static PlanogramComponentView MoveToFixture(PlanogramFixtureView newFixtureParent, PlanogramComponentView component)
    //{
    //    PlanogramComponentView movedItem = component;

    //    if (component.Fixture == newFixtureParent)
    //    {
    //        //already belongs to the fixture.
    //        return component;
    //    }
    //    if (component.Planogram != newFixtureParent.Planogram)
    //    {
    //        throw new ArgumentException("Item and new parent are not in the same planogram!");
    //    }
    //    //if (component.Assembly != null)
    //    //{
    //    //    throw new ArgumentException("This component is a child of an assembly");
    //    //}


    //    PlanogramFixtureView currentParent = component.Fixture;
    //    Point3D worldPos = PlanItemHelper.ToWorld(new Point3D(component.X, component.Y, component.Z), component);

    //    movedItem = newFixtureParent.AddComponentCopy(component);
    //    currentParent.RemoveComponent(component);

    //    //now adjust the position of the component to be local to the new fixture
    //    Point3D newLocalPos = PlanItemHelper.ToLocal(worldPos, movedItem);
    //    movedItem.X = Convert.ToSingle(newLocalPos.X);
    //    movedItem.Y = Convert.ToSingle(newLocalPos.Y);
    //    movedItem.Z = Convert.ToSingle(newLocalPos.Z);


    //    return movedItem;
    //}

    ///// <summary>
    ///// Snaps the given dimensions to the given subcomponent
    ///// </summary>
    ///// <param name="nearestSubComponent"></param>
    ///// <param name="localSubHit"></param>
    ///// <param name="itemSize"></param>
    ///// <param name="itemWorldPos"></param>
    ///// <param name="itemWorldRotation"></param>
    ///// <param name="canSnapSize"></param>
    //public static void SnapDimensions(PlanogramHitTestResult nearestSubHitResult,
    //    ref WidthHeightDepthValue itemSize, ref PointValue itemWorldPos, ref RotationValue itemWorldRotation, 
    //    Boolean canSnapSize, Boolean snapToNotch)
    //{
    //    PlanogramSubComponentView nearestSubComponent = nearestSubHitResult.NearestSubComponent;
    //    if (nearestSubComponent != null)
    //    {
    //        WidthHeightDepthValue newSize = new WidthHeightDepthValue(itemSize.X, itemSize.Y, itemSize.Z);
    //        PointValue newPos = new PointValue(itemWorldPos.X, itemWorldPos.Y, itemWorldPos.Z);
    //        RotationValue newRotation = PlanItemHelper.GetWorldRotation(nearestSubComponent);

    //        Point3D subHitLocalPos = nearestSubHitResult.SubComponentLocalHitPoint;

    //        PointValue newLocalPosition = new PointValue();
    //        newLocalPosition.X = nearestSubComponent.X + subHitLocalPos.X;
    //        newLocalPosition.Y = nearestSubComponent.Y + Math.Round(subHitLocalPos.Y - itemSize.Y / 2.0);
    //        newLocalPosition.Z = nearestSubComponent.Z + subHitLocalPos.Z;

    //        switch (nearestSubHitResult.SubComponentFaceHit)
    //        {
    //            case PlanDocumentHelper.HitFace.Front:
    //                if (canSnapSize)
    //                {
    //                    newSize.X = nearestSubComponent.Width;
    //                }
    //                newLocalPosition.X = nearestSubComponent.X;
    //                break;

    //            case PlanDocumentHelper.HitFace.Back:
    //                if (canSnapSize)
    //                {
    //                    newSize.X = nearestSubComponent.Width;
    //                }
    //                newLocalPosition.X = nearestSubComponent.X + nearestSubComponent.Width;
    //                newRotation.Angle += CommonHelper.ToRadians(-180);
    //                break;

    //            case PlanDocumentHelper.HitFace.Top:
    //                if (canSnapSize)
    //                {
    //                    newSize.X = nearestSubComponent.Width;
    //                    newSize.Z = nearestSubComponent.Depth;
    //                }
    //                newLocalPosition.X = nearestSubComponent.X;
    //                newLocalPosition.Z = nearestSubComponent.Z;
    //                break;

    //            case PlanDocumentHelper.HitFace.Bottom:
    //                if (canSnapSize)
    //                {
    //                    newSize.X = nearestSubComponent.Width;
    //                    newSize.Z = nearestSubComponent.Depth;
    //                }
    //                newLocalPosition.X = nearestSubComponent.X;
    //                newLocalPosition.Y = nearestSubComponent.Y - newSize.Y;
    //                newLocalPosition.Z = nearestSubComponent.Z;
    //                break;

    //            case PlanDocumentHelper.HitFace.Left:
    //                if (canSnapSize)
    //                {
    //                    newSize.X = nearestSubComponent.Depth;
    //                }
    //                newLocalPosition.Z = nearestSubComponent.Z;
    //                newRotation.Angle += CommonHelper.ToRadians(-90);
    //                break;

    //            case PlanDocumentHelper.HitFace.Right:
    //                if (canSnapSize)
    //                {
    //                    newSize.X = nearestSubComponent.Depth;
    //                }
    //                newLocalPosition.Z = nearestSubComponent.Z + newSize.X;
    //                newRotation.Angle += CommonHelper.ToRadians(90);
    //                break;
    //        }


    //        if (snapToNotch)
    //        {
    //            Double? notchSnap =
    //                    PlanDocumentHelper.GetNotchSnap(nearestSubComponent,
    //                    newSize.Y, newLocalPosition.Y, nearestSubHitResult.SubComponentFaceHit);
    //            if (notchSnap.HasValue)
    //            {
    //                newLocalPosition.Y = notchSnap.Value;
    //            }
    //        }

    //        //convert the position back to world space.
    //        newPos = PlanItemHelper.ToWorld(newLocalPosition, nearestSubComponent);



    //        //update the passed in values
    //        if(canSnapSize)
    //        {
    //            itemSize.X = newSize.X;
    //            itemSize.Y = newSize.Y;
    //            itemSize.Z = newSize.Z;
    //        }

    //        itemWorldPos.X = newPos.X;
    //        itemWorldPos.Y = newPos.Y;
    //        itemWorldPos.Z = newPos.Z;

    //        itemWorldRotation.Angle = newRotation.Angle;
    //        itemWorldRotation.Slope = newRotation.Slope;
    //        itemWorldRotation.Roll = newRotation.Roll;
    //    }
    //}

    ///// <summary>
    //   /// Returns the nearest fixture to the 2D hit point based on an intersection ray.
    //   /// </summary>
    //   /// <param name="vp"></param>
    //   /// <param name="camera"></param>
    //   /// <param name="hitPoint"></param>
    //   /// <returns></returns>
    //   [Obsolete]
    //   public static PlanogramFixtureView GetNearestFixture(
    //       Viewport3D vp, ProjectionCamera camera, Point hitPoint, PlanogramView plan)
    //   {
    //       PlanogramFixtureView nearestFixture = null;

    //       Vector3D normal = new Vector3D(camera.LookDirection.X, camera.LookDirection.Y, camera.LookDirection.Z);
    //       normal.Negate();
    //       normal.Normalize();

    //       //get fixture bounds
    //       Rect3D overallBounds = Rect3D.Empty;
    //       Dictionary<PlanogramFixtureView, Rect3D> fixtureBounds = GetFixtureBounds(plan, out overallBounds);

    //       //determine possible intersection points
    //       List<Point3D> posIntersections = new List<Point3D>();
    //       posIntersections = GetBoxPlaneIntersections(overallBounds, Viewport3DHelper.Point2DtoRay3D(vp, hitPoint));


    //       //cycle through the fixtures determining which one is intersected first.
    //       foreach (var fx in fixtureBounds.OrderBy(v => Model3DHelper.GetCameraDistance(v.Value, camera.Position)))
    //       {
    //           Boolean found = false;

    //           foreach (Point3D point in posIntersections)
    //           {
    //               if (fx.Value.Contains(point))
    //               {
    //                   nearestFixture = fx.Key;
    //                   found = true;
    //                   break;
    //               }
    //           }

    //           if (found) break;
    //       }


    //       return nearestFixture;
    //   }
    //   [Obsolete]
    //   public static Dictionary<PlanogramFixtureView, Rect3D> GetFixtureBounds(PlanogramView plan, out Rect3D totalBounds)
    //   {
    //       Rect3D overallBounds = Rect3D.Empty;

    //       Dictionary<PlanogramFixtureView, Rect3D> fixtureBounds = new Dictionary<PlanogramFixtureView, Rect3D>();
    //       foreach (var fixture in plan.Fixtures)
    //       {
    //           Rect3D fixtureRect = new Rect3D();
    //           fixtureRect.SizeX = fixture.Width;
    //           fixtureRect.SizeY = fixture.Height;
    //           fixtureRect.SizeZ = fixture.Depth;

    //           MatrixTransform3D transform = Model3DHelper.CreateTransform(
    //               new RotationValue(fixture.Angle, fixture.Slope, fixture.Roll),
    //               new PointValue(fixture.X, fixture.Y, fixture.Z));
    //           fixtureRect = transform.TransformBounds(fixtureRect);

    //           fixtureBounds.Add(fixture, fixtureRect);

    //           overallBounds = Rect3D.Union(overallBounds, fixtureRect);
    //       }

    //       totalBounds = overallBounds;

    //       return fixtureBounds;
    //   }

    ///// <summary>
    ///// Returns a hit test result for the subcomponent nearest to the 2D hit point 
    ///// based on an intersection ray.
    ///// </summary>
    ///// <param name="vp">The viewport.</param>
    ///// <param name="hitPoint">The 2d hit</param>
    ///// <param name="plan">The source planogram.</param>
    ///// <returns></returns>
    //[Obsolete]
    //public static PlanogramHitTestResult GetNearestSubComponent(Viewport3D vp, Point hitPoint, PlanogramView plan)
    //{
    //    PlanogramHitTestResult planHitResult = new PlanogramHitTestResult();

    //    ProjectionCamera camera = vp.Camera as ProjectionCamera;
    //    if(camera == null)
    //    {
    //        throw new ArgumentException("Viewer must use a projection camera");
    //    }


    //    PlanogramSubComponentView nearestSubComponent = null;

    //    Viewport3DHelper.HitResult hitResult;
    //    ModelConstruct3D nearestModel = GetNearestModelConstruct(vp, camera, hitPoint, out hitResult);
    //    if (nearestModel != null)
    //    {
    //        IPlanPart3DData planPartData = nearestModel.ModelData as IPlanPart3DData;
    //        if (planPartData != null)
    //        {
    //            IPlanItem planItem = planPartData.PlanPart as IPlanItem;
    //            if (planItem != null)
    //            {
    //                if (planItem.PlanItemType == PlanItemType.SubComponent)
    //                {
    //                    nearestSubComponent = planItem.SubComponent;
    //                }
    //            }
    //        }

    //    }

    //    planHitResult.ViewerHitPoint = Model3DHelper.GetPoint3D(hitPoint, vp);

    //    if (nearestSubComponent != null)
    //    {
    //        planHitResult.NearestSubComponentData = nearestModel.ModelData;
    //        planHitResult.NearestSubComponent = nearestSubComponent;
    //        planHitResult.NearestFixture = nearestSubComponent.Fixture;

    //        if (hitResult != null)
    //        {
    //            planHitResult.SubComponentLocalHitPoint = hitResult.RayHit.PointHit;
    //            planHitResult.SubComponentWorldHitPoint = PlanItemHelper.ToWorld(planHitResult.SubComponentLocalHitPoint, nearestSubComponent);
    //            planHitResult.SubComponentFaceHit = NormalToFacing(hitResult.Normal);
    //        }

    //    }
    //    else
    //    {
    //        planHitResult.NearestFixture = GetNearestFixture(vp, camera, hitPoint, plan);
    //    }


    //    return planHitResult;
    //}

    ///// <summary>
    ///// Returns the nearest merchandisable subcomponent to the hit point.
    ///// </summary>
    ///// <param name="vp"></param>
    ///// <param name="camera"></param>
    ///// <param name="hitPoint"></param>
    ///// <param name="plan"></param>
    ///// <returns></returns>
    //[Obsolete]
    //public static PlanogramSubComponentView GetNearestMerchandisableSubComponent(
    //    Viewport3D vp, ProjectionCamera camera, Point hitPoint, PlanogramView plan)
    //{
    //    //first check if we hit a merchandisable subcomponent directly.
    //    PlanogramHitTestResult subHitResult = GetNearestSubComponent(vp, hitPoint, plan);
    //    PlanogramSubComponentView nearestSubComponent = subHitResult.NearestSubComponent;

    //    if (nearestSubComponent == null || !nearestSubComponent.IsMerchandisable)
    //    {
    //        //no merch sub was hit directly so now lets see if we hit any when merch bounds are included.
    //        List<PlanogramSubComponentView> subsToCheck = new List<PlanogramSubComponentView>();

    //        //limit subs to hit fixture if possible.
    //        PlanogramFixtureView hitFixture = subHitResult.NearestFixture;
    //        if (hitFixture != null)
    //        {
    //            subsToCheck.AddRange(PlanItemHelper.GetAllChildItemsOfType(hitFixture, PlanItemType.SubComponent).Cast<PlanogramSubComponentView>());
    //        }
    //        else
    //        {
    //            subsToCheck.AddRange(plan.GetAllSubComponents());
    //        }


    //        Ray3D hitRay = Viewport3DHelper.Point2DtoRay3D(vp, hitPoint);

    //        Dictionary<PlanogramSubComponentView, Rect3D> subToBounds = new Dictionary<PlanogramSubComponentView, Rect3D>();
    //        foreach (PlanogramSubComponentView sub in subsToCheck)
    //        {
    //            if (sub.IsMerchandisable)
    //            {
    //                subToBounds.Add(sub, MerchandisingHelper.GetUnhinderedMerchandisingSpaceBounds(sub, /*toWorld*/true));
    //            }
    //        }

    //        nearestSubComponent = null;
    //        Double lastDist = Double.PositiveInfinity;
    //        foreach (var entry in subToBounds.OrderBy(v => Model3DHelper.GetCameraDistance(v.Value, camera.Position)))
    //        {
    //            List<Point3D> rayPlaneIntersections = GetBoxPlaneIntersections(entry.Value, hitRay);
    //            foreach (Point3D ri in rayPlaneIntersections.OrderBy(ri=> camera.Position.DistanceTo(ri)))
    //            {
    //                if(entry.Value.Contains(ri))
    //                {
    //                    Double distance = ri.DistanceTo(entry.Value.Location);

    //                    Boolean found = (nearestSubComponent == null)
    //                        || (distance < lastDist);


    //                    if(found)
    //                    {
    //                        nearestSubComponent = entry.Key;
    //                        lastDist = distance;
    //                    }
    //                    break;
    //                }
    //            }

    //        }
    //    }

    //    if (nearestSubComponent != null && !nearestSubComponent.IsMerchandisable)
    //    {
    //        //What happened??
    //        nearestSubComponent = null;       
    //    }

    //    return nearestSubComponent;
    //}

    //public static void ShiftByCollisions(IPlanItem item, CameraViewType viewtype)
    //{
    //    if (viewtype == CameraViewType.Perspective) { return; }

    //    Rect3D itemBounds = PlanItemHelper.GetWorldSpaceBounds(item);
    //    if (!itemBounds.IsEmpty)
    //    {
    //        List<IPlanItem> newSubParts = PlanItemHelper.GetAllChildItemsOfType(item, PlanItemType.SubComponent);
    //        Dictionary<PlanogramSubComponentView, Rect3D> newSubBoundsDict = new Dictionary<PlanogramSubComponentView, Rect3D>();

    //        //check if this new part collides with another.
    //        Dictionary<PlanogramSubComponentView, Rect3D> subBoundsDict = new Dictionary<PlanogramSubComponentView, Rect3D>();
    //        foreach (PlanogramSubComponentView sub in item.Planogram.GetAllSubComponents())
    //        {
    //            Rect3D subBounds = PlanItemHelper.GetWorldSpaceBounds(sub);

    //            if (!newSubParts.Contains(sub))
    //            {
    //                subBoundsDict.Add(sub, subBounds);
    //            }
    //            else
    //            {
    //                newSubBoundsDict.Add(sub, subBounds);
    //            }
    //        }

    //        //check if the new part hits anything and if so move it.
    //        foreach (var entry in subBoundsDict)
    //        {
    //            foreach (var newEntry in newSubBoundsDict)
    //            {
    //                if (LocalHelper.CollidesWith(newEntry.Value, entry.Value))
    //                {
    //                    Rect3D collisionBounds = entry.Value;

    //                    switch (viewtype)
    //                    {
    //                        case CameraViewType.Front:
    //                        default:
    //                            item.Z = Convert.ToSingle(Math.Max(item.Z, collisionBounds.Z + collisionBounds.SizeZ));
    //                            break;

    //                        case CameraViewType.Back:
    //                            item.Z = Convert.ToSingle(Math.Min(item.Z, collisionBounds.Z - collisionBounds.SizeZ));
    //                            break;

    //                        case CameraViewType.Left:
    //                            item.X = Convert.ToSingle(Math.Min(item.X, collisionBounds.X - itemBounds.SizeX));
    //                            break;

    //                        case CameraViewType.Right:
    //                            item.X = Convert.ToSingle(Math.Max(item.X, collisionBounds.X + collisionBounds.SizeX));
    //                            break;

    //                        case CameraViewType.Top:
    //                            item.Y = Convert.ToSingle(Math.Max(item.Y, collisionBounds.Y + collisionBounds.SizeY));
    //                            break;

    //                        case CameraViewType.Bottom:
    //                            item.Y = Convert.ToSingle(Math.Min(item.Y, collisionBounds.Y - itemBounds.SizeY));
    //                            break;
    //                    }

    //                    itemBounds.X = item.X;
    //                    itemBounds.Y = item.Y;
    //                    itemBounds.Z = item.Z;
    //                }
    //            }
    //        }
    //    }
    //}

    #endregion

    #endregion
}