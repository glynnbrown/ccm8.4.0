﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
// V8-24265 : J.Pickup
//  LabelSettingsView is now LabelItem
// CCM-24265 : N.Haywood
//  Added custom labels
// V8-25436 : L.Luong
//  Added custom highlight
// V8-27825 : L.Ineson
//  Added document freezing
// V8-28345 : L.Ineson
//  Added NotifyHighlightResultChanged
#endregion
#region Version History: (CCM 8.30)
// V8-32389  : J.Pickup
//  Toggle functionality (highlight, labels).
// V8-32636 : A.Probyn
//  Added ShowAnnotations
#endregion
#endregion

using System;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Framework.Collections;
using System.Windows.Input;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    public interface IPlanDocument : IDisposable
    {
        /// <summary>
        /// Returns the type of this document.
        /// </summary>
        DocumentType DocumentType { get; }

        /// <summary>
        /// Gets/Sets whether this is the active document
        /// for the entire app.
        /// Used for styling purposes.
        /// </summary>
        Boolean IsActiveDocument { get; set; }

        /// <summary>
        /// Gets the document title.
        /// </summary>
        String Title { get; }

        /// <summary>
        /// Gets the document control view.
        /// </summary>
        IPlanDocumentView AttachedDocumentView { get; }

        /// <summary>
        /// The planogram model source
        /// </summary>
        PlanogramView Planogram { get; }

        /// <summary>
        /// Returns the view of selected items.
        /// </summary>
        PlanItemSelection SelectedPlanItems { get; }

        #region Toggle Placeholders

        /// <summary>
        /// Holds the temporary placeholder Product Label Item that is assigned but toggled on/off.
        /// </summary>
        LabelItem ToggleProductLabelItem { get; set; }

        /// <summary>
        /// Holds the temporary placeholder Product Fixture Label Item that is assigned but toggled on/off.
        /// </summary>
        LabelItem ToggleFixtureLabelItem { get; set; }

        /// <summary>
        /// Holds the temporary placeholder Product Label Item that is assigned but toggled on/off.
        /// </summary>
        HighlightItem ToggleHighlightItem { get; set; }

        #endregion

        #region Planogram View Settings

        /// <summary>
        /// Returns true if plan view settings are supported by this document.
        /// </summary>
        Boolean ArePlanogramViewSettingsSupported { get; }

        /// <summary>
        /// Gets/Sets whether position models should be shown.
        /// </summary>
        Boolean ShowPositions { get; set; }

        /// <summary>
        /// Gets/Sets whether position units should be shown
        /// </summary>
        Boolean ShowPositionUnits { get; set; }

        /// <summary>
        /// Gets/Sets whether images should be shown.
        /// </summary>
        Boolean ShowProductImages { get; set; }

        /// <summary>
        /// Gets/Sets whether shapes should be shown.
        /// </summary>
        Boolean ShowProductShapes { get; set; }

        /// <summary>
        /// Gest/Sets whether fill patterns should be shown.
        /// </summary>
        Boolean ShowProductFillPatterns { get; set; }

        /// <summary>
        /// Gets/Sets the product label setting to apply.
        /// </summary>
        LabelItem ProductLabel { get; set; }

        /// <summary>
        /// Gets/Sets whether fixture images should be shown.
        /// </summary>
        Boolean ShowFixtureImages { get; set; }

        /// <summary>
        /// Gets/Sets whether fixture fill patterns should be shown.
        /// </summary>
        Boolean ShowFixtureFillPatterns { get; set; }

        /// <summary>
        /// Gets/Sets the fixture label setting to apply.
        /// </summary>
        LabelItem FixtureLabel { get; set; }

        /// <summary>
        /// Gets/Sets whether chest walls should be shown.
        /// </summary>
        Boolean ShowChestWalls { get; set; }

        /// <summary>
        /// Gets/Sets whether chest components should be rotated.
        /// </summary>
        Boolean RotateTopDownComponents { get; set; }

        /// <summary>
        /// Gets/Sets whether shelf risers should be shown
        /// </summary>
        Boolean ShowShelfRisers { get; set; }

        /// <summary>
        /// Gets/Sets whether pegholes should be shown
        /// </summary>
        Boolean ShowPegHoles { get; set; }

        /// <summary>
        /// Gets/Sets whether pegs should be shown.
        /// </summary>
        Boolean ShowPegs { get; set; }

        /// <summary>
        /// Gets/Sets whether notches should be drawn.
        /// </summary>
        Boolean ShowNotches { get; set; }

        /// <summary>
        /// Gets/Sets whether slot lines should be shown
        /// </summary>
        Boolean ShowDividerLines { get; set; }

        /// <summary>
        /// Gets/Sets whether dividers should be shown.
        /// </summary>
        Boolean ShowDividers { get; set; }

        /// <summary>
        /// Gets/Sets whether gridlines should be shown.
        /// </summary>
        Boolean ShowGridlines { get; set; }

        /// <summary>
        /// Gets/Sets whether the model should be shown as a wireframe.
        /// </summary>
        Boolean ShowWireframeOnly { get; set; }

        /// <summary>
        /// Gets/Sets whether intensive updates should be supressed.
        /// </summary>
        Boolean SuppressPerformanceIntensiveUpdates { get; set; }

        /// <summary>
        /// Gets/Sets the custom product label
        /// </summary>
        LabelItem CustomProductLabel { get; set; }

        /// <summary>
        /// Gets/Sets whether the custom product label is selected
        /// </summary>
        Boolean IsCustomProductLabelSelected { get; set; }

        /// <summary>
        /// Gets/Sets the custom fixture label
        /// </summary>
        LabelItem CustomFixtureLabel { get; set; }

        /// <summary>
        /// Gets/Sets whether the custom fixture label is selected
        /// </summary>
        Boolean IsCustomFixtureLabelSelected { get; set; }

        /// <summary>
        /// Gets/Sets whether the annotations are shown
        /// </summary>
        Boolean ShowAnnotations { get; set; }

        #endregion

        #region Zooming / Navigation

        /// <summary>
        /// Returns true if this document supports the zooming.
        /// </summary>
        Boolean IsZoomingSupported { get; }
        
        /// <summary>
        /// Toggles panning mode.
        /// </summary>
        Boolean IsInPanningMode { get; set; }

        /// <summary>
        /// Toogles zoom to area mode.
        /// </summary>
        Boolean IsInZoomToAreaMode { get; set; }
        
        void ZoomToFit();

        void ZoomToFitHeight();

        void ZoomToFitWidth();
        
        void ZoomIn();
        
        void ZoomOut();

        void ZoomSelectedPlanItems();

        #endregion

        #region Highlighting

        /// <summary>
        /// Returns true if this document supports highlighting
        /// </summary>
        Boolean IsHighlightingSupported { get; }

        /// <summary>
        /// Gets/Sets the current highlight.
        /// </summary>
        HighlightItem Highlight { get; set; }

        /// <summary>
        /// Returns the collection of legend items processed for the current highlight.
        /// </summary>
        ReadOnlyBulkObservableCollection<PlanogramHighlightResultGroup> HighlightLegendItems { get; }

        /// <summary>
        /// Gets/Sets whether the highlight legend should be visible.
        /// </summary>
        Boolean IsHighlightLegendVisible { get; set; }

        /// <summary>
        /// Gets/Sets whether the custom highlight is selected
        /// </summary>
        Boolean IsCustomHighlightSelected { get; set; }

        /// <summary>
        /// Gets/Sets the custom highlight
        /// </summary>
        HighlightItem CustomHighlight { get; set; }

        #endregion

        /// <summary>
        /// Loads settings from the given settings object
        /// </summary>
        /// <param name="settings"></param>
        void LoadSettings(UserEditorSettings settings);

        /// <summary>
        /// Loads the settings from another plan document.
        /// </summary>
        /// <param name="sourceDoc"></param>
        void LoadSettings(IPlanDocument sourceDoc);

        /// <summary>
        /// Carries out any document specific keyboard commands.
        /// </summary>
        /// <param name="modifiers"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        Boolean ProcessKeyboardShortcut(ModifierKeys modifiers, Key key);

        #region Freeze

        void Freeze();

        void Unfreeze();

        #endregion

        /// <summary>
        /// Notifies this document that plan processing is completing.
        /// </summary>
        void OnPlanProcessCompleting();
    }

    /// <summary>
    /// Interface to specific the control as a document view
    /// </summary>
    public interface IPlanDocumentView
    {
        /// <summary>
        /// Returns the plan document viewmodel
        /// </summary>
        /// <returns></returns>
        IPlanDocument GetIPlanDocument();

        void Dispose();
    }

}
