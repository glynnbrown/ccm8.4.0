﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.3)
// V8-31541 : L.Ineson
//  Created
//  V8-31847 : L.Ineson
//      Paths with no fields now get passed to evaluator
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;


namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Represents a single column on the datasheet.
    /// </summary>
    public sealed class DataSheetColumn
    {
        public DataSheetPlanogram Plan { get; set; }
        public CustomColumn Model { get; private set; }
        public Int32 ColumnIdx { get; private set; }
        public List<ObjectFieldInfo> Fields { get; private set; }
        public Type PropertyType { get; private set; }
        public ModelPropertyDisplayType DisplayType { get; private set; }
        public String RowBindingPath { get; private set; }
        public Object FilterValue { get; set; }
        public Boolean IsKeyColumn { get; set; }
        public Boolean IsReadOnly { get; private set; }

        public DataSheetColumn(Int32 colIdx, CustomColumn model, List<ObjectFieldInfo> pathFields,
            Type rowItemType)
        {
            this.Model = model;
            this.Fields = pathFields;
            this.ColumnIdx = colIdx;

            this.RowBindingPath = "[" + colIdx + "].Value";

            //single field and exact match:
            if (pathFields.Count == 1
                && model.Path == pathFields[0].FieldPlaceholder)
            {
                this.PropertyType = pathFields[0].PropertyType;
                this.DisplayType = pathFields[0].PropertyDisplayType;
                this.IsReadOnly = WpfHelper.IsReadOnly(rowItemType, pathFields[0].PropertyName);
            }

            //multiple fields or no fields:
            else
            {
                this.PropertyType = ObjectFieldExpression.GetExpectedValueType(model.Path, pathFields);
                this.DisplayType = ModelPropertyDisplayType.None;
                this.IsReadOnly = true;
            }

        }
    }
}
