﻿#region Header Information

// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0.0)

// V8-27938 : N.Haywood
//  Created
// V8-28040 : A.Probyn
//  ~ Added code to call ApplyFiltersIntoGrid after column set is applied to the grid.
// V8-28594 : A.Silva
//      Amended getter for ColumnLayout Property, so that any changes to the data grid are updated before returning the current Column Layout.

#endregion

#region Version History: (CCM 8.0.1)

// V8-28670 : M.Shelley
//  Modified the code in the OnLoaded method to retrieve the currency UoM from the plan, rather than the Entity
#endregion

#region Version History: (CCM 8.2.0)
//V8-30958 : L.Ineson
//  Refactored column layout manager.
#endregion
#region Version History: (CCM 8.3.0)
//V8-31542 : L.Ineson
//  Removed column manager as this is now different.
//V8-32083 : L.Ineson
//  Forced grid to reapply filters after loading.
#endregion
#endregion

using System;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    ///     Interaction logic for ReportDocumentView.xaml
    /// </summary>
    public sealed partial class ReportDocumentView : IPlanDocumentView
    {
        #region Properties

        #region ViewModel Property

        /// <summary>
        /// ViewModel dependency property definition
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ReportDocument),
                typeof(ReportDocumentView), new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        ///     Handles the change of viemodels for the view.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ReportDocumentView senderControl = (ReportDocumentView)sender;

            if (e.OldValue != null)
            {
                ReportDocument oldModel = (ReportDocument)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.LayoutChanging -= senderControl.ViewModel_LayoutChanging;
                oldModel.LayoutChanged -= senderControl.ViewModel_LayoutChanged;
            }

            if (e.NewValue != null)
            {
                ReportDocument newModel = (ReportDocument)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.LayoutChanging += senderControl.ViewModel_LayoutChanging;
                newModel.LayoutChanged += senderControl.ViewModel_LayoutChanged;
            }
        }

        /// <summary>
        ///     Gets the viewmodel controller for this view.
        /// </summary>
        public ReportDocument ViewModel
        {
            get { return (ReportDocument)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region IsLoading Property

        public static readonly DependencyProperty IsLoadingProperty =
            DependencyProperty.Register("IsLoading", typeof(Boolean), typeof(ReportDocumentView));

        public Boolean IsLoading
        {
            get{return (Boolean)GetValue(IsLoadingProperty);}
            private set { SetValue(IsLoadingProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="planDocument"></param>
        public ReportDocumentView(ReportDocument planDocument)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = planDocument;

            this.DataSheetGrid.DragScope = Application.Current.MainWindow.Content as FrameworkElement;
            this.IsLoading = true;
            this.Loaded += ReportDocumentView_Loaded;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out initial actions on load.
        /// </summary>
        private void ReportDocumentView_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ReportDocumentView_Loaded;

            UpdateColumns();
            
            Dispatcher.BeginInvoke(
                (Action)(() =>
                    {
                        Mouse.OverrideCursor = null;
                        
                    }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        /// <summary>
        /// Called whenever the viewmodel layout has changed
        /// </summary>
        private void ViewModel_LayoutChanging(object sender, EventArgs e)
        {
            ClearGrid();
        }

        /// <summary>
        /// Called whenever the viewmodel layout has changed
        /// </summary>
        private void ViewModel_LayoutChanged(object sender, EventArgs e)
        {
            UpdateColumns();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the datagrid with the new layout.
        /// </summary>
        private void UpdateColumns()
        {
            if (!this.IsLoaded) return;

            ExtendedDataGrid grid = this.DataSheetGrid;
            if (grid == null || this.ViewModel == null) return;

            //change the cursor to a wait
            Boolean cursorSet = false;
            if (Mouse.OverrideCursor == null)
            {
                Mouse.OverrideCursor = Cursors.Wait;
                cursorSet = true;
            }

            ClearGrid();
           


            //get the layout
            CustomColumnLayout customColumnLayout = this.ViewModel.Layout;
            if (this.ViewModel.Layout == null) 
            {
                if(cursorSet)Mouse.OverrideCursor = null;
                return;
            }


            grid.DisplayColumnTotals = customColumnLayout.HasTotals;


            //Columns
            DataGridColumnCollection columnSet = this.ViewModel.CreateColumnSet();
            grid.ColumnSet = columnSet;

            //Nb -filters are already applied.

            // Group descriptions.
            this.ViewModel.CreateGroupDescriptions().ForEach(g => grid.GroupByDescriptions.Add(g));

            // Sort Descriptions
            this.ViewModel.CreateSortDescriptions().ForEach(sort => grid.SortByDescriptions.Add(sort));


            //reapply row and selected rows bindings
            BindingOperations.SetBinding(grid, 
                ExtendedDataGrid.ItemsSourceExtendedProperty,
                new Binding(ReportDocument.RowsProperty.Path));

            BindingOperations.SetBinding(grid, 
                SelectedItemsBehaviour.SynchroniseSelectedItemsProperty,
                new Binding(ReportDocument.SelectedRowsProperty.Path));

            //apply other settings.
            grid.FrozenColumnCount = customColumnLayout.FrozenColumnCount;

            //only allow the user to change groupings if we are not aggregated.
            grid.CanUserGroupBy = !(customColumnLayout.IsGroupDetailHidden && customColumnLayout.GroupList.Any());


            //force the grid to refilter if we have any applied
            if(customColumnLayout.FilterList.Any()) grid.BeginFilter();

            //cancel wait cursor.
            if (cursorSet)
            {
                try { Mouse.OverrideCursor = null; }
                catch { }
            }
        }

        /// <summary>
        /// Clears all layout settings from the
        /// atatched datagrid.
        /// </summary>
        private void ClearGrid()
        {
            ExtendedDataGrid grid = this.DataSheetGrid;
            if (grid == null) return;

            //clear the grid completely
            grid.DisplayColumnTotals = false;
            grid.ClearFilterValues();

            //remove groups first otherwise if we have any virtualised columns
            //the grid could error in an unrecoverable way.
            if (grid.GroupByDescriptions.Any()) grid.GroupByDescriptions.Clear();
            if (grid.SortByDescriptions.Any()) grid.SortByDescriptions.Clear();


            //clear existing columns and bindings
            BindingOperations.ClearBinding(grid, ExtendedDataGrid.ItemsSourceExtendedProperty);
            BindingOperations.ClearBinding(grid, SelectedItemsBehaviour.SynchroniseSelectedItemsProperty);
            if (grid.Columns.Any()) grid.Columns.Clear();
        }

        #endregion

        #region IPlanDocumentView Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean isDisposing)
        {
            if (_isDisposed) return;

            _isDisposed = true;

            this.ViewModel = null;
        }


        public IPlanDocument GetIPlanDocument()
        {
            return ViewModel;
        }

        #endregion
    }
}