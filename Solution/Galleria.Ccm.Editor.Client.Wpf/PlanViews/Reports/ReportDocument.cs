﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27938 : N.Haywood
//  Created
// V8-28191 : N.Haywood
//  Added planController_SelectedPlanDocumentChanged
// V8-28291 : N.Haywood
//  Changed the title to be the name of the column layout created
#endregion

#region Version History: (CCM 810)
// V8-28826 : M.Brumby
//  fetchType is now product if the columns only contain product columns
#endregion

#region Version History: (CCM 830)
// V8-31541 : L.Ineson
//  PCR01412 grouping and calculated columns changes.
// V8-32104 : L.Ineson
//  Stopped custom data sheet name from being overriden.
// V8-32192 : L.Ineson
//  Key columns are no longer placed at the beginning if there is only one planogram.
// V8-32135 : L.Ineson
//  Required updated will now only be flagged if the document is not actually in view.
// V8-32424 : L.Ineson
//  Percentage properties are now forced to display 2dp.
#endregion

#endregion

using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Editor.Wpf.ReportColumnLayouts;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;


namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    ///     Plan document controller for providing a view of products on a planogram
    /// </summary>
    public sealed class ReportDocument : PlanDocument<ReportDocumentView>
    {
        #region Constants

        ///// <summary>
        /////     Type of column layout used by the <see cref="ColumnLayoutManager" /> in this instance.
        ///// </summary>
        //public const CustomColumnLayoutType ColumnLayoutType = CustomColumnLayoutType.DataSheet;

        ///// <summary>
        /////     Name of the column layout preferred by the user to be used by the <see cref="ColumnLayoutManager" /> in this
        /////     instance.
        ///// </summary>
        //public const String ScreenKey = "ReportDocument";

        #endregion

        #region Fields

        private CustomColumnLayout _layout;
        private ReadOnlyCollection<DataSheetPlanogram> _selectedPlanograms;

        private List<DataSheetColumn> _columnDetails;
        private PlanItemType _dataLevel;

        private readonly BulkObservableCollection<DataSheetRow> _rows = new BulkObservableCollection<DataSheetRow>();
        private readonly ObservableCollection<DataSheetRow> _selectedRows = new ObservableCollection<DataSheetRow>();

        private Boolean _isRowRefreshRequired;
        private Boolean _isColumnRefreshRequired;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath RowsProperty = WpfHelper.GetPropertyPath<ReportDocument>(p => p.Rows);
        public static readonly PropertyPath SelectedRowsProperty = WpfHelper.GetPropertyPath<ReportDocument>(p => p.SelectedRows);
        public static readonly PropertyPath LayoutProperty = WpfHelper.GetPropertyPath<ReportDocument>(p => p.Layout);
        #endregion

        #region Properties

        public override DocumentType DocumentType
        {
            get { return DocumentType.DataSheet; }
        }

        /// <summary>
        ///     Returns the readonly collection of rows to display
        /// </summary>
        public ReadOnlyBulkObservableCollection<DataSheetRow> Rows
        {
            get;
            private set;
        }

        /// <summary>
        ///     Returns the editable collection of selected items.
        /// </summary>
        /// <remarks>This is essentially a filtered version of the selected plan items collection.</remarks>
        public ObservableCollection<DataSheetRow> SelectedRows
        {
            get { return _selectedRows; }
        }

        /// <summary>
        ///     Returns the document title.
        /// </summary>
        public override String Title
        {
            get
            {
                return (_layout != null) ? _layout.Name : Message.Generic_New;
            }
        }

        /// <summary>
        /// Gets/Sets the current layout.
        /// </summary>
        public CustomColumnLayout Layout
        {
            get { return _layout; }
            private set
            {
                _layout = value;
                OnPropertyChanged(LayoutProperty);
                OnLayoutChanged(value);
            }
        }

        /// <summary>
        /// Returns the collection of plans that have been included,
        /// mainly for unit test checks.
        /// </summary>
        public ReadOnlyCollection<DataSheetPlanogram> IncludedPlans
        {
            get { return _selectedPlanograms; }
            private set
            {
                if (_selectedPlanograms != value)
                {
                    var oldValue = _selectedPlanograms;
                    _selectedPlanograms = value;
                    OnIncludedPlanogramsChanged(oldValue, value);
                }
            }
        }

        #endregion

        #region Events

        #region LayoutChanging
        /// <summary>
        /// Notifies the control that the layout is about to change
        /// so the grid should be cleared.
        /// </summary>
        public event EventHandler LayoutChanging;

        /// <summary>
        /// Raised the LayoutChanging event.
        /// </summary>
        private void RaiseLayoutChanging()
        {
            if (LayoutChanging != null)
                LayoutChanging(this, EventArgs.Empty);
        }
        #endregion

        #region LayoutChanged
        /// <summary>
        /// Notifies the control that the layout grid needs to be updated.
        /// </summary>
        public event EventHandler LayoutChanged;

        /// <summary>
        /// Raised the LayoutChanged event.
        /// </summary>
        private void RaiseLayoutChanged()
        {
            if (LayoutChanged != null)
                LayoutChanged(this, EventArgs.Empty);
        }
        #endregion

        #endregion

        #region Constructor

        /// <summary>
        ///     Creates a new instance of this type.
        /// </summary>
        public ReportDocument(PlanControllerViewModel parentController)
            : base(parentController)
        {
            this.Rows = new ReadOnlyBulkObservableCollection<DataSheetRow>(_rows);

            this.IncludedPlans = new List<DataSheetPlanogram>{
                new DataSheetPlanogram(this.ParentController, this.ParentController.SourcePlanogram.Name)}
                .AsReadOnly();

            //attach events
            SetPlanEventHandlers(true);

            App.MainPageViewModel.PlanControllers.BulkCollectionChanged += OpenPlanControllers_BulkCollectionChanged;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the current layout changes
        /// </summary>
        /// <param name="newLayout"></param>
        private void OnLayoutChanged(CustomColumnLayout newLayout)
        {
            OnPropertyChanged(TitleProperty);
            OnRefreshRowsAndColumnsRequired();
        }

        /// <summary>
        /// Called whenever the planogram selection changes.
        /// </summary>
        private void OnIncludedPlanogramsChanged(IEnumerable<DataSheetPlanogram> oldValue, IEnumerable<DataSheetPlanogram> newValue)
        {
            if (oldValue != null)
            {
                foreach (DataSheetPlanogram plan in oldValue)
                {
                    plan.Controller.SourcePlanogram.PlanProcessCompleting -= IncludedPlanogram_PlanProcessCompleting;
                }
            }

            if (newValue != null)
            {
                foreach (DataSheetPlanogram plan in newValue)
                {
                    plan.Controller.SourcePlanogram.PlanProcessCompleting += IncludedPlanogram_PlanProcessCompleting;
                }
            }
        }

        /// <summary>
        /// Causes rows to update whenever included planograms change.
        /// </summary>
        private void IncludedPlanogram_PlanProcessCompleting(object sender, EventArgs e)
        {
            OnRefreshRowsRequired();
        }

        /// <summary>
        /// Called whenever the open plan controllers collection changes.
        /// </summary>
        private void OpenPlanControllers_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            if (this.Layout == null) return;

            //validate the plan selection

            if (this.Layout.IncludeAllPlanograms)
            {
                //if we are including all plans then just confirm that we are still doing so:
                List<DataSheetPlanogram> selectedPlans = this.IncludedPlans.ToList();

                foreach (DataSheetPlanogram plan in selectedPlans.ToList())
                {
                    if (plan.Controller == App.MainPageViewModel.ActivePlanController) continue;

                    if (!App.MainPageViewModel.PlanControllers.Contains(plan.Controller))
                    {
                        selectedPlans.Remove(plan);
                    }
                }
                foreach (PlanControllerViewModel controller in App.MainPageViewModel.PlanControllers)
                {
                    if (!selectedPlans.Any(p => p.Controller == controller))
                    {
                        selectedPlans.Add(new DataSheetPlanogram(controller));
                    }
                }

                //order the list
                selectedPlans = selectedPlans.OrderBy(s => s.Controller.SourcePlanogram.Name).ToList();

                //place the active one at the start
                DataSheetPlanogram activePlan = selectedPlans.FirstOrDefault(p => p.Controller == this.ParentController);
                if (activePlan == null) return;
                selectedPlans.Remove(activePlan);
                selectedPlans.Insert(0, activePlan);

                this.IncludedPlans = selectedPlans.AsReadOnly();

                OnRefreshRowsAndColumnsRequired();
            }
            else if (this.IncludedPlans.Count > 1)
            {
                //if we are only including a few plans then check they are all still valid.
                List<DataSheetPlanogram> oldPlans =
                    this.IncludedPlans.Where(i => !App.MainPageViewModel.PlanControllers.Contains(i.Controller)).ToList();
                if (!oldPlans.Any()) return;

                this.IncludedPlans = this.IncludedPlans.Except(oldPlans).ToList().AsReadOnly();

                OnRefreshRowsAndColumnsRequired();
            }
        }

        /// <summary>
        /// Called whenever the visibility of this document changes.
        /// </summary>
        protected override void OnIsVisibleChanged()
        {
            base.OnIsVisibleChanged();

            if (this.IsVisible)
            {
                if (_isColumnRefreshRequired) 
                {
                    OnRefreshRowsAndColumnsRequired();
                }
                else if (_isRowRefreshRequired)
                {
                    OnRefreshRowsRequired();
                }
            }
        }

        #endregion

        #region Methods

        #region Document control

        /// <summary>
        /// Subscribes and unsubscribes event handlers
        /// </summary>
        /// <param name="isAttach"></param>
        private void SetPlanEventHandlers(Boolean isAttach)
        {
            if (isAttach)
            {


            }
            else
            {

            }
        }

        /// <summary>
        /// Called whenever this document should
        /// stop updating
        /// </summary>
        protected override void OnFreeze()
        {
            SetPlanEventHandlers(false);
        }

        /// <summary>
        /// Called whenever this document should
        /// resume updating.
        /// </summary>
        protected override void OnUnfreeze()
        {
            SetPlanEventHandlers(true);

            //update data
            //LoadPlanogramRows();
        }

        #endregion

        #region Rows

        /// <summary>
        /// If the document is visible, performs a row refresh
        /// Otherwise flags if a row refresh is required.
        /// </summary>
        private void OnRefreshRowsRequired()
        {
            if (this.IsVisible)
            {
                RefreshRows();
            }
            else
            {
                _isRowRefreshRequired = true;
            }
        }

        /// <summary>
        /// Refreshes the rows collection.
        /// </summary>
        private void RefreshRows()
        {
            _isRowRefreshRequired = false;

            CustomColumnLayout currentLayout = this.Layout;

            //just clear and return out if we have no layout.
            if (this.Layout == null || !_columnDetails.Any())
            {
                if(_rows.Any()) _rows.Clear();
                return;
            }

            List<DataSheetColumn> columnDetails = _columnDetails;
            PlanItemType fetchType = _dataLevel;
            Int32 cellCount = currentLayout.Columns.Count * this.IncludedPlans.Count;

            //create the new rows.
            if (this.IncludedPlans.Count > 1
               || (currentLayout.IsGroupDetailHidden && currentLayout.GroupList.Any()))
            {
                #region Multiple plans or aggregated:
                Dictionary<Object, DataSheetRow> _rowsByKey = new Dictionary<Object, DataSheetRow>();
                foreach (DataSheetPlanogram planEntry in this.IncludedPlans)
                {
                    List<IPlanItem> planItems = PlanItemHelper.GetPlanItems(planEntry.Controller.SourcePlanogram, fetchType);

                    //Group plan items into rows.
                    IEnumerable<IGrouping<Object, IPlanItem>> itemGroups;
                    if (currentLayout.IsGroupDetailHidden && currentLayout.GroupList.Any())
                    {
                        //use the groupings to find the key value.
                        itemGroups = planItems.GroupBy(i => GetKeyValue(i, currentLayout.GroupList));
                    }
                    else
                    {
                        //use the item type key
                        itemGroups = planItems.GroupBy(i => GetKeyValue(i, fetchType));
                    }


                    //Create or Update rows.
                    foreach (var itemGroup in itemGroups)
                    {
                        List<IPlanItem> sourceItems = itemGroup.ToList();
                        Object key = itemGroup.Key;

                        //Check for existing row or create a new one.
                        DataSheetRow row;
                        if (!_rowsByKey.TryGetValue(key, out row))
                        {
                            row = new DataSheetRow(cellCount);
                            _rowsByKey[key] = row;
                        }

                        //cycle through columns setting cells
                        foreach (CustomColumn col in currentLayout.Columns)
                        {
                            DataSheetColumn colDef = columnDetails.FirstOrDefault(c => c.Model == col && c.Plan == planEntry);
                            if (colDef == null) colDef = columnDetails.FirstOrDefault(c => c.Model == col);
                            if (colDef == null) continue;
                            row[colDef.ColumnIdx] = new DataSheetCell(colDef.Model.Path, colDef.PropertyType, sourceItems, colDef.IsReadOnly);
                        }
                    }

                }

                //clear any existing rows and load the new ones.
                if (_rows.Any()) _rows.Clear();
                _rows.AddRange(_rowsByKey.Select(r => r.Value));
                
                #endregion
            }
            
            else
            {
                #region Normal
                DataSheetPlanogram sourcePlan = this.IncludedPlans.First();
                List<IPlanItem> planItems = PlanItemHelper.GetPlanItems(sourcePlan.Controller.SourcePlanogram, fetchType);

                List<DataSheetRow> keepRows = new List<DataSheetRow>();
                foreach(DataSheetRow row in _rows.ToList())
                {
                    if (row[0].Source.Count == 1 && planItems.Contains(row[0].Source[0]))
                    {
                        keepRows.Add(row);
                        row.RefreshAllCellsAsync();
                        planItems.Remove(row[0].Source[0]);
                    }
                }

                List<DataSheetRow> newRows = new List<DataSheetRow>();
                foreach (IPlanItem sourceItem in planItems)
                {
                    DataSheetRow row = new DataSheetRow(cellCount);
                    foreach (CustomColumn col in currentLayout.Columns)
                    {
                        DataSheetColumn colDef = columnDetails.FirstOrDefault(c => c.Model == col);
                        if (colDef == null) continue;
                        row[colDef.ColumnIdx] = new DataSheetCell(colDef.Model.Path, colDef.PropertyType, sourceItem, colDef.IsReadOnly);
                    }
                    newRows.Add(row);
                }

                //remove old rows & add new ones
                _rows.RemoveRange(_rows.Where(r => !keepRows.Contains(r)).ToList());
                _rows.AddRange(newRows);



                ////just add rows
                //List<DataSheetRow> rows = new List<DataSheetRow>();
                //foreach (IPlanItem sourceItem in planItems)
                //{
                //    DataSheetRow row = new DataSheetRow(cellCount);
                //    foreach (CustomColumn col in currentLayout.Columns)
                //    {
                //        DataSheetColumn colDef = columnDetails.FirstOrDefault(c => c.Model == col);
                //        if (colDef == null) continue;
                //        row[colDef.ColumnIdx] = new DataSheetCell(colDef.Model.Path, colDef.PropertyType, sourceItem, colDef.IsReadOnly);
                //    }
                //    rows.Add(row);
                //}

                ////clear any existing rows and load the new ones.
                //if (_rows.Any()) _rows.Clear();
                //_rows.AddRange(rows);
                
                #endregion
            }

        }

        /// <summary>
        /// Returns the key for the given item based on the load level type.
        /// </summary>
        private static Object GetKeyValue(IPlanItem item, PlanItemType levelType)
        {
            switch (levelType)
            {
                case PlanItemType.Planogram:
                    return item.Planogram.Model.Id;

                case PlanItemType.Fixture:
                    return item.Planogram.Fixtures.OrderBy(f => f.X)
                        .TakeWhile(f => f != item.Fixture).Count() + 1;

                case PlanItemType.Assembly:
                    return item.Assembly.Name;

                case PlanItemType.Component:
                    {
                        Int32 bayNo = item.Planogram.Fixtures.OrderBy(f => f.X)
                        .TakeWhile(f => f != item.Fixture).Count() + 1;

                        Int32 componentNo =
                            item.Fixture.Components.Union(item.Fixture.Assemblies.SelectMany(a => a.Components))
                            .OrderBy(c => c.MetaWorldY)
                            .TakeWhile(c => c != item.Component).Count() + 1;

                        return String.Format(CultureInfo.CurrentCulture, "{0} {1}", bayNo, componentNo);
                    }


                case PlanItemType.Annotation:
                    return item.Annotation.Model.Id;

                case PlanItemType.Position:
                    if (item.Product.MetaPositionCount == 1) return item.Product.Gtin;
                    else return item.Position.Model.Id;

                case PlanItemType.Product:
                    return item.Product.Gtin;

                case PlanItemType.SubComponent:
                    {
                        Int32 bayNo = item.Planogram.Fixtures.OrderBy(f => f.X)
                        .TakeWhile(f => f != item.Fixture).Count() + 1;

                        Int32 componentNo =
                            item.Fixture.Components.Union(item.Fixture.Assemblies.SelectMany(a => a.Components))
                            .OrderBy(c => c.MetaWorldY)
                            .TakeWhile(c => c != item.Component).Count() + 1;

                        Int32 subNo =
                            item.Component.SubComponents.TakeWhile(c => c != item.SubComponent).Count() + 1;

                        return String.Format(CultureInfo.CurrentCulture, "{0} {1} {2}", bayNo, componentNo, subNo);
                    }

                default: return 1;
            }
        }

        /// <summary>
        /// Gets the key for the item based on the aggregated groupings.
        /// </summary>
        private static Object GetKeyValue(IPlanItem item, CustomColumnGroupList groupList)
        {
            List<String> resolvedValues = new List<String>(groupList.Count);

            foreach (CustomColumnGroup group in groupList)
            {
                resolvedValues.Add(Convert.ToString(DataSheetCell.ResolveItemValue(item, group.Grouping), CultureInfo.CurrentCulture));
            }

            return String.Join(",", resolvedValues);
        }

        /// <summary>
        /// Returns the key field for the given level type.
        /// </summary>
        /// <param name="levelType"></param>
        /// <returns></returns>
        internal static String GetKeyField(PlanItemType levelType)
        {
            switch (levelType)
            {
                default:
                case PlanItemType.Planogram:
                    return "[Planogram.Id]";

                case PlanItemType.Fixture:
                    return "[PlanogramFixture.BaySequenceNumber]";

                case PlanItemType.Assembly:
                    return "[PlanogramAssembly.Name]";

                case PlanItemType.Component:
                    return "[PlanogramFixture.BaySequenceNumber] [PlanogramComponent.ComponentSequenceNumber]";

                case PlanItemType.Position:
                case PlanItemType.Product:
                    return "[PlanogramProduct.Gtin]";

                case PlanItemType.SubComponent:
                    return "[PlanogramSubComponent.Id]";
            }
        }

        #endregion

        #region Columns

        /// <summary>
        /// Returns the ordered list of infos for columns to be created
        /// </summary>
        internal List<DataSheetColumn> GetOrderedColumnDetails()
        {
            if (this.Layout == null || !this.Layout.Columns.Any())
                return new List<DataSheetColumn>();

            CustomColumnLayout curLayout = this.Layout;

            //get the fields for each column
            List<ObjectFieldInfo> dataLevelCheckFields = new List<ObjectFieldInfo>();
            Dictionary<Int32, List<ObjectFieldInfo>> colIdToFields = new Dictionary<Int32, List<ObjectFieldInfo>>(curLayout.Columns.Count);
            foreach (CustomColumn col in curLayout.Columns)
            {
                var fields = ObjectFieldInfo.ExtractFieldsFromText(col.Path, PlanogramFieldHelper.EnumerateAllFields());
                colIdToFields[col.Id] = fields;

                //if the column is not an aggregated one, then add the fields to the datalevel check.
                if (!ObjectFieldExpression.IsAggregatedOutput(col.Path))
                {
                    dataLevelCheckFields.AddRange(fields);
                }
            }


            //determine to what level data should be displayed.
            _dataLevel = PlanItemHelper.ToPlanItemType(PlanogramFieldHelper.GetFieldDataLevel(dataLevelCheckFields));

            //determine which column paths should be keys
            List<String> keyColumnPaths =
                /*if hasAggregatedRows*/(curLayout.IsGroupDetailHidden && curLayout.GroupList.Any()) ?
                /*use groups as keys*/curLayout.GroupList.Select(g => g.Grouping).ToList()
                /*else use item key field*/: new List<String> { GetKeyField(_dataLevel) };


            //Determine what planograms the layout should be created for.
            IEnumerable<DataSheetPlanogram> plans = this.IncludedPlans;

            List<DataSheetColumn> colList = new List<DataSheetColumn>(curLayout.Columns.Count * plans.Count());
            Int32 colIdx = 0;

            if (this.Layout.DataOrderType == DataSheetDataOrderType.ByPlanogram)
            {
                foreach (DataSheetPlanogram planEntry in plans)
                {
                    foreach (CustomColumn col in curLayout.Columns.OrderBy(c=> c.Number))
                    {
                        Boolean isKey = keyColumnPaths.Contains(col.Path);

                        //if a key, only add for the first plan.
                        if (isKey && planEntry != plans.First()) continue;

                        CustomColumnFilter filter = curLayout.FilterList.FirstOrDefault(f => f.Path == col.Path);

                        DataSheetColumn colDetail =
                            new DataSheetColumn(colIdx++, col, colIdToFields[col.Id], GetRowType(_dataLevel))
                        {
                            IsKeyColumn = isKey,
                            Plan = planEntry,
                            FilterValue = (filter != null) ? filter.Text : null,
                        };

                        colList.Add(colDetail);
                    }
                }
            }
            else
            {
                foreach (CustomColumn col in curLayout.Columns.OrderBy(c => c.Number))
                {
                    Boolean isKey = keyColumnPaths.Contains(col.Path);

                    foreach (var planEntry in plans)
                    {
                        //if a key, only add for the first plan.
                        if (isKey && planEntry != plans.First()) continue;

                        CustomColumnFilter filter = curLayout.FilterList.FirstOrDefault(f => f.Path == col.Path);

                        DataSheetColumn colDetail =
                            new DataSheetColumn(colIdx++, col, colIdToFields[col.Id], GetRowType(_dataLevel))
                            {
                                IsKeyColumn = isKey,
                                Plan = planEntry,
                                FilterValue = (filter != null) ? filter.Text : null,
                            };

                        colList.Add(colDetail);
                    }
                }
            }

            //Move key columns to the front if we have move than one planogram
            if (plans.Count() > 1)
            {
                keyColumnPaths.Reverse();
                foreach (String keyPath in keyColumnPaths)
                {
                    DataSheetColumn col =
                        colList.FirstOrDefault(c => c.IsKeyColumn && c.Model.Path == keyPath);
                    if (col == null) continue;

                    colList.Remove(col);
                    colList.Insert(0, col);
                }
            }


            return colList;
        }

        /// <summary>
        /// Returns the column definitions to use for the current layout
        /// </summary>
        /// <returns></returns>
        public DataGridColumnCollection CreateColumnSet()
        {
            DataGridColumnCollection columnSet = new DataGridColumnCollection();

            if (this.Layout == null || !this.Layout.Columns.Any()) return columnSet;

            foreach (DataSheetColumn col in _columnDetails)
            {
                columnSet.Add(CreateColumn(col));
            }

            return columnSet;
        }

        /// <summary>
        /// Creates a new DataGridColumn for the given info.
        /// </summary>
        private DataGridColumn CreateColumn(DataSheetColumn columnInfo)
        {
            Boolean isMultiPlan = this.IncludedPlans.Count > 1;

            Type propertyType = columnInfo.PropertyType;
            ModelPropertyDisplayType displayType = columnInfo.DisplayType;

            String propertyDisplayName = columnInfo.Model.DisplayName;

            //Extract fields from the path
            List<ObjectFieldInfo> fields = columnInfo.Fields;

            if (fields.Count == 1)
            {
                ObjectFieldInfo field = fields.First();

                if (String.IsNullOrWhiteSpace(propertyDisplayName))
                {
                    propertyDisplayName =
                        String.Format("{0}{1}{2}", field.OwnerFriendlyName, Environment.NewLine, field.PropertyFriendlyName);
                }
            }

            //create the column
            DataGridColumn column;
            if (columnInfo.IsReadOnly)
            {
                column = CommonHelper.CreateReadOnlyColumn(
                    columnInfo.RowBindingPath,
                    propertyType,
                    propertyDisplayName,
                    displayType,
                    columnInfo.Plan.Controller.SourcePlanogram.DisplayUnits);
            }
            else
            {
                column = CommonHelper.CreateEditableColumn(
                    columnInfo.RowBindingPath,
                    propertyType,
                    propertyDisplayName,
                    displayType,
                    columnInfo.Plan.Controller.SourcePlanogram.DisplayUnits);
            }

            //[32424] if the property type is percentage, then force 2dp for this view.
            if (displayType == ModelPropertyDisplayType.Percentage
                && column is DataGridBoundColumn)
            {
                Binding colBinding = ((DataGridBoundColumn)column).Binding as Binding;
                if(colBinding != null)
                {
                    DisplayUnitOfMeasure displayUom = colBinding.ConverterParameter as DisplayUnitOfMeasure;
                    if (displayUom != null)
                    {
                        displayUom.ForceDecimalDisplay = true;
                    }
                }
            }



            IGalleriaDataGridColumn galCol = column as IGalleriaDataGridColumn;
            if (galCol != null)
            {
                if (isMultiPlan && !columnInfo.IsKeyColumn)
                {
                    if (this.Layout.DataOrderType == DataSheetDataOrderType.ByPlanogram)
                    {
                        galCol.HeaderGroupNames.Add(new DataGridHeaderGroup() { HeaderName = columnInfo.Plan.Alias });
                        galCol.ColumnGroupName = columnInfo.Plan.Alias;
                    }
                    else
                    {
                        galCol.HeaderGroupNames.Add(new DataGridHeaderGroup() { HeaderName = propertyDisplayName.Replace(Environment.NewLine, " ") });
                        column.Header = columnInfo.Plan.Alias;
                        galCol.ColumnGroupName = propertyDisplayName;
                    }
                }

                galCol.SelectedTotalType = (ExtendedDataGridColumnTotalType)columnInfo.Model.TotalType;
            }

            //set the column width
            if (columnInfo.Model.Width != 0)
            {
                column.Width = columnInfo.Model.Width;
            }
            else
            {
                //set a sensible width.
                switch (CommonHelper.GetBasicPropertyType(columnInfo.PropertyType))
                {
                    case CommonHelper.PropertyType.Integer:
                    case CommonHelper.PropertyType.Boolean:
                        column.Width = 60;
                        break;

                    case CommonHelper.PropertyType.Decimal:
                        column.Width = 70;
                        break;

                    case CommonHelper.PropertyType.DateTime:
                        column.Width = 100;
                        break;
                }
            }


            //apply a filter if required.
            if (columnInfo.FilterValue != null)
            {
                ExtendedDataGrid.SetColumnFilterValue(column, columnInfo.FilterValue);
            }


            return column;
        }

        /// <summary>
        ///     Gets all the <see cref="ObjectFieldInfo"/> belonging to the layout type.
        /// </summary>
        /// <returns>An enumeration with the <see cref="ObjectFieldInfo"/> instances.</returns>
        private IEnumerable<ObjectFieldInfo> EnumerateModelObjectFields()
        {
            //Prefix all property names with the owner type name
            //and swap the group names to be the owner friendly names
            foreach (ObjectFieldInfo field in PlanogramFieldHelper.EnumerateAllFields(this.Planogram.Model))
            {
                field.GroupName = field.OwnerFriendlyName;
                field.PropertyName = field.OwnerType.Name + "." + field.PropertyName;
                yield return field;
            }
        }

        /// <summary>
        /// Gets group descriptions to use for the current layout.
        /// </summary>
        public List<PropertyGroupDescription> CreateGroupDescriptions()
        {
            List<PropertyGroupDescription> groupDescriptions = new List<PropertyGroupDescription>();
            if (this.Layout != null && !this.Layout.IsGroupDetailHidden)
            {
                foreach (CustomColumnGroup group in this.Layout.GroupList)
                {
                    DataSheetColumn colDetail = _columnDetails.FirstOrDefault(c => c.Model.Path == group.Grouping);
                    if (colDetail == null) continue;
                    groupDescriptions.Add(new PropertyGroupDescription(colDetail.RowBindingPath));
                }
            }
            return groupDescriptions;
        }

        /// <summary>
        /// Gets sort descriptions to use for the current layout
        /// </summary>
        public List<SortDescription> CreateSortDescriptions()
        {
            List<SortDescription> sortDescriptions = new List<SortDescription>();
            if (this.Layout != null)
            {
                foreach (CustomColumnSort sort in this.Layout.SortList)
                {
                    DataSheetColumn colDetail = _columnDetails.FirstOrDefault(c => c.Model.Path == sort.Path);
                    if (colDetail == null) continue;
                    sortDescriptions.Add(new SortDescription(colDetail.RowBindingPath, sort.Direction));
                }

            }
            return sortDescriptions;
        }

        /// <summary>
        /// Returns the row type for the expected item,
        /// or null if aggregated.
        /// </summary>
        /// <param name="itemType"></param>
        /// <returns></returns>
        public Type GetRowType(PlanItemType itemType)
        {
            if (this.Layout.IsGroupDetailHidden
                && this.Layout.GroupList.Any())
                return null;

            switch (itemType)
            {
                case PlanItemType.Annotation:
                    return typeof(PlanogramAnnotationView);

                case PlanItemType.Assembly:
                    return typeof(PlanogramAssemblyView);

                case PlanItemType.Component:
                    return typeof(PlanogramComponentView);

                case PlanItemType.Fixture:
                    return typeof(PlanogramFixtureView);

                case PlanItemType.Planogram:
                    return typeof(PlanogramView);

                default:
                case PlanItemType.Position:
                    return typeof(PlanogramPositionView);

                case PlanItemType.Product:
                    return typeof(PlanogramProductView);

                case PlanItemType.SubComponent:
                    return typeof(PlanogramSubComponentView);
            }
        }

        #endregion

        /// <summary>
        /// If the document is visible, performs a column refresh
        /// Otherwise flags if a column refresh is required.
        /// </summary>
        private void OnRefreshRowsAndColumnsRequired()
        {
            if (this.IsVisible)
            {
                RefreshRowsAndColumns();
            }
            else
            {
                _isColumnRefreshRequired = true;
            }
        }

        /// <summary>
        /// Causes all rows and columns to be refreshed.
        /// </summary>
        private void RefreshRowsAndColumns()
        {
            _isColumnRefreshRequired = false;

            RaiseLayoutChanging();

            if (this.Layout != null)
            {
                _columnDetails = GetOrderedColumnDetails();
            }
            else
            {
                _columnDetails = new List<DataSheetColumn>();
            }

            //clear existing rows first to force a full refresh.
            if (_rows.Any()) _rows.Clear();

            RefreshRows();

            RaiseLayoutChanged();
        }

        /// <summary>
        /// Shows the datasheet editor to allow the user
        /// to create and load a layout into this document.
        /// </summary>
        /// <returns>True if a layout was selected.</returns>
        public Boolean ShowDataSheetEditorWindow()
        {
            CustomColumnLayout currentLayout = this.Layout;

            //Create the editor window and preselect plans.
            ReportColumnLayoutEditorViewModel editorVm = new ReportColumnLayoutEditorViewModel(currentLayout, this.ParentController);
            if (currentLayout != null && !currentLayout.IncludeAllPlanograms && this.IncludedPlans.Count > 1)
            {
                foreach (var planRow in editorVm.AvailablePlanograms)
                {
                    planRow.IsSelected = (this.IncludedPlans.Select(s => s.Controller).Contains(planRow.PlanController));
                }
            }

            //show the editor window
            CommonHelper.GetWindowService().ShowDialog<ReportColumnLayoutEditorOrganiser>(editorVm);
            if (editorVm.DialogResult != true) return false;

            CustomColumnLayout newLayout = editorVm.CurrentLayout;

            //if the data sheet has been saved and is the same layout
            // then add it to the favourites.
            if(newLayout != null) App.MainPageViewModel.AddFavouriteDataSheet(newLayout);
            if (currentLayout == null
                || (currentLayout != null && String.Equals(currentLayout.Name, newLayout.Name)))
            {
                if (currentLayout != null && !newLayout.IsNew && newLayout.IsDirty)
                {
                    newLayout.Name = Message.DataSheet_CustomDataSheet;
                }
                App.MainPageViewModel.CustomDataSheet = newLayout;
            }

            //Set the selected planograms collection.
            List<DataSheetPlanogram> selectedPlans = new List<DataSheetPlanogram>();
            foreach (var planRow in editorVm.AvailablePlanograms)
            {
                if (!planRow.IsSelected || planRow.PlanController.IsDisposed) continue;
                selectedPlans.Add(new DataSheetPlanogram(planRow.PlanController, planRow.DisplayName));
            }

            //check that the active planogram is in there and that it is first
            DataSheetPlanogram activePlan = selectedPlans.FirstOrDefault(p => p.Controller == this.ParentController);
            if (activePlan == null) activePlan = new DataSheetPlanogram(this.ParentController, this.ParentController.SourcePlanogram.Name);
            else selectedPlans.Remove(activePlan);
            selectedPlans.Insert(0, activePlan);

            this.IncludedPlans = selectedPlans.AsReadOnly();

            //set the layout - this will cause the grid to update.
            this.Layout = newLayout;

            return true;
        }

        /// <summary>
        /// Loads the given layout into this document.
        /// </summary>
        /// <param name="layout"></param>
        public void LoadLayout(CustomColumnLayout layout)
        {
            //Set the selected planograms collection.
            List<DataSheetPlanogram> selectedPlans = new List<DataSheetPlanogram>();
            if (layout.IncludeAllPlanograms)
            {
                foreach (PlanControllerViewModel controller in App.MainPageViewModel.PlanControllers)
                {
                    selectedPlans.Add(new DataSheetPlanogram(controller, controller.SourcePlanogram.Name));
                }
            }

            //check that the active planogram is in there and that it is first
            DataSheetPlanogram activePlan = selectedPlans.FirstOrDefault(p => p.Controller == this.ParentController);
            if (activePlan == null) activePlan = new DataSheetPlanogram(this.ParentController, this.ParentController.SourcePlanogram.Name);
            else selectedPlans.Remove(activePlan);
            selectedPlans.Insert(0, activePlan);

            this.IncludedPlans = selectedPlans.AsReadOnly();

            //set the layout - this will cause the grid to update.
            this.Layout = layout;
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!IsDisposed)
            {
                App.MainPageViewModel.PlanControllers.BulkCollectionChanged -= OpenPlanControllers_BulkCollectionChanged;
                SetPlanEventHandlers(false);

                OnIncludedPlanogramsChanged(this.IncludedPlans, null);

                base.Dispose(disposing);

                IsDisposed = true;
            }
        }

        #endregion

    }

    /// <summary>
    /// Represents a planogram to be included by the datasheet.
    /// </summary>
    public sealed class DataSheetPlanogram
    {
        public PlanControllerViewModel Controller { get; private set; }
        public String Alias { get; private set; }

        public DataSheetPlanogram(PlanControllerViewModel controller)
            : this(controller, controller.SourcePlanogram.Name)
        { }

        public DataSheetPlanogram(PlanControllerViewModel controller, String alias)
        {
            Controller = controller;
            Alias = alias;
        }
    }

}