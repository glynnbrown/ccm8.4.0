﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)

// V8-27938 : N.Haywood
//  Created
// V8-28199 : N.Haywood
//  Made apply save the current data sheet
// V8-28175 : A.Kuszyk
//  Added Planogram Performance decorator to column list and also moved code that modifies
//  fields from column layout factory to constructor.
// V8-28175 : N.Haywood
//  moved the modified fields back to the factory
// V8-28392 : N.Haywood
//  Made the group/sort lists only refresh when the column layout is dirty
#endregion

#region Version History: (CCM 8.0.1)
// V8-28439 : M.Shelley
//  Added a method (UpdateTotalsAggregation) to fix the case when creating a new data sheet that
//  the aggregation total types that the user has selected are lost when saving the data sheet
#endregion

#region Version History: (CCM 8.1.0)
// V8-29452 : M.Brumby
//  Added the use of UpdateTotalsAggregation to fix the case when creating a new data sheet that
//  the aggregation total types that the user has selected are lost when applying the data sheet
// V8-29681 : A.Probyn
// Implemented new RegisterPlanogram when using product column layout factory
#endregion

#region Version History: (CCM 8.1.1)
// V8-30402 : N.Haywood
//  Set header group number for newly added columns
// V8-30005 : N.Haywood
//  changed removing assigned columns so it doesn't refresh the whole unassigned grid
// V8-30532 : M.Shelley
//  Prevent an InvalidOperationException thrown by the ModelList class in the framework
#endregion

#region Version History: (CCM 8.2.0)
//V8-30870 : L.Ineson
//  Design change.
#endregion

#region Version History: (CCM 8.3.0)
// V8-31542 : L.Ineson
//  Added the ability to select planograms,
// Removed group header numbers.
//  Added support for calculated columns.
// V8-32016 : L.Ineson
//  Added EditCalculatedColumnCommand
// V8-32101 : L.Ineson
//  Made sure that the grouping list gets updated correctly.
// V8-32157 : A. Silva
//  Removed Add All and Remove All buttons, amended Add and Remove Selected buttons.
// V8-32249 : L.Ineson
//  Stopped the user from being able to move above locked columns.
// V8-32424 : L.Ineson
//  The active planogram is now used as the context for performance fields.
// V8-32652 : A.Silva
//  Refactored Add Remove selected to use the common icons.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using Csla;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using CommonImageResources = Galleria.Ccm.Common.Wpf.Resources.ImageResources;
using CommonMessage = Galleria.Ccm.Common.Wpf.Resources.Language.Message;

namespace Galleria.Ccm.Editor.Wpf.ReportColumnLayouts
{
    /// <summary>
    ///  Viewmodel controller for the DataSheets column selector window.
    /// </summary>
    public sealed class ReportColumnLayoutEditorViewModel : WindowViewModelBase
    {
        #region Nested Classes

        public sealed class DataSheetFieldGroup
        {
            public String GroupName { get; set; }
            public Boolean IsAllGroup { get; set; }

            public override string ToString()
            {
                return GroupName;
            }
        }

        public sealed class DataSheetField
        {
            #region Fields
            private String _path;
            #endregion

            #region Properties

            public String GroupName { get; set; }
            public ObjectFieldInfo FieldInfo { get; private set; }


            public String Header
            {
                get { return FieldInfo.FieldFriendlyName; }
            }

            public String Path
            {
                get { return FieldInfo.FieldPlaceholder; }
            }

            #endregion

            #region Constructor

            public DataSheetField(ObjectFieldInfo fieldInfo, String groupName)
            {
                this.FieldInfo = fieldInfo;
                this.GroupName = groupName;
            }

            #endregion

            #region Methods
            public override String ToString()
            {
                return Header;
            }
            #endregion
        }

        /// <summary>
        /// Wrapper object for the custom column that includes any lookups
        /// </summary>
        public sealed class DataSheetColumnView : INotifyPropertyChanged
        {
            #region Fields
            private readonly CustomColumn _column;
            private readonly String _headerGroup;
            private readonly String _defaultHeader;

            #endregion

            #region Properties

            public CustomColumn Column
            {
                get { return _column; }
            }

            public String Path
            {
                get { return _column.Path; }
                set { _column.Path = value; }
            }

            public Int32 Number
            {
                get { return _column.Number; }
                set
                {
                    if (_column.Number != value && !_column.IsDeleted)
                    {
                        _column.Number = value;
                        OnPropertyChanged("Number");
                        OnPropertyChanged("ColumnNumberAndHeader");
                    }
                }
            }

            public String Header
            {
                get { return _defaultHeader; }
            }

            public String FullDisplayName
            {
                get { return DisplayName; }
            }

            public String ColumnNumberAndHeader
            {
                get { return Number.ToString() + ". " + Header; }
            }

            /// <summary>
            /// Gets/Sets the column display name
            /// </summary>
            public String DisplayName
            {
                get
                {
                    if (!String.IsNullOrEmpty(_column.DisplayName))
                    {
                        return _column.DisplayName;
                    }
                    return this.Header;
                }
                set
                {
                    _column.DisplayName = value;
                    OnPropertyChanged("DisplayName");
                    OnPropertyChanged("FullDisplayName");
                }
            }

            /// <summary>
            /// Gets/Sets the width of the column.
            /// </summary>
            public Int32 Width
            {
                get { return _column.Width; }
                set
                {
                    _column.Width = value;
                    OnPropertyChanged("Width");
                }
            }

            /// <summary>
            /// Gets/Sets the totals type of the column.
            /// </summary>
            public CustomColumnTotalType TotalType
            {
                get { return _column.TotalType; }
                set
                {
                    _column.TotalType = value;
                    OnPropertyChanged("TotalType");
                }
            }

            /// <summary>
            /// Gets/Sets whether this is a key column that should be locked in place.
            /// </summary>
            public Boolean IsLocked { get; set; }

            /// <summary>
            /// Gets/Sets whether this is a calculated column.
            /// </summary>
            public Boolean IsCalculated { get; set; }

            #endregion

            #region Constructor

            /// <summary>
            /// Default constructor
            /// </summary>
            public DataSheetColumnView(CustomColumn column, String header, String headerGroupName)
            {
                _column = column;
                _defaultHeader = header;
                _headerGroup = headerGroupName;
            }

            #endregion

            #region Override

            public override String ToString()
            {
                return Header;
            }

            #endregion

            #region INotifyProperty Changed

            public event PropertyChangedEventHandler PropertyChanged;

            private void OnPropertyChanged(String property)
            {
                if (PropertyChanged == null) return;
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }

            #endregion

            #region Static 

            public static DataSheetColumnView NewBlankView()
            {
                return new DataSheetColumnView(
                    CustomColumn.NewCustomColumn(CommonMessage.ColumnLayoutEditor_EmptyGroupSelection, 0),
                    null, null);
            }

            #endregion
        }

        /// <summary>
        /// Represents a planogram for which the datasheet may display data.
        /// </summary>
        public sealed class DataSheetPlanogramRow : INotifyPropertyChanged
        {
            #region Fields
            private Boolean _isSelected;
            private String _displayName;
            #endregion

            #region Properties

            /// <summary>
            /// Returns the plan controller represented by this row.
            /// </summary>
            public PlanControllerViewModel PlanController
            {
                get;
                private set;
            }

            /// <summary>
            /// Returns true if this is the active planogram.
            /// </summary>
            public Boolean IsCurrentPlanogram
            {
                get;
                set;
            }

            /// <summary>
            /// Gets/Sets whether this row is selected.
            /// </summary>
            public Boolean IsSelected
            {
                get { return _isSelected; }
                set
                {
                    if (value != _isSelected)
                    {
                        _isSelected = value;
                        OnPropertyChanged("IsSelected");
                    }
                }
            }

            /// <summary>
            /// Gets/Sets the display name
            /// </summary>
            public String DisplayName
            {
                get
                {
                    if (!String.IsNullOrWhiteSpace(_displayName))
                    {
                        return _displayName;
                    }
                    return Name;
                }
                set
                {
                    _displayName =
                        (!String.IsNullOrWhiteSpace(_displayName)) ? value : Name;
                    OnPropertyChanged("DisplayName");
                }
            }

            /// <summary>
            /// The plan name
            /// </summary>
            public String Name
            {
                get { return this.PlanController.SourcePlanogram.Name; }
            }

            /// <summary>
            /// The plan category code
            /// </summary>
            public String CategoryCode
            {
                get { return this.PlanController.SourcePlanogram.CategoryCode; }
            }

            /// <summary>
            /// The plan category name
            /// </summary>
            public String CategoryName
            {
                get { return this.PlanController.SourcePlanogram.CategoryName; }
            }

            /// <summary>
            /// The plan location path 
            /// - either the repository folder or file path.
            /// </summary>
            public String Path
            {
                get
                {
                    return this.PlanController.SourcePlanogram.ParentPackageView.GetLocationPath();
                }
            }

            /// <summary>
            /// The plan overall height
            /// </summary>
            public Single Height
            {
                get { return this.PlanController.SourcePlanogram.Height; }
            }

            /// <summary>
            /// The plan overall width
            /// </summary>
            public Single Width
            {
                get { return this.PlanController.SourcePlanogram.Width; }
            }

            /// <summary>
            /// The plan overall depth
            /// </summary>
            public Single Depth
            {
                get { return this.PlanController.SourcePlanogram.Depth; }
            }

            /// <summary>
            /// The display units collection for the plan.
            /// </summary>
            public DisplayUnitOfMeasureCollection DisplayUnits
            {
                get { return this.PlanController.SourcePlanogram.DisplayUnits; }
            }

            #endregion

            #region Constructor

            /// <summary>
            /// Creates a new instance of this type.
            /// </summary>
            public DataSheetPlanogramRow(PlanControllerViewModel planController)
            {
                this.PlanController = planController;
                this.DisplayName = planController.SourcePlanogram.Name;
            }

            #endregion

            #region INotifyPropertyChanged Members
            public event PropertyChangedEventHandler PropertyChanged;
            private void OnPropertyChanged(String propertyName)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }

        #endregion

        #region Fields

        private Boolean? _dialogResult;
        const CustomColumnLayoutType _fixedColumnLayoutType = CustomColumnLayoutType.DataSheet;

        private CustomColumnLayout _currentLayout;

        private readonly ReadOnlyCollection<DataSheetField> _masterFields;
        private readonly ReadOnlyCollection<DataSheetFieldGroup> _availableFieldGroups;
        private DataSheetFieldGroup _selectedFieldGroup;
        private readonly BulkObservableCollection<DataSheetField> _selectedGroupFields = new BulkObservableCollection<DataSheetField>();
        private ReadOnlyBulkObservableCollection<DataSheetField> _selectedGroupFieldsRO;
        private readonly ObservableCollection<DataSheetField> _selectedFields = new ObservableCollection<DataSheetField>();

        private readonly BulkObservableCollection<DataSheetColumnView> _assignedColumnViews = new BulkObservableCollection<DataSheetColumnView>();
        private ReadOnlyBulkObservableCollection<DataSheetColumnView> _assignedColumnViewsRO;
        private readonly ObservableCollection<DataSheetColumnView> _selectedAssignedColumnViews = new ObservableCollection<DataSheetColumnView>();

        
        private ObservableCollection<DataSheetColumnView> _availableFilterByColumns = new ObservableCollection<DataSheetColumnView>();
        private ObservableCollection<DataSheetColumnView> _availableGroupSortByColumns = new ObservableCollection<DataSheetColumnView>();

        private String _groupByLevelFour = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
        private String _groupByLevelOne = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
        private String _groupByLevelThree = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
        private String _groupByLevelTwo = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;

        private String _sortByLevelFour = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
        private Boolean _sortByLevelFourAscending;
        private Boolean _sortByLevelFourDescending = true;
        private String _sortByLevelOne = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
        private Boolean _sortByLevelOneAscending;
        private Boolean _sortByLevelOneDescending = true;
        private String _sortByLevelThree = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
        private Boolean _sortByLevelThreeAscending;
        private Boolean _sortByLevelThreeDescending = true;
        private String _sortByLevelTwo = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
        private Boolean _sortByLevelTwoAscending;
        private Boolean _sortByLevelTwoDescending = true;

        private CustomColumnFilter _selectedFiltersSelection;
        private DataSheetColumnView _selectedNewFilterColumn;
        private String _selectedNewFilterText = String.Empty;

        private Boolean _suppressPlanPropertyChange;
        
        #endregion

        #region Property Paths

        //Properties
        public static readonly PropertyPath CurrentLayoutProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.CurrentLayout);
        public static readonly PropertyPath AvailableFieldGroupsProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(p => p.AvailableFieldGroups);
        public static readonly PropertyPath SelectedFieldGroupProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(p => p.SelectedFieldGroup);
        public static readonly PropertyPath SelectedGroupFieldsProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(p => p.SelectedGroupFields);
        public static readonly PropertyPath SelectedFieldsProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(p => p.SelectedFields);
        public static readonly PropertyPath AssignedColumnViewsProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(p => p.AssignedColumnViews);
        public static readonly PropertyPath SelectedAssignedColumnViewsProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(p => p.SelectedAssignedColumnViews);

        public static readonly PropertyPath AvailableFilterByColumnsProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.AvailableFilterByColumns);
        public static readonly PropertyPath SelectedFiltersProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.SelectedFilters);
        public static readonly PropertyPath SelectedFiltersSelectionProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.SelectedFiltersSelection);
        public static readonly PropertyPath SelectedNewFilterColumnProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.SelectedNewFilterColumn);
        public static readonly PropertyPath SelectedNewFilterTextProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.SelectedNewFilterText);

        public static readonly PropertyPath AvailableGroupByColumnsFourProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.AvailableGroupByColumnsFour);
        public static readonly PropertyPath AvailableGroupByColumnsThreeProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.AvailableGroupByColumnsThree);
        public static readonly PropertyPath AvailableGroupByColumnsTwoProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.AvailableGroupByColumnsTwo);
        public static readonly PropertyPath AvailableGroupSortByColumnsProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.AvailableGroupSortByColumns);
        public static readonly PropertyPath AvailableSortByColumnsFourProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.AvailableSortByColumnsFour);
        public static readonly PropertyPath AvailableSortByColumnsThreeProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.AvailableSortByColumnsThree);
        public static readonly PropertyPath AvailableSortByColumnsTwoProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.AvailableSortByColumnsTwo);

        public static readonly PropertyPath GroupByLevelFourProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.GroupByLevelFour);
        public static readonly PropertyPath GroupByLevelOneProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.GroupByLevelOne);
        public static readonly PropertyPath GroupByLevelThreeProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.GroupByLevelThree);
        public static readonly PropertyPath GroupByLevelTwoProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.GroupByLevelTwo);

        public static readonly PropertyPath SortByLevelFourAscendingProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.SortByLevelFourAscending);
        public static readonly PropertyPath SortByLevelFourDescendingProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.SortByLevelFourDescending);
        public static readonly PropertyPath SortByLevelFourProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.SortByLevelFour);
        public static readonly PropertyPath SortByLevelOneAscendingProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.SortByLevelOneAscending);
        public static readonly PropertyPath SortByLevelOneDescendingProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.SortByLevelOneDescending);
        public static readonly PropertyPath SortByLevelOneProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.SortByLevelOne);
        public static readonly PropertyPath SortByLevelThreeAscendingProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.SortByLevelThreeAscending);
        public static readonly PropertyPath SortByLevelThreeDescendingProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.SortByLevelThreeDescending);
        public static readonly PropertyPath SortByLevelThreeProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.SortByLevelThree);
        public static readonly PropertyPath SortByLevelTwoAscendingProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.SortByLevelTwoAscending);
        public static readonly PropertyPath SortByLevelTwoDescendingProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.SortByLevelTwoDescending);
        public static readonly PropertyPath SortByLevelTwoProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.SortByLevelTwo);

        public static readonly PropertyPath AvailablePlanogramsProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.AvailablePlanograms);

        // Commands
        public static readonly PropertyPath AddSelectedColumnsCommandProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.AddSelectedColumnsCommand);
        public static readonly PropertyPath MoveColumnDownCommandProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.MoveColumnDownCommand);
        public static readonly PropertyPath MoveColumnUpCommandProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.MoveColumnUpCommand);
        public static readonly PropertyPath RemoveSelectedColumnsCommandProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.RemoveSelectedColumnsCommand);
        public static readonly PropertyPath SaveAsCommandProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.SaveAsCommand);
        public static readonly PropertyPath ApplyCommandProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.ApplyCommand);
        public static readonly PropertyPath CloseCommandProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.CloseCommand);
        public static readonly PropertyPath OpenCommandProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.OpenCommand);
        public static readonly PropertyPath AddFilterCommandProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.AddFilterCommand);
        public static readonly PropertyPath RemoveFilterCommandProperty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.RemoveFilterCommand);
        public static readonly PropertyPath AddCalculatedColumnCommandProeprty = GetPropertyPath<ReportColumnLayoutEditorViewModel>(o => o.AddCalculatedColumnCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the result and closes the 
        /// attached dialog.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;
                CloseDialog(value);
            }
        }

        /// <summary>
        /// Gets the currently loaded custom column layout.
        /// </summary>
        public CustomColumnLayout CurrentLayout
        {
            get { return _currentLayout; }
            set
            {
                CustomColumnLayout oldValue = _currentLayout;

                _currentLayout = value;
                OnPropertyChanged(CurrentLayoutProperty);

                OnCurrentLayoutChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Gets the list of available item types
        /// </summary>
        public ReadOnlyCollection<DataSheetFieldGroup> AvailableFieldGroups
        {
            get { return _availableFieldGroups; }
        }

        /// <summary>
        /// Gets/Sets the selected item type
        /// </summary>
        public DataSheetFieldGroup SelectedFieldGroup
        {
            get { return _selectedFieldGroup; }
            set
            {
                _selectedFieldGroup = value;
                OnPropertyChanged(SelectedFieldGroupProperty);

                OnSelectedFieldGroupChanged(value);
            }
        }

        /// <summary>
        /// Returns the collection of fields available for the selected item type.
        /// </summary>
        public ReadOnlyBulkObservableCollection<DataSheetField> SelectedGroupFields
        {
            get
            {
                if (_selectedGroupFieldsRO == null)
                {
                    _selectedGroupFieldsRO = new ReadOnlyBulkObservableCollection<DataSheetField>(_selectedGroupFields);
                }
                return _selectedGroupFieldsRO;
            }
        }

        /// <summary>
        /// Returns the editable collection of the 
        /// </summary>
        public ObservableCollection<DataSheetField> SelectedFields
        {
            get { return _selectedFields; }
        }

        /// <summary>
        /// Returns the collection of views for columns that have been assigned.
        /// </summary>
        public ReadOnlyBulkObservableCollection<DataSheetColumnView> AssignedColumnViews
        {
            get
            {
                if (_assignedColumnViewsRO == null)
                {
                    _assignedColumnViewsRO = new ReadOnlyBulkObservableCollection<DataSheetColumnView>(_assignedColumnViews);
                }
                return _assignedColumnViewsRO;
            }
        }

        /// <summary>
        /// Returns the editable collection of column views that are selected.
        /// </summary>
        public ObservableCollection<DataSheetColumnView> SelectedAssignedColumnViews
        {
            get { return _selectedAssignedColumnViews; }
        }

        #region Filtering Properties

        /// <summary>
        ///     Gets/Sets the selected filters
        /// </summary>
        public CustomColumnFilterList SelectedFilters
        {
            get { return this.CurrentLayout.FilterList; }
        }

        /// <summary>
        ///     Gets/Sets the selected filter item
        /// </summary>
        public CustomColumnFilter SelectedFiltersSelection
        {
            get { return _selectedFiltersSelection; }
            set
            {
                _selectedFiltersSelection = value;
                OnPropertyChanged(SelectedFiltersSelectionProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets the selected filter column
        /// </summary>
        public DataSheetColumnView SelectedNewFilterColumn
        {
            get { return _selectedNewFilterColumn; }
            set
            {
                _selectedNewFilterColumn = value;
                OnPropertyChanged(SelectedNewFilterColumnProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets the selected new filter text
        /// </summary>
        public String SelectedNewFilterText
        {
            get { return _selectedNewFilterText; }
            set
            {
                _selectedNewFilterText = value;
                OnPropertyChanged(SelectedNewFilterTextProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets the available filter by columns collection
        /// </summary>
        public ObservableCollection<DataSheetColumnView> AvailableFilterByColumns
        {
            get { return _availableFilterByColumns; }
            set
            {
                _availableFilterByColumns = value;
                OnPropertyChanged(AvailableFilterByColumnsProperty);
            }
        }

        #endregion

        #region GroupBy Properties

        /// <summary>
        ///     returns the available group by column collection for level four
        /// </summary>
        public ReadOnlyCollection<DataSheetColumnView> AvailableGroupByColumnsFour
        {
            get { return GetFilteredCollection(AvailableGroupByColumnsFourProperty); }
        }

        /// <summary>
        ///     returns the available group by column collection for level three
        /// </summary>
        public ReadOnlyCollection<DataSheetColumnView> AvailableGroupByColumnsThree
        {
            get { return GetFilteredCollection(AvailableGroupByColumnsThreeProperty); }
        }

        /// <summary>
        ///     returns the available group by column collection for level two
        /// </summary>
        public ReadOnlyCollection<DataSheetColumnView> AvailableGroupByColumnsTwo
        {
            get { return GetFilteredCollection(AvailableGroupByColumnsTwoProperty); }
        }

        /// <summary>
        ///     Gets/Sets the GroupBy selection for level one
        /// </summary>
        public String GroupByLevelOne
        {
            get { return _groupByLevelOne; }
            set
            {
                _groupByLevelOne = value ?? CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                CascadePropertyClear(GroupByLevelOneProperty);

                OnPropertyChanged(GroupByLevelOneProperty);
                OnPropertyChanged(AvailableGroupByColumnsTwoProperty);
                OnPropertyChanged(AvailableGroupByColumnsThreeProperty);
                OnPropertyChanged(AvailableGroupByColumnsFourProperty);

                UpdateGroupByList();
            }
        }

        /// <summary>
        ///     Gets/Sets the GroupBy selection for level two
        /// </summary>
        public String GroupByLevelTwo
        {
            get { return _groupByLevelTwo; }
            set
            {
                _groupByLevelTwo = value ?? CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                CascadePropertyClear(GroupByLevelTwoProperty);

                OnPropertyChanged(GroupByLevelTwoProperty);
                OnPropertyChanged(AvailableGroupByColumnsThreeProperty);
                OnPropertyChanged(AvailableGroupByColumnsFourProperty);

                UpdateGroupByList();
            }
        }

        /// <summary>
        ///     Gets/Sets the GroupBy selection for level three
        /// </summary>
        public String GroupByLevelThree
        {
            get { return _groupByLevelThree; }
            set
            {
                _groupByLevelThree = value ?? CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                CascadePropertyClear(GroupByLevelThreeProperty);

                OnPropertyChanged(GroupByLevelThreeProperty);
                OnPropertyChanged(AvailableGroupByColumnsFourProperty);

                UpdateGroupByList();
            }
        }

        /// <summary>
        ///     Gets/Sets the GroupBy selection for level four
        /// </summary>
        public String GroupByLevelFour
        {
            get { return _groupByLevelFour; }
            set
            {
                _groupByLevelFour = value ?? CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;

                OnPropertyChanged(GroupByLevelFourProperty);

                UpdateGroupByList();
            }
        }

        #endregion

        #region Sort By properties

        /// <summary>
        ///     Gets/Sets the available group & sort by columns collection
        /// </summary>
        public ObservableCollection<DataSheetColumnView> AvailableGroupSortByColumns
        {
            get { return _availableGroupSortByColumns; }
            private set
            {
                _availableGroupSortByColumns = value;
                //fire property changed for the all collections
                OnPropertyChanged(AvailableGroupSortByColumnsProperty);
                OnPropertyChanged(AvailableGroupByColumnsTwoProperty);
                OnPropertyChanged(AvailableGroupByColumnsThreeProperty);
                OnPropertyChanged(AvailableGroupByColumnsFourProperty);
                OnPropertyChanged(AvailableSortByColumnsTwoProperty);
                OnPropertyChanged(AvailableSortByColumnsThreeProperty);
                OnPropertyChanged(AvailableSortByColumnsFourProperty);
            }
        }

        /// <summary>
        ///     returns the available sort by column collection for level four
        /// </summary>
        public ReadOnlyCollection<DataSheetColumnView> AvailableSortByColumnsFour
        {
            get { return GetFilteredCollection(AvailableSortByColumnsFourProperty); }
        }

        /// <summary>
        ///     returns the available sort by column collection for level three
        /// </summary>
        public ReadOnlyCollection<DataSheetColumnView> AvailableSortByColumnsThree
        {
            get { return GetFilteredCollection(AvailableSortByColumnsThreeProperty); }
        }

        /// <summary>
        ///     returns the available sort by column collection for level two
        /// </summary>
        public ReadOnlyCollection<DataSheetColumnView> AvailableSortByColumnsTwo
        {
            get { return GetFilteredCollection(AvailableSortByColumnsTwoProperty); }
        }

        /// <summary>
        ///     Gets/Sets the sort by level four value
        /// </summary>
        public String SortByLevelFour
        {
            get { return _sortByLevelFour; }
            set
            {
                _sortByLevelFour = value ?? CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                OnPropertyChanged(SortByLevelFourProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets whether sort by level four is ascending
        /// </summary>
        public Boolean SortByLevelFourAscending
        {
            get { return _sortByLevelFourAscending; }
            set
            {
                _sortByLevelFourAscending = value;
                OnPropertyChanged(SortByLevelFourAscendingProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets whether sort by level four is descending
        /// </summary>
        public Boolean SortByLevelFourDescending
        {
            get { return _sortByLevelFourDescending; }
            set
            {
                _sortByLevelFourDescending = value;
                OnPropertyChanged(SortByLevelFourDescendingProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets the sort by level one value
        /// </summary>
        public String SortByLevelOne
        {
            get { return _sortByLevelOne; }
            set
            {
                _sortByLevelOne = value ?? CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                CascadePropertyClear(SortByLevelOneProperty);

                OnPropertyChanged(SortByLevelOneProperty);
                OnPropertyChanged(AvailableSortByColumnsTwoProperty);
                OnPropertyChanged(AvailableSortByColumnsThreeProperty);
                OnPropertyChanged(AvailableSortByColumnsFourProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets whether sort by level one is ascending
        /// </summary>
        public Boolean SortByLevelOneAscending
        {
            get { return _sortByLevelOneAscending; }
            set
            {
                _sortByLevelOneAscending = value;
                OnPropertyChanged(SortByLevelOneAscendingProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets whether sort by level one is descending
        /// </summary>
        public Boolean SortByLevelOneDescending
        {
            get { return _sortByLevelOneDescending; }
            set
            {
                _sortByLevelOneDescending = value;
                OnPropertyChanged(SortByLevelOneDescendingProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets the sort by level three value
        /// </summary>
        public String SortByLevelThree
        {
            get { return _sortByLevelThree; }
            set
            {
                _sortByLevelThree = value ?? CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                CascadePropertyClear(SortByLevelThreeProperty);

                OnPropertyChanged(SortByLevelThreeProperty);
                OnPropertyChanged(AvailableSortByColumnsFourProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets whether sort by level three is ascending
        /// </summary>
        public Boolean SortByLevelThreeAscending
        {
            get { return _sortByLevelThreeAscending; }
            set
            {
                _sortByLevelThreeAscending = value;
                OnPropertyChanged(SortByLevelThreeAscendingProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets whether sort by level three is descending
        /// </summary>
        public Boolean SortByLevelThreeDescending
        {
            get { return _sortByLevelThreeDescending; }
            set
            {
                _sortByLevelThreeDescending = value;
                OnPropertyChanged(SortByLevelThreeDescendingProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets the sort by level two value
        /// </summary>
        public String SortByLevelTwo
        {
            get { return _sortByLevelTwo; }
            set
            {
                _sortByLevelTwo = value ?? CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                CascadePropertyClear(SortByLevelTwoProperty);

                OnPropertyChanged(SortByLevelTwoProperty);
                OnPropertyChanged(AvailableSortByColumnsThreeProperty);
                OnPropertyChanged(AvailableSortByColumnsFourProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets whether sort by level two is ascending
        /// </summary>
        public Boolean SortByLevelTwoAscending
        {
            get { return _sortByLevelTwoAscending; }
            set
            {
                _sortByLevelTwoAscending = value;
                OnPropertyChanged(SortByLevelTwoAscendingProperty);
            }
        }

        /// <summary>
        ///     Gets/Sets whether sort by level two is descending
        /// </summary>
        public Boolean SortByLevelTwoDescending
        {
            get { return _sortByLevelTwoDescending; }
            set
            {
                _sortByLevelTwoDescending = value;
                OnPropertyChanged(SortByLevelTwoDescendingProperty);
            }
        }

        #endregion


        /// <summary>
        /// Returns the collection of planograms available for use by 
        /// the current datasheet.
        /// </summary>
        public ReadOnlyObservableCollection<DataSheetPlanogramRow> AvailablePlanograms
        {
            get;
            private set;
        }

        #endregion

        #region Constructor

        /// <summary>
        ///     Constructor
        /// </summary>
        public ReportColumnLayoutEditorViewModel(PlanControllerViewModel parentController)
            : this(null, parentController)
        { }

        /// <summary>
        ///     Constructor
        /// </summary>
        public ReportColumnLayoutEditorViewModel(CustomColumnLayout layout, PlanControllerViewModel parentController)
        {
            IColumnLayoutFactory factory = new ReportColumnLayoutFactory(typeof(Object));
            factory.RegisterPlanogram(App.ViewState.ActivePlanogramView.Model.Model);

            if (layout == null)
            {
                layout = CustomColumnLayout.NewCustomColumnLayout(
                             CustomColumnLayoutType.DataSheet, factory.NewCustomColumnList(/*default*/true));

                //If we are creating a new datasheet then set the command names accordingly.
                this.CloseCommand.FriendlyName = Message.Generic_Cancel;
                this.ApplyCommand.FriendlyName = CommonMessage.Generic_Create;
            }
            Debug.Assert(layout.Type == CustomColumnLayoutType.DataSheet, "Not a valid datasheet!");

            //set the list of available planograms
            ObservableCollection<DataSheetPlanogramRow> availablePlans = new ObservableCollection<DataSheetPlanogramRow>();
            this.AvailablePlanograms = new ReadOnlyObservableCollection<DataSheetPlanogramRow>(availablePlans);
            foreach (var planController in App.MainPageViewModel.PlanControllers)
            {
                Boolean isParent = (planController == parentController);

                DataSheetPlanogramRow row =
                    new DataSheetPlanogramRow(planController)
                    {
                        IsSelected = layout.IncludeAllPlanograms || isParent,
                        IsCurrentPlanogram = isParent
                    };
                row.PropertyChanged += DataSheetPlanogramRow_PropertyChanged;
                availablePlans.Add(row);
            }

            //generate the master column views collection
            CustomColumnLayoutType layoutType = CustomColumnLayoutType.DataSheet;
            Debug.Assert(factory.LayoutType == layoutType, "factory is not for a datasheet?");

            //Setup master field lookup
            Dictionary<String, ObjectFieldInfo> masterFieldLookup = factory.GetModelObjectFields().ToDictionary(p => p.FieldPlaceholder);
            List<DataSheetField> allColumnFields = new List<DataSheetField>();
            foreach (CustomColumn col in factory.GetFullCustomColumnList())
            {
                ObjectFieldInfo fieldInfo;
                if (masterFieldLookup.TryGetValue(col.Path, out fieldInfo))
                {
                    allColumnFields.Add(new DataSheetField(fieldInfo, factory.GetHeaderGroupName(fieldInfo)));
                }
            }
            _masterFields = allColumnFields.AsReadOnly();

            //set the field groups
            List<DataSheetFieldGroup> columnGroups = new List<DataSheetFieldGroup>();
            columnGroups.Add(new DataSheetFieldGroup() { GroupName = Message.DataSheetEditor_AllFieldGroup, IsAllGroup = true });
            columnGroups.AddRange(_masterFields.GroupBy(g => g.GroupName).Select(c => new DataSheetFieldGroup() { GroupName = c.Key }));
            _availableFieldGroups = columnGroups.AsReadOnly();


            //Set initial column layout
            this.CurrentLayout = (layout != null) ? layout.Clone() :
                CustomColumnLayout.NewCustomColumnLayout(layoutType, factory.NewCustomColumnList(/*default*/true));


            //select the first field group
            this.SelectedFieldGroup = this.AvailableFieldGroups.First();

        }

        #endregion

        #region Commands

        #region OpenCommand

        private RelayCommand _openCommand;

        /// <summary>
        ///     Opens the selected <see cref="CustomColumnLayout" /> for edition.
        /// </summary>
        public RelayCommand OpenCommand
        {
            get
            {
                if (_openCommand != null) return _openCommand;

                _openCommand = new RelayCommand(
                    p => Open_Executed(p))
                {
                    FriendlyName = Message.Generic_Open,
                    FriendlyDescription = Message.ColumnLayoutEditor_OpenColumnLayout_Description,
                    Icon = CommonImageResources.Open_16,
                    SmallIcon = CommonImageResources.Open_16
                };
                RegisterCommand(_openCommand);

                return _openCommand;
            }
        }

        private void Open_Executed(Object args)
        {
            //Show the open file dialog
            String fileName = args as String;
            if (String.IsNullOrEmpty(fileName))
            {
                fileName = CustomColumnLayoutHelper.ShowDataSheetOpenFileDialog();
            }
            if (String.IsNullOrEmpty(fileName)) return;


            //load the file.
            base.ShowWaitCursor(true);
            try
            {
                this.CurrentLayout = CustomColumnLayout.FetchByFilename(fileName, /*asReadOnly*/true);

                //unlock the file immediately.
                CustomColumnLayout.UnlockCustomColumnLayoutByFileName(fileName);
            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(fileName, OperationType.Open);
                return;
            }
            base.ShowWaitCursor(false);
        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        ///     Saves the current <see cref="CustomColumnLayout" /> as a new layout.
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand != null) return _saveAsCommand;

                _saveAsCommand = new RelayCommand(
                    p => SaveAs_Executed(p),
                    p => SaveAs_CanExecute())
                {
                    FriendlyName = Message.Generic_SaveAs,
                    FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                    Icon = CommonImageResources.SaveAs_32,
                    SmallIcon = CommonImageResources.SaveAs_16,
                    DisabledReason = Message.Generic_Save_DisabledReasonInvalidData
                };
                RegisterCommand(_saveAsCommand);

                return _saveAsCommand;
            }
        }

        private Boolean SaveAs_CanExecute()
        {
            if (this.CurrentLayout.Type == CustomColumnLayoutType.Undefined)
            {
                SaveAsCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_LayoutLacksType;
                return false;
            }

            if (!this.CurrentLayout.IsValid)
            {
                SaveAsCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_LayoutIsNotValid;
                return false;
            }


            if (this.CurrentLayout.Name.IndexOfAny(Path.GetInvalidPathChars()) >= 0 &&
                     !this.CurrentLayout.IsInitialized)
            {
                SaveAsCommand.DisabledReason =
                    String.Format(Message.ColumnLayoutEditor_DisabledReason_LayoutNameContainsInvalidChars,
                        new String(Path.GetInvalidPathChars()));
                return false;
            }

            if (this.CurrentLayout.Columns.Count <= 0)
            {
                SaveAsCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoVisibleColumns;
                return false;
            }


            return true;
        }

        private void SaveAs_Executed(Object args)
        {
            //get the file name to save to.
            String fileName = args as String;
            if (String.IsNullOrEmpty(fileName)) 
            {
                fileName = CustomColumnLayoutHelper.ShowDataSheetSaveAsFileDialog();
            }
            if (String.IsNullOrEmpty(fileName)) return;


            //save it
            base.ShowWaitCursor(true);
            try
            {
                this.CurrentLayout.Name = Path.GetFileNameWithoutExtension(fileName);

                //check group by list
                UpdateGroupByList();

                //Build sort by list
                BuildSortByList(this.CurrentLayout.SortList);

                //Call save on the current view
                this.CurrentLayout = this.CurrentLayout.SaveAsFile(fileName);

                //unlock the file immediately.
                CustomColumnLayout.UnlockCustomColumnLayoutByFileName(fileName);
            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(fileName, OperationType.Save);
                return;
            }
            base.ShowWaitCursor(false);
        }

        #endregion

        #region ApplyCommand

        private RelayCommand _applyCommand;

        /// <summary>
        ///  Applies and closes the window.
        /// </summary>
        public RelayCommand ApplyCommand
        {
            get
            {
                if (_applyCommand == null)
                {
                    _applyCommand = new RelayCommand(
                        p => Apply_Executed(),
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.ColumnLayoutEditor_Apply,
                        FriendlyDescription = Message.ColumnLayoutEditor_Apply_Description,
                        Icon = CommonImageResources.ColumnLayoutEditor_Apply,
                        SmallIcon = CommonImageResources.ColumnLayoutEditor_Apply,
                        DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoVisibleColumns
                    };
                    RegisterCommand(_applyCommand);
                }

                return _applyCommand;
            }
        }

        private void Apply_Executed()
        {
            ShowWaitCursor(true);

            //if there's been a change, sort out the lists
            if (this.CurrentLayout.IsDirty)
            {
                //Build group by list
                UpdateGroupByList();

                //Build sort by list
                BuildSortByList(this.CurrentLayout.SortList);
            }

            ShowWaitCursor(false);

            //close the window
            this.DialogResult = true;
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;

        /// <summary>
        /// Closes the window
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        p => Close_Executed())
                    {
                        FriendlyName = Message.Generic_Close,
                        FriendlyDescription = CommonMessage.ColumnLayoutEditor_CloseWindow_Description,
                        Icon = CommonImageResources.Close_16,
                        SmallIcon = CommonImageResources.Close_16
                    };
                    RegisterCommand(_closeCommand);
                }

                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #region AddSelectedColumnsCommand

        private RelayCommand _addSelectedColumnsCommand;

        /// <summary>
        ///     Adds any selected available columns to the current view column list
        /// </summary>
        public RelayCommand AddSelectedColumnsCommand
        {
            get
            {
                if (_addSelectedColumnsCommand != null) return _addSelectedColumnsCommand;

                _addSelectedColumnsCommand = new RelayCommand(
                    p => AddSelectedColumns_Executed(),
                    p => AddSelectedColumns_CanExecute())
                {
                    FriendlyDescription =  CommonMessage.ColumnLayoutEditor_AddSelectedColumns_Description,
                    SmallIcon = CommonImageResources.Add_16,
                    DisabledReason = CommonMessage.ColumnLayoutEditor_DisabledReason_NoColumnsSelected
                };
                RegisterCommand(_addSelectedColumnsCommand);

                return _addSelectedColumnsCommand;
            }
        }

        private Boolean AddSelectedColumns_CanExecute()
        {
            if (this.SelectedFields.Count == 0)
            {
                return false;
            }
            return true;
        }

        private void AddSelectedColumns_Executed()
        {
            List<DataSheetField> columnsToAdd = this.SelectedFields.ToList();           
            this.SelectedFields.Clear();
            AddColumns(columnsToAdd);
        }

        #endregion

        #region RemoveSelectedColumnsCommand

        private RelayCommand _removeSelectedColumnsCommand;

        /// <summary>
        ///     Remove any selected available columns from the current view column list
        /// </summary>
        public RelayCommand RemoveSelectedColumnsCommand
        {
            get
            {
                if (_removeSelectedColumnsCommand != null) return _removeSelectedColumnsCommand;

                _removeSelectedColumnsCommand =
                    new RelayCommand(p => RemoveSelectedColumns_Executed(), p => RemoveSelectedColumns_CanExecute())
                    {
                        FriendlyDescription = CommonMessage.ColumnLayoutEditor_RemoveSelectedColumns_Description,
                        SmallIcon = CommonImageResources.Delete_16,
                        DisabledReason = CommonMessage.ColumnLayoutEditor_DisabledReason_NoColumnsSelected
                    };
                RegisterCommand(_removeSelectedColumnsCommand);

                return _removeSelectedColumnsCommand;
            }
        }

        private void RefreshProperties()
        {
            //fire property changed on group/sort properties to ensure
            // - available column collections are updated to reflect removal
            //OnPropertyChanged(AvailableColumnsProperty);
            //OnPropertyChanged(SelectedUiColumnsProperty);
            OnPropertyChanged(GroupByLevelOneProperty);
            OnPropertyChanged(GroupByLevelTwoProperty);
            OnPropertyChanged(GroupByLevelThreeProperty);
            OnPropertyChanged(GroupByLevelFourProperty);
            OnPropertyChanged(SortByLevelOneProperty);
            OnPropertyChanged(SortByLevelTwoProperty);
            OnPropertyChanged(SortByLevelThreeProperty);
            OnPropertyChanged(SortByLevelFourProperty);
        }

        private Boolean RemoveSelectedColumns_CanExecute()
        {
            return SelectedAssignedColumnViews.Any();
        }

        private void RemoveSelectedColumns_Executed()
        {
            RemoveColumns(this.SelectedAssignedColumnViews);
        }

        #endregion

        #region MoveColumnUpCommand

        private RelayCommand _moveColumnUpCommand;

        /// <summary>
        ///     Move column up in the column order
        /// </summary>
        public RelayCommand MoveColumnUpCommand
        {
            get
            {
                if (_moveColumnUpCommand != null) return _moveColumnUpCommand;

                _moveColumnUpCommand = new RelayCommand(
                    p => MoveColumnUp_Executed(),
                    p => MoveColumnUp_CanExecute())
                {
                    FriendlyDescription = Message.ColumnLayoutEditor_MoveColumnUp_Description,
                    SmallIcon = CommonImageResources.ColumnLayoutEditor_MoveColumnUp,
                    DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnSelected
                };
                RegisterCommand(_moveColumnUpCommand);

                return _moveColumnUpCommand;
            }
        }

        private Boolean MoveColumnUp_CanExecute()
        {
            if (this.SelectedAssignedColumnViews == null) 
                return false;

            if (this.SelectedAssignedColumnViews.Count == 0)
            {
                MoveColumnUpCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnSelected;
                return false;
            }
            if (this.SelectedAssignedColumnViews.Count != 1)
            {
                MoveColumnUpCommand.DisabledReason =
                    Message.ColumnLayoutEditor_DisabledReason_MultipleColumnsSelected;
                return false;
            }
            if (this.SelectedAssignedColumnViews[0].Number == (this.AssignedColumnViews.Count(s=> s.IsLocked)+1))
            {
                MoveColumnUpCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_ColumnIsTopAlready;
                return false;
            }
            if (this.SelectedAssignedColumnViews.Any(s => s.IsLocked))
            {
                return false;
            }

            return true;
        }

        private void MoveColumnUp_Executed()
        {
            ExchangeSelectionOrder(-1);
        }

        #endregion

        #region MoveColumnDownCommand

        private RelayCommand _moveColumnDownCommand;

        /// <summary>
        ///     Move column down in the column order
        /// </summary>
        public RelayCommand MoveColumnDownCommand
        {
            get
            {
                if (_moveColumnDownCommand != null) return _moveColumnDownCommand;

                _moveColumnDownCommand = new RelayCommand(
                    p => MoveColumnDown_Executed(),
                    p => MoveColumnDown_CanExecute())
                {
                    FriendlyDescription = Message.ColumnLayoutEditor_MoveColumnDown_Description,
                    SmallIcon = CommonImageResources.ColumnLayoutEditor_MoveColumnDown,
                    DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnSelected
                };
                RegisterCommand(_moveColumnDownCommand);

                return _moveColumnDownCommand;
            }
        }

        private Boolean MoveColumnDown_CanExecute()
        {
            if (this.SelectedAssignedColumnViews == null) return false;

            if (this.SelectedAssignedColumnViews.Count == 0)
            {
                MoveColumnDownCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_NoColumnSelected;
                return false;
            }

            //can only have one columns seleted.
            if (this.SelectedAssignedColumnViews.Count != 1)
            {
                MoveColumnDownCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_MultipleColumnsSelected;
                return false;
            }

            //cannot move down if last already
            Int32 maxNo = this.AssignedColumnViews.Max(c=> c.Number);
            if (this.SelectedAssignedColumnViews.Any(c => c.Number == maxNo))
            {
                MoveColumnDownCommand.DisabledReason = Message.ColumnLayoutEditor_DisabledReason_ColumnIsLastAlready;
                return false;
            }

            if (this.SelectedAssignedColumnViews.Any(s => s.IsLocked))
            {
                return false;
            }


            return true;
        }

        private void MoveColumnDown_Executed()
        {
            ExchangeSelectionOrder(1);
        }

        #endregion

        #region AddFilterCommand

        private RelayCommand _addFilterCommand;

        /// <summary>
        ///     Adds a new filter to the view
        /// </summary>
        public RelayCommand AddFilterCommand
        {
            get
            {
                if (_addFilterCommand != null) return _addFilterCommand;

                _addFilterCommand = new RelayCommand(
                    p => AddFilter_Executed(),
                    p => AddFilter_CanExecute())
                {
                    FriendlyName = Message.ColumnLayoutEditor_AddFilter,
                    FriendlyDescription = Message.ColumnLayoutEditor_AddFilter_Description,
                    SmallIcon = CommonImageResources.ColumnLayoutEditor_Filter_32
                };
                RegisterCommand(_addFilterCommand);

                return _addFilterCommand;
            }
        }


        private Boolean AddFilter_CanExecute()
        {
            return (SelectedNewFilterColumn != null & (SelectedNewFilterText.Length > 0));
        }

        private void AddFilter_Executed()
        {
            //Take copy of the selected new filter column
            DataSheetColumnView selectedFilterColumn = SelectedNewFilterColumn;

            //Remove from available list
            this.AvailableFilterByColumns.Remove(selectedFilterColumn);

            //Add to view filters
            this.CurrentLayout.FilterList.Add(
                CustomColumnFilter.NewCustomColumnFilter(SelectedNewFilterText,
                selectedFilterColumn.Path, selectedFilterColumn.DisplayName));


            //Clear text and select new column
            this.SelectedNewFilterText = String.Empty;
            this.SelectedNewFilterColumn = AvailableFilterByColumns.FirstOrDefault();
        }

        #endregion

        #region RemoveFilterCommand

        private RelayCommand _removeFilterCommand;

        /// <summary>
        ///     Removes an existing filter from a view
        /// </summary>
        public RelayCommand RemoveFilterCommand
        {
            get
            {
                if (_removeFilterCommand != null) return _removeFilterCommand;

                _removeFilterCommand = new RelayCommand(
                    p => RemoveFilter_Executed(),
                    p => RemoveFilter_CanExecute())
                {
                    FriendlyName = Message.ColumnLayoutEditor_RemoveFilter,
                    FriendlyDescription = Message.ColumnLayoutEditor_RemoveFilter_Description,
                    SmallIcon = CommonImageResources.Delete_16
                };
                RegisterCommand(_removeFilterCommand);

                return _removeFilterCommand;
            }
        }

        private Boolean RemoveFilter_CanExecute()
        {
            return (SelectedFiltersSelection != null);
        }

        private void RemoveFilter_Executed()
        {
            //Take copy of the selected filter
            CustomColumnFilter selectedFilter = SelectedFiltersSelection;

            //Remove filter
            this.CurrentLayout.FilterList.Remove(selectedFilter);

            //Get corresponding column
            var column = this.CurrentLayout.Columns.FirstOrDefault(o => o.Path == selectedFilter.Path);
            if (column == null) return;

            //Add back to available list
            DataSheetField colField = _masterFields.FirstOrDefault(f => f.Path == column.Path);
            if (colField == null) return;

            DataSheetColumnView colView = CreateColumnView(column, colField);
            this.AvailableFilterByColumns.Add(colView);
            this.SelectedNewFilterColumn = AvailableFilterByColumns.FirstOrDefault();
        }

        #endregion

        #region AddCalculatedColumnCommand

        private RelayCommand _addCalculatedColumnCommand;

        /// <summary>
        /// Launches the window to allow the user to add a new calculated column.
        /// </summary>
        public RelayCommand AddCalculatedColumnCommand
        {
            get
            {
                if (_addCalculatedColumnCommand == null)
                {
                    _addCalculatedColumnCommand = new RelayCommand(
                        p => AddCalculatedColumn_Executed())
                        {
                            FriendlyName = Message.DataSheetEditor_AddCalculatedColumn,
                            SmallIcon = CommonImageResources.Formula_16,
                        };
                    RegisterCommand(_addCalculatedColumnCommand);
                }
                return _addCalculatedColumnCommand;
            }
        }

        private void AddCalculatedColumn_Executed()
        {
            //Show the field selector window.
            Planogram activePlanogram =
                (App.ViewState.ActivePlanogramView.Model != null) ?
                App.ViewState.ActivePlanogramView.Model.Model : null;

            //Show the field selector window
            FieldSelectorViewModel fieldVm = new FieldSelectorViewModel(
                FieldSelectorInputType.Formula, FieldSelectorResolveType.SingleValue, String.Empty,
                CommonHelper.GetAllPlanogramFieldSelectorGroups(activePlanogram));

            GetWindowService().ShowDialog<FieldSelectorWindow>(fieldVm);
            if (fieldVm.DialogResult != true) return;

            String path = fieldVm.FieldText;

            //first check if the selected patch matches up to an available column
            // if it does then add it normally.
            DataSheetField existingField = _masterFields.FirstOrDefault(f => f.Path == path);
            if (existingField != null)
            {
                AddColumns(new List<DataSheetField> { existingField });
                return;
            }


            //Determine the next header number
            Int32 calcNo = 0;
            foreach (String existing in this.AssignedColumnViews.Select(c => c.DisplayName))
            {
                Match m = Regex.Match(existing, Message.DataSheetEditor_CalculatedColumnHeader + " [0-9+]");
                if (!m.Success) continue;

                Int32 number = Int32.Parse(m.Value.Replace(Message.DataSheetEditor_CalculatedColumnHeader + " ", null));
                if (number > calcNo) calcNo = number;
            }
            calcNo++;

            //Add the column
            CustomColumn column = CustomColumn.NewCustomColumn(path, 0);
            column.DisplayName = Message.DataSheetEditor_CalculatedColumnHeader + " " + calcNo;
            this.CurrentLayout.Columns.Add(column);

            DataSheetColumnView colView = _assignedColumnViews.FirstOrDefault(c => c.Column == column);

            this.AvailableFilterByColumns.Add(colView);
            this.AvailableGroupSortByColumns.Add(colView);

            AdjustNewColumnsOrder(new List<DataSheetColumnView>{colView});
            RefreshProperties();
        }

        #endregion

        #region EditCalculatedColumnCommand

        private RelayCommand _editCalculatedColumnCommand;

        public RelayCommand EditCalculatedColumnCommand
        {
            get
            {
                if (_editCalculatedColumnCommand == null)
                {
                    _editCalculatedColumnCommand =
                        new RelayCommand(
                            p => EditCalculatedColumn_Executed(p as DataSheetColumnView),
                            p => EditCalculatedColumn_CanExecute(p as DataSheetColumnView))
                        {
                            FriendlyName = "...",
                            FriendlyDescription = CommonMessage.ColumnLayoutEditor_EditCalculatedColumn_Desc
                        };
                    RegisterCommand(_editCalculatedColumnCommand);
                }
                return _editCalculatedColumnCommand;
            }
        }

        private Boolean EditCalculatedColumn_CanExecute(DataSheetColumnView colView)
        {
            if (colView == null || !colView.IsCalculated)
            {
                return false;
            }
            return true;
        }

        private void EditCalculatedColumn_Executed(DataSheetColumnView colView)
        {
            //Create the field groups
            List<FieldSelectorGroup> fieldGroups = new List<FieldSelectorGroup>(this.AvailableFieldGroups.Count);
            foreach (var group in _masterFields.GroupBy(f => f.GroupName))
            {
                fieldGroups.Add(new FieldSelectorGroup(group.Key, group.Select(f => f.FieldInfo)));
            }

            //Show the field selector window
            FieldSelectorViewModel fieldVm = new FieldSelectorViewModel(
                FieldSelectorInputType.Formula, FieldSelectorResolveType.SingleValue, colView.Path, fieldGroups);
            GetWindowService().ShowDialog<FieldSelectorWindow>(fieldVm);
            if (fieldVm.DialogResult != true) return;

            colView.Path = fieldVm.FieldText;
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Adds new columns for the given fields.
        /// </summary>
        /// <param name="columnsToAdd"></param>
        private void AddColumns(IEnumerable<DataSheetField> columnsToAdd)
        {
            //remove from the available list
            _selectedGroupFields.RemoveRange(columnsToAdd.Where(c=> c.FieldInfo != null).ToList());

            //add them to the selected columns 
            List<DataSheetColumnView> newColumns = new List<DataSheetColumnView>();
            foreach (DataSheetField field in columnsToAdd)
            {
                if (field.Path == null) continue;

                CustomColumn column = CustomColumn.NewCustomColumn(field.Path, 0);
                if (field.FieldInfo != null && field.FieldInfo.IsNumeric) column.TotalType = CustomColumnTotalType.Sum;
                this.CurrentLayout.Columns.Add(column);

                DataSheetColumnView colView = _assignedColumnViews.FirstOrDefault(c => c.Column == column);
                newColumns.Add(colView);

                this.AvailableFilterByColumns.Add(colView);
                this.AvailableGroupSortByColumns.Add(colView);
            }


            AdjustNewColumnsOrder(newColumns);
            RefreshProperties();
        }

        /// <summary>
        /// Removes the given columns
        /// </summary>
        private void RemoveColumns(IEnumerable<DataSheetColumnView> items)
        {
            AvailableGroupSortByColumns.CollectionChanged -= AvailableGroupSortByColumns_CollectionChanged;

            foreach (var col in items.ToList())
            {
                this.CurrentLayout.Columns.Remove(col.Column);

                AvailableFilterByColumns.Remove(AvailableFilterByColumns.FirstOrDefault(o => o.Path == col.Path));
                SelectedFilters.Remove(SelectedFilters.FirstOrDefault(o => o.Path == col.Path));
                AvailableGroupSortByColumns.Remove(AvailableGroupSortByColumns.FirstOrDefault(o => o == col));
            }
            AvailableGroupSortByColumns.CollectionChanged += AvailableGroupSortByColumns_CollectionChanged;


            OnSelectedFieldGroupChanged(this.SelectedFieldGroup);
            RefreshProperties();

            //CloneCustomColumns(SelectedColumns, this.AssignedColumnViews, ColumnLayoutCopy.Type, MasterFieldLookup);
            AdjustNewColumnsOrder(new List<DataSheetColumnView>());
        }

        /// <summary>
        /// Creates a new view for the given CustomColumn
        /// </summary>
        private DataSheetColumnView CreateColumnView(CustomColumn col, DataSheetField field = null)
        {
            if (field == null) field = _masterFields.FirstOrDefault(f => f.Path == col.Path);


            DataSheetColumnView colView = 
                new DataSheetColumnView(col,
                (field != null) ? field.Header : Message.DataSheetEditor_CalculatedColumnHeader,
                (field != null) ? field.GroupName : null);

            colView.IsCalculated = (field == null);

            //check if this is a key column.
            //determine what data level the rows should be created as 
            if (this.CurrentLayout != null && this.AvailablePlanograms.Count(p=> p.IsSelected)>1)
            {
                List<ObjectFieldInfo> dataLevelCheckFields =
                     this.CurrentLayout.Columns.Where(c => !ObjectFieldExpression.IsAggregatedOutput(c.Path)).SelectMany(c =>
                         ObjectFieldInfo.ExtractFieldsFromText(c.Path, PlanogramFieldHelper.EnumerateAllFields())).ToList();
                PlanogramItemType dataLevel = PlanogramFieldHelper.GetFieldDataLevel(dataLevelCheckFields);

                colView.IsLocked = (col.Path == ReportDocument.GetKeyField(PlanItemHelper.ToPlanItemType(dataLevel)));
            }


            return colView;
        }

        /// <summary>
        ///     Method to cascade through lower property and clear them as necessary
        /// </summary>
        /// <param name="property"></param>
        private void CascadePropertyClear(PropertyPath property)
        {
            if (property.Path == GroupByLevelOneProperty.Path)
            {
                //if group one is empty, clear group two
                if (_groupByLevelOne == CommonMessage.ColumnLayoutEditor_EmptyGroupSelection)
                {
                    GroupByLevelTwo = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                }
                //if a lower group has the same value, clear the lower group
                else if (_groupByLevelTwo == _groupByLevelOne)
                {
                    GroupByLevelTwo = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                }
                else if (_groupByLevelThree == _groupByLevelOne)
                {
                    GroupByLevelThree = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                }
                else if (_groupByLevelFour == _groupByLevelOne)
                {
                    GroupByLevelFour = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                }
            }
            if (property.Path == GroupByLevelTwoProperty.Path)
            {
                //if group two is empty, clear group three
                if (_groupByLevelTwo == CommonMessage.ColumnLayoutEditor_EmptyGroupSelection)
                {
                    GroupByLevelThree = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                }
                //if a lower group has the same value, clear the lower group
                else if (_groupByLevelThree == _groupByLevelTwo)
                {
                    GroupByLevelThree = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                }
                else if (_groupByLevelFour == _groupByLevelTwo)
                {
                    GroupByLevelFour = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                }
            }
            if (property.Path == GroupByLevelThreeProperty.Path)
            {
                //if group three is empty, clear group four
                if (_groupByLevelThree == CommonMessage.ColumnLayoutEditor_EmptyGroupSelection)
                {
                    GroupByLevelFour = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                }
                //if group four holds the same value, clear group four
                else if (_groupByLevelFour == _groupByLevelThree)
                {
                    GroupByLevelFour = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                }
            }
            if (property.Path == SortByLevelOneProperty.Path)
            {
                //if sort one is empty, clear sort two
                if (_sortByLevelOne == CommonMessage.ColumnLayoutEditor_EmptyGroupSelection)
                {
                    SortByLevelTwo = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                }
                //if a lower sort has the same value, clear the lower sort
                else if (_sortByLevelTwo == _sortByLevelOne)
                {
                    SortByLevelTwo = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                }
                else if (_sortByLevelThree == _sortByLevelOne)
                {
                    SortByLevelThree = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                }
                else if (_sortByLevelFour == _sortByLevelOne)
                {
                    SortByLevelFour = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                }
            }
            if (property.Path == SortByLevelTwoProperty.Path)
            {
                //if sort two is empty, clear sort three
                if (_sortByLevelTwo == CommonMessage.ColumnLayoutEditor_EmptyGroupSelection)
                {
                    SortByLevelThree = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                }
                //if a lower sort has the same value, clear the lower sort
                else if (_sortByLevelThree == _sortByLevelTwo)
                {
                    SortByLevelThree = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                }
                else if (_sortByLevelFour == _sortByLevelTwo)
                {
                    SortByLevelFour = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                }
            }
            if (property.Path == SortByLevelThreeProperty.Path)
            {
                //if sort three is empty, clear sort four
                if (_sortByLevelThree == CommonMessage.ColumnLayoutEditor_EmptyGroupSelection)
                {
                    SortByLevelFour = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                }
                //if sort four has the same value, clear sort four
                else if (_sortByLevelFour == _sortByLevelThree)
                {
                    SortByLevelFour = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;
                }
            }
        }

        /// <summary>
        ///     Finds the <see cref="CustomColumn" /> with the provided <paramref name="path" /> and selects it.
        /// </summary>
        /// <param name="path">The path of the <see cref="CustomColumn" /> to locate in the <see cref="SelectedColumns" />.</param>
        private void SelectColumnByPath(String path)
        {
            var match = this.AssignedColumnViews.FirstOrDefault(p => p.Path == path);
            if (match == null) return;
            this.SelectedAssignedColumnViews.Clear();
            this.SelectedAssignedColumnViews.Add(match);
        }


        #region Filtering

        /// <summary>
        ///     Adds, ordered by Header, the columns in the <see cref="ColumnLayoutCopy" /> as FilterBy columns.
        /// </summary>
        /// <param name="selectedFilters">Collection containing any already selected filter by columns.</param>
        private void AddAvailableFilterByColumns(IEnumerable<CustomColumnFilter> selectedFilters)
        {
            AvailableFilterByColumns.Clear();

            foreach (var col in this.CurrentLayout.Columns
                .OrderBy(o => o.Number)
                .Where(col => selectedFilters.All(o => o.Path != col.Path)))
            {
                DataSheetColumnView view = CreateColumnView(col);
                this.AvailableFilterByColumns.Add(view);
            }

            SelectedNewFilterColumn = AvailableFilterByColumns.FirstOrDefault();
        }

        /// <summary>
        ///     Method to filter the AvailableGroupSortByColumns based upon previous levels
        /// </summary>
        /// <param name="property">property to indicate current level</param>
        /// <returns>filtered ObservableCollection</returns>
        private ReadOnlyCollection<DataSheetColumnView> GetFilteredCollection(PropertyPath property)
        {
            String empty = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;

            //group by two
            if (property.Path == AvailableGroupByColumnsTwoProperty.Path)
            {
                if (this.GroupByLevelOne != empty)
                {
                    //return the collection where the item is not group one
                    return this.AvailableGroupSortByColumns.Where(r => r.Path != GroupByLevelOne)
                        .ToList().AsReadOnly();
                }
            }

            //group by three
            else if (property.Path == AvailableGroupByColumnsThreeProperty.Path)
            {
                //if group one is not empty & group two is not empty
                if (this.GroupByLevelOne != empty && this.GroupByLevelTwo != empty)
                {
                    //return the collection where the item is not group one or two
                    return AvailableGroupSortByColumns.Where(
                        r => r.Path != GroupByLevelOne && r.Path != GroupByLevelTwo)
                        .ToList().AsReadOnly();
                }
            }

            //group by four
            else if (property.Path == AvailableGroupByColumnsFourProperty.Path)
            {
                //if group one is not empty & group two is not empty & group three is not empty
                if (this.GroupByLevelOne != empty 
                    && this.GroupByLevelTwo != empty 
                    && this.GroupByLevelThree != empty)
                {
                    //return the collection where the item is not group one or two or three
                    return AvailableGroupSortByColumns.Where(r => 
                        r.Path != GroupByLevelOne && r.Path != GroupByLevelTwo && r.Path != GroupByLevelThree)
                        .ToList().AsReadOnly();
                }
            }

            //sort by two
            else if (property.Path == AvailableSortByColumnsTwoProperty.Path)
            {
                //if sort one is not empty
                if (this.SortByLevelOne != empty)
                {
                    //return the collection where the item is not group one
                    return AvailableGroupSortByColumns.Where(r => r.Path != SortByLevelOne)
                        .ToList().AsReadOnly();
                }
            }

            //sort by three
            else if (property.Path == AvailableSortByColumnsThreeProperty.Path)
            {
                //if sort one is not empty & sort two is not empty
                if (this.SortByLevelOne != empty &&
                    this.SortByLevelTwo != empty)
                {
                    //return the collection where the item is not group one or two
                    return AvailableGroupSortByColumns.Where(r => 
                        r.Path != SortByLevelOne && r.Path != SortByLevelTwo)
                        .ToList().AsReadOnly();
                }
            }

            //sort by four
            else if (property.Path == AvailableSortByColumnsFourProperty.Path)
            {
                //if sort one is not empty & sort two is not empty & sort three is not empty
                if (this.SortByLevelOne != empty &&
                    this.SortByLevelTwo != empty &&
                    this.SortByLevelThree != empty)
                {
                    //return the collection where the item is not group one or two or three
                    return AvailableGroupSortByColumns.Where(r => 
                        r.Path != SortByLevelOne && r.Path != SortByLevelTwo && r.Path != SortByLevelThree)
                        .ToList().AsReadOnly();
                }
            }

            //if at this point there hasn't been a list returned, just return the default one so there isn't an empty combo box.
            return this.AvailableGroupSortByColumns.ToList().AsReadOnly();
        }


        #endregion

        #region Grouping

        /// <summary>
        ///     Adds the <see cref="CustomColumnGroup" /> items according to the selected groups.
        /// </summary>
        /// <param name="columnGroupList">The collection of <see cref="CustomColumnGroup" /> items to be built.</param>
        private void UpdateGroupByList()
        {
            CustomColumnGroupList columnGroupList = this.CurrentLayout.GroupList;
            String empty = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;

            //get the expected levels
            List<CustomColumnGroup> list = new List<CustomColumnGroup>();
            if (this.GroupByLevelOne != empty)
            {
                list.Add(CustomColumnGroup.NewCustomColumnGroup(GroupByLevelOne)); // Add the first grouping selection.

                if (this.GroupByLevelTwo != empty)
                {
                    list.Add(CustomColumnGroup.NewCustomColumnGroup(GroupByLevelTwo));

                    // Add the second grouping selection.
                    if (this.GroupByLevelThree != empty)
                    {
                        list.Add(CustomColumnGroup.NewCustomColumnGroup(GroupByLevelThree));

                        // Add the third grouping selection.
                        if (this.GroupByLevelFour != empty)
                        {
                            list.Add(CustomColumnGroup.NewCustomColumnGroup(GroupByLevelFour));
                            // Add the fourth grouping selection.
                        }
                    }
                }
            }

            //check fi we need to update the main list.
            Boolean needsUpdate = (columnGroupList.Count != list.Count);
            if(!needsUpdate && list.Any())
            {
                //compare items
                for (Int32 i = 0; i < list.Count; i++)
                {
                    if (columnGroupList[i].Grouping != list[i].Grouping)
                    {
                        needsUpdate = true;
                        break;
                    }
                }
            }

            //return out if we don't need to do anything.
            if (!needsUpdate) return;


            //clear the list and add the new items.
            if (columnGroupList.Any()) columnGroupList.Clear();
            columnGroupList.AddList(list); 
            



            //columnGroupList.Clear();

            //String empty = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;

            //if (this.GroupByLevelOne != empty)
            //{
            //    List<CustomColumnGroup> list = new List<CustomColumnGroup>();
            //    list.Add(CustomColumnGroup.NewCustomColumnGroup(GroupByLevelOne)); // Add the first grouping selection.

            //    if (this.GroupByLevelTwo != empty)
            //    {
            //        list.Add(CustomColumnGroup.NewCustomColumnGroup(GroupByLevelTwo));
                    
            //        // Add the second grouping selection.
            //        if (this.GroupByLevelThree != empty)
            //        {
            //            list.Add(CustomColumnGroup.NewCustomColumnGroup(GroupByLevelThree));
                        
            //            // Add the third grouping selection.
            //            if (this.GroupByLevelFour != empty)
            //            {
            //                list.Add(CustomColumnGroup.NewCustomColumnGroup(GroupByLevelFour));
            //                // Add the fourth grouping selection.
            //            }
            //        }
            //    }

            //    columnGroupList.AddList(list); // Update the column grouping list with the selections.
            //}
        }

        /// <summary>
        ///     Assign the selected column groupings to the properties bound on the UI.
        /// </summary>
        /// <param name="groups">Collection of already selected column groupings.</param>
        private void SetupGroupByProperties(IList<CustomColumnGroup> groups)
        {
            GroupByLevelOne = groups.Count > 0 ? groups[0].Grouping : null;
            GroupByLevelTwo = groups.Count > 1 ? groups[1].Grouping : null;
            GroupByLevelThree = groups.Count > 2 ? groups[2].Grouping : null;
            GroupByLevelFour = groups.Count > 3 ? groups[3].Grouping : null;
        }

        #endregion

        #region Sorting

        /// <summary>
        ///     Adds, ordered by Header, the available columns in the SelectedLayoutCopy.
        /// </summary>
        private void AddAvailableGroupSortByColumns()
        {
            if(this.AvailableGroupSortByColumns.Any()) this.AvailableGroupSortByColumns.Clear();

            //Add the blank selection item.
            this.AvailableGroupSortByColumns.Add(DataSheetColumnView.NewBlankView());

            //cycle through adding selected columns.
            foreach (CustomColumn column in this.CurrentLayout.Columns.OrderBy(o => o.Number))
            {
                DataSheetColumnView colView =
                    this.AssignedColumnViews.FirstOrDefault(c => c.Column.Path == column.Path);
                if (colView == null) colView = CreateColumnView(column);

                this.AvailableGroupSortByColumns.Add(colView);
            }
        }

        /// <summary>
        ///     Casts <paramref name="value" /> from a <see cref="Boolean" /> to a <see cref="ListSortDirection" /> value.
        /// </summary>
        /// <param name="value">The value to be cast into a type of <see cref="ListSortDirection" />.</param>
        /// <returns>
        ///     Returns the appropriate <see cref="ListSortDirection" /> based on <paramref name="value" /> (<c>Ascending</c>
        ///     if <c>True</c>, <c>Descending</c> if <c>False</c>.
        /// </returns>
        private static ListSortDirection CastToListSortDirection(Boolean value)
        {
            return value
                ? ListSortDirection.Ascending
                : ListSortDirection.Descending;
        }

        /// <summary>
        ///     Assign the selected column sortings to the properties bound on the UI.
        /// </summary>
        /// <param name="sorts">Collection of already selected column sortings.</param>
        private void SetupSortByProperties(IList<CustomColumnSort> sorts)
        {
            this.SortByLevelOne = sorts.Count > 0 ? sorts[0].Path : null;
            this.SortByLevelOneAscending = sorts.Count > 0 && sorts[0].Direction == ListSortDirection.Ascending;
            this.SortByLevelOneDescending = !SortByLevelOneAscending;
            this.SortByLevelTwo = sorts.Count > 1 ? sorts[1].Path : null;
            this.SortByLevelTwoAscending = sorts.Count > 1 && sorts[1].Direction == ListSortDirection.Ascending;
            this.SortByLevelTwoDescending = !SortByLevelTwoAscending;
            this.SortByLevelThree = sorts.Count > 2 ? sorts[2].Path : null;
            this.SortByLevelThreeAscending = sorts.Count > 2 && sorts[2].Direction == ListSortDirection.Ascending;
            this.SortByLevelThreeDescending = !SortByLevelThreeAscending;
            this.SortByLevelFour = sorts.Count > 3 ? sorts[3].Path : null;
            this.SortByLevelFourAscending = sorts.Count > 3 && sorts[3].Direction == ListSortDirection.Ascending;
            this.SortByLevelFourDescending = !SortByLevelFourAscending;
        }

        /// <summary>
        ///     Adds the <see cref="CustomColumnSort" /> items according to the selected filters.
        /// </summary>
        /// <param name="columnSortList">The collection of <see cref="CustomColumnSort" /> items to be built.</param>
        private void BuildSortByList(CustomColumnSortList columnSortList)
        {
            columnSortList.Clear(); // Initialize the sort selection list.

            String empty = CommonMessage.ColumnLayoutEditor_EmptyGroupSelection;

            if (this.SortByLevelOne != empty)
            {
                List<CustomColumnSort> list = new List<CustomColumnSort>();

                list.Add(CustomColumnSort.NewCustomColumnSort(SortByLevelOne,
                    CastToListSortDirection(SortByLevelOneAscending))); // Add the first sort selection.

                if (this.SortByLevelTwo != empty)
                {
                    list.Add(CustomColumnSort.NewCustomColumnSort(SortByLevelTwo,
                        CastToListSortDirection(SortByLevelTwoAscending))); // Add the second sort selection.

                    if (this.SortByLevelThree != empty)
                    {
                        list.Add(CustomColumnSort.NewCustomColumnSort(SortByLevelThree,
                            CastToListSortDirection(SortByLevelThreeAscending))); // Add the third sort selection.

                        if (this.SortByLevelFour != empty)
                        {
                            list.Add(CustomColumnSort.NewCustomColumnSort(SortByLevelFour,
                                CastToListSortDirection(SortByLevelFourAscending))); // Add the fourth sort selection.
                        }
                    }
                }

                columnSortList.AddList(list); // Update the column sorting list with the selections.
            }
        }

        #endregion

        #region Column Ordering

        /// <summary>
        ///     Moves the selected column (or group columns if a group is selected) the provided offser rows.
        /// </summary>
        /// <param name="offset">Number of positions to move the selection.</param>
        /// <remarks>
        ///     Negative offsets move the selection up, possitive offsets move the selection down. If the target position is not
        ///     valid there will be no effect.
        /// </remarks>
        private void ExchangeSelectionOrder(Int32 offset)
        {
            var source = this.SelectedAssignedColumnViews.FirstOrDefault();
            if (source == null) return;


            ExchangeOrder(source, offset);

            SelectColumnByPath(source.Path);

            OnColumnOrderChanged();
        }

        /// <summary>
        ///  Swaps column order within the group.
        /// </summary>
        /// <param name="source">The <see cref="CustomColumn" /> that will be moved.</param>
        /// <param name="offset">Offset to the desired position.</param>
        /// <param name="itemCollection">The collection containing the items to be exchanged.</param>
        /// <remarks>
        ///     Negative offsets move the column up, possitive offsets move the column down. If the target position is not
        ///     valid there will be no effect.
        /// </remarks>
        private void ExchangeOrder(DataSheetColumnView source, Int32 offset)
        {
            if (source == null || offset == 0) return; // Nothing to exchange.

            Int32 targetOrder = source.Number + offset;
            DataSheetColumnView target = 
                this.AssignedColumnViews.FirstOrDefault(o => o.Number == targetOrder);

            if (target != null)
            {
                target.Number = source.Number;
                source.Number = targetOrder;
            }
        }

        /// <summary>
        ///     Adjusts the order number of each <see cref="CustomColumn" /> from <paramref name="newItems" /> so that they
        ///     can be appended at the end of the <paramref name="itemCollection" />.
        /// </summary>
        /// <param name="newItems">
        ///     The collection of <see cref="CustomColumn" /> that need reordering before appending to
        ///     <paramref name="itemCollection" />.
        /// </param>
        /// <param name="itemCollection">
        ///     The collection of already selected <see cref="CustomColumn" /> which order will be
        ///     preserved.
        /// </param>
        /// <remarks>Each group header's <see cref="CustomColumn" /> collection has its own order within the group.</remarks>
        private void AdjustNewColumnsOrder(List<DataSheetColumnView> newItems)
        {
            if (this.AssignedColumnViews.Count == 0) return;

            CustomColumnLayout curLayout = this.CurrentLayout;
            List<String> newPaths = newItems.Select(c => c.Path).ToList();

            //order columns.
            List<DataSheetColumnView> orderedCols =
                this.AssignedColumnViews.Except(newItems).OrderBy(c => c.Number).Union(newItems).ToList();
            
            //unlock all
            foreach(DataSheetColumnView col in orderedCols) col.IsLocked = false;

            if(curLayout != null && this.AvailablePlanograms.Count(p => p.IsSelected) > 1)
            {
                List<ObjectFieldInfo> dataLevelCheckFields =
                         this.CurrentLayout.Columns.Where(c => !ObjectFieldExpression.IsAggregatedOutput(c.Path)).SelectMany(c =>
                             ObjectFieldInfo.ExtractFieldsFromText(c.Path, PlanogramFieldHelper.EnumerateAllFields())).ToList();
                    PlanogramItemType dataLevel = PlanogramFieldHelper.GetFieldDataLevel(dataLevelCheckFields);


                List<String> keyColumnPaths =
                    /*if hasAggregatedRows*/(curLayout.IsGroupDetailHidden && curLayout.GroupList.Any())?
                    /*use groups as keys*/curLayout.GroupList.Select(g => g.Grouping).ToList()
                    /*else use item key field*/: new List<String> { ReportDocument.GetKeyField(PlanItemHelper.ToPlanItemType(dataLevel)) };

                //Move key columns to the front and set them to locked.
                keyColumnPaths.Reverse();
                foreach (String keyPath in keyColumnPaths)
                {
                    DataSheetColumnView col = orderedCols.FirstOrDefault(c => c.Path == keyPath);
                    if (col == null) continue;
                    col.IsLocked = true;
                    
                    orderedCols.Remove(col);
                    orderedCols.Insert(0, col);
                }
            }

            //update all column numbers.
            Int32 colNo = 1;
            foreach (DataSheetColumnView col in orderedCols)
            {
                col.Number = colNo;
                colNo++;
            }


            OnColumnOrderChanged();
        }

        private void RefreshColumnsOrder()
        {
            AdjustNewColumnsOrder(new List<DataSheetColumnView>());
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the current layout changes.
        /// </summary>
        private void OnCurrentLayoutChanged(CustomColumnLayout oldValue, CustomColumnLayout newValue)
        {
            if (oldValue != null)
            {
                oldValue.PropertyChanged -= CurrentLayout_PropertyChanged;
                oldValue.Columns.BulkCollectionChanged -= CurrentLayout_ColumnsBulkCollectionChanged;
            }


            if (newValue != null)
            {
                //update the column numbers
                Int32 colNumber = 1;
                foreach (CustomColumn col in newValue.Columns.OrderBy(g => g.Number).ToList())
                {
                    col.Number = colNumber++;
                }


                newValue.PropertyChanged += CurrentLayout_PropertyChanged;
                CurrentLayout_PropertyChanged(newValue, new PropertyChangedEventArgs(CustomColumnLayout.IncludeAllPlanogramsProperty.Name));

                CurrentLayout_ColumnsBulkCollectionChanged(newValue, new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));
                newValue.Columns.BulkCollectionChanged += CurrentLayout_ColumnsBulkCollectionChanged;

                //from OnSelectedLayoutChanged
                AvailableFilterByColumns.CollectionChanged -= AvailableFilterByColumns_CollectionChanged;
                AvailableGroupSortByColumns.CollectionChanged -= AvailableGroupSortByColumns_CollectionChanged;

                AddAvailableGroupSortByColumns();
                AddAvailableFilterByColumns(newValue.FilterList);
                SetupGroupByProperties(newValue.GroupList);
                SetupSortByProperties(newValue.SortList);

                //subscribe to collection change events
                AvailableFilterByColumns.CollectionChanged += AvailableFilterByColumns_CollectionChanged;
                AvailableGroupSortByColumns.CollectionChanged += AvailableGroupSortByColumns_CollectionChanged;

                RefreshColumnsOrder();
            }

        }

        /// <summary>
        /// Called whenever a property changes on the current layout.
        /// </summary>
        private void CurrentLayout_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == CustomColumnLayout.IncludeAllPlanogramsProperty.Name)
            {
                if (_suppressPlanPropertyChange) return;

                //update the planogram selections
                _suppressPlanPropertyChange = true;
                foreach (DataSheetPlanogramRow row in this.AvailablePlanograms)
                {
                    row.IsSelected = (this.CurrentLayout.IncludeAllPlanograms || row.IsCurrentPlanogram);
                }
                _suppressPlanPropertyChange = false;

                RefreshColumnsOrder();
            }
            else if (e.PropertyName == CustomColumnLayout.IsGroupDetailHiddenProperty.Name)
            {
                RefreshColumnsOrder();
            }
        }

        /// <summary>
        /// Called whenever the columns collection on the current layout changes.
        /// </summary>
        private void CurrentLayout_ColumnsBulkCollectionChanged(object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (CustomColumn col in e.ChangedItems)
                    {
                        _assignedColumnViews.Add(CreateColumnView(col));
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (CustomColumn col in e.ChangedItems)
                    {
                        DataSheetColumnView colView =
                            _assignedColumnViews.FirstOrDefault(v => v.Column == col);
                        _assignedColumnViews.Remove(colView);
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    _assignedColumnViews.Clear();
                    foreach (CustomColumn col in this.CurrentLayout.Columns)
                    {
                        _assignedColumnViews.Add(CreateColumnView(col));
                    }
                    break;
            }
            OnColumnOrderChanged();
        }

        /// <summary>
        /// Called when the selected item type property value changes.
        /// </summary>
        private void OnSelectedFieldGroupChanged(DataSheetFieldGroup newValue)
        {
            _selectedGroupFields.Clear();

            List<String> takenPaths = this.CurrentLayout.Columns.Select(c => c.Path).ToList();


            _selectedGroupFields.AddRange(
                     _masterFields
                     .Where(f => newValue.IsAllGroup || newValue.GroupName == f.GroupName)
                     .Except(_masterFields.Where(f => takenPaths.Contains(f.Path))));

        }

        /// <summary>
        ///     Event handler for the available filter columns changing
        /// </summary>
        private void AvailableFilterByColumns_CollectionChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            AvailableFilterByColumns.CollectionChanged -= AvailableFilterByColumns_CollectionChanged;

            //Reorder collection
            AvailableFilterByColumns = AvailableFilterByColumns.OrderBy(o => o.Number).ToObservableCollection();
            //Reselect first
            SelectedNewFilterColumn = AvailableFilterByColumns.FirstOrDefault();

            AvailableFilterByColumns.CollectionChanged += AvailableFilterByColumns_CollectionChanged;
        }

        /// <summary>
        ///     Event handler for the available group/sort by columns changing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AvailableGroupSortByColumns_CollectionChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            AvailableGroupSortByColumns.CollectionChanged -= AvailableGroupSortByColumns_CollectionChanged;

            //Reorder collection
            AvailableGroupSortByColumns = AvailableGroupSortByColumns.ToObservableCollection();

            AvailableGroupSortByColumns.CollectionChanged += AvailableGroupSortByColumns_CollectionChanged;

            RefreshColumnsOrder();
        }

        /// <summary>
        /// Called whenever the items in the AvailablePlanograms collection change.
        /// </summary>
        private void AvailablePlanograms_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (DataSheetPlanogramRow r in e.ChangedItems)
                    {
                        r.PropertyChanged += DataSheetPlanogramRow_PropertyChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (DataSheetPlanogramRow r in e.ChangedItems)
                    {
                        r.PropertyChanged -= DataSheetPlanogramRow_PropertyChanged;
                    }
                      break;

                case NotifyCollectionChangedAction.Reset:
                      foreach (DataSheetPlanogramRow r in e.ChangedItems)
                      {
                          r.PropertyChanged -= DataSheetPlanogramRow_PropertyChanged;
                      }
                    break;
            }
        }

        /// <summary>
        /// Called whenever a datasheet planogram row property changes
        /// </summary>
        private void DataSheetPlanogramRow_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsSelected")
            {
                if (_suppressPlanPropertyChange) return;

                _suppressPlanPropertyChange = true;
                this.CurrentLayout.IncludeAllPlanograms = this.AvailablePlanograms.All(p => p.IsSelected);
                _suppressPlanPropertyChange = false;
            }
        }

        #endregion

        #region Events

        public event EventHandler ColumnOrderChanged;

        private void OnColumnOrderChanged()
        {
            if (ColumnOrderChanged != null)
            {
                ColumnOrderChanged(this, EventArgs.Empty);
            }
        }

        #endregion

        #region IDisposable Support

        protected override void Dispose(Boolean disposing)
        {
            if (IsDisposed) return;

            if (disposing)
            {
                this.CurrentLayout = null;

                foreach (DataSheetPlanogramRow row in this.AvailablePlanograms)
                {
                    row.PropertyChanged -= DataSheetPlanogramRow_PropertyChanged;
                }

                AvailableFilterByColumns.CollectionChanged -= AvailableFilterByColumns_CollectionChanged;
                AvailableGroupSortByColumns.CollectionChanged -= AvailableGroupSortByColumns_CollectionChanged;
                
            }
            IsDisposed = true;
        }

        #endregion
    }
}