﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 83)
// V8-31721 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Logging;
using System.Threading.Tasks;
using System.Threading;


namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Represent a single cell on the datasheet.
    /// </summary>
    public sealed class DataSheetCell : INotifyPropertyChanged
    {
        #region Fields
        private Boolean _isInitialized;
        private Object _lock = new object();
        private String _path;
        private Object _value;
        private readonly ReadOnlyCollection<IPlanItem> _source;
        private readonly Boolean _isReadOnly;
        private Type _valueType;
        private Boolean _isRefreshing;
        private CancellationTokenSource _asyncGetCancellation;
        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the cell value
        /// </summary>
        public Object Value
        {
            get
            {
                if (!_isInitialized)
                {
                    //first get needs to be done immediately 
                    _value = GetValue();
                    _isInitialized = true;
                }

                return _value;
            }
            set
            {
                Debug.Assert(!_isReadOnly, "Trying to set a readonly field");
                if (!_isReadOnly)
                {
                    //set the value for all items.
                    SetValue(value);
                }
            }
        }

        /// <summary>
        /// Returns the collection of source items for
        /// this cell.
        /// </summary>
        public ReadOnlyCollection<IPlanItem> Source
        {
            get { return _source; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="items"></param>
        /// <param name="isReadOnly"></param>
        public DataSheetCell(String path, Type valueType, IPlanItem item, Boolean isReadOnly)
            : this(path, valueType, new IPlanItem[] { item }, isReadOnly)
        {
        }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="items"></param>
        /// <param name="isReadOnly"></param>
        public DataSheetCell(String path, Type valueType, IEnumerable<IPlanItem> items, Boolean isReadOnly)
        {
            _path = path;
            _source = items.ToList().AsReadOnly();
            _isReadOnly = isReadOnly;
            _valueType = valueType;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Refreshes the value held by this cell.
        /// </summary>
        public void RefreshValueAsync()
        {
            if (_isRefreshing) return;

            //if we are unit testing then just
            // set the value immediately.
            if (App.IsUnitTesting)
            {
                _value = GetValue();
                OnPropertyChanged("Value");
                return;
            }


            //otherwise we should get the value asynchronously:

            _asyncGetCancellation = new CancellationTokenSource();
            CancellationToken ct = _asyncGetCancellation.Token;

            _isRefreshing = true;

            Task.Factory.StartNew<Object>(
                () =>
                {
                    //Get the value on the background thread:
                    if (ct.IsCancellationRequested) return null;
                    Object result = GetValue();
                    return result;
                },
                ct, TaskCreationOptions.PreferFairness, TaskScheduler.Default)
                .ContinueWith((t) =>
                    {
                        //update the value on the ui thread:
                        _value = t.Result;
                        _isRefreshing = false;
                        OnPropertyChanged("Value");

                        _asyncGetCancellation.Dispose();
                        _asyncGetCancellation = null;
                    },
                    ct,
                    TaskContinuationOptions.None,
                    TaskScheduler.FromCurrentSynchronizationContext());
        }

        /// <summary>
        /// Gets the value for this cell.
        /// </summary>
        private Object GetValue()
        {
            if (!_source.Any()) return null;

            ObjectFieldExpression expression = new ObjectFieldExpression(_path);

            expression.AddDynamicFieldParameters(PlanogramFieldHelper.EnumerateAllFields());


            expression.ResolveDynamicParameter += ResolveExpressionParameter;
            expression.GettingAggregationValues += AggregationValuesRequested;

            Object value = expression.Evaluate(null, _valueType);

            expression.ResolveDynamicParameter -= ResolveExpressionParameter;
            expression.GettingAggregationValues -= AggregationValuesRequested;

            return value;

        }

        private void ResolveExpressionParameter(object sender, ObjectFieldExpressionResolveParameterArgs e)
        {
            PlanogramItemPlacement resolveTarget = e.TargetContext as PlanogramItemPlacement;

            if (resolveTarget != null)
            {
                e.Result = PlanogramFieldHelper.ResolveField(e.Parameter.FieldInfo, resolveTarget);
                return;
            }
            else if (_source.Count == 1)
            {
                e.Result = PlanogramFieldHelper.ResolveField(e.Parameter.FieldInfo, _source[0].ToPlanogramItemPlacement());
                return;
            }
            else
            {
                //we have no target so we resolve for the whole group
                List<Object> values = new List<Object>(_source.Count);
                foreach (IPlanItem item in _source)
                {
                    values.Add(PlanogramFieldHelper.ResolveField(e.Parameter.FieldInfo, item.ToPlanogramItemPlacement()));
                }

                //return an aggregation
                if (values.Distinct().Count() == 1)
                {
                    e.Result = values[0];
                    return;
                }


                try
                {
                    Object result = null;

                    //sum it if numeric, otherwise return null.
                    if (_valueType == typeof(Single))
                    {
                        result = values.Where(v => v != null).Select(v => Convert.ToSingle(v)).Sum();
                    }
                    else if (_valueType == typeof(Single?))
                    {
                        result = values.Select
                            (v => (v!=null)? (Single?)Convert.ToSingle(v) : null)
                            .Sum();
                    }
                    else if (_valueType == typeof(Double))
                    {
                        result = values.Where(v => v != null).Select(v => Convert.ToDouble(v)).Sum();
                    }
                    else if (_valueType == typeof(Double?))
                    {
                        result = values.Select
                            (v => (v != null) ? (Double?)Convert.ToDouble(v) : null)
                            .Sum();
                    }
                    else if (_valueType == typeof(Int16) || _valueType == typeof(Int16?))
                    {
                        result = values.Where(v => v != null).Select(v => Convert.ToDouble(v)).Sum();
                    }
                    else if (_valueType == typeof(Int32))
                    {
                        result = values.Where(v => v != null).Select(v => Convert.ToInt32(v)).Sum();
                    }
                    else if (_valueType == typeof(Int32?))
                    {
                        result = values.Select
                            (v => (v != null) ? (Int32?)Convert.ToInt32(v) : null)
                            .Sum();
                    }
                    else if (_valueType == typeof(Byte) || _valueType == typeof(Byte?))
                    {
                        result = values.Where(v => v != null).Select(v => Convert.ToInt32(v)).Sum();
                    }

                    //Object result = values.Where(v => v != null).Select(v => Convert.ToDouble(v)).Sum();
                    //result = Convert.ChangeType(result, _valueType);

                    e.Result = result;
                }
                catch (Exception)
                {
                    e.Result = 0;
                }
                return;

            }
        }

        private void AggregationValuesRequested(object sender, ObjectFieldExpressionAggregationValuesArgs e)
        {
            if (String.IsNullOrWhiteSpace(e.Formula))
            {
                e.Values = _source.Select(s => s.ToPlanogramItemPlacement()).ToList();
                return;
            }

            //PlanogramItemPlacement target = e.ExpressionTarget as PlanogramItemPlacement;

            var fields = ObjectFieldInfo.ExtractFieldsFromText(e.Formula, PlanogramFieldHelper.EnumerateAllFields());

            //create the new formula
            ObjectFieldExpression expression = new ObjectFieldExpression(e.Formula);
            fields.ForEach(f => expression.AddParameter(f));

            expression.ResolveDynamicParameter += ResolveExpressionParameter;
            expression.GettingAggregationValues += AggregationValuesRequested;


            //determine the data level
            PlanogramItemType aggregationLevel = PlanogramFieldHelper.GetFieldDataLevel(fields);
            List<PlanogramItemPlacement> resolveItems = _source.SelectMany(p => p.ToPlanogramItemPlacement().GetPlanogramItemPlacementOfType(aggregationLevel)).ToList();

            //resolve values for each item
            List<Object> values = new List<object>(resolveItems.Count);
            foreach (PlanogramItemPlacement p in resolveItems)
            {
                values.Add(expression.Evaluate(p));
            }
            e.Values = values;

        }


        /// <summary>
        /// Resolves the path for the given item.
        /// </summary>
        public static Object ResolveItemValue(IPlanItem item, String path)
        {
            return PlanogramFieldHelper.ResolveFieldExpression(
                path,
                PlanogramFieldHelper.EnumerateAllFields(item.Planogram.Model),
                item.ToPlanogramItemPlacement());
        }

        /// <summary>
        /// Sets the value for all items on this cell.
        /// </summary>
        private Boolean SetValue(Object value)
        {
            IEnumerable<ObjectFieldInfo> fields =
                ObjectFieldInfo.ExtractFieldsFromText(_path, PlanogramFieldHelper.EnumerateAllFields());

            //if we have more than one field then this is not settable.
            if (!fields.Any() || fields.Count() > 1) return false;


            ObjectFieldInfo fieldInfo = fields.First();
            PlanItemType dataLevel = PlanItemHelper.ToPlanItemType(PlanogramFieldHelper.GetFieldDataLevel(fields));

            foreach (IPlanItem item in _source)
            {
                WpfHelper.SetItemPropertyValue(PlanItemHelper.GetItemValue(item, dataLevel), fieldInfo.PropertyName, value);
            }

            //force the value to be refetched?

            //set the value 
            _value = value;
            OnPropertyChanged("Value");

            return true;
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
