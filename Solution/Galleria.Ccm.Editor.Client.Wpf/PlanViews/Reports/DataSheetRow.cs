﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 83)
// V8-31721 : L.Ineson
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Represents a single row on the datasheet.
    /// </summary>
    public sealed class DataSheetRow
    {
        #region Fields
        private DataSheetCell[] _cells;
        #endregion

        #region Properties

        public DataSheetCell this[Int32 i]
        {
            get
            {
                if (i >= _cells.Length) return null;
                return _cells[i];
            }
            set { _cells[i] = value; }
        }

        #endregion

        #region Constructor

        public DataSheetRow(Int32 size)
        {
            _cells = new DataSheetCell[size];
        }

        #endregion

        #region Methods

        public void RefreshAllCellsAsync()
        {
            foreach (DataSheetCell c in _cells)
            {
                c.RefreshValueAsync();
            }
        }

        #endregion

    }
}
