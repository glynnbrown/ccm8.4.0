﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// V8-27938 : N.Haywood
//  Created
// V8-28199 : N.Haywood
//  Made apply save the datasheet
// V8-27513 : L.Ineson
//  Made sure grid preview mouse move ignores the event if it came from the scrollbar
// V8-28580 : A.Kuszyk
//  Fixed column grouping.
#endregion

#region Version History: (CCM 8.1.0)
// V8-29742 : L.Ineson
//  Fixed drag drop
#endregion

#region Version History: (CCM 8.1.1)
// V8-30183 : M.Shelley
//  Fixed the group expanders so they actually expand / contract the group details
// V8-30516 : M.Shelley
//  Added mouse double click behaviour to both the available columns and selected columns grids.
#endregion

#region Version History: (CCM 8.2.0)
//V8-30870 : L.Ineson
//  Desing change.
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Editor.Client.Wpf;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Wpf.ReportColumnLayouts
{
    public sealed partial class ReportColumnLayoutEditorOrganiser
    {
        #region Constants

        /// <summary>
        ///     The key to the Dynamic Resource pointing to the RemoveFilterCommand in the view model.
        /// </summary>
        /// <remarks>This is needed so that the command is properly binded to a button inside a datatemplate.</remarks>
        public const String RemoveFilterCommandKey = "RemoveFilterCommand";

        public const String EditCalculatedColumnCommandKey = "EditCalculatedColumnCommand";

        #endregion

        #region Properties

        #region ViewModel Property

        /// <summary>
        /// ViewModel dependency property definition.
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(ReportColumnLayoutEditorViewModel), typeof(ReportColumnLayoutEditorOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Called whenever the viewmodel dependency property value changes.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ReportColumnLayoutEditorOrganiser senderControl = (ReportColumnLayoutEditorOrganiser)obj;

            if (e.OldValue != null)
            {
                ReportColumnLayoutEditorViewModel oldModel = (ReportColumnLayoutEditorViewModel)e.OldValue;
                oldModel.UnregisterWindowControl();
                senderControl.Resources.Remove(RemoveFilterCommandKey);
                senderControl.Resources.Remove(EditCalculatedColumnCommandKey);

                oldModel.ColumnOrderChanged -= senderControl.ViewModel_ColumnOrderChanged;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                ReportColumnLayoutEditorViewModel newModel = (ReportColumnLayoutEditorViewModel)e.NewValue;
                newModel.RegisterWindowControl(senderControl);

                // Add the dynamic resource for the "in row" remove filter command.
                senderControl.Resources.Add(RemoveFilterCommandKey, newModel.RemoveFilterCommand);
                senderControl.Resources.Add(EditCalculatedColumnCommandKey, newModel.EditCalculatedColumnCommand);

                newModel.ColumnOrderChanged += senderControl.ViewModel_ColumnOrderChanged;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }
        }

        /// <summary>
        /// Gets the viewmodel context.
        /// </summary>
        public ReportColumnLayoutEditorViewModel ViewModel
        {
            get { return (ReportColumnLayoutEditorViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public ReportColumnLayoutEditorOrganiser(ReportColumnLayoutEditorViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //set the help file key
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.DataSheets);

            this.ViewModel = viewModel;

            this.Loaded += ReportColumnLayoutEditorOrganiser_Loaded;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out initial load actions.
        /// </summary>
        private void ReportColumnLayoutEditorOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ReportColumnLayoutEditorOrganiser_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        /// <summary>
        /// Called whenever a property changes on the viewmodel.
        /// </summary>
        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ReportColumnLayoutEditorViewModel.SelectedFieldGroupProperty.Path)
            {
                if (!String.IsNullOrWhiteSpace(this.FieldGrid.PrefilterText))
                {
                    //force the grid to refilter.
                    this.FieldGrid.BeginFilter();
                }
            }
        }

        /// <summary>
        /// Called whenever the viewmodel column order changed event is fired.
        /// </summary>
        private void ViewModel_ColumnOrderChanged(object sender, EventArgs e)
        {
            if (this.AssignedColumnsGrid == null) return;

            //forcibly reapply the column sorts.
            try
            {
                SortDescription[] sorts = this.AssignedColumnsGrid.SortByDescriptions.ToArray();
                if (sorts.Any())
                {
                    this.AssignedColumnsGrid.SortByDescriptions.Clear();
                    foreach (var s in sorts) this.AssignedColumnsGrid.SortByDescriptions.Add(s);
                }
                this.AssignedColumnsGrid.Items.Refresh();
            }
            catch (InvalidOperationException) { }
        }

        /// <summary>
        /// Called whenever the user double clicks on the field grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FieldGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (e.RowItem == null) return;
            if (this.ViewModel == null) return;
            this.ViewModel.AddSelectedColumnsCommand.Execute();
        }

        /// <summary>
        /// Called whenever rows are dropped from the assigned columns grid.
        /// </summary>
        private void FieldGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid != AssignedColumnsGrid) return;
            if (this.ViewModel == null) return;
            this.ViewModel.RemoveSelectedColumnsCommand.Execute();
        }

        /// <summary>
        /// Called when the user double clicks on a row in the grid.
        /// </summary>
        private void AssignedColumnsGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            //remove the selected columns
            if (this.ViewModel == null) return;
            this.ViewModel.RemoveSelectedColumnsCommand.Execute();
        }

        /// <summary>
        /// Called whenever rows are dropped from the field grid to this one.
        /// </summary>
        private void AssignedColumnsGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid != FieldGrid) return;
            if (this.ViewModel == null) return;
            this.ViewModel.AddSelectedColumnsCommand.Execute();
        }

        /// <summary>
        /// Called whenever the totals checkbox is checked.
        /// </summary>
        private void HasTotalsCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            this.TotalTypeColumn.Visibility = System.Windows.Visibility.Visible;
        }

        /// <summary>
        /// Called whenever the totals checkbox is unchecked.
        /// </summary>
        private void HasTotalsCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            this.TotalTypeColumn.Visibility = System.Windows.Visibility.Collapsed;
        }

        #endregion

        #region window close

        /// <summary>
        /// Disposes of the viewmodel on close.
        /// </summary>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;
                    if (dis != null) dis.Dispose();
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion

        
    }
}