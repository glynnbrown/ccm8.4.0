﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
// V8-32396 : A.Probyn
//  Updated to allow selection from outside the assortment for buddies
// CCM-18489 : A.Heathcote
//  Changed the error in IDataErrorInfo Members for percentages to allow 1000% instead of 100%
// CCM-14014 : M.Pettit
//  Added child PlanogramProduct property for optional grid columns
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Globalization;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Interfaces;
using System.Collections.Generic;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.ProductBuddy
{
    /// <summary>
    /// Represents a row in the product buddy window
    /// </summary>
    public sealed class AssortmentProductBuddyRow : ViewModelObject
    {
        #region Fields

        private Boolean _isDirty;

        private PlanogramAssortment _assortment;
        private Dictionary<String, IPlanogramProductInfo> _availableProducts;
        private PlanogramAssortmentProductBuddy _existingProductBuddy;
        private PlanogramAssortmentProductBuddy _productBuddy;

        private PlanogramAssortmentProduct _targetProduct;
        private PlanogramProduct _product;

        private ObservableCollection<ProductBuddySourceRow> _sourceRows = new ObservableCollection<ProductBuddySourceRow>();

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath IsDirtyProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyRow>(p => p.IsDirty);
        public static readonly PropertyPath TargetProductProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyRow>(p => p.TargetProduct);
        public static readonly PropertyPath SummaryProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyRow>(p => p.Summary);
        public static readonly PropertyPath SourceRowsProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyRow>(p => p.SourceRows);

        #endregion

        #region Properties

        public Boolean IsDirty
        {
            get { return _isDirty; }
            private set
            {
                _isDirty = value;
                OnPropertyChanged(IsDirtyProperty);
            }
        }

        /// <summary>
        /// Gets the target product for this row.
        /// </summary>
        public PlanogramAssortmentProduct TargetProduct
        {
            get { return _targetProduct; }
        }

        /// <summary>
        /// Gets/Sets the linked Master Product for the assortment product reference, if set
        /// </summary>
        public PlanogramProduct Product
        {
            get { return _product; }
        }

        /// <summary>
        /// Gets a summary of the product buddy
        /// </summary>
        public String Summary
        {
            get
            {
                String value;

                switch (this.Buddy.SourceType)
                {
                    case PlanogramAssortmentProductBuddySourceType.Attribute:

                        value = String.Format(
                            CultureInfo.CurrentCulture,
                            "{0}: {1}, {2}",
                            PlanogramAssortmentProductBuddySourceTypeHelper.FriendlyNames[PlanogramAssortmentProductBuddySourceType.Attribute],
                            PlanogramAssortmentProductBuddyProductAttributeTypeHelper.FriendlyNames[this.Buddy.ProductAttributeType],
                            PlanogramAssortmentProductBuddyTreatmentTypeHelper.FriendlyNames[this.Buddy.TreatmentType]);

                        if (this.Buddy.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg || this.Buddy.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg)
                        {
                            value = String.Format(CultureInfo.CurrentCulture, "{0} : {1}%", value, this.Buddy.TreatmentTypePercentage * 100);
                        }

                        break;

                    case PlanogramAssortmentProductBuddySourceType.Manual:

                        value = PlanogramAssortmentProductBuddySourceTypeHelper.FriendlyNames[PlanogramAssortmentProductBuddySourceType.Manual];

                        if (this.SourceRows.Count > 0)
                        {
                            value =
                                String.Format(
                            CultureInfo.CurrentCulture,
                            "{0}: {1}",
                            value,
                                this.SourceRows[0].Product.Gtin);
                        }
                        if (this.SourceRows.Count > 1)
                        {
                            value = String.Format(
                            CultureInfo.CurrentCulture,
                            "{0} +{1} {2}",
                            value,
                            this.SourceRows.Count - 1,
                            Message.AssortmentProductBuddy_Summary_Others);
                        }

                        break;

                    default:
                        value = PlanogramAssortmentProductBuddySourceTypeHelper.FriendlyNames[this.Buddy.SourceType];
                        break;
                }

                return value;
            }
        }

        /// <summary>
        /// Returns the editing product buddy
        /// </summary>
        public PlanogramAssortmentProductBuddy Buddy
        {
            get { return _productBuddy; }
        }

        /// <summary>
        /// Returns the collection of source rows
        /// </summary>
        public ObservableCollection<ProductBuddySourceRow> SourceRows
        {
            get { return _sourceRows; }
        }

        #endregion

        #region Constructor

        public AssortmentProductBuddyRow(PlanogramAssortmentProductBuddy buddy, Dictionary<String, IPlanogramProductInfo> availableProducts)
        {
            Debug.Assert(buddy.Parent != null, "Parameter should be an existing buddy");

            _existingProductBuddy = buddy;
            _productBuddy = _existingProductBuddy.Clone();
            _assortment = buddy.Parent;
            _availableProducts = availableProducts;

            //get the target assortment product
            if (_assortment != null)
            {
                _targetProduct = _assortment.Products.FirstOrDefault(p => p.Gtin.Equals(buddy.ProductGtin));

                if (_targetProduct == null && App.ViewState.IsConnectedToRepository)
                {
                    List<ProductInfo> repositoryProducts = new List<ProductInfo>();
                    repositoryProducts.AddRange(ProductInfoList.FetchByEntityId(App.ViewState.EntityId).Where(p => p.Gtin.Equals(buddy.ProductGtin)));
                    _targetProduct = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(repositoryProducts.FirstOrDefault().Gtin, repositoryProducts.FirstOrDefault().Name);
                }

                //fetch the source product model object if available as it may be needed for custom columns
                try
                {
                    _product = _targetProduct.GetPlanogramProduct();
                }
                catch
                {
                    //do nothing - the product may have been deleted
                }
            }
            
            
            ResetSourceRows();

            _isDirty = false;
            _productBuddy.PropertyChanged += ProductBuddy_PropertyChanged;
            _sourceRows.CollectionChanged += SourceRows_CollectionChanged;
        }

        public AssortmentProductBuddyRow(PlanogramAssortment assortment, PlanogramAssortmentProduct targetProduct, Dictionary<String, IPlanogramProductInfo> availableProducts)
        {
            _targetProduct = targetProduct;
            _assortment = assortment;
            _availableProducts = availableProducts;

            //check for an existing buddy
            String targetProductGTIN = targetProduct.Gtin;
            _existingProductBuddy = assortment.ProductBuddies.FirstOrDefault(p => p.ProductGtin.Equals(targetProductGTIN));

            if (_existingProductBuddy == null)
            {
                _productBuddy = PlanogramAssortmentProductBuddy.NewPlanogramAssortmentProductBuddy();
                _productBuddy.ProductGtin = targetProductGTIN;
                _isDirty = true;
            }
            else
            {
                _productBuddy = _existingProductBuddy.Clone();
                _isDirty = false;
            }
            //fetch the source product model object if available as it may be needed for custom columns
            try
            {
                _product = _targetProduct.GetPlanogramProduct();
            }
            catch
            {
                //do nothing - the product may have been deleted
            }

            ResetSourceRows();

            _productBuddy.PropertyChanged += ProductBuddy_PropertyChanged;
            _sourceRows.CollectionChanged += SourceRows_CollectionChanged;
        }



        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to changes on the product buddy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductBuddy_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(SummaryProperty);
            this.IsDirty = true;
        }

        /// <summary>
        /// Responds to changes to the source rows collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SourceRows_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //resync all buddies
            _productBuddy.S1ProductGtin = null;
            _productBuddy.S1Percentage = null;
            _productBuddy.S2ProductGtin = null;
            _productBuddy.S2Percentage = null;
            _productBuddy.S3ProductGtin = null;
            _productBuddy.S3Percentage = null;
            _productBuddy.S4ProductGtin = null;
            _productBuddy.S4Percentage = null;
            _productBuddy.S5ProductGtin = null;
            _productBuddy.S5Percentage = null;


            if (this.SourceRows.Count > 0)
            {
                _productBuddy.S1ProductGtin = this.SourceRows[0].Product.Gtin;
                _productBuddy.S1Percentage = this.SourceRows[0].Percentage;
            }

            if (this.SourceRows.Count > 1)
            {
                _productBuddy.S2ProductGtin = this.SourceRows[1].Product.Gtin;
                _productBuddy.S2Percentage = this.SourceRows[1].Percentage;
            }

            if (this.SourceRows.Count > 2)
            {
                _productBuddy.S3ProductGtin = this.SourceRows[2].Product.Gtin;
                _productBuddy.S3Percentage = this.SourceRows[2].Percentage;
            }

            if (this.SourceRows.Count > 3)
            {
                _productBuddy.S4ProductGtin = this.SourceRows[3].Product.Gtin;
                _productBuddy.S4Percentage = this.SourceRows[3].Percentage;
            }

            if (this.SourceRows.Count > 4)
            {
                _productBuddy.S5ProductGtin = this.SourceRows[4].Product.Gtin;
                _productBuddy.S5Percentage = this.SourceRows[4].Percentage;
            }
        }


        #endregion

        #region Methods

        /// <summary>
        /// Resets the source rows collection
        /// </summary>
        private void ResetSourceRows()
        {
            _sourceRows.Clear();

            PlanogramAssortmentProductBuddy buddy = _productBuddy;

            if (_assortment != null)
            {
                //source 1
                if (!String.IsNullOrEmpty(buddy.S1ProductGtin))
                {
                    IPlanogramProductInfo source1 = _availableProducts.ContainsKey(buddy.S1ProductGtin) ? _availableProducts[buddy.S1ProductGtin] : null;
                    _sourceRows.Add(new ProductBuddySourceRow(source1, buddy.S1Percentage));
                }

                //source 2
                if (!String.IsNullOrEmpty(buddy.S2ProductGtin))
                {
                    IPlanogramProductInfo source2 = _availableProducts.ContainsKey(buddy.S2ProductGtin) ? _availableProducts[buddy.S2ProductGtin] : null;
                    _sourceRows.Add(new ProductBuddySourceRow(source2, buddy.S2Percentage));
                }

                //source 3
                if (!String.IsNullOrEmpty(buddy.S3ProductGtin))
                {
                    IPlanogramProductInfo source3 = _availableProducts.ContainsKey(buddy.S3ProductGtin) ? _availableProducts[buddy.S3ProductGtin] : null;
                    _sourceRows.Add(new ProductBuddySourceRow(source3, buddy.S3Percentage));
                }

                //source 4
                if (!String.IsNullOrEmpty(buddy.S4ProductGtin))
                {
                    IPlanogramProductInfo source4 = _availableProducts.ContainsKey(buddy.S4ProductGtin) ? _availableProducts[buddy.S4ProductGtin] : null;
                    _sourceRows.Add(new ProductBuddySourceRow(source4, buddy.S4Percentage));
                }

                //source 5
                if (!String.IsNullOrEmpty(buddy.S5ProductGtin))
                {
                    IPlanogramProductInfo source5 = _availableProducts.ContainsKey(buddy.S5ProductGtin) ? _availableProducts[buddy.S5ProductGtin] : null;
                    _sourceRows.Add(new ProductBuddySourceRow(source5, buddy.S5Percentage));
                }
            }
        }

        /// <summary>
        /// Commits changes back to the source buddy
        /// </summary>
        public void CommitChanges()
        {
            if (_isDirty)
            {
                PlanogramAssortment applyToAssortment = _assortment;
                PlanogramAssortmentProductBuddy existingBuddy = _existingProductBuddy;
                PlanogramAssortmentProductBuddy editBuddy = _productBuddy;

                #region ensure all source percentages are the same as the source rows
                foreach (ProductBuddySourceRow sourceRow in _sourceRows)
                {
                    if (!String.IsNullOrEmpty(editBuddy.S1ProductGtin) && editBuddy.S1ProductGtin.Equals(sourceRow.Product.Gtin))
                    {
                        editBuddy.S1Percentage = sourceRow.Percentage;
                    }
                    if (!String.IsNullOrEmpty(editBuddy.S2ProductGtin) && editBuddy.S2ProductGtin.Equals(sourceRow.Product.Gtin))
                    {
                        editBuddy.S2Percentage = sourceRow.Percentage;
                    }
                    if (!String.IsNullOrEmpty(editBuddy.S3ProductGtin) && editBuddy.S3ProductGtin.Equals(sourceRow.Product.Gtin))
                    {
                        editBuddy.S3Percentage = sourceRow.Percentage;
                    }
                    if (!String.IsNullOrEmpty(editBuddy.S4ProductGtin) && editBuddy.S4ProductGtin.Equals(sourceRow.Product.Gtin))
                    {
                        editBuddy.S4Percentage = sourceRow.Percentage;
                    }
                    if (!String.IsNullOrEmpty(editBuddy.S5ProductGtin) && editBuddy.S5ProductGtin.Equals(sourceRow.Product.Gtin))
                    {
                        editBuddy.S5Percentage = sourceRow.Percentage;
                    }
                }
                #endregion

                if (existingBuddy == null)
                {
                    //just add the edit buddy
                    applyToAssortment.ProductBuddies.Add(editBuddy);

                }
                else
                {
                    //copy the values
                    existingBuddy.SourceType = editBuddy.SourceType;
                    existingBuddy.TreatmentType = editBuddy.TreatmentType;
                    existingBuddy.TreatmentTypePercentage = (editBuddy.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg
                        || editBuddy.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg)
                        ? editBuddy.TreatmentTypePercentage
                        : 1;
                    existingBuddy.ProductAttributeType = editBuddy.ProductAttributeType;
                    existingBuddy.S1ProductGtin = editBuddy.S1ProductGtin;
                    existingBuddy.S2ProductGtin = editBuddy.S2ProductGtin;
                    existingBuddy.S3ProductGtin = editBuddy.S3ProductGtin;
                    existingBuddy.S4ProductGtin = editBuddy.S4ProductGtin;
                    existingBuddy.S5ProductGtin = editBuddy.S5ProductGtin;
                    existingBuddy.S1Percentage = editBuddy.S1Percentage;
                    existingBuddy.S2Percentage = editBuddy.S2Percentage;
                    existingBuddy.S3Percentage = editBuddy.S3Percentage;
                    existingBuddy.S4Percentage = editBuddy.S4Percentage;
                    existingBuddy.S5Percentage = editBuddy.S5Percentage;
                }

                _isDirty = false;
            }
        }

        /// <summary>
        /// Deletes the existing source buddy.
        /// </summary>
        public void DeleteExistingBuddy()
        {
            if (_existingProductBuddy != null)
            {
                _existingProductBuddy.Parent.ProductBuddies.Remove(_existingProductBuddy);
                _existingProductBuddy = null;
                _isDirty = false;
            }
        }

        public void OnSourceRowChanged()
        {
            _isDirty = true;
        }

        public Boolean IsValid()
        {
            if (!_sourceRows.All(r => r.IsValid()) || !this.Buddy.IsValid)
            {
                return false;
            }
            return true;
        }

        #endregion

        #region IDisposable

        protected override void Dispose(bool disposing)
        {
            if (!IsDisposed)
            {
                if (disposing)
                {
                    _productBuddy.PropertyChanged -= ProductBuddy_PropertyChanged;
                }

                IsDisposed = true;
            }
        }

        #endregion
    }

    public sealed class ProductBuddySourceRow : INotifyPropertyChanged, IDataErrorInfo
    {
        #region Fields
        private IPlanogramProductInfo _product;
        private Single? _percentage;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ProductProperty = WpfHelper.GetPropertyPath<ProductBuddySourceRow>(p => p.Product);
        public static readonly PropertyPath ProductNameProperty = WpfHelper.GetPropertyPath<ProductBuddySourceRow>(p => p.ProductName);
        public static readonly PropertyPath PercentageProperty = WpfHelper.GetPropertyPath<ProductBuddySourceRow>(p => p.Percentage);

        #endregion

        #region Properties

        public IPlanogramProductInfo Product
        {
            get { return _product; }
        }

        public String ProductName
        {
            get { return _product != null ? String.Format("{0} {1}", _product.Gtin, _product.Name) : String.Empty; }
        }

        public Single? Percentage
        {
            get { return _percentage; }
            set
            {
                _percentage = value;
                OnPropertyChanged(PercentageProperty);
            }

        }

        #endregion

        #region Constructor

        public ProductBuddySourceRow(IPlanogramProductInfo product, Single? percentage)
        {
            _product = product;
            _percentage = percentage;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns true if this row is valid
        /// </summary>
        public Boolean IsValid()
        {
            return (String.IsNullOrEmpty(this["Percentage"]));
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        public String Error
        {
            get { return String.Empty; }
        }

        public String this[String columnName]
        {
            get
            {
                if (columnName == PercentageProperty.Path)
                {
                    // Changed to allow 1000% as 100% was not enough and previously (in places) it was set to 1000%
                    if (this.Percentage == null || !(this.Percentage > 0 && this.Percentage <= 10))
                    {
                        return Message.AssortmentProductBuddyWizard_InvalidSourcePercentage;
                    }
                }

                return String.Empty;
            }
        }

        #endregion
    }
}
