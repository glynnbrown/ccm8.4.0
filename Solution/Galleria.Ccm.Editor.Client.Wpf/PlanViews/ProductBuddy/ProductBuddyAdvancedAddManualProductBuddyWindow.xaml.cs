﻿#region Header Information
// Copyright © Galleria RTS Ltd 2016

#region Version History: (CCM 8.3.0)
// SA-32855 : A.Probyn
//      Created
// CCM-18310 : A.Heathcote
//  Integrated into SpacePlanning
// CCM-18489 : A.Heathcote
//  Changed the error in IDataErrorInfo Members for percentages to allow 1000% instead of 100%
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using System.ComponentModel;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using System.Windows.Controls.Primitives;
using System.Windows.Controls;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.ProductBuddy
{
    /// <summary>
    /// Interaction logic for ProductBuddyAdvancedAddManualProductBuddyWindow.xaml
    /// </summary>
    public partial class ProductBuddyAdvancedAddManualProductBuddyWindow : ExtendedRibbonWindow, IDataErrorInfo, INotifyPropertyChanged
    {
        #region Properties

        #region SelectedTreatmentTypeProperty

        public static readonly DependencyProperty SelectedTreatmentTypeProperty =
           DependencyProperty.Register("SelectedTreatmentType", typeof(PlanogramAssortmentProductBuddyTreatmentType), typeof(ProductBuddyAdvancedAddManualProductBuddyWindow),
           new PropertyMetadata(PlanogramAssortmentProductBuddyTreatmentType.Avg));

        /// <summary>
        /// Gets/Sets the selection type
        /// </summary>
        public PlanogramAssortmentProductBuddyTreatmentType SelectedTreatmentType
        {
            get { return (PlanogramAssortmentProductBuddyTreatmentType)GetValue(SelectedTreatmentTypeProperty); }
            set { SetValue(SelectedTreatmentTypeProperty, value); }
        }

        #endregion

        #region SelectedTreatmentTypePercentageProperty

        public static readonly DependencyProperty SelectedTreatmentTypePercentageProperty =
            DependencyProperty.Register("SelectedTreatmentTypePercentage", typeof(Single), 
            typeof(ProductBuddyAdvancedAddManualProductBuddyWindow), 
            new PropertyMetadata(0f, OnSelectedTreatmentTypePercentageChanged));

        /// <summary>
        /// Gets/Sets the selection type
        /// </summary>
        public Single SelectedTreatmentTypePercentage
        {
            get { return (Single)GetValue(SelectedTreatmentTypePercentageProperty); }
            set 
            {
                SetValue(SelectedTreatmentTypePercentageProperty, value);
            }
        }

        private static void OnSelectedTreatmentTypePercentageChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ProductBuddyAdvancedAddManualProductBuddyWindow win = sender as ProductBuddyAdvancedAddManualProductBuddyWindow;
            if (win != null)
            {
                win.OnPropertyChanged(new PropertyPath("IsValid"));
            }
        }

        #endregion

        #region SelectedProductTextProperty

        public static readonly DependencyProperty SelectedProductTextProperty =
           DependencyProperty.Register("SelectedProductText", typeof(String), typeof(ProductBuddyAdvancedAddManualProductBuddyWindow),
           new PropertyMetadata(String.Empty));

        /// <summary>
        /// Gets/Sets the selected product text
        /// </summary>
        public String SelectedProductText
        {
            get { return (String)GetValue(SelectedProductTextProperty); }
            set { SetValue(SelectedProductTextProperty, value); }
        }

        public Boolean IsValid
        {
            get { return String.IsNullOrEmpty(this["SelectedTreatmentTypePercentage"]); }
        }        

        #endregion

        #endregion

        #region Constructor

        public ProductBuddyAdvancedAddManualProductBuddyWindow()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;
            InitializeComponent();
            SelectedTreatmentTypePercentage = 100; //default to 100%
            this.Loaded += ProductBuddyAdvancedAddWindow_Loaded;
        }
        
        private void ProductBuddyAdvancedAddWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ProductBuddyAdvancedAddWindow_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Forces the toggle button to be uncheckable
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToggleButton_ForceUncheckable(object sender, MouseButtonEventArgs e)
        {
            if (((ToggleButton)sender).IsChecked == true)
            {
                //if the original source was not an ordinary button
                if (((DependencyObject)e.OriginalSource).FindVisualAncestor<Button>() == null)
                {
                    e.Handled = true;
                }
            }
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        #endregion

        #region IDataErrorInfo Members

        public String Error
        {
            get { return String.Empty; }
        }

        public String this[String columnName]
        {
            get
            {
                //treatment type validation
                if (columnName == SelectedTreatmentTypePercentageProperty.Name)
                {
                    //1000 selected as 100% was not enough (this is converted to a Single in the AssortmentProductBuddyAdvancedAddRow OnSourcePercentageCellValueChanged)
                    if (!(SelectedTreatmentTypePercentage > 0 && SelectedTreatmentTypePercentage <= 1000))
                    {
                        return Message.ProductBuddyReview_ValidateTreatmentPercentage_NotPercentage;
                    }
                }

                return String.Empty;
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
        }

        #endregion

        #region Window close

        /// <summary>
        /// Method to override the on closed method
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion

    }
}
