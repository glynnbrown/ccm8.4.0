﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
// V8-32396 : A.Probyn
//  Updated to allow selection from outside the assortment for buddies
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Model;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.ProductBuddy
{
    /// <summary>
    /// Denotes a row used to display a product buddy source product.
    /// </summary>
    public sealed class AssortmentProductBuddyWizardSourceRow : INotifyPropertyChanged, IDataErrorInfo
    {
        #region Fields
        private IPlanogramProductInfo _product;
        private String _productGtin;
        private Single _percentage;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath GtinProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardSourceRow>(p => p.Gtin);
        public static readonly PropertyPath PercentageProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardSourceRow>(p => p.Percentage);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the Product
        /// this is for.
        /// </summary>
        public IPlanogramProductInfo Product
        {
            get { return _product; }
        }

        /// <summary>
        /// Gets the product description
        /// </summary>
        public String Gtin
        {
            get { return _productGtin; }
        }

        /// <summary>
        /// Gets/Sets the percentage for the product
        /// </summary>
        public Single Percentage
        {
            get { return _percentage; }
            set
            {
                _percentage = value;
                OnPropertyChanged(PercentageProperty);
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new row
        /// </summary>
        /// <param name="id">the product id this is for</param>
        /// <param name="description">the product description</param>
        public AssortmentProductBuddyWizardSourceRow(IPlanogramProductInfo product)
        {
            _product = product;
            _productGtin = product.Gtin;
            _percentage = 1F;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns true if this row is valid
        /// </summary>
        public Boolean IsValid()
        {
            return (String.IsNullOrEmpty(this["Percentage"]));
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        public String Error
        {
            get { return String.Empty; }
        }

        public String this[String columnName]
        {
            get
            {
                if (columnName == PercentageProperty.Path)
                {
                    if (this.Percentage <= 0 || this.Percentage > 10)
                    {
                        return Message.AssortmentProductBuddyWizard_InvalidSourcePercentage;
                    }
                }

                return String.Empty;
            }
        }

        #endregion
    }
}
