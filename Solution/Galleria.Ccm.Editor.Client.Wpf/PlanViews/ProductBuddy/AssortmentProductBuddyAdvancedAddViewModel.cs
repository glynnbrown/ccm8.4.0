﻿#region Header Information
// Copyright © Galleria RTS Ltd 2016

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
// V8-31950 : A.Probyn
//  Updated to remove App.ShowWindow on assortment product selector
// V8-32396 : A.Probyn
//  Updated to allow selection from outside the assortment for buddies
// V8-32718 : D.Pleasance
//  Added planItems to constructor. These are passed through to enable displaying of product / position attributes.
// V8-32812 : A.Heathcote
//  Made changes to the SelectTargetProducts method to allow multiple buddies to be added at once. 
// CCM-18310 : A.Heathcote
//  Added the Treatment Percentage and Treatment Type selection window for manual buddies.
// CCM-18325 : A.Heathcote 
//  Added an if statment to SelectTargetProducts that populates the product selector differently depending upon the state of the repository
// CCM-18458 : M.Brumby
//  Don't allow the selection of assortment products as a target product if it is already a target product. Plus a little performance improvement
// CCM14012 : A.Heathcote
//  Made changes to SelectTargetProducts to use the new constructor and allo the creation of buddies from outside the assortment.
// CCM-18489 : M.Pettit
//  Add selected assortment products to grid
// CCM-18470 : G.Richards
//  Multi-select on combo box options changes all selected rows
// CCM-18458 : A.Heathcote
//  Added the excluded list to satisfy the edited constructor.
// CCM-18560 : D.Pleasance
//  Extended grid to allow multiple updates for editable grid columns.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;


namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.ProductBuddy
{
    /// <summary>
    /// ViewModel controller class for ProductBuddyAdvancedAddWindow
    /// </summary>
    public sealed class AssortmentProductBuddyAdvancedAddViewModel : ViewModelAttachedControlObject<AssortmentProductBuddyAdvancedAddWindow>
    {
        #region Fields

        private IEnumerable<IPlanItem> _planItems;
        private PlanogramAssortment _currentAssortment;
        private Dictionary<String, IPlanogramProductInfo> _availableSourceProductLookup = new Dictionary<String, IPlanogramProductInfo>();
        private Boolean _isRemoveExistingBuddiesSelected = false;
        private BulkObservableCollection<AssortmentProductBuddyAdvancedAddRow> _productBuddyRows = new BulkObservableCollection<AssortmentProductBuddyAdvancedAddRow>();
        private BulkObservableCollection<AssortmentProductBuddyAdvancedAddRow> _productBuddyRowsSelected = new BulkObservableCollection<AssortmentProductBuddyAdvancedAddRow>();
        private String _categoryCode;
        private Boolean _isUpdatingMultiInput;
        #endregion

        #region Binding Property Paths

        //properties
        public static readonly PropertyPath IsRemoveExistingBuddiesSelectedProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddViewModel>(p => p.IsRemoveExistingBuddiesSelected);
        public static readonly PropertyPath ProductBuddyRowsProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddViewModel>(p => p.ProductBuddyRows);
        public static readonly PropertyPath ProductBuddyRowsSelectedProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddViewModel>(p => p.ProductBuddyRowsSelected);
        //commands
        public static readonly PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddViewModel>(p => p.OKCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath RemoveProductBuddyCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddViewModel>(p => p.RemoveProductBuddyCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets flag to indicate if the existing buddies should be cleared.
        /// </summary>
        public Boolean IsRemoveExistingBuddiesSelected
        {
            get { return _isRemoveExistingBuddiesSelected; }
            set
            {
                _isRemoveExistingBuddiesSelected = value;
                OnPropertyChanged(IsRemoveExistingBuddiesSelectedProperty);
            }
        }

        /// <summary>
        /// Returns the collection of product buddy rows to be added.
        /// </summary>
        public BulkObservableCollection<AssortmentProductBuddyAdvancedAddRow> ProductBuddyRows
        {
            get { return _productBuddyRows; }
        }

        /// <summary>
        /// Returns the collection of product buddy rows selected.
        /// </summary>
        public BulkObservableCollection<AssortmentProductBuddyAdvancedAddRow> ProductBuddyRowsSelected
        {
            get { return _productBuddyRowsSelected; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        public AssortmentProductBuddyAdvancedAddViewModel(PlanogramAssortment currentAssortment, IEnumerable<IPlanItem> planItems)
        {
            _planItems = planItems;
            _currentAssortment = currentAssortment;
            
             //Used in the SelectTargetProducts method
            _categoryCode = currentAssortment.Parent.CategoryCode;      
                   
            //Initialise product lists
            if (App.ViewState.IsConnectedToRepository)
            {
                _availableSourceProductLookup = ProductInfoList.FetchByEntityId(App.ViewState.EntityId).Cast<IPlanogramProductInfo>().ToDictionary(p => p.Gtin);
            }
            else
            {
                _availableSourceProductLookup = _currentAssortment.Products.Cast<IPlanogramProductInfo>().ToDictionary(p => p.Gtin);
            }

            this.ProductBuddyRows.BulkCollectionChanged += ProductBuddyRows_BulkCollectionChanged;

            //add an initial blank row
            _productBuddyRows.Add(new AssortmentProductBuddyAdvancedAddRow(_currentAssortment,
                _availableSourceProductLookup, 
                _planItems,
                (Action<AssortmentProductBuddyAdvancedAddRow>)SelectTargetProducts));
        }

        #endregion

        #region Commands

        #region OK

        private RelayCommand _okCommand;

        /// <summary>
        /// Adds the product buddies requested.
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OK_Executed(),
                        p => OK_CanExecute())
                    {
                        FriendlyName = Message.Generic_OK
                    };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        private Boolean OK_CanExecute()
        {
            //must have at least 1 row
            if (this.ProductBuddyRows.Count(p => !p.IsNewPlaceholderRow) == 0)
            {
                this.OKCommand.DisabledReason = Message.AssortmentProductBuddyAdvancedAdd_OK_DisabledNoRows;
                return false;
            }

           // all buddies must be valid
            if (!this.ProductBuddyRows.All(p => (p.IsNewPlaceholderRow) ? true : p.IsValid))
            {
                this.OKCommand.DisabledReason = Message.AssortmentProductBuddyAdvancedAdd_OK_DisabledInvalidRows;
                return false;
            }

            return true;
        }

        private void OK_Executed()
        {
            //clear existing buddies if required
            if (this.IsRemoveExistingBuddiesSelected)
            {
                this._currentAssortment.ProductBuddies.Clear();
            }

            //add the new buddies
            foreach (AssortmentProductBuddyAdvancedAddRow row in this.ProductBuddyRows)
            {
                if (row.TargetProduct != null)
                {
                    //check if a buddy already exists for the target
                    // if yes then remove it first
                    Object tarrgetGTIN = row.TargetProduct.Gtin;
                    PlanogramAssortmentProductBuddy existingBuddy = this._currentAssortment.ProductBuddies.FirstOrDefault(p => tarrgetGTIN.Equals(p.ProductGtin));
                    if (existingBuddy != null)
                    {
                        this._currentAssortment.ProductBuddies.Remove(existingBuddy);
                    }

                    //add the new buddy
                    this._currentAssortment.ProductBuddies.Add(AssortmentProductBuddyAdvancedAddRow.CreateProductBuddy(row));
                }
            }


            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels and closes the window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region RemoveProductBuddy

        private RelayCommand _removeProductBuddyCommand;

        /// <summary>
        /// Removes the given buddy
        /// </summary>
        public RelayCommand RemoveProductBuddyCommand
        {
            get
            {
                if (_removeProductBuddyCommand == null)
                {
                    _removeProductBuddyCommand = new RelayCommand(
                        p => RemoveProductBuddy_Executed(p),
                        p => RemoveProductBuddy_CanExecute(p))
                    {
                        //SmallIcon = ImageResources.ProductBuddyAdvancedAdd_RemoveProductBuddy
                        SmallIcon = ImageResources.AssortmentProductBuddyWizard_RemoveSourceProduct
                    };
                    base.ViewModelCommands.Add(_removeProductBuddyCommand);
                }
                return _removeProductBuddyCommand;
            }
        }

        private Boolean RemoveProductBuddy_CanExecute(Object arg)
        {
            AssortmentProductBuddyAdvancedAddRow buddyToRemove = arg as AssortmentProductBuddyAdvancedAddRow;

            if (buddyToRemove == null)
            {
                return false;
            }

            //must not be the new placeholder row.
            if (buddyToRemove.IsNewPlaceholderRow)
            {
                return false;
            }

            return true;
        }

        private void RemoveProductBuddy_Executed(Object arg)
        {
            AssortmentProductBuddyAdvancedAddRow buddyToRemove = arg as AssortmentProductBuddyAdvancedAddRow;
            if (buddyToRemove != null)
            {
                this.ProductBuddyRows.Remove(buddyToRemove);
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to changes in the product buddy rows collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductBuddyRows_BulkCollectionChanged(Object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (AssortmentProductBuddyAdvancedAddRow row in e.ChangedItems)
                    {
                        row.PropertyChanged += ProductBuddyRow_PropertyChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (AssortmentProductBuddyAdvancedAddRow row in e.ChangedItems)
                    {
                        row.Dispose();
                        row.PropertyChanged -= ProductBuddyRow_PropertyChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    if (e.ChangedItems != null)
                    {
                        foreach (AssortmentProductBuddyAdvancedAddRow row in e.ChangedItems)
                        {
                            row.Dispose();
                            row.PropertyChanged -= ProductBuddyRow_PropertyChanged;
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Responds to a property change on a product buddy row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductBuddyRow_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.TargetProductProperty.Path)
            {
                //add a new blank row if required
                AssortmentProductBuddyAdvancedAddRow blankRow = this.ProductBuddyRows.FirstOrDefault(r => r.TargetProduct == null);
                if (blankRow == null)
                {
                    this.ProductBuddyRows.Add(new AssortmentProductBuddyAdvancedAddRow(_currentAssortment,
                        _availableSourceProductLookup,
                        _planItems, (Action<AssortmentProductBuddyAdvancedAddRow>)SelectTargetProducts));
                }
            }

            if (!_isUpdatingMultiInput)
            {
                if (_productBuddyRowsSelected.Count > 1)
                {
                    if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.SourceTypeProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.TreatmentTypeProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.ProductAttributeTypeProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.TreatmentTypePercentageProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source1CellValueProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source1PercentageCellValueProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source2CellValueProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source2PercentageCellValueProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source3CellValueProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source3PercentageCellValueProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source4CellValueProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source4PercentageCellValueProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source5CellValueProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source5PercentageCellValueProperty.Path)
                    {
                        _isUpdatingMultiInput = true;
                        AssortmentProductBuddyAdvancedAddRow productBuddyRow = (AssortmentProductBuddyAdvancedAddRow)sender;
                        foreach (AssortmentProductBuddyAdvancedAddRow selectedProductBuddyRow in _productBuddyRowsSelected)
                        {
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.SourceTypeProperty.Path)
                            {
                                selectedProductBuddyRow.SourceTypeCellValue = productBuddyRow.SourceTypeCellValue;
                            }

                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.TreatmentTypeProperty.Path)
                            {
                                Boolean applyValue = true;

                                if (selectedProductBuddyRow.SourceType == PlanogramAssortmentProductBuddySourceType.Manual)
                                {
                                    if (productBuddyRow.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg ||
                                        productBuddyRow.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg)
                                    {
                                        applyValue = false;
                                    }
                                }
                                if (applyValue)
                                {
                                    selectedProductBuddyRow.TreatmentTypeCellValue = productBuddyRow.TreatmentTypeCellValue;
                                }
                            }

                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.ProductAttributeTypeProperty.Path)
                            {
                                selectedProductBuddyRow.ProductAttributeTypeCellValue = productBuddyRow.ProductAttributeTypeCellValue;
                            }

                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.TreatmentTypePercentageProperty.Path)
                            {
                                if (selectedProductBuddyRow.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg ||
                                    selectedProductBuddyRow.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg)
                                {
                                    selectedProductBuddyRow.TreatmentTypePercentage = productBuddyRow.TreatmentTypePercentage;
                                }
                            }
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source1CellValueProperty.Path)
                            {
                                if (selectedProductBuddyRow.SourceType == PlanogramAssortmentProductBuddySourceType.Manual)
                                {
                                    selectedProductBuddyRow.Source1CellValue = productBuddyRow.Source1CellValue;
                                }
                            }
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source1PercentageCellValueProperty.Path)
                            {
                                if (selectedProductBuddyRow.Source1CellValue != null)
                                {
                                    selectedProductBuddyRow.Source1PercentageCellValue = productBuddyRow.Source1PercentageCellValue;
                                }
                            }
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source2CellValueProperty.Path)
                            {
                                if (selectedProductBuddyRow.SourceType == PlanogramAssortmentProductBuddySourceType.Manual)
                                {
                                    selectedProductBuddyRow.Source2CellValue = productBuddyRow.Source2CellValue;
                                }
                            }
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source2PercentageCellValueProperty.Path)
                            {
                                if (selectedProductBuddyRow.Source2CellValue != null)
                                {
                                    selectedProductBuddyRow.Source2PercentageCellValue = productBuddyRow.Source2PercentageCellValue;
                                }
                            }
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source3CellValueProperty.Path)
                            {
                                if (selectedProductBuddyRow.SourceType == PlanogramAssortmentProductBuddySourceType.Manual)
                                {
                                    selectedProductBuddyRow.Source3CellValue = productBuddyRow.Source3CellValue;
                                }
                            }
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source3PercentageCellValueProperty.Path)
                            {
                                if (selectedProductBuddyRow.Source3CellValue != null)
                                {
                                    selectedProductBuddyRow.Source3PercentageCellValue = productBuddyRow.Source3PercentageCellValue;
                                }
                            }
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source4CellValueProperty.Path)
                            {
                                if (selectedProductBuddyRow.SourceType == PlanogramAssortmentProductBuddySourceType.Manual)
                                {
                                    selectedProductBuddyRow.Source4CellValue = productBuddyRow.Source4CellValue;
                                }
                            }
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source4PercentageCellValueProperty.Path)
                            {
                                if (selectedProductBuddyRow.Source3CellValue != null)
                                {
                                    selectedProductBuddyRow.Source4PercentageCellValue = productBuddyRow.Source4PercentageCellValue;
                                }
                            }
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source5CellValueProperty.Path)
                            {
                                if (selectedProductBuddyRow.SourceType == PlanogramAssortmentProductBuddySourceType.Manual)
                                {
                                    selectedProductBuddyRow.Source5CellValue = productBuddyRow.Source5CellValue;
                                }
                            }
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source5PercentageCellValueProperty.Path)
                            {
                                if (selectedProductBuddyRow.Source4CellValue != null)
                                {
                                    selectedProductBuddyRow.Source5PercentageCellValue = productBuddyRow.Source5PercentageCellValue;
                                }
                            }
                        }
                        _isUpdatingMultiInput = false;
                    }
                }
            }            
        }
        #endregion

        #region Methods

        /// <summary>
        /// Add passed products as target products
        /// </summary>
        /// <param name="selectedAssortmentProducts"></param>
        public void AddTargetProductsFromList(IEnumerable<IPlanItem> selectedItems)
        {
            if (!selectedItems.Any()) return;

            //remove the placeholder row
            RemovePlaceholderRow();

            foreach (IPlanItem planItem in selectedItems)
            {
                AssortmentProductBuddyAdvancedAddRow row = new AssortmentProductBuddyAdvancedAddRow(
                    _currentAssortment, _availableSourceProductLookup, _planItems, 
                    (Action<AssortmentProductBuddyAdvancedAddRow>)SelectTargetProducts);
                row.SourceType = PlanogramAssortmentProductBuddySourceType.Attribute;
                row.SourceTypeCellValue = row.SourceType.ToString();
                row.IsNewPlaceholderRow = false;
                PlanogramAssortmentProduct product = planItem.Product.Model.GetPlanogramAssortmentProduct();
                row.TargetProduct = product;
                row.TargetProductCellValue = product.ToString();
                row.TargetProductError = null;
                this.ProductBuddyRows.Add(row);
            }

            //add the placeholder row back in
            this.ProductBuddyRows.Add(
                new AssortmentProductBuddyAdvancedAddRow(
                    _currentAssortment, _availableSourceProductLookup, _planItems, 
                    (Action<AssortmentProductBuddyAdvancedAddRow>)SelectTargetProducts));
        }

        /// <summary>
        /// Removes the placeholder row from the buddy rows list
        /// </summary>
        private void RemovePlaceholderRow()
        {
            foreach (AssortmentProductBuddyAdvancedAddRow buddies in ProductBuddyRows.ToList())
            {
                if (!buddies.IsNewPlaceholderRow) continue;
                else
                {
                    ProductBuddyRows.Remove(buddies);
                }
            }
        }

        #region Select Target Products

        /// <summary>
        /// This method is used to set up buddies as selected by the user
        /// </summary>
        /// <param name="row"></param>
        public void SelectTargetProducts(AssortmentProductBuddyAdvancedAddRow row)
        {
            List<String> excludedList = new List<string>();
            foreach (AssortmentProductBuddyAdvancedAddRow advancedRow in ProductBuddyRows)
            {
                if (advancedRow.TargetProduct == null) continue;
                excludedList.Add(advancedRow.TargetProduct.Gtin);
            }
            List<Product> productList = new List<Product>();
            foreach (IPlanItem item in _planItems)
            {
                if (!App.ViewState.IsConnectedToRepository)
                    {
                        //if there is no repository connected then each item in the list is created directly from the products in the assortment.
                        PlanogramProduct planProd = item.Product.AssortmentProduct.GetPlanogramProduct();
                        Product productForList = Product.NewProduct(planProd, false);
                        productList.Add(productForList);
                    }
            }
            ProductSelectorViewModel win = new ProductSelectorViewModel(true, _categoryCode, productList, excludedList);
            CommonHelper.GetWindowService().ShowDialog<ProductSelectorWindow>(win);
            ProductBuddyAdvancedAddManualProductBuddyWindow manualWin = null;

            if (win.DialogResult == true && win.AssignedProducts != null && win.AssignedProducts.Any())
            {
                if (win.ManualBuddyButtonPressed)
                {
                    //Show new manual buddy window, allowing the user to select a treatment percentage and type.
                    manualWin = new ProductBuddyAdvancedAddManualProductBuddyWindow();
                    manualWin.SelectedProductText = String.Format("{0} {1}", win.AssignedProducts.Count, Message.ProductBuddyWizard_MultipleBuddySourceText);
                    CommonHelper.GetWindowService().ShowDialog<ProductBuddyAdvancedAddManualProductBuddyWindow>(manualWin);

                    //Sets the rest of the requiered properties for a manual buddy.
                    row.SourceType = PlanogramAssortmentProductBuddySourceType.Manual;
                    row.SourceTypeCellValue = row.SourceType.ToString();

                    row.Source1 = ProductInfo.NewProductInfo(win.AssignedProducts[0]);
                    row.Source1CellValue = row.Source1.ToString();

                    row.TreatmentType = manualWin.SelectedTreatmentType;
                    row.TreatmentTypeCellValue = row.TreatmentType.ToString();

                    row.Source1Percentage = manualWin.SelectedTreatmentTypePercentage;
                    row.Source1PercentageCellValue = row.Source1Percentage.ToString();
                }
                else
                {
                    row.SourceType = PlanogramAssortmentProductBuddySourceType.Attribute;
                    row.SourceTypeCellValue = row.SourceType.ToString();
                }
                //this is no longer the placeholder.
                row.IsNewPlaceholderRow = false;
                row.TargetProductCellValue = win.AssignedProducts[0].ToString();

                if (App.ViewState.IsConnectedToRepository)
                {
                    AssortmentProduct selectedAssortmentProduct =
                        AssortmentProduct.NewAssortmentProduct(win.AssignedProducts[0]);
                    row.TargetProduct = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(selectedAssortmentProduct);
                }
                if (row.TargetProduct != null && !row.IsNewPlaceholderRow)
                {
                    if (row.TargetProduct.ToString() == win.AssignedProducts[0].ToString())
                    {
                        row.TargetProductError = null;
                        row.Source1Error = null;
                    }
                    else
                    {
                        row.TargetProduct = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(win.AssignedProducts[0]);
                        row.TargetProductError = null;
                    }
                }
            }
            

            if (win.AssignedProducts.Count > 1)
            {
                //Because the first buddy has been completed above the "index" is one so that we dont do the first item twice
                Int32 index = 1;
                foreach (Product assignedProduct in win.AssignedProducts)
                {
                    // The index is used to skip the first item in AssignedProducts as this buddy was created in the 
                    // above if statment, this increments by 1 each time.
                    if (assignedProduct != win.AssignedProducts[index]) continue;
                   
                        AssortmentProductBuddyAdvancedAddRow newRow = new AssortmentProductBuddyAdvancedAddRow(this._currentAssortment,
                            this._availableSourceProductLookup,
                            this._planItems,
                            (Action<AssortmentProductBuddyAdvancedAddRow>)SelectTargetProducts);
                        AssortmentProduct selectedAssortmentProduct =
                            AssortmentProduct.NewAssortmentProduct(win.AssignedProducts[index]);
                        newRow.TargetProduct = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(selectedAssortmentProduct);
                    
                        newRow.TargetProductCellValue = win.AssignedProducts[index].ToString();
                        newRow.TargetProductError = null;

                        if (win.ManualBuddyButtonPressed)
                        {
                            //sets these buddies to manual and has them buddy to themselves
                            newRow.SourceType = PlanogramAssortmentProductBuddySourceType.Manual;
                            newRow.SourceTypeCellValue = newRow.SourceType.ToString();

                            newRow.Source1 = ProductInfo.NewProductInfo(win.AssignedProducts[index]);
                            newRow.Source1CellValue = newRow.Source1.ToString();

                            newRow.Source1Percentage = manualWin.SelectedTreatmentTypePercentage;
                            newRow.Source1PercentageCellValue = newRow.Source1Percentage.ToString();

                            newRow.TreatmentType = manualWin.SelectedTreatmentType;
                            newRow.TreatmentTypeCellValue = newRow.TreatmentType.ToString();
                        }
                        else
                        {
                            //sets the buddies to attribute instead of manual 
                            newRow.SourceType = PlanogramAssortmentProductBuddySourceType.Attribute;
                            newRow.SourceTypeCellValue = newRow.SourceType.ToString();
                        }
                        //this is no longer the placeholder row.
                        newRow.IsNewPlaceholderRow = false;
                        if (newRow.TargetProduct == null) 
                        {
                            newRow.TargetProduct = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(win.AssignedProducts[index]);
                        }

                        //add the rest of the buddies to the collection
                        this.ProductBuddyRows.Add(newRow);
                        index++;

                    //Now we delete the placeholder row and re-add it.
                    //(This is to stop the placeholder row being placed in the middle of the grid)
                }
                foreach (AssortmentProductBuddyAdvancedAddRow buddies in ProductBuddyRows.ToList())
                {
                    if (!buddies.IsNewPlaceholderRow) continue;
                    else
                    {
                        ProductBuddyRows.Remove(buddies);
                    }
                }
            this.ProductBuddyRows.Add(new AssortmentProductBuddyAdvancedAddRow(_currentAssortment, _availableSourceProductLookup, _planItems, (Action
                <AssortmentProductBuddyAdvancedAddRow>)SelectTargetProducts));
            }               
        }

        #endregion

        #region Add Buddies From Clipboard

        /// <summary>
        /// Creates buddy rows from the clipboard string
        /// </summary>
        /// <param name="clipboardText"></param>
        public void AddBuddiesFromClipboard(String clipboardText)
        {
            //split into buddy rows
            String[] rows = clipboardText.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            if (rows.Length != 0)
            {

                //work out the delimiter from the first row
                Boolean isExcelDelim = false;
                String[] row1Split = rows[0].Split("\t".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (row1Split.Length >= 3)
                {
                    isExcelDelim = true;
                }

                //check if this is a comma delim
                Boolean isCommaDelim = false;
                if (!isExcelDelim)
                {
                    row1Split = rows[0].Split(',');
                    if (row1Split.Length >= 3)
                    {
                        isCommaDelim = true;
                    }
                }

                if (isExcelDelim || isCommaDelim)
                {
                    //remove the last blank row
                    this.ProductBuddyRows.Remove(this.ProductBuddyRows.Last());

                    foreach (String row in rows)
                    {
                        //split the row
                        String[] rowSplit;
                        if (isExcelDelim)
                        {
                            rowSplit = row.Split("\t".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                        }
                        else
                        {
                            rowSplit = row.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                        }

                        //create
                        AssortmentProductBuddyAdvancedAddRow buddyRow = new AssortmentProductBuddyAdvancedAddRow(_currentAssortment,
                            _availableSourceProductLookup,
                            _planItems, 
                            (Action<AssortmentProductBuddyAdvancedAddRow>)SelectTargetProducts);
                        buddyRow.TargetProductCellValue = rowSplit[0];

                        if (rowSplit.Length >= 2)
                        {
                            buddyRow.SourceTypeCellValue = rowSplit[1];
                        }

                        if (rowSplit.Length >= 3)
                        {
                            buddyRow.TreatmentTypeCellValue = rowSplit[2];
                        }


                        if (buddyRow.SourceType == PlanogramAssortmentProductBuddySourceType.Attribute)
                        {
                            if (rowSplit.Length >= 4)
                            {
                                buddyRow.ProductAttributeTypeCellValue = rowSplit[3];
                            }
                        }
                        else if (buddyRow.SourceType == PlanogramAssortmentProductBuddySourceType.Manual)
                        {
                            if (rowSplit.Length >= 4)
                            {
                                buddyRow.Source1CellValue = rowSplit[3];
                            }
                            if (rowSplit.Length >= 5)
                            {
                                buddyRow.Source1PercentageCellValue = rowSplit[4];
                            }

                            if (rowSplit.Length >= 6)
                            {
                                buddyRow.Source2CellValue = rowSplit[5];
                            }
                            if (rowSplit.Length >= 7)
                            {
                                buddyRow.Source2PercentageCellValue = rowSplit[6];
                            }

                            if (rowSplit.Length >= 8)
                            {
                                buddyRow.Source3CellValue = rowSplit[7];
                            }
                            if (rowSplit.Length >= 9)
                            {
                                buddyRow.Source3PercentageCellValue = rowSplit[8];
                            }

                            if (rowSplit.Length >= 10)
                            {
                                buddyRow.Source4CellValue = rowSplit[9];
                            }
                            if (rowSplit.Length >= 11)
                            {
                                buddyRow.Source4PercentageCellValue = rowSplit[10];
                            }

                            if (rowSplit.Length >= 11)
                            {
                                buddyRow.Source5CellValue = rowSplit[10];
                            }
                            if (rowSplit.Length >= 12)
                            {
                                buddyRow.Source5PercentageCellValue = rowSplit[11];
                            }
                        }
                        //add the buddy row.
                        this.ProductBuddyRows.Add(buddyRow);
                    }

                    //add the blank row back in
                    this.ProductBuddyRows.Add(new AssortmentProductBuddyAdvancedAddRow(_currentAssortment,
                        _availableSourceProductLookup,
                        _planItems,
                        (Action<AssortmentProductBuddyAdvancedAddRow>)SelectTargetProducts));
                }
            }
        }

        #endregion       

        #endregion

        #region IDisposable
        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.ProductBuddyRows.Clear();//clear rows so they get disposed.
                    this.ProductBuddyRows.BulkCollectionChanged -= ProductBuddyRows_BulkCollectionChanged;
                    _availableSourceProductLookup.Clear();
                    _availableSourceProductLookup = null;
                    _currentAssortment = null;
                }
                base.IsDisposed = true;
            }
        }
        #endregion
    }
}
