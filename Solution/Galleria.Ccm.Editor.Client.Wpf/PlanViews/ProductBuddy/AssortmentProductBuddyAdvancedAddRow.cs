﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
// V8-31950 : A.Probyn
//  Updated to remove App.ShowWindow on assortment product selector
// V8-32396 : A.Probyn
//  Updated to allow selection from outside the assortment for buddies
// V8-32718 : D.Pleasance
//  Added planItems to constructor. These are passed through to enable displaying of product / position attributes.
// CCM-18316 : A.Heathcote
//  Removed the "private" from the setter from "TreatmentType" property (So that /AssortmentProductBuddyAdvancedAddViewModel can change this property)
// CCM-14012 : A.Heathcote 
//  Removed Private and replaced with internal for Target ProductError and Source1Error
// CCM-18316 : A.Heathcote
//  Added multi-select functionality to the SelectSourceProduct command.
// CCM-18490 : M.Pettit
//  AddProducts command now passes the parent planogram's category as an inital category to the selector window
// CCM-18500 : A.Heathcote
//  Removed the _availableSourceProductLookup.clear(); from iDispose as it was causing the sources to become invalid. Now works fine.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Collections.Specialized;
using System.Globalization;

using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Collections;

using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Ccm.Common.Wpf.Selectors;


namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.ProductBuddy
{
    /// <summary>
    /// Represents a product buddy row on the advanced add window.
    /// </summary>
    public sealed class AssortmentProductBuddyAdvancedAddRow : ViewModelObject
    {
        #region Fields

        private Action<AssortmentProductBuddyAdvancedAddRow> _selectTargetProductMethod;

        private IEnumerable<IPlanItem> _planItems;
        private PlanogramAssortment _currentAssortment;
        private Dictionary<String, IPlanogramProductInfo> _availableSourceProductLookup;
        private Boolean _isNewPlaceholderRow;

        private String _targetProductCellValue;
        private PlanogramAssortmentProduct _targetProduct;
        private String _targetProductError;

        private String _sourceTypeCellValue;
        private PlanogramAssortmentProductBuddySourceType? _sourceType;
        private String _sourceTypeError;

        private String _treatmentTypeCellValue;
        private PlanogramAssortmentProductBuddyTreatmentType? _treatmentType;
        private Single _treatmentTypePercentage;
        private String _treatmentTypePercentageError;
        private String _treatmentTypeError;

        private String _productAttributeTypeCellValue;
        private PlanogramAssortmentProductBuddyProductAttributeType? _productAttributeType;
        private String _productAttributeTypeError;

        private String _source1CellValue;
        private IPlanogramProductInfo _source1;
        private String _source1Error;
        private String _source1PercentageCellValue;
        private Single? _source1Percentage;
        private String _source1PercentageError;

        private String _source2CellValue;
        private IPlanogramProductInfo _source2;
        private String _source2Error;
        private String _source2PercentageCellValue;
        private Single? _source2Percentage;
        private String _source2PercentageError;

        private String _source3CellValue;
        private IPlanogramProductInfo _source3;
        private String _source3Error;
        private String _source3PercentageCellValue;
        private Single? _source3Percentage;
        private String _source3PercentageError;

        private String _source4CellValue;
        private IPlanogramProductInfo _source4;
        private String _source4Error;
        private String _source4PercentageCellValue;
        private Single? _source4Percentage;
        private String _source4PercentageError;

        private String _source5CellValue;
        private IPlanogramProductInfo _source5;
        private String _source5Error;
        private String _source5PercentageCellValue;
        private Single? _source5Percentage;
        private String _source5PercentageError;

        private Boolean _percentageCellIsOk = true;

        private Dictionary<PlanogramAssortmentProductBuddyTreatmentType, String> _availableTreatmentTypes = new Dictionary<PlanogramAssortmentProductBuddyTreatmentType, String>();

        #endregion

        #region Binding Property Paths

        //properties
        public static readonly PropertyPath IsNewPlaceholderRowProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.IsNewPlaceholderRow);
        public static readonly PropertyPath IsValidProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.IsValid);
        public static readonly PropertyPath TargetProductCellValueProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.TargetProductCellValue);
        public static readonly PropertyPath TargetProductProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.TargetProduct);
        public static readonly PropertyPath TargetProductErrorProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.TargetProductError);
        public static readonly PropertyPath SourceTypeCellValueProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.SourceTypeCellValue);
        public static readonly PropertyPath SourceTypeProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.SourceType);
        public static readonly PropertyPath SourceTypeErrorProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.SourceTypeError);
        public static readonly PropertyPath TreatmentTypeCellValueProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.TreatmentTypeCellValue);
        public static readonly PropertyPath TreatmentTypeProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.TreatmentType);
        public static readonly PropertyPath TreatmentTypePercentageProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.TreatmentTypePercentage);
        public static readonly PropertyPath TreatmentTypeErrorProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.TreatmentTypeError);
        public static readonly PropertyPath TreatmentTypePercentageErrorProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.TreatmentTypePercentageError);
        public static readonly PropertyPath ProductAttributeTypeCellValueProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.ProductAttributeTypeCellValue);
        public static readonly PropertyPath ProductAttributeTypeProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.ProductAttributeType);
        public static readonly PropertyPath ProductAttributeTypeErrorProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.ProductAttributeTypeError);

        public static readonly PropertyPath Source1CellValueProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source1CellValue);
        public static readonly PropertyPath Source1Property = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source1);
        public static readonly PropertyPath Source1ErrorProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source1Error);
        public static readonly PropertyPath Source1PercentageCellValueProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source1PercentageCellValue);
        public static readonly PropertyPath Source1PercentageProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source1Percentage);
        public static readonly PropertyPath Source1PercentageErrorProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source1PercentageError);

        public static readonly PropertyPath Source2CellValueProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source2CellValue);
        public static readonly PropertyPath Source2Property = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source2);
        public static readonly PropertyPath Source2ErrorProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source2Error);
        public static readonly PropertyPath Source2PercentageCellValueProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source2PercentageCellValue);
        public static readonly PropertyPath Source2PercentageProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source2Percentage);
        public static readonly PropertyPath Source2PercentageErrorProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source2PercentageError);

        public static readonly PropertyPath Source3CellValueProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source3CellValue);
        public static readonly PropertyPath Source3Property = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source3);
        public static readonly PropertyPath Source3ErrorProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source3Error);
        public static readonly PropertyPath Source3PercentageCellValueProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source3PercentageCellValue);
        public static readonly PropertyPath Source3PercentageProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source3Percentage);
        public static readonly PropertyPath Source3PercentageErrorProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source3PercentageError);

        public static readonly PropertyPath Source4CellValueProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source4CellValue);
        public static readonly PropertyPath Source4Property = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source4);
        public static readonly PropertyPath Source4ErrorProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source4Error);
        public static readonly PropertyPath Source4PercentageCellValueProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source4PercentageCellValue);
        public static readonly PropertyPath Source4PercentageProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source4Percentage);
        public static readonly PropertyPath Source4PercentageErrorProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source4PercentageError);

        public static readonly PropertyPath Source5CellValueProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source5CellValue);
        public static readonly PropertyPath Source5Property = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source5);
        public static readonly PropertyPath Source5ErrorProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source5Error);
        public static readonly PropertyPath Source5PercentageCellValueProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source5PercentageCellValue);
        public static readonly PropertyPath Source5PercentageProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source5Percentage);
        public static readonly PropertyPath Source5PercentageErrorProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.Source5PercentageError);
        public static readonly PropertyPath AvailableTreatmentTypesProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.AvailableTreatmentTypes);

        //commands
        public static readonly PropertyPath SelectTargetProductCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.SelectTargetProductCommand);
        public static readonly PropertyPath SelectSourceProductCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddRow>(p => p.SelectSourceProductCommand);

        #endregion

        #region Properties

        public Boolean IsNewPlaceholderRow
        {
            get { return _isNewPlaceholderRow; }
            set
            {
                _isNewPlaceholderRow = value;
                OnPropertyChanged(IsNewPlaceholderRowProperty);
            }
        }

        /// <summary>
        /// Returns true if this row is valid.
        /// </summary>
        public Boolean IsValid
        {
            get
            {
                if (this.TargetProductError != null) { return false; }

                if (this.SourceTypeError != null) { return false; }

                if (this.TreatmentTypeError != null) { return false; }

                if ((this.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg || this.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg)
                    && !((this.TreatmentTypePercentage > 0 && this.TreatmentTypePercentage <= 1)))
                {
                    return false;
                }

                if ((this.SourceType == PlanogramAssortmentProductBuddySourceType.Attribute) && (this.ProductAttributeTypeError != null))
                {
                    return false;
                }

                if (this.SourceType == PlanogramAssortmentProductBuddySourceType.Manual)
                {
                    if (this.Source1Error != null) { return false; }
                }

                if (!this._percentageCellIsOk)
                {
                    return false;
                }

                return true;
            }
        }

        #region TargetProduct

        /// <summary>
        /// Gets/Sets the target product value
        /// </summary>
        public String TargetProductCellValue
        {
            get { return _targetProductCellValue; }
            set
            {
                String finalValue = value.Trim();

                _targetProductCellValue = finalValue;
                OnPropertyChanged(TargetProductCellValueProperty);

                OnTargetProductCellValueChanged(finalValue);
            }
        }

        /// <summary>
        /// Returns the actual target product
        /// </summary>
        public PlanogramAssortmentProduct TargetProduct
        {
            get { return _targetProduct; }
            set
            {
                _targetProduct = value;
                OnPropertyChanged(TargetProductProperty);
            }
        }

        /// <summary>
        ///Gets the target product cell error.
        /// </summary>
        public String TargetProductError
        {
            get { return _targetProductError; }
            internal set
            {
                _targetProductError = value;
                OnPropertyChanged(TargetProductErrorProperty);
                OnPropertyChanged(IsValidProperty);
            }
        }

        #endregion

        #region SourceType

        public String SourceTypeCellValue
        {
            get { return _sourceTypeCellValue; }
            set
            {
                String finalValue = value.Trim();

                _sourceTypeCellValue = finalValue;
                OnPropertyChanged(SourceTypeCellValueProperty);

                OnSourceTypeCellValueChanged(finalValue);
            }
        }

        public PlanogramAssortmentProductBuddySourceType? SourceType
        {
            get { return _sourceType; }
            set
            {
                _sourceType = value;
                UpdateAvailableTreatmentTypes();
                OnPropertyChanged(SourceTypeProperty);
            }
        }

        public String SourceTypeError
        {
            get { return _sourceTypeError; }
            private set
            {
                _sourceTypeError = value;
                OnPropertyChanged(SourceTypeErrorProperty);
                OnPropertyChanged(IsValidProperty);
            }
        }

        #endregion

        #region TreatmentType

        public String TreatmentTypeCellValue
        {
            get { return _treatmentTypeCellValue; }
            set
            {
                String finalValue = value.Trim();

                _treatmentTypeCellValue = finalValue;
                OnPropertyChanged(TreatmentTypeCellValueProperty);

                OnTreatmentTypeCellValueChanged(finalValue);
            }
        }

        public PlanogramAssortmentProductBuddyTreatmentType? TreatmentType
        {
            get { return _treatmentType; }
            set
            {
                _treatmentType = value;
                OnPropertyChanged(TreatmentTypeProperty);
            }
        }

        public Single TreatmentTypePercentage
        {
            get { return _treatmentTypePercentage; }
            set
            {
                _treatmentTypePercentage = value;
                OnPropertyChanged(TreatmentTypePercentageProperty);
                OnTreatmentTypePercentageValueChanged(value);
            }
        }

        public String TreatmentTypePercentageError
        {
            get { return _treatmentTypePercentageError; }
            private set
            {
                _treatmentTypePercentageError = value;
                OnPropertyChanged(TreatmentTypePercentageErrorProperty);
                OnPropertyChanged(IsValidProperty);
            }
        }

        public String TreatmentTypeError
        {
            get { return _treatmentTypeError; }
            private set
            {
                _treatmentTypeError = value;
                OnPropertyChanged(TreatmentTypeErrorProperty);
                OnPropertyChanged(IsValidProperty);
            }
        }

        #endregion

        #region ProductAttributeType

        public String ProductAttributeTypeCellValue
        {
            get { return _productAttributeTypeCellValue; }
            set
            {
                String finalValue = value.Trim();

                _productAttributeTypeCellValue = finalValue;
                OnPropertyChanged(ProductAttributeTypeCellValueProperty);

                OnProductAttributeTypeCellValueChanged(finalValue);
            }
        }

        public PlanogramAssortmentProductBuddyProductAttributeType? ProductAttributeType
        {
            get { return _productAttributeType; }
            private set
            {
                _productAttributeType = value;
                OnPropertyChanged(ProductAttributeTypeProperty);
            }
        }

        public String ProductAttributeTypeError
        {
            get { return _productAttributeTypeError; }
            private set
            {
                _productAttributeTypeError = value;
                OnPropertyChanged(ProductAttributeTypeErrorProperty);
                OnPropertyChanged(IsValidProperty);
            }

        }

        #endregion

        #region Source1

        public String Source1CellValue
        {
            get { return _source1CellValue; }
            set
            {
                String finalValue = value.Trim();

                _source1CellValue = finalValue;
                OnPropertyChanged(Source1CellValueProperty);

                OnSourceCellValueChanged(/*sourceNum*/1, finalValue);
            }
        }

        public IPlanogramProductInfo Source1
        {
            get { return _source1; }
            set
            {
                _source1 = value;
                OnPropertyChanged(Source1Property);
            }
        }

        public String Source1Error
        {
            get { return _source1Error; }
            internal set
            {
                _source1Error = value;
                OnPropertyChanged(Source1ErrorProperty);
                OnPropertyChanged(IsValidProperty);
            }
        }

        #endregion

        #region Source1Percentage

        public String Source1PercentageCellValue
        {
            get { return _source1PercentageCellValue; }
            set
            {
                String finalValue = value.Trim();
                finalValue = finalValue.TrimEnd(new Char[] { '%' });
                finalValue = finalValue.Trim();

                _source1PercentageCellValue = finalValue;
                OnPropertyChanged(Source1PercentageCellValueProperty);

                OnSourcePercentageCellValueChanged(/*sourceNum*/1, finalValue);
            }
        }

        public Single? Source1Percentage
        {
            get { return _source1Percentage; }
            set
            {
                _source1Percentage = value;
                OnPropertyChanged(Source1PercentageProperty);
            }
        }

        public String Source1PercentageError
        {
            get { return _source1PercentageError; }
            private set
            {
                _source1PercentageError = value;
                OnPropertyChanged(Source1PercentageErrorProperty);
                OnPropertyChanged(IsValidProperty);
            }
        }


        #endregion

        #region Source2

        public String Source2CellValue
        {
            get { return _source2CellValue; }
            set
            {
                String finalValue = value.Trim();

                _source2CellValue = finalValue;
                OnPropertyChanged(Source2CellValueProperty);

                OnSourceCellValueChanged(/*sourceNum*/2, finalValue);
            }
        }

        public IPlanogramProductInfo Source2
        {
            get { return _source2; }
            private set
            {
                _source2 = value;
                OnPropertyChanged(Source2Property);
            }
        }

        public String Source2Error
        {
            get { return _source2Error; }
            private set
            {
                _source2Error = value;
                OnPropertyChanged(Source2ErrorProperty);
                OnPropertyChanged(IsValidProperty);
            }
        }

        #endregion

        #region Source2Percentage

        public String Source2PercentageCellValue
        {
            get { return _source2PercentageCellValue; }
            set
            {
                String finalValue = value.Trim();
                finalValue = finalValue.TrimEnd(new Char[] { '%' });
                finalValue = finalValue.Trim();

                _source2PercentageCellValue = finalValue;
                OnPropertyChanged(Source2PercentageCellValueProperty);

                OnSourcePercentageCellValueChanged(/*sourceNum*/2, finalValue);
            }
        }

        public Single? Source2Percentage
        {
            get { return _source2Percentage; }
            private set
            {
                _source2Percentage = value;
                OnPropertyChanged(Source2PercentageProperty);
            }
        }

        public String Source2PercentageError
        {
            get { return _source2PercentageError; }
            private set
            {
                _source2PercentageError = value;
                OnPropertyChanged(Source2PercentageErrorProperty);
                OnPropertyChanged(IsValidProperty);
            }
        }


        #endregion

        #region Source3

        public String Source3CellValue
        {
            get { return _source3CellValue; }
            set
            {
                String finalValue = value.Trim();

                _source3CellValue = finalValue;
                OnPropertyChanged(Source3CellValueProperty);

                OnSourceCellValueChanged(/*sourceNum*/3, finalValue);
            }
        }

        public IPlanogramProductInfo Source3
        {
            get { return _source3; }
            private set
            {
                _source3 = value;
                OnPropertyChanged(Source3Property);
            }
        }

        public String Source3Error
        {
            get { return _source3Error; }
            private set
            {
                _source3Error = value;
                OnPropertyChanged(Source3ErrorProperty);
                OnPropertyChanged(IsValidProperty);
            }
        }

        #endregion

        #region Source3Percentage

        public String Source3PercentageCellValue
        {
            get { return _source3PercentageCellValue; }
            set
            {
                String finalValue = value.Trim();
                finalValue = finalValue.TrimEnd(new Char[] { '%' });
                finalValue = finalValue.Trim();

                _source3PercentageCellValue = finalValue;
                OnPropertyChanged(Source3PercentageCellValueProperty);

                OnSourcePercentageCellValueChanged(/*sourceNum*/3, finalValue);
            }
        }

        public Single? Source3Percentage
        {
            get { return _source3Percentage; }
            private set
            {
                _source3Percentage = value;
                OnPropertyChanged(Source3PercentageProperty);
            }
        }

        public String Source3PercentageError
        {
            get { return _source3PercentageError; }
            private set
            {
                _source3PercentageError = value;
                OnPropertyChanged(Source3PercentageErrorProperty);
                OnPropertyChanged(IsValidProperty);
            }
        }


        #endregion

        #region Source4

        public String Source4CellValue
        {
            get { return _source4CellValue; }
            set
            {
                String finalValue = value.Trim();

                _source4CellValue = finalValue;
                OnPropertyChanged(Source4CellValueProperty);

                OnSourceCellValueChanged(/*sourceNum*/4, finalValue);
            }
        }

        public IPlanogramProductInfo Source4
        {
            get { return _source4; }
            private set
            {
                _source4 = value;
                OnPropertyChanged(Source4Property);
            }
        }

        public String Source4Error
        {
            get { return _source4Error; }
            private set
            {
                _source4Error = value;
                OnPropertyChanged(Source4ErrorProperty);
                OnPropertyChanged(IsValidProperty);
            }
        }

        #endregion

        #region Source4Percentage

        public String Source4PercentageCellValue
        {
            get { return _source4PercentageCellValue; }
            set
            {
                String finalValue = value.Trim();
                finalValue = finalValue.TrimEnd(new Char[] { '%' });
                finalValue = finalValue.Trim();

                _source4PercentageCellValue = finalValue;
                OnPropertyChanged(Source4PercentageCellValueProperty);

                OnSourcePercentageCellValueChanged(/*sourceNum*/4, finalValue);
            }
        }

        public Single? Source4Percentage
        {
            get { return _source4Percentage; }
            private set
            {
                _source4Percentage = value;
                OnPropertyChanged(Source4PercentageProperty);
            }
        }

        public String Source4PercentageError
        {
            get { return _source4PercentageError; }
            private set
            {
                _source4PercentageError = value;
                OnPropertyChanged(Source4PercentageErrorProperty);
                OnPropertyChanged(IsValidProperty);
            }
        }


        #endregion

        #region Source5

        public String Source5CellValue
        {
            get { return _source5CellValue; }
            set
            {
                String finalValue = value.Trim();

                _source5CellValue = finalValue;
                OnPropertyChanged(Source5CellValueProperty);

                OnSourceCellValueChanged(/*sourceNum*/5, finalValue);
            }
        }

        public IPlanogramProductInfo Source5
        {
            get { return _source5; }
            private set
            {
                _source5 = value;
                OnPropertyChanged(Source5Property);
            }
        }

        public String Source5Error
        {
            get { return _source5Error; }
            private set
            {
                _source5Error = value;
                OnPropertyChanged(Source5ErrorProperty);
                OnPropertyChanged(IsValidProperty);
            }
        }

        #endregion

        #region Source5Percentage

        public String Source5PercentageCellValue
        {
            get { return _source5PercentageCellValue; }
            set
            {
                String finalValue = value.Trim();
                finalValue = finalValue.TrimEnd(new Char[] { '%' });
                finalValue = finalValue.Trim();

                _source5PercentageCellValue = finalValue;
                OnPropertyChanged(Source5PercentageCellValueProperty);

                OnSourcePercentageCellValueChanged(/*sourceNum*/5, finalValue);
            }
        }

        public Single? Source5Percentage
        {
            get { return _source5Percentage; }
            private set
            {
                _source5Percentage = value;
                OnPropertyChanged(Source5PercentageProperty);
            }
        }

        public String Source5PercentageError
        {
            get { return _source5PercentageError; }
            private set
            {
                _source5PercentageError = value;
                OnPropertyChanged(Source5PercentageErrorProperty);
                OnPropertyChanged(IsValidProperty);
            }
        }


        #endregion

        /// <summary>
        /// The available product buddy treatment types
        /// </summary>
        public Dictionary<PlanogramAssortmentProductBuddyTreatmentType, String> AvailableTreatmentTypes
        {
            get
            {
                return _availableTreatmentTypes;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="scenario"></param>
        public AssortmentProductBuddyAdvancedAddRow(PlanogramAssortment currentAssortment, Dictionary<String, IPlanogramProductInfo> availableSourceProductLookup, IEnumerable<IPlanItem> planItems, Action<AssortmentProductBuddyAdvancedAddRow> selectTargetProductMethod)
        {
            _selectTargetProductMethod = selectTargetProductMethod;
            _isNewPlaceholderRow = true;

            _planItems = planItems;
            _currentAssortment = currentAssortment;
            _availableSourceProductLookup = availableSourceProductLookup;

            _targetProductError = Message.AssortmentProductBuddyAdvancedAdd_ErrorTargetProductInvalid;

            //default the source type
            _sourceType = PlanogramAssortmentProductBuddySourceType.Attribute;
            _sourceTypeCellValue = PlanogramAssortmentProductBuddySourceTypeHelper.FriendlyNames[_sourceType.Value];

            //default treatment type
            _treatmentType = PlanogramAssortmentProductBuddyTreatmentType.Avg;
            _treatmentTypeCellValue = PlanogramAssortmentProductBuddyTreatmentTypeHelper.FriendlyNames[_treatmentType.Value];

            //default product attribute
            _productAttributeType = PlanogramAssortmentProductBuddyProductAttributeType.Brand;
            _productAttributeTypeCellValue = PlanogramAssortmentProductBuddyProductAttributeTypeHelper.FriendlyNames[_productAttributeType.Value];

            //default source values
            _source1Error = Message.AssortmentProductBuddyAdvancedAdd_ErrorSourceProductInvalid;

            _availableTreatmentTypes.Add(PlanogramAssortmentProductBuddyTreatmentType.Avg, PlanogramAssortmentProductBuddyTreatmentTypeHelper.FriendlyNames[PlanogramAssortmentProductBuddyTreatmentType.Avg]);
            _availableTreatmentTypes.Add(PlanogramAssortmentProductBuddyTreatmentType.Sum, PlanogramAssortmentProductBuddyTreatmentTypeHelper.FriendlyNames[PlanogramAssortmentProductBuddyTreatmentType.Sum]);
            _availableTreatmentTypes.Add(PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg, PlanogramAssortmentProductBuddyTreatmentTypeHelper.FriendlyNames[PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg]);
            _availableTreatmentTypes.Add(PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg, PlanogramAssortmentProductBuddyTreatmentTypeHelper.FriendlyNames[PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg]);
        }

        #endregion

        #region Commands

        #region SelectTargetProduct

        private RelayCommand _selectTargetProductCommand;

        /// <summary>
        /// Shows the scenario product selection window.
        /// </summary>
        public RelayCommand SelectTargetProductCommand
        {
            get
            {
                if (_selectTargetProductCommand == null)
                {
                    _selectTargetProductCommand = new RelayCommand(
                        p => SelectTargetProduct_Executed())
                    {
                        FriendlyName = ".."
                    };
                }
                return _selectTargetProductCommand;
            }
        }

        private void SelectTargetProduct_Executed()
        {
            if (_selectTargetProductMethod != null) _selectTargetProductMethod.Invoke(this);
        }

        #endregion

        #region SelectSourceProduct

        private RelayCommand _selectSourceProductCommand;

        /// <summary>
        /// Shows the dialog to select a source product
        /// <param type="Int32">the source number</param>
        /// </summary>
        public RelayCommand SelectSourceProductCommand
        {
            get
            {
                if (_selectSourceProductCommand == null)
                {
                    _selectSourceProductCommand = new RelayCommand(
                        p => SelectSourceProduct_Executed(p),
                        p => SelectSourceProduct_CanExecute(p))
                    {
                        FriendlyName = ".."
                    };

                }
                return _selectSourceProductCommand;
            }
        }

        private Boolean SelectSourceProduct_CanExecute(Object source)
        {
            Int32 sourceNum;
            if (source != null)
            {
                if (Int32.TryParse(Convert.ToString(source), out sourceNum))
                {
                    if (sourceNum > 5 || sourceNum < 1)
                    {
                        return false;
                    }
                    return true;
                }
            }
            return false;
        }

        private void SelectSourceProduct_Executed(Object source)
        {
            Int32 sourceNum;
            Int32 maximumSelectionCount = 5;
            if (source != null)
            {
                if (Int32.TryParse(Convert.ToString(source), out sourceNum))
                {
                    //determine gtins to be excluded
                    List<String> excludeGtins = new List<String>();
                    if (this.Source1 != null && sourceNum != 1)
                    {
                        excludeGtins.Add(this.Source1.Gtin);
                        maximumSelectionCount--;
                    }
                    if (this.Source2 != null && sourceNum != 2)
                    {
                        excludeGtins.Add(this.Source2.Gtin);
                        maximumSelectionCount--;
                    }
                    if (this.Source3 != null && sourceNum != 3)
                    {
                        excludeGtins.Add(this.Source3.Gtin);
                        maximumSelectionCount--;
                    }
                    if (this.Source4 != null && sourceNum != 4)
                    {
                        excludeGtins.Add(this.Source4.Gtin);
                        maximumSelectionCount--;
                    }
                    if (this.Source5 != null && sourceNum != 5)
                    {
                        excludeGtins.Add(this.Source5.Gtin);
                        maximumSelectionCount--;
                    }

                    List<IPlanogramProductInfo> results = new List<IPlanogramProductInfo>();

                    if (App.ViewState.IsConnectedToRepository)
                    {
                        ProductSelectorHelper productSelectorHelper = new ProductSelectorHelper();
                        productSelectorHelper.IsMultiSelectEnabled = true;
                        productSelectorHelper.ExcludedProducts.AddRange(excludeGtins);
                        productSelectorHelper.MaximumResultCount = maximumSelectionCount;

                        //Get the initial category to use
                        String categoryCode = String.Empty;
                        if (_currentAssortment != null)
                        {
                            var parentPlan = _currentAssortment.Parent as Planogram;
                            categoryCode = parentPlan != null ? parentPlan.CategoryCode : String.Empty;
                        }

                        var winModel = new ProductSelectorViewModel(productSelectorHelper, categoryCode);
                        winModel.ShowDialog();

                        if (winModel.DialogResult == true)
                        {
                            if (winModel.AssignedProducts != null)
                            {
                                results.AddRange(winModel.AssignedProducts);
                            }
                        }
                    }
                    else
                    {
                        AssortmentProductSelectionWindow win = new AssortmentProductSelectionWindow(
                            this._currentAssortment.Products.Select(p => new PlanogramAssortmentProductView(p, _planItems.FirstOrDefault(planItems => planItems.Product.Gtin == p.Gtin))));
                        CommonHelper.GetWindowService().ShowDialog<AssortmentProductSelectionWindow>(win);

                        if (win.DialogResult == true && win.SelectionResult != null && win.SelectionResult.FirstOrDefault() != null)
                        {
                            results.Add(win.SelectionResult.FirstOrDefault().Product);
                        }
                    }

                    if (results.Any())
                    {                                                   
                        switch (sourceNum)
                        {
                            case 1:
                                this.Source1CellValue = results[0].Gtin;
                                break;

                            case 2:
                                this.Source2CellValue = results[0].Gtin;
                                break;

                            case 3:
                                this.Source3CellValue = results[0].Gtin;
                                break;

                            case 4:
                                this.Source4CellValue = results[0].Gtin;
                                break;

                            case 5:
                                this.Source5CellValue = results[0].Gtin;
                                break;                            
                        }
                        if (results.Count() > 1)
                        {
                            foreach (Product prod in results.Skip(1))
                            {                               
                                if (this.Source1CellValue == null)
                                {
                                    this.Source1CellValue = prod.Gtin;
                                    continue;
                                }

                                if (this.Source2CellValue == null)
                                {
                                    this.Source2CellValue = prod.Gtin;
                                    continue;
                                }

                                if (this.Source3CellValue == null)
                                {
                                    this.Source3CellValue = prod.Gtin;
                                    continue;
                                }

                                if (this.Source4CellValue == null)
                                {
                                    this.Source4CellValue = prod.Gtin;
                                    continue;
                                }

                                if (this.Source5CellValue == null)
                                {
                                    this.Source5CellValue = prod.Gtin;
                                    continue;
                                }
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        private void OnTargetProductCellValueChanged(String newValue)
        {
            PlanogramAssortmentProduct targetProduct = null;
            String error = null;

            if (!String.IsNullOrEmpty(newValue))
            {

                //attempt to get the product for the cell value
                String targetGtin = newValue.Split(' ')[0];
                targetProduct = _currentAssortment.Products.FirstOrDefault(p => p.Gtin.Equals(targetGtin));

                if (targetProduct != null)
                {
                    if (newValue != targetProduct.ToString())
                    {
                        //update the cell value silently.
                        _targetProductCellValue = targetProduct.ToString();
                        OnPropertyChanged(TargetProductCellValueProperty);
                    }
                }
                else
                {
                    error = Message.AssortmentProductBuddyAdvancedAdd_ErrorTargetProductInvalid;
                }

            }

            this.TargetProduct = targetProduct;
            this.TargetProductError = error;
            this.IsNewPlaceholderRow = false;


            //check none of the source products are the same
            if (this.TargetProduct != null)
            {
                if (this.Source1 == this.TargetProduct)
                {
                    this.Source1Error = Message.AssortmentProductBuddyAdvancedAdd_ErrorSourceIsTarget;
                }
                if (this.Source2 == this.TargetProduct)
                {
                    this.Source2Error = Message.AssortmentProductBuddyAdvancedAdd_ErrorSourceIsTarget;
                }
                if (this.Source3 == this.TargetProduct)
                {
                    this.Source3Error = Message.AssortmentProductBuddyAdvancedAdd_ErrorSourceIsTarget;
                }
                if (this.Source4 == this.TargetProduct)
                {
                    this.Source4Error = Message.AssortmentProductBuddyAdvancedAdd_ErrorSourceIsTarget;
                }
                if (this.Source5 == this.TargetProduct)
                {
                    this.Source5Error = Message.AssortmentProductBuddyAdvancedAdd_ErrorSourceIsTarget;
                }
            }
        }

        private void OnSourceTypeCellValueChanged(String newValue)
        {
            PlanogramAssortmentProductBuddySourceType? sourceType = null;
            String error = null;

            if (!String.IsNullOrEmpty(newValue))
            {
                sourceType = PlanogramAssortmentProductBuddySourceTypeHelper.ParseFromFriendlyName(newValue, /*closestmatch*/true);
                if (sourceType != null)
                {
                    //update the cell value silently
                    _sourceTypeCellValue = PlanogramAssortmentProductBuddySourceTypeHelper.FriendlyNames[sourceType.Value];
                    OnPropertyChanged(SourceTypeCellValueProperty);
                }
                else
                {
                    error = Message.AssortmentProductBuddyAdvancedAdd_ErrorInvalidValue;
                }
            }
            else
            {
                error = Message.AssortmentProductBuddyAdvancedAdd_ErrorInvalidValue;
            }

            //update values
            this.SourceType = sourceType;
            this.SourceTypeError = error;
        }

        private void OnTreatmentTypeCellValueChanged(String newValue)
        {
            PlanogramAssortmentProductBuddyTreatmentType? treatmentType = null;
            String error = null;

            if (!String.IsNullOrEmpty(newValue))
            {
                treatmentType = PlanogramAssortmentProductBuddyTreatmentTypeHelper.ParseFromFriendlyName(newValue, /*closestmatch*/true);
                if (treatmentType != null)
                {
                    //update the cell value silently
                    _treatmentTypeCellValue = PlanogramAssortmentProductBuddyTreatmentTypeHelper.FriendlyNames[treatmentType.Value];
                    OnPropertyChanged(TreatmentTypeCellValueProperty);
                }
                else
                {
                    error = Message.AssortmentProductBuddyAdvancedAdd_ErrorInvalidValue;
                }
            }
            else
            {
                error = Message.AssortmentProductBuddyAdvancedAdd_ErrorInvalidValue;
            }

            //update values
            this.TreatmentType = treatmentType;
            this.TreatmentTypeError = error;
        }

        private void OnProductAttributeTypeCellValueChanged(String newValue)
        {
            PlanogramAssortmentProductBuddyProductAttributeType? productAttributeType = null;
            String error = null;

            if (!String.IsNullOrEmpty(newValue))
            {
                productAttributeType = PlanogramAssortmentProductBuddyProductAttributeTypeHelper.ParseFromFriendlyName(newValue, /*closestmatch*/true);
                if (productAttributeType != null)
                {
                    //update the cell value silently
                    _productAttributeTypeCellValue = PlanogramAssortmentProductBuddyProductAttributeTypeHelper.FriendlyNames[productAttributeType.Value];
                    OnPropertyChanged(ProductAttributeTypeCellValueProperty);
                }
                else
                {
                    error = Message.AssortmentProductBuddyAdvancedAdd_ErrorInvalidValue;
                }
            }
            else
            {
                error = Message.AssortmentProductBuddyAdvancedAdd_ErrorInvalidValue;
            }

            //update values
            this.ProductAttributeType = productAttributeType;
            this.ProductAttributeTypeError = error;
        }

        private void OnSourceCellValueChanged(Int32 sourceNum, String newValue)
        {
            IPlanogramProductInfo sourceProduct = null;
            String error = null;

            if (!String.IsNullOrEmpty(newValue))
            {
                //attempt to get the product for the cell value
                String sourceGtin = newValue.Split(' ')[0];
                if (_availableSourceProductLookup.TryGetValue(sourceGtin, out sourceProduct))
                {
                    if (newValue != sourceProduct.ToString())
                    {
                        //update the cell value silently.
                        switch (sourceNum)
                        {
                            case 1:
                                _source1CellValue = sourceProduct.ToString();
                                OnPropertyChanged(Source1CellValueProperty);
                                if (sourceProduct != null && _source1Percentage == null)
                                {
                                    this.Source1PercentageCellValue = "100";
                                }
                                break;
                            case 2:
                                _source2CellValue = sourceProduct.ToString();
                                OnPropertyChanged(Source2CellValueProperty);
                                if (sourceProduct != null && _source2Percentage == null)
                                {
                                    this.Source2PercentageCellValue = "100";
                                }
                                break;
                            case 3:
                                _source3CellValue = sourceProduct.ToString();
                                OnPropertyChanged(Source3CellValueProperty);
                                if (sourceProduct != null && _source3Percentage == null)
                                {
                                    this.Source3PercentageCellValue = "100";
                                }
                                break;
                            case 4:
                                _source4CellValue = sourceProduct.ToString();
                                OnPropertyChanged(Source4CellValueProperty);
                                if (sourceProduct != null && _source4Percentage == null)
                                {
                                    this.Source4PercentageCellValue = "100";
                                }
                                break;
                            case 5:
                                _source5CellValue = sourceProduct.ToString();
                                OnPropertyChanged(Source5CellValueProperty);
                                if (sourceProduct != null && _source5Percentage == null)
                                {
                                    this.Source5PercentageCellValue = "100";
                                }
                                break;
                        }
                    }
                }
                else
                {
                    error = Message.AssortmentProductBuddyAdvancedAdd_ErrorSourceProductInvalid;
                }

            }

            //set the values
            switch (sourceNum)
            {
                case 1:
                    this.Source1 = sourceProduct;
                    this.Source1Error = error;
                    break;

                case 2:
                    this.Source2 = sourceProduct;
                    this.Source2Error = error;
                    break;

                case 3:
                    this.Source3 = sourceProduct;
                    this.Source3Error = error;
                    break;

                case 4:
                    this.Source4 = sourceProduct;
                    this.Source4Error = error;
                    break;

                case 5:
                    this.Source5 = sourceProduct;
                    this.Source5Error = error;
                    break;
            }


        }

        private void OnTreatmentTypePercentageValueChanged(Single newValue)
        {
            String error = null;

            //check the bounds
            if (newValue > 0 && newValue <= 1)
            {
                _percentageCellIsOk = true;
            }
            else
            {
                _percentageCellIsOk = false;
                error = Message.AssortmentProductBuddyAdvancedAdd_ErrorTreatmentTypePercentageInvalid;
            }

            this.TreatmentTypePercentageError = error;
        }

        private void OnSourcePercentageCellValueChanged(Int32 sourceNum, String newValue)
        {
            Single? percentage = null;
            String error = null;

            if (!String.IsNullOrEmpty(newValue))
            {
                //parse the string
                Single val;

                if (Single.TryParse(newValue, out val))
                {
                    //check the bounds
                    if (val >= 1 && val <= 1000)
                    {
                        _percentageCellIsOk = true;
                        percentage = (Single)(val / 100.0);

                        //update the cell value
                        switch (sourceNum)
                        {
                            case 1:
                                _source1PercentageCellValue = (val / 100).ToString("P0", CultureInfo.CurrentCulture);
                                OnPropertyChanged(Source1PercentageCellValueProperty);
                                break;

                            case 2:
                                _source2PercentageCellValue = (val / 100).ToString("P0", CultureInfo.CurrentCulture);
                                OnPropertyChanged(Source2PercentageCellValueProperty);
                                break;

                            case 3:
                                _source3PercentageCellValue = (val / 100).ToString("P0", CultureInfo.CurrentCulture);
                                OnPropertyChanged(Source3PercentageCellValueProperty);
                                break;

                            case 4:
                                _source4PercentageCellValue = (val / 100).ToString("P0", CultureInfo.CurrentCulture);
                                OnPropertyChanged(Source4PercentageCellValueProperty);
                                break;

                            case 5:
                                _source5PercentageCellValue = (val / 100).ToString("P0", CultureInfo.CurrentCulture);
                                OnPropertyChanged(Source5PercentageCellValueProperty);
                                break;
                        }
                    }
                    else
                    {
                        _percentageCellIsOk = false;
                        error = Message.AssortmentProductBuddyAdvancedAdd_ErrorPercentageInvalid;
                    }
                }
                else
                {
                    _percentageCellIsOk = false;
                    error = Message.AssortmentProductBuddyAdvancedAdd_ErrorPercentageInvalid;
                }
            }
            else
            {
                _percentageCellIsOk = false;
                error = Message.AssortmentProductBuddyAdvancedAdd_ErrorPercentageInvalid;
            }

            switch (sourceNum)
            {
                case 1:
                    this.Source1Percentage = percentage;
                    this.Source1PercentageError = error;
                    break;

                case 2:
                    this.Source2Percentage = percentage;
                    this.Source2PercentageError = error;
                    break;

                case 3:
                    this.Source3Percentage = percentage;
                    this.Source3PercentageError = error;
                    break;

                case 4:
                    this.Source4Percentage = percentage;
                    this.Source4PercentageError = error;
                    break;

                case 5:
                    this.Source5Percentage = percentage;
                    this.Source5PercentageError = error;
                    break;
            }



        }

        #endregion

        #region IDisposable

        protected override void Dispose(bool disposing)
        {
            this._targetProduct = null;
            this._currentAssortment = null;
            _availableSourceProductLookup = null;
            _source1 = null;
            _source2 = null;
            _source3 = null;
            _source4 = null;
            _source5 = null;
        }

        #endregion

        #region Static Helpers

        /// <summary>
        /// Creates a new product buddy from the given row.
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static PlanogramAssortmentProductBuddy CreateProductBuddy(AssortmentProductBuddyAdvancedAddRow row)
        {
            PlanogramAssortmentProductBuddy nodeBuddy = PlanogramAssortmentProductBuddy.NewPlanogramAssortmentProductBuddy(row.TargetProduct.Gtin);

            nodeBuddy.SourceType = row.SourceType.Value;
            nodeBuddy.TreatmentType = row.TreatmentType.Value;
            nodeBuddy.TreatmentTypePercentage = row.TreatmentTypePercentage;

            if (nodeBuddy.SourceType == PlanogramAssortmentProductBuddySourceType.Attribute)
            {
                nodeBuddy.ProductAttributeType = row.ProductAttributeType.Value;
            }
            else if (nodeBuddy.SourceType == PlanogramAssortmentProductBuddySourceType.Manual)
            {
                if (row.Source1 != null)
                {
                    nodeBuddy.S1ProductGtin = row.Source1.Gtin;
                    nodeBuddy.S1Percentage = row.Source1Percentage;
                }

                if (row.Source2 != null)
                {
                    nodeBuddy.S2ProductGtin = row.Source2.Gtin;
                    nodeBuddy.S2Percentage = row.Source2Percentage;
                }

                if (row.Source3 != null)
                {
                    nodeBuddy.S3ProductGtin = row.Source3.Gtin;
                    nodeBuddy.S3Percentage = row.Source3Percentage;
                }

                if (row.Source4 != null)
                {
                    nodeBuddy.S4ProductGtin = row.Source4.Gtin;
                    nodeBuddy.S4Percentage = row.Source4Percentage;
                }

                if (row.Source5 != null)
                {
                    nodeBuddy.S5ProductGtin = row.Source5.Gtin;
                    nodeBuddy.S5Percentage = row.Source5Percentage;
                }
            }

            return nodeBuddy;
        }

        #endregion

        #region Methods

        private void UpdateAvailableTreatmentTypes()
        {
            PlanogramAssortmentProductBuddyTreatmentType? previousTreatmentType = _treatmentType;

            if (_sourceType != null)
            {
                _availableTreatmentTypes = new Dictionary<PlanogramAssortmentProductBuddyTreatmentType, String>();

                if (_sourceType == PlanogramAssortmentProductBuddySourceType.Manual)
                {
                    _availableTreatmentTypes.Add(PlanogramAssortmentProductBuddyTreatmentType.Avg, PlanogramAssortmentProductBuddyTreatmentTypeHelper.FriendlyNames[PlanogramAssortmentProductBuddyTreatmentType.Avg]);
                    _availableTreatmentTypes.Add(PlanogramAssortmentProductBuddyTreatmentType.Sum, PlanogramAssortmentProductBuddyTreatmentTypeHelper.FriendlyNames[PlanogramAssortmentProductBuddyTreatmentType.Sum]);
                    if (previousTreatmentType != null && (previousTreatmentType == PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg || previousTreatmentType == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg))
                    {
                        previousTreatmentType = PlanogramAssortmentProductBuddyTreatmentType.Avg;
                    }
                }
                else
                {
                    _availableTreatmentTypes.Add(PlanogramAssortmentProductBuddyTreatmentType.Avg, PlanogramAssortmentProductBuddyTreatmentTypeHelper.FriendlyNames[PlanogramAssortmentProductBuddyTreatmentType.Avg]);
                    _availableTreatmentTypes.Add(PlanogramAssortmentProductBuddyTreatmentType.Sum, PlanogramAssortmentProductBuddyTreatmentTypeHelper.FriendlyNames[PlanogramAssortmentProductBuddyTreatmentType.Sum]);
                    _availableTreatmentTypes.Add(PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg, PlanogramAssortmentProductBuddyTreatmentTypeHelper.FriendlyNames[PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg]);
                    _availableTreatmentTypes.Add(PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg, PlanogramAssortmentProductBuddyTreatmentTypeHelper.FriendlyNames[PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg]);
                }
            }

            OnPropertyChanged(AvailableTreatmentTypesProperty);

            this.TreatmentTypeCellValue = previousTreatmentType != null ? PlanogramAssortmentProductBuddyTreatmentTypeHelper.FriendlyNames[(PlanogramAssortmentProductBuddyTreatmentType)previousTreatmentType] : PlanogramAssortmentProductBuddyTreatmentTypeHelper.FriendlyNames[PlanogramAssortmentProductBuddyTreatmentType.Avg];
        }

        #endregion
    }
}
