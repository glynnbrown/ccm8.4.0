﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
// V8-31950 : A.Probyn
//  Updated for multi select products.
//  Updated to remove App.ShowWindow on assortment product selector
// V8-32396 : A.Probyn
//  Updated to allow selection from outside the assortment for buddies
// V8-32718 : D.Pleasance
//  Added planItems to constructor. These are passed through to enable displaying of product / position attributes.
// V8-32823 : A.Heathcote 
//  Added the ScreenKey.
// CCM-14012 : A.Heathcote
//   AddNewBuddies_Executed() has been changed to use the new product selector constructor.
// CCM-18489 : A.Heathcote
//  Changed the error in IDataErrorInfo Members for percentages to allow 1000% instead of 100%
// CCM-18458 : A.Heathcote
//  Added the excluded list to satisfy the edited constructor.
//CCM-14014 : M.Pettit
//  AddNewBuddies_Executed() changed so that list of available products is a list of PlanogramAssortmentProducts from the planogram, not newly created objects.
//  This is so that if the user has custom fields visible, they are correctly populated with PlanogramProduct or AssortmentProduct data
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using System.Diagnostics;
using Galleria.Framework.Collections;
using System.Windows.Controls;
using Galleria.Framework.Controls.Wpf;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Globalization;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Ccm.Common.Wpf.Selectors;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.ProductBuddy
{
    /// <summary>
    /// ViewModel controller for AssortmentProductBuddyWindow
    /// Allows the user to view existing product buddies for the scenario.
    /// Modal.
    /// </summary>
    public sealed class AssortmentProductBuddyViewModel : ViewModelAttachedControlObject<AssortmentProductBuddyWindow>, IDataErrorInfo
    {
        #region Constants
        public const String ScreenKey = "AssortmentProductBuddyWindow";
        #endregion 

        #region Fields

        private IEnumerable<IPlanItem> _planItems;
        private PlanogramAssortment _currentAssortment;
        private Dictionary<String, IPlanogramProductInfo> _availableSourceProductLookup = new Dictionary<String, IPlanogramProductInfo>();

        private BulkObservableCollection<AssortmentProductBuddyRow> _assortmentProductBuddyRows = new BulkObservableCollection<AssortmentProductBuddyRow>();
        private ReadOnlyBulkObservableCollection<AssortmentProductBuddyRow> _assortmentProductBuddyRowsRO;
        private BulkObservableCollection<AssortmentProductBuddyRow> _selectedAssortmentProductBuddyRows = new BulkObservableCollection<AssortmentProductBuddyRow>();

        private List<AssortmentProductBuddyRow> _deletedAssortmentProductBuddyRows = new List<AssortmentProductBuddyRow>();

        private PlanogramAssortmentProductBuddySourceType? _editSourceType;
        private PlanogramAssortmentProductBuddyTreatmentType? _editTreatmentType;
        private Single _editTreatmentTypePercentage;
        private PlanogramAssortmentProductBuddyProductAttributeType? _editAttributeType;

        private BulkObservableCollection<ProductBuddySourceRow> _editSourceRows = new BulkObservableCollection<ProductBuddySourceRow>();
        private ReadOnlyBulkObservableCollection<ProductBuddySourceRow> _editSourceRowsRO;
        private Boolean _isSourceRowsOverwrite;
        private BulkObservableCollection<PlanogramAssortmentProductBuddyTreatmentType> _availableAssortmentProductBuddyTreatmentTypes = new BulkObservableCollection<PlanogramAssortmentProductBuddyTreatmentType>();
        private ReadOnlyBulkObservableCollection<PlanogramAssortmentProductBuddyTreatmentType> _availableAssortmentProductBuddyTreatmentTypesRO;
        private String _categoryCode;
        #endregion

        #region Binding Property Paths

        //properties
        public static readonly PropertyPath AssortmentProductBuddyRowsProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyViewModel>(p => p.AssortmentProductBuddyRows);
        public static readonly PropertyPath SelectedAssortmentProductBuddyRowsProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyViewModel>(p => p.SelectedAssortmentProductBuddyRows);
        public static readonly PropertyPath EditSourceTypeProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyViewModel>(p => p.EditSourceType);
        public static readonly PropertyPath EditTreatmentTypeProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyViewModel>(p => p.EditTreatmentType);
        public static readonly PropertyPath EditTreatmentTypePercentageProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyViewModel>(p => p.EditTreatmentTypePercentage);
        public static readonly PropertyPath EditAttributeTypeProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyViewModel>(p => p.EditAttributeType);
        public static readonly PropertyPath EditSourceRowsProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyViewModel>(p => p.EditSourceRows);
        public static readonly PropertyPath IsSourceRowsOverwriteProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyViewModel>(p => p.IsSourceRowsOverwrite);
        public static readonly PropertyPath AvailableAssortmentProductBuddyTreatmentTypesProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyViewModel>(p => p.AvailableAssortmentProductBuddyTreatmentTypes);

        //commands
        public static readonly PropertyPath ApplyCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyViewModel>(p => p.ApplyCommand);
        public static readonly PropertyPath ApplyAndCloseCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyViewModel>(p => p.ApplyAndCloseCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath AddNewBuddiesCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyViewModel>(p => p.AddNewBuddiesCommand);
        public static readonly PropertyPath RemoveBuddyCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyViewModel>(p => p.RemoveBuddyCommand);
        public static readonly PropertyPath AddSourceProductCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyViewModel>(p => p.AddSourceProductCommand);
        public static readonly PropertyPath RemoveSourceProductCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyViewModel>(p => p.RemoveSourceProductCommand);
        public static readonly PropertyPath RemoveAllBuddiesCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyViewModel>(p => p.RemoveAllBuddiesCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the readonly collection of buddy rows
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssortmentProductBuddyRow> AssortmentProductBuddyRows
        {
            get
            {
                if (_assortmentProductBuddyRowsRO == null)
                {
                    _assortmentProductBuddyRowsRO = new ReadOnlyBulkObservableCollection<AssortmentProductBuddyRow>(_assortmentProductBuddyRows);
                }
                return _assortmentProductBuddyRowsRO;
            }
        }

        /// <summary>
        /// Returns the editable collection of selected buddy rows.
        /// </summary>
        public BulkObservableCollection<AssortmentProductBuddyRow> SelectedAssortmentProductBuddyRows
        {
            get { return _selectedAssortmentProductBuddyRows; }
        }

        /// <summary>
        /// Gets/Sets the source type of the selected rows.
        /// </summary>
        public PlanogramAssortmentProductBuddySourceType? EditSourceType
        {
            get { return _editSourceType; }
            set
            {
                _editSourceType = value;
                PlanogramAssortmentProductBuddyTreatmentType? previousEditTreatmentType = _editTreatmentType;

                if (value.HasValue)
                {
                    foreach (AssortmentProductBuddyRow row in this.SelectedAssortmentProductBuddyRows)
                    {
                        row.Buddy.SourceType = value.Value;
                    }

                    _availableAssortmentProductBuddyTreatmentTypes.Clear();

                    if (_editSourceType == PlanogramAssortmentProductBuddySourceType.Manual)
                    {
                        _availableAssortmentProductBuddyTreatmentTypes.Add(PlanogramAssortmentProductBuddyTreatmentType.Avg);
                        _availableAssortmentProductBuddyTreatmentTypes.Add(PlanogramAssortmentProductBuddyTreatmentType.Sum);
                        if (previousEditTreatmentType != null && (previousEditTreatmentType == PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg || previousEditTreatmentType == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg))
                        {
                            previousEditTreatmentType = PlanogramAssortmentProductBuddyTreatmentType.Avg;
                        }
                    }
                    else
                    {
                        _availableAssortmentProductBuddyTreatmentTypes.Add(PlanogramAssortmentProductBuddyTreatmentType.Avg);
                        _availableAssortmentProductBuddyTreatmentTypes.Add(PlanogramAssortmentProductBuddyTreatmentType.Sum);
                        _availableAssortmentProductBuddyTreatmentTypes.Add(PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg);
                        _availableAssortmentProductBuddyTreatmentTypes.Add(PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg);
                    }
                }

                OnPropertyChanged(EditSourceTypeProperty);
                this.EditTreatmentType = previousEditTreatmentType;
            }
        }

        /// <summary>
        /// Gets/Sets the treatment type of the selected rows.
        /// </summary>
        public PlanogramAssortmentProductBuddyTreatmentType? EditTreatmentType
        {
            get { return _editTreatmentType; }
            set
            {
                _editTreatmentType = value;

                if (value.HasValue)
                {
                    foreach (AssortmentProductBuddyRow row in this.SelectedAssortmentProductBuddyRows)
                    {
                        row.Buddy.TreatmentType = value.Value;
                    }
                }

                OnPropertyChanged(EditTreatmentTypeProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the treatment type percentage of the selected rows.
        /// </summary>
        public Single EditTreatmentTypePercentage
        {
            get { return _editTreatmentTypePercentage; }
            set
            {
                _editTreatmentTypePercentage = value;

                foreach (AssortmentProductBuddyRow row in this.SelectedAssortmentProductBuddyRows)
                {
                    row.Buddy.TreatmentTypePercentage = value;
                }

                OnPropertyChanged(EditTreatmentTypePercentageProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the attribute type of the selected rows.
        /// </summary>
        public PlanogramAssortmentProductBuddyProductAttributeType? EditAttributeType
        {
            get { return _editAttributeType; }
            set
            {
                _editAttributeType = value;

                if (value.HasValue)
                {
                    foreach (AssortmentProductBuddyRow row in this.SelectedAssortmentProductBuddyRows)
                    {
                        row.Buddy.ProductAttributeType = value.Value;
                    }
                }

                OnPropertyChanged(EditAttributeTypeProperty);
            }
        }

        /// <summary>
        /// Returns the collection of edit source rows
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductBuddySourceRow> EditSourceRows
        {
            get
            {
                if (_editSourceRowsRO == null)
                {
                    _editSourceRowsRO = new ReadOnlyBulkObservableCollection<ProductBuddySourceRow>(_editSourceRows);
                }
                return _editSourceRowsRO;
            }
        }

        /// <summary>
        /// Returns true if the sources for the existing rows will be overwritten.
        /// </summary>
        public Boolean IsSourceRowsOverwrite
        {
            get { return _isSourceRowsOverwrite; }
            private set
            {
                _isSourceRowsOverwrite = value;
                OnPropertyChanged(IsSourceRowsOverwriteProperty);
            }
        }

        /// <summary>
        /// The available Product Buddy Treatment Types
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramAssortmentProductBuddyTreatmentType> AvailableAssortmentProductBuddyTreatmentTypes
        {
            get
            {
                if (_availableAssortmentProductBuddyTreatmentTypesRO == null)
                {
                    _availableAssortmentProductBuddyTreatmentTypesRO = new ReadOnlyBulkObservableCollection<PlanogramAssortmentProductBuddyTreatmentType>(_availableAssortmentProductBuddyTreatmentTypes);
                }
                return _availableAssortmentProductBuddyTreatmentTypesRO;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Testing Constructor
        /// </summary>
        public AssortmentProductBuddyViewModel(PlanogramAssortment currentAssortment, IEnumerable<IPlanItem> planItems)
        {
            _planItems = planItems;
            _selectedAssortmentProductBuddyRows.CollectionChanged += SelectedAssortmentProductBuddyRows_CollectionChanged;
            _editSourceRows.BulkCollectionChanged += EditSourceRows_BulkCollectionChanged;

            _currentAssortment = currentAssortment;
            _categoryCode = currentAssortment.Parent.CategoryCode;
            //Initialise product lists
            if (App.ViewState.IsConnectedToRepository)
            {
                _availableSourceProductLookup = ProductInfoList.FetchByEntityId(App.ViewState.EntityId).Cast<IPlanogramProductInfo>().ToDictionary(p => p.Gtin);
            }
            else
            {
                _availableSourceProductLookup = _currentAssortment.Products.Cast<IPlanogramProductInfo>().ToDictionary(p => p.Gtin);
            }

            ReloadAssortmentProductBuddyRows();

            if (this.AssortmentProductBuddyRows.Count > 0)
            {
                this.SelectedAssortmentProductBuddyRows.Add(this.AssortmentProductBuddyRows.First());
            }
        }

        #endregion

        #region Commands

        #region Apply

        private RelayCommand _applyCommand;

        /// <summary>
        /// Applies changes made on the screen
        /// </summary>
        public RelayCommand ApplyCommand
        {
            get
            {
                if (_applyCommand == null)
                {
                    _applyCommand = new RelayCommand(
                        p => Apply_Executed(),
                        p => Apply_CanExecute())
                    {
                        FriendlyName = Message.Generic_Apply
                    };
                    base.ViewModelCommands.Add(_applyCommand);
                }
                return _applyCommand;
            }
        }

        private Boolean Apply_CanExecute()
        {
            if (_deletedAssortmentProductBuddyRows.Count > 0)
            {
                return true;
            }
            //at least one row must be dirty
            if (!this.AssortmentProductBuddyRows.Any(f => f.IsDirty))
            {
                return false;
            }

            //Edit percentage must be valid
            if (this.AssortmentProductBuddyRows.Any(p => (p.Buddy.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg || p.Buddy.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg) && !(p.Buddy.TreatmentTypePercentage > 0 && p.Buddy.TreatmentTypePercentage <= 1)))
            {
                return false;
            }

            //all rows must be valid
            if (!this.AssortmentProductBuddyRows.All(r => r.IsValid()))
            {
                return false;
            }

            return true;
        }

        private void Apply_Executed()
        {
            ApplyCurrentItem();
        }

        private void ApplyCurrentItem()
        {
            base.ShowWaitCursor(true);

            //delete all removed row families
            foreach (AssortmentProductBuddyRow deletedRow in _deletedAssortmentProductBuddyRows)
            {
                deletedRow.DeleteExistingBuddy();
                deletedRow.Dispose();
            }
            _deletedAssortmentProductBuddyRows.Clear();

            //cycle through all product rows applying changes made
            foreach (AssortmentProductBuddyRow row in this.AssortmentProductBuddyRows)
            {
                row.CommitChanges();
            }

            //recreate all rows
            ReloadAssortmentProductBuddyRows();

            base.ShowWaitCursor(false);

        }

        #endregion

        #region ApplyAndClose

        private RelayCommand _applyAndCloseCommand;

        /// <summary>
        /// Applies changes made and closes the window
        /// </summary>
        public RelayCommand ApplyAndCloseCommand
        {
            get
            {
                if (_applyAndCloseCommand == null)
                {
                    _applyAndCloseCommand = new RelayCommand(
                        p => ApplyAndClose_Executed(),
                        p => ApplyAndClose_CanExecute())
                    {
                        FriendlyName = Message.Generic_ApplyAndClose
                    };
                    base.ViewModelCommands.Add(_applyAndCloseCommand);
                }
                return _applyAndCloseCommand;
            }
        }

        private Boolean ApplyAndClose_CanExecute()
        {
            //all rows must be valid
            if (!this.AssortmentProductBuddyRows.All(r => r.IsValid()))
            {
                return false;
            }

            //Edit percentage must be valid
            if (this.AssortmentProductBuddyRows.Any(p => (p.Buddy.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg || p.Buddy.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg) && !(p.Buddy.TreatmentTypePercentage > 0 && p.Buddy.TreatmentTypePercentage <= 1)))
            {
                return false;
            }

            return true;
        }

        private void ApplyAndClose_Executed()
        {
            ApplyCurrentItem();

            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels changes made and closes the 
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region AddNewBuddies

        private RelayCommand _addNewBuddiesCommand;

        /// <summary>
        /// Adds news buddies with the specified criteria
        /// </summary>
        public RelayCommand AddNewBuddiesCommand
        {
            get
            {
                if (_addNewBuddiesCommand == null)
                {
                    _addNewBuddiesCommand = new RelayCommand(
                        p => AddNewBuddies_Executed())
                    {
                        FriendlyName = Message.AssortmentProductBuddy_AddBuddies,
                        SmallIcon = ImageResources.Add_16
                    };
                    base.ViewModelCommands.Add(_addNewBuddiesCommand);
                }
                return _addNewBuddiesCommand;
            }
        }


        private void AddNewBuddies_Executed()
        {
             List<PlanogramAssortmentProduct> targetProductList = new List<PlanogramAssortmentProduct>();
            if (this.AttachedControl != null)
            {
                //build a list of already taken products
                List<String> excludeProductGtins = new List<String>();
                excludeProductGtins.AddRange(this.EditSourceRows.Select(r => r.Product.Gtin));

                //build a lit of available products
                List<Product> availableProducts = new List<Product>();
                foreach (PlanogramAssortmentProduct assortProduct in this._currentAssortment.Products
                    .Where(p => !excludeProductGtins.Contains(p.Gtin)))
                {
                    availableProducts.Add(Product.NewProduct(assortProduct.GetPlanogramProduct(), false));
                }
                List<String> excludedList = new List<String>();
                foreach (AssortmentProductBuddyRow buddyRow in _assortmentProductBuddyRows)
                {
                    excludedList.Add(buddyRow.TargetProduct.Gtin);
                }
                //show the window to fetch the target product buddy
                ProductSelectorViewModel viewModel = new ProductSelectorViewModel(false, _categoryCode, availableProducts, excludedList);
                ProductSelectorWindow win = new ProductSelectorWindow(viewModel);
                CommonHelper.GetWindowService().ShowDialog<ProductSelectorWindow>(win);

                if (win.DialogResult == true &&
                    win.ViewModel.AssignedProducts != null)
                {
                    foreach (Product prod in win.ViewModel.AssignedProducts)
                    {
                        //try to fetch the actual planogram assortment product from the assortment if it exists
                        PlanogramAssortmentProduct planAsstProduct = _currentAssortment.Products
                            .FirstOrDefault(p => p.Gtin == prod.Gtin);
                        if (planAsstProduct == null)
                        {
                            //product was not in the asortment so just create a new one
                            //Note: Actually this should never occur in future as only products in the assortment
                            //should be able to be selected as target products. But for now we leave it in
                            planAsstProduct = PlanogramAssortmentProduct.NewPlanogramAssortmentProduct(prod.Gtin, prod.Name);
                        }
                        targetProductList.Add(planAsstProduct);
                    }
                } 
            }

            if (targetProductList.Count() >= 1)
            {
                base.ShowWaitCursor(true);

                //clear the current row selection for speed
                if (this.SelectedAssortmentProductBuddyRows.Count > 0)
                {
                    this.SelectedAssortmentProductBuddyRows.Clear();
                }

                List<AssortmentProductBuddyRow> newRows = new List<AssortmentProductBuddyRow>();
                List<AssortmentProductBuddyRow> rowsToSelect = new List<AssortmentProductBuddyRow>();

                foreach (PlanogramAssortmentProduct targetProduct in targetProductList)
                {
                    // check if a row already exists
                    AssortmentProductBuddyRow row = this.AssortmentProductBuddyRows.FirstOrDefault(r => r.TargetProduct == targetProduct);
                    if (row == null)
                    {
                        row = new AssortmentProductBuddyRow(_currentAssortment, targetProduct, _availableSourceProductLookup);
                        newRows.Add(row);
                    }

                    // has the product buddy been deleted and re-added, if so remove from deleted list
                    AssortmentProductBuddyRow deletedRow = _deletedAssortmentProductBuddyRows.FirstOrDefault(r => r.TargetProduct == targetProduct);
                    if (deletedRow != null)
                    {
                        _deletedAssortmentProductBuddyRows.Remove(deletedRow);
                    }

                    //Select row
                    rowsToSelect.Add(row);
                }
                _assortmentProductBuddyRows.AddRange(newRows);

                //select all affected rows.
                this.SelectedAssortmentProductBuddyRows.AddRange(newRows);
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region RemoveBuddy

        private RelayCommand _removeBuddyCommand;

        /// <summary>
        /// Removes the given buddy row
        /// </summary>
        public RelayCommand RemoveBuddyCommand
        {
            get
            {
                if (_removeBuddyCommand == null)
                {
                    _removeBuddyCommand = new RelayCommand(
                        p => RemoveBuddy_Executed(p),
                        p => RemoveBuddy_CanExecute(p))
                    {
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeBuddyCommand);
                }
                return _removeBuddyCommand;
            }
        }

        private Boolean RemoveBuddy_CanExecute(Object arg)
        {
            AssortmentProductBuddyRow row = arg as AssortmentProductBuddyRow;

            //must have a row.
            if (row == null)
            {
                return false;
            }

            return true;
        }

        private void RemoveBuddy_Executed(Object arg)
        {
            AssortmentProductBuddyRow row = arg as AssortmentProductBuddyRow;
            if (row != null)
            {
                //clear the current row selection
                if (this.SelectedAssortmentProductBuddyRows.Count > 0)
                {
                    this.SelectedAssortmentProductBuddyRows.Clear();
                }

                //remove the row from the main collection and add to the deleted
                _assortmentProductBuddyRows.Remove(row);
                _deletedAssortmentProductBuddyRows.Add(row);
            }
        }

        #endregion

        #region RemoveAllBuddies

        private RelayCommand _removeAllBuddiesCommand;

        /// <summary>
        /// Removes the given buddy row
        /// </summary>
        public RelayCommand RemoveAllBuddiesCommand
        {
            get
            {
                if (_removeAllBuddiesCommand == null)
                {
                    _removeAllBuddiesCommand = new RelayCommand(
                        p => RemoveAllBuddies_Executed(),
                        p => RemoveAllBuddies_CanExecute())
                    {
                        FriendlyName = Message.AssortmentProductBuddy_RemoveAllBuddies,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeAllBuddiesCommand);
                }
                return _removeAllBuddiesCommand;
            }
        }

        private Boolean RemoveAllBuddies_CanExecute()
        {
            //must have rows
            if (_assortmentProductBuddyRows == null || _assortmentProductBuddyRows.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void RemoveAllBuddies_Executed()
        {
            //remove all and add to the deleted collection
            _deletedAssortmentProductBuddyRows.AddRange(_assortmentProductBuddyRows);
            _assortmentProductBuddyRows.Clear();

            //clear the current row selection
            if (this.SelectedAssortmentProductBuddyRows.Count > 0)
            {
                this.SelectedAssortmentProductBuddyRows.Clear();
            }
        }

        #endregion

        #region AddSourceProduct

        private RelayCommand _addSourceProductCommand;

        /// <summary>
        /// Adds a new source product
        /// </summary>
        public RelayCommand AddSourceProductCommand
        {
            get
            {
                if (_addSourceProductCommand == null)
                {
                    _addSourceProductCommand = new RelayCommand(
                        p => AddSourceProduct_Executed(),
                        p => AddSourceProduct_CanExecute())
                    {
                        FriendlyName = Message.Generic_Add,
                        SmallIcon = ImageResources.Add_16
                    };
                    base.ViewModelCommands.Add(_addSourceProductCommand);
                }
                return _addSourceProductCommand;
            }
        }

        private Boolean AddSourceProduct_CanExecute()
        {
            //should have manual source type selected
            if (this.EditSourceType != PlanogramAssortmentProductBuddySourceType.Manual)
            {
                return false;
            }

            //must not have 5 rows already.
            if (this.EditSourceRows.Count >= 5)
            {
                return false;
            }


            return true;
        }

        private void AddSourceProduct_Executed()
        {
            IEnumerable<IPlanogramProductInfo> sourceProducts = null;

            if (this.AttachedControl != null)
            {
                List<String> excludeProducts = new List<String>();
                excludeProducts.AddRange(this.EditSourceRows.Select(r => r.Product.Gtin));

                if (App.ViewState.IsConnectedToRepository)
                {
                    ProductSelectorHelper productSelectorHelper = new ProductSelectorHelper();
                    productSelectorHelper.IsMultiSelectEnabled = true;
                    productSelectorHelper.ExcludedProducts.AddRange(excludeProducts);

                    var winModel = new ProductSelectorViewModel(productSelectorHelper);
                    CommonHelper.GetWindowService().ShowDialog<ProductSelectorWindow>(winModel);

                    if (winModel.DialogResult == true)
                    {
                        sourceProducts = winModel.AssignedProducts.ToList();
                    }
                }
                else
                {
                    AssortmentProductSelectionWindow win = new AssortmentProductSelectionWindow(this._currentAssortment.Products.Where(p => !excludeProducts.Contains(p.Gtin)).Select(p => new PlanogramAssortmentProductView(p)));
                    win.SelectionMode = DataGridSelectionMode.Extended;
                    CommonHelper.GetWindowService().ShowDialog<AssortmentProductSelectionWindow>(win);

                    if (win.DialogResult == true &&
                        win.SelectionResult != null)
                    {
                        sourceProducts = win.SelectionResult.Select(p => p.Product);
                    }
                }
            }

            if (sourceProducts != null)
            {
                Int32 numberToAdd = 5 - _editSourceRows.Count;
                Single percentage = 1F;

                Boolean clearExisting = _editSourceRows.Count == 0;

                //Take first number to add
                foreach (IPlanogramProductInfo sourceProduct in sourceProducts.Take(numberToAdd))
                {
                    _editSourceRows.Add(new ProductBuddySourceRow(sourceProduct, percentage));
                }

                foreach (AssortmentProductBuddyRow row in this.SelectedAssortmentProductBuddyRows)
                {
                    //if this is the first row then clear any others - 
                    //could be a multiselect overwrite
                    if (this.SelectedAssortmentProductBuddyRows.Count > 1
                        && row.SourceRows.Count != 0
                        && clearExisting)
                    {
                        row.SourceRows.Clear();
                    }

                    foreach (IPlanogramProductInfo sourceProduct in sourceProducts.Take(numberToAdd))
                    {
                        row.SourceRows.Add(new ProductBuddySourceRow(sourceProduct, percentage));
                    }
                }

                this.IsSourceRowsOverwrite = false;
            }
        }

        #endregion

        #region RemoveSourceProduct

        private RelayCommand<ProductBuddySourceRow> _removeSourceProductCommand;

        /// <summary>
        /// Removes the given source product row
        /// </summary>
        public RelayCommand<ProductBuddySourceRow> RemoveSourceProductCommand
        {
            get
            {
                if (_removeSourceProductCommand == null)
                {
                    _removeSourceProductCommand = new RelayCommand<ProductBuddySourceRow>(
                        p => RemoveSourceProduct_Executed(p),
                        p => RemoveSourceProduct_CanExecute(p))
                    {
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeSourceProductCommand);
                }
                return _removeSourceProductCommand;
            }
        }

        private Boolean RemoveSourceProduct_CanExecute(ProductBuddySourceRow row)
        {
            if (row == null)
            {
                return false;
            }

            return true;
        }

        private void RemoveSourceProduct_Executed(ProductBuddySourceRow row)
        {
            _editSourceRows.Remove(row);

            //update the corresponding source row on all selected buddies
            IPlanogramProductInfo product = row.Product;
            foreach (AssortmentProductBuddyRow buddyRow in this.SelectedAssortmentProductBuddyRows)
            {
                ProductBuddySourceRow matchingSource =
                    buddyRow.SourceRows.FirstOrDefault(p => p.Product == product);

                buddyRow.SourceRows.Remove(matchingSource);
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to changes to the collection of selected rows
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedAssortmentProductBuddyRows_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //clear values to null
            _editSourceType = null;
            _editTreatmentType = null;
            _editAttributeType = null;
            if (_editSourceRows.Count > 0)
            {
                _editSourceRows.Clear();
            }


            //reset the edit values
            if (this.SelectedAssortmentProductBuddyRows.Count > 0)
            {
                AssortmentProductBuddyRow firstRow = this.SelectedAssortmentProductBuddyRows.First();
                PlanogramAssortmentProductBuddy firstBuddy = firstRow.Buddy;

                //source type
                PlanogramAssortmentProductBuddySourceType firstSourceType = firstBuddy.SourceType;
                if (this.SelectedAssortmentProductBuddyRows.All(r => r.Buddy.SourceType == firstSourceType))
                {
                    this.EditSourceType = firstSourceType;
                }

                //treatment type
                PlanogramAssortmentProductBuddyTreatmentType firstTreatmentType = firstBuddy.TreatmentType;
                Single firstTreatmentTypePercentage = firstBuddy.TreatmentTypePercentage;
                if (this.SelectedAssortmentProductBuddyRows.All(r => r.Buddy.TreatmentType == firstTreatmentType))
                {
                    _editTreatmentType = firstTreatmentType;
                    _editTreatmentTypePercentage = firstTreatmentTypePercentage;
                }

                //attribute type
                PlanogramAssortmentProductBuddyProductAttributeType firstAttributeType = firstBuddy.ProductAttributeType;
                if (this.SelectedAssortmentProductBuddyRows.All(r => r.Buddy.ProductAttributeType == firstAttributeType))
                {
                    _editAttributeType = firstAttributeType;
                }

                //edit source rows
                //must all have the same buddies
                #region Edit Source Rows

                Boolean sourcesAreSame = false;
                Int32 firstSourceCount = firstRow.SourceRows.Count;
                if (this.SelectedAssortmentProductBuddyRows.All(r => r.SourceRows.Count == firstSourceCount))
                {
                    sourcesAreSame = true;
                    foreach (ProductBuddySourceRow sourceRow in firstRow.SourceRows)
                    {
                        if (!this.SelectedAssortmentProductBuddyRows.All(r =>
                            r.SourceRows.Select(s => s.Product.Gtin)
                            .Contains(sourceRow.Product.Gtin)))
                        {
                            sourcesAreSame = false;
                            break;
                        }
                    }
                }


                if (sourcesAreSame && _currentAssortment != null)
                {
                    this.IsSourceRowsOverwrite = false;

                    //create the source rows
                    foreach (ProductBuddySourceRow sourceRow in firstRow.SourceRows)
                    {
                        IPlanogramProductInfo product = sourceRow.Product;
                        Single? percentage = sourceRow.Percentage;

                        if (!this.SelectedAssortmentProductBuddyRows.All(r =>
                            r.SourceRows.First(s => s.Product == product).Percentage == percentage))
                        {
                            percentage = null;
                        }

                        _editSourceRows.Add(new ProductBuddySourceRow(sourceRow.Product, percentage));
                    }

                }
                else
                {
                    this.IsSourceRowsOverwrite = true;
                }

                #endregion

            }

            OnPropertyChanged(EditSourceTypeProperty);
            OnPropertyChanged(EditTreatmentTypeProperty);
            OnPropertyChanged(EditTreatmentTypePercentageProperty);
            OnPropertyChanged(EditAttributeTypeProperty);
        }

        /// <summary>
        /// Responds to collection changes to the edit source rows
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditSourceRows_BulkCollectionChanged(object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (ProductBuddySourceRow row in e.ChangedItems)
                    {
                        row.PropertyChanged += EditSourceRow_PropertyChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (ProductBuddySourceRow row in e.ChangedItems)
                    {
                        row.PropertyChanged -= EditSourceRow_PropertyChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    foreach (ProductBuddySourceRow row in e.ChangedItems)
                    {
                        row.PropertyChanged -= EditSourceRow_PropertyChanged;
                    }
                    foreach (ProductBuddySourceRow row in this.EditSourceRows)
                    {
                        row.PropertyChanged += EditSourceRow_PropertyChanged;
                    }
                    break;
            }
        }

        /// <summary>
        /// Responds to a property changed on an edit source row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditSourceRow_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ProductBuddySourceRow.PercentageProperty.Path)
            {
                ProductBuddySourceRow sourceRow = sender as ProductBuddySourceRow;
                IPlanogramProductInfo product = sourceRow.Product;

                //update the corresponding source row on all selected buddies
                foreach (AssortmentProductBuddyRow buddyRow in this.SelectedAssortmentProductBuddyRows)
                {
                    ProductBuddySourceRow matchingSource =
                        buddyRow.SourceRows.FirstOrDefault(p => p.Product == product);
                    Debug.Assert(matchingSource != null, "Should always have a matching row");

                    if (matchingSource != null)
                    {
                        matchingSource.Percentage = sourceRow.Percentage;
                    }
                    buddyRow.OnSourceRowChanged();
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                if (this.AssortmentProductBuddyRows.Any(p => p.IsDirty))
                {
                    ChildItemChangeResult result = Framework.Controls.Wpf.Helpers.ShowContinueWithChildItemChangeDialog(this.ApplyCommand.CanExecute());

                    if (result == ChildItemChangeResult.Apply)
                    {
                        this.ApplyCommand.Execute();
                    }
                    else if (result == ChildItemChangeResult.DoNotApply)
                    {
                        //do nothing
                    }
                    else
                    {
                        continueExecute = false;
                    }

                }
            }
            return continueExecute;
        }

        /// <summary>
        /// Reloads the product buddy rows collection
        /// </summary>
        private void ReloadAssortmentProductBuddyRows()
        {
            if (_assortmentProductBuddyRows.Count > 0)
            {
                _assortmentProductBuddyRows.Clear();
            }

            //create a row per node product buddy
            List<AssortmentProductBuddyRow> newRows = new List<AssortmentProductBuddyRow>();
            foreach (PlanogramAssortmentProductBuddy buddy in _currentAssortment.ProductBuddies)
            {
                newRows.Add(new AssortmentProductBuddyRow(buddy, _availableSourceProductLookup));
            }
            _assortmentProductBuddyRows.AddRange(newRows);

        }

        #endregion

        #region IDataErrorInfo Members

        public String Error
        {
            get { return String.Empty; }
        }

        public String this[String columnName]
        {
            get
            {
                //treatment type validation
                if (columnName == EditTreatmentTypePercentageProperty.Path)
                {
                    //Edit TreatmentTypePercentage has been changed to allow 1000% as 100% was not enough and previously some of the product buddy area's allowed 1000%
                    if (EditTreatmentType != null &&
                        (EditTreatmentType.Value == PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg || EditTreatmentType.Value == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg)
                        && !(EditTreatmentTypePercentage > 0 && EditTreatmentTypePercentage <= 10)) 
                    {
                        return Message.AssortmentLocationBuddyReview_EditTreatmentTypePercentagee_NotPercentage;
                    }
                }

                return String.Empty;
            }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _selectedAssortmentProductBuddyRows.CollectionChanged -= SelectedAssortmentProductBuddyRows_CollectionChanged;
                    _editSourceRows.BulkCollectionChanged -= EditSourceRows_BulkCollectionChanged;
                    _deletedAssortmentProductBuddyRows.Clear();
                    _deletedAssortmentProductBuddyRows = null;
                    _selectedAssortmentProductBuddyRows.Clear();
                    _selectedAssortmentProductBuddyRows = null;
                    _assortmentProductBuddyRows.Clear();
                    _assortmentProductBuddyRows = null;
                    _availableAssortmentProductBuddyTreatmentTypes.Clear();
                    _availableAssortmentProductBuddyTreatmentTypes = null;
                    _availableSourceProductLookup.Clear();
                    _availableSourceProductLookup = null;
                    _currentAssortment = null;
                    _editSourceRows.Clear();
                    _editSourceRows = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
