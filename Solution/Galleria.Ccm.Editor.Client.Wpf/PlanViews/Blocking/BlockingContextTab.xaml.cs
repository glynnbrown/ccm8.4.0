﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26891 : L.Ineson 
//  Created.
#endregion

#endregion


using System.Windows;
using Fluent;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Controls.Wpf.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.Blocking
{
    /// <summary>
    /// Interaction logic for BlockingContextTab.xaml
    /// </summary>
    public partial class BlockingContextTab
    {
        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(BlockingPlanDocument), typeof(BlockingContextTab),
                new PropertyMetadata(null));

        public BlockingPlanDocument ViewModel
        {
            get { return (BlockingPlanDocument)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        public BlockingContextTab()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        private void ViewMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (ViewModel != null)
            {
                var senderControl = sender as MenuItem;
                if (senderControl != null)
                {
                    if (senderControl.IsChecked)
                    {
                        PlanogramBlocking blocking = senderControl.CommandParameter as PlanogramBlocking;
                        if(blocking == null || ViewModel == null) return;
                        this.ViewModel.CurrentBlocking = new PlanogramBlockingViewModel(blocking, ViewModel.Planogram.Model.Sequence);
                    }
                }
            }
        }

        #endregion
    }
}
