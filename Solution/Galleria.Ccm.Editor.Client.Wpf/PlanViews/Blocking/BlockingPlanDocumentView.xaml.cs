﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26891 : L.Ineson 
//  Created.
#endregion

#region Version History: CCM802

// V8-28993 : A.Kuszyk
//  Removed obselete blocking information
// V8-28997 : A.Silva
//      Added a handler for the BlockingEditor.DividerPlaced event.

#endregion

#region Version History: CCM803

// V8-29518 : A.Silva
//      Remove handler for BlockingEditor.DividerPlaced event. The sequence generation is now driven by the user.

#endregion

#region Version History: CCM811
// V8-29184 : M.Brumby
//  Added calculation of merchandising volume percentages
#endregion

#region Version History: CCM820
// V8-29968 : A.Kuszyk
//  Added call to NotifyBlockingModified when a divider is moved.
// V8-30536 : A.Kuszyk
//  Removed ProcessMerchandisingPercentages from DividerPlaced event handler and changed Selection event handler
//  to call through to UpdateMerchandisableBlockPercentages.
// V8-30982 : A.Kuszyk
//  Added event handler for mouse move event on blocking editor so that status bar text can be updated.
// V8-31259 : A.Kuszyk
//  Removed assignment of ViewModel to null in Dispose method. This was causing issues with the child view models
//  when the ViewModel bindings were updating.
// V8-30737 : M.Shelley
//  Added the blocking group meta performance data and a converter to allow custom expander / grid resizing
// V8-31501 : L.Ineson
//  Removed change for V8-31259 - the viewmodel must always be set to null otherwise the plan memory leaks!
#endregion

#region Version History: CCM830
// V8-32089 : N.Haywood
//  Added Custom Columns
// CCM-18321 : L.Bailey
//  Multi-select Blocks & Dividers and apply certain settings to all selected
// CCM-18674 : M.Pettit
//  Added validation on limit space percentage textboxes to prevent users entering chars that are not +ve numeric values
#endregion

#endregion

using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Controls.Wpf.Rendering;
using Galleria.Framework.Planograms.Controls.Wpf.BlockingEditor;
using Galleria.Framework.Planograms.Controls.Wpf.ViewModel;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.Planograms.ViewModel;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.Blocking
{
    /// <summary>
    /// Interaction logic for BlockingPlanDocumentView.xaml
    /// </summary>
    public sealed partial class BlockingPlanDocumentView : IPlanDocumentView
    {
        #region Fields

        public static String RemoveProductCommandKey = "RemoveProductCommand";

        private GridLength _structurePanelWidth = new GridLength(250D);
        private GridLength _propertiesPanelWidth = new GridLength(300.0);
        private ColumnLayoutManager _columnLayoutManager;

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(BlockingPlanDocument), typeof(BlockingPlanDocumentView),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Called whenever the viewmodel context changes
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BlockingPlanDocumentView senderControl = (BlockingPlanDocumentView)obj;

            if (e.OldValue != null)
            {
                BlockingPlanDocument oldModel = (BlockingPlanDocument)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.ZoomToFitCommand.Executed -= senderControl.ViewModel_ZoomToFitCommandExecuted;
                oldModel.ZoomToFitHeightCommand.Executed -= senderControl.ViewModel_ZoomToFitHeightCommandExecuted;
                oldModel.ZoomInCommand.Executed -= senderControl.ViewModel_ZoomInCommandExecuted;
                oldModel.ZoomOutCommand.Executed -= senderControl.ViewModel_ZoomOutCommandExecuted;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                oldModel.Planogram.PropertyChanged -= senderControl.PlanogramView_PropertyChanged;                
            }

            if (e.NewValue != null)
            {
                BlockingPlanDocument newModel = (BlockingPlanDocument)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.ZoomToFitCommand.Executed += senderControl.ViewModel_ZoomToFitCommandExecuted;
                newModel.ZoomToFitHeightCommand.Executed += senderControl.ViewModel_ZoomToFitHeightCommandExecuted;
                newModel.ZoomInCommand.Executed += senderControl.ViewModel_ZoomInCommandExecuted;
                newModel.ZoomOutCommand.Executed += senderControl.ViewModel_ZoomOutCommandExecuted;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
                newModel.Planogram.PropertyChanged += senderControl.PlanogramView_PropertyChanged;
            }
        }

        /// <summary>
        /// Gets the viewmodel controller for this view.
        /// </summary>
        public BlockingPlanDocument ViewModel
        {
            get { return (BlockingPlanDocument)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }


        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public BlockingPlanDocumentView(BlockingPlanDocument document)
        {
            this.AddHandler(Divider.MouseRightButtonDownEvent, (MouseButtonEventHandler)OnDividerRightClicked);

            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = document;

            this.Loaded += BlockingPlanDocumentView_Loaded;
        }

        /// <summary>
        ///Carries out intial load actions.
        /// </summary>
        private void BlockingPlanDocumentView_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= BlockingPlanDocumentView_Loaded;
            this.blockingEditor.DividerPlaced += blockingEditor_DividerPlaced;
            this.blockingEditor.SelectionChanged += blockingEditor_SelectionChanged;
            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
            this.blockingEditor.MouseMove += BlockingEditor_MouseMove;

            _columnLayoutManager = new ColumnLayoutManager(
                new PlanogramSequenceGroupProductLayoutFactory(typeof(BlockingPlanDocumentView), this.ViewModel.Planogram.Model),
                this.ViewModel.Planogram.DisplayUnits,
                BlockingPlanDocument.ScreenKey, /*PathMask*/"Product.{0}");

            //_columnLayoutManager.IncludeUnseenColumns = false; // turn off for performance improvement
            _columnLayoutManager.CalculatedColumnPath = "CalculatedValueResolver";
            _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            _columnLayoutManager.AttachDataGrid(this.xProductGrid);


            this.xProductGrid.DragScope = Application.Current.MainWindow.Content as FrameworkElement;
        }


        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the column layout manager columns are about to change.
        /// </summary>
        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            // Add extra columns.
            this.xProductGrid.FrozenColumnCount = 1;

            Int32 curIdx = 0;

            //Position column
            columnSet.Insert(curIdx,
                ExtendedDataGrid.CreateReadOnlyTextColumn(Message.BlockingPlanDocument_SequenceNumber,
            PlanogramSequenceGroupProduct.SequenceNumberProperty.Name, HorizontalAlignment.Left));
            curIdx++;

        }

        private void ViewModel_PropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == BlockingPlanDocument.HighlightProperty.Path)
            {
                if (_columnLayoutManager == null) return;

                _columnLayoutManager.RefreshGrid(/*keepExisting*/true);
            }
        }

        /// <summary>
        /// Called whenever a property changes on the current planogram.
        /// </summary>
        private void PlanogramView_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.ViewModel == null) return;

            if (e.PropertyName == PlanogramView.DisplayUnitsProperty.Path)
            {
                if (_columnLayoutManager == null) return;

                //display units have changed so update the columns to pick up the new settings.
                _columnLayoutManager.DisplayUomCollection = this.ViewModel.Planogram.DisplayUnits;
            }
        }

        #region ViewModel

        private void ViewModel_ZoomToFitCommandExecuted(object sender, EventArgs e)
        {
            this.blockingEditor.ZoomMode = BlockingZoomMode.FitAll;
        }

        private void ViewModel_ZoomToFitHeightCommandExecuted(object sender, EventArgs e)
        {
            this.blockingEditor.ZoomMode = BlockingZoomMode.FitToHeight;
        }

        private void ViewModel_ZoomInCommandExecuted(object sender, EventArgs e)
        {
            this.blockingEditor.ZoomIn();
        }

        private void ViewModel_ZoomOutCommandExecuted(object sender, EventArgs e)
        {
            this.blockingEditor.ZoomOut();
        }

        #endregion

        #region Panels

        private void StructurePanel_OnCollapsed(Object sender, RoutedEventArgs e)
        {
            var index = Grid.GetColumn((Expander)sender);
            var column = xContentLayout.ColumnDefinitions[index];
            _structurePanelWidth = column.Width; // Store the width of the the panel BEFORE it was collapsed.
            column.Width = GridLength.Auto; // Collapse the panel.
        }

        private void StructurePanel_OnExpanded(Object sender, RoutedEventArgs e)
        {
            var index = Grid.GetColumn((Expander)sender);
            var column = xContentLayout.ColumnDefinitions[index];
            column.Width = _structurePanelWidth; // Restore the panel to the size it had before being collapsed.
        }

        private void PropertiesPanel_OnCollapsed(Object sender, RoutedEventArgs e)
        {
            var index = Grid.GetColumn((Expander)sender);
            var column = xContentLayout.ColumnDefinitions[index];
            _propertiesPanelWidth = column.Width; // Store the width of the the panel BEFORE it was collapsed.
            column.Width = GridLength.Auto; // Collapse the panel.
        }

        private void PropertiesPanel_OnExpanded(Object sender, RoutedEventArgs e)
        {
            var index = Grid.GetColumn((Expander)sender);
            var column = xContentLayout.ColumnDefinitions[index];
            column.Width = _propertiesPanelWidth; // Restore the panel to the size it had before being collapsed.
        }

        #endregion

        /// <summary>
        /// Called whtn the mouse is moved over the blocking editor.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BlockingEditor_MouseMove(object sender, MouseEventArgs e)
        {
            App.MainPageViewModel.StatusBarText = GetStatusBarText(e);
        }

        /// <summary>
        /// Called whenever a divider is right clicked.
        /// </summary>
        private void OnDividerRightClicked(Object sender, MouseEventArgs e)
        {
            Divider div = ((DependencyObject)e.OriginalSource).FindVisualAncestor<Divider>();
            if (div == null) return;

            ShowDividerContextMenu(div.Context as PlanogramBlockingDivider, false, null);
        }

        /// <summary>
        /// Called when the user right clicks on a divider grid row.
        /// </summary>
        private void DividerGrid_RowItemMouseRightClick(object sender, ExtendedDataGridItemEventArgs e)
        {

            ExtendedDataGrid grid = (ExtendedDataGrid)sender;
            if (grid.SelectedItems.Count <=1)
            {
                //make sure the row is selected
                DividerGrid.SelectedItem = e.RowItem;
            }
            ShowDividerContextMenu(e.RowItem as PlanogramBlockingDivider, true, grid);
        }

        private void blockingEditor_SelectionChanged(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                BlockingEditor blockingEditor = (BlockingEditor)sender;
                this.ViewModel.SingleSelectedBlock = blockingEditor.SelectedItems.Count < 2;
                this.ViewModel.SelectedItems = blockingEditor.SelectedItems;
                this.ViewModel.UpdateMerchandisableBlockPercentages();
            }
        }

        private void blockingEditor_DividerPlaced(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.NotifyBlockingModified();
            }
        }

        private void LimitBlockSpaceByPlanSpacePercent_PreviewTextInput(Object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsEnteredCharValid(e.Text);
        }

        private void LimitBlockSpaceByBlockSpacePercent_PreviewTextInput(Object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsEnteredCharValid(e.Text);
        }
        #endregion

        #region Methods

        /// <summary>
        /// Is the entered char a valid number or decimal point
        /// </summary>
        /// <param name="charEntered"></param>
        /// <returns></returns>
        private static Boolean IsEnteredCharValid(string charEntered)
        {
            System.Text.RegularExpressions.Regex regex =
                new System.Text.RegularExpressions.Regex("[^0-9.]+"); //only allow numbers and decimal point
            Boolean valueIsValid = !regex.IsMatch(charEntered);
            return valueIsValid;
        }

        /// <summary>
        /// Gets the status bar text, if any, for the position or component the mouse is over in the blocking editor.
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        private String GetStatusBarText(MouseEventArgs e)
        {
            HitTestResult hitResult = VisualTreeHelper.HitTest(
                blockingEditor.HighlightViewport,
                e.GetPosition(blockingEditor.HighlightViewport));
            if (hitResult == null) return null;

            PlanogramPositionViewModel positionHit = GetPlanItemHit<PlanogramPositionViewModel>(hitResult.VisualHit);
            if (positionHit != null)
            {
                return Galleria.Framework.Planograms.Helpers.PlanogramFieldHelper.ResolvePositionLabel(
                    App.ViewState.Settings.Model.PositionHoverStatusBarText,
                    positionHit.Model,
                    positionHit.Product.Model,
                    positionHit.GetParentSubComponentPlacement());
            }

            PlanogramSubComponentViewModel subComponentHit = GetPlanItemHit<PlanogramSubComponentViewModel>(hitResult.VisualHit);
            if (subComponentHit != null)
            {
                PlanogramComponentViewModelBase componentHit = subComponentHit.Component;
                if (componentHit == null) return null;
                return componentHit.GetLabelText(App.ViewState.Settings.Model.ComponentHoverStatusBarText);
            }

            return null;
        }

        /// <summary>
        /// Gets the "plan item" of type T that the cursor is over. Its not really a plan item,
        /// because we're dealing with framework view models.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dep"></param>
        /// <returns></returns>
        private T GetPlanItemHit<T>(DependencyObject dep) where T : ViewModelBase
        {
            while (dep != null)
            {
                ModelConstruct3D depModel = dep as ModelConstruct3D;
                if (depModel != null
                    && depModel.ModelData is IPlanPart3DData
                    && ((IPlanPart3DData)depModel.ModelData).PlanPart != null)
                {
                    IPlanPart3DData planPart3dData = depModel.ModelData as IPlanPart3DData;
                    if (planPart3dData == null) return null;
                    return planPart3dData.PlanPart as T;
                }

                //try next parent.
                dep = VisualTreeHelper.GetParent(dep);

            }
            return null;
        }

        /// <summary>
        /// Shows the divider context menu.
        /// </summary>
        /// <param name="div"></param>
        private void ShowDividerContextMenu(PlanogramBlockingDivider div, Boolean showCopyPaste, object grid)
        {
            Debug.Assert(div != null);
            if (div == null) return;

            //Show the context menu
            Fluent.ContextMenu cm = new Fluent.ContextMenu();

            Fluent.MenuItem item = new Fluent.MenuItem();
            item.Command = this.ViewModel.RemoveDividerCommand;
            item.CommandParameter = div;
            item.Header = this.ViewModel.RemoveDividerCommand.FriendlyName;
            item.Icon = this.ViewModel.RemoveDividerCommand.SmallIcon;
            cm.Items.Add(item);

            if (div.Type == PlanogramBlockingDividerType.Vertical)
            {
                cm.Items.Add(new Separator());

                item = new Fluent.MenuItem();
                item.IsCheckable = true;
                item.Header = Message.BlockingPlanDocument_DividerSnap;

                BindingOperations.SetBinding(item, Fluent.MenuItem.IsCheckedProperty,
                    new Binding(PlanogramBlockingDivider.IsSnappedProperty.Name) { Source = div, Mode = BindingMode.TwoWay });

                cm.Items.Add(item);
            }

            if (showCopyPaste)
            {
                //Copy
                cm.Items.Add(new Separator());
                item = new Fluent.MenuItem();
                item.Command = this.ViewModel.CopyDividerCommand;
                item.CommandParameter = div;
                item.Header = this.ViewModel.CopyDividerCommand.FriendlyName;
                item.Icon = this.ViewModel.CopyDividerCommand.SmallIcon;

                cm.Items.Add(item);

                //Paste
                item = new Fluent.MenuItem();
                item.Command = this.ViewModel.PasteDividerCommand;
                item.CommandParameter = grid;
                item.Header = this.ViewModel.PasteDividerCommand.FriendlyName;
                item.Icon = this.ViewModel.PasteDividerCommand.SmallIcon;

                cm.Items.Add(item);
            }

            cm.Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint;
            cm.IsOpen = true;
        }

        #endregion

        #region IPlanDocumentView Members

        public IPlanDocument GetIPlanDocument()
        {
            return this.ViewModel as IPlanDocument;
        }

        public Boolean IsDisposed { get; private set; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean isDisposing)
        {
            if (IsDisposed) return;


            this.blockingEditor.DividerPlaced -= blockingEditor_DividerPlaced;
            this.blockingEditor.SelectionChanged -= blockingEditor_SelectionChanged;
            this.blockingEditor.MouseMove -= BlockingEditor_MouseMove;

            if (isDisposing)
            {
                //never ever ever comment out this line below.. otherwise the entire plan memory leaks.
                this.ViewModel = null;

                //force the blocking editor to drop the planogram.
                this.blockingEditor.Dispose();
                this.BlockColorPicker.RecentColors = null;
            }

            if (_columnLayoutManager != null)
            {
                _columnLayoutManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                _columnLayoutManager.Dispose();
                _columnLayoutManager = null;
            }

            IsDisposed = true;

        }

        #endregion

               
    }

    public class WidthConverter : IValueConverter
    {
        private const Double ExpanderHeaderWidth = 15.0F;

        /// <summary>
        /// A simple converter to set the width of a grid to the width of the parent Expander minus the 
        /// width of the expander header panel (15 px)
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns>The new width of the grid</returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            // Get the value as a double
            Double? widthValue = value as Double?;
            if (widthValue == null)
            {
                return value;
            }

            // Remove the width of the expander
            widthValue -= ExpanderHeaderWidth;

            return widthValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class WidthMultiConverter : IMultiValueConverter
    {
        private const Double ExpanderHeaderWidth = 15.0F;

        /// <summary>
        /// A simple converter to set the width of a grid to the width of the parent Expander minus the 
        /// width of the expander header panel (15 px)
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values == null || values.Length < 2)
            {
                return null;
            }

            // Get the grid value as a double
            Double? widthValue = values[0] as Double?;
            if (widthValue == null || widthValue == 0.0F)
            {
                return values[0];
            }

            // And get the MinWidth value of the parent grid
            Double? minWidthValue = values[1] as Double?;

            // Remove the width of the expander
            if (widthValue > minWidthValue)
            {
                widthValue = (Double) widthValue - ExpanderHeaderWidth;
            }

            return widthValue;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}