﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26891 : L.Ineson 
//  Created.
// V8-27419 : L.Luong
//  Added method to change name if duplicate name has been entered

#endregion

#region Version History: (CCM 801)

// V8-27554 : I.George
//  Added SmallIcon property to commands where they are missing
// V8-28786 : I.George
//  Updated SmallIcon property to use 16 x 16 icon size
// V8-28103 : M.Shelley
//  Modified ".." to localised message label

#endregion

#region Version History: CCM802

// V8-28990 : A.Silva
//      Removed Dynamic Product List related Commands.
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information
// V8-29000 : A.Silva
//      Added BlockingOnBlockingChangeCompleted handler for when the blocking changes are complete, so that the sequence can be recalculated.
// V8-28997 : A.Silva
//      Added UpdateSequenceProducts() as a public method to get the products in sequence groups from the current blocking.
// V8-28999 : A.Silva
//      Ammended UpdateSequenceProducts to pass the CurrentBlocking as the blocking to be applied.

#endregion

#region Version History: CCM803

// V8-29518 : A.Silva
//      Added GenerateProductSequenceCommand.

#endregion

#region Version History: CCM810

// V8-29794 : M.Pettit
//  Altered Load blocking to load from Planogram for this version as we do not have master blocking templates yet
// V8-28878 : A.Silva
//  Amended LoadFromRepositoryCommand_Executed to not check for AttachedControl, as the method can now be unit tested.

#endregion

#region Version History: CCM811

// V8-29184 : M.Brumby
//  Added calculation of merchandising volume percentages
// V8-30529 : D.Pleasance
//  Added lock to MerchandisingGroups to prevent merchandising groups from changing whilst processing GetPlanogramBlockingGroupMerchandisingVolumePercentages.
// V8-29807 : A.Kuszyk
//  Re-factored duplicate blocking group name logic into blocking model.
// V8-30472 : A.Silva
//  Added resetting the Blocking Tool to pointer when the Document's Controller is no longer active.
// V8-30631 : A.Silva
//  Disabled Generate Product Sequence if there are more than one blocking groups with the same colour.

#endregion

#region Version History: CCM820

// V8-30472 : A.Silva
//  moved the resetting of the Blocking tool to whenever the Blocking Plan Document is no longer active.
// V8-30792 : A.Kuszyk
//  Added support for highlights and introduced ShowHighlightLayer and ShowHighlightLayerEnabled properties.
// V8-29968 : A.Kuszyk
//  Added information message to GenerateProductSequence command and also added awareness of IsSequenceUpToDate flag in CanExecute.
//  Added NotifyBlockingChanged to update status of IsSequenceUpToDate flag under the correct circumstances.
// V8-30791 : D.Pleasance
//  Added color sets (Product, Highlight, Sequence strategy)
// V8-30536 : A.Kuszyk
//  Re-factored updating of merchandising volume percentages into background thread on PlanogramView.
// V8-31077 : A.Kuszyk
//  Added LoadSequence command and modified to use view models.
// V8-31070 : D.Pleasance
//  Removed Sequence strategy color set
// V8-31162 : A.Kuszyk
//  Added Load from file/repository commands.
// V8-30737 : M.Shelley
//  Added blocking group meta performance data
#endregion

#region Version History: CCM830

// V8-31761 : D.Pleasance
//  Amended so that repository planogram group is remembered.
// V8-32089 : N.Haywood
//  Added Custom Columns
// V8-32940 : L.Ineson
//  Added ClearBlockingCommand.
// CCM-18321 : L.Bailey
//  Multi-select Blocks & Dividers and apply certain settings to all selected
// CCM-18674 : M.Pettit
//  Added abiliy for users to set the block space limit by block space % or planogram %
#endregion

#region Version History: CCM833
// CCM-19010 : J.Mendes
//  Disable the ClearBlocking only when there is no groups instead of disabled when there is no divider.
#endregion

#endregion

using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Controls.Wpf.BlockingEditor;
using Galleria.Framework.Planograms.Controls.Wpf.ViewModel;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.ViewModel;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.Blocking
{
    /// <summary>
    /// Plan document controller for viewing and editing the planogram blocking styles.
    /// </summary>
    public sealed class BlockingPlanDocument : PlanDocument<BlockingPlanDocumentView>
    {
        #region Constants

        /// <summary>
        ///     Type of column layout used by the <see cref="ColumnLayoutManager" /> in this instance.
        /// </summary>
        public const CustomColumnLayoutType ColumnLayoutType = CustomColumnLayoutType.Product;

        /// <summary>
        ///     Name of the column layout preferred by the user to be used by the <see cref="ColumnLayoutManager" /> in this
        ///     instance.
        /// </summary>
        public const String ScreenKey = "BlockingPlanDocument";

        #endregion

        #region Fields
        private Single _blockingHeight;
        private Single _blockingWidth;
        private Single _blockingHeightOffset;
        private PlanogramBlockingViewModel _currentBlocking;
        private PlanogramBlockingLocation _selectedBlockLocation;
        private PlanogramBlockingGroupViewModel _selectedBlockGroup;
        private BlockingToolType _toolType;
        private String _labelFormat;
        private Boolean _showHighlightLayer;
        private Boolean _singleSelectedBlock = true;
        private PlanogramBlockingDivider _copiedPasteDivider = null;
        private Boolean _copiedPasteDivider_IsLimit;
        private Boolean _copiedPasteDivider_IsSnapped;
        private Single _copiedPasteDivider_LimitedPercentage;

        private ObservableCollection<IPlanogramBlockingLocation> _selectedItems;

        private Dictionary<Object, Double> _merchandisingVolumePercentagesByBlockingGroupId = new Dictionary<Object, Double>();
        private Int32? _repositoryPlanogramGroupId = null;

        private readonly BulkObservableCollection<ColorItem> _productColors = new BulkObservableCollection<ColorItem>();
        private readonly ReadOnlyBulkObservableCollection<ColorItem> _productColorsRO;

        private readonly BulkObservableCollection<ColorItem> _highlightColors = new BulkObservableCollection<ColorItem>();
        private readonly ReadOnlyBulkObservableCollection<ColorItem> _highlightColorsRO;

        BulkObservableCollection<BlockingGroupDocumentPerformanceItem> _metaPropertyItemsValues = 
            new BulkObservableCollection<BlockingGroupDocumentPerformanceItem>();

        private ObservableCollection<PlanogramBlockingGroupViewModel> _selectedBlockingGroups;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath BlockingHeightProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.BlockingHeight);
        public static readonly PropertyPath BlockingWidthProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.BlockingWidth);
        public static readonly PropertyPath AvailableBlockingProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.AvailableBlocking);
        public static readonly PropertyPath CurrentBlockingProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.CurrentBlocking);
        public static readonly PropertyPath CurrentBlockingHeaderProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.CurrentBlockingHeader);
        public static readonly PropertyPath SelectedBlockLocationProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.SelectedBlockLocation);
        public static readonly PropertyPath SelectedBlockGroupProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.SelectedBlockGroup);
        public static readonly PropertyPath ToolTypeProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.ToolType);
        public static readonly PropertyPath LabelFormatProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.LabelFormat);
        public static readonly PropertyPath ShowHighlightLayerProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.ShowHighlightLayer);
        public static readonly PropertyPath ShowHighlightLayerEnabledProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.ShowHighlightLayerEnabled);
        public static readonly PropertyPath ProductColorsProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.ProductColors);
        public static readonly PropertyPath HighlightColorsProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.HighlightColors);
        public static readonly PropertyPath CurrentProductsProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(o => o.CurrentProducts);
        public static readonly PropertyPath SingleSelectedBlockProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.SingleSelectedBlock);
        public static readonly PropertyPath SelectedItemsProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.SelectedItems);
        public static readonly PropertyPath SelectedBlockingGroupsProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.SelectedBlockingGroups);

        //Commands
        public static readonly PropertyPath LoadCommandProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.LoadCommand);
        public static readonly PropertyPath LoadFromRepositoryCommandProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.LoadFromRepositoryCommand);
        public static readonly PropertyPath LoadFromFileCommandProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.LoadFromFileCommand);
        public static readonly PropertyPath ZoomToFitCommandProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.ZoomToFitCommand);
        public static readonly PropertyPath ZoomToFitHeightCommandProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.ZoomToFitHeightCommand);
        public static readonly PropertyPath ZoomInCommandProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.ZoomInCommand);
        public static readonly PropertyPath ZoomOutCommandProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.ZoomOutCommand);
        public readonly static PropertyPath GenerateProductSequenceCommandProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.GenerateProductSequenceCommand);
        public static readonly PropertyPath LoadSequenceFromRepositoryCommandProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.LoadSequenceFromRepositoryCommand);
        public static readonly PropertyPath LoadSequenceFromFileCommandProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.LoadSequenceFromFileCommand);
        public static readonly PropertyPath ClearBlockingCommandProperty = WpfHelper.GetPropertyPath<BlockingPlanDocument>(p => p.ClearBlockingCommand);

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public PlanogramBlockingDivider CopiedPasteDivider
        {
            get { return _copiedPasteDivider; }
            private set
            {
                _copiedPasteDivider = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean CopiedPasteDivider_IsLimit
        {
            get { return _copiedPasteDivider_IsLimit; }
            private set
            {
                _copiedPasteDivider_IsLimit = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Boolean CopiedPasteDivider_IsSnapped
        {
            get { return _copiedPasteDivider_IsSnapped; }
            private set
            {
                _copiedPasteDivider_IsSnapped = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Single CopiedPasteDivider_LimitedPercentage
        {
            get { return _copiedPasteDivider_LimitedPercentage; }
            private set
            {
                _copiedPasteDivider_LimitedPercentage = value;
            }
        }

        /// <summary>
        /// Returns the height of the blocking area.
        /// </summary>
        public Single BlockingHeight
        {
            get { return _blockingHeight; }
            private set
            {
                _blockingHeight = value;
                OnPropertyChanged(BlockingHeightProperty);
            }
        }

        /// <summary>
        /// Returns the width of the blocking area.
        /// </summary>
        public Single BlockingWidth
        {
            get { return _blockingWidth; }
            private set
            {
                _blockingWidth = value;
                OnPropertyChanged(BlockingWidthProperty);
            }
        }

        /// <summary>
        /// Returns the list of blocking for this planogram.
        /// </summary>
        public PlanogramBlockingList AvailableBlocking
        {
            get { return Planogram.Model.Blocking; }
        }

        /// <summary>
        /// Gets/Sets the currently selected blocking.
        /// </summary>
        public PlanogramBlockingViewModel CurrentBlocking
        {
            get { return _currentBlocking; }
            set
            {
                PlanogramBlockingViewModel oldValue = _currentBlocking;

                _currentBlocking = value;
                OnPropertyChanged(CurrentBlockingProperty);
                OnPropertyChanged(CurrentBlockingHeaderProperty);

                OnCurrentBlockingChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Returns the header value for the current blocking.
        /// </summary>
        public String CurrentBlockingHeader
        {
            get
            {
                if (this.CurrentBlocking == null) return String.Empty;
                if (this.CurrentBlocking.Type == PlanogramBlockingType.Final)
                {
                    return PlanogramBlockingTypeHelper.FriendlyNames[this.CurrentBlocking.Type];
                }
                else
                {
                    return String.Format("{0} {1}",
                        PlanogramBlockingTypeHelper.FriendlyNames[this.CurrentBlocking.Type],
                        Message.BlockingPlanDocument_ReadOnly);
                }
            }
        }

        /// <summary>
        /// Gets/Sets the selected block location.
        /// </summary>
        public PlanogramBlockingLocation SelectedBlockLocation
        {
            get { return _selectedBlockLocation; }
            set
            {
                _selectedBlockLocation = value;
                OnPropertyChanged(SelectedBlockLocationProperty);

                this.SelectedBlockGroup =
                    (value != null) ? 
                    CurrentBlocking.Groups.FirstOrDefault(g => g.Model == value.GetPlanogramBlockingGroup()) : 
                    null;
            }
        }

        /// <summary>
        /// Resturns the currently selected blocking group.
        /// </summary>
        public PlanogramBlockingGroupViewModel SelectedBlockGroup
        {
            get { return _selectedBlockGroup; }
            set
            {
                if (_selectedBlockGroup != value)
                {
                    PlanogramBlockingGroupViewModel oldValue = _selectedBlockGroup;

                    _selectedBlockGroup = value;
                    OnPropertyChanged(SelectedBlockGroupProperty);

                    OnSelectedBlockGroupChanged(oldValue, value);

                    //OnPropertyChanged(MetaPropertyItemValuesProperty);
                    //FetchPerformanceValues();
                }
            }
        }

        /// <summary>
        /// Gets/Sets the active mouse tool type
        /// </summary>
        public BlockingToolType ToolType
        {
            get { return _toolType; }
            set
            {
                _toolType = value;
                OnPropertyChanged(ToolTypeProperty);
            }
        }

        /// <summary>
        /// Gets the label format for the blocking editor rulers to use.
        /// </summary>
        public String LabelFormat
        {
            get { return _labelFormat; }
            private set
            {
                _labelFormat = value;
                OnPropertyChanged(LabelFormatProperty);
            }
        }

        /// <summary>
        /// Indicates whether or not the highlight layer should be rendered.
        /// </summary>
        public Boolean ShowHighlightLayer
        {
            get { return _showHighlightLayer; }
            set
            {
                _showHighlightLayer= value;
                OnPropertyChanged(ShowHighlightLayerProperty);
            }
        }

        /// <summary>
        /// Indicates if the Show Positions button is enabled.
        /// </summary>
        public Boolean ShowHighlightLayerEnabled
        {
            get { return Planogram.EnumerateAllPositions().Any(); }
        }

        public override ReadOnlyBulkObservableCollection<PlanogramHighlightResultGroup> HighlightLegendItems
        {
            get
            {
                return new ReadOnlyBulkObservableCollection<PlanogramHighlightResultGroup>(new BulkObservableCollection<PlanogramHighlightResultGroup>());
            }
        }

        #region CurrentProducts
        /// <summary>
        ///     Holds the value for the <see cref="CurrentProducts"/> property.
        /// </summary>
        private readonly BulkObservableCollection<PlanogramSequenceGroupProduct> _currentProducts = new BulkObservableCollection<PlanogramSequenceGroupProduct>();

        /// <summary>
        ///     Holds the value for the <see cref="CurrentProducts"/> property.
        /// </summary>
        private readonly ReadOnlyBulkObservableCollection<PlanogramSequenceGroupProduct> _currentProductsRo;

        /// <summary>
        ///     Gets or sets the value of the <see cref="CurrentProducts"/> property, notifying when the value changes.
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramSequenceGroupProduct> CurrentProducts
        {
            get { return _currentProductsRo; }
        }

        //public BulkObservableCollection<BlockingGroupDocumentPerformanceItem> MetaPropertyItemValues
        //{
        //    get
        //    {
        //        FetchPerformanceValues();

        //        return _metaPropertyItemsValues;
        //    }
        //}

        #endregion

        Double _selectedMerchandisableBlockPercentage = 1;
        public Double SelectedMerchandisableBlockPercentage
        {
            get
            {
                return _selectedMerchandisableBlockPercentage;
            }
            private set
            {
                _selectedMerchandisableBlockPercentage = value;
                OnPropertyChanged("SelectedMerchandisableBlockPercentage");
            }

        }

        /// <summary>
        /// Returns the collection of available product colours.
        /// </summary>
        public ReadOnlyBulkObservableCollection<ColorItem> ProductColors
        {
            get { return _productColorsRO; }
        }

        /// <summary>
        /// Returns the collection of available highlight colours.
        /// </summary>
        public ReadOnlyBulkObservableCollection<ColorItem> HighlightColors
        {
            get { return _highlightColorsRO; }
        }


        /// <summary>
        /// Indicates whether or not the highlight layer should be rendered.
        /// </summary>
        public Boolean SingleSelectedBlock
        {
            get { return _singleSelectedBlock; }
            set
            {
                _singleSelectedBlock = value;
                OnPropertyChanged(SingleSelectedBlockProperty);

            }
        }

        /// <summary>
        /// Indicates whether or not the highlight layer should be rendered.
        /// </summary>
        public ObservableCollection<IPlanogramBlockingLocation> SelectedItems
        {
            get { return _selectedItems; }
            set
            {
                _selectedItems = value;
                OnPropertyChanged(SelectedItemsProperty);
                //set the list of blocking group view models
                SetSelectedBlockingGroups();
            }
        }

        public ObservableCollection<PlanogramBlockingGroupViewModel> SelectedBlockingGroups
        {
            get { return _selectedBlockingGroups; }
            set
            {
                _selectedBlockingGroups = value;
                OnPropertyChanged(SelectedBlockingGroupsProperty);
            }
        }

        private void SetSelectedBlockingGroups()
        {
            if (_selectedItems != null & _selectedItems.Count > 0)
            {
                List<PlanogramBlockingGroupViewModel> vms = new List<PlanogramBlockingGroupViewModel>();
                foreach (PlanogramBlockingLocation location in _selectedItems)
                {
                    PlanogramBlockingGroupViewModel vm = CurrentBlocking.Groups.FirstOrDefault(g => g.Model == location.GetPlanogramBlockingGroup());
                    if (vm != null)
                    {
                        vms.Add(vm);
                    }
                }
                SelectedBlockingGroups = vms.ToObservableCollection<PlanogramBlockingGroupViewModel>();
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="controller"></param>
        public BlockingPlanDocument(PlanControllerViewModel controller)
            : base(controller)
        {
            _currentProductsRo = new ReadOnlyBulkObservableCollection<PlanogramSequenceGroupProduct>(_currentProducts);
            _productColorsRO = new ReadOnlyBulkObservableCollection<ColorItem>(_productColors);
            _highlightColorsRO = new ReadOnlyBulkObservableCollection<ColorItem>(_highlightColors);
            
            PlanogramView plan = controller.SourcePlanogram;

            Dictionary<String, PlanogramProduct> products = plan.Model.Products.Distinct().ToDictionary(p => p.Gtin);

            foreach (PlanogramSequenceGroup group in plan.Model.Sequence.Groups)
            {
                foreach (PlanogramSequenceGroupProduct prod in group.Products)
                {
                    if(products.ContainsKey(prod.Gtin))
                    {
                        prod.Product = products[prod.Gtin];
                    }
                }
            }

            SetPlanEventHandlers(true);

            //update the blocking height and width
            UpdateBlockingAreaDimensions();

            //make sure there is a final blocking and select it.
            PlanogramBlocking final = plan.Model.Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.Final);
            if (final == null)
            {
                //if we have an initial then take a copy of that.
                PlanogramBlocking initial = plan.Model.Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.Initial);
                final = initial != null
                            ? PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, initial)
                            : PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final);
                plan.Model.Blocking.Add(final);
            }

            UpdateLabelFormat();

            this.CurrentBlocking = new PlanogramBlockingViewModel(final, plan.Model.Sequence);

            // Enable highlighting for this document.
            IsHighlightingSupported = true;
            
            InitialiseProductColorSets();

            // Ensure we stay up-to-date with sequence changes
            base.ParentController.PropertyChanged += new PropertyChangedEventHandler(Controller_PropertyChanged);
        }

        #endregion

        #region Commands

        #region Load Blocking Commands

        #region Load

        private RelayCommand _loadCommand;

        /// <summary>
        /// Allows the user to select a blocking masterdata to load in.
        /// </summary>
        public RelayCommand LoadCommand
        {
            get
            {
                if (_loadCommand == null)
                {
                    _loadCommand = new RelayCommand(
                        p => LoadCommand_Executed(),
                        p => Load_CanExecute())
                    {
                        FriendlyName = Message.BlockingPlanDocument_LoadCommand_Name, 
                        FriendlyDescription = Message.BlockingPlanDocument_LoadCommand_Description,
                        Icon = ImageResources.BlockingPlanDocument_LoadFromRepository32,
                        SmallIcon = ImageResources.BlockingPlanDocument_LoadFromRepository16
                    };
                    base.ViewModelCommands.Add(_loadCommand);
                }
                return _loadCommand;
            }
        }

        private Boolean Load_CanExecute()
        {
            return LoadFromRepositoryCommand.CanExecute() || LoadFromFileCommand.CanExecute();
        }

        /// <summary>
        /// Loads from repository or file.
        /// </summary>
        private void LoadCommand_Executed()
        {
            if (LoadFromRepositoryCommand.CanExecute())
            {
                LoadFromRepositoryCommand.Execute();
            }
            else if (LoadFromFileCommand.CanExecute())
            {
                LoadFromFileCommand.Execute();
            }
        }

        #endregion

        #region LoadFromRepository

        private RelayCommand _loadFromRepositoryCommand;

        /// <summary>
        /// Allows the user to select a blocking masterdata to load in.
        /// </summary>
        public RelayCommand LoadFromRepositoryCommand
        {
            get
            {
                if (_loadFromRepositoryCommand == null)
                {
                    _loadFromRepositoryCommand = new RelayCommand(
                        p => LoadFromRepositoryCommand_Executed(),
                        p => LoadFromRepository_CanExecute())
                        {
                            FriendlyName = Message.BlockingPlanDocument_LoadFromRepository,
                            FriendlyDescription = Message.BlockingPlanDocument_LoadFromRepository_Desc,
                            Icon = ImageResources.BlockingPlanDocument_LoadFromRepository32,
                            SmallIcon = ImageResources.BlockingPlanDocument_LoadFromRepository16
                        };
                    base.ViewModelCommands.Add(_loadFromRepositoryCommand);
                }
                return _loadFromRepositoryCommand;
            }
        }

        private Boolean LoadFromRepository_CanExecute()
        {
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.LoadFromRepositoryCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }

            return true;
        }

        /// <summary>
        ///     Load the <see cref="Blocking"/> Strategy from a user selected <c>Planogram</c> from the <c>Repository</c>.
        /// </summary>
        private void LoadFromRepositoryCommand_Executed()
        {
            Planogram plan = OpenPlanogramFromRepository();
            if (plan == null) return;
            GetBlockingAndCopyToPlan(plan);
        }

        #endregion

        #region LoadFromFile

        private RelayCommand _loadFromFileCommand;

        /// <summary>
        /// Allows the user to select a blocking masterdata to load in.
        /// </summary>
        public RelayCommand LoadFromFileCommand
        {
            get
            {
                if (_loadFromFileCommand == null)
                {
                    _loadFromFileCommand = new RelayCommand(
                        p => LoadFromFileCommand_Executed(),
                        p => LoadFromFile_CanExecute())
                    {
                        FriendlyName = Message.BlockingPlanDocument_LoadFromFileCommand_Name, 
                        FriendlyDescription = Message.BlockingPlanDocument_LoadFromFileCommand_Description,
                        Icon = ImageResources.BlockingPlanDocument_LoadFromRepository32,
                        SmallIcon = ImageResources.BlockingPlanDocument_LoadFromRepository16
                    };
                    base.ViewModelCommands.Add(_loadFromFileCommand);
                }
                return _loadFromFileCommand;
            }
        }

        private Boolean LoadFromFile_CanExecute()
        {
            // Can always load from file.
            return true;
        }

        /// <summary>
        ///     Load the <see cref="Blocking"/> Strategy from a user selected <c>Planogram</c>.
        /// </summary>
        private void LoadFromFileCommand_Executed()
        {
            Planogram plan = OpenPlanogramFromFile();
            if (plan == null) return;
            GetBlockingAndCopyToPlan(plan);
        }

        #endregion 

        /// <summary>
        /// Gets the best blocking from the give <paramref name="plan"/> and copies it to this <see cref="Planogram"/>
        /// as the initial and final blocking.
        /// </summary>
        /// <param name="plan"></param>
        /// <returns></returns>
        private Boolean GetBlockingAndCopyToPlan(Planogram plan)
        {
            if (!plan.Blocking.Any()) return false;

            ShowWaitCursor(true);

            //we have the plan, now try to get the final blocking strategy
            //If there is no final, select the initial instead
            PlanogramBlocking selectedBlocking =
                plan.Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.Final) ??
                plan.Blocking.FirstOrDefault(b => b.Type == PlanogramBlockingType.Initial);

            //If there is no strategy, inform the user
            if (selectedBlocking == null)
            {
                //the plan does not have any blocking strategy
                CommonHelper.GetWindowService().ShowOkMessage(
                    MessageWindowType.Warning,
                    Message.BlockingPlanDocument_Load_PlanHasNoBlockingHeader,
                    String.Format(Message.BlockingPlanDocument_Load_PlanHasNoBlockingDescription, plan.Name));
                ShowWaitCursor(false);
                return false;
            }

            //clear out any existing blocking.
            Planogram.Model.Blocking.Clear();

            //create the initial and the final blocking from the selected planogram's blocking
            PlanogramBlocking copyBlocking = selectedBlocking.Copy();
            copyBlocking.Type = PlanogramBlockingType.Initial;
            Planogram.Model.Blocking.Add(copyBlocking);

            PlanogramBlocking finalBlocking = PlanogramBlocking.NewPlanogramBlocking(PlanogramBlockingType.Final, selectedBlocking);
            Planogram.Model.Blocking.Add(finalBlocking);

            //load in the new one and select
            CurrentBlocking = new PlanogramBlockingViewModel(finalBlocking, Planogram.Model.Sequence);

            Dictionary<String, PlanogramProduct> products = Planogram.Model.Products.Distinct().ToDictionary(p => p.Gtin);

            foreach (PlanogramSequenceGroup group in Planogram.Model.Sequence.Groups)
            {
                foreach (PlanogramSequenceGroupProduct prod in group.Products)
                {
                    if (products.ContainsKey(prod.Gtin))
                    {
                        prod.Product = products[prod.Gtin];
                    }
                }
            }

            ShowWaitCursor(false);
            return true;
        }

        #endregion

        #region RemoveDividerCommand

        private RelayCommand _removeDividerCommand;

        /// <summary>
        /// Removes the given divider
        /// </summary>
        public RelayCommand RemoveDividerCommand
        {
            get
            {
                if (_removeDividerCommand == null)
                {
                    _removeDividerCommand = new RelayCommand(
                        RemoveDivider_Executed,
                        RemoveDivider_CanExecute)
                    {
                        FriendlyName = Message.BlockingPlanDocument_DeleteDividerTool,
                        SmallIcon = ImageResources.BlockingPlanDocument_DeleteDividerTool32
                    };
                    base.ViewModelCommands.Add(_removeDividerCommand);
                }
                return _removeDividerCommand;
            }
        }

        private Boolean RemoveDivider_CanExecute(Object args)
        {
            if (this.CurrentBlocking == null || args == null)
            {
                return false;
            }

            return true;
        }

        private void RemoveDivider_Executed(Object args)
        {
            PlanogramBlockingDivider div = args as PlanogramBlockingDivider;
            if (args == null) return;


            this.CurrentBlocking.Model.Dividers.Remove(div);
        }

        #endregion

        #region ZoomToFit

        private RelayCommand _zoomToFitCommand;

        /// <summary>
        /// Zooms to fit the entire blocking area
        /// </summary>
        public RelayCommand ZoomToFitCommand
        {
            get
            {
                if (_zoomToFitCommand == null)
                {
                    _zoomToFitCommand = new RelayCommand(
                        p => ZoomToFit_Executed(),
                        p => ZoomToFit_CanExecute())
                    {
                        FriendlyName = Message.BlockingPlanDocument_ZoomToFit,
                        FriendlyDescription = Message.BlockingPlanDocument_ZoomToFit_Desc,
                        SmallIcon = ImageResources.BlockingPlanDocument_ZoomToFit16
                    };
                    base.ViewModelCommands.Add(_zoomToFitCommand);
                }
                return _zoomToFitCommand;
            }
        }

        private Boolean ZoomToFit_CanExecute()
        {
            if (this.CurrentBlocking == null)
            {
                this.ZoomToFitCommand.DisabledReason = Message.BlockingPlanDocument_NoCurrentItem;
                return false;
            }

            return true;
        }

        private void ZoomToFit_Executed()
        {
            //Handled in code behind.
        }

        #endregion

        #region ZoomToFitHeight

        private RelayCommand _zoomToFitHeightCommand;

        /// <summary>
        /// Fits the blocking area by height
        /// </summary>
        public RelayCommand ZoomToFitHeightCommand
        {
            get
            {
                if (_zoomToFitHeightCommand == null)
                {
                    _zoomToFitHeightCommand = new RelayCommand(
                        p => ZoomToFitHeight_Executed(),
                        p => ZoomToFitHeight_CanExecute())
                    {
                        FriendlyName = Message.BlockingPlanDocument_ZoomToFitHeight,
                        FriendlyDescription = Message.BlockingPlanDocument_ZoomToFitHeight_Desc,
                        Icon = ImageResources.BlockingPlanDocument_ZoomToFitHeight32,
                        SmallIcon = ImageResources.BlockingPlanDocument_ZoomToFitHeight16
                    };
                    base.ViewModelCommands.Add(_zoomToFitHeightCommand);
                }
                return _zoomToFitHeightCommand;
            }
        }

        private Boolean ZoomToFitHeight_CanExecute()
        {
            if (this.CurrentBlocking == null)
            {
                this.ZoomToFitHeightCommand.DisabledReason = Message.BlockingPlanDocument_NoCurrentItem;
                return false;
            }

            return true;
        }

        private void ZoomToFitHeight_Executed()
        {
            //Handled in code behind.
        }

        #endregion

        #region ZoomIn
        private RelayCommand _zoomInCommand;

        /// <summary>
        /// Zooms in on the blocking area.
        /// </summary>
        public RelayCommand ZoomInCommand
        {
            get
            {
                if (_zoomInCommand == null)
                {
                    _zoomInCommand = new RelayCommand(
                        p => ZoomIn_Executed(),
                        p => ZoomIn_CanExecute())
                    {
                        FriendlyName = Message.BlockingPlanDocument_ZoomIn,
                        FriendlyDescription = Message.BlockingPlanDocument_ZoomIn_Desc,
                        SmallIcon = ImageResources.BlockingPlanDocument_ZoomIn16,
                    };
                    base.ViewModelCommands.Add(_zoomInCommand);
                }
                return _zoomInCommand;
            }
        }

        private Boolean ZoomIn_CanExecute()
        {
            if (this.CurrentBlocking == null)
            {
                this.ZoomInCommand.DisabledReason = Message.BlockingPlanDocument_NoCurrentItem;
                return false;
            }

            return true;
        }

        private void ZoomIn_Executed()
        {
            //Handled in code behind.
        }

        #endregion

        #region ZoomOut

        private RelayCommand _zoomOutCommand;

        /// <summary>
        /// Zooms out the blocking area.
        /// </summary>
        public RelayCommand ZoomOutCommand
        {
            get
            {
                if (_zoomOutCommand == null)
                {
                    _zoomOutCommand = new RelayCommand(
                        p => ZoomOut_Executed(),
                        p => ZoomOut_CanExecute())
                    {
                        FriendlyName = Message.BlockingPlanDocument_ZoomOut,
                        FriendlyDescription = Message.BlockingPlanDocument_ZoomOut_Desc,
                        SmallIcon = ImageResources.BlockingPlanDocument_ZoomOut16,
                    };
                    base.ViewModelCommands.Add(_zoomOutCommand);
                }
                return _zoomOutCommand;
            }
        }

        private Boolean ZoomOut_CanExecute()
        {
            if (this.CurrentBlocking == null)
            {
                this.ZoomOutCommand.DisabledReason = Message.BlockingPlanDocument_NoCurrentItem;
                return false;
            }

            return true;
        }

        private void ZoomOut_Executed()
        {
            //Handled in code behind.
        }

        #endregion

        #region Generate Product Sequence

        /// <summary>
        ///     Reference to the current instance for <see cref="GenerateProductSequenceCommand"/>.
        /// </summary>
        private RelayCommand _generateProductSequenceCommand;

        /// <summary>
        ///     Gets the reference for <see cref="GenerateProductSequenceCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance for it already.</remarks>
        public RelayCommand GenerateProductSequenceCommand
        {
            get
            {
                if (_generateProductSequenceCommand != null) return _generateProductSequenceCommand;

                _generateProductSequenceCommand = new RelayCommand(p => GenerateProductSequence_Executed(),
                                                                   p => GenerateProductSequence_CanExecute())
                                                  {
                                                      FriendlyName = Message.BlockingPlanDocument_GenerateProductSequence,
                                                      FriendlyDescription = Message.BlockingPlanDocument_GenerateProductSequence_Description,
                                                      Icon = ImageResources.BlockingPlanDocument_GenerateProductSequence32,
                                                      SmallIcon = ImageResources.BlockingPlanDocument_GenerateProductSequence16
                                                  };
                ViewModelCommands.Add(_generateProductSequenceCommand);
                return _generateProductSequenceCommand;
            }
        }

        /// <summary>
        ///     Determine when the <see cref="GenerateProductSequenceCommand"/> should be enabled.
        /// </summary>
        private Boolean GenerateProductSequence_CanExecute()
        {
            if (!CurrentBlocking.Groups.Any())
            {
                GenerateProductSequenceCommand.DisabledReason = Message.BlockingPlanDocument_GenerateProductSequence_DisabledReason_NoBlocks; 
                return false;
            }

            //  The planogram must have at least one position to sequence.
            if (!Planogram.EnumerateAllPositions().Any())
            {
                GenerateProductSequenceCommand.DisabledReason = Message.BlockingPlanDocument_GenerateProductSequence_DisabledReason_NoPositions;
                return false;
            }

            // Each Blocking Group needs to have different colours.
            if (CurrentBlocking.Groups.GroupBy(g => g.Colour).Any(g => g.Count() > 1))
            {
                GenerateProductSequenceCommand.DisabledReason = Message.BlockingPlanDocument_GenerateProductSequence_DisabledReason_DuplicateBlockColours;
                return false;
            }

            if (ParentController.IsSequenceUpToDate)
            {
                GenerateProductSequenceCommand.DisabledReason = Message.BlockingPlanDocument_GenerateProductSequence_DisabledReason_SequenceAlreadyUpdated; 
                return false;
            }

            return true;
        }

        /// <summary>
        ///     Generate the product sequences for the final blocking strategy from the current positions.
        /// </summary>
        private void GenerateProductSequence_Executed()
        {
            base.ShowWaitCursor(true);
            Planogram.Model.Sequence.ApplyFromBlocking(CurrentBlocking.Model);
            UpdateCurrentBlockGroupProducts();
            this.ParentController.IsSequenceUpToDate = true;
            base.ShowWaitCursor(false);
            Galleria.Ccm.Services.ServiceContainer.GetService<IWindowService>().ShowOkMessage(
                MessageWindowType.Info,
                Message.BlockingPlanDocument_GenerateProductSequence_ProductSequenceUpdated, 
                Message.BlockingPlanDocument_GenerateProductSequence_ProductSequenceUpdated_Description);
        }

        #endregion

        #region Load Sequence Commands

        #region LoadSequenceFromRepository

        private RelayCommand _loadSequenceFromRepositoryCommand;

        /// <summary>
        /// Allows the user to select a blocking masterdata to load in.
        /// </summary>
        public RelayCommand LoadSequenceFromRepositoryCommand
        {
            get
            {
                if (_loadSequenceFromRepositoryCommand == null)
                {
                    _loadSequenceFromRepositoryCommand = new RelayCommand(
                        p => LoadSequenceFromRepositoryCommand_Executed(),
                        p => LoadSequenceFromRepository_CanExecute())
                    {
                        FriendlyName = Message.BlockingPlanDocument_LoadSequence,
                        FriendlyDescription = Message.BlockingPlanDocument_LoadSequenceDescription,
                        Icon = ImageResources.BlockingPlanDocument_LoadProductSequence32,
                        SmallIcon = ImageResources.BlockingPlanDocument_LoadFromRepository16
                    };
                    base.ViewModelCommands.Add(_loadSequenceFromRepositoryCommand);
                }
                return _loadSequenceFromRepositoryCommand;
            }
        }

        private Boolean LoadSequenceFromRepository_CanExecute()
        {
            if (!App.ViewState.IsConnectedToRepository)
            {
                this.LoadSequenceFromRepositoryCommand.DisabledReason = Message.DisabledReason_NoRepositoryConnected;
                return false;
            }

            return true;
        }

        /// <summary>
        ///     Load the Sequence from a user selected <c>Planogram</c> from the <c>Repository</c>.
        /// </summary>
        private void LoadSequenceFromRepositoryCommand_Executed()
        {
            //Show a window to select a planogram
            Planogram plan = OpenPlanogramFromRepository();
            if (plan == null) return;
            LoadSequenceFromPlanogram(plan);
        }

        #endregion 

        #region LoadSequenceFromFile

        private RelayCommand _loadSequenceFromFileCommand;

        /// <summary>
        /// Allows the user to select a blocking masterdata to load in.
        /// </summary>
        public RelayCommand LoadSequenceFromFileCommand
        {
            get
            {
                if (_loadSequenceFromFileCommand == null)
                {
                    _loadSequenceFromFileCommand = new RelayCommand(
                        p => LoadSequenceFromFileCommand_Executed(),
                        p => LoadSequenceFromFile_CanExecute())
                    {
                        FriendlyName = Message.BlockingPlanDocument_LoadSequenceFromFile_Name, 
                        FriendlyDescription = Message.BlockingPlanDocument_LoadSequenceFromFile_Description,
                        Icon = ImageResources.BlockingPlanDocument_LoadProductSequence32,
                        SmallIcon = ImageResources.BlockingPlanDocument_LoadFromRepository16
                    };
                    base.ViewModelCommands.Add(_loadSequenceFromFileCommand);
                }
                return _loadSequenceFromFileCommand;
            }
        }

        private Boolean LoadSequenceFromFile_CanExecute()
        {
            // Can always load from file.
            return true;
        }

        /// <summary>
        ///     Load the Sequence from a user selected <c>Planogram</c> from the <c>Repository</c>.
        /// </summary>
        private void LoadSequenceFromFileCommand_Executed()
        {
            //Show a window to select a planogram
            Planogram plan = OpenPlanogramFromFile();
            if (plan == null) return;
            LoadSequenceFromPlanogram(plan);
        }

        #endregion 

        /// <summary>
        /// Loads the sequence information from <paramref name="plan"/> into <see cref="Planogram"/>.
        /// </summary>
        /// <param name="plan"></param>
        private void LoadSequenceFromPlanogram(Planogram plan)
        {
            if (plan == null) return;
            ShowWaitCursor(true);
            this.Planogram.Model.Sequence.LoadFrom(plan.Sequence);
            foreach (PlanogramBlockingGroupViewModel blockingGroup in CurrentBlocking.Groups)
            {
                blockingGroup.NotifySequenceGroupChanged();
            }

            Dictionary<String, PlanogramProduct> products = Planogram.Model.Products.Distinct().ToDictionary(p => p.Gtin);

            foreach (PlanogramSequenceGroup group in Planogram.Model.Sequence.Groups)
            {
                foreach (PlanogramSequenceGroupProduct prod in group.Products)
                {
                    if (products.ContainsKey(prod.Gtin))
                    {
                        prod.Product = products[prod.Gtin];
                    }
                }
            }
            ShowWaitCursor(false);
        }

        #endregion

        #region Clear Blocking

        private RelayCommand _clearBlockingCommand;

        /// <summary>
        /// Clears out all blocking.
        /// </summary>
        public RelayCommand ClearBlockingCommand
        {
            get
            {
                if (_clearBlockingCommand == null)
                {
                    _clearBlockingCommand = new RelayCommand(
                        p => ClearBlocking_Executed(),
                        p => ClearBlocking_CanExecute())
                        {
                            FriendlyName = Message.BlockingPlanDocument_ClearBlocking,
                            FriendlyDescription = Message.BlockingPlanDocument_ClearBlocking_Desc,
                            Icon = ImageResources.BlockingPlanDocument_ClearBlocking32
                         };
                    base.ViewModelCommands.Add(_clearBlockingCommand);
                }
                return _clearBlockingCommand;
            }
        }

        private Boolean ClearBlocking_CanExecute()
        {
            //must have final blocking selected.
            if (this.CurrentBlocking == null ||
                this.CurrentBlocking.Model.Type != PlanogramBlockingType.Final)
            {
                this.ClearBlockingCommand.DisabledReason =
                    Message.BlockingPlanDocument_ClearBlocking_DisabledReadOnlyType;
                return false;
            }

            if (!this.CurrentBlocking.Groups.Any())
            {
                this.ClearBlockingCommand.DisabledReason = 
                    Message.BlockingPlanDocument_ClearBlocking_DisabledNoBlocks;
                return false;
            }

            return true;
        }

        private void ClearBlocking_Executed()
        {
            //clear out any existing blocking.
            this.CurrentBlocking.Model.Dividers.Clear();
            this.CurrentBlocking.Model.Groups.Clear();
            this.CurrentBlocking.Model.Locations.Clear();
        }

        #endregion

        #region CopyDividerCommand

        private RelayCommand _copyDividerCommand;

        /// <summary>
        /// Copy the given divider
        /// </summary>
        public RelayCommand CopyDividerCommand
        {
            get
            {
                if (_copyDividerCommand == null)
                {
                    _copyDividerCommand = new RelayCommand(
                        CopyDivider_Executed,
                        CopyDivider_CanExecute)
                    {
                        FriendlyName = Message.BlockingPlanDocument_CopyDividerTool,
                        SmallIcon = ImageResources.Copy_16 
                    };
                    base.ViewModelCommands.Add(_copyDividerCommand);
                }
                return _copyDividerCommand;
            }
        }

        private Boolean CopyDivider_CanExecute(Object args)
        {
            //if (this.CurrentBlocking == null || args == null)
            //{
            //    return false;
            //}

            return true;
        }

        private void CopyDivider_Executed(Object args)
        {
            if (args == null) return;

            CopiedPasteDivider = (PlanogramBlockingDivider)args;
            CopiedPasteDivider_IsLimit = CopiedPasteDivider.IsLimited;
            CopiedPasteDivider_IsSnapped = CopiedPasteDivider.IsSnapped;
            CopiedPasteDivider_LimitedPercentage = CopiedPasteDivider.LimitedPercentage;

        }

        #endregion

        #region PasteDividerCommand
        
        private RelayCommand _pasteDividerCommand;

        /// <summary>
        /// Paste the given divider
        /// </summary>
        public RelayCommand PasteDividerCommand
        {
            get
            {
                if (_pasteDividerCommand == null)
                {
                    _pasteDividerCommand = new RelayCommand(
                        PasteDivider_Executed,
                        PasteDivider_CanExecute)
                    {
                        FriendlyName = Message.BlockingPlanDocument_PasteDividerTool,
                        SmallIcon = ImageResources.Paste_16
                    };
                    base.ViewModelCommands.Add(_pasteDividerCommand);
                }
                return _pasteDividerCommand;
            }
        }

        private Boolean PasteDivider_CanExecute(Object args)
        {
            if (_copiedPasteDivider == null)
            {
                return false;
            }

            return true;
        }

        private void PasteDivider_Executed(Object args)
        {
            if (args == null) return;
            ExtendedDataGrid grid = (ExtendedDataGrid)args;

            foreach (object row in grid.SelectedItems)
            {
                PlanogramBlockingDivider pasteDiv = this.CurrentBlocking.Model.Dividers.FirstOrDefault(p => p == row);
                if (pasteDiv != null)
                {
                    pasteDiv.IsLimited = CopiedPasteDivider_IsLimit;
                    pasteDiv.LimitedPercentage = CopiedPasteDivider_LimitedPercentage;
                    if (CopiedPasteDivider.Type == PlanogramBlockingDividerType.Vertical)
                    {
                        pasteDiv.IsSnapped = CopiedPasteDivider_IsSnapped;
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on the planogram
        /// </summary>
        private void Planogram_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Framework.Planograms.Model.Planogram.HeightProperty.Name
                || e.PropertyName == Framework.Planograms.Model.Planogram.WidthProperty.Name)
            {
                UpdateBlockingAreaDimensions();
            }
            else if (e.PropertyName == PlanogramView.DisplayUnitsProperty.Path)
            {
                UpdateLabelFormat();
            }
            else if (e.PropertyName == Framework.Planograms.Model.Planogram.HighlightSequenceStrategyProperty.Name)
            {
                CreateProductColorSet();
            }

        }

        /// <summary>
        /// Called whenever the current performance selection property value changes.
        /// </summary>
        private void OnCurrentBlockingChanged(PlanogramBlockingViewModel oldValue, PlanogramBlockingViewModel newValue)
        {
            if (oldValue != null)
            {
                SetBlockingEventHandlers(oldValue, false);
            }

            if (newValue != null)
            {
                this.SelectedBlockLocation = newValue.Model.Locations.FirstOrDefault();
                SetBlockingEventHandlers(newValue, true);
            }
            else
            {
                this.SelectedBlockGroup = null;
                this.SelectedBlockLocation = null;
            }
        }

        /// <summary>
        /// Called whenever the dividers collection changes.
        /// </summary>
        private void CurrentBlocking_GroupsBulkCollectionChanged(object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
            NotifyBlockingModified();
            if (this.SelectedBlockGroup == null
                && this.CurrentBlocking.Groups.Count > 0)
            {
                this.SelectedBlockGroup = this.CurrentBlocking.Groups.FirstOrDefault();
            }
        }

        /// <summary>
        /// Called whenever the selected block group changes
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void OnSelectedBlockGroupChanged(PlanogramBlockingGroupViewModel oldValue, PlanogramBlockingGroupViewModel newValue)
        {

            if (oldValue != null)
            {
                //unsubscribe events.
                SetBlockingGroupEventHandlers(oldValue, false);
            }

            if (newValue != null)
            {
                //select the first location.
                if (this.SelectedBlockLocation == null
                   || !this.CurrentBlocking.Model.Locations.Contains(this.SelectedBlockLocation)
                    || !Object.Equals(this.SelectedBlockLocation.PlanogramBlockingGroupId, newValue.Model.Id))
                {
                    this.SelectedBlockLocation =
                        this.CurrentBlocking.Model.Locations.FirstOrDefault(l => Object.Equals(l.PlanogramBlockingGroupId, newValue.Model.Id));
                }

                //subscribe to events
                SetBlockingGroupEventHandlers(newValue, true);
                UpdateCurrentBlockGroupProducts();

                Double percent;
                if (_merchandisingVolumePercentagesByBlockingGroupId.TryGetValue(newValue, out percent))
                {
                    SelectedMerchandisableBlockPercentage = percent;
                }
                
            }
            else
            {
                this.SelectedBlockLocation = null;
            }
        }

        /// <summary>
        /// Called whenever a property on the currently selected blocking group changes
        /// </summary>
        private void SelectedBlockGroup_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == PlanogramBlockingGroup.BlockPlacementPrimaryTypeProperty.Name ||
                e.PropertyName == PlanogramBlockingGroup.BlockPlacementSecondaryTypeProperty.Name ||
                e.PropertyName == PlanogramBlockingGroup.BlockPlacementTertiaryTypeProperty.Name)
            {
                // Notify blocking modified so that we update the IsSequenceUpToDate flag.
                NotifyBlockingModified();
            }
            //Mulit-Select blocking election and changed Can Merge checkBox
            if (e.PropertyName == PlanogramBlockingGroup.CanMergeProperty.Name)
            {
                PlanogramBlockingGroupViewModel blockingGroup = (PlanogramBlockingGroupViewModel)sender;
                MulitSelectedBlockingPropertyUpdate_CanMerge(blockingGroup.CanMerge);                
            }
            //Mulit-Select blocking selection and changed Is Limited checkBox
            if (e.PropertyName == PlanogramBlockingGroup.IsLimitedProperty.Name)
            {
                PlanogramBlockingGroupViewModel blockingGroup = (PlanogramBlockingGroupViewModel)sender;
                MulitSelectedBlockingPropertyUpdate_IsLimited(blockingGroup.IsLimited);
            }
            //Mulit-Select blocking selection and changed Limited Percentage TextBox
            if (e.PropertyName == PlanogramBlockingGroup.LimitedPercentageProperty.Name)
            {
                PlanogramBlockingGroupViewModel blockingGroup = (PlanogramBlockingGroupViewModel)sender;
                if (!blockingGroup.Model.BlockPercentageIsChanged)
                {
                    MultiSelectedBlockingPropertyUpdate_LimitedPercentage(blockingGroup, blockingGroup.LimitedPercentage, false);
                    //synchronise the selected items to all use the same space type
                    MultiSelectedBlockingPropertyUpdate_LimitBlockSpaceByPercentageOfType();
                }
            }
            if (e.PropertyName == PlanogramBlockingGroup.BlockSpaceLimitedPercentageProperty.Name)
            {
                PlanogramBlockingGroupViewModel blockingGroup = (PlanogramBlockingGroupViewModel)sender;
                if (blockingGroup.Model.BlockPercentageIsChanged)
                {
                    MultiSelectedBlockingPropertyUpdate_LimitedPercentage(blockingGroup, blockingGroup.BlockSpaceLimitedPercentage, true);
                    //synchronise the selected items to all use the same space type
                    MultiSelectedBlockingPropertyUpdate_LimitBlockSpaceByPercentageOfType();
                }
            }
            //check for view model property change so we can update all selected items
            if (e.PropertyName == "LimitBlockSpaceByPercentageOfType")
            {
                PlanogramBlockingGroupViewModel blockingGroup = (PlanogramBlockingGroupViewModel)sender;
                MultiSelectedBlockingPropertyUpdate_LimitBlockSpaceByPercentageOfType();
            }
        }

        /// <summary>
        /// Called upon view model changes
        /// </summary>
        /// <param name="propertyName"></param>
        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName == HighlightProperty.Path)
            {
                CreateHighlightColorSet();

            }
            
        }

        /// <summary>
        /// Called whenever planogram child collections change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Planogram_ChildChanged(object sender, ViewModelChildChangedEventArgs e)
        {
            if (e.ChildObject is PlanogramProductViewModelBase)
            {
                if (e.PropertyChangedArgs != null)
                {
                    String propertyName = e.PropertyChangedArgs.PropertyName;

                    if (propertyName == PlanogramProduct.FillColourProperty.Name && !App.MainPageViewModel.IsApplyHighlightAsProductColour)
                    {
                        CreateProductColorSet();
                    }
                }
            }
        }

        private void Controller_PropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("IsSequenceUpToDate"))
            {
                CurrentBlocking.NotifyBlockingOrSequenceGroupsChanged();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Called when the CanMerge has been modified. 
        /// </summary>
        public void MulitSelectedBlockingPropertyUpdate_CanMerge(Boolean value)
        {
            if (this.SelectedItems != null)
            {
                foreach (PlanogramBlockingLocation selectedItem in this.SelectedItems)
                {
                    PlanogramBlockingGroup group = selectedItem.Parent.Groups.FirstOrDefault(p => (int)p.Id == (int)selectedItem.PlanogramBlockingGroupId);
                    if (group != null)
                    {
                        group.CanMerge = value;
                    }
                }
            }
        }

        /// <summary>
        /// Called when the IsLimited has been modified. 
        /// </summary>
        public void MulitSelectedBlockingPropertyUpdate_IsLimited(Boolean value)
        {
            if (this.SelectedItems != null)
            {
                foreach (PlanogramBlockingLocation selectedItem in this.SelectedItems)
                {
                    PlanogramBlockingGroup group = selectedItem.Parent.Groups.FirstOrDefault(p => (int)p.Id == (int)selectedItem.PlanogramBlockingGroupId);
                    if (group != null)
                    {
                        group.IsLimited = value;
                    }
                }
            }
        }

        /// <summary>
        /// Called when the LimitedPercentage has been modified. 
        /// </summary>
        public void MultiSelectedBlockingPropertyUpdate_LimitedPercentage(PlanogramBlockingGroupViewModel sender, Single value, Boolean isValueBlockingSpacePercentConstraintValue)
        {
            if (this.SelectedItems != null)
            {
                foreach (PlanogramBlockingLocation selectedItem in this.SelectedItems)
                {
                    PlanogramBlockingGroup group = selectedItem.Parent.Groups.FirstOrDefault(p => (int)p.Id == (int)selectedItem.PlanogramBlockingGroupId);
                    //do not update the original item again
                    if (group != null && !group.Equals(sender.Model))
                    {
                        if (isValueBlockingSpacePercentConstraintValue)
                        {
                            //the blocking space percent constraint was changed so set this to the value
                            group.BlockSpaceLimitedPercentage = value;
                        }
                        else
                        {
                            group.LimitedPercentage = value;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// For all selected block groups the displayed LimitBlockSpaceByPercentageOfType value is changed to the displayed selected item
        /// (only the value for the last selected item are displayed in the ui)
        /// </summary>
        public void MultiSelectedBlockingPropertyUpdate_LimitBlockSpaceByPercentageOfType()
        {
            if (_selectedBlockingGroups != null)
            {
                foreach (PlanogramBlockingGroupViewModel group in _selectedBlockingGroups.Where(g => !g.Model.Equals(_selectedBlockGroup.Model)))
                {
                    group.LimitBlockSpaceByPercentageOfType = _selectedBlockGroup.LimitBlockSpaceByPercentageOfType;
                }
            }
        }

        /// <summary>
        /// Opens a planogram from file.
        /// </summary>
        /// <returns>The planogram that was selected, or null if no plan was opened.</returns>
        private Planogram OpenPlanogramFromFile()
        {
            String filePath;
            Boolean fileSelected = CommonHelper.GetWindowService().ShowOpenFileDialog(
                App.ViewState.GetSessionDirectory(SessionDirectory.Planogram),
                Message.Generic_PogDialogFilter, 
                out filePath);

            if (!fileSelected || String.IsNullOrEmpty(filePath)) return null;

            App.ViewState.SetSessionDirectory(SessionDirectory.Planogram, Path.GetDirectoryName(filePath));

            ShowWaitCursor(true);

            Package package = null;
            try
            {
                package = Package.FetchByFileName(filePath);
            }
            catch (Exception ex)
            {
                ShowWaitCursor(false);
                Exception rootEx = ex.GetBaseException();
                LocalHelper.RecordException(rootEx);
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(filePath, OperationType.Open);
                return null;
            }

            if (package == null)
            {
                ShowWaitCursor(false);
                return null;
            }

            ShowWaitCursor(false);
            return package.Planograms.FirstOrDefault();
        }

        /// <summary>
        /// Opens a plan from the repository.
        /// </summary>
        /// <returns>The plan that was selected, or null if no plan was loaded.</returns>
        private Planogram OpenPlanogramFromRepository()
        {
            var viewModel = new PlanogramSelectorViewModel(App.ViewState.CustomColumnLayoutFolderPath, App.ViewState.EntityId, _repositoryPlanogramGroupId);
            CommonHelper.GetWindowService().ShowDialog<PlanogramSelectorWindow>(viewModel);

            if (viewModel.DialogResult != true) return null;
            ShowWaitCursor(true);

            if (viewModel.SelectedPlanogramGroup != null)
            {
                _repositoryPlanogramGroupId = (Int32)viewModel.SelectedPlanogramGroup.Id;
            }

            PlanogramInfo newValue = viewModel.SelectedPlanogramInfo;

            //get the planogram
            Planogram plan = null;
            try
            {
                plan = Package.FetchById(newValue.Id).Planograms.FirstOrDefault();
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);
                Exception rootEx = ex.GetBaseException();
                LocalHelper.RecordException(rootEx);
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(newValue.Name, OperationType.Open);
                return null;
            }

            ShowWaitCursor(false);
            return plan;
        }

        /// <summary>
        /// Called when the blocking has been modified. Updates the IsSequenceUpToDate flag on its parent plan controller.
        /// </summary>
        public void NotifyBlockingModified()
        {
            // Mark the plan sequence as out of date due to blocking changes.
            if (this.ParentController != null) this.ParentController.IsSequenceUpToDate = false;
        }

        #region Event Handler Setters

        /// <summary>
        /// Attaches and detaches events to the source planogramview
        /// </summary>
        private void SetPlanEventHandlers(Boolean isAttach)
        {
            if (this.Planogram == null) return;

            //Unsubscribe
            this.Planogram.PropertyChanged -= Planogram_PropertyChanged;
            this.Planogram.ChildChanged -= Planogram_ChildChanged;

            //Subscribe
            if (isAttach)
            {
                this.Planogram.PropertyChanged += Planogram_PropertyChanged;
                this.Planogram.ChildChanged += new EventHandler<ViewModelChildChangedEventArgs>(Planogram_ChildChanged);
            }
        }
        
        /// <summary>
        /// Attaches and detaches events to the given blocking
        /// </summary>
        private void SetBlockingEventHandlers(PlanogramBlockingViewModel blocking, Boolean isAttach)
        {
            if (blocking == null) return;

            //unsubscribe
            blocking.Model.Groups.BulkCollectionChanged -= CurrentBlocking_GroupsBulkCollectionChanged;

            //subscribe
            if (isAttach)
            {
                blocking.Model.Groups.BulkCollectionChanged += CurrentBlocking_GroupsBulkCollectionChanged;
            }
        }

        /// <summary>
        /// Attaches and detaches events to the given blocking group.
        /// </summary>
        private void SetBlockingGroupEventHandlers(PlanogramBlockingGroupViewModel blocking, Boolean isAttach)
        {
            if (blocking == null) return;

            blocking.PropertyChanged -= SelectedBlockGroup_PropertyChanged;

            //subscribe
            if (isAttach)
            {
                blocking.PropertyChanged += SelectedBlockGroup_PropertyChanged;
            }
        }

        /// <summary>
        /// For the currently selected node, retrieve the performance data metrics and associated
        /// values to display in a list box
        /// </summary>
        //private void FetchPerformanceValues()
        //{
        //    _metaPropertyItemsValues.Clear();

        //    if (_selectedBlockGroup == null || _selectedBlockGroup.Model == null)
        //    {
        //        return;
        //    }

        //    // Find the plan performance metrics details
        //    //var cdtNodeItem = SelectedNodes.FirstOrDefault();
        //    var metaPropertyItems = new List<BlockingGroupDocumentPerformanceItem>();

        //    if (Planogram.PerformanceMetrics != null)
        //    {
        //        for (Byte metricCount = 0; metricCount < Planogram.PerformanceMetrics.Count(); metricCount++)
        //        {
        //            var newMetricItem = new BlockingGroupDocumentPerformanceItem()
        //            {
        //                MetaPropertyName = Planogram.PerformanceMetrics[metricCount].Name,

        //                MetaPropertyValue = GetPerformanceMetricValue(_selectedBlockGroup.Model, metricCount),
        //                MetaPropertyPercent = GetPerformanceMetricPercent(_selectedBlockGroup.Model, metricCount),
        //            };

        //            metaPropertyItems.Add(newMetricItem);
        //        }
        //    }

        //    _metaPropertyItemsValues.AddRange(metaPropertyItems);
        //}

        /// <summary>
        /// Return the meta performance data property given the approrpiate metric number
        /// </summary>
        /// <param name="blockingGroup">The CDT node to retrieve the data from</param>
        /// <param name="metricCount">The metric number</param>
        /// <returns>The matching performance data value</returns>
        private static Single? GetPerformanceMetricValue(PlanogramBlockingGroup blockingGroup, Byte metricCount)
        {
            if (blockingGroup == null) return null;

            switch (metricCount)
            {
                case 0: return blockingGroup.MetaP1;
                case 1: return blockingGroup.MetaP2;
                case 2: return blockingGroup.MetaP3;
                case 3: return blockingGroup.MetaP4;
                case 4: return blockingGroup.MetaP5;
                case 5: return blockingGroup.MetaP6;
                case 6: return blockingGroup.MetaP7;
                case 7: return blockingGroup.MetaP8;
                case 8: return blockingGroup.MetaP9;
                case 9: return blockingGroup.MetaP10;
                case 10: return blockingGroup.MetaP11;
                case 11: return blockingGroup.MetaP12;
                case 12: return blockingGroup.MetaP13;
                case 13: return blockingGroup.MetaP14;
                case 14: return blockingGroup.MetaP15;
                case 15: return blockingGroup.MetaP16;
                case 16: return blockingGroup.MetaP17;
                case 17: return blockingGroup.MetaP18;
                case 18: return blockingGroup.MetaP19;
                case 19: return blockingGroup.MetaP20;

                default:
                    return null;
            }
        }

        /// <summary>
        /// Return the meta percentage performance data property given the approrpiate metric number
        /// </summary>
        /// <param name="blockingGroup">The CDT node to retrieve the data from</param>
        /// <param name="metricCount">The metric number</param>
        /// <returns>The matching meta percentage performance data value</returns>
        private static Single? GetPerformanceMetricPercent(PlanogramBlockingGroup blockingGroup, Byte metricCount)
        {
            if (blockingGroup == null) return null;

            switch (metricCount)
            {
                case 0: return blockingGroup.MetaP1Percentage;
                case 1: return blockingGroup.MetaP2Percentage;
                case 2: return blockingGroup.MetaP3Percentage;
                case 3: return blockingGroup.MetaP4Percentage;
                case 4: return blockingGroup.MetaP5Percentage;
                case 5: return blockingGroup.MetaP6Percentage;
                case 6: return blockingGroup.MetaP7Percentage;
                case 7: return blockingGroup.MetaP8Percentage;
                case 8: return blockingGroup.MetaP9Percentage;
                case 9: return blockingGroup.MetaP10Percentage;
                case 10: return blockingGroup.MetaP11Percentage;
                case 11: return blockingGroup.MetaP12Percentage;
                case 12: return blockingGroup.MetaP13Percentage;
                case 13: return blockingGroup.MetaP14Percentage;
                case 14: return blockingGroup.MetaP15Percentage;
                case 15: return blockingGroup.MetaP16Percentage;
                case 16: return blockingGroup.MetaP17Percentage;
                case 17: return blockingGroup.MetaP18Percentage;
                case 18: return blockingGroup.MetaP19Percentage;
                case 19: return blockingGroup.MetaP20Percentage;

                default:
                    return null;
            }
        }

        #endregion

        /// <summary>
        ///     Updates the <see cref="_currentProducts"/> collection to show that of the currently selected <see cref="PlanogramBlockingGroup"/>.
        /// </summary>
        private void UpdateCurrentBlockGroupProducts()
        {
            _currentProducts.Clear();
            if (SelectedBlockGroup == null) return;

            PlanogramSequenceGroup sequenceGroup =
                Planogram.Model.Sequence.Groups.FirstOrDefault(o => o.Colour == SelectedBlockGroup.Colour);
            if (sequenceGroup != null) _currentProducts.AddRange(sequenceGroup.Products);
        }

        /// <summary>
        /// Called whenever this document should
        /// stop updating
        /// </summary>
        protected override void OnFreeze()
        {
            //dettach events
            SetPlanEventHandlers(false);
            SetBlockingEventHandlers(this.CurrentBlocking, false);
            SetBlockingGroupEventHandlers(this.SelectedBlockGroup, false);
        }

        /// <summary>
        /// Called whenever this document should resume
        /// updating.
        /// </summary>
        protected override void OnUnfreeze()
        {
            //reattach events
            SetPlanEventHandlers(true);
            SetBlockingEventHandlers(this.CurrentBlocking, true);
            SetBlockingGroupEventHandlers(this.SelectedBlockGroup, true);

            //update data
            UpdateBlockingAreaDimensions();
            UpdateLabelFormat();
        }

        /// <summary>
        ///     Whenever this instance's IsActiveDocument as changed.
        /// </summary>
        /// <param name="isActive">Current IsActiveDocument state for this instance.</param>
        protected override void OnActiveDocumentChanged(Boolean isActive)
        {
            if (!isActive)
            {
                //  Reset the Tool Type if needed.
                if (ToolType != BlockingToolType.Pointer) ToolType = BlockingToolType.Pointer;
            }

            //  Return control to the base.
            base.OnActiveDocumentChanged(isActive);
        }

        /// <summary>
        /// Updates the values of the BlockingHeight and BlockingWidth properties.
        /// </summary>
        private void UpdateBlockingAreaDimensions()
        {
            Planogram planModel = this.ParentController.SourcePlanogram.Model;
            
            //blocking is always done against design view so calculate the height as the max fixture height
            // and the width as all the fixtures placed next to one another.
            Single blockingHeight, blockingWidth;
            planModel.GetBlockingAreaSize(out blockingHeight, out blockingWidth, out _blockingHeightOffset);
            this.BlockingHeight = blockingHeight;
            this.BlockingWidth = blockingWidth;
        }

        /// <summary>
        /// Updates the label format 
        /// </summary>
        private void UpdateLabelFormat()
        {
            this.LabelFormat = "{0}" + this.Planogram.DisplayUnits.Length.Abbreviation;
        }

        /// <summary>
        /// Updates the merchandisable block percentage of the currently selected block and updates the underlying
        /// dictionary if <paramref name="merchandisingVolumePercentages"/> is supplied.
        /// </summary>
        /// <param name="merchandisingVolumePercentages"></param>
        public void UpdateMerchandisableBlockPercentages(Dictionary<Object, Double> merchandisingVolumePercentages = null)
        {
            if(merchandisingVolumePercentages != null) _merchandisingVolumePercentagesByBlockingGroupId = merchandisingVolumePercentages;
            if (this.SelectedBlockGroup != null)
            {
                //set our new merchandising volume for the current block
                Double percent;
                if (_merchandisingVolumePercentagesByBlockingGroupId.TryGetValue(this.SelectedBlockGroup.Model.Id, out percent))
                {
                    SelectedMerchandisableBlockPercentage = percent;
                }
            
            }
        }

        /// <summary>
        /// Creates the initial color sets for product, highlight and sequence strategy
        /// </summary>
        private void InitialiseProductColorSets()
        {
            CreateProductColorSet();
            CreateHighlightColorSet();
        }

        /// <summary>
        /// Creates the product color set
        /// </summary>
        private void CreateProductColorSet()
        {
            List<Int32> colourLookup = new List<Int32>();
            _productColors.Clear();

            foreach (PlanogramProductView planogramProductView in this.Planogram.Products)
            {
                if (!colourLookup.Contains(planogramProductView.FillColour))
                {
                    colourLookup.Add(planogramProductView.FillColour);
                    _productColors.Add(new ColorItem(CommonHelper.IntToColor(planogramProductView.FillColour), planogramProductView.ColourGroupValue));
                }
            }
        }

        /// <summary>
        /// Creates the current highlight color set
        /// </summary>
        private void CreateHighlightColorSet()
        {
            _highlightColors.Clear();

            if (Highlight != null)
            {
                PlanogramHighlightResult planogramHighlightResult = PlanogramHighlightHelper.ProcessPositionHighlight(
                                            this.Planogram.Model, Highlight);

                foreach (PlanogramHighlightResultGroup planogramHighlightResultGroup in planogramHighlightResult.Groups)
                {
                    _highlightColors.Add(new ColorItem(CommonHelper.IntToColor(planogramHighlightResultGroup.Colour), planogramHighlightResultGroup.Label));
                }
            }
        }

        #endregion

        #region IPlanDocument Members

        public override String Title
        {
            get { return DocumentTypeHelper.FriendlyNames[DocumentType.Blocking]; }
        }

        public override DocumentType DocumentType
        {
            get { return Model.DocumentType.Blocking; }
        }
        
        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!IsDisposed)
            {
                SetPlanEventHandlers(false);
                SetBlockingEventHandlers(CurrentBlocking, false);
                SetBlockingGroupEventHandlers(SelectedBlockGroup, false);
                base.ParentController.PropertyChanged -= new PropertyChangedEventHandler(Controller_PropertyChanged);
            }

            base.Dispose(disposing);
        }

        #endregion
    }

    public class BlockingGroupDocumentPerformanceItem
    {
        public String MetaPropertyName { get; set; }
        public Single? MetaPropertyValue { get; set; }
        public Single? MetaPropertyPercent { get; set; }
    }
}
