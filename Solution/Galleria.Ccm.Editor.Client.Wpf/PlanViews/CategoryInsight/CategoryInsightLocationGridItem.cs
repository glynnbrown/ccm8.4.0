﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015


#region Version History : CCM830
// V8-31834 : L.Ineson
//  Copied from SA
// Removed references to location type.
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Windows;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.CategoryInsight
{
    public sealed class CategoryInsightLocationGridItem : INotifyPropertyChanged
    {
        #region Fields
        private LocationInfo _sourceLocation;
        private String _strategyName;
        private Boolean _isIncluded;
        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath LocationProperty = WpfHelper.GetPropertyPath<CategoryInsightLocationGridItem>(p => p.Location);
        public static readonly PropertyPath StrategyNameProperty = WpfHelper.GetPropertyPath<CategoryInsightLocationGridItem>(p => p.StrategyName);
        public static readonly PropertyPath IsIncludedProperty = WpfHelper.GetPropertyPath<CategoryInsightLocationGridItem>(p => p.IsIncluded);
        #endregion

        #region Properties

        public LocationInfo Location
        {
            get { return _sourceLocation; }
        }

        public String StrategyName
        {
            get { return _strategyName; }
        }

        public Boolean IsIncluded
        {
            get { return _isIncluded; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for a Category Role grid item
        /// </summary>
        public CategoryInsightLocationGridItem(LocationInfo sourceLocation)
        {
            _sourceLocation = sourceLocation;
        }

        /// <summary>
        /// Constructor for a Category Strategy grid item
        /// </summary>
        public CategoryInsightLocationGridItem(LocationInfo sourceLocation, String strategyName)
        {
            _sourceLocation = sourceLocation;
            _strategyName = strategyName;
        }

        /// <summary>
        /// Constructor for a Category Goal or Tactic grid item
        /// </summary>
        public CategoryInsightLocationGridItem(LocationInfo sourceLocation, Boolean isIncluded)
        {
            _sourceLocation = sourceLocation;
            _isIncluded = isIncluded;
        }

        #endregion

        #region INotifyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(PropertyPath property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }
}
