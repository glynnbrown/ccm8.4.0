﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015


#region Version History : CCM830
// V8-31834 : L.Ineson
//  Copied from SA
// Removed references to location type.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.CategoryInsight
{
    public enum CategoryInsightDataType
    {
        CategoryRole,
        CategoryGoal,
        CategoryStrategy,
        CategoryTactic
    }

    public sealed class CategoryInsightLocationSpecificViewModel : ViewModelAttachedControlObject<CategoryInsightLocationSpecificOrganiser>
    {
        #region Fields
        private String _sourceName;
        private String _windowTitle;

        private readonly LocationInfoList _masterLocationList;
        private CategoryInsightDataType _dataType = CategoryInsightDataType.CategoryRole;

        private Dictionary<String, String> _locationCodes = new Dictionary<String, String>();

        private DataGridColumnCollection _columnSet = new DataGridColumnCollection();
        private ObservableCollection<CategoryInsightLocationGridItem> _availableLocations = new ObservableCollection<CategoryInsightLocationGridItem>();
        private ReadOnlyObservableCollection<CategoryInsightLocationGridItem> _availableLocationsRO;
        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath DataTypeProperty = WpfHelper.GetPropertyPath<CategoryInsightLocationSpecificViewModel>(p => p.DataType);
        public static readonly PropertyPath ColumnSetProperty = WpfHelper.GetPropertyPath<CategoryInsightLocationSpecificViewModel>(p => p.ColumnSet);
        public static readonly PropertyPath AvailableLocationsProperty = WpfHelper.GetPropertyPath<CategoryInsightLocationSpecificViewModel>(p => p.AvailableLocations);
        #endregion

        #region Properties

        /// <summary>
        /// Returns the title for the location specific window
        /// </summary>
        public String WindowTitle
        {
            get
            {
                if (String.IsNullOrEmpty(_windowTitle))
                {
                    switch (_dataType)
                    {
                        case CategoryInsightDataType.CategoryRole:
                            _windowTitle = String.Format(Message.CategoryInsightPlanDocument_LocationSpecific, Message.CategoryInsightPlanDocument_TabHeaders_Role, _sourceName);
                            break;
                        case CategoryInsightDataType.CategoryGoal:
                            _windowTitle = String.Format(Message.CategoryInsightPlanDocument_LocationSpecific, Message.CategoryInsightPlanDocument_TabHeaders_Goal, _sourceName);
                            break;
                        case CategoryInsightDataType.CategoryStrategy:
                            _windowTitle = String.Format(Message.CategoryInsightPlanDocument_LocationSpecific, Message.CategoryInsightPlanDocument_CategoryStrategy, _sourceName);
                            break;
                        case CategoryInsightDataType.CategoryTactic:
                            _windowTitle = String.Format(Message.CategoryInsightPlanDocument_LocationSpecific, Message.CategoryInsightPlanDocument_CategoryTactic, _sourceName);
                            break;
                    }
                }
                return _windowTitle;
            }
        }

        /// <summary>
        /// Returns the current data type for the screen
        /// </summary>
        public CategoryInsightDataType DataType
        {
            get { return _dataType; }
            private set
            {
                _dataType = value;
                OnPropertyChanged(DataTypeProperty);
            }
        }

        /// <summary>
        /// Return the column collection for the location grid
        /// </summary>
        public DataGridColumnCollection ColumnSet
        {
            get { return _columnSet; }
        }

        /// <summary>
        /// Returns the collection of location grid items
        /// </summary>
        public ReadOnlyObservableCollection<CategoryInsightLocationGridItem> AvailableLocations
        {
            get
            {
                if (_availableLocationsRO == null)
                {
                    _availableLocationsRO = new ReadOnlyObservableCollection<CategoryInsightLocationGridItem>(_availableLocations);
                }
                return _availableLocationsRO;
            }
        }

        #endregion

        #region Constructor

        public CategoryInsightLocationSpecificViewModel()
        {
        }

        public CategoryInsightLocationSpecificViewModel(IEnumerable<String> locationCodes, String sourceName)
        {

            _sourceName = sourceName;
            _dataType = CategoryInsightDataType.CategoryRole;
            _masterLocationList = LocationInfoList.FetchByEntityIdLocationCodes(App.ViewState.EntityId, locationCodes);

            CreateGridItems();
            ReloadColumns();
        }

        public CategoryInsightLocationSpecificViewModel(Dictionary<String, String> locationCodes, CategoryInsightDataType dataType, String sourceName)
        {

            _sourceName = sourceName;
            _dataType = dataType;
            _locationCodes = locationCodes;
            _masterLocationList = LocationInfoList.FetchByEntityIdLocationCodes(App.ViewState.EntityId, locationCodes.Keys);

            CreateGridItems();
            ReloadColumns();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates a grid item for each of the locations to be displayed
        /// </summary>
        private void CreateGridItems()
        {
            _availableLocations.Clear();

            if (_dataType == CategoryInsightDataType.CategoryRole)
            {
                foreach (LocationInfo location in _masterLocationList)
                {
                    _availableLocations.Add(new CategoryInsightLocationGridItem(location));
                }
            }
            else if (_dataType == CategoryInsightDataType.CategoryStrategy)
            {
                foreach (KeyValuePair<String, String> pair in _locationCodes)
                {
                    LocationInfo foundLocation = _masterLocationList.FirstOrDefault(p => p.Code == pair.Key);
                    if (foundLocation != null)
                    {
                        _availableLocations.Add(new CategoryInsightLocationGridItem(foundLocation, pair.Value));
                    }
                }
            }
            else
            {
                foreach (KeyValuePair<String, String> pair in _locationCodes)
                {
                    LocationInfo foundLocation = _masterLocationList.FirstOrDefault(p => p.Code == pair.Key);
                    if (foundLocation != null)
                    {
                        _availableLocations.Add(new CategoryInsightLocationGridItem(foundLocation, pair.Value.Equals("1")));
                    }
                }
            }
        }

        /// <summary>
        /// Creates the columns for the data grid
        /// </summary>
        private void ReloadColumns()
        {
            _columnSet.Clear();

            //Add the data type specific columns
            if (_dataType == CategoryInsightDataType.CategoryStrategy)
            {
                _columnSet.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(Message.CategoryInsightPlanDocument_LocationSpecific_StrategyHeader, "StrategyName", HorizontalAlignment.Left));
            }
            else if (_dataType == CategoryInsightDataType.CategoryTactic ||  _dataType == CategoryInsightDataType.CategoryGoal)
            {
                _columnSet.Add(ExtendedDataGrid.CreateCheckBoxColumn(Message.CategoryInsightPlanDocument_LocationSpecific_IncludedHeader, "IsIncluded", true));
            }

            _columnSet.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(LocationInfo.CodeProperty.FriendlyName, "Location.Code", HorizontalAlignment.Left));
            _columnSet.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(LocationInfo.NameProperty.FriendlyName, "Location.Name", HorizontalAlignment.Left));
            _columnSet.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(LocationInfo.RegionProperty.FriendlyName, "Location.Region", HorizontalAlignment.Left));
            _columnSet.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(LocationInfo.CountyProperty.FriendlyName, "Location.County", HorizontalAlignment.Left));
            _columnSet.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(LocationInfo.TVRegionProperty.FriendlyName, "Location.TVRegion", HorizontalAlignment.Left));
            _columnSet.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(LocationInfo.Address1Property.FriendlyName, "Location.Address1", HorizontalAlignment.Left));
            _columnSet.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(LocationInfo.Address2Property.FriendlyName, "Location.Address2", HorizontalAlignment.Left));
            _columnSet.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(LocationInfo.CityProperty.FriendlyName, "Location.City", HorizontalAlignment.Left));
            _columnSet.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(LocationInfo.PostalCodeProperty.FriendlyName, "Location.PostalCode", HorizontalAlignment.Left));
            _columnSet.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(LocationInfo.CountryProperty.FriendlyName, "Location.Country", HorizontalAlignment.Left));

            OnPropertyChanged(ColumnSetProperty);
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.AttachedControl = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    
}
