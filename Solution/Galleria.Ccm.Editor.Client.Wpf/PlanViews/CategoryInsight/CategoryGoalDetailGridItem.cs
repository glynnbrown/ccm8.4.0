﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015


#region Version History : CCM830
// V8-31834 : L.Ineson
//  Copied from SA
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Galleria.Ccm.GFSModel;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.CategoryInsight
{
    public sealed class CategoryGoalDetailGridItem : INotifyPropertyChanged
    {
        #region Fields
        private CategoryGoalDetail _sourceCategoryGoalDetail;
        private Boolean _areAllLocationsIncluded;
        private Boolean _areSomeLocationsIncluded;
        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath PriorityProperty = WpfHelper.GetPropertyPath<CategoryGoalDetailGridItem>(p => p.Priority);
        public static readonly PropertyPath NameProperty = WpfHelper.GetPropertyPath<CategoryGoalDetailGridItem>(p => p.Name);
        public static readonly PropertyPath DescriptionProperty = WpfHelper.GetPropertyPath<CategoryGoalDetailGridItem>(p => p.Description);
        public static readonly PropertyPath SuccessCriteriaProperty = WpfHelper.GetPropertyPath<CategoryGoalDetailGridItem>(p => p.SuccessCriteria);
        public static readonly PropertyPath ProgressProperty = WpfHelper.GetPropertyPath<CategoryGoalDetailGridItem>(p => p.Progress);
        public static readonly PropertyPath AreAllLocationsIncludedProperty = WpfHelper.GetPropertyPath<CategoryGoalDetailGridItem>(p => p.AreAllLocationsIncluded);
        public static readonly PropertyPath AreSomeLocationsIncludedProperty = WpfHelper.GetPropertyPath<CategoryGoalDetailGridItem>(p => p.AreSomeLocationsIncluded);
        #endregion

        #region Properties

        /// <summary>
        /// Returns the priority of the source category goal detail
        /// </summary>
        public CategoryGoalDetailPriorityType Priority
        {
            get { return _sourceCategoryGoalDetail.Priority; }
        }

        /// <summary>
        /// Returns the name of the source category goal detail
        /// </summary>
        public String Name
        {
            get
            {
                if (this.AreAllLocationsIncluded)
                {
                    return _sourceCategoryGoalDetail.Name;
                }
                return String.Format("{0} *", _sourceCategoryGoalDetail.Name);
            }
        }

        /// <summary>
        /// Returns the description of the source category goal detail
        /// </summary>
        public String Description
        {
            get { return _sourceCategoryGoalDetail.Description; }
        }

        /// <summary>
        /// Returns the success criteria of the source category goal detail
        /// </summary>
        public String SuccessCriteria
        {
            get { return _sourceCategoryGoalDetail.SuccessCriteria; }
        }

        /// <summary>
        /// Returns the progress of the source category goal detail
        /// </summary>
        public Int16 Progress
        {
            get { return _sourceCategoryGoalDetail.Complete; }
        }

        /// <summary>
        /// Returns the source category goal detail's location collection
        /// </summary>
        public CategoryGoalDetailLocationList Locations
        {
            get { return _sourceCategoryGoalDetail.Locations; }
        }

        /// <summary>
        /// Returns true if the source category goal detail is included for all the locations
        /// </summary>
        public Boolean AreAllLocationsIncluded
        {
            get { return _areAllLocationsIncluded; }
        }

        /// <summary>
        /// Returns true if the source category goal detail is included for at least one location
        /// </summary>
        public Boolean AreSomeLocationsIncluded
        {
            get { return _areSomeLocationsIncluded; }
        }

        #endregion

        #region Constructor

        public CategoryGoalDetailGridItem(CategoryGoalDetail sourceCategoryGoalDetail)
        {
            _sourceCategoryGoalDetail = sourceCategoryGoalDetail;

            //Check if all the locations are included for the source category goal detail
            _areAllLocationsIncluded = _sourceCategoryGoalDetail.Locations.All(p => p.IsIncluded == 1);

            //If not all the locations are included check if none of them are
            if (!_areAllLocationsIncluded)
            {
                _areSomeLocationsIncluded = _sourceCategoryGoalDetail.Locations.Any(p => p.IsIncluded == 1); ;
            }

        }

        #endregion

        #region INotifyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(PropertyPath property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }
}
