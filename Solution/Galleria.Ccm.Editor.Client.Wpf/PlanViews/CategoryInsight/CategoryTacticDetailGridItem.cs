﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015


#region Version History : CCM830
// V8-31834 : L.Ineson
//  Copied from SA
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Galleria.Ccm.GFSModel;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.CategoryInsight
{
    public sealed class CategoryTacticDetailGridItem : INotifyPropertyChanged
    {
        #region Fields
        private CategoryTacticDetail _sourceCategoryTacticDetail;
        private Boolean _areAllLocationsIncluded;
        private Boolean _isGroupHeader;
        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath TacticTypeNameProperty = WpfHelper.GetPropertyPath<CategoryTacticDetailGridItem>(p => p.TacticTypeName);
        public static readonly PropertyPath TacticNameProperty = WpfHelper.GetPropertyPath<CategoryTacticDetailGridItem>(p => p.TacticName);
        public static readonly PropertyPath AreAllLocationsIncudedProperty = WpfHelper.GetPropertyPath<CategoryTacticDetailGridItem>(p => p.AreAllLocationsIncuded);
        public static readonly PropertyPath IsGroupHeaderProperty = WpfHelper.GetPropertyPath<CategoryTacticDetailGridItem>(p => p.IsGroupHeader);
        #endregion

        #region Properties

        public String TacticTypeName
        {
            get
            {
                if (_isGroupHeader)
                {
                    return _sourceCategoryTacticDetail.TacticTypeName;
                }
                return String.Empty;
            }
        }

        /// <summary>
        /// Returns the name of the source category tactic detail
        /// </summary>
        public String TacticName
        {
            get
            {
                if (_areAllLocationsIncluded)
                {
                    return _sourceCategoryTacticDetail.Name;
                }
                return String.Format("{0} *", _sourceCategoryTacticDetail.Name);
            }
        }

        /// <summary>
        /// Returns the source category tactic detail's location collection
        /// </summary>
        public CategoryTacticDetailLocationList Locations
        {
            get { return _sourceCategoryTacticDetail.Locations; }
        }

        /// <summary>
        /// Returns true if all the locations are included on the source category tactic detail
        /// </summary>
        public Boolean AreAllLocationsIncuded
        {
            get { return _areAllLocationsIncluded; }
        }

        public Boolean IsGroupHeader
        {
            get { return _isGroupHeader; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a CategoryTacticDetailGridItem for display on the Category Insights panel
        /// </summary>
        /// <param name="sourceCategoryTacticDetail">The source Category Tactic Detail</param>
        /// <param name="isGroupHeader">If this is the group header for the linked tactic type</param>
        public CategoryTacticDetailGridItem(CategoryTacticDetail sourceCategoryTacticDetail, Boolean isGroupHeader)
        {
            _sourceCategoryTacticDetail = sourceCategoryTacticDetail;
            _areAllLocationsIncluded = _sourceCategoryTacticDetail.Locations.FirstOrDefault(p => p.IsIncluded == 0) == null;

            _isGroupHeader = isGroupHeader;
        }

        #endregion

        #region INotifyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(PropertyPath property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }
}
