﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015


#region Version History : CCM830
// V8-31834 : L.Ineson
//  Copied from SA
#endregion

#endregion

using Csla;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.GFSModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.CategoryInsight
{

    /// <summary>
    ///  Plan document controller for providing a view of Category Insight information for a planogram.
    /// </summary>
    public sealed class CategoryInsightPlanDocument : PlanDocument<CategoryInsightPlanDocumentView>
    {
        #region Constants

        private const String _innerExceptionContents = "DataPortal_Fetch method call failed";
        private const String _noResultsException = "combination returned no results.";

        #endregion

        #region Fields

        private String _gfsEntityName;
        private String _endpointRoot;

        private Boolean _isContentVisible = false;
        private Csla.Threading.BackgroundWorker _categoryInsightLoader;
        private CategoryInsightDisplay _currentDisplay = CategoryInsightDisplay.Loading;

        //Category Role
        private Boolean _noLinkedRoleFound;
        private CategoryRole _linkedCategoryRole;
        private ObservableCollection<IGrouping<String, CategoryRoleLocation>> _locationSpecificRoles = new ObservableCollection<IGrouping<String, CategoryRoleLocation>>();
        private ReadOnlyObservableCollection<IGrouping<String, CategoryRoleLocation>> _locationSpecificRolesRO;

        private GFSModel.Image _image;
        private Int32 _selectedDocumentId;
        private ImageSource _selectedDocumentPreviewImage;
        private Int32 _documentCount = 0;
        private Int32 _selectedDocumentIndex = 0;
        private String _documentPosition;

        private Dictionary<Tuple<String, Int32>, String> _fileIdentifierToPathLookup = new Dictionary<Tuple<String, Int32>, String>(); // CategoryCode, FileId to FileTempPath
        private Dictionary<Tuple<String, Int32>, ImageSource> _fileIdentifierToDocumentPreviewLookup = new Dictionary<Tuple<String, Int32>, ImageSource>(); // CategoryCode, FileId to FileTempPath
        private String _noCategoryRoleMessage = Message.CategoryInsightPlanDocument_RolesTab_NoLink;

        //Category Goal
        private Boolean _noLinkedGoalFound;
        private CategoryGoal _linkedCategoryGoal;
        private ObservableCollection<CategoryGoalDetailGridItem> _availableCategoryGoalDetails = new ObservableCollection<CategoryGoalDetailGridItem>();
        private ReadOnlyObservableCollection<CategoryGoalDetailGridItem> _availableCategoryGoalDetailsRO;
        private String _noCategoryGoalMessage = Message.CategoryInsightPlanDocument_GoalsTab_NoLinkedGoal;

        //Category Tactic
        private Boolean _noLinkedTacticFound;
        private CategoryTactic _linkedCategoryTactic;
        private ObservableCollection<CategoryTacticDetailGridItem> _availableCategoryTacticDetails = new ObservableCollection<CategoryTacticDetailGridItem>();
        private ReadOnlyObservableCollection<CategoryTacticDetailGridItem> _availableCategoryTacticDetailsRO;
        private String _noCategoryTacticMessage = Message.CategoryInsightPlanDocument_TacticTab_NoLinkedTactic;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath HasRequiredInfoProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.HasRequiredInfo);
        public static readonly PropertyPath CategoryProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.Category);

        public static readonly PropertyPath IsContentVisibleProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.IsContentVisible);
        public static readonly PropertyPath CurrentDisplayProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.CurrentDisplay);

        //Category Role
        public static readonly PropertyPath NoLinkedRoleFoundProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.NoLinkedRoleFound);
        public static readonly PropertyPath LinkedCategoryRoleProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.LinkedCategoryRole);
        public static readonly PropertyPath LinkedCategoryRoleNameProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.LinkedCategoryRoleName);
        public static readonly PropertyPath LocationSpecificRolesProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.LocationSpecificRoles);
        public static readonly PropertyPath HasLocationSpecificRoleProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.HasLocationSpecificRole);
        public static readonly PropertyPath ShowNoCategoryRoleMessageProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.ShowNoCategoryRoleMessage);

        public static readonly PropertyPath SelectedDocumentIdProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.SelectedDocumentId);
        public static readonly PropertyPath SelectedDocumentPreviewImageProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.SelectedDocumentPreviewImage);
        public static readonly PropertyPath DocumentCountProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.DocumentCount);
        public static readonly PropertyPath DocumentPositionProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.DocumentPosition);
        public static readonly PropertyPath SelectedDocumentIndexProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.SelectedDocumentIndex);
        public static readonly PropertyPath NoCategoryRoleMessageProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.NoCategoryRoleMessage);

        //Category Goal
        public static readonly PropertyPath NoLinkedGoalFoundProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.NoLinkedGoalFound);
        public static readonly PropertyPath LinkedCategoryGoalProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.LinkedCategoryGoal);
        public static readonly PropertyPath CategoryGoalLastModifiedProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.CategoryGoalLastModified);
        public static readonly PropertyPath AvailableCategoryGoalDetailsProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.AvailableCategoryGoalDetails);
        public static readonly PropertyPath NoCategoryGoalMessageProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.NoCategoryGoalMessage);

        //Category Tactic
        public static readonly PropertyPath NoLinkedTacticFoundProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.NoLinkedTacticFound);
        public static readonly PropertyPath LinkedCategoryTacticProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.LinkedCategoryTactic);
        public static readonly PropertyPath LinkedCategoryTacticNameProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.LinkedCategoryTacticName);
        public static readonly PropertyPath CategoryTacticLastModifiedProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.CategoryTacticLastModified);
        public static readonly PropertyPath CategoryTacticStrategyAllLocationsProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.CategoryTacticStrategyAllLocations);
        public static readonly PropertyPath AvailableCategoryTacticDetailsProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.AvailableCategoryTacticDetails);
        public static readonly PropertyPath NoCategoryTacticMessageProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(p => p.NoCategoryTacticMessage);

        //Commands
        public static readonly PropertyPath NextDocumentCommandProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(c => c.NextDocumentCommand);
        public static readonly PropertyPath PreviousDocumentCommandProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(c => c.PreviousDocumentCommand);
        public static readonly PropertyPath ShowDocumentCommandProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(c => c.ShowDocumentCommand);
        public static readonly PropertyPath OpenLocationSpecificCommandProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(c => c.OpenLocationSpecificCommand);
        public static readonly PropertyPath OpenGoalLinkedLocationsCommandProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(c => c.OpenGoalLinkedLocationsCommand);
        public static readonly PropertyPath OpenStrategyLinkedLocationsCommandProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(c => c.OpenStrategyLinkedLocationsCommand);
        public static readonly PropertyPath OpenTacticLinkedLocationsCommandProperty = WpfHelper.GetPropertyPath<CategoryInsightPlanDocument>(c => c.OpenTacticLinkedLocationsCommand);


        #endregion

        #region Properties

        /// <summary>
        /// Returns true if the planogram and app has all the info it needs to
        /// try fetching category insights
        /// </summary>
        public Boolean HasRequiredInfo
        {
            get
            {
                return
                    !String.IsNullOrWhiteSpace(this.Planogram.CategoryCode)
                    && App.ViewState.IsConnectedToRepository;
            }
        }

        /// <summary>
        /// Returns the display name of the category for which
        /// information is being shown.
        /// </summary>
        public String Category
        {
            get
            {
                if (String.IsNullOrWhiteSpace(this.Planogram.CategoryCode))
                {
                    return null;
                }

                return this.Planogram.CategoryCode + " " + this.Planogram.CategoryName;
            }
        }

        /// <summary>
        /// Return the current display to be shown on the category insights panel
        /// </summary>
        public CategoryInsightDisplay CurrentDisplay
        {
            get { return _currentDisplay; }
            private set
            {
                _currentDisplay = value;
                //OnPropertyChanged(ReviewProductGroupNameProperty);
                OnPropertyChanged(CurrentDisplayProperty);
            }
        }

        /// <summary>
        /// Get/Sets if the panel content is visible
        /// </summary>
        public Boolean IsContentVisible
        {
            get { return _isContentVisible; }
            set
            {
                _isContentVisible = value;
                OnPropertyChanged(IsContentVisibleProperty);
            }
        }

        #region Category Role Related

        /// <summary>
        /// Returns true if there are no links to show on the category role tab
        /// </summary>
        public Boolean NoLinkedRoleFound
        {
            get { return _noLinkedRoleFound; }
            set
            {
                _noLinkedRoleFound = value;
                OnPropertyChanged(NoLinkedRoleFoundProperty);
            }
        }

        /// <summary>
        /// Returns the predominant role linked to this product group in GFS
        /// </summary>
        public CategoryRole LinkedCategoryRole
        {
            get { return _linkedCategoryRole; }
            private set
            {
                _linkedCategoryRole = value;
                OnLinkedCategoryRoleChanged();
                OnPropertyChanged(LinkedCategoryRoleProperty);
                OnPropertyChanged(LinkedCategoryRoleNameProperty);
                OnPropertyChanged(ShowNoCategoryRoleMessageProperty);
            }
        }

        /// <summary>
        /// Returns the name of the linked category role showing if the role is for all locations
        /// </summary>
        public String LinkedCategoryRoleName
        {
            get
            {
                if (_linkedCategoryRole != null &&
                    _linkedCategoryRole.Locations != null)
                {
                    if (_linkedCategoryRole.Locations.Count > 1)
                    {
                        return String.Format("{0} *", _linkedCategoryRole.Name);
                    }
                    return _linkedCategoryRole.Name;
                }
                return Message.CategoryInsightPlanDocument_NoAssignmentFound;
            }
        }

        /// <summary>
        /// Returns the collection of CategoryRoleLocations that are linked to different roles than the predominant
        /// </summary>
        public ReadOnlyObservableCollection<IGrouping<String, CategoryRoleLocation>> LocationSpecificRoles
        {
            get
            {
                if (_locationSpecificRolesRO == null)
                {
                    _locationSpecificRolesRO = new ReadOnlyObservableCollection<IGrouping<String, CategoryRoleLocation>>(_locationSpecificRoles);
                }
                return _locationSpecificRolesRO;
            }
        }

        /// <summary>
        /// Returns true if the CategoryRole contains location specific 
        /// </summary>
        public Boolean HasLocationSpecificRole
        {
            get
            {
                if (_linkedCategoryRole != null)
                {
                    return _linkedCategoryRole.Locations != null
                        && _linkedCategoryRole.Locations.Any();
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Returns true if the no category role message should be shown
        /// </summary>
        public Boolean ShowNoCategoryRoleMessage
        {
            get { return !this.HasLocationSpecificRole && this.NoLinkedRoleFound; }
        }

        /// <summary>
        /// Returns the base error message associated with the fetch of category role
        /// </summary>
        public String NoCategoryRoleMessage
        {
            get { return _noCategoryRoleMessage; }
            set
            {
                _noCategoryRoleMessage = value;
                OnPropertyChanged(NoCategoryRoleMessageProperty);
            }
        }

        /// <summary>
        /// The id of the currently selected document associated with the main CategoryRole
        /// </summary>
        public Int32 SelectedDocumentId
        {
            get { return _selectedDocumentId; }
            set
            {
                _selectedDocumentId = value;
                OnPropertyChanged(SelectedDocumentIdProperty);
                OnSelectedDocumentIdChanged();
            }
        }

        /// <summary>
        /// The document preview for the currently selected document 
        /// from LinkedCategoryRole.Files
        /// </summary>
        public ImageSource SelectedDocumentPreviewImage
        {
            get
            {
                return _selectedDocumentPreviewImage;
            }
            set
            {
                _selectedDocumentPreviewImage = value;
                OnPropertyChanged(SelectedDocumentPreviewImageProperty);
            }
        }

        /// <summary>
        /// The number of documents saved against LinkedCategoryRole.Files
        /// </summary>
        public Int32 DocumentCount
        {
            get { return _documentCount; }
            private set
            {
                _documentCount = value;
                OnPropertyChanged(DocumentCountProperty);
            }
        }

        /// <summary>
        /// The index of the document within LinkedCategoryRole.Files collection 
        /// </summary>
        public Int32 SelectedDocumentIndex
        {
            get { return _selectedDocumentIndex; }
            private set
            {
                _selectedDocumentIndex = value;
                OnPropertyChanged(SelectedDocumentIndexProperty);
            }
        }

        /// <summary>
        /// Document position within the documents collection 
        /// At the moment defined as SelectedDocumentIndex+1 \ DocumentCount value. 
        /// Set within LoadImagePreview to only refresh once the new document preview has been fetched
        /// </summary>
        public String DocumentPosition
        {
            get
            {
                return _documentPosition;
            }
            private set
            {
                _documentPosition = value;
                OnPropertyChanged(DocumentPositionProperty);
            }
        }


        #endregion

        #region Category Goal Related

        /// <summary>
        /// Returns true if there is no link to show on the category goal tab
        /// </summary>
        public Boolean NoLinkedGoalFound
        {
            get { return _noLinkedGoalFound; }
            set
            {
                _noLinkedGoalFound = value;
                OnPropertyChanged(NoLinkedGoalFoundProperty);
            }
        }

        /// <summary>
        /// Returns the base error message associated with the fetch of category goal
        /// </summary>
        public String NoCategoryGoalMessage
        {
            get { return _noCategoryGoalMessage; }
            set
            {
                _noCategoryGoalMessage = value;
                OnPropertyChanged(NoCategoryGoalMessageProperty);
            }
        }

        /// <summary>
        /// Returns the linked category goal
        /// </summary>
        public CategoryGoal LinkedCategoryGoal
        {
            get { return _linkedCategoryGoal; }
            private set
            {
                _linkedCategoryGoal = value;
                OnLinkedCategoryGoalChanged();
                OnPropertyChanged(LinkedCategoryGoalProperty);
                OnPropertyChanged(CategoryGoalLastModifiedProperty);
            }
        }

        /// <summary>
        /// Returns the date the linked goal was last updated, and the user's name
        /// </summary>
        public String CategoryGoalLastModified
        {
            get
            {
                if (_linkedCategoryGoal != null)
                {
                    return String.Format(Message.CategoryInsightPlanDocument_LastUpdated, _linkedCategoryGoal.UserFullName,
                        _linkedCategoryGoal.DateLastModified.ToShortDateString());
                }
                return String.Empty;
            }
        }

        /// <summary>
        /// Returns the collection of category goal details for the linked category goal
        /// </summary>
        public ReadOnlyObservableCollection<CategoryGoalDetailGridItem> AvailableCategoryGoalDetails
        {
            get
            {
                if (_availableCategoryGoalDetailsRO == null)
                {
                    _availableCategoryGoalDetailsRO = new ReadOnlyObservableCollection<CategoryGoalDetailGridItem>(_availableCategoryGoalDetails);
                }
                return _availableCategoryGoalDetailsRO;
            }
        }

        #endregion

        #region Category Tactic Related

        /// <summary>
        /// Returns true if there is no link to show on the category tactic tab
        /// </summary>
        public Boolean NoLinkedTacticFound
        {
            get { return _noLinkedTacticFound; }
            set
            {
                _noLinkedTacticFound = value;
                OnPropertyChanged(NoLinkedTacticFoundProperty);
            }
        }

        /// <summary>
        /// Returns the base error message associated with the fetch of category tactic
        /// </summary>
        public String NoCategoryTacticMessage
        {
            get { return _noCategoryTacticMessage; }
            set
            {
                _noCategoryTacticMessage = value;
                OnPropertyChanged(NoCategoryTacticMessageProperty);
            }
        }

        /// <summary>
        /// Returns the linked category tactic
        /// </summary>
        public CategoryTactic LinkedCategoryTactic
        {
            get { return _linkedCategoryTactic; }
            private set
            {
                _linkedCategoryTactic = value;
                OnLinkedCategoryTacticChanged();
                OnPropertyChanged(LinkedCategoryTacticProperty);
                OnPropertyChanged(LinkedCategoryTacticNameProperty);
                OnPropertyChanged(CategoryTacticLastModifiedProperty);
                OnPropertyChanged(CategoryTacticStrategyAllLocationsProperty);
            }
        }

        /// <summary>
        /// Returns the name of the linked category strategy showing if the strategy is for all locations
        /// </summary>
        public String LinkedCategoryTacticName
        {
            get
            {
                if (this.LinkedCategoryTactic != null)
                {
                    if (!this.CategoryTacticStrategyAllLocations)
                    {
                        return String.Format("{0} *", this.LinkedCategoryTactic.StrategyName);
                    }
                    return this.LinkedCategoryTactic.StrategyName;
                }
                return Message.CategoryInsightPlanDocument_NoAssignmentFound;
            }
        }

        /// <summary>
        /// Returns the name of the user who last updated the tactic, and the date
        /// </summary>
        public String CategoryTacticLastModified
        {
            get
            {
                if (_linkedCategoryTactic != null)
                {
                    return String.Format(Message.CategoryInsightPlanDocument_LastUpdated, _linkedCategoryTactic.UserFullName,
                        _linkedCategoryTactic.DateLastModified.ToShortDateString());
                }
                return String.Empty;
            }
        }

        /// <summary>
        /// Returns true if all the strategy locations for the linked category tactic are included
        /// </summary>
        public Boolean CategoryTacticStrategyAllLocations
        {
            get
            {
                if (_linkedCategoryTactic != null)
                {
                    return _linkedCategoryTactic.StrategyLocations.FirstOrDefault(p => !p.StrategyName.Equals(_linkedCategoryTactic.StrategyName)) == null;
                }
                return true;
            }
        }

        /// <summary>
        /// Returns the collection of available category tactic details
        /// </summary>
        public ReadOnlyObservableCollection<CategoryTacticDetailGridItem> AvailableCategoryTacticDetails
        {
            get
            {
                if (_availableCategoryTacticDetailsRO == null)
                {
                    _availableCategoryTacticDetailsRO = new ReadOnlyObservableCollection<CategoryTacticDetailGridItem>(_availableCategoryTacticDetails);
                }
                return _availableCategoryTacticDetailsRO;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="parentController">the parent plan controller.</param>
        public CategoryInsightPlanDocument(PlanControllerViewModel parentController)
            : base(parentController)
        {
            SetPlanEventHandlers(true);
            RefreshData();

            App.ViewState.EntityIdChanged += ViewState_EntityIdChanged;
        }

        #endregion

        #region Commands

        #region NextDocumentCommand

        private RelayCommand _nextDocumentCommand;

        public RelayCommand NextDocumentCommand
        {
            get
            {
                if (_nextDocumentCommand == null)
                {
                    _nextDocumentCommand = new RelayCommand(
                        p => NextDocumentCommand_Executed(),
                        p => NextDocumentCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Next,
                        Icon = ImageResources.CategoryInsightPlanDocument_NextDocument
                    };
                    this.ViewModelCommands.Add(_nextDocumentCommand);
                }
                return _nextDocumentCommand;
            }
        }

        private Boolean NextDocumentCommand_CanExecute()
        {
            //TODO: Check if there is a next document
            if (this.SelectedDocumentIndex >= this.DocumentCount - 1)
            {
                return false;
            }

            return true;
        }

        private void NextDocumentCommand_Executed()
        {
            base.ShowWaitCursor(true);

            this.SelectedDocumentIndex += 1;
            this.SelectedDocumentId = this.LinkedCategoryRole.Files[_selectedDocumentIndex].FileId;

            base.ShowWaitCursor(false);
        }

        #endregion

        #region PreviousDocumentCommand

        private RelayCommand _previousDocumentCommand;

        public RelayCommand PreviousDocumentCommand
        {
            get
            {
                if (_previousDocumentCommand == null)
                {
                    _previousDocumentCommand = new RelayCommand(
                        p => PreviousDocumentCommand_Executed(),
                        p => PreviousDocumentCommand_CanExecute(),
                        attachToCommandManager: true)
                    {
                        FriendlyName = Message.Generic_Previous,
                        Icon = ImageResources.CategoryInsightPlanDocument_PreviousDocument
                    };
                    this.ViewModelCommands.Add(_previousDocumentCommand);
                }
                return _previousDocumentCommand;
            }
        }

        private Boolean PreviousDocumentCommand_CanExecute()
        {
            //TODO: Check if there is a previous document
            if (this.SelectedDocumentIndex == 0)
            {
                return false;
            }

            return true;
        }

        private void PreviousDocumentCommand_Executed()
        {
            base.ShowWaitCursor(true);

            this.SelectedDocumentIndex -= 1;
            this.SelectedDocumentId = this.LinkedCategoryRole.Files[_selectedDocumentIndex].FileId;

            base.ShowWaitCursor(false);
        }

        #endregion

        #region ShowDocumentCommand

        private RelayCommand _showDocumentCommand;

        /// <summary>
        /// Opens the linked pdf document.
        /// </summary>
        public RelayCommand ShowDocumentCommand
        {
            get
            {
                if (_showDocumentCommand == null)
                {
                    _showDocumentCommand = new RelayCommand(
                        p => ShowDocumentCommand_Executed(),
                        p => ShowDocumentCommand_CanExecute(),
                        attachToCommandManager: true)
                    {
                        FriendlyName = Message.CategoryInsightPlanDocument_ShowDocument_FriendlyName
                    };
                    this.ViewModelCommands.Add(_showDocumentCommand);
                }
                return _showDocumentCommand;
            }
        }

        private Boolean ShowDocumentCommand_CanExecute()
        {
            if (this.LinkedCategoryRole == null)
            {
                return false;
            }
            if (this.DocumentCount < 1)
            {
                return false;
            }
            return true;
        }

        private void ShowDocumentCommand_Executed()
        {
            ShowWaitCursor(true);
            String pdfPath = String.Empty;

            //check if file has been generated in this life of the panel
            Tuple<String, Int32> fileIdentifier = new Tuple<String, int>(this.Planogram.CategoryCode, this.SelectedDocumentId);
            if (_fileIdentifierToPathLookup.ContainsKey(fileIdentifier))
            {
                pdfPath = _fileIdentifierToPathLookup[fileIdentifier];
            }
            else
            {
                Galleria.Ccm.GFSModel.File pdfFile
                    = Galleria.Ccm.GFSModel.File.FetchByFileId(_endpointRoot, this.SelectedDocumentId);

                #region Ensure File Uniqueness
                pdfPath = Path.Combine(Path.GetTempPath(), pdfFile.Name);
                Boolean exists = System.IO.File.Exists(pdfPath);
                Int64 counter = 1;
                while (exists)
                {
                    pdfPath = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(pdfFile.Name) + "(" + counter + ")" + Path.GetExtension(pdfFile.Name));
                    exists = System.IO.File.Exists(pdfPath);
                    counter += 1;
                }
                #endregion

                if (pdfFile != null)
                {
                    Byte[] pdfData = pdfFile.Data;
                    System.IO.File.WriteAllBytes(pdfPath, pdfData);
                }

                //only add after ensuring path uniqness and when the path is the actual file name related
                _fileIdentifierToPathLookup.Add(fileIdentifier, pdfPath);
            }
            if (!String.IsNullOrWhiteSpace(pdfPath))
            {
                System.Diagnostics.Process.Start(pdfPath);
            }

            ShowWaitCursor(false);
        }

        #endregion

        #region OpenLocationSpecificCommand

        private RelayCommand _openLocationSpecificCommand;

        public RelayCommand OpenLocationSpecificCommand
        {
            get
            {
                if (_openLocationSpecificCommand == null)
                {
                    _openLocationSpecificCommand = new RelayCommand(
                        p => OpenLocationSpecific_Executed(p));
                    base.ViewModelCommands.Add(_openLocationSpecificCommand);
                }
                return _openLocationSpecificCommand;
            }
        }

        private void OpenLocationSpecific_Executed(Object sender)
        {
            IGrouping<String, CategoryRoleLocation> senderGrouping = sender as IGrouping<String, CategoryRoleLocation>;
            if (senderGrouping == null) return;

            ShowWaitCursor(true);

            //Create the view model
            var vm =
                new CategoryInsightLocationSpecificViewModel(senderGrouping.Select(p => p.Code), senderGrouping.Key.TrimEnd(" *".ToCharArray()));

            ShowWaitCursor(false);

            //show the window.
            GetWindowService().ShowDialog<CategoryInsightLocationSpecificOrganiser>(vm);
        }

        #endregion

        #region OpenGoalLinkedLocationsCommand

        private RelayCommand _openGoalLinkedLocationsCommand;

        public RelayCommand OpenGoalLinkedLocationsCommand
        {
            get
            {
                if (_openGoalLinkedLocationsCommand == null)
                {
                    _openGoalLinkedLocationsCommand = new RelayCommand(
                        p => OpenGoalLinkedLocations_Executed(p));
                    base.ViewModelCommands.Add(_openGoalLinkedLocationsCommand);
                }
                return _openGoalLinkedLocationsCommand;
            }
        }

        private void OpenGoalLinkedLocations_Executed(Object sender)
        {
            CategoryGoalDetailGridItem senderGridItem = sender as CategoryGoalDetailGridItem;
            if (senderGridItem == null) return;

            ShowWaitCursor(true);

            //Create the view model
            Dictionary<String, String> locationCodes = new Dictionary<String, String>();
            foreach (CategoryGoalDetailLocation location in senderGridItem.Locations)
            {
                locationCodes.Add(location.Code, location.IsIncluded.ToString());
            }

           var vm =
                new CategoryInsightLocationSpecificViewModel(locationCodes,
                    CategoryInsightDataType.CategoryGoal, senderGridItem.Name.TrimEnd(" *".ToCharArray()));


            ShowWaitCursor(false);

            //show the window.
            GetWindowService().ShowDialog<CategoryInsightLocationSpecificOrganiser>(vm);
        }

        #endregion

        #region OpenTacticLinkedLocationsCommand

        private RelayCommand _openTacticLinkedLocationsCommand;

        public RelayCommand OpenTacticLinkedLocationsCommand
        {
            get
            {
                if (_openTacticLinkedLocationsCommand == null)
                {
                    _openTacticLinkedLocationsCommand = new RelayCommand(
                        p => OpenTacticLinkedLocations_Executed(p));
                    base.ViewModelCommands.Add(_openTacticLinkedLocationsCommand);
                }
                return _openTacticLinkedLocationsCommand;
            }
        }

        private void OpenTacticLinkedLocations_Executed(Object sender)
        {
            CategoryTacticDetailGridItem senderGridItem = sender as CategoryTacticDetailGridItem;
            if (senderGridItem == null) return;

            ShowWaitCursor(true);

            //Create the view model
            Dictionary<String, String> locationCodes = new Dictionary<String, String>();
            foreach (CategoryTacticDetailLocation location in senderGridItem.Locations)
            {
                locationCodes.Add(location.Code, location.IsIncluded.ToString());
            }
            var vm = new CategoryInsightLocationSpecificViewModel(locationCodes, 
                CategoryInsightDataType.CategoryTactic,
                senderGridItem.TacticName.TrimEnd(" *".ToCharArray()));

            ShowWaitCursor(false);

            //show the window.
            GetWindowService().ShowDialog<CategoryInsightLocationSpecificOrganiser>(vm);
        }

        #endregion

        #region OpenStrategyLinkedLocationsCommand

        private RelayCommand _openStrategyLinkedLocationsCommand;

        public RelayCommand OpenStrategyLinkedLocationsCommand
        {
            get
            {
                if (_openStrategyLinkedLocationsCommand == null)
                {
                    _openStrategyLinkedLocationsCommand = new RelayCommand(
                        p => OpenStrategyLinkedLocations_Executed());
                    base.ViewModelCommands.Add(_openStrategyLinkedLocationsCommand);
                }
                return _openStrategyLinkedLocationsCommand;
            }
        }

        private void OpenStrategyLinkedLocations_Executed()
        {
            if (this.LinkedCategoryTactic == null) return;

            ShowWaitCursor(true);

            //Create the view model
            Dictionary<String, String> locationCodes = new Dictionary<String, String>();
            foreach (CategoryTacticStrategyLocation location in this.LinkedCategoryTactic.StrategyLocations)
            {
                locationCodes.Add(location.Code, location.StrategyName);
            }
            var vm = new CategoryInsightLocationSpecificViewModel(locationCodes, 
                CategoryInsightDataType.CategoryStrategy, 
                this.LinkedCategoryTactic.StrategyName);

            ShowWaitCursor(false);

            //show the window.
            GetWindowService().ShowDialog<CategoryInsightLocationSpecificOrganiser>(vm);
        }

        #endregion

        #endregion

        #region Event Handlers

        private void ViewState_EntityIdChanged(object sender, EventArgs e)
        {
            OnPlanogramCategoryCodeChanged();
        }

        /// <summary>
        /// Called whenever a property changes on the planogram
        /// </summary>
        private void Planogram_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Framework.Planograms.Model.Planogram.CategoryCodeProperty.Name
                || e.PropertyName == Framework.Planograms.Model.Planogram.CategoryNameProperty.Name)
            {
                OnPlanogramCategoryCodeChanged();
            }
        }

        /// <summary>
        /// Checks if a CategoryInsight reload is required
        /// </summary>
        private void OnPlanogramCategoryCodeChanged()
        {
            OnPropertyChanged(HasRequiredInfoProperty);
            OnPropertyChanged(CategoryProperty);

            if (String.IsNullOrWhiteSpace(this.Planogram.CategoryCode))
            {
                //Set the CurrentDisplay to show the no product group message
                this.CurrentDisplay = CategoryInsightDisplay.NoProductGroup;
                OnPropertyChanged(CategoryProperty);
                return;
            }


            //Reload the category insights for the product group
            _selectedDocumentIndex = 0;
            RefreshData();

        }

        /// <summary>
        /// Sets the NoLinkedRoleFound property base on if there is anything to show
        /// </summary>
        private void OnLinkedCategoryRoleChanged()
        {
            if (this.LinkedCategoryRole == null)
            {
                this.NoLinkedRoleFound = true;
                return;
            }


            if (this.LinkedCategoryRole.ContentVersion == 0
                && this.LinkedCategoryRole.Locations != null
                && this.LinkedCategoryRole.Locations.Count == 0)
            {
                this.NoLinkedRoleFound = true;
            }
            else if (this.LinkedCategoryRole.Files == null
                && this.LinkedCategoryRole.Locations == null
                && String.IsNullOrEmpty(this.LinkedCategoryRole.Name))
            {
                _linkedCategoryRole = null;
                this.NoLinkedRoleFound = true;
            }
            else
            {
                this.NoLinkedRoleFound = false;
            }

        }

        /// <summary>
        /// Sets the NoLinkedGoalFound property based on if there is anything to show
        /// </summary>
        private void OnLinkedCategoryGoalChanged()
        {
            this.NoLinkedGoalFound = (this.LinkedCategoryGoal == null);
        }

        /// <summary>
        /// Sets the NoLinkedTacticFound property based on if there is anything to show
        /// </summary>
        private void OnLinkedCategoryTacticChanged()
        {
            this.NoLinkedTacticFound = (this.LinkedCategoryTactic == null);
        }

        /// <summary>
        /// Requests document preview as per new document ID.
        /// </summary>
        private void OnSelectedDocumentIdChanged()
        {
            if (this.LinkedCategoryRole != null
                && this.LinkedCategoryRole.Files.Any()
                && this.LinkedCategoryRole.Files.Select(f => f.FileId).Contains(this.SelectedDocumentId))
            {
                LoadImagePreview(this.SelectedDocumentId);
            }
        }

        #endregion

        #region Methods

        #region Plan Document related

        /// <summary>
        /// Attaches and detaches events to the source planogramview
        /// </summary>
        private void SetPlanEventHandlers(Boolean isAttach)
        {
            if (this.Planogram == null) return;

            //Unsubscribe
            this.Planogram.PropertyChanged -= Planogram_PropertyChanged;

            //Subscribe
            if (isAttach)
            {
                this.Planogram.PropertyChanged += Planogram_PropertyChanged;
            }
        }

        protected override void OnFreeze()
        {
            //dettach events
            SetPlanEventHandlers(false);
        }

        protected override void OnUnfreeze()
        {
            //reattach events
            SetPlanEventHandlers(true);

            //Update data
            RefreshData();
        }

        #endregion

        #region Category Insight Loading

        /// <summary>
        /// Starts the background loading of the category insights
        /// </summary>
        public void RefreshData()
        {
            if (!this.HasRequiredInfo)
            {
                if (_categoryInsightLoader != null
                    && _categoryInsightLoader.IsBusy)
                {
                    _categoryInsightLoader.CancelAsync();
                }
                return;
            }


            //Reset content visible
            this.IsContentVisible = false;


            //Setup the worker
            if (_categoryInsightLoader == null)
            {
                //_entityId = App.ViewState.EntityId;
                _categoryInsightLoader = new Csla.Threading.BackgroundWorker();
                _categoryInsightLoader.WorkerSupportsCancellation = true;
                _categoryInsightLoader.DoWork += new DoWorkEventHandler(LoadCategoryInsightPlanDocument_DoWork);
                _categoryInsightLoader.RunWorkerCompleted += new RunWorkerCompletedEventHandler(LoadCategoryInsightPlanDocument_RunWorkerCompleted);
            }

            //Cancel the current execution if the worker is running
            if (!_categoryInsightLoader.IsBusy)
            {
                //Set the CurrentDisplay to Loading
                this.CurrentDisplay = CategoryInsightDisplay.Loading;

                _categoryInsightLoader.RunWorkerAsync();
            }
            else
            {
                _categoryInsightLoader.CancelAsync();
            }
        }

        #region Background Worker

        /// <summary>
        /// Loops through the execute steps allowing cancelling
        /// </summary>
        private void LoadCategoryInsightPlanDocument_DoWork(object sender, DoWorkEventArgs e)
        {
            Int32 stepNumber = 0;
            while (!_categoryInsightLoader.CancellationPending && stepNumber <= 3)
            {
                ExecuteStep(stepNumber);
                stepNumber++;
            }

            e.Cancel = _categoryInsightLoader.CancellationPending;
        }

        /// <summary>
        /// Sets the CurrentDisplay and SelectedTab if the loading was not cancelled
        /// Runs the worker again if the previous run was cancelled due to a product group change
        /// </summary>
        private void LoadCategoryInsightPlanDocument_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //The worker will be cancelled if a different product universe is loaded while it is running
            //So the worker needs to be started again to load for the new product universe
            if (e.Cancelled)
            {
                if (!String.IsNullOrWhiteSpace(this.Planogram.CategoryCode))
                {
                    _categoryInsightLoader.RunWorkerAsync();
                }
            }
            else
            {
                _locationSpecificRoles.Clear();
                if (this.LinkedCategoryRole != null &&
                    this.LinkedCategoryRole.Locations != null)
                {
                    foreach (var group in LinkedCategoryRole.Locations.GroupBy(p => p.RoleName))
                    {
                        _locationSpecificRoles.Add(group);
                    }
                    OnPropertyChanged(LocationSpecificRolesProperty);
                    OnPropertyChanged(HasLocationSpecificRoleProperty);
                }

                _availableCategoryGoalDetails.Clear();
                if (this.LinkedCategoryGoal != null)
                {
                    foreach (CategoryGoalDetail goalDetail in this.LinkedCategoryGoal.Details)
                    {
                        _availableCategoryGoalDetails.Add(new CategoryGoalDetailGridItem(goalDetail));
                    }
                    OnPropertyChanged(AvailableCategoryGoalDetailsProperty);
                }

                _availableCategoryTacticDetails.Clear();
                if (this.LinkedCategoryTactic != null)
                {
                    //Group the CategoryTacticDetails by TacticTypeName
                    IEnumerable<IGrouping<String, CategoryTacticDetail>> tacticTypeGroups = this.LinkedCategoryTactic.Details.GroupBy(p => p.TacticTypeName);

                    //Create the GridItems setting the first of each group as the header
                    foreach (IGrouping<String, CategoryTacticDetail> group in tacticTypeGroups)
                    {
                        Boolean isGroupHeader = true;
                        foreach (CategoryTacticDetail tacticDetail in group)
                        {
                            _availableCategoryTacticDetails.Add(new CategoryTacticDetailGridItem(tacticDetail, isGroupHeader));
                            isGroupHeader = false;
                        }
                    }
                    OnPropertyChanged(AvailableCategoryTacticDetailsProperty);
                }

                //Set the CurrentDisplay to show the Category Roles tab
                this.IsContentVisible = true;
                this.CurrentDisplay = CategoryInsightDisplay.Tabs;
            }
        }

        /// <summary>
        /// Executes the insight methods for each step
        /// </summary>
        private void ExecuteStep(Int32 stepNumber)
        {
            switch (stepNumber)
            {
                case 0:
                    RefreshGFSConnectionInfo();
                    break;
                case 1:
                    GetCategoryRoleDetails();
                    break;
                case 2:
                    GetCategoryGoalDetails();
                    break;
                case 3:
                    GetCategoryStrategyAndTacticDetails();
                    break;
            }
        }

        #endregion

        #region Steps

        /// <summary>
        /// Retreives GFS Entity Name for the GfsId saved against current SA Entity
        /// </summary>
        private void RefreshGFSConnectionInfo()
        {
            if (!App.ViewState.IsConnectedToRepository)
            {
                _gfsEntityName = null;
                _endpointRoot = null;
                return;
            }
            try
            {
                Model.Entity ccmEntity = Model.Entity.FetchById(App.ViewState.EntityId);
                if (!ccmEntity.GFSId.HasValue) return;

                _endpointRoot = ccmEntity.SystemSettings.FoundationServicesEndpoint;

                GFSModel.Entity gfsEntity = GFSModel.Entity.FetchByCcmEntity(ccmEntity);
                _gfsEntityName = gfsEntity.Name;

            }
            catch (Exception)
            {
                _gfsEntityName = null;
                _endpointRoot = null;
            }
        }

        /// <summary>
        /// Loads the CategoryRole details from GFS
        /// </summary>
        private void GetCategoryRoleDetails()
        {
            // if failed to fetch GFS Entity Name save time trying to fetch insights
            if (String.IsNullOrEmpty(_gfsEntityName))
            {
                this.NoCategoryRoleMessage = Message.CategoryInsightPlanDocument_FailedToFindMatchingGFSEntity;
                this.LinkedCategoryRole = null;
                this.DocumentCount = 0;
            }
            else
            {
                // reset no category role message
                this.NoCategoryRoleMessage = Message.CategoryInsightPlanDocument_RolesTab_NoLink;
                try
                {
                    //fetch category role for the selected entity id and product group
                    this.LinkedCategoryRole = CategoryRole.FetchCategoryRoleByGfsEntityNameProductGroupCode(_endpointRoot, _gfsEntityName, this.Planogram.CategoryCode);

                    //load preview for the first document, if possible
                    if (this.LinkedCategoryRole != null)
                    {
                        this.DocumentCount = this.LinkedCategoryRole.Files.Count();
                        if (this.DocumentCount > 0)
                        {
                            this.SelectedDocumentId = this.LinkedCategoryRole.Files.First().FileId;
                            this.SelectedDocumentIndex = 0;
                        }
                    }
                }
                catch (DataPortalException e)
                {
                    //populate error message
                    this.NoCategoryRoleMessage = e.GetBaseException().Message;
                    if (e.GetBaseException().Message != e.BusinessException.Message)
                    {
                        this.NoCategoryRoleMessage += "\n" + e.BusinessException.Message;
                    }

                    if (e.InnerException.Message == _innerExceptionContents)
                    {
                        this.LinkedCategoryRole = null;
                        this.DocumentCount = 0;
                    }
                }
            }
        }

        /// <summary>
        /// Loads the CategoryGoal details from GFS
        /// </summary>
        private void GetCategoryGoalDetails()
        {
            // if failed to fetch GFS Entity Name save time trying to fetch insights
            if (String.IsNullOrEmpty(_gfsEntityName))
            {
                this.NoCategoryGoalMessage = Message.CategoryInsightPlanDocument_FailedToFindMatchingGFSEntity;
                this.LinkedCategoryGoal = null;
            }
            else
            {
                // reset no category goal message
                this.NoCategoryGoalMessage = Message.CategoryInsightPlanDocument_GoalsTab_NoLinkedGoal;
                try
                {
                    this.LinkedCategoryGoal = CategoryGoal.FetchCategoryGoalByGfsEntityNameProductGroupCode(_endpointRoot, _gfsEntityName, this.Planogram.CategoryCode);
                }
                catch (DataPortalException e)
                {
                    //if the webservice did not return expected data portal fetch exception with _noResultsException
                    if (!e.Message.Contains(_noResultsException))
                    {
                        //populate error message
                        this.NoCategoryGoalMessage = e.GetBaseException().Message;
                        if (e.GetBaseException().Message != e.BusinessException.Message)
                        { this.NoCategoryGoalMessage += "\n" + e.BusinessException.Message; }
                    }

                    //The category goal service will return an exception if the value is null, so set the linked category goal to null
                    this.LinkedCategoryGoal = null;

                }
            }
        }

        /// <summary>
        /// Loads the CategoryStrategyAndTactics details from GFS
        /// </summary>
        private void GetCategoryStrategyAndTacticDetails()
        {
            // if failed to fetch GFS Entity Name save time trying to fetch insights
            if (String.IsNullOrEmpty(_gfsEntityName))
            {
                this.NoCategoryTacticMessage = Message.CategoryInsightPlanDocument_FailedToFindMatchingGFSEntity;
                this.LinkedCategoryTactic = null;
            }
            else
            {
                // reset no category goal message
                this.NoCategoryTacticMessage = Message.CategoryInsightPlanDocument_TacticTab_NoLinkedTactic;
                try
                {
                    this.LinkedCategoryTactic = CategoryTactic.FetchCategoryTacticByGfsEntityNameProductGroupCode(_endpointRoot, _gfsEntityName, this.Planogram.CategoryCode);
                }
                catch (DataPortalException e)
                {
                    //if the webservice did not return expected data portal fetch exception with _noResultsException
                    if (!e.Message.Contains(_noResultsException))
                    {
                        //populate error message
                        this.NoCategoryTacticMessage = e.GetBaseException().Message;
                        if (e.GetBaseException().Message != e.BusinessException.Message)
                        { this.NoCategoryTacticMessage += "\n" + e.BusinessException.Message; }
                    }
                    this.LinkedCategoryTactic = null;
                }
            }
        }

        #endregion

        #endregion


        /// <summary>
        /// Loads preview image for the selected document and refreshes value of document position description
        /// </summary>
        /// <param name="documentId"></param>
        private void LoadImagePreview(Int32 documentId)
        {
            // check if preview image exists already
            Tuple<String, Int32> fileIdentifier = new Tuple<String, int>(this.Planogram.CategoryCode, this.SelectedDocumentId);
            if (_fileIdentifierToDocumentPreviewLookup.ContainsKey(fileIdentifier)) //if it wasn't created successfully no point in saving it
            {
                this.SelectedDocumentPreviewImage = _fileIdentifierToDocumentPreviewLookup[fileIdentifier];
            }
            else
            {
                // get the preview for the document
                _image = Galleria.Ccm.GFSModel.Image.FetchByFileId(_endpointRoot, documentId);
                this.SelectedDocumentPreviewImage = GFSModel.Image.GetBitmapImage(_image.DocumentPreviewByteArray);
                if (this.SelectedDocumentPreviewImage != null) //if it wasn't created successfully no point in saving it
                {
                    _fileIdentifierToDocumentPreviewLookup.Add(fileIdentifier, this.SelectedDocumentPreviewImage);
                }
            }

            // update document position
            this.DocumentPosition = String.Format("{0} \\ {1}", _selectedDocumentIndex + 1, _documentCount);
        }

        #endregion

        #region IPlanDocument members

        public override String Title
        {
            get { return DocumentTypeHelper.FriendlyNames[DocumentType.CategoryInsight]; }
        }

        public override DocumentType DocumentType
        {
            get { return Model.DocumentType.CategoryInsight; }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (IsDisposed) return;

            App.ViewState.EntityIdChanged -= ViewState_EntityIdChanged;
            SetPlanEventHandlers(false);


            base.Dispose(disposing);
        }

        #endregion

    }

    public enum CategoryInsightDisplay
    {
        Loading,
        Tabs,
        NoProductGroup
    }

    #region CategoryInsightAdorner

    //public sealed class CategoryInsightAdorner : Adorner
    //{
    //    #region Fields

    //    private VisualCollection _visualChildren;
    //    private CategoryInsightOrganiser _categoryInsight;

    //    #endregion

    //    #region Properties

    //    public CategoryInsightPlanDocumentView CategoryInsight
    //    {
    //        get
    //        {
    //            return _categoryInsight;
    //        }
    //    }

    //    #endregion

    //    #region Constructor

    //    public CategoryInsightAdorner(UIElement adornedElement)
    //        : base(adornedElement)
    //    {
    //        _visualChildren = new VisualCollection(this);

    //        //Add the category insight panel
    //        _categoryInsight = new CategoryInsightPlanDocumentView();
    //        _visualChildren.Add(_categoryInsight);
    //    }

    //    #endregion

    //    #region Methods

    //    #endregion

    //    #region Events

    //    #endregion

    //    #region Overrides

    //    protected override Size ArrangeOverride(Size finalSize)
    //    {
    //        //Set the category insight panel's dimensions, use the adorned element's width, and place at the bottom
    //        Rect adornedElementRect = new Rect(base.AdornedElement.RenderSize);
    //        _categoryInsight.Arrange(new Rect(0, adornedElementRect.Height - _categoryInsight.Height, adornedElementRect.Width, _categoryInsight.Height));

    //        return finalSize;
    //    }

    //    protected override int VisualChildrenCount
    //    {
    //        get { return _visualChildren.Count; }
    //    }

    //    protected override Visual GetVisualChild(Int32 index)
    //    {
    //        return _visualChildren[index];
    //    }

    //    #endregion
    //}
    #endregion

    //public enum TransitionType
    //{
    //    Open,
    //    Close
    //}

    #region TransitionEventArgs

    //public sealed class TransitionEventArgs : EventArgs
    //{
    //    #region Fields
    //    private TransitionType _transitionType;
    //    #endregion

    //    #region Properties
    //    public TransitionType TransitionType
    //    {
    //        get { return _transitionType; }
    //    }
    //    #endregion

    //    #region Constructor
    //    public TransitionEventArgs(TransitionType transitionType)
    //        : base()
    //    {
    //        _transitionType = transitionType;
    //    }
    //    #endregion
    //}

    #endregion
}
