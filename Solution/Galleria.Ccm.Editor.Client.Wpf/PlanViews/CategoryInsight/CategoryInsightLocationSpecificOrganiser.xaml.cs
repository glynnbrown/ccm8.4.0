﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015


#region Version History : CCM830
// V8-31834 : L.Ineson
//  Copied from SA
#endregion

#endregion


using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.CategoryInsight
{
    /// <summary>
    /// Interaction logic for CategoryInsightLocationSpecificOrganiser.xaml
    /// </summary>
    public partial class CategoryInsightLocationSpecificOrganiser : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(CategoryInsightLocationSpecificViewModel), typeof(CategoryInsightLocationSpecificOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets the viewmodel controller for this window
        /// </summary>
        public CategoryInsightLocationSpecificViewModel ViewModel
        {
            get { return (CategoryInsightLocationSpecificViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            CategoryInsightLocationSpecificOrganiser senderControl = (CategoryInsightLocationSpecificOrganiser)obj;

            if (e.OldValue != null)
            {
                CategoryInsightLocationSpecificViewModel oldModel = (CategoryInsightLocationSpecificViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                CategoryInsightLocationSpecificViewModel newModel = (CategoryInsightLocationSpecificViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public CategoryInsightLocationSpecificOrganiser(CategoryInsightLocationSpecificViewModel viewModel)
        {
            //Set the wait cursor
            Mouse.OverrideCursor = Cursors.Wait;

            //Create the view model
            this.ViewModel = viewModel;

            //Initialize
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(CategoryInsightLocationSpecificOrganiser_Loaded);
        }

        #endregion

        #region Event Handlers

        private void CategoryInsightLocationSpecificOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= CategoryInsightLocationSpecificOrganiser_Loaded;

            CreateDefaultGroupBy();

            //Cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() =>
            {
                Mouse.OverrideCursor = null;
            }));
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates the default grouping on the locations grid
        /// </summary>
        public void CreateDefaultGroupBy()
        {
            PropertyGroupDescription groupDescription = null;
            if (this.ViewModel.DataType == CategoryInsightDataType.CategoryStrategy)
            {
                groupDescription = new PropertyGroupDescription("StrategyName");
            }
            else if (this.ViewModel.DataType == CategoryInsightDataType.CategoryGoal ||
                     this.ViewModel.DataType == CategoryInsightDataType.CategoryTactic)
            {
                groupDescription = new PropertyGroupDescription("IsIncluded");
            }

            if (groupDescription != null)
            {
                this.locationsGrid.GroupByDescriptions.Add(groupDescription);
            }
        }

        #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            if (this.ViewModel != null)
            {
                IDisposable dis = this.ViewModel;

                this.ViewModel = null;

                if (dis != null)
                {
                    dis.Dispose();
                }

            }
        }

        #endregion
    }
}
