﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015


#region Version History : CCM830
// V8-31834 : L.Ineson
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.CategoryInsight
{
    /// <summary>
    /// Interaction logic for CategoryInsightPlanDocumentView.xaml
    /// </summary>
    public sealed partial class CategoryInsightPlanDocumentView : IPlanDocumentView
    {
        #region Properties

        #region ViewModel Property

        /// <summary>
        /// ViewModel dependency property definition
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(CategoryInsightPlanDocument),
                typeof(CategoryInsightPlanDocumentView), new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Called whenever the value of the viewmodel dependency property changes.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            CategoryInsightPlanDocumentView senderControl = (CategoryInsightPlanDocumentView)sender;

            CategoryInsightPlanDocument oldModel = e.OldValue as CategoryInsightPlanDocument;
            if (oldModel != null)
            {
                oldModel.AttachedControl = null;
            }

            CategoryInsightPlanDocument newModel = e.NewValue as CategoryInsightPlanDocument;
            if (newModel != null)
            {
                newModel.AttachedControl = senderControl;
            }
        }

        /// <summary>
        ///     Gets the viewmodel controller for this view.
        /// </summary>
        public CategoryInsightPlanDocument ViewModel
        {
            get { return (CategoryInsightPlanDocument)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public CategoryInsightPlanDocumentView(CategoryInsightPlanDocument planDocument)
        {
            // Show the busy cursor while loading the screen.
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = planDocument;

            Loaded += OnLoaded;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called on initial load of this control.
        /// </summary>
        private void OnLoaded(Object sender, RoutedEventArgs routedEventArgs)
        {
            Loaded -= OnLoaded;

            Mouse.OverrideCursor = null;
        }

        /// <summary>
        /// Handler for a role document preview click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CategoryRoleDocumentPreview_Click(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.ShowDocumentCommand.Execute();
            }
        }


        #region Panel Resizing Handlers

        private void xCategoryRoleExpander_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = sender as Expander;
            if (exp == null) return;

            this.xCategoryRoleRow.Height =
                (exp.IsExpanded) ?
                new GridLength(1, GridUnitType.Star)
                : GridLength.Auto;

        }

        private void xCategoryRoleExpander_Collapsed(object sender, RoutedEventArgs e)
        {
            Expander exp = sender as Expander;
            if (exp == null) return;

            if (!exp.IsExpanded)
            {
                this.xCategoryRoleRow.Height = GridLength.Auto;
            }
        }

        private void xCategoryGoalExpander_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = sender as Expander;
            if (exp == null) return;

            this.xCategoryGoalRow.Height =
                (exp.IsExpanded) ?
                 new GridLength(1, GridUnitType.Star)
                 : GridLength.Auto;
        }

        private void xCategoryGoalExpander_Collapsed(object sender, RoutedEventArgs e)
        {
            Expander exp = sender as Expander;
            if (exp == null) return;

            if (!exp.IsExpanded)
            {
                this.xCategoryGoalRow.Height = GridLength.Auto;
            }
        }

        private void xCategoryStrategyExpander_Expanded(object sender, RoutedEventArgs e)
        {
            Expander exp = sender as Expander;
            if (exp == null) return;

            if (exp.IsExpanded)
            {
                this.xCategoryStrategyRow.Height = new GridLength(1, GridUnitType.Star);
            }
        }

        private void xCategoryStrategyExpander_Collapsed(object sender, RoutedEventArgs e)
        {
            Expander exp = sender as Expander;
            if (exp == null) return;

            if (!exp.IsExpanded)
            {
                this.xCategoryStrategyRow.Height = GridLength.Auto;
            }
        }

        #endregion

        #endregion

        #region IPlanDocumentView Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean isDisposing)
        {
            if (_isDisposed) return;

            _isDisposed = true;

            this.ViewModel = null;
        }

        public IPlanDocument GetIPlanDocument()
        {
            return ViewModel;
        }

        #endregion
    }
}
