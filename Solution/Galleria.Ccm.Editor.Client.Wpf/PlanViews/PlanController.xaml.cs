﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Ineson 
//  Created.
//  V8-24539 : A.Probyn
//      Turned float back on
// V8-26517 : L.Ineson
//  No longer takes 2 clicks to select a view.
// V8-27121 : A.Kuszyk
//  Amended ArrangeAllWindows() to arrange Assortment, CDT and Blocking Plan Documents.
// V8-27589 : A.Silva
//      Added PlanogramValidationWarningsPanel to ShowHidePanels.

#endregion
#region Version History: (CCM 8.0.2)
// V8-28903 : D.Pleasance
//  Fix for updating selected document and tab, see comments referencing 28903.
#endregion
#region Version History: (CCM 8.1)
// V8-29375 : L.Ineson
//  Datasheets no longer disappears when docking all.
#endregion
#region Version History: (CCM 8.2)
// V8-30635 : L.Ineson
//  Added call to docking manager dispose.
// V8-30681 : L.Ineson
//  Made sure that floating views get made active when clicked.
// V8-31320 : L.Ineson
//  More changes to how the docking panels are controlled and activated to stop infinite loops happening.
// V8-28075 : L.Ineson
// DockAllViews now calls zoom on low priority so controls have change to measure.
#endregion

#region Version History: CCM830

// V8-32343 : A.Silva
//  Added a try-catch to TileWindows when moving the panels to a temporary one, as there seems to be something going inside Exceed that breaks it in some cases.
//  The error seems to not affect the end result and everything is tiled fine.
// V8-32661 : A.Kuszyk
//  Added Sequence Template panel.
#endregion

#endregion

using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews.Blocking;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews.ConsumerDecisionTree;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews.ProductsList;
using Galleria.Ccm.Editor.Client.Wpf.PrimaryWindow;
using Galleria.Framework.Model;
using Galleria.Framework.Rendering;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Xceed.Wpf.AvalonDock;
using Xceed.Wpf.AvalonDock.Controls;
using Xceed.Wpf.AvalonDock.Layout;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Controls all the documents for a given planogram.
    /// </summary>
    public sealed partial class PlanController : UserControl, ILayoutUpdateStrategy
    {
        #region Fields
        private Boolean _supressShowHidePanels;
        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanControllerViewModel), typeof(PlanController),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanController senderControl = (PlanController)obj;

            if (e.OldValue != null)
            {
                PlanControllerViewModel oldModel = (PlanControllerViewModel)e.OldValue;
                oldModel.AttachedControl = null;

                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                oldModel.PlanDocuments.BulkCollectionChanged -= senderControl.ViewModel_PlanDocumentsBulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                PlanControllerViewModel newModel = (PlanControllerViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
                newModel.PlanDocuments.BulkCollectionChanged += senderControl.ViewModel_PlanDocumentsBulkCollectionChanged;
            }

            senderControl.ReloadDocuments();
            senderControl.ShowHidePanels();
        }

        /// <summary>
        /// Gets/Sets the viewmodel controller
        /// </summary>
        public PlanControllerViewModel ViewModel
        {
            get { return (PlanControllerViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region DocumentControls Property

        public static readonly DependencyProperty DocumentControlsProperty =
            DependencyProperty.Register("DocumentControls", typeof(ObservableCollection<IPlanDocumentView>), typeof(PlanController),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the editable collection of document controls.
        /// </summary>
        public ObservableCollection<IPlanDocumentView> DocumentControls
        {
            get { return (ObservableCollection<IPlanDocumentView>)GetValue(DocumentControlsProperty); }
        }


        #endregion

        #region Panels Property

        public static readonly DependencyProperty PanelsProperty =
            DependencyProperty.Register("Panels", typeof(ObservableCollection<UIElement>), typeof(PlanController),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the collection of panels to be displayed by the docking manager.
        /// </summary>
        public ObservableCollection<UIElement> Panels
        {
            get { return (ObservableCollection<UIElement>)GetValue(PanelsProperty); }
        }

        #endregion

        #region ActiveContent Property

        public static readonly DependencyProperty ActiveContentProperty =
            DependencyProperty.Register("ActiveContent", typeof(Object), typeof(PlanController),
           new PropertyMetadata(null, OnActiveContentPropertyChanged));

        private static void OnActiveContentPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((PlanController)obj).OnActiveContentChanged(e.NewValue as IPlanDocumentView);
        }

        /// <summary>
        /// Gets/Sets the currently active content.
        /// </summary>
        public Object ActiveContent
        {
            get { return GetValue(ActiveContentProperty); }
            set { SetValue(ActiveContentProperty, value); }
        }

        /// <summary>
        /// Called whenever the value of the active content property changes.
        /// </summary>
        /// <param name="newValue"></param>
        private void OnActiveContentChanged(IPlanDocumentView newValue)
        {
            if (this.ViewModel == null) return;

            if (newValue != null)
            {
                //make sure the selected plan document is up to date.
                this.ViewModel.SelectedPlanDocument = newValue.GetIPlanDocument();

                //fix to stop drag drop issues
                ((UIElement)newValue).AllowDrop = true;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="viewModel">The viewmodel controller for this.</param>
        public PlanController(PlanControllerViewModel viewModel)
        {
            SetValue(DocumentControlsProperty, new ObservableCollection<IPlanDocumentView>());
            SetValue(PanelsProperty, new ObservableCollection<UIElement>());

            InitializeComponent();
            this.ViewModel = viewModel;


            this.xDockingManager.Layout.ElementAdded += DockingManager_LayoutElementAdded;
            this.xDockingManager.Layout.RootPanel.ChildrenTreeChanged += DockingManager_RootPanelChildrenTreeChanged;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Fix to make sure that clicking a controller in a floating window makes it active.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewMouseUp(System.Windows.Input.MouseButtonEventArgs e)
        {
            base.OnPreviewMouseUp(e);

            if (this.ViewModel == null) return;

            //set this to be the active controller.
            if (!this.ViewModel.IsActivePlanController)
            {
                LayoutDocumentControl parentControl = this.FindVisualAncestor<LayoutDocumentControl>();
                if (parentControl != null)
                {
                    if (this.IsMouseOver && e.ChangedButton == System.Windows.Input.MouseButton.Left)
                    {
                        LayoutDocument doc = parentControl.Model as LayoutDocument;
                        if (doc != null)
                        {
                            if (doc.IsFloating && !doc.IsActive)
                            {
                                doc.IsActive = true;
                            }
                        }

                    }
                }
            }

            //make the floating view active.
            DependencyObject obj = e.OriginalSource as DependencyObject;
            while (obj != null && !(obj is IPlanDocumentView))
            {
                try
                {
                    obj = VisualTreeHelper.GetParent(obj);
                }
                catch (Exception)
                {
                    obj = null;
                    break;
                }
            }

            if (obj != null)
            {
                this.ActiveContent = (IPlanDocumentView)obj;
            }
        }

        /// <summary>
        /// Responds to property changes on the viewmodel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == PlanControllerViewModel.SelectedPlanDocumentProperty.Path)
            {
                if (this.ViewModel != null && this.ViewModel.SelectedPlanDocument != null)
                {
                    this.ActiveContent = this.ViewModel.SelectedPlanDocument.AttachedDocumentView;
                    ResyncAfterTreeChanged();
                }
            }
            else if (e.PropertyName == PlanControllerViewModel.IsPropertiesPanelVisibleProperty.Path ||
                e.PropertyName == PlanControllerViewModel.IsSequenceGroupsPanelVisibleProperty.Path)
            {
                ShowHidePanels();
            }
        }

        /// <summary>
        /// Responds to changes to the viewmodel documents collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PlanDocumentsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        foreach (IPlanDocument doc in e.ChangedItems)
                        {
                            this.DocumentControls.Add(PlanDocumentHelper.CreateDocumentView(doc));
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    {
                        //find the document and remove it.
                        foreach (IPlanDocument doc in e.ChangedItems)
                        {
                            IPlanDocumentView docView = this.DocumentControls.FirstOrDefault(p => p.GetIPlanDocument() == doc);
                            if (docView != null)
                            {
                                this.DocumentControls.Remove(docView);

                                //dispose
                                this.ViewModel.CloseDocument(docView.GetIPlanDocument());
                                docView.Dispose();
                            }
                        }
                    }
                    break;


                case NotifyCollectionChangedAction.Reset:
                    ReloadDocuments();
                    break;

            }
        }

        /// <summary>
        /// Called whenever a root panel child changes.
        /// </summary>
        private void DockingManager_RootPanelChildrenTreeChanged(object sender, ChildrenTreeChangedEventArgs e)
        {
            ResyncAfterTreeChanged();
        }

        /// <summary>
        /// Called whenever an item is added to the docking manager layout.
        /// </summary>
        private void DockingManager_LayoutElementAdded(object sender, LayoutElementEventArgs e)
        {
            //make this active but wait until the control has loaded for it
            // otherwise it bugs and sometimes leaves windows blank.
            this.Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    this.ActiveContent = e.Element;

                }), priority: System.Windows.Threading.DispatcherPriority.Loaded);

        }

        /// <summary>
        /// Called when a document is closed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xDockingManager_DocumentClosed(object sender, DocumentClosedEventArgs e)
        {
            if (e.Document == null) return;

            //remove the document from the viewmodel.
            IPlanDocumentView doc = e.Document.Content as IPlanDocumentView;
            if (doc != null)
            {
                this.ViewModel.CloseDocument(doc.GetIPlanDocument());
                doc.Dispose();
            }


        }

        /// <summary>
        /// Called a panel is hiding.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Panel_Hiding(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _supressShowHidePanels = true;

            LayoutAnchorable senderControl = (LayoutAnchorable)sender;

            if (senderControl.Content is PlanItemPropertiesPanel)
            {
                this.ViewModel.IsPropertiesPanelVisible = false;
            }
            else if (senderControl.Content is SequenceTemplateGroupsPanel)
            {
                this.ViewModel.IsSequenceGroupsPanelVisible = false;
            }

            _supressShowHidePanels = false;
        }

        /// <summary>
        /// Called when a panel is closing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Panel_Closed(object sender, EventArgs e)
        {
            LayoutAnchorable senderControl = (LayoutAnchorable)sender;
            senderControl.Hiding -= Panel_Hiding;
            senderControl.Closed -= Panel_Closed;

            IDisposable disposableContent = senderControl.Content as IDisposable;
            if (disposableContent != null)
            {
                disposableContent.Dispose();
            }

            if (senderControl.Content is PlanItemPropertiesPanel)
            {
                this.ViewModel.IsPropertiesPanelVisible = false;
            }
            else if (senderControl.Content is SequenceTemplateGroupsPanel)
            {
                this.ViewModel.IsSequenceGroupsPanelVisible = false;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Reloads all documents
        /// </summary>
        private void ReloadDocuments()
        {
            if (this.DocumentControls.Count > 0)
            {
                List<IPlanDocumentView> docs = this.DocumentControls.ToList();

                this.DocumentControls.Clear();

                foreach (IPlanDocumentView d in docs)
                {
                    this.ViewModel.CloseDocument(d.GetIPlanDocument());
                    d.Dispose();
                }
            }

            if (this.ViewModel != null)
            {
                foreach (IPlanDocument doc in this.ViewModel.PlanDocuments)
                {
                    this.DocumentControls.Add(PlanDocumentHelper.CreateDocumentView(doc));
                }
            }
        }

        /// <summary>
        /// Shows/Hides the panels
        /// </summary>
        private void ShowHidePanels()
        {
            if (_supressShowHidePanels) { return; }


            if (this.ViewModel != null)
            {
                List<LayoutAnchorable> anchorables = this.xDockingManager.Layout.Descendents().OfType<LayoutAnchorable>().ToList();

                //Properties Panel
                ShowHidePanel<PlanItemPropertiesPanel>(
                    this.ViewModel.IsPropertiesPanelVisible,
                    new Action(
                    () =>
                    {
                        PlanItemPropertiesPanel propertiesPanel = new PlanItemPropertiesPanel();
                        propertiesPanel.ViewModel = this.ViewModel.SelectedPlanItems;
                        propertiesPanel.GroupName = "Properties";
                        propertiesPanel.PlacementStrategy = AnchorableShowStrategy.Right;
                        this.Panels.Add(propertiesPanel);
                    }),
                    anchorables);

                // Validation Warnings Panel
                ShowHidePanel<PlanogramValidationWarningPanel>(
                    ViewModel.IsPropertiesPanelVisible,
                    () => Panels.Add(new PlanogramValidationWarningPanel(ViewModel.SourcePlanogram, ViewModel.SelectedPlanItems)
                    {
                        GroupName = "Properties",
                        PlacementStrategy = AnchorableShowStrategy.Bottom
                    }),
                    anchorables);

                // Sequence Template Panel
                ShowHidePanel<SequenceTemplateGroupsPanel>(
                    ViewModel.IsSequenceGroupsPanelVisible,
                    new Action(
                    () =>
                    {
                        SequenceTemplateGroupsPanel panel = new SequenceTemplateGroupsPanel();
                        panel.ViewModel = ViewModel.SelectedPlanDocument as PlanVisualDocument;
                        panel.GroupName = "Sequence Groups";
                        panel.PlacementStrategy = AnchorableShowStrategy.Right;
                        this.Panels.Add(panel);
                    }),
                    anchorables);
            }
            else
            {
                List<IDisposable> panels = this.Panels.Cast<IDisposable>().ToList();
                this.Panels.Clear();
                panels.ForEach(p => p.Dispose());
            }
        }

        /// <summary>
        /// Tiles all  plan documents horizontally.
        /// </summary>
        public void TileWindows(Orientation orientation)
        {
            if (this.xDockingManager == null) return;

            Orientation tileOrientation = Orientation.Horizontal;
            if (orientation == Orientation.Horizontal) tileOrientation = Orientation.Vertical;


            //get a list of all layout content
            List<LayoutDocument> contentList = this.xDockingManager.Layout.Descendents().OfType<LayoutDocument>().ToList();

            //create a temp pane and move all children there
            LayoutDocumentPane tempPane = new LayoutDocumentPane();
            LayoutDocumentPaneGroup tempGroup = new LayoutDocumentPaneGroup(tempPane);
            this.xDockingManager.Layout.RootPanel.Children.Add(tempGroup);

            foreach (LayoutDocument documentTab in contentList)
            {
                try
                {
                    tempPane.Children.Add(documentTab);
                }
                catch
                {
                    // V8-32343 - AS
                    //  No idea why but the Exceed control is failing while processing the changes to the layout.
                    //  This should be looked at when there is time, but for now, this will just ignore the exception and still tile the windows fine.
                }
            }

            //remove all other panes
            foreach (ILayoutPanelElement ele in this.xDockingManager.Layout.RootPanel.Children.ToList())
            {
                if (ele != this.xRightDockingPane && ele != tempGroup)
                {
                    this.xDockingManager.Layout.RootPanel.Children.Remove(ele);
                }
            }

            this.xDockingManager.Layout.RootPanel.Orientation = Orientation.Horizontal;


            //create the new  group with a pane per doc
            LayoutDocumentPaneGroup newGroup = new LayoutDocumentPaneGroup();
            newGroup.Orientation = tileOrientation;
            this.xDockingManager.Layout.RootPanel.Children.Insert(0, newGroup);

            foreach (LayoutDocument documentTab in contentList)
            {
                newGroup.Children.Add(new LayoutDocumentPane(documentTab));
            }


            //remove the temporary pane again
            this.xDockingManager.Layout.RootPanel.Children.Remove(tempGroup);


            //queue a zoom to fit all
            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    if (this.ViewModel != null)
                    {
                        this.ViewModel.ZoomToFitAll();
                    }

                }), priority: System.Windows.Threading.DispatcherPriority.Render);

        }

        /// <summary>
        /// Docks all plan documents into a single tab group.
        /// </summary>
        public void DockAllViews()
        {
            if (this.xDockingManager == null || this.ViewModel == null) return;


            IPlanDocument selectedDoc = this.ViewModel.SelectedPlanDocument;

            //get a list of all layout content
            List<LayoutDocument> contentList = this.xDockingManager.Layout.Descendents().OfType<LayoutDocument>().ToList();

            //create a temp pane and move all children there
            LayoutDocumentPane tempPane = new LayoutDocumentPane();
            LayoutDocumentPaneGroup tempGroup = new LayoutDocumentPaneGroup(tempPane);
            this.xDockingManager.Layout.RootPanel.Children.Add(tempGroup);

            foreach (LayoutDocument documentTab in contentList)
            {
                tempPane.Children.Add(documentTab);
            }

            //remove all other panes
            foreach (ILayoutPanelElement ele in this.xDockingManager.Layout.RootPanel.Children.ToList())
            {
                if (ele != this.xRightDockingPane && ele != tempGroup)
                {
                    this.xDockingManager.Layout.RootPanel.Children.Remove(ele);
                }
            }

            this.xDockingManager.Layout.RootPanel.Orientation = Orientation.Horizontal;

            this.ViewModel.SelectedPlanDocument = selectedDoc;


            //queue a zoom to fit all
            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    if (this.ViewModel != null)
                    {
                        this.ViewModel.ZoomToFitAll();
                    }

                }), priority: System.Windows.Threading.DispatcherPriority.Background);

        }

        /// <summary>
        /// Arranges all plan document windows.
        /// </summary>
        public void ArrangeAllWindows()
        {
            if (this.xDockingManager == null) return;

            //get a list of all layout content
            List<LayoutDocument> contentList = this.xDockingManager.Layout.Descendents().OfType<LayoutDocument>().ToList();

            //create a temp pane and move all children there
            LayoutDocumentPane tempPane = new LayoutDocumentPane();
            LayoutDocumentPaneGroup tempGroup = new LayoutDocumentPaneGroup(tempPane);
            this.xDockingManager.Layout.RootPanel.Children.Add(tempGroup);

            foreach (LayoutDocument documentTab in contentList)
            {
                tempPane.Children.Add(documentTab);
            }

            //remove all other panes
            foreach (ILayoutPanelElement ele in this.xDockingManager.Layout.RootPanel.Children.ToList())
            {
                if (ele != this.xRightDockingPane && ele != tempGroup)
                {
                    this.xDockingManager.Layout.RootPanel.Children.Remove(ele);
                }
            }

            this.xDockingManager.Layout.RootPanel.Orientation = Orientation.Horizontal;

            //group documents based on where they should live
            List<IPlanDocumentView> leftDocs = new List<IPlanDocumentView>();
            List<IPlanDocumentView> rightDocs = new List<IPlanDocumentView>();
            List<IPlanDocumentView> bottomDocs = new List<IPlanDocumentView>();
            List<IPlanDocumentView> topDocs = new List<IPlanDocumentView>();
            List<IPlanDocumentView> centerDocs = new List<IPlanDocumentView>();

            foreach (IPlanDocumentView docView in this.DocumentControls)
            {
                IPlanDocument planDoc = docView.GetIPlanDocument();
                if (planDoc != null)
                {
                    //if (planDoc is OrthPlanDocument)
                    //{
                    //    OrthPlanDocument orthPlanDoc = (OrthPlanDocument)planDoc;
                    //    switch (orthPlanDoc.ViewType)
                    //    {
                    //        case OrthPlanDocumentType.Front: 
                    //        case OrthPlanDocumentType.Back: centerDocs.Add(docView); break;
                    //        case OrthPlanDocumentType.Bottom: bottomDocs.Add(docView); break;
                    //        case OrthPlanDocumentType.Top: topDocs.Add(docView); break;
                    //        case OrthPlanDocumentType.Left: leftDocs.Add(docView); break;
                    //        case OrthPlanDocumentType.Right: rightDocs.Add(docView); break;
                    //    }
                    //}
                    //else if (planDoc is ThreeDPlanDocument)
                    //{
                    //    centerDocs.Add(docView);
                    //}
                    if (planDoc is PlanVisualDocument)
                    {
                        switch (((PlanVisualDocument)planDoc).ViewType)
                        {
                            default: centerDocs.Add(docView); break;
                            case CameraViewType.Design: centerDocs.Add(docView); break;
                            case CameraViewType.Front: centerDocs.Add(docView); break;
                            case CameraViewType.Back: centerDocs.Add(docView); break;
                            case CameraViewType.Bottom: bottomDocs.Add(docView); break;
                            case CameraViewType.Top: topDocs.Add(docView); break;
                            case CameraViewType.Left: leftDocs.Add(docView); break;
                            case CameraViewType.Right: rightDocs.Add(docView); break;
                            case CameraViewType.Perspective: centerDocs.Add(docView); break;
                        }

                    }
                    else if (planDoc is ProductListPlanDocument || planDoc is AssortmentPlanDocument)
                    {
                        rightDocs.Add(docView);
                    }
                    else if (planDoc is FixtureListPlanDocument || planDoc is BlockingPlanDocument || planDoc is ConsumerDecisionTreePlanDocument)
                    {
                        leftDocs.Add(docView);
                    }
                    else
                    {
                        rightDocs.Add(docView);
                    }
                }
            }

            //create each panel

            //create right
            if (rightDocs.Count > 0)
            {
                LayoutDocumentPaneGroup rightGroup = new LayoutDocumentPaneGroup();
                rightGroup.Orientation = Orientation.Horizontal;
                this.xDockingManager.Layout.RootPanel.Children.Insert(0, rightGroup);

                foreach (IPlanDocumentView docView in rightDocs)
                {
                    LayoutDocument documentTab = contentList.FirstOrDefault(c => c.Content == docView);
                    if (documentTab != null)
                    {
                        LayoutDocumentPane pane = new LayoutDocumentPane();
                        rightGroup.Children.Add(pane);
                        pane.Children.Add(documentTab);
                    }
                }
            }


            //create center vert
            LayoutDocumentPaneGroup centerVertGroup = new LayoutDocumentPaneGroup();
            centerVertGroup.Orientation = Orientation.Vertical;
            this.xDockingManager.Layout.RootPanel.Children.Insert(0, centerVertGroup);

            //create top
            if (topDocs.Count > 0)
            {
                LayoutDocumentPaneGroup topGroup = new LayoutDocumentPaneGroup();
                topGroup.Orientation = Orientation.Vertical;
                centerVertGroup.Children.Add(topGroup);

                foreach (IPlanDocumentView docView in topDocs)
                {
                    LayoutDocument documentTab = contentList.FirstOrDefault(c => c.Content == docView);
                    if (documentTab != null)
                    {
                        LayoutDocumentPane pane = new LayoutDocumentPane();
                        topGroup.Children.Add(pane);
                        pane.Children.Add(documentTab);
                    }
                }
            }

            //create center
            if (centerDocs.Count > 0)
            {
                LayoutDocumentPaneGroup centerGroup = new LayoutDocumentPaneGroup();
                centerGroup.Orientation = Orientation.Horizontal;
                centerVertGroup.Children.Add(centerGroup);

                foreach (IPlanDocumentView docView in centerDocs)
                {
                    LayoutDocument documentTab = contentList.FirstOrDefault(c => c.Content == docView);
                    if (documentTab != null)
                    {
                        LayoutDocumentPane pane = new LayoutDocumentPane();
                        centerGroup.Children.Add(pane);
                        pane.Children.Add(documentTab);
                    }
                }
            }

            //create bottom
            if (bottomDocs.Count > 0)
            {
                LayoutDocumentPaneGroup bottomGroup = new LayoutDocumentPaneGroup();
                bottomGroup.Orientation = Orientation.Vertical;
                centerVertGroup.Children.Add(bottomGroup);

                foreach (IPlanDocumentView docView in bottomDocs)
                {
                    LayoutDocument documentTab = contentList.FirstOrDefault(c => c.Content == docView);
                    if (documentTab != null)
                    {
                        LayoutDocumentPane pane = new LayoutDocumentPane();
                        bottomGroup.Children.Add(pane);
                        pane.Children.Add(documentTab);
                    }
                }
            }

            //create left
            if (leftDocs.Count > 0)
            {
                LayoutDocumentPaneGroup leftGroup = new LayoutDocumentPaneGroup();
                leftGroup.Orientation = Orientation.Horizontal;
                this.xDockingManager.Layout.RootPanel.Children.Insert(0, leftGroup);

                foreach (IPlanDocumentView docView in leftDocs)
                {
                    LayoutDocument documentTab = contentList.FirstOrDefault(c => c.Content == docView);
                    if (documentTab != null)
                    {
                        LayoutDocumentPane pane = new LayoutDocumentPane();
                        leftGroup.Children.Add(pane);
                        pane.Children.Add(documentTab);
                    }
                }
            }


            //remove the temporary pane again
            this.xDockingManager.Layout.RootPanel.Children.Remove(tempGroup);

            //queue a zoom to fit all
            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    if (this.ViewModel != null)
                    {
                        this.ViewModel.ZoomToFitAll();
                    }

                }), priority: System.Windows.Threading.DispatcherPriority.Render);


        }

        /// <summary>
        /// Ensures that active content, selected doc and selected tab match up.
        /// </summary>
        private void ResyncAfterTreeChanged()
        {
            //check active
            this.Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    if (this.ActiveContent != null && this.ActiveContent is LayoutDocument)
                    {
                        IPlanDocumentView activeValue = ((LayoutDocument)this.ActiveContent).Content as IPlanDocumentView;
                        if (activeValue != null)
                        {


                            //Fix to force document selection.
                            foreach (LayoutDocumentPaneControl tabControl in
                                        this.xDockingManager.FindVisualChildren<LayoutDocumentPaneControl>().ToList())
                            {
                                foreach (LayoutDocument doc in tabControl.Items)
                                {
                                    if (doc.Content == activeValue)
                                    {
                                        if (tabControl.SelectedIndex != tabControl.Items.IndexOf(doc))
                                        {
                                            tabControl.SelectedIndex = tabControl.Items.IndexOf(doc);
                                        }
                                        break;
                                    }

                                }
                            }

                            //make sure the selected plan document is correct too.
                            if (this.ViewModel != null)
                            {
                                this.ViewModel.SelectedPlanDocument = activeValue.GetIPlanDocument();
                            }
                        }
                    }

                }), priority: System.Windows.Threading.DispatcherPriority.ContextIdle);
        }

        ///// <summary>
        ///// Arranges the documents based on the given layout
        ///// </summary>
        ///// <param name="layout"></param>
        //public void ArrangeLayout(ViewLayout layout, Dictionary<IPlanDocument, ViewLayoutItem> layoutItemToDocDict)
        //{
        //    if (this.xDockingManager != null && layout != null)
        //    {
        //        //get a list of all layout content
        //        List<LayoutDocument> contentList = this.xDockingManager.Layout.Descendents().OfType<LayoutDocument>().ToList();
        //        var docTabDict = new Dictionary<ViewLayoutItem, LayoutDocument>();
        //        foreach (LayoutDocument docTab in contentList)
        //        {
        //            if (docTab.Content is IPlanDocumentView)
        //            {
        //                IPlanDocument doc = ((IPlanDocumentView)docTab.Content).GetIPlanDocument();
        //                ViewLayoutItem layoutItem;
        //                if (layoutItemToDocDict.TryGetValue(doc, out layoutItem))
        //                {
        //                    docTabDict.Add(layoutItem, docTab);
        //                }
        //            }
        //        }


        //        //create a temp pane and move all children there
        //        LayoutDocumentPane tempPane = new LayoutDocumentPane();
        //        LayoutDocumentPaneGroup tempGroup = new LayoutDocumentPaneGroup(tempPane);
        //        this.xDockingManager.Layout.RootPanel.Children.Add(tempGroup);

        //        foreach (LayoutDocument documentTab in contentList)
        //        {
        //            tempPane.Children.Add(documentTab);
        //        }

        //        //remove all other panes
        //        foreach (ILayoutPanelElement ele in this.xDockingManager.Layout.RootPanel.Children.ToList())
        //        {
        //            if (ele != this.xRightDockingPane && ele != tempGroup)
        //            {
        //                this.xDockingManager.Layout.RootPanel.Children.Remove(ele);
        //            }
        //        }

        //        this.xDockingManager.Layout.RootPanel.Orientation = Orientation.Horizontal;

        //        //create and add the root group
        //        LayoutDocumentPaneGroup group = new LayoutDocumentPaneGroup();
        //        group.Orientation = (layout.IsVertical) ? Orientation.Vertical : Orientation.Horizontal;
        //        this.xDockingManager.Layout.RootPanel.Children.Insert(0, group);

        //        //load the layout items.
        //        LayoutDocumentPane lastDocPane = null;
        //        foreach (ViewLayoutItem item in layout.Items.OrderBy(p => p.Order))
        //        {
        //            lastDocPane = LoadPlanViewLayoutItem(item, group.Children, docTabDict, lastDocPane);
        //        }

        //        //remove the temporary pane again
        //        this.xDockingManager.Layout.RootPanel.Children.Remove(tempGroup);

        //    }

        //}

        #endregion

        #region ILayoutUpdateStrategy Members

        /// <summary>
        /// Called before a document has been inserted
        /// </summary>
        /// <param name="layout">the layout to which it will be added.</param>
        /// <param name="anchorableToShow">the document itself.</param>
        /// <param name="destinationContainer">the specific destination panel or null if the document is new.</param>
        /// <returns>True if the document has been added by this method</returns>
        Boolean ILayoutUpdateStrategy.BeforeInsertDocument(LayoutRoot layout, LayoutDocument anchorableToShow, ILayoutContainer destinationContainer)
        {
            //Turn document floating off as it is causing
            // preview mouse move to trigger and lock the screen up.
            anchorableToShow.CanFloat = true;

            return false;
        }

        /// <summary>
        /// Called after the insertion of a document
        /// </summary>
        /// <param name="layout"></param>
        /// <param name="anchorableShown"></param>
        void ILayoutUpdateStrategy.AfterInsertDocument(LayoutRoot layout, LayoutDocument anchorableShown)
        {

        }

        /// <summary>
        /// Called before an anchorable is to be inserted.
        /// </summary>
        /// <param name="layout">the layout to which it will be added.</param>
        /// <param name="anchorableToShow">the anchorable itself.</param>
        /// <param name="destinationContainer">the specific destination panel or null if the anchorable is new.</param>
        /// <returns>True if the anchorable has been added by this method</returns>
        Boolean ILayoutUpdateStrategy.BeforeInsertAnchorable(LayoutRoot layout, LayoutAnchorable anchorableToShow,
            ILayoutContainer destinationContainer)
        {
            //Turn document floating off as it is causing
            // preview mouse move to trigger and lock the screen up.
            anchorableToShow.CanFloat = true;

            DockingPanelControl panel = anchorableToShow.Content as DockingPanelControl;

            // If the pane is not a DockingPanelControl
            // OR there is a destination container already... just continue.
            if (panel == null || destinationContainer != null)
            {
                return false;
            }

            LayoutAnchorablePane destPane = layout.Descendents().OfType<LayoutAnchorablePane>()
                .FirstOrDefault(d => d.Name == panel.GroupName);
            if (destPane != null)
            {
                destPane.Children.Add(anchorableToShow);
                return true;
            }

            destPane = new LayoutAnchorablePane(anchorableToShow)
            {
                Name = panel.GroupName,
                DockWidth = new GridLength(panel.DockedWidth, GridUnitType.Pixel)
            };

            //Add the pane acording to the placement strategy
            AnchorableShowStrategy strategy = panel.PlacementStrategy;

            Boolean most = (strategy & AnchorableShowStrategy.Most) == AnchorableShowStrategy.Most;
            Boolean left = (strategy & AnchorableShowStrategy.Left) == AnchorableShowStrategy.Left;
            Boolean right = (strategy & AnchorableShowStrategy.Right) == AnchorableShowStrategy.Right;
            Boolean top = (strategy & AnchorableShowStrategy.Top) == AnchorableShowStrategy.Top;
            Boolean bottom = (strategy & AnchorableShowStrategy.Bottom) == AnchorableShowStrategy.Bottom;


            if (layout.RootPanel == null)
            {
                layout.RootPanel = new LayoutPanel
                {
                    Orientation = (left || right ? Orientation.Horizontal : Orientation.Vertical)
                };
            }

            if (left || right)
            {
                if (layout.RootPanel.Orientation == Orientation.Vertical &&
                    layout.RootPanel.ChildrenCount > 1)
                {
                    layout.RootPanel = new LayoutPanel(layout.RootPanel);
                }

                layout.RootPanel.Orientation = Orientation.Horizontal;

                if (left)
                {
                    layout.RootPanel.Children.Insert(0, destPane);
                }
                else
                {
                    layout.RootPanel.Children.Add(destPane);
                }
            }
            else
            {
                if (layout.RootPanel.Orientation == Orientation.Horizontal &&
                    layout.RootPanel.ChildrenCount > 1)
                {
                    layout.RootPanel = new LayoutPanel(layout.RootPanel);
                }

                layout.RootPanel.Orientation = Orientation.Vertical;

                if (top)
                {
                    layout.RootPanel.Children.Insert(0, destPane);
                }
                else
                {
                    layout.RootPanel.Children.Add(destPane);
                }
            }

            return true;
        }

        /// <summary>
        /// Called after an anchorable has been inserted.
        /// </summary>
        /// <param name="layout"></param>
        /// <param name="anchorableShown"></param>
        void ILayoutUpdateStrategy.AfterInsertAnchorable(LayoutRoot layout, LayoutAnchorable anchorableShown)
        {
            anchorableShown.Hiding -= Panel_Hiding;
            anchorableShown.Hiding += Panel_Hiding;

            anchorableShown.Closed -= Panel_Closed;
            anchorableShown.Closed += Panel_Closed;
        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(Boolean isDisposing)
        {
            if (!_isDisposed)
            {
                if (isDisposing)
                {
                    this.ViewModel.IsPropertiesPanelVisible = false;
                    this.ViewModel = null;
                    this.xDockingManager.Layout.ElementAdded -= DockingManager_LayoutElementAdded;
                    this.xDockingManager.Layout.RootPanel.ChildrenTreeChanged -= DockingManager_RootPanelChildrenTreeChanged;
                    
                    //drop all panels.
                    List<IDisposable> panels = this.Panels.Cast<IDisposable>().ToList();
                    this.Panels.Clear();
                    panels.ForEach(p => p.Dispose());

                    //dispose of the docking manager
                    this.xDockingManager.Dispose();
                }

                _isDisposed = true;
            }
        }

        #endregion

        #region Static Helpers

        /// <summary>
        /// Helper to show/hide a specific panel.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="isVisible"></param>
        /// <param name="createPanelAction"></param>
        /// <param name="anchorables"></param>
        private static void ShowHidePanel<T>(Boolean isVisible, Action createPanelAction, IEnumerable<LayoutAnchorable> anchorables)
        {
            LayoutAnchorable panel = anchorables.FirstOrDefault(l => l.Content is T);

            if (isVisible)
            {
                if (panel != null)
                {
                    if (panel.IsHidden)
                    {
                        panel.Show();
                    }
                }
                else
                {
                    createPanelAction.Invoke();
                }
            }
            else
            {
                if (panel != null)
                {
                    if (!panel.IsHidden)
                    {
                        panel.Hide();
                    }
                }

            }

        }

        ///// <summary>
        ///// Loads the given layout item.
        ///// </summary>
        ///// <param name="item"></param>
        ///// <param name="parentPaneCollection"></param>
        ///// <param name="docTabDict"></param>
        ///// <param name="lastDocPane"></param>
        ///// <returns></returns>
        //private static LayoutDocumentPane LoadPlanViewLayoutItem(ViewLayoutItem item,
        //    ObservableCollection<ILayoutDocumentPane> parentPaneCollection,
        //    Dictionary<ViewLayoutItem, LayoutDocument> docTabDict,
        //    LayoutDocumentPane lastDocPane)
        //{
        //    LayoutDocumentPane newLastDocPane = lastDocPane;

        //    if (item.IsDocument)
        //    {
        //        //get the document tab
        //        LayoutDocument documentTab;
        //        if (docTabDict.TryGetValue(item, out documentTab))
        //        {
        //            if (newLastDocPane == null || item.IsNewPane)
        //            {
        //                newLastDocPane = new LayoutDocumentPane();
        //                parentPaneCollection.Add(newLastDocPane);
        //            }

        //            newLastDocPane.Children.Add(documentTab);
        //        }
        //    }
        //    else
        //    {
        //        newLastDocPane = null;

        //        //create and add the group
        //        LayoutDocumentPaneGroup group = new LayoutDocumentPaneGroup();
        //        group.Orientation = (item.IsVertical) ? Orientation.Vertical : Orientation.Horizontal;
        //        parentPaneCollection.Add(group);

        //        foreach (ViewLayoutItem childItem in item.Items.OrderBy(i => i.Order))
        //        {
        //            newLastDocPane = LoadPlanViewLayoutItem(childItem, group.Children, docTabDict, newLastDocPane);
        //        }

        //    }

        //    return newLastDocPane;
        //}

        #endregion
    }
}
