﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion

#region Version History : CCM 830
// V8-32159 : L.Ineson
//  Added LegendItemMouseRightUp event
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Framework.Collections;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Interaction logic for HighlightLegend.xaml
    /// </summary>
    public sealed partial class HighlightLegend : UserControl
    {
        #region Fields

        const Double _mouseAwayOpacity = 0.7;
        const Double _minResizeLength = 100;
        private Canvas _parentCanvas;

        #endregion

        #region Events

        #region CloseRequested
        public static readonly RoutedEvent CloseRequestedEvent =
            EventManager.RegisterRoutedEvent("CloseRequested", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(HighlightLegend));

        public event RoutedEventHandler CloseRequested
        {
            add { AddHandler(CloseRequestedEvent, value); }
            remove { RemoveHandler(CloseRequestedEvent, value); }
        }

        private void OnCloseRequested()
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(HighlightLegend.CloseRequestedEvent));
        }
        #endregion

        #region LegendItemMouseRightUp

        /// <summary>
        /// Event to notify that a legend item has been right clicked.
        /// </summary>
        public static readonly RoutedEvent LegendItemMouseRightUpEvent =
            EventManager.RegisterRoutedEvent("LegendItemMouseRightUp", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(HighlightLegend));

        public event RoutedEventHandler LegendItemMouseRightUp
        {
            add { AddHandler(LegendItemMouseRightUpEvent, value); }
            remove { RemoveHandler(LegendItemMouseRightUpEvent, value); }
        }

        private void OnLegendItemMouseRightUp(PlanogramHighlightResultGroup item)
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(HighlightLegend.LegendItemMouseRightUpEvent, item));
        }

        #endregion

        #endregion

        #region Properties

        #region Highlight Property

        public static readonly DependencyProperty HighlightProperty =
            DependencyProperty.Register("Highlight", typeof(HighlightItem), typeof(HighlightLegend),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the highlight context
        /// </summary>
        public HighlightItem Highlight
        {
            get { return (HighlightItem)GetValue(HighlightProperty); }
            set { SetValue(HighlightProperty, value); }
        }

        #endregion

        #region LegendItems Property

        public static readonly DependencyProperty LegendItemsProperty =
            DependencyProperty.Register("LegendItems", typeof(IList<PlanogramHighlightResultGroup>),
            typeof(HighlightLegend),
            new PropertyMetadata(null, OnLegendItemsPropertyChanged));

        private static void OnLegendItemsPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            HighlightLegend senderControl = (HighlightLegend)obj;

            if (e.OldValue != null)
            {
                var oldCollection = (ReadOnlyBulkObservableCollection<PlanogramHighlightResultGroup>)e.OldValue;
                oldCollection.BulkCollectionChanged -= senderControl.LegendItems_BulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                var newCollection = (ReadOnlyBulkObservableCollection<PlanogramHighlightResultGroup>)e.NewValue;
                newCollection.BulkCollectionChanged += senderControl.LegendItems_BulkCollectionChanged;
            }

            senderControl.UpdateQuadrantColours();
        }

        /// <summary>
        /// Gets/Sets the collection of legend items to display.
        /// </summary>
        public IList<PlanogramHighlightResultGroup> LegendItems
        {
            get { return (IList<PlanogramHighlightResultGroup>)GetValue(LegendItemsProperty); }
            set { SetValue(LegendItemsProperty, value); }
        }

        
        #endregion

        #region Quadrant Groups

        public static readonly DependencyProperty Quadrant1Property =
            DependencyProperty.Register("Quadrant1", typeof(PlanogramHighlightResultGroup), typeof(HighlightLegend));
        public PlanogramHighlightResultGroup Quadrant1
        {
            get { return (PlanogramHighlightResultGroup)GetValue(Quadrant1Property); }
            private set { SetValue(Quadrant1Property, value); }
        }

        public static readonly DependencyProperty Quadrant2Property =
            DependencyProperty.Register("Quadrant2", typeof(PlanogramHighlightResultGroup), typeof(HighlightLegend));
        public PlanogramHighlightResultGroup Quadrant2
        {
            get { return (PlanogramHighlightResultGroup)GetValue(Quadrant2Property); }
            private set { SetValue(Quadrant2Property, value); }
        }


        public static readonly DependencyProperty Quadrant3Property =
           DependencyProperty.Register("Quadrant3", typeof(PlanogramHighlightResultGroup), typeof(HighlightLegend));
        public PlanogramHighlightResultGroup Quadrant3
        {
            get { return (PlanogramHighlightResultGroup)GetValue(Quadrant3Property); }
            private set { SetValue(Quadrant3Property, value); }
        }

        public static readonly DependencyProperty Quadrant4Property =
            DependencyProperty.Register("Quadrant4", typeof(PlanogramHighlightResultGroup), typeof(HighlightLegend));
        public PlanogramHighlightResultGroup Quadrant4
        {
            get { return (PlanogramHighlightResultGroup)GetValue(Quadrant4Property); }
            private set { SetValue(Quadrant4Property, value); }
        }


        #endregion

        #region IsMouseAwayOpaque

        public static readonly DependencyProperty IsMouseAwayOpaqueProperty =
            DependencyProperty.Register("IsMouseAwayOpaque", typeof(Boolean), typeof(HighlightLegend),
            new PropertyMetadata(true));

        public Boolean IsMouseAwayOpaque
        {
            get { return (Boolean)GetValue(IsMouseAwayOpaqueProperty); }
            set { SetValue(IsMouseAwayOpaqueProperty, value); }
        }
        #endregion

        #endregion

        #region Constructor

        public HighlightLegend()
        {
            InitializeComponent();

            this.IsVisibleChanged += This_IsVisibleChanged;

            this.Loaded += new RoutedEventHandler(HighlightLegend_Loaded);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out initial load actions
        /// </summary>
        private void HighlightLegend_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= HighlightLegend_Loaded;

            if (!this.IsMouseOver && this.IsMouseAwayOpaque)
            {
                this.Opacity = _mouseAwayOpacity;
            }
        }

        /// <summary>
        /// Called whenever the visibility of this control changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void This_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            ConstrainPosition();
        }

        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            this.Opacity = 1;
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);

            if (this.IsMouseAwayOpaque)
            {
                this.Opacity = _mouseAwayOpacity;
            }
        }

        /// <summary>
        /// Called when the legend items collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LegendItems_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            UpdateQuadrantColours();
        }

        /// <summary>
        /// Called when the mouse away opacity button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MouseAwayOpacityButton_Click(object sender, RoutedEventArgs e)
        {
            this.IsMouseAwayOpaque = !this.IsMouseAwayOpaque;

            Button senderControl = (Button)sender;
            ((Rectangle)senderControl.Content).Fill = (this.IsMouseAwayOpaque) ? Brushes.Transparent : Brushes.WhiteSmoke;
        }

        /// <summary>
        /// Called when the close button gets clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            OnCloseRequested();
        }

        #region Thumb Handlers

        /// <summary>
        /// Called when a thumb starts dragging.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Thumb_DragStarted(object sender, System.Windows.Controls.Primitives.DragStartedEventArgs e)
        {
            _parentCanvas = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<Canvas>(this);
        }

        /// <summary>
        /// Called when the thumb finishes dragging
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Thumb_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            _parentCanvas = null;
        }

        /// <summary>
        /// Called when the title bar is dragging
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xTitleBar_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            Double curLeft = Canvas.GetLeft(this);
            Double curTop = Canvas.GetTop(this);

            //get the parent Canvas.
            Double maxLeft = Double.PositiveInfinity;
            Double maxTop = Double.PositiveInfinity;
            if (_parentCanvas != null)
            {
                maxLeft = _parentCanvas.ActualWidth;
                maxTop = _parentCanvas.ActualHeight;
            }


            Double newLeft = curLeft + e.HorizontalChange;
            if (newLeft < 0)
            {
                newLeft = 0;
            }
            else if ((newLeft + this.ActualWidth) > maxLeft)
            {
                newLeft = maxLeft - this.ActualWidth;
            }


            Double newTop = curTop + e.VerticalChange;
            if (newTop < 0)
            {
                newTop = 0;
            }
            else if ((newTop + this.ActualHeight) > maxTop)
            {
                newTop = maxTop - this.ActualHeight;
            }



            //apply
            Canvas.SetLeft(this, newLeft);
            Canvas.SetTop(this, newTop);

        }

        /// <summary>
        /// Called when the right resize thumb is dragging
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xRightResizeThumb_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            this.Width = Math.Max(_minResizeLength, this.DesiredSize.Width + e.HorizontalChange);
        }

        /// <summary>
        /// Called when the bottom resize thumb is dragging
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xBottomResizeThumb_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            this.Height = Math.Max(_minResizeLength, this.DesiredSize.Height + e.VerticalChange);
        }

        /// <summary>
        /// Called when the bottom corner resize thumb is dragging
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xBottomCornerThumb_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            this.Width = Math.Max(_minResizeLength, this.DesiredSize.Width + e.HorizontalChange);
            this.Height = Math.Max(_minResizeLength, this.DesiredSize.Height + e.VerticalChange);
        }

        #endregion

        /// <summary>
        /// Called when the user right clicks on a legend item.
        /// </summary>
        private void LegendItemsControl_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            StackPanel itemContainer = ((DependencyObject)e.OriginalSource).FindVisualAncestor<StackPanel>();
            if (itemContainer == null) return;

            PlanogramHighlightResultGroup item = itemContainer.DataContext as PlanogramHighlightResultGroup;
            if (item == null) return;

            OnLegendItemMouseRightUp(item);
        }

        /// <summary>
        /// Called when the user right clicks on a quadrant legend item.
        /// </summary>
        private void Quadrant_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            PlanogramHighlightResultGroup item = ((FrameworkElement)e.OriginalSource).DataContext as PlanogramHighlightResultGroup;
            if (item == null) return;

            OnLegendItemMouseRightUp(item);
        }

        #endregion

        #region Method

        /// <summary>
        /// Checks if this sits within its parent canvas otherwise it is moved
        /// </summary>
        public void ConstrainPosition()
        {
            //only bother if this is actually visible.
            if (this.IsVisible)
            {
                Canvas parent = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<Canvas>(this);
                if (parent != null)
                {
                    Rect parentBounds = new Rect(0, 0, parent.ActualWidth, parent.ActualHeight);
                    Rect thisBounds = new Rect(Canvas.GetLeft(this), Canvas.GetTop(this), this.Width, this.Height);

                    if (!parentBounds.Contains(thisBounds))
                    {
                        if (thisBounds.Left < 0)
                        {
                            Canvas.SetLeft(this, 0);
                        }
                        if (thisBounds.Right > parentBounds.Right)
                        {
                            Canvas.SetLeft(this, parentBounds.Right - this.Width);
                        }
                        if (thisBounds.Top < 0)
                        {
                            Canvas.SetTop(this, 0);
                        }
                        if (thisBounds.Bottom > parentBounds.Bottom)
                        {
                            Canvas.SetTop(this, parentBounds.Bottom - this.Height);
                        }
                        thisBounds = new Rect(Canvas.GetLeft(this), Canvas.GetTop(this), this.Width, this.Height);
                    }
                }
            }
        }

        /// <summary>
        /// Updates the values of the quadrant colour properties
        /// </summary>
        private void UpdateQuadrantColours()
        {
            if (this.LegendItems != null
                    && this.LegendItems.Count >= 4)
            {
                this.Quadrant1 = this.LegendItems[0];
                this.Quadrant2 = this.LegendItems[1];
                this.Quadrant3 = this.LegendItems[2];
                this.Quadrant4 = this.LegendItems[3];
            }
            else
            {
                this.Quadrant1 = null;
                this.Quadrant2 = null;
                this.Quadrant3 = null;
                this.Quadrant4 = null;
            }
        }

        #endregion

        


        

    }
}
