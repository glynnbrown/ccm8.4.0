﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using System.Collections.ObjectModel;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.LocationBuddy
{
    /// <summary>
    /// Interaction logic for AssortmentLocationBuddyReviewWindow.xaml
    /// </summary>
    public partial class AssortmentLocationBuddyReviewWindow : ExtendedRibbonWindow
    {

        #region Fields
        //none
        #endregion

        #region Properties

        #region ViewModel

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentLocationBuddyReviewViewModel), typeof(AssortmentLocationBuddyReviewWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets/Sets the viewmodel controller for this screen
        /// </summary>
        public AssortmentLocationBuddyReviewViewModel ViewModel
        {
            get { return (AssortmentLocationBuddyReviewViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentLocationBuddyReviewWindow senderControl = (AssortmentLocationBuddyReviewWindow)obj;

            String RemoveLocationBuddyCommandKey = "RemoveLocationBuddyCommand";

            if (e.OldValue != null)
            {
                AssortmentLocationBuddyReviewViewModel oldModel = (AssortmentLocationBuddyReviewViewModel)e.OldValue;
                senderControl.Resources.Remove(RemoveLocationBuddyCommandKey);
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                AssortmentLocationBuddyReviewViewModel newModel = (AssortmentLocationBuddyReviewViewModel)e.NewValue;
                senderControl.Resources.Add(RemoveLocationBuddyCommandKey, newModel.RemoveLocationBuddyCommand);
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public AssortmentLocationBuddyReviewWindow(PlanogramAssortment currentAssortment)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.ViewModel = new AssortmentLocationBuddyReviewViewModel(currentAssortment);

            this.Loaded += AssortmentLocationBuddyReviewWindow_Loaded;
        }

        /// <summary>
        /// Carries out initial load actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssortmentLocationBuddyReviewWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= AssortmentLocationBuddyReviewWindow_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers
        //none
        #endregion

        #region Window close

        /// <summary>
        /// Method to override the on closed method
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {
                   IDisposable disposableViewModel = this.ViewModel;
                   this.ViewModel = null;

                   if (disposableViewModel != null)
                   {
                       disposableViewModel.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion

    }
}

