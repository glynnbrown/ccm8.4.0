﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
// V8-32396 : A.Probyn
//  Updated dispose and ensured location selection can be from anywhere in the entity
// CCM-18646 : D.Pleasance
//  Amended constructor to not default first location buddy item as selected row. 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows;

using Galleria.Ccm.Model;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Csla.Core;

using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Collections;
using System.Collections.Specialized;
using System.Windows.Controls;
using System.ComponentModel;

using Csla;
using System.Text.RegularExpressions;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.LocationBuddy
{
    public sealed class AssortmentLocationBuddyReviewViewModel : ViewModelAttachedControlObject<AssortmentLocationBuddyReviewWindow>, IDataErrorInfo
    {

        #region Fields

        private PlanogramAssortment _currentAssortment;
        private BulkObservableCollection<AssortmentLocationBuddyView> _locationBuddies = new BulkObservableCollection<AssortmentLocationBuddyView>();
        private ReadOnlyBulkObservableCollection<AssortmentLocationBuddyView> _locationBuddiesRO;
        private BulkObservableCollection<LocationInfo> _locationsToRemoveQue = new BulkObservableCollection<LocationInfo>();
        private ObservableCollection<AssortmentLocationBuddyView> _selectedAssortmentLocationBuddyViews = new ObservableCollection<AssortmentLocationBuddyView>();
        private BulkObservableCollection<LocationInfo> _availableSourceLocations = new BulkObservableCollection<LocationInfo>();
        private ReadOnlyBulkObservableCollection<LocationInfo> _availableSourceLocationsRO;
        private PlanogramAssortmentLocationBuddyTreatmentType _editTreatmentType;
        private Single? _editSource1Percentage;
        private Single? _editSource2Percentage;
        private Single? _editSource3Percentage;
        private Single? _editSource4Percentage;
        private Single? _editSource5Percentage;
        private LocationInfo _editLocation1;
        private LocationInfo _editLocation2;
        private LocationInfo _editLocation3;
        private LocationInfo _editLocation4;
        private LocationInfo _editLocation5;
        private Boolean _editControlsEnabled;
        private Boolean _isDirty;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath LocationBuddiesProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.LocationBuddies);
        public static readonly PropertyPath EditTreatmentTypeProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.EditTreatmentType);
        public static readonly PropertyPath SelectedAssortmentLocationBuddyViewsProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.SelectedAssortmentLocationBuddyViews);
        public static readonly PropertyPath EditSource1PercentageProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.EditSource1Percentage);
        public static readonly PropertyPath EditSource2PercentageProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.EditSource2Percentage);
        public static readonly PropertyPath EditSource3PercentageProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.EditSource3Percentage);
        public static readonly PropertyPath EditSource4PercentageProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.EditSource4Percentage);
        public static readonly PropertyPath EditSource5PercentageProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.EditSource5Percentage);
        public static readonly PropertyPath EditLocation1Property = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.EditLocation1);
        public static readonly PropertyPath EditLocation2Property = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.EditLocation2);
        public static readonly PropertyPath EditLocation3Property = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.EditLocation3);
        public static readonly PropertyPath EditLocation4Property = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.EditLocation4);
        public static readonly PropertyPath EditLocation5Property = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.EditLocation5);
        public static readonly PropertyPath AvailableSourceLocationsProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.AvailableSourceLocations);
        public static readonly PropertyPath EditControlsEnabledProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.EditControlsEnabled);

        //Commands
        public static readonly PropertyPath ApplyCommandProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.ApplyCommand);
        public static readonly PropertyPath ApplyAndCloseCommandProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.ApplyAndCloseCommand);
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.CloseCommand);
        public static readonly PropertyPath RemoveLocationBuddyCommandProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.RemoveLocationBuddyCommand);
        public static readonly PropertyPath AddNewBuddiesCommandProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.AddNewBuddiesCommand);
        public static readonly PropertyPath AddNewLocationCommandProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyReviewViewModel>(p => p.AddNewLocationCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Get the AssortmentLocationBuddyViews
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssortmentLocationBuddyView> LocationBuddies
        {
            get
            {
                if (_locationBuddiesRO == null)
                {
                    if (_locationBuddies != null)
                    {
                        _locationBuddiesRO = new ReadOnlyBulkObservableCollection<AssortmentLocationBuddyView>(_locationBuddies);
                    }
                }
                return _locationBuddiesRO;
            }
        }
        
        /// <summary>
        /// Gets/Sets the treatment type of the selected rows.
        /// </summary>
        public PlanogramAssortmentLocationBuddyTreatmentType EditTreatmentType
        {
            get { return _editTreatmentType; }
            set
            {
                if (value != null)
                {
                    foreach (AssortmentLocationBuddyView viewToEdit in SelectedAssortmentLocationBuddyViews)
                    {
                        viewToEdit.TreatmentType = value;
                    }
                    _editTreatmentType = value;
                }
                OnPropertyChanged(EditTreatmentTypeProperty);
            }
        }

        /// <summary>
        /// Returns the editable collection of selected buddy views.
        /// </summary>
        public ObservableCollection<AssortmentLocationBuddyView> SelectedAssortmentLocationBuddyViews
        {
            get { return _selectedAssortmentLocationBuddyViews; }
        }

        /// <summary>
        /// Gets/Sets the percentage of source 1.
        /// </summary>
        public Single? EditSource1Percentage
        {
            get { return _editSource1Percentage; }
            set
            {
                _editSource1Percentage = value;

                foreach (AssortmentLocationBuddyView viewToEdit in SelectedAssortmentLocationBuddyViews)
                {
                    viewToEdit.Source1Percentage = value;
                }
                OnPropertyChanged(EditSource1PercentageProperty);
            }
        }


        /// <summary>
        /// Gets/Sets the percentage of source 2.
        /// </summary>
        public Single? EditSource2Percentage
        {
            get { return _editSource2Percentage; }
            set
            {
                _editSource2Percentage = value;

                foreach (AssortmentLocationBuddyView viewToEdit in SelectedAssortmentLocationBuddyViews)
                {
                    viewToEdit.Source2Percentage = value;
                }
                OnPropertyChanged(EditSource2PercentageProperty);
            }
        }


        /// <summary>
        /// Gets/Sets the percentage of source 3.
        /// </summary>
        public Single? EditSource3Percentage
        {
            get { return _editSource3Percentage; }
            set
            {
                _editSource3Percentage = value;

                foreach (AssortmentLocationBuddyView viewToEdit in SelectedAssortmentLocationBuddyViews)
                {
                    viewToEdit.Source3Percentage = value;
                }
                OnPropertyChanged(EditSource3PercentageProperty);
            }
        }


        /// <summary>
        /// Gets/Sets the percentage of source 4.
        /// </summary>
        public Single? EditSource4Percentage
        {
            get { return _editSource4Percentage; }
            set
            {
                _editSource4Percentage = value;

                foreach (AssortmentLocationBuddyView viewToEdit in SelectedAssortmentLocationBuddyViews)
                {
                    viewToEdit.Source4Percentage = value;
                }
                OnPropertyChanged(EditSource4PercentageProperty);
            }
        }


        /// <summary>
        /// Gets/Sets the Percenatage of source 5.
        /// </summary>
        public Single? EditSource5Percentage
        {
            get { return _editSource5Percentage; }
            set
            {
                _editSource5Percentage = value;

                foreach (AssortmentLocationBuddyView viewToEdit in SelectedAssortmentLocationBuddyViews)
                {
                    viewToEdit.Source5Percentage = value;
                }
                OnPropertyChanged(EditSource5PercentageProperty);
            }
        }


        /// <summary>
        /// Gets/Sets the Location source 1
        /// </summary>
        public LocationInfo EditLocation1
        {
            get { return _editLocation1; }
            set
            {
                _editLocation1 = value;

                foreach (AssortmentLocationBuddyView viewToEdit in SelectedAssortmentLocationBuddyViews)
                {
                    viewToEdit.Source1 = _editLocation1;
                    viewToEdit.SourceBuddy.S1LocationCode = _editLocation1.Code;
                }
                OnPropertyChanged(EditLocation1Property);
            }
        }


        /// <summary>
        /// Gets/Sets the Location source 2
        /// </summary>
        public LocationInfo EditLocation2
        {
            get { return _editLocation2; }
            set
            {
                _editLocation2 = value;

                foreach (AssortmentLocationBuddyView viewToEdit in SelectedAssortmentLocationBuddyViews)
                {
                    viewToEdit.Source2 = _editLocation2;
                    viewToEdit.SourceBuddy.S2LocationCode = _editLocation2.Code;
                }
                OnPropertyChanged(EditLocation2Property);
            }
        }


        /// <summary>
        /// Gets/Sets the Location source 3
        /// </summary>
        public LocationInfo EditLocation3
        {
            get { return _editLocation3; }
            set
            {
                _editLocation3 = value;

                foreach (AssortmentLocationBuddyView viewToEdit in SelectedAssortmentLocationBuddyViews)
                {
                    viewToEdit.Source3 = _editLocation3;
                    viewToEdit.SourceBuddy.S3LocationCode = _editLocation3.Code;
                }
                OnPropertyChanged(EditLocation3Property);
            }
        }


        /// <summary>
        /// Gets/Sets the Location source 4
        /// </summary>
        public LocationInfo EditLocation4
        {
            get { return _editLocation4; }
            set
            {
                _editLocation4 = value;

                foreach (AssortmentLocationBuddyView viewToEdit in SelectedAssortmentLocationBuddyViews)
                {
                    viewToEdit.Source4 = _editLocation4;
                    viewToEdit.SourceBuddy.S4LocationCode = _editLocation4.Code;
                }
                OnPropertyChanged(EditLocation4Property);
            }
        }


        /// <summary>
        /// Gets/Sets the Location source 5
        /// </summary>
        public LocationInfo EditLocation5
        {
            get { return _editLocation5; }
            set
            {
                _editLocation5 = value;

                foreach (AssortmentLocationBuddyView viewToEdit in SelectedAssortmentLocationBuddyViews)
                {
                    viewToEdit.Source5 = _editLocation5;
                    viewToEdit.SourceBuddy.S5LocationCode = _editLocation5.Code;
                }
                OnPropertyChanged(EditLocation5Property);
            }
        }


        /// <summary>
        /// Gets/Sets the available source locations
        /// </summary>
        public ReadOnlyBulkObservableCollection<LocationInfo> AvailableSourceLocations
        {
            get
            {
                if (_availableSourceLocationsRO == null)
                {
                    _availableSourceLocationsRO = new ReadOnlyBulkObservableCollection<LocationInfo>(_availableSourceLocations);
                }
                return _availableSourceLocationsRO;
            }
        }


        /// <summary>
        /// Gets/Sets the EditControls available property.
        /// </summary>
        public Boolean EditControlsEnabled
        {
            get
            {
                return _editControlsEnabled;
            }
            set
            {
                _editControlsEnabled = value;
                OnPropertyChanged(EditControlsEnabledProperty);
            }
        }

        /// <summary>
        /// Gets/Sets if IsDirty
        /// </summary>
        public Boolean IsDirty
        {
            get
            {
                return _isDirty;
            }
            set
            {
                _isDirty = value;
                OnPropertyChanged(EditControlsEnabledProperty);
            }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor - creates a new instance of this type.
        /// </summary>
        public AssortmentLocationBuddyReviewViewModel(PlanogramAssortment currentAssortment)
        {
            _currentAssortment = currentAssortment;

            //initialise location lists
            ReloadAvailableLocations();
            
            //Attach event managers
            _selectedAssortmentLocationBuddyViews.CollectionChanged += SelectedLocationBuddyRows_CollectionChanged;

            ReloadLocationBuddies();            
        }

        #endregion

        #region Commands

        #region Apply

        private RelayCommand _applyCommand;
        /// <summary>
        /// Accepts changes and closes the window
        /// </summary>
        public RelayCommand ApplyCommand
        {
            get
            {
                //Lazy load
                if (_applyCommand == null)
                {
                    _applyCommand = new RelayCommand
                        (
                            p => Apply_Executed(),
                            p => Apply_CanExecute()
                        )
                    {
                        FriendlyName = Message.Generic_Apply,
                        DisabledReason = Message.Generic_Ok_DisabledReasonNoChanges
                    };

                    base.ViewModelCommands.Add(_applyCommand);
                }
                return _applyCommand;
            }

        }

        private bool Apply_CanExecute()
        {
            //all rows must be valid
            if (!this.LocationBuddies.All(r => r.SourceBuddy.IsValid))
            {
                return false;
            }

            //If we have manually flagged save required then:
            if (IsDirty == true)
            {
                return true;
            }
            //At least one row must be dirty
            if (this.LocationBuddies.Any(f => f.IsDirty))
            {
                return true;
            }


            return false;
        }


        private void Apply_Executed()
        {
            foreach (AssortmentLocationBuddyView currentAssortmentLocationBuddyView in LocationBuddies)
            {
                if (currentAssortmentLocationBuddyView.IsDirty || IsDirty == true)
                {
                    currentAssortmentLocationBuddyView.CommitChanges();
                }
            }

            List<PlanogramAssortmentLocationBuddy> toRemove = new List<PlanogramAssortmentLocationBuddy>();

            foreach (PlanogramAssortmentLocationBuddy buddy in _currentAssortment.LocationBuddies)
            {
                Boolean buddyRemoved = true;
                foreach (AssortmentLocationBuddyView buddyView in _locationBuddies)
                {
                    if (Object.Equals(buddyView.SourceBuddy.LocationCode, buddy.LocationCode))
                    {
                        buddyRemoved = false;
                    }
                }
                if (buddyRemoved)
                {
                    //If a buddy exists in the buddylist but not the review screen, queue it for removal
                    toRemove.Add(buddy);
                }
            }

            foreach (PlanogramAssortmentLocationBuddy buddy in toRemove)
            {
                //Remove deleted buddy views from the LocationBuddys
                _currentAssortment.LocationBuddies.Remove(buddy);
            }

            IsDirty = false;

        }

        #endregion

        #region Apply and Close

        private RelayCommand _applyAndCloseCommand;
        /// <summary>
        /// Accepts changes and closes the window
        /// </summary>
        public RelayCommand ApplyAndCloseCommand
        {
            get
            {
                //Lazy load
                if (_applyAndCloseCommand == null)
                {
                    _applyAndCloseCommand = new RelayCommand
                        (
                            p => ApplyAndClose_Executed(),
                            p => ApplyAndClose_CanExecute()
                        )
                    {
                        FriendlyName = Message.Generic_ApplyAndClose,
                        DisabledReason = Message.Generic_Ok_DisabledReasonNoChanges
                    };

                    base.ViewModelCommands.Add(_applyAndCloseCommand);
                }
                return _applyAndCloseCommand;
            }

        }

        private bool ApplyAndClose_CanExecute()
        {
            //All rows must be valid
            if (!this.LocationBuddies.All(r => r.SourceBuddy.IsValid))
            {
                return false;
            }

            //If we have manually flagged save required then:
            if (IsDirty == true)
            {
                return true;
            }
            //At least one row must be dirty
            if (this.LocationBuddies.Any(f => f.IsDirty))
            {
                return true;
            }

            return false;
        }


        private void ApplyAndClose_Executed()
        {
            //call the apply excecuted command and reset IsDirty
            Apply_Executed();
            IsDirty = false;

            //close window
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region Close

        private RelayCommand _closeCommand;

        /// <summary>
        /// Discards changes and closes the window
        /// </summary>.
        public RelayCommand CloseCommand
        {
            get
            {
                //Lazy load
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand
                        (
                            p => Close_Executed()
                        )
                    {
                        FriendlyName = Message.Generic_Close,
                    };

                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }

        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region RemoveLocationBuddy

        private RelayCommand _removeLocationBuddyCommand;

        /// <summary>
        /// Removes a location buddy from the scenario.
        /// </summary>.
        public RelayCommand RemoveLocationBuddyCommand
        {
            get
            {
                //Lazy load
                if (_removeLocationBuddyCommand == null)
                {
                    _removeLocationBuddyCommand = new RelayCommand
                        (
                            p => RemoveLocationBuddy_Executed(p),
                            p => RemoveLocationBuddy_CanExecute(p)
                        )
                    {
                        FriendlyName = Message.Generic_Delete,
                        SmallIcon = ImageResources.Delete_16
                    };

                    base.ViewModelCommands.Add(_removeLocationBuddyCommand);
                }
                return _removeLocationBuddyCommand;
            }
        }


        private Boolean RemoveLocationBuddy_CanExecute(Object arg)
        {
            AssortmentLocationBuddyView toRemove = arg as AssortmentLocationBuddyView;

            if (toRemove != null)
            {
                return true;
            }

            return false;
        }

        private void RemoveLocationBuddy_Executed(Object arg)
        {
            AssortmentLocationBuddyView toRemove = arg as AssortmentLocationBuddyView;
            if (toRemove != null)
            {
                //Remove location buddy row
                _locationBuddies.Remove(toRemove);

                IsDirty = true;
            }
        }

        #endregion

        #region AddNewBuddies

        private RelayCommand _addNewBuddiesCommand;

        /// <summary>
        /// Adds new buddies with the specified criteria
        /// </summary>
        public RelayCommand AddNewBuddiesCommand
        {
            get
            {
                if (_addNewBuddiesCommand == null)
                {
                    _addNewBuddiesCommand = new RelayCommand(
                        p => AddNewBuddies_Executed())
                    {
                        FriendlyName = Message.AssortmentLocationBuddy_AddBuddies,
                        SmallIcon = ImageResources.Add_16
                    };
                    base.ViewModelCommands.Add(_addNewBuddiesCommand);
                }
                return _addNewBuddiesCommand;
            }
        }


        private void AddNewBuddies_Executed()
        {
            //Check wether editing has been made on the current location
            Boolean _requestSave = false;
            foreach (AssortmentLocationBuddyView currentAssortmentLocationBuddyView in LocationBuddies)
            {
                if (currentAssortmentLocationBuddyView.IsDirty)
                {
                    _requestSave = true;
                }
            }

            if (IsDirty)
            {
                _requestSave = true;
            }

            //If location buddy sources have been added then we need to confirm save
            if (_requestSave == true)
            {
                ModalMessage ShouldOverwritePrompt = new ModalMessage();
                ShouldOverwritePrompt.Title = Message.AssortmentLocationBuddyReview_SaveEdits_Title;
                ShouldOverwritePrompt.MessageIcon = ImageResources.Dialog_Information;
                ShouldOverwritePrompt.Header = Message.AssortmentLocationBuddyReview_SaveEdits_Header;
                ShouldOverwritePrompt.Description = Message.AssortmentLocationBuddyReview_SaveEdits_Description;
                ShouldOverwritePrompt.ButtonCount = 2;
                ShouldOverwritePrompt.Button1Content = Message.Generic_Apply;
                ShouldOverwritePrompt.Button2Content = Message.Generic_Cancel;
                ShouldOverwritePrompt.WindowStartupLocation = WindowStartupLocation.CenterScreen;

                ShouldOverwritePrompt.ShowDialog();

                if (ShouldOverwritePrompt.Result == ModalMessageResult.Button1)
                {
                    //We need to save then show selection window when user agrees
                    Apply_Executed();
                    IsDirty = false;

                    //Show the selection window
                    if (this.AttachedControl != null)
                    {
                        AssortmentLocationBuddyAddNewWindow win = new AssortmentLocationBuddyAddNewWindow(_currentAssortment);
                        App.ShowWindow(win, this.AttachedControl, /*isModal*/true);

                        ReloadLocationBuddies();

                        OnPropertyChanged(SelectedAssortmentLocationBuddyViewsProperty);
                        SetEditSectionValues();

                        IsDirty = win.ViewModel.IsDirty;
                    }
                }
            }
            else
            {
                //No edits made so show the selection window
                if (this.AttachedControl != null)
                {
                    AssortmentLocationBuddyAddNewWindow win = new AssortmentLocationBuddyAddNewWindow(this._currentAssortment);
                    App.ShowWindow(win, this.AttachedControl, /*isModal*/true);

                    ReloadLocationBuddies();
                    SetEditSectionValues();
                    IsDirty = win.ViewModel.IsDirty;
                }
            }

        }

        #endregion

        #region AddNewLocation

        private RelayCommand _addNewLocationCommand;

        /// <summary>
        /// Adds new buddies with the specified criteria
        /// </summary>
        public RelayCommand AddNewLocationCommand
        {
            get
            {
                if (_addNewLocationCommand == null)
                {
                    _addNewLocationCommand = new RelayCommand(
                        p => AddNewLocationCommand_Excecuted(p),
                        p => AddNewLocationCommand_CanExecute(p)
                        )
                    {
                    };
                    base.ViewModelCommands.Add(_addNewLocationCommand);
                }
                return _addNewLocationCommand;
            }
        }

        private bool AddNewLocationCommand_CanExecute(object locationToModify)
        {
            if (SelectedAssortmentLocationBuddyViews.Count > 0)
            {
                AssortmentLocationBuddyView locationBuddyToAssessOn = SelectedAssortmentLocationBuddyViews.First();

                switch (Convert.ToInt16(locationToModify))
                {
                    case 1:
                        //Will always want to have source 1 acessible
                        return true;
                    case 2:
                        if (locationBuddyToAssessOn.Source1 != null)
                        {
                            return true;
                        }
                        break;
                    case 3:
                        if (locationBuddyToAssessOn.Source2 != null)
                        {
                            return true;
                        }
                        break;
                    case 4:
                        if (locationBuddyToAssessOn.Source3 != null)
                        {
                            return true;
                        }
                        break;
                    case 5:
                        if (locationBuddyToAssessOn.Source4 != null)
                        {
                            return true;
                        }
                        break;
                }
            }

            return false;
        }


        private void AddNewLocationCommand_Excecuted(object locationToModify)
        {
            //We need to check which locations cannot be added
            RefreshLocationsToRemove();

            //Show the selection window
            if (this.AttachedControl != null)
            {
                //create the columnset
                DataGridColumnCollection columnSet = new DataGridColumnCollection();
                columnSet.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(LocationInfo.CodeProperty.FriendlyName, "Code", HorizontalAlignment.Left));
                columnSet.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(LocationInfo.NameProperty.FriendlyName, "Name", HorizontalAlignment.Left));
                columnSet[0].Width = DataGridLength.Auto;
                columnSet[1].Width = DataGridLength.SizeToCells;

                GenericSingleItemSelectorViewModel viewModel = new GenericSingleItemSelectorViewModel("Select Field")
                {
                    ItemSource = _availableSourceLocations.Except(_locationsToRemoveQue).ToList(),
                    SelectionMode = DataGridSelectionMode.Single,
                    ColumnSet = columnSet,
                    ExtendColumn = 1
                };

                CommonHelper.GetWindowService().ShowDialog<GenericSingleItemSelectorWindow>(viewModel);

                if (viewModel.DialogResult == true)
                {
                    switch (Convert.ToInt16(locationToModify))
                    {
                        case 1:
                            EditLocation1 = viewModel.SelectedItems.Cast<LocationInfo>().First();
                            if (EditSource1Percentage == null)
                            {
                                EditSource1Percentage = 1;
                            }
                            break;
                        case 2:
                            EditLocation2 = viewModel.SelectedItems.Cast<LocationInfo>().First();
                            if (EditSource2Percentage == null)
                            {
                                EditSource2Percentage = 1;
                            }
                            break;
                        case 3:
                            EditLocation3 = viewModel.SelectedItems.Cast<LocationInfo>().First();
                            if (EditSource3Percentage == null)
                            {
                                EditSource3Percentage = 1;
                            }
                            break;
                        case 4:
                            EditLocation4 = viewModel.SelectedItems.Cast<LocationInfo>().First();
                            if (EditSource4Percentage == null)
                            {
                                EditSource4Percentage = 1;
                            }
                            break;
                        case 5:
                            EditLocation5 = viewModel.SelectedItems.Cast<LocationInfo>().First();
                            if (EditSource5Percentage == null)
                            {
                                EditSource5Percentage = 1;
                            }
                            break;
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Reloads the LocationBuddies collection within CurrentAssortment
        /// </summary>
        private void ReloadLocationBuddies()
        {
            if (_locationBuddies.Count > 0)
            {
                _locationBuddies.Clear();
            }

            foreach (PlanogramAssortmentLocationBuddy locationBuddy in _currentAssortment.LocationBuddies)
            {
                _locationBuddies.Add(new AssortmentLocationBuddyView(locationBuddy, _currentAssortment, _availableSourceLocations));
            }
        }

        /// <summary>
        /// //Refreshes the collection of previously used locations
        /// </summary>
        private void RefreshLocationsToRemove()
        {

            //Ensure the que is empty
            if (_locationsToRemoveQue != null)
            {
                _locationsToRemoveQue.Clear();
            }

            foreach (AssortmentLocationBuddyView buddyView in _selectedAssortmentLocationBuddyViews)
            {
                foreach (LocationInfo scenLoc in _availableSourceLocations)
                {
                    //TargetLocation
                    if (buddyView.LocationCode.Equals(scenLoc.Code))
                    {
                        if (!_locationsToRemoveQue.Contains(scenLoc))
                        {
                            _locationsToRemoveQue.Add(scenLoc);
                        }
                    }
                    //Source 1
                    if (!String.IsNullOrEmpty(buddyView.Source1LocationCode))
                    {
                        if (buddyView.Source1LocationCode.Equals(scenLoc.Code))
                        {
                            if (!_locationsToRemoveQue.Contains(scenLoc))
                            {
                                _locationsToRemoveQue.Add(scenLoc);
                            }
                        }
                    }
                    //Source2
                    if (!String.IsNullOrEmpty(buddyView.Source2LocationCode))
                    {
                        if (buddyView.Source2LocationCode.Equals(scenLoc.Code))
                        {
                            if (!_locationsToRemoveQue.Contains(scenLoc))
                            {
                                _locationsToRemoveQue.Add(scenLoc);
                            }
                        }
                    }
                    //Source3
                    if (!String.IsNullOrEmpty(buddyView.Source3LocationCode))
                    {
                        if (buddyView.Source3LocationCode.Equals(scenLoc.Code))
                        {
                            if (!_locationsToRemoveQue.Contains(scenLoc))
                            {
                                _locationsToRemoveQue.Add(scenLoc);
                            }
                        }
                    }
                    //Source4
                    if (!String.IsNullOrEmpty(buddyView.Source4LocationCode))
                    {
                        if (buddyView.Source4LocationCode.Equals(scenLoc.Code))
                        {
                            if (!_locationsToRemoveQue.Contains(scenLoc))
                            {
                                _locationsToRemoveQue.Add(scenLoc);
                            }
                        }
                    }
                    //Source5
                    if (!String.IsNullOrEmpty(buddyView.Source5LocationCode))
                    {
                        if (buddyView.Source5LocationCode.Equals(scenLoc.Code))
                        {
                            if (!_locationsToRemoveQue.Contains(scenLoc))
                            {
                                _locationsToRemoveQue.Add(scenLoc);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Assigns values to the appropiate areas within the edit section based on currently selected datagrid items.
        /// </summary>
        private void SetEditSectionValues()
        {
            AssortmentLocationBuddyView firstView = null;

            if (_selectedAssortmentLocationBuddyViews.Count > 0)
            {
                EditControlsEnabled = true;
                firstView = SelectedAssortmentLocationBuddyViews.First();
            }
            else
            {
                EditControlsEnabled = false;
            }

            //everytime selection changes we want to update edit section values:
            if (SelectedAssortmentLocationBuddyViews.Count > 0)
            {
                PlanogramAssortmentLocationBuddyTreatmentType _tmpTreatmentType = firstView.TreatmentType;
                Single? _tmpSourcePercentage = null;

                //Treatment type
                _editTreatmentType = PlanogramAssortmentLocationBuddyTreatmentType.Avg;
                if (SelectedAssortmentLocationBuddyViews.All(r => r.TreatmentType == _tmpTreatmentType))
                {
                    _editTreatmentType = _tmpTreatmentType;
                }
                else
                {
                    _editTreatmentType = PlanogramAssortmentLocationBuddyTreatmentType.Avg;
                }
                OnPropertyChanged(EditTreatmentTypeProperty);


                //Source 1 Percentage
                _tmpSourcePercentage = firstView.Source1Percentage;
                _editSource1Percentage = null;

                if (SelectedAssortmentLocationBuddyViews.All(r => r.Source1Percentage == _tmpSourcePercentage))
                {
                    if (_tmpSourcePercentage != null)
                    {
                        _editSource1Percentage = _tmpSourcePercentage;
                    }
                }
                else
                {
                    _editSource1Percentage = null;
                }
                OnPropertyChanged(EditSource1PercentageProperty);


                //Source 2 Percentage
                _tmpSourcePercentage = firstView.Source2Percentage;
                _editSource2Percentage = null;

                if (SelectedAssortmentLocationBuddyViews.All(r => r.Source2Percentage == _tmpSourcePercentage))
                {
                    if (_tmpSourcePercentage != null)
                    {
                        _editSource2Percentage = _tmpSourcePercentage;
                    }
                }
                else
                {
                    _editSource2Percentage = null;
                }
                OnPropertyChanged(EditSource2PercentageProperty);


                //Source 3 Percentage
                _tmpSourcePercentage = firstView.Source3Percentage;
                _editSource3Percentage = null;
                if (SelectedAssortmentLocationBuddyViews.All(r => r.Source3Percentage == _tmpSourcePercentage))
                {
                    if (_tmpSourcePercentage != null)
                    {
                        _editSource3Percentage = _tmpSourcePercentage;
                    }
                }
                else
                {
                    _editSource3Percentage = null;
                }
                OnPropertyChanged(EditSource3PercentageProperty);


                //Source 4 Percentage
                _tmpSourcePercentage = firstView.Source4Percentage;
                _editSource4Percentage = null;

                if (SelectedAssortmentLocationBuddyViews.All(r => r.Source4Percentage == _tmpSourcePercentage))
                {

                    if (_tmpSourcePercentage != null)
                    {
                        _editSource4Percentage = _tmpSourcePercentage;
                    }
                }
                else
                {
                    _editSource4Percentage = null;
                }
                OnPropertyChanged(EditSource4PercentageProperty);

                //Source 5 Percentage
                _tmpSourcePercentage = firstView.Source5Percentage;
                _editSource5Percentage = null;

                if (SelectedAssortmentLocationBuddyViews.All(r => r.Source5Percentage == _tmpSourcePercentage))
                {

                    if (_tmpSourcePercentage != null)
                    {
                        _editSource5Percentage = _tmpSourcePercentage;
                    }
                }
                else
                {
                    _editSource5Percentage = null;
                }
                OnPropertyChanged(EditSource5PercentageProperty);


                AssortmentLocationBuddyView firstSelectedBuddyView = SelectedAssortmentLocationBuddyViews.First();

                //EditLocation1
                _editLocation1 = null;
                if (firstSelectedBuddyView.Source1 != null)
                {
                    bool locBuddyExists = true;
                    foreach (AssortmentLocationBuddyView locBuddy in SelectedAssortmentLocationBuddyViews)
                    {
                        if (locBuddy.Source1 == null)
                        {
                            locBuddyExists = false;
                        }
                    }

                    if (locBuddyExists == true)
                    {
                        if (SelectedAssortmentLocationBuddyViews.All(r => r.Source1.Code == firstSelectedBuddyView.Source1.Code)) //SelectedAssortmentLocationBuddyViews.firstView.Source2.Code))
                        {
                            foreach (LocationInfo LocationInfo in _availableSourceLocations)
                            {
                                if (LocationInfo == firstView.Source1)
                                {
                                    _editLocation1 = LocationInfo;
                                }
                            }
                        }
                    }
                }
                else
                {
                    _editLocation1 = null;
                }
                OnPropertyChanged(EditLocation1Property);





                //EditLocation2
                _editLocation2 = null;
                if (firstSelectedBuddyView.Source2 != null)
                {
                    bool locBuddyExists = true;
                    foreach (AssortmentLocationBuddyView locBuddy in SelectedAssortmentLocationBuddyViews)
                    {
                        if (locBuddy.Source2 == null)
                        {
                            locBuddyExists = false;
                        }
                    }

                    if (locBuddyExists == true)
                    {
                        if (SelectedAssortmentLocationBuddyViews.All(r => r.Source2.Code == firstSelectedBuddyView.Source2.Code))
                        {
                            foreach (LocationInfo LocationInfo in _availableSourceLocations)
                            {
                                if (LocationInfo == firstView.Source2)
                                {
                                    _editLocation2 = LocationInfo;
                                }
                            }
                        }
                    }
                }
                else
                {
                    _editLocation2 = null;
                }
                OnPropertyChanged(EditLocation2Property);


                //EditLocation3
                _editLocation3 = null;
                if (firstSelectedBuddyView.Source3 != null)
                {
                    bool locBuddyExists = true;
                    foreach (AssortmentLocationBuddyView locBuddy in SelectedAssortmentLocationBuddyViews)
                    {
                        if (locBuddy.Source3 == null)
                        {
                            locBuddyExists = false;
                        }
                    }

                    if (locBuddyExists == true)
                    {
                        if (SelectedAssortmentLocationBuddyViews.All(r => r.Source3.Code == firstSelectedBuddyView.Source3.Code))
                        {
                            foreach (LocationInfo LocationInfo in _availableSourceLocations)
                            {
                                if (LocationInfo == firstView.Source3)
                                {
                                    _editLocation3 = LocationInfo;
                                }
                            }
                        }
                    }
                }
                else
                {
                    _editLocation3 = null;
                }
                OnPropertyChanged(EditLocation3Property);


                //EditLocation4
                _editLocation4 = null;
                if (firstSelectedBuddyView.Source4 != null)
                {
                    bool locBuddyExists = true;
                    foreach (AssortmentLocationBuddyView locBuddy in SelectedAssortmentLocationBuddyViews)
                    {
                        if (locBuddy.Source4 == null)
                        {
                            locBuddyExists = false;
                        }
                    }

                    if (locBuddyExists == true)
                    {
                        if (SelectedAssortmentLocationBuddyViews.All(r => r.Source4.Code == firstSelectedBuddyView.Source4.Code))
                        {
                            foreach (LocationInfo LocationInfo in _availableSourceLocations)
                            {
                                if (LocationInfo == firstView.Source4)
                                {
                                    _editLocation4 = LocationInfo;
                                }
                            }
                        }
                    }
                }
                else
                {
                    _editLocation4 = null;
                }
                OnPropertyChanged(EditLocation4Property);


                //EditLocation5
                _editLocation5 = null;
                if (firstSelectedBuddyView.Source5 != null)
                {
                    bool locBuddyExists = true;
                    foreach (AssortmentLocationBuddyView locBuddy in SelectedAssortmentLocationBuddyViews)
                    {
                        if (locBuddy.Source5 == null)
                        {
                            locBuddyExists = false;
                        }
                    }

                    if (locBuddyExists == true)
                    {
                        if (SelectedAssortmentLocationBuddyViews.All(r => r.Source5.Code == firstSelectedBuddyView.Source5.Code))
                        {
                            foreach (LocationInfo LocationInfo in _availableSourceLocations)
                            {
                                if (LocationInfo == firstView.Source5)
                                {
                                    _editLocation5 = LocationInfo;
                                }
                            }
                        }
                    }
                }
                else
                {
                    _editLocation5 = null;
                }
                OnPropertyChanged(EditLocation5Property);

            }
        }

        /// <summary>
        /// Reloads the list of available products
        /// </summary>
        private void ReloadAvailableLocations()
        {
            if (_availableSourceLocations.Count > 0)
            {
                _availableSourceLocations.Clear();
            }

            var locations = LocationInfoList.FetchByEntityId(App.ViewState.EntityId);

            _availableSourceLocations.AddRange(locations.OrderBy(f => f.Code));
        }

        #endregion

        #region EventHandlers

        /// <summary>
        /// Responds to changes to the collection of selected rows
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedLocationBuddyRows_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //Update the edit section values when selection changes
            SetEditSectionValues();
        }

        #endregion

        #region IDataErrorInfo Members

        public String Error
        {
            get { return String.Empty; }
        }

        public String this[String columnName]
        {
            get
            {
                //source percentage 1 validation
                if (columnName == EditSource1PercentageProperty.Path)
                {
                    if (EditSource1Percentage < 0 || EditSource1Percentage > 10)
                    {
                        return Message.AssortmentLocationBuddyReview_ValidateSourcePercentage_NotPercentage;
                    }
                }

                //source percentage 2 validation
                if (columnName == EditSource2PercentageProperty.Path)
                {
                    if (EditSource2Percentage < 0 || EditSource2Percentage > 10)
                    {
                        return Message.AssortmentLocationBuddyReview_ValidateSourcePercentage_NotPercentage;
                    }
                }

                //source percentage 3 validation
                if (columnName == EditSource3PercentageProperty.Path)
                {
                    if (EditSource3Percentage < 0 || EditSource3Percentage > 10)
                    {
                        return Message.AssortmentLocationBuddyReview_ValidateSourcePercentage_NotPercentage;
                    }
                }

                //source percentage 4 validation
                if (columnName == EditSource4PercentageProperty.Path)
                {
                    if (EditSource4Percentage < 0 || EditSource4Percentage > 10)
                    {
                        return Message.AssortmentLocationBuddyReview_ValidateSourcePercentage_NotPercentage;
                    }
                }

                //source percentage 5 validation
                if (columnName == EditSource5PercentageProperty.Path)
                {
                    if (EditSource5Percentage < 0 || EditSource5Percentage > 10)
                    {
                        return Message.AssortmentLocationBuddyReview_ValidateSourcePercentage_NotPercentage;
                    }
                }
                
                return String.Empty;
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _selectedAssortmentLocationBuddyViews.CollectionChanged -= SelectedLocationBuddyRows_CollectionChanged;
                    _availableSourceLocations.Clear();
                    _availableSourceLocations = null;
                    _currentAssortment = null;
                    _editLocation1 = null;
                    _editLocation2 = null;
                    _editLocation3 = null;
                    _editLocation4 = null;
                    _editLocation5 = null;
                    _locationBuddies.Clear();
                    _locationBuddies = null;
                    _locationsToRemoveQue.Clear();
                    foreach (AssortmentLocationBuddyView view in _selectedAssortmentLocationBuddyViews)
                    {
                        view.Dispose();
                    }
                    _selectedAssortmentLocationBuddyViews.Clear();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
