﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Model;
using System.Globalization;
using System.Text.RegularExpressions;
using Galleria.Framework.Collections;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.LocationBuddy
{
    /// <summary>
    /// Class used to represent Location Buddy in Add New screen
    /// </summary>
    public sealed class AssortmentLocationBuddyRow : IDataErrorInfo
    {

        #region Fields

        private LocationInfo _location;
        private Single _utilisation;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath LocationProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyRow>(p => p.Location);
        public static readonly PropertyPath UtilisationProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyRow>(p => p.Utilisation);

        #endregion

        #region Properties

        /// <summary>
        /// Sets/gets location
        /// </summary>
        public LocationInfo Location
        {
            get { return _location; }
            set
            {
                _location = value;
                OnPropertyChanged(LocationProperty);
            }
        }

        /// <summary>
        /// Sets/gets utilisation percentage
        /// </summary>
        public Single Utilisation
        {
            get { return _utilisation; }
            set
            {
                _utilisation = value;
                OnPropertyChanged(UtilisationProperty);
            }
        }

        #endregion

        #region Constructor

        public AssortmentLocationBuddyRow()
        {
            _location = null;
            this.Utilisation = 100;
        }

        public AssortmentLocationBuddyRow(LocationInfo newLocation, Single newUtilisation)
        {
            _location = newLocation;
            this.Utilisation = newUtilisation;
        }

        #endregion

        #region IDataErrorInfo

        String IDataErrorInfo.Error
        {
            get
            {
                String error = String.Empty;
                IDataErrorInfo thisDataErrorInfo = (IDataErrorInfo)this;

                String newUtilisationError = thisDataErrorInfo[UtilisationProperty.Path];
                if (!String.IsNullOrEmpty(newUtilisationError))
                {
                    error = String.Format(CultureInfo.CurrentCulture, "{0}{1}{2}",
                        error, Environment.NewLine, newUtilisationError);
                }

                return error;
            }
        }

        String IDataErrorInfo.this[String columnName]
        {
            get
            {
                if (_utilisation > 1000 || _utilisation < 0)
                {
                    return Message.AssortmentLocationBuddyRow_UtilizationError;
                }
                //no errors in check columns
                return String.Empty;
            }
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }
}
