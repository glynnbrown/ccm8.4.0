﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
// V8-32396 : A.Probyn
//  Updated dispose and ensured location selection can be from anywhere in the entity
// V8-32828 : A.Heathcote
//  Added Null Exception error protection to dispose to prevent CCM crash
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Model;
using System.Globalization;
using System.Text.RegularExpressions;
using Galleria.Framework.Collections;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.LocationBuddy
{
    /// <summary>
    /// Represents a view of a LocationBuddy.
    /// </summary>
    public sealed class AssortmentLocationBuddyView : INotifyPropertyChanged, IDisposable
    {
        #region Fields

        private PlanogramAssortmentLocationBuddy _sourceBuddy;
        private PlanogramAssortmentLocationBuddy _existingSourceBuddy;
        private String _locationName;
        private LocationInfo _source1;
        private LocationInfo _source2;
        private LocationInfo _source3;
        private LocationInfo _source4;
        private LocationInfo _source5;
        private Boolean _isDirty;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath SourceBuddyProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyView>(p => p.SourceBuddy);
        public static readonly PropertyPath LocationNameProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyView>(p => p.LocationName);
        public static readonly PropertyPath TreatmentTypeProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyView>(p => p.TreatmentType);
        public static readonly PropertyPath ExistingSourceBuddyProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyView>(p => p.ExistingSourceBuddy);
        public static readonly PropertyPath IsDirtyProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyView>(p => p.IsDirty);
        public static readonly PropertyPath Source1Property = WpfHelper.GetPropertyPath<AssortmentLocationBuddyView>(p => p.Source1);
        public static readonly PropertyPath Source1PercentageProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyView>(p => p.Source1Percentage);
        public static readonly PropertyPath Source2Property = WpfHelper.GetPropertyPath<AssortmentLocationBuddyView>(p => p.Source2);
        public static readonly PropertyPath Source2PercentageProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyView>(p => p.Source2Percentage);
        public static readonly PropertyPath Source3Property = WpfHelper.GetPropertyPath<AssortmentLocationBuddyView>(p => p.Source3);
        public static readonly PropertyPath Source3PercentageProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyView>(p => p.Source3Percentage);
        public static readonly PropertyPath Source4Property = WpfHelper.GetPropertyPath<AssortmentLocationBuddyView>(p => p.Source4);
        public static readonly PropertyPath Source4PercentageProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyView>(p => p.Source4Percentage);
        public static readonly PropertyPath Source5Property = WpfHelper.GetPropertyPath<AssortmentLocationBuddyView>(p => p.Source5);
        public static readonly PropertyPath Source5PercentageProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyView>(p => p.Source5Percentage);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the source location buddy
        /// </summary>
        public PlanogramAssortmentLocationBuddy SourceBuddy
        {
            get { return _sourceBuddy; }
        }


        /// <summary>
        /// Gets the existing location buddy
        /// </summary>
        public PlanogramAssortmentLocationBuddy ExistingSourceBuddy
        {
            get { return _existingSourceBuddy; }
        }


        /// <summary>
        /// Gets/Sets the the target location
        /// </summary>
        public String LocationName
        {
            get { return _locationName; }
            set
            {
                _locationName = value;
                OnPropertyChanged(LocationNameProperty);
            }
        }


        /// <summary>
        /// Gets/sets the treatment type friendly name
        /// </summary>
        public PlanogramAssortmentLocationBuddyTreatmentType TreatmentType
        {
            get { return SourceBuddy.TreatmentType; }
            set
            {
                if (value != null)
                {
                    _sourceBuddy.TreatmentType = value;
                    OnPropertyChanged(TreatmentTypeProperty);
                }
            }
        }
        
        /// <summary>
        /// Gets/sets the LocationId
        /// </summary>
        public String LocationCode
        {
            get { return SourceBuddy.LocationCode; }
        }

        /// <summary>
        /// Gets/sets the Source1LocationCode
        /// </summary>
        public String Source1LocationCode
        {
            get { return SourceBuddy.S1LocationCode; }
        }

        /// <summary>
        /// Gets/sets the Source2LocationCode
        /// </summary>
        public String Source2LocationCode
        {
            get { return SourceBuddy.S2LocationCode; }
        }

        /// <summary>
        /// Gets/sets the Source3LocationCode
        /// </summary>
        public String Source3LocationCode
        {
            get { return SourceBuddy.S3LocationCode; }
        }

        /// <summary>
        /// Gets/sets the Source4LocationCode
        /// </summary>
        public String Source4LocationCode
        {
            get { return SourceBuddy.S4LocationCode; }
        }

        /// <summary>
        /// Gets/sets the Source5LocationCode
        /// </summary>
        public String Source5LocationCode
        {
            get { return SourceBuddy.S5LocationCode; }
        }

        /// <summary>
        /// Gets/Sets the source1 description
        /// </summary>
        public LocationInfo Source1
        {
            get { return _source1; }
            set
            {
                _source1 = value;
                OnPropertyChanged(Source1Property);
            }
        }


        /// <summary>
        /// Gets/Sets  the source 1 percentage
        /// </summary>
        public Single? Source1Percentage
        {
            get
            {
                if (_sourceBuddy.S1Percentage != null)
                {
                    return (_sourceBuddy.S1Percentage);
                }
                return null;
            }
            set
            {
                if (value != null)
                {
                    _sourceBuddy.S1Percentage = value;
                    OnPropertyChanged(Source1PercentageProperty);
                }
            }
        }


        /// <summary>
        /// Gets the source2 description
        /// </summary>
        public LocationInfo Source2
        {
            get { return _source2; }
            set
            {
                _source2 = value;
                OnPropertyChanged(Source2Property);
            }
        }


        /// <summary>
        /// Gets/Sets  the source 2 percentage
        /// </summary>
        public Single? Source2Percentage
        {
            get
            {
                if (_sourceBuddy.S2Percentage != null)
                {
                    return _sourceBuddy.S2Percentage;
                }
                return null;
            }
            set
            {
                if (value != null)
                {
                    _sourceBuddy.S2Percentage = value;
                    OnPropertyChanged(Source2PercentageProperty);
                }
            }
        }


        /// <summary>
        /// Gets the source3 description
        /// </summary>
        public LocationInfo Source3
        {
            get { return _source3; }
            set
            {
                _source3 = value;
                OnPropertyChanged(Source3Property);
            }
        }


        /// <summary>
        /// Gets/Sets  the source 3 percentage
        /// </summary>
        public Single? Source3Percentage
        {
            get
            {
                if (_sourceBuddy.S3Percentage != null)
                {
                    return _sourceBuddy.S3Percentage;
                }
                return null;
            }
            set
            {
                if (value != null)
                {
                    _sourceBuddy.S3Percentage = value;
                    OnPropertyChanged(Source3PercentageProperty);
                }
            }
        }


        /// <summary>
        /// Gets the source4 description
        /// </summary>
        public LocationInfo Source4
        {
            get { return _source4; }
            set
            {
                _source4 = value;
                OnPropertyChanged(Source4Property);
            }
        }


        /// <summary>
        /// Gets/Sets the source 4 percentage
        /// </summary>
        public Single? Source4Percentage
        {
            get
            {
                if (_sourceBuddy.S4Percentage != null)
                {
                    return _sourceBuddy.S4Percentage;
                }
                return null;
            }
            set
            {
                if (value != null)
                {
                    _sourceBuddy.S4Percentage = value;
                    OnPropertyChanged(Source4PercentageProperty);
                }
            }
        }


        /// <summary>
        /// Gets the source5 description
        /// </summary>
        public LocationInfo Source5
        {
            get { return _source5; }
            set
            {
                _source5 = value;
                OnPropertyChanged(Source5Property);
            }
        }


        /// <summary>
        /// Gets/Sets  the source 5 percentage
        /// </summary>
        public Single? Source5Percentage
        {
            get
            {
                if (_sourceBuddy.S5Percentage != null)
                {
                    return _sourceBuddy.S5Percentage;
                }
                return null;
            }
            set
            {
                if (value != null)
                {
                    _sourceBuddy.S5Percentage = value;
                    OnPropertyChanged(Source5PercentageProperty);
                }
            }
        }


        /// <summary>
        /// Checks whether the object is dirty
        /// </summary>
        public Boolean IsDirty
        {
            get { return _isDirty; }
            set
            {
                _isDirty = value;
                OnPropertyChanged(IsDirtyProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new view of a locationBuddy
        /// </summary>
        /// <param name="sourceBuddy">The source locationBuddy object</param>
        public AssortmentLocationBuddyView(PlanogramAssortmentLocationBuddy targetToLoad, PlanogramAssortment currentAssortment, IEnumerable<LocationInfo> availableSourceLocations)
        {
            //Initialise lists
            List<Object> _locationCodesToLookup = new List<Object>();                 //used to store ids of locations to find in the LocationList
            List<LocationInfo> _foundLocations = new List<LocationInfo>();  //used to store locations after they've been found by their ID

            _existingSourceBuddy = targetToLoad;

            //Set the sourceBuddy to actually edit
            if (_existingSourceBuddy != null)
            {
                _sourceBuddy = _existingSourceBuddy.Clone();
            }
            else
            {
                //Create a new null source buddy
                _sourceBuddy = PlanogramAssortmentLocationBuddy.NewPlanogramAssortmentLocationBuddy();
            }

            //Set event handlers
            _sourceBuddy.PropertyChanged += SourceBuddy_PropertyChanged;

            //Construct list of locations to lookup
            _locationCodesToLookup.Add(_sourceBuddy.LocationCode);

            if (!String.IsNullOrEmpty(_sourceBuddy.S1LocationCode))
            {
                _locationCodesToLookup.Add(_sourceBuddy.S1LocationCode);
            }
            if (!String.IsNullOrEmpty(_sourceBuddy.S2LocationCode))
            {
                _locationCodesToLookup.Add(_sourceBuddy.S2LocationCode);
            }
            if (!String.IsNullOrEmpty(_sourceBuddy.S3LocationCode))
            {
                _locationCodesToLookup.Add(_sourceBuddy.S3LocationCode);
            }
            if (!String.IsNullOrEmpty(_sourceBuddy.S4LocationCode))
            {
                _locationCodesToLookup.Add(_sourceBuddy.S4LocationCode);
            }
            if (!String.IsNullOrEmpty(_sourceBuddy.S5LocationCode))
            {
                _locationCodesToLookup.Add(_sourceBuddy.S5LocationCode);
            }

            //Initialise foundLocations indexes for later use
            for (Int32 index = 0; index < _locationCodesToLookup.Count; index++)
            {
                _foundLocations.Add(null);
            }

            //Lookup AssortmentLocations by linked Ids for every location
            foreach (LocationInfo location in availableSourceLocations)
            {
                //Compare to all Ids that need finding
                for (Int32 index = 0; index < _locationCodesToLookup.Count; index++)
                {

                    if (location.Code.Equals(_locationCodesToLookup[index]))
                    {
                        //If found, add to foundLocations list, matching order of locationIDs (important for matching correct utilisation to location)
                        _foundLocations[index] = location;
                    }
                }
            }
            _locationName = _foundLocations[0].ToString();
            if (_foundLocations.Count > 1)
            {
                _source1 = _foundLocations[1];
            }
            if (_foundLocations.Count > 2)
            {
                _source2 = _foundLocations[2];
            }
            if (_foundLocations.Count > 3)
            {
                _source3 = _foundLocations[3];
            }
            if (_foundLocations.Count > 4)
            {
                _source4 = _foundLocations[4];
            }
            if (_foundLocations.Count > 5)
            {
                _source5 = _foundLocations[5];
            }

            _isDirty = false;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to changes to the current SourceBuddy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SourceBuddy_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            //Mark the source buddy as dirty when it changes
            this._isDirty = true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Permanently commits any changes made if item is dirty
        /// </summary>
        /// <param name="applyToRange"></param>
        public void CommitChanges()
        {
            _existingSourceBuddy.TreatmentType = SourceBuddy.TreatmentType;

            _existingSourceBuddy.S1Percentage = SourceBuddy.S1Percentage;
            _existingSourceBuddy.S2Percentage = SourceBuddy.S2Percentage;
            _existingSourceBuddy.S3Percentage = SourceBuddy.S3Percentage;
            _existingSourceBuddy.S4Percentage = SourceBuddy.S4Percentage;
            _existingSourceBuddy.S5Percentage = SourceBuddy.S5Percentage;

            _existingSourceBuddy.S1LocationCode = SourceBuddy.S1LocationCode;
            _existingSourceBuddy.S2LocationCode = SourceBuddy.S2LocationCode;
            _existingSourceBuddy.S3LocationCode = SourceBuddy.S3LocationCode;
            _existingSourceBuddy.S4LocationCode = SourceBuddy.S4LocationCode;
            _existingSourceBuddy.S5LocationCode = SourceBuddy.S5LocationCode;

            _isDirty = false;
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            if (_sourceBuddy != null)
            {
                _sourceBuddy.PropertyChanged -= SourceBuddy_PropertyChanged;
            }
            _source1 = null;
            _source2 = null;
            _source3 = null;
            _source4 = null;
            _source5 = null;
            _sourceBuddy = null;
            
        }

        #endregion
    }
}
