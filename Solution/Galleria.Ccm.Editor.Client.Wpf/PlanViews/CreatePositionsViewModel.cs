﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion

#region Version History: (CCM 8.0.2)
// V8-25955 : D.Pleasance
//  Rework screen
#endregion
#endregion

using System;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.Text.RegularExpressions;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    public sealed class CreatePositionsViewModel : ViewModelAttachedControlObject<CreatePositionsWindow>
    {
        #region Fields

        private PlanogramView _plan;

        private readonly BulkObservableCollection<PlanogramProductView> _availableProducts = new BulkObservableCollection<PlanogramProductView>();
        private ReadOnlyBulkObservableCollection<PlanogramProductView> _availableProductsRO;

        private readonly BulkObservableCollection<PlanogramProductView> _filteredAvailableProducts = new BulkObservableCollection<PlanogramProductView>();
        private ReadOnlyBulkObservableCollection<PlanogramProductView> _filteredAvailableProductsRO;
        
        private PlanogramSubComponentView _selectedSubcomponent;
        private PlanogramProductView _selectedProduct;
        private String _filterProductText = String.Empty;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath FilteredAvailableProductsProperty = WpfHelper.GetPropertyPath<CreatePositionsViewModel>(p => p.FilteredAvailableProducts);
        public static readonly PropertyPath SelectedSubcomponentProperty = WpfHelper.GetPropertyPath<CreatePositionsViewModel>(p => p.SelectedSubcomponent);
        public static readonly PropertyPath SelectedProductProperty = WpfHelper.GetPropertyPath<CreatePositionsViewModel>(p => p.SelectedProduct);
        public static readonly PropertyPath FilterProductTextProperty = WpfHelper.GetPropertyPath<CreatePositionsViewModel>(p => p.FilterProductText);
                
        //Commands
        public static readonly PropertyPath OKCommandProperty =  WpfHelper.GetPropertyPath<CreatePositionsViewModel>(p=> p.OKCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<CreatePositionsViewModel>(p=> p.CancelCommand);

        #endregion

        #region Properties

        /// <summary>
        /// The master product list
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramProductView> AvailableProducts
        {
            get
            {
                if (_availableProductsRO == null)
                {
                    _availableProductsRO = new ReadOnlyBulkObservableCollection<PlanogramProductView>(_availableProducts);
                }
                return _availableProductsRO;
            }
        }

        /// <summary>
        /// Returns the collection of available filtered products
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramProductView> FilteredAvailableProducts
        {
            get
            {
                if (_filteredAvailableProductsRO == null)
                {
                    _filteredAvailableProductsRO = new ReadOnlyBulkObservableCollection<PlanogramProductView>(_filteredAvailableProducts);
                }
                return _filteredAvailableProductsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the new selected product
        /// </summary>
        public PlanogramProductView SelectedProduct
        {
            get { return _selectedProduct; }
            set
            {
                this.FilterProductText = value.ToString();
                _selectedProduct = value;
                OnPropertyChanged(SelectedProductProperty);
            }
        }

        /// <summary>
        /// Gets/Sets product filter text
        /// </summary>
        public String FilterProductText
        {
            get { return _filterProductText; }
            set
            {
                _filterProductText = value;
                _selectedProduct = null;
                OnPropertyChanged(FilterProductTextProperty);
                UpdateFilteredAvailableProducts(_filterProductText);
            }
        }
        
        /// <summary>
        /// Gets/Sets the selected subcomponent.
        /// </summary>
        public PlanogramSubComponentView SelectedSubcomponent
        {
            get { return _selectedSubcomponent; }
            set
            {
                _selectedSubcomponent = value;
                OnPropertyChanged(SelectedSubcomponentProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public CreatePositionsViewModel(PlanogramView plan, PlanogramSubComponentView planogramSubComponentView)
        {
            _plan = plan;
            _availableProducts.AddRange(plan.Products.OrderBy(p => p.ToString()));
            _filteredAvailableProducts.AddRange(plan.Products.OrderBy(p => p.ToString()));

            this.SelectedSubcomponent = planogramSubComponentView;
        }

        #endregion

        #region Commands

        #region OKCommand

        private RelayCommand _okCommand;

        /// <summary>
        /// Commits the action and closes the window.
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OK_Executed(),
                        p => OK_CanExecute())
                    {
                        FriendlyName= Message.Generic_OK
                    };
                    base.ViewModelCommands.Add(_okCommand);

                }
                return _okCommand;
            }
        }

        private Boolean OK_CanExecute()
        {
            if (this.SelectedProduct == null)
            {
                return false;
            }

            return true;
        }

        private void OK_Executed()
        {
            List<PlanogramProductView> products = new List<PlanogramProductView>();
            products.Add(this.SelectedProduct);
            this.SelectedSubcomponent.AddPositions(products);

            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Close the window without commiting the action.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);

                }
                return _cancelCommand;
            }
        }


        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #endregion

        #region Methods

        private void UpdateFilteredAvailableProducts(String filterProductText)
        {
            _filteredAvailableProducts.Clear();

            String searchPattern = CommonHelper.GetRexexKeywordPattern(filterProductText);
            
            _filteredAvailableProducts.AddRange(AvailableProducts
                .Where(w => Regex.IsMatch(w.ToString(), searchPattern, RegexOptions.IgnoreCase))
                .OrderBy(l => l.ToString()));
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _availableProducts.Clear();        
                    _filteredAvailableProducts.Clear();
                    _selectedSubcomponent = null;
                    _selectedProduct = null;
        
                    base.IsDisposed = true;
                }
            }
        }

        #endregion
    }
}