﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// L.Hodson ~ Created.
// V8-23726 : N.Haywood
//  Added searching products and fixture components
// V8-24124 : A.Silva ~ AddNewProduct is now Undo-able, which will revert any new addition if the user cancels.
// V8-26685 : A.Silva ~ Implemented ShowValidationTemplate command.
// V8-26860 : L.Luong ~ Added ShowEventLog command
// V8-27239 : I.George
// Removed CanUserSelectComponentsProperty and CanUserSelectPositionProperty
// V8-27825 : L.Ineson
//  Added document freezing
// V8-26279 : L.Luong
//  Removed Undo-able on AddNewProduct, as it was undoing the last change on the plan
// V8-27938 : N.Haywood
//  Added AddDocument
// V8-28191 : N.Haywood
//  Added selected plan document event
// V8-28345 : L.Ineson
//  Reworked how highlights are processed and applied to improve performance.
// V8-28356 : A.Kuszyk
//  Added CopyPerformanceData method.
#endregion

#region Version History : CCM 801

// V8-28764 : A.Kuszyk
//  Passed through selected plan item to PlanogramView.AddPlanItemCopy() in Paste method.
// V8-25148 : I.George
//  changed ShowPlanProperties method to return a boolean based on the widow result 
// V8-28846 : D.Pleasance
//  Amended ParentPackageView_OnSaveCompleted so that windowtitle property changed event is called.
#endregion

#region Version History : CCM 802
//V8-25378 : M.Pettit
//  Added ZoomToFitHeight() method
// V8-25955 : D.Pleasance
//  Reworked AddNewPosition
// V8-28766 : J.Pickup
//  Introduced calls to autofill when we change the fill x y z types to relevant type that is expected to trigger this - UpdatePlanogramProductPlacementsFills()
// V8-29317 : D.Pleasance
//  Amended Unfreeze to clear highlights dictionary
#endregion

#region Version History : CCM 802
// V8-29451 : D.Pleasance
//  Amended WindowTitle to include filename if available.
#endregion

#region Version History : CCM 810
// V8-28766 : J.Pickup
//  Now makes uses the new implementation of autofill I wrote on merchgroup as 
//  opposed to the autofill helper class. (Development still ongoing). 
// V8-30054 : L.Ineson
//  Moved fill command method to here.
#endregion

#region Version History : CCM811

// V8-30406 : A.Silva
//  Updated due to changes in PlanogramMerchandisingGroup.ProcessAutoCap.
// V8-30464 : L.Luong 
//  Added PathToolTip to show directory path as a tooltip when hovering tab
// V8-28688 : A.Probyn
//  Updated reference to PositionPropertiesViewModel to take a new constructor depending on where it is called from.
#endregion

#region Version History : CCM820
// V8-29968 : A.Kuszyk
//  Added IsSequenceUpToDate property and subscribed to PlanogramView MerchandisingGroupsProcessed event.
// V8-30536 : A.Kuszyk
//  Added OnMerchandisingVolumePercentagesChanged and subscribed to MerchandisingVolumePercentagesChanged event
//  on PlanogramView.
// V8-31165 : D.Pleasance
//  Removed logic to validate if product sequence is out of date. This is now always done if its a sequence template.
// V8-31199 : L.Ineson
//  Added Validation Status
// V8-31501 : L.Ineson
//  Amended last closed doc so it just holds settings rather than a full reference to the document.
#endregion

#region Version History : CCM830
// V8-31833 : L.Ineson
//  Moved FillSelectedPositionsOnGivenAxis logic into planogramview
// V8-31951 : N.Haywood
//  Set highlight when making a new view
// V8-32327 : A.Silva
//  Refactored Paste to process all plan items to be pasted in the source plan and not here.
// V8-32584 : N.Haywood
//  Added defensive code so we don't try to select nulls after pasting
// V8-32584 : N.Haywood
//  Fixed issue where pasting to all would crash when having different components selected
// V8-32633 : N.Haywood
//  Moved CleanUpOrphanedParts to the planogram model
// V8-32661 : A.Kuszyk
//  Added flag for sequence template panel
// V8-32636 : A.Probyn
//  Added ShowAnnotations
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.ComponentEditor;
using Galleria.Ccm.Editor.Client.Wpf.PlanProperties;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews.Blocking;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// A viewmodel controller containing multiple documents that all relate to the same planogram view.
    /// </summary>
    public sealed class PlanControllerViewModel : ViewModelAttachedControlObject<PlanController>
    {
        #region Nested Classes

        /// <summary>
        /// Denote the different validation statuses
        /// to be displayed in the status bar.
        /// </summary>
        public enum ValidationStatusType
        {
            None,
            Busy,
            Warning,
            OK
        }

        /// <summary>
        /// Simple class to hold info about the settings for the last
        /// closed document.
        /// </summary>
        public sealed class PlanDocumentSettings
        {
            public HighlightItem Highlight { get; private set; }
            public Boolean IsHighlightLegendVisible { get; private set; }
            public Boolean ShowPositions { get; private set; }
            public Boolean ShowPositionUnits { get; private set; }
            public Boolean ShowProductImages { get; private set; }
            public Boolean ShowProductShapes { get; private set; }
            public Boolean ShowProductFillPatterns { get; private set; }
            public LabelItem ProductLabel { get; private set; }
            public Boolean ShowFixtureImages { get; private set; }
            public Boolean ShowFixtureFillPatterns { get; private set; }
            public LabelItem FixtureLabel { get; private set; }
            public Boolean ShowChestWalls { get; private set; }
            public Boolean RotateTopDownComponents { get; private set; }
            public Boolean ShowShelfRisers { get; private set; }
            public Boolean ShowPegHoles { get; private set; }
            public Boolean ShowPegs { get; private set; }
            public Boolean ShowNotches { get; private set; }
            public Boolean ShowDividerLines { get; private set; }
            public Boolean ShowDividers { get; private set; }
            public Boolean ShowGridlines { get; private set; }
            public Boolean ShowWireframeOnly { get; private set; }
            public Boolean ShowAnnotations { get; private set; }

            /// <summary>
            /// Creates a new snapshot of the settings on the source doc.
            /// </summary>
            public PlanDocumentSettings(IPlanDocument sourceDoc)
            {
                this.ShowPositions = sourceDoc.ShowPositions;
                this.ShowPositionUnits = sourceDoc.ShowPositionUnits;
                this.ShowProductImages = sourceDoc.ShowProductImages;
                this.ShowProductShapes = sourceDoc.ShowProductShapes;
                this.ShowProductFillPatterns = sourceDoc.ShowProductFillPatterns;
                this.ShowFixtureImages = sourceDoc.ShowFixtureImages;
                this.ShowFixtureFillPatterns = sourceDoc.ShowFixtureFillPatterns;
                this.ShowChestWalls = sourceDoc.ShowChestWalls;
                this.RotateTopDownComponents = sourceDoc.RotateTopDownComponents;
                this.ShowShelfRisers = sourceDoc.ShowShelfRisers;
                this.ShowNotches = sourceDoc.ShowNotches;
                this.ShowPegHoles = sourceDoc.ShowPegHoles;
                this.ShowPegs = sourceDoc.ShowPegs;
                this.ShowDividers = sourceDoc.ShowDividers;
                this.ShowDividerLines = sourceDoc.ShowDividerLines;
                this.ProductLabel = sourceDoc.ProductLabel;
                this.FixtureLabel = sourceDoc.FixtureLabel;
                this.ShowGridlines = sourceDoc.ShowGridlines;
                this.ShowWireframeOnly = sourceDoc.ShowWireframeOnly;
                this.Highlight = sourceDoc.Highlight;
                this.ShowAnnotations = sourceDoc.ShowAnnotations;
            }

            /// <summary>
            /// Copies these settings to the given source doc.
            /// </summary>
            public void CopyTo(IPlanDocument sourceDoc)
            {
                sourceDoc.ShowPositions = this.ShowPositions;
                sourceDoc.ShowPositionUnits = this.ShowPositionUnits;
                sourceDoc.ShowProductImages = this.ShowProductImages;
                sourceDoc.ShowProductShapes = this.ShowProductShapes;
                sourceDoc.ShowProductFillPatterns = this.ShowProductFillPatterns;
                sourceDoc.ShowFixtureImages = this.ShowFixtureImages;
                sourceDoc.ShowFixtureFillPatterns = this.ShowFixtureFillPatterns;
                sourceDoc.ShowChestWalls = this.ShowChestWalls;
                sourceDoc.RotateTopDownComponents = this.RotateTopDownComponents;
                sourceDoc.ShowShelfRisers = this.ShowShelfRisers;
                sourceDoc.ShowNotches = this.ShowNotches;
                sourceDoc.ShowPegHoles = this.ShowPegHoles;
                sourceDoc.ShowPegs = this.ShowPegs;
                sourceDoc.ShowDividers = this.ShowDividers;
                sourceDoc.ShowDividerLines = this.ShowDividerLines;
                sourceDoc.ProductLabel = this.ProductLabel;
                sourceDoc.FixtureLabel = this.FixtureLabel;
                sourceDoc.ShowGridlines = this.ShowGridlines;
                sourceDoc.ShowWireframeOnly = this.ShowWireframeOnly;
                sourceDoc.Highlight = this.Highlight;
                sourceDoc.ShowAnnotations = this.ShowAnnotations;
            }
        }

        #endregion

        #region Fields

        private readonly UserEditorSettingsViewModel _appSettingsView;
        private readonly PlanogramView _sourcePlanogram;
        private Boolean _isFrozen;

        private Boolean _isActivePlanController;

        private Boolean _supressPerformanceIntensiveUpdates;

        private readonly BulkObservableCollection<IPlanDocument> _planDocuments = new BulkObservableCollection<IPlanDocument>();
        private ReadOnlyBulkObservableCollection<IPlanDocument> _planDocumentsRO;
        private IPlanDocument _selectedPlanDocument;
        private PlanDocumentSettings _lastClosedDocument;

        private Boolean _isPropertiesPanelVisible = false;
        private Boolean _isSequenceGroupsPanelVisible = false;
        private MouseToolType _selectedMouseToolType = MouseToolType.Pointer;

        private readonly PlanItemSelection _selectedPlanItems;

        private Boolean _autosizeGridlines = true;
        private Double _gridlineFixedHeight;
        private Double _gridlineFixedWidth;
        private Double _gridlineFixedDepth;
        private Double _gridlineMinorDist = 10D;
        private Double _gridlineMajorDist = 50D;

        private readonly Dictionary<HighlightItem, PlanogramHighlightResult> _highlights = new Dictionary<HighlightItem, PlanogramHighlightResult>(); //dictionary of results for processed highlights.

        private Boolean _isSequenceUpToDate;//Indicates whether or not the sequence for this plan is up to date.
        private ValidationStatusType _validationStatus = ValidationStatusType.Busy;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath IsActivePlanControllerProperty = WpfHelper.GetPropertyPath<PlanControllerViewModel>(p => p.IsActivePlanController);
        public static readonly PropertyPath TitleProperty = WpfHelper.GetPropertyPath<PlanControllerViewModel>(p => p.Title);
        public static readonly PropertyPath WindowTitleProperty = WpfHelper.GetPropertyPath<PlanControllerViewModel>(p => p.WindowTitle);
        public static readonly PropertyPath PathToolTipProperty = WpfHelper.GetPropertyPath<PlanControllerViewModel>(p => p.PathToolTip);
        public static readonly PropertyPath SourcePlanogramProperty = WpfHelper.GetPropertyPath<PlanControllerViewModel>(p => p.SourcePlanogram);
        public static readonly PropertyPath PlanDocumentsProperty = WpfHelper.GetPropertyPath<PlanControllerViewModel>(p => p.PlanDocuments);
        public static readonly PropertyPath SelectedPlanDocumentProperty = WpfHelper.GetPropertyPath<PlanControllerViewModel>(p => p.SelectedPlanDocument);
        public static readonly PropertyPath IsPropertiesPanelVisibleProperty = WpfHelper.GetPropertyPath<PlanControllerViewModel>(p => p.IsPropertiesPanelVisible);
        public static readonly PropertyPath IsSequenceGroupsPanelVisibleProperty = WpfHelper.GetPropertyPath<PlanControllerViewModel>(p => p.IsSequenceGroupsPanelVisible);
        public static readonly PropertyPath SelectedPlanItemsProperty = WpfHelper.GetPropertyPath<PlanControllerViewModel>(p => p.SelectedPlanItems);
        public static readonly PropertyPath SelectedMouseToolTypeProperty = WpfHelper.GetPropertyPath<PlanControllerViewModel>(p => p.SelectedMouseToolType);
        public static readonly PropertyPath AutosizeGridlinesProperty = WpfHelper.GetPropertyPath<PlanControllerViewModel>(p => p.AutosizeGridlines);
        public static readonly PropertyPath GridlineFixedHeightProperty = WpfHelper.GetPropertyPath<PlanControllerViewModel>(p => p.GridlineFixedHeight);
        public static readonly PropertyPath GridlineFixedWidthProperty = WpfHelper.GetPropertyPath<PlanControllerViewModel>(p => p.GridlineFixedWidth);
        public static readonly PropertyPath GridlineFixedDepthProperty = WpfHelper.GetPropertyPath<PlanControllerViewModel>(p => p.GridlineFixedDepth);
        public static readonly PropertyPath GridlineMajorDistProperty = WpfHelper.GetPropertyPath<PlanControllerViewModel>(p => p.GridlineMajorDist);
        public static readonly PropertyPath GridlineMinorDistProperty = WpfHelper.GetPropertyPath<PlanControllerViewModel>(p => p.GridlineMinorDist);
        public static readonly PropertyPath SupressPerformanceIntensiveUpdatesProperty = WpfHelper.GetPropertyPath<PlanControllerViewModel>(p => p.SuppressPerformanceIntensiveUpdates);
        public static readonly PropertyPath ValidationStatusProperty = WpfHelper.GetPropertyPath<PlanControllerViewModel>(p => p.ValidationStatus);
        public static readonly PropertyPath ValidationWarningsCountProperty = WpfHelper.GetPropertyPath<PlanControllerViewModel>(p => p.ValidationWarningsCount);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets a flag to indicate if this is the active plan controller
        /// </summary>
        public Boolean IsActivePlanController
        {
            get { return _isActivePlanController; }
            set
            {
                _isActivePlanController = value;
                OnPropertyChanged(IsActivePlanControllerProperty);

                if (!value && this.SelectedMouseToolType != MouseToolType.Pointer)
                {
                    //reset the mouse pointer back
                    this.SelectedMouseToolType = MouseToolType.Pointer;
                }

                UpdateActiveDocumentFlags();
            }
        }

        /// <summary>
        /// Returns true if this controller is currently frozen.
        /// </summary>
        public Boolean IsFrozen
        {
            get { return _isFrozen; }
            private set
            {
                _isFrozen = value;
                OnPropertyChanged("IsFrozen");
            }

        }

        /// <summary>
        /// Indicates whether or not the sequence in the current blocking is up-to-date with plan changes.
        /// </summary>
        public Boolean IsSequenceUpToDate
        {
            get
            {
                return _isSequenceUpToDate;
            }
            set
            {
                _isSequenceUpToDate = value;
                OnPropertyChanged("IsSequenceUpToDate");
            }
        }

        /// <summary>
        /// Returns the controller title.
        /// </summary>
        public String Title
        {
            get
            {
                if (this.SourcePlanogram != null)
                {
                    return this.SourcePlanogram.Name;
                }
                return String.Empty;
            }
        }

        /// <summary>
        /// Returns the controller Directory Path as a tool tip
        /// </summary>
        public String PathToolTip
        {
            get
            {
                if (this.SourcePlanogram.ParentPackageView.Model.PackageType == PackageType.FileSystem)
                {
                    return this.SourcePlanogram.ParentPackageView.FileName;
                }
                else
                {
                    return this.SourcePlanogram.ParentPackageView.RepositoryFolderPath;
                }
            }
        }

        /// <summary>
        /// Returns the controller title to use in the window title bar
        /// </summary>
        public String WindowTitle
        {
            get
            {
                String returnVal = String.Empty;
                if (this.SourcePlanogram != null)
                {
                    if (this.SourcePlanogram.ParentPackageView != null)
                    {
                        returnVal = (this.SourcePlanogram.ParentPackageView.FileName != null && this.SourcePlanogram.ParentPackageView.FileName.Length > 0) ?
                                            Path.GetFileName(this.SourcePlanogram.ParentPackageView.FileName) : this.SourcePlanogram.Name;

                        if (this.SourcePlanogram.ParentPackageView.Model.IsReadOnly)
                        {
                            returnVal = String.Format("{0} {1}", returnVal, Message.MainPage_ReadOnlyTitle);
                        }
                    }
                    else
                    {
                        returnVal = this.SourcePlanogram.Name;
                    }
                }
                return returnVal;
            }
        }

        /// <summary>
        /// Gets the source planogram model.
        /// </summary>
        public PlanogramView SourcePlanogram
        {
            get { return _sourcePlanogram; }
        }

        /// <summary>
        /// Returns the collection of plan documents for the source plan
        /// </summary>
        public ReadOnlyBulkObservableCollection<IPlanDocument> PlanDocuments
        {
            get
            {
                if (_planDocumentsRO == null)
                {
                    _planDocumentsRO = new ReadOnlyBulkObservableCollection<IPlanDocument>(_planDocuments);
                }
                return _planDocumentsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected plan document.
        /// </summary>
        public IPlanDocument SelectedPlanDocument
        {
            get { return _selectedPlanDocument; }
            set
            {
                if (_selectedPlanDocument != value)
                {
                    _selectedPlanDocument = value;
                    OnPropertyChanged(SelectedPlanDocumentProperty);

                    UpdateActiveDocumentFlags();

                    if (SelectedPlanDocumentChanged != null) SelectedPlanDocumentChanged.Invoke(this, new EventArgs());
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether intensive updates should be supressed.
        /// </summary>
        public Boolean SuppressPerformanceIntensiveUpdates
        {
            get { return _supressPerformanceIntensiveUpdates; }
            set
            {
                _supressPerformanceIntensiveUpdates = value;
                OnPropertyChanged(SupressPerformanceIntensiveUpdatesProperty);
            }

        }

        /// <summary>
        /// Gets/Sets whether the properties window is visible
        /// </summary>
        public Boolean IsPropertiesPanelVisible
        {
            get { return _isPropertiesPanelVisible; }
            set
            {
                _isPropertiesPanelVisible = value;
                OnPropertyChanged(IsPropertiesPanelVisibleProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether the sequence groups panel window is visible
        /// </summary>
        public Boolean IsSequenceGroupsPanelVisible
        {
            get { return _isSequenceGroupsPanelVisible; }
            set
            {
                _isSequenceGroupsPanelVisible = value;
                OnPropertyChanged(IsSequenceGroupsPanelVisibleProperty);
            }
        }

        /// <summary>
        /// Returns the view of selected plan items.
        /// </summary>
        public PlanItemSelection SelectedPlanItems
        {
            get { return _selectedPlanItems; }
        }

        /// <summary>
        /// Gets/Sets the current mouse tool in use.
        /// </summary>
        public MouseToolType SelectedMouseToolType
        {
            get { return _selectedMouseToolType; }
            set
            {
                _selectedMouseToolType = value;
                OnPropertyChanged(SelectedMouseToolTypeProperty);

                //nb - doc update done by plandocument.
            }
        }

        /// <summary>
        /// Gets/Sets whether gridlines should be autosized.
        /// </summary>
        public Boolean AutosizeGridlines
        {
            get { return _autosizeGridlines; }
            set
            {
                _autosizeGridlines = value;
                OnPropertyChanged(AutosizeGridlinesProperty);

                //nb - doc update done by plandocument.
            }
        }

        /// <summary>
        /// Gets/Sets the height of fixed gridlines.
        /// </summary>
        public Double GridlineFixedHeight
        {
            get { return _gridlineFixedHeight; }
            set
            {
                _gridlineFixedHeight = value;
                OnPropertyChanged(GridlineFixedHeightProperty);

                //nb - doc update done by plandocument.
            }
        }

        /// <summary>
        /// Gets/Sets the width of fixed gridlines.
        /// </summary>
        public Double GridlineFixedWidth
        {
            get { return _gridlineFixedWidth; }
            set
            {
                _gridlineFixedWidth = value;
                OnPropertyChanged(GridlineFixedWidthProperty);

                //nb - doc update done by plandocument.
            }
        }

        /// <summary>
        /// Gets/Sets the depth of fixed gridlines
        /// </summary>
        public Double GridlineFixedDepth
        {
            get { return _gridlineFixedDepth; }
            set
            {
                _gridlineFixedDepth = value;
                OnPropertyChanged(GridlineFixedDepthProperty);

                //nb - doc update done by plandocument.
            }
        }

        /// <summary>
        /// Gets/Sets the major distance of gridlines
        /// </summary>
        public Double GridlineMajorDist
        {
            get { return _gridlineMajorDist; }
            set
            {
                _gridlineMajorDist = value;
                OnPropertyChanged(GridlineMajorDistProperty);

                //nb - doc update done by plandocument.
            }
        }

        /// <summary>
        /// Gets/Sets the minor distance of gridlines
        /// </summary>
        public Double GridlineMinorDist
        {
            get { return _gridlineMinorDist; }
            set
            {
                _gridlineMinorDist = value;
                OnPropertyChanged(GridlineMinorDistProperty);

                //nb - doc update done by plandocument.
            }
        }

        /// <summary>
        /// Returns the validation status to show in the
        /// status bar.
        /// </summary>
        public ValidationStatusType ValidationStatus
        {
            get { return _validationStatus; }
            private set
            {
                _validationStatus = value;
                OnPropertyChanged(ValidationStatusProperty);
            }
        }

        /// <summary>
        /// Returns the number of validation warnings.
        /// </summary>
        public Int32 ValidationWarningsCount
        {
            get
            {
                return
                this.SourcePlanogram.ValidationWarningRows
                .Count(o => !o.IsCorrected && o.ShowWarningToUser);
            }
        }

        #endregion

        #region Events

        public event SelectedPlanDocumentChangedEventHandler SelectedPlanDocumentChanged;

        /// <summary>
        ///     Represents the method that will handle the <see cref="ColumnLayoutManager.CurrentColumnLayoutChanged" /> event.
        /// </summary>
        public delegate void SelectedPlanDocumentChangedEventHandler(Object sender, EventArgs e);

        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="planView"></param>
        public PlanControllerViewModel(PlanogramView planView)
            : this(planView, App.ViewState.Settings)
        { }

        /// <summary>
        /// Testing constructor
        /// </summary>
        /// <param name="planView"></param>
        /// <param name="appSettings"></param>
        public PlanControllerViewModel(PlanogramView planView, UserEditorSettingsViewModel appSettings)
        {
            _appSettingsView = appSettings;
            _sourcePlanogram = planView;
            _selectedPlanItems = new PlanItemSelection(_sourcePlanogram);

            //attach to the save notifications of the parent package
            if (planView.ParentPackageView != null)
            {
                planView.ParentPackageView.OnSaveBeginning += ParentPackageView_OnSaveBeginning;
                planView.ParentPackageView.OnSaveCompleted += ParentPackageView_OnSaveCompleted;
            }

            SetPlanEventHandlers(true);

            // Set the sequence as up to date if all the positions appear in the sequence already.
            IEnumerable<String> sequencedProducts = _sourcePlanogram.Model.Sequence.Groups
                .SelectMany(g => g.Products.Select(p => p.Gtin)).ToList();
            _isSequenceUpToDate = _sourcePlanogram.Model.Positions
                .All(p => sequencedProducts.Contains(p.GetPlanogramProduct().Gtin));
        }

        #endregion

        #region Command Methods

        /// <summary>
        /// Launches the properties window for this planogram.
        /// </summary>
        /// <returns> true if the dialog was not cancelled</returns>
        public Boolean ShowPlanProperties()
        {
            Int32 preFixtureCount = this.SourcePlanogram.Fixtures.Count;
            PlanogramProductPlacementXType preProductPlacementX = this.SourcePlanogram.Model.ProductPlacementX;
            PlanogramProductPlacementYType preProductPlacementY = this.SourcePlanogram.Model.ProductPlacementY;
            PlanogramProductPlacementZType preProductPlacementZ = this.SourcePlanogram.Model.ProductPlacementZ;

            //show the plan properties dialog.
            Boolean dialogResult = false;
            using (PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(this.SourcePlanogram))
            {
                if (this.SelectedPlanItems.Count > 0)
                {
                    viewModel.SelectedFixture = this.SelectedPlanItems.First().Fixture;
                }
                CommonHelper.GetWindowService().ShowDialog<PlanogramPropertiesWindow>(viewModel);
                dialogResult = (viewModel.DialogResult == true);
            }

            //If the number of fixtures has changed then zoom to fit all.
            if (this.SourcePlanogram.Fixtures.Count != preFixtureCount)
            {
                ZoomToFitAll();
            }

            return dialogResult;
        }

        /// <summary>
        /// Move Fixture Left
        /// </summary>
        public void FixtureMoveLeft()
        {
            using (PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(this.SourcePlanogram))
            {
                //Set the selected fixture
                if (this.SelectedPlanItems.Count > 0)
                {
                    viewModel.SelectedFixture = this.SelectedPlanItems.First().Fixture;
                }

                //Execute moving the fixture left
                if (viewModel.SelectedFixture != null)
                {
                    viewModel.MoveFixtureLeft_Executed();
                }
            }
        }

        /// <summary>
        /// Move Fixture Right
        /// </summary>
        public void FixtureMoveRight()
        {
            using (PlanogramPropertiesViewModel viewModel = new PlanogramPropertiesViewModel(this.SourcePlanogram))
            {
                //Set the selected fixture
                if (this.SelectedPlanItems.Count > 0)
                {
                    viewModel.SelectedFixture = this.SelectedPlanItems.First().Fixture;
                }

                //Execute moving the fixture right
                if (viewModel.SelectedFixture != null)
                {
                    viewModel.MoveFixtureRight_Executed();
                }
            }
        }

        /// <summary>
        /// Launches the edit component window for the selected component.
        /// </summary>
        /// <returns>true if the edit was applied.</returns>
        public Boolean EditComponentParts()
        {
            Boolean editApplied = false;
            PlanogramComponentView component = null;

            IPlanItem item = this.SelectedPlanItems.FirstOrDefault();
            if (item != null)
            {
                if (item.Component == null)
                {
                    if (item.PlanItemType == PlanItemType.Fixture)
                    {
                        item = item.Fixture.Assemblies.FirstOrDefault() as IPlanItem;
                    }

                    if (item != null && item.PlanItemType == PlanItemType.Assembly)
                    {
                        item = item.Assembly.Components.FirstOrDefault() as IPlanItem;
                    }

                    if (item == null) { return false; }
                }


                component = item.Component;

                //create the editor component view.
                PlanogramComponent planComponent = component.ComponentModel;
                EditorComponentView componentView = new EditorComponentView(planComponent);
                ComponentEditorViewModel viewModel = new ComponentEditorViewModel(componentView);

                viewModel.ShowDialog();

                if (viewModel.DialogResult == true)
                {
                    this.SourcePlanogram.BeginUndoableAction();

                    //apply the changes
                    componentView.ToPlanogramComponent();

                    this.SourcePlanogram.EndUndoableAction();
                    editApplied = true;
                }

                componentView.Dispose();
                componentView = null;
            }

            return editApplied;
        }

        /// <summary>
        /// Adds a new product to this plan
        /// </summary>
        public void AddNewProduct()
        {
            PlanogramView plan = this.SourcePlanogram;
            plan.BeginUndoableAction();

            PlanogramProductView newProduct = plan.AddProduct();

            PlanItemSelection selection = new PlanItemSelection();
            selection.Add(newProduct);

            PositionPropertiesViewModel winViewModel = new PositionPropertiesViewModel(plan, selection, true);
            winViewModel.ShowDialog();

            selection.Dispose();

            plan.EndUndoableAction();

            ////if the dialog was cancelled then undo the new product action.
            //if (this.AttachedControl != null && winViewModel.DialogResult != true)
            //{
            //    plan.CancelUndoableAction();
            //}
        }

        /// <summary>
        /// Adds a new position to this plan.
        /// </summary>
        public void AddNewPosition(PlanogramSubComponentView planogramSubComponentView)
        {
            if (this.AttachedControl != null)
            {
                CreatePositionsWindow win = new CreatePositionsWindow(this.SourcePlanogram, planogramSubComponentView);
                WindowHelper.ShowWindow(win, /*isModal*/true);
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on the source planogram.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SourcePlanogram_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Planogram.NameProperty.Name)
            {
                OnPropertyChanged(TitleProperty);
                OnPropertyChanged(PathToolTipProperty);
                OnPropertyChanged(WindowTitleProperty);
            }
            else if (e.PropertyName == Planogram.HeightProperty.Name
                || e.PropertyName == Planogram.WidthProperty.Name
                || e.PropertyName == Planogram.DepthProperty.Name)
            {
                this.GridlineFixedHeight = this.SourcePlanogram.Height;
                this.GridlineFixedWidth = this.SourcePlanogram.Width;
                this.GridlineFixedDepth = this.SourcePlanogram.Depth;
            }
            else if (e.PropertyName == PlanogramView.IsPlanUpToDateProperty.Path)
            {
                UpdateValidationStatus();
            }
        }

        /// <summary>
        /// Called whenever the parent package notifies that it is about to 
        /// perform a save
        /// </summary>
        private void ParentPackageView_OnSaveBeginning(object sender, EventArgs e)
        {
            //Freeze all documents so that they dont update during the save operation
            Freeze();
        }

        /// <summary>
        /// Called whenever the parent package notifies that a save has completed.
        /// </summary>
        private void ParentPackageView_OnSaveCompleted(object sender, EventArgs e)
        {
            //Unfreeze all documents so that they may continue updating.
            Unfreeze();
            OnPropertyChanged(WindowTitleProperty);
            OnPropertyChanged(PathToolTipProperty);
        }

        /// <summary>
        /// Called when merchandising groups are re-processed on the Source Planogram.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SourcePlanogram_MerchandisingGroupsProcessed(Object sender, EventArgs e)
        {
            // Mark the sequence as out of date.
            IsSequenceUpToDate = false;
        }

        /// <summary>
        /// Called whenever a plan process is finishing.
        /// </summary>
        private void SourcePlanogram_PlanProcessCompleting(object sender, EventArgs e)
        {
            //Carry out any processing required by this controller.
            _highlights.Clear();

            foreach (IPlanDocument doc in this.PlanDocuments)
            {
                doc.OnPlanProcessCompleting();
            }
        }

        /// <summary>
        /// Called when the <see cref="MerchandisingVolumePercentagesChanged"/> event is fired on <see cref="PlanogramView"/>.
        /// Iterates over the blocking documents in this controller and updates their merchandising volume percentages where possible.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMerchandisingVolumePercentagesChanged(Object sender, EventArgs<Dictionary<Object, Dictionary<Object, Double>>> e)
        {
            if (e.ReturnValue == null) return;

            foreach (IPlanDocument planDocument in this.PlanDocuments.Where(p => p.DocumentType == DocumentType.Blocking))
            {
                BlockingPlanDocument blockingDocument = planDocument as BlockingPlanDocument;
                if (blockingDocument == null) continue;
                Dictionary<Object, Double> merchandisingVolumePercentages;
                if (!e.ReturnValue.TryGetValue(blockingDocument.CurrentBlocking.Id, out merchandisingVolumePercentages)) continue;
                if (merchandisingVolumePercentages == null) continue;
                blockingDocument.UpdateMerchandisableBlockPercentages(merchandisingVolumePercentages);
            }
        }

        #endregion

        #region Methods

        #region Planogram Control

        /// <summary>
        /// Adds performance data from the given library product to the given planogram product, if both exist.
        /// </summary>
        /// <param name="from">The Library Product whose performance data should be added.</param>
        /// <param name="to">The product whose performance data should be updated.</param>
        public void CopyPerformanceData(
            ProductLibraryProduct from, PlanogramProduct to)
        {
            if (from == null || to == null) return;

            //Try and find performance data
            PlanogramPerformanceData planogramPerformanceData = to.GetPlanogramPerformanceData();

            //Ensure performance data exists
            if (planogramPerformanceData != null)
            {
                //Set values
                from.PerformanceData.CopyTo(planogramPerformanceData);
            }
        }

        private void SetPlanEventHandlers(Boolean isSubscribe)
        {
            if (isSubscribe)
            {
                _sourcePlanogram.PropertyChanged += SourcePlanogram_PropertyChanged;
                _sourcePlanogram.PlanProcessCompleting += SourcePlanogram_PlanProcessCompleting;
                _sourcePlanogram.MerchandisingGroupsProcessed += SourcePlanogram_MerchandisingGroupsProcessed;
                _sourcePlanogram.MerchandisingVolumePercentagesChanged += OnMerchandisingVolumePercentagesChanged;
            }
            else
            {
                _sourcePlanogram.PropertyChanged -= SourcePlanogram_PropertyChanged;
                _sourcePlanogram.PlanProcessCompleting -= SourcePlanogram_PlanProcessCompleting;
                _sourcePlanogram.MerchandisingGroupsProcessed -= SourcePlanogram_MerchandisingGroupsProcessed;
                _sourcePlanogram.MerchandisingVolumePercentagesChanged -= OnMerchandisingVolumePercentagesChanged;
            }
        }

        /// <summary>
        /// Notifies all documents held 
        /// by this controller to stop updating
        /// as the plan changes.
        /// </summary>
        private void Freeze()
        {
            if (!IsFrozen)
            {
                SetPlanEventHandlers(false);

                foreach (var document in this.PlanDocuments)
                {
                    document.Freeze();
                }

                IsFrozen = true;
            }
        }

        /// <summary>
        /// Notifies all documents to refresh and 
        /// start reacting to plan changes again.
        /// </summary>
        private void Unfreeze()
        {
            if (IsFrozen)
            {
                SetPlanEventHandlers(true);
                _highlights.Clear();

                foreach (var document in this.PlanDocuments)
                {
                    document.Unfreeze();
                }

                IsFrozen = false;
            }
        }

        #endregion

        #region Document Related

        /// <summary>
        /// Adds a new document to this controller
        /// </summary>
        /// <param name="docType"></param>
        public IPlanDocument AddNewDocument(DocumentType docType, Boolean checkForExisting)
        {
            //Make sure that we are not frozen.
            Debug.Assert(!IsFrozen, "Cannot add new docs while frozen");
            if (IsFrozen) return null;


            base.ShowWaitCursor(true);

            PlanogramView plan = this.SourcePlanogram;

            //reset the mouse type back to pointer
            this.SelectedMouseToolType = MouseToolType.Pointer;

            IPlanDocument document = null;

            if (checkForExisting)
            {
                document = this.PlanDocuments.FirstOrDefault(d => d.DocumentType == docType);
            }


            //create
            if (document == null)
            {
                document = PlanDocumentHelper.CreateDocument(docType, this);

                //add and select
                if (document != null)
                {
                    PlanDocumentSettings copySettingsFrom
                        = (this.SelectedPlanDocument != null) ?
                        new PlanDocumentSettings(this.SelectedPlanDocument)
                        : _lastClosedDocument;


                    _planDocuments.Add(document);

                    //copy accross settings from the prev selected doc.
                    if (copySettingsFrom != null)
                    {
                        copySettingsFrom.CopyTo(document);
                    }
                    else
                    {
                        document.LoadSettings(_appSettingsView.Model);
                    }
                }
            }

            this.SelectedPlanDocument = document;


            base.ShowWaitCursor(false);

            return document;
        }

        /// <summary>
        /// Adds an existing document to this controller
        /// </summary>
        /// <param name="docType"></param>
        public void AddDocument(IPlanDocument document)
        {
            //Make sure that we are not frozen.
            Debug.Assert(!IsFrozen, "Cannot add new docs while frozen");
            if (IsFrozen) return;

            base.ShowWaitCursor(true);

            PlanogramView plan = this.SourcePlanogram;

            //reset the mouse type back to pointer
            this.SelectedMouseToolType = MouseToolType.Pointer;

            //add and select
            if (document != null)
            {
                PlanDocumentSettings copySettingsFrom =
                    (this.SelectedPlanDocument != null) ?
                    new PlanDocumentSettings(this.SelectedPlanDocument)
                    : _lastClosedDocument;

                _planDocuments.Add(document);

                //copy accross settings from the prev selected doc.
                if (copySettingsFrom != null)
                {
                    copySettingsFrom.CopyTo(document);
                }
                else
                {
                    document.LoadSettings(_appSettingsView.Model);
                }
            }


            this.SelectedPlanDocument = document;


            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Called when a plan document docking tab is closed.
        /// </summary>
        /// <param name="controller"></param>
        public void CloseDocument(IPlanDocument doc)
        {
            _planDocuments.Remove(doc);

            if (this.SelectedPlanDocument == doc)
            {
                this.SelectedPlanDocument = this.PlanDocuments.FirstOrDefault();
            }

            doc.Dispose();

            //keep a reference to this document so that we can reload settings if necessary
            _lastClosedDocument = new PlanDocumentSettings(doc);

            //if this was the last document then clear the selected items
            if (this.SelectedPlanDocument == null)
            {
                this.SelectedPlanItems.Clear();
            }
        }

        /// <summary>
        /// Closes all documents open for this controller
        /// </summary>
        public void CloseAllDocuments()
        {
            foreach (IPlanDocument doc in this.PlanDocuments.ToList())
            {
                CloseDocument(doc);
            }
        }

        /// <summary>
        /// Updates the IsActiveDocument flag for all documents held by this controller
        /// </summary>
        private void UpdateActiveDocumentFlags()
        {
            Boolean thisIsActive = this.IsActivePlanController;
            IPlanDocument selectedDoc = this.SelectedPlanDocument;

            foreach (IPlanDocument doc in this.PlanDocuments)
            {
                //This controller must be active and the document is the selected one.
                doc.IsActiveDocument =
                    (thisIsActive &&
                    doc == selectedDoc);
            }
        }

        #endregion

        /// <summary>
        /// Pastes the given plan items into this planogram.
        /// </summary>
        /// <param name="planItemList"></param>
        public void Paste(List<IPlanItem> planItemList, Boolean targetless = false)
        {
            base.ShowWaitCursor(true);

            //update the selection to the new items.
            IPlanItem targetItem = null;
            if (!targetless) targetItem = this.SelectedPlanItems.FirstOrDefault();
            this.SelectedPlanItems.Clear();
            this.SelectedPlanItems.AddRange(SourcePlanogram.AddPlanItemCopies(planItemList, targetItem).Where(p => p != null));
            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Removes the currently selected items from this plan.
        /// </summary>
        public void RemoveSelectedItems()
        {
            if (!this.SelectedPlanItems.Any()) return;

            List<IPlanItem> removeItems = this.SelectedPlanItems.ToList();

            //if we have any products selected then check if they are placed or unplaced.
            // For placed products, only remove their linked positions.
            Boolean includesUnplacedProducts = false;
            foreach (IPlanItem item in removeItems.Where(i => i.PlanItemType == PlanItemType.Product).ToArray())
            {
                var positions = item.Product.EnumeratePositionViews().ToArray();

                if (positions.Any())
                {
                    //remove the product and add the positions instead.
                    removeItems.Remove(item);
                    removeItems.AddRange(positions);
                }
                else
                {
                    includesUnplacedProducts = true;
                }
            }


            // If we have any unplaced products then warn the user that
            // they will be removed from the planogram.
            if (includesUnplacedProducts)
            {
                var promptResult =
                CommonHelper.GetWindowService().ShowMessage(MessageWindowType.Warning,
                    Message.RemoveSelectedItems_UnplacedProductWarningHeader,
                    Message.RemoveSelectedItems_UnplacedProductWarningDesc,
                    Message.Generic_DialogOk,
                    Message.Generic_DialogCancel);
                if (promptResult != ModalMessageResult.Button1) return;
            }


            //clear out the current selection.
            this.SelectedPlanItems.Clear();


            //now remove the items from the plan as a single undoable action
            PlanogramView plan = this.SourcePlanogram;
            plan.BeginUndoableAction();
            foreach (IPlanItem item in removeItems.Distinct())
            {
                plan.RemovePlanItem(item);
            }
            plan.EndUndoableAction();

        }

        /// <summary>
        /// Returns the result for the given highlight.
        /// </summary>
        public PlanogramHighlightResult GetHighlightResult(HighlightItem highlight)
        {
            PlanogramHighlightResult result;
            if (!_highlights.TryGetValue(highlight, out result))
            {
                result = this.SourcePlanogram.ProcessHighlight(highlight);
                _highlights[highlight] = result;
            }
            return result;
        }

        /// <summary>
        /// Removes all images from the planogram.
        /// </summary>
        public void RemoveImagesFromPlanogram(Boolean clearProductImages, Boolean clearComponentImages)
        {
            //  Prompt the user to confirm the operation before continuing.
            if (CommonHelper.GetWindowService().ShowMessage(
                MessageWindowType.Warning,
                Message.PromptConfirmation_RemoveImagesFromPlanogram_Header,
                Message.PromptConfirmation_RemoveImagesFromPlanogram_Description,
                Message.Generic_OK, Message.Generic_Cancel) == ModalMessageResult.Button2) return;


            PlanogramView planogram = this.SourcePlanogram;

            if (clearProductImages)
            {
                // Remove any images from the products
                foreach (PlanogramProductView product in planogram.Products)
                {
                    product.Model.ClearImages();
                }
            }

            if (clearComponentImages)
            {
                // Remove any images from the components.
                foreach (PlanogramSubComponentView subComponent in planogram.EnumerateAllSubComponents())
                {
                    subComponent.Model.ClearImages();
                }
            }
            // Remove any orphan image.
            planogram.Model.RemoveOrphanImages();
        }

        /// <summary>
        /// Bakes all external product images into the planogram.
        /// </summary>
        public void AddProductImagesToPlanogram()
        {
            PlanogramView planogram = this.SourcePlanogram;

            planogram.BeginUndoableAction();
            ProductImageHelper.StoreExternalImages(planogram.Products);
            planogram.EndUndoableAction();
        }

        /// <summary>
        /// Handles the filling of an axis within a merchgroup on only the selected positions
        /// </summary>
        /// <param name="axisToFill">The axis we would like to fill the selected positions on.</param>
        /// <remarks>Shrinks the positions down to mimium before filling wide - does not increment from start facings.</remarks>
        public void FillSelectedPositionsOnGivenAxis(AxisType axisToFill)
        {
            base.ShowWaitCursor(true);
            this.SourcePlanogram.FillPositionsOnGivenAxis(axisToFill, this.SelectedPlanItems);
            base.ShowWaitCursor(false);
        }


        /// <summary>
        /// Returns true if zooming can be performed on this controller
        /// </summary>
        /// <returns></returns>
        public Boolean CanZoom(out String disabledReason)
        {
            //must have a selected document.
            if (this.SelectedPlanDocument == null)
            {
                disabledReason = Message.DisabledReason_NoActivePlanogram;
                return false;
            }

            //must be supported
            if (!this.SelectedPlanDocument.IsZoomingSupported)
            {
                disabledReason = Message.DisabledReason_NotSupportedByDocument;
                return false;
            }

            disabledReason = null;
            return true;
        }

        /// <summary>
        /// Zooms to fit on all documents that support it.
        /// </summary>
        public void ZoomToFitAll()
        {
            //Zoom to fit on all documents that support it
            foreach (IPlanDocument doc in this.PlanDocuments)
            {
                if (doc.IsZoomingSupported)
                {
                    doc.ZoomToFit();
                }
            }
        }

        /// <summary>
        /// Zooms to fit on all documents that support it.
        /// </summary>
        public void ZoomToFitHeight()
        {
            //Zoom to fit on all documents that support it
            foreach (IPlanDocument doc in this.PlanDocuments)
            {
                if (doc.IsZoomingSupported)
                {
                    doc.ZoomToFitHeight();
                }
            }
        }

        /// <summary>
        /// Tiles all  plan documents in the given orientation
        /// </summary>
        public void TileWindows(System.Windows.Controls.Orientation orientation)
        {
            //Pass through
            if (this.AttachedControl != null)
            {
                this.AttachedControl.TileWindows(orientation);
            }
        }

        /// <summary>
        /// Docks all plan documents into a single tab group.
        /// </summary>
        public void DockAllViews()
        {
            //Pass through
            if (this.AttachedControl != null)
            {
                this.AttachedControl.DockAllViews();
            }
        }

        /// <summary>
        /// Arranges all plan documents
        /// </summary>
        public void ArrangeAllWindows()
        {
            //Pass through
            if (this.AttachedControl != null)
            {
                this.AttachedControl.ArrangeAllWindows();
            }
        }


        /// <summary>
        /// Sets the ValidationStatus property
        /// </summary>
        private void UpdateValidationStatus()
        {
            if (this.SourcePlanogram == null)
            {
                this.ValidationStatus = ValidationStatusType.None;
            }
            else if (!this.SourcePlanogram.IsPlanUpToDate)
            {
                this.ValidationStatus = ValidationStatusType.Busy;
            }
            else
            {
                Int32 validationWarningCount = this.ValidationWarningsCount;
                this.ValidationStatus = (validationWarningCount != 0) ? ValidationStatusType.Warning : ValidationStatusType.OK;

            }
            OnPropertyChanged(ValidationWarningsCountProperty);
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    if (_sourcePlanogram.ParentPackageView != null)
                    {
                        _sourcePlanogram.ParentPackageView.OnSaveBeginning -= ParentPackageView_OnSaveBeginning;
                        _sourcePlanogram.ParentPackageView.OnSaveCompleted -= ParentPackageView_OnSaveCompleted;
                    }
                    SetPlanEventHandlers(false);

                    //dispose all documents
                    List<IPlanDocument> docList = _planDocuments.ToList();
                    _planDocuments.Clear();
                    docList.ForEach(d => d.Dispose());

                    _lastClosedDocument = null;

                    //dispose of the items
                    _selectedPlanItems.Dispose();
                }

                base.IsDisposed = true;
            }
        }

        #endregion
    }

}
