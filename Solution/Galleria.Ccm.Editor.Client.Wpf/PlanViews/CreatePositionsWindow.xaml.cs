﻿#region Header Information

// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
// V8-24124 : A.Silva ~ Added drag and drop support between datagrids.
#endregion
#region Version History: (CCM 8.0.2)
// V8-25955 : D.Pleasance
//  Rework screen
#endregion
#endregion

using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Editor.Client.Wpf.PlanViews;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.Collections.Generic;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    public sealed partial class CreatePositionsWindow
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof (CreatePositionsViewModel), typeof (CreatePositionsWindow),
                new PropertyMetadata(null, OnViewModelPropertyChanged));

        public CreatePositionsViewModel ViewModel
        {
            get { return (CreatePositionsViewModel) GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = (CreatePositionsWindow) obj;

            if (e.OldValue != null)
            {
                var oldModel = (CreatePositionsViewModel) e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                var newModel = (CreatePositionsViewModel) e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        ///     Constructor
        /// </summary>
        public CreatePositionsWindow(PlanogramView plan, PlanogramSubComponentView planogramSubComponentView)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            ViewModel = new CreatePositionsViewModel(plan, planogramSubComponentView);

            Loaded += This_Loaded;
        }

        /// <summary>
        ///     Called on initial load of this window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void This_Loaded(object sender, RoutedEventArgs e)
        {
            Loaded -= This_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action) (() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Events

        private void ProductFilter_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel != null)
            {
                ListBoxItem item = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
                if (item != null)
                {
                    this.ViewModel.SelectedProduct = item.Content as PlanogramProductView;
                    Keyboard.ClearFocus();
                }
            }
        }

        #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
                (Action) (() =>
                {
                    if (ViewModel != null)
                    {
                        IDisposable dis = ViewModel;
                        ViewModel = null;

                        if (dis != null)
                        {
                            dis.Dispose();
                        }
                    }
                }), DispatcherPriority.Background);
        }

        #endregion       
    }      
}