﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// L.Hodson ~ Created.
// V8-24265 : J.Pickup
//  LabelSettingsView is now LabelItem
// CCM-24265 : N.Haywood
//  Added custom labels
// CCM-25963 : N.Haywood
//  Added missing OnPropertyChanged for SelectedFixtureLabelProperty
// V8-26248  : L.Luong
//  Changed IsCustomProductLabelSelectedProperty and IsCustomFixtureLabelSelectedProperty to match new design
// V8-25436  : L.Luong
//  Added custom highlights
// V8-27239 : I.George
// changed to use properties from the UserEditorSettings
// V8-27825 : L.Ineson
//  Added document freezing

#endregion

#region Version History: (CCM 8.0.2)

// V8-29039 : D.Pleasance
//  Added OnShowProductImagesChanged, so that image provider is started to fetch images or paused if images are not required.

#endregion

#region Version History: (CCM 8.1.1)

// V8-28985 : A.Probyn
//  Added busy cursor to OnShowProductImagesChanged

#endregion

#region Version History: CCM820

// V8-30472 : A.Silva
//  Added OnActiveDocumentChanged so that inheritor classes can act when the document becomes active/inactive.
// V8-30792 : A.Kuszyk
//  Added ShowFixtures property to satisfy IPlanRenderSettings. Not settable from the UI.
// V8-30932 : L.Ineson
//  Updated selection helper methods.
#endregion

#region Version History: CCM830
// V8-32135 : L.Ineson
//  Added IsVisible to allow the ability to suspend updates whilst the document is not actually in view.
// V8-32322 : A.Silva
//  Added LongUiProcess wrapper to automatically show and hide the busy cursor.
// V8-32389  : J.Pickup
//  Toggle functionality (highlight, labels).
// V8-32636 : A.Probyn
//  Added ShowAnnotations
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Services;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews
{
    /// <summary>
    /// Abstract class implementing IPlanDocument
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class PlanDocument<T> : ViewModelAttachedControlObject<T>, IPlanDocument
        where T : System.Windows.FrameworkElement
    {
        #region Fields

        private PlanControllerViewModel _parentController;
        private Boolean _isActiveDocument;
        private Boolean _isFrozen;

        private Boolean _isZoomingSupported;
        private Boolean _isInPanningMode;
        private Boolean _isInZoomToAreaMode;

        private Boolean _isHighlightingSupported;
        private HighlightItem _highlight;
        private Boolean _isHighlightLegendVisible;

        private Boolean _arePlanogramViewSettingsSupported;
        private Boolean _showPositions = true;
        private Boolean _showPositionUnits = true;
        private Boolean _showProductImages = false;
        private Boolean _showProductShapes = false;
        private Boolean _showProductFillPatterns = false;
        private Boolean _showFixtureImages = false;
        private Boolean _showFixtureFillPatterns = false;
        private Boolean _showChestWalls = false;
        private Boolean _showChestsTopDown = false;
        private Boolean _showShelfRisers = true;
        private Boolean _showNotches = true;
        private Boolean _showPegHoles = true;
        private Boolean _showPegs = true;
        private Boolean _showSlotLines = true;
        private Boolean _showDividers = true;
        private Boolean _showAnnotations = true;
        private LabelItem _productLabel;
        private LabelItem _fixtureLabel;
        private Boolean _showGridlines = true;
        private Boolean _showWireframeOnly = false;
        private LabelItem _customProductLabel = null;
        private LabelItem _customFixtureLabel = null;
        private HighlightItem _customHighlight = null;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath IsActiveDocumentProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.IsActiveDocument);
        public static readonly PropertyPath TitleProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.Title);
        public static readonly PropertyPath PlanogramProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.Planogram);
        public static readonly PropertyPath SelectedPlanItemsProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.SelectedPlanItems);

        public static readonly PropertyPath IsZoomingSupportedProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.IsZoomingSupported);
        public static readonly PropertyPath IsInPanningModeProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.IsInPanningMode);
        public static readonly PropertyPath IsInZoomToAreaModeProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.IsInZoomToAreaMode);

        public static readonly PropertyPath IsHighlightingSupportedProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.IsHighlightingSupported);
        public static readonly PropertyPath HighlightProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.Highlight);
        public static readonly PropertyPath HighlightLegendItemsProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.HighlightLegendItems);
        public static readonly PropertyPath IsHighlightLegendVisibleProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.IsHighlightLegendVisible);

        public static readonly PropertyPath ArePlanogramViewSettingsSupportedProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.ArePlanogramViewSettingsSupported);
        public static readonly PropertyPath ShowPositionsProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.ShowPositions);
        public static readonly PropertyPath ShowPositionUnitsProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.ShowPositionUnits);
        public static readonly PropertyPath ShowProductImagesProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.ShowProductImages);
        public static readonly PropertyPath ShowProductShapesProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.ShowProductShapes);
        public static readonly PropertyPath ShowProductFillPatternsProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.ShowProductFillPatterns);
        public static readonly PropertyPath ShowFixtureImagesProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.ShowFixtureImages);
        public static readonly PropertyPath ShowFixtureFillPatternsProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.ShowFixtureFillPatterns);
        public static readonly PropertyPath ShowChestWallsProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.ShowChestWalls);
        public static readonly PropertyPath ShowChestsTopDownProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.RotateTopDownComponents);
        public static readonly PropertyPath ShowShelfRisersProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.ShowShelfRisers);
        public static readonly PropertyPath ShowNotchesProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.ShowNotches);
        public static readonly PropertyPath ShowPegHolesProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.ShowPegHoles);
        public static readonly PropertyPath ShowPegsProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.ShowPegs);
        public static readonly PropertyPath ShowDividerLinesProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.ShowDividerLines);
        public static readonly PropertyPath ShowDividersProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.ShowDividers);
        public static readonly PropertyPath ProductLabelProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.ProductLabel);
        public static readonly PropertyPath FixtureLabelProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.FixtureLabel);
        public static readonly PropertyPath ShowGridlinesProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.ShowGridlines);
        public static readonly PropertyPath ShowWireframeOnlyProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.ShowWireframeOnly);
        public static readonly PropertyPath SuppressPerformanceIntensiveUpdatesProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.SuppressPerformanceIntensiveUpdates);
        public static readonly PropertyPath CustomProductLabelProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.CustomProductLabel);
        public static readonly PropertyPath IsCustomProductLabelSelectedProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.IsCustomProductLabelSelected);
        public static readonly PropertyPath CustomFixtureLabelProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.CustomFixtureLabel);
        public static readonly PropertyPath IsCustomFixtureLabelSelectedProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.IsCustomFixtureLabelSelected);
        public static readonly PropertyPath CustomHighlightProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.CustomHighlight);
        public static readonly PropertyPath IsCustomHighlightSelectedProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.IsCustomHighlightSelected);
        public static readonly PropertyPath ShowAnnotationsProperty = WpfHelper.GetPropertyPath<IPlanDocument>(p => p.ShowAnnotations);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the parent controller of this document.
        /// </summary>
        protected PlanControllerViewModel ParentController
        {
            get { return _parentController; }
        }

        /// <summary>
        /// Gets/Sets whether this is the active document for the application.
        /// </summary>
        public Boolean IsActiveDocument
        {
            get { return _isActiveDocument; }
            set
            {
                _isActiveDocument = value;
                OnPropertyChanged(IsActiveDocumentProperty);
                OnActiveDocumentChanged(value);

                //auto turn off panning mode
                if (!_isActiveDocument && this.IsInPanningMode)
                {
                    this.IsInPanningMode = false;
                }
            }
        }

        /// <summary>
        /// Returns the document title.
        /// </summary>
        public abstract String Title { get; }

        /// <summary>
        /// Returns the attached document view.
        /// </summary>
        public IPlanDocumentView AttachedDocumentView
        {
            get { return this.AttachedControl as IPlanDocumentView; }
        }

        /// <summary>
        /// Returns true if zooming is supported
        /// </summary>
        public Boolean IsZoomingSupported
        {
            get { return _isZoomingSupported; }
            protected set
            {
                _isZoomingSupported = value;
                OnPropertyChanged(IsZoomingSupportedProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether the document is in panning mode.
        /// Requires IsZoomingSupported = true.
        /// </summary>
        public Boolean IsInPanningMode
        {
            get { return _isInPanningMode; }
            set
            {
                _isInPanningMode = value;
                OnPropertyChanged(IsInPanningModeProperty);

                //Turn off Zoom to area mode
                if (this.IsInZoomToAreaMode)
                {
                    this.IsInZoomToAreaMode = false;
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether the canvas is in zoom to area mode.
        /// </summary>
        public Boolean IsInZoomToAreaMode
        {
            get { return _isInZoomToAreaMode; }
            set
            {
                _isInZoomToAreaMode = value;
                OnPropertyChanged(IsInZoomToAreaModeProperty);

                //Turn off panning mode
                if (this.IsInPanningMode)
                {
                    this.IsInPanningMode = false;
                }
            }
        }

        /// <summary>
        /// Returns true if highlighting is supported
        /// </summary>
        public Boolean IsHighlightingSupported
        {
            get { return _isHighlightingSupported; }
            protected set
            {
                _isHighlightingSupported = value;
                OnPropertyChanged(IsHighlightingSupportedProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the current highlight
        /// </summary>
        public HighlightItem Highlight
        {
            get { return _highlight; }
            set
            {
                if (_highlight != value)
                {
                    _highlight = value;
                    OnPropertyChanged(HighlightProperty);
                    OnPropertyChanged(IsCustomHighlightSelectedProperty);
                }
            }
        }

        /// <summary>
        /// Returns the collection of legend items processed for the highlight.
        /// </summary>
        public virtual ReadOnlyBulkObservableCollection<PlanogramHighlightResultGroup> HighlightLegendItems
        {
            get
            {
                Debug.Assert(!this.IsHighlightingSupported, "Should have been overriden");
                return new ReadOnlyBulkObservableCollection<PlanogramHighlightResultGroup>(new BulkObservableCollection<PlanogramHighlightResultGroup>());
            }
        }

        /// <summary>
        /// Gets/Sets whether the highlight legend should be displayed.
        /// </summary>
        public Boolean IsHighlightLegendVisible
        {
            get { return _isHighlightLegendVisible; }
            set
            {
                _isHighlightLegendVisible = value;
                OnPropertyChanged(IsHighlightLegendVisibleProperty);
            }
        }

        /// <summary>
        /// Returns true if planogram view settings are supported by this document.
        /// </summary>
        public Boolean ArePlanogramViewSettingsSupported
        {
            get { return _arePlanogramViewSettingsSupported; }
            protected set
            {
                _arePlanogramViewSettingsSupported = value;
                OnPropertyChanged(ArePlanogramViewSettingsSupportedProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether positions should be shown.
        /// Only used if IsPositionShowHideSupported=true
        /// </summary>
        public Boolean ShowPositions
        {
            get { return _showPositions; }
            set
            {
                if (_showPositions != value)
                {
                    _showPositions = value;
                    OnPropertyChanged(ShowPositionsProperty);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether individual units should be shown for positions.
        /// </summary>
        public Boolean ShowPositionUnits
        {
            get { return _showPositionUnits; }
            set
            {
                if (_showPositionUnits != value)
                {
                    _showPositionUnits = value;
                    OnPropertyChanged(ShowPositionUnitsProperty);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether images should be shown.
        /// Only used if IsPositionShowHideSupported=true
        /// </summary>
        public Boolean ShowProductImages
        {
            get { return _showProductImages; }
            set
            {
                if (_showProductImages != value)
                {
                    _showProductImages = value;
                    OnPropertyChanged(ShowProductImagesProperty);
                }
            }
        }
        
        /// <summary>
        /// Gets/Sets whether shapes should be shown.
        /// Only used if IsPositionShowHideSupported=true
        /// </summary>
        public Boolean ShowProductShapes
        {
            get { return _showProductShapes; }
            set
            {
                if (_showProductShapes != value)
                {
                    _showProductShapes = value;
                    OnPropertyChanged(ShowProductShapesProperty);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether product fill patterns should be shown.
        /// Only used if IsPositionShowHideSupported=true
        /// </summary>
        public Boolean ShowProductFillPatterns
        {
            get { return _showProductFillPatterns; }
            set
            {
                if (_showProductFillPatterns != value)
                {
                    _showProductFillPatterns = value;
                    OnPropertyChanged(ShowProductFillPatternsProperty);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether images should be shown.
        /// Only used if IsPositionShowHideSupported=true
        /// </summary>
        public Boolean ShowFixtureImages
        {
            get { return _showFixtureImages; }
            set
            {
                if (_showFixtureImages != value)
                {
                    _showFixtureImages = value;
                    OnPropertyChanged(ShowFixtureImagesProperty);
                }
            }
        }

        /// <summary>
        /// This property is present to satisfy IPlanRenderSettings, but isn't settable in this UI at present (V8-30792).
        /// </summary>
        public Boolean ShowFixtures
        {
            get { return true; }
            set { /* Do nothing */ }
        }

        /// <summary>
        /// Gets/Sets whether fixture fill patterns should be shown.
        /// Only used if IsPositionShowHideSupported=true
        /// </summary>
        public Boolean ShowFixtureFillPatterns
        {
            get { return _showFixtureFillPatterns; }
            set
            {
                if (_showFixtureFillPatterns != value)
                {
                    _showFixtureFillPatterns = value;
                    OnPropertyChanged(ShowFixtureFillPatternsProperty);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether chest walls should be displayed.
        /// </summary>
        public Boolean ShowChestWalls
        {
            get { return _showChestWalls; }
            set
            {
                if (_showChestWalls != value)
                {
                    _showChestWalls = value;
                    OnPropertyChanged(ShowChestWallsProperty);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether chests should be shown top down
        /// </summary>
        public Boolean RotateTopDownComponents
        {
            get { return _showChestsTopDown; }
            set
            {
                _showChestsTopDown = value;
                OnPropertyChanged(ShowChestsTopDownProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether shelf risers should be shown.
        /// </summary>
        public Boolean ShowShelfRisers
        {
            get { return _showShelfRisers; }
            set
            {
                _showShelfRisers = value;
                OnPropertyChanged(ShowShelfRisersProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether notches should be shown.
        /// </summary>
        public Boolean ShowNotches
        {
            get { return _showNotches; }
            set
            {
                _showNotches = value;
                OnPropertyChanged(ShowNotchesProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether peg holes should be shown
        /// </summary>
        public Boolean ShowPegHoles
        {
            get { return _showPegHoles; }
            set
            {
                _showPegHoles = value;
                OnPropertyChanged(ShowPegHolesProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether pegs should be shown
        /// </summary>
        public Boolean ShowPegs
        {
            get { return _showPegs; }
            set
            {
                _showPegs = value;
                OnPropertyChanged(ShowPegsProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether slot lines should be shown
        /// </summary>
        public Boolean ShowDividerLines
        {
            get { return _showSlotLines; }
            set
            {
                _showSlotLines = value;
                OnPropertyChanged(ShowDividerLinesProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether dividers should be shown.
        /// </summary>
        public Boolean ShowDividers
        {
            get { return _showDividers; }
            set
            {
                _showDividers = value;
                OnPropertyChanged(ShowDividersProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the current product label setting.
        /// </summary>
        public LabelItem ProductLabel
        {
            get { return _productLabel; }
            set
            {
                if (_productLabel != value)
                {
                    _productLabel = value;
                    OnPropertyChanged(ProductLabelProperty);
                    OnPropertyChanged(IsCustomProductLabelSelectedProperty);
                }
            }
        }

        /// <summary>
        /// Gets/Sets the current fixture label setting.
        /// </summary>
        public LabelItem FixtureLabel
        {
            get { return _fixtureLabel; }
            set
            {
                if (_fixtureLabel != value)
                {
                    _fixtureLabel = value;
                    OnPropertyChanged(FixtureLabelProperty);
                    OnPropertyChanged(IsCustomFixtureLabelSelectedProperty);
                }
            }
        }

        /// <summary>
        /// Gets/Sets whether grid lines should be shown
        /// </summary>
        public Boolean ShowGridlines
        {
            get { return _showGridlines; }
            set
            {
                _showGridlines = value;
                OnPropertyChanged(ShowGridlinesProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether the model should be shown as a wireframe
        /// </summary>
        public Boolean ShowWireframeOnly
        {
            get { return _showWireframeOnly; }
            set
            {
                _showWireframeOnly = value;
                OnPropertyChanged(ShowWireframeOnlyProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the custom product label
        /// </summary>
        public LabelItem CustomProductLabel
        {
            get { return _customProductLabel; }
            set
            {
                _customProductLabel = value;
                OnPropertyChanged(CustomProductLabelProperty);
                OnPropertyChanged(IsCustomProductLabelSelectedProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether the custom product label is selected
        /// </summary>
        public Boolean IsCustomProductLabelSelected
        {
            get
            {
                if (_productLabel == null)
                {
                    return false;
                }
                if (_productLabel != null &&
                    _productLabel.Name != "")
                {
                    return false;
                }
                return _customProductLabel == _productLabel;
            }
            set
            {
                _productLabel = _customProductLabel;
                OnPropertyChanged(IsCustomProductLabelSelectedProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the custom fixture label
        /// </summary>
        public LabelItem CustomFixtureLabel
        {
            get { return _customFixtureLabel; }
            set
            {
                _customFixtureLabel = value;
                OnPropertyChanged(CustomFixtureLabelProperty);
                OnPropertyChanged(IsCustomFixtureLabelSelectedProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether the custom fixture label is selected
        /// </summary>
        public Boolean IsCustomFixtureLabelSelected
        {
            get
            {
                if (_fixtureLabel == null)
                {
                    return false;
                }
                if (_fixtureLabel != null &&
                    _fixtureLabel.Name != "")
                {
                    return false;
                }
                return _customFixtureLabel == _fixtureLabel;
            }
            set
            {
                _fixtureLabel = _customFixtureLabel;
                OnPropertyChanged(IsCustomFixtureLabelSelectedProperty);
            }
        }

        public HighlightItem CustomHighlight
        {
            get { return _customHighlight; }
            set
            {
                _customHighlight = value;
                OnPropertyChanged(CustomHighlightProperty);
                OnPropertyChanged(IsCustomHighlightSelectedProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether the custom fixture label is selected
        /// </summary>
        public Boolean IsCustomHighlightSelected
        {
            get
            {
                if (_highlight == null)
                {
                    return false;
                }
                if (_highlight != null &&
                    _highlight.Name != "")
                {
                    return false;
                }
                return _customHighlight == _highlight;
            }
            set
            {
                _highlight = _customHighlight;
                OnPropertyChanged(IsCustomHighlightSelectedProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether positions should be shown.
        /// Only used if IsPositionShowHideSupported=true
        /// </summary>
        public Boolean ShowAnnotations
        {
            get { return _showAnnotations; }
            set
            {
                if (_showAnnotations != value)
                {
                    _showAnnotations = value;
                    OnPropertyChanged(ShowAnnotationsProperty);
                }
            }
        }

        #region Toggle Placeholders

        /// <summary>
        /// Holds the temporary placeholder Product Label Item that is assigned but toggled on/off.
        /// </summary>
        public LabelItem ToggleProductLabelItem { get; set; }

        /// <summary>
        /// Holds the temporary placeholder Fixture Label Item that is assigned but toggled on/off.
        /// </summary>
        public LabelItem ToggleFixtureLabelItem { get; set; }

        /// <summary>
        /// Holds the temporary placeholder Product Label Item that is assigned but toggled on/off.
        /// </summary>
        public HighlightItem ToggleHighlightItem { get; set; }

        #endregion

        #region Controller Inherited Properties

        /// <summary>
        /// Gets the source planogram model.
        /// </summary>
        public PlanogramView Planogram
        {
            get { return _parentController.SourcePlanogram; }
        }

        /// <summary>
        /// Returns the view of selected plan items.
        /// </summary>
        public PlanItemSelection SelectedPlanItems
        {
            get { return _parentController.SelectedPlanItems; }
        }

        /// <summary>
        /// Gets/Sets whether intensive updates should be supressed.
        /// </summary>
        public Boolean SuppressPerformanceIntensiveUpdates
        {
            get { return _parentController.SuppressPerformanceIntensiveUpdates; }
            set { _parentController.SuppressPerformanceIntensiveUpdates = value; }
        }

        #endregion


        public abstract DocumentType DocumentType { get; }

        /// <summary>
        /// Returns true if this document is actually in view.
        /// </summary>
        public Boolean IsVisible
        {
            get
            {
                if (App.IsUnitTesting) return true;

                if (this.AttachedControl == null) return false;

                return this.AttachedControl.IsVisible;
            }
        }

        #endregion

        #region Events

        #region Zooming

        /// <summary>
        /// Requests that a zoom be performed
        /// </summary>
        public event EventHandler<ZoomEventArgs> Zooming;

        private void OnZooming(ZoomType type, Object args = null)
        {
            if (Zooming != null)
            {
                Zooming(this, new ZoomEventArgs(type, args));
            }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanDocument(PlanControllerViewModel parentController)
        {
            _parentController = parentController;
            _parentController.PropertyChanged += ParentController_PropertyChanged;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on the parent controller.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ParentController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == PlanControllerViewModel.SupressPerformanceIntensiveUpdatesProperty.Path)
            {
                OnPropertyChanged(SuppressPerformanceIntensiveUpdatesProperty);
            }

            //pass through.
            OnParentControllerPropertyChanged(e.PropertyName);
        }

        /// <summary>
        /// Called whenever a property changes on the parent controller.
        /// </summary>
        protected virtual void OnParentControllerPropertyChanged(String propertyName)
        {
            //override if required.
        }

        /// <summary>
        /// Sets event handlers when the attached control changes
        /// </summary>
        protected override void OnAttachedControlChanged(T oldControl, T newControl)
        {
            if (oldControl != null)
            {
                oldControl.IsVisibleChanged -= AttachedControl_IsVisibleChanged;
            }

            if (newControl != null)
            {
                newControl.IsVisibleChanged += AttachedControl_IsVisibleChanged;
                if(this.IsVisible) OnIsVisibleChanged();
            }

            base.OnAttachedControlChanged(oldControl, newControl);

        }

        /// <summary>
        /// Called whenever the is visible state of the attached control changes.
        /// </summary>
        private void AttachedControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.IsDisposed) return;

            OnPropertyChanged("IsVisible");
            OnIsVisibleChanged();
        }

        /// <summary>
        /// Called whenever the visibility of this document changes.
        /// </summary>
        protected virtual void OnIsVisibleChanged()
        {
            //override if required.
        }

        #endregion

        #region Methods

        /// <summary>
        /// Loads settings
        /// </summary>
        /// <param name="settings"></param>
        public void LoadSettings(UserEditorSettings settings)
        {
            this.ShowPositionUnits = settings.PositionUnits;
            this.ShowProductImages = settings.ProductImages;
            this.ShowProductShapes = settings.ProductShapes;
            this.ShowProductFillPatterns = settings.ProductFillPatterns;
            this.ShowFixtureImages = settings.FixtureImages;
            this.ShowFixtureFillPatterns = settings.FixtureFillPatterns;
            this.ShowChestWalls = settings.ChestWalls;
            this.RotateTopDownComponents = settings.RotateTopDownComponents;
            this.ShowShelfRisers = settings.ShelfRisers;
            this.ShowNotches = settings.Notches;
            this.ShowPegHoles = settings.PegHoles;
            this.ShowPegs = settings.Pegs;
            this.ShowDividers = settings.Dividers;
            this.ShowDividerLines = settings.DividerLines;
            this.ShowAnnotations = settings.Annotations;

            //this.ProductLabel = settings.ProductLabel;
            //this.FixtureLabel = settings.FixtureLabel;
        }

        /// <summary>
        /// Copies the settings of the given doc to this one.
        /// </summary>
        /// <param name="sourceDoc"></param>
        public void LoadSettings(IPlanDocument sourceDoc)
        {
            this.Highlight = sourceDoc.Highlight;
            this.IsHighlightLegendVisible = sourceDoc.IsHighlightLegendVisible;
            this.ShowPositions = sourceDoc.ShowPositions;
            this.ShowPositionUnits = sourceDoc.ShowPositionUnits;
            this.ShowProductImages = sourceDoc.ShowProductImages;
            this.ShowProductShapes = sourceDoc.ShowProductShapes;
            this.ShowProductFillPatterns = sourceDoc.ShowProductFillPatterns;
            this.ShowFixtureImages = sourceDoc.ShowFixtureImages;
            this.ShowFixtureFillPatterns = sourceDoc.ShowFixtureFillPatterns;
            this.ShowChestWalls = sourceDoc.ShowChestWalls;
            this.RotateTopDownComponents = sourceDoc.RotateTopDownComponents;
            this.ShowShelfRisers = sourceDoc.ShowShelfRisers;
            this.ShowNotches = sourceDoc.ShowNotches;
            this.ShowPegHoles = sourceDoc.ShowPegHoles;
            this.ShowPegs = sourceDoc.ShowPegs;
            this.ShowDividers = sourceDoc.ShowDividers;
            this.ShowDividerLines = sourceDoc.ShowDividerLines;
            this.ProductLabel = sourceDoc.ProductLabel;
            this.FixtureLabel = sourceDoc.FixtureLabel;
            this.ShowGridlines = sourceDoc.ShowGridlines;
            this.ShowWireframeOnly = sourceDoc.ShowWireframeOnly;
            this.ShowAnnotations = sourceDoc.ShowAnnotations;
        }

        /// <summary>
        /// Updates the main status bar text
        /// </summary>
        /// <param name="hoveredItem"></param>
        public void UpdateStatusBarText(IPlanItem hoveredItem)
        {
            String newStatusText = null;

            if (hoveredItem != null)
            {
                var settings = App.ViewState.Settings.Model;

                if (hoveredItem.PlanItemType == PlanItemType.Position)
                {
                    newStatusText = hoveredItem.Position.GetLabelText(settings.PositionHoverStatusBarText);
                }
                else if (hoveredItem.Component != null)
                {
                    PlanogramComponentView comp = hoveredItem.Component;
                    if (hoveredItem.Component == null && hoveredItem.PlanItemType == PlanItemType.Assembly)
                    {
                        comp = hoveredItem.Assembly.Components.FirstOrDefault();
                    }

                    if (comp != null)
                    {
                        newStatusText = comp.GetLabelText(settings.ComponentHoverStatusBarText);
                    }
                }

            }

            App.MainPageViewModel.StatusBarText = newStatusText;
        }

        /// <summary>
        /// Returns true if the user is allowed to click select the given plan item.
        /// </summary>
        public Boolean CanUserSelect(IPlanItem item)
        {
            if (item == null) return false;

            PlanItemSelectionType selectMode = App.ViewState.SelectionMode;

            Boolean canSelectComponents =
                selectMode != PlanItemSelectionType.OnlyPositions
                && selectMode != PlanItemSelectionType.AllProductPositions;

            Boolean canSelectPositions = 
                selectMode != PlanItemSelectionType.OnlyComponents;


            //check if component selection is allowed.
            if (PlanItemHelper.IsFixtureType(item.PlanItemType) && !canSelectComponents) 
                return false;


            //check if position selection is allowed.
            if (item.PlanItemType == PlanItemType.Position && !canSelectPositions) 
                return false;


            return true;
        }

        /// <summary>
        /// Returns all items that should be selected if the linked one is.
        /// Includes the item.
        /// </summary>
        /// <returns></returns>
        protected IEnumerable<IPlanItem> GetSelectionModeLinkedItems(IPlanItem item)
        {
            switch (App.ViewState.SelectionMode)
            {
                case PlanItemSelectionType.AllProductPositions:
                case PlanItemSelectionType.ProductsAndComponents:
                    {
                        if (item.PlanItemType == PlanItemType.Position
                            || item.PlanItemType == PlanItemType.Product)
                        {
                            //select other positions for the same product
                            Object productId = item.Product.Model.Id;

                            return
                                this.Planogram.EnumerateAllPositions()
                                .Where(p => Object.Equals(productId, p.Model.PlanogramProductId))
                                .ToArray();
                        }
                    }
                    break;
            }

            return new IPlanItem[] { item };
        }

        /// <summary>
        /// Processes the keyboard shortcut against any document 
        /// specific actions.
        /// </summary>
        /// <param name="modifiers"></param>
        /// <param name="key"></param>
        public virtual Boolean ProcessKeyboardShortcut(ModifierKeys modifiers, Key key)
        {
            //override if required.
            return false;
        }

        /// <summary>
        /// Notifies this document that plan processing is completing.
        /// </summary>
        public virtual void OnPlanProcessCompleting()
        {
            //override as required.
        }

        /// <summary>
        ///     Whenever this instance's IsActiveDocument as changed.
        /// </summary>
        /// <param name="isActive">Current IsActiveDocument state for this instance.</param>
        protected virtual void OnActiveDocumentChanged(Boolean isActive)
        {
            //  Override if required.
        }

        #region Zooming

        /// <summary>
        /// Zooms to fit the entire plan bounds
        /// </summary>
        public void ZoomToFit()
        {
            OnZooming(ZoomType.ZoomToFit);
        }

        /// <summary>
        /// Zooms to fit the plan by height
        /// </summary>
        public void ZoomToFitHeight()
        {
            OnZooming(ZoomType.ZoomToFitHeight);
        }

        /// <summary>
        /// Zoom to fit the plan width
        /// </summary>
        public void ZoomToFitWidth()
        {
            OnZooming(ZoomType.ZoomToFitWidth);
        }

        /// <summary>
        /// Zooms in on the plan
        /// </summary>
        public void ZoomIn()
        {
            OnZooming(ZoomType.ZoomIn);
        }

        /// <summary>
        /// Zooms the plan out
        /// </summary>
        public void ZoomOut()
        {
            OnZooming(ZoomType.ZoomOut);
        }

        /// <summary>
        /// Zooms to fit the currently selected plan items.
        /// </summary>
        public void ZoomSelectedPlanItems()
        {
            OnZooming(ZoomType.ZoomItems, this.SelectedPlanItems.ToList());
        }

        #endregion

        #region Freeze

        /// <summary>
        /// Gets/Sets whether this document has been frozen.
        /// </summary>
        protected Boolean IsFrozen
        {
            get { return _isFrozen; }
            set { _isFrozen = value; }
        }


        void IPlanDocument.Freeze()
        {
            if (!_isFrozen)
            {
                OnFreeze();
                _isFrozen = true;
            }
        }

        /// <summary>
        /// Stops this document from updating
        /// or reacting to changes
        /// </summary>
        protected abstract void OnFreeze();

        void IPlanDocument.Unfreeze()
        {
            if (_isFrozen)
            {
                OnUnfreeze();
                _isFrozen = false;
            }
        }

        /// <summary>
        /// Refreshes and allows this document to update
        /// and react to changes again.
        /// </summary>
        protected abstract void OnUnfreeze();

        #endregion

        /// <summary>
        /// Returns the active window service.
        /// </summary>
        protected IWindowService GetWindowService()
        {
            return Galleria.Ccm.Services.ServiceContainer.GetService<IWindowService>();
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                _parentController.PropertyChanged -= ParentController_PropertyChanged;

                base.IsDisposed = true;
            }
        }

        #endregion

    }

}
