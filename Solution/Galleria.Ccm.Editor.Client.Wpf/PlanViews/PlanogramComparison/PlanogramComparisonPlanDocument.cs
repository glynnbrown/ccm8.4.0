﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31742 : A.Silva
//  Created
// V8-31745 : A.Silva
//  Added support for the row collection and view type change.
// V8-31747 : A.Silva
//  Added commands for the context tab.
// V8-31763 : A.Silva
//  Added showing Planogram Comparison Settings Window.
// V8-31819 : A.Silva
//  Updated to use the Planogram Comparison model objects.
// V8-31862 : A.Silva
//  Added PlanogramResultStatus as a dictionary wrapper that hides any missing keys in the dictionary.
// V8-31871 : A.Silva
//  Amended Refresh Command to become Compare Command. Changed Can Execute rules.
// V8-31885 : A.Silva
//  Added handling for the Comparison Results Updated event.
// V8-31873 : A.Silva
//  Added HasComparisonResults and updated the view each time this changed. Now the list is filtered by this as well.
// V8-31863 : A.Silva
//  Added support for comparing positions.
// V8-31881 : A.Silva
//  Amended PlanogramComparisonRow to expose more properties to display on the list.
// V8-31912 : A.Silva
//  Amended to display values for other planograms in the comparison IF they are open.
// V8-31920 : A.Silva
//  Added updating selection of products on other lists.
// V8-31961 : A.Silva
//  Added Date Last Compared Property.
// V8-31960 : A.Silva
//  Added code to control the Planograms to Compare panel.
// V8-31967 : A.Silva
//  Added new properties to diplay field values and master planogram status through PlanogramComparisonRow.
// V8-32063 : A.Silva
//  Implemented ICalculatedValueResolver for Planogram Row.
// V8-32732 : A.Silva
//  Amended an incorrect status when the item was missing from the master plan (now correctly marked as Not Found).
// V8-32930 : A.Silva
//  Renamed Position ID column to Position instance and changed the display to just the instance number (without the GTIN).
// V8-32931 : A.Silva
//  Amended NewPlanogramComparisonRow so that items missing from the Master Plan are set as Status.NotFound.

#endregion

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Editor.Client.Wpf.Common;
using Galleria.Ccm.Editor.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;
using PlanComparison = Galleria.Framework.Planograms.Model.PlanogramComparison;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.PlanogramComparison
{
    /// <summary>
    ///     <see cref="PlanDocument{PlanogramComaprePlanDocumentView}"/> controller to compare one planogram to others.
    /// </summary>
    public sealed class PlanogramComparisonPlanDocument : PlanDocument<PlanogramComparisonPlanDocumentView>
    {
        #region Constants

        /// <summary>
        ///     Constant value to indicate a Silent execution of DoCompare(Boolean).
        /// </summary>
        private const Boolean Silent = true;

        #endregion

        #region Fields

        /// <summary>
        ///     Current state of subscription for the event handlers.
        /// </summary>
        private Boolean _areEventHandlersSubscribed;
        
        private readonly ObservableCollection<PlanogramComparisonPlanogramRow> _availablePlans = new ObservableCollection<PlanogramComparisonPlanogramRow>();
        
        private DateTime? _dateLastCompared;

        /// <summary>
        ///     Currently selected <see cref="PlanItemType"/> to display on the list.
        /// </summary>
        private PlanogramComparisonFilterType _filterType = PlanogramComparisonFilterType.Products;
        
        private Boolean _hasComparisonResults;
        
        /// <summary>
        ///     Whether to <see cref="IncludeAllPlanograms"/> in the comparison or not.
        /// </summary>
        private Boolean _includeAllPlanograms = true;

        /// <summary>
        ///     Whether the selection collections are being synchronized at the moment.
        /// </summary>
        private Boolean _isSyncingSelection;

        private Boolean _isShowUnchanged;

        /// <summary>
        ///     The master collection of <see cref="PlanogramComparisonRow"/> in the planogram.
        /// </summary>
        private readonly List<PlanogramComparisonRow> _masterRows = new List<PlanogramComparisonRow>();

        private Boolean _planogramPanelIsExpanded;

        /// <summary>
        ///     The read only collection of displayed <see cref="PlanogramComparisonRow"/>.
        /// </summary>
        private ReadOnlyBulkObservableCollection<PlanogramComparisonRow> _readOnlyRows;

        /// <summary>
        ///     The collection of displayed <see cref="PlanogramComparisonRow"/>.
        /// </summary>
        private readonly BulkObservableCollection<PlanogramComparisonRow> _rows = new BulkObservableCollection<PlanogramComparisonRow>();

        /// <summary>
        ///     The collection of currently selected <see cref="PlanogramComparisonRow"/>.
        /// </summary>
        private readonly ObservableCollection<PlanogramComparisonRow> _selectedRows = new ObservableCollection<PlanogramComparisonRow>();

        private Boolean _suppressPlanPropertyChange;

        /// <summary>
        ///     <see cref="DispatcherTimer"/> instance to control delayed syncing of newly selected rows.
        /// </summary>
        private readonly DispatcherTimer _syncAddTimer;

        /// <summary>
        ///     The queue of newly selected plan items to be synced.
        /// </summary>
        private readonly List<IPlanItem> _syncAddQueue = new List<IPlanItem>();

        /// <summary>
        ///     <see cref="DispatcherTimer"/> instance to control delayed syncing of newly deselected rows.
        /// </summary>
        private readonly DispatcherTimer _syncRemoveTimer;

        /// <summary>
        ///     The queue of newly deselected plan items to be synced.
        /// </summary>
        private readonly List<IPlanItem> _syncRemoveQueue = new List<IPlanItem>();

        #endregion

        #region Binding Property Paths

        /// <summary>
        ///     <see cref="PropertyPath"/> definition for the <see cref="AvailablePlanograms"/> property.
        /// </summary>
        public static readonly PropertyPath AvailablePlanogramsProperty = GetPropertyPath(o => o.AvailablePlanograms);

        /// <summary>
        ///     <see cref="PropertyPath"/> to <see cref="DateLastCompared"/>.
        /// </summary>
        public static readonly PropertyPath DateLastComparedProperty = GetPropertyPath(o => o.DateLastCompared);
        
        /// <summary>
        ///     <see cref="PropertyPath"/> to <see cref="FilterType"/>.
        /// </summary>
        public static readonly PropertyPath FilterTypeProperty = GetPropertyPath(o => o.FilterType);
        
        /// <summary>
        ///     <see cref="PropertyPath"/> to <see cref="HasComparisonResults"/>.
        /// </summary>
        public static readonly PropertyPath HasComparisonResultsProperty = GetPropertyPath(o => o.HasComparisonResults);
        
        /// <summary>
        ///     <see cref="PropertyPath"/> definition for the <see cref="IncludeAllPlanograms"/> property.
        /// </summary>
        public static readonly PropertyPath IncludeAllPlanogramsProperty = GetPropertyPath(o => o.IncludeAllPlanograms);

        /// <summary>
        ///     <see cref="PropertyPath"/> to <see cref="IsShowUnchanged"/>.
        /// </summary>
        public static readonly PropertyPath IsShowUnchangedProperty = GetPropertyPath(o => o.IsShowUnchanged);

        /// <summary>
        ///     <see cref="PropertyPath"/> to <see cref="PlanogramPanelIsExpanded"/>.
        /// </summary>
        public static readonly PropertyPath PlanogramPlanelIsExpandedProperty = GetPropertyPath(o => o.PlanogramPanelIsExpanded);

        /// <summary>
        ///     <see cref="PropertyPath"/> to <see cref="Rows"/>.
        /// </summary>
        public static readonly PropertyPath RowsProperty = GetPropertyPath(o => o.Rows);

        /// <summary>
        ///     <see cref="PropertyPath"/> to <see cref="SelectedRows"/>.
        /// </summary>
        public static readonly PropertyPath SelectedRowsProperty = GetPropertyPath(o => o.SelectedRows);

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the <see cref="AvailablePlanograms"/> for the comparison.
        /// </summary>
        public ReadOnlyObservableCollection<PlanogramComparisonPlanogramRow> AvailablePlanograms { get; private set; }

        public DateTime? DateLastCompared
        {
            get { return _dateLastCompared; }
            set
            {
                _dateLastCompared = value;
                OnPropertyChanged(DateLastComparedProperty);
            }
        }

        /// <summary>
        ///     Get the <see cref="DocumentType"/> for this instance.
        /// </summary>
        public override DocumentType DocumentType { get { return DocumentType.PlanogramComparison; } }

        /// <summary>
        ///     Get the document title for this instance.
        /// </summary>
        public override String Title { get { return DocumentTypeHelper.FriendlyNames[DocumentType.PlanogramComparison]; } }

        /// <summary>
        ///     Get/set the currently selected <see cref="PlanItemType"/> to display on the list.
        /// </summary>
        public PlanogramComparisonFilterType FilterType
        {
            get { return _filterType; }
            set
            {
                if (_filterType == value) return;

                _filterType = value;
                ReloadRows();
                OnPropertyChanged(FilterTypeProperty);
            }
        }

        public Boolean HasComparisonResults
        {
            get { return _hasComparisonResults; }
            set
            {
                _hasComparisonResults = value;
                OnPropertyChanged(HasComparisonResultsProperty);
                OnPropertyChanged(FilterTypeProperty);
                OnHasComparisonResultsChanged(value);
            }
        }

        /// <summary>
        ///     Processes any operations that depend on the <see cref="HasComparisonResults"/> property.
        /// </summary>
        /// <param name="value">Current value of <see cref="HasComparisonResults"/>.</param>
        private void OnHasComparisonResultsChanged(Boolean value)
        {
            PlanogramPanelIsExpanded = !value;
        }

        /// <summary>
        ///     Gets or sets whether to <see cref="IncludeAllPlanograms"/> in the comparison or not.
        /// </summary>
        public Boolean IncludeAllPlanograms
        {
            get { return _includeAllPlanograms; }
            set
            {
                _includeAllPlanograms = value;
                if (_includeAllPlanograms)
                {
                    List<PlanogramComparisonPlanogramRow> unselectedRows = _availablePlans.Where(row => !row.IsSelected).ToList();
                    foreach (PlanogramComparisonPlanogramRow row in unselectedRows)
                    {
                        row.IsSelected = true;
                    }
                }
                OnPropertyChanged(IncludeAllPlanogramsProperty);
            }
        }

        public Boolean IsShowUnchanged
        {
            get { return _isShowUnchanged; }
            set
            {
                _isShowUnchanged = value;
                OnPropertyChanged(IsShowUnchangedProperty);
                ReloadRows();
            }
        }

        /// <summary>
        ///     Get/set whether the Planograms Panel is expanded.
        /// </summary>
        public Boolean PlanogramPanelIsExpanded
        {
            get { return _planogramPanelIsExpanded; }
            set
            {
                _planogramPanelIsExpanded = value;
                OnPropertyChanged(PlanogramPlanelIsExpandedProperty);
            }
        }

        /// <summary>
        ///     Get the read only collection of <see cref="PlanogramComparisonRow"/> to display.
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramComparisonRow> Rows
        {
            get { return _readOnlyRows ?? (_readOnlyRows = new ReadOnlyBulkObservableCollection<PlanogramComparisonRow>(_rows)); }
        }

        /// <summary>
        ///     Get the planograms that are currently marked as selected.
        /// </summary>
        public IEnumerable<Planogram> SelectedPlanograms
        {
            get { return AvailablePlanograms.Where(row => row.IsSelected).Select(row => row.PlanController.SourcePlanogram.Model).ToList(); }
        }

        /// <summary>
        ///     Get the collection of currently selected <see cref="PlanogramComparisonRow"/> in the list.
        /// </summary>
        public ObservableCollection<PlanogramComparisonRow> SelectedRows { get { return _selectedRows; } }

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of <see cref="PlanogramComparisonPlanDocument"/> belonging to the provided <paramref name="parentController"/>.
        /// </summary>
        /// <param name="parentController">The view model that will contain the newly created <see cref="PlanogramComparisonPlanDocument"/>.</param>
        public PlanogramComparisonPlanDocument(PlanControllerViewModel parentController) : base(parentController)
        {
            //  Initialize the sync timers for the product list.
            _syncAddTimer = new DispatcherTimer(
                TimeSpan.FromMilliseconds(10),
                DispatcherPriority.Background,
                SyncAddHandler,
                Dispatcher.CurrentDispatcher);
            _syncRemoveTimer = new DispatcherTimer(
                TimeSpan.FromMilliseconds(10),
                DispatcherPriority.Background,
                SyncRemoveHandler,
                Dispatcher.CurrentDispatcher);

            //  Set all the read only collections.
            _readOnlyRows = new ReadOnlyBulkObservableCollection<PlanogramComparisonRow>(_rows);


            //  Initialize the list of available planograms.
            AvailablePlanograms = new ReadOnlyObservableCollection<PlanogramComparisonPlanogramRow>(_availablePlans);
            IEnumerable<PlanogramComparisonPlanogramRow> comparisonPlanogramRows =
                App.MainPageViewModel.PlanControllers.Select(planController => PlanogramComparisonPlanogramRow.NewFrom(ParentController, planController, IncludeAllPlanograms));
            foreach (PlanogramComparisonPlanogramRow row in comparisonPlanogramRows)
            {
                row.PropertyChanged += PlanogramComparePlanogramRowOnPropertyChanged;
                _availablePlans.Add(row);
            }

            //  Subscribe event handlers.
            SetPlanEventHandlers(true);

            // Load the rows.
            ReloadRows();
        }

        #endregion

        #region Commands

        #region Compare

        /// <summary>
        ///     The <see cref="RelayCommand"/> used to run a silent planogram comparison.
        /// </summary>
        private RelayCommand _compareCommand;

        /// <summary>
        ///     <see cref="PropertyPath"/> to <see cref="CompareCommand"/>.
        /// </summary>
        public static readonly PropertyPath CompareCommandProperty = GetPropertyPath(o => o.CompareCommand);

        /// <summary>
        ///     Get the <see cref="RelayCommand"/> used to run a silent planogram comparison.
        /// </summary>
        public RelayCommand CompareCommand
        {
            get
            {
                if (_compareCommand != null) return _compareCommand;

                _compareCommand =
                    new RelayCommand(o => Compare_Executed(), o => Compare_CanExecute())
                    {
                        FriendlyName = Message.PlanogramComparisonPlanDocument_CompareCommand_Name,
                        FriendlyDescription = Message.PlanogramComparisonPlanDocument_CompareCommand_Description,
                        Icon = ImageResources.PlanogramComparisonPlanDocument_Compare32,
                        SmallIcon = ImageResources.PlanogramComparisonPlanDocument_Compare16
                    };
                RegisterCommand(_compareCommand);

                return _compareCommand;
            }
        }

        /// <summary>
        ///     Invoked before the <see cref="CompareCommand"/> is allowed to execute.
        /// </summary>
        /// <returns><c>True</c> if <see cref="CompareCommand"/> can be executed, <c>false</c> otherwise.</returns>
        [DebuggerStepThrough]
        private Boolean Compare_CanExecute()
        {
            //  There must be at least 2 plans open for a silent planogram comparison to be executable.
            if (App.MainPageViewModel.PlanControllers.Count >= 2) { return true; }
            CompareCommand.DisabledReason = Message.PlanogramComparisonPlanDocument_CompareCommand_DisableReason_NoPlanogramsToCompare;
            return false;
        }

        /// <summary>
        ///     Invoked whenever the <see cref="CompareCommand"/> is executed.
        /// </summary>
        private void Compare_Executed()
        {
            DoCompare(Silent);
        }

        #endregion

        #region ShowSettings

        /// <summary>
        ///     The <see cref="RelayCommand"/> used to present the user with planogram comparison settings.
        /// </summary>
        private RelayCommand _showSettingsCommand;

        /// <summary>
        ///     <see cref="PropertyPath"/> to <see cref="ShowSettingsCommand"/>.
        /// </summary>
        public static readonly PropertyPath ShowSettingsCommandProperty = GetPropertyPath(o => o.ShowSettingsCommand);

        /// <summary>
        ///     Get the <see cref="RelayCommand"/> used to run a silent planogram comparison.
        /// </summary>
        public RelayCommand ShowSettingsCommand
        {
            get
            {
                if (_showSettingsCommand != null) return _showSettingsCommand;

                _showSettingsCommand =
                    new RelayCommand(o => ShowSettings_Executed())
                    {
                        FriendlyName = Message.PlanogramComparisonPlanDocument_ShowSettingsCommand_Name,
                        FriendlyDescription = Message.PlanogramComparisonPlanDocument_ShowSettingsCommand_Description,
                        Icon = ImageResources.PlanogramComparisonPlanDocument_ShowSettings32,
                        SmallIcon = ImageResources.PlanogramComparisonPlanDocument_ShowSettings16
                    };
                RegisterCommand(_showSettingsCommand);

                return _showSettingsCommand;
            }
        }

        /// <summary>
        ///     Invoked whenever the <see cref="ShowSettingsCommand"/> is executed.
        /// </summary>
        private void ShowSettings_Executed()
        {
            DoCompare();
        }

        #endregion

        #endregion

        #region EventHandlers

        private void PlanogramComparePlanogramRowOnPropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "IsSelected") return;

            if (_suppressPlanPropertyChange) return;

            _suppressPlanPropertyChange = true;
            IncludeAllPlanograms = AvailablePlanograms.All(p => p.IsSelected);
            _suppressPlanPropertyChange = false;
        }

        /// <summary>
        ///     Invoked whenever the collection of selected plan items in the planogram changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedPlanItemsOnBulkCollectionChanged(Object sender, BulkCollectionChangedEventArgs e)
        {
            //  Ensure that the collection is not being synchronized already (the sync started in the plan document's collection).
            if (_isSyncingSelection) return;
            _isSyncingSelection = true;

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (PlanogramComparisonRow row in EnumerateUnselectedMatchingComparisonRows(e.ChangedItems.OfType<IPlanItem>())) 
                    {
                        SelectedRows.Add(row);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (PlanogramComparisonRow row in EnumerateMatchingComparisonRows(e.ChangedItems.OfType<IPlanItem>()))
                    {
                        SelectedRows.Remove(row);
                    }
                    break;
                case NotifyCollectionChangedAction.Reset:
                    SelectedRows.Clear();

                    foreach (PlanogramComparisonRow row in EnumerateUnselectedMatchingComparisonRows(SelectedPlanItems))
                    {
                        SelectedRows.Add(row);
                    }
                    break;
            }

            _isSyncingSelection = false;
        }

        /// <summary>
        ///     Invoked whenever the plan document's collection of selected rows changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedRowsOnCollectionChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            //  Ensure that the collection is not being synchronized already (the sync started in the planograms selected plan item's collection).
            if (_isSyncingSelection) return;
            _isSyncingSelection = true;

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    //  Add the new items to the SyncAddQueue.
                    SyncAddQueue(e.NewItems.Cast<PlanogramComparisonRow>().Where(row => row.PlanogramProduct != null).Select(row => row.PlanogramProduct));
                    break;
                case NotifyCollectionChangedAction.Remove:
                    //  Add the old items to the SyncRemoveQueue.
                    SyncRemoveQueue(e.OldItems.Cast<PlanogramComparisonRow>().Select(row => row.PlanogramProduct));
                    break;
                case NotifyCollectionChangedAction.Reset:
                    // NB We want to clear the delayed additions and removals as we are clearing the collection anyway.
                    ClearSyncAdd();
                    ClearSyncRemove();

                    // We mustn't deselect any non positional items from here when clearing...
                    List<IPlanItem> nonPositionItemsToReintroduce = SelectedPlanItems.Where(p => p.PlanItemType != PlanItemType.Position).ToList();

                    SelectedPlanItems.Clear();

                    List<PlanogramProductView> productsToSelect = SelectedRows.Select(r => r.PlanogramProduct).ToList();

                    List<PlanogramPositionView> positionsToSelect =
                        Planogram.EnumerateAllPositions().Where(i => productsToSelect.Contains(i.Product)).ToList();

                    var itemsToSelect = new List<IPlanItem>();
                    itemsToSelect.AddRange(positionsToSelect);
                    //itemsToSelect.AddRange(SelectedRows.Where(p => p. == 0).Select(p => p.Product));
                    itemsToSelect.AddRange(nonPositionItemsToReintroduce);

                    SelectedPlanItems.AddRange(itemsToSelect);
                    break;
            }

            _isSyncingSelection = false;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Create the correct Validation Key for the given <paramref name="item"/>
        /// </summary>
        /// <param name="item">The <see cref="IPlanItem"/> instance to create a comparison key for.</param>
        /// <returns>A string containing a unique key to use when identifying other items comparable to this one.</returns>
        private static String CreateComparisonKey(IPlanItem item)
        {
            switch (item.PlanItemType)
            {
                case PlanItemType.Fixture:
                    break;
                case PlanItemType.Assembly:
                    break;
                case PlanItemType.Component:
                    break;
                case PlanItemType.SubComponent:
                    break;
                case PlanItemType.Position:
                    return PlanogramComparerHelper.CreateComparisonKey(item.Position.Model);
                case PlanItemType.Annotation:
                    break;
                case PlanItemType.Product:
                    return PlanogramComparerHelper.CreateComparisonKey(item.Product.Model);
                case PlanItemType.Planogram:
                    break;
                default:
                    Debug.Fail("Unknown Plan Item Type when creating comparison key.");
                    break;
            }
            Debug.Fail("Could not create comparison key for unsupported type.");
            return null;
        }

        /// <summary>
        ///     Performs a comparison between the Master Planogram and others.
        /// </summary>
        /// <param name="isSilent">Whether to present the user with a settings window to configure the comparison before actually comparing.</param>
        /// <remarks>When then comparison is silent all open planograms are automatically selected for the comparison.</remarks>
        private void DoCompare(Boolean isSilent = false)
        {
            PlanComparison planComparison = Planogram.Model.Comparison;
            var viewModel = new PlanogramComparisonEditorViewModel(planComparison, SelectedPlanograms.Count() > 1);

            if (isSilent)
            {
                //  Compare silently, using the existing options.
                viewModel.ApplyCommand.Execute();
            }
            else
            {
                //  Show the settings window to the user before comparing.
                GetWindowService().ShowDialog<PlanogramComparisonEditorOrganiser>(viewModel);
                if (viewModel.DialogResult != true) return;
            }

            //  Update the results and reload the rows.
            planComparison.UpdateResults(SelectedPlanograms);
            ReloadRows();
        }

        /// <summary>
        ///     Stop any updates or reactions to changes in this instance.
        /// </summary>
        protected override void OnFreeze()
        {
            //  Unsubscribe event handlers.
            SetPlanEventHandlers(false);
        }

        /// <summary>
        ///     Refresh and allow updates and reactions to changes in this instance.
        /// </summary>
        protected override void OnUnfreeze()
        {
            //  Subscribe event handlers.
            SetPlanEventHandlers(true);
        }

        /// <summary>
        ///     Initializes and loads the comparison rows.
        /// </summary>
        private void ReloadRows()
        {
            PlanComparison comparison = Planogram.Comparison;
            PlanogramComparisonResultList comparisonResults = comparison.Results;
            HasComparisonResults = comparisonResults.Any();
            DateLastCompared = comparison.DateLastCompared;

            //  Get the list of product comparison items in the planogram comparison.
            List<IGrouping<String, PlanogramComparisonItem>> itemsByKey = comparisonResults.SelectMany(r => r.ComparedItems).GroupBy(i => i.ItemId).ToList();

            IEnumerable<String> otherPlanNames = comparisonResults.Select(result => result.PlanogramName);
            // NB: Ordering other plans in the comparison by date last mod, so that the newer ones are at the end 
            //  (and thus in case of conflict those are the ones displayed).
            List<PlanogramView> otherPlans = new List<PlanogramView>();
            // Removed other plans data to avoid information for missing items to appear as if belonging to the master plan.
                //App.MainPageViewModel.PlanControllers.Where(c => otherPlanNames.Contains(c.SourcePlanogram.Name))
                //   .Select(c => c.SourcePlanogram)
                //   .OrderBy(p => p.Model.Parent.DateLastModified)
                //   .ToList();
            // NB: Add the Master Planogram's products and positions last so they are kept if there are conflicts.
            IEnumerable<IPlanItem> otherPlanItems = ((IEnumerable<IPlanItem>) otherPlans.SelectMany(p => p.Products)).Union(otherPlans.SelectMany(p => p.EnumerateAllPositions()));
            IEnumerable<IPlanItem> planItems = otherPlanItems.Union(Planogram.Products).Union(Planogram.EnumerateAllPositions());
            Dictionary<String, IPlanItem> itemViewsByComparisonKey = planItems.ToLookupDictionary(CreateComparisonKey);

            //  If there are rows already, remove any old ones.
            if (_masterRows.Any()) _masterRows.Clear();

            //  Add new rows.
            _masterRows.AddRange(
                itemsByKey.Select(comparisonItemsByItemId =>
                                  PlanogramComparisonRow.NewPlanogramComparisonRow(comparisonItemsByItemId.Key,
                                                                                   comparisonItemsByItemId.ToList(),
                                                                                   itemViewsByComparisonKey)));

            IEnumerable<PlanogramComparisonRow> rowsToDisplay;
            switch (FilterType)
            {
                case PlanogramComparisonFilterType.Products:
                    rowsToDisplay = _masterRows.Where(r => r.ItemType == PlanogramItemType.Product);
                    break;
                case PlanogramComparisonFilterType.Positions:
                    rowsToDisplay = _masterRows.Where(r => r.ItemType == PlanogramItemType.Position);
                    break;
                default:
                    rowsToDisplay = _masterRows;
                    break;
            }

            if (!IsShowUnchanged)
                rowsToDisplay = rowsToDisplay.Where(r => r.Results.Any(pair => pair.Value != PlanogramComparisonItemStatusType.Unchanged));

            if (_rows.Any()) _rows.Clear();

            _rows.AddRange(rowsToDisplay.OrderBy(row => row.Key).ThenBy(row => row.Gtin));
        }

        /// <summary>
        ///     Subscribe or unsubscribe the event handlers.
        /// </summary>
        /// <param name="subscribe">Whether the events should be handled or not.</param>
        private void SetPlanEventHandlers(Boolean subscribe)
        {
            //  Do not subscribe or unsubscribe more than once.
            if (_areEventHandlersSubscribed == subscribe) return;
            _areEventHandlersSubscribed = subscribe;

            //  If event handlers should be set, do so and return.
            if (_areEventHandlersSubscribed)
            {
                SelectedPlanItems.BulkCollectionChanged += SelectedPlanItemsOnBulkCollectionChanged;
                SelectedRows.CollectionChanged += SelectedRowsOnCollectionChanged;
                App.MainPageViewModel.PlanControllers.BulkCollectionChanged += PlanControllersOnBulkCollectionChanged;
                return;
            }
            
            //  Unsubscribe all the event handlers.
            SelectedPlanItems.BulkCollectionChanged -= SelectedPlanItemsOnBulkCollectionChanged;
            SelectedRows.CollectionChanged -= SelectedRowsOnCollectionChanged;
            App.MainPageViewModel.PlanControllers.BulkCollectionChanged -= PlanControllersOnBulkCollectionChanged;
        }

        /// <summary>
        ///     Invoked whenever the collection of Plan Controllers changes (Planograms are added or removed)
        /// </summary>
        private void PlanControllersOnBulkCollectionChanged(Object sender, BulkCollectionChangedEventArgs e)
        {

            IEnumerable<PlanControllerViewModel> affectedViewModels = e.ChangedItems.OfType<PlanControllerViewModel>();
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                case NotifyCollectionChangedAction.Reset:
                    AddToAvailablePlans(affectedViewModels, e.Action == NotifyCollectionChangedAction.Reset);
                    break;
                case NotifyCollectionChangedAction.Remove:
                    RemoveFromAvailablePlans(affectedViewModels);
                    break;
            }

            OnPropertyChanged(AvailablePlanogramsProperty);
        }

        #endregion

        #region Helper Methods

        /// <summary>
        ///     Add the items in the enumeration of <see cref="PlanControllerViewModel"/> to the <see cref="AvailablePlanograms"/> collection as new <see cref="PlanogramComparisonPlanogramRow"/>.
        /// </summary>
        /// <param name="affectedViewModels">The collection of <see cref="PlanControllerViewModel"/> that will be added as new <see cref="PlanogramComparisonPlanogramRow"/>.</param>
        /// <param name="clearPrevious">Whether to clear the previous plans from the <see cref="AvailablePlanograms"/> collection.</param>
        private void AddToAvailablePlans(IEnumerable<PlanControllerViewModel> affectedViewModels, Boolean clearPrevious)
        {
            if (clearPrevious) _availablePlans.Clear();

            IEnumerable<PlanogramComparisonPlanogramRow> newRows =
                affectedViewModels.Select(viewModel => PlanogramComparisonPlanogramRow.NewFrom(ParentController, viewModel, IncludeAllPlanograms));
            foreach (PlanogramComparisonPlanogramRow row in newRows)
            {
                row.PropertyChanged += PlanogramComparePlanogramRowOnPropertyChanged;
                _availablePlans.Add(row);
            }
        }

        /// <summary>
        ///     Clear the sync add queue in case it is no longer needed (when resetting the collection with pending changes, for example.)
        /// </summary>
        private void ClearSyncAdd()
        {
            if (_syncAddTimer == null) return;
            _syncAddTimer.Stop();
            lock (_syncAddTimer) { _syncAddQueue.Clear(); }
        }

        /// <summary>
        ///     Clear the sync remove queue in case it is no longer needed (when resetting the collection with pending changes, for example)
        /// </summary>
        private void ClearSyncRemove()
        {
            if (_syncRemoveTimer == null) return;
            _syncRemoveTimer.Stop();
            lock (_syncRemoveTimer) { _syncRemoveQueue.Clear(); }
        }

        /// <summary>
        ///     Select the <see cref="PlanogramComparisonRow"/> corresponding to each given <see cref="IPlanItem"/>.
        /// </summary>
        /// <param name="planItems">The collection of <see cref="IPlanItem"/> to get the matching collection of <see cref="PlanogramComparisonRow"/>.</param>
        /// <returns></returns>
        private IEnumerable<PlanogramComparisonRow> EnumerateMatchingComparisonRows(IEnumerable<IPlanItem> planItems)
        {
            return planItems.Where(item => item.Product != null).Select(item => Rows.FirstOrDefault(row => row.PlanogramProduct == item.Product));
        }

        /// <summary>
        ///     Select the <see cref="PlanogramComparisonRow"/> corresponding to each given <see cref="IPlanItem"/> that are not currently selected.
        /// </summary>
        /// <param name="planItems">The collection of <see cref="IPlanItem"/> to get the matching collection of unselected <see cref="PlanogramComparisonRow"/>.</param>
        /// <returns></returns>
        private IEnumerable<PlanogramComparisonRow> EnumerateUnselectedMatchingComparisonRows(IEnumerable<IPlanItem> planItems)
        {
            return EnumerateMatchingComparisonRows(planItems).Where(row => row != null).Where(row => !SelectedRows.Contains(row));
        }

        /// <summary>
        ///     Helper method to get the property path for the <see cref="PlanogramComparisonPlanDocument" /> property indicated by the
        ///     <paramref name="expression" />.
        /// </summary>
        /// <param name="expression">Lambda expression to access the property.</param>
        /// <returns>
        ///     Returns a <see cref="PropertyPath" /> object with the path to the property in the
        ///     <paramref name="expression" />.
        /// </returns>
        private static PropertyPath GetPropertyPath(Expression<Func<PlanogramComparisonPlanDocument, Object>> expression)
        {
            return WpfHelper.GetPropertyPath(expression);
        }

        /// <summary>
        ///     Registers the given command so that keyboard shortcuts can be handled.
        /// </summary>
        private void RegisterCommand(IRelayCommand command)
        {
            Debug.Assert(!ViewModelCommands.Contains(command), "Command already registered");

            ViewModelCommands.Add(command);
        }

        /// <summary>
        ///     Remove the items in the enumeration of <see cref="PlanControllerViewModel"/> from the <see cref="AvailablePlanograms"/> collection.
        /// </summary>
        /// <param name="affectedViewModels">The collection of <see cref="PlanControllerViewModel"/> that will be removed if found.</param>
        private void RemoveFromAvailablePlans(IEnumerable<PlanControllerViewModel> affectedViewModels)
        {
            List<PlanogramComparisonPlanogramRow> oldRows =
                _availablePlans.Where(row => affectedViewModels.Any(model => model == row.PlanController)).ToList();
            foreach (PlanogramComparisonPlanogramRow row in oldRows)
            {
                row.PropertyChanged -= PlanogramComparePlanogramRowOnPropertyChanged;
                _availablePlans.Remove(row);
            }
        }

        /// <summary>
        ///     Handle the delayed Synchronize Added items.
        /// </summary>
        private void SyncAddHandler(Object sender, EventArgs eventArgs)
        {
            var timer = sender as DispatcherTimer;
            if (timer == null) return;
            timer.Stop();
            lock (timer)
            {
                //  Make sure the collection is synced only once from here... so set the is syncing flag.
                _isSyncingSelection = true;
                IEnumerable<IPlanItem> productPositions = Planogram.EnumerateAllPositions().Where(view => _syncAddQueue.Contains(view.Product)).ToList();

                if (!Keyboard.IsKeyDown(Key.LeftCtrl) &&
                    !Keyboard.IsKeyDown(Key.RightCtrl) &&
                    !Keyboard.IsKeyDown(Key.LeftShift) &&
                    !Keyboard.IsKeyDown(Key.RightShift)) SelectedPlanItems.Clear();

                SelectedPlanItems.AddRange(_syncAddQueue.Concat(productPositions));


                _syncAddQueue.Clear();
                _isSyncingSelection = false;
            }
        }

        /// <summary>
        ///     Add the given <paramref name="items"/> to the <see cref="_syncAddQueue"/> collection and start the <see cref="_syncAddTimer"/>.
        /// </summary>
        /// <param name="items">The <see cref="PlanogramPositionView"/> items to be added with a delay.</param>
        private void SyncAddQueue(IEnumerable<PlanogramProductView> items)
        {
            //  Temporarily stop the timer and add the new items for delayed syncing.
            _syncAddTimer.Stop();
            lock (_syncAddTimer) { _syncAddQueue.AddRange(items); }
            _syncAddTimer.Start();
        }

        /// <summary>
        ///     Handle the delayed Synchronize Remove items.
        /// </summary>
        private void SyncRemoveHandler(Object sender, EventArgs eventArgs)
        {
            var timer = sender as DispatcherTimer;
            if (timer == null) return;
            timer.Stop();
            lock (timer)
            {
                //  Make sure the collection is synced only once from here... so set the is syncing flag.
                _isSyncingSelection = true;
                IEnumerable<IPlanItem> productPositions = Planogram.EnumerateAllPositions().Where(view => _syncRemoveQueue.Contains(view.Product)).ToList();
                SelectedPlanItems.RemoveRange(_syncRemoveQueue.Concat(productPositions));
                _syncRemoveQueue.Clear();
                _isSyncingSelection = false;
            }
        }

        /// <summary>
        ///     Add the given <paramref name="items"/> to the <see cref="_syncRemoveQueue"/> collection and start the <see cref="_syncRemoveTimer"/>.
        /// </summary>
        /// <param name="items">The <see cref="PlanogramPositionView"/> items to be removed with a delay.</param>
        private void SyncRemoveQueue(IEnumerable<PlanogramProductView> items)
        {
            //  Temporarily stop the timer and add the old items for delayed syncing.
            _syncRemoveTimer.Stop();
            lock (_syncRemoveTimer) { _syncRemoveQueue.AddRange(items); }
            _syncRemoveTimer.Start();
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (IsDisposed) return;

            SetPlanEventHandlers(false);
            base.Dispose(disposing);
            if (_syncAddTimer != null) _syncAddTimer.Stop();
            if (_syncRemoveTimer != null) _syncRemoveTimer.Stop();

            IsDisposed = true;
        }

        #endregion
    }

    #region Supporting Classes

    #region PlanogramComparisonRow

    /// <summary>
    ///     View model for a planogram compare row.
    /// </summary>
    public sealed class PlanogramComparisonRow
    {
        #region Fields
        private CalculatedValueResolver _calculatedValueResolver;
        #endregion

        #region Properties

        public WpfSafeDictionary<String, WpfSafeDictionary<String, String>> Values { get; set; }

        private IPlanItem PlanItem { get; set; }

        public PlanogramProductView PlanogramProduct { get { return PlanItem != null ? PlanItem.Product ?? PlanogramPosition.Product : null; } }
        public PlanogramProductView ValuePlanogramProduct { get { return PlanogramProduct; } }

        public PlanogramPositionView PlanogramPosition { get { return PlanItem != null ? PlanItem.Position : null; } }
        public PlanogramPositionView ValuePlanogramPosition { get { return PlanogramPosition; } }

        public PlanogramComponentView PlanogramComponent
        {
            get { return PlanItem != null ? PlanItem.Component ?? PlanogramPosition.Component : null; }
        }

        public PlanogramComponentView ValuePlanogramComponent { get { return PlanogramComponent; } }

        public PlanogramSubComponentView PlanogramSubComponent
        {
            get { return PlanItem != null ? PlanItem.SubComponent ?? PlanogramPosition.SubComponent : null; }
        }

        public PlanogramSubComponentView ValuePlanogramSubComponent { get { return PlanogramSubComponent; } }

        public PlanogramView Planogram
        {
            get
            {
                if (PlanItem == null) return null;

                if (PlanItem.Planogram != null) return PlanItem.Planogram;

                if (PlanItem.Position != null &&
                    PlanItem.Position.Planogram != null) return PlanItem.Position.Planogram;

                if (PlanItem.Product != null &&
                    PlanItem.Product.Planogram != null) return PlanItem.Product.Planogram;

                return null;
            }
        }

        public PlanogramView ValuePlanogram { get { return Planogram; } }

        public PlanogramFixtureView PlanogramFixture
        {
            get
            {
                if (PlanItem == null) return null;

                if (PlanItem.Fixture != null) return PlanItem.Fixture;

                if (PlanogramPosition != null &&
                    PlanogramPosition.Fixture != null) return PlanogramPosition.Fixture;

                return null;
            }
        }

        public PlanogramFixtureView ValuePlanogramFixture { get { return PlanogramFixture; } }

        public PlanogramItemType ItemType { get; private set; }

        public WpfSafeDictionary<String, PlanogramComparisonItemStatusType> Results { get; private set; }

        public String Key { get; private set; }

        public String Gtin { get { return PlanogramProduct != null ? PlanogramProduct.Gtin : String.Empty; } }

        public String PositionInstance { get; set; }

        public PlanogramItemComparisonStatusType Status { get; set; }

        /// <summary>
        /// Returns the resolver used for binding to calculated values.
        /// </summary>
        public CalculatedValueResolver CalculatedValueResolver
        {
            get
            {
                if (_calculatedValueResolver == null)
                    _calculatedValueResolver = new CalculatedValueResolver(GetCalculatedValue);

                return _calculatedValueResolver;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of <see cref="PlanogramComparisonRow"/>.
        /// </summary>
        /// <param name="comparisonKey">The <c>Comparison Key</c> for this <see cref="PlanogramComparisonRow"/> instance.</param>
        /// <param name="comparisonItems">The collection of <see cref="PlanogramComparisonItem"/> that refer to the same <see cref="IPlanItem"/> across all compared planograms.</param>
        /// <param name="planItemsByComparisonKey">Lookup of <see cref="IPlanItem"/> instances by Comparison Key.</param>
        /// <returns></returns>
        public static PlanogramComparisonRow NewPlanogramComparisonRow(String comparisonKey,
                                                                       List<PlanogramComparisonItem> comparisonItems,
                                                                       Dictionary<String, IPlanItem> planItemsByComparisonKey)
        {
            Debug.Assert(comparisonItems.Count > 0, "There should be at least one item compared as we are displaying rows.");
            IPlanItem planItem = GetValueOrDefault(comparisonKey, planItemsByComparisonKey);
            var row = new PlanogramComparisonRow
                      {
                          ItemType = comparisonItems.First().ItemType,
                          Key = comparisonKey,
                          Results = LookupStatusByPlanogramName(comparisonItems),
                          PlanItem = planItem,
                          Values = LookupValuesByPlanogramName(comparisonItems)
                      };

            if (row.ItemType == PlanogramItemType.Position)
            {
                Match match = Regex.Match(row.Key, @"\((\d+)\)");
                if (match.Success && match.Groups.Count > 0)
                row.PositionInstance = match.Groups[0].Value.Replace("(","").Replace(")","");
            }
            
            if (planItem == null)
            {
                row.Status = PlanogramItemComparisonStatusType.NotFound;
                return row;
            }

            UpdateRowStatus(planItem, row);

            return row;
        }

        #endregion

        #region Static Helper Methods

        /// <summary>
        ///     Update the <paramref name="row"/> status depending on the type of <paramref name="planItem"/>.
        /// </summary>
        /// <param name="planItem"></param>
        /// <param name="row"></param>
        private static void UpdateRowStatus(IPlanItem planItem, PlanogramComparisonRow row)
        {
            PlanComparison comparison = planItem.Planogram.Comparison;
            row.Status = PlanogramItemComparisonStatusType.Unchanged;
            switch (planItem.PlanItemType)
            {
                case PlanItemType.Fixture:
                    break;
                case PlanItemType.Assembly:
                    break;
                case PlanItemType.Component:
                    break;
                case PlanItemType.SubComponent:
                    break;
                case PlanItemType.Position:
                    row.Status = comparison.GetComparisonStatus(planItem.Position.Model);
                    break;
                case PlanItemType.Annotation:
                    break;
                case PlanItemType.Product:
                    row.Status = comparison.GetComparisonStatus(planItem.Product.Model);
                    break;
                case PlanItemType.Planogram:
                    break;
                default:
                    Debug.Fail("Unknown Plan Item Type when calling UpdateRowStatus.");
                    break;
            }
        }

        /// <summary>
        ///     Lookup all values per field placeholder for the given <paramref name="item"/>.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private static WpfSafeDictionary<String, String> GetItemFieldValuesByFieldPlaceholder(PlanogramComparisonItem item)
        {
            return new WpfSafeDictionary<String, String>(item.FieldValues.ToLookupDictionary(value => value.FieldPlaceholder, value => value.Value),
                                                         null);
        }

        /// <summary>
        ///     Lookup all the values by planogram name.
        /// </summary>
        /// <param name="comparisonItems">The comparison items contaning values per planogram.</param>
        /// <returns></returns>
        private static WpfSafeDictionary<String, WpfSafeDictionary<String, String>> LookupValuesByPlanogramName(
            IEnumerable<PlanogramComparisonItem> comparisonItems)
        {
            return new WpfSafeDictionary<String, WpfSafeDictionary<String, String>>(
                comparisonItems.ToLookupDictionary(item => item.Parent.PlanogramName, GetItemFieldValuesByFieldPlaceholder),
                new WpfSafeDictionary<String, String>(new Dictionary<String, String>(), null));
        }

        /// <summary>
        ///     Lookup all the item statuses by planogram name.
        /// </summary>
        /// <param name="comparisonItems">The comparison items contaning item statuses per planogram.</param>
        /// <returns></returns>
        private static WpfSafeDictionary<String, PlanogramComparisonItemStatusType> LookupStatusByPlanogramName(
            IEnumerable<PlanogramComparisonItem> comparisonItems)
        {
            return new WpfSafeDictionary<String, PlanogramComparisonItemStatusType>(
                comparisonItems.ToLookupDictionary(item => item.Parent.PlanogramName, item => item.Status),
                PlanogramComparisonItemStatusType.Unchanged);
        }

        /// <summary>
        ///     Get the value matching the given <paramref name="key"/> in the <paramref name="dictionary"/> or the default value if missing.
        /// </summary>
        /// <typeparam name="TKey">Type of the key used in the <paramref name="dictionary"/>.</typeparam>
        /// <typeparam name="TValue">Type of the value in the <paramref name="dictionary"/>.</typeparam>
        /// <param name="key">The key being matched.</param>
        /// <param name="dictionary">The dictionary contaning the values.</param>
        /// <returns>The value if the key is found, or the default value if not.</returns>
        private static TValue GetValueOrDefault<TKey, TValue>(TKey key, IDictionary<TKey, TValue> dictionary)
        {
            TValue value;
            return dictionary.TryGetValue(key, out value) ? value : default(TValue);
        }

        #endregion

        #region ICalculatedValueResolver Implementation

        private Object GetCalculatedValue(String expressionText)
        {
            var expression = new ObjectFieldExpression(expressionText);
            expression.AddDynamicFieldParameters(PlanogramFieldHelper.EnumerateAllFields());

            expression.ResolveDynamicParameter += ExpressionOnResolveDynamicParameter;

            //  Evaluate.
            Object returnValue = expression.Evaluate();

            expression.ResolveDynamicParameter -= ExpressionOnResolveDynamicParameter;

            return returnValue;
        }

        private void ExpressionOnResolveDynamicParameter(Object sender, ObjectFieldExpressionResolveParameterArgs e)
        {
            Object item = GetObjectByType(e.Parameter.FieldInfo.OwnerType);
            if (item != null) e.Result = e.Parameter.FieldInfo.GetValue(item);
        }

        private Object GetObjectByType(Type type)
        {
            if (type == typeof (Planogram)) return Planogram;
            if (type == typeof (PlanogramComponent)) return PlanogramComponent;
            if (type == typeof (PlanogramFixture)) return PlanogramFixture;
            if (type == typeof (PlanogramPosition)) return PlanogramPosition;
            if (type == typeof (PlanogramProduct)) return PlanogramProduct;
            if (type == typeof (PlanogramSubComponent)) return PlanogramSubComponent;

            Debug.Fail("Type not implemented when invoking PlanogramComparisonPlanDocument.GetObjectByType(Type)");
            return null;
        }

        #endregion
    }

    public sealed class WpfSafeDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {
        private readonly Dictionary<TKey, TValue> _dictionary;
        private readonly TValue _defaultValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="defaultValue"></param>
        public WpfSafeDictionary(Dictionary<TKey, TValue> dictionary, TValue defaultValue)
        {
            _dictionary = dictionary;
            _defaultValue = defaultValue;
            Keys = new List<TKey>();
            Values = new List<TValue>();
        }

        #region Implementation of IEnumerable

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1"/> that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return _dictionary.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator"/> object that can be used to iterate through the collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Implementation of ICollection<KeyValuePair<TKey,TValue>>

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1"/>.</param><exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.</exception>
        public void Add(KeyValuePair<TKey, TValue> item)
        {
            if (!_dictionary.ContainsKey(item.Key)) _dictionary.Add(item.Key, item.Value);
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only. </exception>
        public void Clear()
        {
            _dictionary.Clear();
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1"/> contains a specific value.
        /// </summary>
        /// <returns>
        /// true if <paramref name="item"/> is found in the <see cref="T:System.Collections.Generic.ICollection`1"/>; otherwise, false.
        /// </returns>
        /// <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.ICollection`1"/>.</param>
        public Boolean Contains(KeyValuePair<TKey, TValue> item)
        {
            return _dictionary.Contains(item);
        }

        /// <summary>
        ///     Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" /> to an
        ///     <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">
        ///     The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied
        ///     from <see cref="T:System.Collections.Generic.ICollection`1" />. The <see cref="T:System.Array" /> must have
        ///     zero-based indexing.
        /// </param>
        /// <param name="arrayIndex">The zero-based index in <paramref name="array" /> at which copying begins.</param>
        /// <exception cref="T:System.ArgumentNullException"><paramref name="array" /> is null.</exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException"><paramref name="arrayIndex" /> is less than 0.</exception>
        /// <exception cref="T:System.ArgumentException">
        ///     <paramref name="array" /> is multidimensional.-or-The number of elements
        ///     in the source <see cref="T:System.Collections.Generic.ICollection`1" /> is greater than the available space from
        ///     <paramref name="arrayIndex" /> to the end of the destination <paramref name="array" />.-or-Type
        ///     <paramref name="array" /> cannot be cast automatically to the type of the destination <paramref name="array" />.
        /// </exception>
        public void CopyTo(KeyValuePair<TKey, TValue>[] array, Int32 arrayIndex)
        {
            if (array == null) throw new ArgumentNullException("array");

            if (arrayIndex < 0 ||
                arrayIndex > array.Length) throw new ArgumentOutOfRangeException("arrayIndex");

            if (array.Length - arrayIndex < Count)
                throw new ArgumentException("Destination array is not large enough. Check array.Length and arrayIndex");

            foreach (KeyValuePair<TKey, TValue> keyValuePair in this)
            {
                array[arrayIndex++] = keyValuePair;
            }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <returns>
        /// true if <paramref name="item"/> was successfully removed from the <see cref="T:System.Collections.Generic.ICollection`1"/>; otherwise, false. This method also returns false if <paramref name="item"/> is not found in the original <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </returns>
        /// <param name="item">The object to remove from the <see cref="T:System.Collections.Generic.ICollection`1"/>.</param><exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.</exception>
        public Boolean Remove(KeyValuePair<TKey, TValue> item)
        {
            return _dictionary.ContainsKey(item.Key) && _dictionary.Remove(item.Key);
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </summary>
        /// <returns>
        /// The number of elements contained in the <see cref="T:System.Collections.Generic.ICollection`1"/>.
        /// </returns>
        public Int32 Count { get { return _dictionary.Count; } }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.
        /// </summary>
        /// <returns>
        /// true if the <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only; otherwise, false.
        /// </returns>
        public Boolean IsReadOnly { get; private set; }

        #endregion

        #region Implementation of IDictionary<TKey,TValue>

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.IDictionary`2"/> contains an element with the specified key.
        /// </summary>
        /// <returns>
        /// true if the <see cref="T:System.Collections.Generic.IDictionary`2"/> contains an element with the key; otherwise, false.
        /// </returns>
        /// <param name="key">The key to locate in the <see cref="T:System.Collections.Generic.IDictionary`2"/>.</param><exception cref="T:System.ArgumentNullException"><paramref name="key"/> is null.</exception>
        public Boolean ContainsKey(TKey key)
        {
            return _dictionary.ContainsKey(key);
        }

        /// <summary>
        /// Adds an element with the provided key and value to the <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        /// </summary>
        /// <param name="key">The object to use as the key of the element to add.</param><param name="value">The object to use as the value of the element to add.</param><exception cref="T:System.ArgumentNullException"><paramref name="key"/> is null.</exception><exception cref="T:System.ArgumentException">An element with the same key already exists in the <see cref="T:System.Collections.Generic.IDictionary`2"/>.</exception><exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.IDictionary`2"/> is read-only.</exception>
        public void Add(TKey key, TValue value)
        {
            _dictionary.Add(key, value);
        }

        /// <summary>
        /// Removes the element with the specified key from the <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        /// </summary>
        /// <returns>
        /// true if the element is successfully removed; otherwise, false.  This method also returns false if <paramref name="key"/> was not found in the original <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        /// </returns>
        /// <param name="key">The key of the element to remove.</param><exception cref="T:System.ArgumentNullException"><paramref name="key"/> is null.</exception><exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.IDictionary`2"/> is read-only.</exception>
        public Boolean Remove(TKey key)
        {
            return _dictionary.Remove(key);
        }

        /// <summary>
        /// Gets the value associated with the specified key.
        /// </summary>
        /// <returns>
        /// true if the object that implements <see cref="T:System.Collections.Generic.IDictionary`2"/> contains an element with the specified key; otherwise, false.
        /// </returns>
        /// <param name="key">The key whose value to get.</param><param name="value">When this method returns, the value associated with the specified key, if the key is found; otherwise, the default value for the type of the <paramref name="value"/> parameter. This parameter is passed uninitialized.</param><exception cref="T:System.ArgumentNullException"><paramref name="key"/> is null.</exception>
        public Boolean TryGetValue(TKey key, out TValue value)
        {
            return _dictionary.TryGetValue(key, out value);
        }

        /// <summary>
        /// Gets or sets the element with the specified key.
        /// </summary>
        /// <returns>
        /// The element with the specified key.
        /// </returns>
        /// <param name="key">The key of the element to get or set.</param><exception cref="T:System.ArgumentNullException"><paramref name="key"/> is null.</exception><exception cref="T:System.Collections.Generic.KeyNotFoundException">The property is retrieved and <paramref name="key"/> is not found.</exception><exception cref="T:System.NotSupportedException">The property is set and the <see cref="T:System.Collections.Generic.IDictionary`2"/> is read-only.</exception>
        public TValue this[TKey key]
        {
            get
            {
                TValue value;
                return !_dictionary.TryGetValue(key, out value) ? _defaultValue : value;
            }
            set { _dictionary[key] = value; }
        }

        /// <summary>
        /// Gets an <see cref="T:System.Collections.Generic.ICollection`1"/> containing the keys of the <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.Generic.ICollection`1"/> containing the keys of the object that implements <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        /// </returns>
        public ICollection<TKey> Keys { get; private set; }

        /// <summary>
        /// Gets an <see cref="T:System.Collections.Generic.ICollection`1"/> containing the values in the <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.Generic.ICollection`1"/> containing the values in the object that implements <see cref="T:System.Collections.Generic.IDictionary`2"/>.
        /// </returns>
        public ICollection<TValue> Values { get; private set; }

        #endregion
    }

    #endregion

    #endregion
}