﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31742 : A.Silva
//  Created.
// V8-31745 : A.Silva
//  Hardcoded column sets for Products and Positions.
// V8-31862 : A.Silva
//  Added column Manager support and predetermined columns (GTIN and compared Planograms Result).
// V8-31863 : A.Silva
//  Added specific columns for products and positions.
// V8-31881 : A.Silva
//  Amended to use ComparisonColumnLayoutFactory.
// V8-31960 : A.Silva
//  Added support for the Planograms To Compare Panel.
// V8-31967 : A.Silva
//  ColumnManagerOnColumnSetChanging now adds extra columns for the compared fields in the Master planogram and in all others.
// V8-32110 : A.Silva
//  Amended loading of non Display field columns.
// V8-32930 : A.Silva
//  Renamed Position ID column to Position instance and changed the display to just the instance number (without the GTIN).
// V8-32926 : A.Silva
//  Amended GroupColumnsByPlanogram and GroupColumnsByAttribute to ensure fields are ordered as the user set them up.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Binding = System.Windows.Data.Binding;
using Cursors = System.Windows.Input.Cursors;
using Message = Galleria.Ccm.Editor.Client.Wpf.Resources.Language.Message;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.PlanogramComparison
{
    /// <summary>
    ///     Interaction logic for PlanogramComparisonPlanDocumentView.xaml
    /// </summary>
    public sealed partial class PlanogramComparisonPlanDocumentView : IPlanDocumentView
    {
        #region Constants

        /// <summary>
        ///     Key identifying this screen to the column layout manager when displaying Product Comparison Results.
        /// </summary>
        private const String ProductsScreenKey = "PlanogramComparisonPlanDocumentProducts";

        /// <summary>
        ///     Key identifying this screen to the column layout manager when displaying Position Comparison Results.
        /// </summary>
        private const String PositionsScreenKey = "PlanogramComparisonPlanDocumentPositions";

        /// <summary>
        ///     Path for binding the Key column.
        /// </summary>
        private const String KeyColumnBindingPath = "Key";

        /// <summary>
        ///     Path for binding the Position Instance column.
        /// </summary>
        private const String PositionInstanceColumnBindingPath = "PositionInstance";

        /// <summary>
        ///     Path for binding the GTIN column.
        /// </summary>
        private const String GtinColumnBindingPath = "Gtin";

        /// <summary>
        ///     Path for binding the Status column.
        /// </summary>
        private const String StatusColumnBindingPath = "Status";

        /// <summary>
        ///     Path for binding the Results column.
        /// </summary>
        private const String ResultsColumnBindingPath = "Results[{0}]";

        /// <summary>
        ///     Path for binding the Values column.
        /// </summary>
        private const String ValuesColumnBindingPath = "Values[{0}][{1}]";

        #endregion

        #region Fields

        /// <summary>
        ///     The current column manager in charge of creating custom column sets.
        /// </summary>
        private ColumnLayoutManager _columnManager;

        /// <summary>
        ///     The current layout factory when displaying product comparison data.
        /// </summary>
        private ComparisonColumnLayoutFactory _productComparisonLayoutFactory;

        /// <summary>
        ///     The current layout factory when displaying position comparison data.
        /// </summary>
        private ComparisonColumnLayoutFactory _positionComparisonLayoutFactory;

        private readonly Dictionary<PlanogramComparisonFilterType, CustomColumnLayout> _currentColumnLayout = new Dictionary<PlanogramComparisonFilterType, CustomColumnLayout>();

        private GridLength _planogramsPanelWidth;

        private GridLength _legendPanelHeight;

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel",
                                        typeof (PlanogramComparisonPlanDocument),
                                        typeof (PlanogramComparisonPlanDocumentView),
                                        new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        ///     Get/set the <see cref="PlanogramComparisonPlanDocument" /> that is used as view model.
        /// </summary>
        public PlanogramComparisonPlanDocument ViewModel
        {
            get { return (PlanogramComparisonPlanDocument) GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of <see cref="PlanogramComparisonPlanDocumentView" /> using the provided
        ///     <paramref name="document" />.
        /// </summary>
        /// <param name="document">The <see cref="PlanogramComparisonPlanDocument" /> to be used as view model.</param>
        public PlanogramComparisonPlanDocumentView(PlanogramComparisonPlanDocument document)
        {
            //  Show the busy cursor while loading the view.
            Mouse.OverrideCursor = Cursors.Wait;

            //  Initialize the component.
            InitializeComponent();

            //  Update initial size of Planograms Panel.
            _planogramsPanelWidth = new GridLength(250);

            ViewModel = document;

            //  Continue with post loaded initialization when ready.
            Loaded += OnLoaded;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Finalizes the initialization that needs the view to be completely loaded.
        /// </summary>
        private void OnLoaded(Object sender, RoutedEventArgs e)
        {
            Loaded -= OnLoaded;

            //  Update the column set to get the right manager for the current view.
            UpdateColumnSet();

            //  Update initial size of Planograms Panel.
            _planogramsPanelWidth = new GridLength(250);

            //  Remove the busy cursor after the view is fully loaded.
            Dispatcher.BeginInvoke((Action) (() => { Mouse.OverrideCursor = null; }));
        }

        /// <summary>
        ///     Invoked every time the <see cref="ViewModel" /> property changes.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            //  Check that the sender is of the expected type.
            PlanogramComparisonPlanDocumentView view = sender as PlanogramComparisonPlanDocumentView;
            if (view == null) return;

            //  Clean up the old model if there was one.
            PlanogramComparisonPlanDocument oldModel = e.OldValue as PlanogramComparisonPlanDocument;
            if (oldModel != null)
            {
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= view.ViewModelOnPropertyChanged;
            }

            //  Check whether there is a new model or not.
            PlanogramComparisonPlanDocument newModel = e.NewValue as PlanogramComparisonPlanDocument;
            if (newModel == null) return;

            //  Initialize the new model.
            newModel.AttachedControl = view;
            newModel.PropertyChanged += view.ViewModelOnPropertyChanged;
        }

        /// <summary>
        ///     Invoked whenever a property changes on the current view model.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModelOnPropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            //  Update the column set when the view type changes in the document.
            if (e.PropertyName == PlanogramComparisonPlanDocument.FilterTypeProperty.Path) UpdateColumnSet();
        }

        private void PlanogramsPanel_OnCollapsed(Object sender, RoutedEventArgs e)
        {
            Int32 index = Grid.GetColumn((Expander)sender);
            ColumnDefinition column = xContentLayout.ColumnDefinitions[index];
            _planogramsPanelWidth = column.Width; // Store the width of the the panel BEFORE it was collapsed.
            column.Width = GridLength.Auto; // Collapse the panel.
        }

        private void PlanogramsPanel_OnExpanded(Object sender, RoutedEventArgs e)
        {
            Int32 index = Grid.GetColumn((Expander)sender);
            ColumnDefinition column = xContentLayout.ColumnDefinitions[index];
            column.Width = _planogramsPanelWidth; // Restore the panel to the size it had before being collapsed.
        }

        private void LegendPanel_OnCollapsed(Object sender, RoutedEventArgs e)
        {
            Int32 index = Grid.GetRow((Expander)sender);
            RowDefinition column = xDocumentLayout.RowDefinitions[index];
            _legendPanelHeight = column.Height; // Store the height of the the panel BEFORE it was collapsed.
            column.Height = GridLength.Auto; // Collapse the panel.
        }

        private void LegendPanel_OnExpanded(Object sender, RoutedEventArgs e)
        {
            Int32 index = Grid.GetRow((Expander)sender);
            RowDefinition column = xDocumentLayout.RowDefinitions[index];
            column.Height = _legendPanelHeight; // Restore the panel to the size it had before being collapsed.
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Update the column set so that it is the correct one for the current view type.
        /// </summary>
        private void UpdateColumnSet()
        {
            //  Clear the existing column manager, if there is one.
            if (_columnManager != null)
            {
                _columnManager.ColumnSetChanging -= ColumnManagerOnColumnSetChanging;
                _columnManager.CurrentLayoutChanged -= ColumnManagerOnCurrentLayoutChanged;
                _columnManager.ClearGrid();
                _columnManager.Dispose();
                _columnManager = null;
            }

            CustomColumnLayout currentColumnLayout;
            _currentColumnLayout.TryGetValue(ViewModel.FilterType, out currentColumnLayout);

            IColumnLayoutFactory layoutFactory = GetCurrentLayoutFactory();
            String screenKey = GetCurrentScreenKey();

            _columnManager = new ColumnLayoutManager(layoutFactory, ViewModel.Planogram.DisplayUnits, screenKey);
            if (currentColumnLayout != null)
            {
                _columnManager.CurrentColumnLayout = currentColumnLayout;
            }
            else
            {
                _currentColumnLayout[ViewModel.FilterType] = _columnManager.CurrentColumnLayout;
            }

            _columnManager.ColumnSetChanging += ColumnManagerOnColumnSetChanging;
            _columnManager.CurrentLayoutChanged += ColumnManagerOnCurrentLayoutChanged;
            _columnManager.AttachDataGrid(xMainGrid);
        }

        private String GetCurrentScreenKey()
        {
            switch (ViewModel.FilterType)
            {
                case PlanogramComparisonFilterType.Products:
                    return ProductsScreenKey;
                case PlanogramComparisonFilterType.Positions:
                    return PositionsScreenKey;
                default:
                    Debug.Fail("Unsupported View Type when calling GetCurrentScreenKey.");
                    break;
            }

            return ProductsScreenKey;
        }

        /// <summary>
        ///     Invoked whenever the current layout has changed.
        /// </summary>
        private void ColumnManagerOnCurrentLayoutChanged(Object sender, EventArgs e)
        {
            //  Update the current layout with the changes.
            _currentColumnLayout[ViewModel.FilterType] = _columnManager.CurrentColumnLayout;
        }

        private void ColumnManagerOnColumnSetChanging(Object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            DataGridColumn newColumn;
            PlanogramItemType planItemType;
            Type propertyTypeString = typeof (String);
            switch (ViewModel.FilterType)
            {
                case PlanogramComparisonFilterType.Products:
                    planItemType = PlanogramItemType.Product;

                    //  Add an initial column with the GTIN.
                    newColumn = CommonHelper.CreateReadOnlyColumn(KeyColumnBindingPath, propertyTypeString, Message.PlanogramComparisonPlanDocument_KeyColumnHeader_Gtin, ModelPropertyDisplayType.None, ViewModel.Planogram.DisplayUnits);
                    columnSet.Insert(0, newColumn);
                    break;

                case PlanogramComparisonFilterType.Positions:
                    planItemType = PlanogramItemType.Position;

                    //  Add an initial column with the position key.
                    newColumn = CommonHelper.CreateReadOnlyColumn(PositionInstanceColumnBindingPath, propertyTypeString, Message.PlanogramComparisonPlanDocument_KeyColumnHeader_PositionId, ModelPropertyDisplayType.None, ViewModel.Planogram.DisplayUnits);
                    columnSet.Insert(0, newColumn);

                    //  Add an initial column with the GTIN.
                    newColumn = CommonHelper.CreateReadOnlyColumn(GtinColumnBindingPath, propertyTypeString, Message.PlanogramComparisonPlanDocument_KeyColumnHeader_Gtin, ModelPropertyDisplayType.None, ViewModel.Planogram.DisplayUnits);
                    columnSet.Insert(0, newColumn);
                    break;

                default:
                    Debug.Fail("Comparison view type not implemented.");
                    return;
            }

            //  Fetch the Comparison we are getting columns ready for.
            Framework.Planograms.Model.PlanogramComparison comparison = ViewModel.Planogram.Model.Comparison;

            if (comparison.DataOrderType == DataOrderType.ByPlanogram) 
                GroupColumnsByPlanogram(columnSet, comparison, planItemType);
            else
                GroupColumnsByAttribute(columnSet, comparison, planItemType);
        }

        private void GroupColumnsByPlanogram(DataGridColumnCollection columnSet, Framework.Planograms.Model.PlanogramComparison comparison, PlanogramItemType planItemType)
        {
            //  NB: No need to prefetch the binding paths if we are not removing duplicate columns, see note below.
            //List<String> bindingPathsForColumnSet = columnSet.Where(column => column.Visibility == Visibility.Visible).Select(GetBindingPath).ToList();

            //  Add a column per compared field for the Master Planogram.
            columnSet.Add(ToPlanogramResultDataGridColumn(ViewModel.Planogram.Name, StatusColumnBindingPath, typeof (PlanogramItemComparisonStatusType)));

            //  For each field of the appropriate type that is to be displayed...
            //      Create a new column.
            foreach (PlanogramComparisonField field in comparison.GetOrderedFieldsOfType(planItemType))
            {
                String path = field.FieldPlaceholder.Replace("[", String.Empty).Replace("]", String.Empty);
                // NB: for now, lets keep the duplicate columns if the user decides to re add them manually,
                //  that way the comparison columns don't get messed up.
                //if (bindingPathsForColumnSet.Any(bindingPath => bindingPath == path)) continue;
                DataGridColumn column = ToPlanogramValueDataGridColumn(ViewModel.Planogram.Name, field, String.Format("Value{0}", path));
                
                columnSet.Add(column);
            }

            //  Add a column group per Planogram Result.
            foreach (String planName in comparison.Results.Select(result => result.PlanogramName))
            {
                columnSet.Add(ToPlanogramResultDataGridColumn(planName, ResultsColumnBindingPath, typeof (PlanogramComparisonItemStatusType)));

                //  For each field of the appropriate type that is to be displayed...
                //      Create a new column.
                foreach (PlanogramComparisonField field in comparison.GetOrderedFieldsOfType(planItemType)) 
                {
                    columnSet.Add(ToPlanogramValueDataGridColumn(planName, field, ValuesColumnBindingPath));
                }
            }
        }

        private void GroupColumnsByAttribute(DataGridColumnCollection columnSet, Framework.Planograms.Model.PlanogramComparison comparison, PlanogramItemType planItemType)
        {
            //  NB: No need to prefetch the binding paths if we are not removing duplicate columns, see note below.
            //List<String> bindingPathsForColumnSet = columnSet.Where(column => column.Visibility == Visibility.Visible).Select(GetBindingPath).ToList();

            //  Add a column group for the status.
            columnSet.Add(ToPlanogramResultDataGridColumn(ViewModel.Planogram.Name, StatusColumnBindingPath, typeof(PlanogramItemComparisonStatusType), DataOrderType.ByAttribute));
            foreach (String planName in comparison.Results.Select(result => result.PlanogramName))
            {
                columnSet.Add(ToPlanogramResultDataGridColumn(planName, ResultsColumnBindingPath, typeof(PlanogramComparisonItemStatusType), DataOrderType.ByAttribute));
            }

            //  Add a column group per attribute.
            foreach (PlanogramComparisonField field in comparison.GetOrderedFieldsOfType(planItemType))
            {
                String path = field.FieldPlaceholder.Replace("[", String.Empty).Replace("]", String.Empty);
                // NB: for now, lets keep the duplicate columns if the user decides to re add them manually,
                //  that way the comparison columns don't get messed up.
                //if (bindingPathsForColumnSet.Any(bindingPath => bindingPath == path)) continue;
                columnSet.Add(ToPlanogramValueDataGridColumn(ViewModel.Planogram.Name, field, String.Format("Value{0}", path), DataOrderType.ByAttribute));
                foreach (String planName in comparison.Results.Select(result => result.PlanogramName))
                {
                    columnSet.Add(ToPlanogramValueDataGridColumn(planName, field, ValuesColumnBindingPath, DataOrderType.ByAttribute));
                }
            }
        }

        private DataGridColumn ToPlanogramResultDataGridColumn(String planName, String bindingPathMask, Type resultType, DataOrderType dataOrderType = DataOrderType.ByPlanogram)
        {
            String header = dataOrderType == DataOrderType.ByPlanogram ? Message.PlanogramComparisonPlanDocument_ResultsColumnHeader_Status : planName;
            String columnGroup = dataOrderType == DataOrderType.ByPlanogram ? planName : Message.PlanogramComparisonPlanDocument_ResultsColumnHeader_Status;
            DataGridColumn readOnlyColumn =
                CommonHelper.CreateReadOnlyColumn(String.Format(bindingPathMask, planName),
                                                  resultType,
                                                  header,
                                                  ModelPropertyDisplayType.None,
                                                  ViewModel.Planogram.DisplayUnits,
                                                  columnGroup);
            var column = readOnlyColumn as DataGridExtendedTextColumn;
            if (column == null) return readOnlyColumn;
            
            column.Binding.FallbackValue = PlanogramComparisonItemStatusTypeHelper.FriendlyNames[PlanogramComparisonItemStatusType.NotFound];
            column.HeaderGroupNames.Add(new DataGridHeaderGroup {HeaderName = columnGroup});
            return readOnlyColumn;
        }

        private DataGridColumn ToPlanogramValueDataGridColumn(String planName, IPlanogramComparisonSettingsField field, String bindingPathMask, DataOrderType dataOrderType = DataOrderType.ByPlanogram)
        {
            String header = dataOrderType == DataOrderType.ByPlanogram ? field.DisplayName : planName;
            String columnGroup = dataOrderType == DataOrderType.ByPlanogram ? planName : field.DisplayName;
            DataGridColumn readOnlyColumn =
                CommonHelper.CreateReadOnlyColumn(String.Format(bindingPathMask, planName, field.FieldPlaceholder),
                                                  typeof (String),
                                                  header,
                                                  ModelPropertyDisplayType.None,
                                                  ViewModel.Planogram.DisplayUnits,
                                                  columnGroup,
                                                  field.Display);
            var column = readOnlyColumn as DataGridExtendedTextColumn;
            if (column == null) return readOnlyColumn;
            
            column.Binding.FallbackValue = "N/A";
            column.HeaderGroupNames.Add(new DataGridHeaderGroup { HeaderName = columnGroup });
            return readOnlyColumn;
        }

        /// <summary>
        ///     Get or set the current column layout depending on the view type.
        /// </summary>
        private IColumnLayoutFactory GetCurrentLayoutFactory()
        {
            //  Create a new column manager for the view type.
            IColumnLayoutFactory layoutFactory = null;
            switch (ViewModel.FilterType)
            {
                case PlanogramComparisonFilterType.Products:
                    layoutFactory = _productComparisonLayoutFactory ?? (_productComparisonLayoutFactory = new ComparisonColumnLayoutFactory(typeof (Object), PlanogramItemType.Product, ViewModel.Planogram.Model));
                    break;
                case PlanogramComparisonFilterType.Positions:
                    layoutFactory = _positionComparisonLayoutFactory ?? (_positionComparisonLayoutFactory = new ComparisonColumnLayoutFactory(typeof (Object), PlanogramItemType.Position, ViewModel.Planogram.Model));
                    break;
                default:
                    Debug.Fail("Comparison view type not implemented.");
                    break;
            }
            return layoutFactory;
        }

        #endregion

        #region IPlanDocumentView Members

        /// <summary>
        ///     Get the view's view model.
        /// </summary>
        /// <returns>The <see cref="IPlanDocument" /> currently used as view model.</returns>
        public IPlanDocument GetIPlanDocument()
        {
            return ViewModel;
        }

        #endregion

        #region Static Helper Methods

        /// <summary>
        ///     Get the path for the binding in the given <paramref name="column"/>.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        /// <remarks>If the the column somehow cannot be bound, or the binding is empty the corresponding warning will be returned.</remarks>
        private static String GetBindingPath(DataGridColumn column)
        {
            var extendedColumn = column as DataGridBoundColumn;
            if (extendedColumn == null) return "Not valid column";

            var binding = extendedColumn.Binding as Binding;
            return binding == null ? "Not Bound" : binding.Path.Path;
        }

        #endregion

        #region IDisposable Members

        private Boolean IsDisposed { get; set; }

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(Boolean isDisposing)
        {
            if (IsDisposed) return;

            if (isDisposing)
            {
                // Dispose of the view model to avoid a memory leak.
                ViewModel = null;
            }

            IsDisposed = true;
        }

        #endregion
    }
}