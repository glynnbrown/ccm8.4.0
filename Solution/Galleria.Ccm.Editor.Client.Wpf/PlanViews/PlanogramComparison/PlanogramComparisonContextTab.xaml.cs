﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31747 : A.Silva
//  Created.

#endregion

#endregion

using System.Windows;

namespace Galleria.Ccm.Editor.Client.Wpf.PlanViews.PlanogramComparison
{
    /// <summary>
    /// Interaction logic for ComponentContextTab.xaml
    /// </summary>
    public sealed partial class PlanogramCompareContextTab
    {
        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanogramComparisonPlanDocument), typeof(PlanogramCompareContextTab),
            new PropertyMetadata(null));

        public PlanogramComparisonPlanDocument ViewModel
        {
            get { return (PlanogramComparisonPlanDocument)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        public PlanogramCompareContextTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}